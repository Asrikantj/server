package com.computhink.vwc.notification;

import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.computhink.common.Document;
import com.computhink.common.Notification;
import com.computhink.common.NotificationSettings;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.image.Images;
import com.computhink.vws.server.Client;

public class VWDocNotification implements VWNotificationConstants, Runnable {

	private static JDialog dlgDocNotification;
	private VWNotificationSettingsPanel settingsPanel;
	private JPanel buttonPanel;
	private ResourceManager resourceManager=null;
	private JButton btnSave;
	private JButton btnCancel;
	private JButton btnHelp;

	private static boolean active = false;

	private int sid;
	private WindowAdapter secAdapter;
	private static VWClient vwc;
	private Document doc;
	private boolean windowActive=true;    
	public static boolean inherit = false;
	public static int prevSID;
	public static int prevDocId;

	public static boolean isWebClient = false;

	public VWDocNotification(VWClient vwc, int sid, Document doc, JDialog parent) {
		try{
			if (active)
			{
				if(sid == prevSID && doc != null && doc.getId() == prevDocId){
					dlgDocNotification.requestFocus();
					dlgDocNotification.toFront();
					return;
				}
				else{
					closeDialog();
				}
			}
			new Images();
			this.vwc = vwc;

			if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
					vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
				isWebClient = true;

			this.sid = sid;
			prevSID = sid;
			this.doc = doc;
			if(doc != null)
				prevDocId = doc.getId();
			if (parent == null )
				dlgDocNotification = new JDialog();
			else
				dlgDocNotification = new JDialog(parent);			
			initComponents();
			dlgDocNotification.setVisible(true);
			dlgDocNotification.setModal(true);
			active = true;

			if(parent != null)
				parent.setEnabled(false);
		}catch(Exception e){
			VWClient.printToConsole("Exception in VWDocNotificationSettings : "+e.getMessage() );
		}
	}

	private void initComponents() {
		try{
			resourceManager=ResourceManager.getDefaultManager();
			dlgDocNotification.setTitle(resourceManager.getString("Notification.Title")+doc.getName());
			if(isWebClient)
				dlgDocNotification.setBounds(320, 200, 435, 357); //(320, 200, 394, 362);
			else
				dlgDocNotification.setBounds(320, 200, 394, 362);
			dlgDocNotification.getContentPane().setLayout(null);
			dlgDocNotification.setResizable(false);

			settingsPanel = new VWNotificationSettingsPanel(vwc, sid, DOCUMENT);
			//settingsPanel.setBorder(new LineBorder(Color.red));
			dlgDocNotification.getContentPane().add(settingsPanel);

			buttonPanel = new JPanel();
			buttonPanel.setLayout(null);
			//buttonPanel.setBorder(new LineBorder(Color.black));
			if(isWebClient)
				buttonPanel.setBounds(0, 285, 435, 43);
			else
				buttonPanel.setBounds(0, 285, 407, 43);

			btnHelp = new JButton();
			btnHelp.setBounds(10, 10, 20, 20);
			btnHelp.setIcon(Images.help);
			btnHelp.setMnemonic('H');
			btnHelp.setBorderPainted(false);
			btnHelp.setFocusPainted(false);
			btnHelp.setContentAreaFilled(false);
			buttonPanel.add(btnHelp);

			btnSave = new JButton(resourceManager.getString("btnSave.Save"));
			if(isWebClient)
				btnSave.setBounds(250, 10, 80, 23);
			else
				btnSave.setBounds(207, 10, 80, 23);
			buttonPanel.add(btnSave);

			btnCancel = new JButton(resourceManager.getString("btnCancel.Cancel"));
			if(isWebClient)
				btnCancel.setBounds(340, 10, 80, 23);
			else
				btnCancel.setBounds(297, 10, 80, 23);
			buttonPanel.add(btnCancel);

			SymAction symAction = new SymAction();
			btnSave.addActionListener(symAction);
			btnCancel.addActionListener(symAction);
			btnHelp.addActionListener(symAction);

			dlgDocNotification.add(buttonPanel);

			secAdapter = new WindowAdapter(){
				public void windowClosing(WindowEvent evt){
					VWClient.printToConsole("Window Closing ");
					closeDialog();
				}
				public void windowLostFocus(WindowEvent e){
					if(windowActive){
						dlgDocNotification.requestFocus();
						dlgDocNotification.toFront();
					}
				}
				public void windowDeactivated(WindowEvent e) {
					if(windowActive){
						dlgDocNotification.requestFocus();
						dlgDocNotification.toFront();
					}
				}
				public void windowStateChanged(WindowEvent e) {
					dlgDocNotification.toFront();
				}
				public void windowDeiconified(WindowEvent e) {
					dlgDocNotification.toFront();
				}
			};
			dlgDocNotification.addWindowListener(secAdapter);
			loadSettings();
		}catch(Exception e){
			VWClient.printToConsole("Exception while initializing document notification settings : "+e.getMessage() );
		}
	}

	private void loadSettings() {
		try{
			NotificationSettings settings = new NotificationSettings();
			settingsPanel.loadSettings(doc.getId(),settings);
			if (doc.getId() != settings.getReferenceId() && settings.getReferenceId() != 0)
				inherit = true;
			else
				inherit = false;			
		}catch(Exception e){
			VWClient.printToConsole("Exception while loading document NotificationSettings : "+e.getMessage() );
		}
	}

	class SymAction implements java.awt.event.ActionListener{
		public void actionPerformed(ActionEvent event) {
			Object object = event.getSource();
			if(object == btnSave){
				btnSave_actionPerformed(event);
			}else if(object == btnCancel){
				btnCancel_actionPerformed(event);				
			}else if(object == btnHelp){
				btnHelp_actionPerformed(event);
			}			
		}
	}

	public void btnHelp_actionPerformed(ActionEvent event) {

		try{
			String path = VWCUtil.getHome() + "\\help\\VWUSER.chm";
			java.lang.Runtime.getRuntime().exec("hh.exe " + path+"::/vwuserguide/Notification.htm");
		}catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}
	}

	public void btnCancel_actionPerformed(ActionEvent event) {
		closeDialog();		
	}

	public void btnSave_actionPerformed(ActionEvent event) {
		try{
			VWClient.printToConsole("Saving Document Notification Settings ");
			NotificationSettings settings = new NotificationSettings();
			settings.setNodeType(DOCUMENT);
			int ret = settingsPanel.getNotifictionSettings(settings);
			settings.setReferenceId(doc.getId());
			settings.setProcessType(0); // Individual/Bulk option will not be there for Document.
			settings.setFolderInherit(0);
			settings.setDocumentInherit(0);

			if(inherit){
				if(ret >= 0){

					int answer=-2;
					windowActive = false;
					answer = JOptionPane.showConfirmDialog(dlgDocNotification, "Updating will break/alter the inherited values. Do you want to continue?", "Inherit Document Notification Settings", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					windowActive = true;
					if(answer!=-2){
						dlgDocNotification.requestFocus();
						dlgDocNotification.toFront();
					}
					if(answer == JOptionPane.YES_OPTION){
						ret = vwc.saveNotificationSettings(sid, settings);
					}
				}	
			} else {
				if(ret >= 0){
					ret = vwc.saveNotificationSettings(sid, settings);	
				}
			}


			//ret = vwc.saveNotificationSettings(sid, settings);
			VWClient.printToConsole("Return value from document save() : "+ret);
			if(ret >= 0)
				closeDialog();

		}catch(Exception e){
			VWClient.printToConsole("Exception while saving document notification settings : "+e.getMessage());
		}
	}

	public void run() {
		show();
	}

	public void show()
	{
		if (vwc == null || doc == null || doc.getId() <= 0) return;
		/*
       	 * new frame.show(); method is replaced with new frame.setVisible(true); 
       	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
       	 */
		dlgDocNotification.setVisible(true);
	}

	private void closeDialog() {
		active = false;
		dlgDocNotification.getParent().setEnabled(true);
		dlgDocNotification.dispose();		
	}	

	public static void main(String[] args) {
		try {
			/*VWDocNotification dialog = new VWDocNotification();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);*/

			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			VWClient vwc = new VWClient("c:\\temp");
			int sid = vwc.login("10.4.8.149", "VW7", "vwadmin", "vw");
			VWDocNotification vwDocNotification = new VWDocNotification(vwc, sid, new Document(8), new JDialog());
			new Thread(vwDocNotification).start();			
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
