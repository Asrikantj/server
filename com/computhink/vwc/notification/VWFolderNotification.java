package com.computhink.vwc.notification;


import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.computhink.common.Node;
import com.computhink.common.Notification;
import com.computhink.common.NotificationSettings;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.image.Images;
import com.computhink.vws.server.Client;

public class VWFolderNotification implements VWNotificationConstants, Runnable{

	private static ResourceManager connectorManager = ResourceManager.getDefaultManager();
	private static JDialog dlgFolderNotification;
	private VWNotificationSettingsPanel settingsPanel;
	private JPanel buttonPanel;

	private JButton btnSave;
	private JButton btnCancel;
	private JButton btnHelp;
	private JButton btnReset;

	private static boolean active = false;

	private int sid;
	private WindowAdapter secAdapter;
	private static VWClient vwc;
	private Node node;
	private boolean windowActive=true;    
	public static boolean inherit = false;
	public static int prevSID = 0;
	public static int prevNodeId = 0;

	public static boolean isWebClient = false;

	public VWFolderNotification(VWClient vwc, int sid, Node node, JDialog parent) {
		try{
			if (active)
			{
				if(sid == prevSID && node != null && node.getId() == prevNodeId){
					dlgFolderNotification.requestFocus();
					dlgFolderNotification.toFront();
					return;
				}
				else{
					closeDialog();
				}				
			}
			new Images();
			this.vwc = vwc;

			if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
					vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
				isWebClient = true;

			this.sid = sid;
			prevSID = sid;
			this.node = node;
			if(node != null)
				prevNodeId = node.getId();
			if (parent == null )
				dlgFolderNotification = new JDialog();
			else
				dlgFolderNotification = new JDialog(parent);
			initComponents();
			active = true;

			if(parent != null)
				parent.setEnabled(false);
		}catch(Exception e){
			VWClient.printToConsole("Exception in VWFolderNotificationSettings : "+e.getMessage() );
		}
	}

	private void initComponents() {
		try{
			dlgFolderNotification.setTitle(connectorManager.getString("foldernotification.title")+node.getName());

			if(isWebClient)
				dlgFolderNotification.setBounds(320, 200, 435, 357);
			else
				dlgFolderNotification.setBounds(320, 200, 394, 362);
			dlgFolderNotification.getContentPane().setLayout(null);
			dlgFolderNotification.setResizable(false);

			settingsPanel = new VWNotificationSettingsPanel(vwc, sid, FOLDER);
			if(isWebClient)
				settingsPanel.setBounds(0, 0, 420, 290);
			else
				settingsPanel.setBounds(0, 0, 388, 290);
			//settingsPanel.setBorder(new LineBorder(Color.red));

			dlgFolderNotification.getContentPane().add(settingsPanel);

			buttonPanel = new JPanel();
			buttonPanel.setLayout(null);
			//buttonPanel.setBorder(new LineBorder(Color.black));
			if(isWebClient)
				buttonPanel.setBounds(0, 285, 435, 43);
			else
				buttonPanel.setBounds(0, 285, 407, 43);

			int y = 10, height = 23;
			btnHelp = new JButton();
			btnHelp.setBounds(10, y, 20, height-3);
			btnHelp.setIcon(Images.help);
			btnHelp.setMnemonic('H');
			btnHelp.setBorderPainted(false);
			btnHelp.setFocusPainted(false);
			btnHelp.setContentAreaFilled(false);
			buttonPanel.add(btnHelp);

			btnReset = new JButton(connectorManager.getString("general.reset"));
			if(isWebClient)
				btnReset.setBounds(160, y, 80, height);
			else
				btnReset.setBounds(117, y, 80, height);
			buttonPanel.add(btnReset);


			btnSave = new JButton(connectorManager.getString("general.save"));
			if(isWebClient)
				btnSave.setBounds(250, y, 80, height);
			else
				btnSave.setBounds(207, y, 80, height);
			buttonPanel.add(btnSave);

			btnCancel = new JButton(connectorManager.getString("general.cancel"));
			if(isWebClient)
				btnCancel.setBounds(340, y, 80, height);
			else
				btnCancel.setBounds(297, y, 80, height);
			buttonPanel.add(btnCancel);

			SymAction symAction = new SymAction();
			btnSave.addActionListener(symAction);
			btnCancel.addActionListener(symAction);
			btnHelp.addActionListener(symAction);
			btnReset.addActionListener(symAction);

			dlgFolderNotification.add(buttonPanel);

			secAdapter = new WindowAdapter(){
				public void windowClosing(WindowEvent evt){
					VWClient.printToConsole("Window Closing ");
					closeDialog();
				}
				public void windowLostFocus(WindowEvent e){
					if(windowActive){
						dlgFolderNotification.requestFocus();
						dlgFolderNotification.toFront();
					}
				}
				public void windowDeactivated(WindowEvent e) {
					if(windowActive){
						dlgFolderNotification.requestFocus();
						dlgFolderNotification.toFront();
					}
				}
				public void windowStateChanged(WindowEvent e) {
					dlgFolderNotification.toFront();
				}
				public void windowDeiconified(WindowEvent e) {
					dlgFolderNotification.toFront();
				}
			};
			dlgFolderNotification.addWindowListener(secAdapter);
			loadSettings();
			dlgFolderNotification.setVisible(true);
		}catch(Exception e){
			VWClient.printToConsole("Exception while initializing Folder notification settings: "+e.getMessage() );
		}
	}

	private void loadSettings() {
		try{
			NotificationSettings settings = new NotificationSettings();
			settings.setFolderInherit(1); //By Default Inherit has to be checked for folder settings
			settings.setDocumentInherit(1);
			settingsPanel.loadSettings(node.getId(),settings);
			if (node.getId() != settings.getReferenceId() && settings.getReferenceId() != 0)
				inherit = true;
			else
				inherit = false;
		}catch(Exception e){
			VWClient.printToConsole("Exception in Folder loadSettings : "+e.getMessage() );
		}
	}

	class SymAction implements java.awt.event.ActionListener{
		public void actionPerformed(ActionEvent event) {
			Object object = event.getSource();
			if(object == btnSave){
				btnSave_actionPerformed(event);
			}else if(object == btnCancel){
				btnCancel_actionPerformed(event);				
			}else if(object == btnHelp){
				btnHelp_actionPerformed(event);
			}else if(object == btnReset){
				btnReset_actionPerformed(event);
			}			
		}
	}

	public void btnReset_actionPerformed(ActionEvent event) {
		settingsPanel.unCheckAll(settingsPanel.model);
	}


	public void btnHelp_actionPerformed(ActionEvent event) {
		try{
			String path = VWCUtil.getHome() + "\\help\\VWUSER.chm";
			java.lang.Runtime.getRuntime().exec("hh.exe " + path+"::/vwuserguide/Notification.htm");
		}catch (Exception e) {
			// TODO: handle exception
			//e.printStackTrace();
		}
	}

	public void btnCancel_actionPerformed(ActionEvent event) {
		closeDialog();
	}

	public void btnSave_actionPerformed(ActionEvent event) {
		try{
			VWClient.printToConsole("Saving Document Notification Settings ");
			NotificationSettings settings = new NotificationSettings();
			settings.setNodeType(FOLDER);
			int isEventSelected = settingsPanel.getNotifictionSettings(settings);			
			settings.setReferenceId(node.getId());			
			//settings.setAttachment("00"); // No Attachment option for folder

			if(inherit){				 
				if(isEventSelected >= 0){
					int answer=-2;
					windowActive = false;
					answer = JOptionPane.showConfirmDialog(dlgFolderNotification, connectorManager.getString("foldernotification.jmsg1"),connectorManager.getString("foldernotification.jtitle1"), JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
					windowActive = true;
					if(answer!=-2){
						dlgFolderNotification.requestFocus();
						dlgFolderNotification.toFront();
					}
					if(answer == JOptionPane.YES_OPTION){
						int ret = vwc.saveNotificationSettings(sid, settings);
					}
				}					 				 
			}else{
				if(isEventSelected >= 0){
					int ret = vwc.saveNotificationSettings(sid, settings);
				}
			}
			if(isEventSelected == -1){

				Notification[] emptyNotification = new Notification[1];
				emptyNotification[0] = new Notification();
				emptyNotification[0].setId(-1);
				emptyNotification[0].setModule("none");
				settings.setNotifications(emptyNotification);
				int ret = vwc.saveNotificationSettings(sid, settings);
			}
			if(isEventSelected != -2)
				closeDialog();
		}catch(Exception e){
			VWClient.printToConsole("Exception while saving folder notification settings : "+e.getMessage());
		}

	}

	public void run() {
		show();
	}
	public void show()
	{
		if (vwc == null || node == null || node.getId() <= 0) return;
		dlgFolderNotification.setVisible(true);
	}
	private void closeDialog() {
		active = false;
		dlgFolderNotification.getParent().setEnabled(true);
		dlgFolderNotification.dispose();		
	}


	public static void main(String[] args) {
		try {
			/*VWFolderNotification dialog = new VWFolderNotification();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);*/

			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			VWClient vwc = new VWClient("c:\\temp");
			int sid = vwc.login("10.4.8.149", "VW7", "vwadmin", "vw");
			VWFolderNotification vwFolderNotification = new VWFolderNotification(vwc, sid, new Node(8), new JDialog());
			new Thread(vwFolderNotification).start();	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}

}
