package com.computhink.vwc.notification;

import java.awt.Color;
import java.awt.Component;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import org.japura.gui.CheckList;
import org.japura.gui.event.ListCheckListener;
import org.japura.gui.event.ListEvent;
import org.japura.gui.model.DefaultListCheckModel;
import org.japura.gui.model.ListCheckModel;

import com.computhink.common.Constants;
import com.computhink.common.Notification;
import com.computhink.common.NotificationSettings;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.Session;
import com.computhink.vwc.VWClient;
import com.computhink.vws.server.Client;

public class VWNotificationSettingsPanel extends JPanel implements VWNotificationConstants, Constants{
	//JPanel settingsPanel = new JPanel();

	private static ResourceManager connectorManager = ResourceManager.getDefaultManager();
	String languageLocale=connectorManager.getLocale().toString();
	private JLabel lblNotifyMeWhen = new JLabel(connectorManager.getString("notificationpanel.label1"));
	private JLabel lblNotifyEvent = new JLabel(connectorManager.getString("notificationpanel.label2"));;
	private JLabel lblNotifyMail = new JLabel(connectorManager.getString("notificationpanel.label3"));
	private JLabel lblUserMailid = new JLabel(connectorManager.getString("notificationpanel.label4"));
	private JLabel lblLoggedUserMail = new JLabel();
	private JLabel lblAdditionalEmails = new JLabel(connectorManager.getString("notificationpanel.label5"));
	private JLabel lblInclude = new JLabel(connectorManager.getString("notificationpanel.label6"));
	private JLabel lblInherit = new JLabel(connectorManager.getString("notificationpanel.label7"));

	private JRadioButton rdbtnOneTime = new JRadioButton(connectorManager.getString("notificationpanel.radiobutton1"));
	private JRadioButton rdbtnContinuous = new JRadioButton(connectorManager.getString("notificationpanel.radiobutton2"));
	private JRadioButton rdbtnIndividual = new JRadioButton(connectorManager.getString("notificationpanel.radiobutton3"));
	private JRadioButton rdbtnBulk = new JRadioButton(connectorManager.getString("notificationpanel.radiobutton4"));

	private JCheckBox chckbxViewwiseReference = new JCheckBox(connectorManager.getString("CVProduct.Name") +" "+connectorManager.getString("notificationpanel.checkbox1"));
	private JCheckBox chckbxDocumentHistory = new JCheckBox(connectorManager.getString("notificationpanel.checkbox2"));
	private JCheckBox chckbxFolderInherit = new JCheckBox(connectorManager.getString("notificationpanel.checkbox3"));
	private JCheckBox chckbxDocumentInherit = new JCheckBox(connectorManager.getString("notificationpanel.checkbox4"));

	private JTextArea txtrAdditionalEmails = new JTextArea(3, 20);
	private JScrollPane scrPane = new JScrollPane(txtrAdditionalEmails);

	public List notificationList = new List();
	public ListCheckModel model = new DefaultListCheckModel();
	public CheckList checkList;
	public JScrollPane spCheckList;

	private int sid;
	Session session = null;
	private static VWClient vwc;
	private int NOTIFICATION_TYPE = -1;
	private String notificationModule = "";
	private Notification[] currentNotificationList = null;

	public static boolean isWebClient = false;

	public VWNotificationSettingsPanel(VWClient vwc, int sid, int notificationType) {	
		try{
			this.vwc = vwc;

			if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
					vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
				isWebClient = true;

			this.sid = sid;
			session = vwc.getSession(sid);
			NOTIFICATION_TYPE = notificationType;
			notificationModule = NotificationModules[notificationType];
			initComponents();
		}catch(Exception e){
			VWClient.printToConsole("Exception in VWNotificationSettingsPanel() constructor : "+e.getMessage());
		}
	}

	public void updateSessionInfo(VWClient vwc, int sid, int selectedModuleIndex){
		this.vwc = vwc;
		this.sid = sid;
		this.NOTIFICATION_TYPE = selectedModuleIndex;
	}

	private void initComponents() {
		try{
			VWClient.printToConsole("Initializing Notification Settings Panel");
			int x=10, y=12, h = 20, x1 = 100, x2 = 230, gap=5;
			if(languageLocale.equals("nl_NL")){
				x1=170;
				x2=290;
			}
			if(isWebClient){
				x1 = 115;
				if(NOTIFICATION_TYPE == FOLDER)
					x2 = 250;
				else
					x2 = 290;
			}
			setLayout(null);
			setBorder(new EmptyBorder(5, 5, 5, 5));
			if(isWebClient)
				setBounds(0, 0, 420, 290);
			else
				setBounds(0, 0, 388, 290);
			//setBorder(new LineBorder(Color.red));

			RadioBtnListener rbListener = new RadioBtnListener();
			if(NOTIFICATION_TYPE != DOCUMENT && NOTIFICATION_TYPE != FOLDER){
				setBackground(Color.white);
				setBounds(0, 0, 388, 315);
			}

			if(isWebClient)
				lblNotifyMeWhen.setBounds(x, y, 95, h);
			else{
				if(languageLocale.equals("nl_NL")){
					lblNotifyMeWhen.setBounds(x, y, 87+50, h);
				}else
				lblNotifyMeWhen.setBounds(x, y, 87, h);
			}
			add(lblNotifyMeWhen);

			//checkList = getCheckList(NOTIFICATION_TYPE);
			checkList = new CheckList();
			getCheckList(NOTIFICATION_TYPE);
			spCheckList = new JScrollPane(checkList);

			if(isWebClient)
				spCheckList.setBounds(x1, y, 305, 115);
			else
				spCheckList.setBounds(x1, y, 276, 115);
			add(spCheckList);

			y += 115+(gap*2);

			lblNotifyEvent.setBounds(x, y, 87, h);
			add(lblNotifyEvent);
			rdbtnOneTime.setBounds(x1, y, 109, h);
			rdbtnOneTime.setSelected(true);
			add(rdbtnOneTime);
			rdbtnOneTime.setBackground(getBackground());

			rdbtnContinuous.setBounds(x2, y, 144, h);
			add(rdbtnContinuous);
			rdbtnContinuous.setBackground(getBackground());

			//For folder also bulk and individual is not required.
			/*if(NOTIFICATION_TYPE == FOLDER){
				y += h+gap;
				lblNotifyMail.setBounds(x, y, 87, h);
				add(lblNotifyMail);
				rdbtnIndividual.setBounds(x1, y, 109, h);
				rdbtnIndividual.setSelected(true);
				add(rdbtnIndividual);
				rdbtnIndividual.setBackground(getBackground());
				rdbtnIndividual.addActionListener(rbListener);
				rdbtnBulk.setBounds(x2, y, 109, h);
				add(rdbtnBulk);
				rdbtnBulk.setBackground(getBackground());
				rdbtnBulk.addActionListener(rbListener);
			}*/
			y += h+(gap*2);
			if(languageLocale.equals("nl_NL")){
				lblUserMailid.setBounds(x, y, 70+50, h);
			}else
			lblUserMailid.setBounds(x, y, 70, h);
			add(lblUserMailid);
			lblLoggedUserMail = new JLabel(getUserMailId());
			lblLoggedUserMail.setBounds(x1, y, 276, h);
			add(lblLoggedUserMail);

			y += h+gap;

			if(isWebClient)
				lblAdditionalEmails.setBounds(x, y, 105, h);
			else{
				if(languageLocale.equals("nl_NL")){
					lblAdditionalEmails.setBounds(x, y, 88+50, h);
				}else
				lblAdditionalEmails.setBounds(x, y, 88, h);
			}
			add(lblAdditionalEmails);
			txtrAdditionalEmails.setToolTipText(connectorManager.getString("notificationpanel.emailtolltip"));
			if(isWebClient)
				scrPane.setBounds(x1, y, 305, 60);
			else
				scrPane.setBounds(x1, y, 276, 60);
			add(scrPane);

			if(NOTIFICATION_TYPE == DOCUMENT ){
				y += 60+(gap*2);
				lblInclude.setBounds(x, y, 46, h);
				add(lblInclude);

				if(isWebClient)
					chckbxViewwiseReference.setBounds(x1, y, 175, h);
				else
					chckbxViewwiseReference.setBounds(x1, y, 124, h);
				add(chckbxViewwiseReference);
				chckbxViewwiseReference.setBackground(getBackground());

				if(isWebClient)
					chckbxDocumentHistory.setBounds(x2, y, 135, h);
				else
					chckbxDocumentHistory.setBounds(x2, y, 117, h);
				add(chckbxDocumentHistory);
				chckbxDocumentHistory.setBackground(getBackground());
			}

			if(NOTIFICATION_TYPE == FOLDER){
/*				lblInclude.setBounds(x, y, 46, h);
				add(lblInclude);
*/				y += 60+(gap*2);

				 lblInherit.setBounds(x, y, 46, h);
				 add(lblInherit);

				 chckbxFolderInherit.setBounds(x1, y, 130, 20);
				 chckbxFolderInherit.setSelected(true);
				 add(chckbxFolderInherit);
				 if(isWebClient)
					 chckbxDocumentInherit.setBounds(x2, y, 150, 20);
				 else
					 chckbxDocumentInherit.setBounds(230, y, 130, 20);
				 chckbxDocumentInherit.setSelected(true);
				 add(chckbxDocumentInherit);

//				 lblInclude.setEnabled(false);
				 /*chckbxViewwiseReference.setEnabled(false);
				chckbxDocumentHistory.setEnabled(false);*/				
			}

			rdbtnOneTime.addActionListener(rbListener);
			rdbtnContinuous.addActionListener(rbListener);
			CheckListener checkListener = new CheckListener();
			model.addListCheckListener(checkListener);
		}catch(Exception ex){
			VWClient.printToConsole("Exception while Initializing Notification Settings Panel : "+ex.getMessage());
			ex.printStackTrace();
		}
	}

	class CheckListener implements ListCheckListener{
		public void addCheck(ListEvent event) {
			try{
				ListCheckModel src = event.getSource();
				if(NOTIFICATION_TYPE == DOCUMENT){
					checkAll(src);				
				}else if(NOTIFICATION_TYPE == FOLDER){
					checkFolderNotifications(src);
				}else if(NOTIFICATION_TYPE == ROUTE || NOTIFICATION_TYPE == RETENTION){
					/**
					 * Need to have Delete Route/ Delete Retention option always at last.
					 */
					checkLastEvent(src);
				}
			}catch(Exception e){
				VWClient.printToConsole("Exception while adding checks () : "+e.getMessage());
				e.printStackTrace();
			}
		}

		public void removeCheck(ListEvent event) {
			try{
				ListCheckModel src = event.getSource();				
				if(NOTIFICATION_TYPE == DOCUMENT){
					//unCheckAll(src);
					if(model.isChecked(model.getElementAt(0)) && (model.getChecksCount() == (model.getSize()-1)))
						model.removeCheck(model.getElementAt(0));
				}
				else if(NOTIFICATION_TYPE == FOLDER){
					uncheckFolderNotification(src);
				}else if(NOTIFICATION_TYPE == ROUTE || NOTIFICATION_TYPE == RETENTION){
					/**
					 * Need to have Delete Route/ Delete Retention option always at last.
					 */
					removeLastEvent(src);
				}
			}catch(Exception e){
				VWClient.printToConsole("Exception while removing checks () : "+e.getMessage());
				e.printStackTrace();
			}
		}
	}
	private void removeLastEvent(ListCheckModel src) {
		boolean remove = true;
		/**
		 * Need to have Delete Route/ Delete Retention option always at last.
		 */
		for(int i=0; i<model.getSize()-1; i++){
			if(model.isChecked(model.getElementAt(i))){
				remove = false;
				break;//i=model.getSize();
			}
		}
		if(remove){
			int lastEvent = model.getSize()-1;
			if(model.isChecked(model.getElementAt(lastEvent)) && model.isLocked(model.getElementAt(lastEvent)))
				model.removeCheck(model.getElementAt(lastEvent));
			model.removeLock(model.getElementAt(lastEvent));
		}
	}
	private void checkLastEvent(ListCheckModel src) {
		/**
		 * Need to have Delete Route/ Delete Retention option always at last.
		 */
		if(model.getElementAt(model.getSize()-1).toString().trim().equalsIgnoreCase("Delete "+ WORKFLOW_MODULE_NAME) || 
				model.getElementAt(model.getSize()-1).toString().trim().equalsIgnoreCase("Delete Retention")){
			model.addCheck(model.getElementAt(model.getSize()-1));
			model.addLock(model.getElementAt(model.getSize()-1));
		}
	}

	public Notification[] getNotificationForModule(int notificationType){
		Vector notificationVector = new Vector();								
		Vector result = new Vector();
		vwc.getNotificationListByModule(sid, NotificationModules[notificationType]  , result);
		notificationVector.addAll(result);
		Notification[] notificationList = new Notification[notificationVector.size()];
		for (int i = 0; i < notificationVector.size(); i++){
			notificationList[i] = (Notification)notificationVector.get(i);				
		}			
		return notificationList;
	}

	private void checkFolderNotifications(ListCheckModel src) {
		try{
			//Notification[] folderList = FOLDER_NOTIFICATION_LIST;
			//Notification[] docList = DOCUMENT_NOTIFICATION_LIST;			
			Notification[] folderList = getNotificationForModule(FOLDER);
			Notification[] docList = getNotificationForModule(DOCUMENT);

			int fLen = folderList.length;
			int dLen = docList.length;

			int count = getCheckCount(0, fLen, src);
			if((src.isChecked(folderList[0].getNotifyDesc())) || (count == (fLen-1))){
				checkEvents(0, fLen);
			}

			count = getCheckCount(fLen, model.getSize(), src);
			if((src.isChecked(docList[0].getNotifyDesc())) || (count == (dLen-1))){
				checkEvents(fLen, model.getSize());				
			}

			if(count > 0){
				/*chckbxFolderInherit.setSelected(true);
				chckbxFolderInherit.setEnabled(true);
				chckbxDocumentInherit.setSelected(true);
				chckbxDocumentInherit.setEnabled(true);				
				if(rdbtnIndividual.isSelected()){
					lblInclude.setEnabled(true);
					chckbxDocumentHistory.setEnabled(true);
					chckbxViewwiseReference.setEnabled(true);
				}*/
			}

		}catch(Exception e){
			VWClient.printToConsole("Exception while checking all : "+e.getMessage());
		}			
	}

	private void checkEvents(int start, int end) {
		for(int i=start; i<end; i++){
			model.addCheck(model.getElementAt(i));			
		}		
	}

	private int getCheckCount(int start, int end, ListCheckModel src) {
		int count = 0;
		for(int i=start; i<end; i++){
			if(src.isChecked(model.getElementAt(i))){
				count++;
			}
		}
		return count;
	}

	public void uncheckFolderNotification(ListCheckModel src) {
		try{
			int count = 0;
			Notification[] folderList =  getNotificationForModule(FOLDER);//FOLDER_NOTIFICATION_LIST;
			Notification[] docList = getNotificationForModule(DOCUMENT);//DOCUMENT_NOTIFICATION_LIST;

			int fLen = folderList.length;
			int dLen = docList.length;

			count = getCheckCount(0, fLen, src);
			String temp = folderList[0].getNotifyDesc();
			if(src.isChecked(temp) && count == (fLen-1)){
				model.removeCheck(temp);
			}

			count = getCheckCount(fLen, model.getSize(), src);
			temp = docList[0].getNotifyDesc();
			if(src.isChecked(temp)&& count == (docList.length-1)){
				model.removeCheck(temp);
			}

			if(count == 0){
				//chckbxInherit.setSelected(false);
				chckbxFolderInherit.setEnabled(true);
				chckbxDocumentInherit.setEnabled(true);

				lblInclude.setEnabled(false);
				chckbxDocumentHistory.setEnabled(false);
				chckbxDocumentHistory.setSelected(false);

				chckbxViewwiseReference.setEnabled(false);
				chckbxViewwiseReference.setSelected(false);
			}
		}catch(Exception e){

		}

	}

	public void loadSettings(int nodeId, NotificationSettings settings) {
		try{
			int ret = 0;

			if(NOTIFICATION_TYPE == FOLDER){
				//Set the module as "DF" to get the notification of both Document(D) and Folder(F)
				String module = "DF";
				ret = vwc.loadNotificationSettings(sid, nodeId, session.user, module, settings);
			}else{
				ret = vwc.loadNotificationSettings(sid, nodeId, session.user, notificationModule, settings);
			}
			if(settings == null)
				return;
			loadUI(settings);
		}catch(Exception ex){
			VWClient.printToConsole("Exception while loading settings : "+ex.getMessage());
		}
	}

	public void unCheckAll(ListCheckModel src) {
		try{
			//if(!src.isChecked(model.getElementAt(0))){
			for(int i=0; i<model.getSize(); i++){
				model.removeCheck(model.getElementAt(i));
				//model.removeLock(model.getElementAt(i));
			}
			//}			
		}catch(Exception e){
			VWClient.printToConsole("Exception while unchecking all : "+e.getMessage());
		}
	}

	public void checkAll(ListCheckModel src) {
		try{
			if((src.isChecked(model.getElementAt(0))) || (model.getChecksCount() == (model.getSize()-1))){
				for(int i=0; i<model.getSize(); i++){
					model.addCheck(model.getElementAt(i));
					//model.addLock(model.getElementAt(i));
				}
			}
		}catch(Exception e){
			VWClient.printToConsole("Exception while checking all : "+e.getMessage());
		}
	}

	public void loadUI(NotificationSettings settings, String userMailId) {
		try{
			loadUI(settings);
			String emails = (settings.getEmails() == null ? "" : settings.getEmails().trim());

			if(userMailId != null && userMailId.trim().length() > 0){
				userMailId = userMailId.trim() + ";";
			}

			if(emails != null && emails.contains(userMailId)){
				emails = emails.replace(userMailId, "");				
			}

			if(emails != null && !emails.equals("-"))
				txtrAdditionalEmails.setText(emails);
		}catch(Exception e){
			VWClient.printToConsole("Exception in loadUI() : "+e.getMessage());		
		}
	}

	public void loadUI(NotificationSettings settings) {
		try{
			if(settings == null)
				return;

			Notification[] notifications = settings.getNotifications();
			model.removeChecks();
			if(notifications != null && notifications.length>0){
				for(int i=0; i<notifications.length; i++){
					String tempStr = notifications[i].getNotifyDesc().trim();
					model.addCheck(tempStr);
				}
			}


			if(settings.getNotifyCaptureType() == 0){
				rdbtnOneTime.setSelected(true);
				rdbtnContinuous.setSelected(false);			
			}else if(settings.getNotifyCaptureType() == 1){
				rdbtnOneTime.setSelected(false);
				rdbtnContinuous.setSelected(true);			
			}

			if(settings.getProcessType() == 0){
				rdbtnIndividual.setSelected(true);
				rdbtnBulk.setSelected(false);
			}else if(settings.getProcessType() == 1){
				rdbtnIndividual.setSelected(false);
				rdbtnBulk.setSelected(true);
			}

			String emails = (settings.getEmails() == null ? "" : settings.getEmails().trim());			
			String userEmail = getUserMailId()+";";
			if(emails != null && emails.contains(userEmail)){
				emails = emails.replace(userEmail, "");				
			}

			if(emails != null && !emails.equals("-"))
				txtrAdditionalEmails.setText(emails);

			String attachment = String.valueOf(settings.getAttachment());
			if(attachment.charAt(0) == '0')
				chckbxViewwiseReference.setSelected(false);
			else if(attachment.charAt(0) == '1')
				chckbxViewwiseReference.setSelected(true);

			if(attachment.charAt(1) == '0')
				chckbxDocumentHistory.setSelected(false);
			else if(attachment.charAt(1) == '1')
				chckbxDocumentHistory.setSelected(true);	

			boolean inherit = (settings.getFolderInherit() == 1 ? true : false);
			chckbxFolderInherit.setSelected(inherit);

			inherit = (settings.getDocumentInherit() == 1 ? true : false);
			chckbxDocumentInherit.setSelected(inherit);


		}catch(Exception e){
			VWClient.printToConsole("Exception in loadUI(settings) : "+e.getMessage());
		}
	}

	private String getUserMailId() {
		try{
			String userMail = "";
			String userName = "";
			Vector userMailId = new Vector();

			if(session == null)	
				return userMail;
			else
				userName = session.user;

			int ret = vwc.getUserMail(sid, userName, userMailId);
			if(userMailId != null && userMailId.size()>0){
				if(userMailId.get(0) != null && !userMailId.get(0).toString().trim().equals("")){
					userMail = userMailId.get(0).toString().trim();
					VWClient.printToConsole("userMail : "+userMail);
				}
			}
			return userMail;
		}catch(Exception e){
			VWClient.printToConsole("Exception while getting user mail Id :: "+e.getMessage());
			e.printStackTrace();
			return "";
		}
	}

	class RadioBtnListener implements ActionListener{
		public void actionPerformed(ActionEvent event) {
			try{
				Object object = event.getSource();
				if(object == rdbtnOneTime){
					rdbtnOneTime_actionPerformed(event);
				}
				else if(object == rdbtnContinuous){
					rdbtnContinuous_actionPerformed(event);
				}
				else if(object == rdbtnIndividual){
					rdbtnIndividual_actionPerformed(event);
				}
				else if(object == rdbtnBulk){
					rdbtnBulk_actionPerformed(event);
				}
			}catch(Exception e){
				VWClient.printToConsole("Exception in Radio Button action performed : "+e.getMessage());
			}
		}
	}

	public void rdbtnBulk_actionPerformed(ActionEvent event) {
		rdbtnBulk.setSelected(true);
		rdbtnIndividual.setSelected(false);

		lblInclude.setEnabled(false);

		chckbxDocumentHistory.setSelected(false);
		chckbxDocumentHistory.setEnabled(false);

		chckbxViewwiseReference.setSelected(false);
		chckbxViewwiseReference.setEnabled(false);
	}

	public void rdbtnIndividual_actionPerformed(ActionEvent event) {
		int count = 0;

		rdbtnIndividual.setSelected(true);
		rdbtnBulk.setSelected(false);
		Notification[] folderList =  getNotificationForModule(FOLDER);//FOLDER_NOTIFICATION_LIST;
		// need to verify this - PR 
		count = getCheckCount(/*FOLDER_NOTIFICATION_LIST*/folderList.length, model.getSize(), model);
		if(count > 0){
			lblInclude.setEnabled(true);
			chckbxDocumentHistory.setEnabled(true);
			chckbxViewwiseReference.setEnabled(true);
		}
	}

	public void rdbtnContinuous_actionPerformed(ActionEvent event) {
		rdbtnContinuous.setSelected(true);
		rdbtnOneTime.setSelected(false);
	}

	public void rdbtnOneTime_actionPerformed(ActionEvent event) {
		rdbtnOneTime.setSelected(true);
		rdbtnContinuous.setSelected(false);

	}

	public void getCheckList(int notificationType){
		//CheckList checkList = new CheckList();
		try{		
			setNotificationList(notificationType);

			model.clear();

			for(int i=0; i<notificationList.getItemCount(); i++){
				model.addElement(notificationList.getItem(i));			
			}
			checkList.setModel(model);
		}catch(Exception ex){
			VWClient.printToConsole("Exception while getting check list : "+ex.getMessage() );			
		}
		//return new JScrollPane(checkList);
	}

	public void setNotificationList(int notificationType){
		try{
			if(notificationType < 0)
				return;
			notificationList.removeAll();
			if (notificationType == FOLDER){
				Notification[] folders = getNotificationForModule(FOLDER);
				Notification[] documents = getNotificationForModule(DOCUMENT);
				currentNotificationList = new Notification[folders.length + documents.length];  
				System.arraycopy(folders, 0, currentNotificationList, 0, folders.length);
				System.arraycopy(documents, 0, currentNotificationList, folders.length, documents.length); 
			}else{
				currentNotificationList = getNotificationForModule(notificationType);
			}



/*		switch(notificationType){
		case FOLDER : //currentNotificationList = FOLDER_NOTIFICATION_LIST; 
					  int folderListLength = FOLDER_NOTIFICATION_LIST.length;
					  int docListLength = DOCUMENT_NOTIFICATION_LIST.length;
					  currentNotificationList = new Notification[folderListLength + docListLength];
					  System.arraycopy(FOLDER_NOTIFICATION_LIST, 0, currentNotificationList, 0, folderListLength);
					  System.arraycopy(DOCUMENT_NOTIFICATION_LIST, 0, currentNotificationList, folderListLength, docListLength);
					  break;
		case DOCUMENT : currentNotificationList = DOCUMENT_NOTIFICATION_LIST; break;
		case DOCUMENT_TYPE : currentNotificationList = DOCUMENT_TYPE_NOTIFICATION_LIST; break;
		case STORAGE_MANAGEMENT : currentNotificationList = STORAGE_MANAGEMENT_NOTIFICATION_LIST; break;
		case AUDIT_TRAIL : currentNotificationList = AUDIT_TRAIL_NOTIFICATION_LIST; break;
		case RECYCLE_DOCUMENTS : currentNotificationList = RECYCLE_NOTIFICATION_LIST; break;
		case RESTORE_DOCUMENTS : currentNotificationList = RESTORE_NOTIFICATION_LIST; break;
		case REDACTIONS : currentNotificationList = REDACTION_NOTIFICATION_LIST; break;
		case RETENTION: currentNotificationList = RETENTION_NOTIFICATION_LIST; break;
		case ROUTE: currentNotificationList = ROUTE_NOTIFICATION_LIST; break;
		}*/

			if(currentNotificationList != null && currentNotificationList.length>0){
				for(int i=0; i<currentNotificationList.length; i++){
					notificationList.add(currentNotificationList[i].getNotifyDesc());
				}		
			}
		}catch(Exception ex){
			VWClient.printToConsole("Exception while setting notification list : "+ex.getMessage());			
		}
	}

	public int getNotifictionSettings(NotificationSettings settings) {
		int ret = 0;
		String emails = "", userEmail = "", attachment = "";

		try{
			userEmail = lblLoggedUserMail.getText().trim();
			Notification[] notifications = getSelectedNotifications();




			if(!userEmail.equals("") && !userEmail.equals("-"))
				emails = userEmail + ";" ;
			if(!txtrAdditionalEmails.getText().trim().equals(""))
				emails = emails + txtrAdditionalEmails.getText().trim();

			if(!isValidEmail(emails)){
				ret = -2;
				JOptionPane.showMessageDialog(this, connectorManager.getString("notificationpanel.ValidEmail"), NOTIFICATION_TITLE, JOptionPane.INFORMATION_MESSAGE);
				return ret;
			}

			if(chckbxViewwiseReference.isSelected())
				attachment = "1";
			else 
				attachment = "0";

			if(chckbxDocumentHistory.isSelected())
				attachment += "1";
			else
				attachment += "0";

			String userName = getUserName();					
			settings.setNotifications(notifications);
			settings.setUserName(userName);
			settings.setEmails(emails);
			settings.setNotifyCaptureType(rdbtnOneTime.isSelected() ? 0 : 1);
			settings.setProcessType(rdbtnIndividual.isSelected() ? 0 : 1);
			settings.setAttachment(attachment);
			settings.setIpAddress(vwc.getClientIP());
			settings.setFolderInherit(chckbxFolderInherit.isSelected() ? 1 : 0);
			settings.setDocumentInherit(chckbxDocumentInherit.isSelected() ? 1 : 0);
			if(notifications == null){
				ret = -1;
				if (NOTIFICATION_TYPE != FOLDER)
					JOptionPane.showMessageDialog(this, SELECT_NOTIFICATIONS, NOTIFICATION_TITLE, JOptionPane.INFORMATION_MESSAGE);		
				//return ret;
			}			

			if(emails.length() <= 0 && (ret == 0 || NOTIFICATION_TYPE == FOLDER)){
				//ret = -3;
				JOptionPane.showMessageDialog(this, ENTER_EMAILS, NOTIFICATION_TITLE, JOptionPane.INFORMATION_MESSAGE);
				//return ret;
			}
		}catch(Exception e){
			VWClient.printToConsole("Exception while getting Notification settings");
		}
		return ret;		
	}	

	private boolean isValidEmail(String emails) {
		boolean valid = false;
		StringTokenizer st = new StringTokenizer(emails, ";");
		while(st.hasMoreTokens()){
			String email = st.nextToken();
			valid = EmailAddressValidator.isValidEmailAddress(email);
			if(!valid)
				return false;
		}		
		return true;
	}

	private String getUserName() {
		String user = "";
		try{
			user = session.user;
		}catch(Exception ex){
			user = "";
		}
		return user;
	}

	public Notification[] getSelectedNotifications(){
		Notification[] notifications = null;

		try{
			ArrayList checkedList = (ArrayList) model.getCheckeds();

			if(checkedList != null && checkedList.size()>0){
				notifications = new Notification[checkedList.size()];
				int k=0;
				for(int i=0; i<checkedList.size(); i++){
					String notifyDesc = checkedList.get(i).toString().trim();
					for(int j=0; j<currentNotificationList.length; j++){
						if(notifyDesc.equalsIgnoreCase(currentNotificationList[j].getNotifyDesc().trim())){
							notifications[k] = currentNotificationList[j];
							k++;
							break;
						}
					}
				}
			}
		}catch(Exception ex){
			VWClient.printToConsole("Exception while getting selected Notifications :: "+ex.getMessage());
			ex.printStackTrace();
		}
		return notifications;
	}

	public void setNotifications(int notificationType) {
		try{
			setNotificationList(notificationType);
			model.clear();

			if(notificationList != null && notificationList.getItemCount() > 0){
				for(int i=0; i<notificationList.getItemCount(); i++){
					model.addElement(notificationList.getItem(i));
				}
			}
		}catch(Exception e){
			VWClient.printToConsole("Exception in Settings Panel while setting Notifications : "+e.getMessage());
			e.printStackTrace();
		}
	}

	public void setUserMailId(String mailId){
		try{
			lblLoggedUserMail.setText(mailId);
		}catch(Exception ex){
			VWClient.printToConsole("Exception while setting user mail id : "+ex.getMessage());
		}
	}
}

/**
 * A class to provide stronger validation of email addresses.
 * devdaily.com, no rights reserved. :)
 *
 */
class EmailAddressValidator
{
	public static boolean isValidEmailAddress(String emailAddress)
	{
		//an empty string is valid
		if(emailAddress.trim().equals(""))
			return true;
		// a null string is invalid
		if ( emailAddress == null )
			return false;

		// a string without a "@" is an invalid email address
		if ( emailAddress.indexOf("@") < 0 )
			return false;

		// a string without a "."  is an invalid email address
		if ( emailAddress.indexOf(".") < 0 )
			return false;

		if ( !lastEmailFieldTwoCharsOrMore(emailAddress))
			return false;

		return true;
	}

	/**
	 * Returns true if the last email field (i.e., the country code, or something
	 * like .com, .biz, .cc, etc.) is two chars or more in length, which it really
	 * must be to be legal.
	 */
	private static boolean lastEmailFieldTwoCharsOrMore(String emailAddress)
	{
		StringTokenizer st = new StringTokenizer(emailAddress,".");
		String lastToken = null;

		int count = st.countTokens();
		if(count < 2)
			return false;
		while ( st.hasMoreTokens() )
		{
			lastToken = st.nextToken();
		}

		if ( lastToken.length() >= 2 )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}

