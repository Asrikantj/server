package com.computhink.vwc.notification;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;

import com.computhink.common.Document;
import com.computhink.common.Node;
import com.computhink.common.NotificationSettings;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.Session;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.image.Images;
import com.computhink.vws.server.Client;


public class VWExistingNotifications implements VWNotificationConstants, Runnable{

	private static ResourceManager connectorManager = ResourceManager.getDefaultManager();
	private static JDialog dlgExistingNotification;
	JScrollPane SPTable ;
	VWNotificationTable Table;
	private JPanel mainPanel = new JPanel();

	private JButton btnRefresh = new JButton(connectorManager.getString("notification.refresh"));
	private JButton btnEdit = new JButton(connectorManager.getString("notification.edit"));
	private JButton btnDelete = new JButton(connectorManager.getString("notification.delete"));
	private JButton btnClose = new JButton(connectorManager.getString("notification.close"));
	private JButton btnHelp = new JButton();

	private static boolean active = false;

	private int sid;
	private Session session;
	private WindowAdapter secAdapter;
	private static VWClient vwc;
	private boolean windowActive=true;
	private boolean isLoaded = false;
	public static int prevSID=0;
	
	public static boolean isWebClient = false;

	public VWExistingNotifications(VWClient vwc, int sid) {
		try{
			if (active)
			{
				if(sid == prevSID){
					this.vwc = vwc;
					this.sid = sid;
					session = vwc.getSession(sid);	
					dlgExistingNotification.requestFocus();
					dlgExistingNotification.toFront();
					//loadAvailableNotifications();
					return;
				}
				else{
					closeDialog();
				}
			}						
			//dlgExistingNotification.setModal(true);
			new Images();
			this.vwc = vwc;
			this.sid = sid;
			prevSID = sid;
			session = vwc.getSession(sid);
			
			if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
					vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
				isWebClient = true;

			initComponents();
			active = true;			
			loadAvailableNotifications();
		}catch(Exception e){
			VWClient.printToConsole("Exception in VWDocNotificationSettings : "+e.getMessage() );
		}
	}

	private void initComponents() {
		try{
			//dlgExistingNotification.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			dlgExistingNotification = new JDialog();
			dlgExistingNotification.setTitle(connectorManager.getString("notification.title")+" "+getUserName());
			dlgExistingNotification.setBounds(200, 150, 650, 462);
			dlgExistingNotification.getContentPane().setLayout(null);
			dlgExistingNotification.setResizable(false);

			Table = new VWNotificationTable(this);
			SPTable = new JScrollPane();
			
			if(!isWebClient)
				Table.setBackground(Color.white);
			SPTable.setViewportView(Table);
		    Table.setRowSelectionAllowed(true);
		    
		    if(!isWebClient)
		    	SPTable.getViewport().setBackground(Color.WHITE);
			
			mainPanel.setLayout(null);
			mainPanel.add(SPTable);
			SPTable.setBounds(12, 12, 620, 370);
			mainPanel.setBounds(0, 0, 640, 430);

			btnHelp.setBounds(10, 395, 20, 20);
			btnHelp.setIcon(Images.help);
			btnHelp.setMnemonic('H');
			btnHelp.setBorderPainted(false);
			btnHelp.setFocusPainted(false);
			btnHelp.setContentAreaFilled(false);
			mainPanel.add(btnHelp);
			String languageLocale=connectorManager.getLocale().toString();
			VWClient.printToConsole("languageLocale :::"+languageLocale);
			if(languageLocale.equals("nl_NL")){
				btnRefresh.setBounds(284-80, 395, 80+20, 23);
			}else
			btnRefresh.setBounds(284, 395, 80, 23);
			mainPanel.add(btnRefresh);
			if(languageLocale.equals("nl_NL")){
				btnEdit.setBounds(374-60, 395, 80+20, 23);
			}else
			btnEdit.setBounds(374, 395, 80, 23);
			mainPanel.add(btnEdit);
			if(languageLocale.equals("nl_NL")){
				btnDelete.setBounds(464-40, 395, 80+20, 23);	
			}else
			btnDelete.setBounds(464, 395, 80, 23);
			mainPanel.add(btnDelete);
			if(languageLocale.equals("nl_NL")){
				btnClose.setBounds(554-20, 395, 80+20, 23);	
			}else
			btnClose.setBounds(554, 395, 80, 23);
			mainPanel.add(btnClose);

			SymAction symAction = new SymAction();
			btnRefresh.addActionListener(symAction);
			btnEdit.addActionListener(symAction);
			btnDelete.addActionListener(symAction);
			btnClose.addActionListener(symAction);
			btnHelp.addActionListener(symAction);

			secAdapter = new WindowAdapter(){
				public void windowClosing(WindowEvent evt){
					closeDialog();
				}				
				/*public void windowLostFocus(WindowEvent e){
					if(windowActive){
						dlgExistingNotification.requestFocus();
						dlgExistingNotification.toFront();
					}
				}
				public void windowDeactivated(WindowEvent e) {
					if(windowActive){
						dlgExistingNotification.requestFocus();
						dlgExistingNotification.toFront();
					}
				}
				public void windowStateChanged(WindowEvent e) {
					//dlgExistingNotification.toFront();
				}
				public void windowDeiconified(WindowEvent e) {
					dlgExistingNotification.toFront();
				}*/
			};
			dlgExistingNotification.addWindowListener(secAdapter);
			dlgExistingNotification.getContentPane().add(mainPanel);
			setEnableMode(MODE_VIEW);
			SPTable.setPreferredSize(new Dimension(700, 500));					
			//dlgExistingNotification.addMouseListener(mouseListener);
		}catch(Exception e){
			VWClient.printToConsole("Exception while initializing document notification settings : "+e.getMessage() );
		}
	}
	
	private void loadAvailableNotifications() {
		try{
			Table.clearData();
			String userName = getUserName();
			int isAdmin = vwc.isAdmin(sid);
			Vector settings = new Vector();
			//set module as "DF" to get all notifications from 'Document(D)' and 'Folder(F)' modules 

			String module = "DF"; 
			int ret = 0;
			if(userName != null && !userName.equals("")){
				userName = userName.trim();
				ret = vwc.getAllNotifications(sid, isAdmin, userName, module, settings);
			}
			Table.addData(settings);
			//Table.setLastProcessedRowSelected();
		}catch(Exception ex){
			VWClient.printToConsole("Exception while loading Available Notifications : "+ex.getMessage());
		}
	}

	private String getUserName() {
		String userName = "";
		try{
			if(session == null)
				userName = "";
			else
				userName = session.user;

		}catch(Exception ex){
			userName = "";
			VWClient.printToConsole("Exception while getting user name : "+ex.getMessage());
		}
		return userName;
	}


	class SymAction implements java.awt.event.ActionListener{
		public void actionPerformed(ActionEvent event) {
			Object object = event.getSource();
			if(object == btnRefresh){
				btnRefresh_actionPerformed(event);
			}else if(object == btnEdit){
				btnEdit_actionPerformed(event);
			}else if(object == btnDelete){
				btnDelete_actionPerformed(event);
			}else if(object == btnClose){
				btnClose_actionPerformed(event);				
			}else if(object == btnHelp){
				btnHelp_actionPerformed(event);
			}
		}
	}

	public void btnHelp_actionPerformed(ActionEvent event) {
		//loadAvailableNotifications();
		System.out.println("btnHelp Clicked");

	}

	public void btnRefresh_actionPerformed(ActionEvent event) {
		loadAvailableNotifications();
		setEnableMode(MODE_VIEW);
		System.out.println("btnRefresh Clicked");

	}
	
	public void btnDelete_actionPerformed(ActionEvent event) {
		int retVal = JOptionPane.showConfirmDialog(dlgExistingNotification,connectorManager.getString("notification.jmsg1"),connectorManager.getString("notification.jtitle1"), 
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

		if (retVal == 0) {
			int[] selectedRows = Table.getSelectedRows();
			
			for(int i=0; i<selectedRows.length; i++){
				int row = selectedRows[i];
				NotificationSettings settings = Table.getRowData(row); 					
				int ret = vwc.deleteNotificationSettings(sid, settings);
			}
			Table.deleteSelectedRows();
			setEnableMode(MODE_VIEW);
		}

	}

	public void btnEdit_actionPerformed(ActionEvent event) {
		try{

			int selectedRow = Table.getSelectedRow();

			NotificationSettings settings = Table.getRowData(selectedRow);
			if(settings != null){
				int nodeType = settings.getNodeType();

				if(nodeType == DOCUMENT){					
					Document doc = new Document(settings.getReferenceId());
					doc.setName(settings.getNodeName());
					int ret = vwc.openNotificationSettings(sid, doc, dlgExistingNotification);
				}
				else if(nodeType == FOLDER){
					Node node = new Node(settings.getReferenceId());
					node.setName(settings.getNodeName());
					int ret = vwc.openNotificationSettings(sid, node, dlgExistingNotification);
				}
			}

		}catch(Exception ex){
			VWClient.printToConsole("Exception while editing notification : "+ex.getMessage());
		}
	}

	public void btnClose_actionPerformed(ActionEvent event) {
		closeDialog();		
	}

	public void btnOK_actionPerformed(ActionEvent event) {
		try{
			System.out.println("btnOK Clicked");

		}catch(Exception e){
			VWClient.printToConsole("Exception while saving document notification settings : "+e.getMessage());
		}
	}

	public void run() {
		show();
	}

	public void show()
	{
		if (vwc == null) return;
		dlgExistingNotification.setVisible(true);
	}

	private void closeDialog() {
		active = false;		
		isLoaded = false;
		//Table.clearData();
		dlgExistingNotification.setVisible(false);
		dlgExistingNotification.dispose();		
	}

	public void setEnableMode(int mode){
		switch(mode){
		case MODE_VIEW:	btnEdit.setEnabled(false);
		btnDelete.setEnabled(false);
		break;
		
		case MODE_EDIT_DELETE:btnEdit.setEnabled(true);
		btnDelete.setEnabled(true);
		break;
		
		case MODE_DELETE1:btnEdit.setEnabled(false);
		btnDelete.setEnabled(true);
		break;
		}
	}

	public static void main(String[] args) {
		try {			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			VWClient vwc = new VWClient("c:\\temp");
			int sid = vwc.login("10.4.8.149", "VW7", "vwadmin", "vw");
			VWExistingNotifications vwAvailableNotifications = new VWExistingNotifications(vwc, sid);
			new Thread(vwAvailableNotifications).start();	
		} 
		catch (Exception e) {
			e.printStackTrace();
		}
	}
}
