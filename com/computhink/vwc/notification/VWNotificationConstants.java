package com.computhink.vwc.notification;

import com.computhink.common.Constants;
import com.computhink.common.Notification;
import com.computhink.resource.ResourceManager;
public interface VWNotificationConstants {

	public int FOLDER = 0;
	public int DOCUMENT = 1;
	public int DOCUMENT_TYPE = 2;
	public int STORAGE_MANAGEMENT = 3;
	public int AUDIT_TRAIL = 4;
	public int RECYCLE_DOCUMENTS = 5;
	public int RESTORE_DOCUMENTS= 6;
	public int REDACTIONS = 7;
	public int RETENTION= 8;
	public int ROUTE= 9;
	
	public String NOTIFICATION_TITLE = ResourceManager.getDefaultManager().getString("NotifiConstants.NOTIFICATION_TITLE");
	public String SELECT_NOTIFICATIONS = ResourceManager.getDefaultManager().getString("NotifiConstants.SelectNotifyEvents");
	public String ENTER_EMAILS = ResourceManager.getDefaultManager().getString("NotifiConstants.ENTER_EMAILS");
	
	String[] NotificationModules = {"Folder", "Document", "DocumentType", "Storage Management", "AuditTrail", 
			"Recycle Documents", "Restore Documents", "Redactions", "Retention", Constants.WORKFLOW_MODULE_NAME};
	String[] NotificationModulesAdminWise = {ResourceManager.getDefaultManager().getString("AdminWiseNotfication.Folder"),
			ResourceManager.getDefaultManager().getString("AdminWiseNotfication.Document"),
			ResourceManager.getDefaultManager().getString("AdminWiseNotfication.DocumentType"),
			ResourceManager.getDefaultManager().getString("AdminWiseNotfication.StorageManagement"),
			ResourceManager.getDefaultManager().getString("AdminWiseNotfication.AuditTrail"), 
			ResourceManager.getDefaultManager().getString("AdminWiseNotfication.RecycleDocuments"),
			ResourceManager.getDefaultManager().getString("AdminWiseNotfication.RestoreDocuments"), 
			ResourceManager.getDefaultManager().getString("AdminWiseNotfication.Redactions"), 
			ResourceManager.getDefaultManager().getString("AdminWiseNotfication.Retention"),
			ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")};
	
	public int MODE_VIEW = 0;
	public int MODE_EDIT_DELETE = 1;
	public int MODE_DELETE1 = 2;
	
	/*public static final Notification[] DOCUMENT_NOTIFICATION_LIST = {
		new Notification(1, "Any changes to Document(s)", "Document"),
		new Notification(2, "Properties (Index value) are Modified", "Document"),
		new Notification(3, "Document Pages added/updated/removed", "Document"),
		new Notification(4, "Comments are added/modified","Document"),
		new Notification(5, "Document is moved","Document"),
		new Notification(6, "Freeze Document","Document"),
		new Notification(7, "Un-Freeze Document","Document"),
		new Notification(8, "Signature applied", "Document"),
		new Notification(9, "Document Indexed", "Document"),
		new Notification(10, "Permission is Modified", "Document"),
		new Notification(11, "Document Check Out", "Document"),
		new Notification(12, "Document Check In / Release Session", "Document"),
		new Notification(13, "Route Ended", "Document")																	
	};
	
	public static final Notification[] FOLDER_NOTIFICATION_LIST = { 	
		new Notification(31, "Any changes to folder", "Folder"),
		new Notification(32, "Folder / Document / Sub Folder moved", "Folder"),
		new Notification(33, "Folder / Document / Sub Folder copied", "Folder"),
		new Notification(34, "Document /  Sub Folder is created", "Folder"),
		new Notification(35, "Document / Sub Folder is removed", "Folder"),
		new Notification(36, "Permission is modified", "Folder"),
		new Notification(37, "Node Properties", "Folder"),
		new Notification(38, "Custom columns", "Folder"),
		new Notification(39, "AIP/ EAS document uploaded to folder", "Folder"),
		new Notification(40, "Folder Check Out", "Folder"),
		new Notification(41, "Folder Check In", "Folder")

	};
	
	public static final Notification[] DOCUMENT_TYPE_NOTIFICATION_LIST = {
		new Notification(51, "Version revision Enabled / Disabled for Doc type", "DocumentType"),
		new Notification(52, "Assigned route for Doc type", "DocumentType"),
		new Notification(53, "External DB for Doc type", "DocumentType"),
		new Notification(54, "Selection value update from DSN", "DocumentType")
	};
	
	public static final Notification[] STORAGE_MANAGEMENT_NOTIFICATION_LIST = {
		new Notification(71, "Storage location changed", "Storage Management"),
		new Notification(72, "Storage location Moved", "Storage Management")		
	};
	
	public static final Notification[] AUDIT_TRAIL_NOTIFICATION_LIST = {
		new Notification(76, "Update setting", "AuditTrail")
	};
	
	public static final Notification[] RECYCLE_NOTIFICATION_LIST = {	
		new Notification(81, "Empty Recycle", "Recycle Documents"),
		new Notification(82, "Purge documents", "Recycle Documents"),
		new Notification(83, "Restore documents", "Recycle Documents")		
	};
	
	public static final Notification[] RESTORE_NOTIFICATION_LIST = {
		new Notification(86, "Doc type created while restoring", "Restore Documents"),
		new Notification(87, "Index created while restoring", "Restore Documents")
	};
	
	public static final Notification[] REDACTION_NOTIFICATION_LIST = {
		new Notification(91, "Replace Passwords", "Redactions")
	};
	
	public static final Notification[] RETENTION_NOTIFICATION_LIST = {	
		new Notification(96, "Documents Expired", "Retention"),
		new Notification(97, "Update Retention", "Retention"),
		new Notification(98, "Delete Retention", "Retention")
	};
	
	public static final Notification[] ROUTE_NOTIFICATION_LIST = {	
		new Notification(110, "Route completed", "Route"),
		new Notification(111, "Valid to Invalid Route", "Route"),
		new Notification(112, "Invalid to Valid Route", "Route")
	};
	*/
	/*public static final Notification[] BACKUP_NOTIFICATION_LIST = {	new Notification(21, "Backup Document", "Backup Documents")
															};
	*/
}
