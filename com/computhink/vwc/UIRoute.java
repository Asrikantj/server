package com.computhink.vwc;

import com.computhink.common.Principal;
import com.computhink.common.RouteInfo;
import com.computhink.vwc.image.*;
/**
 *
 * @author  Administrator
 */
import javax.swing.JLabel;
//import java.awt.Font;

public class UIRoute extends JLabel
{
    private RouteInfo routeInfo;
    
    public UIRoute(RouteInfo p)
    {
    	routeInfo = p;
        init();
    }
    private void init()
    {
        new Images();
        setName(routeInfo.getRouteName());
        setText(routeInfo.getRouteName());
        /*if (principal.getType() == (Principal.GROUP_TYPE)) 
            setIcon(Images.grp);
        else
            setIcon(Images.usr);*/
        //setFont(new Font("Arial", 0, 12)); 
    }
    public RouteInfo getRouteInfo()
    {
        return this.routeInfo;
    }
    public String toString()
    {
        return routeInfo.getRouteName();
    }
    
}