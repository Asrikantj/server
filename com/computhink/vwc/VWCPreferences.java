/*
 * VWCPreferences.java
 *
 * Created on 22 ����� ������, 2003, 03:21 �
 */
package com.computhink.vwc;
import com.computhink.common.*;
import com.computhink.resource.ResourceManager;

/**
 *
 * @author  Administrator
 */
import java.util.ArrayList;
import java.util.prefs.*;

import javax.swing.JOptionPane;

public final class VWCPreferences implements VWCConstants, Constants
{
    private static Preferences VWCPref;
    private static Preferences CU_VWCPref; //CurrentUser_VWCPreferences
    private static boolean userPrefs = false;
    private static boolean regEntryFlag = false;//Exception number 578
    private static boolean dtcFlag = false;//Exception number 578
    private static boolean AWFlag = false;//Exception number 959
    private static ResourceManager resourceManager=null;
    static
    {
        try
        {
  
            //System.out.println("VWCPreferences initializing  "); 
            VWCPref= Preferences.systemRoot().node(VWC_PREF_ROOT);
            CU_VWCPref = Preferences.userRoot().node(VWC_PREF_ROOT);
            getNode("general").put("home", "try");
        }
        catch(Exception e){userPrefs = true;
        getCUNode("general").put("home", "try");
        
        //System.out.println("VWCPreferences  " + e.getMessage());
        //Util.Msg(null, "Exception in writing to regitry " + e.getMessage(), "ViewWise");
        //System.exit(0);
        }
        
/*        if (userPrefs)
        {
            try
            {
                VWCPref= Preferences.userRoot().node(VWC_PREF_ROOT);
            }
            catch(Exception e){}
        }*/
    }
    private static Preferences getNode(String node)
    {
        return VWCPref.node(node);
    }
    
    /**
     * getCUNode - Method added to return current user node
     * @param node - Node to get from current user registry
     * @return - returns the node under current user
     */
    private static Preferences getCUNode(String node)
    {
        return CU_VWCPref.node(node);
    }
    public static void setComPort(int port)
    {
    	try{
	        getNode("host").putInt("comport", port);
	        flush();
    	}catch(Exception ex){
    		if(!regEntryFlag){
    	    	//regEntryFlag = true;
    			Util.Msg(null, resourceManager.getString("VWCPrefMSG.RegistryException")+" "+ex.getMessage(), PRODUCT_NAME);//Exception 1 
    			
    		}
    	}
    }
    public static int getComPort()
    {
        return getNode("host").getInt("comport", 0);
    }
     public static void setDataPort(int port)
    {
    	 try{
	        getNode("host").putInt("dataport", port);
	        flush();
    	 }catch(Exception ex){
    		if(!regEntryFlag){
    	    	//regEntryFlag = true;
    			Util.Msg(null, resourceManager.getString("VWCPrefMSG.RegistryException")+" "+ex.getMessage(), PRODUCT_NAME);//Exception 2 
    		}
    	}
    }
    public static int getDataPort()
    {
        return getNode("host").getInt("dataport", 0);
    }
    public static ServerSchema[] getVWSs() 
    {
    	Preferences serverPref = getNode("servers");
    	Preferences curUserServerPref = getCUNode("servers");
    	String[] serverNames = null;
    	String[] curUserServerNames = null;
    	try
    	{
    		serverNames = serverPref.childrenNames();
    		curUserServerNames = curUserServerPref.childrenNames();
    	}
    	catch(BackingStoreException e){}

    	int serverCount = serverNames.length;
    	ServerSchema[] servers = new ServerSchema[serverCount];
    	for (int i=0; i < serverCount; i++)
    	{
    		servers[i] = getVWS(serverNames[i], false);
    	}

    	ArrayList<ServerSchema> curUserServers = new ArrayList<ServerSchema>();
    	if(curUserServerNames != null && curUserServerNames.length > 0){
    		for(int i=0; i<curUserServerNames.length; i++){
    			ServerSchema server = getVWS(curUserServerNames[i], true);
    			if(server != null && server.address.trim().length() > 0 && server.comport != 0){
    				curUserServers.add(server);
    			}
    		}
    	}

    	if(curUserServers != null && curUserServers.size() > 0){
    		ServerSchema[] allServers = new ServerSchema[servers.length + curUserServers.size()];
    		int count = 0;
    		for(int i=0; i < servers.length; i++){
    			allServers[count] = servers[i];
    			count++;
    		}
    		for(int i=0; i < curUserServers.size(); i++){
    			allServers[count] = curUserServers.get(i);
    			count++;
    		}
    		return allServers;    		
    	}else{
    		return servers;
    	}    	
    }
    public static ServerSchema getVWS(String name, boolean isCurUserRegistry)
    {
    	ServerSchema ss = new ServerSchema();
        Preferences serverPref = null;
        if(!isCurUserRegistry)
        	serverPref = getNode("servers/" + name);
        else
        	serverPref = getCUNode("servers/" + name);
        
        ss.name = name;
        ss.address = serverPref.get("host", "");
        ss.comport = serverPref.getInt("port", 0);
        return ss;
    }
// Enhancement Show/Hide Room
    public static void addSelectedRoom(String selectedRoom,String server) {
/*
Issue No / Purpose:  <05/24/2006 VWC Registration>
Created by: <Pandiya Raj.M>
Date: <14 Jun 2006>
Read/Store the registry entry from the HKEY_CURRENT_USER.
 */    	
    	try{
    		Preferences serverPref  = Preferences.userRoot().node(VWC_PREF_ROOT).node("servers/" + server);//getNode("servers/" + server);
    		serverPref.put("rooms",selectedRoom);
    		flush();
    	} catch(Exception ex){
    		if(!regEntryFlag){
    			//regEntryFlag = true;
    			Util.Msg(null,  resourceManager.getString("VWCPrefMSG.RegistryException")+" "+ex.getMessage(), PRODUCT_NAME);//Exception 3
    		}
    	}
    }
//End
    public static void addVWS(ServerSchema server) 
    {
    	try{
    		getNode("general").put("vwtemp", "true");
    		getNode("general").remove("vwtemp");
    		Preferences serverPref  = getNode("servers/" + server.name);
    		serverPref.put("host", server.address);
    		serverPref.putInt("port", server.comport);
    		flush();
    	}catch(Exception ex){
    		try{
    			getCUNode("general").put("vwtemp", "true");
    			getCUNode("general").remove("vwtemp");
    			Preferences serverPref  = getCUNode("servers/" + server.name);
    			serverPref.put("host", server.address);
    			serverPref.putInt("port", server.comport);
    			flush();
    		}catch (Exception e) { }
    	}
    }
    public static void removeVWS(String server) 
    {
        //Preferences serverPref  = getNode("servers/" + server);
    	Preferences serverPref  = Preferences.systemRoot().node(VWC_PREF_ROOT).node("servers/" + server);
        try
        {
            serverPref.removeNode() ;
            flush();
        }
        catch(Exception e){
        	try{
        		serverPref  = Preferences.userRoot().node(VWC_PREF_ROOT).node("servers/" + server);
        		serverPref.removeNode();
        		flush();
        	}catch (Exception ex) { }
        }
    }
    public static void setProxy(boolean b)
    {
    	try{
    		getNode("proxy").putBoolean("enabled", b);
    		flush();
    	}catch(Exception ex){
    		if(!regEntryFlag && !dtcFlag){
//    			regEntryFlag = true;
    			dtcFlag = true;
    			//Util.Msg(null, "Exception in writing to registry : "+ex.getMessage(), PRODUCT_NAME);//Exception 5 
    		}
    		try{
    			getCUNode("proxy").putBoolean("enabled", b);
    			flush();
    		}catch (Exception e) { }
    	}
    }
/*
Issue No / Purpose:  <09/06/2006 Web Client Sort Navigator>
Created by: <Pandiya Raj.M>
Date: <16 Jun 2006>
These methods are used to store/read the sort navigator value from registry
*/


    public static void setSortNavigator(boolean b)
    {
    	try{
    		getNode("general").putBoolean("sortNavigator", b);
    		flush();
    	}catch(Exception ex){
    		if(!regEntryFlag){
//    			regEntryFlag = true;
    			//Util.Msg(null, "Exception in writing to registry : "+ex.getMessage(), PRODUCT_NAME);//Exception 6 
    		}
    		try{
    			getCUNode("general").putBoolean("sortNavigator", b);
    		}
    		catch(Exception e){
    			e.printStackTrace();
    		}
    		
    	}
    }
    public static boolean getSortNavigator()
    {
    	/*
Issue No / Purpose:  <09/06/2006 Web Client Sort Navigator>
Created by: <Pandiya Raj.M>
Date: <29 Jun 2006>
The �Sort Navigator� default value is selected. so set the default value 'true' for a following method
    	 */    
    	boolean isSortNavigator=true;
    	try
    	{
    		if(!isKeyExistsInCurrentUserRegistry("general","sortNavigator"))
    			isSortNavigator= getNode("general").getBoolean("sortNavigator", true);
    		else
    			isSortNavigator= getCUNode("general").getBoolean("sortNavigator", true);
    	}
    	catch(Exception e)	{ 	}
    	return isSortNavigator;	
    }
    public static void setProxyHost(String server)
    {
    	try{
    		getNode("proxy").put("host", server);
    		flush();
    	}catch(Exception ex){
    		if(!regEntryFlag && !dtcFlag){
    			dtcFlag = true;
//    			regEntryFlag = true;
    			Util.Msg(null,  resourceManager.getString("VWCPrefMSG.RegistryException")+" "+ex.getMessage(), PRODUCT_NAME);//Exception 7 
    		}
    	}
    }
    public static void setProxyPort(int port)
    {
    	try{
    		getNode("proxy").putInt("port", port);
    		flush();
    	}catch(Exception ex){
    		if(!regEntryFlag && !dtcFlag){
    			dtcFlag = true;
//    			regEntryFlag = true;
    			Util.Msg(null, resourceManager.getString("VWCPrefMSG.RegistryException")+" "+ex.getMessage(), PRODUCT_NAME);//Exception 8 
    		}
    		if(dtcFlag){
    			dtcFlag = false;
    		}
    	}
    }
    public static boolean getProxy()
    {
    	boolean isProxyEnabled = false;
    	try{
    		if(!isKeyExistsInCurrentUserRegistry("proxy", "enabled"))
    			isProxyEnabled = getNode("proxy").getBoolean("enabled", false);
    		else
    			isProxyEnabled = getCUNode("proxy").getBoolean("enabled", false);
    	}catch (Exception e) {
    		isProxyEnabled = getCUNode("proxy").getBoolean("enabled", false);
    	}
    	return isProxyEnabled;
    }
    public static String getProxyHost()
    {
        return getNode("proxy").get("host", "");
    }
    public static int getProxyPort()
    {
        return getNode("proxy").getInt("port", DEFAULT_PROXY_PORT);
    }
    public static void setHome(String home)
    {
    	try{
    		getNode("general").put("home", home);
    		flush();
    	}catch(Exception ex){
    		if(!regEntryFlag && !AWFlag){
//    			regEntryFlag = true;
    			AWFlag = true;
    			dtcFlag = true;
    			//Util.Msg(null, "Exception in writing to registry : "+ex.getMessage(), PRODUCT_NAME);//Exception 9
    		}
    	}
    }
    public static void setConnectionMode(String mode)
    {
    	try{
    		getNode("general").put("connection", mode);
    		flush();
    	}catch(Exception ex){
    		if(!regEntryFlag){
//    			regEntryFlag = true;
    			Util.Msg(null,  resourceManager.getString("VWCPrefMSG.RegistryException")+" "+ex.getMessage(), PRODUCT_NAME);//Exception 10 
    		}
    	}
    }
    public static String getConnectionMode()
    {
        return getNode("general").get("connection", "auto");
    }
    public static void setServerProxy(String server, boolean b)
    {
    	try{
    		Preferences serverPref  = getNode("servers/" + server);
    		serverPref.putBoolean("Penabled", b);
    		flush();
    	}catch(Exception ex){
    		/*if(!regEntryFlag){
//    			regEntryFlag = true;
    			Util.Msg(null, "11 Exception in writing to registry : "+ex.getMessage(), PRODUCT_NAME);//Exception 11 
    		}*/
    		try{
    			Preferences serverPref  = getCUNode("servers/" + server);
    			serverPref.putBoolean("Penabled", b);
    			flush();
    		}catch (Exception e) { }
    	}
    }
    public static void setServerProxyHost(String server, String host)
    {
    	try{
    		Preferences serverPref  = getNode("servers/" + server);
    		serverPref.put("Phost", host);
    		flush();
    	}catch(Exception ex){
    		/*if(!regEntryFlag){
//    			regEntryFlag = true;
    			Util.Msg(null, "12 Exception in writing to registry : "+ex.getMessage(), PRODUCT_NAME);//Exception 12 
    		}*/
    		try{
    			Preferences serverPref  = getCUNode("servers/" + server);
    			serverPref.put("Phost", host);
    			flush();
    		}catch (Exception e) { }
    	}
    }
    public static boolean getServerProxy(String server)
    {
    	boolean pEnabled = false;
    	try{
    		Preferences serverPref;
    		if(!isKeyExistsInCurrentUserRegistry("servers/" + server, "Penabled"))
    			serverPref = getNode("servers/" + server);
    		else
    			serverPref = getCUNode("servers/" + server);
    		
    		pEnabled = serverPref.getBoolean("Penabled", false);
    	}catch (Exception e) {
    		pEnabled = false;
    	}
    	return pEnabled;
    }
    public static String getServerProxyHost(String server)
    {
    	String pHost = "";
    	try{
    		Preferences serverPref;
    		if(!isKeyExistsInCurrentUserRegistry("servers/" + server, "Phost"))
    			serverPref = getNode("servers/" + server);
    		else
    			serverPref = getCUNode("servers/" + server);
    		pHost = serverPref.get("Phost", "");
    	}catch (Exception e) {
    		pHost = "";
    	}
    	return pHost;
    }
    
    public static String getFontName(){
    	return getNode("general").get("fontname", "Aerial");
    }
    public static int getFontSize(){
    	return getNode("general").getInt("fontsize", 11);
    }
    static private void flush()
    {
        try
        {
            VWCPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){
            
            System.out.println("Exception in flush " + e.getMessage());            
        }
    }
    private static boolean isKeyExistsInCurrentUserRegistry(String nodeName, String keyName){
    	boolean isKeyExists = false;
    	try{
    		String proxyKeys[] = getCUNode(nodeName).keys();
    		if(proxyKeys != null && proxyKeys.length > 0){
    			for(String key : proxyKeys){
    				if(key != null && key.trim().equalsIgnoreCase(keyName))
    					isKeyExists = true;
    			}
    		}
    	}catch (Exception e) {
    		isKeyExists = false;
		}
    	//VWClient.printToConsole(nodeName + " : " + keyName+ "isKeyExists : "+isKeyExists);
    	return isKeyExists;
    }
}