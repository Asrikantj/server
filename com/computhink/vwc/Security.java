/*
 * Security.java
 *
 * Created on March 18, 2004, 11:49 AM
 */
package com.computhink.vwc;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.text.Collator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Position;

import com.computhink.common.Acl;
import com.computhink.common.AclEntry;
import com.computhink.common.Constants;
import com.computhink.common.Node;
import com.computhink.common.Principal;
import com.computhink.common.Util;
import com.computhink.common.VWAccess;
import com.computhink.vwc.image.Images;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.Client;

public class Security implements VWCConstants, KeyListener, Runnable, Constants
{
    private JPanel pnMain;
    private JPanel pnObject;
    private JPanel pnDPermissions, pnEPermissions, pnAllPermissions;
    private JScrollPane scPrincipals;
    private JLabel lblUsersAndGroups, lblFilter, lblPermission, lblEffective, lblDirect;
    private JTextField txtFilter;
    private JScrollPane scDpermission, scEpermission;
    
    private JList lstPrincipals;
    private JComboBox comQuickAssign;
    private JButton btRemove, btOK, btAdd, btCancel, btApply, btNewEntry, btHelp, btnReset, btReport;
    private JLabel lbObject;
    private Node node, parentNode;
    
    private JButton btWarning;
    JLabel lineBorderLabel =  new JLabel("", JLabel.CENTER);
    JLabel whiteLineBorderLabel =  new JLabel("", JLabel.CENTER);
    
    private String activePrincipal = "";
    private String warning = "";
    private int session;
    private PrincipalListModel listModel;
    private Hashtable dPermissions = new Hashtable();
    private Hashtable ePermissions = new Hashtable();
    private boolean docPermission = false;
    private static boolean active = false;
    private WindowAdapter secAdapter;
    //button width, height 
    private final int btnWidth = 80;
    private final int btnHeight = 23;
    private final int btnMaxWidth = 90;
    
    private static VWClient vwc;
    private static JDialog dlgMain;
    private boolean windowActive=true;    
    public boolean sorted = true;
    public boolean setACLFlag = false;
    
    public static int prevSID;
    public static int prevNodeId;
    
    public static boolean isWebClient = false;
    
   private ResourceManager connectorManager = ResourceManager.getDefaultManager();
   
 
    public Security(){
    	initComponents();
    }
	//Enhancement for Security - No 32
    public Security(VWClient vwc, int session, Node node) 
    {
    	if (active)
    	{
    		if(session == prevSID && node != null && node.getId() == prevNodeId){
    			dlgMain.requestFocus();
    			dlgMain.toFront();
    			return;
    		}
    		else{
    			closeDialog();
    		}
    	}

        new Images();
        this.vwc = vwc;
        
        if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
			isWebClient = true;
        
        initComponents();       
        this.session = session;
        prevSID = session;
        this.node = node;
        if(node != null)
        	prevNodeId = node.getId();
        describeNode();
        getDirectAcl();
        active = true;        
    }
    private void describeNode()
    {
        if (node.getType() == Node.FOLDER){
            //lbObject.setText("Permissions for Node: " + node.getName());
        	dlgMain.setTitle(connectorManager.getString("security.dlgName_Node")+" "+ node.getName());
        }
        else
        {
            //lbObject.setText("Permissions for Document: " + node.getName());
        	dlgMain.setTitle(connectorManager.getString("security.dlgName_Document")+" "+ node.getName());
            docPermission = true;
        }
    }
    private void loadAssignments()
    {
        Vector assigns = new Vector();
        assigns.addElement(connectorManager.getString("security.auto"));
        assigns.addElement(AA_BROWSE_DENYOTHERS);
        assigns.addElement(AA_BROWSE_LEAVEOTHERS);
        assigns.addElement(AA_INPUT_DENYOTHERS);
        assigns.addElement(AA_INPUT_LEAVEOTHERS);
        assigns.addElement(AA_FACCESS_DENYOTHERS);
        assigns.addElement(AA_FACCESS_LEAVEOTHERS);
        assigns.addElement(AA_SUPERVISOR);
        comQuickAssign = new JComboBox(assigns);
        comQuickAssign.setSelectedIndex(0);
    }
    private void initControls()
    {
        loadAssignments();
        dlgMain = new JDialog();
        dlgMain.setTitle(connectorManager.getString("security.label"));
        pnMain = new JPanel();
        pnDPermissions = new JPanel();
        pnEPermissions = new JPanel();
        pnAllPermissions = new JPanel();
        lblPermission = new JLabel(connectorManager.getString("security.Permission"));
        lblEffective  = new JLabel(connectorManager.getString("security.Effective"));
        lblDirect = new JLabel(connectorManager.getString("security.Direct"));
        pnObject = new JPanel();
        btAdd = new JButton(connectorManager.getString("security.Add"));
        btRemove = new JButton(connectorManager.getString("security.Remove"));
        btOK = new JButton(connectorManager.getString("security.OK"));
        btCancel = new JButton(connectorManager.getString("security.Cancel"));
        btnReset = new JButton(connectorManager.getString("security.Reset"));
        btNewEntry = new JButton("");
        btNewEntry.setToolTipText(connectorManager.getString("security.NewEntry"));
        btApply = new JButton(connectorManager.getString("security.Apply"));
        btHelp = new JButton("");
        btReport = new JButton(connectorManager.getString("security.PrintPermissionReport"));
        lbObject = new JLabel();
        btWarning = new JButton("!");
        lstPrincipals = new JList();
        lstPrincipals.setCellRenderer(new PrincipalsCellRenderer());
        listModel = new PrincipalListModel();
        lstPrincipals.setModel(listModel); 
        lstPrincipals.setSelectionMode(0);
        scPrincipals = new JScrollPane();
        scDpermission = new JScrollPane();
        scEpermission = new JScrollPane();
    }
    private void initComponents() 
    {
        initControls();
        dlgMain.getContentPane().setLayout(null);
        pnMain.setLayout(null);
        //pnMain.setBorder(new BevelBorder(BevelBorder.RAISED));
        //pnMain.setBorder(new Border());
                
        //scPrincipals.setBorder(new TitledBorder(new SoftBevelBorder
        //                              (BevelBorder.RAISED), "Groups & Users"));
	scPrincipals.setBounds(5, 12, 188, 176);
        scPrincipals.getViewport().setView(lstPrincipals);
        pnMain.add(scPrincipals);        
        
        pnDPermissions.setLayout(null);
        pnEPermissions.setLayout(null);
        pnAllPermissions.setLayout(null);
        
        if(!isWebClient){
        	pnDPermissions.setBackground(Color.WHITE);
        	pnEPermissions.setBackground(Color.WHITE);
        	pnAllPermissions.setBackground(Color.WHITE);
        }
/*        pnEPermissions.setBorder(new LineBorder(Color.RED));
        pnDPermissions.setBorder(new LineBorder(Color.blue));
        pnAllPermissions.setBorder(new LineBorder(Color.ORANGE));
*/        //pnDPermissions.setBorder(new TitledBorder
        //      (new SoftBevelBorder(BevelBorder.LOWERED), "Direct Permissions"));
        
        //pnEPermissions.setBorder(new TitledBorder
        //   (new SoftBevelBorder(BevelBorder.LOWERED), "Effective Permissions"));
        //pnDPermissions.setBorder(new TitledBorder("Direct Permissions"));
        //pnEPermissions.setBorder(new TitledBorder("Effective Permissions"));
        
/*        scDpermission.setBorder(new TitledBorder(new SoftBevelBorder
                (BevelBorder.RAISED), "Direct Permissions"));
        scDpermission.setBounds(205, 40, 155, 310);
        scDpermission.getViewport().setView(pnDPermissions);

        scEpermission.setBorder(new TitledBorder(new SoftBevelBorder
                (BevelBorder.RAISED), "Effective Permissions"));
        scEpermission.setBounds(380, 40, 155, 310);
        scEpermission.getViewport().setView(pnEPermissions);
        
        pnMain.add(scDpermission);
        pnMain.add(scEpermission);*/
        pnAllPermissions.setBorder(new SoftBevelBorder(BevelBorder.LOWERED));
        pnAllPermissions.setBounds(204, 40, 328, 276);
        pnMain.add(pnAllPermissions); 
              
        lblPermission.setBounds(8, 8, 100, 10);
        pnAllPermissions.add(lblPermission);
        
        lblDirect.setBounds(170, 8, 70, 10);
        pnAllPermissions.add(lblDirect);
        
        lblEffective.setBounds(250, 8, 100, 10);
        pnAllPermissions.add(lblEffective);
        
        if (!isWebClient){ 
        	lblPermission.setForeground(new Color(7, 38, 58));
        	lblEffective.setForeground(Color.GRAY);
        }
        
        pnDPermissions.setBounds(156, 19, 32, 300);
        pnAllPermissions.add(pnDPermissions); 
        
        pnEPermissions.setBounds(240, 19, 32, 300);
        pnAllPermissions.add(pnEPermissions); 
        
        
        //pnObject.setLayout(null);
	        //pnObject.setBorder(new SoftBevelBorder(BevelBorder.LOWERED));

	        //lbObject.setBounds(5, 5, 350, 20);
	        //pnObject.add(lbObject);

	        //pnMain.add(pnObject);

	        //pnObject.setBounds(20, 20, 528, 30);
        
        dlgMain.getContentPane().add(pnMain);
        pnMain.setBounds(5, 5, 540, 320);
        int top = 353;
        btnReset.setMnemonic('e');
        dlgMain.getContentPane().add(btnReset);
        btnReset.setBounds(48, top, 88, btnHeight);
        btnReset.setIcon(Images.reset);
        
        btOK.setMnemonic('O');
        dlgMain.getContentPane().add(btOK);
        btOK.setBounds(280, top, btnWidth, btnHeight);
        dlgMain.getContentPane().add(btCancel);
        btCancel.setMnemonic('C');
        btCancel.setBounds(366, top, btnWidth, btnHeight);
        btApply.setMnemonic('A');
        dlgMain.getContentPane().add(btApply);
        btApply.setBounds(455, top, btnWidth, btnHeight);

        btReport.setMnemonic('t');
        if(!isWebClient)
        	btReport.setForeground(new Color(24, 50, 118));
        btReport.setBorderPainted(false);
        btReport.setFocusPainted(false);
        btReport.setContentAreaFilled(false);
        dlgMain.getContentPane().add(btReport);
        btReport.setIconTextGap(1);
        btReport.setBounds(0, 318, 185, btnHeight);
        btReport.setHorizontalAlignment(SwingConstants.LEFT);

        btHelp.setMnemonic('H');
        dlgMain.getContentPane().add(btHelp);
        btHelp.setBounds(2, top, 36, btnHeight);
        btHelp.setBorderPainted(false);
        btHelp.setFocusPainted(false);
        btHelp.setIcon(Images.help);
        btHelp.setHorizontalAlignment(SwingConstants.LEFT);
        btHelp.setContentAreaFilled(false);
	    /**
	     * Code added by Vijaypriya.B.K on March 23 2009 - Disabling help button in security for web top client
	     */
	    try{    
		    String path = VWCUtil.getHome() + "\\help\\VWUSER.chm";
		    System.out.println("Help : " + path);
		    if (path == null ||(path != null && !new File(path).exists()))
		    	btHelp.setEnabled(false);
	    }catch(Exception ex){
	    	
	    }	
	    
        
        btAdd.setMnemonic('d');
        //btAdd.setIcon(Images.add);
        pnMain.add(btAdd);
        btAdd.setBounds(4, 200, 86, btnHeight);
        
        btRemove.setMnemonic('R');
        //btRemove.setIcon(Images.remove);
        pnMain.add(btRemove);        
        btRemove.setBounds(94, 200, 96, btnHeight);
        
        btNewEntry.setMnemonic('N');
        pnMain.add(btNewEntry);
        btNewEntry.setBounds(508, 13, 24, btnHeight);
        btNewEntry.setIcon(Images.newentry);
        

        
        btWarning.setBounds(425, 355, 20, btnHeight);
        btWarning.setVisible(false);
        btWarning.setForeground(Color.red);
        btWarning.setMargin(new Insets(5, 5, 5, 5));
        btWarning.setFont(new Font("Tahoma", 1, 14));
        pnMain.add(btWarning);

    
        pnMain.add(comQuickAssign);
	        comQuickAssign.setBounds(204, 13, 302, btnHeight);
 
	    lineBorderLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
	    lineBorderLabel.setBounds(12, 341, 523, 1);
	    dlgMain.getContentPane().add(lineBorderLabel); 

	    whiteLineBorderLabel.setBorder(BorderFactory.createLineBorder(Color.white));
	    whiteLineBorderLabel.setBounds(14, 342, 524, 1);
	    dlgMain.getContentPane().add(whiteLineBorderLabel); 
	    
	        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	dlgMain.setSize(560, 416);
        dlgMain.setLocation((screenSize.width-560)/2,(screenSize.height-416)/2);
        dlgMain.setResizable(false);
        //------------------------Listeners-------------------------------------
        lstPrincipals.addKeyListener(this);
        btAdd.addKeyListener(this);
        btRemove.addKeyListener(this);
        btApply.addKeyListener(this);
        comQuickAssign.addKeyListener(this);
        btNewEntry.addKeyListener(this);
        btHelp.addKeyListener(this);        
        secAdapter = new WindowAdapter(){
            public void windowClosing(WindowEvent evt){
                closeDialog();
            }
            public void windowLostFocus(WindowEvent e){
            	if(windowActive){
                dlgMain.requestFocus();
                dlgMain.toFront();
            }
            }
            public void windowDeactivated(WindowEvent e) {
            	if(windowActive){
                dlgMain.requestFocus();
                dlgMain.toFront();
            }
            }
            public void windowStateChanged(WindowEvent e) {
                dlgMain.toFront();
            }
             public void windowDeiconified(WindowEvent e) {
                dlgMain.toFront();
            }
        };
        dlgMain.addWindowListener(secAdapter);
        btOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	if(!setACLFlag)
            		setACL();
                closeDialog(); 
            }
        });
        btApply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	setACLFlag = true;
                setACL();
                displayPermissions();
            }
        });
        btCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                closeDialog();
            }
        });
        btnReset.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                resetDialog();
            }
        });
        comQuickAssign.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                quickAssign((String) comQuickAssign.getSelectedItem());
            }
        });
        btAdd.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                showPrincipals();
            }
        });
        btRemove.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                removeAcl();
            }
        });
        btNewEntry.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                newEntry();
            }
        });
        btHelp.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                fireHelp();
            }
        });        
        lstPrincipals.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                setActivePrincipal();
            }
        });
        btReport.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
            	generateReport();
            }
        });        
        //Enabling or disabling Reset and Report button based on listbox contents are 0 or non 0.       
        if(listModel.getSize()==0){
        	this.btnReset.setEnabled(false);
        	/**
             * This code is added for enabling and disabling the report button based on the listbox content.
             * Issue no: PVW6.10193
             * Added by: Vijaypriya.B.K on 30 Dec 2008.
             */
        	this.btReport.setEnabled(false);
        }
        else{
        	this.btnReset.setEnabled(true);
        	this.btReport.setEnabled(true);
        }
        
        //------------------------Listeners-------------------------------------
    }
    public void keyPressed(KeyEvent e){
        if (e.getKeyCode() == KeyEvent.VK_ESCAPE) closeDialog();
        }
    public void keyReleased(KeyEvent e){
        }
    public void keyTyped(KeyEvent e){
        }
    private void setActivePrincipal()
    {
    	//Enabling or disabling Reset button based on listbox contents are 0 or non 0.       
        if(listModel.getSize()==0){
        	this.btnReset.setEnabled(false);
        	this.btReport.setEnabled(false);
        }
        else{
        	this.btnReset.setEnabled(true);
        	this.btReport.setEnabled(true);
        }
        if (listModel.getSize() == 0 || lstPrincipals.getSelectedIndex() < 0 ) 
                                                                         return;
        String selectedPrincipal = ((JLabel) lstPrincipals.
                                                  getSelectedValue()).getText();
        if (selectedPrincipal.equals("") || 
                              selectedPrincipal.equals(activePrincipal)) return;
        activePrincipal = selectedPrincipal;
        displayPermissions();
    }
    //Function for Issue 793
    public void resetDialog(){
    	int result=-2;
    	windowActive = false;
    	result=JOptionPane.showConfirmDialog(dlgMain,connectorManager.getString("security.msg_1"),connectorManager.getString("security.permission_Title"),JOptionPane.YES_NO_OPTION);
    	windowActive = true;
    	if(result!=-2){
    		dlgMain.requestFocus();
    		dlgMain.toFront();
    	}
 		try{
    		if(result==0){
	    		int aclID=node.getAclId();
	    		Vector vResult=new Vector();
	   			int x=vwc.resetSecurityDetails("VWRoom",session,node.getId(),aclID,vResult);
   				listModel.removePrincipal((UIPrincipal) listModel.getElementAt(lstPrincipals.getSelectedIndex()));
				pnDPermissions.removeAll();
				pnDPermissions.paintImmediately(pnDPermissions.getVisibleRect());
				pnEPermissions.removeAll();
				pnEPermissions.paintImmediately(pnEPermissions.getVisibleRect());
				lstPrincipals.clearSelection();
				if (listModel.getSize() > 0) lstPrincipals.setSelectedIndex(0);
			}
 		}catch(Exception e){
  		
  		}
    }
    
    private void displayPermissions()
    {
	displayPermissionLabel(activePrincipal);
        displayDPermissions(activePrincipal);
        getEffectiveAcl(activePrincipal);
        displayEPermissions(activePrincipal);
        
    }
    private void newEntry()
    {
    	// Change the Condition to reset the old state when user clicks on "New Entry" button.
    	
        if (activePrincipal.equals("")) return;// || 
                                //pnDPermissions.getComponentCount() != 0) return;
        dPermissions.put(activePrincipal, new UIAccess(docPermission, true));
        displayDPermissions(activePrincipal);
        comQuickAssign.setSelectedIndex(0);
        displayPermissionLabel(activePrincipal);
    }
    private void showPrincipals()
    {
        dlgMain.removeWindowListener(secAdapter);
        new Principals(dlgMain, true).setVisible(true);
        dlgMain.addWindowListener(secAdapter);
    }
    private void getDirectAcl()
    {
        Acl acl = new Acl(node);
        vwc.getNodeAcl(session, acl, true);
        Iterator iterator = acl.getEntries().iterator();
        AclEntry entry = null;
        while (iterator.hasNext())
        {
            entry = (AclEntry) iterator.next();
            addDirectEntry(entry);
        }
        if (listModel.getSize() > 0) lstPrincipals.setSelectedIndex(0);        
    }
    private void addDirectEntry(AclEntry entry)
    {
        UIPrincipal principal = new UIPrincipal(entry.getPrincipal());
        if (entry.isInherited())
        {
            listModel.addPrincipal(principal);
            return;
        }
        UIAccess access = new UIAccess(docPermission, true);
        access.setAccess(entry.getAccess());
        listModel.addPrincipal(principal, access);
    }
    private void getEffectiveAcl(String name)
    {
        Acl acl = new Acl(node);
        Principal principal = listModel.getPrincipalByName(name);
        vwc.getAclFor(session, acl, principal);
        Iterator iterator = acl.getEntries().iterator();
        AclEntry entry = null;
        while (iterator.hasNext())
        {
            entry = (AclEntry) iterator.next();
            UIAccess access = new UIAccess(docPermission, false);
            access.setAccess(entry.getAccess());
            ePermissions.put(entry.getPrincipal().getName(), access);
        }
        //if (principal.isGroup())
        //{
        //    checkDiscrepancy(principal, entry.getAccess().getMask());
        //}
        //else
        //    btWarning.setVisible(false);
    }
    private void checkDiscrepancy(Principal group, String mask)
    {
        Vector principals = new Vector();
        String pid = String.valueOf(group.getId());
        vwc.getPrincipalMembers(session, pid , principals);
        for (int i = 0; i < principals.size(); i++) 
        {
            Principal principal = (Principal) principals.elementAt(i);
            Acl acl = new Acl(node);
            vwc.getAclFor(session, acl, principal);
            Iterator iterator = acl.getEntries().iterator();
            AclEntry entry = null;
            while (iterator.hasNext())
            {
                entry = (AclEntry) iterator.next();
                if(!entry.getAccess().getMask().equals(mask))
                {
                	warning = connectorManager.getString("security.warning_1")+" '"+ group.getName()+"' "  
                			+" "+connectorManager.getString("security.warning_2")+" '"+principal.getName()+"' "
                			+" "+connectorManager.getString("security.warning_3") +
                			connectorManager.getString("security.warning_4") +
                			connectorManager.getString("security.warning_5")+" "+
                			connectorManager.getString("security.warning_6");
                    btWarning.setVisible(true);
                    return;
                }
            }
        }
        btWarning.setVisible(false);
    }
    private void showDiscrepancy()
    {
        dlgMain.removeWindowListener(secAdapter);
        JOptionPane optionPane = new JOptionPane();
        optionPane.showMessageDialog(null, warning, connectorManager.getString("security.warning_Title"),
                                               JOptionPane.WARNING_MESSAGE);
        dlgMain.addWindowListener(secAdapter);
    }
    private void displayEPermissions(String principal)
    {
	displayPermissionLabel(principal);
        UIAccess access = (UIAccess) ePermissions.get(principal);
        if (access == null) return;
        Vector perms = access.getPermissions();
        pnEPermissions.removeAll();
        for (int i = 0; i < perms.size(); i++)
        {
            JLabel permission = (JLabel) perms.get(i);
            if (permission.getIcon() == Images.grn)
        	permission.setIcon(Images.dgrn);
            if (permission.getIcon() == Images.dny )
        	permission.setIcon(Images.ddny);

            pnEPermissions.add(permission);
        }
        pnEPermissions.paintImmediately(pnEPermissions.getVisibleRect());
        pnAllPermissions.repaint();
    }
    private void quickAssign(String assign)
    {
        UIAccess access = (UIAccess) dPermissions.get(activePrincipal);
        if (access != null) access.quickAssign(assign);
    }
    private void setACL()
    {
        Vector principals = listModel.getPrincipals();
        int size = principals.size();
        if (size > 0)
        {
            Acl acl = new Acl(node);
            for (int i = 0; i < size; i++)
            {
                Principal p = ((UIPrincipal) principals.get(i)).getPrincipal();
                UIAccess uAccess = (UIAccess) dPermissions.get(p.getName());
                if (uAccess == null) continue;
                VWAccess vAccess = uAccess.getAccess();
                //Discard access with all permissions = inherit
                if (vAccess.inheritsAll()) continue;
                AclEntry entry = acl.newEntry(p);
                entry.setMask(vAccess.getMask());
            }
            vwc.setNodeAcl(session, acl);
        }
        else
            //clear all entries for this node
        vwc.setNodeAcl(session, new Acl(node));
    }
    private void removeAcl()
    {
        listModel.removePrincipal((UIPrincipal) listModel.getElementAt
                                            (lstPrincipals.getSelectedIndex()));
        pnDPermissions.removeAll();
        pnDPermissions.paintImmediately(pnDPermissions.getVisibleRect());
        pnEPermissions.removeAll();
        pnEPermissions.paintImmediately(pnEPermissions.getVisibleRect());
        lstPrincipals.clearSelection();
        if (listModel.getSize() > 0) lstPrincipals.setSelectedIndex(0);
    }
    private void displayPermissionLabel(String principal)
    {
        pnAllPermissions.removeAll();
        //btRemove.setEnabled(false);
        //pnDPermissions.paintImmediately(pnDPermissions.getVisibleRect());
        
        pnAllPermissions.add(lblPermission);
        pnAllPermissions.add(lblDirect);
        pnAllPermissions.add(lblEffective);
        
        UIAccess access = (UIAccess) ePermissions.get(principal);
        if (access == null) return;
        Vector perms = access.getPermissions();
        int iTop = 24, x = 2;
        try{
        for (int i = 0; i < perms.size(); i++)
        {
            JLabel permission = (JLabel) perms.get(i);
            JLabel permissionLabel = new JLabel(permission.getText());
            x = (i > 2 && i < 9 ?40:15);
            permissionLabel.setBounds(x, iTop, 152, 20);
            iTop += 19;           
            pnAllPermissions.add(permissionLabel);
            //btRemove.setEnabled(true);
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        
        pnAllPermissions.add(pnDPermissions); 
        pnAllPermissions.add(pnEPermissions); 
        
        //pnAllPermissions.revalidate();
        //pnAllPermissions.repaint();
        pnAllPermissions.paintImmediately(pnAllPermissions.getVisibleRect());
    }
    
    private void displayDPermissions(String principal)
    {
        pnDPermissions.removeAll();
        btRemove.setEnabled(false);
        pnDPermissions.paintImmediately(pnDPermissions.getVisibleRect());
        UIAccess access = (UIAccess) dPermissions.get(principal);
        if (access == null) return;
        Vector perms = access.getPermissions();
        for (int i = 0; i < perms.size(); i++)
        {
            JLabel permission = (JLabel) perms.get(i);
            pnDPermissions.add(permission);
            btRemove.setEnabled(true);
        }
        pnDPermissions.paintImmediately(pnDPermissions.getVisibleRect());
        pnAllPermissions.repaint();
    }
    private void fireHelp()
    {
        String path = VWCUtil.getHome() + "\\help\\VWUSER.chm";
        if(path != null) 
        {
            try
            {
                //java.lang.Runtime.getRuntime().exec("winhlp32.exe " + path);
                java.lang.Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + path);
            }
            catch(java.io.IOException ioe){}
        }
    }
    private void generateReport()
    {
    	dlgMain.removeWindowListener(secAdapter);
    	Session sessionObj = vwc.getSession(session);
        Vector htmlContents = new Vector();
        String err = "";
        String title = PRODUCT_NAME + " Security Report";
     
    	htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
		htmlContents.add("<html>");
		htmlContents.add("<head>");
		htmlContents.add("<title>" + title + "</title>");
		htmlContents.add("<style>");
		htmlContents.add("TH {");
		htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 12px;COLOR='#336699'; font-weight:bold");
		htmlContents.add("}");
		htmlContents.add("TD {");
		htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 11px");
		htmlContents.add("}");
		htmlContents.add("</style>");
		htmlContents.add("</head>");
		htmlContents.add("<body BGCOLOR='#F7F7E7'>");
		htmlContents.add("<table border=1 width=475 cellspacing=0 cellpadding=0 ><tr><td align=center><font size='4'><b>"+ title + "</b></font></td></tr></table>");
		htmlContents.add("<p></p>");    
        
		htmlContents.add("<table border=1 width=475 cellspacing=0 cellpadding=0 ><tr>");
		htmlContents.add("<td width=180><b>" + PRODUCT_NAME + " Room:</b> " +  sessionObj.room + "</td>");
		htmlContents.add("<td width=295><b>"+ (node.getType() == Node.DOCUMENT ? "Document " : "Node ") + "Name:</b> " +  node.getName() + "</td>");             
		htmlContents.add("</tr></table>");
		htmlContents.add("<p></p>");
		if (listModel == null || listModel.getSize() == 0) err = "No user/groups available";
		Vector principals = listModel.getPrincipals();        
		for (int index = 0; index < principals.size(); index++) {
			try{
			boolean isDirectAccessExist = true;
			htmlContents.add("<table border = 1 width=475 cellspacing=0 cellpadding=0>");
			Principal currentPrincipal = ((UIPrincipal)principals.elementAt(index)).getPrincipal();

			htmlContents.add("<tr><td width=20% BGCOLOR=#cccc99 align=center >Name : "+  currentPrincipal.getName()+ "</td><td width=20% colspan=2 BGCOLOR=#cccc99 align=center> Type : "+ (currentPrincipal.getType() == Principal.GROUP_TYPE?"Group":"User") + "</td></tr>");
			UIAccess directAccess = (UIAccess) dPermissions.get(currentPrincipal.getName());
			UIAccess effectiveAccess = (UIAccess) ePermissions.get(currentPrincipal.getName());
			if (effectiveAccess == null){
		        getEffectiveAcl(currentPrincipal.getName());
		        effectiveAccess = (UIAccess) ePermissions.get(currentPrincipal.getName());
			}
			
			if (directAccess == null)
				isDirectAccessExist = false;
			
			VWAccess directPermission = null;
			if (isDirectAccessExist)
					directPermission = directAccess.getAccess();			
			VWAccess effectivePermission = effectiveAccess.getAccess();
			htmlContents.add("<tr><td width=20% BGCOLOR=#cccc99>Permissions</td><td width=20% BGCOLOR=#cccc99>Direct Permissions</td><td width=20% BGCOLOR=#cccc99>Effective Permissions</td></tr>");
			htmlContents.add("<tr><td width=20%>Assign</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canAssign()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canAssign()) + "</td></tr>");
			htmlContents.add("<tr><td width=20%>Delete</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canDelete()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canDelete())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Share</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canShare()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canShare())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Launch</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canLaunch()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canLaunch())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Export</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canExport()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canExport())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Print/Fax</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canPrint()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canPrint())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Mail</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canMail()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canMail())+  "</td></tr>");
			htmlContents.add("<tr><td width=20%>Send To</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canSendTo()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canSendTo())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Check In/Out</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canCheckInOut()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canCheckInOut())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Modify</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canModify()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canModify())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Create</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canCreate()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canCreate())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>View</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canView()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canView())+ "</td></tr>");
			htmlContents.add("<tr><td width=20%>Navigate</td><td width=20%>&nbsp;"+ ((isDirectAccessExist)?getPermissionValue(directPermission.canNavigate()):"&nbsp;") +"</td><td width=20%>&nbsp;"+getPermissionValue(effectivePermission.canNavigate())+ "</td></tr>");
			htmlContents.add("</table>");
			htmlContents.add("<p></p>");
        }catch(Exception ex){
        	err = "Error generating report - " + ex.getMessage();
        	//htmlContents.add(err);
        }	
		}
		htmlContents.add("<table><tr><td>Legend : </td><td>G - Granted, </td><td>D - Denied, </td><td>I - Inherited</td></tr></table>");
		htmlContents.add("<table><tr><td><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></td></tr></table>");
        htmlContents.add("</body></html>");
        try{
	        JFileChooser fileChooser = new JFileChooser();
	        File file = new  File("c:\\SecurityPermission.html");
	        fileChooser.setSelectedFile(file);
	        int returnVal = fileChooser.showSaveDialog(dlgMain);
	        
	        String savePath="";
	        if(returnVal == JFileChooser.APPROVE_OPTION)
	        {
	        	savePath=fileChooser.getSelectedFile().getPath();	        	
	            fileChooser.setVisible(false);
	            fileChooser=null;
	        }
        File reportFile = new File(savePath);        
        VWCUtil.writeListToFile(htmlContents, reportFile.getPath(), err, "UTF-8");
        }catch(Exception ex){
        	
        }
        dlgMain.addWindowListener(secAdapter);
    }
    public String getPermissionValue(int permission){
    	String result = "G";
    	if (permission == 0){
    		result = "D";
    	}else if (permission == 1){
    		result = "G";
    	}else
    		result = "I";
    	return result;
    }
    private void closeDialog() 
    {
        active = false;
        dlgMain.getOwner().dispose();
    }
    public void run()
    {
        show();
    }
    public void show()
    {
	if (vwc == null || node == null || node.getId() <= 0) return;
	/*
 	 * new frame.show(); method is replaced with new frame.setVisible(true); 
 	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
 	 */
        dlgMain.setVisible(true);
    }
    //------------------------------Classes-------------------------------------
    class PrincipalsCellRenderer extends DefaultListCellRenderer 
    { 
       public Component getListCellRendererComponent(JList list, Object value, 
                            int index, boolean isSelected, boolean cellHasFocus) 
        { 
            Component retValue = super.getListCellRendererComponent( 
                             	list, value, index, isSelected, cellHasFocus);
            setText( ((JLabel) value).getText());
            setIcon( ((JLabel) value).getIcon());
            return retValue;
         } 
    }
    //--------------------------------------------------------------------------
    class Principals extends JDialog 
    {
         private JScrollPane scPPrincipals;
         private JList lstPPrincipals;
         private JButton btShowUsers, btPOK, btPCancel, btGroupMembers;
         private KeyBuffer keyBuffer = new KeyBuffer();;
         
         private PrincipalListModel PlistModel;
         private Vector principals = new Vector(); 

         public Principals(JDialog parent, boolean modal)
         {
            super(parent, modal);
            new Images();
            initComponents();
            loadGroups();
            keyBuffer.start();
         }
         private void initComponents() 
         {
            btShowUsers = new JButton(connectorManager.getString("security.ShowUsers"));
            btGroupMembers = new JButton();
            btPOK = new JButton(connectorManager.getString("security.OK"));
            btPCancel = new JButton(connectorManager.getString("security.Cancel"));
            scPPrincipals = new JScrollPane();
            lstPPrincipals = new JList();
            lstPPrincipals.setCellRenderer(new PrincipalsCellRenderer());
            PlistModel = new PrincipalListModel();
            lstPPrincipals.setModel(PlistModel); 
            //lstPPrincipals.setAutoscrolls(true);
            getContentPane().setLayout(null);
            setTitle("Groups & Users");
            lblUsersAndGroups = new JLabel(connectorManager.getString("security.Groups&Users>>"));   
    		lblUsersAndGroups.setToolTipText(connectorManager.getString("security.Clickforsorting"));
            lblUsersAndGroups.setBounds(8, 5, 200, 10);
            getContentPane().add(lblUsersAndGroups);
            
            scPPrincipals.setViewportView(lstPPrincipals);
            scPPrincipals.setBounds(10, 20, 340, 354);
            getContentPane().add(scPPrincipals);
            
            lblFilter = new JLabel(connectorManager.getString("security.Startswith"));
            lblFilter.setBounds(10, 390, 80, 10);
            getContentPane().add(lblFilter);

            txtFilter = new JTextField();
            txtFilter.setBounds(110, 385, 200, 20);
            lblUsersAndGroups.addMouseListener(new MouseAdapter(){
            	public void mouseClicked(MouseEvent me){  
            		String filter = txtFilter.getText().trim();	 
            		if(btShowUsers.isEnabled()){
            			loadGroups(filter);
            		}else{
            			loadGroups(filter);
            			loadUsers(filter);
            		}
               		PlistModel.sortList();
            	}
            });
            txtFilter.addKeyListener(new KeyAdapter(){
            	public void keyReleased(java.awt.event.KeyEvent event){
            		if(event.getSource()==txtFilter){
	            		String filter = txtFilter.getText().trim();	            		
	            		lstPPrincipals.removeAll();
	            		if (!filter.trim().equals("")) {
	                    	 PlistModel.removeAllPrincipals();
	                    }
	            		loadGroups(filter);
	            		if(!btShowUsers.isEnabled())
	            			loadUsers(filter);
            		}
            	}
            });
            
            getContentPane().add(txtFilter);
            btShowUsers.setMnemonic('S');
            getContentPane().add(btShowUsers);
            btShowUsers.setBounds(12, 416, btnMaxWidth, btnHeight);
            
            btGroupMembers.setMnemonic('M');
            getContentPane().add(btGroupMembers);
            btGroupMembers.setIcon(Images.usr);
            btGroupMembers.setToolTipText(connectorManager.getString("security.GroupMembers"));
            btGroupMembers.setBounds(110, 416, 28, btnHeight);
            btGroupMembers.setEnabled(false);
            
            btPOK.setMnemonic('O');
            getContentPane().add(btPOK);
            btPOK.setBounds(160, 416, btnMaxWidth, btnHeight);
            
            btPCancel.setMnemonic('C');
            getContentPane().add(btPCancel);
            btPCancel.setBounds(256, 416, btnMaxWidth, btnHeight);

            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            setSize(360, 480);//560, 480
            setLocation((screenSize.width-290)/2,(screenSize.height-420)/2);
            setResizable(false);
            
            
            btPCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
                   closeDialog();
                }
            });
            btPOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
                   addSelected();
                   closeDialog();
                }
            });
            btShowUsers.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
            	   String filter = txtFilter.getText().trim();	 
                   loadUsers(filter); 
                   btShowUsers.setEnabled(false);
                }
            });
            btGroupMembers.addActionListener(new ActionListener(){
            	public void actionPerformed(ActionEvent evt){
            		int gpId = lstPPrincipals.getSelectedIndex();
            		if(gpId!=-1){
	            		String selectedGroupName = lstPPrincipals.getSelectedValue().toString();
	            		Principal p1 = null; 
	            		int count = -1;
	            		for (int i = 0; i < principals.size(); i++)
	            		{
	            			Principal p = (Principal) principals.get(i);
	            			if (p.getType() == Principal.GROUP_TYPE)
	            			{
	            				if(selectedGroupName.trim().equalsIgnoreCase(p.getName().trim()) && p.getType()==Principal.GROUP_TYPE){
	            					gpId = p.getId();
	            					count = i;
	            					break;
	            				}
	            			}else{
	            				gpId = -1;
	            				count = -1;
	            			}
	            		}
	            		if(count!=-1 && gpId!=-1){
		            		p1 = (Principal) principals.get(count);
		            		Vector groupMembersPrincipals = new Vector();
		            		String groupId = String.valueOf(p1.getId());
		            		String selGroupName = p1.getName();
		            		
		            		vwc.getPrincipalMembers(session, groupId , groupMembersPrincipals);

		        			if(groupMembersPrincipals.size()>0){
		        				new GroupMembers(dlgMain, true, p1).setVisible(true);
		        			}else{
		        				JOptionPane.showMessageDialog(dlgMain, selGroupName+connectorManager.getString("security.group"));
		        			}
		            		//btGroupMembers.setEnabled(true);
		            		//dlgMain.addWindowListener(secAdapter);
	            		}
            		}
            	}
            });
            lstPPrincipals.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent evt) {
                if (evt.getClickCount() > 1) 
                {
                    addSelected();
                    closeDialog();
                }if(evt.getClickCount()==1){
                	int gpId = lstPPrincipals.getSelectedIndex();
                	int gpIdList[] = lstPPrincipals.getSelectedIndices();
                	if(gpId!=-1){
                		String selectedGroupName = lstPPrincipals.getSelectedValue().toString();
	            		for (int i = 0; i < principals.size(); i++)
	            		{
	            			Principal p = (Principal) principals.get(i);
	            			if (p.getType() == Principal.GROUP_TYPE)
	            			{
	            				if(gpIdList.length>1)
	            				{
	            					btGroupMembers.setEnabled(false);
	            				}else{
	            					if(selectedGroupName.trim().equalsIgnoreCase(p.getName().trim())){
	            						btGroupMembers.setEnabled(true);
	            						break;
	            					}
	            				}
	            			}else{
	            				btGroupMembers.setEnabled(false);
	            			}
	            		}
                	}else{
                		btGroupMembers.setEnabled(false);
                	}
                }
                }
            });
            lstPPrincipals.addKeyListener(new KeyAdapter(){
            public void keyPressed(KeyEvent e){
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) closeDialog();
                    keyBuffer.appendKey(e.getKeyChar());
                }
            public void keyReleased(KeyEvent e){
                }
            public void keyTyped(KeyEvent e){
                }
            });
             
         }
         private void closeDialog() 
         {
             keyBuffer.kill();
             setVisible(false);
             dispose();
         }
         public void locate(String prefix)
         {
            int prevIndex = lstPPrincipals.getSelectedIndex(); 
            int index = lstPPrincipals.getNextMatch(prefix, prevIndex, 
                                                         Position.Bias.Forward);
            if (index < 0) 
                index = lstPPrincipals.getNextMatch(prefix, prevIndex, 
                                                        Position.Bias.Backward);
            if (index >= 0)
            {
                lstPPrincipals.setSelectedIndex(index);
                lstPPrincipals.ensureIndexIsVisible(index); 
                lstPPrincipals.paintImmediately(lstPPrincipals.getVisibleRect());
            }
         }
         private void addSelected()
         {
            int[] ids = lstPPrincipals.getSelectedIndices();
            UIPrincipal uip = null;
            for (int i = 0; i < ids.length; i++)
            {
                uip = (UIPrincipal) PlistModel.getElementAt(ids[i]);
                listModel.addPrincipal(uip);
            }
            if (uip != null)
            {
                lstPrincipals.clearSelection();
                lstPrincipals.setSelectedValue(uip, true);
                newEntry();
            }
        }
        
         private void loadGroups(){
        	 loadGroups("");
         }
         private void loadUsers(){
        	 loadUsers("");
         }
         
         private void loadGroups(String startsWith)
         {
        	 setCursor(new Cursor(Cursor.WAIT_CURSOR));
        	 vwc.getPrincipals(session, principals);
        	 for (int i = 0; i < principals.size(); i++)
        	 {
        		 Principal p = (Principal) principals.get(i);        		 
        		 if (p.getType() == Principal.GROUP_TYPE)
        		 {
        			 String upperUsername = startsWith.toUpperCase();
        			 if(p.getName().trim().toUpperCase().startsWith(upperUsername)){                		
        				 UIPrincipal principal = new UIPrincipal(p);                		 
        				 PlistModel.addPrincipal(principal);                		 
        			 }
        		 }
        	 }
        	 PlistModel.update();
        	 setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
         }
         private void loadUsers(String startsWith)
         {
        	 setCursor(new Cursor(Cursor.WAIT_CURSOR));
        	 for (int i = 0; i < principals.size(); i++)
        	 {
        		 Principal p = (Principal) principals.get(i);
        		 if (p.getType() == Principal.USER_TYPE)
        		 {
        			 String upperUsername = startsWith.toUpperCase();
        			 if(p.getName().trim().toUpperCase().startsWith(upperUsername)){
        				 UIPrincipal principal = new UIPrincipal(p);                		
        				 PlistModel.addPrincipal(principal);
        			 }
        		 }
        	 }
        	 PlistModel.update();
        	 setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
         }
        private class KeyBuffer extends Thread
        {
            private String prefix = "";
            private boolean run = true;
            public KeyBuffer()
            {
                setPriority(Thread.MIN_PRIORITY);
            }
            public void appendKey(char c)
            {
                prefix+=c;
            }
            public void kill()
            {
                run = false;
            }
            public void run()
            {
                while (run)
                {
                    Util.sleep(100);
                    if (prefix.length() == 0) continue;
                    locate(prefix);
                    prefix = "";
                }
            }
        }
    }
//  --------------------------------------------------------------------------
    class GroupMembers extends JDialog 
    {
         private JScrollPane scPPrincipals;
         private JList lstPPrincipals;
         private JButton btPOK, btPCancel;
         //private KeyBuffer keyBuffer = new KeyBuffer();;
         
         private PrincipalListModel PlistModel;
         private Vector principals = new Vector(); 

         public GroupMembers(JDialog parent, boolean modal, Principal p)
         {
            super(parent, modal);
            String selectedGroupName = p.getName();
            int groupId = p.getId();
            setTitle(connectorManager.getString("security.GroupMembers_Title_1")+" "+selectedGroupName+connectorManager.getString("security.GroupMembers_Title_2"));
            new Images();
            initComponents();
            
    		if(groupId!=-1){
   				loadGroupMembers(groupId);	
    		}
            //keyBuffer.start();
         }
         private void initComponents() 
         {
        	 btPOK = new JButton(connectorManager.getString("security.OK"));
        	 btPCancel = new JButton(connectorManager.getString("security.Cancel"));
        	 scPPrincipals = new JScrollPane();
        	 lstPPrincipals = new JList();
        	 lstPPrincipals.setCellRenderer(new PrincipalsCellRenderer());
        	 PlistModel = new PrincipalListModel();
        	 lstPPrincipals.setModel(PlistModel); 
        	 //lstPPrincipals.setAutoscrolls(true);
        	 getContentPane().setLayout(null);

        	 scPPrincipals.setViewportView(lstPPrincipals);
        	 scPPrincipals.setBounds(10, 20, 340, 354);
        	 getContentPane().add(scPPrincipals);

        	 btPOK.setMnemonic('O');
        	 getContentPane().add(btPOK);
        	 btPOK.setBounds(160, 416, btnMaxWidth, btnHeight);

        	 btPCancel.setMnemonic('C');
        	 getContentPane().add(btPCancel);
        	 btPCancel.setBounds(256, 416, btnMaxWidth, btnHeight);

        	 Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        	 setSize(360, 480);
        	 setLocation((screenSize.width-290)/2,(screenSize.height-420)/2);
        	 setResizable(false);


        	 btPCancel.addActionListener(new ActionListener(){
        		 public void actionPerformed(ActionEvent evt) {
        			 closeDialog();
        		 }
        	 });
        	 btPOK.addActionListener(new ActionListener(){
        		 public void actionPerformed(ActionEvent evt) {
        			 addSelected();
        			 closeDialog();
        		 }
        	 });

        	 lstPPrincipals.addMouseListener(new MouseAdapter() {
        		 public void mouseClicked(MouseEvent evt) {
        			 if (evt.getClickCount() > 1) 
        			 {
        				 //addSelected();
        				 closeDialog();
        			 }if(evt.getClickCount()==1){
        				 System.out.println("##########################");
        			 }
        		 }
        	 });
        	 lstPPrincipals.addKeyListener(new KeyAdapter(){
        		 public void keyPressed(KeyEvent e){
        			 if (e.getKeyCode() == KeyEvent.VK_ESCAPE) closeDialog();
        			 //keyBuffer.appendKey(e.getKeyChar());
        		 }
        		 public void keyReleased(KeyEvent e){
        		 }
        		 public void keyTyped(KeyEvent e){
        		 }
        	 });
         }
         private void closeDialog() 
         {
             //keyBuffer.kill();
             setVisible(false);
             dispose();
         }
        
         private void addSelected()
         {
            int[] ids = lstPPrincipals.getSelectedIndices();
            UIPrincipal uip = null;
            for (int i = 0; i < ids.length; i++)
            {
                uip = (UIPrincipal) PlistModel.getElementAt(ids[i]);
                listModel.addPrincipal(uip);
            }
            if (uip != null)
            {
                lstPrincipals.clearSelection();
                lstPrincipals.setSelectedValue(uip, true);
                newEntry();
            }
        }
     
        /*private class KeyBuffer extends Thread
        {
            private String prefix = "";
            private boolean run = true;
            public KeyBuffer()
            {
                setPriority(Thread.MIN_PRIORITY);
            }
            public void appendKey(char c)
            {
                prefix+=c;
            }
            public void kill()
            {
                run = false;
            }
            public void run()
            {
                while (run)
                {
                    Util.sleep(100);
                    if (prefix.length() == 0) continue;
                    locate(prefix);
                    prefix = "";
                }
            }
        }*/
         
         private void loadGroupMembers(int gpId)
         {
        	 setCursor(new Cursor(Cursor.WAIT_CURSOR));
        	 String selectedGroupId = String.valueOf(gpId);
        	 vwc.getPrincipalMembers(session, selectedGroupId , principals);
        	 if(principals!= null && principals.size()>0){
        		 for (int i = 0; i < principals.size(); i++)
        		 {
        			 Principal p = (Principal) principals.get(i);        		 
        			 UIPrincipal principal = new UIPrincipal(p);       
        			 PlistModel.addPrincipal(principal);                		 
        		 }
        		 PlistModel.update();
        		 setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        	 }
         }
    }
    
    //--------------------------------------------------------------------------
    
    //--------------------------------------------------------------------------
    class PrincipalListModel extends AbstractListModel
    { 
    	private Vector principals = new Vector(); 
    	
    	private void update() 
    	{
    		//Updated 2-3-2005 to avoid delays in populating list
    		//sort(); 
    		fireContentsChanged(this, 0, getSize()); 
    	}
    	public void sortList()
    	{
    		if(sorted){
    			lblUsersAndGroups.setText(connectorManager.getString("security.Groups&Users>>"));
    			sort();
    		}
    		else{        		
    			lblUsersAndGroups.setText(connectorManager.getString("security.Groups&Users<<"));
    			sortDesc();
    		}
    	}
    	public void addPrincipal(UIPrincipal principal) 
    	{ 
    		if (contains(principal)) return;
    		principals.addElement(principal);
    		update();
    	} 
    	public void addPrincipal(UIPrincipal principal, UIAccess access) 
    	{
    		if (contains(principal)) return;
    		principals.addElement(principal);
    		dPermissions.put(principal.getText(), access);
    		update();
    	} 
    	public void removePrincipal(UIPrincipal principal) 
    	{ 
    		principals.removeElement(principal);
    		dPermissions.remove(principal.getText());
    		update(); 
    	} 
    	public int getSize() 
    	{ 
    		return principals.size(); 
    	} 
    	public Object getElementAt(int index) 
    	{ 
    		return (UIPrincipal) principals.elementAt(index); 
    	}
    	private boolean contains(UIPrincipal uip)
    	{
    		int count = getSize();
    		String name = uip.getText();
    		Principal  uiPrincipal = uip.getPrincipal();
    		int pType =  uiPrincipal.getType();
    		for (int i = 0; i < count; i++)
    		{
   				Principal principal  = ((UIPrincipal)principals.get(i)).getPrincipal();
    			if (name.equals(((UIPrincipal) principals.get(i)).getText()) && pType == principal.getType())
    				return true;
    		}
    		return false;
    	}
    	private void sort() 
    	{
    		sorted = false;
    		int count = getSize();
    		JLabel[] a = new JLabel[count];
    		for (int i = 0; i < count; i++)
    		{
    			a[i] = (JLabel) principals.get(i);
    		}
    		sortArray(Collator.getInstance(), a);
    		for (int i = 0; i < count; i++) 
    		{
    			principals.setElementAt(a[i], i);
    		}
    	}
    	private void sortArray(Collator collator, JLabel[] strArray) 
    	{
    		if (strArray.length == 1) return;
    		for (int i = 0; i < strArray.length; i++) 
    		{
    			for (int j = i + 1; j < strArray.length; j++) 
    			{
    				if(collator.compare(strArray[i].getText(), 
    						strArray[j].getText() ) > 0 ) 
    				{
    					JLabel tmp = strArray[i];
    					strArray[i] = strArray[j];
    					strArray[j] = tmp;
    				}
    			}
    		}
    	}
    	
    	//The below method sorts descending
    	private void sortDesc() 
    	{
    		sorted = true;
    		int count = getSize();
    		JLabel[] a = new JLabel[count];
    		for (int i = 0; i < count; i++)
    		{
    			a[i] = (JLabel) principals.get(i);
    		}
    		sortArrayDesc(Collator.getInstance(), a);
    		for (int i = 0; i < count; i++) 
    		{
    			principals.setElementAt(a[i], i);
    		}
    	}
    	private void sortArrayDesc(Collator collator, JLabel[] strArray) 
    	{
    		if (strArray.length == 1) return;
    		for (int i = 0; i < strArray.length; i++) 
    		{
    			for (int j = i + 1; j < strArray.length; j++) 
    			{
    				if(collator.compare(strArray[i].getText(), 
    						strArray[j].getText() ) < 0 ) 
    				{
    					JLabel tmp = strArray[i];
    					strArray[i] = strArray[j];
    					strArray[j] = tmp;
    				}
    			}
    		}
    	}
    	
    	public Principal getPrincipalByName(String name) 
    	{ 
    		for (int i = 0; i < principals.size(); i++)
    		{
    			UIPrincipal uip = (UIPrincipal) principals.get(i);
    			if (uip.getText().equals(name))
    				return uip.getPrincipal();
    		}
    		return null;
    	}
    	public Vector getPrincipals()
    	{
    		return this.principals;
    	}
    	
    	public void removeAllPrincipals() 
    	{ 
    		principals.removeAllElements();            
    		update(); 
    	}
    } 
     public static void main(String[] args) {
    	    try 
            {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            VWClient vwc = new VWClient("c:\\temp");
            int sid = vwc.login("Syntaxwkt199", "VW7", "admin", "vw");
            Security security = new Security(vwc, sid, new Node(8));
            new Thread(security).start();	
            }
            catch(Exception e) 
            {
        	e.printStackTrace();
            }
	}
}
