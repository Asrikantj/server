package com.computhink.vwc;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.image.Images;
import com.computhink.vws.server.VWS;

public class VWRouteSummaryInfo extends JDialog implements VWCConstants{
	private static ResourceManager resourceManager=ResourceManager.getDefaultManager();
	
	//public String documentName = null;
	private static VWClient vwc;
	private VWRouteSummaryInfoTable infoTable;
	//public static JFrame routeInfoMainDlg;
	public static JPanel routeInfoPanel;
	public static JScrollPane scRouteInfoPanel;
	JPanel documentInfoPanel = null;
	JLabel lblDocumentInfo = null;
	public JFrame parent = null;
	int width = 0, height = 0, top = 0;
	public VWRouteSummaryInfo() {
		
	}
	
	public VWRouteSummaryInfo(VWClient vwc) {
		this.vwc = vwc;
		routeInfoPanel = new JPanel();	
	}
	public VWRouteSummaryInfo(JFrame parent, boolean modal, VWClient vwc, int sessionID, int documentID, String documentName) {
		super(parent, true);
		this.vwc = vwc;
		this.parent = parent;
		routeInfoPanel = new JPanel();	
		VWClient.printToConsole("inside VWRouteSummaryInfo.....");
		setTitle(resourceManager.getString("RouteSummaryInfo.Title") + " - "+documentName);
		setIconImage(Images.task.getImage());
		initRouteInfoComponents(null, documentID, 0, 0, 0, sessionID, 0);		
		
		setSize(500, 350);
		setLocation((parent.getWidth()/2)-100, (parent.getHeight()/2)-100);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	
	public VWRouteSummaryInfoTable initRouteInfoComponents(JPanel propertiesPanel, int documentID, int parentWidth, int parentHeight, int parentTop, int sessionID, int left) {
		VWClient.printToConsole("inside summaryinfo initcomponents.....parentWidth :"+parentWidth+ " parentHeight :"+parentHeight+" parentTop :"+parentTop);
		
		width = parentWidth; height = parentHeight; top = parentTop;
		scRouteInfoPanel = new JScrollPane();
		if (propertiesPanel == null) {
			getContentPane().add(routeInfoPanel);			
			routeInfoPanel.setBounds(5, 10, 485, 308);
			scRouteInfoPanel.setBounds(5, 10, 485, 298);
		} else {			
			documentInfoPanel = new JPanel();
			//documentInfoPanel.setVisible(false);
			//routeInfoPanel.setVisible(false);
			documentInfoPanel.setLayout(null);
			lblDocumentInfo = new JLabel();
			documentInfoPanel.setBounds(left, top, width, 20);
			documentInfoPanel.setBackground(new Color(112,146,190));
			propertiesPanel.add(documentInfoPanel);
			
			lblDocumentInfo.setBounds(0,0,width,20);
			lblDocumentInfo.setText(resourceManager.getString("RouteSummaryInfo.Title"));
			lblDocumentInfo.setFont(new Font("ARIAL", Font.BOLD, 12));
			lblDocumentInfo.setForeground(Color.white);			
			documentInfoPanel.add(lblDocumentInfo);
			
			routeInfoPanel.setBounds(left, top+11, width, height);
			propertiesPanel.add(routeInfoPanel);
			scRouteInfoPanel.setBounds(0, 10, routeInfoPanel.getWidth(), height-10);
			//documentInfoPanel.setVisible(false);
			//routeInfoPanel.setVisible(false);
		}
		routeInfoPanel.setLayout(null);		
				
		infoTable = new VWRouteSummaryInfoTable(this);
		VWClient.printToConsole("After initializing the table object :"+infoTable);		
		scRouteInfoPanel.setViewportView(infoTable);
		routeInfoPanel.add(scRouteInfoPanel);		
		VWClient.printToConsole("Before calling loadSummaryInfoDetails....sessionID :"+sessionID+"documentID :"+documentID);
		loadSummaryInfoDetails(sessionID, documentID);
		VWClient.printToConsole("After calling loadSummaryInfoDetails....");
		//Component listener added for resizing the RouteSummary dialog and resize the table and columns accordingly
        /*ComponentListener cListener = new ComponentAdapter() {
        	public void componentResized(ComponentEvent evt) {
        		Component c = (Component) evt.getSource();
        		//Dimension screenSize1 = c.getSize();
        		//setSize(screenSize1);
        		routeInfoPanel.setBounds(0, 0, width, height);
        		//scRoutePane.setBounds(5, 10, dlgMain.getWidth()-30, dlgMain.getHeight()-62);
        		scRouteInfoPanel.setBounds(5, 10, width, height-10);
        		routeInfoPanel.repaint();
        		int tableWidth = scRouteInfoPanel.getWidth();
        		infoTable.setBounds(0, 0, tableWidth, height-20);
        		infoTable.repaint();
        	}
		};
        routeInfoPanel.addComponentListener(cListener);*/
		return infoTable;
	}
	
	private void loadSummaryInfoDetails(int sessionID, int docID) {
		VWClient.printToConsole("inside loadSummaryInfoDetails ");
		Session session = null;
		String rowData = null;
		VWS vws = null;
		try {
			VWClient.printToConsole("vwc :"+vwc);
			session = vwc.getSession(sessionID);
			VWClient.printToConsole("session :"+session);
	        if (session != null) {
		        vws = vwc.getServer(session.server);
		        VWClient.printToConsole("vws :"+vws);
		        if (vws != null) {
		        	VWClient.printToConsole("Before clearing infoTable...");
		        	infoTable.clearData();
		        	VWClient.printToConsole("After clearing infoTable...");
		        	//VW_VR_Getsettings
		        	VWClient.printToConsole("Before calling getDocumentProperties....");
		        	Vector<String> indices = vws.getDocumentProperties(session.room, sessionID, docID, new Vector());
		        	VWClient.printToConsole("After calling getDocumentProperties....");
		        	VWClient.printToConsole("indices :::"+indices);
		        	if (indices != null) {
		        		 Vector data = new Vector();
		        		 for (int i = 0; i< indices.size() ; i++){
		            		 rowData = "";    		 
		            		 String value = indices.get(i).toString();
		            		 StringTokenizer tokens = new StringTokenizer(value, Util.SepChar);
		            		 try{
		            			 rowData += tokens.nextToken() + Util.SepChar;// Index Name
		            			 rowData += tokens.nextToken() + Util.SepChar;// Index value
		            		 }catch(Exception ex){
		            			 //System.out.println("Exception in loadSummaryDetails" + ex.getMessage());
		            		 }
		            		 data.addElement(rowData);
		            	 }
		        		 infoTable.addData(data);
		        		 //infoTable.setLastProcessedRowSelected();
		        	}
		        }
	        }
		} catch (Exception e) {
			VWClient.printToConsole("Exception in loadSummaryInfoDetails : "+e.fillInStackTrace().getMessage().toString());
		} finally {
			vws = null; 
			session = null;
		}
	}
	
	/*public void showDocumentProperties(boolean isDocumentInfoEnabled) {
		VWClient.printToConsole("isDocumentInfoEnabled 1...."+isDocumentInfoEnabled);
		documentInfoPanel.setVisible(isDocumentInfoEnabled);
		routeInfoPanel.setVisible(isDocumentInfoEnabled);
	}*/
}

class VWRouteSummaryInfoTable extends JTable {
	public static int maxRowHeight = 0;
	public String documentName = null;
	public int ttlColumns=0;
	public int indexNameColumn=0;
	protected VWRouteSummaryInfo parent = null; 
	protected VWRouteSummaryInfoTableData m_data = null; 
	public VWRouteSummaryInfoTable(VWRouteSummaryInfo parent){
		super();
		new Images();
		VWClient.printToConsole("inside VWRouteSummaryInfoTable.....");
		//this.documentName = documentName;
		this.parent = parent;
		getTableHeader().setReorderingAllowed(false);
		m_data = new VWRouteSummaryInfoTableData(this);
		setCellSelectionEnabled(false);
		setAutoCreateColumnsFromModel(false);
		setModel(m_data);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		/setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		VWClient.printToConsole("scRouteInfoPanel width :"+parent.scRouteInfoPanel.getWidth());
		int x = (int)(parent.scRouteInfoPanel.getWidth() * 0.99f);
		int y = (int) (parent.scRouteInfoPanel.getHeight() * 0.99f);
		Dimension tableWith = new Dimension(x, y);
		
		//Dimension tableWith = parent.scRouteInfoPanel.getSize();
		VWClient.printToConsole("tableWith.width....."+tableWith.width);
		VWClient.printToConsole("Table column length....."+VWRouteSummaryInfoTableData.info_columns.length);
		for (int k = 0; k < VWRouteSummaryInfoTableData.info_columns.length; k++) {
			TableCellRenderer renderer;
			DefaultTableCellRenderer textRenderer;
			if(VWRouteSummary.isWebClient)
				textRenderer =new DefaultTableCellRenderer();
			else
				textRenderer =new VWRouteTableCellRenderer();
			textRenderer.setHorizontalAlignment(VWRouteSummaryInfoTableData.info_columns[k].m_alignment);
			renderer = textRenderer;
			VWRouteSummaryInfoRowEditor editor=new VWRouteSummaryInfoRowEditor(this);
			VWClient.printToConsole("xxxx :"+tableWith.width*VWRouteSummaryInfoTableData.info_columns[k].m_width);
			TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWRouteSummaryInfoTableData.info_columns[k].m_width),renderer,editor);
			VWClient.printToConsole("column....."+column);
			VWClient.printToConsole("column width....."+column.getWidth());
			addColumn(column);
			textRenderer = null;
		}
		setBackground(java.awt.Color.white);
		ttlColumns = getColumnModel().getColumnCount();
		VWClient.printToConsole("ttlColumns....."+ttlColumns);
		indexNameColumn = ttlColumns-1;
		VWClient.printToConsole("indexNameColumn 1....."+indexNameColumn);
		int tableWidth = parent.scRouteInfoPanel.getWidth();
		//getColumnModel().getColumn((indexNameColumn)).setPreferredWidth((int)(tableWidth*0.65f));
		int indexValueColumn = ttlColumns-2;
		VWClient.printToConsole("indexNameColumn 2....."+indexValueColumn);
		//getColumnModel().getColumn((indexValueColumn)).setPreferredWidth((int)(tableWidth*0.35f));
		
		setRowHeight(20);
		/*getColumnModel().getColumn(m_data.COL_INDEXNAME).setPreferredWidth(90);
		getColumnModel().getColumn(m_data.COL_INDEXVALUE).setPreferredWidth(350);*/
		
		getColumnModel().getColumn(m_data.COL_INDEXNAME).setCellRenderer(new SummaryInfoMultiLineCellRenderer());
		getColumnModel().getColumn(m_data.COL_INDEXVALUE).setCellRenderer(new SummaryInfoMultiLineCellRenderer());
		
		JTableHeader header = getTableHeader();
		header.setBorder(new EmptyBorder(1, 3, 1, 3));
        header.setUpdateTableInRealTime(false);
        //header.addMouseListener(m_data.new ColumnListener(this));
        //header.setReorderingAllowed(true);
		
		setAutoResizeMode(JTable.AUTO_RESIZE_OFF);      
	}
	//	--------------------------------------------------------------------------
	public void clearData() {
		m_data.clear();
		m_data.fireTableDataChanged();
	}
	//	--------------------------------------------------------------------------
	public void setLastProcessedRowSelected() {
		int rowToSelect = -1;
		if(m_data.m_vector!=null && m_data.m_vector.size()>0){
			for(int count=(m_data.m_vector.size()-1);count>=0;count--){
				try{
					RouteSummaryInfoRowData row = (RouteSummaryInfoRowData)m_data.m_vector.elementAt(count);
					if(!row.m_IndexName.toString().trim().equalsIgnoreCase("") && !row.m_IndexName.toString().trim().equalsIgnoreCase("-")){									
						rowToSelect = count;
						break;
					}
				}catch(Exception e){
					//System.out.println("EXCEPTION :"+e.toString());
				}
			}		
		}
		if(rowToSelect >=0)
			setRowSelectionInterval(rowToSelect, rowToSelect);
	}
	//  --------------------------------------------------------------------------
	public void addData(List list) {
		m_data.setData(list);
		m_data.fireTableDataChanged();
	}
	public String[][] getData() {
		return m_data.getData();
	}
}

//--------------------------------------------------------------------------
class RouteSummaryInfoColumnData {
	public String  m_title;
	float m_width;
	int m_alignment;
	
	public RouteSummaryInfoColumnData(String title, float width, int alignment) {
		m_title = title;
		m_width = width;
		m_alignment = alignment;
	}
}
//--------------------------------------------------------------------------
class VWRouteSummaryInfoTableData extends AbstractTableModel {
	public static final RouteSummaryInfoColumnData info_columns[] = {
		new RouteSummaryInfoColumnData(VWCConstants.RouteInfoColNames[0],0.3F, JLabel.LEFT),
		new RouteSummaryInfoColumnData(VWCConstants.RouteInfoColNames[1],0.7F,JLabel.LEFT),
	};
	public static final int COL_INDEXNAME = 0;
	public static final int COL_INDEXVALUE = 1;
	
	protected VWRouteSummaryInfoTable m_parent;
	protected Vector m_vector;
	
	public VWRouteSummaryInfoTableData(VWRouteSummaryInfoTable parent) {
		m_parent = parent;
		m_vector = new Vector();
	}
//	--------------------------------------------------------------------------
	
	
	public void setData(List data) {
		m_vector.removeAllElements();
		if (data==null) return;
		int count =data.size();
		for(int i=0;i<count;i++)
			m_vector.addElement(new RouteSummaryInfoRowData((String)data.get(i)));
	}
//	--------------------------------------------------------------------------
	public void setData(String[][] data) {
		m_vector.removeAllElements();
		int count =data.length;
		for(int i=0;i<count;i++) {
			RouteSummaryInfoRowData row =new RouteSummaryInfoRowData(data[i]);
			if(i==2)
				m_vector.addElement(new Boolean(true));
			else
				m_vector.addElement(row);
		}
	}
	//--------------------------------------------------------------------------
	public String[][] getData() {
		int count=getRowCount();
		String[][] data=new String[count][VWCConstants.RouteInfoColNames.length];
		for(int i=0;i<count;i++) {
			RouteSummaryInfoRowData row=(RouteSummaryInfoRowData)m_vector.elementAt(i);
			//data[i][0]=row.m_SerailNumber;
			data[i][0]=row.m_IndexName;
			data[i][1]=row.m_IndexValue;
			
		}
		return data;
	}
//	--------------------------------------------------------------------------
	public String[] getRowData(int rowNum) {
		String[] RouteSummaryInfoRowData=new String[VWCConstants.RouteInfoColNames.length];
		RouteSummaryInfoRowData row=(RouteSummaryInfoRowData)m_vector.elementAt(rowNum);
	//	RouteSummaryRowData[0]=row.m_SerailNumber;
		RouteSummaryInfoRowData[0]=row.m_IndexName;
		RouteSummaryInfoRowData[1]=row.m_IndexValue;
		
		return RouteSummaryInfoRowData;
	}
	
	public RouteSummaryInfoRowData getRowData0(int rowNum) {
		RouteSummaryInfoRowData row=(RouteSummaryInfoRowData)m_vector.elementAt(rowNum);
		return row;
	}
	
//	--------------------------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size();
	}
//	--------------------------------------------------------------------------
	public int getColumnCount() {
		return info_columns.length;
	}
//	--------------------------------------------------------------------------
	public String getColumnName(int column) {
		String str = info_columns[column].m_title;
        return str;
	}
//	--------------------------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		return true;
	}
//	--------------------------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";
		RouteSummaryInfoRowData row = (RouteSummaryInfoRowData)m_vector.elementAt(nRow);		
		switch (nCol) {
			case COL_INDEXNAME:	 return (row.m_IndexName.trim().equals("-"))?"":row.m_IndexName;
			case COL_INDEXVALUE:	 return (row.m_IndexValue.trim().equals("-"))?"":row.m_IndexValue;
		}
		return "";
	}
//	--------------------------------------------------------------------------
	public void setValueAt(Object value, int nRow, int nCol) {
		if (nRow < 0 || nRow >= getRowCount())
			return;
		RouteSummaryInfoRowData row = (RouteSummaryInfoRowData)m_vector.elementAt(nRow);
		String svalue = value.toString().trim().equals("-")?" ":value.toString();
		
		switch (nCol) {
			case COL_INDEXNAME:
				row.m_IndexName = svalue;
				break;
			case COL_INDEXVALUE:
				row.m_IndexValue = svalue;
				break;
		}
	}
//	--------------------------------------------------------------------------
	public void clear() {
		m_vector.removeAllElements();
	}
//	--------------------------------------------------------------------------
	public void insert(RouteSummaryRowData AdvanceSearchRowData) {
		m_vector.addElement(AdvanceSearchRowData);
	}
//	--------------------------------------------------------------------------
	public boolean remove(int row){
		if (row < 0 || row >= m_vector.size())
			return false;
		m_vector.remove(row);
		return true;
	}
//	--------------------------------------------------------------------------
}
	class RouteSummaryInfoRowData {
		public String 	m_IndexName;
		public String   m_IndexValue;
		
		
		public RouteSummaryInfoRowData() {
			m_IndexName="";
			m_IndexValue="";
		}
//		--------------------------------------------------------------------------------
		public RouteSummaryInfoRowData(String indexName, String indexValue) {
			m_IndexName = indexName;
			m_IndexValue = indexValue;
		}
//		--------------------------------------------------------------------------------
		public RouteSummaryInfoRowData(String str) {
			StringTokenizer tokens=new StringTokenizer(str,Util.SepChar,false);		
			try {	
				m_IndexName = tokens.nextToken();	
				m_IndexValue = tokens.nextToken();

			}catch(Exception ex){
			}
		}
//		--------------------------------------------------------------------------------
		public RouteSummaryInfoRowData(String[] data) {
			int i=0;
			m_IndexName = data[i++];
			m_IndexValue = data[i++];
		}
	}
	
	class SummaryInfoMultiLineCellRenderer extends JTextArea implements TableCellRenderer {
		 
	    private List<List<Integer>> rowColHeight = new ArrayList<>();
	 
	    public SummaryInfoMultiLineCellRenderer() {
	        setLineWrap(true);
	        setWrapStyleWord(true);
	        setOpaque(true);      
	    }
	 
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	    	if (column == 0)
	    		VWRouteSummaryInfoTable.maxRowHeight = 20;
	    	int rowHeight = VWRouteSummaryInfoTable.maxRowHeight;
	        if (isSelected) {
	            setForeground(table.getSelectionForeground());
	            setBackground(table.getSelectionBackground());
	        } else {
	        	if(row%2==0) {
	                setBackground(new Color(15658734));
	        	} else {	        		
	        		setForeground(table.getForeground());
	        		setBackground(table.getBackground());
	        	}
	        }
	        setFont(table.getFont());
	 
	        if (hasFocus) {
	            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
	            if (table.isCellEditable(row, column)) {
	                setForeground(UIManager.getColor("Table.focusCellForeground"));
	                setBackground(UIManager.getColor("Table.focusCellBackground"));
	            }
	        } else {
	            setBorder(new EmptyBorder(1, 3, 1, 3));
	        	table.setShowHorizontalLines(false);
	            table.setShowVerticalLines(false);
	        }
	 
	        if (value != null) {
	            setText(value.toString());
	             
	        } else {
	            setText("");
	        }
	        try {
		        FontMetrics fm = getFontMetrics(this.getFont());
		        int fontHeight = fm.getHeight() + table.getRowMargin();
		        int textLength = fm.stringWidth(getText());  // length in pixels
		        int colWidth = table.getColumnModel().getColumn(column).getWidth();
		        int lines = textLength / colWidth +1; // +1, because we need at least 1 row.
		        int height = fontHeight * lines;
		        if (column == 0 && height <= 20) {
		        	height = 20; // This is for by default all the rows height will be 15
		        }
		        if (height > rowHeight) {
		        	//VWClient.printToConsole("Wrapped value  XXX:"+value.toString());
		        	//VWClient.printToConsole("Height :"+height);
		            table.setRowHeight(row, height);
		            rowHeight = height;
		        } else if (column == 0) {
		        	table.setRowHeight(row, height);
		        }
		        VWRouteSummaryInfoTable.maxRowHeight = rowHeight;
		    } catch (Exception e) {
	        	VWClient.printToConsole("Exception in getTableCellRendererComponent ......."+e.getMessage());
	        }
	 
	        return this;
	    }
	 
	    private void adjustRowHeight(JTable table, int row, int column) {
	    	try{
	    		int cWidth = table.getTableHeader().getColumnModel().getColumn(column).getWidth();
	    		//VWClient.printToConsole("cWidth :::"+cWidth);
	    		setSize(new Dimension(cWidth, 1000));
	    		int prefH = getPreferredSize().height;
	    		while (rowColHeight.size() <= row) {
	    			rowColHeight.add(new ArrayList<Integer>(column));
	    		}
	    		List<Integer> colHeights = rowColHeight.get(row);
	    		while (colHeights.size() <= column) {
	    			colHeights.add(0);
	    		}
	    		colHeights.set(column, prefH);
	    		int maxH = prefH;
	    		for (Integer colHeight : colHeights) {
	    			if (colHeight > maxH) {
	    				maxH = colHeight;
	    			}
	    		}
	    		if (table.getRowHeight(row) != maxH) {
	    			table.setRowHeight(row, maxH);
	    		}
	    	
	    }catch(Exception e){
	    	VWClient.printToConsole("Exception in adjust row height :::"+e.getMessage());
	    }
	    }
}
