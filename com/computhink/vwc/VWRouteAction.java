package com.computhink.vwc;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import com.computhink.common.Constants;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.Signature;
import com.computhink.common.Util;
import com.computhink.common.login.VWLoginScreen;
import com.computhink.common.util.JTextFieldLimit;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.doctype.VWUtil.VWComboBox;
import com.computhink.vwc.image.Images;
import com.computhink.vws.server.Client;


public class VWRouteAction implements Runnable, VWCConstants, Constants
{
	public String[] docIds=null;
	public static boolean isWebClient = false;
	private static ResourceManager connectorManager=ResourceManager.getDefaultManager();
	public String errorMessage = null;
	
     public VWRouteAction()
     {     
        new Images();
        initComponents();
        dlgMain.setVisible(true);
        keyBuffer.start();
     }
     
     public VWRouteAction(VWClient vwc, int sessionId, int docId, RouteMasterInfo routeMasterInfo, int action, 
    		 String documentName, int clientType)
     {
		 /**CV2019 merges from SIDBI line - Extra parameter has added***/
    	 this(vwc, sessionId, docId, routeMasterInfo, action, documentName, clientType, null, 0);
     }
     public VWRouteAction(VWClient vwc, int sessionId, int docId, RouteMasterInfo routeMasterInfo, int action, 
    		 String documentName, int clientType, Signature sign)
     {
    	 this(vwc, sessionId, docId, routeMasterInfo, action, documentName, clientType, null, 0);
     }
	/**CV2019 merges from SIDBI line - Extra parameter has added***/
     public VWRouteAction(VWClient vwc, int sessionId, int docId, RouteMasterInfo routeMasterInfo, int action, 
    		 String documentName, int clientType, Signature sign, int assignSecurity)
     {
    	 VWClient.printToConsole("VWRouteAction 2");
    	 this.vwc = vwc; 
    	 this.sessionId = sessionId;
    	 this.action = action;
    	 this.documentName = documentName;
    	 this.routeMasterInfo = routeMasterInfo;
    	 this.routeMasterInfo.setDocName(documentName);
    	 this.clientType = clientType;
    	 this.docId = docId;
    	 this.sign = sign;
    	 this.assignSec = assignSecurity;
    	 if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
 				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
 			isWebClient = true;
         
    	 if (active)
    	 {
    		 if(dlgMain != null && dlgMain.isVisible() && sessionId == prevSID && docId == prevDocId){
    			 dlgMain.requestFocus();
    			 dlgMain.toFront();
    			 return;
    		 }else{
    			 closeDialog();
    		 }    			 
    	 }
    	 prevSID = sessionId;
    	 prevDocId = docId;
    	 initComponents();
    	 active = true;        
    	 keyBuffer.start();    	 
     }
     
     /***
      * CV10.2 - Document multiselection enhancement
      * @param vwc
      * @param sessionId
      * @param docId
      * @param routeMasterInfo
      * @param action
      * @param documentName
      */
     public VWRouteAction(VWClient vwc, int sessionId, String docId, Vector<RouteMasterInfo> routeMasterInfoList, int action, 
    		 String documentName)
     {
    	 VWClient.printToConsole("sid inside setRouteStatusFotTodoMultiSelect::::"+sessionId);
    	 
    	 this.vwc = vwc; 
    	 this.sessionId = sessionId;
    	 this.action = action;
    	 VWClient.printToConsole("documentName :"+documentName);
    	 VWClient.printToConsole("docId setRouteStatusFotTodoMultiSelect::::"+docId);
    	 if (docId != null)
			this.docIds = docId.split("\t");
    	 this.routeMasterInfoList = new Vector<RouteMasterInfo>(routeMasterInfoList);
    	 
    	 if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
 				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
 			isWebClient = true;
         
    	 if (active)
    	 {
    		 VWClient.printToConsole("docId inside Active setRouteStatusFotTodoMultiSelect::::"+docId);
    		 if(dlgMain != null && dlgMain.isVisible() && sessionId == prevSID){
    			 dlgMain.requestFocus();
    			 dlgMain.toFront();
    			 return;
    		 }else{
    			 closeDialog();
    		 }    			 
    	 }
    	 prevSID = sessionId;    	 
    	 VWClient.printToConsole("before initComponents call setRouteStatusFotTodoMultiSelect::::"+sessionId);
    	 initComponents();
    	 active = true;        
    	 keyBuffer.start();    	 
     }
     
     
     //In routing for end route action, multiple document can be selected in adminWise. To handle that, this constructor added.
	public VWRouteAction(VWClient vwc, int sessionId, Vector selectedDocs, int clientType)
     {
		VWClient.printToConsole("VWRouteAction 3");
        if (active)
        {
        	if(dlgMain != null && dlgMain.isVisible() && sessionId == prevSID && docId == prevDocId){
        		dlgMain.requestFocus();
        		dlgMain.toFront();
        		return;
        	}else{
        		closeDialog();
        	}
        }    	 
    	this.vwc = vwc; 
		this.sessionId = sessionId;
    	this.selectedDocs = selectedDocs;
    	this.action = 4; // end route
		this.clientType = clientType;
		prevSID = sessionId;
		prevDocId = docId;
        initComponents();
        //dlgMain.setVisible(true);
        active = true;        
        keyBuffer.start();               
     }
     
	void checkSignature(){
		sign = new Signature();
		sign.setDocId(docId);
		if (action == 1)   		 
		{
			if (routeMasterInfo.getRouteUserSignType() == 2){
				int signedType = vwc.isUserHasSign(sessionId, routeMasterInfo.getRouteUserSignType());
				if (signedType == 0) {
					JOptionPane.showMessageDialog(dlgMain,connectorManager.getString("routeaction.jmsg1")+" " + PRODUCT_NAME + " "+connectorManager.getString("routeaction.jmsg11"));
					isSigned = false;
				}else{
					vwc.printToConsole("checkSignature ");
					checkCertifySign();
					show();
					System.out.println("Login completed");
				}    		 
			}else if (routeMasterInfo.getRouteUserSignType() == 1){
				isSigned = true;
			}
		}    	 
	}
    
    public void AfterLogin(int output){
		 if (output == 1 && routeMasterInfo.getRouteUserSignType() == 2){
			 signResult = checkCertifySign();
			 if (signResult == 1){
				 isSigned = true;
			 }else 
				 isSigned = false;
		 }else if(output == 1 && routeMasterInfo.getRouteUserSignType() == 1){
			 isSigned = true;
		 }else{
			 JOptionPane.showMessageDialog(dlgMain, connectorManager.getString("routeaction.jmsg2"));
			 isSigned = false;
		 }
    }
    
	private int checkCertifySign(){

		sign.setUserName(vwc.getSession(sessionId).user);
		sign.setType(Signature.Template_Type);
		vwc.getUserSign(sessionId, sign);
		//System.out.println("sign.getSignFilePath() " + sign.getSignFilePath());
		String templateStr=VWCUtil.getStrFromFile(new File(sign.getSignFilePath()));
		//System.out.println("templateStr " + templateStr);
		int result=VWCUtil.validateSign(templateStr,0);
		//System.out.println("validateSign " + result);
		if(result==0)   {
			isSigned = false;
			System.out.println("result " + result);;
			return 0;
		}else{
			sign.setType(Signature.Certified_Type);
			isSigned = true;
			vwc.printToConsole("certify sign is done");			
		}
		//if(checkOnly==1)  System.out.println("checkonly == 1" + result);
		//if(VWCUtil.signatureConfirm()==0) System.out.println("confirmed " + result);;
		return 1;
	}
     
     private void initComponents() 
     {
    	 VWClient.printToConsole("docIds in initComponents::"+docIds);
    	 VWClient.printToConsole("routeMasterInfoList in initComponents::"+routeMasterInfoList);
    	routeMain = new JPanel();
    	dlgMain = new JDialog();
    	dlgMain.getContentPane().setLayout(null);    	
    	dlgMain.getContentPane().add(routeMain);
    	dlgMain.setSize(415, 325);
    	dlgMain.setModal(true);
    	routeMain.setBounds(0, 0, 415, 290);
    	//routeMain.setBorder(BorderFactory.createLineBorder(Color.red));
    	routeMain.setLayout(null);
    	btnOk = new JButton(connectorManager.getString("general.btnok"));
        btnCancel = new JButton(connectorManager.getString("general.btncancel"));
        
        lblRouteNameLabel = new JLabel(connectorManager.getString("genaral.workflow") +" "+connectorManager.getString("general.Name"));
        if(isWebClient)
        	lblRouteNameLabel.setBounds(10, 10, 100, 25);
        else
        	lblRouteNameLabel.setBounds(10, 10, 90, 25);
        //lblRouteNameLabel.setBorder(BorderFactory.createLineBorder(Color.red));
        routeMain.add(lblRouteNameLabel);
        
        lblRouteName = new JLabel();
        if(isWebClient)
        	lblRouteName.setBounds(110, 10, 125, 25);
        else
        	lblRouteName.setBounds(100, 10, 125, 25);
        //lblRouteName.setBorder(BorderFactory.createLineBorder(Color.red));
        routeMain.add(lblRouteName);
        
        lblTaskNameLabel = new JLabel(connectorManager.getString("routeaction.taskname"));
        if(isWebClient)
        	lblTaskNameLabel.setBounds(210, 10, 70, 25);
        else
        	lblTaskNameLabel.setBounds(210, 10, 60, 25);
        //lblTaskNameLabel.setBorder(BorderFactory.createLineBorder(Color.red));
        routeMain.add(lblTaskNameLabel);
        
        lblTaskName = new JLabel();
        if(isWebClient)
        	lblTaskName.setBounds(283, 10, 125, 25);
        else
        	lblTaskName.setBounds(273, 10, 125, 25);
        //lblTaskName.setBorder(BorderFactory.createLineBorder(Color.red));
        routeMain.add(lblTaskName);
        
        lblTaskDescLabel = new JLabel(connectorManager.getString("routeaction.taskdesc"));
        lblTaskDescLabel.setBounds(10, 35, 110, 25);
        //lblTaskDescLabel.setBorder(BorderFactory.createLineBorder(Color.red));
        routeMain.add(lblTaskDescLabel);
        
        txtareaTaskDesc = new JTextArea();
        txtareaTaskDesc.setBounds(10, 60, 386, 60);
        txtareaTaskDesc.setLineWrap(true);
        txtareaTaskDesc.setEditable(false);
        
        if(!isWebClient)
        	txtareaTaskDesc.setBackground(Color.white);
        txtareaTaskDesc.setFont(new Font("Arial", Font.PLAIN, 12));
        txtareaTaskDesc.setBorder(BorderFactory.createLineBorder(Color.gray));
        routeMain.add(txtareaTaskDesc);
        
        if(routeMasterInfo != null){
        	lblRouteName.setText(routeMasterInfo.getRouteName());
        	lblTaskName.setText(routeMasterInfo.getTaskName());
        	VWClient.printToConsole("routeMasterInfo.getTaskDescription() ::: "+routeMasterInfo.getTaskDescription());
        	txtareaTaskDesc.setText(routeMasterInfo.getTaskDescription().equalsIgnoreCase("-") ? "" : routeMasterInfo.getTaskDescription());        	
        } else {
        	/***CV10.2 - Added for Document multiselection enhancement****/
        	lblRouteName.setText("-");
        	lblTaskName.setText("-");
        }
        
        scRoutePane = new JScrollPane();
        commentsTextArea = new JTextArea();
		
		/**CV2019 merges from SIDBI line - WordWrap has added for comments***/
		/**Action 5,6,7 and 8 added for SIDBI**/
        commentsTextArea.setFont(new Font("Arial Unicode MS Regular", Font.PLAIN, 12));
        /*if(this.clientType == Client.Adm_Client_Type){
        	commentsTextArea.setDocument(new JTextFieldLimit(460));
        	commentsTextArea.setToolTipText("Max Char 460.");
        }else{*/
        	commentsTextArea.setDocument(new JTextFieldLimit(500));
        	commentsTextArea.setToolTipText("Max Char 500.");
        	try {
        		commentsTextArea.setLineWrap(true);
        		commentsTextArea.setWrapStyleWord(true);
        	//JScrollPane scrollPane = new JScrollPane(commentsTextArea);
        	} catch (Exception wrap) {
        		VWClient.printToConsole("Exception in comment wordwrap : "+wrap.getMessage());
        	}
        //}
        dlgMain.getContentPane().setLayout(null);
        String title = "";
        VWClient.printToConsole("action -------->"+action);
		/**CV2019 merges from SIDBI line & CV10.2 - document multiple selection***/
        if(action == 1) {
        	boolean sidbiCustomizationFlg = vwc.getSidbiCustomizationFlag(sessionId);
        	if (sidbiCustomizationFlg) {
        		title = " " + "Accept and Forward"; //" Accept ";
        	} else {
	        	if (routeMasterInfo != null)
	        		title = " " + routeMasterInfo.getAcceptLabel(); //" Accept ";
	        	else
	        		title = "  Accept ";
        	}
        } else if(action == 2) {	
        	if (routeMasterInfo != null)
        		title = " " + routeMasterInfo.getRejectLabel(); //" Accept ";
        	else
        		title = "  Reject ";
        } else if(action == 3)
        	title =connectorManager.getString("routeaction.review") ;
        else if(action == 4)
        	title = " "+connectorManager.getString("routeaction.end")+" "+connectorManager.getString("genaral.workflow")+" "; 
        else if(action == 5)
        	title = " "+connectorManager.getString("routeaction.approve")+" and "+connectorManager.getString("routeaction.end")+" "+connectorManager.getString("genaral.workflow")+" "; 
        else if(action ==6)
        	title=" "+"Approve and PSR";
        else if(action ==7)
        	title=" "+"PSR and EndworkFlow";
        else if(action ==8)
        	title=" "+"Intimate";
        
        	

        if(this.documentName!=null && !this.documentName.equals(""))
        	dlgMain.setTitle(connectorManager.getString("WORKFLOW_MODULE_NAME") +" "+connectorManager.getString("routeaction.document")+" '"+this.documentName+"' - " + title);
        else {
        	/***if else condition has added for document multiple selection*****/
        	if (docIds==null) { 
        		dlgMain.setTitle(connectorManager.getString("WORKFLOW_MODULE_NAME") +" "+connectorManager.getString("routeaction.document-")+" "+ title);
        	} else {
        		dlgMain.setTitle(title);
        	}
        }
        lblActionStatus =  new JLabel();
        lblActionStatus.setText(connectorManager.getString("routeaction.action"));
        lblComments =  new JLabel();
        lblComments.setText(connectorManager.getString("routeaction.comment"));
        routeMain.add(lblComments);
        lblComments.setBounds(10, 125, 70, 25);
        scRoutePane.setViewportView(commentsTextArea);
        scRoutePane.setBounds(10, 150, 386, 90);
        routeMain.add(scRoutePane);
       
		//Display all previous Task List and immediate next task, if Reject menu option is selected
		Vector routeTaskDetails = new Vector();
		/***CV10.2 - Added for Document multiselection enhancement - routeMasterInfo object will be null for multiple document selection****/
		if(action == 2 && routeMasterInfo != null && routeMasterInfo.getOnRejectAction() == -1){
			vwc.getTaskInfo(this.sessionId, routeMasterInfo.getRouteId(), 0, routeTaskDetails);
        	Vector data = new Vector();
        	boolean isFinalApprover = false;
			if(routeTaskDetails!=null && routeTaskDetails.size()!=1){
				for (int i = 0; i< routeTaskDetails.size() ; i++)
		        {
					RouteTaskInfo routeTaskInfo = (RouteTaskInfo)routeTaskDetails.get(i);
					if(routeTaskInfo.getTaskSequence()<routeMasterInfo.getLevelSeq()) 
					{
						data.add("("+routeTaskInfo.getTaskSequence()+")"+"   "+routeTaskInfo.getTaskName());
						vecPreviousApproversLevelSeq.add(routeTaskInfo.getTaskSequence());
						treesetPreviousApproversLevelSeq.add(routeTaskInfo.getTaskSequence());
							}
					if(routeTaskInfo.getTaskSequence() == routeMasterInfo.getLevelSeq() && i == routeTaskDetails.size()-1){
						isFinalApprover = true; 
					}
		        }
				for (int i = 0; i< routeTaskDetails.size() ; i++)
		        {
					RouteTaskInfo routeTaskInfo = (RouteTaskInfo)routeTaskDetails.get(i);
					if(routeTaskInfo.getTaskSequence() > routeMasterInfo.getLevelSeq()){
						data.add("("+routeTaskInfo.getTaskSequence()+")"+"   "+routeTaskInfo.getTaskName());
						vecPreviousApproversLevelSeq.add(routeTaskInfo.getTaskSequence());
						treesetPreviousApproversLevelSeq.add(routeTaskInfo.getTaskSequence());
							break;
						}
					}
		        }
			if(isFinalApprover || (routeTaskDetails!=null && routeTaskDetails.size() == 1) ){
	        	data.add(endRoute);
	        	vecPreviousApproversLevelSeq.add(-1);
				treesetPreviousApproversLevelSeq.add(-1);
	        }
	        if(data!=null && data.size()>0)
	        {
		        lblPreviousApprovers =  new JLabel();
		        lblPreviousApprovers.setBounds(10, 255, 110, 25);
		        lblPreviousApprovers.setBackground(Color.red);
		        lblPreviousApprovers.setText(connectorManager.getString("routeaction.movedoc"));
		        routeMain.add(lblPreviousApprovers);
		        
		        cmbPreviousApprovers = new VWComboBox(data);
		        cmbPreviousApprovers.setBounds(130, 255, 100, 22);
		        routeMain.add(cmbPreviousApprovers);
		        if(data.size()!=1){
		        	int lastLevelSeq = Integer.parseInt(String.valueOf(treesetPreviousApproversLevelSeq.last()));
			        int count = 0; 
					for(count=0;count<vecPreviousApproversLevelSeq.size();count++){
						if( Integer.parseInt(String.valueOf(vecPreviousApproversLevelSeq.get(count))) == lastLevelSeq) {
							break;
						}
					}
					cmbPreviousApprovers.setSelectedIndex(count);	
		        }
	        }	
        }//end of if
		/***CV10.2 - Added for Document multiselection enhancement - routeMasterInfo object will be null for multiple document selection****/
		if(action == 2 && routeMasterInfo != null) {
			int selectedTaskSeq = routeMasterInfo.getOnRejectAction();
			lblPreviousApprovers =  new JLabel();
			lblPreviousApprovers.setBounds(10, 255, 220, 25);
			lblPreviousApprovers.setBackground(Color.red);
			lblPreviousApprovers.setText("");
			if(action == 2 && routeMasterInfo.getOnRejectAction()>0){
				int routeId = routeMasterInfo.getRouteId();
				lblPreviousApprovers.setText(connectorManager.getString("routeaction.movedoc"));
				routeMain.add(lblPreviousApprovers);
				Vector vTaskInfo = new Vector();
				int ret = vwc.getTaskInfo(this.sessionId, routeId, selectedTaskSeq, vTaskInfo);
				if(ret == 0  && vTaskInfo!=null && vTaskInfo.size()>0){
						RouteTaskInfo routeTaskInfo  = (RouteTaskInfo)vTaskInfo.get(0);
						String taskName = routeTaskInfo.getTaskName();
						lblPreviousApprovers.setText(connectorManager.getString("routeaction.docreject")+taskName+"'.");
						previousApproversLevelSeq = routeMasterInfo.getOnRejectAction();
				}
			}else if(action == 2 && routeMasterInfo.getOnRejectAction()== 0){
				routeMain.add(lblPreviousApprovers);
				lblPreviousApprovers.setText(connectorManager.getString("routeaction.rejectnotify"));
				previousApproversLevelSeq = 0;
			}else if(action == 2 && routeMasterInfo.getOnRejectAction() == -2){
				lblPreviousApprovers.setText(connectorManager.getString("WORKFLOW_NAME") +" "+connectorManager.getString("routeaction.endfordoc"));
				previousApproversLevelSeq = selectedTaskSeq;
			}
			routeMain.add(lblPreviousApprovers);
		}
        routeMain.add(btnOk);
        btnOk.setBounds(245, 255, 70, 22);
        
        routeMain.add(btnCancel);
        btnCancel.setBounds(325, 255, 70, 22);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();        
        dlgMain.setLocation((screenSize.width-440)/2,(screenSize.height-290)/2);
        dlgMain.setResizable(false);
		dlgMain.addWindowListener(symWindow);
		VWClient.printToConsole("routeMasterInfoList before clicking ok button :"+routeMasterInfoList);
        btnCancel.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent evt) {
               closeDialog();
            }
        });
        btnOk.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt) {
        		//Desc   :Display Previous Approvers List only if Reject menu option is selected. And next approver also added
        		//Author :Nishad Nambiar
        		//Date   :17-May-2007, 19 May 2007
        		int signId = 0;
        		VWClient.printToConsole("routeMasterInfo onclick of ok:"+routeMasterInfo);
        		VWClient.printToConsole("routeMasterInfoList onclick of ok :"+routeMasterInfoList);
        		/***CV10.2 - Added for Document multiselection enhancement - routeMasterInfo object will be null for multiple document selection****/
        		if (routeMasterInfo != null) {
	        		vwc.userSelectedValue =0;
	        		if(action == 2&& routeMasterInfo.getOnRejectAction() == -1){
	        			try{
	        				previousApproversLevelSeq = Integer.parseInt(String.valueOf(vecPreviousApproversLevelSeq.get(cmbPreviousApprovers.getSelectedIndex())));
	        				vwc.userSelectedValue=previousApproversLevelSeq;
	        			}catch(Exception e){
	        			}
	        			if(cmbPreviousApprovers.getSelectedItem().toString().equalsIgnoreCase(endRoute)){
	        				previousApproversLevelSeq = -2;
	        			}
	        		}	        		
	        		if (sign != null) {
	        			vwc.printToConsole("sign object is " + sign);
	        			if (routeMasterInfo.getRouteUserSignType() >= 1 && sign != null){
	        				vwc.printToConsole("sign type is " + routeMasterInfo.getRouteUserSignType());
	        				sign.setType(routeMasterInfo.getRouteUserSignType());
	        				sign.setDocId(docId);
	        				signId = vwc.updateDocumentSign(sessionId, sign);
	        			}
	        		}
	        	}
        		
        		/*if(addSelected(signId)
        				closeDialog();*/
        		VWClient.printToConsole("routeMasterInfoList XXXXX :"+routeMasterInfoList);
        		doWorkFlowAction(signId);        		
        	}
        });
        
		commentsTextArea.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
			}
		});
		commentsTextArea.addKeyListener(new KeyAdapter(){
			public void keyPressed(KeyEvent e){
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) closeDialog();
				keyBuffer.appendKey(e.getKeyChar());
			}
			public void keyReleased(KeyEvent e){
			}
			public void keyTyped(KeyEvent e){
			}
		});
		
	}
	
     public void doWorkFlowAction(int signId) {
    	 try{
    		 VWClient.printToConsole("routeMasterInfoList in doWorkFlowAction :"+routeMasterInfoList);
    		 String comments = "";
    		 comments = commentsTextArea.getText();
    		 if(this.clientType == Client.Adm_Client_Type && comments.trim().equals("")){
    			 dlgMain.removeWindowListener(symWindow);
    			 JOptionPane.showMessageDialog(null, connectorManager.getString("routeaction.jmsg3"),connectorManager.getString("routeaction.jtitle"), JOptionPane.OK_OPTION);
    			 dlgMain.addWindowListener(symWindow);        			
    		 } else if(this.clientType == Client.Fat_Client_Type && comments.trim().equals("")&& action == 2){
    			 dlgMain.removeWindowListener(symWindow);
    			 JOptionPane.showMessageDialog(null, connectorManager.getString("routeaction.jmsg3"),connectorManager.getString("routeaction.jtitle"), JOptionPane.OK_OPTION);
    			 dlgMain.addWindowListener(symWindow);    			 
    		 } else if(this.clientType == Client.All_Client_Type && comments.trim().equals("")){
    			 dlgMain.removeWindowListener(symWindow);
    			 JOptionPane.showMessageDialog(null, connectorManager.getString("routeaction.jmsg3"),connectorManager.getString("routeaction.jtitle"), JOptionPane.OK_OPTION);
    			 dlgMain.addWindowListener(symWindow);    			 
    		 } else{
    			 /***CV10.2 - Added for Document multiselection enhancement****/
    			 btnOk.setEnabled(false);
    			 dlgMain.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    			 addSelected(signId, comments);
    		 }
    	 }catch (Exception e) {
    		 //vwc.printToConsole("Exception in doAction : "+e.getMessage());			
    	 }
     }

	class SymWindow extends WindowAdapter{
            public void windowLostFocus(WindowEvent e){
            	if(active){
	            	dlgMain.requestFocus();
	                dlgMain.toFront();
            	}
            }
            public void windowDeactivated(WindowEvent e) {
            	if(active){
            		dlgMain.requestFocus();
            		dlgMain.toFront();
            	}
            }
            public void windowStateChanged(WindowEvent e) {
            	if(active){	
            		dlgMain.toFront();
            	}
            }
             public void windowDeiconified(WindowEvent e) {
            	 if(active){
            		 dlgMain.toFront();
            	 }
            }
             public void windowClosing(WindowEvent e) {         
            	 closeDialog();
             }
             
     }
     public void run()
     {
    	 System.out.println("run method show " + isSigned);
         show();
     }
     public void show()
     {
         if (vwc == null) return;
         dlgMain.setVisible(true);
     }
     
     private void closeDialog() 
     {
         active = false;
         keyBuffer.kill();
         dlgMain.setVisible(false);
         dlgMain.dispose();
         
     }
	/**CV2019 merges from SIDBI line***/
	/**Action 5, 6, 7 and 8 has added**/
	private boolean addSelected(int signId, String comments)
     {                  
		int processCount = 0;
		VWClient.printToConsole("routeMasterInfoList YYYYYY :"+routeMasterInfoList);
		VWClient.printToConsole("action : "+action);
		if(this.clientType == Client.Adm_Client_Type)
				comments = comments+connectorManager.getString("routeaction.comment1")+" "+ PRODUCT_NAME+" "+connectorManager.getString("routeaction.comment2");
         String actionResult = "";
			int selectedTaskSeq = previousApproversLevelSeq;
         if(action == 1)
        	 actionResult = Constants.Route_Status_Approve;
         else if(action == 2)
        	 actionResult = Constants.Route_Status_Reject;
         else if(action == 3) 
        	 actionResult = Constants.Route_Status_Review;
         else if(action == 4) 
        	 actionResult = Constants.Route_Status_End;
         else if(action == 5) 
        	 actionResult = Constants.Route_Status_Approve_End;
         else if(action == 6)
        	 actionResult=Constants.Route_Status_Approve_PSR;
         else if(action==7)
        	 actionResult= Constants.Route_Status_PSR_End;
        	
         else if(action==8)
        	 actionResult=Constants.Route_Status_Approve_Intimation;

         if(this.selectedDocs!=null && selectedDocs.size()>0){
        	 for(int i = 0; i<selectedDocs.size(); i++){
        		 RouteMasterInfo master = (RouteMasterInfo) selectedDocs.get(i);
					master.setComments(comments);
					//session, docId, routeMasterInfo.getRouteId(), 
					//routeMasterInfo.getRouteUserId(), routeMasterInfo.getLevelSeq(), actionResult, comments, documentName, 
					//routeMasterInfo.getStatus(), routeMasterInfo.getRouteMasterId(), selectedTaskSeq
					/**CV2019 merges from SIDBI line***/
					vwc.moveDocOnApproveReject(sessionId, master, actionResult, comments, selectedTaskSeq, true, signId, assignSec);
        	 }
         } else { 
        	 /***CV10.2 - Added for Document multiselection enhancement****/
        	 if (docIds != null && docIds.length > 0) {
        		 errorMessage = "";
        		 RouteMasterInfo routeMasterInfo = null;
        		 for (int i =0 ; i< docIds.length; i++) {
        			 VWClient.printToConsole("docIds :"+docIds[i]);
        			 VWClient.printToConsole("routeMasterInfoList :"+routeMasterInfoList);
        			 VWClient.printToConsole("routeMasterInfoList.size :"+routeMasterInfoList.size());
        			 routeMasterInfo = routeMasterInfoList.get(i);
        			 VWClient.printToConsole("routeMasterInfo :"+routeMasterInfo);
        			 if (routeMasterInfo != null) {
	        			 VWClient.printToConsole("routeMasterInfo.getDocName() :"+routeMasterInfo.getDocName());
	        			 VWClient.printToConsole("routeMasterInfo.getActionPermission() :"+routeMasterInfo.getActionPermission());
	        			 if (routeMasterInfo.getActionPermission() != 0) {
		        			 boolean isValid = vwc.getDrsValidateTaskDocumentInfo(sessionId, action, routeMasterInfo);
		        			 if (isValid) {
				        		 routeMasterInfo.setComments(comments);
				        		 VWClient.printToConsole("before caling in moveDocOnApproveReject::"+errorMessage);
				        		 //vwc.moveDocOnApproveReject(sessionId, routeMasterInfo, actionResult, comments, selectedTaskSeq, signId);
				            	 vwc.moveDocOnApproveReject(sessionId, routeMasterInfo, actionResult, comments, selectedTaskSeq, true, signId, 0, null, null, errorMessage);
				            	 VWClient.printToConsole("After caling in moveDocOnApproveReject::"+errorMessage);
		        			 } else {
		        				 errorMessage = errorMessage + "\n"+ routeMasterInfo.getDocName();
		        			 }
	        			 } else {
	        				 //Action is review not accept
	        				 errorMessage = errorMessage + "\n"+ routeMasterInfo.getDocName();
	        			 }
	        			 processCount ++;
        			 }
        		 }
        		 VWClient.printToConsole("errorMessage :"+errorMessage);
        		 if (errorMessage != null && errorMessage.trim().length() > 0) {
        			 /*if (errorList.startsWith(",")) 
        				 errorList = errorList.substring(1, errorList.length());*/
             		String errorMsg = "Failed to apply the action for one or more document(s) "+errorMessage;
     				JOptionPane.showMessageDialog(null,errorMsg);
         			//VWMessage.showMessage(errorMsg);
             	 }
        	 }else{
        		 routeMasterInfo.setComments(comments);
            	 vwc.moveDocOnApproveReject(sessionId, routeMasterInfo, actionResult, comments, selectedTaskSeq, signId);
        	 }
        	 /***End of CV10.2 merges*****************************/
         }
         VWClient.printToConsole("processCount :"+processCount);
         closeDialog();
         dlgMain.setCursor(Cursor.getDefaultCursor());
         return true;
		/*}*/
         
     }
     
    private class KeyBuffer extends Thread
    {
        private String prefix = "";
        private boolean run = true;
        public KeyBuffer()
        {
            setPriority(Thread.MIN_PRIORITY);
        }
        public void appendKey(char c)
        {
            prefix+=c;
        }
        public void kill()
        {
            run = false;
        }
        public void run()
        {
            while (run)
            {
                Util.sleep(100);
                if (prefix.length() == 0) continue;
                prefix = "";
            }
        }
    }
    
    boolean isSigned = false;
    private JScrollPane scRoutePane;
    private JTextArea commentsTextArea;
    private JButton btnOk, btnCancel;
    private KeyBuffer keyBuffer = new KeyBuffer();;
    private JPanel routeMain;
    private JLabel lblActionStatus;
    private JLabel lblComments;
    private JLabel lblRouteNameLabel;
    private JLabel lblRouteName;
    private JLabel lblTaskNameLabel;
    private JLabel lblTaskName;
    private JLabel lblTaskDescLabel;
    private JTextArea txtareaTaskDesc;
        
    private static VWClient vwc;
	private int sessionId;
	private int clientType;
    private RouteMasterInfo routeMasterInfo;
    private Vector<RouteMasterInfo> routeMasterInfoList;
    private int action;
    private String documentName;
    public static boolean active = false;
    private Vector selectedDocs = new Vector();
    public static JDialog dlgMain;
    private int docId = 0;
    private int signResult = 0; 
    private JLabel lblPreviousApprovers=null;
    public VWComboBox cmbPreviousApprovers=null;    
    public Vector vecPreviousApproversLevelSeq = new Vector();
    public TreeSet treesetPreviousApproversLevelSeq = new TreeSet();
    public static int previousApproversLevelSeq = -1;
    public static String endRoute = "End "+ WORKFLOW_MODULE_NAME.toLowerCase();
	SymWindow symWindow = new SymWindow();
	Signature sign = null;
	public int assignSec;
    
	public static int prevSID;
	public static int prevDocId;
	
    public static void main(String[] args) {
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}catch(Exception ex){}
		System.load("c:\\Program Files\\ViewWise Client\\system\\VWSign.dll");	
		String templateStr=VWCUtil.getStrFromFile(new File("C:\\DOCUME~1\\PANDIY~1.M\\LOCALS~1\\Temp\\Temp\\StandardDemo\\Sign\\admin\\admin_T.tmp"));
        System.out.println("templateStr " + templateStr);
        int result=VWCUtil.validateSign(templateStr,0);
		new Thread(new VWRouteAction()).start();		
	} 
}