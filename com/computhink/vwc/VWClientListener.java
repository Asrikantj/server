/*
 * VWClientListener.java
 *
 * Created on March 11, 2004, 12:28 PM
 */

package com.computhink.vwc;
import com.computhink.common.VWEvent;
/**
 *
 * @author  Administrator
 */
public interface VWClientListener extends java.util.EventListener  
{
     void eventHandler(VWEvent event);
}
