/**
 * 
 */
package com.computhink.vwc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;


import com.computhink.common.Constants;
import com.computhink.common.DocTypeList;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.Client;

/**
 * @author Gurumurthy.T.S
 *
 */
public class VWDocTypeListDlg implements Runnable, Constants {
	public static JDialog dlgMain;
	public static int sessionID;
	public static int prevSID;
	public static int prevNodeId;
	public static int nodeId;
	public static int mode;
	private JButton btnOk, btnCancel;
	JPanel pnlList, pnlButton;
	VWCheckList list = null;
	public static boolean isWebClient = false;

	SymWindow symWindow = new SymWindow();
	private static ResourceManager connectorManager = ResourceManager.getDefaultManager();
	public VWDocTypeListDlg(int mode){
		super();
		active = true;
		initComponents();
		dlgMain.setVisible(true);
		keyBuffer.start();
	}

	@SuppressWarnings("static-access")
	public VWDocTypeListDlg(VWClient vwc, int sessionID, int mode)
	{
		super();
		this.sessionID = sessionID;
		this.mode = mode;
		if (active)
		{
			if(sessionID == prevSID){
				dlgMain.requestFocus();
				dlgMain.toFront();
				return;
			}
			else{
				closeDialog();
			}
		}    	 
		this.vwc = vwc; 
		
		if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
			isWebClient = true;
		
		this.sessionID = sessionID;
		prevSID = sessionID;
		initComponents();
		dlgMain.setVisible(true);
		active = true;        
		keyBuffer.start();
	}
	@SuppressWarnings("static-access")
	public VWDocTypeListDlg(VWClient vwc, int sessionID, int mode, int nodeId)
	{
		super();
		this.nodeId = nodeId;
		this.sessionID = sessionID;
		this.mode = mode;
		if (active)
		{
			if(sessionID == prevSID && nodeId == prevNodeId){
				dlgMain.requestFocus();
				dlgMain.toFront();
				return;
			}
			else{
				closeDialog();
			}			
		}    	 
		this.vwc = vwc; 
		if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
			isWebClient = true;

		this.sessionID = sessionID;
		prevSID = sessionID;
		prevNodeId = nodeId;
		initComponents();
		dlgMain.setVisible(true);
		active = true;        
		keyBuffer.start();
	}


	public void run()
	{
		System.out.println("Printing in Run");
		if (vwc == null) return;
		dlgMain.setVisible(true);
	}
	private class KeyBuffer extends Thread
	{
		private String prefix = "";
		private boolean run = true;
		public KeyBuffer()
		{
			setPriority(Thread.MIN_PRIORITY);
		}
		public void appendKey(char c)
		{
			prefix+=c;
		}
		public void kill()
		{
			run = false;
		}
		public void run()
		{
			while (run)
			{
				try 
				{
					Thread.currentThread().sleep(100);
				}
				catch (InterruptedException ie) 
				{
				}
				if (prefix.length() == 0) continue;
				prefix = "";
			}
		}
	}



	public void initComponents(){
		dlgMain = new JDialog();
		btnOk = new JButton();	
		title = new JLabel();
		btnOk.setText(connectorManager.getString("indices.ok"));

		btnCancel = new JButton();
		btnCancel.setText(connectorManager.getString("indices.cancel"));
		pnlList=new JPanel();
		pnlButton=new JPanel();


		list= new VWCheckList();
		Vector vIndices = new Vector();
		vwc.getDocumentTypes(this.sessionID,0,vIndices);
		Vector vecIndices = new Vector();

		//Desc   :Implemeted DocTypeList bean to hold the VWChecklist box values.
		//Author :Gurumurthy.T.S
		//Date    :20-June-2014
		for(int count=0;count<vIndices.size();count++){
			String curIndice = (String) vIndices.get(count);
			StringTokenizer st = new StringTokenizer(curIndice,"\t");
			String id = st.nextToken().toString();
			String name = st.nextToken().toString();
			DocTypeList docTypeList = new DocTypeList(Integer.parseInt(id),name);
			vecIndices.add(docTypeList);
		}
		list.removeItems();
		list.loadData(vecIndices);
		dlgMain.getContentPane().setLayout(null);
		dlgMain.setTitle(connectorManager.getString("indices.settitle"));
		title.setText(connectorManager.getString("VWDocTypeListDlg.settext"));
		dlgMain.getContentPane().add(title);
		title.setVisible(true);
		title.setBounds(15,10,310,20);
		dlgMain.getContentPane().add(list);
		list.setBounds(15,30,310,380);

		dlgMain.getContentPane().add(btnOk);
		btnOk.setBounds(165, 430, 75,22);

		dlgMain.getContentPane().add(btnCancel);
		btnCancel.setBounds(250, 430, 75,22);
		System.out.println("Printing the JDialog ");

		dlgMain.setSize(350, 495);
		dlgMain.setLocation(350, 100);
		dlgMain.setVisible(true);	
		dlgMain.setResizable(false);
		btnOk.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				saveToRegistry();
//				VWCUtil.CustomDialogOk(1);
			}
		});


		btnCancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				closeDialog();
			}
		}); 
		dlgMain.addWindowListener(symWindow);
		list.setItemCheck(0,false);
        list.setItemEnable(0,true);

		loadFromRegistry();

	}
	class SymWindow extends WindowAdapter{
		public void windowLostFocus(WindowEvent e){
			if(active){
				dlgMain.requestFocus();
				dlgMain.toFront();
			}
		}
		public void windowDeactivated(WindowEvent e) {
			if(active){
				dlgMain.requestFocus();
				dlgMain.toFront();
			}
		}
		public void windowStateChanged(WindowEvent e) {
			if(active){	
				dlgMain.toFront();
			}
		}
		public void windowDeiconified(WindowEvent e) {
			if(active){
				dlgMain.toFront();
			}
		}
		public void windowClosing(WindowEvent e) {         
			closeDialog();
		}

	}
	public static void printToConsole(String msg)
	{
		try
		{
			Preferences clientPref =  null;        	
			boolean displayFlag = false;
			try{
				clientPref =  Preferences.userRoot().node("/computhink/" + PRODUCT_NAME.toLowerCase() + "/VWClient");
				displayFlag  = clientPref.getBoolean("debug",false);
			}catch(Exception ex){
				displayFlag = false;
			}

			if (displayFlag){
				String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
				msg = now + " : " + msg + " \n";    			
				java.util.Properties props = System.getProperties(); 
				String filepath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + File.separator + "VWClient.log";
				File log = new File(filepath);        				                                    
				FileOutputStream fos = new FileOutputStream(log, true);
				fos.write(msg.getBytes());
				fos.close();            
				fos.close();
				log = null;              
			}
		}catch (Exception e)
		{
			System.out.println( "printToConsole");	
		}
	}    

	public String getIndicesIds(LinkedList list){
		String id="";
		for(int count=0;count<list.size();count++){
			if(count==(list.size()-1) ){
				DocTypeList docTypeList = (DocTypeList) list.get(count);
				id += docTypeList.getId();
			}
			else{
				DocTypeList docTypeList = (DocTypeList) list.get(count);
				id +=docTypeList.getId()+"," ;
			}
		}
		return id;
	}

	private void saveToRegistry(){
		LinkedList selItems = (LinkedList) list.getSelectedItems();
		String indexIds = "";
		String indexNames = "";
		indexIds = getIndicesIds(selItems).trim();
		List selectedItems  = new LinkedList();
		selectedItems  = list.getSelectedItems();
		int count=selectedItems.size();
		/*
		 * Issue Fix :- CV10.1
		 * Allowing maximum number of document type to seat at the cabinet level. 
		 * (previous value is 33)
		 */
		if(count>75){
			dlgMain.removeWindowListener(symWindow);
			JOptionPane.showMessageDialog(null,connectorManager.getString("indice.jmsg1"),connectorManager.getString("indices.jtitle1"), JOptionPane.OK_OPTION);
			dlgMain.addWindowListener(symWindow);
			return;
		}
		Vector docTypes = new Vector();
		int ret = vwc.setDocumentIndexIdsInRegistry(sessionID, indexIds, nodeId,docTypes);
		if(docTypes!=null&&docTypes.size()>0&&!docTypes.get(0).toString().equalsIgnoreCase("1")){
			JOptionPane.showMessageDialog(null,connectorManager.getString("VWDocTypeListDlg.selectDoctypeMessage")+" "+docTypes.get(0),"Select Document types",javax.swing.JOptionPane.OK_OPTION);
			return;
//			JOptionPane.showMessageDialog(null,"Please selct the doc type(s)",String.valueOf(docTypes.get(0)), JOptionPane.INFORMATION_MESSAGE);
//			dlgMain.setDefaultCloseOperation(dlgMain.DO_NOTHING_ON_CLOSE);
		}
//		Session session = vwc.getSession(this.sessionID);
//		String curRoom = session.room;

		dlgMain.removeWindowListener(symWindow);

		closeDialog();
	}

	private void loadFromRegistry(){
		Vector indexInfo = new Vector();
		vwc.getDocumentIndexIdsFromRegistry(sessionID, nodeId, indexInfo);
		String indexIds = "";
		if(indexInfo!=null && indexInfo.size()>0){
			indexIds = indexInfo.get(0).toString();
		}
		if(indexIds.contains(","))
		indexIds = indexIds.replace(",", "\t");
		if(!indexIds.equalsIgnoreCase("0"))
		selectItems(indexIds);
	}

	public void selectItems(String indexNames){
		StringTokenizer stokens=new StringTokenizer(indexNames,"\t");
		while(stokens.hasMoreTokens()){
			try{
				String curToken = stokens.nextToken();
				list.setItemCheck(3,true,Integer.parseInt(curToken));
			}catch(Exception e){

			}
		}
		dlgMain.repaint();
		
	}


	private void closeDialog() 
	{
		try{
			active = false;
			keyBuffer.kill();
			dlgMain.setVisible(false);
			dlgMain.dispose();
		}catch(Exception ex){
		}
	}


	public static void main(String[] args) {
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}catch(Exception ex){}

		new VWDocTypeListDlg(1);	
	}
	private static VWClient vwc;
	private int session;
	private static boolean active = false;
	private KeyBuffer keyBuffer = new KeyBuffer();
	private static JLabel title;
}
