package com.computhink.vwc;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.Collator;
import java.util.Vector;

import javax.swing.AbstractListModel;
import javax.swing.DefaultListCellRenderer;
import javax.swing.DefaultListSelectionModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.computhink.common.Constants;
import com.computhink.common.RouteInfo;
import com.computhink.common.Util;
import com.computhink.drs.server.DRSLog;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.image.Images;


public class VWRouteList implements Runnable
{
	private static ResourceManager connectorManager = ResourceManager.getDefaultManager();
     public VWRouteList()
     {    
        new Images();
        initComponents();
        dlgMain.setVisible(true);
        keyBuffer.start();
     }
     
    /**
     * CV10.1 Enhancement: sendToOption parameter added for checking whether the document is sent from tool bar 
     * sendTo option or not
     * @param vwc
     * @param session
     * @param docTypeId
     * @param docId
     * @param documentName
     * @param sendToOption
     */
	public VWRouteList(VWClient vwc, int session, int docTypeId, int docId, String documentName, boolean sendToOption) {
		this.docId = docId;
		active=false;
		if (active) {
			if (session == prevSessionId && docTypeId == prevDocTypeId && docId == prevDocId) {
				dlgMain.requestFocus();
				dlgMain.toFront();
				return;
			} else {
				closeDialog();
			}
		}
		this.vwc = vwc;
		this.session = session;
		prevSessionId = session;
		this.docTypeId = docTypeId;
		prevDocTypeId = docTypeId;
		prevDocId = docId;
		this.documentName = documentName;

		vwc.getAssignedRoutesOfDocType(session, docTypeId, routeList);
		VWClient.printToConsole("routeList Inside Parameterized Constructor of VWRouteList::::" + routeList+" sendToOption :"+sendToOption);
		VWClient.printToConsole("routeList.size Inside Parameterized Constructor of VWRouteList::::" + routeList.size());
		if (session > 0) {
			/**
			 * CV 10.1 Enhancement: If else condition is added for implementing if the document is sent
			 * from SendTo tool bar and documentType is having only one workflow then the workflow pop up
			 * will not come.
			 */
			if (sendToOption == true) {
				if(routeList.size() == 0){
					VWClient.printToConsole("Size == 0 VWRouteList Parameterized Constructor!!!!");
					JOptionPane.showMessageDialog(null,connectorManager.getString("routelist.AssignDoctypeMsg1")+" "+connectorManager.getString("genaral.workflow")+" "+connectorManager.getString("routelist.AssignDoctypeMsg2"));
				} else if (routeList.size() == 1) {
					VWClient.printToConsole("\n\n");
					VWClient.printToConsole("Size == 1 VWRouteList Parameterized Constructor!!!!");
					initComponentsForSingleWf();
					VWClient.printToConsole("routeList.size() == 1::::" + routeList.size());
					VWClient.printToConsole("routeList VWRouteList.loadRouteList 1::::" + routeList);
				} else {
					initComponents();
					VWClient.printToConsole("Size > 1 VWRouteList Parameterized Constructor For Toolbar Button!!!!");
					show();
					dlgMain.setModal(true);
					loadRouteList();
					active = true;
					keyBuffer.start();					
				}
			} else {
				initComponents();
				VWClient.printToConsole("VWRouteList Parameterized Constructor For Right Click!!!!");
				show();
				dlgMain.setModal(true);
				loadRouteList();
				active = true;
				keyBuffer.start();
			}
		}
	}
     
     private void initComponents() 
     {
    	routeMain = new JPanel();
    	dlgMain = new JDialog();
    	dlgMain.getContentPane().setLayout(null);
    	dlgMain.getContentPane().add(routeMain);
    	dlgMain.setSize(290, 415);
    	routeMain.setBounds(0, 0, 290, 415);
    	routeMain.setLayout(null);
        btPSelect = new JButton(connectorManager.getString("routelist.apply"));
        btPCancel = new JButton(connectorManager.getString("routelist.cancel"));
        scRoutePane = new JScrollPane();
        lstRouteList = new JList();
        lstRouteList.setSelectionMode(DefaultListSelectionModel.SINGLE_SELECTION);

        lstRouteList.setCellRenderer(new RouteCellRenderer());
        RlistModel = new RouteListModel();
        lstRouteList.setModel(RlistModel); 
        //lstPPrincipals.setAutoscrolls(true);
        dlgMain.getContentPane().setLayout(null);
        dlgMain.setTitle(connectorManager.getString("routelist.available")+" "+connectorManager.getString("Route.Workflows"));
        scRoutePane.setBorder(new TitledBorder(new BevelBorder
                                  (BevelBorder.LOWERED),connectorManager.getString("routelist.Select")+" "+connectorManager.getString("genaral.workflow")+" "+connectorManager.getString("routelist.for")+" '"+ documentName+"'"));
        scRoutePane.setViewportView(lstRouteList);
        scRoutePane.setBounds(10, 10, 262, 318);
        routeMain.add(scRoutePane);
               
        
        routeMain.add(btPSelect);
        btPSelect.setBounds(120, 345, 70, 25);
        
        routeMain.add(btPCancel);
        btPCancel.setBounds(200, 345, 70, 25);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();        
        VWClient.printToConsole("screenSize.............x :"+(screenSize.width-1000)/2+ "("+screenSize.width+")");
        VWClient.printToConsole("screenSize.............y :"+(screenSize.height-650)/2+ "("+screenSize.height+")");
        dlgMain.setLocation((screenSize.width-1000)/2,(screenSize.height-650)/2);
        dlgMain.setResizable(false);
        
        
        btPCancel.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent evt) {
               closeDialog();
            }
        });
        btPSelect.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent evt) {
               addSelected();
               closeDialog();
            }
        });

        lstRouteList.addMouseListener(new MouseAdapter() {
        public void mouseClicked(MouseEvent evt) {
            /*if (evt.getClickCount() > 1) 
            {
                addSelected();
                closeDialog();
            }*/
            }
        });
        lstRouteList.addKeyListener(new KeyAdapter(){
        public void keyPressed(KeyEvent e){
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) closeDialog();
                keyBuffer.appendKey(e.getKeyChar());
            }
        public void keyReleased(KeyEvent e){
            }
        public void keyTyped(KeyEvent e){
            }
        });
         
     }
     
     /**
      * Enhancement:CV10.1 To display the confirmation optionPane.
      */     
     private void initComponentsForSingleWf() 
     {
    	 VWClient.printToConsole("initComponentsForSingleWf......");
    	 int selectedOption = JOptionPane.showConfirmDialog(null,connectorManager.getString("VWRouteList.sendtoWorkflow")+"\t\t"
    			 +routeList.get(0).toString()+"\t ",connectorManager.getString("genaral.workflow"), JOptionPane.YES_NO_OPTION);
    	 if (selectedOption == JOptionPane.YES_OPTION) {
			 /**CV2019 merges from SIDBI line******/
    		 SendWorkflowThread thread=new SendWorkflowThread(); 
    		 thread.start();
    		// addDocumentToSingleWorkFlow();
    	 }
     }
     
     public void run()
     {
         show();
     }
     public void show()
     {
         if (vwc == null) return;
         /*
      	 * new frame.show(); method is replaced with new frame.setVisible(true); 
      	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
      	 */
             dlgMain.setVisible(true);
     }
     
     private void closeDialog() 
     {
    	 try{
         active = false;
         keyBuffer.kill();
         dlgMain.dispose();
    	 }catch(Exception e){
    		 VWClient.printToConsole("Exception in close Dialog ::::::::::"+e.getMessage());
    	 }
     }

     private void loadRouteList(){
    	 if (session > 0){
    		 //Enhancement :CV10.1 Commented for single workflow
    		 //vwc.getAssignedRoutesOfDocType(session, docTypeId, routeList);
             for (int i = 0; i < routeList.size(); i++)
             {
                 RouteInfo route = (RouteInfo) routeList.get(i);
                 UIRoute routeInfo = new UIRoute(route);
                 RlistModel.addRouteInfo(routeInfo);
             }
    	 }
     }
     /**
      * Method added in CV10.1
      * For sending the document to the single workflow without opening the dialog box
      */
     private void addDocumentToSingleWorkFlow(){
    	 try{
    	 String sentWfName=routeList.get(0).toString();
    	 VWClient.printToConsole("sentWfName VWRouteList.addDocumentToSingleWorkFlow::::"+sentWfName);
    	 int id = 0;
    	 VWClient.printToConsole("id VWRouteList.addDocumentToSingleWorkFlow::::" + id);
    	 // Validation for selection of any route name.
    	 if (id == -1) {
    		 active = false;
    		 JOptionPane.showMessageDialog(null,
    				 connectorManager.getString("routelist.jmsg1") + " " + connectorManager.getString("genaral.workflow")
    				 + " " + connectorManager.getString("routelist.jmsg2"));
    		 active = true;
    	 }

    	 RouteInfo route = (RouteInfo) routeList.get(0);
    	 VWClient.printToConsole("route.toString() VWRouteList.addDocumentToSingleWorkFlow::::" + route.toString());
    	 UIRoute routeInfo = new UIRoute(route);
    	 VWClient.printToConsole(
    			 "routeInfo.toString() VWRouteList.addDocumentToSingleWorkFlow::::" + routeInfo.toString());
    	 int routeId = routeInfo.getRouteInfo().getRouteId();
    	 String routeName = routeInfo.getRouteInfo().getRouteName();
    	 VWClient.printToConsole("VWRouteList.addDocumentToSingleWorkFlow:routeName:::" + routeName+"::routeId::"+routeId);
    	 int taskSequence = 1;
    	 VWClient.printToConsole("Before calling getDocumentsInRoute........1");
    	 if (vwc.getDocumentsInRoute(session, docId, new Vector()) == 0) {
    		 Vector resultVector = new Vector();
    		 int ret = vwc.validateRouteDocument(session, routeId, docId, resultVector);
    		 VWClient.printToConsole("ret from VWRouteList.addDocumentToSingleWorkFlow::::" + ret);
    		 VWClient.printToConsole("resultVector VWRouteList.addDocumentToSingleWorkFlow::::" + resultVector);
    		 if (resultVector != null && resultVector.size() > 0) {
    			 String msg = (String) resultVector.get(0);
    			 DRSLog.war(msg + " <" + documentName + "> ");
    			 active = false;
    			 // closeDialog();
    			 JOptionPane.showMessageDialog(null, msg + " <" + documentName + "> ");
    			 active = true;
    			 return;
    		 }
    		 VWClient.printToConsole("sendDocsInManualRoute method calling from addDocumentToSingleWorkFlow method.....");
    		 vwc.sendDocInManualRoute(session, docId, documentName, routeId, routeName, taskSequence);
    		 closeDialog();
    	 } else {
    		 active = false;
    		 JOptionPane.showMessageDialog(null, connectorManager.getString("routelist.jmsg3") + ".");
    		 active = true;
    	 }
    	 }catch(Exception e){
    		 VWClient.printToConsole("Exception while sending single workflow :::::::::"+e.getMessage());
    	 }
     }
     
     private void addSelected()
     {
    	 VWClient.printToConsole("addSelected........");
    	 int id = lstRouteList.getSelectedIndex();         
    	 //Validation for selection of any route name.
    	 if(id==-1){
    		 active = false;
    		 JOptionPane.showMessageDialog(null,connectorManager.getString("routelist.jmsg1")+" "+connectorManager.getString("genaral.workflow")+" "+connectorManager.getString("routelist.jmsg2"));
    		 active = true;
    	 }
    	 UIRoute uiRoute = null;         
    	 uiRoute = (UIRoute) RlistModel.getElementAt(id);         
    	 int routeId = uiRoute.getRouteInfo().getRouteId();
    	 String routeName = uiRoute.getRouteInfo().getRouteName();
    	 int taskSequence = 1;
    	 VWClient.printToConsole("Before calling getDocumentsInRoute........2");
    	 if(vwc.getDocumentsInRoute(session, docId, new Vector()) == 0){
    		 Vector resultVector = new Vector();
    		 int ret = vwc.validateRouteDocument(session, routeId, docId, resultVector);
    		 if(resultVector != null && resultVector.size() > 0){
    			 String msg = (String) resultVector.get(0);
    			 //DRSLog.add("Document <"+documentName+"> does not contains the indices \""+ msg+"\"");
    			 DRSLog.war(msg+" <"+documentName+"> ");
    			 active = false;
    			 JOptionPane.showMessageDialog(null, msg+" <"+documentName+"> ");
    			 active = true;
    			 return;
    		 }
    		 VWClient.printToConsole("sendDocsInManualRoute method calling from addselected method.....");
    		// vwc.sendDocInManualRoute(session, docId, documentName, routeId, routeName, taskSequence);
			/**CV2019 merges from SIDBI line******/
    		int retFlag= vwc.sendDocInManualRoute(session, docId, documentName, routeId, routeName, taskSequence);
    		/*if(retFlag==0){
    			com.computhink.common.Document doc=new 	com.computhink.common.Document(docId);
    			Vector types=new Vector();
    			vwc.getDocumentInfo(session,doc,types);
    			if(types.size()>0&&types!=null){
    				com.computhink.common.Document doc1=(com.computhink.common.Document)types.get(0);
    				active = false;
    				JOptionPane.showMessageDialog(null,doc1.getDocType().getName()+ " submitted sucessfully.");
    				active = true;
    			}
    		}*/
    		 
    	 }
    	 else{
    		 active = false;
    		 JOptionPane.showMessageDialog(null,connectorManager.getString("routelist.jmsg3")+".");
    		 active = true;
    		 closeDialog();
    	 }
     }
	 /**CV2019 merges from SIDBI line******/
     private class SendWorkflowThread extends Thread
     {
    	  public void run(){
    	    	 try{
    	    	 String sentWfName=routeList.get(0).toString();
    	    	 VWClient.printToConsole("sentWfName VWRouteList.addDocumentToSingleWorkFlow::::"+sentWfName);
    	    	 int id = 0;
    	    	 VWClient.printToConsole("id VWRouteList.addDocumentToSingleWorkFlow::::" + id);
    	    	 // Validation for selection of any route name.
    	    	 if (id == -1) {
    	    		 active = false;
    	    		 JOptionPane.showMessageDialog(null,
    	    				 connectorManager.getString("routelist.jmsg1") + " " + connectorManager.getString("genaral.workflow")
    	    				 + " " + connectorManager.getString("routelist.jmsg2"));
    	    		 active = true;
    	    	 }

    	    	 RouteInfo route = (RouteInfo) routeList.get(0);
    	    	 VWClient.printToConsole("route.toString() VWRouteList.addDocumentToSingleWorkFlow::::" + route.toString());
    	    	 UIRoute routeInfo = new UIRoute(route);
    	    	 VWClient.printToConsole(
    	    			 "routeInfo.toString() VWRouteList.addDocumentToSingleWorkFlow::::" + routeInfo.toString());
    	    	 int routeId = routeInfo.getRouteInfo().getRouteId();
    	    	 String routeName = routeInfo.getRouteInfo().getRouteName();
    	    	 VWClient.printToConsole("VWRouteList.addDocumentToSingleWorkFlow:routeName:::" + routeName+"::routeId::"+routeId);
    	    	 int taskSequence = 1;
    	    	 VWClient.printToConsole("Before calling getDocumentsInRoute........4");
    	    	 if (vwc.getDocumentsInRoute(session, docId, new Vector()) == 0) {
    	    		 Vector resultVector = new Vector();
    	    		 int ret = vwc.validateRouteDocument(session, routeId, docId, resultVector);
    	    		 VWClient.printToConsole("ret from VWRouteList.addDocumentToSingleWorkFlow::::" + ret);
    	    		 VWClient.printToConsole("resultVector VWRouteList.addDocumentToSingleWorkFlow::::" + resultVector);
    	    		 if (resultVector != null && resultVector.size() > 0) {
    	    			 String msg = (String) resultVector.get(0);
    	    			 DRSLog.war(msg + " <" + documentName + "> ");
    	    			 active = false;
    	    			 // closeDialog();
    	    			 JOptionPane.showMessageDialog(null, msg + " <" + documentName + "> ");
    	    			 active = true;
    	    			 return;
    	    		 }
    	    		 vwc.sendDocInManualRoute(session, docId, documentName, routeId, routeName, taskSequence);
    	    		 closeDialog();
    	    	 } else {
    	    		 active = false;
    	    		 JOptionPane.showMessageDialog(null, connectorManager.getString("routelist.jmsg3")+ ".");
    	    		 active = true;
    	    	 }
    	    	 }catch(Exception e){
    	    		 VWClient.printToConsole("Exception while sending single workflow :::::::::"+e.getMessage());
    	    	 }
    	     }
     }
     
     
     
    private class KeyBuffer extends Thread
    {
        private String prefix = "";
        private boolean run = true;
        public KeyBuffer()
        {
            setPriority(Thread.MIN_PRIORITY);
        }
        public void appendKey(char c)
        {
            prefix+=c;
        }
        public void kill()
        {
            run = false;
        }
        public void run()
        {
            while (run)
            {
                Util.sleep(100);
                if (prefix.length() == 0) continue;
                prefix = "";
            }
        }
    }
    private JScrollPane scRoutePane;
    private JList lstRouteList;
    private JButton btPSelect, btPCancel;
    private KeyBuffer keyBuffer = new KeyBuffer();;
    private JPanel routeMain;
    
    private RouteListModel RlistModel;
    private Vector routeList = new Vector(); 
    private static VWClient vwc;
    private int session;
    public static int prevSessionId = 0;
    private int docTypeId;
    public static int prevDocTypeId = 0;
    private int docId;
    public static int prevDocId = 0;
    private String documentName;
    private static boolean active = false;
    
    private static JDialog dlgMain;
    
    public static void main(String[] args) {
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}catch(Exception ex){}
		new Thread(new VWRouteList()).start();		
	} 
}
//--------------------------------------------------------------------------
 class RouteListModel extends AbstractListModel
 { 
	private Vector routeList = new Vector(); 

    private void update() 
    {
        //Updated 2-3-2005 to avoid delays in populating list
        //sort(); 
	    fireContentsChanged(this, 0, getSize()); 
	}
    public void sortList()
    {
        sort();
    }
    public void addRouteInfo(UIRoute route) 
    { 
        if (contains(route)) return;
        routeList.addElement(route);
        update();
	} 

	public void removePrincipal(UIRoute route) 
    { 
	} 
	public int getSize() 
    { 
	    return routeList.size(); 
	} 
	public Object getElementAt(int index) 
    { 
	    return (UIRoute) routeList.elementAt(index); 
	}
    private boolean contains(UIRoute uip)
    {
        int count = getSize();
        String name = uip.getText();
        for (int i = 0; i < count; i++)
        {
           if (name.equals(((UIRoute) routeList.get(i)).getText()))
                return true;
        }
        return false;
    }
    private void sort() 
    {
         int count = getSize();
         JLabel[] a = new JLabel[count];
         for (int i = 0; i < count; i++)
         {
            a[i] = (JLabel) routeList.get(i);
         }
         sortArray(Collator.getInstance(), a);
         for (int i = 0; i < count; i++) 
         {
        	 routeList.setElementAt(a[i], i);
         }
    }
    private void sortArray(Collator collator, JLabel[] strArray) 
    {
       if (strArray.length == 1) return;
       for (int i = 0; i < strArray.length; i++) 
       {
           for (int j = i + 1; j < strArray.length; j++) 
           {
               if(collator.compare(strArray[i].getText(), 
                                               strArray[j].getText() ) > 0 ) 
               {
                   JLabel tmp = strArray[i];
                   strArray[i] = strArray[j];
                   strArray[j] = tmp;
                }
            }
        }
    }
    public RouteInfo getRouteByName(String name) 
    { 
        for (int i = 0; i < routeList.size(); i++)
        {
        	UIRoute uip = (UIRoute) routeList.get(i);
            if (uip.getText().equals(name))
                    return uip.getRouteInfo();
        }
        return null;
	}
    public Vector getRouteInfo()
    {
        return this.routeList;
    }
 } 
 class RouteCellRenderer extends DefaultListCellRenderer 
 { 
    public Component getListCellRendererComponent(JList list, Object value, 
                         int index, boolean isSelected, boolean cellHasFocus) 
     { 
         Component retValue = super.getListCellRendererComponent( 
                          	list, value, index, isSelected, cellHasFocus);
         setText( ((JLabel) value).getText());
        // setIcon( ((JLabel) value).getIcon());
         return retValue;
      } 
 }