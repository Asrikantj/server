package com.computhink.vwc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import com.computhink.vwc.VWTableResizer;
import com.computhink.vwc.doctype.VWUtil.IconData;
import com.computhink.vwc.image.Images;

import sun.swing.DefaultLookup;

import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.common.util.VWPrint;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.util.List;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

//--------------------------------------------------------------------------
public class VWRouteSummaryTable extends JTable implements VWCConstants {
	protected VWRouteSummaryTableData m_data;
	public int ttlColumns=0;
	public int summaryColumn=0;
	public String documentName = "";
	public VWRouteSummary parent = null;
	public VWRouteSummaryTable(){
	    this(new VWRouteSummary(0), "");
	}
	public VWRouteSummaryTable(VWRouteSummary parent, String documentName){
		super();
		new Images();
		this.documentName = documentName;
		this.parent = parent;
		getTableHeader().setReorderingAllowed(false);
		m_data = new VWRouteSummaryTableData(this);
		setCellSelectionEnabled(false);
		setAutoCreateColumnsFromModel(false);
		setModel(m_data);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		/setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		VWClient.printToConsole("VWRouteSummaryTableData.m_columns.length...."+VWRouteSummaryTableData.m_columns.length);
		Dimension tableWith = getPreferredScrollableViewportSize();
		for (int k = 0; k < VWRouteSummaryTableData.m_columns.length; k++) {
			VWClient.printToConsole("VWRouteSummaryTableData.m_columns[k] :"+VWRouteSummaryTableData.m_columns[k]);
			TableCellRenderer renderer;
			
			DefaultTableCellRenderer textRenderer;
			if(VWRouteSummary.isWebClient)
				textRenderer =new DefaultTableCellRenderer();
			else
				textRenderer =new VWTableCellRenderer();
			
			textRenderer.setHorizontalAlignment(VWRouteSummaryTableData.m_columns[k].m_alignment);
			//textRenderer.setVerticalAlignment(SwingConstants.CENTER);
			renderer = textRenderer;
			VWRouteSummaryRowEditor editor=new VWRouteSummaryRowEditor(this);
			TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWRouteSummaryTableData.m_columns[k].m_width),renderer,editor);
			//column.setCellRenderer(new WrappableCellRenderer());
			addColumn(column);
		}
		
		ttlColumns = getColumnModel().getColumnCount();
		summaryColumn = ttlColumns-1;
		getColumnModel().getColumn((summaryColumn)).setPreferredWidth(130);
		int statusColumn = ttlColumns-2;
		getColumnModel().getColumn((statusColumn)).setPreferredWidth(130);
		
		//Desc   :Set Display wrappable
		//Author :Nishad Nambiar
		//Date   :24-Jul-2007
		//getColumnModel().getColumn(summaryColumn).setCellRenderer(new WrappableCellRenderer());
		//getColumnModel().getColumn(summaryColumn).setCellEditor(new WrappableTextEditor());
		
		
		//Hide Task Summary Column
		getColumnModel().getColumn(TASK_DESCRIPTION_COL_INDEX).setPreferredWidth(0); 
		getColumnModel().getColumn(TASK_DESCRIPTION_COL_INDEX).setMinWidth(0);
		getColumnModel().getColumn(TASK_DESCRIPTION_COL_INDEX).setMaxWidth(0);
		
		getColumnModel().getColumn(m_data.COL_SERIALNUMBER).setPreferredWidth(32);
		getColumnModel().getColumn(m_data.COL_USERNAME).setPreferredWidth(70);
		getColumnModel().getColumn(m_data.COL_TASK).setPreferredWidth(90);
		getColumnModel().getColumn(m_data.COL_COMMENTS).setPreferredWidth(326);//320
		getColumnModel().getColumn(m_data.COL_STATUS).setPreferredWidth(80);//120
		getColumnModel().getColumn(m_data.COL_RECEIVEDON).setPreferredWidth(85);
		getColumnModel().getColumn(m_data.COL_PROCESSEDON).setPreferredWidth(79);
		getColumnModel().getColumn(m_data.COL_ROUTENAME).setPreferredWidth(84);
		getColumnModel().getColumn(m_data.COL_USERSTATUS).setPreferredWidth(85);
		
		/****CV2019 merges from SIDBI line--------------------------------***/
		setRowHeight(20);
		getColumnModel().getColumn(m_data.COL_SERIALNUMBER).setCellRenderer(new WrappableCellRenderer());
		getColumnModel().getColumn(m_data.COL_USERNAME).setCellRenderer(new WrappableCellRenderer());
		//getColumnModel().getColumn(m_data.COL_TASK).setCellRenderer(new WrappableCellRenderer());
		getColumnModel().getColumn(m_data.COL_COMMENTS).setCellRenderer(new WrappableCellRenderer());
		//getColumnModel().getColumn(m_data.COL_STATUS).setCellRenderer(new WrappableCellRenderer());
		getColumnModel().getColumn(m_data.COL_RECEIVEDON).setCellRenderer(new WrappableCellRenderer());
		getColumnModel().getColumn(m_data.COL_PROCESSEDON).setCellRenderer(new WrappableCellRenderer());
		getColumnModel().getColumn(m_data.COL_ROUTENAME).setCellRenderer(new WrappableCellRenderer());
		getColumnModel().getColumn(m_data.COL_USERSTATUS).setCellRenderer(new WrappableCellRenderer());
		/*------------------End of SIDBI merges--------------------------------------------*/
		setRowSelectionAllowed(true);
		
		if(!VWRouteSummary.isWebClient)
			setBackground(java.awt.Color.white);
		//To get horizontal scroll bar
		//setAutoResizeMode(JTable.AUTO_RESIZE_OFF);  //commented by Srikanth on 16 Nov 2015 for resizing all column
		setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS); //added by Srikanth on 16 Nov 2015 for resizing all column
		SymMouse aSymMouse = new SymMouse();
		addMouseListener(aSymMouse);
		
		
		JTableHeader header = getTableHeader();
		VWClient.printToConsole("header.getBackground() :"+header.getBackground());
		//header.setBackground(Color.DARK_GRAY);
		//header.setBorder(new EmptyBorder(1, 1, 1, 1));
		header.setBorder(new EmptyBorder(1, 3, 1, 3));
		VWClient.printToConsole("header.getBackground() after setting the color :"+header.getBackground());
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        
        setTableResizable();        
	}
	public void showPopupMenu(MouseEvent e){
	    JPopupMenu menu = new JPopupMenu();
	    JMenuItem export = new JMenuItem("Export", Images.export);
	    JMenuItem print = new JMenuItem("Print "+ Constants.WORKFLOW_MODULE_NAME +" Summary", Images.print);
	    JMenuItem taskDetails = new JMenuItem("Task Details", Images.taskDetails);
	    JMenuItem close = new JMenuItem("Close", Images.taskClose);
	    
	    menu.add(export);
	    menu.add(print);
	    menu.add(taskDetails);
	    menu.add(close);	    
/*	    if ("Windows".equalsIgnoreCase(UIManager.getLookAndFeel().getName())){
		  //System.out.println("UI Modified");
		    export.setMargin(new java.awt.Insets(2, -12, 2, 2));
		    print.setMargin(new java.awt.Insets(2, -12, 2, 2));
		    taskDetails.setMargin(new java.awt.Insets(2, -12, 2, 2));
		    close.setMargin(new java.awt.Insets(2, -12, 2, 2));		
	    }*/


	    
	    export.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		      parent.doExport();
		  }
	    });
	    print.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		      parent.doPrint();
		  }
	    });
	    taskDetails.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		      new VWRouteSummaryDetails(parent.dlgMain, parent);
		  }
	    });

	    close.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		      parent.closeDialog();
		  }
	    });
	    menu.show(this, e.getX(), e.getY());
	}
	public void setTableResizable() {
	// Resize Table
	new VWTableResizer(this);
	}	
	
    //Desc   :This function shows the Task Description as tooltip, whenever mouse is pointed to task column.
    //Author :Nishad Nambiar
    //Date   :7-May-2007
    public String getToolTipText(MouseEvent e) {
        String tip = null;
        java.awt.Point p = e.getPoint();
        int rowIndex = rowAtPoint(p);
        int colIndex = columnAtPoint(p);
        int realColumnIndex = convertColumnIndexToModel(colIndex);
        RouteSummaryRowData row = (RouteSummaryRowData) m_data.m_vector.elementAt(rowIndex);        
        if (realColumnIndex == m_data.COL_TASK) { 
            tip = " " + " " + row.m_TaskDesc;
        }
        return tip;
    }
    

//	--------------------------------------------------------------------------
	class SymMouse extends java.awt.event.MouseAdapter{
		public void mouseClicked(java.awt.event.MouseEvent event) {
			Object object = event.getSource();
			if (object instanceof JTable)
				if(event.getModifiers()==event.BUTTON3_MASK)
					VWDocRouteTable_RightMouseClicked(event);
				else if(event.getModifiers()==event.BUTTON1_MASK)
					VWDocRouteTable_LeftMouseClicked(event);
		}
	}
//	--------------------------------------------------------------------------
	void VWDocRouteTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			lSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			lDoubleClick(event,row,column);
	}
//	--------------------------------------------------------------------------
	void VWDocRouteTable_RightMouseClicked(java.awt.event.MouseEvent event) {
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			rSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			rDoubleClick(event,row,column);
	}
//	--------------------------------------------------------------------------
	private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {
	    showPopupMenu(event);
	}
//	--------------------------------------------------------------------------
	private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
		
	}
//	--------------------------------------------------------------------------
	private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
		
	}
//	--------------------------------------------------------------------------
	private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
	    new VWRouteSummaryDetails(parent.dlgMain, parent);
	}
//	--------------------------------------------------------------------------
	//Desc   :Select the last processed row in the table.
	//Author :Nishad Nambiar
	//Date   :21-Aug-2008 
	public void setLastProcessedRowSelected() {
		int rowToSelect = -1;
		if(m_data.m_vector!=null && m_data.m_vector.size()>0){
			for(int count=(m_data.m_vector.size()-1);count>=0;count--){
				try{
					RouteSummaryRowData row = (RouteSummaryRowData)m_data.m_vector.elementAt(count);
					if(!row.m_ProcessedOn.toString().trim().equalsIgnoreCase("") && !row.m_ProcessedOn.toString().trim().equalsIgnoreCase("-")){									
						rowToSelect = count;
						break;
					}
				}catch(Exception e){
					//System.out.println("EXCEPTION :"+e.toString());
				}
			}		
		}
		if(rowToSelect >=0)
			setRowSelectionInterval(rowToSelect, rowToSelect);
	}
//		--------------------------------------------------------------------------
		public void addData(List list) {
		m_data.setData(list);
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public void addData(String[][] data) {
		m_data.setData(data);
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public String[][] getData() {
		return m_data.getData();
	}
//	--------------------------------------------------------------------------
	public void removeData(int row) {
		m_data.remove(row);
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	
	public void insertData(String[][] data) {
		for(int i=0;i<data.length;i++) {
			m_data.insert(new RouteSummaryRowData(data[i]));
		}
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public String[] getRowData(int rowNum) {
		return m_data.getRowData(rowNum);
	}
//	--------------------------------------------------------------------------
	public int getRowDocLockId(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return Util.to_Number((row[0]));
	}
//	--------------------------------------------------------------------------
	public String getRowDocName(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return row[1];
	}
//	--------------------------------------------------------------------------
	public int getRowType() {
		return getRowType(getSelectedRow());
	}
//	--------------------------------------------------------------------------
	public int getRowType(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return Util.to_Number((row[6]));
	}
//	--------------------------------------------------------------------------
	public void clearData() {
		m_data.clear();
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
}
class RouteSummaryRowData {
	public String 	m_SerailNumber;
	public String   m_UserName;
	public String   m_RouteName;
	public String   m_ReceivedOn;
	public String   m_ProcessedOn;
	public String   m_Status;
	public String   m_Comments;
	public String   m_Task;
	public String   m_TaskDesc;
	public String   m_SignType;
	public String   m_UserStatus;
	
	
	public RouteSummaryRowData() {
		m_SerailNumber="";
		m_UserName="";
		m_RouteName="";
		m_ReceivedOn="";
		m_ProcessedOn="";
		m_Status="";
		m_Comments="";
		m_Task = "";
		m_TaskDesc = "";
		m_SignType = "";
		m_UserStatus = "";
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData(String userName, String routeName, String receivedOn,
			String status,  String processedOn, String comments, String task, String taskDesc, String serialNumber, String signType, String userStatus) {
		m_SerailNumber = serialNumber;
		m_UserName = userName;
		m_RouteName = routeName; 
		m_ReceivedOn = receivedOn;
		m_ProcessedOn = processedOn;
		m_Status = status;
		m_Comments = comments;
		m_Task = task;
		m_TaskDesc = taskDesc;
		m_SignType = signType;
		m_UserStatus = userStatus;
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData(String str) {
		StringTokenizer tokens=new StringTokenizer(str,Util.SepChar,false);		
		//System.out.println("Token size " + tokens.countTokens());
		try {	
			m_SerailNumber = tokens.nextToken();	
			m_RouteName = tokens.nextToken();
			m_UserName = tokens.nextToken();
			m_ReceivedOn=tokens.nextToken();		
			m_Status=Util.getFixedValue(tokens.nextToken(),"");
			m_ProcessedOn=Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"","");
			m_Comments=Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"","");
			m_Task=Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"","");
			m_TaskDesc = Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"","");
			String sign = Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"",""); 
			if(m_Status.contains("Rejected"))
				sign = "-";
			m_SignType = sign.equals("1")?"Authorized":(sign.equals("2")?"Certified":"-");
			m_UserStatus = tokens.nextToken();

		}catch(Exception ex){
			//System.out.println(m_RouteName + " ::: "+  m_UserName + " ::: " + m_ReceivedOn +  " ::: " + m_Status +   " ::: " +  m_ProcessedOn + " ::: "  +  m_Comments); 					
			//System.out.println(" RouteSummaryRowData Exception  " + ex.getMessage() );
		}
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData(String[] data) {
		int i=0;
		m_SerailNumber = data[i++];
		m_UserName = data[i++];
		m_ReceivedOn = data[i++];		
		m_Status = data[i++];
		m_ProcessedOn = data[i++];
		m_Comments = data[i++];
		m_Task = data[i++];
		m_TaskDesc = data[i++];
		m_SignType = data[i++];
		m_UserStatus = data[i++];
	}
}
//--------------------------------------------------------------------------
class RouteSummaryColumnData {
	public String  m_title;
	float m_width;
	int m_alignment;
	
	public RouteSummaryColumnData(String title, float width, int alignment) {
		m_title = title;
		m_width = width;
		m_alignment = alignment;
	}
}
//--------------------------------------------------------------------------
class VWRouteSummaryTableData extends AbstractTableModel {
	public static final RouteSummaryColumnData m_columns[] = {
		new RouteSummaryColumnData(VWCConstants.RouteColNames[0],0.1F,JLabel.LEFT ),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[1],0.2F, JLabel.LEFT),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[2],0.2F,JLabel.LEFT),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[3],0.2F,JLabel.LEFT),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[4],0.2F,JLabel.LEFT),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[5],0.2F,JLabel.LEFT),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[6],0.2F,JLabel.LEFT),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[7],0.2F,JLabel.LEFT),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[8],0.2F,JLabel.LEFT),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[9],0.2F,JLabel.LEFT ),
		new RouteSummaryColumnData(VWCConstants.RouteColNames[10],0.2F,JLabel.LEFT ),
	};
	public static final int COL_SERIALNUMBER = 0;
	public static final int COL_USERNAME = 1;
	public static final int COL_TASK= 2;
	public static final int COL_COMMENTS= 3;
	public static final int COL_TASKDESC= 4;
	public static final int COL_STATUS = 5;
	public static final int COL_RECEIVEDON = 6;
	public static final int COL_PROCESSEDON = 7;
	public static final int COL_ROUTENAME = 8;
	public static final int COL_SIGNTYPE = 9;
	public static final int COL_USERSTATUS = 10;

	 protected int m_sortCol = 0;
	 protected boolean m_sortAsc = true;
	
	protected VWRouteSummaryTable m_parent;
	protected Vector m_vector;
	
	public VWRouteSummaryTableData(VWRouteSummaryTable parent) {
		m_parent = parent;
		m_vector = new Vector();
	}
//	--------------------------------------------------------------------------
	
	
	public void setData(List data) {
		m_vector.removeAllElements();
		if (data==null) return;
		int count =data.size();
		for(int i=0;i<count;i++) {
			//VWClient.printToConsole("setData........."+new RouteSummaryRowData((String)data.get(i)));
			m_vector.addElement(new RouteSummaryRowData((String)data.get(i)));
		}
	}
//	--------------------------------------------------------------------------
	public void setData(String[][] data) {
		m_vector.removeAllElements();
		int count =data.length;
		for(int i=0;i<count;i++) {
			RouteSummaryRowData row =new RouteSummaryRowData(data[i]);
			if(i==2)
				m_vector.addElement(new Boolean(true));
			else
				m_vector.addElement(row);
		}
	}
//	--------------------------------------------------------------------------
	public String[][] getData() {
		int count=getRowCount();
		String[][] data=new String[count][VWCConstants.RouteColNames.length];
		for(int i=0;i<count;i++) {
			RouteSummaryRowData row=(RouteSummaryRowData)m_vector.elementAt(i);
			data[i][0]=row.m_SerailNumber;
			data[i][1]=row.m_UserName;
			data[i][2]=row.m_Task;
			data[i][3]=row.m_Comments;
			data[i][4]=row.m_TaskDesc;
			data[i][5]=row.m_Status;
			data[i][6]=row.m_ReceivedOn;
			data[i][7]=row.m_ProcessedOn;			
			data[i][8]=row.m_RouteName;
			data[i][9]=row.m_SignType;
			data[i][10]=row.m_UserStatus;
			
		}
		return data;
	}
//	--------------------------------------------------------------------------
	public String[] getRowData(int rowNum) {
		String[] RouteSummaryRowData=new String[VWCConstants.RouteColNames.length];
		RouteSummaryRowData row=(RouteSummaryRowData)m_vector.elementAt(rowNum);
		RouteSummaryRowData[0]=row.m_SerailNumber;
		RouteSummaryRowData[1]=row.m_UserName;
		RouteSummaryRowData[2]=row.m_RouteName;

		RouteSummaryRowData[3]=row.m_Task;
		RouteSummaryRowData[4]=row.m_TaskDesc;

		
		RouteSummaryRowData[5]=row.m_ReceivedOn;
		RouteSummaryRowData[6]=row.m_ProcessedOn;
		RouteSummaryRowData[7]=row.m_Status;
		RouteSummaryRowData[8]=row.m_Comments;
		RouteSummaryRowData[9]=row.m_SignType;
		RouteSummaryRowData[10]=row.m_UserStatus;
		
		return RouteSummaryRowData;
	}
	
	public RouteSummaryRowData getRowData0(int rowNum) {
		RouteSummaryRowData row=(RouteSummaryRowData)m_vector.elementAt(rowNum);
		return row;
	}
	
//	--------------------------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size();
	}
//	--------------------------------------------------------------------------
	public int getColumnCount() {
		return m_columns.length;
	}
//	--------------------------------------------------------------------------
	public String getColumnName(int column) {
		String str = m_columns[column].m_title;
        if (column==m_sortCol)
            str += m_sortAsc ? " �" : " �";
        return str;
	}
//	--------------------------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		return true;
	}
//	--------------------------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";
		RouteSummaryRowData row = (RouteSummaryRowData)m_vector.elementAt(nRow);
		
		switch (nCol) {
		case COL_SERIALNUMBER: return (row.m_SerailNumber.trim().equals("-"))?"":row.m_SerailNumber;
		case COL_USERNAME:	 return (row.m_UserName.trim().equals("-"))?"":row.m_UserName;
		case COL_ROUTENAME:	 return (row.m_RouteName.trim().equals("-"))?"":row.m_RouteName;
		case COL_RECEIVEDON:     return (row.m_ReceivedOn.trim().equals("-"))?"":row.m_ReceivedOn;
		case COL_PROCESSEDON:     return (row.m_ProcessedOn.trim().equals("-"))?"":row.m_ProcessedOn;
		case COL_STATUS:    return new IconData(getStatusIcon((row.m_Status.trim().equals("-"))?"":row.m_Status), (row.m_Status.trim().equals("-"))?"":row.m_Status);
		case COL_COMMENTS:    return (row.m_Comments.trim().equals("-"))?"":row.m_Comments;
		case COL_TASK:  
		    boolean flag = (row.m_Task.trim().equals("-") || row.m_Task.trim().length() == 0);
		    if (flag)
			return new IconData(null, " ");
		    else {
		    	//VWClient.printToConsole("Images.task...."+Images.task);
		    	return  new IconData(Images.task, row.m_Task.trim());
		    }
		    
		case COL_TASKDESC:    return (row.m_TaskDesc.trim().equals("-"))?"":row.m_TaskDesc;
		case COL_SIGNTYPE:    return (row.m_SignType.trim().equals("-"))?"":row.m_SignType;
		case COL_USERSTATUS: 	return (row.m_UserStatus.trim().equals("-"))?"":row.m_UserStatus;
		}
		return "";
	}
	
	public ImageIcon getStatusIcon(String status){
	    if (status.equalsIgnoreCase("originator"))
		return Images.originator;
	    else if(status.equalsIgnoreCase("pending"))
		return Images.pending;	
	    else if (status.equalsIgnoreCase("Rejected, Task completed") || status.equalsIgnoreCase("Rejected"))		
		return Images.rejected;
	    else if (status.equalsIgnoreCase("Approved, Task completed") || status.equalsIgnoreCase("Approved"))
		return Images.approved;
	    return null;
	}
//	--------------------------------------------------------------------------
	public void setValueAt(Object value, int nRow, int nCol) {
		if (nRow < 0 || nRow >= getRowCount())
			return;
		RouteSummaryRowData row = (RouteSummaryRowData)m_vector.elementAt(nRow);
		String svalue = value.toString().trim().equals("-")?" ":value.toString();
		
		switch (nCol) {
		case COL_SERIALNUMBER:
			row.m_SerailNumber = svalue;
			break;
		case COL_USERNAME:
			row.m_UserName = svalue;
			break;
		case COL_ROUTENAME:
			row.m_RouteName = svalue;
			break;			
		case COL_RECEIVEDON:
			row.m_ReceivedOn = svalue;
			break;
		case COL_PROCESSEDON:
			row.m_ProcessedOn = svalue;
			break;                
		case COL_STATUS:
			row.m_Status = svalue;
			break;
		case COL_COMMENTS :
			row.m_Comments= svalue;
			break;
		case COL_TASK :
			row.m_Task = svalue;
			break;
		case COL_TASKDESC :
			row.m_TaskDesc = svalue;
			break;
		case COL_SIGNTYPE:
			row.m_SignType = svalue;
			break;
		case COL_USERSTATUS:
			row.m_UserStatus = svalue;
			
		}
	}
//	--------------------------------------------------------------------------
	public void clear() {
		m_vector.removeAllElements();
	}
//	--------------------------------------------------------------------------
	public void insert(RouteSummaryRowData AdvanceSearchRowData) {
		m_vector.addElement(AdvanceSearchRowData);
	}
//	--------------------------------------------------------------------------
	public boolean remove(int row){
		if (row < 0 || row >= m_vector.size())
			return false;
		m_vector.remove(row);
		return true;
	}
//	--------------------------------------------------------------------------
	
	
	
    class ColumnListener extends MouseAdapter {
        protected VWRouteSummaryTable m_table;
    //--------------------------------------------------------------------------
        public ColumnListener(VWRouteSummaryTable table){
            m_table = table;
        }
    //--------------------------------------------------------------------------
        public void mouseClicked(MouseEvent e){
        	if(e.getModifiers()==e.BUTTON1_MASK){
                sortCol(e);
            }
        }
        
    //--------------------------------------------------------------------------
        private void sortCol(MouseEvent e) {    	 
            TableColumnModel colModel = m_table.getColumnModel();
            int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
            int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
            
            if (modelIndex < 0) return;
            if (m_sortCol==modelIndex)
                m_sortAsc = !m_sortAsc;
            else
                m_sortCol = modelIndex;
            for (int i=0; i < colModel.getColumnCount();i++) {
                TableColumn column = colModel.getColumn(i);
                column.setHeaderValue(getColumnName(column.getModelIndex()));
            }
            m_table.getTableHeader().repaint();
            Collections.sort(m_vector, new DocRouteSummaryComparator(modelIndex, m_sortAsc));
            m_table.tableChanged(
            new TableModelEvent(VWRouteSummaryTableData.this));
            m_table.repaint();
        }
   }
    
    
    
    class DocRouteSummaryComparator implements Comparator {
        protected int     m_sortCol;
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public DocRouteSummaryComparator(int sortCol, boolean sortAsc) {
            m_sortCol = sortCol;
            m_sortAsc = sortAsc;
        }
    //--------------------------------------------------------------------------
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof RouteSummaryRowData) || !(o2 instanceof RouteSummaryRowData))
                return 0;
            RouteSummaryRowData s1 = (RouteSummaryRowData)o1;
            RouteSummaryRowData s2 = (RouteSummaryRowData)o2;
            int result = 0;
            Integer serialNo1, serialNo2;
           
            double d1, d2;
            switch (m_sortCol) {          
            	case COL_SERIALNUMBER:
            		serialNo1 = Integer.valueOf(s1.m_SerailNumber);
            		serialNo2 = Integer.valueOf(s2.m_SerailNumber);
            		//result = s1.m_SerailNumber.toLowerCase().compareTo(s2.m_SerailNumber.toLowerCase());
            		result = serialNo1.compareTo(serialNo2);
            		break;
            	case COL_USERNAME:
                    result = s1.m_UserName.toLowerCase().compareTo(s2.m_UserName.toLowerCase());
                    break;
                case COL_ROUTENAME:
                   result = s1.m_RouteName.toLowerCase().compareTo(s2.m_RouteName.toLowerCase());
                   break;  
                case COL_TASK:
                    result = s1.m_Task.toLowerCase().compareTo(s2.m_Task.toLowerCase());
                    break;  
                case COL_RECEIVEDON:
                    result = s1.m_ReceivedOn.toLowerCase().compareTo(s2.m_ReceivedOn.toLowerCase());
                    break;  
                case COL_PROCESSEDON:
                    result = s1.m_ProcessedOn.toLowerCase().compareTo(s2.m_ProcessedOn.toLowerCase());
                    break;  
                case COL_STATUS:
                    result = s1.m_Status.toLowerCase().compareTo(s2.m_Status.toLowerCase());
                    break;  
                case COL_COMMENTS:
                    result = s1.m_Comments.toLowerCase().compareTo(s2.m_Comments.toLowerCase());
                    break;  
                case COL_SIGNTYPE:
                    result = s1.m_SignType.toLowerCase().compareTo(s2.m_SignType.toLowerCase());
                    break;                      
                case COL_USERSTATUS:
                    result = s1.m_UserStatus.toLowerCase().compareTo(s2.m_UserStatus.toLowerCase());
                    break;
           }
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof DocRouteSummaryComparator) {
            	DocRouteSummaryComparator compObj = (DocRouteSummaryComparator)obj;
                return (compObj.m_sortCol==m_sortCol) &&
                (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    } 
    
    
}
class VWTableCellRenderer extends DefaultTableCellRenderer
{	
	//private List<List<Integer>> rowColHeight = new ArrayList<>();
	public Component getTableCellRendererComponent(JTable table,Object value,
        boolean isSelected,boolean hasFocus,int row,int column) 
    {	
		if(row%2==0)
            setBackground(new Color(15658734));
        else {
	    	if(!VWRouteSummary.isWebClient)
	    		setBackground(Color.white);
        }
		if (value instanceof IconData){
			IconData data = (IconData) value;
		    setText(data.m_data.toString());
		    if (data.m_data != null && data.m_data.toString().length() > 0 ) {
		    	setIcon(data.m_icon);
		    }
		    setVerticalAlignment(SwingConstants.TOP);
		}
		return super.getTableCellRendererComponent(table,value,isSelected,false,row,column);
    }
}

/**
 * @author Nishad Nambiar
 * Desc   :Added Classes WrappableCellRenderer and WrappableTextEditor for allowing user to scroll the cell value for comments.
 * Issue  :Comments is unreadable in the summary dialog,
 * 		   We should put an additional scrollable multiline label field in the summary dialog to display the comments.	
 * Author :Nishad Nambiar	   
 * Date   :24-Jul-2007
 *
 */
/****CV2019 merges from SIDBI line--------------------------------***/
class WrappableCellRenderer extends JTextArea implements TableCellRenderer {
	private List<List<Integer>> rowColHeight = new ArrayList<>();
    public WrappableCellRenderer() {
    	setOpaque(true);
        setLineWrap(true);
        setWrapStyleWord(true);
   }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    	if (column == 0)
    		SummaryRowHeight.maxRowHeight = 20;
    	int rowHeight = SummaryRowHeight.maxRowHeight;
    	if (isSelected) {
            setForeground(table.getSelectionForeground());
            setBackground(table.getSelectionBackground());
        } else {    
        	setForeground(table.getForeground());
        	if(row%2==0) {
                setBackground(new Color(15658734));
        	} else {
                setBackground(table.getBackground());
            }
            
        }
    	setFont(table.getFont());
    	//VWClient.printToConsole("row :"+row );
        if (hasFocus) {
            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
            if (table.isCellEditable(row, column)) {
                setForeground(UIManager.getColor("Table.focusCellForeground"));
                setBackground(UIManager.getColor("Table.focusCellBackground"));
            }
        } else {
        	//VWClient.printToConsole("inside nofocus border....");
        	setBorder(new EmptyBorder(1, 3, 1, 3));
        	table.setShowHorizontalLines(false);
            table.setShowVerticalLines(false);
        }
 
        if (value != null) {
        	//VWClient.printToConsole("row :"+value.toString() );
        	if (value instanceof IconData){
    			IconData data = (IconData) value;
    		    setText(data.m_data.toString());
    		    if (data.m_data != null && data.m_data.toString().length() > 0 ) {
    		    	JLabel jLabel = new JLabel();
    		    	//JLabel jLabel = (JLabel) getTableCellRendererComponent(table,value,isSelected,hasFocus,row,column);
    		    	//VWClient.printToConsole("Image location :"+new ImageIcon((data.m_icon).toString()));
    		    	jLabel.setIcon(new ImageIcon((data.m_icon).toString()));
    		    }
    		} else {
    			setText(value.toString());
    		}
        	//if (value != null) VWClient.printToConsole("value in WrappableCellRenderer....."+value.toString());
        } else {
            setText("");
        }
        try {
        	//VWClient.printToConsole("------------------------------------------------------------------");
        	//VWClient.printToConsole("Wrapped value :"+value.toString());
	        FontMetrics fm = getFontMetrics(this.getFont());
	        int fontHeight = fm.getHeight() + table.getRowMargin();
	        int textLength = fm.stringWidth(getText());  // length in pixels
	        //VWClient.printToConsole("textLength :"+textLength);
	        int colWidth = table.getColumnModel().getColumn(column).getWidth();
	        //VWClient.printToConsole("colWidth :"+colWidth);
	        int lines = textLength / colWidth + 3; // +1, because we need at least 1 row.
	        //VWClient.printToConsole("lines :"+lines);
	        int height = fontHeight * lines;
	        //VWClient.printToConsole("height :"+height);
	        if (column == 0 && height <= 20) {
	        	height = 20; // This is for by default all the rows height will be 15
	        }
	       /* VWClient.printToConsole("height......."+height);
	        VWClient.printToConsole("row......."+row);
	        VWClient.printToConsole("column......."+column);
	        VWClient.printToConsole("rowHeight......."+rowHeight);*/
	        // ensure the row height fits the cell with most lines
	        
	        if (height > rowHeight) {
	        	//VWClient.printToConsole("Wrapped value  XXX:"+value.toString());
	        	//VWClient.printToConsole("Height :"+height);
	            table.setRowHeight(row, height);
	            rowHeight = height;
	        } else if (column == 0) {
	        	table.setRowHeight(row, height);
	        }
	        //VWClient.printToConsole("after setting stRowHeight....");
	        SummaryRowHeight.maxRowHeight = rowHeight;
	        //VWClient.printToConsole("before adjust row count :::"+table.getRowCount());
	        //VWClient.printToConsole("before adjust col count :::"+table.getColumnCount());
	        //VWClient.printToConsole("before row :::"+row);
	        //VWClient.printToConsole("before column :::"+column);
	        //adjustRowHeight(table, row, column);
	        //VWClient.printToConsole("End of the getTableCellRendererComponent...");
        } catch (Exception e) {
        	//VWClient.printToConsole("Exception in getTableCellRendererComponent ......."+e.getMessage());
        }
        return this;
    }
 
    private void adjustRowHeight(JTable table, int row, int column) {
    	try{
    		int cWidth = table.getTableHeader().getColumnModel().getColumn(column).getWidth();
    		//VWClient.printToConsole("cWidth :::"+cWidth);
    		setSize(new Dimension(cWidth, 1000));
    		int prefH = getPreferredSize().height;
    		while (rowColHeight.size() <= row) {
    			rowColHeight.add(new ArrayList<Integer>(column));
    		}
    		List<Integer> colHeights = rowColHeight.get(row);
    		while (colHeights.size() <= column) {
    			colHeights.add(0);
    		}
    		colHeights.set(column, prefH);
    		int maxH = prefH;
    		for (Integer colHeight : colHeights) {
    			if (colHeight > maxH) {
    				maxH = colHeight;
    			}
    		}
    		if (table.getRowHeight(row) != maxH) {
    			table.setRowHeight(row, maxH);
    		}
    	
    }catch(Exception e){
    	//VWClient.printToConsole("Exception in adjust row height :::"+e.getMessage());
    }
    }
}

class SummaryRowHeight {
	public static int maxRowHeight = 0;
}
/*--------------------------------------------------------------*/
class WrappableTextEditor extends AbstractCellEditor implements TableCellEditor {
	protected JTextArea ta;
	String txt;
	public WrappableTextEditor() {
		ta = new JTextArea();
		ta.setEditable(false);
	}
	//Implement the one CellEditor method that AbstractCellEditor doesn't.
	public Object getCellEditorValue() {
		return txt;
	}
	//Implement the one method defined by TableCellEditor.
	public Component getTableCellEditorComponent(javax.swing.JTable table, Object value, boolean isSelected, int row, int column) {
		txt = value.toString();
		ta.setText(txt);
		ta.setLineWrap(true);
		return new JScrollPane(ta);
	}
}