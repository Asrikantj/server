package com.computhink.vwc.recover;

/*
Copyright (c) 1997-2001
   Computhink Software

All rights reserved.
 */

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.3 $
 * @author      <a href="mailto:pandiyaraj.m@syntaxsoft.com">Pandiya Raj.M</a>
 **/

import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultCellEditor;
import javax.swing.InputMap;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

import com.computhink.common.Constants;
import com.computhink.common.Document;
import com.computhink.common.Index;
import com.computhink.common.Util;
import com.computhink.common.VWDoc;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.doctype.VWUtil.ColoredTableCellRenderer;
import com.computhink.vwc.doctype.VWUtil.ColorData;
import com.computhink.vwc.Session;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.VWTableResizer;

import java.util.Date;
import java.text.Format;
import java.text.SimpleDateFormat;

public class VWDocListTable extends JTable implements Constants
{
	protected VWDocListTableData m_data;
	public static ResourceManager resourceManager=ResourceManager.getDefaultManager();
	public static int COL_COUNT;
	public static int ROW_COUNT;
	JPopupMenu pMenu;
	JMenuItem uploadMenuitem;
	public static int sid;
	public static VWClient vwc;
	public static String roomName;
	public static String serverName;
	int row;
	int column;
	public VWDocListTable(VWClient vwc, int sid, String roomName){
		super();
		this.sid = sid;
		this.vwc = vwc;
		this.roomName = roomName;
		
		Session session = vwc.getSession(sid);
		this.serverName = session.server;
		TableColumn column = null;
		JTextField readOnlyText=new JTextField();
		setFont(readOnlyText.getFont());
//		setBorder(new EtchedBorder());
		setVisible(true);
		m_data = new VWDocListTableData(this);
		setAutoCreateColumnsFromModel(false);
		setCellSelectionEnabled(false);
		setModel(m_data); 
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		readOnlyText.setEditable(false);
		Dimension tableWith = getPreferredScrollableViewportSize();
		COL_COUNT = VWDocListTableData.m_columns.length;
		for (int k = 0; k < VWDocListTableData.m_columns.length; k++) {
			TableCellRenderer renderer;
			DefaultTableCellRenderer textRenderer =new ColoredTableCellRenderer();
			textRenderer.setHorizontalAlignment(VWDocListTableData.m_columns[k].m_alignment);
			renderer = textRenderer;
			JTextField txtField = new JTextField();
			txtField.setEditable(false);
			DefaultCellEditor editor= new DefaultCellEditor(txtField);
			column = new TableColumn(k,Math.round(tableWith.width*VWDocListTableData.m_columns[k].m_width), 
					renderer, editor);
			//column.setPreferredWidth((getPreferredScrollableViewportSize().width / 2)-28);
			addColumn(column);    
		}
		getTableHeader().setReorderingAllowed(false);
		/*        //Set the height and width for setting space between Table Cells.        
int columnWidth = 2;
int columnHeight = 0;
this.setIntercellSpacing(new Dimension(columnWidth, columnHeight));
		 */        setRowHeight(18);
//		 this property will set the value on lost focus of field - earlier user have to clicks on enter key
		 putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
		 JTableHeader header = getTableHeader();        
//		 header.setUpdateTableInRealTime(true);
		 header.addMouseListener(m_data.new ColumnListener(this));
		 setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		 setTableResizable();
		 setSurrendersFocusOnKeystroke(true);
		 InputMap im = getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

//		 Have the enter key work the same as the tab key

		 KeyStroke tab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);
		 KeyStroke shiftTab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB , 1);
		 KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
		 im.put(enter, im.get(tab));

//		 Override the default tab behaviour
//		 Tab to the next editable cell. When no editable cells goto next cell.

		 final Action oldTabAction = getActionMap().get(im.get(tab));
		 final Action oldShiftTabAction = getActionMap().get(im.get(shiftTab));

		 Action tabAction = new AbstractAction()
		 {
			 public void actionPerformed(ActionEvent e)
			 {
				 if (e.getModifiers() == 0)
					 oldTabAction.actionPerformed(e);
				 if (e.getModifiers() ==1)
					 oldShiftTabAction.actionPerformed(e);
				 JTable table = (JTable)e.getSource();               
				 int row = table.getSelectedRow();
				 int column = 1 ;
				 if (e.getModifiers() == 1){
					 --row;
					 if (row <= -1){
						 row = table.getRowCount() - 1;
					 }	
				 } 
				 try{
					 setEditingRow(row);
					 setEditingColumn(column);
					 editCellAt(row, column);
					 table.changeSelection(row, 1, false, false);
				 }catch(Exception ex){
//					 System.out.println("Ex in 170 " + ex.getMessage());
				 }

			 }
		 };
		 getActionMap().put(im.get(tab), tabAction);
		 getActionMap().put(im.get(shiftTab), tabAction);

		 SymMouse aSymMouse = new SymMouse();
		 addMouseListener(aSymMouse);

		 pMenu = new JPopupMenu();
		 uploadMenuitem = new JMenuItem(resourceManager.getString("DocListMenu.Upload"));

		 SysAction aSysAction = new SysAction();
		 uploadMenuitem.addActionListener((ActionListener) aSysAction);
		 pMenu.add(uploadMenuitem);
		 	 
	}
	class SysAction implements ActionListener{
		public void actionPerformed(ActionEvent e) {
			
			Object object = e.getSource();
			if(object == uploadMenuitem)
				upload_actionPerformed(e);
		}

		private void upload_actionPerformed(ActionEvent e) {
			int ret = -1;
			DocListRowData rowData = m_data.getDocument(row);
			Document doc = new Document();
			doc.setId(Integer.parseInt(rowData.m_docId.toString()));
			doc.setName(rowData.m_docName.toString());
			doc.setRoomName(rowData.m_roomName.toString());
			doc.setLocation(rowData.m_location.toString());
			doc.setCreator(rowData.m_user.toString());
			doc.setCreationDate(rowData.m_createdDate.toString());
			java.util.Properties props = System.getProperties();
			doc.setVWDoc(new VWDoc());
			String source = props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + 
			File.separator + doc.getRoomName();
			ret = vwc.setDocFile(sid, doc, source, true, false, null, false, false, true);
			System.out.println("return value from setDocFile() : "+ret);
			if(ret>=0)
				deleteRowData(row);
		}
	}
	class SymMouse extends java.awt.event.MouseAdapter{
		public void mouseClicked(java.awt.event.MouseEvent event)
		{
			Object object = event.getSource();
			if (object instanceof JTable)
				if(event.getModifiers()==event.BUTTON3_MASK)
					VWDocumentListTable_RightMouseClicked(event);

		}
	}

	void VWDocumentListTable_RightMouseClicked(java.awt.event.MouseEvent event)
	{
		Point origin = event.getPoint();
		row = rowAtPoint(origin);
		column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			rSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			rDoubleClick(event,row,column);
	}

	private void rSingleClick(MouseEvent event, int row, int column) {
	    setRowSelectionInterval(row, row);
		pMenu.show(this,event.getX(), event.getY());		
	}


	private void rDoubleClick(MouseEvent event, int row, int column) {
		// TODO Auto-generated method stub

	}
	public void paintComponent(){

	}
	public void setTableResizable() {
//		Resize Table
		new VWTableResizer(this);
	}
//	--------------------------------------------------------------------------
	public void addData(Vector data)
	{
		if (data==null || data.size()==0)
			return ;
		m_data.setData(serverName, data);
		m_data.fireTableDataChanged();
		ROW_COUNT = getRowCount();
	}
//	--------------------------------------------------------------------------
	public Vector getData()
	{
		return m_data.getData();
	}

//	----------------------------------------------------------------------------
	public void clearData()
	{
		m_data.clear();
		m_data.fireTableDataChanged();
	}
//	------------------------------------------------------------------------------
	public int getRowCount()
	{
		if (m_data==null) return 0;
		return m_data.getRowCount();
	}
//	------------------------------------------------------------------------------
	public void deleteRowData(int row){
		m_data.deleteRowData(row);
		m_data.fireTableDataChanged();
	}
}
class DocListRowData
{
	public Object m_docName;
	public Object m_roomName;
	public Object m_serverName;
	public Object m_location;
	public Object m_user;
	public Object m_createdDate;
	public Object m_fileSize;
	public Object m_docId;	

	public DocListRowData() {
		m_docName = "";
		m_roomName = "";
		m_serverName = "";
		m_location = "";
		m_user = "";
		m_createdDate = "";
		m_fileSize = null;
		m_docId = "";
	}
	
	public DocListRowData(String serverName, Document doc){
		long size = 0;
		String docSize = "0" ,subSize =""; //docSize is in terms of MB
		
		m_docName = doc.getName();
		m_roomName = doc.getRoomName();
		m_serverName = serverName;
		m_location = doc.getLocation();
		m_user = doc.getCreator();
		m_createdDate = doc.getCreationDate();

		try{
			size=doc.getVWDoc().getSize();
			if(size>0) {	
				size=size/1024;	//To convert long to KB
				docSize=String.valueOf(size/1024);//To convert long to MB
				subSize=String.valueOf((double)size%1024);
				if(subSize!=null && subSize.length()>2)
					subSize=subSize.substring(0,2);
				subSize=String.valueOf((double)size%1024).substring(0,2);
				String val = docSize+(subSize.equals("")?"":"."+subSize);
				Float floatVal = Float.parseFloat(val);
				float floatValue = Math.round(floatVal);
				int intValue = new Float(floatValue).intValue();
				docSize=String.valueOf(intValue);        					
			}			
		}catch(Exception ex){
			System.out.println("Error while getting the Document Size");
		}
		m_fileSize = docSize; //doc.getVWDoc().getSize();
		m_docId = doc.getId();
	}
}
//--------------------------------------------------------------------------
class DocIndicesColumnData
{
	public String  m_title;
	float m_width;
	int m_alignment;

	public DocIndicesColumnData(String title, float width, int alignment) {
		m_title = title;
		m_width = width;
		m_alignment = alignment;
	}
}
//--------------------------------------------------------------------------
class VWDocListTableData extends AbstractTableModel
{
	public static final DocIndicesColumnData m_columns[] = {
		new DocIndicesColumnData(VWDocListTable.resourceManager.getString("DocListCol.DocName"),0.25F,JLabel.LEFT),
		new DocIndicesColumnData(VWDocListTable.resourceManager.getString("DocListCol.RoomName"),0.2F,JLabel.LEFT),
		new DocIndicesColumnData(VWDocListTable.resourceManager.getString("DocListCol.ServName"),0.2F,JLabel.LEFT),
		new DocIndicesColumnData(VWDocListTable.resourceManager.getString("DocListCol.Location"),0.2F,JLabel.LEFT),
		new DocIndicesColumnData(VWDocListTable.resourceManager.getString("DocListCol.User"),0.2F,JLabel.LEFT),
		new DocIndicesColumnData(VWDocListTable.resourceManager.getString("DocListCol.CreatedDT"),0.25F,JLabel.LEFT),
		new DocIndicesColumnData(VWDocListTable.resourceManager.getString("DocListCol.FileSize"),0.2F,JLabel.LEFT),
		new DocIndicesColumnData(VWDocListTable.resourceManager.getString("DocListCol.DocumentId"),0.2F,JLabel.LEFT)
	};

	public static final int COL_DOCUMENTNAME = 0;
	public static final int COL_ROOMNAME = 1;
	public static final int COL_SERVERNAME = 2;
	public static final int COL_LOCATION = 3;
	public static final int COL_USER = 4;
	public static final int COL_CREATEDDATE = 5;
	public static final int COL_FILESIZE = 6;	
	public static final int COL_DOCUMENTID = 7;

	protected VWDocListTable m_parent;
	protected Vector m_vector;
	protected int m_sortCol = 0;
	protected boolean m_sortAsc = true;  
	public String sortColumn ="";
	public VWDocListTableData(VWDocListTable parent) {
		m_parent = parent;
		m_vector = new Vector();    
	}
//	--------------------------------------------------------------------------
	public void setData(String serverName, Vector data){
		m_vector.removeAllElements();
		if (data==null) return;
		int count =data.size();
		for(int i=0;i<count;i++)
			m_vector.addElement(new DocListRowData(serverName, (Document)data.get(i)));
	}
//	--------------------------------------------------------------------------
	public void insertRow(DocListRowData rowdata) {
		m_vector.addElement(rowdata);
	}
//	--------------------------------------------------------------------------
	public Vector getData() {
		int count=getRowCount();
		Vector data=new Vector();
		for(int i=0;i<count;i++)
		{
			DocListRowData row=(DocListRowData)m_vector.elementAt(i);

		}
		return data;
	}
//	--------------------------------------------------------------------------

	public DocListRowData getDocument(int rowNum) {
		if (rowNum >= m_vector.size()) return null;
		return (DocListRowData)m_vector.elementAt(rowNum);
	}
//	--------------------------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size(); 
	}
//	--------------------------------------------------------------------------
	public int getColumnCount() { 
		return m_columns.length; 
	} 
//	--------------------------------------------------------------------------
	public String getColumnName(int column) { 
		return m_columns[column].m_title; 
	}
//	--------------------------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		if(nCol!=0)
			return true;
		else
			return false;
	}
	
// 	--------------------------------------------------------------------------
	
	public void deleteRowData(int row){
		m_vector.removeElementAt(row);
	}


//	--------------------------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";
		DocListRowData row = (DocListRowData)m_vector.elementAt(nRow);
//		System.out.println(" DT id "+ VWRulesDlgSettings.currentIndices.indexOf(row.m_index.getId()));
		/* if(row.m_value.toString().trim().equalsIgnoreCase("VWBlank:"))
row.m_value = "";*/
		switch (nCol) {

		case COL_DOCUMENTNAME:return row.m_docName;
		case COL_ROOMNAME: return row.m_roomName;
		case COL_SERVERNAME: return row.m_serverName;
		case COL_LOCATION:return row.m_location;
		case COL_USER:return row.m_user; 
		case COL_CREATEDDATE: return row.m_createdDate;
		case COL_FILESIZE: return row.m_fileSize;
		case COL_DOCUMENTID:return row.m_docId; 

		}
		return "";
	}
	public void setValueAt(Object value, int nRow, int nCol) {
		if (nRow < 0 || nRow >= getRowCount())
			return;
		DocListRowData row = (DocListRowData)m_vector.elementAt(nRow);
		String svalue = (value== null)?"":value.toString();

		switch (nCol) {
		case COL_DOCUMENTNAME:
			row.m_docName = svalue;
			break;
		case COL_ROOMNAME:
			row.m_roomName = svalue;
			break;
		case COL_SERVERNAME:
			row.m_serverName = svalue;
			break;
		case COL_LOCATION:
			row.m_location = svalue;
			break;
		case COL_USER:
			row.m_user = svalue;
			break;
		case COL_CREATEDDATE:
			row.m_createdDate = svalue;
			break;
		case COL_FILESIZE:
			row.m_fileSize = svalue;
			break;
		case COL_DOCUMENTID:
			row.m_docId = svalue;
			break;
		}
	}
//	--------------------------------------------------------------------------
	public void clear(){
		m_vector.removeAllElements();
	}
	class ColumnListener extends MouseAdapter
	{
		protected VWDocListTable m_table;      
//		-----------------------------------------------------------
		public ColumnListener(VWDocListTable table){
			m_table = table;
		}
//		-----------------------------------------------------------
		public void mouseClicked(MouseEvent e){

			if(e.getModifiers()==e.BUTTON1_MASK){
				try{
					sortCol(e);
//					System.out.println("mouseClicked for sorting.");
				}catch(Exception ex){
				}
			}
		}    
//		-----------------------------------------------------------
//		Changed to capture which column sorted. 
		private void sortCol(MouseEvent e)
		{
			/*when table cell is in editable mode, click of header the value 
			 * will not be moved with the Column name.Value will be in the same row for the 
			 * other column. 
			 * To avoid the above scenario, stop the cell editing on click of header for sorting. 
			 * In otherwords stop the cell editing while sorting.
			 * --PR-- 
			 */
			try{
				if (m_table.getCellEditor() != null){    			
					m_table.getCellEditor().stopCellEditing();
				}
			}catch(Exception ec){}
			try{
				TableColumnModel colModel = m_table.getColumnModel();
				int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
				int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();

				try{
					sortColumn = getColumnName(columnModelIndex);
				}catch(Exception ex){
//					System.out.println("Exception in sortColumn " + ex.getMessage());
					return;
				}	        
				if (modelIndex < 0)
					return;
				if (m_sortCol==modelIndex)
					m_sortAsc = !m_sortAsc;
				else
					m_sortCol = modelIndex;

				for (int i=0; i < colModel.getColumnCount();i++) {
					TableColumn column = colModel.getColumn(i);
					try{
						column.setHeaderValue(getColumnName(column.getModelIndex()));
					}catch(Exception ex){
//						System.out.println("ex in header " + ex.getMessage());
						return;
					}
				}
				m_table.getTableHeader().repaint();
				Collections.sort(m_vector, new IndexListComparator(modelIndex, m_sortAsc));
				m_table.tableChanged(
						new TableModelEvent(VWDocListTableData.this)); 
				m_table.repaint();
			}catch(Exception ex){
//				System.out.println("Exception in Sorting " + ex.getMessage());
			}
		}

//		--------------------------------------------------------------------------

//		--------------------------------------------------------------------------
	}
	class IndexListComparator implements Comparator
	{
		protected int     m_sortCol;
		protected boolean m_sortAsc;
//		-----------------------------------------------------------
		public IndexListComparator(int sortCol, boolean sortAsc) {
			m_sortCol = sortCol;
			m_sortAsc = sortAsc;
		}
//		-----------------------------------------------------------
		public int compare(Object o1, Object o2) {
			if(!(o1 instanceof DocListRowData) || !(o2 instanceof DocListRowData))
				return 0;
			DocListRowData s1 = (DocListRowData)o1;
			DocListRowData s2 = (DocListRowData)o2;
			int result = 0;       
			switch (m_sortCol) {    
			case COL_DOCUMENTNAME: //0:
				result = s1.m_docName.toString().compareTo(s2.m_docName.toString());
				break;
			case COL_ROOMNAME: //1:
				result = s1.m_roomName.toString().compareTo(s2.m_roomName.toString());
				break;
			case COL_SERVERNAME: //2:
				result = s1.m_serverName.toString().compareTo(s2.m_serverName.toString());
				break;
			case COL_LOCATION: //3:
				result = s1.m_location.toString().compareTo(s2.m_location.toString());
				break;
			case COL_USER: //4:
				result = s1.m_user.toString().compareTo(s2.m_user.toString());
				break;
			case COL_CREATEDDATE: //5:
				result = s1.m_createdDate.toString().compareTo(s2.m_createdDate.toString());
				break;
			case COL_FILESIZE: //6:
				result = s1.m_fileSize.toString().compareTo(s2.m_fileSize.toString());
				break;	
			case COL_DOCUMENTID: //7:
				result = s1.m_docId.toString().compareTo(s2.m_docId.toString());
				break;
			}
			if (!m_sortAsc)
				result = -result;
			return result;
		}
//		-----------------------------------------------------------
		public boolean equals(Object obj) {
			if (obj instanceof IndexListComparator) {
				IndexListComparator compObj = (IndexListComparator)obj;
				return (compObj.m_sortCol==m_sortCol) && 
				(compObj.m_sortAsc==m_sortAsc);
			}
			return false;
		}
	}
}
