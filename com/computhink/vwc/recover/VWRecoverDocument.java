package com.computhink.vwc.recover;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.Collator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.AbstractListModel;
import javax.swing.BorderFactory;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.Position;

import com.computhink.common.Acl;
import com.computhink.common.AclEntry;
import com.computhink.common.Constants;
import com.computhink.common.Document;
import com.computhink.common.Node;
import com.computhink.common.Principal;
import com.computhink.common.Util;
import com.computhink.common.VWAccess;
import com.computhink.common.VWDoc;
import com.computhink.vwc.Security;
import com.computhink.vwc.Session;
import com.computhink.vwc.UIAccess;
import com.computhink.vwc.UIPrincipal;
import com.computhink.vwc.VWCConstants;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWClient;

import com.computhink.vwc.image.Images;
import com.computhink.resource.ResourceManager;

public class VWRecoverDocument implements Constants, VWCConstants, KeyListener, Runnable
{
	JLabel lineBorderLabel =  new JLabel("", JLabel.CENTER);
	JLabel whiteLineBorderLabel =  new JLabel("", JLabel.CENTER);

	private static boolean active = false;
	private WindowAdapter secAdapter;
	//button width, height 

	private static VWClient vwc;
	public static int sid;
	public static String roomName;
	private static JDialog dlgMain;
	private boolean windowActive=true;    
	public boolean sorted = true;    

	public VWDocListTable Table;
	public javax.swing.JScrollPane SPIndicesTable;
	public javax.swing.JPanel PanelTable;
	public static int prevSID = 0;

	public VWRecoverDocument(){
		initComponents();
	}
	//Enhancement for Security - No 32
	public VWRecoverDocument(VWClient vwc, int session, String roomName) 
	{
		if (active)
		{
			if(session == prevSID){
				dlgMain.requestFocus();
				dlgMain.toFront();
				return;
			}
			else{
				closeDialog();
			}				
		}
		new Images();		
		this.vwc = vwc;
		this.sid = session;
		prevSID = session;
		this.roomName = roomName;
		active = true;    
		initComponents();
		loadData(roomName);
	}

	private void loadData(String roomName) {
		String rowData = "";
		String str = "";
		String name = "";
		String value = "";
		Vector<Document> data = new Vector<Document>();

		try {
			java.util.Properties props = System.getProperties();
			String recoverPath = props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + File.separator + roomName;
			File recoverFolder = new File(recoverPath);
			String[] docIds = recoverFolder.list();
			for(int i=0; i<docIds.length; i++){
				String docPropFilePath = recoverPath+File.separator+docIds[i]+File.separator+"Document.properties";
				File docPropFile = new File(docPropFilePath);
				Document doc = new Document();

				if(docPropFile.exists()){
					BufferedReader reader = new BufferedReader(new FileReader(docPropFile));
					while((str = reader.readLine())!=null){
						StringTokenizer st = new StringTokenizer(str,"=");
						name = st.nextToken();

						if(name.equalsIgnoreCase("DocumentId"))
							doc.setId(st.hasMoreTokens()?Integer.parseInt(st.nextToken()):0);
						else if(name.equalsIgnoreCase("DocumentName"))
							doc.setName(st.hasMoreTokens()?st.nextToken():"-");
						else if(name.equalsIgnoreCase("RoomName"))
							doc.setRoomName(st.hasMoreTokens()?st.nextToken():"-");
						else if(name.equalsIgnoreCase("Location"))
							doc.setLocation(st.hasMoreTokens()?st.nextToken():"-");
						else if(name.equalsIgnoreCase("User"))
							doc.setCreator(st.hasMoreTokens()?st.nextToken():"-");
						else if(name.equalsIgnoreCase("CreatedDate"))
							doc.setCreationDate(st.hasMoreTokens()?st.nextToken():"-");						
					}
					reader.close();
				}
				String backupFilePath = recoverPath+File.separator+docIds[i]+File.separator+DOC_CONTAINER;
				File backupFile = new File(backupFilePath);
				long size = 0;
				if(backupFile.exists()){
					size = backupFile.length();
				}
				VWDoc vwDoc = new VWDoc();
				vwDoc.setSize(size);
				doc.setVWDoc(vwDoc);
					
				data.add(doc);
			}
			Table.addData(data);				
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void initControls()
	{
		dlgMain = new JDialog();
		dlgMain.setTitle(VWDocListTable.resourceManager.getDefaultManager().getString("Recover.Title"));
	}
	private void initComponents() 
	{
		initControls();
		Table = new VWDocListTable(vwc, sid, roomName);
		SPIndicesTable = new javax.swing.JScrollPane(Table);
		PanelTable = new javax.swing.JPanel();
		PanelTable.setBounds(11, 48, 414, 292);	        
		PanelTable.setLayout(new BorderLayout());
		//PanelTable.add(JLabel1,BorderLayout.NORTH);
		PanelTable.setBackground(java.awt.Color.white);	        
		PanelTable.add(SPIndicesTable,BorderLayout.CENTER);
		Table.setBackground(Color.white);
		Table.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
	    Table.setColumnSelectionAllowed(false);
	    Table.setRowSelectionAllowed(true);
	    dlgMain.add(PanelTable);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		dlgMain.setSize(560, 416);
		dlgMain.setLocation((screenSize.width-560)/2,(screenSize.height-416)/2);
		dlgMain.setResizable(false);
		dlgMain.setModal(true);
		//------------------------Listeners-------------------------------------

		secAdapter = new WindowAdapter(){
			public void windowClosing(WindowEvent evt){
				closeDialog();
			}
			public void windowLostFocus(WindowEvent e){
				if(windowActive){
					dlgMain.requestFocus();
					dlgMain.toFront();
				}
			}
			public void windowDeactivated(WindowEvent e) {
				if(windowActive){
					dlgMain.requestFocus();
					dlgMain.toFront();
				}
			}
			public void windowStateChanged(WindowEvent e) {
				dlgMain.toFront();
			}
			public void windowDeiconified(WindowEvent e) {
				dlgMain.toFront();
			}
		};
		dlgMain.addWindowListener(secAdapter);
	}
	public void keyPressed(KeyEvent e){
		if (e.getKeyCode() == KeyEvent.VK_ESCAPE) closeDialog();
	}
	public void keyReleased(KeyEvent e){
	}
	public void keyTyped(KeyEvent e){
	}
	private void closeDialog() 
	{
		active = false;
		dlgMain.getOwner().dispose();
	}
	public void run()
	{
		show();
	}
	public void show()
	{
		if (vwc == null) return;
		/*
       	 * new frame.show(); method is replaced with new frame.setVisible(true); 
       	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
       	 */
		dlgMain.setVisible(true);
	}

	public static void main(String[] args) {
		try 
		{
			//String plasticLookandFeel = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			//String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
    		  
			UIManager.setLookAndFeel(plasticLookandFeel);
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			VWClient vwc = new VWClient("c:\\temp");
			//int sid = vwc.login("Syntaxwkt199", "Sta", "admin", "vw");
			VWRecoverDocument security = new VWRecoverDocument(vwc, 433, "VW7");
			new Thread(security).start();	
		}
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}
}