/**
 * 
 */
package com.computhink.vwc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;

import com.computhink.common.Constants;
import com.computhink.common.CustomList;
import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.Client;
import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;

/**
 * @author Nishad Nambiar
 *
 */
public class VWIndicesListDlg implements Runnable, Constants {
	public static JDialog dlgMain;
	public static int sessionID;
	public static int prevSID;
	public static int prevNodeId;
	public static int nodeId;
	public static int mode;
	private JButton btnOk, btnCancel, btnDefault;
	JPanel pnlList, pnlButton;
	VWCheckList list = null;
	public static boolean isWebClient = false;

	SymWindow symWindow = new SymWindow();
	private static ResourceManager connectorManager = ResourceManager.getDefaultManager();
	public VWIndicesListDlg(int mode){
		super();
		active = true;
		initComponents();
		dlgMain.setVisible(true);
		keyBuffer.start();
	}

	public VWIndicesListDlg(VWClient vwc, int sessionID, int mode)
	{
		super();
		this.sessionID = sessionID;
		this.mode = mode;
		if (active)
		{
			if(sessionID == prevSID){
				dlgMain.requestFocus();
				dlgMain.toFront();
				//this.setModal(true);
				return;
			}
			else{
				closeDialog();
			}
		}    	 
		this.vwc = vwc; 
		
		if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
			isWebClient = true;
		
		this.sessionID = sessionID;
		prevSID = sessionID;
		this.docId = docId;
		initComponents();
		dlgMain.setVisible(true);
		vwc.printToConsole("VWRouteList " + session + " ::: " + docId);
		active = true;        
		keyBuffer.start();
	}
	public VWIndicesListDlg(VWClient vwc, int sessionID, int mode, int nodeId)
	{
		super();
		this.nodeId = nodeId;
		this.sessionID = sessionID;
		this.mode = mode;
		if (active)
		{
			if(sessionID == prevSID && nodeId == prevNodeId){
				dlgMain.requestFocus();
				dlgMain.toFront();
				//this.setModal(true);
				return;
			}
			else{
				closeDialog();
			}			
		}    	 
		this.vwc = vwc; 
		if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
			isWebClient = true;

		this.sessionID = sessionID;
		this.docId = docId;
		prevSID = sessionID;
		prevNodeId = nodeId;
		initComponents();
		dlgMain.setVisible(true);
		//vwc.printToConsole("VWRouteList " + session + " ::: " + docId + " NodeId : " + nodeId);
		active = true;        
		keyBuffer.start();
	}


	public void run()
	{
		System.out.println("Printing in Run");
		if (vwc == null) return;
		dlgMain.setVisible(true);
	}
	/*    public void show()
    {
        //if (vwc == null) return;
        setVisible(true);
    }
	 */    


	private class KeyBuffer extends Thread
	{
		private String prefix = "";
		private boolean run = true;
		public KeyBuffer()
		{
			setPriority(Thread.MIN_PRIORITY);
		}
		public void appendKey(char c)
		{
			prefix+=c;
		}
		public void kill()
		{
			run = false;
		}
		public void run()
		{
			while (run)
			{
				try 
				{
					Thread.currentThread().sleep(100);
				}
				catch (InterruptedException ie) 
				{
				}
				if (prefix.length() == 0) continue;
				prefix = "";
			}
		}
	}



	public void initComponents(){
		//this = new JDialog();
		dlgMain = new JDialog();
		btnOk = new JButton();	
		title = new JLabel();
		btnOk.setText(connectorManager.getString("indices.ok"));

		btnCancel = new JButton();
		btnCancel.setText(connectorManager.getString("indices.cancel"));
		pnlList=new JPanel();
		pnlButton=new JPanel();

		btnDefault = new JButton();
		btnDefault.setText(connectorManager.getString("indices.default"));


		list= new VWCheckList();
		Vector vIndices = new Vector();
		vwc.getIndices(this.sessionID,0,vIndices);
		Vector vecIndices = new Vector();

		//Desc   :Implemeted CustomList bean to hold the VWChecklist box values.
		//Author :Nishad Nambiar
		//Date    :10-Dec-2007
		for(int count=0;count<vIndices.size();count++){
			String curIndice = (String) vIndices.get(count);
			StringTokenizer st = new StringTokenizer(curIndice,"\t");
			String id = st.nextToken().toString();
			String name = st.nextToken().toString();
			String type = st.nextToken().toString();
			CustomList customList = new CustomList(Integer.parseInt(id),name, type);
			vecIndices.add(customList);
		}
		Vector vIndicesList = getOnlyIndices(vecIndices);
		list.loadData(vIndicesList);
		dlgMain.getContentPane().setLayout(null);
		dlgMain.setTitle(connectorManager.getString("indices.settitle"));
		//Select the indices you want to display for documents.
		title.setText(connectorManager.getString("indices.settext"));
		dlgMain.getContentPane().add(title);
		title.setVisible(true);
		title.setBounds(15,10,310,20);
		dlgMain.getContentPane().add(list);
		list.setBounds(15,30,310,380);


		dlgMain.getContentPane().add(btnDefault);
		btnDefault.setBounds(15,430,75,22);


		dlgMain.getContentPane().add(btnOk);
		btnOk.setBounds(165, 430, 75,22);

		dlgMain.getContentPane().add(btnCancel);
		btnCancel.setBounds(250, 430, 75,22);
		System.out.println("Printing the JDialog ");

		dlgMain.setSize(350, 495);
		dlgMain.setLocation(350, 100);
		dlgMain.setVisible(true);	
		dlgMain.setResizable(false);
		btnOk.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				saveToRegistry();
				VWCUtil.CustomDialogOk(1);
			}
		});

		btnDefault.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				selectAllDefaults();
			}
		});

		btnCancel.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				closeDialog();
			}
		}); 
		dlgMain.addWindowListener(symWindow);
		list.setItemCheck(0,true);
		list.setItemEnable(0,false);

		loadFromRegistry();

	}
	class SymWindow extends WindowAdapter{
		public void windowLostFocus(WindowEvent e){
			if(active){
				dlgMain.requestFocus();
				dlgMain.toFront();
			}
		}
		public void windowDeactivated(WindowEvent e) {
			if(active){
				dlgMain.requestFocus();
				dlgMain.toFront();
			}
		}
		public void windowStateChanged(WindowEvent e) {
			if(active){	
				dlgMain.toFront();
			}
		}
		public void windowDeiconified(WindowEvent e) {
			if(active){
				dlgMain.toFront();
			}
		}
		public void windowClosing(WindowEvent e) {         
			closeDialog();
		}

	}
	//Desc   :Selects all default items, when user clicks on the Default button.
	//Author :Nishad Nambiar
	//Date   :8-May-2007
	public void selectAllDefaults(){
		int itemCount = list.getItemsCount();

		for(int count=0;count<itemCount;count++){
			if(count<9){
				list.setItemCheck(count,true);
			}
			else{
				list.setItemCheck(count,false);
			}
		}
		list.repaint();


	}

	public static void printToConsole(String msg)
	{
		try
		{
			Preferences clientPref =  null;        	
			boolean displayFlag = false;
			try{
				clientPref =  Preferences.userRoot().node("/computhink/" + PRODUCT_NAME.toLowerCase() + "/VWClient");
				displayFlag  = clientPref.getBoolean("debug",false);
			}catch(Exception ex){
				displayFlag = false;
			}

			if (displayFlag){
				String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
				msg = now + " : " + msg + " \n";    			
				java.util.Properties props = System.getProperties(); 
				String filepath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + File.separator + "VWClient.log";
				File log = new File(filepath);        				                                    
				FileOutputStream fos = new FileOutputStream(log, true);
				fos.write(msg.getBytes());
				fos.close();            
				fos.close();
				log = null;              
			}
		}catch (Exception e)
		{
			System.out.println( "printToConsole");	
		}
	}    

	public void loadListToTable(){
		Vector vecIndices = new Vector();
		//vwc.getIndices(this.sessionID,0,vecIndices);
		vecIndices.insertElementAt("-2"+Util.SepChar+"Document",0);
		vecIndices.insertElementAt("-4"+Util.SepChar+"Type",1);
		vecIndices.insertElementAt("-5"+Util.SepChar+"Creator",2);
		vecIndices.insertElementAt("-6"+Util.SepChar+"Creation Date",3);
		vecIndices.insertElementAt("-7"+Util.SepChar+"Modified Date",4);
		vecIndices.insertElementAt("-10"+Util.SepChar+"Pages",5);
		vecIndices.insertElementAt("-11"+Util.SepChar+"Comments",6);
		vecIndices.insertElementAt("-12"+Util.SepChar+"References",7);
		vecIndices.insertElementAt("-8"+Util.SepChar+"V/R",8);


		for(int count=0;count < vecIndices.size();count++){
			String curIndice = (String)vecIndices.get(count);
			StringTokenizer st = new StringTokenizer(curIndice,"\t");
			String id = st.nextToken();
			String name = st.nextToken();
		}
	}

	public String getIndicesIds(LinkedList list){
		String id="";
		for(int count=0;count<list.size();count++){
			if(count==(list.size()-1) ){
				CustomList customList = (CustomList) list.get(count);
				id += customList.getId();
			}
			else{
				CustomList customList = (CustomList) list.get(count);
				id +=customList.getId()+"\t" ;
			}
		}
		return id;
	}

	public String getIndicesNames(LinkedList list){
		String names="";
		for(int count=0;count<list.size();count++){
			if(count==(list.size()-1) ){
				CustomList customList = (CustomList) list.get(count);
				names += customList.getName();
			}
			else{
				CustomList customList = (CustomList) list.get(count);
				names += customList.getName()+"\t" ;
			}
		}
		return names;
	}

	public String getIndicesTypes(LinkedList list){
		String types="";
		for(int count=0;count<list.size();count++){
			if(count==(list.size()-1) ){
				CustomList customList = (CustomList) list.get(count);
				types += customList.getType();
			}
			else{
				CustomList customList = (CustomList) list.get(count);
				types += customList.getType()+"\t" ;
			}
		}
		return types;
	}	

	private void saveToRegistry(){
		//This piece of code loads indexids and names.
		loadListToTable();
		list.setItemCheck(0,true,0);
		LinkedList selItems = (LinkedList) list.getSelectedItems();
		String indexIds = "";
		String indexNames = "";
		String indexTypes = "";
		indexIds = getIndicesIds(selItems).trim();
		indexNames = getIndicesNames(selItems).trim();
		indexTypes = getIndicesTypes(selItems).trim();
		//printToConsole("IDs "+indexIds);
		//printToConsole("Names "+indexNames);
		//printToConsole("Types "+indexTypes);
		List selectedItems  = new LinkedList();
		selectedItems  = list.getSelectedItems();
		int count=selectedItems.size();
		if(count>33){
			dlgMain.removeWindowListener(symWindow);
			JOptionPane.showMessageDialog(null,connectorManager.getString("indice.jmsg1"),connectorManager.getString("indices.jtitle1"), JOptionPane.OK_OPTION);
			dlgMain.addWindowListener(symWindow);
			return;
		}
		printToConsole(" Count ="+count);
		int ret = vwc.setCustomIndexIdsInRegistry(sessionID, indexIds, indexNames, indexTypes, mode, nodeId);
		Session session = vwc.getSession(this.sessionID);
		String curRoom = session.room;

		dlgMain.removeWindowListener(symWindow);
		//JOptionPane.showMessageDialog(null,"You need to disconnect and connect to "+ curRoom +" inorder for the view to take effect.");

		closeDialog();
	}

	private void loadFromRegistry(){
		Vector indexInfo = new Vector();
		vwc.getCustomIndexIdsFromRegistry(sessionID, mode,  nodeId, indexInfo);
		String indexIds = "";
		String indexNames = "";
		String indexTypes = "";
		if(indexInfo!=null && indexInfo.size()>2){
			indexIds = indexInfo.get(0).toString();
			indexNames = indexInfo.get(1).toString();
			indexTypes = indexInfo.get(2).toString();
		}
		selectItems(indexIds);
	}

	public void selectItems(String indexNames){


		if(indexNames.trim().equals("")){
			selectAllDefaults();
		}

		loadListToTable();
		StringTokenizer stokens=new StringTokenizer(indexNames,"\t");
		while(stokens.hasMoreTokens()){
			try{
				String curToken = stokens.nextToken();
				list.setItemCheck(3,true,Integer.parseInt(curToken));
			}catch(Exception e){

			}
		}
		dlgMain.repaint();
	}


	private void closeDialog() 
	{
		try{
			active = false;
			keyBuffer.kill();
			dlgMain.setVisible(false);
			dlgMain.dispose();
		}catch(Exception ex){
			//System.out.println("Err in close Dlg " + ex.getMessage());
			//ex.printStackTrace();
		}
	}

	public Vector getOnlyIndices(Vector indices){
		Vector vIndices=new Vector();		
		indices.insertElementAt(new CustomList(-2,"Document"),0);
		indices.insertElementAt(new CustomList(-4,"Type"),1);
		indices.insertElementAt(new CustomList(-5,"Creator"),2);
		indices.insertElementAt(new CustomList(-6,"Creation Date"),3);
		indices.insertElementAt(new CustomList(-7,"Modified Date"),4);
		indices.insertElementAt(new CustomList(-10,"Pages"),5);
		indices.insertElementAt(new CustomList(-11,"Comments"),6);
		indices.insertElementAt(new CustomList(-12,"References"),7);
		indices.insertElementAt(new CustomList(-8,"V/R"),8);
		if(indices!=null && indices.size()>0){
			for(int count=0;count < indices.size();count++){
				CustomList curIndice = (CustomList)indices.get(count);
				vIndices.add(new CustomList(curIndice.getId(),curIndice.getName(), curIndice.getType()));					
			}
		}
		return vIndices;
	}

	/*	
	public Vector getOnlyIndicesIds(Vector indices){
		Vector vIndices=new Vector();		
		indices.insertElementAt("-2"+Util.SepChar+"Document",0);
		indices.insertElementAt("-4"+Util.SepChar+"Type",1);
		indices.insertElementAt("-5"+Util.SepChar+"Creator",2);
		indices.insertElementAt("-6"+Util.SepChar+"Creation Date",3);
		indices.insertElementAt("-7"+Util.SepChar+"Modified Date",4);
		indices.insertElementAt("-10"+Util.SepChar+"Pages",5);
		indices.insertElementAt("-11"+Util.SepChar+"Comments",6);
		indices.insertElementAt("-12"+Util.SepChar+"References",7);
		indices.insertElementAt("-8"+Util.SepChar+"V/R",8);


		if(indices!=null && indices.size()>0){
			for(int count=0;count < indices.size();count++){
				CustomList curIndice = (CustomList)indices.get(count);
			}
		}
		return vIndices;
	}
	 */	
	public static void main(String[] args) {
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}catch(Exception ex){}

		new VWIndicesListDlg(1);	
	}
	private static VWClient vwc;
	private int session;
	private int docId;
	private int routeId;
	private static boolean active = false;
	private KeyBuffer keyBuffer = new KeyBuffer();
	//private static JDialog this;
	private static JLabel title;
}
