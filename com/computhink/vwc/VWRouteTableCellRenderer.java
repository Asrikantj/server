package com.computhink.vwc;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.*;
import javax.swing.table.TableCellRenderer;

import com.computhink.vwc.doctype.VWUtil.ColorData;
import com.computhink.vwc.doctype.VWUtil.IconData;

import javax.swing.border.*;
import java.awt.Component;
import java.awt.Color;
import java.awt.Rectangle;

public class VWRouteTableCellRenderer extends DefaultTableCellRenderer
{
    public Component getTableCellRendererComponent(JTable table,Object value,
        boolean isSelected,boolean hasFocus,int row,int column) 
    {
        if(row%2==0)
            setBackground(new Color(15658734));
            ///setBackground(Color.lightGray);
        else
            setBackground(Color.white);
        if (value instanceof IconData) 
        {
        	IconData ivalue = (IconData)value;
          setIcon(ivalue.m_icon);
          setText(ivalue.m_data.toString());
          if (ivalue.foreColor != null )
        	  setForeground(ivalue.foreColor);
        }
        if (value instanceof ColorData) 
        {
        	
        	ColorData ivalue = (ColorData)value;
        	setForeground(ivalue.getFColor());
        	setText(ivalue.toString());
        }        
        return super.getTableCellRendererComponent(table,value,isSelected,false,row,column);
    }
}