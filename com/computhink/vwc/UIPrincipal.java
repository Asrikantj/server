/*
 * UIPrincipal.java
 *
 * Created on March 18, 2004, 11:49 AM
 */
package com.computhink.vwc;
import com.computhink.common.Principal;
import com.computhink.vwc.image.*;
/**
 *
 * @author  Administrator
 */
import javax.swing.JLabel;
//import java.awt.Font;

public class UIPrincipal extends JLabel
{
    private Principal principal;
    
    public UIPrincipal(Principal p)
    {
        principal = p;
        init();
    }
    private void init()
    {
        new Images();
        setName(principal.getName());
        setText(principal.getName());
        if (principal.getType() == (Principal.GROUP_TYPE)) 
            setIcon(Images.grp);
        else
            setIcon(Images.usr);
        //setFont(new Font("Arial", 0, 12)); 
    }
    public Principal getPrincipal()
    {
        return this.principal;
    }
    public String toString()
    {
        return principal.getName();
    }
    
}