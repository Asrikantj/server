package com.computhink.vwc;


public interface VWCheckListListener extends java.util.EventListener {
    public void VWItemChecked(VWItemCheckEvent event);
    public void VWItemUnchecked(VWItemCheckEvent event);
}