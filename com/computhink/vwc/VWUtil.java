/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWUtil<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package com.computhink.vwc;

import javax.swing.JApplet;
import javax.swing.JTable;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.Toolkit;
import java.net.URL;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileFilter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
//import java.net.MalformedURLException;
//import java.awt.Panel;
import java.util.List;
//import java.util.LinkedList;
//import ViewWise.WebClient.VWPanel.VWWebPanel;
//import ViewWise.WebClient.VWUtil.VWStringTokenizer;
//import ViewWise.WebClient.VWConstant;
//import ViewWise.WebClient.VWWeb;
//import java.awt.Panel;
import java.io.*;
import java.util.Date;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class VWUtil// implements VWConstant
{
    public VWUtil(JApplet applet)
    {
        try{
            this.applet=applet;
            DetectBrowserType();
            gAppURL=applet.getCodeBase();
            gDocURL=applet.getDocumentBase();
            ///System.out.println(gDocURL.toString());
            ///System.out.println(gAppURL.toString());
        }
        catch (Exception e){}
    }
//------------------------------------------------------------------------------
    public static int DetectBrowserType()
    {
        String appletContext = applet.getAppletContext().toString();
        if(appletContext.startsWith("sun.applet.AppletViewer"))
            gBrowser_Type = 0;
        else
        if(appletContext.startsWith("netscape.applet."))
            gBrowser_Type = 2;
        else
        if(appletContext.startsWith("com.ms.applet."))
            gBrowser_Type = 1;
        else
        if(appletContext.startsWith("sunw.hotjava.tags.TagAppletPanel"))
            gBrowser_Type = 3;
        else
        if(appletContext.startsWith("sun.plugin.navig.win32.AppletPlugin"))
            gBrowser_Type = 4;
        else
        if(appletContext.startsWith("sun.plugin.ocx.ActiveXApplet"))
            gBrowser_Type = 0;
        return gBrowser_Type;
    }
//------------------------------------------------------------------------------
    public static String GetFixedString(int i, int len)
    {
        String str;
        for(str = String.valueOf(i); str.length() < len; str = str + " ");
        return str;
    }
//------------------------------------------------------------------------------
    public static String GetFixedString(String str, int len)
    {
        for(; str.length() < len; str = str + " ");
        return str;
    }
//------------------------------------------------------------------------------
    public static Dimension getScreenSize()
    {
        Toolkit theKit = applet.getToolkit();
        gScreen_Size = theKit.getScreenSize();
        if(gBrowser_Type == 2)
        {
            gScreen_Size.width = (gScreen_Size.width * 96) / 100;
            gScreen_Size.height = (gScreen_Size.height * 78) / 100;
        }
        return gScreen_Size;
    }
//------------------------------------------------------------------------------
public static URL getCodeBase()
{
    return gAppURL;
}
//------------------------------------------------------------------------------
public static URL getDocBase()
{
    return gDocURL;
}
//------------------------------------------------------------------------------
public static Toolkit getToolkit()
{
    return applet.getToolkit();
}
//------------------------------------------------------------------------------

/*
public static String[][] listTo2DArrOfString(List list)	
{
    if(list==null || list.size()==0) return null;
    int listCount=list.size();
    VWStringTokenizer token=new VWStringTokenizer((String)list.get(0),
        VWConstant.SepChar,false);
    int recordCount=token.countTokens();
    token=null;
    String[][] data=new String[listCount][recordCount];
    for(int i=0;i<listCount;i++)
    {
        VWStringTokenizer tokens=new VWStringTokenizer((String)list.get(i),
            VWConstant.SepChar,false);
        for(int j=0;j<recordCount;j++)
        {
            data[i][j]=tokens.nextToken();
        }
    }
    return data;
  }
 */

//------------------------------------------------------------------------------
  public static String[] listTo1DArrOfString(List list)	
{
    if(list==null || list.size()==0) return null;
    int listCount=list.size();
    String[] data=new String[listCount];
    for(int i=0;i<listCount;i++)
            data[i]=(String)list.get(i);
    return data;
  }
//------------------------------------------------------------------------------
public static JApplet getApplet()
{
    return applet;
}
//------------------------------------------------------------------------------
    public static int to_Number(String str)
    {
        try
        {
            return Integer.parseInt(str);
        }
        catch(Exception e){return 0;}
    }
//------------------------------------------------------------------------------
    public static int getCharCount(String str,String chr)
    {
        int charCount=0;
        int count=str.length();
        for(int i=0;i<count;i++)
        {
            if(str.substring(i,i+1).equalsIgnoreCase(chr)) charCount++;
        }
        return charCount;
    }
//------------------------------------------------------------------------------
    public static String fixNodePath(String nodePath)
    {
        if(nodePath== null || nodePath.equals("") || nodePath.equals(".")) return "";
        String m_Path="";
        VWStringTokenizer pathTokens=new VWStringTokenizer(nodePath,"\\",false);
        String tmp="";
        while (pathTokens.hasMoreTokens())
        {   
            tmp=pathTokens.nextToken();
            m_Path=tmp + "\\" + m_Path;
        }
        return m_Path;
    }
//------------------------------------------------------------------------------
public static int isArrayContaintsString(String str,String[] strArr)
    {
        int count=strArr.length;
        for(int i=0;i<count;i++)
        {
            if(str.indexOf(strArr[i])>=0) 
            {
                return i;
            }
        }
        return -1;
    }    
//------------------------------------------------------------------------------
    public static void deleteDirectory(String directoryName)
    {
        //Log.verboseDebug("Deleting " + directoryName);
        File directoryFile = new File(directoryName);
        // delete files and sub-directories
        File[] files = directoryFile.listFiles();
        int fileCount=0;
        if(files!=null) fileCount=files.length;
        for (int i=0;i<fileCount;i++)
        {
            if (files[i].isDirectory())
            {
                deleteDirectory(files[i].getAbsolutePath());
            }
            else
            {
                //Log.verboseDebug("Deleting "+files[i].getAbsolutePath());
                files[i].delete();
            }
        }
        // delete the directory
        directoryFile.delete();
    }
//------------------------------------------------------------------------------
    public static Dimension gScreen_Size = null;
    public static int gBrowser_Type = 0;
    private static URL gAppURL=null;
    private static URL gDocURL=null;
    private static JApplet applet=null;
/*
    public static boolean writeListToFile(List list,String path)
    {
        int count=list.size();
        try
        { 
            File filePath=new File(path);
            File folderPath=filePath.getParentFile();
            if(!folderPath.canWrite())
               path= VWProperties.getCacheDirPath()+File.separator+filePath.getName();
            FileOutputStream ufos = new FileOutputStream(path);
            OutputStreamWriter osw = new OutputStreamWriter(ufos,"UTF-16");
            for(int i=0;i<count;i++) osw.write((String)list.get(i)+NewLineChar);
            osw.close();
        }
        catch(IOException e)
        {
            return false;
        }
        return true;
    }
  */ 
    
    public static String removeCharFromString(String str,String chr) {
        String retStr="";
        if(str==null || str.equals("")) return "";
        int count=str.length();
        int index=0;
        int chrLen=chr.length();
        while (index<count)
        {
            if(index+chrLen>count)
            {
                return retStr+=str.substring(index,count);
            }
            String tmpStr=str.substring(index,index+chrLen);
            if(!tmpStr.equalsIgnoreCase(chr))
            {
                retStr+=str.substring(index,index+1);
                index++;
            }
            else
            {
                index=index+chrLen;
            }
        }
        return retStr;
    }
    public static String getFormatedDate(String dateStr, String toMask) {
        if(toMask==null || toMask.equals("") || dateStr==null || dateStr=="")
            return dateStr;
        Date date=new Date();
        SimpleDateFormat dateFormat = null;
        try
        {
        // It will give an exception when casting to DateFormat.SHORT. So change the parse date format to "MM/dd/yyyy"
        //date = DateFormat.getDateInstance(DateFormat.SHORT).parse(dateStr);
		// If date format is "yyyyMMdd", convert into "MM/dd/yyyy"            
        	if (dateStr.length() == 8){
        		dateStr = (dateStr.substring(4,6)+"/"+dateStr.substring(6,8)+"/"+dateStr.substring(0,4));
        	}
        	dateFormat= new SimpleDateFormat("MM/dd/yyyy");
    		date= dateFormat.parse(dateStr);        	
        }
        catch (java.text.ParseException e){}
        Format formatter;
        formatter = new SimpleDateFormat(toMask,Locale.ENGLISH);
        String s = formatter.format(date);
        return s;
    }
    //-----------------------------------------------------------
    public static String getFormatedDate(Date date,String mask) {
        if(mask==null || mask.equals("") || date==null)
            return "";
        Format formatter;
        formatter = new SimpleDateFormat(mask,Locale.ENGLISH);
        String s = formatter.format(date);
        return s;
    }
  /*
    public static boolean writeRefFile(String str,String path){
        try {
            File filePath=new File(path);
            File folderPath=filePath.getParentFile();
            if(!folderPath.canWrite())
                path= VWProperties.getCacheDirPath()+File.separator+filePath.getName();
            FileOutputStream ufos = new FileOutputStream(path);
            OutputStreamWriter osw = new OutputStreamWriter(ufos);
            osw.write(str);
            osw.close();
        }
        catch(IOException e) {
            return false;
        }
        return true;
    }
   */ 
     //--------------------------------------------------------------------------
    
    public static String replaceWord(String original, String find, String replacement) 
    {
        int i = original.indexOf(find);
        if (i < 0) {
            return original;  // return original if 'find' is not in it.
        }

        String partBefore = original.substring(0, i);
        String partAfter  = original.substring(i + find.length());

        return partBefore + replacement + partAfter;
    }
    
    
   public static void writeToCSVFile(JTable table){
	   //Desc   :Load the filedialog and save the table contents in the .csv file.
	   //Author :Nishad Nambiar
	   //Date   :23-Mar-2007
	   FileDialog fd = new FileDialog(new Frame(), "Save", FileDialog.SAVE);
       fd.setVisible(true);
       String directory = fd.getDirectory();
       String fileName = fd.getFile();
       String filePath = directory + fileName;
       try{
			PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(filePath)));
			for(int row=0;row<table.getRowCount();row++){
				for(int col=0;col<table.getColumnCount();col++){
					out.write(table.getValueAt(row,col).toString()+",");		
				}
				out.write("\n");
			}
			out.close();
		}catch(Exception e){
			
		}
   }
   //-----------------------------------------------------------
   public static String fixXMLString(String str) {
       String fixedStr="";
       if(str==null || str.trim().equals("")) return fixedStr;
       int charCount=str.length();
       for(int i=0;i<charCount;i++) {
           String tmpChar=str.substring(i,i+1);
           if(tmpChar.equals("<"))         fixedStr=fixedStr+"&lt;";
           else if(tmpChar.equals(">"))    fixedStr=fixedStr+"&gt;";
           else if(tmpChar.equals("\""))   fixedStr=fixedStr+"&quot;";
           else if(tmpChar.equals("'"))    fixedStr=fixedStr+"&apos;";
           else if(tmpChar.equals("&"))    fixedStr=fixedStr+"&amp;";
           //fixedStr=fixedStr+"_";
           else
               fixedStr=fixedStr+tmpChar;
       }
       return fixedStr;
   }
   //-----------------------------------------------------------
   public static String fixXMLEntityString(String str) {
		if(str == null || str.trim().equals("")) return "";
		str = str.replaceAll("&amp;", "&");
		str = str.replaceAll("&apos;", "'");
		str = str.replaceAll("&quot;", "\"");
		str = str.replaceAll("&gt;", ">");
		str = str.replaceAll("&lt;", "<");
		return str;
	}
   //-----------------------------------------------------------
}