/*
 * VWSUtil.java
 *
 * Created on 22 ����� ������, 2003, 03:28 �
 */
package com.computhink.vwc;
import com.computhink.common.*;
import com.computhink.drs.server.DRSLog;

import java.io.*;
import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.io.File;
import java.util.StringTokenizer;

public class VWCUtil 
{
    public static String home = "";
    //Added for issue fix 184
    public static String ErrorDesc = "";
    
    private static native int sendFindResultReadyMsg(long hWnd, Vector results);
    private static native int sendFindDataReadyMsg(long hWnd, int value);
    private static native int sessionDisconnected(int sid, int eid);
    private static native String signCreate(String sign1,String sign2,
              String sign3,String sign4,String sign5,String sign6,String sign7);
    private static native int signValidate(String template, long hwnd);
    private static native int signRender(String filePath, String signStr);
    private static native int signConfirm();
    
    public static native void Add_Begin(String folder);
    public static native void Add_Page(String SourceFile,String DestFolder);
    public static native void Add_End(String SourceFolder);
    public static native void CustomDialogOk(long OkButtonClicked);
    
    public static native int documentCheckOut(String documentName);
    public static Vector DateVector = new Vector ();
    public static LinkedList DateList = new LinkedList();
    public static int DocId = -1;
    
    //-----------------------NETCO SDK---------------
    //  8-10-2005
    private static native void Publish(String pathTo, String xmlFile, 
                           String pathFrom, String svr, String room, int docID);
    private static native void PublishDirect(String pathTo, String xmlFile, 
            String pathFrom, String svr, String room, int docID, int xml, int compress);
    /****CV2019 merges from SIDBI line--------------------------------***/
    private static native void PublishDirectWeb(String pathTo, String xmlFile, 
            String pathFrom, String svr, String room, int docID, int xml, int compress);    
    private static native int ConvertDocToPDFDRS(String DestPath, String AllDotZipPath,int flag);
	//private native static String ConvertDocToPDFDRS(String DestPath, String AllDotZipPath,String fileRange,String Password, int nOverlay, int nWhiteout, int nColor, int nDepth, int orientation);
	private native static void ConverttoPDFandAddWatermarkToPDF(String docId, String docName, String srcPath,
			String xmlfile, String AllDotZipPath, String fileRange, String Password, int nOverlay, int nWhiteout,
			int nColor, int nDepth, int orientation, int hcPdf, String hcPdfProfile, int appendReport, int xresolution,
			int yresolution);
	/****End of SIDBI line--------------------------------***/
	/****CV2019 merges from 10.2 line and changed the return type as void--------------------------------***/
	private native static void ConvertDocToPDFDRS(String DestPath, String AllDotZipPath,String fileRange,String Password, int nOverlay, int nWhiteout, int nColor, int nDepth, int orientation, int hcPDF, String pdfProfile, int qFactor);
	
    private static native int Wrap(String pathTo, String pathFrom, int djvu);
    private native static int DecryptFile(String dtcFileOut, String dtcFileIn);
    private native static int EncryptFile(String dtcFileOut, String dtcFileIn);
    
    static 
    {
        findHome();
    }
    /**
     * loadNativeLibraryForDRS created for convert to pdf from client dll for drs endworkflow implementation.
     * 
    */
    public static boolean loadNativeLibraryForDRS(String libName,String libPath)
    {
    	File libraryFilePath =  null;
    	String serverlibPath = null;
    	boolean retFlag = false;
    	if(libPath == null || libPath.equals("")){
    		
    		libPath = home + Util.pathSep + "system" + Util.pathSep;
    		if (!libPath.endsWith(Util.pathSep)) libPath += Util.pathSep;
    		if(libPath.contains("Contentverse Client")){
				try {
					libraryFilePath = new File(libPath + libName);
					DRSLog.dbg("Is " + libName + " available in " + libPath + " : " + libraryFilePath.exists());
					if (libraryFilePath.exists()) {
						System.load(libPath + libName);
						DRSLog.dbg(libName + "library loaded from " + libPath);
						retFlag = true;
					} else {
						DRSLog.dbg(libName + "library not available in " + libPath);
					}
				} catch (UnsatisfiedLinkError e) {
					DRSLog.dbg("Exception while loading " + libName + " :" + e);
				} 
    		}else if(libPath.contains("Contentverse")){
    			serverlibPath = libPath;
    			libPath=libPath.replace("Contentverse", "Contentverse Client");
				try {
					libraryFilePath = new File(libPath + libName);
					DRSLog.dbg("Is +" + libName + " available in " + libPath + " : " + libraryFilePath.exists());
					if (libraryFilePath.exists()) {
						System.load(libPath + libName);
						DRSLog.dbg(libName + "library loaded from " + libPath);
						retFlag = true;
					} else {
						DRSLog.dbg(libName + "library not available in " + libPath);
						libraryFilePath = new File(serverlibPath + libName);						
						if (libraryFilePath.exists()) {
							System.load(serverlibPath + libName);
							DRSLog.dbg(libName + "library loaded from " + serverlibPath);
							retFlag = true;
						}
					}
				} catch (UnsatisfiedLinkError e) {
					DRSLog.dbg("Exception while loading " + libName + " :" + e);
				} 
    		}
    	}
    	return retFlag;
    }
    
    public static void loadNativeLibrary(String libName)
    {
        loadNativeLibrary(libName, null);
    }
    public static boolean loadNativeLibrary(String libName, String libPath)
    {
        if(libPath == null || libPath.equals(""))
                        libPath = home + Util.pathSep + "system" + Util.pathSep;
        try
        {
            if (!libPath.endsWith(Util.pathSep)) libPath += Util.pathSep;
            File serverlibFilePath=new File(libPath+libName);
            DRSLog.dbg("Is "+libName+" available in "+libPath+" : "+serverlibFilePath.exists());
            if (serverlibFilePath.exists()) {
            	System.load(libPath + libName);
            	DRSLog.dbg(libName +"library loaded from "+libPath);
            } else {
            	DRSLog.dbg(libName +"library not available in "+libPath);
            }
        }
        catch (UnsatisfiedLinkError e)
        {
            return false;
        }
        return true;
    }
    public static String getHome()
    {
        return home;
    }
    private static void findHome()
    {
        String  urlPath = "";
        String  indexerHome = "";
        String res = "com/computhink/vwc/image/images/vwc.gif";
        String jarres = "/lib/viewwise.jar!";
        
        int startIndex = 0;
        int endIndex = 0;
        try
        {
            URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
            urlPath = url.getPath();
        }
        catch(Exception e){urlPath="";}
        if (!urlPath.equalsIgnoreCase(""))
        {
            if (urlPath.startsWith("file"))
            {
                startIndex = 6;
                endIndex = urlPath.length() - res.length() - jarres.length();
            }
            else
            {
                startIndex = 1;
                endIndex = urlPath.length() - res.length();
            }
            indexerHome = urlPath.substring(startIndex, endIndex-1);  
            /* getHome is going wrong in Linux box because of root folder i.e /
            * So checking OS and and (/) root folder is added. 24 Mar 2008
             */
            if(Util.getEnvOSName().indexOf("Linux") == -1)
            		indexerHome = indexerHome.replace('/', '\\');
            else if(Util.getEnvOSName().indexOf("Linux")!=-1 && (!indexerHome.startsWith("/"))){
            	indexerHome = "/"+indexerHome;
            }
        }
        home = cleanUP(indexerHome);
    }
    private static String cleanUP(String str) // removes %20    
    {
       String cleanStr = ""; 
     /*  StringTokenizer st = new StringTokenizer(str, "%20");
       while (st.hasMoreElements())
       {
           cleanStr = cleanStr + st.nextToken() + " ";
       }
    */    
    	
    	/*
        New Issue
        Created by: <Mallikarjuna S C>	Date: <04 Aug 2006>
        jar mismatch error, when the ViewWise Client was installed or executed from a location d:/vw60dtc.
        String Replace function is used instaed of StringTokenizer

        */		
    	cleanStr = str.replace ("%20", " ");
       return (cleanStr.equalsIgnoreCase("")? str : cleanStr.trim());
    }
    public static boolean publish(String pathTo, String xmlFile, 
                           String pathFrom, String svr, String room, int docID)
    {
        try
        {
            Publish(pathTo, xmlFile, pathFrom, svr, room, docID);
        }
        catch(UnsatisfiedLinkError e)
        {
            return false;
        }
        return true;
    }
    // Add the functionality for extract the pages in the native format. 
    public static boolean publishDirect(String pathTo, String xmlFile,
    		String pathFrom, String svr, String room, int docID, int xml, int compress) {
    	try {
    		PublishDirect(pathTo, xmlFile, pathFrom, svr, room, docID, xml, compress);
    	} catch (UnsatisfiedLinkError e) {
    		return false;
    	}
    	return true;
    }
    /**
     * CV2019 merges - SIDBI enhancement
     * @param pathTo
     * @param xmlFile
     * @param pathFrom
     * @param svr
     * @param room
     * @param docID
     * @param xml
     * @param compress
     * @return
     */
    public static boolean PublishDirectFileWeb(String pathTo, String xmlFile,
    		String pathFrom, String svr, String room, int docID, int xml, int compress) {
    	try {
    		PublishDirectWeb(pathTo, xmlFile, pathFrom, svr, room, docID, xml, compress);
    	} catch (UnsatisfiedLinkError e) {
    		return false;
    	}
    	return true;
    }
    
    /**
     * Metod added for convertoppdf for drsmail.
     * @param DestPath
     * @param AllDotZipPath
     * @param range
     * @param Password
     * @param nOverlay
     * @param nWhiteout
     * @param nColor
     * @param nDepth
     * @param orientataion
     * @return
     */
	/****CV2019 merges - convertoPDF issue fixes***/
    public static void convertToPDF(String DestPath, String AllDotZipPath, String range,String Password, int nOverlay, int nWhiteout, int nColor, int nDepth,int orientataion, int hcPDF, String pdfProfile, int qFactor) {
    	try {
    		DRSLog.dbg("Before calling ConvertDocToPDFDRS JNI call ");
    		DRSLog.dbg("Parameters passing to convertToPDF  nOverlay :::: "+nOverlay+"\t nWhiteout :::: "+nWhiteout+"\t nColor :::: "+nColor+"\t nDepth :::: "+nDepth);
    		//DRSLog.add("DestPath:"+DestPath+" AllDotZipPath:"+AllDotZipPath+" range:"+range+" Password:"+Password+" nOverlay:"+nOverlay+" nWhiteout :"+nWhiteout+" nColor :"+nColor+" nDepth :"+nDepth+" orientataion :"+orientataion);
    		ConvertDocToPDFDRS(DestPath,  AllDotZipPath, range, Password,  nOverlay,  nWhiteout, nColor,  nDepth, orientataion, hcPDF, pdfProfile, qFactor);
    		DRSLog.dbg("After calling ConvertDocToPDFDRS JNI call ");
    	} catch (UnsatisfiedLinkError e) {
    		DRSLog.add("Exception inside convertToPDF method UnsatisfiedLinkError:"+e.getMessage());
    	} catch (Exception e) {
    		DRSLog.add("Exception inside convertToPDF method :"+e.getMessage());
    	}
    } 

    /****CV2019 merges - SIDBI WaterMark to PDF enhancement --------------------------------***/
	public static int cvwebConverttoPDFandAddWatermarkToPDF(String docId, String docName, String srcPath, String html,
			String destPath, String fileRange, String pwd, int overLay, int whiteOut, int color, int depth,
			int orientation, int hcPdf, String hcPdfProfile, int appendReport, int xresolution, int yresolution) {
		int ret=0;
		DRSLog.dbg("Native cvwebConverttoPDFandAddWatermarkToPDF pdf path :"+srcPath);
		DRSLog.dbg("Parameters passing to cvwebConverttoPDFandAddWatermarkToPDF  nOverlay :::: "+overLay+"\t nWhiteout :::: "+whiteOut+"\t nColor :::: "+color+"\t nDepth :::: "+depth);
		
		ConverttoPDFandAddWatermarkToPDF(docId, docName, srcPath, html, destPath, fileRange, pwd, overLay, whiteOut,
				color, depth, orientation, hcPdf, hcPdfProfile, appendReport, xresolution, yresolution);
		return ret;
	}
	/****End of SIDBI WaterMark to PDF enhancement --------------------------------***/
    public static int wrap(String pathTo, String pathFrom)
    {
        int ret = -1;
        try
        {
            ret = Wrap(pathTo, pathFrom, 0);
        }
        catch(UnsatisfiedLinkError e)
        {
        }
        return ret;
    }
    public static boolean sendFindDataReady(long hWnd, int value)
    {
        try
        {
            int retVal = sendFindDataReadyMsg(hWnd, value);
        }
        catch(UnsatisfiedLinkError e)
        {
        }
        return true;
        
    }
    public static boolean sendResults(long hWnd, Vector results)
    {
        try
        {
            int retVal = sendFindResultReadyMsg(hWnd, results);
        }
        catch(UnsatisfiedLinkError e)
        {
        }
        return true;
    }
    public static boolean sendDisconnect(int session, int eventId)
    {
        try
        {
            sessionDisconnected(session, eventId);
            return true;
        }
        catch(UnsatisfiedLinkError e)
        {
            return false;
        }
    }
    public static void deleteDirectory(String directoryName)
    {
        if(directoryName.length()<3) return;
        File directoryFile = new File(directoryName);
        File[] files = directoryFile.listFiles();
        int fileCount=0;
        if(files!=null) fileCount=files.length;
        for (int i=0;i<fileCount;i++)
        {
            if (files[i].isDirectory())
            {
                deleteDirectory(files[i].getAbsolutePath());
            }
            else
            {
                files[i].delete();
            }
        }
        directoryFile.delete();
    }
    public static boolean writeListToFile(Vector data, String path, String error)
    {
        return writeListToFile(data, path, error, "UTF-8");
    }
    public static boolean writeListToFile(Vector data, String path, String error, String format)
    {
        int count=data.size();
        error = "";
        try
        {
            File filePath = new File(path);
            filePath.getParentFile().mkdirs();
            if(filePath.exists())
            {
            	String fileNameWithoutExt=path.substring(0,path.lastIndexOf("."));
            	String ext=path.substring(path.lastIndexOf(".")+1,path.length());
                filePath.renameTo(new File(fileNameWithoutExt+"_"+Util.getNow(1)+"."+ext));
            }
            FileOutputStream ufos = new FileOutputStream(path);
            OutputStreamWriter osw = new OutputStreamWriter(ufos, format);
            for(int i=0;i<count;i++) osw.write((String)data.get(i)+Constants.NewLineChar);
            osw.close();
        }
        catch(IOException e)
        {
        	error=e.getLocalizedMessage();
        	//Added for issue fix 184
        	ErrorDesc = e.getLocalizedMessage();
        	return false;
        }
        return true;
    }
    
    public static boolean writeArrayListToFile(ArrayList<String> data, String path, String error, String format)
    {
        int count=data.size();
        error = "";
        try
        {
            File filePath = new File(path);
            filePath.getParentFile().mkdirs();
            if(filePath.exists())
            {
            	String fileNameWithoutExt=path.substring(0,path.lastIndexOf("."));
            	String ext=path.substring(path.lastIndexOf(".")+1,path.length());
                filePath.renameTo(new File(fileNameWithoutExt+"_"+Util.getNow(1)+"."+ext));
            }
            FileOutputStream ufos = new FileOutputStream(path);
            OutputStreamWriter osw = new OutputStreamWriter(ufos, format);
            for(int i=0;i<count;i++) osw.write((String)data.get(i)+Constants.NewLineChar);
            osw.close();
        }
        catch(IOException e)
        {
        	error=e.getLocalizedMessage();
        	//Added for issue fix 184
        	ErrorDesc = e.getLocalizedMessage();
        	return false;
        }
        return true;
    }
    
    public static void appendFile(Vector data, String path)
    {
    try { 
        BufferedWriter out = new BufferedWriter(new FileWriter(path, true)); 
        for(int i=0;i<data.size();i++) out.write((String)data.get(i)+Constants.NewLineChar);
        out.close(); 
    } catch (IOException e) { 
    } 
    }

    public static String fixXMLString(String str)
    {
    	String fixedStr="";
        if(str==null || str.trim().equals("")) return fixedStr;
        fixedStr=str.replaceAll("&", "&amp;");
        fixedStr=fixedStr.replaceAll("<", "&lt;");
        fixedStr=fixedStr.replaceAll(">", "&gt;");
        fixedStr=fixedStr.replaceAll("\"", "&quot;");
        fixedStr=fixedStr.replaceAll("'", "&apos;");
        fixedStr=fixedStr.replaceAll("&", "&amp;");
        
        /*int charCount=str.length();
         for(int i=0;i<charCount;i++)
        {
            String tmpChar=str.substring(i,i+1);
            if(tmpChar.equals("<"))         fixedStr=fixedStr+"&lt;";
            else if(tmpChar.equals(">"))    fixedStr=fixedStr+"&gt;";
            else if(tmpChar.equals("\""))   fixedStr=fixedStr+"&quot;";
            else if(tmpChar.equals("'"))    fixedStr=fixedStr+"&apos;";
            else if(tmpChar.equals("&"))    fixedStr=fixedStr+"&amp;";
            //fixedStr=fixedStr+"_";
            else 
                fixedStr=fixedStr+tmpChar;
        }*/
        return fixedStr;
    }
    public static boolean copyDirectory(File sourceFolder, File destFolder)
    {
        if (!sourceFolder.exists() || !sourceFolder.canRead())    return false;
        
        destFolder.mkdirs();
        if (!destFolder.exists() || !destFolder.canWrite())    return false;
        
        File[] contents = sourceFolder.listFiles();
        int count=contents.length;
        for(int i=0;i<count;i++)
        {
            File sourceFile=contents[i];
            File destFile=new File(destFolder,sourceFile.getName());
            if (sourceFile.isDirectory())
            {
                if (!copyDirectory(sourceFile, destFile))
                    return false;
            }
            else
            {
                if (!copyFile(sourceFile, destFile))
                    return false;
            }
        }
        return true;
    }
    public static boolean copyFile(File fromFile,File toFile)
    {   
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        try
        {
            in = new BufferedInputStream(new FileInputStream(fromFile));
            out = new BufferedOutputStream(new FileOutputStream(toFile));
            int bufferSize = Math.min((int)fromFile.length(), 1024000);
            byte[] buffer = new byte[bufferSize];
            int numRead;
            while ((numRead = in.read(buffer, 0, buffer.length)) != -1)
            {
                out.write(buffer, 0, numRead);
            }
            out.flush();
            in.close();
        }
        catch (IOException e)
        {
            return false;
        }
        finally
        {
            try
            {
                if (in != null) in.close();
                if (out != null) out.close();
            }
            catch (IOException e)
            {
            }
        }
        return true;
    }
    public static String GetFixedString(int i, int len)
    {
        String str;
        for(str = String.valueOf(i); str.length() < len; str = str + " ");
        return str;
    }
    public static String GetFixedString(String str, int len)
    {
        if(str==null) str="";
        for(; str.length() < len; str = str + " ");
        return str;
    }   
    public static boolean CopyDirectory(File sourceFolder, File destFolder)
    {
        if (!sourceFolder.exists() || !sourceFolder.canRead())    return false;
        
        destFolder.mkdirs();
        if (!destFolder.exists() || !destFolder.canWrite())    return false;
        
        File[] contents = sourceFolder.listFiles();
        int count=contents.length;
        for(int i=0;i<count;i++)
        {
            File sourceFile=contents[i];
            File destFile=new File(destFolder,sourceFile.getName());
            if (sourceFile.isDirectory())
            {
                if (!CopyDirectory(sourceFile, destFile))
                    return false;
            }
            else
            {
                if (!CopyFile(sourceFile, destFile))
                    return false;
            }
        }
        return true;
    }
    public static boolean CopyFile(File fromFile,File toFile)
    {   
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        try
        {
            in = new BufferedInputStream(new FileInputStream(fromFile));
            out = new BufferedOutputStream(new FileOutputStream(toFile));
            int bufferSize = Math.min((int)fromFile.length(), 1024000);
            byte[] buffer = new byte[bufferSize];
            int numRead;
            while ((numRead = in.read(buffer, 0, buffer.length)) != -1)
            {
                out.write(buffer, 0, numRead);
            }
            out.flush();
            in.close();
        }
        catch (IOException e)
        {
            return false;
        }
        finally
        {
            try
            {
                if (in != null) in.close();
                if (out != null) out.close();
            }
            catch (IOException e)
            {
            }
        }
        return true;
    }
    public static String createSign(Vector signs)
    {
        if(signs !=null && signs.size()>=7)
        {
            return signCreate((String)signs.get(0), (String)signs.get(1), 
            (String)signs.get(2), (String)signs.get(3),
            (String)signs.get(4), (String)signs.get(5),
            (String)signs.get(6));
        }
        return "";
    }
    public static int validateSign(String template, long hwnd)
    {
        return signValidate(template, hwnd);
    }
    public static int signatureConfirm()
    {
        return signConfirm();
    }
    public static int createSignImage(String filePath, String signStr)
    {
        return signRender(filePath, signStr); 
    }
    public static String getStrFromFile(File filePath) 
    {
        String str="";
        String str1="";
        try {
            BufferedReader rdr = new BufferedReader(
            new InputStreamReader(new FileInputStream(filePath.getPath())/*, "UTF-16"*/));
            while ((str1 = rdr.readLine()) != null) {
                str += str1;
            }
            rdr.close();
            return str;
        }
        catch(IOException e) {
            return "";
        }
    }
    
//  Changes made by Mallikarjuna, to read XML file and extrace Created-Date and Modified Date
	public static void parseXML (String FilePath)
	{
		try {
			javax.xml.parsers.DocumentBuilderFactory docBuilderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();	
			javax.xml.parsers.DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			org.w3c.dom.Document doc = docBuilder.parse (new File(FilePath));
			
			org.w3c.dom.NodeList CreaModiDate = doc.getElementsByTagName("Creation-Date");
			String CreatedDate = CreaModiDate.item (0).getTextContent();
			
			CreaModiDate = doc.getElementsByTagName("Modified-Date");
			String ModifiedDate = CreaModiDate.item (0).getTextContent();
			
			Vector oTempVector = new Vector (); 
			oTempVector.add (CreatedDate);
			oTempVector.add (ModifiedDate);
			DateVector.add (oTempVector);
			/*
			 * LinkedList is used for Bug-206 fix, as vector is a synchronized it will
			 * override the values earlier, every time reinitialized
			 * Gurumurthy.T.S 22=01-2014
			 */
			DateList.add(oTempVector);
			}catch (org.xml.sax.SAXParseException sAXe) {
//				op.showMessageDialog (null,"VWCUtil : parseXML SAXParseException " +sAXe.getMessage() , " VWCUtil : parseXML SAXParseException " , javax.swing.JOptionPane.INFORMATION_MESSAGE);				
			}catch (org.xml.sax.SAXException se) {
//				op.showMessageDialog (null,"VWCUtil : parseXML SAXException "+ se.getMessage(), " VWCUtil : parseXML SAXException " , javax.swing.JOptionPane.INFORMATION_MESSAGE);
			}catch (Exception e) {
//				op.showMessageDialog (null,"VWCUtil : parseXML Exception "+ e.getMessage(), " VWCUtil : parseXML Exception " , javax.swing.JOptionPane.INFORMATION_MESSAGE);
		}
	}
	/**
	 * @return Returns the docId.
	 */
	public static int getDocId() {
		return DocId;
	}
	
	public static void setDocId(int docId) {
		DocId = docId;
	}
	
	
	public static String fixFileName(String name)
	{
		//'/',':','*','?','"','<','>','|'and also new line, tab, carriage return.
		//sFileName < 2309 && sFileName > 2361 is for hindi charaters
		if(name!=null && name.length() > 0){
			char[] fChars = name.toCharArray();
			for(int i=0;i<fChars.length;i++)
			{
				short sFileName = (short)fChars[i];
				if(!Character.isDefined(fChars[i])) {
					fChars[i]='_' ;
				}else if( fChars[i] =='/' || fChars[i] ==':' || fChars[i] =='\\'
					|| fChars[i] == '*' || fChars[i]=='?' || fChars[i]=='"' || fChars[i]=='<'
						|| fChars[i]=='>' || fChars[i]=='|' || fChars[i]=='\n' || fChars[i]=='\r'
							|| fChars[i]=='\t' || fChars[i] ==';' || (sFileName >=147 && (sFileName < 2309 && sFileName > 2361))){
					fChars[i]='_' ;           
				}

			}
			return new String(fChars);
		}else{
			return "VWReport";
		}

	} 
	
	//Writing metadata values for routing publish method
	static OutputStreamWriter osw = null;
	
	public static boolean initFile(String path,String error)
    {
        try
        {
            File filePath = new File(path);
            filePath.getParentFile().mkdirs();
            if(filePath.exists())
            {
                filePath.renameTo(new File(path+Util.getNow(1)));
            }
            FileOutputStream ufos = new FileOutputStream(path);
            osw = new OutputStreamWriter(ufos);
        }
        catch(IOException e)
        {
            error=e.getLocalizedMessage();
            return false;
        }
        return true;
    }
	public static int getDocumentMetaData(int sid, VWClient vwClient, Document doc)
	{

		Session session=vwClient.getSession(sid);
	    Hashtable indicesInfo=new Hashtable();
		// Comment : Read the encoding type from server registry . UTF-8 is default encoding type.
		
		String encoding = vwClient.getEncoding(sid);
		
		Vector docs=new Vector();
        vwClient.getDocumentInfo(sid,doc,docs);
        doc=(Document)docs.get(0);	
		
		writeToFile("<?xml version='1.0' encoding='"+ encoding +"'?>");
		writeToFile("<VWDocument id='"+doc.getId()+"' >");
		
		writeToFile("<Room>"+VWCUtil.fixXMLString(session.room)+"</Room>");
	    writeToFile("<Name>"+VWCUtil.fixXMLString(doc.getName())+"</Name>");
	    writeToFile("<Path>"+VWCUtil.fixXMLString(getDocPath(sid,vwClient,doc.getId()))+"</Path>");
	    writeToFile("<PathIDs>"+VWCUtil.fixXMLString(getDocPathIDs(sid,vwClient,doc.getId()))+"</PathIDs>");
	    writeToFile("<Creator>"+fixXMLString(doc.getCreator())+"</Creator>");
	    writeToFile("<Creation-Date>"+fixXMLString(doc.getCreationDate())+"</Creation-Date>");
	    writeToFile("<Modified-Date>"+fixXMLString(doc.getModifyDate())+"</Modified-Date>");
	    writeToFile("<PageCount>"+String.valueOf(doc.getPageCount())+"</PageCount>");
		writeToFile("<DocType>");

		Vector docTypes=new Vector();
		DocType docType=doc.getDocType();
		vwClient.getDocType(sid, docType, docTypes);
		if(docTypes!=null && docTypes.size()>0)
		{
			docType=(DocType)docTypes.get(0);
			writeToFile("<Name>"+fixXMLString(docType.getName())+"</Name>");
			writeToFile("<EnableSIC>"+docType.getEnableTextSearch()+"</EnableSIC>");
			writeToFile("<AutoMail_To>"+fixXMLString(docType.getTo())+"</AutoMail_To>");
			writeToFile("<AutoMail_Cc>"+fixXMLString(docType.getCc())+"</AutoMail_Cc>");
			writeToFile("<AutoMail_Subject>"+fixXMLString(docType.getSubject())+"</AutoMail_Subject>");
			getDocTypeVerRev(sid, vwClient, docType);
			getDocTypeIndices(sid, vwClient, docType);
		}
		writeToFile("</DocType>");
		getDocIndices(sid, vwClient, doc);
		writeToFile("</VWDocument>");
		return 0;
	}

	private static String getDocPath(int sid,VWClient vwClient,int nodeId)
	{
	    Vector nodes=new Vector();
	    vwClient.getNodeParents(sid, new Node(nodeId), nodes);
	    String docPath="";
	    if(nodes==null||nodes.size()==0) return docPath;
	    int count =nodes.size();
	    for(int i =0;i<count;i++)   docPath=((Node)nodes.get(i)).getName()+Util.SepChar+docPath;
	    return docPath.substring(0,docPath.length()-1);
	}

	private static String getDocPathIDs(int sid,VWClient vwClient,int nodeId)
	{
	    String docPath="";
	    try
	    {
	        Vector nodes=new Vector();
	        vwClient.getNodeParents(sid, new Node(nodeId), nodes);
	        if(nodes==null || nodes.size()==0)  return docPath;
	        int count=nodes.size();
	        docPath="";
	        for(int i=0;i<count;i++)
	        {
	            Node node=(Node)nodes.get(i);
	            if(node.getId()!=0) docPath=String.valueOf(node.getId())
	                +Util.SepChar+docPath;
	        }
	        docPath=docPath.substring(0,docPath.length()-1);
	    }
	    catch(Exception e){
	        docPath="";}
	    return docPath;
	}
	
	private static void getDocIndices(int sid,VWClient vwClient, Document doc)
	{
		writeToFile("<Indices>");
		doc.setVersion("");
		vwClient.getDocumentIndices(sid, doc);
		if(doc.getDocType().getIndices()!=null && doc.getDocType().getIndices().size()>0)
		{
			Vector docIndices=doc.getDocType().getIndices();
			writeToFile("<Count>"+docIndices.size()+"</Count>");
			for(int i=0;i<docIndices.size();i++)
			{  
				Index index=(Index)docIndices.get(i);
				writeToFile("<DocIndex id='"+(i+1)+"' >");
				writeToFile("<Name id='"+(i+1)+"'>"+fixXMLString(index.getName())+"</Name>");
				writeToFile("<Value id='"+(i+1)+"'>"+fixXMLString(index.getValue())+"</Value>");
				try{
					writeToFile("<ActValue id='"+(i+1)+"'>"+fixXMLString(index.getActValue())+"</ActValue>");
				}
				catch(Exception e1){writeToFile("<ActValue id='"+(i+1)+"'></ActValue>");};
				writeToFile("</DocIndex>");
			}
		}
		writeToFile("</Indices>");
	}
	private static void getDocTypeVerRev(int sid, VWClient vwClient, DocType docType)
	{
		Vector result=new Vector();
		vwClient.getDTVersionSettings(sid, docType);
		if(docType.getVREnable().equalsIgnoreCase("1"))
		{
			writeToFile("<VREnable>"+docType.getVREnable()+"</VREnable>");
			writeToFile("<VRInitVersion>"+docType.getVRInitVersion()+"</VRInitVersion>");
			writeToFile("<VRInitRevision>"+docType.getVRInitRevision()+"</VRInitRevision>");
			writeToFile("<VRPagesChange>"+docType.getVRPagesChange()+"</VRPagesChange>");
			writeToFile("<VRIncPagesChange>"+docType.getVRIncPagesChange()+"</VRIncPagesChange>");
			writeToFile("<VRPageChange>"+docType.getVRPageChange()+"</VRPageChange>");
			writeToFile("<VRIncPageChange>"+docType.getVRIncPageChange()+"</VRIncPageChange>");
			writeToFile("<VRPropertiesChange>"+docType.getVRPropertiesChange()+"</VRPropertiesChange>");
			writeToFile("<VRIncPropertiesChange>"+docType.getVRIncPropertiesChange()+"</VRIncPropertiesChange>");
		}
	}
	private static void getDocTypeIndices(int sid, VWClient vwClient, DocType docType)
	{
		vwClient.getDocTypeIndices(sid,docType);
		int indicesCount=docType.getIndices().size();
		writeToFile("<IndicesCount>"+indicesCount+"</IndicesCount>");
		for(int i=1;i<=indicesCount;i++)
		{
			writeToFile("<Index id='"+(i)+"' >");
			Index index=(Index)docType.getIndices().get(i-1);
			writeToFile("<NewMask id='"+(i)+"'>"+fixXMLString(index.getMask())+"</NewMask>");
			writeToFile("<NewDefault id='"+(i)+"'>"+fixXMLString((String)index.getDef().get(0))+"</NewDefault>");
			writeToFile("<Required id='"+(i)+"'>"+index.isRequired()+"</Required>");
			writeToFile("<IndexOrder id='"+(i)+"'>"+i+"</IndexOrder>");
			writeToFile("<NewInfo id='"+(i)+"'>"+fixXMLString(index.getInfo())+"</NewInfo>");
			writeToFile("<NewDescription id='"+(i)+"'>"+fixXMLString(index.getDescription())+"</NewDescription>");
			writeToFile("<DTKey id='"+(i)+"'>"+index.isKey()+"</DTKey>");

			index=getIndex(sid, vwClient, index);

			writeToFile("<Name id='"+(i)+"'>"+fixXMLString(index.getName())+"</Name>");
			writeToFile("<Type id='"+(i)+"'>"+index.getType()+"</Type>");
			writeToFile("<Mask id='"+(i)+"'>"+fixXMLString(index.getMask())+"</Mask>");
			writeToFile("<Default id='"+(i)+"'>"+fixXMLString((String)index.getDef().get(0))+"</Default>");
			writeToFile("<SVid id='"+(i)+"'>"+index.getSystem()+"</SVid>");
			writeToFile("<Info id='"+(i)+"'>"+fixXMLString(index.getInfo())+"</Info>");
			writeToFile("<Description id='"+(i)+"'>"+fixXMLString(index.getDescription())+"</Description>");
			if(index.getType()==4) /*Selection Type*/
			{
				String strValues="";
				for(int sel=0;sel<index.getDef().size()-1;sel++)
					strValues+=((String)index.getDef().get(sel)).trim() + "|";
				strValues+=((String)index.getDef().get(index.getDef().size()-1)).trim();

				writeToFile("<SelectionValues id='"+(i)+"'>"+fixXMLString(strValues)+"</SelectionValues>");
			}
			writeToFile("</Index>");
		}
	}
	private static Index getIndex(int sid, VWClient vwClient, Index index)
    {
        Vector result=new Vector();
        vwClient.getIndices(sid, index.getId(), result);
        if(result==null || result.size()==0) return index;
        StringTokenizer st = new StringTokenizer((String)result.get(0), "\t");
        try
        {
            st.nextToken();
            index.setName(st.nextToken());
            index.setType(Util.to_Number(st.nextToken()));
            index.setMask (st.nextToken());
            index.setDef(st.nextToken());
            st.nextToken();
            index.setCounter(Util.to_Number(st.nextToken()));
            st.nextToken();
            index.setSystem(Util.to_Number(st.nextToken()));
            index.setInfo(st.nextToken());
            index.setDescription(st.nextToken());
        }
        catch (Exception e){}
        return index;
    }
	public static boolean closeFile()
	{
		try
		{
			osw.close();
		}
		catch(IOException e)
		{
			return false;
		}
		return true;
	}
	public static boolean writeToFile(String data)
	{
		try
		{
			osw.write(data+Constants.NewLineChar);
		}
		catch(IOException e)
		{
			return false;
		}
		return true;
	}
	
	public static int sendDecryptedFileForDllEncryption(String dtcFileOut, String dtcFileIn) {
		int ret = 0;
    	try {
    		ret = EncryptFile(dtcFileOut, dtcFileIn);
    	} catch (UnsatisfiedLinkError e) {
    		ret = -1;
    	}
    	return ret;
	}
	
	public static int sendEncryptedFileForDllDecryption(String dtcFileOut, String dtcFileIn) {
		int ret = 0;
    	try {    		
    		ret = DecryptFile(dtcFileOut, dtcFileIn);
    	} catch (UnsatisfiedLinkError e) {
    		ret = -1;
    	}
    	return ret;
	}
	
	public static ArrayList getPdfProfileValues() {
		ArrayList<String> values = new ArrayList<String>();
		values.add("scan100");
		values.add("scan150");
		values.add("scan200");
		values.add("scan300");
		values.add("scan400");
		values.add("scan500");
		values.add("scan600");
		values.add("photo100");
		values.add("photo150");
		values.add("photo200");
		values.add("photo300");
		values.add("photo400");
		values.add("photo500");
		values.add("photo600");		
		return values;
	}
	
	public static void main(String[] args) {
		/*File clientlibFilePath=new File("D:\\tmp\\system\\VWView.dll");
		if(clientlibFilePath.exists()){
			System.out.println("Library exist.....");
			loadNativeLibraryForDRS("VWView.dll","D:\\tmp\\system");
			System.out.println("Dll loaded.....");
			convertToPDF("D:\\tmp\\1.pdf", "D:\\tmp\\New folder\\all.zip", "","", 0, 0, 1, 24,1);
		}
		String fileout = "D:\\CheckOut\\1.tif";
		String filein = "D:\\CheckOut\\1.tmp";*/
		//sendEncryptedFileForCheckInDoc(fileout, filein);
	
	
		/*VWCUtil vwUtil = new VWCUtil();
		String libName = "VWView.dll";
		String libPath = "D:\\tmp\\System\\";
		System.out.println("before calling loadNativeLibraryForDRS");
		home = "D:\\tmp\\";
		vwUtil.loadNativeLibraryForDRS(libName, "");
		vwUtil.convertToPDF("D:\\tmp\\1workflow11_manual workflow.pdf", "D:\\tmp\\New folder\\all.zip", "","", 0, 0, 1, 24,1);
		System.out.println("after dlll method....");*/
	}

}