package com.computhink.vwc.image;
import javax.swing.ImageIcon;
import javax.swing.Icon;

public class Images
{
    public static ImageIcon usr;
    public static ImageIcon grp;
    public static ImageIcon grn;
    public static ImageIcon dny;
    public static ImageIcon idl;
    public static ImageIcon vwc;
    public static ImageIcon war;
    public static ImageIcon dgrn;
    public static ImageIcon ddny;
    public static ImageIcon newentry;
    public static ImageIcon reset;
    public static ImageIcon help;
    
    public static ImageIcon task;
    public static ImageIcon originator;
    public static ImageIcon pending;    
    public static ImageIcon approved;
    public static ImageIcon rejected;
    
    public static ImageIcon taskDetails;
    public static ImageIcon print;
    public static ImageIcon taskClose;
    public static ImageIcon export;
    
    public static ImageIcon connectIcon=null;
    public static ImageIcon closeIcon=null;
    
    public static ImageIcon infoIcon=null;
    
    public Images()
    {
        try
        {
            vwc = new ImageIcon(getClass().getResource("images/vwc.gif"));
            usr = new ImageIcon(getClass().getResource("images/usr.gif"));
            grp = new ImageIcon(getClass().getResource("images/grp.gif"));
            grn = new ImageIcon(getClass().getResource("images/grn.png"));
            dny = new ImageIcon(getClass().getResource("images/dny.png"));
            idl = new ImageIcon(getClass().getResource("images/idl.png"));
            dgrn = new ImageIcon(getClass().getResource("images/Dgrn.png"));
            ddny = new ImageIcon(getClass().getResource("images/Ddny.png"));
            newentry = new ImageIcon(getClass().getResource("images/newentry.png"));
            reset = new ImageIcon(getClass().getResource("images/reset.png"));
            help = new ImageIcon(getClass().getResource("images/Help.png"));
            task = new ImageIcon(getClass().getResource("images/Task.png"));
            pending = new ImageIcon(getClass().getResource("images/TaskPending.png"));
            originator = new ImageIcon(getClass().getResource("images/Originator.png"));
            approved = new ImageIcon(getClass().getResource("images/Approved.png"));
            rejected = new ImageIcon(getClass().getResource("images/Rejected.png"));
            taskDetails = new ImageIcon(getClass().getResource("images/TaskDetails.png"));
            print = new ImageIcon(getClass().getResource("images/Print.png"));
            taskClose = new ImageIcon(getClass().getResource("images/WindowClose.png"));
            export = new ImageIcon(getClass().getResource("images/ExportHTML.png"));
            //war = new ImageIcon(getClass().getResource("images/war.gif"));
            connectIcon= new ImageIcon(getClass().getResource("images/Connect.gif"));
            closeIcon=new ImageIcon(getClass().getResource("images/Close.gif"));
            infoIcon=new ImageIcon(getClass().getResource("images/Info.png"));
        }
        catch (Exception e)
        {
        }
    }
}
