/*
 * @(#)VWClient.java	5.60 06/12/04
 *
 * Copyright 2004 Computhink, Inc. All rights reserved.
 * COMPUTHINK PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.computhink.vwc;
import com.computhink.vwc.notification.VWDocNotification;
import com.computhink.vwc.notification.VWFolderNotification;
import com.computhink.vwc.recover.VWRecoverDocument;
import com.computhink.vws.server.*;
import com.computhink.dss.server.*;
import com.computhink.mdss.*;
import com.computhink.ars.server.ARSPreferences;
import com.computhink.common.*;
/**
 *
 * @author  Administrator
 */
import java.net.*;
import java.util.*;
import java.io.*;
import java.rmi.RemoteException;
import javax.swing.*;
/**
 * 
 */
public class VWAdmin extends VWClient
{   
    public VWAdmin(String workFolder)
    {
        super("0B9B5177-1011-4BBF-B617-455288043D4A", workFolder, Client.Adm_Client_Type);
    }
    public int deleteDocType(int sid, DocType docType)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            retVal=vws.deleteDocType(session.room, sid, docType);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    public int deleteIndex(int sid, Index index)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            retVal=vws.deleteIndex(session.room, sid, index);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    public int deleteDTIndex(int sid, Index index)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            retVal=vws.deleteDTIndex(session.room, sid, index);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    
    public int getRecycleDocList(int sid, int rowCount, int lastRowId, int actionType, Vector docs)
    {
	return getRecycleDocList(sid, rowCount, lastRowId, actionType, "", docs);
    }
     
	
    public int getRecycleDocList(int sid, int rowCount, int lastRowId, int actionType, String indexValue, Vector docs)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            Vector ret=vws.getRecycleDocList(session.room, sid, rowCount, lastRowId, actionType, indexValue, docs);
            docs.addAll(ret);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    public int restoreNodeFromRecycle(int sid, Node node)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.restoreNodeFromRecycle(session.room, sid, node);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getUserRedactions(int sid, String userName,Vector docs)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            Vector ret=vws.getUserRedactions(session.room, sid, userName, docs);
            docs.addAll(ret);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    public int setUserRedactions(int sid, String userName,
        String oldPassword, String newPassword, Vector docs)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            retVal=vws.setUserRedactions(session.room, sid, userName, 
                encryptStr(oldPassword), encryptStr(newPassword), docs);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
   
    public int getATSettings(int sid, Vector ATSettings)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            Vector ret=vws.getATSettings(session.room, sid);
            ATSettings.addAll(ret);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    public int setATSetting(int sid, int eventId, int status)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            retVal=vws.setATSetting(session.room, sid, eventId, status);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    
    public int detATData(int sid, Vector filter,String archivePath)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            retVal=vws.detATData(session.room, sid, filter, archivePath);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    
    
    public int getDocTypeInfo(int sid, int docTypeId, Vector docTypeInfo)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = 
                   vws.getDocTypeInfo(session.room, sid, docTypeId, docTypeInfo);
            docTypeInfo.addAll(ret);
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int resyncDirectory(int sid)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            vws.resyncDirectory(session.room, sid);
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }
    }    
    public int getLockDocList(int sid, Vector docs)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            Vector ret=vws.getLockDocList(session.room, sid, docs);
            docs.addAll(ret);
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    
    public int getRoomStatistics(int sid, Vector info)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getRoomStatistics(session.room, sid, info);
            info.addAll(ret);
            return NoError; 
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    
    public int getConnectedClients(int sid, Vector clients)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getConnectedClients(session.room, sid, clients);
            clients.addAll(ret);
            return NoError; 
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    public int isConnectionEnabled(int sid)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.isConnectionEnabled(session.room, sid);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int logoutClient(int sid, int clientsid)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            vws.logoutClient(session.room, sid, clientsid);
            removeSession(clientsid);
            return NoError; 
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    /*
     * Added for Novell Netware issue 736 - MDSS server ping issue
     */
    public int getIdleTime(int sid,String roomName){
    	int idleTime = 0;
    	System.out.println(" sid /// roomName " + sid + " /// " + roomName);
    	Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        
        VWS vws = getServer(session.server);
        if(vws == null) return 0;
        try
        {
        	//System.out.println(" Call the Method ");
            idleTime = vws.getRoomIdleTime(roomName);
        }
        catch(Exception e)
        {
            return Error;
        }
        
    	return idleTime;
    }
    /*
     * Added for Novell Netware issue 736 - MDSS server ping issue
     */
    public int isRoomIdle(int sid,String roomName){

    	int isIdle = connectionaAvailable;
    	Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        
        VWS vws = getServer(session.server);
        if(vws == null) return destinationRoomServerInactive;
        try
        {
        	//System.out.println(" Call the Method ");
            isIdle = vws.isRoomIdle(sid,roomName);
        }
        catch(Exception e)
        {
            return invalidSessionId;
        }            	
    	return isIdle;
    }
    
    /**
     * getAvailableRetention - is to get the availabe retentions for the logged in room
     * @param sid
     * @param availRetention
     * @return
     */
    // Yet to get the stored procedure for this - All the retentions for the room level
    public int getAvailableRetention(int sid, int docTypeId, Vector availRetention)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = 
                   vws.getAvailableRetentions(session.room, sid, availRetention);
            availRetention.addAll(ret);
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /**
     * getRetentionSettings - This is to get the retention settings for selected retention id 
     * @param sid
     * @param docTypeId
     * @param availRetention
     * @return
     */
    public int getRetentionSettings(int sid, int retentionId, Vector retentionSettings)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = 
                   vws.getRetentionSettings(session.room, sid, retentionId, retentionSettings);
            //System.out.println("Size of return value of vector is : "+ret.size()); 
            retentionSettings.addAll(ret);
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /**
     * getRetentionIndexInfo - getting the index info for the selected retention id
     * @param sid
     * @param retentionId
     * @param retentionIndexInfo
     * @return
     */
    public int getRetentionIndexInfo(int sid, int retentionId, Vector retentionIndexInfo)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = 
                   vws.getRetentionIndexInfo(session.room, sid, retentionId, retentionIndexInfo);
            retentionIndexInfo.addAll(ret);
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /**
     * setRetentionSettings - used to set the retention settings
     */
    public int setRetentionSettings(int sid, VWRetention retention, Vector retentionId)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.setRetentionSettings(session.room, sid, retention, retentionId);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    
    public int checkCVSearchNode(int sid,String nodePath)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.checkCVSearchNode(session.room, sid,nodePath);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /**
     * setIndexConditions - This method is used to set the indices condition for the documen type
     * @param sid
     * @param retention
     * @return
     */
    public int setIndexConditions(int sid, VWRetention retention)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.setIndexConditions(session.room, sid, retention);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
	public int delRetentionSettings(int sid, VWRetention retention, int deleteOption) {
		Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.delRetentionSettings(session.room, sid, retention, deleteOption);
        }
        catch(Exception e)
        {
            return Error;
        }
	}
	
	public int delIndexInfo(int sid, VWRetention retention) {
		Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.delIndexInfo(session.room, sid, retention);
        }
        catch(Exception e)
        {
            return Error;
        }
	}
	public int checkRetentionInUse(int sid, int retId, Vector retention) {
		Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.checkRetentionInUse(session.room, sid, retId, retention);
        }
        catch(Exception e)
        {
            return Error;
        }
	}
	/**
	 * isRetentionNameFound - It is used to check for the duplication of the retentionName
	 * @param sid
	 * @param retentionName
	 * @return
	 */
	public int isRetentionNameFound(int sid, String retentionName){
		Session session = getSession(sid);
	    if(session==null) return invalidSessionId;
	    VWS vws = getServer(session.server);
	    if (vws == null) return ServerNotFound;
	    int retentionId =0;
	    try{        	
	    	retentionId = vws.isRetentionNameFound(session.room, sid, retentionName);        	
	        //printToConsole("isRetentionNameFound " + retentionId);        	
	    }catch(Exception ex){}
		return retentionId;
	}
	public void delIndexConditions(int id, VWRetention retentionBean) {
		// TODO Auto-generated method stub
		
	}
	
	public int getRetentionCompletedDocs(int sid, int retentionId, Vector retentionSettings)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = 
                   vws.getRetentionCompletedDocs(session.room, sid, retentionId, retentionSettings);
            retentionSettings.addAll(ret);
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
	public int getRetentionDocumentList(int sid, int retentionId, Vector retentionDocs)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getRetentionDocumentList(session.room, sid, retentionId, retentionDocs);
            retentionDocs.addAll(ret);
            return vws.getActiveError();
        }
        catch(Exception e)
        {
            return Error;
        }
    }
	public int delEmptyUsers(int sid) {
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try
		{
			int result = vws.deleteEmptyUsers(session.room);
			return result;

		}catch(Exception e)
		{
			return Error;
		} 
	}
	
	public int getPurgeDocs(int sid, Vector purgeDocsFlag){
		
		Session session = getSession(sid);
 		if(session==null) return invalidSessionId;
 		VWS vws = getServer(session.server);
 		if (vws == null) return ServerNotFound;
 		try
 		{
 			boolean flag = vws.getPurgeDocs(session.room, sid, purgeDocsFlag);
 			if(flag)
 				purgeDocsFlag.add("true");
 			else
 				purgeDocsFlag.add("false");
            return vws.getActiveError();
 		}
 		catch(Exception e)
 		{
 			return Error;
 		}
	}
	
	public int getKeepDocType(int sid, Vector keepdoctypeFlag){
		
		Session session = getSession(sid);
 		if(session==null) return invalidSessionId;
 		VWS vws = getServer(session.server);
 		if (vws == null) return ServerNotFound;
 		try
 		{
 			boolean flag = vws.getKeepDocType(session.room, sid, keepdoctypeFlag);
 			if(flag)
 				keepdoctypeFlag.add("true");
 			else
 				keepdoctypeFlag.add("false");
            return vws.getActiveError();
 		}
 		catch(Exception e)
 		{
 			return Error;
 		}
	}
	
	public int setPurgeDocs(int sid, String purgeDocsFlag){
		
		Session session = getSession(sid);
 		if(session==null) return invalidSessionId;
 		VWS vws = getServer(session.server);
 		if (vws == null) return ServerNotFound;
 		try
 		{
 			int  ret = vws.setPurgeDocs(session.room, sid, purgeDocsFlag);
            return vws.getActiveError();
 		}
 		catch(Exception e)
 		{
 			return Error;
 		}
	}
	
	public int setKeepDocType(int sid, String keepDocTypeFlag) {
		Session session = getSession(sid);
 		if(session==null) return invalidSessionId;
 		VWS vws = getServer(session.server);
 		if (vws == null) return ServerNotFound;
 		try
 		{
 			int  ret = vws.setKeepDocType(session.room, sid, keepDocTypeFlag);
            return vws.getActiveError();
 		}
 		catch(Exception e)
 		{
 			return Error;
 		}
	}

	public int getARSServer(int sid){
		
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try
		{
			int  ret = vws.getARSServer(session.room, sid);
			return ret;
		}
		catch(Exception e)
		{
			return Error;
		}
	}
	
	public int getTreeDetails(int sid, Vector treeDetails){
		Session session = getSession(sid);
		if(session == null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if(vws==null) return ServerNotFound;

		try{
			Vector treeView = new Vector();
			treeView = vws.getTreeDetails(session.room, sid);				
			treeDetails.addAll(treeView);    		
		}catch(Exception ex){
			return Error;
		}
		return NoError;
	}
	/*public static void openRecoverDocuments(VWClient vwc, int sid, String roomName) {
		String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
		try {
			UIManager.setLookAndFeel(plasticLookandFeel);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		VWRecoverDocument recover = new VWRecoverDocument(vwc, sid, roomName);
		new Thread(recover).start();	

	}*/

	/*
	 * 
	 * 
	 * Notification Service Methods
	 * 
	 */

	/*public int openNotificationSettings(int sid, Node node)
	{
		try 
		{
			String windowsLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(windowsLookandFeel);
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e) 
		{}

		VWFolderNotification vwFolderNotification = new VWFolderNotification(this, sid, node);
		new Thread(vwFolderNotification).start();
		return NoError;
	}*/
/*	public int openNotificationSettings(int sid, Document doc)
	{
		try 
		{
			String windowsLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(windowsLookandFeel);
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}
		catch(Exception e) 
		{}

		VWDocNotification vwDocNotification = new VWDocNotification(this, sid, doc);
		new Thread(vwDocNotification).start();	
		return NoError;
	}*/


	public int saveNotificationSettings(int sid, NotificationSettings settings) {
		Session session = getSession(sid);
		if (session == null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try {
			vws.saveNotificationSettings(session.room, sid, settings);
		} catch (Exception ex) {
			return Error;
		}
		return NoError;
	}

	public int loadNotificationSettings(int sid, int nodeId, String userName, String module, NotificationSettings settings) {
		Session session = getSession(sid);
		if (session == null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;

		try {
			int isAdmin = isAdmin(sid);
			Vector output = new Vector();
			output = vws.loadNotificationSettings(session.room, sid, nodeId, isAdmin, userName, module);
			vectorToNotification(output, settings);
		} catch (Exception ex) {
			return Error;
		}
		return NoError;
	}

	private int vectorToNotification(Vector v, NotificationSettings settings)
    {
    	//1     10    1     5     Documents is moved      Document	1     1     1     vwadmin     10.4.8.145  11      srikanth.a@syntaxsoft.com;pandiya.m@syntaxsoft.com
    	//2     10    1     6     Documents is added      Document 	1     1     1     vwadmin     10.4.8.145  11      srikanth.a@syntaxsoft.com;pandiya.m@syntaxsoft.com
    	//1		280	  122	1	1	Any changes to this Document	Document	1	0	0	vwadmin	10.4.8.149	11	vijaykumar.s@syntaxsoft.com;dd	-
    	Vector docs = new Vector();	    	
    	Notification[] notificationList = new Notification[v.size()]; 
        for (int i = 0; i < v.size(); i++)
        {	        	
        	 StringTokenizer st = new StringTokenizer((String) v.elementAt(i), "\t");
        	 st.nextToken();//id
        	 settings.setReferenceId(Util.to_Number(st.nextToken()));//nodeId
        	 settings.setNodeName(st.nextToken());
        	 settings.setNodeType(Util.to_Number(st.nextToken()));
        	 Notification notification = new Notification(Util.to_Number(st.nextToken()), st.nextToken(), st.nextToken());
        	 notificationList[i] = notification;	        	 
        	 settings.setNotifyCaptureType(Util.to_Number(st.nextToken()));
        	 settings.setProcessType(Util.to_Number(st.nextToken()));
        	 settings.setFolderInherit(Util.to_Number(st.nextToken()));
        	 settings.setDocumentInherit(Util.to_Number(st.nextToken()));
        	 settings.setUserName( st.nextToken());
        	 settings.setIpAddress( st.nextToken());
        	 settings.setAttachment(st.nextToken());
        	 settings.setEmails( st.nextToken());	
        	 settings.setLastNotifyTime(st.nextToken()); // For Last Alert
        }
        settings.setNotifications(notificationList);
        return NoError;
    }
	
	public int readNotificationDetails(String room, int sid, String FailedFlag, String NotificationRetries, Vector notificationHistoryVector){
    	Session session = getSession(sid);
    	if (session == null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	
    	try {
    		Vector notificationDetails = new Vector();
    		notificationDetails = vws.readNotificationDetails(room, sid, FailedFlag, NotificationRetries);
    		
    		Vector notificationHistory = vectorToNotificationHistoryList(notificationDetails);
    		
    		notificationHistoryVector.addAll(notificationHistory);
    	} catch (Exception ex) {
    		return Error;
    	}
    	return NoError;
    }
    
	private Vector vectorToNotificationHistoryList(Vector notificationDetails) {
		// TODO Auto-generated method stub
		Vector notifyHistoryVector= new Vector();
		
		if(notificationDetails!=null && notificationDetails.size()>0){
			
			for(int i=0; i<notificationDetails.size(); i++){
				String dbstring = notificationDetails.get(i).toString();
				NotificationHistory notifyHistory = new NotificationHistory(dbstring);
				notifyHistoryVector.add(notifyHistory);
			}
		}
		/*if(notifyHistoryVector!=null && notifyHistoryVector.size()>0)
			//VNSLog.add("notifyHistoryVector : "+notifyHistoryVector.size());
		else
			//VNSLog.add("notifyHistoryVector is null");
*/			return notifyHistoryVector;
	}
	
	public int getLoggedInUserMailId(int sid, Vector loggedInUserMailId)
 	{
 		Session session = getSession(sid);
 		if(session==null) return invalidSessionId;
 		VWS vws = getServer(session.server);
 		if (vws == null) return ServerNotFound;
 		try
 		{
 			Vector mailId = new Vector();
 			mailId = vws.getLoggedInUserMailId(session.room, sid);
 			loggedInUserMailId.addAll(mailId);
 			return vws.getActiveError();
 		}
 		catch(Exception e)
 		{
 			return Error;
 		}
 	}
	
	public int addNotificationHistory(int sid, int nodeId, int nodeType, int notifyId, String message){
		Session session = getSession(sid);
    	if (session == null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try {
    		vws.addNotificationHistory(session.room, sid, nodeId, nodeType, notifyId, session.user, "-", "-", message);
    	}catch(Exception ex){
    		return Error;
    	}
		return NoError;
	}
	
	public int CheckNotificationExist(int sid, int nodeId, int nodeType, int notifyId){
		Session session = getSession(sid);
    	if (session == null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try {
    		vws.CheckNotificationExist(session.room, sid, nodeId, nodeType, notifyId, session.user);
    	}catch(Exception ex){
    		return Error;
    	}
		return NoError;
	}
	
	public int getDSNInfo(int sid, int docTypeId, int indexId, int dsnType, Vector dsnInfo){
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try{
			Vector result = new Vector(); 
			result = vws.getDSNInfo(session.room, sid, docTypeId, indexId, dsnType);
			dsnInfo.addAll(result);
		}catch(Exception ex){
			return Error;
		}

		return NoError;
	}
	/**
	 * Enhancement :-security report for user/group to confirm access CV10 Date:- 9-2-2016
	 * @param sid
	 * @param selectedPrincipal
	 * @param reportNodeId
	 * @param checkBoxValue
	 * @param reportData
	 * @return
	 */
	public int generateNodeSecurityReport(int sid,String selectedPrincipal,
			int reportNodeId, int checkBoxValue,Vector reportData) {
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try{
			Vector result = new Vector(); 
			result = vws.generateNodeSecurityReport(session.room, sid, selectedPrincipal, reportNodeId, checkBoxValue);
			reportData.addAll(result);
		}catch(Exception ex){
			return Error;
		}

		return NoError;
	}
	public int getGroupsData(int sid, Vector groupsData)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getGroups(session.room, sid, groupsData);
            groupsData.addAll(ret);
            return vws.getActiveError();
        }
        catch(Exception e)
        {
            return Error;
        }
    }
	
	/**
	 * Ehancment:- SubAdministrato CV10
	 * @param sid
	 * @param groupName
	 * @param roles
	 * @param updFlag
	 * @return
	 */
	   public int setSubAdminRoles(int sid,String user,String roles,int updFlag){
		   printToConsole("User Name in setSubAdminRoles :"+user);
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		printToConsole("Before calling setSubAdminRoles ....");
	    		Vector ret=vws.setSubAdminRoles(session.room,sid,user,roles,updFlag);
	    		printToConsole("After calling setSubAdminRoles ...."+updFlag);
	    		return NoError;
	    	}catch(Exception e){
	    		printToConsole("Exception in setSubAdminRoles :"+e);
	    		return Error;
	    	}
	    }
	   
	   public int getSubAdminRoles(int sid,String user,Vector rolesData){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getSubAdminRoles(session.room,sid,user);	  
	    		rolesData.addAll(ret);
	    		return NoError;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	   
	   
	   public int removeSubAdminRoles(int sid,String groupName){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		vws.removeSubAdminRoles(session.room,sid,groupName);	  
	    		return NoError;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	   
	   public int getSubAdminTabs(int sid,String userName,Vector tabsData){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getSubAdminTabs(session.room,sid,userName);	  
	    		tabsData.addAll(ret);
	    		return NoError;
	    	}catch(Exception e){
	    		VWClient.printToConsole("exception while getUsertoNotifyDocs:::"+e.getMessage());
	    		return Error;
	    	}
	    }
	   public int checkSubAdmin(String room,int sid,String userName){
		   try{
			   int returnVal=0;
			   Session session = getSession(sid);
			   if(session==null) return invalidSessionId;
			   VWS vws = getServer(session.server);
			   if (vws == null) return ServerNotFound;
			   Vector ret=vws.checkSubAdmin(session.room,sid,userName);	 
			   if(ret.size()>0&&ret!=null){
				   returnVal=Integer.parseInt(ret.get(0).toString());
				   return returnVal;
			   }
			   else
				   return NoError;
		   }catch(Exception e){
			   return Error;
		   }

	   }
	      
	    
	   
	   
	public int getNamedProfessionalUser(int sid, Vector userData)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector UserVector=new Vector();
           	Vector adminVector=new Vector();
            String namedUser = vws.getNamedUsers();
            String namedOnlineUser=vws.getNamedOnlineUsers();
            String concurrentOnlineUser=vws.getConcurrentOnlineUsers();
            String professionalUser=vws.getProfessionalNamedUsers(); 	
            String enterpriseConcurrent=vws.getEnterPriseConcurrent();
            String professionalConcurrent=vws.getProfessionalConcurrent();
            UserVector.add(0,namedUser);
            UserVector.add(1,professionalUser);
            getAdminGroup(sid,adminVector);
            VWClient.printToConsole("adminVector.get(0).toString()"+adminVector.get(0).toString());
            UserVector.add(2,adminVector.get(0).toString());
            UserVector.add(3,enterpriseConcurrent);
            UserVector.add(4,professionalConcurrent);
            UserVector.add(5,namedOnlineUser);
            UserVector.add(6,concurrentOnlineUser);
            userData.addAll(UserVector);
            return vws.getActiveError();
        }
        catch(Exception e)
        {
            return Error;
        }
    }
	//Enhancement :- CV10 Sub administrator Method added to get the admin group to filter the groups combo in sub administrator
	public int getAdminGroup(int sid,Vector admingroup){
		int returnVal=0;
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		RoomProperty[] rooms = null;
		try
		{
			rooms = vws.getRegRooms();
			if (rooms != null && rooms.length > 0)
			{
				for (int i=0; i < rooms.length; i++)
				{
					if(rooms[i].getName().equals(session.room)){
						admingroup.add(rooms[i].getAdmins());
					}
				}
			}

			return NoError;
		}

		catch(Exception e){
			return Error;
		}

	}
	
	public int setTabStatus(int sid,int tabStatus){
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try {
			vws.setTabStatus(session.room, tabStatus);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			return Error;
		}
		return NoError;
	}
	public int setToggleStatus(int sid,int tabStatus){
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try {
			vws.setToggleStatus(session.room, tabStatus);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			return Error;
		}
		return NoError;
	}
	public int getTabStatus(int sid,Vector tabStatus){
		int returnVal=0;
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		RoomProperty[] rooms = null;
		try
		{
			rooms = vws.getRegRooms();
			if (rooms != null && rooms.length > 0)
			{
				for (int i=0; i < rooms.length; i++)
				{
					if(rooms[i].getName().equals(session.room)){
						tabStatus.clear();
						tabStatus.add(String.valueOf(rooms[i].getTabStatus()));
					}
				}
			}

			return NoError;
		}

		catch(Exception e){
			return Error;
		}

	}
	public int getToggleStatus(int sid,Vector toggleStatus){
		int returnVal=0;
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		RoomProperty[] rooms = null;
		try
		{
			rooms = vws.getRegRooms();
			if (rooms != null && rooms.length > 0)
			{
				for (int i=0; i < rooms.length; i++)
				{
					if(rooms[i].getName().equals(session.room)){
						toggleStatus.add(String.valueOf(rooms[i].getToggleStatus()));
					}
				}
			}

			return NoError;
		}

		catch(Exception e){
			return Error;
		}

	}
	/**
	 * CV10.1 Enhancement :- 2114 Document Templates
	 * @param sid
	 * @param selectValues
	 * @return
	 */
	public int getTemplateFiles(int sid, Vector selectValues){
    	VWClient.printToConsole("getTemplateFiles::::");
    	Session session = getSession(sid);
    	//System.out.println("index.getid::::"+index.getId());
    	VWClient.printToConsole("getTemplateFiles sid:"+sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try{
    		File[] listOfFiles = vws.getTemplateFiles();
    		for(int i=0;i<listOfFiles.length;i++){
    			selectValues.add(listOfFiles[i].getName());
    		}
    		VWClient.printToConsole("getTemplateFiles sid:"+selectValues);
    		if (sid>0 && vws.getActiveError()<0 ){
    			return NoError;
    		} else {
    			return vws.getActiveError();
    		}

    	} catch(Exception e)
    	{
    		return Error;
    	}

    }
	
	/**
	 * Fetches secure link data for connected room
	 * @author apurba.m
	 * @param sid
	 * @param info
	 * @return
	 */
	public int getSecureLinkDataAdminwise(int sid, Vector info)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getSecureLinkDataAdminwise(session.room, sid, info);
            info.addAll(ret);
            return NoError; 
        }
        catch(Exception re)
        {
            return Error;
        }
    }
	
	/**
	 * Expires selected secure link
	 * @param sid
	 * @param secureLinkId
	 * @return
	 */
	public int expireSelectedLink(int sid, int secureLinkId){
		
		Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            int ret = vws.expireSelectedLink(session.room, sid, secureLinkId);
            return NoError; 
        }
        catch(Exception re)
        {
            return Error;
        }
		
	}
	
	public boolean checkSubAdminUser(int sid, String user) {
		printToConsole("sid inside checkSubAdminUser :" + sid);
		boolean isSubAdminGroup = false;
		Session session = getSession(sid);
		if (session != null) {
			VWS vws = getServer(session.server);
			if (vws != null) {
				try {
					isSubAdminGroup = vws.checkSubAdminUser(session.room, sid, user);
				} catch (Exception e) {
					printToConsole("Exception in checkSubAdminUser :" + e);
				}
			}
		}
		return isSubAdminGroup;
	}
}