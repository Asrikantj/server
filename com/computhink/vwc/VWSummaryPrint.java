package com.computhink.vwc;

import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

public class VWSummaryPrint implements Printable {
	private Printable delegate;
    private int offset;
    
    public VWSummaryPrint(Printable delegate, int offset) {
    	this.offset = offset;
        this.delegate = delegate;
    }
    @Override
    public int print(Graphics graphics, PageFormat pageFormat, int pageIndex) throws PrinterException {
    	try {
    		return delegate.print(graphics, pageFormat, pageIndex-offset);
    	} catch (Exception e) {
    		VWClient.printToConsole("Exception while printing summary report in VWSummaryPrint class :"+e);
    		return Printable.NO_SUCH_PAGE;
    	}
    }
}
