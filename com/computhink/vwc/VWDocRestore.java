/*
                      Copyright (c) 1997-2001
                         Computhink Software
 
                      All rights reserved.
 */
/**
 * VWRestoreConnector<br>
 *
 * @version     $Revision: 1.9 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
 **/
package com.computhink.vwc;

import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Hashtable;
import java.util.List;
import java.util.LinkedList;
import java.awt.Dimension;
import java.io.*;

import javax.swing.BorderFactory;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

//import org.japura.controller.modals.WarningPanel;

import com.computhink.resource.ResourceManager;
import com.computhink.common.*;
import com.computhink.vns.server.VNSConstants;

//--------------------------------------------------------------------------
public class VWDocRestore implements Constants, ViewWiseErrors {
    private VWClient vwClient=null;
    private Hashtable documentInfo=new Hashtable();
    private Hashtable indicesTable=new Hashtable();
    private Hashtable docTypeStatus=new Hashtable();
    HashMap indexInfo = new HashMap();
    List indicesList= null; 
    private File restoreDocFile=null;
    int retVal=0;
    int restoreType=0;
    private boolean restoreWithVR=false;
    private boolean restoreReferences=false;
    private boolean restoreWithAT=false;
    private boolean restoreWithFolders=false;
    private int restoreSequenceSeed=0;
    private boolean enableSearchInContaints=false;
    private boolean restoreWriteInfoLog = false;
    private ResourceManager resourceManager=ResourceManager.getDefaultManager();
    Vector restoreIndiceCheck = null;
    public static String backupMsg = "";
// Changes by Mallikarjuna to include "Creation and modify dates from backup set "
    private boolean restoreCreatedDate = false;  
    public Hashtable<String, Integer> docTypeTable = new Hashtable<>();
/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
Add the argument docTypeStatus to get the existing restored document type status
*/    
    public int restoreDocument(int sid, VWClient vwClient, String docDir,
    int parentNodeId, int restoreType, boolean restoreWithVR,
    boolean restoreReferences, boolean restoreWithAT,
    boolean restoreWithFolders, int restoreSequenceSeed,
    		int sessionId,int withUniquenessCheck ) {
    		return restoreDocument(sid,vwClient,docDir,parentNodeId,restoreType,
    			restoreWithVR,restoreReferences,restoreWithAT,restoreWithFolders,
    			restoreSequenceSeed,sessionId,withUniquenessCheck,null,null);
    }
// Added By Mallikarjuna, Issue 712 - Restoration of Created Date and Modifed Date     
public int restoreDocument(int sid, VWClient vwClient, String docDir,
    int parentNodeId, int restoreType, boolean restoreWithVR,
    boolean restoreReferences, boolean restoreWithAT,
    boolean restoreWithFolders, int restoreSequenceSeed,
    int sessionId,int withUniquenessCheck,Hashtable docTypeStatus,HashMap indexInfo){
return restoreDocument(sid,vwClient,docDir,parentNodeId,restoreType,
    	    			restoreWithVR,restoreReferences,restoreWithAT,restoreWithFolders,
    	    			restoreSequenceSeed,sessionId,withUniquenessCheck,null,null,restoreCreatedDate);    	
    }
    public int restoreDocument(int sid, VWClient vwClient, String docDir,
    int parentNodeId, int restoreType, boolean restoreWithVR,
    boolean restoreReferences, boolean restoreWithAT,
    boolean restoreWithFolders, int restoreSequenceSeed,
    // Added By Mallikarjuna to Restore Created and Modifed Dates    
    int sessionId,int withUniquenessCheck,Hashtable docTypeStatus,HashMap indexInfo, boolean restoreCreatedDate) {
    	this.vwClient=vwClient;
        this.restoreType=restoreType;
        this.restoreWithVR=restoreWithVR;
        this.restoreReferences=restoreReferences;
        this.restoreWithAT=restoreWithAT;
        this.restoreWithFolders=restoreWithFolders;
        this.restoreSequenceSeed=restoreSequenceSeed;
        this.docTypeStatus = docTypeStatus;
        // Added By Mallikarjuna to Restore Created and Modifed Dates        
        this.restoreCreatedDate = restoreCreatedDate;   
        this.indexInfo = indexInfo;
        
        Session session = vwClient.getSession(sid);
        if(session==null) return invalidSessionId;
        
        VWSAXParser parser = new VWSAXParser();
        restoreDocFile=new File(docDir,BackupFileName);
        parser.getParser(restoreDocFile);
        documentInfo=parser.getDocumentInfo();
        if(documentInfo==null || documentInfo.size()==0) {
            ///errorList.add(restoreDocPath+" :Error while reading document info");
            return DocumentInfoErr;
        }
        String prefix="VWDocument\\";
        int backupType=getIntValue(prefix+"Path");
        if(backupType==VWDocBackup.COPYTYPE_TYPE)
        {
            return readOnlyDocumentErr;
        }
        if(parentNodeId==0) parentNodeId=addDocNodes(sid);
        if(parentNodeId==0) {
            ///errorList.add(restoreDocPath+" :Error while creating parent node");
            return CreateParentNodeErr;
        }
        int docType=addDocType(sid);
        if(docType<=0) {
            ///errorList.add(restoreDocPath+" :Error while creating document type");
            return (docType==0?CreateDocTypeErr:docType);
        }
        Vector validatNode = new Vector();
        /**
         * **
         * Enhancement :-Locked document type
         * checks whether the node is having permission to restore the document 
         * with the  document type to the destination node.
         * If the return value is not equal to 1 then DocumentType Not Permitted alert message will be displayed.
         */
        vwClient.ValidateNodeDocType(sid,2,docType,parentNodeId,validatNode);
        if(!validatNode.get(0).toString().equalsIgnoreCase("1")){
        	return DocumentTypeNotPermitted;
        }
        // Added By Mallikarjuna to Restore Created and Modifed Dates  
        int nodeId=addNewDocument(sid,parentNodeId,docType,sessionId,withUniquenessCheck, restoreCreatedDate );
        /*
        if(nodeId<0) {
            ///errorList.add(restoreDocPath+" :Error while creating document");
            return CreateDocumentErr;
        }
         */
        if(nodeId==-1) {
                ///errorList.add(restoreDocPath+" :Error while creating document");
                return CheckUniquenessIndexErr;
            }
        else if(restoreType==1 && nodeId<0) {
            ///errorList.add(restoreDocPath+" :Document already restored.");
            return DocumentAlreadyRestored;
        }
        else if(restoreType>1 && nodeId<0) {
            if(restoreType==2) {
                vwClient.purgeDocument(sid,new Document(nodeId));
            }
            else if(restoreType==3) {
                vwClient.deleteDocument(sid,new Document(nodeId));
            }
            // Added By Mallikarjuna to Restore Created and Modifed Dates             
            nodeId=addNewDocument(sid, parentNodeId,docType,sessionId,withUniquenessCheck, restoreCreatedDate);
            if(nodeId<0) {
                return CreateDocumentErr;
            }
        }
        if(restoreType<0 && nodeId==0) {
            ///errorList.add(restoreDocPath+" :Error while creating document");
            return CreateDocumentErr;
        }
       // addComments(sid, nodeId);
        addRedactionsData(sid, nodeId);
        if(restoreWithVR) {
            addVerRevData(sid, nodeId, docType);
        }
        if(restoreWithAT) {
            addATData(sid, nodeId, docType);
        }
        if(restoreReferences) {
            addReferecesData(sid, nodeId);
        }
        addComments(sid, nodeId);
        uploadFiles(sid, new Document(nodeId));
        return nodeId;
    }
    //--------------------------------------------------------------------------
    private void addVerRevData(int sid, int nodeId,int docTypeId) {
    	 //Added for Change Document Type Enhancement,Gurumurthy.T.S
    	int docTypeCount=Util.to_Number(getValue("VWDocument\\DocTypeCount"));
    	String verDocTypeName =" ";
    	//
        if(docTypeCount==0){
    	String prefix="VWDocument\\VerRev\\";
        int recCount=Util.to_Number(getValue(prefix+"Count"));
        prefix+="VRDoc\\";
        String params="";
        for(int i=1;i<=recCount;i++) {
            params=String.valueOf(nodeId)+Util.SepChar+String.valueOf(docTypeId);
            String value=getValue(prefix+"Value"+i);
            params+=Util.SepChar+value;
            String creator=getValue(prefix+"Creator"+i);
            params+=Util.SepChar+creator;
            String creationDate=getValue(prefix+"CreationDate"+i);
            params+=Util.SepChar+creationDate;
            String comment=getValue(prefix+"Comment"+i);
            params+=Util.SepChar+comment;
            params+=Util.SepChar+(i==recCount?"1":"0");
            String docFileName=getValue(prefix+"DocFileName"+i);
            params+=Util.SepChar+docFileName;
            int count=getIntValue(prefix+"Count"+i);
            params+=Util.SepChar+count;
            for(int j=1;j<=count;j++) {
                String indexName=getValue(prefix+"Index\\"+"Name"+i+"_"+j);
                String VRIndexKey = "";
                try{
                	VRIndexKey=getValue(prefix+"Index\\"+"VRIndexKey"+i+"_"+j);
                	if(VRIndexKey != null && VRIndexKey.trim().length() > 0){
                		VRIndexKey=VRIndexKey.equalsIgnoreCase("true")?"1":"0";
                	}
                }catch(Exception e){
                	VRIndexKey="";                	                	
                }
                String indexValue=getValue(prefix+"Index\\"+"Value"+i+"_"+j);
                String indexActValue=getValue(prefix+"Index\\"+"ActValue"+i+"_"+j);
                String indexId=(String)indicesTable.get(indexName);
                params+=Util.SepChar+indexId+Util.SepChar+VRIndexKey+Util.SepChar+indexValue+Util.SepChar+indexActValue;
            }
            Vector result=new Vector();
            //Desc   :If nodeid not equals 0 then call this procedure
            //Author :Nishad Nambiar
            //Date   :6-Dec-2007
            if(nodeId!=0){
            	vwClient.adminWise(sid,"VR_RestoreData",params,result);
            }
        }
        //Added for Change Document Type Enhancement,Gurumurthy.T.S
        }
        else
        {
        	String prefix="VWDocument\\VerRev\\";
        	int recCount=Util.to_Number(getValue(prefix+"Count"));
        	prefix+="VRDoc\\";
        	String params="";
        	VWClient.printToConsole("indicesTable : "+indicesTable);
        	for(int i=1;i<=recCount;i++) {
        		verDocTypeName = getValue(prefix+"VerdocTypeName"+i);
        		docTypeId=docTypeTable.get(verDocTypeName);
        		params=String.valueOf(nodeId)+Util.SepChar+String.valueOf(docTypeId);
        		String value=getValue(prefix+"Value"+i);
        		params+=Util.SepChar+value;
        		String creator=getValue(prefix+"Creator"+i);
        		params+=Util.SepChar+creator;
        		String creationDate=getValue(prefix+"CreationDate"+i);
        		params+=Util.SepChar+creationDate;
        		String comment=getValue(prefix+"Comment"+i);
        		params+=Util.SepChar+comment;
        		params+=Util.SepChar+(i==recCount?"1":"0");
        		String docFileName=getValue(prefix+"DocFileName"+i);
        		params+=Util.SepChar+docFileName;
        		int count=getIntValue(prefix+"Count"+i);
        		params+=Util.SepChar+count;
        		for(int j=1;j<=count;j++) {
        			String indexName=getValue(prefix+"Index\\"+"Name"+i+"_"+j);
        			String VRIndexKey = "";
        			try{
        				VRIndexKey=getValue(prefix+"Index\\"+"VRIndexKey"+i+"_"+j);
        				if(VRIndexKey != null && VRIndexKey.trim().length() > 0){
        					VRIndexKey=VRIndexKey.equalsIgnoreCase("true")?"1":"0";
        				}
        			}catch(Exception e){
        				VRIndexKey="";                	                	
        			}
        			String indexValue=getValue(prefix+"Index\\"+"Value"+i+"_"+j);
        			String indexActValue=getValue(prefix+"Index\\"+"ActValue"+i+"_"+j);
        			String indexId=(String)indicesTable.get(indexName);
        			params+=Util.SepChar+indexId+Util.SepChar+VRIndexKey+Util.SepChar+indexValue+Util.SepChar+indexActValue;
        		}
        		Vector result=new Vector();
        		//Desc   :If nodeid not equals 0 then call this procedure
        		//Author :Nishad Nambiar
        		//Date   :6-Dec-2007
        		VWClient.printToConsole("params in addVerRevData : "+params);
        		if(nodeId!=0){
        			vwClient.adminWise(sid,"VR_RestoreData",params,result);
        		}
        	}

        }
    }
    //--------------------------------------------------------------------------
    private void addRedactionsData(int sid, int nodeId) {
        String prefix="VWDocument\\Redactions\\";
        int recCount=getIntValue(prefix+"Count");
        prefix+="Redaction\\";
        String params="";
        for(int i=1;i<=recCount;i++) {
            params=String.valueOf(nodeId);
            params+=Util.SepChar+getIntValue(prefix+"PageID"+i);
            params+=Util.SepChar+getIntValue(prefix+"FrameID"+i);
            params+=Util.SepChar+getIntValue(prefix+"RedactionID"+i);
            params+=Util.SepChar+getValue(prefix+"UserName"+i);
            params+=Util.SepChar+getValue(prefix+"Password"+i);
            params+=Util.SepChar+getValue(prefix+"VerRev"+i);
            Vector result=new Vector();
            vwClient.adminWise(sid,"DOC_RestoreRedaction",params,result);
        }
    }
    //--------------------------------------------------------------------------
    private void addReferecesData(int sid, int nodeId) {
        String prefix="VWDocument\\References\\";
        int recCount=getIntValue(prefix+"Count");
        prefix+="Reference\\";
        String params="";
        Document doc=new Document();
        for(int i=1;i<=recCount;i++) {
            int nodeRefid=getRefrencesNodeId(sid,getValue(prefix+"NodeRefPath"+i));
            if(nodeRefid>0) {
                vwClient.addRef(sid, doc, new Document(nodeRefid),true);
            }
        }
    }
    //--------------------------------------------------------------------------
    private void addATData(int sid, int nodeId, int docType) {
        String prefix="VWDocument\\AuditTrail\\";
        int recCount=Util.to_Number(getValue(prefix+"Count"));
        prefix+="ATDoc\\";
        String nodeName="";
        String userName="";
        String clientIP="";
        String dActionDate="";
        String actionType="";
        String location="";
        String description="";
        String actionDate="";
        String atDocData="";
        for(int i=1;i<=recCount;i++) {
            nodeName=getValue(prefix+"NodeName"+i);
            userName=getValue(prefix+"UserName"+i);
            clientIP=getValue(prefix+"ClientIP"+i);
            dActionDate=getValue(prefix+"DActionDate"+i);
            actionType=getValue(prefix+"ActionType"+i);
            location=getValue(prefix+"Location"+i);
            description=getValue(prefix+"Description"+i);
            actionDate=getValue(prefix+"ActionDate"+i);
            atDocData=String.valueOf(nodeId)+Util.SepChar+"2"+Util.SepChar+nodeName+Util.SepChar+
            userName+Util.SepChar+clientIP+Util.SepChar+description+Util.SepChar+location+Util.SepChar+
            dActionDate+Util.SepChar+actionDate+Util.SepChar+actionType;
            Vector result=new Vector();
            vwClient.adminWise(sid,"AT_RestoreData",atDocData,result);
        }
    }
    //--------------------------------------------------------------------------
    private int addNewDocument(int sid, int parentNodeId, int docType, int sessionId
    		   ,int withUniquenessCheck, boolean restoreCreatedDate) {
    	//Added for Change Document Type Enhancement,Gurumurthy.T.S
    	int documentType=0;
    	String restoreDocTypeName ="";
        String prefix="VWDocument\\";
        String path=getValue(prefix+"PathIDs");
        String pageCount=getValue(prefix+"PageCount");
        int docIndexId = VWCUtil.getDocId();
        StringTokenizer tokens = new StringTokenizer(path,Util.SepChar,false);
        String backupNodeid="";
        while(tokens.hasMoreTokens())   backupNodeid=tokens.nextToken();
        prefix="VWDocument\\Indices\\";
        int indicesCount=getIntValue(prefix+"Count");
        Session session = vwClient.getSession(sid);
        String docInfo = ""; Vector oTempVector = new Vector ();
        //Added for Change Document Type Enhancement,Gurumurthy.T.S
        documentType=Util.to_Number(getValue("VWDocument\\Indices\\DocTypeId"));
        restoreDocTypeName = getValue("VWDocument\\Indices\\DocTypeName");
        if(documentType>0)
        	docType = docTypeTable.get(restoreDocTypeName);
        
        // Added By Mallikarjuna to Restore Created and Modifed Dates
        if (docIndexId > -1) {
        	//Vector has replaced with the LinkedList for Bug-206 fix,Gurumurthy.T.S 22=01-2014
        		oTempVector = (Vector) VWCUtil.DateList.get(docIndexId);
            String DateStr = "";
            if (restoreCreatedDate)
            	DateStr = oTempVector.elementAt (0).toString()+Util.SepChar+oTempVector.elementAt (1).toString();
            else
            	DateStr = "0" + Util.SepChar + "0";
            
            docInfo= DateStr+ Util.SepChar +session.user+Util.SepChar;
        }
        else
        	docInfo="0" + Util.SepChar +
        			"0" + Util.SepChar +
        			session.user+Util.SepChar;

        docInfo = docInfo + vwClient.getClientIP()+Util.SepChar+
        					String.valueOf(docType)+Util.SepChar+
        					String.valueOf(parentNodeId)+Util.SepChar+
        String.valueOf(restoreSequenceSeed)+Util.SepChar+
        restoreType+Util.SepChar+
        backupNodeid+Util.SepChar+
        sessionId+Util.SepChar+
        pageCount+Util.SepChar+
        withUniquenessCheck+Util.SepChar+
        String.valueOf(indicesCount);
        prefix+="DocIndex\\";
        for(int i=1;i<=indicesCount;i++) {
            String indexName=getValue(prefix+"Name"+i);
            String indexValue=getValue(prefix+"Value"+i);
            String indexActValue=getValue(prefix+"ActValue"+i);
            String indexId="0";
            indexId=(String)indicesTable.get(indexName);
            docInfo+=Util.SepChar+indexId+Util.SepChar+indexValue+Util.SepChar+indexActValue;
        }
        Vector result=new Vector();
        vwClient.adminWise(sid,"Document_Restore",docInfo,result);

        if(result==null || result.size()==0) return 0;
        return Util.to_Number((String)result.get(0));
    }
/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
Get the status of the document type. Index is exist or not, index is mismatch or not
*/
   //Modified for Change Document Type Enhancement,Gurumurthy.T.S
    private int checkDocType(int sid,DocType docType, int doctypeCount){
    	int docTypeStatusId = 0;
    	int dTStatusId = 0;
    	 //Added for Change Document Type Enhancement,Gurumurthy.T.S
    	int dtindexCount = 0;
    	boolean docTypeMismatch = false;
    	 //Modified for Change Document Type Enhancement,Gurumurthy.T.S
    	if(doctypeCount==0)
    		dtindexCount=Util.to_Number(getValue("VWDocument\\DocType\\IndicesCount"));
    	else
    		dtindexCount=Util.to_Number(getValue("VWDocument\\DocType\\IndicesCount"+doctypeCount));
        indicesList = new LinkedList();
        String mismatchIndexNames = "";
        for(int j=0;j<dtindexCount;j++) {
        	 //Modified for Change Document Type Enhancement,Gurumurthy.T.S
            Index index=getIndex(j+1,doctypeCount);
            int retCheck = 0;
            if (index.getName() == null || (index.getName() != null && index.getName().equals(""))){
        	docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_INDEX_EMPTY));
        	docTypeMismatch = true; 
            }
            retCheck=checkIndexIsFound(sid,index, docType);
            if(retCheck==0) {     
            	Util.Msg(null, resourceManager.getString("RestoreMsg.NoDocIndexCVRestore1")+" "+ docType.getName()+" " + resourceManager.getString("RestoreMsg.NoDocIndexCVRestore2") + index.getName() + ">>", PRODUCT_NAME +" "+ resourceManager.getString("RestoreMsg.NoDocIndexCVRestore3"));
            	if (docTypeStatusId == 0)
            		docTypeStatusId = 1;
                indicesList.add(index);
            }
            else if (retCheck>0) {
            	if (retCheck  == 999999999){
            		int indexId = getIndexId(sid, index);
            		index.setId(indexId);
            	}else{
            		index.setId(retCheck);
            	}
                indicesList.add(index);
                indicesTable.put(index.getName(),String.valueOf(index.getId()));                
            }else if (retCheck<0) {
            	docTypeStatusId = retCheck;
            	docTypeMismatch = true;            	
            	mismatchIndexNames += " <<" + index.getName() + ">> ";

            }
        } 
        if(docTypeStatus!= null && docTypeStatus.containsKey(docType.getName())){
        	dTStatusId = Integer.parseInt(docTypeStatus.get(docType.getName()).toString());
        }
        if (docTypeStatusId < 0 && mismatchIndexNames.trim().length() > 0 && dTStatusId != DOCUMENT_TYPE_MISMATCH && dTStatusId != DOCUMENT_TYPE_MISMATCH_SKIP_DOCUMENT){
        	Util.Msg(null, resourceManager.getString("RestoreMsg.DocTypeIndexPropMismatch1")+" " + docType.getName() + " "+resourceManager.getString("RestoreMsg.DocTypeIndexPropMismatch2")+" " + mismatchIndexNames , PRODUCT_NAME +" "+ resourceManager.getString("RestoreMsg.DocTypeIndexPropMismatch3"));
        	docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_MISMATCH));
        }
    	return (docTypeStatusId);
    }
	private int getIndexId(int sid, Index index) {
    	int indexId = 0;
    	try{
    		indexId = vwClient.getIndexId(sid, index);
    	}catch (Exception e) {
			
		}
    	return indexId;
    }
	/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
Display the Error message for document type is not exist, index is not exist and document type is mismatch
Check the current document type already processed for restore.If it is true retrieve from hashmap "indexInfo",else read from database.
*/
    public int addDocType(int sid) 
    {
        boolean addNewDocType=true;
        docTypeTable = new Hashtable<>();
        int docTypeStatusId = -1;
        if(docTypeStatus == null){
        	docTypeStatus = new Hashtable();
        }
      //Added for Change Document Type Enhancement,Gurumurthy.T.S
        int docTypeId = 0;
        DocType docType = null;
        int docTypeCount=0;
        docTypeCount=Util.to_Number(getValue("VWDocument\\DocTypeCount"));
        if(docTypeCount==0)
        {
        docType=getDocType(0);
        String docTypeName = docType.getName().trim();
        if(docTypeName == null || docTypeName.equals("")){
        	return DocumentTypeNameEmptyErr;
        }
        if(docTypeStatus!= null && docTypeStatus.containsKey(docTypeName)){
        	docTypeStatusId = Integer.parseInt(docTypeStatus.get(docTypeName).toString());
        }
        if(docTypeStatusId == DOCUMENT_TYPE_MISMATCH_SKIP_DOCUMENT){
        	return CheckDocumentTypeErr;
        }
        if(docTypeStatusId  == DOCUMENT_TYPE_NOT_EXIST){
        	return CancelCreateDocTypeProcess;
        }
        int docTypeCheckStatusId = 0;
        enableSearchInContaints=docType.getEnableTextSearch();
        docType.setId(checkDocTypeIsFound(sid,docType.getName()));
        if(docType.getId()>0) 
        {
            addNewDocType=false;
        }
        /*
    	 * Implemented to check whether the index field matches in the Backup set and Restoring room,
    	 * Before Restoration. Gurumurthy.T.S.
    	 * Bug 206 CV8B5-002
    	 */
        if(docType.getId()>0){
        	int dtindexCount=Util.to_Number(getValue("VWDocument\\DocType\\IndicesCount"));
        	restoreIndiceCheck = new Vector();
        	Vector resultVector = new Vector(); 
        	int restoreReturn=0;
        	for(int i=0;i<=dtindexCount;i++)
        	{
        		//Modified for Change Document Type Enhancement,Gurumurthy.T.S
        		Index restoreIndex = getIndex(i+1,0);
        		restoreIndiceCheck.add( restoreIndex.getName());
        	}
        	restoreReturn = restoreIndicesCheck(sid,docType,dtindexCount,restoreIndiceCheck,resultVector);
        	if(resultVector!=null && !resultVector.isEmpty() && !resultVector.elementAt(0).equals("1"))
        	{
        		backupMsg = resourceManager.getString("RestoreMsg.NoDocTypeIndex1")+resultVector+resourceManager.getString("RestoreMsg.NoDocTypeIndex2")+" < "+docType.getName()+" > "
        				+resourceManager.getString("RestoreMsg.NoDocTypeIndex3");
        		JOptionPane
        		.showMessageDialog(null, backupMsg,resourceManager.getString("DocRestore.Restore"),JOptionPane.WARNING_MESSAGE);
        		return 0;
        	}
        }
        //end of Bug 206 CV8B5-002
        if(docTypeStatusId == DOCUMENT_TYPE_SUCCESS && (indexInfo !=null && indexInfo.containsKey(docTypeName))){
        	indicesList = (List) indexInfo.get(docTypeName);
            for(int count = 0;count<indicesList.size();count++){
            	Index idx = (Index) indicesList.get(count);
            	indicesTable.put(idx.getName(),String.valueOf(idx.getId()));
            }        	
        }
        else//Modified for Change Document Type Enhancement,Gurumurthy.T.S
        	docTypeCheckStatusId = checkDocType(sid,docType,0);   
        if(addNewDocType){
/*
Issue No / Purpose:  <563/Displayed the document type name when it ask for create a document type or not>
Created by: <Pandiya Raj.M>
Date: <17 Aug 2006>
*/        	///
        	try{
        	String indicesInfo = "Index Details \n";
            for(int count = 0;count<indicesList.size();count++){
            	Index idx = (Index) indicesList.get(count);
            	indicesInfo += "< " +  idx.getName() + "> \n";
            }        	
        	///     
            String message = "The Destination Document Type < " + docType.getName() + " >is not available, Create Document Type (Yes/No). \n" + indicesInfo;
            VWClient.printToConsole("Message 2 : "+message);
            JTextArea messageLbl = new JTextArea(message);
            messageLbl.setBounds(10, 80, 500, 180);
            JScrollPane msgPanel = new JScrollPane(messageLbl);
            msgPanel.setBorder(BorderFactory.createEmptyBorder());
            //msgPanel.setViewportBorder(null);
            msgPanel.setPreferredSize(new Dimension(540, 150));
            messageLbl.setBackground(msgPanel.getBackground());
            messageLbl.setEditable(false);
        	if(!Util.Ask(null, msgPanel, PRODUCT_NAME + " Restore"))
        	{
        		docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_NOT_EXIST)); 
        		return CreateDocTypeErr;
        	}
        	}catch(Exception ex){}
        }        

        if (docTypeCheckStatusId < 0){
        	int errorNumber = -811;
        	switch(docTypeCheckStatusId){
        	case -1:
        		errorNumber = -8001;
        		break;
        	case -2:
        		errorNumber = -8002;
        		break;
        	case -4:
        		errorNumber = -8004;
        		break;
        	default:
        		errorNumber = -8008;        		
        	}        	
        	if(Util.Ask(null,"The Destination Document Type does not match, proceed with restore and skip document(Y/N).", PRODUCT_NAME + " Restore")){
        		docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_MISMATCH_SKIP_DOCUMENT));
        		return errorNumber;
        }
        	else{
        		docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_MISMATCH));
        		return CancelRestoreProcess;
        	}
        }

        
        if (docTypeCheckStatusId == 1 && !addNewDocType){
        	if(!Util.Ask(null,"The Destination Index is not available, Create Index (Yes/No). ", PRODUCT_NAME + " Restore"))
        	{
        		docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_INDEX_NOT_EXIST));
        		return IndexAlreadyExists;
        	}        	
        }
        
	        
        int dtindexCount=Util.to_Number(getValue("VWDocument\\DocType\\IndicesCount"));
        /*String indexFail="";        
        for(int j=0;j<dtindexCount;j++) {
            Index index=getIndex(j+1);
            int retCheck=checkIndexIsFound(sid,index);
            if(retCheck==0) {
                indicesList.add(index);
            }
            else if (retCheck>0) {
                index.setId(retCheck);
                indicesList.add(index);
                indicesTable.put(index.getName(),String.valueOf(retCheck));
            }
            else {
                return IndexAlreadyExists;
                ///indexFail=index.getName();
                ///continue;
            }
        }*/
        /*
        if(!indexFail.equals(""))
        {
            ///errorList.add(restoreDocPath+" :Index field "+indexFail+" exists with mismatched properties");
        }
         */
         docTypeId=docType.getId();
        if(docTypeId==0) {
            docTypeId=updateDocType(sid, docType);
            docType.setId(docTypeId);
            Util.Msg(null,resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully1")+" "+ docType.getName() + " "+resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully2"), PRODUCT_NAME +" "+ resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully3"));
        }
    	String indicesInfo = "Index Details \n";
    	boolean isIndexCreated = false;
        for(int k=0;k<dtindexCount;k++) {
            Index index=(Index)indicesList.get(k);
            if(index.getId()==0) {
                index.setId(updateIndex(sid, index));
                indicesTable.put(index.getName(),String.valueOf(index.getId()));
                //Util.Msg(null,"Index < "+ index.getName() + " > created successfully.","ViewWise Restore");
                indicesInfo += "< " +  index.getName() + "> \n";
                isIndexCreated = true;
            }
            index=updateIndexToDTIndex(index,k+1,0);
            if(checkDTIndexNameIsExists(sid, docType.getId(), index.getId())==0)
                updateDTIndex(sid, docType.getId(), index, k+1);
        }
        if (isIndexCreated)
		Util.Msg(null,resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully1")+" "
        + docType.getName() +" "+resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully2") 
        + indicesInfo, PRODUCT_NAME +" "+ resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully3"));
        if(!docType.getTo().equals("") ||
        !docType.getCc().equals("") ||
        !docType.getSubject().equals("")) {
            String autoMail_To=docType.getTo();
            int sepIndex=0;
            if((sepIndex=autoMail_To.indexOf("|"))>0) {
                String indexName=autoMail_To.substring(0,sepIndex);
                for(int s=0;s<dtindexCount;s++) {
                    Index index=(Index)indicesList.get(s);
                    if(indexName.equalsIgnoreCase(index.getName())) {
                        docType.setTo(index.getName()+"|"+index.getId());
                        break;
                    }
                }
            }
            String autoMail_Cc=docType.getCc();
            sepIndex=0;
            if((sepIndex=autoMail_Cc.indexOf("|"))>0) {
                String indexName=autoMail_Cc.substring(0,sepIndex);
                for(int s=0;s<dtindexCount;s++) {
                    Index index=(Index)indicesList.get(s);
                    if(indexName.equalsIgnoreCase(index.getName())) {
                        docType.setCc(index.getName()+"|"+index.getId());
                        break;
                    }
                }
            }
            String autoMail_Subject=docType.getSubject();
            sepIndex=0;
            if(autoMail_Subject.indexOf("|")>0) {
                StringTokenizer tokens = new StringTokenizer(autoMail_Subject,"|",false);
                autoMail_Subject="";
                while (tokens.hasMoreTokens()) {
                    String indexName=tokens.nextToken();
                    indexName=indexName.substring(1,indexName.length()-1);
                    for(int s=0;s<dtindexCount;s++) {
                        Index index=(Index)indicesList.get(s);
                        if(indexName.equalsIgnoreCase(index.getName())) {
                            autoMail_Subject+="["+index.getName()+"]"+"|"+index.getId()+"|";
                            break;
                        }
                    }
                    tokens.nextToken();
                }
                if(autoMail_Subject.endsWith("|"))
                    autoMail_Subject=autoMail_Subject.substring(0,autoMail_Subject.length()-1);
                docType.setSubject(autoMail_Subject);
            }
            if(addNewDocType) 
                vwClient.setDocTypeAutoMail(sid, docType);
        }
        if(addNewDocType) {
            String param=String.valueOf(docType.getId())+Util.SepChar+
            docType.getVREnable()+Util.SepChar+
            docType.getVRInitVersion()+Util.SepChar+
            docType.getVRInitRevision()+Util.SepChar+
            docType.getVRPagesChange()+Util.SepChar+
            docType.getVRIncPagesChange()+Util.SepChar+
            docType.getVRPageChange()+Util.SepChar+
            docType.getVRIncPageChange()+Util.SepChar+
            docType.getVRPropertiesChange()+Util.SepChar+
            docType.getVRIncPropertiesChange()+Util.SepChar;
            Vector result=new Vector();
            vwClient.setDTVersionSettings(sid, docType);
            ///vwClient.adminWise(sid, "VR_SetSettings", param, result);
        }
      //Added for Change Document Type Enhancement,Gurumurthy.T.S
        }
        else{
        	for(int l=0;l<docTypeCount;l++)
        	{
            docType=getDocType(l+1);
            String docTypeName = docType.getName().trim();
            if(docTypeName == null || docTypeName.equals("")){
            	return DocumentTypeNameEmptyErr;
            }
            if(docTypeStatus!= null && docTypeStatus.containsKey(docTypeName)){
            	docTypeStatusId = Integer.parseInt(docTypeStatus.get(docTypeName).toString());
            }
            if(docTypeStatusId == DOCUMENT_TYPE_MISMATCH_SKIP_DOCUMENT){
            	return CheckDocumentTypeErr;
            }
            if(docTypeStatusId  == DOCUMENT_TYPE_NOT_EXIST){
            	return CancelCreateDocTypeProcess;
            }
            int docTypeCheckStatusId = 0;
            enableSearchInContaints=docType.getEnableTextSearch();
            docType.setId(checkDocTypeIsFound(sid,docType.getName()));
            if(docType.getId()>0) 
            {
                addNewDocType=false;
                docTypeTable.put(docType.getName(), docType.getId());
            }
            /*
        	 * Implemented to check whether the index field matches in the Backup set and Restoring room,
        	 * Before Restoration. Gurumurthy.T.S.
        	 * Bug 206 CV8B5-002
        	 */
            if(docType.getId()>0){
            	int dtindexCount=Util.to_Number(getValue("VWDocument\\DocType\\IndicesCount"+(l+1)));
            	restoreIndiceCheck = new Vector();
            	Vector resultVector = new Vector(); 
            	int restoreReturn=0;
            	for(int i=0;i<=dtindexCount;i++)
            	{
            		Index restoreIndex = getIndex(i+1,l+1);
            		restoreIndiceCheck.add( restoreIndex.getName());
            	}
            	restoreReturn = restoreIndicesCheck(sid,docType,dtindexCount,restoreIndiceCheck,resultVector);
            	if(resultVector!=null && !resultVector.isEmpty() && !resultVector.elementAt(0).equals("1"))
            	{
            		backupMsg = resourceManager.getString("RestoreMsg.NoDocTypeIndex1")+resultVector+resourceManager.getString("RestoreMsg.NoDocTypeIndex2")+" < "+docType.getName()+" > "
            				+resourceManager.getString("RestoreMsg.NoDocTypeIndex3");
            		JOptionPane
            		.showMessageDialog(null, backupMsg,resourceManager.getString("DocRestore.Restore"),JOptionPane.WARNING_MESSAGE);
            		return 0;
            	}
            }
            //end of Bug 206 CV8B5-002
            if(docTypeStatusId == DOCUMENT_TYPE_SUCCESS && (indexInfo !=null && indexInfo.containsKey(docTypeName))){
            	indicesList = (List) indexInfo.get(docTypeName);
                for(int count = 0;count<indicesList.size();count++){
                	Index idx = (Index) indicesList.get(count);
                	indicesTable.put(idx.getName(),String.valueOf(idx.getId()));
                }        	
            }
            else
            	docTypeCheckStatusId = checkDocType(sid,docType,l+1);   
            if(addNewDocType){
    /*
    Issue No / Purpose:  <563/Displayed the document type name when it ask for create a document type or not>
    Created by: <Pandiya Raj.M>
    Date: <17 Aug 2006>
    */        	///
            	try{
            	String indicesInfo = "Index Details \n";
                for(int count = 0;count<indicesList.size();count++){
                	Index idx = (Index) indicesList.get(count);
                	indicesInfo += "< " +  idx.getName() + "> \n";
                }        	
            	///     
                String message = "The Destination Document Type < "+ docType.getName() + " >is not available, Create Document Type (Yes/No). \n" + indicesInfo;
                VWClient.printToConsole("Message 1 : "+message);
                JTextArea messageLbl = new JTextArea(message);
                messageLbl.setBounds(10, 80, 500, 180);
                JScrollPane msgPanel = new JScrollPane(messageLbl);
                msgPanel.setBorder(BorderFactory.createEmptyBorder());
                //msgPanel.setViewportBorder(null);
                msgPanel.setPreferredSize(new Dimension(540, 150));
                messageLbl.setBackground(msgPanel.getBackground());
                messageLbl.setEditable(false);                 
            	if(!Util.Ask(null,msgPanel, PRODUCT_NAME + " Restore"))
            	{
            		docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_NOT_EXIST)); 
            		return CreateDocTypeErr;
            	}
            	}catch(Exception ex){}
            }        

            if (docTypeCheckStatusId < 0){
            	int errorNumber = -811;
            	switch(docTypeCheckStatusId){
            	case -1:
            		errorNumber = -8001;
            		break;
            	case -2:
            		errorNumber = -8002;
            		break;
            	case -4:
            		errorNumber = -8004;
            		break;
            	default:
            		errorNumber = -8008;        		
            	}        	
            	if(Util.Ask(null,"The Destination Document Type does not match, proceed with restore and skip document(Y/N).", PRODUCT_NAME + " Restore")){
            		docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_MISMATCH_SKIP_DOCUMENT));
            		return errorNumber;
            }
            	else{
            		docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_MISMATCH));
            		return CancelRestoreProcess;
            	}
            }

            
            if (docTypeCheckStatusId == 1 && !addNewDocType){
            	if(!Util.Ask(null,"The Destination Index is not available, Create Index (Yes/No). ", PRODUCT_NAME + " Restore"))
            	{
            		docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_INDEX_NOT_EXIST));
            		return IndexAlreadyExists;
            	}        	
            }
            
    	        
            int dtindexCount=Util.to_Number(getValue("VWDocument\\DocType\\IndicesCount"+(l+1)));
            /*String indexFail="";        
            for(int j=0;j<dtindexCount;j++) {
                Index index=getIndex(j+1);
                int retCheck=checkIndexIsFound(sid,index);
                if(retCheck==0) {
                    indicesList.add(index);
                }
                else if (retCheck>0) {
                    index.setId(retCheck);
                    indicesList.add(index);
                    indicesTable.put(index.getName(),String.valueOf(retCheck));
                }
                else {
                    return IndexAlreadyExists;
                    ///indexFail=index.getName();
                    ///continue;
                }
            }*/
            /*
            if(!indexFail.equals(""))
            {
                ///errorList.add(restoreDocPath+" :Index field "+indexFail+" exists with mismatched properties");
            }
             */
             docTypeId=docType.getId();
            if(docTypeId==0) {
                docTypeId=updateDocType(sid, docType);
                docTypeTable.put(docType.getName(), docTypeId);
                docType.setId(docTypeId);
                Util.Msg(null,resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully1")+" "+ docType.getName() + " "+resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully2"), PRODUCT_NAME +" "+ resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully3"));
            }
        	String indicesInfo = "Index Details \n";
        	boolean isIndexCreated = false;
            for(int k=0;k<dtindexCount;k++) {
                Index index=(Index)indicesList.get(k);
                if(index.getId()==0) {
                    index.setId(updateIndex(sid, index));
                    indicesTable.put(index.getName(),String.valueOf(index.getId()));
                    //Util.Msg(null,"Index < "+ index.getName() + " > created successfully.","ViewWise Restore");
                    indicesInfo += "< " +  index.getName() + "> \n";
                    isIndexCreated = true;
                }
                index=updateIndexToDTIndex(index,k+1,l+1);
                if(checkDTIndexNameIsExists(sid, docType.getId(), index.getId())==0)
                    updateDTIndex(sid, docType.getId(), index, k+1);
            }
            if (isIndexCreated)
    		Util.Msg(null,resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully1")+" "
            + docType.getName() +" "+resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully2") 
            + indicesInfo, PRODUCT_NAME +" "+ resourceManager.getString("RestoreMsg.DocTypeCreatedSucessfully3"));
            if(!docType.getTo().equals("") ||
            !docType.getCc().equals("") ||
            !docType.getSubject().equals("")) {
                String autoMail_To=docType.getTo();
                int sepIndex=0;
                if((sepIndex=autoMail_To.indexOf("|"))>0) {
                    String indexName=autoMail_To.substring(0,sepIndex);
                    for(int s=0;s<dtindexCount;s++) {
                        Index index=(Index)indicesList.get(s);
                        if(indexName.equalsIgnoreCase(index.getName())) {
                            docType.setTo(index.getName()+"|"+index.getId());
                            break;
                        }
                    }
                }
                String autoMail_Cc=docType.getCc();
                sepIndex=0;
                if((sepIndex=autoMail_Cc.indexOf("|"))>0) {
                    String indexName=autoMail_Cc.substring(0,sepIndex);
                    for(int s=0;s<dtindexCount;s++) {
                        Index index=(Index)indicesList.get(s);
                        if(indexName.equalsIgnoreCase(index.getName())) {
                            docType.setCc(index.getName()+"|"+index.getId());
                            break;
                        }
                    }
                }
                String autoMail_Subject=docType.getSubject();
                sepIndex=0;
                if(autoMail_Subject.indexOf("|")>0) {
                    StringTokenizer tokens = new StringTokenizer(autoMail_Subject,"|",false);
                    autoMail_Subject="";
                    while (tokens.hasMoreTokens()) {
                        String indexName=tokens.nextToken();
                        indexName=indexName.substring(1,indexName.length()-1);
                        for(int s=0;s<dtindexCount;s++) {
                            Index index=(Index)indicesList.get(s);
                            if(indexName.equalsIgnoreCase(index.getName())) {
                                autoMail_Subject+="["+index.getName()+"]"+"|"+index.getId()+"|";
                                break;
                            }
                        }
                        tokens.nextToken();
                    }
                    if(autoMail_Subject.endsWith("|"))
                        autoMail_Subject=autoMail_Subject.substring(0,autoMail_Subject.length()-1);
                    docType.setSubject(autoMail_Subject);
                }
                if(addNewDocType) 
                    vwClient.setDocTypeAutoMail(sid, docType);
            }
            if(addNewDocType) {
                String param=String.valueOf(docType.getId())+Util.SepChar+
                docType.getVREnable()+Util.SepChar+
                docType.getVRInitVersion()+Util.SepChar+
                docType.getVRInitRevision()+Util.SepChar+
                docType.getVRPagesChange()+Util.SepChar+
                docType.getVRIncPagesChange()+Util.SepChar+
                docType.getVRPageChange()+Util.SepChar+
                docType.getVRIncPageChange()+Util.SepChar+
                docType.getVRPropertiesChange()+Util.SepChar+
                docType.getVRIncPropertiesChange()+Util.SepChar;
                Vector result=new Vector();
                vwClient.setDTVersionSettings(sid, docType);
                ///vwClient.adminWise(sid, "VR_SetSettings", param, result);
            }
        }

        }
/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
Set the restored document type status to docTypeStatus object.
*/    
        if(docTypeId>0){
        	if (indexInfo == null) indexInfo = new HashMap();
        	indexInfo.put(docType.getName(),indicesList);
        	docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_SUCCESS));              
        }
        else
        	docTypeStatus.put(docType.getName(),new Integer(DOCUMENT_TYPE_FAILED));
        return docTypeId;
    }
    //--------------------------------------------------------------------------
    public int addDocNodes(int sid) {
        String prefix="VWDocument\\";
        String path=getValue(prefix+"Path");
        String pathIds=getValue(prefix+"PathIDs");
        StringTokenizer tokens = new StringTokenizer(path, Util.SepChar, false);
        StringTokenizer tokensIds = new StringTokenizer(pathIds, Util.SepChar, false);
        int nodeId=0;
        int nodeCount=tokens.countTokens()-1;
        if(nodeCount<=0) {
            tokens = new StringTokenizer(path,"\\",false);
            nodeCount=tokens.countTokens()-1;
        }
        for(int i=0;i<nodeCount;i++) {
            String nodeName=tokens.nextToken();
            Node node=new Node(0);
            node.setName(nodeName);
            node.setParentId(nodeId);            
            if (restoreType == 5){
        	try{
        	    nodeId = Integer.parseInt(tokensIds.nextToken().trim());
        	}catch(Exception ex){
        	    nodeId=addUniqueNode(sid, node, !restoreWithFolders);
        	}
            }else                        
        	nodeId=addUniqueNode(sid, node, !restoreWithFolders);
            if(nodeId <= 0) return 0;
        }
        return nodeId;
    }
    //--------------------------------------------------------------------------
    public int addUniqueNode(int sid, Node node,boolean checkOnly) {
        Vector result=new Vector();
        return vwClient.createNode(sid, node, (checkOnly?2:1));
    }
    //--------------------------------------------------------------------------
  //Modified for Change Document Type Enhancement,Gurumurthy.T.S
    private Index getIndex(int indexNum,int loopCount) {
    	// We are reading NewInfo and NewMask for setInfo and setMast, now we are checking the info and mask value with the document type indices. 
    	// info and mask are from index table and newinfo and newmask from document type indices table  
        String prefix="VWDocument\\DocType\\Index\\";
      //Modified for Change Document Type Enhancement,Gurumurthy.T.S
        Index index =null;
        if(loopCount==0){
        index = new Index(0);
        //
        index.setName(getValue(prefix+"Name"+indexNum));
        index.setType(getIntValue(prefix+"Type"+indexNum));
        index.setMask(getValue(prefix+"NewMask"+indexNum));
        index.setDef(getValue(prefix+"Default"+indexNum));
        index.setSystem(getIntValue(prefix+"SVid"+indexNum));
        index.setKey(getValue(prefix+"DTKey"+indexNum).equalsIgnoreCase("true"));
        index.setRequired(getValue(prefix+"Required"+indexNum).equalsIgnoreCase("true"));
        index.setDescription(getValue(prefix+"Description"+indexNum));
        index.setInfo(getValue(prefix+"NewInfo"+indexNum));
        if(index.getType()==4)//Selection type
        {
            ///index.setActDef(()index.getDef().get(0));
            StringTokenizer tokens = new StringTokenizer
            (getValue(prefix+"SelectionValues"+indexNum),"|",false);
            Vector values=new Vector();
            while(tokens.hasMoreTokens())   values.add(tokens.nextToken());
            index.setDef(values);
        }
      //Added for Change Document Type Enhancement,Gurumurthy.T.S
        }
        else{
            index = new Index(0);
            index.setName(getValue(prefix+"Name"+(indexNum)+(loopCount)));
            index.setType(getIntValue(prefix+"Type"+(indexNum)+(loopCount)));
            index.setMask(getValue(prefix+"NewMask"+(indexNum)+(loopCount)));
            index.setDef(getValue(prefix+"Default"+(indexNum)+(loopCount)));
            index.setSystem(getIntValue(prefix+"SVid"+(indexNum)+(loopCount)));
            index.setKey(getValue(prefix+"DTKey"+(indexNum)+(loopCount)).equalsIgnoreCase("true"));
            index.setRequired(getValue(prefix+"Required"+(indexNum)+(loopCount)).equalsIgnoreCase("true"));
            index.setDescription(getValue(prefix+"Description"+(indexNum)+(loopCount)));
            index.setInfo(getValue(prefix+"NewInfo"+(indexNum)+(loopCount)));
            if(index.getType()==4)//Selection type
            {
                ///index.setActDef(()index.getDef().get(0));
                StringTokenizer tokens = new StringTokenizer
                (getValue(prefix+"SelectionValues"+indexNum),"|",false);
                Vector values=new Vector();
                while(tokens.hasMoreTokens())   values.add(tokens.nextToken());
                index.setDef(values);
            }
        }
        return index;
    }
    //--------------------------------------------------------------------------
  //Modified for Change Document Type Enhancement,Gurumurthy.T.S
    private DocType getDocType(int id) {
    	String prefix="";
    	 DocType docType=new DocType(0);
    	if(id==0){
    //
    		prefix="VWDocument\\DocType\\";
            docType.setName(getValue(prefix+"Name"));
            docType.setTo(getValue(prefix+"AutoMail_To"));
            docType.setCc(getValue(prefix+"AutoMail_Cc"));
            docType.setSubject(getValue(prefix+"AutoMail_Subject"));
            docType.setEnableTextSearch(getValue(prefix+"EnableSIC").equalsIgnoreCase("true"));
            docType.setVREnable(getValue(prefix+"VREnable"));
            docType.setVRInitVersion(getValue(prefix+"VRInitVersion"));
            docType.setVRInitRevision(getValue(prefix+"VRInitRevision"));
            docType.setVRPagesChange(getValue(prefix+"VRPagesChange"));
            docType.setVRIncPagesChange(getValue(prefix+"VRIncPagesChange"));
            docType.setVRPageChange(getValue(prefix+"VRPageChange"));
            docType.setVRIncPageChange(getValue(prefix+"VRIncPageChange"));
            docType.setVRPropertiesChange(getValue(prefix+"VRPropertiesChange"));
            docType.setVRIncPropertiesChange(getValue(prefix+"VRIncPropertiesChange"));
       
        //Added for Change Document Type Enhancement,Gurumurthy.T.S
    	}
    	else{
    		prefix="VWDocument\\DocType\\";
        docType.setName(getValue(prefix+"Name"+id));
        docType.setTo(getValue(prefix+"AutoMail_To"+id));
        docType.setCc(getValue(prefix+"AutoMail_Cc"+id));
        docType.setSubject(getValue(prefix+"AutoMail_Subject"+id));
        docType.setEnableTextSearch(getValue(prefix+"EnableSIC"+id).equalsIgnoreCase("true"));
        docType.setVREnable(getValue(prefix+"VREnable"+id));
        docType.setVRInitVersion(getValue(prefix+"VRInitVersion"+id));
        docType.setVRInitRevision(getValue(prefix+"VRInitRevision"+id));
        docType.setVRPagesChange(getValue(prefix+"VRPagesChange"+id));
        docType.setVRIncPagesChange(getValue(prefix+"VRIncPagesChange"+id));
        docType.setVRPageChange(getValue(prefix+"VRPageChange"+id));
        docType.setVRIncPageChange(getValue(prefix+"VRIncPageChange"+id));
        docType.setVRPropertiesChange(getValue(prefix+"VRPropertiesChange"+id));
        docType.setVRIncPropertiesChange(getValue(prefix+"VRIncPropertiesChange"+id));
    	}
        return docType;
    }
    //--------------------------------------------------------------------------
    private Index updateIndexToDTIndex(Index index,int indexNum,int loopCount) {
    	String prefix ="VWDocument\\DocType\\Index\\";
        String recStr=getValue(prefix+"NewMask"+(indexNum)+(loopCount));
        recStr=recStr==null?"":recStr;
        index.setMask(recStr);
        
        recStr=getValue(prefix+"NewDefault"+(indexNum)+(loopCount));
        recStr=recStr==null?"":recStr;
        index.setDef(recStr);
        
        recStr=getValue(prefix+"NewInfo"+(indexNum)+(loopCount));
        recStr=recStr==null?"":recStr;
        index.setInfo(recStr);
        
        recStr=getValue(prefix+"NewDescription"+(indexNum)+(loopCount));
        recStr=recStr==null?"":recStr;
        index.setDescription(recStr);
        
        return index;
    }
    //--------------------------------------------------------------------------
    private String getValue(String key) {
        String value ="";
        try{
            value=VWUtil.fixXMLEntityString((String)documentInfo.get(key));
            if(value==null) value="";
            return value;
        }
        catch(Exception e){}
        return "";
    }
    //--------------------------------------------------------------------------
    private int getIntValue(String key) {
        String value="";
        try{
            value=(String)documentInfo.get(key);
            if(value==null) return 0;
            return Util.to_Number(value);
        }
        catch(Exception e){}
        return 0;
    }
    //--------------------------------------------------------------------------
    private int uploadFiles(int sid, Document doc) {
        File pageFiles=new File(restoreDocFile.getParentFile(),"Pages");
        String prefix="VWDocument\\";
        String pagesChanged=getValue(prefix+"PagesChanged");
        String pageChanged=getValue(prefix+"PageChanged");
        String pageCount=getValue(prefix+"DocPageCount");
        int retVal=0;
        if(pagesChanged.equals("1") || pageChanged.equals("1"))
        {
            String pageInfo="0"+Util.SepChar+
                pageCount+Util.SepChar+
                pagesChanged+Util.SepChar+
                pageChanged;
            retVal=vwClient.setDocFile(sid, doc, pageFiles.getPath(), false,
                            false, pageInfo,true);
        }
        else
        {
            retVal=vwClient.setDocFolder(sid, doc, pageFiles.getPath());
        }
        return retVal;
    }
    //--------------------------------------------------------------------------
    public int checkDTIndexNameIsExists(int sid, int docTypeId,int indexId) {
        Vector result=new Vector();
        
        vwClient.adminWise(sid,"CheckDocTypeIndex_Isfound",
        String.valueOf(docTypeId)+Util.SepChar+String.valueOf(indexId),result);
        if(result==null || result.size()==0) return 0;
        int dtIndexId=Util.to_Number((String)result.get(0));
        return dtIndexId;
    }
    //--------------------------------------------------------------------------
    public int getRefrencesNodeId(int sid, String refrencesDocPath) {
        StringTokenizer tokens = new StringTokenizer(refrencesDocPath,"\\",false);
        int nodeId=0;
        int nodeCount=tokens.countTokens();
        for(int i=0;i<nodeCount;i++) {
            String nodeName=tokens.nextToken();
            Node node =new Node(nodeId,nodeName);
            nodeId=addUniqueNode(sid, node, true);
            if(nodeId<=0)   return 0;
        }
        return nodeId;
    }
    //--------------------------------------------------------------------------
    public int checkDocTypeIsFound(int sid, String docTypeName) {
        Vector result=new Vector();
        return vwClient.isDocTypeFound(sid,new DocType(0,docTypeName));
    }
    //--------------------------------------------------------------------------
    public int checkIndexIsFound(int sid, Index index, DocType docType) {
        return vwClient.isIndexFound(sid, index, 0, docType);
    }
    //--------------------------------------------------------------------------
    public int updateDocType(int sid, DocType docType) {
        try{
        	int ret = vwClient.updateDocType(sid, docType);
        	try{
        		if(docType.getId()==0){
        			//Notification - Restore module
        			String message = "Document Type '"+docType.getName()+"' created while restoring document.";
        			vwClient.addNotificationHistory(sid, 0, VNSConstants.nodeType_RestoreDocs, VNSConstants.notify_Restore_NewDT, message);
        		}
        	}catch(Exception ex){}
            return ret;
        }
        catch(Exception e){};
        return 0;
    }
    //--------------------------------------------------------------------------
    public int updateDTIndex(int sid, int docTypeId, Index newIndex, int indexOreder) {
        newIndex.setOrder(indexOreder);
        
        try{
            int ret = vwClient.updateDTIndex(sid, docTypeId, newIndex);
            if(newIndex.getDTIndexId()==0){
            	//Notification - index creation while restoring the document
            	String message = "Index '"+newIndex.getName()+"' created while restoring document";
            	vwClient.addNotificationHistory(sid, 0, VNSConstants.nodeType_RestoreDocs, VNSConstants.notify_Restore_NewIndex, message);
            }
            return ret;
        }
        catch(Exception e){};
        
        return 0;
    }
    //--------------------------------------------------------------------------
    public int updateIndex(int sid, Index newIndex) {
        int newId=0;
        try{
            newId=vwClient.updateIndex(sid, newIndex);
        }
        catch(Exception e){
            newId=0;
        };
        newIndex.setId(newId);
        if(newId>0 && newIndex.getType()==4)
            updateIndexSelectionValues(sid,newIndex);
        return newId;
    }
    //--------------------------------------------------------------------------
    public void updateIndexSelectionValues(int sid, Index index) {
        if(index.getDef()==null) return;
        /**
         * Code commented for selection list optimization. 
         */
        /*
         String strValues="";
         Vector result=new Vector();
         for (int i=0;i<index.getDef().size()-1;i++)
            strValues+=(String)index.getDef().get(i)+"|";
         strValues+=(String)index.getDef().get(index.getDef().size()-1);
         vwClient.updateIndexSelection(sid, index, strValues);
        */
        vwClient.updateIndexSelection(sid, index, index.getDef());
    }
    //--------------------------------------------------------------------------
    public void addComments(int sid, int nodeId)
    {
        int count=Util.to_Number(getValue("VWDocument\\DocComments\\CommentCount"));
        /*Issue #776 When a comment is added to a Checked out Document in MiniWise, 
        * and then the document is checked in, the comment column does not increment
        * C.Shanmugavalli Date 22 Nov 2006
        */
        Vector existingComments = new Vector();
        // Delete existing comments and add comments from xml file.
        vwClient.getDocumentComment(sid, new Document(nodeId), existingComments);
		for(int i=0;i<existingComments.size();i++)
		{
	        DocComment docCmt=(DocComment)existingComments.get(i);
	        vwClient.deleteDocumentComment(sid, docCmt);
		}
        for(int indexNum=1;indexNum<=count;indexNum++)
        {
            String prefix="VWDocument\\DocComments\\DocComment\\";
            DocComment docComment = new DocComment();
            docComment.setDocId(nodeId);
            docComment.setComment(getValue(prefix+"Comment"+indexNum));
            docComment.setUser(getValue(prefix+"UserName"+indexNum));
            docComment.setTimeStamp(getValue(prefix+"Created"+indexNum));
            docComment.setPublic(getValue(prefix+"Scope"+indexNum).equalsIgnoreCase("true"));       
            vwClient.setDocumentComment(sid, docComment, true,1);
        }
    }
    //----------------------------------------------------------------------
    /*
	 * Implemented to check whether the index field mathes in the Backup set and Restoring room,
	 * Before Restoration. Gurumurthy.T.S.
	 * Bug 206 CV8B5-002
	 */
    public int restoreIndicesCheck(int sid,DocType docType, int dtindexCount,
    		Vector restoreIndiceCheck, Vector resultVector) {
    	return vwClient.checkRestoreIndices(sid,docType,dtindexCount,restoreIndiceCheck,resultVector);
    }
}