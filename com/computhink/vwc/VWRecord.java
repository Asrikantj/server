/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWRecord<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package com.computhink.vwc;

//import ViewWise.WebClient.VWConstant;
import com.computhink.vwc.VWStringTokenizer;

public class VWRecord implements java.io.Serializable
{
	public static String SepChar=(new Character((char)9)).toString();
	
    public VWRecord(String recordString)
    {
        VWStringTokenizer tokens = new VWStringTokenizer(recordString,SepChar,false);
        if(tokens.countTokens() >= 2)
        {
            id = VWUtil.to_Number(tokens.nextToken());
            name= tokens.nextToken();
        }
        if(tokens.countTokens() >= 3)
        {
            secName= tokens.nextToken();
        }
}
//------------------------------------------------------------------------------
public VWRecord(String recordString,int type)
    {
        VWStringTokenizer tokens = new VWStringTokenizer(recordString,SepChar,false);
        String str=tokens.nextToken();
        secName=str.substring(0,2).trim();
        name=str.substring(2);
        id = VWUtil.to_Number(tokens.nextToken().trim());
}
//------------------------------------------------------------------------------
    public VWRecord(int recId,String recName)
    {
        id = recId;
        name= recName;
    }
//------------------------------------------------------------------------------
    public VWRecord(int recId,String recName,String recSecName)
    {
        id = recId;
        name= recName; 
        secName=recSecName;
    }
//------------------------------------------------------------------------------
    public int getId()
    {
        return id;
    }
//------------------------------------------------------------------------------
    public String getName()
    {
        return name;
    }
//------------------------------------------------------------------------------
    public String getSecName()
    {
        return secName;
    }
//------------------------------------------------------------------------------
    public void setId(int recId)
    {
        id=recId;
    }
//------------------------------------------------------------------------------
    public void setName(String recName)
    {
        name=recName;
    }
//------------------------------------------------------------------------------
    public void setSecName(String recName)
    {
        secName=recName;
    }
//------------------------------------------------------------------------------
    public String toString()
    {
        return this.name;
    }
//------------------------------------------------------------------------------
    private int id=0;
    private String name="";
    private String secName="";
}