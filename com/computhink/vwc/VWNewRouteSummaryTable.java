package com.computhink.vwc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.AbstractCellEditor;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelEvent;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;
import com.computhink.vwc.VWTableResizer;
import com.computhink.vwc.doctype.VWUtil.IconData;
import com.computhink.vwc.image.Images;
import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.common.util.VWPrint;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.util.List;
import java.util.Objects;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

//--------------------------------------------------------------------------
public class VWNewRouteSummaryTable extends JTable implements VWCConstants {
	protected VWNewRouteSummaryTableData m_data;
	public int ttlColumns=0;
	public int summaryColumn=0;
	public String documentName = "";
	public VWNewRouteSummary parent = null;
	public VWNewRouteSummaryTable(){
	    //this(new VWNewRouteSummary(0), "");
		new Images();
	}
	public VWNewRouteSummaryTable(VWNewRouteSummary vwRouteSummary1, String documentName){
		super();
		new Images();
		this.documentName = documentName;
		this.parent = vwRouteSummary1;
		getTableHeader().setReorderingAllowed(false);
		m_data = new VWNewRouteSummaryTableData(this);
		setCellSelectionEnabled(false);
		setAutoCreateColumnsFromModel(false);
		setModel(m_data);
		setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
//		/setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		Dimension tableWith = getPreferredScrollableViewportSize();
		VWClient.printToConsole("Before loading VWNewRouteSummaryTableData.....");
		for (int k = 0; k < VWNewRouteSummaryTableData.m_columns.length; k++) {
			TableCellRenderer renderer;
			
			DefaultTableCellRenderer textRenderer;
			if(VWRouteSummary.isWebClient)
				textRenderer =new DefaultTableCellRenderer();
			else
				textRenderer =new VWTableCellRenderer1();
			
			textRenderer.setHorizontalAlignment(VWNewRouteSummaryTableData.m_columns[k].m_alignment);
			renderer = textRenderer;
			VWNewRouteSummaryRowEditor editor=new VWNewRouteSummaryRowEditor(this);
			TableColumn column = new TableColumn(k,Math.round(tableWith.width*VWNewRouteSummaryTableData.m_columns[k].m_width),renderer,editor);
			addColumn(column);
		}
		VWClient.printToConsole("After loading VWNewRouteSummaryTableData column .....");
		ttlColumns = getColumnModel().getColumnCount();
		summaryColumn = ttlColumns-1;
		getColumnModel().getColumn((summaryColumn)).setPreferredWidth(130);
		int statusColumn = ttlColumns-2;
		getColumnModel().getColumn((statusColumn)).setPreferredWidth(130);
		
	
		//new MultiLineCellRenderer(this.getPreferredSize())
		setRowHeight(20);
		getColumnModel().getColumn(m_data.COL_USERNAME).setCellRenderer(new MultiLineCellRenderer());
		getColumnModel().getColumn(m_data.COL_RECEIVEDON).setCellRenderer(new MultiLineCellRenderer());
		getColumnModel().getColumn(m_data.COL_VersionofNote).setCellRenderer(new MultiLineCellRenderer());
		getColumnModel().getColumn(m_data.COL_Comments).setCellRenderer(new MultiLineCellRenderer());
		getColumnModel().getColumn(m_data.COL_ForwardTo).setCellRenderer(new MultiLineCellRenderer());
		getColumnModel().getColumn(m_data.COL_ActionDt).setCellRenderer(new MultiLineCellRenderer());
		getColumnModel().getColumn(m_data.COL_Action).setCellRenderer(new MultiLineCellRenderer());
		getColumnModel().getColumn(m_data.COL_NumberofRefernce).setCellRenderer(new MultiLineCellRenderer());
		setRowSelectionAllowed(true);
		VWClient.printToConsole("After loading VWNewRouteSummaryTableData.....");
		
		if(!VWRouteSummary.isWebClient)
			setBackground(java.awt.Color.white);
		//To get horizontal scroll bar
		//setAutoResizeMode(JTable.AUTO_RESIZE_OFF);  //commented by Srikanth on 16 Nov 2015 for resizing all column
		setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS); //added by Srikanth on 16 Nov 2015 for resizing all column
		SymMouse aSymMouse = new SymMouse();
		addMouseListener(aSymMouse);

		JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
        this.getTableHeader().setDefaultRenderer(new SimpleHeaderRenderer());
        for(int i=0;i< this.getTableHeader().getColumnModel().getColumnCount();i++){
            this.getTableHeader().getColumnModel().getColumn(i).setHeaderRenderer(new SimpleHeaderRenderer());
    	}
        VWClient.printToConsole("After loading VWNewRouteSummaryTable header.....");
        /*this.setTableHeader(new JTableHeader(this.getColumnModel()) {
        	  @Override public Dimension getPreferredSize() {
        	    Dimension d = super.getPreferredSize();
        	    d.width=50;
        	    d.height = 80;
        	    return d;
        	  }
        	});*/
        setTableResizable();        
	}
	public void showPopupMenu(MouseEvent e){
	    JPopupMenu menu = new JPopupMenu();
	    JMenuItem export = new JMenuItem("Export", Images.export);
	    JMenuItem print = new JMenuItem("Print "+ Constants.WORKFLOW_MODULE_NAME +" Summary", Images.print);
	    JMenuItem taskDetails = new JMenuItem("Task Details", Images.taskDetails);
	    JMenuItem close = new JMenuItem("Close", Images.taskClose);
	    
	    menu.add(export);
	    menu.add(print);
	    menu.add(taskDetails);
	    menu.add(close);	    



	    
	    export.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		      parent.doExport();
		  }
	    });
	    print.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		      parent.doPrint();
		  }
	    });
	    taskDetails.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		      new VWNewRouteSummaryDetails(parent.dlgMain, parent);
		  }
	    });

	    close.addActionListener(new ActionListener(){
		  public void actionPerformed(ActionEvent e){
		      parent.closeDialog();
		  }
	    });
	    menu.show(this, e.getX(), e.getY());
	}
	public void setTableResizable() {
	// Resize Table
	new VWTableResizer(this);
	}	
	
    //Desc   :This function shows the Task Description as tooltip, whenever mouse is pointed to task column.
    //Author :Nishad Nambiar
    //Date   :7-May-2007
    public String getToolTipText(MouseEvent e) {
        String tip = null;
        java.awt.Point p = e.getPoint();
        int rowIndex = rowAtPoint(p);
        int colIndex = columnAtPoint(p);
        int realColumnIndex = convertColumnIndexToModel(colIndex);
        RouteSummaryRowData1 row = (RouteSummaryRowData1) m_data.m_vector.elementAt(rowIndex);        
       //comment on 27 August madhavan
        /* if (realColumnIndex == m_data.COL_TASK) { 
            tip = " " + " " + row.m_TaskDesc;
        }*/
        return tip;
    }
    

//	--------------------------------------------------------------------------
	class SymMouse extends java.awt.event.MouseAdapter{
		public void mouseClicked(java.awt.event.MouseEvent event) {
			Object object = event.getSource();
			if (object instanceof JTable)
				if(event.getModifiers()==event.BUTTON3_MASK)
					VWDocRouteTable_RightMouseClicked(event);
				else if(event.getModifiers()==event.BUTTON1_MASK)
					VWDocRouteTable_LeftMouseClicked(event);
		}
	}
//	--------------------------------------------------------------------------
	void VWDocRouteTable_LeftMouseClicked(java.awt.event.MouseEvent event) {
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			lSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			lDoubleClick(event,row,column);
	}
//	--------------------------------------------------------------------------
	void VWDocRouteTable_RightMouseClicked(java.awt.event.MouseEvent event) {
		Point origin = event.getPoint();
		int row = rowAtPoint(origin);
		int column = columnAtPoint(origin);
		if (row == -1 || column == -1)
			return; // no cell found
		if(event.getClickCount() == 1)
			rSingleClick(event,row,column);
		else if(event.getClickCount() == 2)
			rDoubleClick(event,row,column);
	}
//	--------------------------------------------------------------------------
	private void rSingleClick(java.awt.event.MouseEvent event,int row,int col) {
	    showPopupMenu(event);
	}
//	--------------------------------------------------------------------------
	private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
		
	}
//	--------------------------------------------------------------------------
	private void lSingleClick(java.awt.event.MouseEvent event,int row,int col) {
		
	}
//	--------------------------------------------------------------------------
	private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col) {
	    new VWNewRouteSummaryDetails(parent.dlgMain, parent);
	}
//	--------------------------------------------------------------------------
	//Desc   :Select the last processed row in the table.
	//Author :Nishad Nambiar
	//Date   :21-Aug-2008 
	public void setLastProcessedRowSelected() {
		int rowToSelect = -1;
		if(m_data.m_vector!=null && m_data.m_vector.size()>0){
			for(int count=(m_data.m_vector.size()-1);count>=0;count--){
				try{
					RouteSummaryRowData1 row = (RouteSummaryRowData1)m_data.m_vector.elementAt(count);
					//comment on 27 Aug madhavan
					/*	if(!row.m_ProcessedOn.toString().trim().equalsIgnoreCase("") && !row.m_ProcessedOn.toString().trim().equalsIgnoreCase("-")){									
						rowToSelect = count;
						break;
					}*/
				}catch(Exception e){
					//System.out.println("EXCEPTION :"+e.toString());
				}
			}		
		}
		if(rowToSelect >=0)
			setRowSelectionInterval(rowToSelect, rowToSelect);
	}
//		--------------------------------------------------------------------------
		public void addData(List list) {
		m_data.setData(list);
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public void addData(String[][] data) {
		m_data.setData(data);
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public String[][] getData() {
		return m_data.getData();
	}
//	--------------------------------------------------------------------------
	public void removeData(int row) {
		m_data.remove(row);
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	
	public void insertData(String[][] data) {
		for(int i=0;i<data.length;i++) {
			m_data.insert(new RouteSummaryRowData1(data[i]));
		}
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
	public String[] getRowData(int rowNum) {
		return m_data.getRowData(rowNum);
	}
//	--------------------------------------------------------------------------
	public int getRowDocLockId(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return Util.to_Number((row[0]));
	}
//	--------------------------------------------------------------------------
	public String getRowDocName(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return row[1];
	}
//	--------------------------------------------------------------------------
	public int getRowType() {
		return getRowType(getSelectedRow());
	}
//	--------------------------------------------------------------------------
	public int getRowType(int rowNum) {
		String[] row=m_data.getRowData(rowNum);
		return Util.to_Number((row[6]));
	}
//	--------------------------------------------------------------------------
	public void clearData() {
		m_data.clear();
		m_data.fireTableDataChanged();
	}
//	--------------------------------------------------------------------------
}
class RouteSummaryRowData1 {
	//public String 	m_SerailNumber;
	

	public static final int COL_USERNAME = 0;
	public static final int COL_RECEIVEDON= 1;
	public static final int COL_Action= 2;
	public static final int COL_ActionDt= 3;
	public static final int COL_Comments = 4;
	public static final int COL_VersionofNote = 5;
	//public static final int COL_NumberofPages= 6;
	public static final int COL_ForwardTo = 6;
	public static final int COL_NumberofRefernce= 7;
	public String   m_UserName;
	public String   m_ReceivedOn;
	public String   m_Action;
	public String   m_ActionDt;
	public String   m_Comments;
	public String   m_VersionofNote;
	//	public String   m_NumberofPages;
	public String   m_ForwardTo;
	public String   m_NumberofReference;
	
	
	public RouteSummaryRowData1() {
	//	m_SerailNumber="";
		m_UserName="";
		m_ReceivedOn="";
		m_Action="";
		m_ActionDt="";
		m_Comments="";
		m_VersionofNote="";
	//	m_NumberofPages = "";
		m_ForwardTo = "";
		m_NumberofReference = "";
		//m_UserStatus = "";
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData1(String userName, String receivedOn, String action,
			String actionDt,  String comments, String task, String taskDesc, String signType, String serialNumber, String userStatus) {
		//m_SerailNumber = serialNumber;
		m_UserName = userName;
		m_ReceivedOn = receivedOn;
		m_Action = action;
		m_ActionDt = actionDt;
		m_Comments = comments;
		m_VersionofNote = task;
		//m_NumberofPages = taskDesc;
		m_ForwardTo = signType;
		m_NumberofReference = userStatus;
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData1(String str) {
		StringTokenizer tokens=new StringTokenizer(str,Util.SepChar,false);		
		//System.out.println("Token size " + tokens.countTokens());
		try {	
		//	m_SerailNumber = tokens.nextToken();	
		//	m_RouteName = tokens.nextToken();
			//VWClient.printToConsole("tokens count is ::::"+tokens.countTokens());
			m_UserName = tokens.nextToken();
			//VWClient.printToConsole("m_UserName :::::"+m_UserName);
			m_ReceivedOn=tokens.nextToken();		
			//VWClient.printToConsole("m_ReceivedOn :::::"+m_ReceivedOn);

			m_Action=Util.getFixedValue(tokens.nextToken(),"");
			//VWClient.printToConsole("m_Action :::::"+m_Action);
			m_ActionDt=Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"","");
			//VWClient.printToConsole("m_ActionDt :::::"+m_ActionDt);
			m_Comments=Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"","");
			//VWClient.printToConsole("m_Comments :::::"+m_Comments);
			m_VersionofNote=Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"","");
			//VWClient.printToConsole("m_VersionofNote :::::"+m_VersionofNote);
			m_ForwardTo = Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"","");
			//VWClient.printToConsole("m_ForwardTo :::::"+m_ForwardTo);
			m_NumberofReference = Util.getFixedValue(tokens.hasMoreTokens()?tokens.nextToken():"",""); 
			//VWClient.printToConsole("m_NumberofReference :::::"+m_NumberofReference);
			/*if(m_Status.contains("Rejected"))
				sign = "-";
			m_SignType = sign.equals("1")?"Authorized":(sign.equals("2")?"Certified":"-");
			m_UserStatus = tokens.nextToken();*/

		}catch(Exception ex){
			VWClient.printToConsole("Exception in routesummary new:::: "+ex.getMessage());
			//System.out.println(m_RouteName + " ::: "+  m_UserName + " ::: " + m_ReceivedOn +  " ::: " + m_Status +   " ::: " +  m_ProcessedOn + " ::: "  +  m_Comments); 					
			//System.out.println(" RouteSummaryRowData Exception  " + ex.getMessage() );
		}
	}
//	--------------------------------------------------------------------------------
	public RouteSummaryRowData1(String[] data) {
		int i=0;
		//m_SerailNumber = data[i++];
		m_UserName = data[i++];
		m_ReceivedOn = data[i++];		
		m_Action = data[i++];
		m_ActionDt = data[i++];
		m_Comments = data[i++];
		m_VersionofNote = data[i++];
		m_ForwardTo = data[i++];
		m_NumberofReference = data[i++];
		//m_UserStatus = data[i++];
	}
}
//--------------------------------------------------------------------------
class RouteSummaryColumnData1 {
	public String  m_title;
	float m_width;
	int m_alignment;
	
	public RouteSummaryColumnData1(String title, float width, int alignment) {
		m_title = title;
		m_width = width;
		m_alignment = alignment;
	}
}
//--------------------------------------------------------------------------
class VWNewRouteSummaryTableData extends AbstractTableModel {
	public static final RouteSummaryColumnData1 m_columns[] = {
	//	new RouteSummaryColumnData(VWCConstants.RouteColNames[0],0.1F,JLabel.LEFT ),
		new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[0],0.35F, JLabel.LEFT),
		new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[1],0.25F,JLabel.LEFT),
		new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[2],0.17F,JLabel.LEFT),
		new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[3],0.25F,JLabel.LEFT),
		new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[4],0.25F,JLabel.LEFT),
		new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[5],0.25F,JLabel.LEFT),
		new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[6],0.1F,JLabel.LEFT),
		new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[7],0.2F,JLabel.LEFT),
		//new RouteSummaryColumnData1(VWCConstants.NewRouteColNames[8],0.2F,JLabel.LEFT ),
	//	new RouteSummaryColumnData1(VWCConstants.RouteColNames[9],0.2F,JLabel.LEFT ),
	};
	//public static final int COL_SERIALNUMBER = 0;
	public static final int COL_USERNAME = 0;
	public static final int COL_RECEIVEDON= 1;
	public static final int COL_Action= 2;
	public static final int COL_ActionDt= 3;
	public static final int COL_Comments = 4;
	public static final int COL_VersionofNote = 5;
	//public static final int COL_NumberofPages= 6;
	public static final int COL_ForwardTo = 6;
	public static final int COL_NumberofRefernce= 7;
//	public static final int COL_USERSTATUS = 9;

	// protected int m_sortCol = 0;
	 protected boolean m_sortAsc = true;
	
	protected VWNewRouteSummaryTable m_parent;
	protected Vector m_vector;
	
	public VWNewRouteSummaryTableData(VWNewRouteSummaryTable parent) {
		m_parent = parent;
		m_vector = new Vector();
	}
//	--------------------------------------------------------------------------
	
	
	public void setData(List data) {
		VWClient.printToConsole("dataList inside setData ...."+data);
		m_vector.removeAllElements();
		if (data==null) return;
		int count =data.size();
		for(int i=0;i<count;i++)
			m_vector.addElement(new RouteSummaryRowData1((String)data.get(i)));
	}
//	--------------------------------------------------------------------------
	public void setData(String[][] data) {
		m_vector.removeAllElements();
		int count =data.length;
		for(int i=0;i<count;i++) {
			RouteSummaryRowData1 row =new RouteSummaryRowData1(data[i]);
			if(i==2)
				m_vector.addElement(new Boolean(true));
			else
				m_vector.addElement(row);
		}
	}
//	--------------------------------------------------------------------------
	public String[][] getData() {
		int count=getRowCount();
		String[][] data=new String[count][VWCConstants.RouteColNames.length];
		for(int i=0;i<count;i++) {
			RouteSummaryRowData1 row=(RouteSummaryRowData1)m_vector.elementAt(i);
			//data[i][0]=row.m_SerailNumber;
			data[i][0]=row.m_UserName;
			data[i][1]=row.m_ReceivedOn;
			data[i][2]=row.m_Action;
			data[i][3]=row.m_ActionDt;
			data[i][4]=row.m_Comments;
			data[i][5]=row.m_VersionofNote;
			data[i][6]=row.m_ForwardTo;
			data[i][7]=row.m_NumberofReference;
			//data[i][8]=
			//data[i][9]=row.m_UserStatus;
			
		}
		return data;
	}
//	--------------------------------------------------------------------------
	public String[] getRowData(int rowNum) {
		String[] RouteSummaryRowData=new String[VWCConstants.RouteColNames.length];
		RouteSummaryRowData1 row=(RouteSummaryRowData1)m_vector.elementAt(rowNum);
	//	RouteSummaryRowData[0]=row.m_SerailNumber;
		RouteSummaryRowData[0]=row.m_UserName;
		RouteSummaryRowData[1]=row.m_ReceivedOn;

		RouteSummaryRowData[2]=row.m_Action;
		RouteSummaryRowData[3]=row.m_ActionDt;

		
		RouteSummaryRowData[4]=row.m_Comments;
		RouteSummaryRowData[5]=row.m_VersionofNote;
		RouteSummaryRowData[6]=row.m_ForwardTo;
		RouteSummaryRowData[7]=row.m_NumberofReference;
		//RouteSummaryRowData[8]=
		//RouteSummaryRowData[9]=row.m_UserStatus;
		
		return RouteSummaryRowData;
	}
	
	public RouteSummaryRowData1 getRowData0(int rowNum) {
		RouteSummaryRowData1 row=(RouteSummaryRowData1)m_vector.elementAt(rowNum);
		return row;
	}
	
//	--------------------------------------------------------------------------
	public int getRowCount() {
		return m_vector==null ? 0 : m_vector.size();
	}
//	--------------------------------------------------------------------------
	public int getColumnCount() {
		return m_columns.length;
	}
//	--------------------------------------------------------------------------
	public String getColumnName(int column) {
		String str = m_columns[column].m_title;
		//if (column==m_sortCol)
            str += m_sortAsc ? " �" : " �";
        return str;
	}
//	--------------------------------------------------------------------------
	public boolean isCellEditable(int nRow, int nCol) {
		return true;
	}
//	--------------------------------------------------------------------------
	public Object getValueAt(int nRow, int nCol) {
		if (nRow < 0 || nRow>=getRowCount())
			return "";
		RouteSummaryRowData1 row = (RouteSummaryRowData1)m_vector.elementAt(nRow);
	
		switch (nCol) {
		//case COL_SERIALNUMBER: return (row.m_SerailNumber.trim().equals("-"))?"":row.m_SerailNumber;
		case COL_USERNAME:	 return (row.m_UserName.trim().equals("-"))?"":row.m_UserName;
		//case COL_ROUTENAME:	 return (row.m_RouteName.trim().equals("-"))?"":row.m_RouteName;
		case COL_RECEIVEDON:     return (row.m_ReceivedOn.trim().equals("-"))?"":row.m_ReceivedOn;
		case COL_Action:     return (row.m_Action.trim().equals("-"))?"":row.m_Action;
		case COL_ActionDt:    return (row.m_ActionDt.trim().equals("-"))?"":row.m_ActionDt;
		case COL_Comments:    return (row.m_Comments.trim().equals("-"))?"":row.m_Comments;
		case COL_VersionofNote:   return (row.m_VersionofNote.trim().equals("-"))?"":row.m_VersionofNote;
		//case COL_NumberofPages:    return (row.m_NumberofPages.trim().equals("-"))?"":row.m_NumberofPages;
		case COL_ForwardTo:    return (row.m_ForwardTo.trim().equals("-"))?"":row.m_ForwardTo;
		case COL_NumberofRefernce: 	return (row.m_NumberofReference.trim().equals("-"))?"":row.m_NumberofReference;
		}
		return "";
	}
	
	public ImageIcon getStatusIcon(String status){
	    if (status.equalsIgnoreCase("originator"))
		return Images.originator;
	    else if(status.equalsIgnoreCase("pending"))
		return Images.pending;	
	    else if (status.equalsIgnoreCase("Rejected, Task completed") || status.equalsIgnoreCase("Rejected"))		
		return Images.rejected;
	    else if (status.equalsIgnoreCase("Approved, Task completed") || status.equalsIgnoreCase("Approved"))
		return Images.approved;
	    return null;
	}
//	--------------------------------------------------------------------------
	/*	public static final int COL_RECEIVEDON= 1;
	public static final int COL_Action= 2;
	public static final int COL_ActionDt= 3;
	public static final int COL_Comments = 4;
	public static final int COL_VersionofNote = 5;
	public static final int COL_NumberofPages= 6;
	public static final int COL_ForwardTo = 7;
	public static final int COL_NumberofRefernce= 8;*/
	public void setValueAt(Object value, int nRow, int nCol) {
		if (nRow < 0 || nRow >= getRowCount())
			return;
		RouteSummaryRowData1 row = (RouteSummaryRowData1)m_vector.elementAt(nRow);
		String svalue = value.toString().trim().equals("-")?" ":value.toString();
		
		switch (nCol) {
		/*case COL_SERIALNUMBER:
			row.m_SerailNumber = svalue;
			break;*/
		case COL_USERNAME:
			row.m_UserName = svalue;
			break;
		case COL_RECEIVEDON:
			row.m_ReceivedOn = svalue;
			break;			
		case COL_Action:
			row.m_Action = svalue;
			break;
		case COL_ActionDt:
			row.m_ActionDt = svalue;
			break;                
		case COL_Comments:
			row.m_Comments = svalue;
			break;
		case COL_VersionofNote :
			row.m_VersionofNote= svalue;
			break;
		/*case COL_NumberofPages :
			row.m_NumberofPages = svalue;
			break;*/
		case COL_ForwardTo :
			row.m_ForwardTo = svalue;
			break;
		case COL_NumberofRefernce:
			row.m_NumberofReference = svalue;
			break;
	/*	case COL_USERSTATUS:
			row.m_UserStatus = svalue;*/
			
		}
	}
//	--------------------------------------------------------------------------
	public void clear() {
		m_vector.removeAllElements();
	}
//	--------------------------------------------------------------------------
	public void insert(RouteSummaryRowData1 AdvanceSearchRowData) {
		m_vector.addElement(AdvanceSearchRowData);
	}
//	--------------------------------------------------------------------------
	public boolean remove(int row){
		if (row < 0 || row >= m_vector.size())
			return false;
		m_vector.remove(row);
		return true;
	}
//	--------------------------------------------------------------------------
	
	
	
    class ColumnListener extends MouseAdapter {
        protected VWNewRouteSummaryTable m_table;
    //--------------------------------------------------------------------------
        public ColumnListener(VWNewRouteSummaryTable vwRouteSummaryTable1){
            m_table = vwRouteSummaryTable1;
        }
    //--------------------------------------------------------------------------
        public void mouseClicked(MouseEvent e){
        	if(e.getModifiers()==e.BUTTON1_MASK){
                sortCol(e);
            }
        }
        
    //--------------------------------------------------------------------------
        private void sortCol(MouseEvent e) {    	 
            TableColumnModel colModel = m_table.getColumnModel();
            int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
            int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
            
            if (modelIndex < 0) return;
          /*  if (m_sortCol==modelIndex)
                m_sortAsc = !m_sortAsc;
            else
                m_sortCol = modelIndex;*/
          for (int i=0; i < colModel.getColumnCount();i++) {
                TableColumn column = colModel.getColumn(i);
                column.setHeaderValue(getColumnName(column.getModelIndex()));
                column.setHeaderRenderer(new SimpleHeaderRenderer());
            }
            m_table.getTableHeader().setDefaultRenderer(new SimpleHeaderRenderer());
            
         /*  m_table.setTableHeader(new JTableHeader(m_table.getColumnModel()) {
          	  @Override public Dimension getPreferredSize() {
          	    Dimension d = super.getPreferredSize();
          	    d.width=50;
          	    d.height = 80;
          	    return d;
          	  }
          	});*/
            m_table.getTableHeader().repaint();
            Collections.sort(m_vector, new DocRouteSummaryComparator(modelIndex, m_sortAsc));
            m_table.tableChanged(
            new TableModelEvent(VWNewRouteSummaryTableData.this));
            m_table.repaint();
        }
   }
    
    
    
    class DocRouteSummaryComparator implements Comparator {
        protected int     m_sortCol;
        protected boolean m_sortAsc;
    //--------------------------------------------------------------------------
        public DocRouteSummaryComparator(int sortCol, boolean sortAsc) {
            m_sortCol = sortCol;
            m_sortAsc = sortAsc;
        }
    //--------------------------------------------------------------------------
        public int compare(Object o1, Object o2) {
            if(!(o1 instanceof RouteSummaryRowData1) || !(o2 instanceof RouteSummaryRowData1))
                return 0;
            RouteSummaryRowData1 s1 = (RouteSummaryRowData1)o1;
            RouteSummaryRowData1 s2 = (RouteSummaryRowData1)o2;
            int result = 0;
            Integer serialNo1, serialNo2;
           
            double d1, d2;
            switch (m_sortCol) {          
            	/*case COL_SERIALNUMBER:
            		serialNo1 = Integer.valueOf(s1.m_SerailNumber);
            		serialNo2 = Integer.valueOf(s2.m_SerailNumber);
            		//result = s1.m_SerailNumber.toLowerCase().compareTo(s2.m_SerailNumber.toLowerCase());
            		result = serialNo1.compareTo(serialNo2);
            		break;*/
            	case COL_USERNAME:
                    result = s1.m_UserName.toLowerCase().compareTo(s2.m_UserName.toLowerCase());
                    break;
                case COL_RECEIVEDON:
                   result = s1.m_ReceivedOn.toLowerCase().compareTo(s2.m_ReceivedOn.toLowerCase());
                   break;  
                case COL_Action:
                    result = s1.m_Action.toLowerCase().compareTo(s2.m_Action.toLowerCase());
                    break;  
                case COL_ActionDt:
                    result = s1.m_ActionDt.toLowerCase().compareTo(s2.m_ActionDt.toLowerCase());
                    break;  
                case COL_Comments:
                    result = s1.m_Comments.toLowerCase().compareTo(s2.m_Comments.toLowerCase());
                    break;  
                case COL_VersionofNote:
                    result = s1.m_VersionofNote.toLowerCase().compareTo(s2.m_VersionofNote.toLowerCase());
                    break;  
               /* case COL_NumberofPages:
                    result = s1.m_NumberofPages.toLowerCase().compareTo(s2.m_NumberofPages.toLowerCase());
                    break;  */
                case COL_ForwardTo:
                    result = s1.m_ForwardTo.toLowerCase().compareTo(s2.m_ForwardTo.toLowerCase());
                    break;                      
                case COL_NumberofRefernce:
                    result = s1.m_NumberofReference.toLowerCase().compareTo(s2.m_NumberofReference.toLowerCase());
                    break;
           }
            if (!m_sortAsc)
                result = -result;
            return result;
        }
    //--------------------------------------------------------------------------
        public boolean equals(Object obj) {
            if (obj instanceof DocRouteSummaryComparator) {
            	DocRouteSummaryComparator compObj = (DocRouteSummaryComparator)obj;
                return (compObj.m_sortCol==m_sortCol) &&
                (compObj.m_sortAsc==m_sortAsc);
            }
            return false;
        }
    } 
    
    
}
class VWTableCellRenderer1 extends DefaultTableCellRenderer
{
    public Component getTableCellRendererComponent(JTable table,Object value,
        boolean isSelected,boolean hasFocus,int row,int column) 
    {
/*        if(row%2==0)
            setBackground(new Color(15658734));
        else
*/          
    	if(!VWRouteSummary.isWebClient)
    		setBackground(Color.white);
	if (value instanceof IconData){
	    IconData data = (IconData) value;
	    
	    setText(data.m_data.toString());
	    if (data.m_data != null && data.m_data.toString().length() > 0 )
		setIcon(data.m_icon);
	}
	    
        return super.getTableCellRendererComponent(table,value,isSelected,false,row,column);
    }

}

 class SimpleHeaderRenderer extends JTextArea implements TableCellRenderer {
	 private List<List<Integer>> rowColHeight = new ArrayList<>();
    public SimpleHeaderRenderer() {
    	 setLineWrap(true);
	        setWrapStyleWord(true);
	        setOpaque(true);
      //  setFont(new Font("Consolas", Font.BOLD, 14));
      //  setForeground(Color.BLUE);
	        Border headerBorder = UIManager.getBorder("TableHeader.cellBorder");
	        setBorder(headerBorder);
	    /*    JTableHeader hed = table.getTableHeader();
	        hed.setBorder(border);*/
       // setBorder(BorderFactory.createLineBorder(Color.darkGray));
    }
     
    public void setHorizontalAlignment(int m_alignment) {
		// TODO Auto-generated method stub
		
	}

	@Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        setText(value.toString());
        adjustRowHeight(table, row, column);
        return this;
    }
	private void adjustRowHeight(JTable table, int row, int column) {
    	try{
    		int cWidth = table.getTableHeader().getColumnModel().getColumn(column).getWidth();
    		//VWClient.printToConsole("cWidth :::"+cWidth);
    		setSize(new Dimension(cWidth, 1000));
    		int prefH = getPreferredSize().height;
    		while (rowColHeight.size() <= row) {
    			rowColHeight.add(new ArrayList<Integer>(column));
    		}
    		List<Integer> colHeights = rowColHeight.get(row);
    		while (colHeights.size() <= column) {
    			colHeights.add(0);
    		}
    		colHeights.set(column, prefH);
    		int maxH = prefH;
    		for (Integer colHeight : colHeights) {
    			if (colHeight > maxH) {
    				maxH = colHeight;
    			}
    		}
    		if (table.getRowHeight(row) != maxH) {
    			table.setRowHeight(row, maxH);
    		}
    	
    }catch(Exception e){
    	//VWClient.printToConsole("Exception in adjust row height :::"+e.getMessage());
    }
    }
}
class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {
	 
	    private List<List<Integer>> rowColHeight = new ArrayList<>();
	 
	    public MultiLineCellRenderer() {
	        setLineWrap(true);
	        setWrapStyleWord(true);
	        setOpaque(true);
	       // setBorder(BorderFactory.createEtchedBorder());
	        
	    }
	 
	    @Override
	    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	    	if (column == 0)
	    		OfficeSummaryRowHeight.maxRowHeight = 0;
	    	int rowHeight = OfficeSummaryRowHeight.maxRowHeight;
	        if (isSelected) {
	            setForeground(table.getSelectionForeground());
	            setBackground(table.getSelectionBackground());
	        } else {
	            setForeground(table.getForeground());
	            setBackground(table.getBackground());
	        }
	        setFont(table.getFont());
	 
	        if (hasFocus) {
	            setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
	            if (table.isCellEditable(row, column)) {
	                setForeground(UIManager.getColor("Table.focusCellForeground"));
	                setBackground(UIManager.getColor("Table.focusCellBackground"));
	            }
	        } else {
	            setBorder(new EmptyBorder(1, 2, 1, 2));
	        }
	 
	        if (value != null) {
	            setText(value.toString());
	             
	        } else {
	            setText("");
	        }
	        try {
		        FontMetrics fm = getFontMetrics(this.getFont());
		        int fontHeight = fm.getHeight() + table.getRowMargin();
		        int textLength = fm.stringWidth(getText());  // length in pixels
		        int colWidth = table.getColumnModel().getColumn(column).getWidth();
		        int lines = textLength / colWidth +1; // +1, because we need at least 1 row.
		        int height = fontHeight * lines;
		        if (column == 0 && height < 20) height = 20; // This is for by default all the rows height will be 20
		        /*VWClient.printToConsole("height......."+height);
		        VWClient.printToConsole("row......."+row);
		        VWClient.printToConsole("column......."+column);
		        VWClient.printToConsole("rowHeight......."+rowHeight);*/
		        // ensure the row height fits the cell with most lines
		        if (column == 0 || height > rowHeight) {
		            table.setRowHeight(row, (height+5));
		            rowHeight = height;
		        } 
		        //VWClient.printToConsole("after setting stRowHeight....");
		        OfficeSummaryRowHeight.maxRowHeight = rowHeight;
		        //VWClient.printToConsole("before adjust row count :::"+table.getRowCount());
		        //VWClient.printToConsole("before adjust col count :::"+table.getColumnCount());
		        //VWClient.printToConsole("before row :::"+row);
		        //VWClient.printToConsole("before column :::"+column);
		        //adjustRowHeight(table, row, column);
		        //VWClient.printToConsole("End of the getTableCellRendererComponent...");
	        } catch (Exception e) {
	        	VWClient.printToConsole("Exception in getTableCellRendererComponent ......."+e.getMessage());
	        }
	 
	        return this;
	    }
	 
	    private void adjustRowHeight(JTable table, int row, int column) {
	    	try{
	    		int cWidth = table.getTableHeader().getColumnModel().getColumn(column).getWidth();
	    		//VWClient.printToConsole("cWidth :::"+cWidth);
	    		setSize(new Dimension(cWidth, 1000));
	    		int prefH = getPreferredSize().height;
	    		while (rowColHeight.size() <= row) {
	    			rowColHeight.add(new ArrayList<Integer>(column));
	    		}
	    		List<Integer> colHeights = rowColHeight.get(row);
	    		while (colHeights.size() <= column) {
	    			colHeights.add(0);
	    		}
	    		colHeights.set(column, prefH);
	    		int maxH = prefH;
	    		for (Integer colHeight : colHeights) {
	    			if (colHeight > maxH) {
	    				maxH = colHeight;
	    			}
	    		}
	    		if (table.getRowHeight(row) != maxH) {
	    			table.setRowHeight(row, maxH);
	    		}
	    	
	    }catch(Exception e){
	    	VWClient.printToConsole("Exception in adjust row height :::"+e.getMessage());
	    }
	    }
	}
	class OfficeSummaryRowHeight {
		public static int maxRowHeight = 0;
	}
