package com.computhink.vwc;

import java.io.Serializable;
import java.sql.Connection;

@SuppressWarnings("serial")
public class DBConnectionBean implements Serializable {
	public Connection connection = null;

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection con) {
		this.connection = con;
	}
}
