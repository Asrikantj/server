/*
 * @(#)VWClient.java	5.60 06/12/04
 *
 * Copyright (c) 2004 Computhink, Inc. All rights reserved.
 * COMPUTHINK PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.computhink.vwc;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.Preferences;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import com.computhink.VWDocument.VWPageInfo;
import com.computhink.VWDocument.ViewerInfo;
import com.computhink.ars.server.ARSConstants;
import com.computhink.ars.server.ARSPreferences;
import com.computhink.biffileparser.EHGeneralPanel;
import com.computhink.common.Acl;
import com.computhink.common.AclEntry;
import com.computhink.common.CVPreferences;
import com.computhink.common.Constants;
import com.computhink.common.Creator;
import com.computhink.common.CustomList;
import com.computhink.common.DBLookup;
import com.computhink.common.DocComment;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.common.EWorkMap;
import com.computhink.common.EWorkSubmit;
import com.computhink.common.EWorkTemplate;
import com.computhink.common.Index;
import com.computhink.common.Node;
import com.computhink.common.NodeIndexRules;
import com.computhink.common.Notification;
import com.computhink.common.NotificationHistory;
import com.computhink.common.NotificationSettings;
import com.computhink.common.Principal;
import com.computhink.common.RoomProperty;
import com.computhink.common.RouteIndexInfo;
import com.computhink.common.RouteInfo;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.RouteUsers;
import com.computhink.common.Search;
import com.computhink.common.SearchCond;
import com.computhink.common.ServerSchema;
import com.computhink.common.ShortCut;
import com.computhink.common.Signature;
import com.computhink.common.Util;
import com.computhink.common.VWATEvents;
import com.computhink.common.VWColor;
import com.computhink.common.VWDoc;
import com.computhink.common.VWRetention;
import com.computhink.common.ViewWiseErrors;
import com.computhink.common.login.VWLoginScreen;
import com.computhink.common.vwfind.connector.VWRoom;
import com.computhink.common.vwfind.ui.VWFindDlg;
import com.computhink.common.vwmessage.VWMessage;
import com.computhink.coverpageresource.ResourceManager;
import com.computhink.drs.server.DRSConstants;
import com.computhink.drs.server.DRSLog;
import com.computhink.drs.server.DRSPreferences;
import com.computhink.drs.server.RouteDocuments;
import com.computhink.dss.server.DSS;
import com.computhink.dss.server.DSSCipher;
import com.computhink.mdss.MDSS;
import com.computhink.pc1.CryptographyUtils;
import com.computhink.vns.server.VNSConstants;
import com.computhink.vns.server.VNSPreferences;
import com.computhink.vwc.VWDocATDetails.VWAuditTrailDetails;
import com.computhink.vwc.doctype.VWRulesDlgSettings;
import com.computhink.vwc.notification.VWDocNotification;
import com.computhink.vwc.notification.VWExistingNotifications;
import com.computhink.vwc.notification.VWFolderNotification;
import com.computhink.vwc.recover.VWRecoverDocument;
import com.computhink.vws.license.Base64Coder;
import com.computhink.vws.csserver.CSSPreferences;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.file.CloudFile;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.CloudFileShare;
import com.microsoft.azure.storage.file.ListFileItem;

/**
	 *
	 *
	 */
public class VWClient implements Constants, VWCConstants, ViewWiseErrors, Serializable 
{
	    private static Hashtable registeredVWSs = new Hashtable();
	    private Hashtable connectedVWSs = new Hashtable();
	    private Hashtable sessions = new Hashtable();
	    private ServerSchema mySchema; 
	    private int type;
	    private String cache;
	    private boolean outputLibraryLoaded = false;
	    private boolean viewLibraryLoaded = false;
	    private boolean wtsLibraryLoaded = false;
	    private Class VWERROR = null;
	    private final String VWERROR_CLASS = "com.computhink.common.ViewWiseErrors";
	    private String docPageFile = "";
	    private String pHost ="";
	    BufferedReader br = null;
	    private int pPort =80;
	    String dllLocation = "C:\\Program Files\\ContentVerse Client\\System"; 
	    // If this variable set to true.Some of the document related function will be executed through the 
	    // the DSS server.
	    private boolean useDSS = false;
	    private static ResourceManager resourceManager=null;
	    VWFindDlg findDialog = null;
	    public static Vector drsValidateTaskDocumentVwctor = null;
	    public static int userSelectedValue = 0;
	    /**CV2019 merges from SIDBI line*/
	    public static boolean clientRegFlag=false;
	    public int isEncryptedVal = 1; 
	    /*
	     *
	     *  Natives in VWJClient.dll
	     */
	    private native void Export(Vector outputInfo, VWClient client);
	    private native String GetAppsList();
	    private native void SendToConfigure();
	    private native String WTSgetClientIPAddress();
	    private native String ConvertDocToPDF(String DestPath, String AllDotZipPath, String Password, int nOverlay, int nWhiteout, int nColor, int nDepth); 
	    //----------------------C O N S T R U C T O R-------------------------------
	    /**
	     * Creates a new VWClient object with the specified workFolder.  
	     * @param workFolder
	     */
	    public VWClient(String workFolder)
	    {
	        this("", workFolder, Client.sdk_Client_Type);
	    }
	    /**
	     * Creates a new VWClient object with the specified workFolder and type.
	     * @param workFolder
	     * @param type
	     */
	    public VWClient(String workFolder, int type)
	    {
	        this("", workFolder, type);
	    }
	    /**
	     * Creates a new VWClient object with the specified workFolder, type and sign
	     * @param sign
	     * @param workFolder
	     * @param type
	     */
	    public VWClient(String sign, String workFolder, int type)
	    {
	    	this(sign, workFolder, type, "", "");
	    }
	    
	    public VWClient(String sign, String workFolder, int type, String pHost, String pPort)
	    {
	        if (!sign.equals(SIGN)) type = Client.sdk_Client_Type;
	        if (!assertClientInstance(workFolder, type)) return;
	        if (type == Client.Agt_Client_Type) this.type = Client.Fat_Client_Type;
	        try{
	        	if (pHost != null && pPort != null && pHost.length() > 0 && pPort.length() > 0){
	        		setWebPreferences(pHost, Util.to_Number(pPort));
	        	}
	        }catch(Exception ex){}
	        
	        setPreferences();
	        createMyServer();
	        VWClient.printToConsole("type ......"+type);
	        if (type != Client.Web_Client_Type && type != Client.WebL_Client_Type )
	        {
	            if (type != Client.Fat_Client_Type){//Other than DTC we are allowing to set the Home
	        	registerHome();
	            }
	            //Issue No: 873 ARS should run without client installation
	            if (type == Client.Ars_Client_Type || type == Client.Drs_Client_Type || type==Client.Notification_Client_Type|| type==Client.ContentSentinel_client_Type) 
	            	registeredDummyVWS(type);
	            else
	            	refreshRegisteredServers();
	            /**
	             * If condition added for convert to pdf from client dll for drs endworkflow implementation.
	             * 
	             */
	            if(type==Client.Drs_Client_Type){
	            	VWCUtil.loadNativeLibraryForDRS(LIB_VIEWER,"");
	            	VWClient.printToConsole("Before loading lead dll..");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_DATABASE,"");
	            	/**CV2019 - Libraries are added for final action pdf convertion. Added by Vanitha.S **/
	            	VWCUtil.loadNativeLibraryForDRS(LIB_LEAD,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_DIALOGENU,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_VWDJVU,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_VWINDEX,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_VWMAGIK,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_VWPDF,"");
	            	
	            	VWCUtil.loadNativeLibraryForDRS(LIB_LTRIM1,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_LTRIM2,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_LTRIM3,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_LTDLGKRNU,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_LTDLGIMGEFXU,"");
	            	VWCUtil.loadNativeLibraryForDRS(LIB_LTDLGCLRU,"");
	            	VWClient.printToConsole("After loading lead dll..");
	            	/**End of CV2019-Final action issue fix********/
	            	VWCUtil.loadNativeLibrary(LIB_SIGN);
	            	VWCUtil.loadNativeLibrary(LIB_CLIENT);
	            }else{
	            	VWCUtil.loadNativeLibrary(LIB_VIEWER);
	            	VWCUtil.loadNativeLibrary(LIB_SIGN);
	            	VWCUtil.loadNativeLibrary(LIB_CLIENT);
	            }
	        }
	        else{
	        	refreshRegisteredServers();
	        }
	        
	        VWCUtil.loadNativeLibrary(LIB_OUTPUT);
	        /**
	         * CV2019 merges from SIDBI line
	         */
	        clientRegFlag= checkVWClientDisplayFlag();
	    }
	    
	    public int getClientType(){
	    	return type;
	    }
	    /***
	     * This method is used to reset the Security details once 
	     * the user clicks the reset button in the security dialog of DTC or Webtop
	     * @param r
	     * @param sid
	     * @param nodeID
	     * @param aclID
	     * @param result
	     * @return
	     * @throws RemoteException
	     */
	    public int resetSecurityDetails(String r,int sid, int nodeID,int aclID,Vector result) throws RemoteException{
	    		 Session session = getSession(sid);
	    		 VWClient.printToConsole("sessionId in resetSecurityDetails::"+sid);
		         if(session==null) return invalidSessionId;
		         VWS vws = getServer(session.server);
		         if (vws == null) return ServerNotFound;
		         try
		         {
		            return vws.resetSecurityDetails( sid,session.room,nodeID,aclID,result);
		         }
		         catch(Exception e)
		         {
	
		         }
		             return Error;
		         }
	    	
	    	
	    
	
	    //----------------------------P R I V A T E S-------------------------------
	    //----------------------------P R I V A T E S-------------------------------
	    private void setWebPreferences(String proxyHost, int proxyPort)
	    {
	            Util.RegisterProxy(proxyHost, proxyPort, VWC_HTTP_AGENT);
	            try
	            {System.getProperties().put("java.rmi.server.disableHttp", "false");}catch(Exception e){}
	            try
	            {
	                sun.rmi.transport.proxy.RMIMasterSocketFactory.setSocketFactory
	                    (new sun.rmi.transport.proxy.RMIHttpToPortSocketFactory());
	            }
	            catch(Exception e){}
	    }
	    private void setPreferences()
	    {
	        String proxyHost = VWCPreferences.getProxyHost();
	        int proxyPort =  VWCPreferences.getProxyPort();
	        
	        if (VWCPreferences.getProxy())
	            Util.RegisterProxy(proxyHost, proxyPort, VWC_HTTP_AGENT);
	        
	        if (VWCPreferences.getConnectionMode().equalsIgnoreCase("http"))
	            try
	            {
	                sun.rmi.transport.proxy.RMIMasterSocketFactory.setSocketFactory
	                    (new sun.rmi.transport.proxy.RMIHttpToPortSocketFactory());
	            }
	            catch(Exception e){}
	    }
    private void createMyServer()
    {
        mySchema = Util.getMySchema();
        mySchema.comport = VWCPreferences.getComPort();
        mySchema.dataport = VWCPreferences.getDataPort();
        mySchema.type = SERVER_MDSS;
        if(type != 2){
        if (type != 8)
        {
            if (mySchema.comport == 0)
                mySchema.comport = Util.createServer(SERVER_MDSS);
            else
                Util.createServer(mySchema);
        }
    }
    }
    private void setMySchema(String folder, int port)
    {
        mySchema = Util.getMySchema();
        mySchema.comport = port;
        mySchema.path = folder;
        mySchema.type = SERVER_MDSS;
    }

    /**Login to a ViewWise Server/Room combination
     * Logs in to a ViewWise Room consuming a license seat
     * @param server ViewWise Server name
     * @param room ViewWise room name
     * @param user user name
     * @param pass password
     * @return session id for success or negative value error code
     */
    public int login(String server, String room, String user, String pass){
    	isEncryptedVal = 1;
    	return login(server, room, user, pass, true);
    }
    
    public int login(String server, String room, String user, String pass, int isEncrypted){
    	isEncryptedVal = 0;
    	System.out.println("isEncryptedVal inside new method 1 :::: "+isEncryptedVal);
    	return login(server, room, user, pass, true);
    }
    
    /**Login to a ViewWise Server/Room combination with use of DSS option
     * @param server ViewWise Server name
     * @param room ViewWise room name
     * @param user user name
     * @param pass password
     * @param useDSS if true use DSS server, if false will not DSS server
     * @return session id for success or negative value error code
     **/
    public int login(String server, String room, String user, String pass, boolean useDSS)
    {
        String address = mySchema.address;
        VWClient.printToConsole("client ip address :"+address);
        if (type == Client.Fat_Client_Type)
        {
        	isEncryptedVal = 1;
            String wtsAddress = getWTSAddress();
            VWClient.printToConsole("wtsAddress :"+address);
            if (wtsAddress.length() > 0) address = wtsAddress;
        }
        return login(server, room, user, pass, address,mySchema.address, useDSS);
    }

	public int login(String svr, String room, String usr, String pass, String ip) {
		isEncryptedVal = 1;
		return login(svr, room, usr, pass, ip, "", true);
	}
    
	public int login(String svr, String room, String usr, String pass, String ip, int isEncrypted) {
		isEncryptedVal = 0;
		System.out.println("isEncryptedVal inside new method 2 :::: "+isEncryptedVal);
		return login(svr, room, usr, pass, ip, "", true);
	}
    /*
    Issue No / Purpose:  <650/Terminal Services Client>
    Date: <19 Jul 2006>
    Check that stubIPaddress is different from address, If it is different call is made from Terminal Service Client.
    Get the MDSS object using stubIPaddress.
    */	
    private int login(String svr, String room, String usr, String pass, String ip, String stubIPAddress){
    	isEncryptedVal = 1;
    	return login(svr, room, usr, pass, ip, stubIPAddress, true);
    }
    private int login(String svr, String room, String usr, String pass, String ip, String stubIPAddress, boolean useDSS)
    {
        if (room == null) return invalidParameter;
        if (pass.trim().length() == 0) return invalidPassword;
        VWS vws = getServer(svr);
        if (vws == null) return ServerNotFound;
        try
        {     	
        	/*CV2019 merges from SIDBI line------------------------------------------------***/
        	boolean isEmptyPwdCheckRequired = true;
			boolean systemSettingsInfo = vws.getServerSettingsInfo(room, 0, "SIDBI Customization");
			VWClient.printToConsole("SIDBI Customization systemSettingsInfo :" + systemSettingsInfo);
			if (systemSettingsInfo) {
				if (type == Client.Fat_Client_Type) {
					systemSettingsInfo = vws.getServerSettingsInfo(room, 0, "Bypass Security for DTC");
					if (systemSettingsInfo) {
						isEmptyPwdCheckRequired = false;
					}
				}
				if (type == Client.Web_Client_Type || type == Client.WebAccess_Client_Type
						|| type == Client.Custom_Label_WebAccess_Client_Type
						|| type == Client.Professional_WebAccess_Client_Type
						|| type == Client.Concurrent_WebAccess_Client_Type
						|| type == Client.Professional_Conc_WebAccess_Client_Type) {
					systemSettingsInfo = vws.getServerSettingsInfo(room, 0, "Bypass Security for WebAccess");
					if (systemSettingsInfo) {
						isEmptyPwdCheckRequired = false;
					}
				}
			}
			VWClient.printToConsole("isEmptyPwdCheckRequired :" + isEmptyPwdCheckRequired);
			if (isEmptyPwdCheckRequired) {
				if (pass.trim().length() == 0)
					return invalidPassword;
			}
			/*------------------------End of SIDBI merges-----------------------------------*/
        	/*	Enhancement No:66-ViewWise Client-Server Synchronization step on launch to confirm the client is up-to-date (e.g. updated jars).
        	 *	Created by: <Shanmugavalli.C>	Date: <04 Aug 2006>
        	 *	Read buildTime stored in manifest file of jar files in both client and server place and compare  
        	 */
        	/*Enhancement 66 is implemented for DTC and AdminWise, C.Shanmugavalli 30 Oct 2006*/
        	if(type == Client.Fat_Client_Type || type == Client.Adm_Client_Type || type == Client.NFat_Client_Type){ 
	        	String serverJarBuildTime = vws.getSvrJarBuildTime();
	        	if (serverJarBuildTime == null) return  JarFileMisMatchErr;
	        	if (!serverJarBuildTime.equalsIgnoreCase("-1")){ 
	            String jarRes = "\\Lib\\ViewWise.jar";
	        	String clientJarLocation = VWCUtil.getHome()+jarRes;
	        	String clientJarBuildTime = Util.getJarManifestInfo(clientJarLocation);
	        	if(serverJarBuildTime!=null && clientJarBuildTime!=null){
	        		if(!serverJarBuildTime.equalsIgnoreCase(clientJarBuildTime))
	        			return JarFileMisMatchErr;
	        	}
        	}
        	}
        	/* end of 66*/
        	/*CV2019 merges from SIDBI line------------------------------------------------***/
        	int id = 0;
        	VWClient.printToConsole("usr :" + usr);
        	//VWClient.printToConsole("pass :" + pass);
			VWClient.printToConsole("type inside VWCLIENT.login :::: " + type);
			if (type == Client.Web_Client_Type || type == Client.WebAccess_Client_Type
					|| type == Client.Custom_Label_WebAccess_Client_Type
					|| type == Client.Professional_WebAccess_Client_Type
					|| type == Client.Concurrent_WebAccess_Client_Type
					|| type == Client.Professional_Conc_WebAccess_Client_Type 
					|| type == Client.Fat_Client_Type || type == Client.AIP_Client_Type || type == Client.Adm_Client_Type
					|| type == Client.IXR_Client_Type) {
				VWClient.printToConsole("isEncryptedVal ::::  " + isEncryptedVal);
				if (isEncryptedVal == 0) {
					usr = Base64Coder.encode(usr);
					pass = Base64Coder.encode(pass);
				}
				id = vws.login(room, type, ip, usr, pass, mySchema.comport, stubIPAddress);
				VWClient.printToConsole("id :::: " + id);				
			} else {
				id = vws.login(room, type, ip, usr, pass, mySchema.comport, stubIPAddress);
			}
			/*------------------------End of SIDBI merges--------------------------------------*/
            if (id > 0) 
            {
		
				String ssType = vws.getSSType();
/*		if (ssType.equalsIgnoreCase(SSCHEME_NDS)
					|| ssType.equalsIgnoreCase(SCHEME_LDAP_ADS)
					|| ssType.equalsIgnoreCase(SCHEME_LDAP_NOVELL)
					|| ssType.equalsIgnoreCase(SCHEME_LDAP_LINUX))
*/		{
		
				    //Known Issue: The username and context name is given by user it substring the username and added to the 
				    // Session object. In Novell Ldap user name can have a '.' character in the user name, that scenario failed to store 
				    // correct username to session object. 
			    
/*		    if (usr != null && usr.indexOf(".") != -1) {
					usr = usr.substring(0, usr.indexOf("."));
				    }*/		
				VWClient.printToConsole("usr before getting actual username ::::  " + usr);
				VWClient.printToConsole("usr before getting actual username ::::  " + usr.indexOf("."));
				/**Following if condition has commented in CV2019 - SIDBI merges on 16/Oct/2019. Bz previously there was no userName encoding.
				 *  so if the name contains '.' then only have to get the actual username of the logged in user. But now have introduced username encoding. 
				 	usr variable contains encoded userName. Hence always has to get the actual user name irrespective of the condition  *****/
				//if (usr.indexOf(".") != -1){
					String userName = vws.getLoggedInUser(room, id);
					if (userName != null && userName.trim().length() > 0)
					    usr = userName;
				    }
				//}
				VWClient.printToConsole("usr after getting actual username ::::  " + usr);
		        	/*if (type == Client.Web_Client_Type || type == Client.WebL_Client_Type) {
				    usr = Util.decryptKey(usr);
		        	}*/
		        	
		                Session s = new Session(id, svr, room, cache, usr);
		                sessions.put(new Integer(id), s);
		                new File(s.cacheFolder).mkdirs();
		                //setColumnArragementInfo(id, 1, "", "", "", "");
            }
            // Set the boolean loadBalance, DSS will be used based on this variable.
            if (useDSS){            	
            	setUseDSS(true);
            }else {
            	setUseDSS(false);
            }
            /*CV2019 merges from SIDBI line------------------------------------------------***/
            /* This code is added for initializing Jpanel, JFrame and image instances for summary report****/
			if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type) {
				new Thread(new VWNewRouteSummary(type)).start();
			}
			VWClient.printToConsole("Final return value from login : " + id);
			/*------------------------End of SIDBI merges--------------------------------*/
            return id;
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    /**
     * Used for Authenticate the user from the directory 
     * for 
     * @param sid
     * @param user
     * @param pass
     * @return
     */
    public int authenticateUser(int sid, String user, String pass){
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in authenticateUser::"+sid);
        if (pass.trim().length() == 0) return invalidPassword;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {     	
        	return vws.authenticateUser(session.room, sid, user, pass);
        }catch(Exception ex){
        	return Error;
        }
    }
    private void registerHome()
    {
        String home = VWCUtil.getHome();
        if (home.length() == 0 ) return;
        home = home.replace('\\', '/');
        VWCPreferences.setHome(home.toLowerCase());
    }
    
    /** Deletes cabinet/drawer/folder
     * @param sid Session id obtained by login
     * @param id Id of the Node
     * @param recycle true - moves to recycle bin
     * @return 0 for success or negative value error code
     */
    public int deleteNode(int sid, int id, boolean recycle)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteNod::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        String storageType="";
        String storagepath="";
        String azureUserName="";
        String azurePassword="";

		CloudStorageAccount storageAccount=null;
		CloudFileClient fileClient =null;
		CloudFile cloudFile=null;
		CloudFileShare share=null;
		CloudFileDirectory rootDir=null;
		CloudFileDirectory sampleDir =null;
		try{
			Vector storageVect=new Vector();
			getAzureStorageCredentials(sid,id,storageVect);
			if(storageVect!=null&&storageVect.size()>0){
				VWClient.printToConsole("storageVect inside delnode::::"+storageVect.get(0).toString());
				String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
				VWClient.printToConsole("Stroage type from storage array in delnode"+storageArray[7]);
				storagepath=storageArray[5]+"\\"+String.valueOf(id);
				VWClient.printToConsole("value of id is:::"+id);
				storageType=storageArray[7];
				azureUserName=storageArray[8];
				azurePassword=storageArray[9];
				if(storageType.equals("On Premises")){
					return vws.delNode(session.room, sid, id, recycle);
				}else if(storageType.equals("Azure Storage")){
					if(recycle==false){
						String accountName="AccountName="+azureUserName+";";
						String password="AccountKey="+azurePassword;
						String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
						System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
						storageAccount = CloudStorageAccount.parse(storageConnectionString1);
						fileClient = storageAccount.createCloudFileClient();
						String pattern = Pattern.quote(System.getProperty("file.separator"));
						String dbpath[]=storagepath.split(pattern);

						for(int i=0;i<dbpath.length;i++){
							if(i==2)
							{
								share = fileClient.getShareReference(dbpath[i]);
							}
							else if(i==3)
							{
								sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
								rootDir = sampleDir;
								sampleDir = null;
							}
							else if(i>=4)
							{
								if ((i+1) != dbpath.length) {
									sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
									if(!sampleDir.exists()){
										sampleDir.create();
									}
									rootDir = sampleDir;
									sampleDir = null;
								}
								else if ((i+1) == dbpath.length) {
									sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
									if(!sampleDir.exists()){
										sampleDir.create();
									}
									rootDir = sampleDir;
									sampleDir = null;
								}
							}

						}
						sampleDir = rootDir;
						VWClient.printToConsole("Sample directory uri in deleteNode:::"+sampleDir.getUri());
						if(sampleDir.exists()){
							VWClient.printToConsole("Before delete");
							for ( ListFileItem fileItem1 : sampleDir.listFilesAndDirectories() ) {
								VWClient.printToConsole("fileItem1 in deleteNode version:::::::::::"+fileItem1.getUri());
								String filesURI=fileItem1.getUri().toString();
								String fileName=filesURI.substring(filesURI.lastIndexOf("/")+1, filesURI.length());
								VWClient.printToConsole("fileName in deleteNode version::::"+fileName);
								cloudFile=sampleDir.getFileReference(fileName);
								if(cloudFile.exists()){
									cloudFile.delete();
								}
							}
							sampleDir.delete();
						}
						vws.azureARSDeleteNode(session.room, sid, id, recycle);
					}else{
						vws.delNode(session.room, sid, id, recycle);
					}
				}
			}
		}
        catch(Exception e)
        {
        	VWClient.printToConsole("Exception while deleting document::::"+e.getMessage());
            return Error;
        }
		return NoError;
    }
    
    public int getDocumentsToPurge(int sid, int docsCountToPurge, Vector docsToPurge)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentsToPurge::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        { 
           Vector ret = new Vector(); 
           ret =  vws.getDocsToPurge(session.room, sid, docsCountToPurge, docsToPurge);
           docsToPurge.addAll(ret);
           return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    private boolean isReferenced(int sid, Document doc, Document rdoc)
    {
        int rid = rdoc.getId();
        Vector docs = new Vector();
        if ( getRefs(sid, doc, docs) == NoError)
        {
            for (int i = 0; i < docs.size(); i++)
            {
                if (((Document) docs.elementAt(i)).getId() == rid) return true;
            }
            return false;
        }
        else
            return true;
    }
    private Vector vectorToEWorkTemplate(Vector v)
    {
        Vector ewTemplates = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            EWorkTemplate ewTemplate = new EWorkTemplate((String) v.elementAt(i));
            ewTemplates.addElement(ewTemplate);
        }
        return ewTemplates;
    }
    private Vector vectorToEWorkMap(Vector v)
    {
        Vector ewMapping = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            EWorkMap ewMap = new EWorkMap((String) v.elementAt(i));
            ewMapping.addElement(ewMap);
        }
        return ewMapping;
    }
    private Vector vectorToEWorkSubmit(Vector v)
    {
        Vector ewSubmits = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            EWorkSubmit ewSubmit = new EWorkSubmit((String) v.elementAt(i));
            ewSubmits.addElement(ewSubmit);
        }
        return ewSubmits;
    }
    /**
     * Enhancement :- CV10 Azure restore document to 
     * Method added to get the list of backup file locations
     * @param srcPath
     * @return
     */
    private ArrayList getAzureBackupList(String srcPath) {
    	ArrayList fileList = new ArrayList();
    	try
    	{
    		String modifedSrcPath = srcPath;
    		File path = new File(modifedSrcPath);
    		if (path.isDirectory())
    		{
    			File[] fa = path.listFiles();
    			if (fa.length > 0)
    				for (int i = 0; i < fa.length; i++)
    				{
    					fileList.add(fa[i].getPath());
    				}
    		} 
    	}        
    	catch(Exception e){return null;}
    	return fileList;
    }
    private int checkNextDocVersion(int sid, Document doc, String pi, 
                                                      boolean newDoc, Vector lv)
    {
        Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in checkNextDocVersion::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.checkNextDocVersion(session.room, sid, doc, pi, 
                                                                        newDoc);
            if (ret.size() > 0) lv.addAll(ret);
        	VWClient.printToConsole("Active Error in checkNextDocVersion--->"+vws.getActiveError());
        	//return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
        		return NoError;
        	} else {
        		return vws.getActiveError();
        	}
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    
    //Desc   :This method updates the pagecount, after doing a set active version.
    public int setDocAuditInfo(int sid, Document doc, boolean newDoc, 
                                         String pageInfo, String lastDocVersion)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWClient.printToConsole("sessionId in setDocAuditInfo::"+sid);
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.setDocAuditInfo(session.room, sid, doc ,newDoc, pageInfo);
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    private String getAuditInfo(File file)
    {
        String pageInfo = "";
        File infoFile = new File(file.getParent(), "database.info");
        if(!infoFile.exists() || !infoFile.canRead() || infoFile.length()==0)
            return "";
        
        try 
        {
            RandomAccessFile raf = new RandomAccessFile(infoFile,"r");
            pageInfo = raf.readLine();
            raf.close();
        }
        catch(IOException e) {
            return "";
        }
        return pageInfo;
    }
    private boolean assertClientInstance(String folder, int type)
    {
    	//Changed after ARS client type included, 20 July 2006, Valli
    	//if (type < 0 || type > 12) return false;
    	//if (type < 0 || type > 13) return false;
    	//Changed after DRS client type included, 29 Dec 2006, Valli
    	//if (type < 0 || type > 17) return false;
    	//Changed after VNS client type included, 25 Nov 2011
    	//if (type < 0 || type > 18) return false;
    	//Changed for RightFax Implemention, 9 JUN 2015
    	if ((type < 0 || type > 21)&&(type!=24)) {
    		return false;
    	}
        //No root cache allowed
        if (new File(folder).getParent() == null) {
        	return false;
        }
        this.cache = folder;
        this.type = type;      
        return true;
    }
    private ServerSchema getServerSchema(String key)
    {
        ServerSchema ss = null;
        if (!registeredVWSs.isEmpty() && registeredVWSs.containsKey(key))
        {
            ss = (ServerSchema) registeredVWSs.get(key);
            ss.type = SERVER_VWS;
        }
        return ss;
    }
    /**
     * Used to remove the session from the client HashTable
     * @param sid
     */
    protected void removeSession(int sid)
    {
        sessions.remove(new Integer(sid));
    }
    protected VWS getServer(String server)
    {
        VWS vws = null;
        
        // For Web Client, first check the remote object with given address. If it is not available it will try to connect 
        // using proxy address. it will execute the newViewWiseConnection
        ServerSchema ss = (ServerSchema) registeredVWSs.get(server);
        if (!connectedVWSs.containsKey(server)){
	    	try{
	        	vws = (VWS) Util.getServer(ss);
	        	if (vws.ping()) {
	        		connectedVWSs.put(server, vws);
	        		return vws;
	        	}
	    	}catch(Exception ex){}
        }
        if (!connectedVWSs.isEmpty() && connectedVWSs.containsKey(server))
        {
        	vws = (VWS) connectedVWSs.get(server);
            try
            {
                if (vws.ping()) return vws; 
            }
            catch (java.rmi.NoSuchObjectException nsoe)
            {
                return newViewWiseConnection(server);
            }
            catch (Exception e)
            {
                connectedVWSs.remove(server);
                removeOpenSessions(server);
                return null;
            }
        }
        if(!registeredVWSs.containsKey(server))
        {
            refreshRegisteredServers();
        }
        return newViewWiseConnection(server);
    }
    private VWS newViewWiseConnection(String server)
    {
        VWS vws = null;
        ServerSchema ss = (ServerSchema) registeredVWSs.get(server);
        if (ss == null) return vws;
        ss.type = SERVER_VWS;
        // Saad: 11-17-2004
        // check if server resides behind a fireWall
        // if so we connect to fireWall which should be listening on behalf of VWS
        if (type == Client.Fat_Client_Type || type == Client.Web_Client_Type || type == Client.WebL_Client_Type) //only types behind firewall
        {
            if (isServerProxyEnabled(server)) ss.address = 
                                                     getServerProxyHost(server);
                }  
        vws = (VWS) Util.getServer(ss);
        if (vws != null) 
        {
            String fqdn = getListenerNameFromObject(vws);
            if (!fqdn.equals(ss.address)) addDNSEntry(ss.address, fqdn);
            if (canConnect(fqdn)) connectedVWSs.put(server, vws);
        }
        return vws;
    }
    private String getListenerNameFromObject(Object vws)
    {
        final String marker = "endpoint:["; 
        String str = vws.toString();
        str = str.substring(str.indexOf(marker) + 10);
        str = str.substring(0, str.indexOf(":"));
        return str;
    }
   
    private boolean canConnect(String host)
    {
        try
        {
            java.net.InetAddress address = java.net.InetAddress.getByName(host);
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    private boolean addDNSEntry(String ip, String fqdn)
    {
        final String winPathVar = "%systemroot%\\system32\\drivers\\etc\\hosts";
        byte[] path = new byte[255];
        String winPath = "";
        String newEntry = ip + "\t" + fqdn;
        String fileEntry = "";
        try
        {
            Process p = Runtime.getRuntime().exec("cmd /c echo " + winPathVar);
            p.getInputStream().read(path);
            p.waitFor();
            p.destroy();
            winPath = new String(path).trim();
            if (winPathVar.equalsIgnoreCase(winPath)) return false;
            File hosts = new File(winPath);
            RandomAccessFile raf = new RandomAccessFile(hosts,"rw");
            while( (fileEntry = raf.readLine()) != null)
            {
                if (fileEntry.trim().equals(newEntry))
                {
                    raf.close();
                    return true;
                }
            }
            raf.seek(hosts.length());
            raf.writeBytes("\r\n" + newEntry);
            raf.close();
            
            p = Runtime.getRuntime().exec("ipconfig /registerdns");
            p.waitFor();
            p.destroy();
        }
        catch(Exception e){return false;}
        return true;
    }
    //Issue No: 873 ARS should run without client installation
    private void refreshRegisteredServers()
    {
        ServerSchema[] servers = VWCPreferences.getVWSs();
        registeredVWSs.clear();
        for (int i=0; i < servers.length; i++)
        {
            registeredVWSs.put(servers[i].name, servers[i]);
        }
    }
    private void registeredDummyVWS(int clientType)
    {
        ServerSchema server = new ServerSchema();
        if(clientType == Client.Ars_Client_Type){
        	server = ARSPreferences.getVWS();
            server.name = ARSConstants.DUMMY_SERVERNAME+"ARS";
        }else if(clientType == Client.Drs_Client_Type){
        	server = DRSPreferences.getVWS();
        	server.name = ARSConstants.DUMMY_SERVERNAME+"DWS";
        }else if(clientType == Client.Notification_Client_Type){
        	server = VNSPreferences.getVWS();
        	server.name = VNSConstants.DUMMY_SERVERNAME+"VNS";
        }else if(clientType == Client.ContentSentinel_client_Type) {
        	server = CSSPreferences.getVWS();
        	server.name = VNSConstants.DUMMY_SERVERNAME+"CSS";
        }
        registeredVWSs.put(server.name, server);
   }
    
    private void removeOpenSessions(String key)
    {
        Collection colSessions = sessions.values();
        Iterator it = colSessions.iterator();
        while (it.hasNext())
        {
            Session session = (Session) it.next();
            if ( session.server.equals(key)) it.remove();
        }
    }
    private Vector vectorToDocTypes(Vector v)
    {
        Vector dts = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            DocType dt = new DocType( (String) v.elementAt(i));
            dts.addElement(dt);
        }
        return dts;
    }
    private Vector StringToNodes(Vector v)
    {
        Vector nodes = new Vector();
        if (v.size() > 0)
        {
            StringTokenizer st = new StringTokenizer((String) v.get(0), "\t");
            while (st.hasMoreTokens())
            {
                nodes.add(
                      new Node(Util.to_Number(st.nextToken()), st.nextToken()));  
            }
        }
        return nodes;
    }
    private Vector vectorToNodes(Vector v){
    	return vectorToNodes(v, true);
    }
    private Vector vectorToNodes(Vector v, boolean mode)
    {
        Vector nodes = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
        	if(mode)
        		nodes.add( new Node( (String) v.get(i)));
        	else
        		nodes.add( new Node( (String) v.get(i), mode));
        }
        return nodes;
    }
    private Vector vectorToIndices(Vector v){
    	return vectorToIndices(v, 0);
    }
    private Vector vectorToIndices(Vector v, int mode)
    {
        Vector indices = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
        	Index idx = null;
        	if (mode == 0)
        		idx = new Index((String) v.elementAt(i));        	
        	else
        		idx = new Index((String) v.elementAt(i), mode);
            idx.setOrder(i);
            /**
     		 * Ehancement:Sow2096_891-Add Default Index with users and groups
     		 * Code added get the selection list in dtc.
     		 */
            String elementValue=v.elementAt(i).toString();
            int indexVal=elementValue.lastIndexOf("\t");
            String selectionVal=elementValue.substring(indexVal+1);
            idx.setSelectValue(Integer.parseInt(selectionVal));
            indices.addElement(idx);
        }
        return indices;
    }
    private String getExistingFileForVerRev(Vector versionList, String Version, File docFile){
    	VWClient.printToConsole("first line of getExistingFileForVerRev Version"+Version+"docFile:::"+docFile);
    	boolean findFile = false;
    	File selectedFile = null;
    	for(int index= versionList.size()- 1; index >= 0; index--){
    		Document doc = (Document) versionList.get(index);
    		VWClient.printToConsole("inside doc getversion:::"+doc.getVersion());
    		if (doc.getVersion().equals(doc.getVersion())){
    			VWClient.printToConsole("inside if condtion:::");
    			findFile = true;
    		}
    		if (findFile){   
    			VWClient.printToConsole("inside find file");
    			selectedFile = new File(docFile.getParent() + File.separator + doc.getVersion());
    			VWClient.printToConsole("selectedFile.getpath:::"+selectedFile.getPath());
    			//if (selectedFile.exists()){  
    				return selectedFile.getPath();
    			//}
    		}
    	}
    	return "";
    }
    private void setIndices(Vector v, Document doc)
    {
        Vector indices = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            StringTokenizer st = new StringTokenizer
                                        ( (String) v.elementAt(i), "\t");
            String name = st.nextToken();
            String value = st.nextToken();
            Index idx = new Index(Util.to_Number(st.nextToken()));
            idx.setName(name);
            idx.setValue(value);
            if (doc.getDocType() == null)
                doc.setDocType(new DocType(Util.to_Number(st.nextToken())));
            else
                st.nextToken();
            idx.setType(Util.to_Number(st.nextToken()));
            idx.setKey(st.nextToken().equals("1")? true : false);            
            idx.setActValue(st.nextToken());
            idx.setRequired(st.nextToken().equals("1")? true : false);
            indices.addElement(idx);
            //if (idx.isKey()) doc.setName(idx.getValue());
        }
        doc.getDocType().setIndices(indices);
    }
    private Vector vectorToSearchs(Vector v)
    {
        Vector srchs = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            StringTokenizer st = new StringTokenizer
                                        ( (String) v.elementAt(i), "\t");
            Search srch = new Search(Util.to_Number(st.nextToken()), 
                                                                st.nextToken());
            srchs.addElement(srch);
        }
        return srchs;
    }
    private Vector vectorToDocuments(Vector v, boolean ver)
    {
        Vector docs = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            Document doc = new Document((String) v.elementAt(i), ver);
            docs.addElement(doc);
        }
        return docs;
    }
    /*Document constructor changed for to handle Hit list also.
    * Valli 13 May 2007 
    */
    private Vector vectorToDocumentWithIndexValues(Vector v, int hitList)
    {
        Vector docs = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            Document doc = new Document((String) v.elementAt(i), hitList);
            //printToConsole( " id "+doc.getId());
            docs.addElement(doc);
        }
        return docs;
    }
    
    private Vector vectorToAllDocuments(Vector v, boolean ver)
    {
    	 Vector docs = new Vector();
    	try{
	        for (int i = 0; i < v.size(); i++)
	        {
	        	 StringTokenizer st = new StringTokenizer((String) v.elementAt(i), "\t");
	        	 String DocID = st.nextToken();			// DocID
	        	 String NodeName = st.nextToken();			// NodeName
	        	 
	        	 st.nextToken();			// Storage Id
	        	 st.nextToken();			// Parent Id
	        	 st.nextToken();			// ACL ID
	        	
	        	 String nodeType = st.nextToken();		// NodeType
	        	 String SessionId = st.nextToken();		// SessionId
	        	 String docLockType = st.nextToken();		// Doc Status, Ckecked out or Checkedin
	        	 
	        	 if (Integer.parseInt(nodeType)==1)
	        	 {
	  //      		 Document doc = new Document(DocID, ver);
	        		 docs.addElement(DocID);
	        	 }
	        }
//	       System.out.println ("VWClient vectorToAllDocuments docs " + docs);
	        return docs;
	    	}
    	catch (Exception  e)
    	{
    		//System.out.println ("VWClient vectorToAllDocuments Exception " + e.toString());
    		e.printStackTrace (System.out);
    	}
        return docs;
    }
    private Vector vectorToSearchDocuments(Vector v, String room, int loadFromModule)
    {
        Vector docs = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
        	try {
	            StringTokenizer st = new StringTokenizer
	                                        ((String) v.elementAt(i), "\t");
	            Document doc = new Document( Util.to_Number(st.nextToken()));
	            doc.setName(st.nextToken());
	            doc.setDocType(new DocType(Util.to_Number(st.nextToken()), 
	                                                               st.nextToken()));
	            doc.setCreator(st.nextToken());
	            doc.setCreationDate(st.nextToken());
	            doc.setModifyDate(st.nextToken());
	            doc.setVersion(st.nextToken());
	            if (doc.getVersion().equals("."))doc.setVersion("");
	            doc.setSessionId(Util.to_Number(st.nextToken()));
	            doc.setPageCount(Util.to_Number(st.nextToken()));
	            doc.setCommentCount(Util.to_Number(st.nextToken()));
	            doc.setRefCount(Util.to_Number(st.nextToken()));
	            doc.setLocation(st.nextToken());
	            try
	            {
	                doc.setSignType(Util.to_Number(st.nextToken()));
	                doc.setSignId(Util.to_Number(st.nextToken()));
	            }
	            catch(Exception e)
	            {
	                doc.setSignType(-1);
	                doc.setSignId(0);
	            }
	            try{
	            	doc.setCommentInclude(Util.to_Number(st.nextToken()));// Comments
	            	doc.setFreezed(Util.to_Number(st.nextToken()));
	            }catch(Exception ex){}
	            //Purpose: nodeType added for newSearch which will list folder/cabinet/drawer/document
	            try{
	            	doc.setNodeType(st.nextToken());// get node type
	            }catch(Exception ex){}
	            try{
	            	doc.setColorCode(st.nextToken());// set Color node
	            	doc.setScope(Util.to_Number(st.nextToken()));// set Scope private/public for color code
	            }catch(Exception ex){}
	            try{
	            	doc.setStatus(Util.to_Number(st.nextToken()));// set status for document is in Route or Not 
	            }catch(Exception ex){}           
	
	            doc.setRoomName(room);
	            doc.setRouteUserSignType(Util.to_Number(st.nextToken()));
	            doc.setNotificationExist(Util.to_Number(st.nextToken()));
	            //Added to Check the lock type of the Document,Gurumurthy.T.S 01-03-2014
	            doc.setLockType(Util.to_Number(st.nextToken()));
	            // For Recycle Tab search, we will get the desc from procedure which contains deleted username and host
	            doc.setDocPendingStatus(Util.to_Number(st.nextToken()));
	            
	    	    try{
	    	    	if (loadFromModule == 1 && doc.getSessionId()  == -1){
	    	    		doc.setDeletedUser(st.nextToken());
	    	    		doc.setDeletedHost(st.nextToken());
	    	    		doc.setDeletedTime(st.nextToken());
	    	    		doc.setDeletedLocation(st.nextToken());
	    	    	}
	    	    }catch (Exception e) {
					// TODO: handle exception    	    	
				}  
	            
	            docs.addElement(doc);
        	} catch (Exception e) {
        		VWClient.printToConsole("Exception while tokenizing search result :"+e);
        	}
        }
        return docs;
    }
    private Vector vectorToScs(Vector v)
    {
        Vector scs = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            StringTokenizer st = new StringTokenizer
                                        ( (String) v.elementAt(i), "\t");
            int id = Util.to_Number(st.nextToken()); 
            int nid = Util.to_Number(st.nextToken());
            String name = st.nextToken();
            String sUserName = st.nextToken();
            int type = Util.to_Number(st.nextToken());
            ShortCut sc = new ShortCut(id, nid, name, type);
            if(sUserName == null  || sUserName.trim().equals("")) 
                                                             sc.setPublic(true); 
            scs.addElement(sc);
        }
        return scs;
    }
    private Vector vectorToDocComments(Vector v)
    {
        Vector dComments = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            DocComment dComment = new DocComment((String) v.get(i));
            dComments.add(dComment);
        }
        return dComments;
    }    
    private Vector vectorToCreators(Vector v)
    {
        Vector creators = new Vector();
        Creator creator=null;
        for (int i = 0; i < v.size(); i++)
        {
        	try{
        		/*
        		 * CV2019 merges from CV10.2 line - else if condition has added
        		 */
        		
	            StringTokenizer st = new StringTokenizer
	                                        ( (String) v.elementAt(i), "\t");
	            if(st.countTokens()==4){
	             creator = new Creator(Util.to_Number(st.nextToken()),
	                                                    st.nextToken(),st.nextToken(),st.nextToken());
	            }else if(st.countTokens()==3){
	            	  creator = new Creator(Util.to_Number(st.nextToken()),
                              st.nextToken(),st.nextToken());
	            }
	            creators.addElement(creator);
        	}catch(Exception e){
        		printToConsole(e.toString());
        	}
        }
        return creators;
    }
    private void decryptRedactionsPassword(Vector pwords)
    {
         if( pwords == null || pwords.size()== 0 ) return;
         for(int i=0; i < pwords.size(); i++)
         {
            String encRedaction = (String) pwords.get(i);
            String decRedaction = decryptRP(encRedaction);
            pwords.removeElementAt(i);
            pwords.add(i, decRedaction);
         }
     }
    private String decryptRP(String encRedaction)
    {
        StringTokenizer st = new StringTokenizer(encRedaction, Util.SepChar);
        String decRedaction = "";
        while(st.hasMoreTokens())
        {
            for(int j=0; j < 2; j++) 
            {
                decRedaction += st.nextToken() + Util.SepChar;
            }
            String strPassword="";
            if(st.hasMoreTokens())
                strPassword=st.nextToken();
            decRedaction += decryptStr(strPassword);
        }
        return decRedaction;
     }
    private Vector stripIds(Vector v)
    {
        for (int i = 0; i < v.size(); i++)
        {
            StringTokenizer st = new StringTokenizer
                                               ( (String) v.elementAt(i), "\t");
            //st.nextToken();     //id to remove from selection values
            v.remove(i);
            v.add(i, st.nextToken());
        }
        return v;
    }
    private Vector vectorToPrincipals(Vector v)
    {
        Vector principals = new Vector();
        String userType = null;
        for (int i=0; i < v.size(); i++)
        {
        	StringTokenizer st = new StringTokenizer
                                                 ((String) v.elementAt(i),"\t");
			Principal principal = new Principal(Util.to_Number(st.nextToken()), st.nextToken(),
					Util.to_Number(st.nextToken()));			
			if (st.hasMoreTokens()) {
				userType = st.nextToken();
				principal.setAdmin(userType.equals("1"));
				principal.setSubAdmin(userType.equals("2"));
			}
			if (st.hasMoreTokens()) {
				principal.setHasASign(!st.nextToken().equals("0"));
				principal.setHasCSign(!st.nextToken().equals("0"));
			}
			// Update the field Email if email found... else make it ""...
			if (st.hasMoreTokens()) {
				String mailId = st.nextToken();
				if (mailId.trim().equals("-"))
					mailId = "";
				principal.setEmail(mailId);
			} else {
				principal.setEmail("");
			}
            principals.addElement(principal);
        }
        return principals;
    }
    private Vector vectorToDRSPrincipals(Vector v)
    {
        Vector principals = new Vector();
        for (int i=0; i < v.size(); i++)
        {
            StringTokenizer st = new StringTokenizer
                                                 ((String) v.elementAt(i),"\t");
            Principal principal = new Principal(
                                        Util.to_Number(st.nextToken()),
                                        st.nextToken(), 
                                        Util.to_Number(st.nextToken()), Util.to_Number(st.nextToken()));
            st.nextToken();
            st.nextToken();
        	//Update the field Email if email found... else make it ""...
            if(st.hasMoreTokens()){
            	String mailId = st.nextToken();
            	if(mailId.trim().equals("-"))	mailId = "";	
            	principal.setEmail(mailId);
            }
            else{
            	principal.setEmail("");
            }
            principals.addElement(principal);
        }
        return principals;
    }
    private Vector vectorToSearchConds(Vector v)
    {
        Vector scConds = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            SearchCond cond = new SearchCond((String) v.get(i));
            scConds.add(cond);
        }
        return scConds;
    }
    private int loadWTSLibrary()
    {
        if( !wtsLibraryLoaded )
        {
            if(VWCUtil.loadNativeLibrary("VWwts.dll", null))
                wtsLibraryLoaded = true;
            else
                return NativeLibraryErr;
        }
        return NoError;
    }
    private int loadOutputLibrary(String libraryPath)
    {
        if( !outputLibraryLoaded )
        {
            if(VWCUtil.loadNativeLibrary("VWOutput.dll", libraryPath))
                outputLibraryLoaded = true;
            else
                return NativeLibraryErr;
        }
        return NoError;
    }
    
    private int loadViewLibrary(String libraryPath)
    {
        if( !viewLibraryLoaded )
        {
            if(VWCUtil.loadNativeLibrary("VWView.dll", libraryPath))
        	viewLibraryLoaded = true;
            else
                return NativeLibraryErr;
        }
        return NoError;
    }   
    
    private int getDocType(int sid, int did, Vector types)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocType VWClient::::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getDocType(session.room, sid, did, types); 
            types.addAll(vectorToDocTypes(ret));
            VWClient.printToConsole("types VWClient::::"+types);
        	VWClient.printToConsole("Active Error in getDocType--->"+vws.getActiveError());
           // return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    private int addShortcut(int sid, int nid, ShortCut sc, int type) 
    {
        if (sc == null || sc.getName().equals("")) return invalidParameter;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in addShortcut::"+sid);
        if(session==null) return invalidSessionId;      
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        String user = (sc.getPublic()? "" : session.user); 
        try
        {
            return vws.addShortcut(session.room, sid, nid, 
                                                     sc.getName(), user, type);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    private int setSec(int sid, Node node)
    {
    	if (isAdmin(sid) == 0)
    	{
    		Acl acl = new Acl(node);
    		if (getAcl(sid, acl) != 0) return Error;
    		Iterator iterator = acl.getEntries().iterator();
    		AclEntry entry = (AclEntry) iterator.next();
    		if (entry.canAssign() != 1) return accessDenied;
    	}
    	int ret = getNodeProperties(sid, node);
    	if (ret != NoError || node.getName() == null || node.getName().equals("")) 
    		return invalidParameter;
		try {
			if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type) {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			}

		} catch (Exception e) {
		}
    	Security security = new Security(this, sid, node);
    	new Thread(security).start();
    	return NoError;
    }
    /**
     * Enhancement:-Register Security officer (USER name) for server at Connection & security.
     * If specified, Administaors will not have the default behaiviour of Assign permisison,
     * and  only this user can assign security to Administrator.
     * @param sid
     * @return
     */
    public int isSecurityAdmin(int sid){

        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isSecurityAdmin::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        try
        {
            return (vws.isSecurityAdmin(session.room, sid))? 1:0;
            
        }
        catch(Exception e)
        {
            return Error;
        }  
    
    }
    /**
     * Enhancement:-Register Security officer (USER name) for server at Connection & security.
     * If specified, Administaors will not have the default behaiviour of Assign permisison,
     * and  only this user can assign security to Administrator.
     * @param sid
     * @return
     */
    public int enableSecurityAdmin(int sid){
  	  Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        try
        {
            return (vws.enableSecurityAdmin(session.room, sid))? 1:0;
            
        }
        catch(Exception e)
        {
            return Error;
        }  
  }
    public int openFindDialog(int sid, Vector roomsList, Vector data){
    	return openFindDialog(sid, null, roomsList, data, 100, "", 0);
    }
    public int openFindDialog(int sid, Frame parent, Vector roomsList,
    		Vector data, int rowCount, String selectedPath, int nodeId) {
    	return openFindDialog(sid, null, roomsList, data, rowCount, selectedPath, nodeId, false, false);
    }
    public int openFindDialog(int sid, Frame parent, Vector roomsList,
    		Vector data, int rowCount, String selectedPath, int nodeId, boolean isCustomList, boolean isWebClientLocation) {
    	return openFindDialog(sid, null, roomsList, data, rowCount, selectedPath, nodeId, false, false, 0);
    }
    public int openFindDialog(int sid, Frame parent, Vector roomsList,
    		Vector data, int rowCount, String selectedPath, int nodeId, boolean isCustomList, boolean isWebClientLocation, int mode) {
    	try{
    			//findDialog = null; 
		    	if (findDialog == null) 
		    	{
		    		findDialog = new VWFindDlg(parent == null ? new Frame() : parent);
		    		
		    	}
	    		if(type == Client.Web_Client_Type || type == Client.WebL_Client_Type || type == Client.NWeb_Client_Type || type == Client.NWebL_Client_Type){
	    			findDialog.webClientType = true;
	    		}else{
	    			findDialog.webClientType = false;
	    		}
		    	findDialog.configure(sid, this, type, roomsList, selectedPath, nodeId, isCustomList, rowCount, isWebClientLocation, mode);
				while(!findDialog.connector.searchCancel){
					 synchronized(this){
			    		 try{
			    			 wait(300);
			    		 }catch(Exception ex){
			    		 }
			    	}
				}
				 synchronized(this){
		    		 try{
		    			 this.notify();
		    		 }catch(Exception ex){
		    			 System.out.println("notify thread exception " + ex.getMessage());
		    		 }
				 }
		    	if (findDialog.connector.getRoomList() != null && !findDialog.connector.isCancel) {
		    		data.add(findDialog.findPanel.ChkAppend.isSelected());
		    		data.add(findDialog.connector.getRoomList());
		    	}else
		    		data = null;		    			    		
    	}catch(Exception ex){
    		return Error;
    	}
	    	return NoError;
    }
    public int executeFind(int sid, Vector data){
    	if (findDialog != null){
    		findDialog.findPanel.doFind();
			while(!findDialog.connector.searchCancel){
				 synchronized(this){
		    		 try{
		    			 wait(300);
		    		 }catch(Exception ex){
		    			 System.out.println("waiting thread exception " + ex.getMessage());
		    		 }
		    	}
			}
			 synchronized(this){
	    		 try{
	    			 this.notifyAll();
	    		 }catch(Exception ex){
	    			 System.out.println("notify thread exception " + ex.getMessage());
	    		 }
			 }
	    	if (findDialog.connector.getRoomList() != null) {
	    		data.add(findDialog.findPanel.ChkAppend.isSelected());
	    		data.add(findDialog.connector.getRoomList());
	    	}
    	}
    	return NoError;
    }
    public int quickFind(int sid, Search searchList, Vector data) {
    	VWRoom roomInfo = null;
    	Session session = getSession(sid);    
    	VWClient.printToConsole("session id inside quickFind::::"+sid);
    	ArrayList roomList = new ArrayList();
    	roomInfo = new VWRoom(sid, session.room);
    	roomInfo.setSearch(searchList);
    	int resultCount = find(sid, searchList, false);
    	if (resultCount > 0) {
    		roomInfo.setResultCount(resultCount);    		
    		roomList.add(roomInfo);
    	}    	
    	data.add(false);
    	data.add(roomList);
    	return NoError;
    }    
    public int openRuleConfig(int sid, int nodeId, String nodeName)
    {
        try 
        {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) 
        {}
        VWRulesDlgSettings rulesDialog = new VWRulesDlgSettings(this, sid, nodeId, nodeName);
        new Thread(rulesDialog).start();
        return NoError;
    }
	
    /**
     * DRS related methods 30 Jan 2007
     * Enhancement:- CV10.1 
     * sendToOption boolean parameter is added for checking the document is sent to workflow from the 
     * toolbar/popup menu from DTC. 
     * @param sid
     * @param docTypeId
     * @param docId
     * @param documentName
     * @param sendToOption
     * @return
     */
    public int setAssignRoute(int sid, int docTypeId, int docId, String documentName, boolean sendToOption){
        try 
        {
        	if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type)
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e) 
        {
        }
        VWRouteList routeListDlg = new VWRouteList(this, sid, docTypeId, docId, documentName, sendToOption);
        /** Enhancement :CV10.1 commented for stopping the visibility of the dialog box for single work
         *  flow and no work flow from tool bar
         */
        //new Thread(routeListDlg).start();
    	return NoError;
    }
    
    /*CV2019 merges from SIDBI line--------------------------------***/
    public int setRouteStatus(int sid, int docId, RouteMasterInfo routeMasterInfo, int action, String documentName)
    {
    	return setRouteStatus( sid,  docId,  routeMasterInfo,  action,  documentName, 0);
    }

	//public int setRouteStatus(int sid, int docId, int routeId, int routeUserId, int routeUserLevel, boolean status, String documentName, String prevStatus)
    public int setRouteStatus(int sid, int docId, RouteMasterInfo routeMasterInfo, int action, String documentName, int assignSecurity)
    {
	    try 
	    {
	    	VWClient.printToConsole("sid inside setRouteStatus::::"+sid);
	    	VWClient.printToConsole("assignSecurity inside setRouteStatus :::: "+assignSecurity);
	    	
	    	if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type)
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }
	    catch(Exception e){
	    }
	    Session session = getSession(sid);
	    if (action == 1 && routeMasterInfo.getRouteUserSignType() >= 1){
			int signedType = isUserHasSign(sid, routeMasterInfo.getRouteUserSignType());
			if (signedType > 0) {
				drsValidateTaskDocumentVwctor = new Vector();
				drsValidateTaskDocument(sid, routeMasterInfo.getDocId(), routeMasterInfo.getRouteId(),action,"0", drsValidateTaskDocumentVwctor);
				if(drsValidateTaskDocumentVwctor!=null&&!drsValidateTaskDocumentVwctor.isEmpty()&&drsValidateTaskDocumentVwctor.size()>0&&!((String) drsValidateTaskDocumentVwctor.get(0)).trim().equalsIgnoreCase("-1") && (action==1 ||action==2)){
		    		JOptionPane.showMessageDialog(null, drsValidateTaskDocumentVwctor.get(0).toString());
		    	}
				else{
		    	VWLoginScreen login = new VWLoginScreen(this, sid, "Authenticate Signature", session.user, docId,routeMasterInfo, action, documentName, type);
				new Thread(login).start();	
				}
			}else{
				String signTypeStr = "Authorized";
				if (routeMasterInfo.getRouteUserSignType() == 2){
					signTypeStr = "Certified";
				}
				JOptionPane.showMessageDialog(null, signTypeStr + " signature is not available.\nPlease contact " + PRODUCT_NAME + " Adminstrator.");
			}
	    }else{ 
	    	drsValidateTaskDocumentVwctor = new Vector();
	    	String routeTaskSequence ="";
	    	routeTaskSequence =(action==1?"0":String.valueOf(routeMasterInfo.getLevelSeq()));
	    	drsValidateTaskDocument(sid, routeMasterInfo.getDocId(), routeMasterInfo.getRouteId(),action,"0", drsValidateTaskDocumentVwctor);
	    	if(drsValidateTaskDocumentVwctor!=null&&!drsValidateTaskDocumentVwctor.isEmpty()&&drsValidateTaskDocumentVwctor.size()>0&&!((String) drsValidateTaskDocumentVwctor.get(0)).trim().equalsIgnoreCase("-1") && (action==1 ||action==2)){
	    		JOptionPane.showMessageDialog(null, drsValidateTaskDocumentVwctor.get(0).toString());
	    	}
	    	else{
	    		VWRouteAction routeActionDlg = new VWRouteAction(this, sid, docId, routeMasterInfo, action, documentName, type, null, assignSecurity);	    
	    		new Thread(routeActionDlg).start();
	    	}
	    }
		return NoError;
    }
    
    public int setRouteStatusNew(int sid, int docId, RouteMasterInfo routeMasterInfo, int action, String documentName,String selTaskName,String doctypeName, int assignSecurity)
    {
	    try 
	    {
	    	VWClient.printToConsole("sid inside setRouteStatusNew::::"+sid+"selTaskName :"+selTaskName+"doctypeName ::"+doctypeName);
	    	VWClient.printToConsole("assignSecurity inside setRouteStatusNew :::: "+assignSecurity);
	    	
	    	if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type)
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }
	    catch(Exception e){
	    }
	    Session session = getSession(sid);
	    if (action == 1 && routeMasterInfo.getRouteUserSignType() >= 1){
			int signedType = isUserHasSign(sid, routeMasterInfo.getRouteUserSignType());
			if (signedType > 0) {
				drsValidateTaskDocumentVwctor = new Vector();
				drsValidateTaskDocument(sid, routeMasterInfo.getDocId(), routeMasterInfo.getRouteId(),action,"0", drsValidateTaskDocumentVwctor);
				if(drsValidateTaskDocumentVwctor!=null&&!drsValidateTaskDocumentVwctor.isEmpty()&&drsValidateTaskDocumentVwctor.size()>0&&!((String) drsValidateTaskDocumentVwctor.get(0)).trim().equalsIgnoreCase("-1") && (action==1 ||action==2)){
		    		JOptionPane.showMessageDialog(null, drsValidateTaskDocumentVwctor.get(0).toString());
		    	}
				else{
		    	VWLoginScreen login = new VWLoginScreen(this, sid, "Authenticate Signature", session.user, docId,routeMasterInfo, action, documentName, type);
				new Thread(login).start();	
				}
			}else{
				String signTypeStr = "Authorized";
				if (routeMasterInfo.getRouteUserSignType() == 2){
					signTypeStr = "Certified";
				}
				JOptionPane.showMessageDialog(null, signTypeStr + " signature is not available.\nPlease contact " + PRODUCT_NAME + " Adminstrator.");
			}
	    }else{
	    	 drsValidateTaskDocumentVwctor = new Vector();
	    	String routeTaskSequence ="";
	    	routeTaskSequence =(action==1?"0":String.valueOf(routeMasterInfo.getLevelSeq()));
	    	drsValidateTaskDocument(sid, routeMasterInfo.getDocId(), routeMasterInfo.getRouteId(),action,"0", drsValidateTaskDocumentVwctor);
	    	if(drsValidateTaskDocumentVwctor!=null&&!drsValidateTaskDocumentVwctor.isEmpty()&&drsValidateTaskDocumentVwctor.size()>0&&!((String) drsValidateTaskDocumentVwctor.get(0)).trim().equalsIgnoreCase("-1") && (action==1 ||action==2)){
	    		JOptionPane.showMessageDialog(null, drsValidateTaskDocumentVwctor.get(0).toString());
	    	} else{
	    		VWRouteActionNew routeActionDlg = new VWRouteActionNew(this, sid, docId, routeMasterInfo, action, documentName, type,selTaskName,doctypeName, assignSecurity);	    
	    		new Thread(routeActionDlg).start();
	    	}
	    }
		return NoError;
    }
    /*----------------------------End of SIDBI merges----------------------------------*/
    
    //In routing for end route action, multiple document can be selected in adminWise. To handle that, this method added. 
    public int setRouteStatusForEndRoute(int sid,  Vector selectedDocs)
    {
	    try 
	    {
	    	VWClient.printToConsole("sid inside setRouteStatusForEndRoute::::"+sid);
	    	if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type)
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	    }
	    catch(Exception e){
	    }
	    
	    //printToConsole("selectedDocs size :: "+selectedDocs.size());

	    if(selectedDocs != null && selectedDocs.size() == 1){
	    	RouteMasterInfo routeMasterInfo = (RouteMasterInfo) selectedDocs.get(0);
	    	VWRouteAction routeActioDlg = new VWRouteAction(this, sid, routeMasterInfo.getDocId(), routeMasterInfo, 4, routeMasterInfo.getDocName(), type);
	    	new Thread(routeActioDlg).start();
	    }else{
	    	VWRouteAction routeActionDlg = new VWRouteAction(this, sid, selectedDocs, type);
	    	new Thread(routeActionDlg).start();
	    }
		return NoError;
    }
    
    public int setRouteSummary(int sid, int docId, int mode, String documentName){
        try 
        {
        	/*CV2019 merges from SIDBI line------------------------------------------------***/
        	Vector wfDoctype=new Vector();
        	VWClient.printToConsole("sid inside setRouteSummary::::"+sid+"docId :::"+docId+"mode :::"+mode+"documentName :::"+documentName);
        	new Thread(new VWNewRouteSummary(type)).start();
        	Vector doctypes=new Vector();
        	Document doc=new Document(docId);
        	getDocumentInfo(sid,doc,doctypes);
        	// VWClient.printToConsole("doctypes value in summary is :::"+doctypes);
        	Document doc1=(Document)doctypes.get(0);
        	//VWClient.printToConsole("doc 1 object id is ::::"+doc1.getId());
        	VWClient.printToConsole("doc1.getDocType().getName() :"+doc1.getDocType().getName());
        	getWorkflowDoctype(sid,wfDoctype);
        	if (wfDoctype.size() > 0) {
	        	String wfDoctypStr=(String) wfDoctype.get(0);
	        	int sidbi = checkCustomizationFlagValues(sid, "SIDBI Customization");
	        	if(sidbi == 1 && wfDoctypStr.length()>0&&wfDoctypStr!=null){
	        		String[] wfdoctyparray = wfDoctypStr.split(";");
	        		List<String> wfdoctypeList=new ArrayList<String>();
	        		Collections.addAll(wfdoctypeList, wfdoctyparray);
	        		if(wfdoctypeList.contains(doc1.getDocType().getName())){
	        			try{
	        				VWClient.printToConsole("before calling new route summary object .....");
	        				VWNewRouteSummary routeSummaryDlg = new VWNewRouteSummary(this, sid, docId, mode, documentName);
	        				new Thread(routeSummaryDlg).start();
	        			}catch(Exception e){
	        				VWClient.printToConsole("Exception in setsummary::::"+e.getMessage());
	        			}
	        		}else{
	        			VWClient.printToConsole("Inside else1 of new route summary object .....");
	
	        			VWRouteSummary routeSummaryDlg = new VWRouteSummary(this, sid, docId, mode, documentName);
	        			new Thread(routeSummaryDlg).start();
	        		}
	        	} else {

	        		VWClient.printToConsole("Inside else2 of new route summary object .....");

	        		VWRouteSummary routeSummaryDlg = new VWRouteSummary(this, sid, docId, mode, documentName);
	        		new Thread(routeSummaryDlg).start();
	        	
	        	}
        	} 
        	VWClient.printToConsole("End of setRouteSummary");
        	/*------------------------End of SIDBI merges--------------------------------------*/

        }
        catch(Exception e){
        }
    	return NoError;
    }

    
    public int setDocumentAuditDetails(int sid, int docId, int mode, String documentName){
        try 
        {
        	if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type)
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e){
        }
        VWAuditTrailDetails auditTrailDlg = new VWAuditTrailDetails(this, sid, docId, mode, documentName);
        new Thread(auditTrailDlg).start();
    	return NoError;
    }   
    //This functions displays IndicesDlg...
    public int setIndicesDlg(int sid, int mode){
		return setIndicesDlg(sid, mode, 0);
    }
    public int setIndicesDlg(int sid, int mode, int nodeId){
        try 
        {
        	if (type == 1 || type == 9)
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(Exception e){
        }
        VWIndicesListDlg vwIndicesListDlg = new VWIndicesListDlg(this, sid, mode, nodeId);
        new Thread(vwIndicesListDlg).start();
    	return NoError;
    }   

    public int addCustomList(int sid, int objectType, String settingsId, String settingsValue){
	return addCustomList(sid, objectType, settingsId, settingsValue, 0);
    }
    public int addCustomList(int sid, int objectType, String settingsId, String settingsValue, int nodeId){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in addCustomList::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        String user = session.user;                
        try{        	
        	vws.addCustomList(sid, session.room, user, objectType, settingsId, settingsValue, nodeId);        	
        }catch(Exception ex){
        	return Error;
        }
    	return NoError;
    }   
    /***CV2019 Merges from CV10.2 Line*****************************/
    //Read/Write the customised settings information to the Database
    public Vector getCustomList(int sid, int objectType, Vector settingsValue){
    	return getCustomList(sid, objectType, settingsValue, 0);
    }
    /***End of CV10.2 Line merges*****************************/
    public Vector getCustomList(int sid, int objectType, Vector settingsValue, int index){
        Session session = getSession(sid);
        //if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        //if (vws == null) return ServerNotFound;
        String user = session.user;
    	Vector result = new Vector();
        try{

        	result = vws.getCustomList(sid, session.room, user, objectType, result, index);
        	//settingsValue.addAll(result);
 
        }catch(Exception ex){
        	//return Error;
        }
    	return result;
    	//return NoError;
    }   
    public int deleteCustomList(int sid){
	return deleteCustomList(sid, 0, 0);
    }
    public int deleteCustomList(int sid, int mode){
	return deleteCustomList(sid, mode, 0);
    }
    // Delete customised settings information from Database to restore default columns    
    public int deleteCustomList(int sid, int mode, int nodeId){
        Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in deleteCustomList::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try{

        	vws.deleteCustomList(sid, session.room, session.user, mode, nodeId);
 
        }catch(Exception ex){
        	return Error;
        }
    	return NoError;
    }   
    
    private Vector vectorToServerSchema(Vector v)
    {
        Vector dss = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            ServerSchema ss = new ServerSchema( (String) v.elementAt(i));
            dss.addElement(ss);
        }
        return dss;
    }
    //------------------------------P U P L I C S-------------------------------
    //------------------------------P U P L I C S-------------------------------
    //------------------------------P U P L I C S-------------------------------
    /**Returns a list of registered Server names.
     * @param servers The list of registered server names
     * @return 0 for success or negative value error code
     */
    public int getServers(Vector servers)
    {
        if (servers == null) return invalidParameter;
        Enumeration keys = registeredVWSs.keys();
        while (keys.hasMoreElements())
        {
            servers.addElement((String) keys.nextElement()); 
        }
        return NoError;
    }
    
    /** Returns a list of online Rooms for a ViewWise Server.
     * @param server The ViewWise server name for which to return the rooms
     * @param rooms The list of room names
     * @return 0 for success or negative value error code
     */
    public int getRooms(String server, Vector rooms)
    {
    	VWClient.printToConsole("Server inside getRooms method "+server);
    	try
    	{
    		if (rooms == null || server == null) return invalidParameter;
    		VWS vws = getServer(server);
    		VWClient.printToConsole("vws :"+vws);
    		if (vws == null) return ServerNotFound;    		
    		boolean isHosting = false;
    		try{
    			isHosting = CVPreferences.getHosting();
    		}catch (Exception e) {
    			VWClient.printToConsole("Exception while getting check server reg : "+e.getMessage());
    			isHosting = false;
    		}
    		//VWClient.printToConsole("checkForServer :: "+checkForServer);

    		if(isHosting){
    			getRoomsFromServer(server, rooms);
    		}else{
    			VWClient.printToConsole("Before calling getRoomNames method......");
    			Vector ret = vws.getRoomNames();
    			VWClient.printToConsole("After calling getRoomNames method......"+ret);
    			VWClient.printToConsole("Client type :"+type);
    			//	 Start 
    			//   Enhancement of Show/Hide Room; Based on user selection client can view the client.
    			if (type == Client.Adm_Client_Type || type==Client.Web_Client_Type || type== Client.NWeb_Client_Type || type==Client.WebL_Client_Type || type== Client.NWebL_Client_Type) {
    				rooms.addAll(ret);
    			}
    			else {
    				/*
						Issue No / Purpose:  <05/24/2006 VWC Registration>
						Created by: <Pandiya Raj.M>
						Date: <14 Jun 2006>
						Read the registry entry from the HKEY_CURRENT_USER.
    				 */

    				Preferences clientPref =  null;
    				String displayFlag = null;
    				try{
    					clientPref =  Preferences.userRoot().node("/computhink/" + PRODUCT_NAME.toLowerCase() + "/vwc/servers/"+server);
    					displayFlag  = clientPref.get("rooms","");
    				}catch(Exception e){displayFlag = "";}

    				//Util.Msg(null,"Client displayFlag "+displayFlag,"VWClient");
    				StringTokenizer sToken = null;
    				boolean flag = false;
    				VWClient.printToConsole("displayFlag flag :"+displayFlag);
    				if (displayFlag != null && displayFlag.length()>0) {
    					sToken = new StringTokenizer(displayFlag,";");
    					String currentRoom  = "";	           
    					while(sToken.hasMoreTokens()){
    						currentRoom = String.valueOf(sToken.nextToken());
    						VWClient.printToConsole("room name inside loop :"+currentRoom);
    						//Util.Msg(null,"Room "+ret.contains(currentRoom),currentRoom);
    						if (ret.contains(currentRoom)){            		
    							//Util.Msg(null,"Remove Room "+currentRoom,"VWC");
    							rooms.add(currentRoom);
    							flag = true;
    						}
    					}
    				}
    				VWClient.printToConsole("flag :"+flag);
    				if(!flag)
    					rooms.addAll(ret);   
    			}
    		}
    		VWClient.printToConsole("Final rooms list  :"+rooms);
    		// End
    		return NoError;
    	}
    	catch(Exception re)
    	{
    		return Error;
    	}
    }
    
    
    /**
     * getDBDetails fro CVWeb
     * Modified Date:- 28_09_2017
     * Modified By:-Madhavan
     * @param server
     * @param rooms
     * @return
     */
    public int getDBDetails(String server,Vector rooms)
    {
    	try{
    		
    		if (rooms == null || server == null) return invalidParameter;
    		VWS vws = getServer(server);
    		if (vws == null) return ServerNotFound;
    		Vector dbConfiguration=new Vector();
			RoomProperty[] roomProperty = vws.getRooms();
    		for(int i=0;i<roomProperty.length;i++){
    			VWClient.printToConsole("roomProperty[i].getDatabaseEngineName()"+roomProperty[i].getDatabaseEngineName());
    			VWClient.printToConsole("roomProperty[i].getDatabaseName()"+roomProperty[i].getDatabaseName());
    			VWClient.printToConsole("roomProperty[i].getDatabasePassword()"+roomProperty[i].getDatabasePassword());
    			VWClient.printToConsole("roomProperty[i].getDatabasePort()"+roomProperty[i].getDatabasePort());

    			String dbDetails=roomProperty[i].getDatabaseEngineName()+Util.SepChar+roomProperty[i].getDatabaseName()+Util.SepChar+roomProperty[i].getDatabaseHost()+Util.SepChar+roomProperty[i].getDatabaseUser()+Util.SepChar+roomProperty[i].getDatabasePassword()+
    					Util.SepChar+roomProperty[i].getDatabasePort()+Util.SepChar+roomProperty[i].isEnabled()+Util.SepChar+roomProperty[i].getName()+Util.SepChar+roomProperty[i].getAuthenticationMode();
    			VWClient.printToConsole("dbDetails::::"+dbDetails);
    			System.out.println("dbDetails::::"+dbDetails);
    			dbConfiguration.add(dbDetails);
    			
    		}
    		rooms.addAll(dbConfiguration);
    		
    	}catch(Exception e){
    		return Error;
    	}
		return NoError;
    }
    
    

    private int getRoomsFromServer(String server, Vector rooms) {
    	try{
    		if (rooms == null || server == null) return invalidParameter;
    		VWS vws = getServer(server);
    		if (vws == null) return ServerNotFound;

    		ServerSchema schema = vws.getSchema();
    		String serverIP = schema.getAddress();

    		InetAddress serverInetAddress = InetAddress.getByName(serverIP);
    		InetAddress clientInetAddress = InetAddress.getLocalHost();

    		boolean isServerMachine = false;
    		if(clientInetAddress.equals(serverInetAddress)){
    			isServerMachine = true;
    		}else{
    			Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
    			for (; n.hasMoreElements();)
    			{
    				NetworkInterface e = n.nextElement();
    				//VWClient.printToConsole("Interface: " + e.getName());
    				Enumeration<InetAddress> a = e.getInetAddresses();
    				for (; a.hasMoreElements();)
    				{
    					InetAddress clientNetworkInetAddr = a.nextElement();
    					//VWClient.printToConsole("inetaddr : "+clientNetworkInetAddr);
    					
    					if(clientNetworkInetAddr.equals(serverInetAddress)){
    						isServerMachine = true;
    					}
    				}
    			}
    		}

    		Vector ret = vws.getRoomNames();
    		//VWClient.printToConsole("ret :: "+ret);
    		
    		if ((isServerMachine && type == Client.Adm_Client_Type) || type == Client.IXR_Client_Type || type==Client.Web_Client_Type || type== Client.NWeb_Client_Type || type==Client.WebL_Client_Type || type== Client.NWebL_Client_Type) {
    			rooms.addAll(ret);
    		}
    		else {
    			Preferences clientPref =  null;
    			String displayFlag = null;
    			try{
    				clientPref =  Preferences.userRoot().node("/computhink/" + PRODUCT_NAME.toLowerCase() + "/vwc/servers/"+server);
    				displayFlag  = clientPref.get("rooms","");
    			}catch(Exception e){displayFlag = "";}


    			StringTokenizer sToken = null;
    			VWClient.printToConsole("displayFlag  :: "+displayFlag );
    			//boolean flag = false;
    			if (displayFlag != null && displayFlag.length()>0) {
    				sToken = new StringTokenizer(displayFlag,";");
    				String currentRoom  = "";	           
    				while(sToken.hasMoreTokens()){
    					currentRoom = String.valueOf(sToken.nextToken());
    					if (ret.contains(currentRoom)){            		
    						rooms.add(currentRoom);
    						//flag = true;
    					}
    				}
    			}else if(isServerMachine){
    				rooms.addAll(ret);   
    			}
    			//System.out.println("rooms ::: "+rooms);
    		}

    		return NoError;


    	}catch (Exception e) {
    		VWClient.printToConsole("Exception in getRooms() : "+e.getMessage());
    		return Error;
    	}
    }
    
    /** To get the session id
     * @param sessionId
     * @return returns session object
     */
    public Session getSession(int sessionId)
    {
        return (Session) sessions.get(new Integer(sessionId));
    }
    public String getCacheFolder(int sessionId)
    {
        Session session = getSession(sessionId);
        if(session==null) 
        {
            return "";
        }
        return session.cacheFolder;
    }
    
    /** Destroys a VWClient instance
     * @return 0 for success or negative value error code
     */
    public int shutdown()
    {
        VWClient.printToConsole("inside shutdown");
        if (mySchema.comport == 0)  return NoError;
        if (Util.killServer(mySchema) ) return NoError;
        return Error;
    }
    public int addListener(VWClientListener obj)
    {
        if (obj == null) return invalidParameter;
        MDSS listener = (MDSS) Util.getServer(mySchema);
        if (listener == null) return Error;
        try
        {
            listener.addListener(obj);
            return NoError;
        }
        catch (Exception e){return Error;}
    }
    public int registerServer(String server, String address, int port)
    {
        if (registeredVWSs.containsKey(server)) return NoError;
        ServerSchema ss = new ServerSchema();
        ss.name = server;
        ss.address = address;
        ss.comport = port;
        ss.type = SERVER_VWS;
        registeredVWSs.put(server, ss);
        return NoError;
    }
    public int registerServer(ServerSchema ss)
    {
        ss.type = SERVER_VWS;
        ss.isProxySet=true;
        registeredVWSs.put(ss.name, ss);
        return NoError;
    }
    public int isHacker(String server, String room)
    {
        VWS vws = getServer(server);
        if (vws == null) return ServerNotFound;
        try
        {
             return vws.isHacker(room, mySchema.address); 
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getMyPrincipal(int sid, Principal principal)
    {
        if (principal == null) return invalidParameter;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getMyPrincipal::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector result=new Vector();
            	principal.setName(session.user);
            result= vws.getPrincipal(session.room, sid, principal ,result); 
            Principal princ =new Principal((String)result.get(0));
            principal.set(princ);
            VWClient.printToConsole("Active Error in getMyPrincipal--->"+vws.getActiveError());
          //  return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
            
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    public int getNodePath(int sid, int nodeId, Vector output)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNodePath::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector location = vws.getNodePath(session.room, sid, nodeId);
            output.addAll(location);
            VWClient.printToConsole("Active Error in getNodePath--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    public int getNodeProperties(int sid, Node node)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNodeProperties::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Node n = vws.getNodeProperties(session.room, sid, node); 
            node.set(n);
            VWClient.printToConsole("Active Error in getNodeProperties--->"+vws.getActiveError());
            //return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
   
    /* Issue Description - AIP Hangs when processing large rooms
*  Developer - Nebu Alex
*  Code Description - This function makes a database call and
*					- returns node information, 
* Arguments required - Nodes parentid and nodes name
* 
*/
	public int getNodeDetails(int sid, String parentid, String nodeName, Vector nodes)
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getNodeDetails::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getNodeDetails(session.room,sid, parentid, nodeName, nodes); 
            nodes.addAll(vectorToNodes(ret));
            VWClient.printToConsole("Active Error in getNodeDetails--->"+vws.getActiveError());
            //return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    // End of code change
    // Fixing Issue 735, add the sort argument to retrieve the data based on sort value. Sort will be done based on 'sort' value
    /** Returns a list of child nodes of the specified node.
     * @param sid Session id obtained by login 
     * @param node The parent node for which to return child nodes
     * @param coid Always set to 0
     * @param nodes The list of child nodes returned
     * @return 0 for success or negative value error code
     */
    public int getNodeContents(int sid, Node node, int coid, Vector nodes)
    {
    	return getNodeContents(sid, node, coid, nodes, 0);
    }

	/** Returns a list of child nodes of the specified node.
	 * @param sid Session id obtained by login 
     * @param node The parent node for which to return child nodes
     * @param coid Always set to 0
     * @param nodes The list of child nodes returned
	 * @param sort 1 - sort navigator, 0 do not sort navigator
	 * @return 0 for success or negative value error code
	 */
	public int getNodeContents(int sid, Node node, int coid, Vector nodes, int sort)
    {
        Session session = getSession(sid);
      	VWClient.printToConsole("sessionId in getNodeContents::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getNodeContents
                                 (session.room, sid, node.getId(), coid, nodes, sort); 
            nodes.addAll(vectorToNodes(ret));
            VWClient.printToConsole("Active Error in getNodeContents--->"+vws.getActiveError());
           // return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
	
	/** Returns a list of child nodes of the specified node.
	 * @param sid Session id obtained by login 
     * @param node The parent node for which to return child nodes
     * @param coid Always set to 0
     * @param nodes The list of child nodes returned
	 * @param sort 1 - sort navigator, 0 do not sort navigator
	 * @return 0 for success or negative value error code
	 */
	public int getNodeContentsWS(int sid, Node node, int coid, Vector nodes, int sort)
    {
        Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getNodeContentsWS::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getNodeContentsWS
                                 (session.room, sid, node.getId(), coid, nodes, sort); 
            nodes.addAll(vectorToNodes(ret));
            VWClient.printToConsole("Active Error in getNodeContentsWS--->"+vws.getActiveError());
           // return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
	
	 /** Returns a list of child nodes of the specified node with Acl information.
     * @param sid Session id obtained by login 
     * @param node The parent node for which to return child nodes
     * @param coid Always set to 0
     * @param nodes The list of child nodes returned
     * @return 0 for success or negative value error code
	 */
	public int getNodeContentsWithAcl(int sid, Node node, int coid, Vector nodes, int sort)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNodeContentsWithAcl::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getNodeContentsWithAcl
                                 (session.room, sid, node.getId(), coid, nodes, sort); 
            nodes.addAll(vectorToNodes(ret, false));
            VWClient.printToConsole("Active Error in getNodeContentsWithAcl--->"+vws.getActiveError());
           // return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
	public int copyNodeSecurity(int sid, int nodeId, String userGroup, String permission)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in copyNodeSecurity::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        String ip = mySchema.address;
        try
        {
            int ret = vws.copyNodeSecurity
                                 (session.room, sid, nodeId, ip, userGroup, permission); 
            
            return ret;
        }
        catch(Exception e)
        {
            return Error;
        }
    }	
	
    /** Returns a list of child Documents of the specified node id.
     * @param sid Session id obtained by login
     * @param nid The node id for which to return child documents
     * @param coid row count
     * @param docs The list of child documents returned
     * @return 0 for success or negative value error code
     */
	
    /**  getNodeDocuments override method changes are done by Srikanth 
     *    for getting the refNode info along with normal nodes
     */
	
    public int getNodeDocuments(int sid, int nid, int coid,  Vector docs)
    {
    	return getNodeDocuments(sid, nid, coid, 0, docs);
    }
    public int getNodeDocuments(int sid, int nid, int coid, int refNid,  Vector docs)
    {
    	
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNodeDocuments::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getNodeDocuments
                                           (session.room, sid, nid, coid, refNid, docs); 
            docs.addAll(vectorToDocuments(ret, false));
            VWClient.printToConsole("Active Error in getNodeDocuments--->"+vws.getActiveError());
           // return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    public int getAIPNodeDocuments(int sid, int nid, int coid, int docTypeId, String curIndexVal, Vector docs)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getAIPNodeDocuments::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getAIPNodeDocuments(session.room, sid, nid, coid, docTypeId, curIndexVal, docs); 
            docs.addAll(vectorToDocuments(ret, false));
            VWClient.printToConsole("Active Error in getAIPNodeDocuments--->"+vws.getActiveError());
           // return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
       
    public int getAllNodeDocuments(int sid, int nid, String ipAddress, Vector docs)
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getAllNodeDocuments::"+sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	
    	try{
    		Vector ret = vws.getAllNodeDocuments (session.room, sid, nid, ipAddress, docs); 
    		//System.out.println ("VWClient getAllNodeDocuments  ret " + ret);   
    		docs.addAll(vectorToAllDocuments(ret, false));   
    		VWClient.printToConsole("Active Error in getAllNodeDocuments--->"+vws.getActiveError());
    		//return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
    	}catch (Exception e)
    	{
    		//System.out.println ("VWClient getAllNodeDocuments  Exception " + e.getMessage() ); 
    		//System.out.println ("VWClient getAllNodeDocuments  Exception " + e.toString());
    		e.printStackTrace(System.out);
    		return Error;
    	}
    }
    public int getDocumentByInfo(int sid, String nodePath, String docTypeName,
                   String indexName,String indexValue,int indexKey, Vector docs)
    {
        Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getDocumentByInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getDocumentByInfo(session.room, sid, nodePath,
                docTypeName,indexName,indexValue,indexKey);
            docs.addAll(vectorToDocuments(ret, false));
            VWClient.printToConsole("Active Error in getDocumentByInfo--->"+vws.getActiveError());
           // return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Returns a lis of parent nodes of the specified node.
     * @param sid  Session id obtained by login
     * @param node The node for which to return parent nodes
     * @param nodes The list of parent nodes returned
     * @return 0 for success or negative value error code
     */
    public int getNodeParents(int sid, Node node, Vector nodes)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNodeParents::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getNodeParents(session.room, sid, node.getId(), 
                                                                         nodes);
            nodes.addAll(StringToNodes(ret));
            VWClient.printToConsole("Active Error in getNodeParents--->"+vws.getActiveError());
            //return vws.getActiveError();
        	if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Creates a node
     * @param sid Session id obtained by login
     * @param node The node to Create
     * @return 0 for success or negative value error code
     */
    public int createNode(int sid, Node node)
    {
        return createNode(sid, node, 0);
    }
    
    /** Creates a node with unique name option
     * @param sid Session id obtained by login
     * @param node The node to Create
     * @param withUniqueName 2 unique name , 1 not unique name
     * @return 0 for success or negative value error code
     */
    public int createNode(int sid, Node node,int withUniqueName)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in createNode::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	//Desc   :Replace TAB character in nodename with a space. Restoring documents
        	//		  were creating problem in AdminWise, since TAB is the delimeter,
        	//		  used in DB side.
        	//Author :Nishad Nambiar
        	//Date   :23-Nov-2007
        	String paramNodeName = node.getName() ;
        	String nodeName = paramNodeName.replaceAll("	"," ");
        	node.setName(nodeName.trim());
        	int ret = vws.createNode(session.room, sid, node, withUniqueName);
        	node.setId(ret);
        	return ret;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Deletes a node
     * @param sid
     * @param node
     * @return 0 for success or negative value error code
     */
    public int deleteNode(int sid, Node node)
    {
        return deleteNode(sid, node.getId(), true);
    }
    
    /** Deletes the document
     * @param sid Session id obtained by login
     * @param doc The document to be deleted
     * @return 0 for success or negative value error code
     */
    public int deleteDocument(int sid, Document doc)
    {
        return deleteNode(sid, doc.getId(), true);
    }
    public int purgeDocument(int sid, Document doc)
    {
        return deleteNode(sid, doc.getId(), false);
    }
    public int purgeNode(int sid, Node node)
    {
        return deleteNode(sid, node.getId(), false);
    }
    
    /** Renames a Node
     * @param sid Session id obtained by login
     * @param node The node to rename
     * @return 0 for success or negative value error code
     */
    public int renameNode(int sid, Node node)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in renameNode::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.renameNode(session.room, sid, node);
        }
        catch(Exception e)
        {
            return Error;
        }
    }

    /** Copies a Node to a different parent Node
     * @param sid Session id obtained by login
     * @param node The Node to copy
     * @return 0 for success or negative value error code
     */
    public int copyNode(int sid, Node node)
    {
	return copyNode(sid, node, 0);
    }
    public int copyNode(int sid, Node node, int nodeProperties)
    {
    	Vector storageVect=new Vector();
    	int retdssspace=0;
    	Session session = getSession(sid);
    	String storageType="";
    	String azureUserName="";
    	String azurePassword="";
    	int azureCopyret=0;
    	try{

    		getAzureStorageCredentials(sid,node.getId(),storageVect);
    		if(storageVect!=null&&storageVect.size()>0){
    			VWClient.printToConsole("storageVect inside copynode::::"+storageVect.get(0).toString());
    			String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
    			VWClient.printToConsole("Stroage type from storage array in copynode"+storageArray[7]);
    			storageType=storageArray[7];
    			azureUserName=storageArray[8];
    			azurePassword=storageArray[9];
    		}
    		if(storageType.equals("On Premises")){
    			Vector docIds=new Vector();
    			Vector returnVect= new Vector();
    			Vector latestdocid=new Vector();
    			/*getLatestDocId(sid,latestdocid);
    			int maxDocId=Integer.parseInt(latestdocid.get(0).toString());*/
    			//CV10 Issue fix modified from maxdocid from procedure to node Id
    			Document maxdoc=new Document(node.getId());
    			checkAvailableSpaceInDSS(sid,maxdoc,"",returnVect);
    			long availableSpace=(Long) returnVect.get(0);
    			availableSpace=availableSpace*1024;
    			long registrySpace=(Long) returnVect.get(1);
    			registrySpace=registrySpace*1024;
    			getChildNodes(sid,node.getId(),docIds);
    			int count=docIds.size();
    			long size=0;
    			for( int i=0;i<count;i++) {
    				Document doc=new Document(VWUtil.to_Number((String)docIds.get(i)));
    				getDocumentSize(sid, doc, true);
    				size+=doc.getVWDoc().getSize();
    			}
    			size=size/1024;
    			if(registrySpace>(availableSpace-size)){
    				SendEmailFromDSS(availableSpace/1024,sid,maxdoc);
    				return dssCriticalError;
    			}
    		}
    		/**
    		 * Enhancement:-Sequence Unique to Room
    		 * Copy Node validation call 
    		 */
    		int ret=1;
    		ret=checkValidateCopySeq(sid, node, nodeProperties);
    		if(ret==0)
    			return CopyNodeUniqSeqViolation;
    	}
    	catch(Exception e){
    		VWClient.printToConsole("Exception in DSS check of copy"+e.getMessage());
    	}
    	//  Session session = getSession(sid);
    	if(session==null) return invalidSessionId;
    	VWClient.printToConsole("sessionId in copyNode::"+sid);
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try
    	{
    		if(storageType.equals("On Premises")){
    			return vws.copyNode(session.room, sid, node, nodeProperties);
    		}								

    		if(storageType.equalsIgnoreCase("Azure Storage")){
    			String sourceFile = "";
    			String destinationFile = "";
    			Node tempNode = new Node(node.getId());
    			Node nodeObj= vws.getNodePropertiesByNodeID(session.room, sid, tempNode);
    			int currentParentId = tempNode.getParentId();
    			Vector path = new Vector();
    			getNodePath( sid,currentParentId,path);
    			if(path!=null && path.size()>0){
    				sourceFile = path.get(0).toString();
    			}
    			path = new Vector();
    			int copyToParentId = node.getParentId();
    			VWSLog.add("copyToParentId:::::::::"+copyToParentId);
    			getNodePath(sid, copyToParentId, path);
    			VWSLog.add("path 2:::::::::"+path);
    			if(path!=null && path.size()>0){
    				destinationFile = path.get(0).toString();
    				VWSLog.add("destPath 1:::::::::"+destinationFile);
    			}
    			Vector result=vws.azureCopyNode(session.room, sid, node, nodeProperties);
    			if(result==null||result.size()==0)
    				return 0;
    				if(result!=null&&result.size()>0){
    				for(int count=0;count<result.size();count++){
    					String totalPath[]=result.get(count).toString().split(Util.SepChar);
    					sourceFile=totalPath[0];
    					VWClient.printToConsole("sourcePath from storage :::::::"+sourceFile);
    					destinationFile=totalPath[1];
    					VWClient.printToConsole("destPath from storage::::::::::"+destinationFile);

    					try {
    						CloudStorageAccount storageAccount=null;
    						CloudFileClient fileClient =null;
    						CloudFileClient fileClient1 =null;
    						CloudFile cloudFile=null;
    						CloudFileShare share=null;
    						CloudFileShare share1=null;
    						CloudFileDirectory rootDir=null;
    						CloudFileDirectory rootDir1=null;
    						CloudFileDirectory sampleDir =null;
    						CloudFileDirectory sampleDir1 =null;
    						ListFileItem fileItem=null;
    						String accountName="AccountName="+azureUserName+";";
    						String password="AccountKey="+azurePassword;
    						String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
    						System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
    						storageAccount = CloudStorageAccount.parse(storageConnectionString1);
    						fileClient = storageAccount.createCloudFileClient();
    						String pattern = Pattern.quote(System.getProperty("file.separator"));
    						String sourcepath[]=sourceFile.split(pattern);
    						String destpath[]=destinationFile.split(pattern);
    						for(int i=0;i<sourcepath.length-1;i++){
    							if(i==2)
    							{
    								share = fileClient.getShareReference(sourcepath[i]);
    								if(!share.exists())
    									share.create();
    							}
    							else if(i==3)
    							{
    								sampleDir=	share.getRootDirectoryReference().getDirectoryReference(sourcepath[i]);
    								if(!sampleDir.exists()){
    									sampleDir.create();

    								} 
    								rootDir = sampleDir;
    								sampleDir = null;

    							}
    							else if(i>=4)
    							{
    								if ((i+1) != sourcepath.length) {
    									sampleDir =	rootDir.getDirectoryReference(sourcepath[i]);
    									if(!sampleDir.exists()){
    										sampleDir.create();
    									}
    									rootDir = sampleDir;
    									sampleDir = null;
    								}
    								else if ((i+1) == sourcepath.length) {
    									sampleDir =	rootDir.getDirectoryReference(sourcepath[i]);
    									if(!sampleDir.exists()){
    										sampleDir.create();
    									}
    									rootDir = sampleDir;

    									sampleDir = null;
    								}
    							}

    						}
    						for(int i=0;i<destpath.length-1;i++){
    							if(i==2)
    							{/*
       						VWClient.printToConsole("destpath[i] inside 1 is zero-----"+destpath[i]);
    					share1 = fileClient1.getShareReference(destpath[i]);
    					VWClient.printToConsole("share1:::::::::"+share1.getUri());
    					if(!share1.exists())
    						share1.create();
    					VWClient.printToConsole("After share creation");
    							 */}
    							else if(i==3)
    							{

    								//For destination folder reference
    								sampleDir1=	share.getRootDirectoryReference().getDirectoryReference(destpath[i]);
    								if(!sampleDir1.exists()){
    									sampleDir1.create();

    								} 
    								rootDir1 = sampleDir1;
    								sampleDir1 = null;
    							}
    							else if(i>=4)
    							{

    								if ((i+1) != destpath.length) {
    									sampleDir1 =	rootDir1.getDirectoryReference(destpath[i]);
    									if(!sampleDir1.exists()){
    										sampleDir1.create();
    									}
    									rootDir1 = sampleDir1;
    									sampleDir1 = null;
    								}
    								else if ((i+1) == destpath.length) {
    									sampleDir1 =	rootDir1.getDirectoryReference(destpath[i]);
    									if(!sampleDir1.exists()){
    										sampleDir1.create();
    									}
    									rootDir1 = sampleDir1;

    									sampleDir1 = null;
    								}
    							}

    						}

    						VWClient.printToConsole("root directory rootDir uri::::"+rootDir.getUri());
    						VWClient.printToConsole("root directory rootDir1 uri::::"+rootDir1.getUri());
    						sourceFile=sourceFile.substring(0,sourceFile.length()-1);
    						String srcReference=sourceFile.substring(sourceFile.lastIndexOf(File.separator)+1,sourceFile.length());
    						destinationFile=destinationFile.substring(0,destinationFile.length()-1);
    						String destReference=destinationFile.substring(destinationFile.lastIndexOf(File.separator)+1,destinationFile.length());
    						sampleDir=rootDir.getDirectoryReference(srcReference);
    						sampleDir1=rootDir1.getDirectoryReference(destReference);
    						VWClient.printToConsole("Sample directory::::::::::"+sampleDir.getUri());
    						VWClient.printToConsole("Sample directory1::::::::::"+sampleDir1.getUri());
    						for ( ListFileItem fileItem1 : sampleDir.listFilesAndDirectories() ) {
    							VWClient.printToConsole("fileItem1:::::::::::"+fileItem1.getUri());
    							String filesURI=fileItem1.getUri().toString();
    							// String pattern1 = Pattern.quote(System.getProperty("file.separator"));
    							String fileName=filesURI.substring(filesURI.lastIndexOf("/")+1, filesURI.length());
    							VWClient.printToConsole("fileName::::"+fileName);
    							CloudFile cloudFile1=sampleDir.getFileReference(fileName);
    							CloudFile cloudFile2=sampleDir1.getFileReference(fileName);
    							if(!sampleDir1.exists()){
    								sampleDir1.create();
    							}
    							cloudFile2.startCopy(cloudFile1);
    							VWClient.printToConsole("file names:::::"+fileName);
    							if(cloudFile2.exists()){
    								VWClient.printToConsole("Inse azure copy cloud file exist:::::");
    								azureCopyret=1;
    							}else{
    								return ViewWiseErrors.azurecopyFailed;
    							}
    						}
    					}

    					catch (Exception e) {
    						VWClient.printToConsole("Exception while copy file in azure storage :::"+e.getMessage());
    						// Handle the exception
    					}
    				}
    		} 
    		}

    	}
    	catch(Exception e)
    	{
    		return Error;
    	}
    	return azureCopyret;
    	}
     
    /**Moves a Node to a different parent Node
     * @param sid Session id obtained by login
     * @param node 	The node to move
     * @return 0 for success or negative value error code
     */
    public int moveNode(int sid, Node node)
    {
	return moveNode(sid, node, 0);
    }
    public int moveNode(int sid, Node node, int nodeProperties)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in moveNode::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.moveNode(session.room, sid, node, nodeProperties);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
// Fixing Issue No 694. DTC passing the frames count. to displyaed the number of pages.
    public int getDocCoverPage(int sid, Document doc)
    {
    	return getDocCoverPage(sid,doc,0);
    }
// Fixing Issue No 694. DTC passing the frames count. to displyaed the number of pages.
    public int getDocCoverPage(int sid, Document doc,int frames)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocCoverPage::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        Vector docs = new Vector();
        getDocumentInfo(sid, doc, docs);
        if (docs.size() == 0) return Error;
        Document document = (Document) docs.elementAt(0);
        getDocumentIndices(sid, document);
        Vector vers = new Vector();
        getDocumentVers(sid, document, vers);
        if (vers.size() > 0) 
            document.setVersion(((Document) vers.elementAt(vers.size()-1)).
                                                                  getVersion());
        
        Vector signs = new Vector();
        getDocumentSigns(sid, document, signs);
// Fixing Issue No 694. DTC passing the frames count. to displyaed the number of pages.
        if (getCoverPage(sid, document, signs, session.room, frames)) return NoError;
        return Error;
    }
    
    /** Returns a list of available DocTypes in a ViewWise Room
     * @param sid Session id obtained by login
     * @param types The list of doctypes returned
     * @return 0 for success or negative value error code
     */
    public int getDocTypes(int sid, Vector types)
    {
        return getDocType(sid, 0, types);
    }
    
    /** Returns a particular DocType information in a ViewWise Room
     * @param sid Session id obtained by login
     * @param dt The document type
     * @param types The list of doctypes returned
     * @return 0 for success or negative value error code
     */
    public int getDocType(int sid, DocType dt, Vector types)
    {
        return getDocType(sid, dt.getId(), types);
    }
    
    /**CV10.2 Merges - Workflow report enhancement*******************************************/
    public ArrayList<CustomList> getDocTypeIndices(int sid, ArrayList<DocType> docTypeList) {
    	ArrayList<CustomList> customColumnList = null;
    	CustomList customColumn = null;
    	ArrayList<Index> indicesList = null;
    	Index indices = null;
    	if (docTypeList != null && docTypeList.size() > 0) {
    		customColumnList = new ArrayList<CustomList>();
    		for (DocType docType : docTypeList) {
    			VWClient.printToConsole("docID in getDocTypeIndices VWClient::::"+docType.getId());
    			getDocTypeIndices(sid, docType, "");
    			indicesList = new ArrayList<Index>(docType.getIndices());
    			if (indicesList != null && indicesList.size() > 0) {
    				for (int listIndex = 0; listIndex < indicesList.size(); listIndex ++) {
     					indices = indicesList.get(listIndex);
     					customColumn = new CustomList(indices.getId(), indices.getName(), String.valueOf(indices.getType()));
     					customColumnList.add(customColumn);
     				}
    			}
    		}
    	}
    	return customColumnList;
    }
    /**End of CV10.2 Merges*************************************************************/
    
    /** Returns the indices of a DocType           
     * @param sid Session id obtained by login
     * @param dt The doctype for which to return the indices
     * @return 0 for success or negative value error code
     */
    public int getDocTypeIndices(int sid, DocType dt)
    {
    	return getDocTypeIndices(sid, dt, "");
    }
    public int getDocTypeIndices(int sid, DocType dt, String fromRoute)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocTypeIndices VWClient::::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        Vector indices = new Vector();
        try
        {
        	 VWClient.printToConsole("Before calling  getDocTypeIndices procedured::::");
            Vector ret = 
                   vws.getDocTypeIndices(session.room, sid, dt.getId(), indices);
            VWClient.printToConsole("After calling  getDocTypeIndices procedured::::"+ret);
            indices.addAll(vectorToIndices(ret));
            VWClient.printToConsole("After setting to indices vector in   getDocTypeIndices metho::::"+indices);
            dt.setIndices(indices);
            ret.removeAllElements();
            VWClient.printToConsole("Before start of for loop in getDocTypeIndices JNI::::");
            /**
             * SelectIndex values should not call from Route
             */
            VWClient.printToConsole("fromRoute::::"+fromRoute);
            if(fromRoute.length()==0){
            	for (int i=0; i < indices.size(); i++)
            	{
            		Index idx = (Index) indices.get(i);
            		VWClient.printToConsole("Index idx ::::"+idx);
            		/**
            		 * Ehancement:Sow2096_891-Add Default Index with users and groups
            		 * Code added get the selection list in dtc.
            		 */
            		VWClient.printToConsole("idx.getSelectValue()::::"+idx.getSelectValue());
            		VWClient.printToConsole("idx.getType()::::"+idx.getType());
            		if(idx.getSelectValue()==1&&idx.getType()==Index.IDX_TEXT){
            			ret = vws.populateSelectionIndex(session.room, sid,-1, ret);	
            			String defVal=idx.getDefaultValue();
            			idx.setDef(stripIds(ret));
            			idx.setDef(defVal);
            			ret.removeAllElements();
            		}
            		VWClient.printToConsole("Before calling populateSelectionIndex 1.....");
            		if(idx.getSelectValue()==2&&idx.getType()==Index.IDX_TEXT){
            			ret = vws.populateSelectionIndex(session.room, sid,-2, ret);	
            			String defVal=idx.getDefaultValue();
            			idx.setDef(stripIds(ret));
            			idx.setDef(defVal);
            			ret.removeAllElements();
            		}
            		VWClient.printToConsole("Before calling populateSelectionIndex 2.....");
            		if ( idx.getType() == Index.IDX_SELECTION )
            		{
            			ret = vws.populateSelectionIndex(session.room, 
            					sid, idx.getId(), ret);
            			String defVal=idx.getDefaultValue();
            			idx.setDef(stripIds(ret));
            			//if (this.type != Client.Adm_Client_Type)
            				idx.setDef(defVal);
            				ret.removeAllElements();
            		}
            		VWClient.printToConsole("After calling populateSelectionIndex 2.....");
            	}
            }
            VWClient.printToConsole("End of for loop in getDocTypeIndices JNI::::");
            VWClient.printToConsole("Active Error in getDocTypeIndices--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
        	VWClient.printToConsole("Exception in getDocTypeIndices "+Error+" ErrMessage :"+e.getStackTrace());
            return Error;
        }
    }
//This method returns all the indices with default values selected in the Default Properties for the node.
    public int getAllDocTypeIndices(int sid, int nodeId, Vector indices){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getAllDocTypeIndices::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;       
        try
        {
            Vector ret = 
                   vws.getAllDocTypeIndices(session.room, sid, nodeId, indices);
            indices.addAll(vectorToIndices(ret, 1));
            ret.removeAllElements();
            for (int i=0; i < indices.size(); i++)
            {
                Index idx = (Index) indices.get(i);
                if ( idx.getType() == Index.IDX_SELECTION )
                {
                    ret = vws.populateSelectionIndex(session.room, 
                                                         sid, idx.getId(), ret);
                    String defVal=idx.getDefaultValue();
                    idx.setDef(stripIds(ret));
                    idx.setDef(defVal);
                    ret.removeAllElements();
                }
            }
            VWClient.printToConsole("Active Error in getAllDocTypeIndices--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
            
        }
        catch(Exception e)
        {
            return Error;
        }
    }   
    //This method return the list of indices for given document type id and the default values from the node index rules will be retrieved
    public int getDocTypeIndicesWithRules(int sid, int nodeId, DocType dt){
    	Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocTypeIndicesWithRules::"+sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	Vector indices = new Vector();
    	if (vws == null) return ServerNotFound;       
    	try
    	{
    		Vector ret = 
    				vws.getDocTypeIndicesWithRules(session.room, sid, nodeId, dt.getId(), indices);
    		indices.addAll(vectorToIndices(ret));
    		dt.setIndices(indices);
    		ret.removeAllElements();
    		for (int i=0; i < indices.size(); i++)
    		{
    			Index idx = (Index) indices.get(i);
    			if ( idx.getType() == Index.IDX_SELECTION )
    			{
    				ret = vws.populateSelectionIndex(session.room, 
    						sid, idx.getId(), ret);
    				String defVal=idx.getDefaultValue();
    				idx.setDef(stripIds(ret));
    				idx.setDef(defVal);
    				ret.removeAllElements();
    			}
    			/**
    			 * Ehancement:Sow2096_891-Add Default Index with users and groups
    			 * Code added get the selection list in dtc.
    			 */
    			if(idx.getSelectValue()==1&&idx.getType()==Index.IDX_TEXT){
    				ret = vws.populateSelectionIndex(session.room, sid,-1, ret);	
    				String defVal=idx.getDefaultValue();
    				idx.setDef(stripIds(ret));
    				idx.setDef(defVal);
    				ret.removeAllElements();
    			}
    			if(idx.getSelectValue()==2&&idx.getType()==Index.IDX_TEXT){
    				ret = vws.populateSelectionIndex(session.room, sid,-2, ret);	
    				String defVal=idx.getDefaultValue();
    				idx.setDef(stripIds(ret));
    				idx.setDef(defVal);
    				ret.removeAllElements();
    			}
    		}
    		  VWClient.printToConsole("Active Error in getDocTypeIndicesWithRules--->"+vws.getActiveError());
    		//return vws.getActiveError();
    		  if (sid>0 && vws.getActiveError()<0 ){
              	return NoError;
              } else {
              	return vws.getActiveError();
              }
    	}
    	catch(Exception e)
    	{
    		return Error;
    	}
    }
    //This methos Create/update the nodeIndexrules to the database.
    public int updateNodeIndexRules(int sid, NodeIndexRules nodeRules){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateNodeIndexRules::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;       
        try
        {
        	nodeRules.setCreatorName(session.user);
            int ret = 
                   vws.updateNodeRules(session.room, sid, nodeRules);            
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }    	
    }
    // This method return the node index rules for the given node id. 
    public int getNodeIndexRules(int sid, int nodeId, Vector nodeRules){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNodeIndexRules::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;       
        try
        {
        	NodeIndexRules nodeRulesObj = new NodeIndexRules();
            Vector ret = 
                   vws.getNodeRules(session.room, sid, nodeId);
            nodeRules.addAll(ret);
            return NoError;
        }
        catch(Exception e)
        {
        	VWSLog.errMsg("Exception in getDocTypeIndices "+Error+" ErrMessage :"+e.getStackTrace());
            return Error;
        }    	
    } 
    // This method retur the rule exist node id  and default document type id
    public int checkNodeIndexRules(int sid, int nodeId, Vector nodeRules){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in checkNodeIndexRules::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;       
        try
        {
            Vector ret = 
                   vws.checkNodeIndexRules(session.room, sid, nodeId);
            nodeRules.addAll(ret);
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }    	
    }
    // This method return binary value depends on default value exist or not for the node properties
    public int checkNodeTreeIndexRules(int sid, int nodeId, Vector nodeExists){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in checkNodeTreeIndexRules::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;       
        try
        {
            Vector ret = 
                   vws.checkNodeTreeIndexRules(session.room, sid, nodeId);
            nodeExists.addAll(ret);
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }    	
    }
    // This method delete rule for a node id
    public int deleteNodeRules(int sid, int nodeId){
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in deleteNodeRules::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;       
        try
        {
            return vws.deleteNodeRules(session.room, sid, nodeId);
        }
        catch(Exception e)
        {
            return Error;
        }    	
    }
    
    /** Creates a document
     * @param sid Session id obtained by login
     * @param doc The Document to Create
     * @return 0 for success or negative value error code
     */
    public int createDocument(int sid, Document doc)
    {
    	try{
    		String storageType="";
    		Vector storageVect=new Vector();
    		getAzureStorageCredentials(sid,doc.getId(),storageVect);
    		if(storageVect!=null&&storageVect.size()>0){
    			String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
    			storageType=storageArray[7];
    		}
        	VWClient.printToConsole("Storage type in create document::::"+storageType);
    		if(storageType.equals("On Premises")){
    			Vector result=new Vector();
    			DSS ds = null;
    			getLatestDocId(sid,result);
    			int maxDocId=Integer.parseInt(result.get(0).toString());
    			Document maxdoc=new Document(maxDocId);
    			Vector size=new Vector();
    			int retval=checkAvailableSpaceInDSS(sid,maxdoc,"",size);
    			long availableSpace=(Long) size.elementAt(0);
    			if(retval==dssCriticalError){
    				SendEmailFromDSS(availableSpace,sid,maxdoc);
    				return dssCriticalError;  
    			}
    		}
    	}
    	catch(Exception excep){
    		VWClient.printToConsole("DSS space check exception inside Createdoc:::: "+excep.getMessage());
    		return Error;
    	}
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in createDocument::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Document retDoc = vws.createDocument(session.room, sid, doc);
           doc.set(retDoc);
           return doc.getId(); 
           
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    //Add the Encoding method to read the encoding type from server registry. To avoid the conflict in the Check out.
    public String getEncoding(int sid)
    {
        Session session = getSession(sid);
        String defaultEncoding = "UTF-8";
        VWS vws = getServer(session.server);
        if (vws == null) return defaultEncoding;
        try
        {
          String encoding = vws.getEncoding();
           return encoding;
        }
        catch(Exception e)
        {
            return defaultEncoding ;
        }
    }
    
    /** Returns the values of the Indicies of a Document
     * @param sid Session id obtained by login
     * @param doc The Document for which to return indices values
     * @return 0 for success or negative value error code
     */
    public int getDocumentIndices(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentIndices::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector ret = vws.getDocumentIndices
                    (session.room, sid, doc.getId(), doc.getVersion(), new Vector());
           //Added for Change Document type Enhancement,Gurumurthy.T.S
           VWDocBackup.docIndi= new Vector();
           VWDocBackup.docIndi.addAll(ret);
           //
           setIndices(ret, doc); 
           VWClient.printToConsole("Active Error in getDocumentIndices--->"+vws.getActiveError());
           //return vws.getActiveError();
           if (sid>0 && vws.getActiveError()<0 ){
           	return NoError;
           } else {
           	return vws.getActiveError();
           }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Sets values of a Document's indices
     * @param sid Session id obtained by login
     * @param doc The document referencing the indices for which to set values
     * @return 0 for success or negative value error code
     */
    public int setDocumentIndices(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDocumentIndices::"+sid);
        if(session==null) return invalidSessionId;    
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.setDocumentIndices(session.room, sid, doc);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    public int updateDocumentDocType(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateDocumentDocType::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        String storageType="";
        String storagePath="";
        String azureUserName="";
        String azurePassword="";
        try
        {

        	Vector storageVect=new Vector();
        	getAzureStorageCredentials(sid,doc.getId(),storageVect);
        	if(storageVect!=null&&storageVect.size()>0){
        		String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
        		storagePath=storageArray[5];
        		storageType=storageArray[7];
        		azureUserName=storageArray[8];
        		azurePassword=storageArray[9];
        	}
        	if(storageType.equals("On Premises")){
        		return vws.updateDocumentDocType(session.room, sid, doc);
        	}else if(storageType.equals("Azure Storage")){
        		Vector versionDt=vws.updateAzureDocumentDocType(session.room, sid, doc);
        		VWClient.printToConsole("version Dt inside updateDocumentDocType:::::: "+versionDt);
        		if(versionDt!=null&&versionDt.size()>0){
        			CloudStorageAccount storageAccount=null;
        			CloudFileClient fileClient =null;
        			CloudFile cloudFile=null;
        			CloudFile cloudFile1=null;
        			CloudFile cloudFile2=null;
        			CloudFileShare share=null;
        			CloudFileDirectory rootDir=null;
        			CloudFileDirectory sampleDir =null;
        			try{
        				String accountName="AccountName="+azureUserName+";";
        				String password="AccountKey="+azurePassword;
        				String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
        				System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
        				storageAccount = CloudStorageAccount.parse(storageConnectionString1);
        				fileClient = storageAccount.createCloudFileClient();
        				String pattern = Pattern.quote(System.getProperty("file.separator"));
        				storagePath=storagePath+"\\"+String.valueOf(doc.getId());
        				String dbpath[]=storagePath.split(pattern);
        				for(int i=0;i<dbpath.length;i++){
        					VWClient.printToConsole("value of i------"+i);
        					if(i==2)
        					{
        						VWClient.printToConsole("dbpath[i] inside 1 equal to 2-----"+dbpath[i]);
        						share = fileClient.getShareReference(dbpath[i]);
        						VWClient.printToConsole("After share creation");
        					}
        					else if(i==3)
        					{
        						VWClient.printToConsole("dbpath[i] inside i equal to 3"+dbpath[i]);
        						sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
        						rootDir = sampleDir;
        						sampleDir = null;
        					}
        					else if(i>=4)
        					{
        						VWClient.printToConsole("dbpath[i] inside i greater than 4"+dbpath[i]);
        						if ((i+1) != dbpath.length) {
        							VWClient.printToConsole("Inside if condition of  not equal to dbpath length :::::"+i);
        							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        							if(!sampleDir.exists()){
        								sampleDir.create();
        							}
        							rootDir = sampleDir;
        							sampleDir = null;
        						}
        						else if ((i+1) == dbpath.length) {
        							VWClient.printToConsole("Inside else if conditionof equal to dbpath lenght ::::::::::"+i);
        							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        							if(!sampleDir.exists()){
        								sampleDir.create();
        							}
        							rootDir = sampleDir;
        							sampleDir = null;
        						}
        					}

        				}
        				sampleDir = rootDir;
        				VWClient.printToConsole("sampleDir get uri changedoctype:::::"+sampleDir.getUri());
        				cloudFile = sampleDir.getFileReference("all.zip");
        				cloudFile1=sampleDir.getFileReference(versionDt.get(0).toString());
        				if(cloudFile.exists()){
        					cloudFile1.startCopy(cloudFile.getUri());
        					VWClient.printToConsole("sampleDir get uri changedoctype cloudFile:::::"+cloudFile.getUri());
        					VWClient.printToConsole("sampleDir get uri changedoctype cloudFile:::::"+cloudFile1.getUri());
        				}
        			}catch(Exception e){
        				VWClient.printToConsole("Exception while azure change document type::::"+e.getMessage());
        				return ViewWiseErrors.azureChangeDoctypeFailed;
        			}


        		}
        	}
        }
        catch(Exception e)
        {
            return Error;
        }
		return NoError;
    }
    
    /** Sets the active version for a Document
     * @param sid Session id obtained by login
     * @param doc The document for which to set the active version
     * @return 0 for success or negative value error code
     */
    public int setDocActiveVer(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDocActiveVer::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        String storageType="";
        String azureUserName="";
		String azurePassword="";
        try
        {
        	Vector storageVect=new Vector();
        	getAzureStorageCredentials(sid,doc.getId(),storageVect);
        	if(storageVect!=null&&storageVect.size()>0){
        		VWClient.printToConsole("storageVect inside setDocActiveVer::::"+storageVect.get(0).toString());
        		String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
        		storageType=storageArray[7];
        		azureUserName=storageArray[8];
        		azurePassword=storageArray[9];
        	}
    		if(storageType.equals("On Premises")){
    			return vws.setDocActiveVer(session.room, sid, doc.getId(), 
    					doc.getVersion());
    		}else if(storageType.equals("Azure Storage")){

    			CloudStorageAccount storageAccount=null;
    			CloudFileClient fileClient =null;
    			CloudFile cloudFile=null;
    			CloudFile cloudFile1=null;
    			CloudFile cloudFile2=null;
    			//CloudFile cloudFile3=null;
    			CloudFileShare share=null;
    			CloudFileDirectory rootDir=null;
    			CloudFileDirectory sampleDir =null;
    			String dbStoragePath="";
    			String sspath="";
    			String ver="";
    			String newVers="";
    			Vector result=vws.setDocActiveVerAzure(session.room, sid, doc.getId(),doc.getVersion());
    			if(result!=null&&result.size()>0){
    				String pathArray[]=	result.get(0).toString().split(Util.SepChar);
    				sspath=pathArray[0];
    				ver=pathArray[1];
    				newVers=pathArray[2];
    			}
    			try{
    				String accountName="AccountName="+azureUserName+";";
    				String password="AccountKey="+azurePassword;
    				String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
    				System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
    				storageAccount = CloudStorageAccount.parse(storageConnectionString1);
    				dbStoragePath=sspath.substring(0,sspath.length()-1);
    				File dstFile=new File(mySchema.path);
    				fileClient = storageAccount.createCloudFileClient();
    				String pattern = Pattern.quote(System.getProperty("file.separator"));
    				String dbpath[]=dbStoragePath.split(pattern);
    				for(int i=0;i<dbpath.length;i++){
    					VWClient.printToConsole("value of i------"+i);
    					if(i==2)
    					{
    						VWClient.printToConsole("dbpath[i] inside 1 equal to 2-----"+dbpath[i]);
    						share = fileClient.getShareReference(dbpath[i]);
    						VWClient.printToConsole("After share creation");
    					}
    					else if(i==3)
    					{
    						VWClient.printToConsole("dbpath[i] inside i equal to 3"+dbpath[i]);
    						sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
    						rootDir = sampleDir;
    						sampleDir = null;
    					}
    					else if(i>=4)
    					{
    						VWClient.printToConsole("dbpath[i] inside i greater than 4"+dbpath[i]);
    						if ((i+1) != dbpath.length) {
    							VWClient.printToConsole("Inside if condition of  not equal to dbpath length :::::"+i);
    							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
    							if(!sampleDir.exists()){
    								sampleDir.create();
    							}
    							rootDir = sampleDir;
    							sampleDir = null;
    						}
    						else if ((i+1) == dbpath.length) {
    							VWClient.printToConsole("Inside else if conditionof equal to dbpath lenght ::::::::::"+i);
    							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
    							if(!sampleDir.exists()){
    								sampleDir.create();
    							}
    							rootDir = sampleDir;
    							sampleDir = null;
    						}
    					}

    				}
    				sampleDir = rootDir;
    				VWClient.printToConsole("sampleDir get uri setDocActiveVer:::::"+sampleDir.getUri());
    				cloudFile = sampleDir.getFileReference("all.zip");
    				cloudFile1=sampleDir.getFileReference(ver);
    				cloudFile2=sampleDir.getFileReference(newVers);
    				VWClient.printToConsole("cloudFile setDocActiveVer::::::::::::::"+cloudFile.getUri());
    				VWClient.printToConsole("cloudFile1 setDocActiveVer::::::::::::::"+cloudFile1.getUri());
    				VWClient.printToConsole("cloudFile2 setDocActiveVer::::::::::::::"+cloudFile2.getUri());
    				if(cloudFile.exists()&&cloudFile1.exists()){
    					if(cloudFile2.exists()){
    						cloudFile.delete();
    						cloudFile.startCopy(cloudFile1.getUri());
    					}else{
    						cloudFile2.startCopy(cloudFile.getUri());
    						cloudFile.delete();
    						cloudFile.startCopy(cloudFile1.getUri());
    					}
    				}
    			}catch(Exception e){
    				VWClient.printToConsole("Exception while setactiver version::::"+e.getMessage());
    				return ViewWiseErrors.azureActiveVersionFailed;
    			}

    		}
        }
        catch(Exception e)
        {
        	VWClient.printToConsole("Exception while set active version::::::"+e.getMessage());
            return Error;
        }
		return NoError;
    }
    
    /** Returns a list of Documents that represent different versions of a Document
     * @param sid Session id obtained by login
     * @param doc The Document for which to return document vesrions
     * @param vers A list of document verions
     * @return 0 for success or negative value error code
     */
    public int getDocumentVers(int sid, Document doc, Vector vers)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentVers::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector ret = vws.getDocumentVers
                                         (session.room, sid, doc.getId(), vers);
           vers.addAll(vectorToDocuments(ret, true));
           VWClient.printToConsole("Active Error in getDocumentVers--->"+vws.getActiveError());
         //  return vws.getActiveError(); 
           if (sid>0 && vws.getActiveError()<0 ){
        	   return NoError;
           } else {
        	   return vws.getActiveError();
           }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /**
     * This method is created to fetch versions of a document from EWA
     * @author apurba.m
     * @param sid
     * @param doc
     * @param vers
     * @return
     */
    public int getDocumentVersEWA(int sid, Document doc, Vector vers)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentVersEWA ::::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector ret = vws.getDocumentVers(session.room, sid, doc.getId(), vers);
           vers.addAll(ret);
           VWClient.printToConsole("Active Error in getDocumentVersEWA :::: "+vws.getActiveError());
         //  return vws.getActiveError(); 
           if (sid>0 && vws.getActiveError()<0 ){
        	   return NoError;
           } else {
        	   return vws.getActiveError();
           }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Checks wether the specified value violates a unique Index
     * @param sid - Session id obtained by login
     * @param dt - The doctype of the index 
     * @param index - The unique index for which to check the value
     * @param value - The value to check
     * @return 1 (true), 0 (false)
     */
    public int violatesUnique(int sid, DocType dt, Index index, String value)
    {
        return violatesUnique(sid, 0, dt, index, value);
    }
    /** Checks wether the specified value violates a unique Index
     * @param sid - Session id obtained by login
     * @param nodeId - node id
     * @param dt - The doctype of the index 
     * @param index - The unique index for which to check the value
     * @param value - The value to check
     * @return 1 (true), 0 (false)
     */
    public int violatesUnique(int sid, int nodeId, DocType dt, Index index, 
                                                                   String value)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in violatesUnique::"+sid);
        if(session==null) return invalidSessionId;     
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.violatesUnique(session.room, sid, nodeId, dt.getId(), 
                                                          index.getId(), value);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /**  Returns all comments for a Document.   
     * @param sid  Session id obtained by login
     * @param doc The document for which need to get comments
     * @param comments The list of doccomments returned
     * @return 0 for success or negative value error code
     */
    public int getDocumentComment(int sid, Document doc, Vector comments)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentComment::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector ret = vws.getDocumentComment
                                     (session.room, sid, doc.getId(),doc.getVersion(), comments);
           comments.addAll(vectorToDocComments(ret));
           VWClient.printToConsole("Active Error in getDocumentComment--->"+vws.getActiveError());
        //   return vws.getActiveError(); 
           if (sid>0 && vws.getActiveError()<0 ){
        	   return NoError;
           } else {
        	   return vws.getActiveError();
           }
           
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    
    /** Deletes Document's comment
     * @param sid Session id obtained by login
     * @param dComment  The comment to delete
     * @return 0 for success or negative value error code
     */
    public int deleteDocumentComment(int sid,DocComment dComment)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteDocumentComment::"+sid);
        if(session==null) return invalidSessionId;   
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             return  vws.deleteDocumentComment
                                     (session.room, sid, dComment.getId());
        }
        catch(Exception e)
        {
            return Error;
        }     
    }
    
    /** Locks a Document to render in view only mode
     * @param sid Session id obtained by login
     * @param doc The document to lock
     * @return 0 for success or negative value error code
     */
    public int lockDocument(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in lockDocument::"+sid);
        if(session==null) return invalidSessionId;  
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.lockDocument(session.room, sid, doc.getId());
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    public int updateLockedDocument(int sid, int nodeId)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateLockedDocument::"+sid);
        if(session==null) return invalidSessionId;     
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.updateLockedDocument(session.room, sid, nodeId);
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    
    /** un locks document
     * @param sid Session id obtained by login
     * @param doc The document to be unlocked
     * @return 0 for success or negative value error code
     */
    public int unlockDocument(int sid, Document doc)
    {
        return unlockDocument(sid, doc, 0);
    }
    
    /** un locks document
     * @param sid Session id obtained by login
     * @param doc The document to be unlocked
     * @param force true - Locked by another user will be unlocked by admin
     * @return 0 for success or negative value error code
     */
    public int unlockDocument(int sid, Document doc, int force)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in unlockDocument::"+sid);
        if(session==null) return invalidSessionId;   
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.unlockDocument(session.room, sid, doc.getId(), force);
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    
    /** Returns a list of users who have a lock on the specified Document
     * @param sid Session id obtained by login
     * @param doc The document for which to return lock owners
     * @param name The list of use names
     * @return 0 for success or negative value error code
     */
    public int getLockOwner(int sid, Document doc, Vector name)
    {
        return getLockOwner(sid, doc, name, false);
    }
    
    /** Returns a list of users who have a lock on the specified Document
     * @param sid Session id obtained by login
     * @param doc The document for which to return lock owners
     * @param name The list of use names
     * @param withMe true, false  
     * @return 0 for success or negative value error code
     */
    public int getLockOwner(int sid, Document doc, Vector name, boolean withMe)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getLockOwner::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector ret = vws.getLockOwner(session.room, sid, doc.getId(), name, 
                                                                        withMe);
           name.addAll(ret);
           VWClient.printToConsole("Active Error in getLockOwner--->"+vws.getActiveError());
           //return vws.getActiveError();
           if (sid>0 && vws.getActiveError()<0 ){
           	return NoError;
           } else {
           	return vws.getActiveError();
           }
           
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    // Enhancement for 567 Start
    public int getSessionOwner(int sid, int did, Vector name)
    {
        return getSessionOwner(sid, did, name, false);
    }
    public int getSessionOwner(int sid, int did, Vector name, boolean withMe)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getSessionOwner::"+sid);
        if(session==null) return invalidSessionId;
        
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
             Vector ret = vws.getSessionOwner(session.room, sid, did, name, 
                                                                        withMe);
             name.addAll(ret);
             VWClient.printToConsole("Active Error in getSessionOwner--->"+vws.getActiveError());
            // return vws.getActiveError();
             if (sid>0 && vws.getActiveError()<0 ){
             	return NoError;
             } else {
             	return vws.getActiveError();
             }
             
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    
    //Nebu
    // End of enhansment for 567
    public int getRedactions(int sid, Document doc, Vector redactions)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRedactions::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector ret = vws.getRedactions(session.room, sid, doc.getId(), 
                                                  doc.getVersion(), redactions);
           decryptRedactionsPassword(ret);
           redactions.addAll(ret);
           VWClient.printToConsole("Active Error in getRedactions--->"+vws.getActiveError());
          // return vws.getActiveError();
           if (sid>0 && vws.getActiveError()<0 ){
           	return NoError;
           } else {
           	return vws.getActiveError();
           }
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    public int getAllRedactions(int sid, Document doc, Vector redactions)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getAllRedactions::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getAllRedactions(session.room, sid, doc, redactions);
            redactions.addAll(ret);
            VWClient.printToConsole("Active Error in getAllRedactions--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /** Sets a comment for a Document
     * @param sid Session id obtained by login
     * @param docComment The docComment referencing the Document for which to set the comment
     * @return 0 for success or negative value error code
     */
    public int setDocumentComment(int sid, DocComment docComment)
    {
        return setDocumentComment(sid, docComment, false);
    }
    /**
     * CV10.1 Enhancement :-comments to initiate V/R
     * @param sid
     * @param docComment
     * @param withCheck
     * @return
     *docRestoreFlag is added newly and its value is 0 for other clients and 
     *1 if it is from adminwise restore.
     */
    public int setDocumentComment(int sid, DocComment docComment, boolean withCheck)
    {
        return setDocumentComment(sid, docComment, false,0);
    }
    
    /**
     * @param sid Session id obtained by login
     * @param docComment The docComment referencing the Document for which to set the comment
     * @param withCheck false - new comment, true - update old comment 
     * @return 0 for success or negative value error code
     */
    public int setDocumentComment(int sid, DocComment docComment, boolean withCheck,int docRestoreFlag )
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDocumentComment::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            if(docComment.getComment() !=null &&  
                        docComment.getComment().length() > MAX_COMMENT_LENGTH)
              docComment.setComment
                      (docComment.getComment().substring(0,MAX_COMMENT_LENGTH));
           Vector ret = vws.setDocumentComment(session.room, sid,  docComment, 
                                                                     withCheck,docRestoreFlag);                     
           if(ret !=null && ret.size() > 0)
           {
                StringTokenizer st = new StringTokenizer
                                            ((String) ret.get(0), Util.SepChar);
              docComment.setId(Util.to_Number(st.nextToken()));
              docComment.setUser(st.nextToken());
              docComment.setTimeStamp(st.nextToken());
           }
           return docComment.getId();
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    public int setRedactions(int sid, Document doc, String redactions) 
    {              
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setRedactions::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        StringTokenizer st = new StringTokenizer(redactions, "\t");
        redactions="";
        try
        {        
        int count = 0;	
        int first = 1;
        while (st.hasMoreTokens())
        {
            redactions+=st.nextToken() + Util.SepChar;
            redactions+=st.nextToken() + Util.SepChar;
            redactions+=st.nextToken() + Util.SepChar;
            redactions+=st.nextToken() + Util.SepChar;
            
            if (st.hasMoreTokens())
                redactions += encryptStr(st.nextToken());
            redactions+= (st.hasMoreTokens()?Util.SepChar:"");
            count++;
            if (redactions != null && redactions.length() > 3950){
            	vws.setRedactions(session.room, sid, doc.getId(),  first + Util.SepChar + count + Util.SepChar + redactions);
            	redactions = "";
            	count = 0;
            	first  =0;
            }
        }
        if (redactions != null && redactions.length() > 0){
        	return vws.setRedactions(session.room, sid, doc.getId(), first + Util.SepChar + count + Util.SepChar + redactions);
        }
        }
        catch(Exception e)
        {
            return Error;
        }
        return NoError;
    }
    
    /** Returns the next serial number for a sequence Index.
     * @param sid Session id obtained by login
     * @param index The sequence index for which to return the next serial number 
     * @return 0 for success or negative value error code
     */
    public int getNextSerial(int sid, Index index)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNextSerial::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Index idx = vws.getNextSerial(session.room, sid, index);
           index.setDef((String) idx.getDef().get(0));
           VWClient.printToConsole("Active Error in getNextSerial--->"+vws.getActiveError());
          // return vws.getActiveError();
           if (sid>0 && vws.getActiveError()<0 ){
           	return NoError;
           } else {
           	return vws.getActiveError();
           }
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    /**
     * CV10.1 Enhancement:- Nodeid is passed to validate the parentid is valid. 
     * @param sid
     * @param index
     * @param docType
     * @param nodeid
     * @param ret
     * @return
     */
    public int getNextSequence(int sid, Index index, DocType docType,int nodeid, Vector ret)
    {
    	return getNextSequence(sid, index.getId(), docType.getId(),nodeid, ret);
    }
    public int getNextSequence(int sid, int indexId, int docTypeId,int nodeid, Vector ret)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNextSequence::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector output= vws.getNextSequence(session.room, sid, indexId, docTypeId,nodeid);
           ret.addAll(output);
           VWClient.printToConsole("Active Error in getNextSequence--->"+vws.getActiveError());
          // return vws.getActiveError();
           if (sid>0 && vws.getActiveError()<0 ){
           	return NoError;
           } else {
           	return vws.getActiveError();
           }
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
        
    
    /** Adds a Document as a reference to another Document
     * @param sid Session id obtained by login
     * @param doc Document to be referenced
     * @param rdoc Reference Document
     * @param bi
     * @return 0 for success or negative value error code
     */
    public int addRef(int sid, Document doc, Document rdoc, boolean bi)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in addRef::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || rdoc == null) return invalidParameter;
        if (doc.getId() == rdoc.getId()) return invalidReference;
        if (isReferenced(sid, doc, rdoc)) return invalidReference;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.addRef(session.room, sid, doc.getId(), rdoc.getId(), bi);
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    
    /** Deletes a reference to a document
     * @param sid sid Session id obtained by login
     * @param doc The document to which reference doc need to be deleted
     * @param rdoc The reference document
     * @return 0 for success or negative value error code
     */
    public int delRef(int sid, Document doc, Document rdoc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in delRef::"+sid);
        if(session==null) return invalidSessionId;   
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.delRef(session.room, sid, doc.getId(), rdoc.getId());
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    
    /** Returns a list of Documents that reference the specified Document.
     * @param sid Session id obtained by login
     * @param doc The document for which to return reference documents
     * @param refs The list of reference documents
     * @return 0 for success or negative value error code
     */
    public int getRefs(int sid, Document doc, Vector refs) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRefs::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           Vector ret = vws.getRefs(session.room, sid, doc.getId(), refs);
           refs.addAll(vectorToDocuments(ret, false));
           VWClient.printToConsole("Active Error in getRefs--->"+vws.getActiveError());
          // return vws.getActiveError(); 
           if (sid>0 && vws.getActiveError()<0 ){
           	return NoError;
           } else {
           	return vws.getActiveError();
           }
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    
    /** Adds a Shortcut for a Node
     * @param sid Session id obtained by login
     * @param node Shortucut for this Node
     * @param sc Shortcut object
     * @return 0 for success or negative value error code
     */
    public int addShortcut(int sid, Node node, ShortCut sc)
    {
        if (node == null || node.getId() <= 0) return invalidParameter;
        return addShortcut(sid, node.getId(), sc, ShortCut.NODE_SC);
    }
    
    /** Adds a Shortcut for a Document
     * @param sid  Session id obtained by login
     * @param doc Shortucut for this Document
     * @param sc Shortcut object
     * @return 0 for success or negative value error code
     */
    public int addShortcut(int sid, Document doc, ShortCut sc) 
    {
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        return addShortcut(sid, doc.getId(), sc, ShortCut.DOC_SC);
    }
    
    /** Returns a list of available Shortcuts
     * @param sid Session id obtained by login
     * @param scs The list of available shortcuts
     * @return 0 for success or negative value error code
     */
    public int getShortcuts(int sid, Vector scs) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getShortcuts::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getShortcuts(session.room, sid, scs);
            scs.addAll(vectorToScs(ret));
            VWClient.printToConsole("Active Error in getShortcuts--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
         	   return NoError;
            } else {
         	   return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Updates a Shortcut
     * @param sid Session id obtained by login
     * @param sc The short cut to be updated.
     * @return 0 for success or negative value error code
     */
    public int updateShortcut(int sid, ShortCut sc) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateShortcut::"+sid);
        if(session==null) return invalidSessionId;      
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.updateShortcut
                                  (session.room, sid, sc.getId(), sc.getName(),sc.getType());
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Deletes a short cut
     * @param sid sid Session id obtained by login
     * @param sc The short cut to be deleted 
     * @return  0 for success or negative value error code
     */
    public int delShortcut(int sid, ShortCut sc) 
    {
        if (sc == null || sc.getId() <= 0) return invalidParameter;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in delShortcut::"+sid);
        if(session==null) return invalidSessionId;  
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.delShortcut(session.room, sid, sc.getId());
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    public int getFolders(int sid, String folderName, Vector folderList){
		return getFolders(sid, folderName, folderList, 0);
    }
    public int getFolders(int sid, String folderName, Vector folderList, int mode){
	return getFolders(sid, folderName, folderList, 0, 0);
    }
    
    public int getFolders(int sid, String folderName, Vector folderList,
	    int mode, int searchForSubFolder) {
	Session session = getSession(sid);
	 VWClient.printToConsole("sessionId in getFolders::"+sid);
	if (session == null) return invalidSessionId;
	VWS vws = getServer(session.server);
	if (vws == null) return ServerNotFound;
	try {
	    folderName +=  Util.SepChar  + mode + Util.SepChar  + searchForSubFolder;
	    Vector result = vws.getFolders(session.room, sid, folderName);
	    if (searchForSubFolder == 0) {
		for (int i = 0; i < result.size(); i++) {
		    Node node = strToNode(result.get(i).toString());
		    folderList.add(node);
		}
	    } else {
		for (int i = 0; i < result.size(); i++) {
		    if (!result.get(i).toString().startsWith("0")){ 
			Node node = strToNode(result.get(i).toString());
			folderList.add(node);
		    }else{
			Document node = strToDocument(result.get(i).toString());
			folderList.add(node);			
		    }		    
		}
		}	    
	} catch (Exception e) {
	    return Error;
	}
	return NoError;
    }
    
    /*
     * CV2019 merges from SIDBI line
    */
    public int searchNodePath(int sid, String folderName,String folderPath, Vector folderList,
    	    int mode, int searchForSubFolder) {
    	Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in getFolders::"+sid);
    	if (session == null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try {
    		
    	    folderName +=  Util.SepChar  + mode + Util.SepChar  + searchForSubFolder;
    	    Vector result = vws.searchNodePath(session.room, sid, folderName,folderPath);
    	    if (searchForSubFolder == 0) {
    		for (int i = 0; i < result.size(); i++) {
    		    Node node = strToNode(result.get(i).toString());
    		    folderList.add(node);
    		}
    	    } else {
    		for (int i = 0; i < result.size(); i++) {
    		    if (!result.get(i).toString().startsWith("0")){ 
    			Node node = strToNode(result.get(i).toString());
    			folderList.add(node);
    		    }else{
    			Document node = strToDocument(result.get(i).toString());
    			folderList.add(node);			
    		    }		    
    		}
    		}	    
    	} catch (Exception e) {
    	    return Error;
    	}
    	return NoError;
    }
    /*------------------------End of SIDBI merges--------------------------------------*/
    
    public int searchByIndex(int sid, String docTypeName, String indexName, String indexValue, int conditionType, Vector documents){
    	return searchByIndex(sid, docTypeName, indexName, indexValue, conditionType, 0, documents);
    }
    public int searchByIndex(int sid, String docTypeName, String indexName, String indexValue, int conditionType, int includeCustom, Vector documents){
    	Session session = getSession(sid);
    	if (session == null) return invalidSessionId;
   	 	VWClient.printToConsole("sessionId in searchByIndex::"+sid);
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try{
    		Vector output = new Vector(); 
    		output = vws.searchByIndex(session.room, sid, docTypeName, indexName, indexValue, conditionType, includeCustom);
    		documents.addAll(vectorToDocumentWithIndexValues(output, 2));
    	}catch(Exception ex){
    		
    	}
    	return NoError;
    }
    
    public Node strToNode(String dbString){
	    StringTokenizer tokens = new StringTokenizer(dbString, "\t");
	    int type = Integer.parseInt(tokens.nextToken().trim());
	    int nodeId = Integer.parseInt(tokens.nextToken().trim());
	    String nodeName = tokens.nextToken().trim();
	    int parentId = Integer.parseInt(tokens.nextToken().trim());
	    String path = tokens.nextToken().trim();
	    Node node = new Node(nodeId, nodeName);
	    node.setParentId(parentId);
	    node.setPath(path);	
	    node.setType(type);
	return node;	
    }
    
    public Document strToDocument(String dbString){
	    StringTokenizer tokens = new StringTokenizer(dbString, "\t");
	    int type = Integer.parseInt(tokens.nextToken().trim());	    
	    int docId = Integer.parseInt(tokens.nextToken().trim());
	    String docName = tokens.nextToken().trim();	    
	    int parentId = Integer.parseInt(tokens.nextToken().trim());
	    String path = tokens.nextToken().trim();
	    int pageCount = Integer.parseInt(tokens.nextToken().trim());
	    int docTypeId = Integer.parseInt(tokens.nextToken().trim());
	    String docTypeName = tokens.nextToken().trim();
	    String creator = tokens.nextToken().trim();
	    String creationDate = tokens.nextToken().trim();
	    String modifyDate = tokens.nextToken().trim();
	    DocType docType = new DocType(docTypeId, docTypeName);
	    Document doc = new Document(docId);
	    doc.setParentId(parentId);
	    doc.setName(docName);
	    doc.setLocation(path);	
	    doc.setPageCount(pageCount);
	    doc.setDocType(docType);
	    doc.setCreator(creator);
	    doc.setCreationDate(creationDate);
	    doc.setModifyDate(modifyDate);	    
	return doc;	
}
    
    public int convertToPDF(String srcPath, String destPath, String libPath, Vector result){
		return convertToPDF(srcPath, destPath, libPath, "", 0, 0, 1, 1, result);
    }
    public int convertToPDF(String srcPath, String destPath, String libPath, String pwd, int overLay, int whiteOut, int color, int depth, Vector result){
	int ret = loadViewLibrary(libPath);
	    ret = loadOutputLibrary(libPath);
	if ( ret == NoError){
	    String output = ConvertDocToPDF(destPath, srcPath, pwd, overLay, whiteOut, color, depth);
	    result.add(output);
	}
	return ret;
    }
    
    /**  Creates a temporary or permanent Search for the session's user
     * @param sid Session id obtained by login
     * @param srch The Search to create
     * @return 0 for success or negative value error code
     */
    public int createSearch(int sid, Search srch) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in createSearch::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            int ret = vws.createSearch(session.room, sid, srch);
            srch.setId(ret);
            return ret;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getSearchPages(int sid, Document doc, String txt, Vector pages) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getSearchPages::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getSearchPages(session.room, sid, doc.getId(), 
                                                               txt, pages);
            pages.addAll(ret);
            VWClient.printToConsole("Active Error in getSearchPages--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Deletes saved search
     * @param sid sid Session id obtained by login
     * @param srch The search need to be deleted
     * @return  0 for success or negative value error code
     */
    public int delSearch(int sid, Search srch) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in delSearch::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.delSearch(session.room, sid, srch.getId());
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Returns a list of available Creators (users) on the network
     * @param sid Session id obtained by login
     * @param creators The list of creators returned
     * @return 0 for success or negative value error code
     */
    public int getCreators(int sid, Vector creators) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getCreators::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getCreators(session.room, sid, creators);
            creators.addAll(vectorToCreators(ret));
            VWClient.printToConsole("Active Error in getCreators--->"+vws.getActiveError());
          //  return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Returns a list of permanent (saved) Searches
     * @param sid Session id obtained by login
     * @param srchs The list of searches
     * @return 0 for success or negative value error code
     */
    public int getSearchs(int sid, Vector srchs) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getSearchs::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getSearchs(session.room, sid, srchs);
            srchs.addAll(vectorToSearchs(ret));
            VWClient.printToConsole("Active Error in getSearchs--->"+vws.getActiveError());
          //  return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Returns the details of a Search
     * @param sid Session id obtained by login
     * @param srch The search to return details for
     * @return 0 for success or negative value error code
     */
    public int getSearch(int sid, Search srch) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getSearch::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Search srchRet =vws.getSearch(session.room, sid,srch);
            srch.set(srchRet);
            Vector ret = vws.getSearchAuthors(session.room, sid, srch.getId(), 
                                                                srch.getCreators());
            srch.getCreators().addAll(vectorToCreators(ret));
            ret.removeAllElements();
            
            ret = vws.getSearchDocTypes(session.room, sid, srch.getId(), 
                                                                 srch.getDocTypes());
            srch.getDocTypes().addAll(vectorToDocTypes(ret));
            ret.removeAllElements();
            
            ret = vws.getSearchConds(session.room, sid, srch.getId(), srch.getConds());
            srch.getConds().addAll(vectorToSearchConds(ret));
            VWClient.printToConsole("Active Error in getSearch--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /**Finds (executes) a Search.
     * @param sid - Session Id
     * @param srch - Search object
     * @return The number of Documents matching the search criteria
     */
    public int find(int sid, Search srch)
    {
        return find(sid, srch, 0);
    }
    /**Finds (executes) a Search.
     * @param sid  Session id obtained by login
     * @param srch  The search to execute
     * @param docsOnly true - returns only documents, false - returns nodes also.
     * @return The number of Documents matching the search criteria
     */
    public int find(int sid, Search srch, boolean docsOnly)
    {
        return find(sid, srch, 0, docsOnly);
    }
    /** Finds (executes) a Search.
     * @param sid  Session id obtained by login
     * @param srch  The search to execute
     * @param hWnd window handle to return result
     * @return The number of Documents matching the search criteria
     */
    public int find(int sid, Search srch, long hWnd)
    {
    	return find(sid, srch, hWnd, true);
    }
    //onlyDocs flag added to get only document for AdminWise, QC indexer and GroupWise Find
    
    /** Finds (executes) a Search.
     * @param sid Session id obtained by login
     * @param srch The search to execute
     * @param hWnd window handle to return result
     * @param docsOnly true - returns only documents, false - returns nodes also.
     * @return The number of Documents matching the search criteria
     */
    public int find(int sid, Search srch, long hWnd, boolean docsOnly)
    {
        
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in find::"+sid);
        if(session==null) return invalidSessionId;  
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
	        if(hWnd != 0)
            {
                int customList = 0;
                new searchThread(1, vws, session.room, sid, srch, hWnd, customList, "", docsOnly).start();
                return NoError;
            }
            else{
                Vector indexInfo = new Vector();
             	String customIndexList = "";
             	//There is no custom field search from AdminWise and QC indexer
            	//find signature changed for enh 1157  - audit catch for search criteria
            	int findReturn = vws.find(session.room, sid, srch, docsOnly);
            	if(!docsOnly){
    	         	getCustomIndexIdsFromRegistry(sid,1, 0, indexInfo);
    	         	// if customIndexList values are empty should not call setCustomisedIndexList. Valli  21 May 2007
    	         	if(indexInfo!=null && indexInfo.size()>0)
    	         		customIndexList = removeNegativeIds(indexInfo.get(0).toString());
    	         	/*if(customIndexList!=null && !customIndexList.equals(""))
    	         		vws.setCustomisedIndexList(1, sid, session.room, srch.getNodeId(), srch.getRowNo() , customIndexList);*/
                }
            	return findReturn;
                //return  vws.find(session.room, sid, srch.getId(), srch.getHitListId(), srch.getRowNo(), docsOnly);
            }
            
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Returns a list of Documents that match the Search criteria
     * @param sid Session id obtained by login
     * @param srch The search for which to return documents
     * @param docs The list of documents matching the search criteria
     * @return 0 for success or negative value error code
     */
    public int getFindResult(int sid, Search srch, Vector docs)
    {
        return getFindResult(sid, srch, docs, 0, false, 0);
    }
    
   /**
    * @param sid Session id obtained by login
    * @param srch The search for which to return documents
    * @param docs The list of documents matching the search criteria
    * @param hWnd window handle
    * @return 0 for success or negative value error code
    */
    public int getFindResult(int sid, Search srch, Vector docs, long hWnd)
    {
    	return getFindResult(sid, srch, docs, hWnd, false, 0);
    }
    
    public int getFindResult(int sid, Search srch, Vector docs, long hWnd, boolean docsOnly){
    	return getFindResult(sid, srch, docs, hWnd, docsOnly, 0);    	
    }
    
    /**
    * @param sid Session id obtained by login
    * @param srch The search for which to return documents
    * @param docs The list of documents matching the search criteria
    * @param hWnd window handle
    * @param docsOnly true - result will have onlys docs, false - result will have nodes also
    * @return 0 for success or negative value error code
    */
    public int getFindResult(int sid, Search srch, Vector docs, long hWnd, boolean docsOnly, int loadModule)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getFindResult::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            if (hWnd > 0)
            {
            	int customList = 0;
                new searchThread(2,vws, session.room, sid, srch, hWnd, customList, "",docsOnly ).start(); 
                return NoError;
            }
            else
            {
                Vector ret = vws.getFindResult(session.room, sid, srch, docs);
                docs.addAll(vectorToSearchDocuments(ret, session.room, loadModule));
                VWClient.printToConsole("Active Error in getFindResult--->"+vws.getActiveError());
               // return vws.getActiveError();
                if (sid>0 && vws.getActiveError()<0 ){
                	return NoError;
                } else {
                	return vws.getActiveError();
                }
            }
                     
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getIndexValues(int sid, int dtid, int iid, Vector values)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getIndexValues::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret =  vws.getIndexValues(session.room, sid, 
                                                             dtid, iid, values); 
            values.addAll(ret);
            VWClient.printToConsole("Active Error in getIndexValues--->"+vws.getActiveError());
          //  return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
                     
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getRoomStatistics(int sid, Vector info)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRoomStatistics::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getRoomStatistics(session.room, sid, info);
            info.addAll(ret);
            VWClient.printToConsole("Active Error in getRoomStatistics--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    
    /** Checks wether FTS is enabled for the session's Room
     * @param sid Session id obtained by login
     * @return 1 (true), 0 (false)
     */
    public int isTextSearchEnabled(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isTextSearchEnabled::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.isTextSearchEnabled(session.room, sid);
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    public int adminWise(int sid, String action, String param, Vector result)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in adminWise::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = 
                        vws.adminWise(session.room, sid, action, param, result);
            result.addAll(ret);
            VWClient.printToConsole("Active Error in adminWise--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    protected void finalize() throws Throwable
    {
        shutdown();
        super.finalize();
    }
    //--------------------------------Security----------------------------------
    
    /** Checks wether the session's user is a ViewWise Server Administrator.
     * @param sid Session id obtained by login
     * @return 1 (true), 0 (false)
     */
    public int isAdmin(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isAdmin::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return (vws.isAdmin(session.room, sid))? 1:0;
            
        }
        catch(Exception e)
        {
            return Error;
        }  
    }
    public int isManager(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isManager::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return (vws.isManager(session.room, sid))? 1:0;
            
        }
        catch(Exception e)
        {
            return Error;
        }  
    }
    public int getPrincipals(int sid, Vector principals)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getPrincipals::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getPrincipals(session.room, sid, principals);
            principals.addAll(vectorToPrincipals(ret));
            VWClient.printToConsole("Active Error in getPrincipals--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /*
     * CV2019 merges from SIDBI line
    */
    public int getPrincipalsSecurity(int sid, int mode, Vector principals)
    {
        return getPrincipalsSecurity(sid,principals,"",mode);
    }
    
    public int getPrincipalsSecurity(int sid, Vector principals,String startsWith,int mode)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getPrincipals::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getPrincipalsSecurity(session.room, sid, principals,startsWith,mode);
            VWClient.printToConsole("Before clearing principals in getPrincipals--->"+principals);
            principals.clear();
            VWClient.printToConsole("After clearing principals in getPrincipals--->"+principals);
            principals.addAll(vectorToPrincipals(ret));
            VWClient.printToConsole("After adding new principals in getPrincipals--->"+principals);
            VWClient.printToConsole("Active Error in getPrincipals--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    public int getPrincipalsWithNoSign(int sid, Vector principals)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getPrincipals::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getPrincipalsWithNoSign(session.room, sid, principals);
            principals.addAll(vectorToPrincipals(ret));
            VWClient.printToConsole("Active Error in getPrincipals--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /*------------------------End of SIDBI merges--------------------------------------*/
    public int getPrincipalMembers(int sid, String pid, Vector princ)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getPrincipalMembers::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getPrincipalMembers(session.room, sid, pid, princ);
            princ.addAll(vectorToPrincipals(ret));
            VWClient.printToConsole("Active Error in getPrincipalMembers--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int removePrincipal(int sid, Principal principal)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in removePrincipal::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            vws.removePrincipal(session.room, sid, principal);
            VWClient.printToConsole("Active Error in removePrincipal--->"+vws.getActiveError());
          //  return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int updatePrincipal(int sid, Principal principal)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updatePrincipal::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            vws.updatePrincipal(session.room, sid, principal);
            VWClient.printToConsole("Active Error in updatePrincipal--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int addPrincipal(int sid, Principal principal)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in addPrincipal::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            vws.addPrincipal(session.room, sid, principal);
            VWClient.printToConsole("Active Error in addPrincipal--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /**
     * Method created for slected single user info sync from adminwise user&groups table.
     * Created by:-Madhavan.b
     * @param sid
     * @param principal
     * @return
     */
    public int syncPrincipals(int sid,Principal principal)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in syncPrincipals::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = (VWS) connectedVWSs.get(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            vws.syncPrincipals(session.room, sid, principal);
            VWClient.printToConsole("Active Error in syncPrincipals--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int syncPrincipals(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in syncPrincipals::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = (VWS) connectedVWSs.get(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            vws.syncPrincipals(session.room, sid);
            VWClient.printToConsole("Active Error in syncPrincipals--->"+vws.getActiveError());
            //return vws.getActiveError();
            return vws.getActiveError();
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getDirectoryPrincipals(int sid, Vector principals)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDirectoryPrincipals::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getDirectoryPrincipals(session.room, sid);
            principals.addAll(ret);
            VWClient.printToConsole("Active Error in getDirectoryPrincipals--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /**
     * CV2019 - Added for No data found issue fix in Administration add dialog search [User & Groups]
     * @param sid
     * @param searchForPName
     * @author vanitha.s
     * @return
     */
    public Vector<Principal> getDirectoryPrincipalsNew(int sid, String searchForPName) {
    	Vector principals = new Vector();
    	getDirectoryPrincipalsNew(sid, principals, searchForPName);
    	return principals;
    }
    
    public int getDirectoryPrincipalsNew(int sid, Vector principals,String searchForPName)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDirectoryPrincipals::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getDirectoryPrincipalsNew(session.room, sid, searchForPName );
            principals.addAll(ret);
            VWClient.printToConsole("Active Error in getDirectoryPrincipals--->"+vws.getActiveError());
           
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int setDocAcl(int sid, Acl acl) 
    {
         return setNodeAcl(sid, acl);
    }
    public int setNodeAcl(int sid, Acl acl)
    {         
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setNodeAcl::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.setNodeAcl(session.room, sid, acl);
        }
        catch(Exception e)
        {
            return Error;
        }    
    }
    /** Sets Security privileges on a Document (fires the Security dialog).
     * @param sid Session id obtained by login
     * @param doc The document for which to fire the security dialog
     * @return 0 for success or negative value error code
     */
    public int setSecurity(int sid, Document doc)
    {
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in docSetSecurity::"+sid);
        if(session==null) return invalidSessionId;
        return setSec(sid, new Node(doc.getId()));
    }
    
    /** Sets Security privileges on a Node (fires the Security dialog).
     * @param sid Session id obtained by login
     * @param node The Node for which to fire the security dialog
     * @return 0 for success or negative value error code
     */
    public int setSecurity(int sid, Node node)
    {
        if (node == null || node.getId() <= 0) return invalidParameter;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in nodeSetSecurity::"+sid);
        if(session==null) return invalidSessionId;
        return setSec(sid, node);
     }
  // For Security Enhancement, check the acl entry exist for the node
    public int getAclForNode( int sid, int nodeId){
    	int ret = 0;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getAclForNode::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {           	
        	if (nodeId <= 0) return Error;     	
            ret = vws.getAclForNode(session.room, sid, nodeId, session.user);            
        }
        catch(Exception e)
        {
            return Error;
        }    

    	return ret;
    }
    
    /** Returns the privileges for the session's user on a Node or Document
     * @param sid Session id obtained by login
     * @param acl The acl referencing the node or Document for which privileges are returned 
     * @return 0 for success or negative value error code
     */
    public int getAcl(int sid, Acl acl)
    {
        if (acl == null || acl.getNodeId() <= 0) return invalidParameter;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getAcl::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        Principal me = new Principal();
        VWClient.printToConsole("before calling myPrincipal....");
        getMyPrincipal(sid, me);
        VWClient.printToConsole("getMyPrincipal return value :"+me);
        AclEntry entry = acl.newEntry(me);
        try
        {
        	VWClient.printToConsole("before calling getAcl ...");
            Acl ret = vws.getAcl(session.room, sid, acl);
            VWClient.printToConsole("getAcl return value :"+ret);
            acl.set(ret);
        }
        catch(Exception e)
        {
            return Error;
        }    
        return NoError;
    }
    public int getAclFor(int sid, Acl acl, Principal princ)
    {
        if (acl == null || acl.getNodeId() <= 0) return invalidParameter;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getAclFor::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        AclEntry entry = acl.newEntry(princ);
        try
        {
            Acl ret = vws.getAclFor(session.room, sid, acl, princ.getName());
            acl.set(ret);
        }
        catch(Exception e)
        {
            return Error;
        }    
        return NoError;
    }
   //---------------------------------------------------------------------------
    public int getDocAcl(int sid, Acl acl, boolean effective)
    {
        return getNodeAcl(sid, acl, null, effective);
    }
    public int getDocAcl(int sid, Acl acl, Principal princ, boolean effective)
    {
        return getNodeAcl(sid, acl, princ, effective);
    }
    public int getNodeAcl(int sid, Acl acl, boolean effective)
    {
        return getNodeAcl(sid, acl, null, effective);                                 
    }
    public int getNodeAcl(int sid, Acl acl, Principal princ, boolean effective)                                 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNodeAcl::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Acl ret = vws.getNodeAcl(session.room, sid, acl, princ, effective);
            acl.set(ret);
        }
        catch(Exception e)
        {
            return Error;
        }    
        return NoError;
    }
    public int getConnectedClients(int sid, Vector clients)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getConnectedClients::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getConnectedClients(session.room, sid, clients);
            clients.addAll(ret);
            return NoError; 
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    public int setAllowConnection(int sid, boolean allow) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setAllowConnection::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.setAllowConnection(session.room, sid, allow);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int isConnectionEnabled(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isConnectionEnabled::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.isConnectionEnabled(session.room, sid);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int logoutClient(int sid, int clientsid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in logoutClient::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int ret = 0;
        try
        {
            ret = vws.logoutClient(session.room, sid, clientsid);
            sessions.remove(new Integer(clientsid));
            return ret; 
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    public int extract(String sourceFile, String destFolder)
    {
        if(sourceFile == null) return FileNotFoundErr;
        File docFile = new File(sourceFile);
        if(docFile == null || !docFile.exists()) return FileNotFoundErr;
        File extractedDir = null;
        if(destFolder==null)
        {
            extractedDir = docFile.getParentFile();
        }
        else
        {
            extractedDir = new File(destFolder);
            if(!extractedDir.exists()) extractedDir.mkdirs();
        }
        
        try
        {
            ZipUtil.extract(docFile, extractedDir);
        }
        catch(java.io.IOException e){return Error;}
        return NoError;
    }
    
    /** Logs out from a ViewWise Room releasing a license seat.
     * @param sid Session id obtained by login
     * @return 0 for success or negative value error code
     */
    public int logout(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in logout::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int ret = 0;
        try
        {
            ret = vws.logout(session.room, sid);
            sessions.remove(new Integer(sid));
            return ret; 
        }
        catch(Exception re)
        {
            return Error;
        }
    }
    
    /** Copies the Document file to the DSS Server. VWClient looks for the Document file in the cache folder specified in the VWClient constructor 
     * @param sid Session id obtained by login 
     * @param doc The document for which to copy the file
     * @return 0 for success or negative value error code
     */
    public int setDocFile(int sid, Document doc)
    {
    	VWClient.printToConsole("setDocFile 1.......");
        return this.setDocFile(sid, doc, null, true);
    }
    
    public int setDocFile(int sid, Document doc, boolean newDoc)
    {
    	VWClient.printToConsole("setDocFile 2.......");
    	return this.setDocFile(sid, doc, null, newDoc);
    }
    
    /**
     * @param sid Session id obtained by login
     * @param doc The document for which to copy the file
     * @param source The source folder path
     * @return 0 for success or negative value error code
     */
    public int setDocFile(int sid, Document doc, String source)
    {
    	VWClient.printToConsole("setDocFile 3.......");
        return this.setDocFile(sid, doc, source, true, true,true);
    }
    
    public int setDocFile(int sid, Document doc, String source, boolean newDoc)
    {
    	VWClient.printToConsole("setDocFile 4.......");
        return this.setDocFile(sid, doc, source, true, newDoc,true);
    }
    
    /**
     * @param sid Session id obtained by login
     * @param doc The document for which to copy the file
     * @param source The source folder path
     * @param withId false - do not assume id folder, true - assume id folder
     * @param newDoc true - newDoc , false - old Doc
     * @return 0 for success or negative value error code
     */
    public int setDocFile(int sid, Document doc, String source, boolean withId, 
            boolean newDoc)
    {
    	VWClient.printToConsole("setDocFile 5.......");
    		return setDocFile(sid, doc, source, withId, newDoc, null, true);
    }
    
    public int setDocFileWS(int sid, Document doc, String source, boolean withId, 
            boolean newDoc, String pageString)
    {
    		return setDocFileWS(sid, doc, source, withId, newDoc, null, true, pageString);
    }
    
    public int setDocFileAnnotation(int sid, Document doc, String source, boolean withId, 
            boolean newDoc, String pageString)
    {
    		return setDocFileAnnotation(sid, doc, source, withId, newDoc, null, true, pageString);
    }

    public int setDocFile(int sid, Document doc, String source, boolean withId, 
                                                                 boolean newDoc,boolean indexerFlag)
    {
    	VWClient.printToConsole("setDocFile 6.......");
        return setDocFile(sid, doc, source, withId, newDoc, null, indexerFlag);
    }
    public int setDocFile(int sid, Document doc, String source, boolean withId, 
            boolean newDoc, String pageInfo, boolean indexerFlag){
    	VWClient.printToConsole("setDocFile 7.......");
    	return setDocFile(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, false);
    }
    public int setDocFileWS(int sid, Document doc, String source, boolean withId, 
            boolean newDoc, String pageInfo, boolean indexerFlag, String pageString){
    	return setDocFileWS(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, false, pageString);
    }
    public int setDocFileAnnotation(int sid, Document doc, String source, boolean withId, 
            boolean newDoc, String pageInfo, boolean indexerFlag, String pageString){
    	return setDocFileAnnotation(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, false, pageString);
    }
    public int setDocFile(int sid, Document doc, String source, boolean withId, 
            boolean newDoc, String pageInfo, boolean indexerFlag, boolean retry){
    	VWClient.printToConsole("setDocFile 8.......");
    	return setDocFile(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, retry, false);
    }
    public int setDocFileWS(int sid, Document doc, String source, boolean withId, 
            boolean newDoc, String pageInfo, boolean indexerFlag, boolean retry, String pageString){
    	return setDocFileWS(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, retry, false, pageString);
    }
    public int setDocFileAnnotation(int sid, Document doc, String source, boolean withId, 
            boolean newDoc, String pageInfo, boolean indexerFlag, boolean retry, String pageString){
    	return setDocFileAnnotation(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, retry, false, pageString);
    }    
    /**
     * @param sid Session id obtained by login
     * @param doc The document for which to copy the file
     * @param source The source folder path
     * @param withId false - do not assume id folder, true - assume id folder
     * @param newDoc true - newDoc , false - old Doc
     * @param pageInfo page information
     * @param indexerFlag true - add document to indexer queue.
     * @return 0 for success or negative value error code
     * @author apurba.m
     */
    public int setDocFileWS(int sid, Document doc, String source, boolean withId, 
			boolean newDoc, String pageInfo, boolean indexerFlag, boolean retry, boolean isBackupDoc, String pageString){
    	System.out.println();
    	System.out.println();

    	if (doc == null || doc.getId() <= 0) return invalidParameter;
    	Session session = getSession(sid);
    	long registrySize=0;
    	long availableSpace=0;
    	VWClient.printToConsole("sessionId in setDocFile::"+sid);
    	System.out.println("sessionId in setDocFile::"+sid);
    	if(session==null) return invalidSessionId;
    	DSS ds = null;
    	Document maxDoc=null;
    	String azureUserName="";
    	String azurePassword="";
    	String dbStoragePath="";
    	String storageType="";
    	CloudStorageAccount storageAccount=null;
    	CloudFileClient fileClient =null;
    	CloudFile cloudFile=null;
    	CloudFileShare share=null;
    	CloudFileDirectory rootDir=null;
    	CloudFileDirectory sampleDir =null;
    	try{
    		Vector storageVect=new Vector();
    		VWClient.printToConsole("Before calling azure credentials in setDocFile::::::::::::"+Util.getNow(2));
    		System.out.println("Before calling azure credentials in setDocFile::::::::::::"+Util.getNow(2));
    		getAzureStorageCredentials(sid,doc.getId(),storageVect);
    		VWClient.printToConsole("After calling azure credentials setDocFiles::::::::::::"+Util.getNow(2));
    		System.out.println("After calling azure credentials setDocFiles::::::::::::"+Util.getNow(2));
    		if(storageVect!=null&&storageVect.size()>0){
    			StringTokenizer st = new StringTokenizer(storageVect.get(0).toString(), Util.SepChar); 
    			String storageid=st.nextToken();
    			String systemName=st.nextToken();
    			String ipaddress=st.nextToken();
    			String port=st.nextToken();
    			String hostIp=st.nextToken();
    			dbStoragePath=st.nextToken();
    			String Uncpath=st.nextToken();
    			String azureStroageType=st.nextToken();
    			storageType=azureStroageType;
    			VWClient.printToConsole("storageType from database::::::"+azureStroageType);
    			System.out.println("storageType from database::::::"+azureStroageType);

    			azureUserName=st.nextToken();
    			VWClient.printToConsole("storageType from azureUserName::::::"+azureUserName);
    			System.out.println("storageType from azureUserName::::::"+azureUserName);
    			azurePassword=st.nextToken();
    			VWClient.printToConsole("storageType from azurePassword::::::"+azurePassword);
    			System.out.println("storageType from azurePassword::::::"+azurePassword);
    		}
    	}catch(Exception e){
    		VWClient.printToConsole("Exception while getting the storage type in setdocFile:::::::"+e.getMessage());
    		System.out.println("Exception while getting the storage type in setdocFile:::::::"+e.getMessage());
    	}

    	if(storageType.equals("On Premises")){
    		Vector resultvect=new Vector();
    		getLatestDocId(sid,resultvect);
    		//CV10 :- Issue fix maxdocId is commented and we are passing the current doc object
    		//	int maxDocId=Integer.parseInt(resultvect.get(0).toString());
    		//	maxDoc=new Document(maxDocId);
    		//CV10 Issue fix modified from maxdocid from procedure to docID
    		Vector size=new Vector();
    		VWClient.printToConsole("Before calling checkavailable space in dss::::::::::::"+Util.getNow(2));
    		System.out.println("Before calling checkavailable space in dss::::::::::::"+Util.getNow(2));
    		checkAvailableSpaceInDSS(sid,doc,"",size);
    		VWClient.printToConsole("After calling checkavailable space in dss::::::::::::"+Util.getNow(2));
    		System.out.println("After calling checkavailable space in dss::::::::::::"+Util.getNow(2));
    		availableSpace= (long) size.get(0); 
    		availableSpace=availableSpace*1024;
    		registrySize=(long)size.get(1);
    		registrySize=registrySize*1024;
    	}
    	File file = null;
    	if (source == null)
    	{
    		VWClient.printToConsole("Inside source is null");
    		System.out.println("Inside source is null");
    		file = new File(session.cacheFolder + Util.pathSep + doc.getId() + 
    				Util.pathSep + DOC_CONTAINER);
    		VWClient.printToConsole("file path"+file.getAbsolutePath());
    		System.out.println("file path"+file.getAbsolutePath());


    	}
    	else
    	{
    		VWClient.printToConsole("Inside else with source withId:::"+withId);
    		System.out.println("Inside else with source withId:::"+withId);
    		if (!source.endsWith(Util.pathSep)) source += Util.pathSep;
    		if (withId)
    		{

    			file = new File(source + doc.getId() + Util.pathSep 
    					+ DOC_CONTAINER);
    			VWClient.printToConsole("inside withId if condition:::"+file.getAbsolutePath());
    			System.out.println("inside withId if condition:::"+file.getAbsolutePath());
    		}
    		else
    		{
    			file = new File(source + DOC_CONTAINER);
    			VWClient.printToConsole("inside else of ::::"+file.getAbsolutePath());
    			System.out.println("inside else of ::::"+file.getAbsolutePath());
    		}
    	}
    	if (!file.exists()){
    		VWClient.printToConsole("Inside document not found");
    		System.out.println("Inside document not found");
    		return documentNotFound;
    	}
    	/*long fileSize = file.length();
    	VWClient.printToConsole("All.zip file size in source location :"+fileSize);
    	VWClient.printToConsole("File size in KB :"+(fileSize/2014));
    	if ((fileSize / 1024) < 5) {
    		VWClient.printToConsole("Invalid file size......");
    		System.out.println("Invalid file size......");
    		return InvalidAllDotZip;
    	}
    	boolean isValid = Util.isValid(file.getPath());
    	if (!isValid) {
    		VWClient.printToConsole("all.zip corrupted in source location....");
    		System.out.println("all.zip corrupted in source location....");
    		return ClientZipCorrupted;
    	}*/
    	VWClient.printToConsole("pageInfo in set docFile:::"+pageInfo);
    	System.out.println("pageInfo in set docFile:::"+pageInfo);
    	if(pageInfo == null) {
    		VWClient.printToConsole("Before calling getAuditInfo ::::::::::::"+Util.getNow(2));
    		System.out.println("Before calling getAuditInfo ::::::::::::"+Util.getNow(2));
    		pageInfo = pageString;//getAuditInfo(file);   
    		VWClient.printToConsole("after calling getAuditInfo ::::::::::::"+Util.getNow(2));
    		System.out.println("after calling getAuditInfo ::::::::::::"+Util.getNow(2));
    		VWClient.printToConsole("Page info from database info file:::"+pageInfo);
    		System.out.println("Page info from database info file:::"+pageInfo);
    	}
    	ServerSchema dssSchema = new ServerSchema();
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try
    	{
    		VWClient.printToConsole("Before calling getDSSforDoc:::::"+"session.room::"+session.room+"sid:::"+"docId:::"+doc.getId()+"dssSchema:::"+dssSchema.getPath());
    		System.out.println("Before calling getDSSforDoc:::::"+"session.room::"+session.room+"sid:::"+"docId:::"+doc.getId()+"dssSchema:::"+dssSchema.getPath());
    		ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
    		dssSchema.set(ss);
    	}
    	catch(RemoteException re)
    	{
    		connectedVWSs.remove(session.server);
    		VWClient.printToConsole("Inside remote exception :::::"+re.getMessage());
    		System.out.println("Inside remote exception :::::"+re.getMessage());
    		return Error;
    	}
    	/* Purpose:  Direct DSS call changed. And it is through VWS
    	 *  Created by: C.Shanmugavalli			Date: 09 Oct 2006
    	 */        
    	//1 Direct DSS call changed - Valli
    	//DSS ds = (DSS) Util.getServer(dssSchema);
    	//if (ds == null) return dssInactive;
    	VWClient.printToConsole("calling set doc file next version:::");
    	System.out.println("calling set doc file next version:::");
    	Vector lv = new Vector();
    	VWClient.printToConsole("calling checkNextDocVersion:::::");
    	System.out.println("calling checkNextDocVersion:::::");
    	checkNextDocVersion(sid, doc, pageInfo, newDoc, lv);
    	if(lv.size() > 0) doc.setVersion( (String) lv.get(0));
    	VWClient.printToConsole("calling set doc file next version:::doc.getVersion()"+doc.getVersion());
    	System.out.println("calling set doc file next version:::doc.getVersion()"+doc.getVersion());
    	VWDoc vwdoc = doc.getVWDoc();
    	VWClient.printToConsole("setting to vwdoc source file:::"+file.getPath());
    	System.out.println("setting to vwdoc source file:::"+file.getPath());
    	VWClient.printToConsole("setting to vwdoc dss file path:::"+dssSchema.path);
    	System.out.println("setting to vwdoc dss file path:::"+dssSchema.path);
    	System.out.println("vwdoc :::: "+vwdoc);
    	vwdoc.setSrcFile(file.getPath());
    	vwdoc.setDstFile(dssSchema.path);
    	try{
    		if(storageType.equals("On Premises")){
    			File destFile=new File(dssSchema.path);
    			long destFileSize=getFileSize(destFile);
    			destFileSize=destFileSize/1024;
    			long sourceFileSize=vwdoc.getSize();
    			sourceFileSize=sourceFileSize/1024;
    			if(registrySize>(availableSpace-(sourceFileSize-destFileSize))){
    				SendEmailFromDSS(availableSpace/1024,sid,maxDoc);
    				return dssCriticalError;
    			}
    		}
    	}
    	catch(Exception dssexp){
    		VWClient.printToConsole("DSS Space Check inside setDoc :"+dssexp.getMessage());
    		System.out.println("DSS Space Check inside setDoc :"+dssexp.getMessage());
    	}

    	try
    	{
    		java.util.Properties props = System.getProperties();
    		String backupPath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + File.separator + session.room + File.separator + doc.getId();
    		File backupLocation = new File(backupPath);
    		boolean ret = false;
    		/*
    		 *  This condition will be checking the  DSS is used or not
    		 */
    		VWClient.printToConsole("isDSSUsed::::"+isDSSUsed());
    		if (isDSSUsed()) {
    			VWClient.printToConsole("Inside DSS");
    			ds = (DSS) Util.getServer(dssSchema);
    			/*if (ds == null) {
        			vws.sendNotification(session.room, sid, VWSConstants.DSS, resourceManager.getString("DSSInactive.Mail"));
        			return dssInactive;      
        		}*/
    			VWClient.printToConsole("Before calling setDocument inside if condition of dssused:::isBackupDoc"+isBackupDoc);
    			System.out.println("Before calling setDocument inside if condition of dssused:::isBackupDoc"+isBackupDoc);
    			VWClient.printToConsole("doc.getid() : " + doc.getId());
    			System.out.println("doc.getid() : " + doc.getId());
    			VWClient.printToConsole("isBackupDoc : " + isBackupDoc);
    			System.out.println("isBackupDoc : " + isBackupDoc);
    			vwdoc = ds.setDocument(doc, session.room, isBackupDoc);
    			VWClient.printToConsole("After calling setDocument inside if condition of dssused:::");   //last checked
    			System.out.println("After calling setDocument inside if condition of dssused:::");
    			if(storageType.equals("On Premises")){
    				System.out.println("Before WHILE !!!! ");
    				while (vwdoc.hasMoreChunks() && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE)
    				{
    					System.out.println("INSIDE WHILE !!!! ");
    					doc.setVWDoc(vwdoc);
    					VWClient.printToConsole("Inside while before calling setdocument");
    					System.out.println("Inside while before calling setdocument");
    					vwdoc = ds.setDocument(doc, session.room, isBackupDoc);	          
    					VWClient.printToConsole("Inside while after calling setdocument");
    					System.out.println("Inside while after calling setdocument");
    				}
    				System.out.println("AFTER WHILE !!!! ");
    			}
    		}else {
    			// Pass the schema to displayed the client address when upload the document
    			VWClient.printToConsole("Inside else before calling set documents");
    			System.out.println("Inside else before calling set documents");
    			VWClient.printToConsole("dssSchema.path : " + dssSchema.path);
    			System.out.println("dssSchema.path : " + dssSchema.path);
    			VWClient.printToConsole("doc.getid() : " + doc.getId());
    			System.out.println("doc.getid() : " + doc.getId());
    			VWClient.printToConsole("file : " + file);
    			System.out.println("file : " + file);
    			vwdoc = vws.setDocument(session.room, dssSchema, doc, file, mySchema);
    			VWClient.printToConsole("Inside else after calling set documents");
    			System.out.println("Inside else after calling set documents");
    			//vwdoc = ds.setDocument(doc, session.room);
    			//Checks whether the document has more chunks(Each chunk is of size 4MB)
    			if(storageType.equals("On Premises")){ 
    				while (vwdoc.hasMoreChunks() && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE)
    				{
    					VWClient.printToConsole("Inside else of while looop before calling set document");
    					System.out.println("Inside else of while looop before calling set document");
    					doc.setVWDoc(vwdoc);
    					vwdoc = vws.setDocument(session.room, dssSchema, doc, file, mySchema);
    				}
    			}
    		}
    		if(storageType.equals("On Premises")){
    			System.out.println("On Premises !!!! ");

    			if ((!vwdoc.isVaildZip() || !vwdoc.getIntegrity()) && !retry && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE){
    				addATRecord(vws, session.room, sid, doc.getId(),
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_DOC_FILEUPDATE_FAIL_DESC);
    				System.out.println("After addATRecord !!!! ");
    				vwdoc.setVaildZip(true);
    				return setDocFile(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, true);

    			}
    			if ((!vwdoc.isVaildZip() || !vwdoc.getIntegrity()) && (retry || vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE)){
    				//copied all.zip and moved to Application Data\ViewWise\Recover\Room\docId location.  
    				System.out.println("copied all.zip and moved to Application Data location");
    				try{
    					if (!backupLocation.exists()) backupLocation.mkdirs();
    					VWClient.printToConsole("backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER::::0"+backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER);
    					System.out.println("backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER::::0"+backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER);
    					boolean result = VWCUtil.copyFile(file, new File(backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER));
    					// To create a document.properties file in the backup location
    					VWClient.printToConsole("before calling createDocProperties:::"+backupPath);
    					System.out.println("before calling createDocProperties:::"+backupPath);
    					createDocProperties(sid, doc, backupPath);
    					VWClient.printToConsole("after calling createDocProperties:::"+backupPath);
    					System.out.println("after calling createDocProperties:::"+backupPath);
    				}catch (Exception e) {
    					VWClient.printToConsole("Inside corrupted 1:::::"+e.getMessage());
    					System.out.println("Inside corrupted 1:::::"+e.getMessage());
    					return DSSZipCorrupted;
    				}
    				if (vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE){
    					System.out.println("DISKSPACE_NOT_AVAILABLE !!!!");
    					return DSSDiskSpaceNotAvailable;
    				}else{
    					VWClient.printToConsole("Inside corrupted 2:::::");
    					System.out.println("Inside corrupted 2:::::");
    					return DSSZipCorrupted;
    				}

    			}
    			if (vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE){
    				System.out.println("DISKSPACE_NOT_AVAILABLE 2 !!!!");
    				return DSSDiskSpaceNotAvailable;
    			}
    		}//CV10 Enhancement added for Azure storage file upload
    		else if(storageType.equals("Azure Storage")){
    			System.out.println("Azure Storage !!!! ");
    			if (!backupLocation.exists()) 
    				backupLocation.mkdirs();

    			boolean result = VWCUtil.copyFile(file, new File(backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER));
    			VWClient.printToConsole("result in Azure Storage::::"+result);
    			System.out.println("result in Azure Storage::::"+result);

    			try {
    				String accountName="AccountName="+azureUserName+";";
    				String password="AccountKey="+azurePassword;
    				String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
    				System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
    				System.out.println("Azure Storage try !!!! ");

    				storageAccount = CloudStorageAccount.parse(storageConnectionString1);
    				VWClient.printToConsole("storageAccount connection established :::"+storageAccount.getFileStorageUri());
    				System.out.println("storageAccount connection established :::"+storageAccount.getFileStorageUri());
    				fileClient = storageAccount.createCloudFileClient();
    				VWClient.printToConsole("File client storage Uri ::"+fileClient.getStorageUri());
    				System.out.println("File client storage Uri ::"+fileClient.getStorageUri());
    				String pattern = Pattern.quote(System.getProperty("file.separator"));
    				String dbpath[]=dbStoragePath.split(pattern);

    				for(int i=0;i<dbpath.length;i++){
    					if(i==2)
    					{
    						share = fileClient.getShareReference(dbpath[i]);
    						VWClient.printToConsole("Inside loop count 2 share :"+share.getUri());
    						System.out.println("Inside loop count 2 share :"+share.getUri());
    						if(!share.exists())
    							share.create();
    					}
    					else if(i==3)
    					{
    						sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
    						VWClient.printToConsole("Inside loop count 3 share :"+share.getUri());
    						System.out.println("Inside loop count 3 share :"+share.getUri());
    						if(!sampleDir.exists()){
    							sampleDir.create();

    						} 
    						rootDir = sampleDir;
    						sampleDir = null;
    					}
    					else if(i>=4)
    					{
    						VWClient.printToConsole("Inside loop count 4 share :"+share.getUri());
    						System.out.println("Inside loop count 4 share :"+share.getUri());
    						if ((i+1) != dbpath.length) {
    							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
    							if(!sampleDir.exists()){
    								sampleDir.create();
    							}
    							rootDir = sampleDir;
    							sampleDir = null;
    						}
    						else if ((i+1) == dbpath.length) {
    							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
    							if(!sampleDir.exists()){
    								sampleDir.create();
    							}
    							rootDir = sampleDir;

    							sampleDir = null;
    						}
    					}

    				}
    				sampleDir =	rootDir.getDirectoryReference(String.valueOf(doc.getId()));
    				VWClient.printToConsole("sampleDir.geturi:  " + sampleDir.getUri());
    				System.out.println("sampleDir.geturi:  " + sampleDir.getUri());
    				if(!sampleDir.exists()){
    					sampleDir.create();

    				}
    				cloudFile = sampleDir.getFileReference("all.zip");

    				if (sampleDir.exists()) {
    					/*
    					 * CV10 Azure set active version issue fix.Upload document after setactive version or properties update
    					 * Zip creation was not hapenning
    					 */
    					if(cloudFile.exists()){
    						String newVersion = "";
    						Vector newVersVector = new Vector();
    						DocType dt = doc.getDocType();
    						VWClient.printToConsole("Azure Before getDTVersionSettings call");
    						System.out.println("Azure Before getDTVersionSettings call");
    						getDTVersionSettings(sid, dt);
    						if (dt!=null && dt.getVREnable().equals("1")){
    							VWClient.printToConsole("Azure Before checkNextDocVersion call");
    							System.out.println("Azure Before checkNextDocVersion call");
    							checkNextDocVersion(sid, doc, pageInfo, newDoc, newVersVector);
    							if(newVersVector!=null && newVersVector.size() > 0) {
    								newVersion =  (String) newVersVector.get(0);

    							}
    							if (newVersion != null && !newVersion.equals(""))
    							{
    								CloudFile currentvercloudfile = sampleDir.getFileReference(doc.getVersion());
    								if(!currentvercloudfile.exists()){
    									currentvercloudfile.startCopy(cloudFile);
    								}
    							}
    						}/*else{
								VWClient.printToConsole("Inside else before delete docversion :::::::::::::"+doc.getVersion());
								if(!doc.getVersion().equals("")){
									CloudFile neVercloudfile = sampleDir.getFileReference(doc.getVersion());
									VWClient.printToConsole("neVercloudfile geturi::::::::::::"+neVercloudfile.getUri());
									if(!neVercloudfile.exists()){
										neVercloudfile.startCopy(cloudFile);
									}
								}
							}*/
    						cloudFile.delete();

    					}
    					VWClient.printToConsole("RoomName getEncryption in setdocfile::::::::::"+session.room);
    					System.out.println("RoomName getEncryption in setdocfile::::::::::"+session.room);
    					String key=vws.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
    					DSSCipher cipher = new DSSCipher(key);
    					if ((cipher != null)&&(!key.equals("")))  
    					{
    						String fileSrcPath=file.getPath();
    						cipher.encryptFile(fileSrcPath);
    						cloudFile.uploadFromFile(fileSrcPath);
    						VWClient.printToConsole("Azure file upload with encryption");
    						System.out.println("Azure file upload with encryption");
    					}else{
    						VWClient.printToConsole("Azure file upload without encryption");
    						System.out.println("Azure file upload without encryption");
    						cloudFile.uploadFromFile(file.getPath());
    					}
    					if(cloudFile.exists())
    						VWClient.printToConsole("Document with "+" "+ doc.getId()+" "+"Uploaded SucessFully to Azure Storage");
    					System.out.println("Document with "+" "+ doc.getId()+" "+"Uploaded SucessFully to Azure Storage");
    				} 

    				if(indexerFlag){
    					addIndexerDoc(vws, session.room, sid, doc.getId(),3);
    				}

    			}catch (Exception e) {
    				VWClient.printToConsole("Failed to save document to cloud :::"+e.getMessage());
    				System.out.println("Failed to save document to cloud :::"+e.getMessage());
    				String errorMessage=e.getMessage().toString();
    				VWClient.printToConsole("errorMessage:::::"+errorMessage);
    				System.out.println("errorMessage:::::"+errorMessage);
    				if(errorMessage.equals("Storage Key is not a valid base64 encoded string.")){
    					VWClient.printToConsole("Inside if condition of exception");
    					System.out.println("Inside if condition of exception");
    					return ViewWiseErrors.invalidCredentials;
    				}else{
    					VWClient.printToConsole("Inside else condition of exception");
    					System.out.println("Inside else condition of exception");
    					return ViewWiseErrors.uploadFailed;
    				}

    			}




    			// To create a document.properties file in the backup location
    			createDocProperties(sid, doc, backupPath);

    			/*
					Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
					Created by: <Pandiya Raj.M>
					Date: <6 Jun 2006>
    			 */
    			/*
    			 * Issue No / Purpose:  Indexer issue # 633 this method not sending indexOption value '3'>
    			 * i.e the new status '3' is not going as param to DB Whenever add/modify a page in a document in DTC,
    			 * Created by:	C.Shanmugavalli	Date: 2 July 2006
    			 */
    			// Check that document modified, if modified it will delete the page reference in indexer table and store the '3' for 'qc' column for that document            	                
    			int indexOption = 3;
    			boolean flag = false;
    			if (pageInfo != null && pageInfo.length() > 0){
    				ArrayList listPage = new ArrayList();
    				StringTokenizer sToken = new StringTokenizer(pageInfo,",");
    				String pageNo,pageID,operation = null;
    				while(sToken.hasMoreTokens()){listPage.add(sToken.nextToken());}
    				int count = 1;
    				for(int index=0;index<(listPage.size()/3);index++)
    				{
    					try {
    						pageNo = String.valueOf(listPage.get(count++));
    						pageID = String.valueOf(listPage.get(count++));
    						operation = String.valueOf(listPage.get(count++));                			
    						if ("Y".equals(operation.toUpperCase()) || "D".equals(operation.toUpperCase())){ // Y means Modified.
    							int pageid = 0;
    							if (pageID!= null) {pageid =  Integer.parseInt(pageID);}

    							VWClient.printToConsole("Inside azure Before callind DBRemovePageText::::pageid"+pageid);
    							System.out.println("Inside azure Before callind DBRemovePageText::::pageid"+pageid);
    							vws.DBRemovePageText(session.room,doc.getId(),pageid);
    							flag =true;
    						}
    						//W means page added. D means deleted page
    						// N means Not deleted page. Y means page modified
    						if(("W".equals(operation.toUpperCase()))){ 
    							//if(!("W".equals(operation.toUpperCase()))){ // commented for Issue # 633 
    							flag =true;
    						}
    					}catch(Exception exception){}
    				}
    			}

    			if(indexerFlag){
    				addIndexerDoc(vws, session.room, sid, doc.getId(),3);
    			}
    			if(isBackupDoc){
    				addATRecord(vws, session.room, sid, doc.getId(), 
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_REC_DOC_FILEUPDATE_DESC);
    				if(backupLocation.exists()){
    					VWClient.printToConsole("before delete backuplocaion::::"+backupLocation);
    					System.out.println("before delete backuplocaion::::"+backupLocation);
    					ret = deleteBackupLocation(backupLocation);		
    				}
    			}else{
    				addATRecord(vws, session.room, sid, doc.getId(), 
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_DOC_FILEUPDATE_DESC);
    			}
    			//After uploaded the all.zip; we need to make a database call to get the current version 
    			//Create the method �updateVersionRevision()� which will create a copy of all.zip with current version 
    			int result1 =  	vws.setDocAuditInfo(session.room, sid, doc, (indexerFlag?newDoc:false), pageInfo);
    			String updatedVersion = "";
    			Vector output = new Vector();
    			DocType dt = doc.getDocType();
    			VWClient.printToConsole("Azure Before getDTVersionSettings call");
    			System.out.println("Azure Before getDTVersionSettings call");
    			getDTVersionSettings(sid, dt);
    			if (dt!=null && dt.getVREnable().equals("1")){
    				VWClient.printToConsole("Azure Before checkNextDocVersion call");
    				System.out.println("Azure Before checkNextDocVersion call");
    				checkNextDocVersion(sid, doc, pageInfo, newDoc, output);
    				if(output!=null && output.size() > 0) {
    					updatedVersion =  (String) output.get(0);
    					if (updatedVersion != null && updatedVersion.trim().length() >0){
    						if (isDSSUsed()) {

    							VWClient.printToConsole("Azure In if dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
    							System.out.println("Azure In if dssused updateversion revision:::::updatedVersion::::"+updatedVersion);

    							if (updatedVersion != null && !updatedVersion.equals(""))
    							{   //CV10 Enhancement update document to azure with version revision commented.

    								CloudFile cloudFile1 = sampleDir.getFileReference(updatedVersion);
    								if(cloudFile.exists()){
    									try {
    										VWClient.printToConsole("Before start copy :::::");
    										System.out.println("Before start copy :::::");
    										cloudFile1.startCopy(cloudFile.getUri());
    										VWClient.printToConsole("After start copy :::::");
    										System.out.println("After start copy :::::");
    										cloudFile1.abortCopy(cloudFile1.getProperties().getCopyState().getCopyId());
    									}
    									catch (StorageException e) {
    										VWClient.printToConsole("Inside storage exception e::::"+e.getMessage());
    										System.out.println("Inside storage exception e::::"+e.getMessage());
    										if (!e.getErrorCode().contains("NoPendingCopyOperation")) {
    											return  ViewWiseErrors.renameAzureDoc;
    										}
    									}

    								}
    							}	

    						}
    						else{
    							VWClient.printToConsole("Azure In else of dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
    							System.out.println("Azure In else of dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
    							vws.updateVersionRevision(session.room, dssSchema, doc,  updatedVersion);   
    						}
    					}
    				}
    			}
    			return result1;
    		}//End of azure file upload

    		if (vwdoc.getIntegrity())
    		{            	
    			/*
				Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
				Created by: <Pandiya Raj.M>
				Date: <6 Jun 2006>
    			 */
    			/*
    			 * Issue No / Purpose:  Indexer issue # 633 this method not sending indexOption value '3'>
    			 * i.e the new status '3' is not going as param to DB Whenever add/modify a page in a document in DTC,
    			 * Created by:	C.Shanmugavalli	Date: 2 July 2006
    			 */
    			// Check that document modified, if modified it will delete the page reference in indexer table and store the '3' for 'qc' column for that document            	                
    			int indexOption = 3;
    			boolean flag = false;
    			if (pageInfo != null && pageInfo.length() > 0){
    				ArrayList listPage = new ArrayList();
    				StringTokenizer sToken = new StringTokenizer(pageInfo,",");
    				String pageNo,pageID,operation = null;
    				while(sToken.hasMoreTokens()){listPage.add(sToken.nextToken());}
    				int count = 1;
    				for(int index=0;index<(listPage.size()/3);index++)
    				{
    					try {
    						pageNo = String.valueOf(listPage.get(count++));
    						pageID = String.valueOf(listPage.get(count++));
    						operation = String.valueOf(listPage.get(count++));                			
    						if ("Y".equals(operation.toUpperCase()) || "D".equals(operation.toUpperCase())){ // Y means Modified.
    							int pageid = 0;
    							if (pageID!= null) {pageid =  Integer.parseInt(pageID);}
    							VWClient.printToConsole("azure before dbremovepagetText:::"+pageid);
    							System.out.println("azure before dbremovepagetText:::"+pageid);
    							vws.DBRemovePageText(session.room,doc.getId(),pageid);
    							flag =true;
    						}
    						//W means page added. D means deleted page
    						// N means Not deleted page. Y means page modified
    						if(("W".equals(operation.toUpperCase()))){ 
    							//if(!("W".equals(operation.toUpperCase()))){ // commented for Issue # 633 
    							flag =true;
    						}
    					}catch(Exception exception){}
    				}
    			}
    			/*                if(flag){
                	indexOption = 3;
                } */  

    			if(indexerFlag){
    				addIndexerDoc(vws, session.room, sid, doc.getId(),3);
    			}
    			if(isBackupDoc){
    				addATRecord(vws, session.room, sid, doc.getId(), 
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_REC_DOC_FILEUPDATE_DESC);
    				if(backupLocation.exists())
    					ret = deleteBackupLocation(backupLocation);					
    			}else{
    				addATRecord(vws, session.room, sid, doc.getId(), 
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_DOC_FILEUPDATE_DESC);
    			}
    			//After uploaded the all.zip; we need to make a database call to get the current version 
    			//Create the method �updateVersionRevision()� which will create a copy of all.zip with current version 

    			int result =  	vws.setDocAuditInfo(session.room, sid, doc, (indexerFlag?newDoc:false), pageInfo);
    			String updatedVersion = "";
    			Vector output = new Vector();
    			DocType dt = doc.getDocType();
    			getDTVersionSettings(sid, dt);
    			if (dt!=null && dt.getVREnable().equals("1")){
    				VWClient.printToConsole("Inside azure before calling checknextdocversion:::::"+pageInfo+"newDoc::::"+newDoc+"output:::"+output);
    				System.out.println("Inside azure before calling checknextdocversion:::::"+pageInfo+"newDoc::::"+newDoc+"output:::"+output);
    				checkNextDocVersion(sid, doc, pageInfo, newDoc, output);
    				if(output!=null && output.size() > 0) {
    					updatedVersion =  (String) output.get(0);
    					if (updatedVersion != null && updatedVersion.trim().length() >0){
    						if (isDSSUsed()) {
    							VWClient.printToConsole("Inside azure if isDSSUsed updateVersionRevision:::::"+updatedVersion);
    							System.out.println("Inside azure if isDSSUsed updateVersionRevision:::::"+updatedVersion);

    							ds.updateVersionRevision(doc, updatedVersion);
    						}
    						else{
    							VWClient.printToConsole("Inside azure else isDSSUsed updateVersionRevision:::::"+updatedVersion);
    							System.out.println("Inside azure else isDSSUsed updateVersionRevision:::::"+updatedVersion);

    							vws.updateVersionRevision(session.room, dssSchema, doc,  updatedVersion);     
    						}
    					}
    				}
    			}
    			return result;
    		}
    		else
    			return documentNotSet;
    	}
    	catch(Exception e)
    	{
    		VWClient.printToConsole("Error while set docFile::::"+e.getMessage());
    		System.out.println("Error while set docFile::::"+e.getMessage());
    		return Error;

    	}

    }
    
    /**
     * @param sid Session id obtained by login
     * @param doc The document for which to copy the file
     * @param source The source folder path
     * @param withId false - do not assume id folder, true - assume id folder
     * @param newDoc true - newDoc , false - old Doc
     * @param pageInfo page information
     * @param indexerFlag true - add document to indexer queue.
     * @return 0 for success or negative value error code
     * @author apurba.m
     */
    public int setDocFileAnnotation(int sid, Document doc, String source, boolean withId, 
    		boolean newDoc, String pageInfo, boolean indexerFlag, boolean retry, boolean isBackupDoc, String pageString){
    	System.out.println();
    	System.out.println();

    	if (doc == null || doc.getId() <= 0) return invalidParameter;
    	Session session = getSession(sid);
    	long registrySize=0;
    	long availableSpace=0;
    	VWClient.printToConsole("sessionId in setDocFile::"+sid);
    	System.out.println("sessionId in setDocFile::"+sid);
    	if(session==null) return invalidSessionId;
    	DSS ds = null;
    	Document maxDoc=null;
    	String azureUserName="";
    	String azurePassword="";
    	String dbStoragePath="";
    	String storageType="";
    	CloudStorageAccount storageAccount=null;
    	CloudFileClient fileClient =null;
    	CloudFile cloudFile=null;
    	CloudFileShare share=null;
    	CloudFileDirectory rootDir=null;
    	CloudFileDirectory sampleDir =null;
    	try{
    		Vector storageVect=new Vector();
    		VWClient.printToConsole("Before calling azure credentials in setDocFile::::::::::::"+Util.getNow(2));
    		System.out.println("Before calling azure credentials in setDocFile::::::::::::"+Util.getNow(2));
    		getAzureStorageCredentials(sid,doc.getId(),storageVect);
    		VWClient.printToConsole("After calling azure credentials setDocFiles::::::::::::"+Util.getNow(2));
    		System.out.println("After calling azure credentials setDocFiles::::::::::::"+Util.getNow(2));
    		if(storageVect!=null&&storageVect.size()>0){
    			StringTokenizer st = new StringTokenizer(storageVect.get(0).toString(), Util.SepChar); 
    			String storageid=st.nextToken();
    			String systemName=st.nextToken();
    			String ipaddress=st.nextToken();
    			String port=st.nextToken();
    			String hostIp=st.nextToken();
    			dbStoragePath=st.nextToken();
    			String Uncpath=st.nextToken();
    			String azureStroageType=st.nextToken();
    			storageType=azureStroageType;
    			VWClient.printToConsole("storageType from database::::::"+azureStroageType);
    			System.out.println("storageType from database::::::"+azureStroageType);

    			azureUserName=st.nextToken();
    			VWClient.printToConsole("storageType from azureUserName::::::"+azureUserName);
    			System.out.println("storageType from azureUserName::::::"+azureUserName);
    			azurePassword=st.nextToken();
    			VWClient.printToConsole("storageType from azurePassword::::::"+azurePassword);
    			System.out.println("storageType from azurePassword::::::"+azurePassword);
    		}
    	}catch(Exception e){
    		VWClient.printToConsole("Exception while getting the storage type in setdocFile:::::::"+e.getMessage());
    		System.out.println("Exception while getting the storage type in setdocFile:::::::"+e.getMessage());
    	}

    	if(storageType.equals("On Premises")){
    		Vector resultvect=new Vector();
    		getLatestDocId(sid,resultvect);
    		//CV10 :- Issue fix maxdocId is commented and we are passing the current doc object
    		//	int maxDocId=Integer.parseInt(resultvect.get(0).toString());
    		//	maxDoc=new Document(maxDocId);
    		//CV10 Issue fix modified from maxdocid from procedure to docID
    		Vector size=new Vector();
    		VWClient.printToConsole("Before calling checkavailable space in dss::::::::::::"+Util.getNow(2));
    		System.out.println("Before calling checkavailable space in dss::::::::::::"+Util.getNow(2));
    		checkAvailableSpaceInDSS(sid,doc,"",size);
    		VWClient.printToConsole("After calling checkavailable space in dss::::::::::::"+Util.getNow(2));
    		System.out.println("After calling checkavailable space in dss::::::::::::"+Util.getNow(2));
    		availableSpace= (long) size.get(0); 
    		availableSpace=availableSpace*1024;
    		registrySize=(long)size.get(1);
    		registrySize=registrySize*1024;
    	}
    	File file = null;
    	if (source == null)
    	{
    		VWClient.printToConsole("Inside source is null");
    		System.out.println("Inside source is null");
    		file = new File(session.cacheFolder + Util.pathSep + doc.getId() + 
    				Util.pathSep + DOC_CONTAINER);
    		VWClient.printToConsole("file path"+file.getAbsolutePath());
    		System.out.println("file path"+file.getAbsolutePath());


    	}
    	else
    	{
    		VWClient.printToConsole("Inside else with source withId:::"+withId);
    		System.out.println("Inside else with source withId:::"+withId);
    		if (!source.endsWith(Util.pathSep)) source += Util.pathSep;
    		if (withId)
    		{

    			file = new File(source + doc.getId() + Util.pathSep 
    					+ DOC_CONTAINER);
    			VWClient.printToConsole("inside withId if condition:::"+file.getAbsolutePath());
    			System.out.println("inside withId if condition:::"+file.getAbsolutePath());
    		}
    		else
    		{
    			file = new File(source + DOC_CONTAINER);
    			VWClient.printToConsole("inside else of ::::"+file.getAbsolutePath());
    			System.out.println("inside else of ::::"+file.getAbsolutePath());
    		}
    	}
    	if (!file.exists()){
    		VWClient.printToConsole("Inside document not found");
    		System.out.println("Inside document not found");
    		return documentNotFound;
    	}
    	VWClient.printToConsole("pageInfo in set docFile:::"+pageInfo);
    	System.out.println("pageInfo in set docFile:::"+pageInfo);
    	if(pageInfo == null) {
    		VWClient.printToConsole("Before calling getAuditInfo ::::::::::::"+Util.getNow(2));
    		System.out.println("Before calling getAuditInfo ::::::::::::"+Util.getNow(2));
    		pageInfo = pageString;   
    		VWClient.printToConsole("after calling getAuditInfo ::::::::::::"+Util.getNow(2));
    		System.out.println("after calling getAuditInfo ::::::::::::"+Util.getNow(2));
    		VWClient.printToConsole("Page info from database info file:::"+pageInfo);
    		System.out.println("Page info from database info file:::"+pageInfo);
    	}
    	ServerSchema dssSchema = new ServerSchema();
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try
    	{
    		VWClient.printToConsole("Before calling getDSSforDoc:::::"+"session.room::"+session.room+"sid:::"+"docId:::"+doc.getId()+"dssSchema:::"+dssSchema.getPath());
    		System.out.println("Before calling getDSSforDoc:::::"+"session.room::"+session.room+"sid:::"+"docId:::"+doc.getId()+"dssSchema:::"+dssSchema.getPath());
    		ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
    		dssSchema.set(ss);
    	}
    	catch(RemoteException re)
    	{
    		connectedVWSs.remove(session.server);
    		VWClient.printToConsole("Inside remote exception :::::"+re.getMessage());
    		System.out.println("Inside remote exception :::::"+re.getMessage());
    		return Error;
    	}
    	/* Purpose:  Direct DSS call changed. And it is through VWS
    	 *  Created by: C.Shanmugavalli			Date: 09 Oct 2006
    	 */        
    	//1 Direct DSS call changed - Valli
    	//DSS ds = (DSS) Util.getServer(dssSchema);
    	//if (ds == null) return dssInactive;
    	VWClient.printToConsole("calling set doc file next version:::");
    	System.out.println("calling set doc file next version:::");
    	Vector lv = new Vector();
    	VWClient.printToConsole("calling checkNextDocVersion:::::");
    	System.out.println("calling checkNextDocVersion:::::");
    	checkNextDocVersion(sid, doc, pageInfo, newDoc, lv);
    	if(lv.size() > 0) doc.setVersion( (String) lv.get(0));
    	VWClient.printToConsole("calling set doc file next version:::doc.getVersion()"+doc.getVersion());
    	System.out.println("calling set doc file next version:::doc.getVersion()"+doc.getVersion());
    	VWDoc vwdoc = doc.getVWDoc();
    	VWClient.printToConsole("setting to vwdoc source file:::"+file.getPath());
    	System.out.println("setting to vwdoc source file:::"+file.getPath());
    	VWClient.printToConsole("setting to vwdoc dss file path:::"+dssSchema.path);
    	System.out.println("setting to vwdoc dss file path:::"+dssSchema.path);
    	System.out.println("vwdoc :::: "+vwdoc);
    	vwdoc.setSrcFile(file.getPath());
    	vwdoc.setDstFile(dssSchema.path);
    	try{
    		if(storageType.equals("On Premises")){
    			File destFile=new File(dssSchema.path);
    			long destFileSize=getFileSize(destFile);
    			destFileSize=destFileSize/1024;
    			long sourceFileSize=vwdoc.getSize();
    			sourceFileSize=sourceFileSize/1024;
    			if(registrySize>(availableSpace-(sourceFileSize-destFileSize))){
    				SendEmailFromDSS(availableSpace/1024,sid,maxDoc);
    				return dssCriticalError;
    			}
    		}
    	}
    	catch(Exception dssexp){
    		VWClient.printToConsole("DSS Space Check inside setDoc :"+dssexp.getMessage());
    		System.out.println("DSS Space Check inside setDoc :"+dssexp.getMessage());
    	}

    	try
    	{
    		java.util.Properties props = System.getProperties();
    		String backupPath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + File.separator + session.room + File.separator + doc.getId();
    		File backupLocation = new File(backupPath);
    		boolean ret = false;
    		/*
    		 *  This condition will be checking the  DSS is used or not
    		 */
    		VWClient.printToConsole("isDSSUsed::::"+isDSSUsed());
    		if (isDSSUsed()) {
    			VWClient.printToConsole("Inside DSS");
    			ds = (DSS) Util.getServer(dssSchema);
    			/*if (ds == null) {
        			vws.sendNotification(session.room, sid, VWSConstants.DSS, resourceManager.getString("DSSInactive.Mail"));
        			return dssInactive;      
        		}*/
    			VWClient.printToConsole("Before calling setDocument inside if condition of dssused:::isBackupDoc"+isBackupDoc);
    			System.out.println("Before calling setDocument inside if condition of dssused:::isBackupDoc"+isBackupDoc);
    			VWClient.printToConsole("doc.getid() : " + doc.getId());
    			System.out.println("doc.getid() : " + doc.getId());
    			VWClient.printToConsole("isBackupDoc : " + isBackupDoc);
    			System.out.println("isBackupDoc : " + isBackupDoc);
    			vwdoc = ds.setDocument(doc, session.room, isBackupDoc);
    			VWClient.printToConsole("After calling setDocument inside if condition of dssused:::");   //last checked
    			System.out.println("After calling setDocument inside if condition of dssused:::");
    			if(storageType.equals("On Premises")){
    				System.out.println("Before WHILE !!!! ");
    				while (vwdoc.hasMoreChunks() && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE)
    				{
    					System.out.println("INSIDE WHILE !!!! ");
    					doc.setVWDoc(vwdoc);
    					VWClient.printToConsole("Inside while before calling setdocument");
    					System.out.println("Inside while before calling setdocument");
    					vwdoc = ds.setDocument(doc, session.room, isBackupDoc);	          
    					VWClient.printToConsole("Inside while after calling setdocument");
    					System.out.println("Inside while after calling setdocument");
    				}
    				System.out.println("AFTER WHILE !!!! ");
    			}
    		}else {
    			// Pass the schema to displayed the client address when upload the document
    			VWClient.printToConsole("Inside else before calling set documents");
    			System.out.println("Inside else before calling set documents");
    			VWClient.printToConsole("dssSchema.path : " + dssSchema.path);
    			System.out.println("dssSchema.path : " + dssSchema.path);
    			VWClient.printToConsole("doc.getid() : " + doc.getId());
    			System.out.println("doc.getid() : " + doc.getId());
    			VWClient.printToConsole("file : " + file);
    			System.out.println("file : " + file);
    			vwdoc = vws.setDocument(session.room, dssSchema, doc, file, mySchema);
    			VWClient.printToConsole("Inside else after calling set documents");
    			System.out.println("Inside else after calling set documents");
    			//vwdoc = ds.setDocument(doc, session.room);
    			//Checks whether the document has more chunks(Each chunk is of size 4MB)
    			if(storageType.equals("On Premises")){ 
    				while (vwdoc.hasMoreChunks() && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE)
    				{
    					VWClient.printToConsole("Inside else of while looop before calling set document");
    					System.out.println("Inside else of while looop before calling set document");
    					doc.setVWDoc(vwdoc);
    					vwdoc = vws.setDocument(session.room, dssSchema, doc, file, mySchema);
    				}
    			}
    		}
    		if(storageType.equals("On Premises")){
    			System.out.println("On Premises !!!! ");

    			if ((!vwdoc.isVaildZip() || !vwdoc.getIntegrity()) && !retry && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE){
    				addATRecord(vws, session.room, sid, doc.getId(),
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_DOC_FILEUPDATE_FAIL_DESC);
    				System.out.println("After addATRecord !!!! ");
    				vwdoc.setVaildZip(true);
    				return setDocFile(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, true);

    			}
    			if ((!vwdoc.isVaildZip() || !vwdoc.getIntegrity()) && (retry || vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE)){
    				//copied all.zip and moved to Application Data\ViewWise\Recover\Room\docId location.  
    				System.out.println("copied all.zip and moved to Application Data location");
    				try{
    					if (!backupLocation.exists()) backupLocation.mkdirs();
    					VWClient.printToConsole("backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER::::0"+backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER);
    					System.out.println("backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER::::0"+backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER);
    					boolean result = VWCUtil.copyFile(file, new File(backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER));
    					// To create a document.properties file in the backup location
    					VWClient.printToConsole("before calling createDocProperties:::"+backupPath);
    					System.out.println("before calling createDocProperties:::"+backupPath);
    					createDocProperties(sid, doc, backupPath);
    					VWClient.printToConsole("after calling createDocProperties:::"+backupPath);
    					System.out.println("after calling createDocProperties:::"+backupPath);
    				}catch (Exception e) {
    					VWClient.printToConsole("Inside corrupted 1:::::"+e.getMessage());
    					System.out.println("Inside corrupted 1:::::"+e.getMessage());
    					return DSSZipCorrupted;
    				}
    				if (vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE){
    					System.out.println("DISKSPACE_NOT_AVAILABLE !!!!");
    					return DSSDiskSpaceNotAvailable;
    				}else{
    					VWClient.printToConsole("Inside corrupted 2:::::");
    					System.out.println("Inside corrupted 2:::::");
    					return DSSZipCorrupted;
    				}

    			}
    			if (vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE){
    				System.out.println("DISKSPACE_NOT_AVAILABLE 2 !!!!");
    				return DSSDiskSpaceNotAvailable;
    			}
    		}//CV10 Enhancement added for Azure storage file upload
    		else if(storageType.equals("Azure Storage")){
    			System.out.println("Azure Storage !!!! ");
    			if (!backupLocation.exists()) 
    				backupLocation.mkdirs();

    			boolean result = VWCUtil.copyFile(file, new File(backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER));
    			VWClient.printToConsole("result in Azure Storage::::"+result);
    			System.out.println("result in Azure Storage::::"+result);

    			try {
    				String accountName="AccountName="+azureUserName+";";
    				String password="AccountKey="+azurePassword;
    				String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
    				System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
    				System.out.println("Azure Storage try !!!! ");

    				storageAccount = CloudStorageAccount.parse(storageConnectionString1);
    				VWClient.printToConsole("storageAccount connection established :::"+storageAccount.getFileStorageUri());
    				System.out.println("storageAccount connection established :::"+storageAccount.getFileStorageUri());
    				fileClient = storageAccount.createCloudFileClient();
    				VWClient.printToConsole("File client storage Uri ::"+fileClient.getStorageUri());
    				System.out.println("File client storage Uri ::"+fileClient.getStorageUri());
    				String pattern = Pattern.quote(System.getProperty("file.separator"));
    				String dbpath[]=dbStoragePath.split(pattern);

    				for(int i=0;i<dbpath.length;i++){
    					if(i==2)
    					{
    						share = fileClient.getShareReference(dbpath[i]);
    						VWClient.printToConsole("Inside loop count 2 share :"+share.getUri());
    						System.out.println("Inside loop count 2 share :"+share.getUri());
    						if(!share.exists())
    							share.create();
    					}
    					else if(i==3)
    					{
    						sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
    						VWClient.printToConsole("Inside loop count 3 share :"+share.getUri());
    						System.out.println("Inside loop count 3 share :"+share.getUri());
    						if(!sampleDir.exists()){
    							sampleDir.create();

    						} 
    						rootDir = sampleDir;
    						sampleDir = null;
    					}
    					else if(i>=4)
    					{
    						VWClient.printToConsole("Inside loop count 4 share :"+share.getUri());
    						System.out.println("Inside loop count 4 share :"+share.getUri());
    						if ((i+1) != dbpath.length) {
    							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
    							if(!sampleDir.exists()){
    								sampleDir.create();
    							}
    							rootDir = sampleDir;
    							sampleDir = null;
    						}
    						else if ((i+1) == dbpath.length) {
    							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
    							if(!sampleDir.exists()){
    								sampleDir.create();
    							}
    							rootDir = sampleDir;

    							sampleDir = null;
    						}
    					}

    				}
    				sampleDir =	rootDir.getDirectoryReference(String.valueOf(doc.getId()));
    				VWClient.printToConsole("sampleDir.geturi:  " + sampleDir.getUri());
    				System.out.println("sampleDir.geturi:  " + sampleDir.getUri());
    				if(!sampleDir.exists()){
    					sampleDir.create();

    				}
    				cloudFile = sampleDir.getFileReference("all.zip");

    				if (sampleDir.exists()) {
    					/*
    					 * CV10 Azure set active version issue fix.Upload document after setactive version or properties update
    					 * Zip creation was not hapenning
    					 */
    					if(cloudFile.exists()){
    						String newVersion = "";
    						Vector newVersVector = new Vector();
    						DocType dt = doc.getDocType();
    						VWClient.printToConsole("Azure Before getDTVersionSettings call");
    						System.out.println("Azure Before getDTVersionSettings call");
    						getDTVersionSettings(sid, dt);
    						if (dt!=null && dt.getVREnable().equals("1")){
    							VWClient.printToConsole("Azure Before checkNextDocVersion call");
    							System.out.println("Azure Before checkNextDocVersion call");
    							checkNextDocVersion(sid, doc, pageInfo, newDoc, newVersVector);
    							if(newVersVector!=null && newVersVector.size() > 0) {
    								newVersion =  (String) newVersVector.get(0);

    							}
    							if (newVersion != null && !newVersion.equals(""))
    							{
    								CloudFile currentvercloudfile = sampleDir.getFileReference(doc.getVersion());
    								if(!currentvercloudfile.exists()){
    									currentvercloudfile.startCopy(cloudFile);
    								}
    							}
    						}/*else{
								VWClient.printToConsole("Inside else before delete docversion :::::::::::::"+doc.getVersion());
								if(!doc.getVersion().equals("")){
									CloudFile neVercloudfile = sampleDir.getFileReference(doc.getVersion());
									VWClient.printToConsole("neVercloudfile geturi::::::::::::"+neVercloudfile.getUri());
									if(!neVercloudfile.exists()){
										neVercloudfile.startCopy(cloudFile);
									}
								}
							}*/
    						cloudFile.delete();

    					}
    					VWClient.printToConsole("RoomName getEncryption in setdocfile::::::::::"+session.room);
    					System.out.println("RoomName getEncryption in setdocfile::::::::::"+session.room);
    					String key=vws.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
    					DSSCipher cipher = new DSSCipher(key);
    					if ((cipher != null)&&(!key.equals("")))  
    					{
    						String fileSrcPath=file.getPath();
    						cipher.encryptFile(fileSrcPath);
    						cloudFile.uploadFromFile(fileSrcPath);
    						VWClient.printToConsole("Azure file upload with encryption");
    						System.out.println("Azure file upload with encryption");
    					}else{
    						VWClient.printToConsole("Azure file upload without encryption");
    						System.out.println("Azure file upload without encryption");
    						cloudFile.uploadFromFile(file.getPath());
    					}
    					if(cloudFile.exists())
    						VWClient.printToConsole("Document with "+" "+ doc.getId()+" "+"Uploaded SucessFully to Azure Storage");
    					System.out.println("Document with "+" "+ doc.getId()+" "+"Uploaded SucessFully to Azure Storage");
    				} 

    				if(indexerFlag){
    					addIndexerDoc(vws, session.room, sid, doc.getId(),3);
    				}

    			}catch (Exception e) {
    				VWClient.printToConsole("Failed to save document to cloud :::"+e.getMessage());
    				System.out.println("Failed to save document to cloud :::"+e.getMessage());
    				String errorMessage=e.getMessage().toString();
    				VWClient.printToConsole("errorMessage:::::"+errorMessage);
    				System.out.println("errorMessage:::::"+errorMessage);
    				if(errorMessage.equals("Storage Key is not a valid base64 encoded string.")){
    					VWClient.printToConsole("Inside if condition of exception");
    					System.out.println("Inside if condition of exception");
    					return ViewWiseErrors.invalidCredentials;
    				}else{
    					VWClient.printToConsole("Inside else condition of exception");
    					System.out.println("Inside else condition of exception");
    					return ViewWiseErrors.uploadFailed;
    				}

    			}




    			// To create a document.properties file in the backup location
    			createDocProperties(sid, doc, backupPath);

    			/*
					Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
					Created by: <Pandiya Raj.M>
					Date: <6 Jun 2006>
    			 */
    			/*
    			 * Issue No / Purpose:  Indexer issue # 633 this method not sending indexOption value '3'>
    			 * i.e the new status '3' is not going as param to DB Whenever add/modify a page in a document in DTC,
    			 * Created by:	C.Shanmugavalli	Date: 2 July 2006
    			 */
    			// Check that document modified, if modified it will delete the page reference in indexer table and store the '3' for 'qc' column for that document            	                
    			int indexOption = 3;
    			boolean flag = false;
    			if (pageInfo != null && pageInfo.length() > 0){
    				ArrayList listPage = new ArrayList();
    				StringTokenizer sToken = new StringTokenizer(pageInfo,",");
    				String pageNo,pageID,operation = null;
    				while(sToken.hasMoreTokens()){listPage.add(sToken.nextToken());}
    				int count = 1;
    				for(int index=0;index<(listPage.size()/3);index++)
    				{
    					try {
    						pageNo = String.valueOf(listPage.get(count++));
    						pageID = String.valueOf(listPage.get(count++));
    						operation = String.valueOf(listPage.get(count++));                			
    						if ("Y".equals(operation.toUpperCase()) || "D".equals(operation.toUpperCase())){ // Y means Modified.
    							int pageid = 0;
    							if (pageID!= null) {pageid =  Integer.parseInt(pageID);}

    							VWClient.printToConsole("Inside azure Before callind DBRemovePageText::::pageid"+pageid);
    							System.out.println("Inside azure Before callind DBRemovePageText::::pageid"+pageid);
    							vws.DBRemovePageText(session.room,doc.getId(),pageid);
    							flag =true;
    						}
    						//W means page added. D means deleted page
    						// N means Not deleted page. Y means page modified
    						if(("W".equals(operation.toUpperCase()))){ 
    							//if(!("W".equals(operation.toUpperCase()))){ // commented for Issue # 633 
    							flag =true;
    						}
    					}catch(Exception exception){}
    				}
    			}

    			if(indexerFlag){
    				addIndexerDoc(vws, session.room, sid, doc.getId(),3);
    			}
    			if(isBackupDoc){
    				addATRecord(vws, session.room, sid, doc.getId(), 
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_REC_DOC_FILEUPDATE_DESC);
    				if(backupLocation.exists()){
    					VWClient.printToConsole("before delete backuplocaion::::"+backupLocation);
    					System.out.println("before delete backuplocaion::::"+backupLocation);
    					ret = deleteBackupLocation(backupLocation);		
    				}
    			}else{
    				addATRecord(vws, session.room, sid, doc.getId(), 
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_DOC_FILEUPDATE_DESC);
    			}
    			//After uploaded the all.zip; we need to make a database call to get the current version 
    			//Create the method ?updateVersionRevision()? which will create a copy of all.zip with current version
    			int result1 =  	vws.setDocAuditInfo(session.room, sid, doc, (indexerFlag?newDoc:false), pageInfo);
    			String updatedVersion = "";
    			Vector output = new Vector();
    			DocType dt = doc.getDocType();
    			VWClient.printToConsole("Azure Before getDTVersionSettings call");
    			System.out.println("Azure Before getDTVersionSettings call");
    			getDTVersionSettings(sid, dt);
    			if (dt!=null && dt.getVREnable().equals("1")){
    				VWClient.printToConsole("Azure Before checkNextDocVersion call");
    				System.out.println("Azure Before checkNextDocVersion call");
    				checkNextDocVersion(sid, doc, pageInfo, newDoc, output);
    				if(output!=null && output.size() > 0) {
    					updatedVersion =  (String) output.get(0);
    					if (updatedVersion != null && updatedVersion.trim().length() >0){
    						if (isDSSUsed()) {

    							VWClient.printToConsole("Azure In if dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
    							System.out.println("Azure In if dssused updateversion revision:::::updatedVersion::::"+updatedVersion);

    							if (updatedVersion != null && !updatedVersion.equals(""))
    							{   //CV10 Enhancement update document to azure with version revision commented.

    								CloudFile cloudFile1 = sampleDir.getFileReference(updatedVersion);
    								if(cloudFile.exists()){
    									try {
    										VWClient.printToConsole("Before start copy :::::");
    										System.out.println("Before start copy :::::");
    										cloudFile1.startCopy(cloudFile.getUri());
    										VWClient.printToConsole("After start copy :::::");
    										System.out.println("After start copy :::::");
    										cloudFile1.abortCopy(cloudFile1.getProperties().getCopyState().getCopyId());
    									}
    									catch (StorageException e) {
    										VWClient.printToConsole("Inside storage exception e::::"+e.getMessage());
    										System.out.println("Inside storage exception e::::"+e.getMessage());
    										if (!e.getErrorCode().contains("NoPendingCopyOperation")) {
    											return  ViewWiseErrors.renameAzureDoc;
    										}
    									}

    								}
    							}	

    						}
    						else{
    							VWClient.printToConsole("Azure In else of dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
    							System.out.println("Azure In else of dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
    							vws.updateVersionRevision(session.room, dssSchema, doc,  updatedVersion);   
    						}
    					}
    				}
    			}
    			return result1;
    		}//End of azure file upload

    		if (vwdoc.getIntegrity())
    		{            	
    			/*
				Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
				Created by: <Pandiya Raj.M>
				Date: <6 Jun 2006>
    			 */
    			/*
    			 * Issue No / Purpose:  Indexer issue # 633 this method not sending indexOption value '3'>
    			 * i.e the new status '3' is not going as param to DB Whenever add/modify a page in a document in DTC,
    			 * Created by:	C.Shanmugavalli	Date: 2 July 2006
    			 */
    			// Check that document modified, if modified it will delete the page reference in indexer table and store the '3' for 'qc' column for that document            	                
    			int indexOption = 3;
    			boolean flag = false;
    			if (pageInfo != null && pageInfo.length() > 0){
    				ArrayList listPage = new ArrayList();
    				StringTokenizer sToken = new StringTokenizer(pageInfo,",");
    				String pageNo,pageID,operation = null;
    				while(sToken.hasMoreTokens()){listPage.add(sToken.nextToken());}
    				int count = 1;
    				for(int index=0;index<(listPage.size()/3);index++)
    				{
    					try {
    						pageNo = String.valueOf(listPage.get(count++));
    						pageID = String.valueOf(listPage.get(count++));
    						operation = String.valueOf(listPage.get(count++));                			
    						if ("Y".equals(operation.toUpperCase()) || "D".equals(operation.toUpperCase())){ // Y means Modified.
    							int pageid = 0;
    							if (pageID!= null) {pageid =  Integer.parseInt(pageID);}
    							VWClient.printToConsole("azure before dbremovepagetText:::"+pageid);
    							System.out.println("azure before dbremovepagetText:::"+pageid);
    							vws.DBRemovePageText(session.room,doc.getId(),pageid);
    							flag =true;
    						}
    						//W means page added. D means deleted page
    						// N means Not deleted page. Y means page modified
    						if(("W".equals(operation.toUpperCase()))){ 
    							//if(!("W".equals(operation.toUpperCase()))){ // commented for Issue # 633 
    							flag =true;
    						}
    					}catch(Exception exception){}
    				}
    			}
    			/*                if(flag){
                	indexOption = 3;
                } */  

    			if(indexerFlag){
    				addIndexerDoc(vws, session.room, sid, doc.getId(),3);
    			}
    			if(isBackupDoc){
    				addATRecord(vws, session.room, sid, doc.getId(), 
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_REC_DOC_FILEUPDATE_DESC);
    				if(backupLocation.exists())
    					ret = deleteBackupLocation(backupLocation);					
    			}else{
    				addATRecord(vws, session.room, sid, doc.getId(), 
    						VWATEvents.AT_OBJECT_DOC,
    						VWATEvents.AT_DOC_FILEUPDATE, 
    						VWATEvents.AT_DOC_FILEUPDATE_DESC);
    			}
    			//After uploaded the all.zip; we need to make a database call to get the current version 
    			//Create the method ?updateVersionRevision()? which will create a copy of all.zip with current version

    			int result =  	vws.setDocAuditInfo(session.room, sid, doc, (indexerFlag?newDoc:false), pageInfo);
    			String updatedVersion = "";
    			Vector output = new Vector();
    			DocType dt = doc.getDocType();
    			getDTVersionSettings(sid, dt);
    			if (dt!=null && dt.getVREnable().equals("1")){
    				VWClient.printToConsole("Inside azure before calling checknextdocversion:::::"+pageInfo+"newDoc::::"+newDoc+"output:::"+output);
    				System.out.println("Inside azure before calling checknextdocversion:::::"+pageInfo+"newDoc::::"+newDoc+"output:::"+output);
    				checkNextDocVersion(sid, doc, pageInfo, newDoc, output);
    				if(output!=null && output.size() > 0) {
    					updatedVersion =  (String) output.get(0);
    					if (updatedVersion != null && updatedVersion.trim().length() >0){
    						if (isDSSUsed()) {
    							VWClient.printToConsole("Inside azure if isDSSUsed updateVersionRevision:::::"+updatedVersion);
    							System.out.println("Inside azure if isDSSUsed updateVersionRevision:::::"+updatedVersion);

    							ds.updateVersionRevision(doc, updatedVersion);
    						}
    						else{
    							VWClient.printToConsole("Inside azure else isDSSUsed updateVersionRevision:::::"+updatedVersion);
    							System.out.println("Inside azure else isDSSUsed updateVersionRevision:::::"+updatedVersion);

    							vws.updateVersionRevision(session.room, dssSchema, doc,  updatedVersion);     
    						}
    					}
    				}
    			}
    			return result;
    		}
    		else
    			return documentNotSet;
    	}
    	catch(Exception e)
    	{
    		VWClient.printToConsole("Error while set docFile::::"+e.getMessage());
    		System.out.println("Error while set docFile::::"+e.getMessage());
    		return Error;

    	}
    }
    /*
     * To check the Available Disk space from DSS 
     * for Dss low Disk Space alert.
     */
    public int checkAvailableSpaceInDSS(int sid, Document doc,String movePath,Vector size){
    	ServerSchema dssSchema = new ServerSchema();
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in checkAvailableSpaceInDSS::"+sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try
    	{
    		ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
    		dssSchema.set(ss);
    	}
    	catch(RemoteException re)
    	{
    		connectedVWSs.remove(session.server);
    		return Error;
    	}
    	DSS ds = null;

    	long availableSpace = 0;
    	
    	try{
    		ds = (DSS) Util.getServer(dssSchema);
    		if (ds == null) {
    			vws.sendNotification(session.room,sid,VWSConstants.DSS,VWSConstants.DSS_INACTIVE_MSG);
    			return dssInactive;   
    		}
    		long registrySpace=ds.getStoragelimit();
    		if (isDSSUsed()) {
    			VWClient.printToConsole("movePath : "+movePath);
    			if(!movePath.isEmpty()){
    				availableSpace =  ds.checkAvailableSpace(movePath);
    			}
    			else{
    				availableSpace =  ds.checkAvailableSpace(dssSchema.path);
    			}
    			size.add(availableSpace);
    		}else {
    			availableSpace =  vws.checkAvailableSpace(dssSchema, dssSchema.path);
    			size.add(availableSpace);
    		}
    	
    		size.add(registrySpace);
    		VWClient.printToConsole("Available disk space in MB"+availableSpace);
    		VWClient.printToConsole("Size from Registry in MB"+registrySpace);
    		if(registrySpace>availableSpace)
    		{    			
    			return -507;
    		}

    	}catch (Exception e) {
    		// TODO: handle exception
    		if (ds == null) {
    			try {

    				vws.sendNotification(session.room, sid, VWSConstants.DSS,VWSConstants.DSS_INACTIVE_MSG);
    			} catch (RemoteException e1) {
    				// TODO Auto-generated catch block
    				VWClient.printToConsole("Exception in sending mail"+e1.getMessage());
    			}

    		}
    		VWClient.printToConsole("Exception in  checkAvailableSpaceInDSS"+e.getMessage());
    	}
    	return NoError;
    }
    
	/**
     * deleteBackup -  To delete the backup folder
     * @param backupLocation - Location where the all.zip and document.properties exists
     * @return - Returns true if the backup folder is deleted successfully otherwise false
     */
    private boolean deleteBackupLocation(File backupLocation) {
    	boolean ret = false;
    	try{
    	File[] files = backupLocation.listFiles();
    	if(files != null && files.length>0){
			for(int i=0; i<files.length; i++){
				deleteBackupLocation(files[i]);
			}
		}
		ret = backupLocation.delete();
    	}catch(Exception e){
    		printToConsole("Error while deleting the backup location : "+e.getMessage());
    	}
		return ret;
	}
	/**
     * createDocProperties: To create a Document.properties file in the backup location
     * @param sid - session id
     * @param doc - Document object to which the properties file is created
     * @param backupPath - backup location where the properties file is created
     */
    private void createDocProperties(int sid, Document doc, String backupPath) {
    	try{
    		VWClient.printToConsole("inside createdoc properties::::");
    		Session session = getSession(sid);
    		String serverName = session.server;
    		Vector docInfo = new Vector();
    		String data = "";
    		data = "SessionId="+sid;
    		docInfo.add(data);		
    		data = "DocumentName="+doc.getName();
    		docInfo.add(data);		
    		data = "RoomName="+doc.getRoomName();
    		docInfo.add(data);
    		data = "ServerName="+serverName;
    		docInfo.add(data);
    		data = "Location="+doc.getLocation();
    		docInfo.add(data);		
    		data = "User="+doc.getCreator();
    		docInfo.add(data);		
    		data = "CreatedDate="+doc.getCreationDate();
    		docInfo.add(data);		
    		data = "DocumentId="+String.valueOf(doc.getId());
    		docInfo.add(data);

    		// Writing Document Information to the document.properties file
    		String docPropFilePath = backupPath +File.separator+"Document.properties";
    		File docPropFile = new File(docPropFilePath);

    		if(!docPropFile.exists()) 
    			docPropFile.createNewFile();
    		if(docPropFile != null && docPropFile.exists()){
    			BufferedWriter writer = new BufferedWriter(new FileWriter(docPropFile, true));
    			for(int i=0; i<docInfo.size(); i++){
    				writer.write(docInfo.get(i).toString());
    				writer.write("\n");
    			}
    			writer.close();
    		}
    	}catch(Exception e){
    		printToConsole("Error while writing Document.properties file"+e.getMessage());
    	}

    }
	public int registerOutput(int sid, String output)
    {  
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in registerOutput::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        if(output ==null || output.trim().equals("")) return invalidParameter;
        try
        {
              return vws.registerOutput(session.room, sid, output);
        }
        catch(Exception e)
        {
            return Error;
        }      
    }  
    protected void addATRecord(VWS vws, String room, int sid, int nid, int oid, 
                                                           int eid, String desc)
    {
        try
        {
            vws.addATRecord(room, sid, nid, oid, eid, desc);  
        }
        catch(RemoteException re){}
    } 
    //    Passed the indexOption value to update the 'IndexerDocs' table.
    protected void addIndexerDoc(VWS vws, String room, int sid, int nid, int option)
    {
        try
        {
            vws.addIndexerDoc(room, sid, nid,option);  
        }
        catch(RemoteException re){}
    }    
    public int addIndexerDoc(int sid, Document doc, int option)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in addIndexerDoc::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
              vws.addIndexerDoc(session.room, sid, doc.getId(),option);
        }
        catch(Exception e)
        {
            return Error;
        }
        return NoError;
    }    
    public int setDocFolder(int sid, Document doc, String srcFolder)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDocFolder::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        File file = new File(srcFolder);
        if (!file.isDirectory() || !file.exists()) return documentNotFound;
    	CloudStorageAccount storageAccount=null;
		CloudFileClient fileClient =null;
		CloudFile cloudFile=null;
		CloudFileShare share=null;
		CloudFileDirectory rootDir=null;
		CloudFileDirectory sampleDir =null;
		String dbStoragePath="";
		String azureUserName="";
		String azurePassword="";
		String storageType="";
		Vector storageVect=new Vector();
		getAzureStorageCredentials(sid,doc.getId(),storageVect);
		String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
        
        ServerSchema dssSchema = new ServerSchema();
        
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, 
                                                                     dssSchema);
            dssSchema.set(ss);
        }
        catch(RemoteException re)
        {
            connectedVWSs.remove(session.server);
            return Error;
        }
        //2 Direct DSS call changed - Valli
        //DSS ds = (DSS) Util.getServer(dssSchema);
        //if (ds == null) return dssInactive;
        if(storageArray[7].equals("On Premises")){
        VWDoc vwdoc = doc.getVWDoc();
        vwdoc.setSrcFile(file.getPath());
        vwdoc.setDstFile(dssSchema.path);
        try
        {
        	if (isDSSUsed()) {	    		
	            DSS ds = (DSS) Util.getServer(dssSchema);
	           /* if (ds == null){
	            	vws.sendNotification(session.room, sid, VWSConstants.DSS, resourceManager.getString("DSSInactive.Mail"));
	            	return dssInactive;                	        
	            }*/
        	
        	//DSS call changed to VWS Call and session.room param is not necessary
	        	//vwdoc = vws.setDocFolder(doc);
	            vwdoc = ds.setDocFolder(doc, session.room);
	            while (vwdoc.hasMoreChunks())
	            {
	            	//vwdoc = vws.setDocFolder(doc);
	                vwdoc = ds.setDocFolder(doc, session.room);
	                doc.setVWDoc(vwdoc);
	            }
        	}else{
        	vwdoc = vws.setDocFolder(doc);
            //vwdoc = ds.setDocFolder(doc, session.room);
            while (vwdoc.hasMoreChunks())
            {
            	vwdoc = vws.setDocFolder(doc);
                //vwdoc = ds.setDocFolder(doc, session.room);
                doc.setVWDoc(vwdoc);
            }
        	}
            if (vwdoc.getIntegrity()){
                //Census Issue No 875 - Indexer is not processing docs that have been added into ViewWise by the restore method
            	addIndexerDoc(vws, session.room, sid, doc.getId(),3);
                return NoError;
            }
            else
                return documentNotSet;
        }
        //catch(RemoteException re)
        catch(Exception re)
        {
            return Error;
        }
        }else if(storageArray[7].equals("Azure Storage")){
        	String restoredstPath=dssSchema.path;
        	restoredstPath=restoredstPath.substring(0, restoredstPath.length()-1);
        	String restoreSrcPath=file.getAbsolutePath();
        	VWClient.printToConsole("restoredstPath::::"+restoredstPath);
        	VWClient.printToConsole("restoreSrcPath::::"+restoreSrcPath);
        	try{
        		azureUserName=storageArray[8];
        		azurePassword=storageArray[9];
        		String accountName="AccountName="+azureUserName+";";
        		String password="AccountKey="+azurePassword;
        		String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
        		System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
        		storageAccount = CloudStorageAccount.parse(storageConnectionString1);
        		dbStoragePath=restoredstPath;
        		VWClient.printToConsole("dbStoragePath:::::::::::"+dbStoragePath);
        		File dstFile=new File(restoreSrcPath);
        		VWClient.printToConsole("dstFile::::::"+dstFile.getAbsolutePath());
        		fileClient = storageAccount.createCloudFileClient();
        		String pattern = Pattern.quote(System.getProperty("file.separator"));
        		String dbpath[]=dbStoragePath.split(pattern);

        		for(int i=0;i<dbpath.length;i++){
        			VWClient.printToConsole("value of i------"+i);
        			if(i==2)
        			{
        				VWClient.printToConsole("dbpath[i] inside 1 equal to 2-----"+dbpath[i]);
        				share = fileClient.getShareReference(dbpath[i]);
        				VWClient.printToConsole("After share creation");
        			}
        			else if(i==3)
        			{
        				VWClient.printToConsole("dbpath[i] inside i equal to 3"+dbpath[i]);
        				sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
        				rootDir = sampleDir;
        				sampleDir = null;
        			}
        			else if(i>=4)
        			{
        				VWClient.printToConsole("dbpath[i] inside i greater than 4"+dbpath[i]);
        				if ((i+1) != dbpath.length) {
        					VWClient.printToConsole("Inside if condition of  not equal to dbpath length :::::"+i);
        					sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        					if(!sampleDir.exists()){
        						sampleDir.create();
        					}
        					rootDir = sampleDir;
        					sampleDir = null;
        				}
        				else if ((i+1) == dbpath.length) {
        					VWClient.printToConsole("Inside else if conditionof equal to dbpath lenght ::::::::::"+i);
        					sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        					if(!sampleDir.exists()){
        						sampleDir.create();
        					}
        					rootDir = sampleDir;
        					sampleDir = null;
        				}
        			}

        		}
        		sampleDir=rootDir;
        		ArrayList backupList=getAzureBackupList(restoreSrcPath);
        		for(int i=0;i<backupList.size();i++){
        			String backupListFilePath=backupList.get(i).toString();
        			String backupDocName=backupListFilePath.substring(backupListFilePath.lastIndexOf("\\")+1,+backupListFilePath.length());
        			cloudFile=sampleDir.getFileReference(backupDocName);
        			cloudFile.uploadFromFile(backupListFilePath);
        		}
        	}

        	catch(Exception e){VWClient.printToConsole("Exception while getting file location from backuplist::::"+e.getMessage());
        	}
        }
		return NoError;
		}
    
    /** Copies a Document's file to the cache folder specified in VWClient constructor.
     * @param sid Session id obtained by login
     * @param doc The Document for which to copy the file
     * @return 0 for success or negative value error code
     */
    public int getDocFile(int sid, Document doc)
    {
        return this.getDocFile(sid, doc, null, false);
    }
    
    /** Copies a Document's file to the cache folder specified in VWClient constructor.
     * @param sid Session id obtained by login
     * @param doc The Document for which to copy the file
     * @param destination the destination folder
     * @return 0 for success or negative value error code
     */
    public int getDocFile(int sid, Document doc, String destination)
    {
        return this.getDocFile(sid, doc, destination, false);
    }
    
    /** Copies a Document's file to the cache folder specified in VWClient constructor.
     * @param sid Session id obtained by login
     * @param doc The Document for which to copy the file
     * @param cover true - will have cover page.
     * @return 0 for success or negative value error code
     */
    public int getDocFile(int sid, Document doc, boolean cover)
    {
        return this.getDocFile(sid, doc, null, cover);
    }
    public int getDocFile(int sid, Document doc, String destination, boolean cover)
    {
    	return this.getDocFile(sid, doc, destination, cover, 0);
    }

	/** Copies a Document's file to the cache folder specified in VWClient constructor.
	 * @param sid Session id obtained by login
	 * @param doc The Document for which to copy the file
	 * @param destination the destination folder
	 * @param cover true - will have cover page.
	 * @return 0 for success or negative value error code
	 */
	public int getDocFile(int sid, Document doc, String destination, boolean cover, int frameCount)
	{
		VWClient.printToConsole("----------------Start of getDocFile::::::::::::"+Util.getNow(2)+"----------------------------");
		VWClient.printToConsole("doc.getId() ::::::::::::"+doc.getId());
		if (doc.getId() <= 0) return invalidParameter;
		Session session = getSession(sid);
		VWClient.printToConsole("sessionId in getDocFile::"+session);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		VWClient.printToConsole("vws ::::::::::::"+vws);
		if (vws == null) return ServerNotFound;
		ServerSchema dssSchema = new ServerSchema();
		CloudStorageAccount storageAccount=null;
		CloudFileClient fileClient =null;
		CloudFile cloudFile=null;
		CloudFileShare share=null;
		CloudFileDirectory rootDir=null;
		CloudFileDirectory sampleDir =null;
		String dbStoragePath="";
		String azureUserName="";
		String azurePassword="";
		String storageType="";
		try
		{
			Vector storageVect=new Vector();
			VWClient.printToConsole("Before calling getazure credentials ::::::::::::"+Util.getNow(2));
			int aret = getAzureStorageCredentials(sid,doc.getId(),storageVect);		
			VWClient.printToConsole("getAzureStorageCredentials aret ::::::::::::"+aret);
			if(aret < 0)
			{
				VWClient.printToConsole("getAzureStorageCredentials aret ::::::::::::"+aret);
				return aret;		
			}
			VWClient.printToConsole("After calling getazure credentials ::::::::::::"+Util.getNow(2));
			VWClient.printToConsole("storageVect ::::::::::::"+storageVect);
			if(storageVect!=null&&storageVect.size()>0){
				StringTokenizer st = new StringTokenizer(storageVect.get(0).toString(), Util.SepChar); 
				String storageid=st.nextToken();
				String systemName=st.nextToken();
				String ipaddress=st.nextToken();
				String port=st.nextToken();
				String hostIp=st.nextToken();
				dbStoragePath=st.nextToken();
				String Uncpath=st.nextToken();
				storageType=st.nextToken();
				azureUserName=st.nextToken();
				azurePassword=st.nextToken();
				VWClient.printToConsole("storageType in getDocFile::::::"+storageType);
			}
			VWClient.printToConsole("Before getDSSforDoc::::::::::::"+Util.getNow(2));
			ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc,dssSchema);
			VWClient.printToConsole("After getDSSforDoc::::::::::::"+Util.getNow(2));

			if (ss != null && !ss.path.equals(""))
			{
				dssSchema.set(ss);
			}
			else
			{
				VWClient.printToConsole("doc.getVersion()::::::::::::"+doc.getVersion());
				if (!doc.getVersion().equals(""))
					return versionNotFound;
				else
				{
					VWClient.printToConsole("doc.getVersion.equals = NULL ::::::::::::"+Error);
					return Error;
				}
			}
		}
		catch(RemoteException re)
		{
			VWClient.printToConsole("Exception in getDocFile : "+re.getMessage());
			connectedVWSs.remove(session.server);
			return Error;
		}
		//3 Direct DSS call changed - Valli
		//DSS ds = (DSS) Util.getServer(dssSchema);
		//if (ds == null) return dssInactive;
		boolean insideIdFolder = true;
		if (destination != null)
		{
			mySchema.path = destination;
			insideIdFolder = false;
		}
		else {
			mySchema.path = session.cacheFolder;
			VWClient.printToConsole("session. cachefolder:::"+session.cacheFolder);
		}
		VWClient.printToConsole("insideIdFolder:::"+insideIdFolder);
		try
		{
			boolean gotDocFile = false;
			String dstfile11 ="";
			Vector versionList =  new Vector();
			int ret  = getDocumentVers( sid, doc, versionList);
			if (isDSSUsed())//(isServerProxyEnabled(session.server) || type == 1 || type == 2)
			{
				DSS ds = (DSS) Util.getServer(dssSchema);
				VWClient.printToConsole("getServer ds :::"+ds);
				if (ds == null) return dssInactive;
				VWClient.printToConsole("storageType::::::::::::"+storageType);
				if(storageType.equals("On Premises")){
					VWClient.printToConsole("Before getRMIDoc::::::::::::"+Util.getNow(2));
					VWDoc vwdoc = ds.getRMIDoc(doc, session.room, dssSchema.path, mySchema, insideIdFolder, versionList);
					VWClient.printToConsole("After getRMIDoc::::::::::::"+Util.getNow(2));
					VWClient.printToConsole("getRMIDoc vwdoc :::"+vwdoc);
					if (vwdoc == null) return documentHasNoPage;
					doc.setVWDoc(vwdoc);
					VWClient.printToConsole("before calling vwdoc writeContents ");
					vwdoc.writeContents();
					VWClient.printToConsole("after calling vwdoc writeContents ");
					while(vwdoc.getBytesWritten()<vwdoc.getSize()){
						VWClient.printToConsole("before calling getNextRMIDoc ");
						vwdoc = ds.getNextRMIDoc(dssSchema, doc);
						VWClient.printToConsole("after calling getNextRMIDoc ");
						doc.setVWDoc(vwdoc);              	
						vwdoc.writeContents();
						VWClient.printToConsole("vwdoc.getBytesWritten() :"+vwdoc.getBytesWritten());
						VWClient.printToConsole("vwdoc.getSize() :"+vwdoc.getSize());
					}
					VWClient.printToConsole("After writing contents::::::::::::"+Util.getNow(2));
				}else if(storageType.equals("Azure Storage")){
					try{
						String accountName="AccountName="+azureUserName+";";
						String password="AccountKey="+azurePassword;
						String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
						System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
						storageAccount = CloudStorageAccount.parse(storageConnectionString1);
						
						File dstFile=new File(mySchema.path);
						fileClient = storageAccount.createCloudFileClient();
						String pattern = Pattern.quote(System.getProperty("file.separator"));
						String dbpath[]=dbStoragePath.split(pattern);

						for(int i=0;i<dbpath.length;i++){
							if(i==2)
							{
								share = fileClient.getShareReference(dbpath[i]);
							}
							else if(i==3)
							{
								sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
								rootDir = sampleDir;
								sampleDir = null;
							}
							else if(i>=4)
							{
								if ((i+1) != dbpath.length) {
									VWClient.printToConsole("Inside if condition of  not equal to dbpath length :::::"+i);
									sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
									if(!sampleDir.exists()){
										sampleDir.create();
									}
									rootDir = sampleDir;
									sampleDir = null;
								}
								else if ((i+1) == dbpath.length) {
									sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
									if(!sampleDir.exists()){
										sampleDir.create();
									}
									rootDir = sampleDir;
									sampleDir = null;
								}
							}

						}
						sampleDir = rootDir.getDirectoryReference(String.valueOf(doc.getId()));
						VWClient.printToConsole("GetdocFile sampleDir geturi::::"+sampleDir.getUri());
						if(doc.getVersion().length()>0){
							cloudFile = sampleDir.getFileReference(doc.getVersion());
						}else {
							cloudFile = sampleDir.getFileReference("all.zip");
						}

						File partenFile=new File(mySchema.path);
						if(!partenFile.exists()){
							partenFile.mkdir();
						}

						if(doc.getVersion().length()>0){
							String destinationFile=partenFile.getAbsolutePath()+"\\"+String.valueOf(doc.getId())+"\\"+doc.getVersion();
							VWClient.printToConsole("destinationFile path::::"+destinationFile);
							File destinationFolder=new File(partenFile.getAbsolutePath());
							if(!destinationFolder.exists()){
								destinationFolder.mkdir();
							}

							File docFile=new File(destinationFile);
							if(!docFile.exists())
								docFile.createNewFile();
							File tempFile=new File(destinationFile+"\\"+doc.getVersion());
							if(!tempFile.exists()){
								cloudFile.downloadToFile(destinationFile+"\\"+doc.getVersion());
								File oldName = new File(destinationFile+"\\"+doc.getVersion());
								String newFile=destinationFile+"\\"+"all.zip";
								File newName = new File(newFile);
								if(oldName.renameTo(newName)) {
									VWClient.printToConsole("File renamed sucessfully");
									String key=vws.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
									if(newName.exists()&&oldName.exists())
										oldName.delete();
									DSSCipher cipher = new DSSCipher(key);
									if ((cipher != null)&&(!key.equals("")))  
									{
										cipher.decryptFile(newFile);
										VWClient.printToConsole("After decrypting file in azure getdocfile");
									}
								} else {
									VWClient.printToConsole("Error while renaming file");
								}
							}
						}else{
							String key=vws.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
							DSSCipher cipher = new DSSCipher(key);

							if ((cipher != null)&&(!key.equals("")))  
							{
								String fileSrcPath=mySchema.path+"\\"+String.valueOf(doc.getId())+"\\"+"all.zip";
								cloudFile.downloadToFile(fileSrcPath);
								cipher.decryptFile(fileSrcPath);
								VWClient.printToConsole("Downloading file from azure and decrypting");
							}else{
								VWClient.printToConsole("Downloading file from azure without decrypting");
								File tempFile1=new File(mySchema.path+"\\"+String.valueOf(doc.getId())+"\\"+"all.zip");
								if(!tempFile1.exists())
								cloudFile.downloadToFile(mySchema.path+"\\"+String.valueOf(doc.getId())+"\\"+"all.zip");
							}

							VWClient.printToConsole("Document with "+" "+ doc.getId()+" "+"downloaded from Azure Storage ");
						} 
					}catch(Exception e){
						VWClient.printToConsole("Exception while downloading the file from azure:::::"+e.getMessage());
					}

				}	
				mySchema.setRoom(session.room);
				ds.writeLogMsg(mySchema, doc,session.room);
				gotDocFile = doc.getVWDoc().getIntegrity();
		

			}
			else
			{
				VWClient.printToConsole("Inside else of dss used operations beforgetRMIDoc call");
				VWClient.printToConsole("Before getRMIDoc::::::::::::"+Util.getNow(2));
				VWDoc vwdoc = vws.getRMIDoc(doc, session.room, dssSchema.path, dssSchema, mySchema, insideIdFolder,versionList);        
				VWClient.printToConsole("After getRMIDoc::::::::::::"+Util.getNow(2));
				VWClient.printToConsole("getRMIDoc vwdoc :::"+vwdoc);
				if (vwdoc == null){
					if ( vws.getActiveError() == dssInactive) return dssInactive;
					return documentHasNoPage;
				}
				//VWDoc vwdoc = ds.getRMIDoc(doc, session.room, dssSchema.path, mySchema, insideIdFolder);
				doc.setVWDoc(vwdoc);        
				VWClient.printToConsole("Before write contents else of dss read operations");
				vwdoc.writeContents();
				VWClient.printToConsole("after write contents else of dss read operations");
				//System.out.println(" getBytesWritten ="+vwdoc.getBytesWritten());

				//vwdoc.getSrcFile();
				VWClient.printToConsole("vwdoc.getBytesWritten():::"+vwdoc.getBytesWritten());
				VWClient.printToConsole("vwdoc.getSize():::"+vwdoc.getSize());

				while(vwdoc.getBytesWritten()<vwdoc.getSize()){
					VWClient.printToConsole(" before getNextRMIDoc");
					vwdoc = vws.getNextRMIDoc(doc, session.room, dssSchema.path, dssSchema, mySchema, insideIdFolder);
					VWClient.printToConsole(" after getNextRMIDoc");
					doc.setVWDoc(vwdoc);              	
					vwdoc.writeContents();
					//System.out.println(" 2 getBytesWritten ="+vwdoc.getBytesWritten());
				}
				vws.writeLogMsg(dssSchema, doc,session.room);

				/*while (!vwdoc.finished())
                {
                	vwdoc = vws.getRMIDoc(doc, session.room, dssSchema.path, dssSchema, mySchema, insideIdFolder);
                    //vwdoc = ds.getRMIDoc(doc, session.room, dssSchema.path, mySchema, insideIdFolder); 
                    
                    vwdoc.writeContents();
                    doc.setVWDoc(vwdoc);
                }*/
				if(storageType.equals("On Premises"))
					gotDocFile = vwdoc.getIntegrity();
			}
			if(storageType.equals("On Premises")){
				VWClient.printToConsole("gotDocFile :::"+gotDocFile);
				if (gotDocFile)
				{
					addATRecord(vws, session.room, sid, doc.getId(), 
							VWATEvents.AT_OBJECT_DOC,
							VWATEvents.AT_DOC_OPEN, 
							VWATEvents.AT_DOC_FILEOPEN_DESC);
					if(doc!=null){
						int pageCount = (frameCount>0)?frameCount:doc.getPageCount();
						if (cover) getDocCoverPage(sid, doc, pageCount);
					}
					VWClient.printToConsole("----------------End of getDocFile::::::::::::"+Util.getNow(2)+"----------------------------");
					return NoError;
				}
				else
					return documentNotFound;
			}else if(storageType.equals("Azure Storage")){

				addATRecord(vws, session.room, sid, doc.getId(), 
						VWATEvents.AT_OBJECT_DOC,
						VWATEvents.AT_DOC_OPEN, 
						VWATEvents.AT_DOC_FILEOPEN_DESC);
				if(doc!=null){
					int pageCount = (frameCount>0)?frameCount:doc.getPageCount();
					if (cover) getDocCoverPage(sid, doc, pageCount);
				}
				return NoError;

			}
		}
		catch(RemoteException re)
		{
			/**
			 * SIDBI This code is added for general error code -1 . This will try to connect with DSS server 2 times. As discussed with Rajesh on 09 Apr 2019 
			 * @author : Vanitha
			 */
			int errorMsg =  0;
			VWClient.printToConsole("Exception while getdocFile 1:::::"+re.getMessage());
			VWClient.printToConsole("Exception while getdocFile:::::"+re.getStackTrace());
			if (re.getMessage().contains(".SocketException")) {
				try {					
					VWClient.printToConsole("Before calling getDocFile from exception block ");
					errorMsg = getDocFile(sid, doc, destination, cover, frameCount);
					VWClient.printToConsole("After calling getDocFile from exception block errorMsg 1:"+errorMsg);
					if (errorMsg == -1) {
						errorMsg = getDocFile(sid, doc, destination, cover, frameCount);
						VWClient.printToConsole("After calling getDocFile from exception block errorMsg 2:"+errorMsg);
						return errorMsg;
					}
				} catch (Exception err) {
					VWClient.printToConsole("Exception in getDocFile 2:::::"+err.getMessage());
					return Error;
				}
			} else
				return Error;
			//return Error;			
		}
		return NoError;
	}
	
	/**
	 * CV2019 enhancement
	 * @param sid
	 * @param doc
	 * @param destination
	 * @return
	 */
	public int getDocFileWeb(int sid, Document doc, String destination)
	{
		return this.getDocFileWeb(sid, doc, destination, false, 0);
	}
	/**
	 * CV2019 enhancement
	 * @param sid
	 * @param doc
	 * @param destination
	 * @param cover
	 * @param frameCount
	 * @return
	 */
	public int getDocFileWeb(int sid, Document doc, String destination, boolean cover, int frameCount)
	{
		VWClient.printToConsole("----------------Start of getDocFile::::::::::::"+Util.getNow(2)+"----------------------------");
		if (doc.getId() <= 0) return invalidParameter;
		Session session = getSession(sid);
		System.out.println("sessionId in getDocFileWeb::"+sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		ServerSchema dssSchema = new ServerSchema();
		CloudStorageAccount storageAccount=null;
		CloudFileClient fileClient =null;
		CloudFile cloudFile=null;
		CloudFileShare share=null;
		CloudFileDirectory rootDir=null;
		CloudFileDirectory sampleDir =null;
		String dbStoragePath="";
		String azureUserName="";
		String azurePassword="";
		String storageType="";
		ServerSchema mySchemaLocalVar = Util.getMySchema();
		System.out.println("mySchemaLocalVar getdocfileweb  ::::  "+mySchemaLocalVar+" =  "+sid);
		System.out.println("session.cacheFolder ::::  "+session.cacheFolder+" =  "+sid);
		try
		{
			Vector storageVect=new Vector();
			System.out.println("Before calling getdocfileweb credentials ::::::::::::"+Util.getNow(2)+" = "+sid);
			getAzureStorageCredentials(sid,doc.getId(),storageVect);
			System.out.println("After calling getdocfileweb credentials ::::::::::::"+Util.getNow(2)+" = "+sid);
			if(storageVect!=null&&storageVect.size()>0){
				StringTokenizer st = new StringTokenizer(storageVect.get(0).toString(), Util.SepChar); 
				String storageid=st.nextToken();
				String systemName=st.nextToken();
				String ipaddress=st.nextToken();
				String port=st.nextToken();
				String hostIp=st.nextToken();
				dbStoragePath=st.nextToken();
				String Uncpath=st.nextToken();
				storageType=st.nextToken();
				azureUserName=st.nextToken();
				azurePassword=st.nextToken();
				System.out.println("storageType in getdocfileweb :::::: "+storageType+" = "+sid);
			}
			System.out.println("Before getDSSforDoc::::::::::::"+Util.getNow(2)+" = "+sid);
			ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc,dssSchema);
			System.out.println("After getDSSforDoc::::::::::::"+Util.getNow(2)+" = "+sid);

			if (ss != null && !ss.path.equals(""))
			{
				dssSchema.set(ss);
			}
			else
			{
				if (!doc.getVersion().equals(""))
					return versionNotFound;
				else
					return Error;
			}
		}
		catch(RemoteException re)
		{
			connectedVWSs.remove(session.server);
			return Error;
		}
		boolean insideIdFolder = true;
		if (destination != null)
		{
			mySchema.path = destination;
			mySchemaLocalVar.setPath(destination);
			insideIdFolder = false;
		}
		else {
			mySchema.path = session.cacheFolder;
			mySchemaLocalVar.setPath(session.cacheFolder);
			System.out.println("session. cachefolder:::"+session.cacheFolder);
		}
		System.out.println("insideIdFolder:::"+insideIdFolder+" = "+sid);
		try
		{
			boolean gotDocFile = false;
			String dstfile11 ="";
			Vector versionList =  new Vector();
			int ret  = getDocumentVers( sid, doc, versionList);
			if (isDSSUsed())//(isServerProxyEnabled(session.server) || type == 1 || type == 2)
			{
				DSS ds = (DSS) Util.getServer(dssSchema);
				if (ds == null) return dssInactive;

				if(storageType.equals("On Premises")){
					System.out.println("Before getRMIDoc::::::::::::"+Util.getNow(2)+" = "+sid);
					System.out.println(" dssSchema.path in getdocFileweb is :::"+ dssSchema.path+" = "+sid);
					System.out.println("mySchema in getdocFileweb is :::"+ mySchemaLocalVar+" = "+sid);
					System.out.println("insideIdFolder in getdocFileweb is :::"+ insideIdFolder+" = "+sid);
					VWDoc vwdoc = ds.getRMIDocWeb(doc, session.room, dssSchema.path, mySchemaLocalVar, insideIdFolder, versionList);
					System.out.println("After getRMIDoc::::::::::::"+Util.getNow(2)+" = "+sid);
					if (vwdoc == null) return documentHasNoPage;
					doc.setVWDoc(vwdoc);    
					vwdoc.writeContents();  
					while(vwdoc.getBytesWritten()<vwdoc.getSize()){
						vwdoc = ds.getNextRMIDoc(dssSchema, doc);
						doc.setVWDoc(vwdoc);              	
						vwdoc.writeContents();
					}
					System.out.println("After writing contents::::::::::::"+Util.getNow(2)+" = "+sid);
				}
				mySchema.setRoom(session.room);
				mySchemaLocalVar.setRoom(session.room);
				ds.writeLogMsg(mySchemaLocalVar, doc,session.room);
				gotDocFile = doc.getVWDoc().getIntegrity();

			}
			else
			{
				System.out.println("Inside else of dss used operations beforgetRMIDoc call   "+" = "+sid);
				System.out.println("Before getRMIDoc::::::::::::"+Util.getNow(2)+" = "+sid);
				VWDoc vwdoc = vws.getRMIDocWeb(doc, session.room, dssSchema.path, dssSchema, mySchemaLocalVar, insideIdFolder,versionList);        
				System.out.println("After getRMIDoc  :::::::::::: "+Util.getNow(2)+" = "+sid);
				if (vwdoc == null){
					if ( vws.getActiveError() == dssInactive) return dssInactive;
					return documentHasNoPage;
				}
				//VWDoc vwdoc = ds.getRMIDoc(doc, session.room, dssSchema.path, mySchema, insideIdFolder);
				doc.setVWDoc(vwdoc);        
				System.out.println("Before write contents else of dss read operations	"+" = "+sid);
				vwdoc.writeContents();
				System.out.println("after write contents else of dss read operations	"+" = "+sid);
				System.out.println(" getBytesWritten =	"+vwdoc.getBytesWritten()+" = "+sid);

				//vwdoc.getSrcFile();
			    System.out.println("vwdoc.getBytesWritten():::"+vwdoc.getBytesWritten()+" = "+sid);
				System.out.println("vwdoc.getSize():::"+vwdoc.getSize()+" = "+sid);

				while(vwdoc.getBytesWritten()<vwdoc.getSize()){
					System.out.println(" before getNextRMIDoc	"+" = "+sid);
					vwdoc = vws.getNextRMIDoc(doc, session.room, dssSchema.path, dssSchema, mySchemaLocalVar, insideIdFolder);
					System.out.println(" after getNextRMIDoc	"+" = "+sid);
					doc.setVWDoc(vwdoc);              	
					vwdoc.writeContents();
					System.out.println(" 2 getBytesWritten =	"+vwdoc.getBytesWritten()+" = "+sid);
				}
				vws.writeLogMsg(dssSchema, doc,session.room);

			
				if(storageType.equals("On Premises"))
					gotDocFile = vwdoc.getIntegrity();
			}
			if(storageType.equals("On Premises")){
				if (gotDocFile)
				{
					addATRecord(vws, session.room, sid, doc.getId(), 
							VWATEvents.AT_OBJECT_DOC,
							VWATEvents.AT_DOC_OPEN, 
							VWATEvents.AT_DOC_FILEOPEN_DESC);
					if(doc!=null){
						int pageCount = (frameCount>0)?frameCount:doc.getPageCount();
						if (cover) getDocCoverPage(sid, doc, pageCount);
					}
					System.out.println("----------------End of getDocFileWeb::::::::::::"+Util.getNow(2)+"----------------------------");
					return NoError;
				}
				else
					return documentNotFound;
			}
		}
		catch(RemoteException re)
		{
			System.out.println("Exception while getdocFileWeb:::::"+re.getMessage()+" = "+sid);
			return Error;
		}
		return NoError;
	}
    public int moveDocFolder(int sid, Document doc, ServerSchema Ddss)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in moveDocFolder::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0 || Ddss == null) 
                                                        return invalidParameter;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        ServerSchema Sdss = new ServerSchema();
        try
        {
            ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, Sdss);
            Sdss.set(ss);
        }
        catch(RemoteException re)   
        {
            connectedVWSs.remove(session.server);
            return Error;
        }
        //4 Direct DSS call changed - Valli
        //DSS ds = (DSS) Util.getServer(Sdss);
        //if (ds == null) return dssInactive;
        try
        {
         	String srcPath = String.valueOf(Sdss.path);
        	String dstPath = String.valueOf(Ddss.path);
        	if(!Sdss.path.substring(0,1).equalsIgnoreCase(Ddss.path.substring(0,1))){
        		Vector latestDocId = new Vector();
        		Vector size = new Vector();
        		getLatestDocId(sid, latestDocId);

        		Document latestDoc = new Document(Integer.parseInt((String) latestDocId.get(0)));
        		int retValue = checkAvailableSpaceInDSS(sid, latestDoc, Ddss.path, size);
        		 long availableSpace=(Long) size.elementAt(0);
        		if(retValue==dssCriticalError){
        			SendEmailFromDSS(availableSpace,sid,latestDoc);
        			VWClient.printToConsole("retValue : "+retValue); 
        			return dssCriticalError;
        		}
        	}
        	boolean isMoved = false;
        	if (isDSSUsed()) {
	            DSS ds = (DSS) Util.getServer(Sdss);
	            if (ds == null) return dssInactive;  
                if (ds.moveDocFolder(doc, session.room, Sdss, Ddss)) isMoved = true;
        	}else {
        		if (vws.moveDocFolder(doc, session.room, Sdss, Ddss)) isMoved = true;
        	}
        	if(isMoved)
            {
                addATRecord(vws, session.room, sid, doc.getId(), 
                                        VWATEvents.AT_OBJECT_DOC,
                                        VWATEvents.AT_DOC_FILEMOVE, 
                    "Document file moved from "+Sdss.getName()+ ":" + 
                    Sdss.getAddress() + " to " + Ddss.getName() + ":" + 
                                                             Ddss.getAddress());
                Node node = new Node(doc.getId());
                Node nodeObj= vws.getNodePropertiesByNodeID(session.room, sid, node);
                node.set(nodeObj);
                
                String docName = node.getName();
                
                String message = "Document '"+docName+"' moved from '"+Sdss.getAddress()+"-"+Sdss.getPath()+ "' to '"+Ddss.getAddress()+"-"+ Ddss.getPath()+"'";
                setNotificationHistory(sid, 0, 0, VNSConstants.nodeType_Storage, VNSConstants.notify_Storage_LocationMov, session.user, message);
                return NoError;
            }
            else
            {
                return documentNotFound;
            }
        }
        catch(RemoteException re)
        {
            return Error;
        }
    }
    public int getDocFolder(int sid, Document doc, String desFolder)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocFolder::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        String storageType="";
        String azureUserName="";
        String azurePassword="";
        ServerSchema dssSchema = new ServerSchema();
        try
        {
            ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
            dssSchema.set(ss);
        	Vector storageVect=new Vector();
        	getAzureStorageCredentials(sid,doc.getId(),storageVect);
           	if(storageVect!=null&&storageVect.size()>0){
           	String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
           	storageType=storageArray[7];
           	azureUserName=storageArray[8];
			azurePassword=storageArray[9];
           	VWClient.printToConsole("storageType in getDocFolder:::::"+storageType);
           	}
        }
        catch(RemoteException re)
        {
            connectedVWSs.remove(session.server);
            return Error;
        }
        boolean insideIdFolder = true;
        if (desFolder != null)
        {
            mySchema.path = desFolder;
            insideIdFolder = false;
        }
        else
            mySchema.path = session.cacheFolder; 
        
        VWClient.printToConsole("schema path is getDocFolder::::::"+mySchema.path);
        //DSS ds = (DSS) Util.getServer(dssSchema);
        //if (ds == null) return dssInactive;
        //mySchema.path = desFolder; 
        	boolean gotDocFile =false;
        	VWDoc vwdoc = null;
        	if (isDSSUsed()){
               // try{
                	
               
               
               	if(storageType.equals("On Premises")){
               	try{
        		DSS ds = (DSS) Util.getServer(dssSchema);
                if (ds == null) return dssInactive;
        		ArrayList list = ds.getRMIDocList(dssSchema.path);
                /*if (ds.getDocFolder(doc, session.room, dssSchema.path, mySchema)) 
        		VWDoc vwdoc = ds.getRMIDocFolder(doc, session.room, dssSchema.path, mySchema);
	            doc.setVWDoc(vwdoc);
	            vwdoc.writeContents();
	            while( vwdoc.hasMoreChunks())
                {	            		            
	            	vwdoc.writeContents();
                                    
                }*/        		
        		int docId = doc.getId();
        		while(list.size()>0){
        			dssSchema.path = list.get(0).toString();
        			String fileName = new File(list.get(0).toString()).getName();        			
        			doc = new Document(docId);
        			if (!fileName.equals(DOC_CONTAINER))
        			doc.setVersion(fileName);
        			vwdoc = ds.getRMIDoc(doc, session.room, dssSchema.path, mySchema);
        			/*Check Out document is not working if the doc is > 4MB - Fixed
		         	* Issue No 791 implemented here also
		         	* C.Shanmugavalli      28 Feb 2007*/
	            	doc.setVWDoc(vwdoc);           	
	            	vwdoc.writeContents();   
	            	while(vwdoc.getBytesWritten()<vwdoc.getSize()){
	                	vwdoc = ds.getNextRMIDoc(dssSchema, doc);
	                	doc.setVWDoc(vwdoc);              	
	                    vwdoc.writeContents();
	                }
	            	
	            	/*End*/
	            	list.remove(0);
        		}
        		ds.writeLogMsg(mySchema, doc,session.room);
               	}catch(Exception e){
               		
               	}
               	}else if(storageType.equals("Azure Storage")){
               		VWClient.printToConsole("Inside azure 12323");
               		CloudStorageAccount storageAccount=null;
            		CloudFileClient fileClient =null;
            		CloudFile cloudFile=null;
            		CloudFileShare share=null;
            		CloudFileDirectory rootDir=null;
            		CloudFileDirectory sampleDir =null;
            		try{
               		VWClient.printToConsole("schema path is ::::::"+mySchema.path);
               		VWClient.printToConsole("dssSchema.path:::::"+dssSchema.path);
               		String azureDSSLocation=dssSchema.path;
               		String localStoragePath=mySchema.path;
               		String accountName="AccountName="+azureUserName+";";
    				String password="AccountKey="+azurePassword;
               		String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
            		System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
            		storageAccount = CloudStorageAccount.parse(storageConnectionString1);
            		fileClient = storageAccount.createCloudFileClient();
    				String pattern = Pattern.quote(System.getProperty("file.separator"));
               		String dbpath[]=azureDSSLocation.split(pattern);
               		//VWClient.printToConsole("dbpath. length -1::::"+(dbpath.length-1));
               		for(int i=0;i<dbpath.length;i++){
						if(i==2)
						{
							share = fileClient.getShareReference(dbpath[i]);
						}
						else if(i==3)
						{
							sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
							rootDir = sampleDir;
							sampleDir = null;
						}
						else if(i>=4)
						{
							if ((i+1) != dbpath.length) {
								sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
								if(!sampleDir.exists()){
									sampleDir.create();
								}
								rootDir = sampleDir;
								sampleDir = null;
							}
							else if ((i+1) == dbpath.length) {
								sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
								if(!sampleDir.exists()){
									sampleDir.create();
								}
								rootDir = sampleDir;
								sampleDir = null;
							}
						}

					}
					sampleDir = rootDir;
					VWClient.printToConsole("Sample directory uri in getDocFolder:::"+sampleDir.getUri());
					if(sampleDir.exists()){
						VWClient.printToConsole("Before getDocFolder");
						File localFile=new File(localStoragePath);
						if(!localFile.exists())
							localFile.mkdir();
						for ( ListFileItem fileItem1 : sampleDir.listFilesAndDirectories() ) {
							VWClient.printToConsole("fileItem1 in getDocFolder version:::::::::::"+fileItem1.getUri());
							String filesURI=fileItem1.getUri().toString();
							String fileName=filesURI.substring(filesURI.lastIndexOf("/")+1, filesURI.length());
							VWClient.printToConsole("fileName in getDocFolder version::::"+fileName);
							cloudFile=sampleDir.getFileReference(fileName);
							
							cloudFile.downloadToFile(localStoragePath+"\\"+fileName);
							
						}
					}
                }catch(Exception re)
                {
                	VWClient.printToConsole("Exception while downlaoding from cloud::::"+re.getMessage());
                    return Error;
                }
               	}
            		
        }else{
	        	try{
		        	ArrayList list = vws.getRMIDocList(dssSchema, dssSchema.path);
		       		vwdoc = null;
		       		int docId = doc.getId();
		    		while(list!=null && list.size()>0){
		    			dssSchema.path = list.get(0).toString();
		    			/* Issue/Purpose: Download failed for all the version for the corresponding document. 
		    			 * New document object is created for each version and version set. Issue fixed  
		    			 * Pandiyaraj.M & C.Shanmugavalli    1 March 2007*/
	        			String fileName = new File(list.get(0).toString()).getName();        			
	        			doc = new Document(docId);
	        			if (!fileName.equals(DOC_CONTAINER))
	        			doc.setVersion(fileName);
	        			/*End*/
		    			vwdoc = vws.getRMIDoc(doc, session.room, dssSchema.path, mySchema,dssSchema);
		    			//vwdoc.writeContents();
		         		/*Check Out document is not working if the doc is > 4MB - Fixed
		         		 * Issue No 791 implemented here also
		         		 * C.Shanmugavalli      28 Feb 2007*/
		            	doc.setVWDoc(vwdoc);              	
		                vwdoc.writeContents();
		                while(vwdoc.getBytesWritten()<vwdoc.getSize()){
		                	vwdoc = vws.getNextRMIDoc(doc, session.room, dssSchema.path, dssSchema, mySchema, insideIdFolder);
		                	doc.setVWDoc(vwdoc);              	
		                    vwdoc.writeContents();
		                }
		                vws.writeLogMsg(dssSchema, doc,session.room);
		            	/*End*/	
		                list.remove(0);		                
		    		}
	        }catch(RemoteException re){return Error;}
        }
        try{        	
        	if (vwdoc != null){
            	gotDocFile = vwdoc.getIntegrity();
            	if(gotDocFile)
            		return NoError;
            	else
            		return documentNotFound;
        	}else
                return documentNotFound;
        }catch(Exception re){return Error;}
    }
    /**
     *CV10 Enahcement Backup with Room level encryption	during backup
     * @param sid
     * @param doc
     * @param desFolder
     * @param withUnencryptedPages
     * @return
     */
    public int getDocFolderBackup(int sid, Document doc, String desFolder,boolean withUnencryptedPages)
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getDocFolderBackupRestore::"+sid);
    	if(session==null) return invalidSessionId;
    	if (doc == null || doc.getId() <= 0) return invalidParameter;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	CloudStorageAccount storageAccount=null;
		CloudFileClient fileClient =null;
		CloudFile cloudFile=null;
		CloudFileShare share=null;
		CloudFileDirectory rootDir=null;
		CloudFileDirectory sampleDir =null;
		String dbStoragePath="";
		String azureUserName="";
		String azurePassword="";
		String storageType="";
		Vector storageVect=new Vector();
		getAzureStorageCredentials(sid,doc.getId(),storageVect);
		String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
		if(storageArray[7].equals("On Premises")){

			ServerSchema dssSchema = new ServerSchema();
			try
			{
				ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
				dssSchema.set(ss);
			}
			catch(RemoteException re)
			{
				connectedVWSs.remove(session.server);
				return Error;
			}
			boolean insideIdFolder = true;
			if (desFolder != null)
			{
				mySchema.path = desFolder;
				insideIdFolder = false;
			}
			else
				mySchema.path = session.cacheFolder; 
			boolean gotDocFile =false;
			VWDoc vwdoc = null;

			if (isDSSUsed()){
				try{
					DSS ds = (DSS) Util.getServer(dssSchema);
					if (ds == null) return dssInactive;
					ArrayList list = ds.getRMIDocList(dssSchema.path);
					VWClient.printToConsole("list:::::::::::"+list);
					String key=vws.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
					DSSCipher cipher = new DSSCipher(key);
					int docId = doc.getId();
					while(list.size()>0){
						dssSchema.path = list.get(0).toString();
						VWClient.printToConsole("dssSchema.path::::::::::"+dssSchema.path);
						String fileName = new File(list.get(0).toString()).getName();        			
						doc = new Document(docId);
						if (!fileName.equals(DOC_CONTAINER))
							doc.setVersion(fileName);
						vwdoc = ds.getRMIDoc(doc, session.room, dssSchema.path, mySchema);
						/*Check Out document is not working if the doc is > 4MB - Fixed
						 * Issue No 791 implemented here also
						 * C.Shanmugavalli      28 Feb 2007*/

						doc.setVWDoc(vwdoc);           	
						VWClient.printToConsole("vwdoc.getsrcfile:::"+vwdoc.getSrcFile());
						VWClient.printToConsole("vwdoc.getdstfile:::"+vwdoc.getDstFile());
						vwdoc.writeContents();   
						while(vwdoc.getBytesWritten()<vwdoc.getSize()){
							vwdoc = ds.getNextRMIDoc(dssSchema, doc);
							doc.setVWDoc(vwdoc);              	
							vwdoc.writeContents();
						}

						/*End*/
						list.remove(0);
					}
					ds.writeLogMsg(mySchema, doc,session.room);
					if ((cipher != null)&&(!key.equals("")))  
					{
						if(withUnencryptedPages==false){
							cipher.encryptFile(vwdoc.getDstFile());
						}
					}
				}catch(RemoteException re)
				{
					return Error;
				}
			}else{
				VWClient.printToConsole("Inside else dss used");
				try{
					ArrayList list = vws.getRMIDocList(dssSchema, dssSchema.path);
					VWClient.printToConsole("inside else list :::::"+list);
					vwdoc = null;
					String key=vws.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
					DSSCipher cipher = new DSSCipher(key);
					int docId = doc.getId();
					while(list!=null && list.size()>0){
						dssSchema.path = list.get(0).toString();
						VWClient.printToConsole("dssSchema.path:::::::::"+dssSchema.path);
						/* Issue/Purpose: Download failed for all the version for the corresponding document. 
						 * New document object is created for each version and version set. Issue fixed  
						 * Pandiyaraj.M & C.Shanmugavalli    1 March 2007*/
						String fileName = new File(list.get(0).toString()).getName();        			
						doc = new Document(docId);
						if (!fileName.equals(DOC_CONTAINER))
							doc.setVersion(fileName);
						/*End*/
						vwdoc = vws.getRMIDoc(doc, session.room, dssSchema.path, mySchema,dssSchema);
						//vwdoc.writeContents();
						/*Check Out document is not working if the doc is > 4MB - Fixed
						 * Issue No 791 implemented here also
						 * C.Shanmugavalli      28 Feb 2007*/
						doc.setVWDoc(vwdoc);              	
						vwdoc.writeContents();
						while(vwdoc.getBytesWritten()<vwdoc.getSize()){
							vwdoc = vws.getNextRMIDoc(doc, session.room, dssSchema.path, dssSchema, mySchema, insideIdFolder);
							doc.setVWDoc(vwdoc);              	
							vwdoc.writeContents();
						}
						vws.writeLogMsg(dssSchema, doc,session.room);
						/*End*/	
						list.remove(0);		  
						if ((cipher != null)&&(!key.equals("")))  
						{
							if(withUnencryptedPages==false){
								cipher.encryptFile(vwdoc.getDstFile());
							}
						}
					}
				}catch(RemoteException re){return Error;}
			}
			try{        	
				if (vwdoc != null){
					gotDocFile = vwdoc.getIntegrity();
					if(gotDocFile)
						return NoError;
					else
						return documentNotFound;
				}else
					return documentNotFound;
			}catch(Exception re){return Error;}
		}
		else if(storageArray[7].equals("Azure Storage")){
			ServerSchema dssSchema = new ServerSchema();
			try
			{
				ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
				dssSchema.set(ss);
			}
			catch(RemoteException re)
			{
				connectedVWSs.remove(session.server);
				return Error;
			}

			String azureSchemaPath=dssSchema.path;
			String azureBackupPath=desFolder;
			try{
				azureUserName=storageArray[8];
				azurePassword=storageArray[9];
				String accountName="AccountName="+azureUserName+";";
				String password="AccountKey="+azurePassword;
				String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
				System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
				storageAccount = CloudStorageAccount.parse(storageConnectionString1);
				dbStoragePath=azureSchemaPath.substring(0, azureSchemaPath.length()-1);
				VWClient.printToConsole("dbStoragePath:::::::::::"+dbStoragePath);
				File dstFile=new File(azureBackupPath);
				VWClient.printToConsole("dstFile::::::"+dstFile.getAbsolutePath());
				fileClient = storageAccount.createCloudFileClient();
				String pattern = Pattern.quote(System.getProperty("file.separator"));
				String dbpath[]=dbStoragePath.split(pattern);

				for(int i=0;i<dbpath.length;i++){
					VWClient.printToConsole("value of i------"+i);
					if(i==2)
					{
						VWClient.printToConsole("dbpath[i] inside 1 equal to 2-----"+dbpath[i]);
						share = fileClient.getShareReference(dbpath[i]);
						VWClient.printToConsole("After share creation");
					}
					else if(i==3)
					{
						VWClient.printToConsole("dbpath[i] inside i equal to 3"+dbpath[i]);
						sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
						rootDir = sampleDir;
						sampleDir = null;
					}
					else if(i>=4)
					{
						VWClient.printToConsole("dbpath[i] inside i greater than 4"+dbpath[i]);
						if ((i+1) != dbpath.length) {
							VWClient.printToConsole("Inside if condition of  not equal to dbpath length :::::"+i);
							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
							if(!sampleDir.exists()){
								sampleDir.create();
							}
							rootDir = sampleDir;
							sampleDir = null;
						}
						else if ((i+1) == dbpath.length) {
							VWClient.printToConsole("Inside else if conditionof equal to dbpath lenght ::::::::::"+i);
							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
							if(!sampleDir.exists()){
								sampleDir.create();
							}
							rootDir = sampleDir;
							sampleDir = null;
						}
					}

				}

				sampleDir=rootDir;
				VWClient.printToConsole("getDocFolderBackup Sample directory get uri::::"+sampleDir.getUri());
				for ( ListFileItem fileItem1 : sampleDir.listFilesAndDirectories() ) {
					VWClient.printToConsole("getDocFolderBackup fileItem1:::::::::::"+fileItem1.getUri());
					String filesURI=fileItem1.getUri().toString();
					String fileName=filesURI.substring(filesURI.lastIndexOf("/")+1, filesURI.length());
					VWClient.printToConsole("getDocFolderBackup fileName::::"+fileName);
					CloudFile cloudFile1=sampleDir.getFileReference(fileName);
					VWClient.printToConsole("getDocFolderBackup cloudFile1.getUri()"+cloudFile1.getUri());
					File parentFile=new File(azureBackupPath);
					if(!parentFile.exists())
						parentFile.mkdirs();
					cloudFile1.downloadToFile(azureBackupPath+"\\"+fileName);
				}
			}

			catch(Exception e){
				VWClient.printToConsole("Exception while getting backup from azure storage::::::"+e.getMessage());
			}
		}
		return NoError;
    }
    /**
     * CV10 Enahcement Backup with Room level encryption during backup 		
     * @param sid
     * @param doc
     * @param destination
     * @param withUnencryptedPages
     * @param cover
     * @param frameCount
     * @return
     */
    public int getDocFileBackup(int sid, Document doc, String destination,boolean withUnencryptedPages,boolean cover,int frameCount)
    {
    	if (doc.getId() <= 0) return invalidParameter;

    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getDocFile::"+sid);
    	if(session==null) return invalidSessionId;

    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	ServerSchema dssSchema = new ServerSchema();

    	try
    	{
    		ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, 
    				dssSchema);

    		if (ss != null && !ss.path.equals(""))
    		{
    			dssSchema.set(ss);
    		}
    		else
    		{
    			if (!doc.getVersion().equals(""))
    				return versionNotFound;
    			else
    				return Error;
    		}
    	}
    	catch(RemoteException re)
    	{
    		connectedVWSs.remove(session.server);
    		return Error;
    	}
    	//3 Direct DSS call changed - Valli
    	//DSS ds = (DSS) Util.getServer(dssSchema);
    	//if (ds == null) return dssInactive;
    	boolean insideIdFolder = true;
    	if (destination != null)
    	{
    		mySchema.path = destination;
    		insideIdFolder = false;
    	}
    	else
    		mySchema.path = session.cacheFolder;
    	try
    	{
    		boolean gotDocFile = false;
    		Vector versionList =  new Vector();
    		int ret  = getDocumentVers( sid, doc, versionList);
    		String key=vws.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
    		DSSCipher cipher = new DSSCipher(key);
    		if (isDSSUsed())//(isServerProxyEnabled(session.server) || type == 1 || type == 2)
    		{
    			DSS ds = (DSS) Util.getServer(dssSchema);
    			if (ds == null) return dssInactive;
    			VWDoc vwdoc = ds.getRMIDoc(doc, session.room, dssSchema.path, mySchema, insideIdFolder, versionList);
    			if (vwdoc == null) return documentHasNoPage;
    			doc.setVWDoc(vwdoc);    
    			vwdoc.writeContents();  
    			while(vwdoc.getBytesWritten()<vwdoc.getSize()){
    				vwdoc = ds.getNextRMIDoc(dssSchema, doc);
    				doc.setVWDoc(vwdoc);              	
    				vwdoc.writeContents();
    			}

    			mySchema.setRoom(session.room);
    			ds.writeLogMsg(mySchema, doc,session.room);
    			gotDocFile = doc.getVWDoc().getIntegrity();

    			if ((cipher != null)&&(!key.equals("")))  
    			{
    				if(withUnencryptedPages==false){
    					cipher.encryptFile(vwdoc.getDstFile());
    				}
    			}
    		}
    		else
    		{
    			VWDoc vwdoc = vws.getRMIDoc(doc, session.room, dssSchema.path, dssSchema, mySchema, insideIdFolder,versionList);            	
    			if (vwdoc == null){
    				if ( vws.getActiveError() == dssInactive) return dssInactive;
    				return documentHasNoPage;
    			}
    			doc.setVWDoc(vwdoc);              	
    			vwdoc.writeContents();

    			while(vwdoc.getBytesWritten()<vwdoc.getSize()){
    				vwdoc = vws.getNextRMIDoc(doc, session.room, dssSchema.path, dssSchema, mySchema, insideIdFolder);
    				doc.setVWDoc(vwdoc);              	
    				vwdoc.writeContents();
    			}
    			vws.writeLogMsg(dssSchema, doc,session.room);

    			gotDocFile = vwdoc.getIntegrity();
    			if ((cipher != null)&&(!key.equals("")))  
    			{
    				if(withUnencryptedPages==false){
    					cipher.encryptFile(vwdoc.getDstFile());
    				}
    			}
    		}

    		if (gotDocFile)
    		{
    			addATRecord(vws, session.room, sid, doc.getId(), 
    					VWATEvents.AT_OBJECT_DOC,
    					VWATEvents.AT_DOC_OPEN, 
    					VWATEvents.AT_DOC_FILEOPEN_DESC);
    			if(doc!=null){
    				int pageCount = (frameCount>0)?frameCount:doc.getPageCount();
    				if (cover) getDocCoverPage(sid, doc, pageCount);
    			}
    			return NoError;
    		}
    		else
    			return documentNotFound;
    	}
    	catch(RemoteException re)
    	{
    		return Error;
    	}
    }
    public int deleteDocFile(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteDocFile::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        
        int docid = doc.getId();
        if (docid <= 0) return Error;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        ServerSchema dssSchema = new ServerSchema();
        try
        {
            ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
            dssSchema.set(ss);
        }
        catch(RemoteException re)
        {
            //connection lost
            connectedVWSs.remove(session.server);
            return Error;
        }
        //6 Direct DSS call changed - Valli
        //DSS ds = (DSS) Util.getServer(dssSchema);
        //if (ds == null) return dssInactive;
        doc.getVWDoc().setDstFile(dssSchema.path);
        try
        {
        	if (isDSSUsed()) {
	            DSS ds = (DSS) Util.getServer(dssSchema);
	            if (ds == null) return dssInactive;         	
	            if (ds.deleteDocument(doc)) 
	                return NoError;
	            else
	                return documentNotFound;
        	}else {
        	if (vws.deleteDocument(dssSchema,doc))
            //if (ds.deleteDocument(doc)) 
                return NoError;
            else
                return documentNotFound;
        }
        }
        catch(RemoteException re)
        {
            return Error;
        }
    }
    /*CV2019 merges from SIDBI line------------------------------------------------***/
    public int isAllWebExist(int sid, Document doc)
    {
    	int retFlag=0;
        Session session = getSession(sid);
        System.out.println("sessionId in isAllWebExist::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        ServerSchema dssSchema = new ServerSchema();
        try
        {
            ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
            dssSchema.set(ss);
        }
        catch(RemoteException re)
        {
            connectedVWSs.remove(session.server);
            return Error;
        }
        //7 Direct DSS call changed - Valli
        //DSS ds = (DSS) Util.getServer(dssSchema);
        //if (ds == null) return dssInactive;
        doc.getVWDoc().setDstFile(dssSchema.path);
        try
        {
        	boolean status = false;
        	if (isDSSUsed()) {	    		
        		DSS ds = (DSS) Util.getServer(dssSchema);
        		if (ds == null) return dssInactive;        		            
        		status = ds.isAllWebExist(doc);
        		System.out.println("status from dss "+status);
        		if(status==true){
        			retFlag= 1;
        		}else {
        			retFlag= 0;
        		}

        	}else{
        		status= vws.isAllWebExist(dssSchema, doc);
        		if(status==true){
        			retFlag= 1;
        		}else {
        			retFlag= 0;
        		}
        	}
        }catch(RemoteException re)
        {
            return Error;
        }
		return retFlag;
    }
    /*------------------------End of SIDBI merges--------------------------------------*/
    
    public int getLastModifyDate(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getLastModifyDate::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        ServerSchema dssSchema = new ServerSchema();
        try
        {
            ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
            dssSchema.set(ss);
        }
        catch(RemoteException re)
        {
            connectedVWSs.remove(session.server);
            return Error;
        }
        //7 Direct DSS call changed - Valli
        //DSS ds = (DSS) Util.getServer(dssSchema);
        //if (ds == null) return dssInactive;
        doc.getVWDoc().setDstFile(dssSchema.path);
        try
        {
        	long date = 0;
        	if (isDSSUsed()) {	    		
	            DSS ds = (DSS) Util.getServer(dssSchema);
	            if (ds == null) return dssInactive;        		            
	            date = ds.getLastModifyDate(doc);
        	}else{
        		date = vws.getLastModifyDate(dssSchema, doc);
        	}
            doc.getVWDoc().setModifyDate(date);
            return NoError;
        }
        catch(RemoteException re)
        {
            return Error;
        }
    }
    public int getDocumentSize(int sid, Document doc)
    {
         return getDocumentSize(sid, doc, false);
    }
/*
Enhancement No / Purpose:  <63/While Downloading display the downloaded size>
Created by: <Pandiya Raj.M>
Date: <03 Aug 2006>
*/
    public int getDocumentSize(int sid, Document doc, boolean withVR)
    {
         return getDocumentSize(sid, doc, withVR,"");
    }
    public int getDocumentSize(int sid, Document doc, boolean withVR,String Version)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentSize::"+sid);
        if(session==null) return invalidSessionId;
        doc.setRoomName(session.room);
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        Vector storageVect=new Vector();
		getAzureStorageCredentials(sid,doc.getId(),storageVect);
		String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
        ServerSchema dssSchema = new ServerSchema();
        try
        {
            ServerSchema ss = vws.getDSSforDoc
                                            (session.room, sid, doc, dssSchema);
            if(ss.getId() == Error)
            	return Error;
            dssSchema.set(ss);
        }
        catch(RemoteException re)
        {
            connectedVWSs.remove(session.server);
            return Error;
        }
        //8 Direct DSS call changed - Valli
        //DSS ds = (DSS) Util.getServer(dssSchema);
        //if (ds == null) return dssInactive;
        doc.getVWDoc().setDstFile(dssSchema.path);
        try
        {
        	// Change the signature to retrieve the document size
        	long size = 0;
        	if (isDSSUsed()) {
	            DSS ds = (DSS) Util.getServer(dssSchema);
	            if (ds == null) return dssInactive;        	        	
	            size = ds.getDocSize(doc, withVR, Version);
	            //--------------------------------Srikanth on 23 Sep 2015 CV83B3 For Zerokb issue fix in Properties General Tab.
	            if (size ==0) {
	            	File file = null;
	            	file = new File(session.cacheFolder);
	            	if(storageArray[7].equals("Azure Storage")){
	            		if(doc.getVersion().equals("")){
	            			File orginalFile=new File(file.getPath() + File.separator + String.valueOf(doc.getId())  + File.separator + "all.zip");
	            			size=orginalFile.length();
	            			VWClient.printToConsole("File path in getDocumentSize without version:::::"+file.getAbsolutePath()+"Size::::"+size);
	            			doc.getVWDoc().setSize(size);
	            		}else if(doc.getVersion().length()>0){
	            			File verFile=new File(file.getPath() + File.separator + String.valueOf(doc.getId())  + File.separator +doc.getVersion()+File.separator + "all.zip");
	            			if(verFile.exists()){
	            				size=verFile.length();
	            				VWClient.printToConsole("File path in getDocumentSize with version1:::::"+file.getAbsolutePath()+"Size::::"+size);
	            			}
	            			else {
	            				File curVerFile=new File(file.getPath() + File.separator + String.valueOf(doc.getId())  + File.separator + "all.zip");
	            				size=curVerFile.length();
	            				VWClient.printToConsole("File path in getDocumentSize with version2:::::"+curVerFile.getAbsolutePath()+"Size::::"+size);
	            			}
	            			
	            			doc.getVWDoc().setSize(size);
	            		}
	            	}
	            	else if(storageArray[7].equals("On Premises")){
	            	if (!file.getPath().endsWith(doc.getVersion())){
	            		file = new File(file.getPath() + File.separator + String.valueOf(doc.getId()) + File.separator + Version + File.separator + "all.zip");
	            		VWClient.printToConsole("FullFilePath before getting the size:::"+file.getAbsolutePath());
	            		size = file.length();
	            		VWClient.printToConsole("getDocumentSize size: " + size);
	            	}
	            	}
	            }
	            
	            //--------------------------------Srikanth on 23 Sep 2015 CV83B3 For Zerokb issue fix in Properties General Tab.
        	}else{
        		size = vws.getDocSize(dssSchema, doc, withVR, Version);        	
        	}
            //long size = ds.getDocSize(doc, withVR,Version);
            doc.getVWDoc().setSize(size);
            return NoError;
        }
        catch(RemoteException re)
        {
            return Error;
        }
        catch(Exception exception){
        	return Error;
        }
    }
        
    /** Returns the number of pages for the specified Document.
     * @param sid Session id obtained by login
     * @param doc The document for which to return he number of pages
     * @return The number of pages
     */
    public int getPageCount(int sid, Document doc)
    {
    	VWClient.printToConsole("getPageCount......."+doc);
    	CloudStorageAccount storageAccount=null;
    	CloudFileClient fileClient =null;
    	CloudFile cloudFile=null;
    	CloudFileShare share=null;
    	CloudFileDirectory rootDir=null;
    	CloudFileDirectory sampleDir =null;
    	String dbStoragePath="";
    	String azureUserName="";
    	String azurePassword="";
    	String storageType="";
    	String cacheFolderPath="";
    	int pageCount=0;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getPageCount::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        try{
    		Vector storageVect=new Vector();
    		getAzureStorageCredentials(sid,doc.getId(),storageVect);
    		if(storageVect!=null&&storageVect.size()>0){
    			String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
    			dbStoragePath=storageArray[5];
    			storageType=storageArray[7];
    			azureUserName=storageArray[8];
    			azurePassword=storageArray[9];
    			cacheFolderPath=session.cacheFolder;
    			VWClient.printToConsole("StoragType in getPageCount:::::"+storageType);
    		}
    	}catch(Exception e){
    		VWClient.printToConsole("Exception while getting storage type in page count");
    	}
        if(storageType.equals("On Premises")){
        	if (doc.getVersion().equals(""))
        	{
        		int pCount = -1;
        		Vector docs = new Vector();
        		getDocumentInfo(sid, doc, docs);
        		if (docs.size() > 0)
        			pCount = ((Document) docs.elementAt(0)).getPageCount();
        		if (pCount >= 0) return pCount;
        	}
        	VWS vws = getServer(session.server);
        	if (vws == null) return ServerNotFound;

        	ServerSchema dssSchema = new ServerSchema();
        	try
        	{
        		ServerSchema ss = vws.getDSSforDoc
        				(session.room, sid, doc, dssSchema);
        		dssSchema.set(ss);
        	}
        	catch(RemoteException re)
        	{
        		//connection lost
        		VWClient.printToConsole("Exception in getpagecount 1:::"+re.getMessage());
        		connectedVWSs.remove(session.server);
        		return Error;
        	}
        	//9 Direct DSS call changed - Valli
        	//DSS ds = (DSS) Util.getServer(dssSchema);
        	//if (ds == null) return dssInactive;
        	doc.getVWDoc().setDstFile(dssSchema.path);
        	try
        	{
        		Vector versionList = new Vector();
        		if (isDSSUsed()) {
        			DSS ds = (DSS) Util.getServer(dssSchema);
        			if (ds == null) return dssInactive;	            
        			int ret  = getDocumentVers( sid, doc, versionList);
        			return ds.getPageCount(doc, session.room, versionList);
        		}else{
        			int ret  = getDocumentVers( sid, doc, versionList);
        			return vws.getPageCount(dssSchema, doc, session.room, versionList);
        		}
        		//return ds.getPageCount(doc, session.room); 
        	}
        	catch(RemoteException re)
        	{
        		VWClient.printToConsole("Exception in getpagecount :::"+re.getMessage());
        		return Error;
        	}
        }else if (storageType.equalsIgnoreCase("Azure Storage")){
        	try{
        		String accountName="AccountName="+azureUserName+";";
        		String password="AccountKey="+azurePassword;
        		File dstFile=new File(cacheFolderPath);
        		String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
        		System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
        		storageAccount = CloudStorageAccount.parse(storageConnectionString1);
        		fileClient = storageAccount.createCloudFileClient();
        		String pattern = Pattern.quote(System.getProperty("file.separator"));
        		String dbpath[]=dbStoragePath.split(pattern);

        		for(int i=0;i<dbpath.length;i++){
        			if(i==2)
        			{
        				share = fileClient.getShareReference(dbpath[i]);
        			}
        			else if(i==3)
        			{
        				sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
        				rootDir = sampleDir;
        				sampleDir = null;
        			}
        			else if(i>=4)
        			{
        				if ((i+1) != dbpath.length) {
        					sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        					if(!sampleDir.exists()){
        						sampleDir.create();
        					}
        					rootDir = sampleDir;
        					sampleDir = null;
        				}
        				else if ((i+1) == dbpath.length) {
        					sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        					if(!sampleDir.exists()){
        						sampleDir.create();
        					}
        					rootDir = sampleDir;
        					sampleDir = null;
        				}
        			}

        		}

        		sampleDir = rootDir.getDirectoryReference(String.valueOf(doc.getId()));
        		VWClient.printToConsole("GetPageCount sampleDir geturi::::"+sampleDir.getUri());
        		String version="";
        		if(doc.getVersion().length()>0){
        			version=doc.getVersion();
        			cloudFile = sampleDir.getFileReference(version);
        		}else {
        			cloudFile = sampleDir.getFileReference("all.zip");
        		}

        		File parentFile=new File(cacheFolderPath);
        		if(!parentFile.exists()){
        			parentFile.mkdir();
        		}
        		if(doc.getVersion().length()>0){
        			String destinationFile=parentFile.getAbsolutePath()+"\\"+String.valueOf(doc.getId())+"\\"+doc.getVersion();
        			VWClient.printToConsole("destinationFile::::"+destinationFile);
        			File destinationFolder=new File(parentFile.getAbsolutePath());
        			if(!destinationFolder.exists()){
        				destinationFolder.mkdir();
        			}
        			File dstFolder=new File(destinationFile);
        			if(!dstFolder.exists())
        				dstFolder.mkdir();
        			File tempFile1=new File(destinationFile+"\\"+doc.getVersion());
        			String newFile=destinationFile+"\\"+"all.zip";
    				File newName = new File(newFile);
        			if(!tempFile1.exists()){
        				cloudFile.downloadToFile(destinationFile+"\\"+doc.getVersion());

        				File oldName = new File(destinationFile+"\\"+doc.getVersion());
        				
        				if(oldName.renameTo(newName)) {
        					VWClient.printToConsole("File renamed sucessfully");
        					VWS vws1 = getServer(session.server);
        					if (vws1 == null) return ServerNotFound;
        					String key=vws1.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
        					DSSCipher cipher = new DSSCipher(key);
        					if ((cipher != null)&&(!key.equals("")))  
        					{
        						VWClient.printToConsole("Before decrypting azure newfile in azure getdocfile");
        						cipher.decryptFile(newFile);
        						VWClient.printToConsole("After decrypting new  file in azure getdocfile");
        					}
        				} else {
        					VWClient.printToConsole("Error while renaming file");
        				}
        			}
        				ZipFile zfile = new ZipFile(newName);
        				java.util.Enumeration e = zfile.entries();
        				while (e.hasMoreElements())
        				{
        					if ( ((ZipEntry) e.nextElement()).getName().endsWith(".vw") ) 
        						pageCount++;
        				}
        				zfile.close();
        				VWClient.printToConsole("Getting page count inside version docs::::::::"+pageCount);
        			
        		}else{
        			if(cloudFile.exists()){
        				VWClient.printToConsole("Downloading file from azure without in getpagecount decrypting");
        				File downloadedFile=new File(cacheFolderPath+"\\"+String.valueOf(doc.getId()));
        				if(!downloadedFile.exists())
        					downloadedFile.mkdir();
        				File tempFile=new File(cacheFolderPath+"\\"+String.valueOf(doc.getId())+"\\"+"all.zip");
        				if(!tempFile.exists()){
        					cloudFile.downloadToFile(cacheFolderPath+"\\"+String.valueOf(doc.getId())+"\\"+"all.zip");

        					VWS vws1 = getServer(session.server);
        					if (vws1 == null) return ServerNotFound;
        					String key=vws1.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
        					DSSCipher cipher = new DSSCipher(key);
        					if ((cipher != null)&&(!key.equals("")))  
        					{
        						cipher.decryptFile(cacheFolderPath+"\\"+String.valueOf(doc.getId())+"\\"+"all.zip");
        						VWClient.printToConsole("After decrypting file in azure getpagecount");
        					}
        				}
        					ZipFile zfile = new ZipFile(downloadedFile+"\\"+"all.zip");
        					java.util.Enumeration e = zfile.entries();
        					while (e.hasMoreElements())
        					{
        						if ( ((ZipEntry) e.nextElement()).getName().endsWith(".vw") ) pageCount++;
        					}
        					zfile.close();
        					VWClient.printToConsole("Getting page count without version docs::::::::"+pageCount);
        				
        			}else{
        				return pageCount;
        			}
        		}
        		VWClient.printToConsole("Document with "+" "+ doc.getId()+" "+"downloaded from azure storage to get pagecount");

        	}catch(Exception e){
    			VWClient.printToConsole("Exception while downloading the file from azure:::::"+e.getMessage());
    		}
        }
		return pageCount;
    }
    
    /** Sets the comment for a Document version
     * @param sid Session id obtained by login
     * @param doc The document referencing the version for which to set the comment
     * @return 0 for success or negative value error code
     */
    public int  setDocVerComment(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDocVerComment::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0) return invalidParameter; 
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            if(doc.getVRComment() !=null &&  
                        doc.getVRComment().length() > MAX_COMMENT_LENGTH)
              doc.setVRComment
                     (doc.getVRComment().substring(0,MAX_COMMENT_LENGTH));
            return vws.setDocVerComment(session.room, sid, doc.getId(),
                                          doc.getVersion(), doc.getVRComment());  
        }
        catch(RemoteException re)
        {
            return Error;
        }    
    }    
    
    /** Deletes all docuemnt versions
     * @param sid  Session id obtained by login
     * @param doc The document to which versions need to be deleted
     * @return  0 for success or negative value error code
     */
    public int deleteDocVersions(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteDocVersions::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() <= 0) return invalidParameter;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        String storageType="";
        String storagepath="";
        String azureUserName="";
        String azurePassword="";
        ServerSchema dssSchema = new ServerSchema();
        int docid = doc.getId();
        try
        {
            if (vws.deleteDocumentVers(session.room, sid, docid) == NoError)
            {
                ServerSchema ss = vws.getDSSforDoc
                                            (session.room, sid, doc, dssSchema);
                dssSchema.set(ss);
            }
            else
                return Error;
        }
        catch(RemoteException re)
        {
            connectedVWSs.remove(session.server);
            return Error;
        }
        //10 Direct DSS call changed - Valli
        //DSS ds = (DSS) Util.getServer(dssSchema);
        //if (ds == null) return dssInactive;
        doc.getVWDoc().setDstFile(dssSchema.path);
        try
        {
        	if (isDSSUsed()) {

            	Vector storageVect=new Vector();
            	getAzureStorageCredentials(sid,doc.getId(),storageVect);
            	if(storageVect!=null&&storageVect.size()>0){
            		VWClient.printToConsole("storageVect inside setDocActiveVer::::"+storageVect.get(0).toString());
            		String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
            		storagepath=storageArray[5]+"\\"+doc.getId();
            		storageType=storageArray[7];
            		azureUserName=storageArray[8];
        			azurePassword=storageArray[9];
        	
            	}
        		if(storageType.equals("On Premises")){
	            DSS ds = (DSS) Util.getServer(dssSchema);        
	            if (ds.deleteDocumentVersions(doc))  
	            	return NoError;
	            else
	            	return documentNotFound;
        		}else if(storageType.equals("Azure Storage")){

        			CloudStorageAccount storageAccount=null;
        			CloudFileClient fileClient =null;
        			CloudFile cloudFile=null;
        			CloudFile cloudFile1=null;
        			CloudFile cloudFile2=null;
        			CloudFileShare share=null;
        			CloudFileDirectory rootDir=null;
        			CloudFileDirectory sampleDir =null;
        			String dbStoragePath="";
        		
        			try{
        				String accountName="AccountName="+azureUserName+";";
        				String password="AccountKey="+azurePassword;
        				String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
        				System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
        				storageAccount = CloudStorageAccount.parse(storageConnectionString1);
        				fileClient = storageAccount.createCloudFileClient();
        				String pattern = Pattern.quote(System.getProperty("file.separator"));
        				String dbpath[]=storagepath.split(pattern);
        				for(int i=0;i<dbpath.length;i++){
        					if(i==2)
        					{
        						share = fileClient.getShareReference(dbpath[i]);
        					}
        					else if(i==3)
        					{
        						sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
        						rootDir = sampleDir;
        						sampleDir = null;
        					}
        					else if(i>=4)
        					{
        						if ((i+1) != dbpath.length) {
        							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        							if(!sampleDir.exists()){
        								sampleDir.create();
        							}
        							rootDir = sampleDir;
        							sampleDir = null;
        						}
        						else if ((i+1) == dbpath.length) {
        							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        							if(!sampleDir.exists()){
        								sampleDir.create();
        							}
        							rootDir = sampleDir;
        							sampleDir = null;
        						}
        					}

        				}
        				sampleDir = rootDir;
        				for ( ListFileItem fileItem1 : sampleDir.listFilesAndDirectories() ) {
        					VWClient.printToConsole("fileItem1 in delet all version:::::::::::"+fileItem1.getUri());
        					String filesURI=fileItem1.getUri().toString();
        					String fileName=filesURI.substring(filesURI.lastIndexOf("/")+1, filesURI.length());
        					VWClient.printToConsole("fileName in delete all version::::"+fileName);
        					if(!fileName.equals("all.zip"))
        						cloudFile=sampleDir.getFileReference(fileName);
        					if(cloudFile.exists()){
        						cloudFile.delete();
        					}
        				}
        				Vector vers = new Vector();
        				getDocumentVers(sid, doc, vers);
        				if(vers!=null&&vers.size()>0){
        					Document doc1 = (Document) vers.get(0);
        					if(doc1.getVersion().length()>0&&doc1.getVersion()!=null){
        						cloudFile1=sampleDir.getFileReference("all.zip");
        						cloudFile2=sampleDir.getFileReference(doc1.getVersion());
        						if(cloudFile1.exists()){
        							cloudFile2.startCopy(cloudFile1.getUri());
        						}
        					}
        				}
        			}catch(Exception e){
        				VWClient.printToConsole("Exception while delet all versions::::"+e.getMessage());
        				return ViewWiseErrors.azureDelVersionsFailed;
        			}

        		
        		}
        	}else{
            if (vws.deleteDocumentVersions(dssSchema,doc)) 
                return NoError;
            else
                return documentNotFound;
        }
        }
        catch(RemoteException re)
        {
            return Error;
        }
        return NoError;
    }
    
    /** Returns a description string of an error code.
     * @param err The error code
     * @return The description string
     */
    public String getErrorDescription(int err)
    {
    	VWClient.printToConsole("Error code in getErrorDescription method :"+err);
        String desc = "";
        try
        {
            if (VWERROR == null) VWERROR = Class.forName(VWERROR_CLASS);
            desc = (String) VWERROR.getField("E" + err * -1).get(null);
            VWClient.printToConsole("Error desc :"+desc);
        }
        catch (Exception e){}
        return desc;
    }
    public String encryptStr(String str)
    {
        return Util.encryptKey(str);
    }    
    public String decryptStr(String str)
    {
        return Util.decryptKey(str);
    }    
    
    /** Registers a ViewWise Server on the local registry
     * @param name Name of the ViewWise Server to register
     * @param ipAddress IP address of the ViewWise Server to register
     * @param port Port of the ViewWise Server to register
     * @return 0 for success or negative value error code
     */
    public static int addVWS(String name, String ipAddress, int port)
    {
       VWClient.printToConsole("inside addVWS .....");
       ServerSchema server = new ServerSchema();
       server.name = name;
       server.address = ipAddress;
       server.comport = port;
       VWClient.printToConsole("server.name :"+server.name);
       VWClient.printToConsole("server.address :"+server.address);
       VWClient.printToConsole("server.port :"+server.comport);
       VWCPreferences.addVWS(server);
       return NoError;     
    }    
    public static int getVWSs(Vector servers)
    {
      ServerSchema[] sSchemas = VWCPreferences.getVWSs();
      if(sSchemas !=null && sSchemas.length >0)
          for(int i=0; i < sSchemas.length; i++)
          {
            String ret =  sSchemas[i].name + Util.SepChar + 
                          sSchemas[i].address + Util.SepChar + 
                          sSchemas[i].comport;
            servers.add(ret);
          }           
       return NoError;
    }
    
    /** Removes (unregisters) a ViewWise Server
     * @param serverName The server name to remove
     * @return 0 for success or negative value error code
     */
    public static int removeVWS(String serverName)
    {
       VWCPreferences.removeVWS(serverName);
       return NoError;
    } 
    
    /** Sets (registers) the IP address and port of the proxy server.
     * @param proxyAddress The IP address of the proxy server
     * @param port The port of the proxy server
     * @return 0 for success or negative value error code
     */
    public static int setProxy(String proxyAddress, int port)
    {
        VWCPreferences.setProxyHost(proxyAddress);
        VWCPreferences.setProxyPort(port);
        return NoError;
    }
    
    /** Returns the IP address of the proxy server
     * @return The IP address
     */
    public static String getProxyHost()
    {
        return VWCPreferences.getProxyHost();
    }
    
    /** Returns the port number of the proxy server.
     * @return The port number
     */
    public static int getProxyPort()
    {
        return VWCPreferences.getProxyPort();
    }
    
    /** Checks wether the a proxy server is enabled.
     * @return 1 (true), 0 (false)
     */
    public static boolean isProxyEnabled()
    {
        return VWCPreferences.getProxy();
    }  
    
    /** Specifies that VWClient will communicate with the ViewWise Server through the specified proxy server.
     * @param b true to enable communication through proxy
     */
    public static void setProxyEnable(boolean b)
    {
        VWCPreferences.setProxy(b);
    }
/*
Issue No / Purpose:  <09/06/2006 Web Client Sort Navigator>
Created by: <Pandiya Raj.M>
Date: <16 Jun 2006>
The web client calls the following method to set/get the registry value
*/

    public static void setSortNavigator(boolean b)
    {
        VWCPreferences.setSortNavigator(b);
    }
    public static boolean getSortNavigator()
    {
        return VWCPreferences.getSortNavigator();
    }   
    public static boolean isServerProxyEnabled(String server)
    {        
        ServerSchema ss = (ServerSchema) registeredVWSs.get(server);
        if(ss!=null && ss.isProxySet)
        {
                if(ss.proxyHost!=null && !ss.proxyHost.equals(""))
                    return true;
                return false;
        }
        else
        {
                return VWCPreferences.getServerProxy(server);
        }
    } 
    public static void setServerProxyEnable(String server, boolean b)
    {
        VWCPreferences.setServerProxy(server, b);
    }
    public static String getServerProxyHost(String server)
    {
        ServerSchema ss = (ServerSchema) registeredVWSs.get(server);
        if(ss!=null && ss.isProxySet)
        {
                return ss.proxyHost;
        }
        else
        {
                return VWCPreferences.getServerProxyHost(server);
        }
        
    }
    public static void setServerProxyHost(String server, String host)
    {
        VWCPreferences.setServerProxyHost(server, host);
    }
   public int backupDoc(int sid, Document doc, boolean withHTML, boolean withVR, 
   boolean withAT, boolean withReferences, boolean withMiniViewer , 
                                boolean withUnencryptedPages , String backuPath) 
    {
        return backupDoc(sid, doc, withHTML, withVR, 
        withAT, withReferences, withMiniViewer , 
            withUnencryptedPages, false,backuPath);
    }
    public int backupDoc(int sid, Document doc, boolean withHTML, 
            boolean withVR, boolean withAT, boolean withReferences, 
            boolean withMiniViewer , boolean withUnencryptedPages , 
            boolean withPageText, String backuPath) 
    {
        String path=new File(backuPath,"Document").getPath();
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try {
            int retVal= (new VWDocBackup()).backupDocument(sid, this,doc,
            withHTML, withVR, withAT, withReferences, withMiniViewer, 
            withUnencryptedPages, withPageText,path, VWDocBackup.BACKUP_TYPE,"");
            addATRecord(vws, session.room, sid, doc.getId(), 
                    VWATEvents.AT_OBJECT_DOC,
                    VWATEvents.AT_DOC_BACKUP,"Backup to "+path);
            return retVal;
    }
        catch(Exception e)
        {
            return Error;
        }
    }
/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
Add the argument docTypeStatus and indexInfo to get the existing restored document type status
*/

    public int restoreDoc(int sid, String docPath, int parentNodeId, 
        int restoreType, boolean restoreWithVR, boolean restoreReferences,
                boolean restoreWithAT, boolean restoreWithFolders, 
                int restoreSequenceSeed)
    {
        return restoreDoc(sid, docPath, parentNodeId, restoreType, 
            restoreWithVR, restoreReferences, restoreWithAT, restoreWithFolders, 
            restoreSequenceSeed,0,null,null);
    }
// Added By Mallikarjuna to Restore Created and Modifed Dates 
    public int restoreDoc(int sid, String docPath, int parentNodeId, 
            int restoreType, boolean restoreWithVR, boolean restoreReferences,
                    boolean restoreWithAT, boolean restoreWithFolders, 
                    int restoreSequenceSeed,Hashtable docTypeStatus,HashMap indexInfo, boolean restoreCreatedDate){
    	 return restoreDoc(sid, docPath, parentNodeId, restoreType, 
                 restoreWithVR, restoreReferences, restoreWithAT, restoreWithFolders, 
                 restoreSequenceSeed,0,docTypeStatus,indexInfo, restoreCreatedDate);
    }
    public int restoreDoc(int sid, String docPath, int parentNodeId, 
            int restoreType, boolean restoreWithVR, boolean restoreReferences,
                    boolean restoreWithAT, boolean restoreWithFolders, 
                    int restoreSequenceSeed,Hashtable docTypeStatus,HashMap indexInfo)
        {
            return restoreDoc(sid, docPath, parentNodeId, restoreType, 
                restoreWithVR, restoreReferences, restoreWithAT, restoreWithFolders, 
                restoreSequenceSeed,0,docTypeStatus,indexInfo);
    }
 // Added By Mallikarjuna to Restore Created and Modifed Dates       
        public int restoreDoc(int sid, String docPath, int parentNodeId, 
        int restoreType, boolean restoreWithVR, boolean restoreReferences,
                boolean restoreWithAT, boolean restoreWithFolders, 
                int restoreSequenceSeed, int sessionId,Hashtable docTypeStatus,HashMap indexInfo){
            return restoreDoc(sid, docPath, parentNodeId, restoreType, 
                restoreWithVR, restoreReferences, restoreWithAT, restoreWithFolders, 
                restoreSequenceSeed,0,docTypeStatus,indexInfo,false);                	
}
        
    public int restoreDoc(int sid, String docPath, int parentNodeId, 
        int restoreType, boolean restoreWithVR, boolean restoreReferences,
                boolean restoreWithAT, boolean restoreWithFolders, 
                int restoreSequenceSeed, int sessionId,Hashtable docTypeStatus,HashMap indexInfo, boolean restoreCreatedDate)
    {
    	String storageType="";
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in restoreDoc::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector storageVect=new Vector();
        	getAzureStorageCredentials(sid,0,storageVect);
        	String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
        	storageType=storageArray[7];
        	if(storageType.equals("On Premises")){
        		Vector result = new Vector();
        		Vector size = new Vector();
        		getLatestDocId(sid, result);
        		int docId = Integer.parseInt((String) result.get(0));
        		Document maxdoc = new Document(docId);
        		int retValue = checkAvailableSpaceInDSS(sid, maxdoc,"",size);
        		long availableSpace=(Long) size.elementAt(0);
        		if(retValue==dssCriticalError){
        			SendEmailFromDSS(availableSpace,sid,maxdoc);
        			return dssCriticalError;
        		}
        	}
            int retVal = (new VWDocRestore()).restoreDocument(sid, this, docPath,
                parentNodeId, restoreType, restoreWithVR, restoreReferences, 
                restoreWithAT, restoreWithFolders, restoreSequenceSeed,sessionId,1,docTypeStatus,indexInfo, restoreCreatedDate);
            addATRecord(vws, session.room, sid, retVal, 
                    VWATEvents.AT_OBJECT_DOC,
                    VWATEvents.AT_DOC_RESTORE,"Restore from "+docPath);
            return retVal;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getDocumentInfo(int sid, Document doc, Vector docs)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getDocumentInfo(session.room, sid, doc, docs);
            docs.addAll(vectorToDocuments(ret, false));
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public String getClientIP()
    {
        try
        {
            return java.net.InetAddress.getLocalHost().getHostAddress();
        }
        catch(java.net.UnknownHostException e)
         {}
        return "Local";
    }

    public int getNodeAllDocuments(int sid, int nid, Vector docs)
    {
        return getNodeAllDocuments(sid, nid, 0, docs);
    }
    public int getNodeAllDocuments(int sid, int nid, int sessionId, Vector docs)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getNodeAllDocuments::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getNodeAllDocuments(session.room, sid, nid, sessionId, docs);
            docs.addAll(vectorToDocuments(ret, false));
            VWClient.printToConsole("Active Error in getNodeAllDocuments--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Checks out a Document
     * @param sid  Session id obtained by login
     * @param nodes A list of Nodes or Documents to check out
     * @param sName The name of the check out session
     * @param path The path to check out to
     * @param checkOutType if checkOutType = 1 is check out and checkOutType = 2 is copy
     * @param docs
     * @return 0 for success or negative value error code
     * Modified for DSS low disk space alert in checkout
     * and write the size inside checkout folder with sizeinfo.txt
     */
    //CV10 Enchancement:-passsword added for miniwise 
    public int checkOutDoc(int sid, Vector nodes, String sName, String path, 
        int checkOutType, Vector docs, String passsword)
    {
        Vector info=new Vector();
        String infoFilePath="C:\\CheckOutInfo.inf";
        path=new File(path,"Docs_"+sName+Util.getNow(1)).getPath();
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in checkOutDoc::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0,sessionId=0;
        docs.removeAllElements();
        Node node = null;
        info.add("Total Memory 1->"+Runtime.getRuntime().totalMemory());
        info.add("Free Memory 1->"+Runtime.getRuntime().freeMemory());
        info.add("Free Memory 1->"+Runtime.getRuntime().freeMemory());
        long fileCheckOut=0;
        int settingsId = -1;
        String message = "";
        int nodeIdToNotify=-1;  
        
        if(nodes.size() > 0 && nodes.get(0) instanceof Node)
        {
            node = (Node) nodes.get(0);
            nodeIdToNotify = node.getId();
            settingsId = CheckNotificationExist(sid, nodeIdToNotify, VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_Checkout, session.user);
            message = "Checked Out '"+node.getName()+"' Folder";
            getNodeAllDocuments(sid, node.getId(), 1,docs);
            info.add("Free Memory 2->"+Runtime.getRuntime().freeMemory());
        }
        else
        if(nodes.size() > 0 && nodes.get(0) instanceof Document)
        {
            for (int i=0; i < nodes.size(); i++)
            {
                Document doc = (Document) nodes.get(i);
                Vector docVec=new Vector();
                getDocumentInfo(sid, doc, docVec);
                if(docVec!=null && docVec.size()>0) docs.add(docVec.get(0));
                info.add("Free Memory 3." + i + "->"+Runtime.getRuntime().freeMemory());
            }
        }
        else
        {
            return NoDocumentToCheckOut;
        }
        info.add("Free Memory -4>"+Runtime.getRuntime().freeMemory());      
        int count=docs.size();
        if(count==0) return NoDocumentToCheckOut;
        Principal me = new Principal();
        getMyPrincipal(sid, me);
        for(int i=0;i<docs.size();i++)
        {
            Vector name=new Vector();
            Document doc=(Document) docs.get(i);
            doc.setLocation(path);
            doc.setTag("");
        	//if checkOutType = 1 means check out and checkOutType = 2 means copy
            //If the check out is copy type, checked out can be copied out.
            if(doc.getSessionId()>0 && checkOutType ==1)
            {
            	//Below line is added, because it is doing check out document again check out in the folder check out.
                doc.setTag(String.valueOf(alreadyCheckOutDocument));
                count--;
            }
            else
            {
            	//If the check out is copy type no need to check document lock.
            	if(checkOutType ==1){
	                getLockOwner(sid, doc, name);
	                if(name!=null && name.size() > 0) ///return SomeDocLocked;
	                {
	                    doc.setTag(String.valueOf(SomeDocLocked));
	                    count--;
	                }else{
		                /* Added security check for each document, to check 'check out' permission
	 	                * if the document is not locked, then check out permission check is added.
	 	                * Since it will be skipped already by getLockOwner check. */	
 		                Acl acl = new Acl(doc);
 		                getAcl(sid, acl);
 		                Iterator iterator = acl.getEntries().iterator();
 		                if(iterator.hasNext()){
 		                	AclEntry aclEntry = (AclEntry)iterator.next();
 		                	if(aclEntry.canCheckInOut() != 1){
 		                		doc.setTag(String.valueOf(checkOutDenied));
 		                		count--;
 		                	}
 		                }
 	                }
            	}
            }
        }
        info.add("Free Memory 5->"+Runtime.getRuntime().freeMemory());
        info.add("Total Memory 2->"+Runtime.getRuntime().totalMemory());
        int backupCount=0;
        if(count==0) return NoDocumentToCheckOut;
        VWDocBackup docBackup = new VWDocBackup();
        VWCUtil.appendFile(info, infoFilePath);
        info.removeAllElements();
        for(int i=0; i<docs.size(); i++)
        {
            info.add("Free Memory 6."+i+"->"+Runtime.getRuntime().freeMemory());
            Document doc=(Document) docs.get(i);
            if(!doc.getTag().equals("")) continue;
            // Add the following condition checking to execute the native call only for DTC. It is not required for Web Client
            if (type == 1 || type == 9)
            VWCUtil.documentCheckOut(doc.getName());
            ///retVal= (new VWDocBackup())
            fileCheckOut=fileCheckOut+doc.getVWDoc().getSize();
            docBackup.backupDocument(sid, this, doc, false, true, false, false, 
                true, true, true, path, checkOutType,passsword);
            info.add("Free Memory 7."+i+"->"+Runtime.getRuntime().freeMemory());
            if(retVal<0 && retVal!=documentNotFound && retVal!=DocTextNotFoundErr)
            {
                doc.setTag(String.valueOf(retVal));
            }
            else
            {
                ///retVal = backupDoc(sid, doc, false, false, false, false, true, true, path);
        	String checkOutDesc = "Checkout to ";
        	if ( checkOutType == 2){
        	    checkOutDesc = "Checkout - copied to ";
        	}
                addATRecord(vws, session.room, sid, doc.getId(), 
                                        VWATEvents.AT_OBJECT_DOC,
                                        VWATEvents.AT_DOC_CHECKOUT, 
                                        checkOutDesc + path);
                backupCount++;
            }
            VWCUtil.appendFile(info, infoFilePath);
            info.removeAllElements();
            /* Issue: When check out from folder level if some of the docs are not 
            * checked out because of some reason (e.g locked), it was not continuing with rest of the docs.
            * Setting sessionid for the document should be in this for loop. Valli 23 Oct 2007 */ 
            if(checkOutType==1)
            {
                try
                {
                    if(node!=null)
                        sessionId=vws.createSession(session.room, sid, node, sName);
                        if (doc.getTag().equals(""))
                        {
                            sessionId=vws.createSession(session.room, sid, 
                                new Node(doc.getId()), sName);
                        }
                }
                catch(Exception e)
                {
                    return Error;
                }
            }
        }
        try {
        	 
			String content =Long.toString(fileCheckOut);
			String filepath=path+"/sizeinfo.txt";
			File file = new File(filepath);
 
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
 
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
 
			System.out.println("Done");
 
		} catch (IOException e) {
			e.printStackTrace();
		}
        
        if(settingsId>=0){
        	setNotificationHistory(sid, settingsId, nodeIdToNotify, VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_Checkout, session.user, message);
        }
        
        info.add("Free Memory 8->"+Runtime.getRuntime().freeMemory());
        if(backupCount==0) return NoDocumentToCheckOut;
        /*if(checkOutType==1)
        {
            try
            {
                if(node!=null)
                    sessionId=vws.createSession(session.room, sid, node, sName);
                for(int i=0;i<backupCount;i++)
                {
                    Document doc = (Document) docs.get(i);
                    if (doc.getTag().equals(""))
                    {
                        sessionId=vws.createSession(session.room, sid, 
                            new Node(doc.getId()), sName);
                    }
                }
            }
            catch(Exception e)
            {
                return Error;
            }
        }*/
        info.add("Free Memory 9->"+Runtime.getRuntime().freeMemory());
        info.add("Total Memory 3->"+Runtime.getRuntime().totalMemory());
        VWCUtil.appendFile(info, infoFilePath);
        info.removeAllElements();
        return sessionId;
    }
    
    /** Check in a Document
     * @param sid Session id obtained by login
     * @param sessionId  Check out session ID
     * @param docPath The path of the checked out Document
     * @param results Results of the check in process
     * @return 0 for success or negative value error code
     */
    public int checkInDoc(int sid, int sessionId, String docPath, Vector results)
    {
        return checkInDoc(sid, sessionId, docPath, false, results);
    }
    
    /** Check in a Document
     * @param sid Session id obtained by login
     * @param sessionId  Check out session ID
     * @param docPath The path of the checked out Document
     * @param deleteFolder true - delete source folder
     * @param results Results of the check in process
     * @return 0 for success or negative value error code
     */
    public int checkInDoc(int sid, int sessionId, String docPath, 
            boolean deleteFolder, Vector results)
    {
    	return checkInDoc(sid, sessionId, 0, docPath, deleteFolder, results);
    }
    public int checkInDoc(int sid, int sessionId, int nodeId, String docPath, 
                            boolean deleteFolder, Vector results)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in checkInDoc::"+sid);
        VWClient.printToConsole("checkInDoc path ::"+docPath);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        if(results==null)   results = new Vector();
        Vector docFolders = getDocFolders(docPath);
        int count=docFolders.size();
        if(count==0) return NoDocumentToCheckIn;
        int retVal=0;
        String storageType="";
        String storagepath="";
        String azureUserName="";
        String azurePassword="";
        String checkInFolderPath = ""; 
        String sizefromFile="";
        try{
        	Vector storageVect=new Vector();
			getAzureStorageCredentials(sid,0,storageVect);
			if(storageVect!=null&&storageVect.size()>0){
				VWClient.printToConsole("storageVect inside delnode::::"+storageVect.get(0).toString());
				String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
				VWClient.printToConsole("Stroage type from storage array in delnode"+storageArray[7]);
				storageType=storageArray[7];
				azureUserName=storageArray[8];
				azurePassword=storageArray[9];
			}
			if(storageType.equals("On Premises")){
				File checkingfile=new File(docPath);
				long totalsize= getFileSize(checkingfile);    
				totalsize=totalsize/1024;
				Vector result=new Vector();
				getLatestDocId(sid,result);
				int maxDocId=Integer.parseInt(result.get(0).toString());
				Document maxdoc=new Document(maxDocId);
				Vector size=new Vector();
				checkAvailableSpaceInDSS(sid,maxdoc,"",size);
				long availableSpace= (long) size.get(0); 
				long registrySize=(long)size.get(1);
				registrySize=registrySize*1024;
				availableSpace=availableSpace*1024;
				String filepath=docPath+"\\sizeinfo.txt";
				VWClient.printToConsole("The file path"+filepath);
				br = new BufferedReader(new FileReader(filepath));
				if ((sizefromFile = br.readLine()) != null) {
					long fileSize=Long.parseLong(sizefromFile)/1024;
					if(registrySize>(availableSpace-(totalsize-fileSize))){
						SendEmailFromDSS(availableSpace/1024,sid,maxdoc);
						return dssCriticalError;
					}
				}
			}

        } catch (IOException e) {
        	VWClient.printToConsole("Exception in DSS space while checkin"+e.getMessage());
        } finally {
        	try {
        		if (br != null)br.close();
        	} catch (IOException ex) {
        		ex.printStackTrace();
        	}
        }

		
        
        for(int i=0;i<count;i++)
        {
            String docFolderPath=((File)docFolders.get(i)).getPath();
            if(i == 0)
            	checkInFolderPath = docFolderPath;

            VWClient.printToConsole("checkInFolderPath :::"+checkInFolderPath);
            retVal = (new VWDocRestore()).restoreDocument(sid, this, docFolderPath,
                0, 5, false, false, 
                false, false, 1, sessionId, 0);
            ///retVal=restoreDoc(sid,docFolderPath, 0 ,
             ///   5/*Overwrite and purge old document*/,
             ///   false, false, false, false, 1/*Keep document value*/,sessionId); 
            if(retVal>0)
            {
                addATRecord(vws, session.room, sid, retVal, 
                                        VWATEvents.AT_OBJECT_DOC,
                                        VWATEvents.AT_DOC_CHECKIN, 
                                        "Checkin from " + docPath);
                results.add(String.valueOf(retVal));
                Node node=new Node(retVal);
                node.setSessionId(sessionId);
                releaseSession(sid, node, false);
                if(deleteFolder)
                {
                    VWCUtil.deleteDirectory(docFolderPath);
                    File sessionFolder=new File(docFolderPath).getParentFile();
                    int fileCount=sessionFolder.listFiles().length;
                    if(fileCount==0)
                        VWCUtil.deleteDirectory(sessionFolder.getPath());
                    /*
                    boolean found=false;
                    for(int j=0;j<fileCount;j++)
                    {
                        if(sessionFolder.listFiles()[i].isDirectory())
                        {
                            found=true;
                            break;
                        }
                    }
                    if(!found)  VWCUtil.deleteDirectory(sessionFolder.getPath());
                     */
                }
            }
        }
        try{
        	if(deleteFolder){
        		if(new File(docPath+"\\sizeinfo.txt").exists())
        			new File(docPath+"\\sizeinfo.txt").delete();
        		if(new File(docPath).exists())
        			new File(docPath).delete();
        	}
        }
        catch(Exception ex)
        {
        	printToConsole("Exception while deleting sizeinfo.txt : "+ex.getMessage());
        }
        try{
        	//This is getting called on selection of folder from tree and checkin
        	String message = "Checkin From '"+checkInFolderPath;
        	printToConsole("message : "+message);
        	if (nodeId > 0){
        		addNotificationHistory(sid, nodeId, VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_CheckIn, message);
        	}
        }catch(Exception ex){
        	//VWSLog.add("Exception ex checkInDoc for Notification: "+ex.getMessage());
        }
        return NoError;
    }
    public int releaseSession(int sid, int sessionId)
    {
            Node node = new Node(0);
            node.setSessionId(sessionId);
            releaseSession(sid, node);
            return NoError;
    }
    public int releaseSession(int sid, Node node)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in releaseSession::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
        	Node tempNode = new Node(node.getId());
        	tempNode = vws.getNodePropertiesByNodeID(session.room, sid, tempNode);
            vws.releaseSession(session.room, sid, node);
            releaseSession(sid, node, true);
            try{
            	//This method is called for tree - release session
            	String message = "Checkin to '"+tempNode.getPath();
            	addNotificationHistory(sid, node.getId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_CheckIn, message);
            }catch(Exception ex){
            	//VWSLog.add("Exception ex checkInDoc for Notification: "+ex.getMessage());
            }
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }
    
    private int releaseSession(int sid, Node node, boolean callATAdd)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in releaseSession1::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        try
        {
            vws.releaseSession(session.room, sid, node);
            if(callATAdd)
            	addATRecord(vws, session.room, sid, node.getId(), 
            			VWATEvents.AT_OBJECT_DOC,
            			VWATEvents.AT_DOC_CANCELCHECKOUT,"");
        }
        catch(Exception e)
        {
            return Error;
        }
        return retVal;
    }

    public Vector getDocFolders(String docFolder)
    {
        Vector docFolders=new Vector();
        File selFolder=new File(docFolder);
        File[] subFolders=selFolder.listFiles();
        int subFolderCount=subFolders.length;
        int retValue = checkIsDocBackupDir(selFolder);
        if(retValue > 0)
        {
            docFolders.add(selFolder);
        }
        else
        {
            for(int i=0;i<subFolderCount;i++)
                if(checkIsDocBackupDir(subFolders[i])>0)
                {
                    docFolders.add(subFolders[i]);
                }
        }
        return docFolders;
    }
    public int checkIsDocBackupDir(File docDir)
    {
        if(docDir==null) return 0;
        File[] subDir = docDir.listFiles();
        if(subDir==null)return 0;
        boolean withPages=false;
        boolean validFolder=false;
        for(int i=0; i < subDir.length; i++)
        {
            if(subDir[i].getName().equalsIgnoreCase(BackupFileName)) 
                                                        validFolder = true;
            if(subDir[i].getName().equalsIgnoreCase(BackupPageFolderName)) 
            {
                File pagesFolder=new File(subDir[i].getPath());
                if(pagesFolder.list().length>0) withPages = true;
            }
        }
        if(withPages) return 2;
        if(validFolder) return 1;
        return 0;
    }
    public int outputDocs(Vector outputInfo, VWClient client, String libPath)
    {
        int ret = 0;
        ret = loadOutputLibrary(libPath);
        if( ret == NoError) Export(outputInfo, client);
        return ret;
    }
    public String getAppList(String libPath)
    {
        if(loadOutputLibrary(libPath)==NoError) return GetAppsList();
        return "";
    }
    public void sendToConfigure(String libPath)
    {
        if(loadOutputLibrary(libPath)==NoError) SendToConfigure();
    }
    public String getWTSAddress()
    {
        if(loadWTSLibrary() == NoError)
            return WTSgetClientIPAddress();
        else
            return "";
    }
    public int getDSSList(int sid, Vector dssList)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getDSSList(session.room, sid, dssList);
            dssList.addAll(vectorToServerSchema(ret));
            VWClient.printToConsole("Active Error in getDSSList--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getDSSforDoc(int sid, Document doc, ServerSchema dssSchema)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDSSforDoc::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
            dssSchema.set(ss);
            VWClient.printToConsole("Active Error in getDSSforDoc--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int isDSSLocationExists(int sid, ServerSchema dssSchema, String location){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isDSSLocationExists::"+sid);
        VWClient.printToConsole("Storage type in isDSSLocationExists::: "+dssSchema.storageType);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {  
        	if(dssSchema.storageType.equals("On Premises")){
        		VWClient.printToConsole("Inside onpremises storage condition");
            boolean  result = vws.isDSSLocationExists(dssSchema, location);
            if (result) return NoError;
            return Error;
        	}else if(dssSchema.storageType.equals("Azure Storage")){
        		try{
        			CloudFileShare share=null;
        			CloudFileDirectory rootDir=null;
        			CloudFileDirectory sampleDir =null;
        			CloudFileClient fileClient=null;
        			CloudStorageAccount storageAccount =null;
        			VWClient.printToConsole("Inside azure storage test");
        			String accountName="AccountName="+dssSchema.getUserName()+";";
        			String password="AccountKey="+dssSchema.getPassword();
        			String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
        			System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
        			 storageAccount = CloudStorageAccount.parse(storageConnectionString1);
        			VWClient.printToConsole("storageAccount: file endpoint::::"+storageAccount.getFileEndpoint());
        			String schemaPath=location;
        			VWClient.printToConsole("schemaPath :::::"+schemaPath);
        			String pattern = Pattern.quote(System.getProperty("file.separator"));
        			String dbpath[]=schemaPath.split(pattern);
        			 fileClient = storageAccount.createCloudFileClient();
        			VWClient.printToConsole("fileClient:::"+fileClient.getStorageUri());
        			for(int i=0;i<dbpath.length;i++){
        				if(i==2)
        				{
        					share = fileClient.getShareReference(dbpath[i]);
        					VWClient.printToConsole("inside condition 2  :"+share.getUri());
        					/*if(!share.exists())
        						share.create();*/
        				}
        				else if(i==3)
        				{
        					sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
        					VWClient.printToConsole("inside condition 3  :"+share.getUri());
        					/*if(!sampleDir.exists()){
        						sampleDir.create();

        					} */
        					rootDir = sampleDir;
        					sampleDir = null;
        				}
        				else if(i>=4)
        				{
        					VWClient.printToConsole("inside condition 4  :"+share.getUri());
        					if ((i+1) != dbpath.length) {
        						sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        					/*	if(!sampleDir.exists()){
        							sampleDir.create();
        						}*/
        						rootDir = sampleDir;
        						sampleDir = null;
        					}
        					else if ((i+1) == dbpath.length) {
        						sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        						/*if(!sampleDir.exists()){
        							sampleDir.create();
        						}*/
        						rootDir = sampleDir;

        						sampleDir = null;
        					}
        				}

        			}
        			VWClient.printToConsole("rootDir geturi :::::"+rootDir.getUri());
        			if(rootDir.exists()){
        				VWClient.printToConsole("Inside share exist ");
        				return NoError;
        			}else{
        				VWClient.printToConsole("Inside share not exist");
        				return Error;
        			}

        		}catch(Exception e){
        			VWClient.printToConsole("Inside exception 1::::::"+e.getMessage());
        			 return Error;
        		}
			
        	}
          
        
        }
        catch(Exception e)
        {
        	VWClient.printToConsole("Inside exception 2::::::"+e.getMessage());
            e.printStackTrace();
            return Error;
        }
		return NoError;        
    }
    public int getLockDocList(int sid, Vector docs)
    {
        Session session = getSession(sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret=vws.getLockDocList(session.room, sid, docs);
            docs.addAll(ret);
            VWClient.printToConsole("Active Error in getLockDocList--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int setNodeStorage(int sid, Node node, int withInherit)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setNodeStorage::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.setNodeStorage(session.room, sid, node, withInherit);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int isDocTypeFound(int sid, DocType docType)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isDocTypeFound::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.isDocTypeFound(session.room, sid, docType);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int isIndexFound(int sid, Index index, int checkType){
    	return isIndexFound(sid, index, checkType, null);
    }
    public int isIndexFound(int sid, Index index, int checkType, DocType dType)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isIndexFound::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.isIndexFound(session.room, sid, index, checkType, dType);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int deleteDocType(int sid, DocType docType)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteDocType::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.deleteDocType(session.room, sid, docType);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int deleteIndex(int sid, Index index)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteIndex::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.deleteIndex(session.room, sid, index);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int deleteDTIndex(int sid, Index index)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteDTIndex::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.deleteDTIndex(session.room, sid, index);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int setDocTypeAutoMail(int sid, DocType docType)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDocTypeAutoMail::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.setDocTypeAutoMail(session.room, sid, docType);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getRecycleDocList(int sid, int rowCount, int lastRowId, int actionType, Vector docs)
    {
	return getRecycleDocList(sid, rowCount, lastRowId, actionType, "", docs);
    }
    
    public int getRecycleDocList(int sid, int rowCount, int lastRowId, int actionType, String indexValue, Vector docs)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRecycleDocList::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret=vws.getRecycleDocList(session.room, sid, rowCount, lastRowId, actionType, docs);
            docs.addAll(ret);
            VWClient.printToConsole("Active Error in getRecycleDocList--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getRecycleDocCount(int sid){
	return getRecycleDocCount(sid, "");
    }

    public int getRecycleDocCount(int sid, String indexValue){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRecycleDocCount::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           int ret=vws.getRecycleDocCount(session.room, sid, indexValue);
            return ret;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int restoreNodeFromRecycle(int sid, Node node)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in restoreNodeFromRecycle::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.restoreNodeFromRecycle(session.room, sid, node);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getUserRedactions(int sid, String userName,Vector docs)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getUserRedactions::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret=vws.getUserRedactions(session.room, sid, userName, docs);
            docs.addAll(ret);
            VWClient.printToConsole("Active Error in getUserRedactions--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int setUserRedactions(int sid, String userName, String oldPassword, 
                                                String newPassword, Vector docs)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setUserRedactions::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.setUserRedactions(session.room, sid, userName, 
                        encryptStr(oldPassword), encryptStr(newPassword), docs);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int setDTVersionSettings(int sid, DocType docType)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDTVersionSettings::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.setDTVersionSettings(session.room, sid, docType);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getDTVersionSettings(int sid, DocType docType)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDTVersionSettings::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector settings=vws.getDTVersionSettings(session.room, sid, docType);
            docType.setSettings(settings);
            VWClient.printToConsole("Active Error in getDTVersionSettings--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getATSettings(int sid, Vector ATSettings)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getATSettings::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret=vws.getATSettings(session.room, sid);
            ATSettings.addAll(ret);
            VWClient.printToConsole("Active Error in getATSettings--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int setATSetting(int sid, int eventId, int status)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setATSetting::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.setATSetting(session.room, sid, eventId, status);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getATData(int sid, Vector filter, Vector data)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getATData::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getATData(session.room, sid, filter);
            data.addAll(ret);
            VWClient.printToConsole("Active Error in getATData--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int detATData(int sid, Vector filter,String archivePath)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in detATData::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.detATData(session.room, sid, filter, archivePath);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int updateDocType(int sid, DocType docType)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateDocType::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.updateDocType(session.room, sid, docType);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int updateIndex(int sid, Index index)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateIndex::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.updateIndex(session.room, sid, index);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int updateDTIndex(int sid, int docTypeId, Index index)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateDTIndex::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.updateDTIndex(session.room, sid, docTypeId, index);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Returns a list of values for a Selection Index
     * @param sid Session id obtained by login
     * @param index The selection index for which to return values
     * @return 0 for success or negative value error code
     */
    public int populateSelectionIndex(int sid, Index index)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in populateSelectionIndex::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = new Vector();
            ret = vws.populateSelectionIndex(session.room, sid, index.getId(), ret);
            index.setDef(stripIds(ret));
            VWClient.printToConsole("Active Error in populateSelectionIndex--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
         	   return NoError;
            } else {
         	   return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int updateIndexSelection(int sid, Index index, Vector selValues){
    	return updateIndexSelection(sid, index, selValues, "A");
    }
    public int updateIndexSelection(int sid, Index index, String selValues){
    	/*
    	 * clientMode will be 'A' from AdminWise and 'D' from DTC
    	 */
    	return updateIndexSelection(sid, index, selValues, "A");
    }
    public int updateIndexSelection(int sid, Index index, String selValues, String clientMode)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateIndexSelection::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.updateIndexSelection(session.room, sid, index, selValues, clientMode);
        }
        catch(Exception e) 
        {
            return Error;
        }
    }
    public int updateIndexSelection(int sid, Index index, Vector selValues, String clientMode)
    {
    	Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in updateIndexSelection1::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.updateIndexSelection(session.room, sid, index, selValues, clientMode);
        }
        catch(Exception e) 
        {
            return Error;
        }
    }
    public int getIndices(int sid, int indexId, Vector indices)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getIndices::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getIndices(session.room, sid, indexId, indices);
            indices.addAll(ret);
            VWClient.printToConsole("Active Error in getIndices--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
            /*
             indices.addAll(vectorToIndices(ret));
            for (int i=0; i < indices.size(); i++)
            {
                Index idx = (Index) indices.get(i);
                if ( idx.getType() == Index.IDX_SELECTION )
                {
                    ret = vws.populateSelectionIndex(session.room, 
                                                         sid, idx.getId(), ret);
                    idx.setDef(stripIds(ret));
                }
            }
             */
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getDocTypeInfo(int sid, int docTypeId, Vector docTypeInfo)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocTypeInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = 
                  vws.getDocTypeInfo(session.room, sid, docTypeId, docTypeInfo);
            docTypeInfo.addAll(ret);
            VWClient.printToConsole("Active Error in getDocTypeInfo--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
            /*
            indices.addAll(vectorToIndices(ret));
            for (int i=0; i < indices.size(); i++)
            {
                Index idx = (Index) indices.get(i);
                if ( idx.getType() == Index.IDX_SELECTION )
                {
                    ret = vws.populateSelectionIndex(session.room, 
                                                         sid, idx.getId(), ret);
                    idx.setDef(stripIds(ret));
                }
            }
             */
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getRoomSessionId(String server, String room)
    {
        Collection colSessions = sessions.values();
        Iterator it = colSessions.iterator();
        while (it.hasNext())
        {
            Session session = (Session) it.next();
            if (session.server.equals(server) && session.room.equals(room))
                return session.sessionId;
        }
        return 0;
    }     
    public int resyncDirectory(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in resyncDirectory::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.resyncDirectory(session.room, sid);
        }
        catch(Exception e)
        {
            return Error;
        }
    }     
    public String readConFile(String path) 
    {
    	String registerInfo = Util.readRegistrationFile(path); 
        String serverInfo =	"";
        String selectedRoom =	"";
        //Util.Msg(null,"registerInfo  "+registerInfo,"VWClient");
        // Start Enhancement of Show/Hide Room. Store the selected rooms from .vws file to client registry
        if (registerInfo.indexOf("###") != -1) {
        	serverInfo = registerInfo.substring(0,(registerInfo.indexOf("###")));
        	selectedRoom = registerInfo.substring((registerInfo.indexOf("###")+3),registerInfo.length());
        }
        else 
        	serverInfo = registerInfo;
         //	  Util.Msg(null,"Server Info "+serverInfo,"VWClient");
         //	  Util.Msg(null,"registerInfo "+registerInfo,"VWClient");
         //	  Util.Msg(null,"selectedRoom "+selectedRoom,"VWClient");
        VWCPreferences.addSelectedRoom(selectedRoom,serverInfo.substring(0,serverInfo.indexOf("@")));
        // End Enhancement
        return serverInfo;
    }    
    public int getEWorkTemplates(int sid, Vector templates)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getEWorkTemplates::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getEWorkTemplates(session.room, sid, templates);
            templates.addAll(vectorToEWorkTemplate(ret));
            VWClient.printToConsole("Active Error in getEWorkTemplates--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
         	   return NoError;
            } else {
         	   return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }   
    public int newEWorkTemplate(int sid, EWorkTemplate template) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in newEWorkTemplate::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            EWorkTemplate retTemplate = vws.newEWorkTemplate(session.room, sid, 
                                                                      template);
            template.setId(retTemplate.getId());
            return retTemplate.getId();
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int updateEWorkTemplate(int sid,  EWorkTemplate template)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateEWorkTemplate::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.updateEWorkTemplate(session.room, sid, template);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int deleteEWorkTemplate(int sid,  EWorkTemplate template)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteEWorkTemplate::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.deleteEWorkTemplate(session.room, sid, template);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getEWorkTemplateMapping(int sid, EWorkTemplate template) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getEWorkTemplateMapping::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        Vector mapping = new Vector();
        try
        {
            Vector ret = vws.getEWorkTemplateMapping(session.room, sid, 
                                                     template.getId(), mapping);
            mapping.addAll(vectorToEWorkMap(ret));
            template.setMapping(mapping);
            ret.removeAllElements();
            VWClient.printToConsole("Active Error in getEWorkTemplateMapping--->"+vws.getActiveError());
          //  return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int newEWorkSubmit(int sid, EWorkSubmit ewSubmit)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in newEWorkSubmit::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            EWorkSubmit retSubmit = vws.newEWorkSubmit(session.room, sid, 
                                                                      ewSubmit);
            ewSubmit.setId(retSubmit.getId());
            return retSubmit.getId();
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getEWorkSubmits(int sid, Vector ewSubmits)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getEWorkSubmits::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getEWorkSubmits(session.room, sid, ewSubmits);
            ewSubmits.addAll(vectorToEWorkSubmit(ret));
            VWClient.printToConsole("Active Error in getEWorkSubmits--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getEWorkSubmitMapping(int sid, EWorkSubmit submit) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getEWorkSubmitMapping::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retVal=0;
        Vector mapping = new Vector();
        try
        {
            Vector ret = vws.getEWorkSubmitMapping(session.room, sid, submit.getId(), mapping);
            mapping.addAll(vectorToEWorkMap(ret));
            submit.getEWorkTemplate().setMapping(mapping);
            ret.removeAllElements();
            VWClient.printToConsole("Active Error in getEWorkSubmitMapping--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int endEWorkSubmit(int sid, EWorkSubmit submit) 
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in endEWorkSubmit::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           return vws.endEWorkSubmit(session.room, sid, submit.getId());
        }
        catch(Exception e)
        {
            return Error;
        }      
    }
    public int createSignTemplate(Vector userSigns, String path)
    {
        if(userSigns!=null)
        {
            String signStr=VWCUtil.createSign(userSigns);
            if(signStr==null || signStr.equals(""))
                return CreateSignTemplateError;
            File folder = new File(new File(path).getParent());
            try
            {
                if (!folder.exists()) folder.mkdirs();
                FileOutputStream ufos = new FileOutputStream(path);
                OutputStreamWriter osw = new OutputStreamWriter(ufos);
                osw.write(signStr);
                osw.close();
            }
            catch (Exception e){}
        }
        return NoError;
    }
    
    public int isSignAnnotationExist(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isSignAnnotationExist::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.isSignAnnotationExist(sid, session.room);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    public int setUserSign(int sid, String userName, int signType, String signFile)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setUserSign::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            VWDoc signDoc=new VWDoc();
            signDoc.setSrcFile(signFile);
            return vws.setUserSign(session.room, sid, userName ,signType, 
                                                                       signDoc);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int removeUserSign(int sid, String userName, int signType)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in removeUserSign::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.removeUserSign(session.room, sid, userName ,signType);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getUserSign(int sid, Signature sign)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getUserSign::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if(sign.getSignFilePath()==null || sign.getSignFilePath().equals(""))
        {
            File file= sign.makeSignFilePath( getCacheFolder(sid),
                sign.getUserName(),sign.getType());
            sign.setSignFilePath(file.getPath());
        }
        if (vws == null) return ServerNotFound;
        File signFile=new File(sign.getSignFilePath());
        if(signFile.exists()) return NoError; 
        try
        {
            if(sign.getUserName()==null || sign.getUserName().equals("")) 
                                                 sign.setUserName(session.user);
            VWDoc signDoc = new VWDoc();
            signDoc=vws.getUserSign(session.room, sid, sign.getUserName() ,
                                                       sign.getType(), signDoc);
            signDoc.setDstFile(signFile.getPath());
            signDoc.writeContents();
            VWClient.printToConsole("Active Error in getUserSign--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    /**
     * To check the license of signwise
     * @param sid
     * @return
     */
    public int isVWSignInstalled(int sid)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isVWSignInstalled::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.isVWSignInstalled();
        }
        catch(Exception e)
        {
            return Error;
        }
        /*
        VWCUtil.loadNativeLibrary("VWView.dll",null);
        if(VWCUtil.loadNativeLibrary("VWSign.dll",null))
           return 1;
        return 0;
         */ 
    }
    public int isUserHasCSign(int sid)
    {
        Principal principal=new Principal();
        getMyPrincipal(sid, principal);
        if(principal.isHasCSign()) return 1;
        return 0;
    }
    public int isUserHasASign(int sid){
        Principal principal=new Principal();
        getMyPrincipal(sid, principal);
        if(principal.isHasASign())
            return 1;
        return 0;
    }
    public int isUserHasSign(int sid, int signType){
    	Principal principal=new Principal();
    	getMyPrincipal(sid, principal);
    	if(signType == 1){
    		if (principal.isHasASign())
    			return 1;
    	}else if(signType == 2){
    		if(principal.isHasCSign())
    			return 2;
    	}
    	return 0;
    }
    public int uploadWebClientDocument(int sid,String serverName,String roomName,String userName,int docId,String source, boolean newDoc, String pageInfo){
    	//System.out.println(" uploadWebClientDocument -> sid , docId, source,  newDoc,  pageInfo " + sid + ", " + docId+ ", " + source+ ", " +newDoc+ ", " +pageInfo+" ::: " + roomName+" ::: " + userName + "   :::: " + serverName);
    	if (getSession(sid) == null) {
	        Session s = new Session(sid, serverName, roomName, source, userName);
	        //System.out.println(" Session created for " + sid + " session " + s);
	        sessions.put(new Integer(sid), s);
    	}    	
        Document doc=new Document(docId);    	
    	int ret = 0;//setDocFile(sid, doc,source,false,newDoc,pageInfo);
    	//System.out.println("uploadWebClientDocument  Return Value is ... " + ret);
    	return ret;
    }
    public int setDocumentSign(int sid, Signature sign, int checkOnly, long hwnd)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDocumentSign::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        sign.setUserName(session.user);
        if(sign.getType()==Signature.Certified_Type)
        {
            sign.setType(Signature.Template_Type);
            sign.setSignFilePath("");
            getUserSign(sid, sign);
            String templateStr=VWCUtil.getStrFromFile(new File(sign.getSignFilePath()));
            int result=VWCUtil.validateSign(templateStr,hwnd);
            if(result==0)   return result;
            if(checkOnly==1)  return result;
            if(VWCUtil.signatureConfirm()==0) return 0;
            sign.setType(Signature.Certified_Type);
        }
        try
        {
            return vws.setDocumentSign(session.room, sid, sign);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int updateDocumentSign(int sid, Signature sign){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateDocumentSign::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        sign.setUserName(session.user);
        try
        {
            return vws.setDocumentSign(session.room, sid, sign);
        }
        catch(Exception e)
        {
            return Error;
        }    	
    }
    public int getDocumentSign(int sid, Signature sign)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentSign::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector signInfo=vws.getDocumentSign(session.room, sid, sign);
            if(signInfo!=null && signInfo.size()>0)
            {
                sign.setSignatureInfo((String)signInfo.get(0));
                getUserSign(sid, sign);
            }
        }
        catch(Exception e)
        {
            return Error;
        }
        return NoError;
    }
    public int getDocumentSigns(int sid, Document doc,Vector signs)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentSigns::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector signInfo = vws.getDocumentSigns(session.room, sid, doc);
            VWClient.printToConsole("Active Error in getDocumentSigns--->"+vws.getActiveError());
            if(signInfo == null || signInfo.size()==0) 
                                                    return vws.getActiveError();
            for(int i=0;i<signInfo.size();i++)
            {
                signs.add(new Signature((String)signInfo.get(i)));
            }
        }
        catch(Exception e)
        {
            return Error;
        }
        return NoError;
    }
    public int updateStorageInfo(int sid, ServerSchema storage)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in updateStorageInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            return vws.updateStorageInfo(session.room, sid, storage);
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    
    /** Publishes document in cache folder
     * @param sid Session id obtained by login
     * @param doc The document to be published
     * @return 0 for success or negative value error code
     */
    public int publish(int sid, Document doc)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in publish::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() == 0) return invalidParameter;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        int docID = doc.getId();
        int ret = getDocFile(sid, new Document(docID));
        if (ret != NoError) return ret;
        String srcPath = session.cacheFolder + File.separator + docID + 
                                                 File.separator + DOC_CONTAINER;
        String dstPath = session.cacheFolder + File.separator + "Publish" + 
                                                         File.separator + docID;
        new File(dstPath).mkdirs();
        String xmlPath = session.cacheFolder + File.separator + "Publish" + 
                          File.separator + docID + File.separator + "Pages.xml";
        
        if (VWCUtil.publish(dstPath, xmlPath, srcPath, session.server, 
                                                           session.room, docID))
            ret =  NoError;
        else
            ret =  Error;
        cleanUpForDoc(session, docID);
        return ret;
    }
    
    /** Publishes document in a specified folder
     * @param sid Session id obtained by login
     * @param doc The document to be published
     * @param disLocation The destination location to publish the document
     * @return 0 for success or negative value error code
     */
    public int publish(int sid, Document doc, String disLocation)
    {
    	int ret = 0;
    	try{
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in publish1::"+sid);
	        if(session==null) return invalidSessionId;
	        if (doc == null || doc.getId() == 0) return invalidParameter;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        
	        int docID = doc.getId();
	        ret = getDocFile(sid, new Document(docID));
	        if (ret != NoError) return ret;
	        String srcPath = session.cacheFolder + File.separator + docID + 
	                                                 File.separator + DOC_CONTAINER;
	        String dstPath = disLocation + File.separator + docID;
	        new File(dstPath).mkdirs();
	        String xmlPath = disLocation + File.separator + docID + File.separator + "Pages.xml";
	        if (VWCUtil.publish(dstPath, xmlPath, srcPath, session.server, 
	                                                           session.room, docID))
	            ret =  NoError;
	        else
	            ret =  Error;
	        cleanUpForDoc(session, docID);
    	}catch(Exception ex){
    		printToConsole(" Exception "+ex.toString());
    		ret =  Error;
    	}
        return ret;
    }
//  Add the functionality for extract the pages in the native format.
    
    /** Publishes document in a specified folder with xml and compression options
     * @param sid Session id obtained by login
     * @param doc The document to be published
     * @param destPath The destination location to publish the documen
     * @param xml 1 - creates Pages.xml, 0 - will not create Pages.xml
     * @param compress 1- Compress files into the zip format, 0 - will not compress 
     * @return 0 for success or negative value error code
     */
    public int publishDirect(int sid, Document doc, String destPath, int xml, int compress)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in publishDirect::"+sid);
        if(session==null) return invalidSessionId;
        if (doc == null || doc.getId() == 0) return invalidParameter;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        
        int docID = doc.getId();
        int ret = getDocFile(sid, new Document(docID));
        if (ret != NoError) return ret;
        String srcPath = "", dstPath = "";        
        srcPath  = session.cacheFolder + File.separator + docID + 
                                                 File.separator + DOC_CONTAINER;       
        if (destPath == null || destPath.equals(""))
        	dstPath = session.cacheFolder + File.separator + "Publish" + 
                                                         File.separator + docID;
        else
        	dstPath = destPath;
        
        new File(dstPath).mkdirs();
        String xmlPath = dstPath + File.separator + "Pages.xml";
        if (VWCUtil.publishDirect(dstPath, xmlPath, srcPath, session.server, 
                                                          session.room, docID, xml, compress))            
        	ret =  NoError;
        else
            ret =  Error;
        return ret;
    }
    /*CV2019 merges from SIDBI line------------------------------------------------***/
	public int cvwebConverttoPDFandAddWatermarkToPDF(int sid, String docId, String docName, 
			String srcPath,	String htmlPath, String destPath, int hcPdf, String hcPdfProfile, 
			int appendReport, int xresolution,int yresolution, int orientation)	{
		Session session = getSession(sid);
		if(session==null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try{
			/**
			 * @author apurba.m
			 * Below Registry values are added because it should read from the registry instead of
			 * sending static values for convert to pdf
			 **/
			String burnAnnotationRegistry = DRSPreferences.getBurnAnnotation();
			VWClient.printToConsole("burnAnnotationRegistry cvwebConverttoPDFandAddWatermarkToPDF  :::: "+burnAnnotationRegistry);
			String whiteoutValRegistry = DRSPreferences.getWhiteoutValue();
			VWClient.printToConsole("whiteoutValRegistry cvwebConverttoPDFandAddWatermarkToPDF  :::: "+whiteoutValRegistry);
			String depthValRegistry = DRSPreferences.getDepthValue();
			VWClient.printToConsole("depthValRegistry cvwebConverttoPDFandAddWatermarkToPDF  :::: "+depthValRegistry);
			String colorValRegistry = DRSPreferences.getColorValue();
			VWClient.printToConsole("colorValRegistry cvwebConverttoPDFandAddWatermarkToPDF  :::: "+colorValRegistry);

			VWCUtil.cvwebConverttoPDFandAddWatermarkToPDF(docId, docName, srcPath, htmlPath, destPath, "", "", Integer.parseInt(burnAnnotationRegistry), Integer.parseInt(whiteoutValRegistry), Integer.parseInt(colorValRegistry),
					Integer.parseInt(depthValRegistry), orientation, hcPdf, hcPdfProfile, appendReport, xresolution, yresolution);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	/*------------------------End of SIDBI merges--------------------------------------*/
	public int ConvertDocToPDFDRS(int sid,String srcPath, String destPath) {
    	return ConvertDocToPDFDRS(sid, srcPath, destPath, 0, "photo600", 2, 0);
	}
   /**
    * Method added fro Convertopdf for drs mail.
    */    
    public int ConvertDocToPDFDRS(int sid,String srcPath, String destPath, int hcPdf, String pdfProfile, int qFactor, int orientation)
    {
    	//DRSLog.add("Iniside convert to pdf of drs....");
    	Session session = getSession(sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try{
//    		boolean sidbiCustomizationFlag = false;
//    		getServerSettingsSystemInfo(sid, session.room, "SIDBI Customization", result);
//    		sidbiCustomizationFlag = getSidbiCustomizationFlag(sid);
//    		VWClient.printToConsole("result inside ConvertDocToPDFDRS :::: "+sidbiCustomizationFlag);
//    		DRSLog.dbg("result inside ConvertDocToPDFDRS :::: "+sidbiCustomizationFlag);
    		/**
			 * @author apurba.m
			 * Below Registry values are added because it should read from the registry instead of
			 * sending static values for convert to pdf
			 **/
    		String burnAnnotationRegistry = DRSPreferences.getBurnAnnotation();
    		VWClient.printToConsole("burnAnnotationRegistry ConvertDocToPDFDRS  :::: "+burnAnnotationRegistry);
    		String whiteoutValRegistry = DRSPreferences.getWhiteoutValue();
    		VWClient.printToConsole("whiteoutValRegistry ConvertDocToPDFDRS  :::: "+whiteoutValRegistry);
    		String depthValRegistry = DRSPreferences.getDepthValue();
    		VWClient.printToConsole("depthValRegistry ConvertDocToPDFDRS  :::: "+depthValRegistry);
    		String colorValRegistry = DRSPreferences.getColorValue();
    		VWClient.printToConsole("colorValRegistry ConvertDocToPDFDRS  :::: "+colorValRegistry);
    		
    		VWCUtil.convertToPDF(srcPath, destPath,"", "",Integer.parseInt(burnAnnotationRegistry), Integer.parseInt(whiteoutValRegistry), Integer.parseInt(colorValRegistry), Integer.parseInt(depthValRegistry), orientation, hcPdf, pdfProfile, qFactor);
    	}catch(Exception e){
    		//DRSLog.dbg("Exception in convert to pdf.."+e.getMessage());
    		return Error;
    	}
    	return NoError;
    }
    public int wrap(int sid, String dstPath, String srcPath)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in wrap::"+sid);
        if(session==null) return invalidSessionId;
        if (dstPath == null) return invalidParameter;
        if (!new File(dstPath).exists()) return invalidParameter;
        if (srcPath == null) return invalidParameter;
        // Start of Fix : If srcPath contains multiple file , it will seperated by semicolon
        // Validate the each file using tokenizer logic.
       	StringTokenizer sToken = new StringTokenizer(srcPath,";");
    	if (sToken.countTokens() > 1){
    		String sPath = "";
    		while(sToken.hasMoreTokens()){
    			sPath = sToken.nextToken();
    			if (!new File(sPath).exists()) return invalidParameter;    				
    		}
    	}
    	else{    	     
        if (!new File(srcPath).exists()) return invalidParameter;
    	}
    // End Of Fix
        if (VWCUtil.wrap(dstPath, srcPath) == 1)
            return NoError;
        else
            return Error;
    }
    private void cleanUpForDoc(Session session, int docID)
    {
        File folder = new File(session.cacheFolder + File.separator + docID);
        if (!folder.exists() || !folder.isDirectory()) return;
        cleanUpFolder(folder);
    }
    private void cleanUpFolder(File folder)
    {
        if( folder.getParent() == null) return; //will not delete a root
        File[] files = folder.listFiles();
        for (int i = 0; i < files.length; i++)
        {
            File file = files[i];
            if (file.isDirectory()) 
                cleanUpFolder(file);
            else
                file.delete();
        }
         folder.delete();
     }
    public int getDocPageText(int sid, Document doc, String path)
    {
        int pageNo=0;
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocPageText::"+sid);
        VWClient.printToConsole("getDocPageText destination path ::"+path);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        File pageFileDest= new File(path);
        pageFileDest.mkdirs();
        if(pageFileDest==null || pageFileDest.equals("") || !pageFileDest.canWrite())
        {
            return FileNotFoundErr;
        }
        try
        {
            Vector pages =vws.DBGetDocPageText(session.room, sid, doc);
            VWClient.printToConsole("Document pages : "+pages.size());
            if(pages==null || pages.size()==0) return DocTextNotFoundErr;
            for(int i=0;i<pages.size();i++)
            {
                VWDoc vwdoc=(VWDoc)pages.get(i);
                vwdoc.setDstFile((new File(pageFileDest,"Page" + String.valueOf(i+1))).getPath());
                vwdoc.writeContents();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
        return NoError;
    }
// Fixing Issue No 694. DTC passing the frames count. to displyaed the number of pages.
    private boolean getCoverPage(int sid, Document doc, Vector signs, String room)
    {
    	return getCoverPage(sid, doc, signs, room, 0);
    }
    private boolean getCoverPage(int sid, Document doc, Vector signs, String room, int frames)
    {    	
    	resourceManager=ResourceManager.getDefaultManager();
        Vector h = new Vector();
        h.add("<html>");
        h.add("<body>");
        h.add("<p align=center><b><font face=Arial size=2>" + PRODUCT_NAME.toUpperCase() +" "+resourceManager.getString("CoverPage.Title")+"</font></b></p>");
        h.add("<p><font size=2>"+resourceManager.getString("CoverPage.DateIssue")+" ");
        h.add(Calendar.getInstance().getTime().toString() + "</font></p>");
        h.add("<p><font size=2>" + PRODUCT_NAME + " "+resourceManager.getString("CoverPage.Room")+" "+room+ "</font></p>");
        h.add("<p><i><b><font size=2>"+resourceManager.getString("CoverPage.Properties")+"</font></b></i></p>");

        h.add("<table><tr>");
        h.add("<td width=102 height=26><font size=2>"+resourceManager.getString("CoverPage.DocName")+"</font></td>");
        h.add("<td width=205 height=26><font size=2>" + doc.getName() + "</font></td>");
        h.add("<td width=68 height=26><font size=2>"+resourceManager.getString("CoverPage.Version")+"</font></td>");
        h.add("<td width=122 height=26><font size=2>" + doc.getVersion() + "</font></td>");
        h.add("</tr></table>");
        
        h.add("<table><tr>");
        h.add("<td width=102 height=26><font size=2>"+resourceManager.getString("CoverPage.DocType")+"</font></td>");
        h.add("<td width=205 height=26><font size=2>" + doc.getDocType().getName() + "</font></td>");
        h.add("<td width=72 height=26><font size=2>"+resourceManager.getString("CoverPage.CreatedOn")+"</font></td>");
        h.add("<td width=124 height=26><font size=2>" + doc.getCreationDate() + "</font></td>");
        h.add("</tr></table>");
        
        h.add("<table><tr>");
        h.add("<td width=97 height=26><font size=2>"+resourceManager.getString("CoverPage.CreatedBy")+"</font></td>");
        h.add("<td width=239 height=26><font size=2>" + doc.getCreator() + "</font></td>");
        h.add("</tr></table>");
        
       
        h.add("<p><i><b><font size=2>"+resourceManager.getString("CoverPage.Indices")+"</font></b></i></p>");

        h.add("<table>");
        Vector indices=doc.getDocType().getIndices();
        for (int i=0; i < indices.size(); i++)
        {
            Index idx = (Index) indices.get(i);
            h.add("<tr><td width=166 height=26><font size=2>" + idx.getName() + "</font></td>");
            h.add("<td width=199 height=26><font size=2>" + idx.getValue() + "</font></td></tr>");
        }
        h.add("</table>");
// Fixing Issue No 694. DTC passing the frames count. to displyaed the number of pages.        
        h.add("<p><font size=2>");
        if (frames <= 0)
        	h.add(resourceManager.getString("CoverPage.TotalPageAvail"));
        else
        	h.add(resourceManager.getString("CoverPage.TotalNOPages") + frames +" "+ resourceManager.getString("CoverPage.ExcludeCoverPage"));
        h.add("</font></p>");
        h.add("<p><font size=2>");
        h.add(resourceManager.getString("CoverPage.SignatureCoverpage")); 
        h.add(" "+resourceManager.getString("CoverPage.CurrentAuthorized"));
        h.add(" "+resourceManager.getString("CoverPage.DateListedAbove1")+" " + PRODUCT_NAME.toUpperCase() + resourceManager.getString("CoverPage.DateListedAbove2")); 
        h.add(" "+resourceManager.getString("CoverPage.Notified"));
        h.add(" "+resourceManager.getString("CoverPage.DocIssuing1")+" "+ PRODUCT_NAME.toUpperCase() +" "+resourceManager.getString("CoverPage.DocIssuing2")); 
        h.add(" "+resourceManager.getString("CoverPage.SignatureApplicable")); 
        h.add(" "+resourceManager.getString("CoverPage.Referenced1")+" " + PRODUCT_NAME.toUpperCase() +" "+ resourceManager.getString("CoverPage.Referenced2")+"</font></p>");

        h.add("<p><i><b><font size=2>"+resourceManager.getString("CoverPage.Signatures")+"</font></b></i></p>");
        
        if (signs.size() > 0)
        {
            for(int i=0; i < signs.size(); i++)
            {
                Signature sign=(Signature)signs.get(i);
                if(sign.getStatus()>0)
                {
                    h.add("<table><tr>");
                    h.add("<td width=143 height=47><font size=2><p>&nbsp;</p>" + sign.getSignDate() + "</font></td>");
                    String msg = (sign.getType()==1?
                                    resourceManager.getString("CoverPage.Authorized")+" " + sign.getUserName() :
                                    	resourceManager.getString("CoverPage.Certified")+" "+ sign.getUserName());
                                        
                    h.add("<td width=180 height=47><font size=2><p>&nbsp;</p>" + msg + "</font></td>");
                    getUserSign(sid, sign);
                   
                    h.add("<td width=152 height=47 align=left valign=top>");
                    h.add("<img src=\"" + sign.getSignFilePath().trim() + "\" width=168 height=110></td>");
                    h.add("</tr></table>");
                }
                
            }
        }
        else
        {
             h.add("<p><font size='2'>"+resourceManager.getString("CoverPage.ActiveSignature")+"</font></p>");
        }
        h.add("</body></html>");
        File hFile = new File(getCacheFolder(sid) + Util.pathSep + doc.getId() 
                                                     + Util.pathSep + "CP.html");
        String err = "";
        VWCUtil.writeListToFile(h, hFile.getPath(), err, "UTF-8");
        return true;
    }
    // Start of Methods added for ARS ******************************************/
    /*
    Purpose:  Following methods are used by ARS
    Created by: Shanmugavalli.C
    Date: <20 July 2006>
    */    

    /*
     * To set Retention details of Document type @param - sessionid, @param-DocType
     * @return int 
     */
     public int setDTRetentionSettings(int sid, DocType docType)
     {
         Session session = getSession(sid);
         VWClient.printToConsole("sessionId in setDtRetentionSettings::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
             return vws.setDTRetentionSettings(session.room, sid, docType);
         }
         catch(Exception e)
         {
             return Error;
         }
     }

     /*
     * To get Retention details of Document type @param - sessionid, @param - DocType
     * @return int 
     */
 	public int getDTRetentionSettings(int sid, DocType docType)
 	{
 		Session session = getSession(sid);
 		 VWClient.printToConsole("sessionId in getDTRetentionSettings::"+sid);
 		if(session==null) return invalidSessionId;
 		VWS vws = getServer(session.server);
 		if (vws == null) return ServerNotFound;
 		try
 		{
 			Vector settings=vws.getDTRetentionSettings(session.room, sid, docType);
 			docType.setARSSettings(settings);
 			  VWClient.printToConsole("Active Error in getDTRetentionSettings--->"+vws.getActiveError());
 			//return vws.getActiveError();
 			 if (sid>0 && vws.getActiveError()<0 ){
             	return NoError;
             } else {
             	return vws.getActiveError();
             }
 		}
 		catch(Exception e)
 		{
 			return Error;
 		}
     }
 	public int getRetentionDocuments(int sid, Vector retentionDocs)
 	{
 		Session session = getSession(sid);
 		VWClient.printToConsole("sessionId in getRetentionDocuments::"+sid);
 		if(session==null) return invalidSessionId;
 		VWS vws = getServer(session.server);
 		if (vws == null) return ServerNotFound;
 		try
 		{
 			/*Purpose:	ARS method changed for Oracle script changes
 			* Name:	C.Shanmugavalli		Date:	08 Sep 2006 
 			 */
 			//retentionDocs = vws.getRetentionDocuments(session.room);
 			Vector retentionDocuments = new Vector();
 			retentionDocuments = vws.getRetentionDocuments(sid, session.room, retentionDocs);
 			retentionDocs.addAll(retentionDocuments);
 			 VWClient.printToConsole("Active Error in getRetentionDocuments--->"+vws.getActiveError());
 			//return vws.getActiveError();
 			 if (sid>0 && vws.getActiveError()<0 ){
             	return NoError;
             } else {
             	return vws.getActiveError();
             }
 		}
 		catch(Exception e)
 		{
 			return Error;
 		}
 	}
 	
 	public int getRetentionDocumentList(int sid, int retentionId, Vector retentionDocs)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRetentionDocumentList::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getRetentionDocumentList(session.room, sid, retentionId, retentionDocs);
            retentionDocs.addAll(ret);
            VWClient.printToConsole("Active Error in getRetentionDocumentList--->"+vws.getActiveError());
           //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }

      /*
     * To get Disposal action values
     * @return int
     */
 	public int getDisposalActions(int sid, Vector disposalActs)
 	{
 		Session session = getSession(sid);
 		VWClient.printToConsole("sessionId in getDisposalActions::"+sid);
 		if(session==null) return invalidSessionId;
 		VWS vws = getServer(session.server);
 		if (vws == null) return ServerNotFound;
 		try
 		{
 			vws.getDisposalActions(session.room, sid, disposalActs);
 			VWClient.printToConsole("Active Error in getDisposalActions--->"+vws.getActiveError());
 			//return vws.getActiveError();
 			 if (sid>0 && vws.getActiveError()<0 ){
             	return NoError;
             } else {
             	return vws.getActiveError();
             }
 		}
 		catch(Exception e)
 		{
 			return Error;
 		}
     }
     
 	/*
 	 * To add ARS session obj into sessions Hashtable
 	 */
     public void setSession(int sessionId, String serverName, String roomName, String arsAdminUser)
     {
          	try{
         		Session s = new Session(sessionId, serverName, roomName, cache, arsAdminUser);
         		sessions.put(new Integer(sessionId), s);
         		new File(s.cacheFolder).mkdirs();
         	}catch(Exception ex){
         		//return Error;
         	}
      }

      public int unFreezeDocument(int sid, Document doc)
      {
             Session session = getSession(sid);
             VWClient.printToConsole("sessionId in unFreezeDoc::"+sid);
             if(session==null) return invalidSessionId;
             VWS vws = getServer(session.server);
             if (vws == null) return ServerNotFound;
             try
             {
             	return vws.unFreezeDocument(session.room, sid, doc.getId());
             }
             catch(Exception e)
             {
                 return Error;
             }
      }
      public int freezeDocument(int sid, Document doc){
    	  return freezeDocument(sid, doc, "", "0");
      }
      
      public int freezeDocument(int sid, Document doc, String docName, String retentionId)
      {
             Session session = getSession(sid);
             VWClient.printToConsole("sessionId in freezeDoc::"+sid);
             if(session==null) return invalidSessionId;
             VWS vws = getServer(session.server);
             if (vws == null) return ServerNotFound;
             try
             {
            	 retentionId = String.valueOf(0);//For manual freeze, set the retention id to zero.
                 return vws.freezeDocument(session.room, sid, doc.getId(), doc.getName(), retentionId);
             }
             catch(Exception e)
             {
                 return Error;
             }
      }

      public int getFreezeDocs(int sid, Vector docs)
      {
             Session session = getSession(sid);
     		 VWClient.printToConsole("sessionId in getFreezeDocs::"+sid);
             if(session==null) return invalidSessionId;
             VWS vws = getServer(session.server);
             if (vws == null) return ServerNotFound;
             try
             {
             	
             	Vector output = new Vector();
             	output = vws.getFreezeDocs(session.room, sid);
             	docs.addAll(output);
             	VWClient.printToConsole("Active Error in getFreezeDocs--->"+vws.getActiveError());
                //return vws.getActiveError();
             	 if (sid>0 && vws.getActiveError()<0 ){
              	   return NoError;
                 } else {
              	   return vws.getActiveError();
                 }
             }
             catch(Exception e)
             {
             	//VWSLog.err("isDocFreezed error "+doc.getId()+e.getMessage());
                 return Error;
             }
       }
      
      
      public int isDocFreezed(int sid, Document doc)
      {
             Session session = getSession(sid);
             VWClient.printToConsole("sessionId in isDocFreeze::"+sid);
             if(session==null) return invalidSessionId;
             VWS vws = getServer(session.server);
             if (vws == null) return ServerNotFound;
             try
             {
             	int ret = -1;
             	ret = vws.isDocFreezed(session.room, sid, doc.getId());
                return ret;
             }
             catch(Exception e)
             {
             	//VWSLog.err("isDocFreezed error "+doc.getId()+e.getMessage());
                 return Error;
             }
       }
      /* Purpose:	Master Switch off added ARS in registry
      * Created By: C.Shanmugavalli		Date:	20 Sep 2006
      */
      public int isARSEnabled(int sid){
      	 Session session = getSession(sid);
      	  VWClient.printToConsole("sessionId in isARSEnabled::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
        try
        {
        	int isARSEnable = vws.getARSEnable(sid);
         	//VWSLog.err("isARSEnable "+isARSEnable);
         	return isARSEnable;
        }catch(Exception e)
        {
            return Error;
        }
    }
    public int getARSReportDocTypes(int sid, Vector getARSReportDocTypes) throws RemoteException
    {  
    	  Session session = getSession(sid);
          VWClient.printToConsole("sessionId in getARSReportDocTypes::"+sid);
          if(session==null) return invalidSessionId;
          VWS vws = getServer(session.server);
          if (vws == null) return ServerNotFound;
          try
          {
        	  Vector reportDocTypes = new Vector();
        	  reportDocTypes = vws.getARSReportDocTypes(sid, session.room, getARSReportDocTypes);
        	  getARSReportDocTypes.addAll(reportDocTypes);
        	  return NoError;
          }catch(Exception e)
          {
              return Error;
          }
    }
      /* Purpose:	Created client method for DTC to know the user belongs to which scheme 
      * Created By: C.Shanmugavalli		Date:	24 Oct 2006
      */      
      public int getSSType(int sid){
     	 Session session = getSession(sid);
     	  VWClient.printToConsole("sessionId in getSSType::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
	        {
	            String SSType = vws.getSSType();
	            //VWSLog.err(" SSType "+SSType);
	            if(SSType.equalsIgnoreCase(SSCHEME_ADS))
	            	return 1; // for ADS
	            else if(SSType.equalsIgnoreCase(SSCHEME_NDS))
	            	return 2; // for NDS
	            else if(SSType.equalsIgnoreCase(SSCHEME_SSH))
	            	return 3; // for SSH
	            else if(SSType.equalsIgnoreCase(SCHEME_LDAP_ADS) || 
	            		SSType.equalsIgnoreCase(SCHEME_LDAP_NOVELL)||
	            		SSType.equalsIgnoreCase(SCHEME_LDAP_LINUX))
	            	return 4; // for SSH
	            return Error;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
      }
      	
      // End of methods added for ARS**********************************************/
      /*
      * Added for Novell Netware issue 736 - MDSS server ping issue
      */
      public int getIdleTime(int sid,String roomName){
    	int idleTime = 0;
    	//System.out.println(" sid /// roomName " + sid + " /// " + roomName);
    	Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getIdleTime::"+sid);
        if(session==null) return invalidSessionId;
        
        VWS vws = getServer(session.server);
        if(vws == null) return 0;
        try
        {
        	//System.out.println(" Call the Method ");
            idleTime = vws.getRoomIdleTime(roomName);
        }
        catch(Exception e)
        {
        	//VWSLog.err(" Error in getting idle time "+e.getMessage());
            return Error;
        }
        
    	return idleTime;
    }
    
      /*
       * Added for Novell Netware issue 736 - MDSS server ping issue
       */
      
      public int isRoomIdle(int sid,String roomName){

    	int isIdle = 0;
    	Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isRoomIdle::"+sid);
        if(session==null) return invalidSessionId;
        
        VWS vws = getServer(session.server);
        if(vws == null) return destinationRoomServerInactive;
        try
        {
        	//System.out.println(" Call the Method ");
            isIdle = vws.isRoomIdle(sid,roomName);
        }
        catch(Exception e)
        {
            return invalidSessionId;
        }            	
    	return isIdle;
    }

      /*
       * Not in use , will be removed later - Valli
       */
    private boolean isDSSUsed(){
    	return useDSS;
    }
    private void setUseDSS(boolean useDSS){
    	this.useDSS = useDSS;
    }
    private final String SIGN = "0B9B5177-1011-4BBF-B617-455288043D4A";
    private class searchThread extends java.lang.Thread 
    {
        private VWS m_vws; 
        private String m_room;
        private int m_sid;
        private Search m_srch; 
        private long m_hWnd;
        private int m_action;
        private int m_customList;
        private String m_customIndexList;
        private boolean m_docsOnly;
       
        public searchThread(){}
        /*Constructor changed to handle document list and hit list enhancement 
        * Valli 13 May 2007
        */
        public searchThread(int action, VWS vws, String room, int sid, 
                     Search srch, long hWnd, int customList, String customIndexList, boolean docsOnly)
        {
            m_action = action;
            m_vws = vws; 
            m_room = room;
            m_sid = sid;
            m_srch = srch; 
            m_hWnd = hWnd;
            m_customList = customList;
            m_customIndexList = customIndexList;
            m_docsOnly = docsOnly;
        }
        public void run() 
        {
            try
            {
                if(m_action == 1) //Find
                {
                	//boolean docsOnly = true;
                    //int nRetVal = m_vws.find(m_room, m_sid, m_srch.getId(), 
                    //                    m_srch.getHitListId(), m_srch.getRowNo(), m_docsOnly);
                	//find signature changed for enh 1157
                    int nRetVal = m_vws.find(m_room, m_sid, m_srch, m_docsOnly);
                    if(!m_docsOnly){	
	                    Vector indexInfo = new Vector();
	                 	getCustomIndexIdsFromRegistry(m_sid,1, 0, indexInfo);
	                 	// if customIndexList values are empty should not call setCustomisedIndexList. Valli  21 May 2007
	                 	String customIndexList = "";
	                 	if(indexInfo!=null && indexInfo.size()>0)
	                 		customIndexList = removeNegativeIds(indexInfo.get(0).toString());
	                 	//Commented and moved to SearchThread class after executing find method.                 	
/*	                 	if(customIndexList!=null && !customIndexList.equals(""))
	                 		m_vws.setCustomisedIndexList(1, m_sid, m_room, m_srch.getNodeId(), m_srch.getRowNo() , customIndexList);
*/                    }

                    VWCUtil.sendFindDataReady(m_hWnd, nRetVal);
                }
                if(m_action == 2) //FindResult
                {
                    
                    Vector docs = new Vector();
                    if(m_customList == 0){
                    	Vector ret =  m_vws.getFindResult(m_room, m_sid, m_srch, docs);                
                    	VWCUtil.sendResults(m_hWnd, ret);
                    }
                    else if(m_customList == 1){
                    	Vector ret =  m_vws.getCustomisedSearchResult(m_room, m_sid, m_customIndexList, m_srch, docs);
                    	//VWSLog.err("result "+ret.toString());
                    	VWCUtil.sendResults(m_hWnd, ret);
                    }
                }
            }
            catch(Exception e){}
        }
    }
    
    /*Start of DRS methods*/
    
    public int sendDocInManualRoute(int sid, int docId, String docName, int routeId, String routeName, int taskSequence){
    	return sendDocInManualRoute(sid, docId, docName, routeId, routeName, taskSequence, 0);
    }
    /*CV2019 merges from SIDBI line**/
    public int sendDocInManualRoute(int sid, int docId, String docName, int routeId, String routeName, int taskSequence, int signId) {
    	return sendDocInManualRoute(sid, docId, docName, routeId, routeName, taskSequence, signId, null);
    }
    public int sendDocInManualRoute(int sid, int docId, String docName, int routeId, String routeName, int taskSequence, int signId, String secureURL){
    	Session session = getSession(sid);
    	int result = 0;
    	VWClient.printToConsole("sessionId in VWClient.sendDocInManualRoute::::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
    	Vector vTaskInfo = new Vector();
    	StringBuilder emailUsersList = null; 
           	try
            {
    		Vector ret = vws.getRouteTaskInfo(session.room, sid, routeId, taskSequence, vTaskInfo);
    		vTaskInfo.addAll(vectorToRouteTask(ret));
    		if(vTaskInfo!=null&& vTaskInfo.size()>0)
    		{
    			VWClient.printToConsole("Inside vTaskInfo not null");
	    		RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(0);	    		
	    		int routeTaskId = routeTaskInfo.getRouteTaskId();
	    		int TaskSeq = routeTaskInfo.getTaskSequence();
	    		Vector vRouteUsers = new Vector();
	    		ret = vws.getRouteUsers(session.room, sid, routeTaskId, vRouteUsers, docId);
	    		VWClient.printToConsole("After calling getRouteUsers");
	    		VWClient.printToConsole("getRouteUsers...."+ret);
	    		ArrayList listRouteUsers = vectorToRouteUsersArrayList(ret);
	    		if(listRouteUsers!=null && listRouteUsers.size()>0){
	    			/*CV2019 merges from SIDBI line------------------------------------------------***/
	    			boolean sidbiCustomization = vws.getSidbiCustomizationFlag(session.room, sid);
	    			VWClient.printToConsole("After verifying sidbicustomization");
    				for(int k=0; k<listRouteUsers.size(); k++)
    				{
    					RouteUsers routeUser = (RouteUsers)listRouteUsers.get(k);
    					int routeUserId =routeUser.getRouteUserId();
    					String groupName = routeUser.getGroupName();
    					String userName = "";
    					//Manual route will be called only by Client and not AdminWise, so sending empty string for userName
    					VWClient.printToConsole("Before setRouteHistory call...");
    					result = vws.setRouteHistory(session.room, sid, docId, routeId, TaskSeq, routeUserId, "", userName, userName, signId, 0, groupName);//routeMasterId = 0
    					VWClient.printToConsole("after setRouteHistory call..."+result);
    					try{
    						VWClient.printToConsole("routeUser.getSendEmail()......"+routeUser.getSendEmail());
    						if(routeUser.getSendEmail() == 1){
    							VWClient.printToConsole("routeTaskInfo.getEmailToPrvTaskUsers()......"+routeTaskInfo.getEmailToPrvTaskUsers());    							
    							// sendMailWithAttachment should be called only if the emailToPreviousUser check should be disabled(0)
    							//if ((!sidbiCustomization) || (sidbiCustomization == true && routeTaskInfo.getEmailToPrvTaskUsers() != 1)) {
    							if (!sidbiCustomization) {
    								VWClient.printToConsole("before calling sendMailWithAttachment.....");  
	    							vws.sendMailWithAttachment(session.room, session.server, sid, routeUser.getUserName(), docName, routeName, docId, new Vector(),
	    									null, Route_Status_Pending, null, null, "", null, secureURL);
	    							VWClient.printToConsole("after calling sendMailWithAttachment.....");  
    							} else {
    								VWClient.printToConsole("inside else......");
    								VWClient.printToConsole("emailUsersList...."+emailUsersList);
    								if (emailUsersList == null) {
    									emailUsersList = new StringBuilder();
    									emailUsersList.append(routeUser.getUserName());
    								} else {
    									emailUsersList.append(";"+routeUser.getUserName());
    								}
    								VWClient.printToConsole("emailUsersList.tostring()...."+emailUsersList.toString());
    							}
    						}
    					}catch(Exception ex){
    						
    					}
    				}
	    		}
	    		VWClient.printToConsole("Final emailUsersList....."+emailUsersList);
	    		if (emailUsersList != null && emailUsersList.length() > 0) {
	    			VWClient.printToConsole("Before sendMailWithAttachment call..");
	    			vws.sendMailWithAttachment(session.room, session.server, sid, emailUsersList.toString(), docName, routeName, docId, new Vector(),
							null, Route_Status_Pending, null, null, "", null, secureURL);
				 }
    		}
    		/*------------------------End of SIDBI merges--------------------------------------*/
            }catch(Exception e){
            	 return Error;
            }
    	return NoError;
    }    
    /**
     * Method added to send Email From WorkFlow Update.
     * Enhancement :- WorkFlow Update CV83B5 Date:-22/12/2015
     * @param sid
     * @param docId
     * @param docName
     * @param routeId
     * @param routeName
     * @param taskSequence
     * @param refId
     * @return
     */
    public int sendWorkFlowUpdateMail(int sid, int docId, String docName, int routeId, String routeName, int taskSequence,int refId){
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in sendWorkFlowUpdateMail::"+sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	Vector vTaskInfo = new Vector();
    	StringBuilder emailUsersList = null;
    	try
    	{
    		Vector ret = vws.getRouteTaskInfo(session.room, sid, routeId, taskSequence, vTaskInfo);
    		vTaskInfo.addAll(vectorToRouteTask(ret));
    		if(vTaskInfo!=null&& vTaskInfo.size()>0)
    		{
    			RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(0);
    			int routeTaskId = routeTaskInfo.getRouteTaskId();
    			int TaskSeq = routeTaskInfo.getTaskSequence();
    			Vector vRouteUsers = new Vector();
    			ret = vws.getRouteUsers(session.room, sid, routeTaskId, vRouteUsers, docId);
    			ArrayList listRouteUsers = vectorToRouteUsersArrayList(ret);
    			if(listRouteUsers!=null && listRouteUsers.size()>0){
    				/*CV2019 merges from SIDBI line------------------------------------------------***/
    				boolean sidbiCustomization = vws.getSidbiCustomizationFlag(session.room, sid);
    				for(int k=0; k<listRouteUsers.size(); k++)
    				{
    					RouteUsers routeUser = (RouteUsers)listRouteUsers.get(k);
    					int routeUserId =routeUser.getRouteUserId();
    					String groupName = routeUser.getGroupName();
    					String userName = "";
    					try{
    						if(routeUser.getSendEmail() == 1){
    							// sendMailWithAttachment should be called only if the emailToPreviousUser check should be disabled(0)
    							//if ((!sidbiCustomization) || (sidbiCustomization == true && routeTaskInfo.getEmailToPrvTaskUsers() != 1)) {
    							if (!sidbiCustomization) {
    								vws.sendMailWithAttachment(session.room, session.server, sid, routeUser.getUserName(), docName, routeName, docId, new Vector(),
	    									null, Route_Status_Pending, null, null, "");
	    							vws.delUpdateWorkFlow(session.room, sid, refId);
    							} else {
    								if (emailUsersList == null) {
    									emailUsersList = new StringBuilder();
    									emailUsersList.append(routeUser.getUserName());
    								} else {
    									emailUsersList.append(";"+routeUser.getUserName());
    								}
    							}
    						}
    					}catch(Exception ex){
    						vws.delUpdateWorkFlow(session.room, sid, refId);
    					}
    				}
    				if (emailUsersList != null && emailUsersList.length() > 0) {
    					vws.sendMailWithAttachment(session.room, session.server, sid, emailUsersList.toString(), docName, routeName, docId, new Vector(),
								null, Route_Status_Pending, null, null, "");
	   				 }
    				/*------------------------End of SIDBI merges--------------------------------------*/
    			}
    		}
    	}catch(Exception e){
    		return Error;
    	}
    	return NoError;
    }
    
    
    
    public int getRouteSummary(int sid, int docId, int mode, Vector routeSummary){
    	VWClient.printToConsole("inside getRouteSummary.......");
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getRouteSummary::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector routeDetails = new Vector();
        	routeDetails = vws.getRouteSummary(session.room, sid, docId, mode);
        	routeSummary.addAll(routeDetails);
        	//printToConsole("routeSummary " + routeSummary.size());
        	//printToConsole("routeSummary " + routeSummary);
        }catch(Exception ex){
        }
    	return NoError;
    }
    
    
   
    /*CV2019 merges from SIDBI line------------------------------------------------***/
    public int getRouteNewSummary(int sid, int docId, int mode, Vector routeSummary){
    	VWClient.printToConsole("inside getRouteNewSummary");
        Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getRouteNewSummary::"+sid);
    	VWClient.printToConsole("getRouteNewSummary:: docId :"+docId+" mode :"+mode);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector routeDetails = new Vector();
        	routeDetails = vws.getRouteNewSummary(session.room, sid, docId, mode);
        	VWClient.printToConsole("routeDetails :::"+routeDetails);
        	routeSummary.addAll(routeDetails);
        	//printToConsole("routeSummary " + routeSummary.size());
        	//printToConsole("routeSummary " + routeSummary);
        }catch(Exception ex){
        	VWClient.printToConsole("getRouteNewSummary ::::"+ex.getMessage());
        }
    	return NoError;
    }
    /*------------------------End of SIDBI merges--------------------------------------*/
    public int getDocumentAuditDetails(int sid, int docId, int mode, Vector auditDetails){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentAuditDetails::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector vAuditDetails = new Vector();
        	vAuditDetails = vws.getDocumentAuditDetails(session.room, sid, docId, mode);
        	auditDetails.addAll(vAuditDetails);
        }catch(Exception ex){
        }
    	return NoError;
    }

    
    public int isRouteNameFound(int sid, String routeName){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isRouteNameFound::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int routeId =0;
        try{        	
        	routeId = vws.isRouteNameFound(session.room, sid, routeName);        	
        	//printToConsole("isRouteNameFound " + routeId);        	
        }catch(Exception ex){}
    	return routeId;
    }
    
    public int isRouteLocationFound(int sid, String nodeId){
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isRouteLocationFound::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int routeId =0;
        try{        	
        	routeId = vws.isRouteLocationFound(session.room, sid, nodeId);        	
        	//printToConsole("isRouteLocationFound " + routeId);        	
        }catch(Exception ex){}
    	return routeId;
    }
    
    public int getRouteInfo(int sid,  int routeId, Vector routeInfo){
    	
    	return getRouteInfo(sid, routeId, 0, routeInfo);
    }
    public int getRouteInfo(int sid,  int routeId, int routeUserLevel, Vector vRouteInfo)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRouteInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getRouteInfo(session.room, sid, routeId, vRouteInfo);
            VWClient.printToConsole("getRouteInfo return value ::"+ret);
            vRouteInfo.addAll(vectorToDocRoute(ret));
            if(routeId!=-1 && routeId!=0){
	            Vector routeIndexInfo = new Vector();
	            ret = vws.getRouteIndexInfo(session.room, sid, routeId, routeIndexInfo);
	            VWClient.printToConsole("getRouteIndexInfo return value in getRouteInfo ::"+ret);
	            ArrayList routeIndexArrayList = vectorToRouteIndexArray(ret);	            
	            Vector vRouteTaskInfo = new Vector();
	            int taskSequence = 0;
	            ret = vws.getRouteTaskInfo(session.room, sid, routeId, taskSequence, vRouteTaskInfo);
	            VWClient.printToConsole("getRouteTaskInfo return value in getRouteInfo ::"+ret);
	            ArrayList routeTaskArrayList = vectorToRouteTaskList(ret);
	            if(routeTaskArrayList!=null && routeTaskArrayList.size()>0){
	            	for(int i = 0; i< routeTaskArrayList.size(); i++){
	            		RouteTaskInfo routeTaskInfo = (RouteTaskInfo)routeTaskArrayList.get(i);
	            		int routeTaskId = routeTaskInfo.getRouteTaskId();
	            		Vector vRouteUsers = new Vector();
	            		ret = vws.getRouteUsers(session.room, sid, routeTaskId, vRouteUsers);
	            		VWClient.printToConsole("getRouteUsers return value in getRouteInfo ::"+ret);
	            		ArrayList routeUserArrayList = vectorToRouteUsersArrayList(ret);
	            		routeTaskInfo.setRouteUsersList(routeUserArrayList);
	            		routeTaskArrayList.remove(i);
	            		routeTaskArrayList.add(i, routeTaskInfo);
	            	}
	            }
	            if(vRouteInfo!=null && vRouteInfo.size()>0){
	            	RouteInfo routeInfo = (RouteInfo)vRouteInfo.get(0);
	            	routeInfo.setRouteIndexList(routeIndexArrayList);
	            	routeInfo.setRouteTaskList(routeTaskArrayList);
	            	vRouteInfo.clear();
	            	vRouteInfo.add(routeInfo);
	            }
            }
            VWClient.printToConsole("Active Error in getRouteInfo--->"+vws.getActiveError());
            //return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
        	VWClient.printToConsole("Exception in getRouteInfo method generalErrCode -1, message : "+e.getStackTrace());
            return Error;
        }
    }
    public int getRouteImage(int sid, int routeId, RouteInfo routeInfo)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRouteImage::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	File routeImageFile=new File(routeInfo.getImageFilePath());
            VWDoc routeImageDoc = new VWDoc();
            routeImageDoc=vws.getRouteImage(session.room, sid, routeId, routeImageDoc);
            routeImageDoc.setDstFile(routeImageFile.getPath());
            routeImageDoc.writeContents();
            VWClient.printToConsole("Active Error in getRouteImage--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
            	return NoError;
            } else {
            	return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getTaskInfo(int sid,  int routeId, int taskSequence, Vector vRouteTaskInfo)
    {
    	Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getTaskInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector ret = vws.getRouteTaskInfo(session.room, sid, routeId, taskSequence, vRouteTaskInfo);
        	vRouteTaskInfo.addAll(vectorToRouteTask(ret));
            return NoError;
        }catch(Exception e){
            return Error;
        }
    }
    
    public int getRouteUsers(int sid,  int routeTaskId, Vector vRouteUsers)
    {
    	return getRouteUsers(sid, routeTaskId, vRouteUsers, 0);
    }
    public int getRouteUsers(int sid,  int routeTaskId, Vector vRouteUsers, int docId)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getRouteUsers::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector ret = vws.getRouteUsers(session.room, sid,  routeTaskId, vRouteUsers, docId);
        	vRouteUsers.addAll(vectorToRouteUsers(ret));
        	 VWClient.printToConsole("Active Error in getRouteUsers--->"+vws.getActiveError());
           // return vws.getActiveError();
        	 if (sid>0 && vws.getActiveError()<0 ){
             	return NoError;
             } else {
             	return vws.getActiveError();
             }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getDocumentsInRoute(int sid, Vector documentInRoute){
    	VWClient.printToConsole("inside getDocumentsInRoute.....3");
    	return getDocumentsInRoute(sid, 0, documentInRoute);
    }
    public int getDocumentsInRoute(int sid, int docId, Vector documentInRoute){
    	return getDocumentsInRoute(sid, docId, documentInRoute, 0);
    }
    public int getDocumentsInRoute(int sid, int docId, Vector documentInRoute, int sourceType){
    	// When user pass the docid as zero, it will retrieve all the docments available in Route will come
    	// When user pass the docid, it will return either 0 or 1. If '0' returns document is not in the route otherwise document in route. 
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getDocumentsInRoute::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retValue = 0;
        try{
        	Vector ret = vws.getDocumentsInRoute(session.room, sid, docId, sourceType);
        	documentInRoute.addAll(ret);
        	
        }catch(Exception ex){
        }
        /**
         * CV2019 merges from SIDBI line - else if condition has added
         */
        if(docId > 0 && documentInRoute != null && documentInRoute.size() > 0 && documentInRoute.get(0).equals("1")){
        	return 1;
        }//Else if condition added for enhancement  Sow :-2117_1155 Clearly identify documents that are in workflow in a folder by color in DTC.
        else if(docId > 0 && documentInRoute != null && documentInRoute.size() > 0 && documentInRoute.get(0).equals("2")){
        	return 2;
        } else if(docId > 0 && documentInRoute != null && documentInRoute.size() > 0 && documentInRoute.get(0).equals("3")){
        	return 3;
        }
    	return NoError;
    }
    public int deleteRouteInfo(int sid, int routeId, int isRouteNotUsed){
    	
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteRouteInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retValue = 0;
        try{
        	//In a VW_DelRouteInfo stored procedure, deleting user will be taken care.
        	//retValue = vws.deleteDocumentUsers(session.room, sid, routeId);
        	retValue = vws.deleteRouteInfo(session.room, sid, routeId, isRouteNotUsed);
        }catch(Exception ex){
        	//printToConsole("deleteDocumentRoute Exception ex " + ex.getMessage());
        	return Error;
        }
    	return retValue;
    }
    
    public int deleteRouteTaskUserIndexInfo(int sid, int routeId, int markAsDelete, int indexBaseRoute){    	
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in deleteRouteTaskUserIndexInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retValue = 0;
        try{
        	retValue = vws.deleteRouteTaskUserIndexInfo(session.room, sid, routeId, markAsDelete, indexBaseRoute);
        }catch(Exception ex){
        	//printToConsole("deleteDocumentRoute Exception ex " + ex.getMessage());
        	return Error;
        }
    	return retValue;
    }
    
    public int isRouteInUse(int sid, int routeId, int mode){
    	
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in isRouteInUse::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retValue = 0;
        try{
        	retValue = vws.isRouteInUse(session.room, sid, routeId, mode);
        }catch(Exception ex){
        	//printToConsole("isRouteInUse Exception ex " + ex.getMessage());
        	return Error;
        }
    	return retValue;
    }
    public int isUserInRoute(int sid, int principalId, Vector routeList){

	Session session = getSession(sid);
    VWClient.printToConsole("sessionId in isUserInRoute::"+sid);
	if(session==null) return invalidSessionId;
	VWS vws = getServer(session.server);
	if (vws == null) return ServerNotFound;
	int retValue = 0;
	try{
	    Vector result = new Vector();
	    result = vws.isUserInRoute(session.room, sid, principalId);
	    retValue = result.size();
	    routeList.addAll(result);
	}catch(Exception ex){
	    //printToConsole("isRouteInUse Exception ex " + ex.getMessage());
	    return Error;
	}
	return retValue;
    }

    /*public int setRouteInfo(int sid, RouteInfo routeInfo){
    	return setRouteInfo(sid, routeInfo, 0, false);
    }*/
    public int setRouteInfo(int sid, RouteInfo routeInfo, int routeId, int markAsDelete, int routeValidity)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setRouteInfo::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        int retValue = 0;        
        String desc = null;
        try
        {   //Code added for workflow validation (24-11-2014)
        	ArrayList routeTaskList1 = routeInfo.getRouteTaskList();
        	if(routeTaskList1!=null&&routeTaskList1.size()>0)
        	{
        		int sequence=1;
        		for(int i=0;i<routeTaskList1.size();i++)
        		{
        			RouteTaskInfo routeTaskInfo = (RouteTaskInfo)routeTaskList1.get(i);
        			int routeTaskSequence = routeTaskInfo.getTaskSequence();
        			/**
        			 * Enchancment:- WorkFlow Validation
        			 * Added for worflow validation to check if sequence mismatch
        			 * Date:-24/11/2014
        			 * 
        			 */
        			if(sequence!=routeTaskSequence){
        				JOptionPane.showMessageDialog(null, resourceManager.getString("WorkFlowValidation.FailtoSave"));
        				return -971;
        			}
        			++sequence;

        		}
        	}

        	if(routeTaskList1!=null && routeTaskList1.size()>0){
        		for(int i = 0; i<routeTaskList1.size(); i++){
        			RouteTaskInfo routeTaskInfo = (RouteTaskInfo)routeTaskList1.get(i);
        			ArrayList routeUserList1 = routeTaskInfo.getRouteUsersList();
        			if(routeTaskList1!=null&&routeTaskList1.size()>0){
        				for(int j=0;j<routeUserList1.size();j++)
        				{
        					RouteUsers routeUsers = (RouteUsers)routeUserList1.get(j);
        					int chcekGroupName = vws.checkGroup(session.room, sid, routeUsers.getGroupName());
        					int checkUserVariable = vws.checkUser(session.room, sid, routeUsers.getUserName());
        					/**
        					 * Enchancment:- WorkFlow Validation 
        					 * Added for worflow validation to check if usres and group mismatch
        					 * Date:-24/11/2014
        					 */
        					if(chcekGroupName>0&&checkUserVariable>0){
        						if(routeUsers.getGroupName().equalsIgnoreCase(routeUsers.getUserName())){
        							JOptionPane.showMessageDialog(null, resourceManager.getString("WorkFlowValidation.InvalidTaskProp"));
        							return -971;
        						}
        					}
        					else if(chcekGroupName>0&&checkUserVariable==0){
        						if(routeUsers.getGroupName().equalsIgnoreCase(routeUsers.getUserName())){
        							JOptionPane.showMessageDialog(null, resourceManager.getString("WorkFlowValidation.InvalidTaskProp"));
        							return -971;
        						}
        					}

        				}
        			}
        		}
        	}//End of workflow validation (24-11-2014)
        	int rId = routeId;
        	if(routeId==0){
        		rId = vws.setRouteInfo(session.room, sid, routeInfo);
        	}
        	else{
        		vws.setRouteInfo(session.room, sid, routeInfo);
        	}
        	if(rId!=0)
        		routeInfo.setRouteId(rId);
        	else
        		routeInfo.setRouteId(routeId);
        	
        	//Delete records from all 3 tables for the particular route id passed, if a route update is called.
        	if(rId>0){
        		int indexBase = routeInfo.getStartAction().equalsIgnoreCase("2") ? 1 : 0;
        		/**
				 * Blocking for invalid routes not to delete the entries from the table
				 * routeValidity is 1 for invalid routes 
				 */
        		if(routeValidity!=INVALIDROUTE)
        			deleteRouteTaskUserIndexInfo(sid,rId,markAsDelete, indexBase);
        	}
        	
        	//inserting route index info
        	if(routeInfo.getStartAction().equalsIgnoreCase("2")){
	        	ArrayList routeIndexList = routeInfo.getRouteIndexList();
	        	if(routeIndexList!=null && routeIndexList.size()>0){
	        		for(int i = 0; i<routeIndexList.size(); i++){
	        			RouteIndexInfo routeIndexInfo = (RouteIndexInfo)routeIndexList.get(i);
	        			routeIndexInfo.setRouteId(rId);
	        			vws.setRouteIndexInfo(session.room, sid, routeIndexInfo);
	        		}
        	}           
        	}
            ArrayList routeTaskList = routeInfo.getRouteTaskList();
            if(routeTaskList!=null && routeTaskList.size()>0){
	            for(int i = 0; i<routeTaskList.size(); i++){
	            	RouteTaskInfo routeTaskInfo = (RouteTaskInfo)routeTaskList.get(i);
	            	routeTaskInfo.setRouteId(rId);
	        		int routeTaskId = vws.setRouteTaskInfo(session.room, sid, routeTaskInfo);
		        	ArrayList routeUserList = routeTaskInfo.getRouteUsersList();
	       			vws.setRouteUsers(session.room, sid, routeUserList, rId, routeTaskId);
	            }
            }
            /**
             * For invalid route need to Sync RouteUsers;
             * This procedure will get called only for the invalid routes on updation process 
             */
            if(routeValidity==INVALIDROUTE){
            	vws.syncInvalidRouteUsers(session.room, sid, routeId);
            }
            /**
             * Enhancement :- WorkFlow Updated
             * Version:- After CV83B3
             */
            Vector resultValidate=new Vector();
            String retValidateVal="";
         
          
            resultValidate= vws.getDRSValidateUpdateRoute(session.room, sid,rId,routeInfo.getRouteName());
            retValidateVal =resultValidate.get(0).toString();
            if(retValidateVal.length()>0&&(!(retValidateVal.equalsIgnoreCase("1")))){
            	JOptionPane.showMessageDialog(null,retValidateVal);
            	return -336;
            }//End of WorkFlow update procedure call.
            //For New Route
            if(routeId == 0){
            	desc = "New "+ WORKFLOW_MODULE_NAME +" : <"+routeInfo.getRouteName()+"> Create By User : <"+session.user+">";
            	vws.addATRecord(session.room, sid, rId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_NEW_ROUTE, desc);
            }else{//For Update Route
            	desc = "Update "+ WORKFLOW_MODULE_NAME +" : <"+routeInfo.getRouteName()+"> Updated By User : <"+session.user+">";
            	vws.addATRecord(session.room, sid, rId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_UPDATE_ROUTE, desc);
            }
            return rId;
        }
        catch(Exception e)
        {  
        	//printToConsole("setRouteUsers Exception  " + e.getMessage());      	
            return Error;
        }
    }
    
    
    private Vector vectorToDocRoute(Vector v)
    {
        Vector vRouteInfo = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
        	RouteInfo routeInfo = new RouteInfo((String) v.get(i));
        	vRouteInfo.add(routeInfo);
        }
        return vRouteInfo;
    }
    private Vector vectorToRouteIndex(Vector v)
    {
        Vector routeIndex = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
        	RouteIndexInfo routeIndexInfo = new RouteIndexInfo((String) v.get(i));
        	routeIndex.add(routeIndexInfo);
        }
        return routeIndex;
    }
    private ArrayList vectorToRouteIndexArray(Vector v)
    {
        Vector routeIndex = new Vector();
        ArrayList routeIndexList = new ArrayList();
        for (int i = 0; i < v.size(); i++)
        {
        	RouteIndexInfo routeIndexInfo = new RouteIndexInfo((String) v.get(i));
        	routeIndexList.add(routeIndexInfo);
        }
        return routeIndexList;
    }
    private Vector vectorToRouteTask(Vector v)
    {
        Vector routeTask = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
        	RouteTaskInfo routeTaskInfo = new RouteTaskInfo((String) v.get(i));
        	routeTask.add(routeTaskInfo);
        }
        return routeTask;
    }
    private ArrayList vectorToRouteTaskList(Vector v)
    {
    	ArrayList routeTaskArrayList = new ArrayList();
        for (int i = 0; i < v.size(); i++)
        {
        	RouteTaskInfo routeTaskInfo = new RouteTaskInfo((String) v.get(i));
        	routeTaskArrayList.add(routeTaskInfo);
        }
        return routeTaskArrayList;
    }
    // Need to remove later. Valli
    private ArrayList vectorToRouteUsersArrayList(Vector v)
    {
    	ArrayList routeUsersArrayList = new ArrayList();
        for (int i = 0; i < v.size(); i++)
        {
        	RouteUsers routeUsers = new RouteUsers((String) v.get(i));
        	routeUsersArrayList.add(routeUsers);
        }
        return routeUsersArrayList;
    }
    
    
    private Vector vectorToRouteMaster(Vector toDoRouteList, Vector v)
    {
        for (int i = 0; i < v.size(); i++)
        {
        	RouteMasterInfo routeMasterInfo = new RouteMasterInfo((String) v.get(i));
        	toDoRouteList.add(routeMasterInfo);
        }
        return toDoRouteList;
    }   

    private Vector vectorToRouteMaster(Vector escalatedDocsList, Vector v, int escalatedFlag)
    {
        for (int i = 0; i < v.size(); i++)
        {
        	RouteMasterInfo routeMasterInfo = new RouteMasterInfo((String) v.get(i), escalatedFlag);
        	escalatedDocsList.add(routeMasterInfo);
        }
        return escalatedDocsList;
    }
    private Vector vectorToRouteInfo(Vector docRoute, Vector v)
    {        
        for (int i = 0; i < v.size(); i++)
        {
        	StringTokenizer token = new StringTokenizer(v.get(i).toString(), Util.SepChar); 
        	RouteInfo routeInfo = new RouteInfo(Util.to_Number(token.nextToken()), token.nextToken());
        	docRoute.add(routeInfo);
        }
        return docRoute;
    }   
    private Vector vectorToRouteUsers(Vector v)
    {
        Vector ret = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
        	RouteUsers routeUsers = new RouteUsers( (String) v.elementAt(i));
            ret.addElement(routeUsers);
        }
        return ret;
    }
    public int setDocTypeRoutes(int sid, int routeId, int docTypeId) 
    {
    	Session session = getSession(sid);
        VWClient.printToConsole("sessionId in setDocTypeRoutes::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {   
        	vws.setDocTypeRoutes(session.room, sid, routeId, docTypeId);
        	
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }

    public int setDocTypeRoutes(int sid, ArrayList routeId, int docTypeId) 
    {
    	Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in setDocTypeRoutes::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {   
        	//printToConsole(" setDocTypeRoutes " + routeId.size());
        	vws.setDocTypeRoutes(session.room, sid, routeId, docTypeId);
        	
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }

    
    public int getDocTypeRoutes(int sid, int DocTypeId, Vector docTypeRoutes) 
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getDocTypeRoutes::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {   Vector docRouteUsers = new Vector();
        	docRouteUsers = vws.getDocTypeRoutes(session.room, sid, DocTypeId, docTypeRoutes);
        	docTypeRoutes.addAll(docRouteUsers);
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }
    
    public int setUserMail(int sid, int userId, String userMailId) 
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in setUserMail::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {   
        	vws.setUserMail(session.room, sid, userId,  userMailId);
        	
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }
    public int getUserMail(int sid, String userName, Vector userMailId) 
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getUserMail::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector userId = new Vector();
        	userId = vws.getUserMail(session.room, sid, userName, userId);
        	userMailId.addAll(userId);        	
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }
    
    public int getAssignedRoutesOfDocType(int sid, int docTypeId, Vector assignedRoutesOfDocType) 
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getAssignedRoutesOfDocType::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {   
        	Vector ret = new Vector();
        	ret = vws.getAssignedRoutesOfDocType(session.room, sid, docTypeId);
        	vectorToRouteInfo(assignedRoutesOfDocType, ret);
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }
    
    public int getToDoRouteList(int sid, int adminUser, int routeId,  Vector toDoRouteList){
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getToDoRouteList::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
    	try
        {
        	Vector ret = new Vector();
        	ret = vws.getToDoRouteList(session.room,adminUser, routeId, sid);
        	vectorToRouteMaster(toDoRouteList, ret);
        	
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }
    
    public int getRouteCompletedDocuments(int sid, int routeId, int adminUser, Vector vecRouteCompletedDocs){
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getRouteCompletedDocuments::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
    	try
        {
        	Vector ret = new Vector();
        	ret = vws.getRouteCompletedDocuments(session.room,sid, routeId, adminUser);
        	vectorToRouteMaster(vecRouteCompletedDocs, ret);        	
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }
// This method return documents which is completed by all task and waiting for process final Action
    public int getTaskCompletedDocuments(int sid, int routeId, Vector vecTaskCompletedDocs){
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getTaskCompletedDocuments::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
    	try
        {
        	Vector ret = new Vector();
        	ret = vws.getTaskCompletedDocuments(session.room, sid, routeId);
        	vectorToRouteMaster(vecTaskCompletedDocs, ret);        	
        }catch(Exception e)
        {
            return Error;
        }
    	return NoError;
    }

    
    public int getToDoRouteList(int sid, Vector toDoRouteList) {
    	try
    	{
    		int adminUser = 0;
    		int routeId = 0;
    		getToDoRouteList(sid, adminUser, routeId, toDoRouteList);
    	}catch(Exception e)
    	{
    		return Error;
    	}
    	return NoError;
    }
    
    /**
     * CV2019 - Method overloading has added for moveDocOnApproveReject method after CV10.2 and SIDBI merges 
    **/
    public int moveDocOnApproveReject(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq){
    	return moveDocOnApproveReject(sid,  routeMaster, status, comments, selectedTaskSeq, 0);
    }
    public int moveDocOnApproveReject(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq, int signId){
    	return moveDocOnApproveReject(sid,  routeMaster, status, comments, selectedTaskSeq, true, signId);
    }
    public int moveDocOnApproveReject(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq, boolean notificationReq, int signId) {
    	return moveDocOnApproveReject(sid,  routeMaster, status, comments, selectedTaskSeq, notificationReq, signId, 0);
    }
    public int moveDocOnApproveReject(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq, boolean notificationReq, int signId, int assignSecurity) {
    	return moveDocOnApproveReject(sid,  routeMaster, status, comments, selectedTaskSeq, notificationReq, signId, assignSecurity, null, null);
    }
    public int moveDocOnApproveReject(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq, boolean notificationReq, int signId, int assignSecurity, String clientType, String secureURL) {
    	return moveDocOnApproveReject(sid,  routeMaster, status, comments, selectedTaskSeq, notificationReq, signId, assignSecurity, clientType, secureURL, null);
    }
    /**
     * CV2019 Merges from SIDBI
     * @param sid
     * @param routeMaster
     * @param status
     * @param comments
     * @param selectedTaskSeq
     * @param notificationReq
     * @param signId
     * @param assignSecurity
     * @param clientType
     * @param secureURL
     * @return
     */
    public int moveDocOnApproveReject(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq, boolean notificationReq, int signId, int assignSecurity, String clientType, String secureURL, String errorMsg)
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in moveDocOnApproveReject::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	String userName = "";
        	if(type == Client.Adm_Client_Type)
        		userName = session.user;
        	//printToConsole(" Before update routehistory");   
        	//VWClient.printToConsole("drsValidateTaskDocumentVwctor.get(0)).trim() :::"+drsValidateTaskDocumentVwctor.get(0));
        	if(drsValidateTaskDocumentVwctor!=null&&!drsValidateTaskDocumentVwctor.isEmpty()&&drsValidateTaskDocumentVwctor.size()>0&&((String) drsValidateTaskDocumentVwctor.get(0)).trim().equalsIgnoreCase("-1") && userSelectedValue!=0){
        		drsValidateTaskDocumentVwctor =new Vector();
        		drsValidateTaskDocument(sid, routeMaster.getDocId(), routeMaster.getRouteId(),2,String.valueOf(userSelectedValue), drsValidateTaskDocumentVwctor);
        		if(drsValidateTaskDocumentVwctor!=null&&!drsValidateTaskDocumentVwctor.isEmpty()&&drsValidateTaskDocumentVwctor.size()>0 ){
        			String msg = drsValidateTaskDocumentVwctor.get(0).toString();
        			VWClient.printToConsole("errorList condition in drsValidateTaskDocument::"+msg);
        			VWClient.printToConsole("errorMsg::"+errorMsg);
        			/**CV10.2 - If condition added for multiple document selection ***/ 
        			if (errorMsg != null) {
        				errorMsg = errorMsg + "," + routeMaster.getDocName();
	        			JOptionPane.showMessageDialog(null,msg);
	        			VWMessage.showMessage(msg);
        			} else {
        				JOptionPane.showMessageDialog(null,msg);
	        			VWMessage.showMessage(msg);
        			}
        			return 0;
        		}
        	}
            int returnValue = vws.updateRouteHistory(session.room, sid, routeMaster.getDocId(), routeMaster.getRouteId(), 
            			routeMaster.getRouteUserId(), status, comments, routeMaster.getStatus(), routeMaster.getRouteMasterId(),routeMaster.getRouteHistoryId(), userName, userName, signId);
            VWClient.printToConsole("updateRouteHistory return value : "+returnValue);
            //Added by Srikanth to get the email originator status for given routeid.  
            Vector getRouteInfoVec = new Vector();
			returnValue = getRouteInfo(sid, routeMaster.getRouteId(), getRouteInfoVec);
			VWClient.printToConsole("Route info return value : "+returnValue);
			VWClient.printToConsole("RouteInfo List: "+getRouteInfoVec);
			RouteInfo routeInfo = (RouteInfo) getRouteInfoVec.get(0);
			
			
            Vector vRouteTaskInfo = new Vector();
            int currentTaskSequence = routeMaster.getLevelSeq();
        	//printToConsole(" currentTaskSequence "+currentTaskSequence);
            Vector ret = vws.getRouteTaskInfo(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, vRouteTaskInfo);
            VWClient.printToConsole("getRouteTaskInfo return value :"+ret);
            if (ret != null)
            	vRouteTaskInfo.addAll(vectorToRouteTask(ret));
            if(vRouteTaskInfo!=null && vRouteTaskInfo.size()>0){
            	boolean sidbiCustomization = vws.getSidbiCustomizationFlag(session.room, sid);
            	RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vRouteTaskInfo.get(0);
            	int minApprovalCount = vws.getTaskApproversCount(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId());
            	VWClient.printToConsole("Before calling moveDocOnApproveRejectTask......");
                returnValue = moveDocOnApproveRejectTask(sid, routeMaster, status, selectedTaskSeq, notificationReq, 
						currentTaskSequence, routeTaskInfo, minApprovalCount, sidbiCustomization,
						routeInfo.getEmailOriginator(), assignSecurity, clientType, secureURL);
            	VWClient.printToConsole("After calling moveDocOnApproveRejectTask......"+returnValue);
            	if (returnValue >= 0) {
	            	try {
		            	int isDocMovedToNextTaskUser = vws.getDocumentStatusInWorkflow(session.room, sid, routeMaster.getDocId());
			            VWClient.printToConsole("isDocMovedToNextTaskUser : "+isDocMovedToNextTaskUser);
			            //isDocMovedToNextTaskUser value = {0 : Moved to next task   1 : Failed to move next task}
			            if (isDocMovedToNextTaskUser == 1) {
			                moveDocOnApproveRejectTask(sid, routeMaster, status, selectedTaskSeq, notificationReq,
									currentTaskSequence, routeTaskInfo, minApprovalCount, sidbiCustomization,
									routeInfo.getEmailOriginator(), assignSecurity, clientType, secureURL);
			            }
	            	} catch (Exception se) {
	            		VWClient.printToConsole("Exception in moveDocOnApproveReject internal call "+se.getMessage());
	            	}
            	}
            }
            VWClient.printToConsole("End of moveDocOnApproveReject method");
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }      
	}
    
    /**
     * CV2019 Merges from SIDBI 
     * @param sid
     * @param routeMaster
     * @param status
     * @param selectedTaskSeq
     * @param notificationReq
     * @param currentTaskSequence
     * @param routeTaskInfo
     * @param minApprovalCount
     * @param sidbiCustomization
     * @param emailOriginator
     * @param assignSecurity
     * @param clientType
     * @param secureURL
     * @return
     */
    public int moveDocOnApproveRejectTask(int sid, RouteMasterInfo routeMaster, String status, int selectedTaskSeq,
			boolean notificationReq, int currentTaskSequence, RouteTaskInfo routeTaskInfo,
			int minApprovalCount, boolean sidbiCustomization, int emailOriginator, int assignSecurity, String clientType, String secureURL) {
    	VWClient.printToConsole("inside moveDocOnApproveRejectTask.........");
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in moveDocOnApproveReject::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
    	
    	VWClient.printToConsole("Minimum Approval Count : "+minApprovalCount);
    	VWClient.printToConsole("status in moveDocOnApproveRejectTask...: "+status);
    	int returnValue = -1;
    	try {
	    	int nextRouteTaskSeq = -1;
	    	if(status.equalsIgnoreCase(Route_Status_Approve)||status.equalsIgnoreCase(Route_Status_Approve_PSR)||status.equalsIgnoreCase(Route_Status_Approve_Intimation))
	    	{
	    		try {
		    		int ApprrovedCount = vws.getApprovedCountForTask(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId(),routeMaster.getRouteUserId());
		    		VWClient.printToConsole("Approved task count : "+ApprrovedCount);
		    		//printToConsole(" ApprrovedCount "+ApprrovedCount);
		    		//Code modified for task escalation enhancement
		    		if(minApprovalCount <= ApprrovedCount){
		    			int routeTaskCount = vws.getRouteTaskCount(session.room, sid, routeMaster.getRouteId());
		    			VWClient.printToConsole("Route task count : "+routeTaskCount);
		    			nextRouteTaskSeq = currentTaskSequence+1;
		    			//printToConsole(" routeTaskCount="+routeTaskCount+" nextRouteTaskSeq="+nextRouteTaskSeq);
		    			if(nextRouteTaskSeq <= routeTaskCount){
		    				VWClient.printToConsole("Approved - Inside if condition.....");
		    				returnValue = vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnApprove);
		    				VWClient.printToConsole("updateStatusOfOutOfTskUsrs method returnValue "+returnValue);
		                    //CV2019 Merges from SIDBI - parameter assignSecurity is not being used anywhere. Removed the parameter passing to moveDocToAnotherTask
		    				returnValue = moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
		    						routeMaster.getRouteName(), routeMaster.getDocName(), status);
		    				VWClient.printToConsole("moveDocToAnotherTask method returnValue "+returnValue);
		    			}else{
		    				VWClient.printToConsole("Approved - Inside else condition.....");
		    				// Need to update tasks completed status, so final action will takes place.
		    				returnValue = vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence,Route_Status_OnApproveOnLastTask);
		    				VWClient.printToConsole("updateStatusOfOutOfTskUsrs method returnValue "+returnValue);
		    				vws.updateRouteCompletedDocs(session.room, sid, routeMaster.getRouteMasterId(), "TASK");
		    				if (emailOriginator==1) {
		        				vws.sendMailWithAttachment(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), 
		            					routeMaster.getRouteName(), routeMaster.getDocId(), new Vector(), null, status, null, null, "", clientType, secureURL);
		    				}
		    			}
		    		}
	    		} catch (Exception e) {
	    			VWClient.printToConsole("Exception while Status Approve/Approve PSR/Approve Intimation in moveDocOnApproveReject : "+e.getMessage());
	    			returnValue = Error;
	    		}
	    	}else if(status.equalsIgnoreCase(Route_Status_Reject)){
	    		try {
		    		if(selectedTaskSeq == 0){// moving to originator
		    			returnValue = vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnReject);
		    			VWClient.printToConsole("updateStatusOfOutOfTskUsrs method returnValue "+returnValue);
		    			nextRouteTaskSeq = 0;
		    			//CV2019 Merges from SIDBI - parameter assignSecurity is not being used anywhere. Removed the parameter passing to moveDocToAnotherTask
		    			returnValue = moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(), 
		    					routeMaster.getRouteName(), routeMaster.getDocName(), status);
		    			VWClient.printToConsole("moveDocToAnotherTask method returnValue "+returnValue);
		    			//sending mail to other users of route
		    			notificationOnReject(sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
		    					routeMaster.getRouteName(), routeMaster.getDocName(), status, clientType);
		    			//sending mail to originator
		    			if (!sidbiCustomization) {
		        			vws.sendMailWithAttachment(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), routeMaster.getRouteName(),
		        					routeMaster.getDocId(), new Vector(), null, status, null, null, "", clientType, secureURL);
		    			}
		    		}else if(selectedTaskSeq == -2){ // End route selected from adminwise reject task drop down or from Client ToDoList
		    			returnValue = vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnRejectOnLastTask);
		    			VWClient.printToConsole("updateStatusOfOutOfTskUsrs method returnValue "+returnValue);
						//vws.updateRouteCompletedDocs(session.room, sid, routeMaster.getRouteMasterId(), "TASK");
						/**Implementation change for Reject criteria->User Selects task / Select task from UI-> End workflow should not take final action. 
						 * As requested by Rajesh after CV8.3.3
						 * Request Received Date:-06-Nov-2015
						 */        				
						nextRouteTaskSeq = 0;
						//CV2019 Merges from SIDBI - parameter assignSecurity is not being used anywhere. Removed the parameter passing to moveDocToAnotherTask
						returnValue = moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(), 
		    					routeMaster.getRouteName(), routeMaster.getDocName(), status);
						VWClient.printToConsole("moveDocToAnotherTask method returnValue "+returnValue);
						if (!sidbiCustomization) {
							vws.sendMailWithAttachment(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), 
			    					routeMaster.getRouteName(), routeMaster.getDocId(), new Vector(), null, status, null, null, "", clientType, secureURL);
						}
		    		}else if(selectedTaskSeq > 0){
		   				nextRouteTaskSeq = selectedTaskSeq;
		   				returnValue = vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnReject);
		   				VWClient.printToConsole("updateStatusOfOutOfTskUsrs method returnValue "+returnValue);
		   				//CV2019 Merges from SIDBI - parameter assignSecurity is not being used anywhere. Removed the parameter passing to moveDocToAnotherTask
		   				returnValue = moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
		    					routeMaster.getRouteName(), routeMaster.getDocName(), status);
		   				VWClient.printToConsole("moveDocToAnotherTask method returnValue "+returnValue);
		    		}
	    		} catch (Exception e) {
	    			VWClient.printToConsole("Exception while Status Reject in moveDocOnApproveReject : "+e.getMessage());
	    			returnValue = Error;
	    		}
	       	}else if(status.equalsIgnoreCase(Route_Status_Review)){
	    	}else if(status.equalsIgnoreCase(Route_Status_End)||status.equalsIgnoreCase(Route_Status_Approve_End)||status.equalsIgnoreCase(Route_Status_PSR_End)){
	    		try {
		    		returnValue = vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnEnd);
		    		VWClient.printToConsole("updateStatusOfOutOfTskUsrs method returnValue "+returnValue);
					//sending mail to other users of route
					if (notificationReq){
						VWClient.printToConsole("Before calling  notificationOnReject method....");
					    notificationOnReject(sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
						    routeMaster.getRouteName(), routeMaster.getDocName(), status, clientType);
					    //sending mail to originator
					    VWClient.printToConsole("After calling  notificationOnReject method....");
					    if (!sidbiCustomization) {
					    	VWClient.printToConsole("Before calling  sendMailWithAttachment method....");
					    	vws.sendMailWithAttachment(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), 
		    				    routeMaster.getRouteName(), routeMaster.getDocId(), new Vector(), null, status, null, null, "", clientType, secureURL);
					    }
					}
	    		} catch (Exception e) {
	    			VWClient.printToConsole("Exception while Status End/Approve End/Psr End in moveDocOnApproveReject : "+e.getMessage());
	    			returnValue = Error;
	    		}
	    	} 
    	} catch (Exception e) {
			VWClient.printToConsole("Exception in moveDocOnApproveRejectTask : "+e.getMessage());
			returnValue = Error;
		}
    	return returnValue;
    }
    
    public int moveDocOnApproveRejectForEscalatedDocs(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq)
    {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in moveDocOnApproveRejectForEscalatedDocs::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	String userName = "";
        	if(type == Client.Adm_Client_Type)
        		userName = session.user;
        	//printToConsole(" Before update routehistory");
            Vector getRouteInfoVec = new Vector();
			getRouteInfo(sid, routeMaster.getRouteId(), getRouteInfoVec);
			RouteInfo routeInfo = (RouteInfo) getRouteInfoVec.get(0);

            Vector vRouteTaskInfo = new Vector();
            int currentTaskSequence = routeMaster.getLevelSeq();
        	//printToConsole(" currentTaskSequence "+currentTaskSequence);
            Vector ret = vws.getRouteTaskInfo(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, vRouteTaskInfo);
            vRouteTaskInfo.addAll(vectorToRouteTask(ret));
            if(vRouteTaskInfo!=null && vRouteTaskInfo.size()>0){
            	boolean sidbiCustomization = vws.getSidbiCustomizationFlag(session.room, sid);
            	RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vRouteTaskInfo.get(0);
            	int minApprovalCount = vws.getTaskApproversCount(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId());
            	//printToConsole(" minApprovalCount "+minApprovalCount);
            	int nextRouteTaskSeq = -1;
            	if(status.equalsIgnoreCase(Route_Status_Approve))
            	{
            		int ApprrovedCount = vws.getApprovedCountForTask(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId(), routeMaster.getRouteUserId());
            		//printToConsole(" ApprrovedCount "+ApprrovedCount);
            		//Code modified for task escalation enhancement
            		if(minApprovalCount <= ApprrovedCount){
            			int routeTaskCount = vws.getRouteTaskCount(session.room, sid, routeMaster.getRouteId());
            			nextRouteTaskSeq = currentTaskSequence+1;
            			//printToConsole(" routeTaskCount="+routeTaskCount+" nextRouteTaskSeq="+nextRouteTaskSeq);
            			if(nextRouteTaskSeq <= routeTaskCount){
            				vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnApprove);
            				moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
            						routeMaster.getRouteName(), routeMaster.getDocName(), status);
            			}else{
            				// Need to update tasks completed status, so final action will takes place.
            				vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence,Route_Status_OnApproveOnLastTask);
            				vws.updateRouteCompletedDocs(session.room, sid, routeMaster.getRouteMasterId(), "TASK");
            				if (routeInfo.getEmailOriginator()==1) {
	            				vws.sendMailWithAttachment(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), 
	                					routeMaster.getRouteName(), routeMaster.getDocId(), new Vector(), null, status, null, null, "");
            				}
            				
            				RouteDocuments routeDocuments = new RouteDocuments(this, sid, session.room);
            					routeDocuments.getRouteCompletedDocs();
            			}
            		}
            	}else if(status.equalsIgnoreCase(Route_Status_Reject)){
            		//moving to originator
            		vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnReject);
            		nextRouteTaskSeq = 0;
            		moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(), 
            				routeMaster.getRouteName(), routeMaster.getDocName(), status);
            		//sending mail to other users of route
            		notificationOnReject(sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
            				routeMaster.getRouteName(), routeMaster.getDocName(), status,"");
            		//sending mail to originator
            		if (!sidbiCustomization) {
	            		vws.sendMailWithAttachment(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), routeMaster.getRouteName(),
	            				routeMaster.getDocId(), new Vector(), null, status, null, null, "");
            		}
	           	}
            }
            return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }      
	}
    
    private int moveDocToAnotherTask(int sid, int routeId, int routeTaskSeq, int docId, int routeMasterId, 
    		String routeName, String docName, String status){
    	return moveDocToAnotherTask(sid, routeId, routeTaskSeq, docId, routeMasterId, routeName, docName, status, 0);
    }
    private int moveDocToAnotherTask(int sid, int routeId, int routeTaskSeq, int docId, int routeMasterId, 
    		String routeName, String docName, String status, int signId) {
    	return moveDocToAnotherTask(sid, routeId, routeTaskSeq, docId, routeMasterId, 
        		routeName, docName, status, signId, null);
    }
    private int moveDocToAnotherTask(int sid, int routeId, int routeTaskSeq, int docId, int routeMasterId, 
    		String routeName, String docName, String status, int signId, String openDocUrl){
    	VWClient.printToConsole("inside moveDocToAnotherTask method ....."+routeTaskSeq);
    	int returnValue = 0, retValue = 0;
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in moveDocToAnotherTask::"+sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try
    	{
    		String userName = "";
    		if(routeTaskSeq>0){
    			Vector vTaskInfo = new Vector();
    			StringBuilder emailUsersList = null;
    			Vector ret = vws.getRouteTaskInfo(session.room, sid, routeId, routeTaskSeq, vTaskInfo);
    			vTaskInfo.addAll(vectorToRouteTask(ret));
    			// There is no need to call setRouteHistory and send mail if the status is end route
    			if(vTaskInfo!=null&& vTaskInfo.size()>0 && !status.equalsIgnoreCase(Route_Status_End))
    			{
    				RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(0);
    				int routeTaskId = routeTaskInfo.getRouteTaskId();
    				Vector vRouteUser = new Vector();
    				ret = vws.getRouteUsers(session.room, sid, routeTaskId, vRouteUser, docId);
    				ArrayList listRouteUsers = vectorToRouteUsersArrayList(ret);
    				/*CV2019 merges from SIDBI line------------------------------------------------***/
    				boolean sidbiCustomization = vws.getSidbiCustomizationFlag(session.room, sid);
    				/*------------------------End of SIDBI merges--------------------------------------*/
    				if (listRouteUsers.size() > 0) {
	   					for(int k=0; k<listRouteUsers.size(); k++)
	    				{
	    					RouteUsers routeUser = (RouteUsers)listRouteUsers.get(k);
	    					int routeUserId =routeUser.getRouteUserId();
	    					String groupName = routeUser.getGroupName();
	    					
	    					if (type == Client.Adm_Client_Type){
	    						if(isAdmin(sid)==1){
	    							userName = session.user;
	    						}
	    					}
	    					else if (type==Client.Web_Client_Type || type== Client.NWeb_Client_Type){
	    						userName = "";
	    					}
	
	    					retValue = vws.setRouteHistory(session.room, sid, docId, routeId, routeTaskInfo.getTaskSequence(), routeUserId, "", userName, userName, signId, routeMasterId, groupName);
	    					VWClient.printToConsole("return value of setRouteHistory :"+retValue);
	    					try{
	    						VWClient.printToConsole("routeTaskInfo.getEmailToPrvTaskUsers() ::: "+routeTaskInfo.getEmailToPrvTaskUsers());
	    						if(routeUser.getSendEmail() == 1) {
	    							/*CV2019 merges from SIDBI line------------------------------------------------***/
	    							VWClient.printToConsole("sidbiCustomization..... :"+sidbiCustomization);
	    							// sendMailWithAttachment should be called only if the emailToPreviousUser check should be disabled(0)
	    							//if ((!sidbiCustomization) || (sidbiCustomization == true && routeTaskInfo.getEmailToPrvTaskUsers() != 1)) {
	    							if (!sidbiCustomization) {
		    							vws.sendMailWithAttachment(session.room, session.server, sid, routeUser.getUserName(), docName, routeName, 
		    									docId, new Vector(), null, Route_Status_Pending, null, null, "", null, openDocUrl);
	    							} else {
	    								if (emailUsersList == null) {
	    									emailUsersList = new StringBuilder();
	    									emailUsersList.append(routeUser.getUserName());
	    								} else {
	    									emailUsersList.append(";"+routeUser.getUserName());
	    								}
	    							}
	    							/*------------------------End of SIDBI merges--------------------------------------*/
	    						} 
	    					}catch(Exception ex){
	    						returnValue = Error;
	    					}
	    				}
	   					/*CV2019 merges from SIDBI line------------------------------------------------***/
	   					VWClient.printToConsole("Final emailUsersList....."+emailUsersList);
	   					if (emailUsersList != null && emailUsersList.length() > 0) {
							vws.sendMailWithAttachment(session.room, session.server, sid, emailUsersList.toString(), docName, routeName, 
									docId, new Vector(), null, Route_Status_Pending, null, null, "");
						}
	   					/*------------------------End of SIDBI merges--------------------------------------*/
    				}
    			}
    		}else if(routeTaskSeq == 0){// move to originator on Reject
    			if (type == Client.Adm_Client_Type){
					if(isAdmin(sid)==1){
						userName = session.user;
					}
				}
    			retValue = vws.setRouteHistory(session.room, sid, docId, routeId, routeTaskSeq, 0, "", userName, userName, signId, routeMasterId); //routeUserId is 0 here
    			VWClient.printToConsole("return value of setRouteHistory :"+retValue);
    		}
    	}catch(Exception e){
    		returnValue = Error;
    	}
    	return returnValue;
    	
    }
    
    private int notificationOnReject(int sid, int routeId, int currentTaskSequence, int docId, int routeMasterId, 
    		String routeName, String docName, String status,String clientType){
    	Session session = getSession(sid);
    	StringBuilder emailUsersList = null;
    	VWClient.printToConsole("sessionId in notificationOnReject::"+sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	try{
    		//Sending mail to all previous task users in case of end route action or reject notify originator.
			Vector vTaskInfo = new Vector();
			Vector ret = vws.getRouteTaskInfo(session.room, sid, routeId, 0, vTaskInfo);
			vTaskInfo.addAll(vectorToRouteTask(ret));
			HashSet hashSet = new HashSet();
			/*CV2019 merges from SIDBI line------------------------------------------------***/
			boolean sidbiCustomization = vws.getSidbiCustomizationFlag(session.room, sid);
			/*------------------------End of SIDBI merges------------------------------*/
			for(int i=0; vTaskInfo!=null && i<vTaskInfo.size(); i++){
				RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(i);
				if(routeTaskInfo.getTaskSequence()<=currentTaskSequence){
					int routeTaskId = routeTaskInfo.getRouteTaskId();
					Vector vRouteUser = new Vector();
					//Modified by Srikanth added docId 
					ret = vws.getRouteUsers(session.room, sid, routeTaskId, vRouteUser, docId);
					ArrayList listRouteUsers = vectorToRouteUsersArrayList(ret);
					for(int k=0; k<listRouteUsers.size(); k++)
					{   
						RouteUsers routeUser = (RouteUsers)listRouteUsers.get(k);
						int userId = routeUser.getUserId();
						//To avoid sending duplicate mails to same user id. If the same user repeated in different task.
						//printToConsole("notificationOnReject userId:"+userId);
						boolean isUserAdded = hashSet.add(""+userId);
						if(/*isUserAdded && */ routeUser.getSendEmail() == 1) {
							/*CV2019 merges from SIDBI line------------------------------------------------***/
							// sendMailWithAttachment should be called only if the emailToPreviousUser check should be disabled(0)
							//if ((!sidbiCustomization) || (sidbiCustomization == true && routeTaskInfo.getEmailToPrvTaskUsers() != 1)) {							
							if (!sidbiCustomization) {
								vws.sendMailWithAttachment(session.room, session.server, sid, routeUser.getUserName(), docName, routeName,
										docId, new Vector(), null, status, null, null, "");
							} else {
								if (emailUsersList == null) {
									emailUsersList = new StringBuilder();
									emailUsersList.append(routeUser.getUserName());
								} else {
									emailUsersList.append(";"+routeUser.getUserName());
								}
							}
							/*------------------------End of SIDBI merges------------------------------*/
						}
						
                	}
			   }
        	}
			/*CV2019 merges from SIDBI line------------------------------------------------***/
			VWClient.printToConsole("Final emailUsersList : "+emailUsersList);
			if (emailUsersList != null && emailUsersList.length() > 0) {
				vws.sendMailWithAttachment(session.room, session.server, sid, emailUsersList.toString(), docName, routeName, 
						docId, new Vector(), null, status, null, null, "");
				emailUsersList = null;
			}
			/*------------------------End of SIDBI merges------------------------------*/
    	}catch(Exception e){
            return Error;
        }      
    	return NoError;
    }

    public int checkDocsInRoute_CheckedOut(int sid, int nodeId, boolean flagRoute)
    {
		Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in checkDocsInRoute_CheckedOut::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	return vws.checkDocsInRoute_CheckedOut(sid, session.room, nodeId, flagRoute);
        }
        catch(Exception e)
        {
            return Error;
        }    
    }
  //Overrided JNI method used to check locked document from DTC(ViewWise Client)
    public int checkDocsInRoute_CheckedOut(int sid, int nodeId, int flagRoute)
    {
		Session session = getSession(sid);
		VWClient.printToConsole("sessionId in checkDocsInRoute_CheckedOut::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	return vws.checkDocsInRoute_CheckedOut(sid, session.room, nodeId, flagRoute);
        }
        catch(Exception e)
        {
            return Error;
        }    
    }
	public int getDocsToAutoRoute(int sid, Vector sendDocumentsInRoute){
		Session session = getSession(sid);
		VWClient.printToConsole("sessionId in getDocsToAutoRoute::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector sendDocsInRoute = new Vector();
        	sendDocsInRoute = vws.getDocsToAutoRoute(session.room, sid,  sendDocumentsInRoute);
        	sendDocumentsInRoute.addAll(sendDocsInRoute);
        	return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }    
	}
	public int getDocsForFinalAction(int sid, Vector taskCompletedDocs){
		Session session = getSession(sid);
		VWClient.printToConsole("sessionId in getDocsForFinalAction::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector ret = new Vector();
        	ret = vws.getDocsForFinalAction(session.room, sid,  taskCompletedDocs);
        	taskCompletedDocs.addAll(ret);
        	return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }    
	}
	public int getDRSPrincipals(int sid, int type, String groupName, Vector princ){
		Session session = getSession(sid);
		VWClient.printToConsole("sessionId in getDRSPrincipals::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
        	Vector ret = new Vector();
        	ret = vws.getDRSPrincipals(session.room, sid, type, groupName, princ);
        	princ.addAll(vectorToDRSPrincipals(ret));
        	return NoError;
        }catch(Exception e)
        {
            return Error;
        }   
	 }
	 /* Purpose:	Master Switch off added DRS in registry
     * Created By: C.Shanmugavalli		Date:	13 Feb 2007
     */
     public int isDRSEnabled(int sid){
     	Session session = getSession(sid);
		VWClient.printToConsole("sessionId in isDRSEnabled::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
       	int isDRSEnable = vws.getDRSEnable(sid);
        	//VWSLog.err("isDRSEnable "+isDRSEnable);
        	return isDRSEnable;
        }catch(Exception e)
        {
           return Error;
        }
     }
       
     public int setDRSEnabled(int sid, String status){
	     	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setDRSEnabled::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	       	int isDRSEnable = vws.setDRSEnable(sid, status);
	        	//VWSLog.err("isDRSEnable "+isDRSEnable);
	        	return isDRSEnable;
	        }catch(Exception e)
	        {
	           return Error;
	        }
	     }
     
     public int checkGroupsInRoute(int sid, Vector routeList) {
	 Session session = getSession(sid);
 	 VWClient.printToConsole("sessionId in checkGroupsInRoute::"+sid);
	 if(session==null) return invalidSessionId;
	 VWS vws = getServer(session.server);
	 if (vws == null) return ServerNotFound;
	 try
	 {
	     Vector output = new Vector();
	     output = vws.checkGroupsInRoute(session.room, sid);
	     vectorToRouteMaster(routeList, output);  
	 }catch(Exception e)
	 {
	     return Error;
	 }
	 return NoError;
     }
     
     public int syncRouteGroups(int sid, String processFlag) {
	 Session session = getSession(sid);
	 VWClient.printToConsole("sessionId in syncRouteGroups::"+sid);
	 if(session==null) return invalidSessionId;
	 VWS vws = getServer(session.server);
	 if (vws == null) return ServerNotFound;
	 try
	 {
	     int result = vws.syncRouteGroups(session.room, sid, processFlag);
	     return result;

	 }catch(Exception e)
	 {
	     return Error;
	 } 
     }
  //For Invalid Route
     public int isRouteValid(int sid, int routeId){

    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in isRouteValid::"+sid);
    	if(session==null) return invalidSessionId;
    	VWS vws = getServer(session.server);
    	if (vws == null) return ServerNotFound;
    	int retValue = 0;
    	try{
    		retValue = vws.isRouteValid(session.room, sid, routeId);
    	}catch(Exception ex){
    		//printToConsole("isRouteValid Exception ex " + ex.getMessage());
    		return Error;
    	}
    	return retValue;
    }
     
     public int syncInvalidRouteUsers(int sid, int routeId){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in syncInvalidRouteUsers::"+sid);
    	 if(session==null) return invalidSessionId;
    	 VWS vws = getServer(session.server);
    	 if (vws == null) return ServerNotFound;
    	 try
    	 {
    		 int result = vws.syncInvalidRouteUsers(session.room, sid, routeId);
    		 return result;

    	 }catch(Exception e)
    	 {
    		 return Error;
    	 } 
     }

     public int getRouteInvalidTasks(int sid, int routeId, Vector tasks) {
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in getRouteInvalidTasks::"+sid);
    	 if(session==null) return invalidSessionId;
    	 VWS vws = getServer(session.server);
    	 if (vws == null) return ServerNotFound;
    	 try
    	 {
    		 Vector result = vws.getRouteInvalidTasks(session.room, sid, routeId, tasks);
    		 tasks.addAll(result);
    		 return NoError;

    	 }catch(Exception e)
    	 {
    		 return Error;
    	 } 
     }
     public int getEscalatedDocsList(int sid, Vector escalatedDocsList){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in getEscalatedDocsList::"+sid);
    	 if(session==null) return invalidSessionId;
    	 VWS vws = getServer(session.server);
    	 if (vws == null) return ServerNotFound;
    	 try
    	 {
    		 Vector result = vws.getEscalatedDocList(session.room, sid, escalatedDocsList);
    		 //For escalation sending 1 to RouteMasterInfo bean
    		 vectorToRouteMaster(escalatedDocsList, result, 1);
    		 return NoError;

    	 }catch(Exception e) {
    		 return Error;
    	 } 
     }
     public int setUserEscalation(int sid, RouteMasterInfo routeMasterInfo){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in setUserEscalation::"+sid);
    	 if(session==null) return invalidSessionId;
    	 VWS vws = getServer(session.server);
    	 if (vws == null) return ServerNotFound;
    	 try
    	 {
    		 int ret = vws.setUserEscalation(session.room, sid, routeMasterInfo);
    		 return NoError;
    	 }catch(Exception ex){
    		 return Error;
    	 }
     }
  //-------
     /*End of DRS methods*/

     // Following 3 methods are for another room node short cut enhancement.
     public int nodeShortCutAdd(int sid, int parentNodeId, String nodeName, String description){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in nodeShortCutAdd::"+sid);
    	 if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
         	vws.nodeShortCutAdd(sid, session.room, parentNodeId, nodeName, description);
         	return NoError;
         }
         catch(Exception e)
         {
             return Error;
         }    
     }
     
     public int nodeShortCutDelete(int sid,  int nodeId){
    	 Session session = getSession(sid);
      	 VWClient.printToConsole("sessionId in nodeShortCutDelete::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
          	vws.nodeShortCutDelete(sid, session.room, nodeId);
          	return NoError;
         }
         catch(Exception e)
         {
             return Error;
         } 
         
     }
     
     public int getNodeShortCutDesc(int sid,  int nodeId, Vector desc){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in getNodeShortCutDesc::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
          	Vector ret = vws.getNodeShortCutDesc(sid, session.room, nodeId);
          	desc.addAll(ret);
          	return NoError;
         }
         catch(Exception e)
         {
             return Error;
         } 
     }
     
     
     //Desc   :Inserts page info in DB
     //Author :Nishad Nambiar
     public int doDBInsertPageText(int sid, String room, int did, int pno, int pid, int pvr,int qc,String localPath)
     {
     	Session session = getSession(sid);
     	VWClient.printToConsole("sessionId in doDBInsertPageText::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
             boolean ret = vws.DBInsertPageText(sid, room, did, pno, pid, pvr, qc, localPath);
             return 0;
         }
         catch(Exception e)
         {
             return 0;
         }
     }
   
   //Desc   :Checks if FTS is enabled or not for a doctyoe.
   //Author :Nishad Nambiar
     public int isFTSEnabled(int sid, String room, String doctypeID)
     {
    	int retVal=0;
     	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in isFTSEnabled::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
             boolean ret = vws.isFTSEnabled(sid, room, doctypeID);
             if(ret)
            	 retVal=1;
             else
            	 retVal=-1;
         }
         catch(Exception e)
         {
             return -1;
         }
         return retVal;
     }
     //Document List enhancement methods - Not in use
     public int setCustomisedIndexList(int sid, int nodeId, int rowCount){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in setCustomisedIndexList::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
        	Vector indexInfo = new Vector();
        	getCustomIndexIdsFromRegistry(sid, indexInfo); 
        	int ret = vws.setCustomisedIndexList(0, sid, session.room, nodeId, rowCount , indexInfo.get(0).toString());
         	return ret;
         }catch(Exception e)
         {
            return Error;
         }
    	 
     }
     
     /**  getCustomisedIndexValues override method changes are done by Srikanth 
      *    for getting the refNode info along with normal nodes
      */
     public int getCustomisedIndexValues(int sid, int nodeId, int rowCount, Vector docs)
     {
    	 return getCustomisedIndexValues(sid, nodeId, rowCount, 0, docs);
     }
     public int getCustomisedIndexValues(int sid, int nodeId, int rowCount, int refNid, Vector docs){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in getCustomisedIndexValues::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
        	Vector indexInfo = new Vector();
         	getCustomIndexIdsFromRegistry(sid, 0, nodeId, indexInfo);
         	String customIndexList = "";
         	if(indexInfo!=null && indexInfo.size()>0)
         		customIndexList = removeNegativeIds(indexInfo.get(0).toString());
         	int hitList = 0;
         	/*int ret = vws.setCustomisedIndexList(hitList, sid, session.room, nodeId, rowCount , customIndexList);*/
         	Vector result = vws.getCustomisedIndexValues(sid, session.room, nodeId, rowCount, refNid, customIndexList, docs);
        	docs.addAll(vectorToDocumentWithIndexValues(result, 0));
        	VWClient.printToConsole("Active Error in getCustomisedIndexValues--->"+vws.getActiveError());
            //return vws.getActiveError();
        	 if (sid>0 && vws.getActiveError()<0 ){
          	   return NoError;
             } else {
          	   return vws.getActiveError();
             }
         }catch(Exception e)
         {
            return Error;
         }
     }
     
     public int getCustomisedSearchResult(int sid, Search srch, Vector docs){
    	 getCustomisedSearchResult(sid, srch, docs, 0);
    	 return NoError;
     }
     
     public int getCustomisedSearchResult(int sid, Search srch, Vector docs, long hWnd){
    	 
    	 getCustomisedSearchResult(sid, srch, docs, 0, false);
    	 return NoError;
     }
     /********CV10.2 Workflow report enhancement changes***/
     public int getCustomisedSearchResult(int sid, Search srch, Vector docs, long hWnd,boolean docsOnly){
    	 return getCustomisedSearchResult(sid, srch, docs, hWnd, false, 1, null);
     }
     /********End of CV10.2 Workflow report enhancement changes***/
     public int getCustomisedSearchResult(int sid, Search srch, Vector docs, long hWnd,boolean docsOnly, int objectType, Vector<String> resultSet){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in getCustomisedSearchResult::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         Vector indexInfo = new Vector();
         VWClient.printToConsole("objectType ---->"+objectType);
         VWClient.printToConsole("srch.getId() ---->"+srch.getId());
         /**CV10.2 Merges - If condition has added for General report in administration worklow tab**/
         if (objectType == 2)
        	 getCustomIndexIdsFromRegistry(sid, objectType, srch.getId(), indexInfo);
         else
        	 getCustomIndexIdsFromRegistry(sid, objectType, indexInfo);
      	 VWClient.printToConsole("indexInfo :"+indexInfo);
      	 String customIndexList = "";
      	 if (indexInfo != null && indexInfo.size() > 0) {
      		/**CV10.2 Merges - else condition has added for General report in administration worklow tab**/
			if (objectType != 2) {
				VWClient.printToConsole("Document search.....");
      			customIndexList = removeNegativeIds(indexInfo.get(0).toString());
      		} else {
      			VWClient.printToConsole("General search.....");
      			customIndexList = indexInfo.get(0).toString();
      		}
      	 }
    	 int hitList = 1;
         try
         {
    		 /*int ret = vws.setCustomisedIndexList(hitList, sid, session.room, srch.getLastDocId(), srch.getRowNo() , customIndexList);*/
        	 if (hWnd > 0)
             {
        		 //VWSLog.err("here 1");
        		 int customList = 1;
                 new searchThread(2, vws, session.room, sid, srch, hWnd, customList, customIndexList, docsOnly).start(); 
                 return NoError;
             }
             else
             { 
	        	 //printToConsole(" before calling");
	        	 Vector result = vws.getCustomisedSearchResult(session.room, sid,   customIndexList,  srch,  docs);
	         	 
	        	 /********CV10.2 Workflow report enhancement changes***/
	        	 if (resultSet != null) {
	        		 resultSet.addAll(result);
	        		 VWClient.printToConsole("resultSet :"+resultSet);
	        	 }
				 /********End of Workflow report enhancement changes***/
	        	 
	        	 docs.addAll(vectorToDocumentWithIndexValues(result, 1));
	         	 return NoError;
             }
         }catch(Exception e)
         {
            return Error;
         }
     }
     
     //Junk code. Need to optimize later. Should call 'addCustomList' from client itself. Valli 18 May 2007
     public int setCustomIndexIdsInRegistry(int sid, String indexIds, String indexNames, String indexTypes){
	 return setCustomIndexIdsInRegistry(sid, indexIds, indexNames, indexTypes, 0, 0);
     }
     public int setCustomIndexIdsInRegistry(int sid, String indexIds, String indexNames, String indexTypes, int mode, int nodeId){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in setCustomIndexIdsInRegistry::"+sid);
    	 VWClient.printToConsole("mode in setCustomIndexIdsInRegistry::"+mode);
    	 VWClient.printToConsole("nodeId in setCustomIndexIdsInRegistry::"+nodeId);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try{
/*	         addCustomList(sid, 0, "1", indexIds, nodeId);
	         addCustomList(sid, 0, "2", indexNames , nodeId);
	         //Adding '7' as the data type of index fields.
	         addCustomList(sid, 0, "7", indexTypes , nodeId);
	         addCustomList(sid, 1, "1", indexIds, nodeId);
	         addCustomList(sid, 1, "2", indexNames, nodeId);
	         addCustomList(sid, 1, "7", indexTypes, nodeId);
*/
    		 
    		 addCustomList(sid, mode, "1", indexIds, nodeId);
	         addCustomList(sid, mode, "2", indexNames, nodeId);
	         addCustomList(sid, mode, "7", indexTypes, nodeId);
	         /**
	          * Code added to fix the custom column re-order issue fix
	          * Modified Date:-21/9/2015
	          * Modified By:-Madhavan
	          */
	         StringTokenizer indexNameToken = new StringTokenizer(indexNames, Util.SepChar );
        	 String spaceValue="";
        	 String idOrder="";
        	 for(int i=0;i<indexNameToken.countTokens();i++){
        		 spaceValue+="100"+"#";
        		 idOrder+=i+"#";
        	 }        	
        	 spaceValue = spaceValue.substring(0,spaceValue.length()-1);
        	 VWClient.printToConsole("spaceValue:::::"+spaceValue);
        	 idOrder = idOrder.substring(0,idOrder.length()-1);
        	 VWClient.printToConsole("idOrder:::::"+idOrder);
	         addCustomList(sid, mode, "3", spaceValue, nodeId);
	         addCustomList(sid, mode, "4", idOrder, nodeId);
	         addCustomList(sid, mode, "5", "0", nodeId);
	         addCustomList(sid, mode, "6", "0", nodeId);
	         //End of code added for custom column re-order issue fix
	     
	         Node node = new Node(nodeId);
	         try{
	        	 node = vws.getNodePropertiesByNodeID(session.room, sid, node);
	        	 String message = "Document List Custom columns modified for '"+node.getName()+"'";
	        	 vws.addNotificationHistory(session.room, sid, nodeId, VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_CustomCols, session.user, "-", "-", message);
	         }catch(Exception ex){

	         }
         }catch(Exception ex){
        	 return Error;
         }
    	 return NoError;
     }
     public int getCustomIndexIdsFromRegistry(int sid, Vector indexInfo){
	 return getCustomIndexIdsFromRegistry(sid, 0, 0, indexInfo);
     }

     public int getCustomIndexIdsFromRegistry(int sid, int mode, Vector indexInfo){
	 return getCustomIndexIdsFromRegistry(sid, mode, 0, indexInfo);
     }
   
     //Junk code. Need to optimize later. Should call 'getCustomList' from client itself. Valli 18 May 2007
     public int getCustomIndexIdsFromRegistry(int sid, int mode, int nodeId, Vector indexInfo){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in getCustomIndexIdsFromRegistry::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try{
        	 Vector result = new Vector();
	         result = vws.getCustomList(sid, session.room, session.user, mode, indexInfo, nodeId);
	         indexInfo.addAll(getIdInfo(result));
	         
         }catch(Exception e){
        	 return Error;
         }
    	 return NoError;
    	 
     }
     public int isCustomListInherited(int sid, int objectType, int nodeId){
    	 Session session = getSession(sid);
    	 VWClient.printToConsole("sessionId in isCustomListInherited::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try{    	
             return vws.isCustomListInherited(sid, session.room, objectType, nodeId);	         	         
         }catch(Exception e){
    	 return Error;
     }	  
         
     }
     // return data format 8	vwadmin	0	1	-2	-4	60	61
     // Need to be removed later. valli
     private Vector getIdInfo(Vector indexInfo){
    	 Vector retVector = new Vector();
    	 if(indexInfo!=null && indexInfo.size()>0){
    		 for(int i=0; i<indexInfo.size(); i++){
    			 StringTokenizer st = new StringTokenizer(indexInfo.get(i).toString(), Util.SepChar );
    			 String objType = st.nextToken();
    				 if(objType.equalsIgnoreCase("1")){
    					 String temp = "";
    					 int j = 0;
    					 int count = st.countTokens();
    					 while(st.hasMoreTokens()){
    						 if(j == count-1)
    							 temp = temp+st.nextToken();
    						 else
    							 temp = temp+st.nextToken()+Util.SepChar;
    						 j++;
    					 }
    					 retVector.add(0, temp);
    				 }
    				 else if(objType.equalsIgnoreCase("2")){
    					 String temp = "";
    					 int j = 0;
    					 int count = st.countTokens();
    					 while(st.hasMoreTokens()){
    						 if(j == count-1)
    							 temp = temp+st.nextToken();
    						 else
    							 temp = temp+st.nextToken()+Util.SepChar;
    						 j++;
    					 }
    					 retVector.add(1, temp);
    				 }
    				 else if(objType.equalsIgnoreCase("7")){
    					 String temp = "";
    					 int j = 0;
    					 int count = st.countTokens();
    					 while(st.hasMoreTokens()){
    						 if(j == count-1)
    							 temp = temp+st.nextToken();
    						 else
    							 temp = temp+st.nextToken()+Util.SepChar;
    						 j++;
    					 }
    					 retVector.add(2, temp);
    				 }
    		 }
    	 }
    	 return retVector;
     }
     // Need to be removed later. valli
     private Vector getColumnInfo(Vector columnInfo){
    	 Vector retVector = new Vector();
    	 if(columnInfo!=null && columnInfo.size()>0){
    		 for(int i=2; i<columnInfo.size(); i++){
    			 StringTokenizer st = new StringTokenizer(columnInfo.get(i).toString(), Util.SepChar);
    			 String objType = st.nextToken();    			 
    				 if(objType.equalsIgnoreCase("3")){
    					 retVector.add(0, st.nextToken());
    				 }
    				 else if(objType.equalsIgnoreCase("4")){
    					 retVector.add(1, st.nextToken());
    				 }
    				 else if(objType.equalsIgnoreCase("5")){
    					 retVector.add(2, st.nextToken());
    				 }
    				 else if(objType.equalsIgnoreCase("6")){
    					 retVector.add(3, st.nextToken());
    				 }
    		 }
    		 //printToConsole(" retVector "+retVector.toString());
    	 }
    	 return retVector;
     }
     
     private Vector getColumnArrangementInfo(Vector columnInfo){
    	 Vector retVector = new Vector();
    	 if(columnInfo!=null && columnInfo.size()>0){
    		 retVector.addAll(columnInfo);
    		 VWClient.printToConsole(" getColumnArrangementInfo : "+retVector);
    	 }
    	 return retVector;
     }
     
     /*
     * This is to remove default ids ( -ve ids) from registry 
     */
     private String removeNegativeIds(String ids){
    	String customIndexList = "";
    	if(ids!=null && !ids.equals("")){
	      	StringTokenizer st = new StringTokenizer(ids, Util.SepChar);
	      	int count = st.countTokens();
	      	for(int i=0; i<count; i++)
	      	{
	      		String temp = st.nextToken().toString();
	      		if(Integer.parseInt(temp) > 0){
	      			
	      			if(i == count-1)
	      				customIndexList = customIndexList+temp;
	      			else
	      				customIndexList = customIndexList+temp+Util.SepChar;
	      		}
	      	}
    	}
      	return customIndexList;
     }
     
     public int setColumnArragementInfo(int sessionId, int documentList , String docColumnWidths, String docPositions, 
    		 String docColumnSort, String sortOrder){
	return setColumnArragementInfo(sessionId, documentList, docColumnWidths, docPositions, docColumnSort, sortOrder, 0); 
     }
     public int setColumnArragementInfo(int sessionId, int documentList , String docColumnWidths, String docPositions, 
    		 String docColumnSort, String sortOrder, int nodeId){
    	 Session session = getSession(sessionId);
    	 VWClient.printToConsole("sessionId in setColumnArragementInfo::"+sessionId);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try{
	         addCustomList(sessionId, documentList, "3", docColumnWidths, nodeId);
	         addCustomList(sessionId, documentList, "4", docPositions, nodeId);
	         addCustomList(sessionId, documentList, "5", docColumnSort, nodeId);
	         addCustomList(sessionId, documentList, "6", sortOrder, nodeId);
         }catch(Exception e){
        	 //printToConsole("Error "+e.getMessage());
        	 return Error;
         }
    	 return NoError;
     }
     public int getColumnArragementInfo(int sessionId, int documentList , Vector columnInfo){
	 return getColumnArragementInfo(sessionId, documentList, 0, columnInfo);
     }
     public int getColumnArragementInfo(int sessionId, int documentList , int nodeId, Vector columnInfo){
    	 Session session = getSession(sessionId);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try{	
        	 Vector result = new Vector();
	         result = vws.getCustomList(sessionId, session.room, session.user, documentList, columnInfo, nodeId);
	         if (documentList != 15) {
	        	 columnInfo.addAll(getColumnInfo(result));
	         } else {
	        	 columnInfo.addAll(getColumnArrangementInfo(result));
	         }
	         VWClient.printToConsole("getColumnArragementInfo return value :"+columnInfo);
	         //VWSLog.err("  columnInfo "+columnInfo.toString());
	     }catch(Exception e){
	    	 return Error;
	     }
	 return NoError;	 
     }
     
 	//Desc   :Added method 'getFTSEnabledPages()' which will give the FTS Enabled Pages list.
 	//Author :Nishad Nambiar
 	//Date   :13-Nov-2007
     public int getFTSEnabledPages(int sid, int nodeId, Vector vecPages)
     {
         Session session = getSession(sid);
         VWClient.printToConsole("sessionId in getFTSEnabledPages::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try
         {
             Vector ret = vws.getFTSEnabledPages(session.room, sid, nodeId, vecPages);
             vecPages.addAll(ret);
             return NoError; 
         }
         catch(Exception re)
         {
             return Error;
         }
     }     
     
     
 	//Desc   :Added new method for BIFFileParser utility.
  	//Author :Nishad Nambiar
  	//Date   :21-Jan-2008
    public int createDocumentFromBIF(int sid, String bifFilePath) {
		int returnCode = -1;
		Session session = getSession(sid);
		if (session == null)
			return invalidSessionId;
		try {
			VWClient vwc = this;
			returnCode = EHGeneralPanel.createDocumentFromBIF(sid, vwc, bifFilePath);
		} catch (Exception e) {
			printToConsole("Exception in VWClient.createDocumentFromBIF() Method :" + e.toString());
			return Error;
		}
		return returnCode;
	}  
    
    public int getKeyIndexName(int sid, int docTypeId, Vector vecKeyIndexName)
    {
        Session session = getSession(sid);
        VWClient.printToConsole("sessionId in getKeyIndexName::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
            Vector ret = vws.getKeyIndexName(session.room, sid, docTypeId, vecKeyIndexName);             
            vecKeyIndexName.addAll(ret);
            VWClient.printToConsole("Active Error in getKeyIndexName--->"+vws.getActiveError());
           // return vws.getActiveError();
            if (sid>0 && vws.getActiveError()<0 ){
         	   return NoError;
            } else {
         	   return vws.getActiveError();
            }
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getMessageIDExpiry(int sid) {
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in getMessageIDExpiry::"+sid);
        if(session==null) return defaultMsgLifeTime;
        VWS vws = getServer(session.server);
        if (vws == null) return defaultMsgLifeTime;
        try
        {
            int msgLifeTime = vws.getMessageIDExpiry(session.room);             
            return msgLifeTime;
        }
        catch(Exception e)
        {
            return defaultMsgLifeTime;
        }

    }
    public int setMessageIDExpiry(int sid, int hours){
    	Session session = getSession(sid);
    	VWClient.printToConsole("sessionId in setMessageIDExpiry::"+sid);
        if(session==null) return invalidSessionId;
        VWS vws = getServer(session.server);
        if (vws == null) return ServerNotFound;
        try
        {
           vws.setMessageIDExpiry(session.room, hours);             
           return NoError;
        }
        catch(Exception e)
        {
            return Error;
        }    	
    }
	public static void printToConsole(String msg)
	{
    	try
    	{
        	Preferences clientPref =  null;        	
        	boolean displayFlag = false;
        	try{
        		clientPref =  Preferences.userRoot().node("/computhink/" + PRODUCT_NAME.toLowerCase() + "/VWClient");
        		displayFlag  = clientPref.getBoolean("debug",false);
        	}catch(Exception ex){
        		displayFlag = false;
        	}
        	
        	if (displayFlag)
        	{
        		String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
				msg = now + " : " + msg + " \n";    			
				java.util.Properties props = System.getProperties(); 
		     	String filepath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + File.separator + "VWClient.log";
		     	//System.out.println("log filepath :"+filepath);
		     	File log = new File(filepath);
				String logFilePath = null;
				if (log.exists()) {
					if ((log.length() / (1024 * 1024)) >= 1) {						
						logFilePath = filepath.substring(0, (filepath.length() - 4));
						log.renameTo(new File(logFilePath +"_"+ Util.getNow(1) + ".log"));
					}					
				}
				//System.out.println("before writing message in log file...");
		        FileOutputStream fos = new FileOutputStream(log, true);
		        fos.write(msg.getBytes());
		        fos.close();            
		        fos.close();
		        log = null;  
 
        	}
        }catch (Exception e)
        {
        	System.out.println( "Exception in while writing message in log file "+e +" "+e.getMessage() );	
        }
	}
	   public int isUserExistsInRoute(int sid, int userId, Vector settingsValue){
	        Session session = getSession(sid);
	      	VWClient.printToConsole("sessionId in isUserExistsInRoute::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	    	int result = -1;
	        try{
	        	result = vws.isUserExistsInRoute(session.room, sid, userId);	        		 
	        }catch(Exception ex){
	        	return Error;
	        }
	    	return result;
	    }   
	   /*********************************************************************************************	   
	    * The following methods are related to lookup documet type data from external table. 	*	
	    * Populate the value from  the external table based on trigger value sent by DTC		*
	    *********************************************************************************************
	    */
/*	   public int setDSNInfo(int sid, int docTypeId, String dsnName, String userName, String password, String externalTable){
	       Session session = getSession(sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;	  
	        try{
	            int result = vws.setDSNInfo(session.room, sid, docTypeId, dsnName, userName, password, externalTable);	        		 
	        }catch(Exception ex){
	        	return Error;
	        }       
	       return NoError;
	   }
*/	   public int setDBLookup(int sid, DBLookup lookupInfo){
	       Session session = getSession(sid);
	       VWClient.printToConsole("sessionId in setDBLookup::"+sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;	  
	        try{
	            int result = vws.setDBLookup(session.room, sid, lookupInfo);	        		 
	        }catch(Exception ex){
	        	return Error;
	        }       
	       return NoError;
	   }

	   
		public int getDSNInfo(int sid, int docTypeId, Vector dsnInfo){
			return getDSNInfo(sid, docTypeId, 0, 0, dsnInfo);
		}

	   public int getDSNInfo(int sid, int docTypeId, int indexId, int dsnType, Vector dsnInfo){
	       Session session = getSession(sid);
	       VWClient.printToConsole("sessionId in getDSNInfo::"+sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;
	        try{
	            Vector result = new Vector(); 
	            result = vws.getDSNInfo(session.room, sid, docTypeId, indexId, dsnType);
	            dsnInfo.addAll(result);
	        }catch(Exception ex){
	        	return Error;
	        }
	       
	       return NoError;
	   }
/*	   public int setIndexMapping(int sid, DocType dt, int triggerDataType){
	       Session session = getSession(sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;
	        try{
	            int result = vws.setIndexMapping(session.room, sid, dt, triggerDataType);
	        }catch(Exception ex){
	        	return Error;
	        }
	       
	       return NoError;
	   }*/
	   public int getIndexMapping(int sid, DocType dt){
		   Session session = getSession(sid);
		   VWClient.printToConsole("sessionId in getIndexMapping::"+sid);
		   if(session==null) return invalidSessionId;
		   VWS vws = getServer(session.server);
		   if (vws == null) return ServerNotFound;
		   try{
			   getDocTypeIndices(sid, dt);	            
			   Vector result = vws.getIndexMapping(session.room, sid, dt);	            
			   Hashtable mapList = new Hashtable();
			   if (result != null && result.size() > 0){
				   for (int i = 0; i < result.size(); i++){
					   String data = result.get(i).toString();
					   StringTokenizer tokens = new StringTokenizer(data, Util.SepChar);
					   if (tokens.hasMoreTokens()){
						   String indexId = tokens.nextToken();
						   if (indexId != null && indexId.trim().length() > 0){
							   mapList.put(indexId.trim(), data);
						   }
					   }
				   }

				   Vector indices = dt.getIndices();
				   Vector modifedIndices = new Vector();
				   for (int count = 0; count < indices.size(); count++){
					   Index idx = (Index) indices.get(count);
					   idx.setExternalColumn("");
					   /**
					    * Issue fix on Feb 28 - Code commented for below functionality.
					    * If date column is not mapped in DBLookup of AW and date field property set to Current Date, 
					    * then while creating document , date value should remain as the current date itself even on clicking of DBLookup button.
					    */
					   //idx.setMask("");
					   if (mapList.containsKey(String.valueOf(idx.getId()))){
						   String indexId = "";
						   String externalColumn = "";
						   String colDataType = "";
						   String triggerColumn= "";
						   String mask = "";
						   String data = mapList.get(String.valueOf(idx.getId())).toString();	        		
						   try{
							   StringTokenizer tokens = new StringTokenizer(data, Util.SepChar);
							   if (tokens.hasMoreTokens()) indexId = tokens.nextToken();
							   if (tokens.hasMoreTokens()) externalColumn = tokens.nextToken();
							   if (tokens.hasMoreTokens()) colDataType = tokens.nextToken();
							   if (tokens.hasMoreTokens()) triggerColumn = tokens.nextToken();
							   if (tokens.hasMoreTokens()) mask = tokens.nextToken();
							   idx.setExternalColumn(externalColumn);
							   idx.setMask(mask);
							   if (triggerColumn.trim().equalsIgnoreCase("1")){
								   idx.setTriggerEnabled(true);
								   /*dt.setTriggerIndexId(idx.getId());
								   dt.setTriggerIndexDataType(Util.to_Number(colDataType));*/
							   }
							   idx.setTriggerDataType(Util.to_Number(colDataType));

						   }catch(Exception ex){
							   System.out.println("Err " + ex.getMessage());
							   return Error;
						   }	        		
					   }
					   modifedIndices.add(idx);
				   }
				   dt.setIndices(modifedIndices);
			   }
		   }catch(Exception ex){
			   return Error;
		   }

		   return NoError;
	   }
	   public int getExternalData(int sid, DocType dt){
	       return getExternalData(sid, dt, new Vector());
	   }
	   public int getExternalData(int sid, DocType dt, Vector outputLog){
	       Session session = getSession(sid);
	       VWClient.printToConsole("sessionId in getExternalData::"+sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;
	       int activeError=0;
	       try{
		   //Calling the DocType Indices to get the indices properties;
	    	   /**
	    	    * Commented getDocTypeIndices: Reason - This call will over write the indices which will 
	    	    * lead to loose the trigger index value.
	    	    */
		   //getDocTypeIndices(sid, dt);
		   Vector output = vws.getExternalData(session.room, sid, dt);
		   if (output != null && output.size() > 0){
		       int lastPos = output.size() -2;
		       DocType resultDocType= (DocType)output.get(lastPos);
		       dt.setIndices(resultDocType.getIndices());  
		       output.removeElementAt(lastPos);
		       activeError=Integer.parseInt(output.get(output.size() -1).toString());
		       output.removeElementAt(output.size() -1);
		       outputLog.addAll(output);
		   }
		    if(activeError==-779)
		    	return ViewWiseErrors.dsnConnError;
		    else if (activeError == -900)
		    	return ViewWiseErrors.dsnLkupConnError;
	       }catch(Exception ex){
	    	   VWClient.printToConsole("exception inside getExternalData:::"+ex);
		   return Error;
	       }
	       return NoError;
	   }
	   public int getExternalLookupColumns(int sid, String dsnName, String userName, String passWord, String externalTable, String query, Vector columnList) {
	       Session session = getSession(sid);
	       VWClient.printToConsole("sessionId in getExternalLookupColumns::"+sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;
	       try{
		   Vector result = new Vector();
		   result = vws.getExternalLookupColumns(session.room, sid, dsnName, userName, passWord, externalTable, query);
		   columnList.addAll(result);
	       }catch(Exception ex){
		   return Error;
	       }
	       return NoError;
	   }
	   /*Enhancement for custom color*/
	   public int getNodeRGBColor(int sid, Vector output){
	   	 Session session = getSession(sid);
	      VWClient.printToConsole("sessionId in getNodeRGBColor::"+sid);
         if(session==null) return invalidSessionId;
         VWS vws = getServer(session.server);
         if (vws == null) return ServerNotFound;
         try{        	
    	 Vector result = new Vector(); 
    	 result = vws.getNodeRGBColor(session.room, sid);
    	 output.addAll(vectorToVWColor(result));
    	 }catch(Exception ex){
         	return Error;
         }
     	return NoError;		   	
	   }
	   public int setNodeCustomColor(int sid, int nodeId, int colorId, boolean type, boolean inherit){
		   return setNodeCustomColor(sid, nodeId, colorId, type, inherit, 1);
	   }
	   public int setNodeCustomColor(int sid, int nodeId, int colorId, boolean type, boolean inherit, int scope){
		   Session session = getSession(sid);
		   VWClient.printToConsole("sessionId in setNodeCustomColor::"+sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;
	       try{ 
	    	     vws.setNodeCustomColor(session.room, sid, nodeId, colorId, type, inherit, scope);
	       }catch(Exception ex){
	        	return Error;
	       }
	       return NoError;
		   }
	   public int getNodeCustomColorCode(int sid, int nodeId, Vector output){
		   Session session = getSession(sid);
		   VWClient.printToConsole("sessionId in getNodeCustomColorCode::"+sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;
	       try{	       
	       Vector result =  new Vector();
	       result = vws.getNodeCustomColorCode(session.room,sid,nodeId);
	       if (result != null && result.size() > 0){		      
			   String data = result.get(0).toString();
			   StringTokenizer st = new StringTokenizer(data, "\t");	       
			   output.add(st.nextToken());	//Color Code
			   output.add(st.nextToken());  //Inherit
			   output.add(st.nextToken());	//scope
	       }
	       }catch(Exception ex){
	        	return Error;
	       }
	       return NoError;
	   	}	 
	   public int deleteNodeCustomColorCode(int sid, int nodeId){
		   Session session = getSession(sid);
		   VWClient.printToConsole("sessionId in deleteNodeCustomColorCode::"+sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;
	       try{	       
	      vws.deleteNodeCustomColorCode(session.room,sid,nodeId);	       
	       }catch(Exception ex){
	        	return Error;
	       }
	       return NoError;
	   	}	 
	   
	    private Vector vectorToVWColor(Vector v)
	    {
	        Vector colors = new Vector();
	        for (int i = 0; i < v.size(); i++)
	        {
	        		colors.add( new VWColor( (String) v.get(i)));
	        }
	        return colors;
	    }
	 /**
	     * getAvailableRetention - is to get the availabe retentions for the logged in room
	     * @param sid
	     * @param availRetention
	     * @return
	     */
	    // Yet to get the stored procedure for this - All the retentions for the room level
	    public int getAvailableRetention(int sid, int docTypeId, Vector availRetention)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getAvailableRetention::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = 
	                   vws.getAvailableRetentions(session.room, sid, availRetention);
	            availRetention.addAll(ret);
	            return NoError;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    /**
	     * getRetentionSettings - This is to get the retention settings for selected retention id 
	     * @param sid
	     * @param docTypeId
	     * @param availRetention
	     * @return
	     */
	    public int getRetentionSettings(int sid, int retentionId, Vector retentionSettings)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getRetentionSettings::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = 
	                   vws.getRetentionSettings(session.room, sid, retentionId, retentionSettings);
	            System.out.println("In VWClient is : "+retentionSettings.size());
	            retentionSettings.addAll(ret);
	            return NoError;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    /**
	     * getRetentionIndexInfo - getting the index info for the selected retention id
	     * @param sid
	     * @param retentionId
	     * @param retentionIndexInfo
	     * @return
	     */
	    public int getRetentionIndexInfo(int sid, int retentionId, Vector retentionIndexInfo)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getRetentionIndexInfo::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = 
	                   vws.getRetentionIndexInfo(session.room, sid, retentionId, retentionIndexInfo);
	            retentionIndexInfo.addAll(ret);
	            return NoError;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    /**
	     * setRetentionSettings - used to set the retention settings
	     * @param sid
	     * @param retention
	     * @param retentionId
	     * @return
	     */
	    public int setRetentionSettings(int sid, VWRetention retention, Vector retentionId)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in setRetentionSettings::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	           return vws.setRetentionSettings(session.room, sid, retention, retentionId);
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    /**
	     * setIndexConditions - This method is used to set the indices condition for the documen type
	     * @param sid
	     * @param retention
	     * @return
	     */
	    public int setIndexConditions(int sid, VWRetention retention)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in setIndexConditions::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	           return vws.setIndexConditions(session.room, sid, retention);
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    /**
	     * delRetentionSettings - This method is used to delete the retention settings for the selected retention id
	     * @param sid
	     * @param retention
	     * @return
	     */
	    public int delRetentionSettings(int sid, VWRetention retention, int deleteOption) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in delRetentionSettings::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	           return vws.delRetentionSettings(session.room, sid, retention, deleteOption);
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
		}
		
	    /**
	     * delIndexInfo - This method is used to delete the indices condition for the selected retention id 
	     * @param sid
	     * @param retention
	     * @return
	     */
		public int delIndexInfo(int sid, VWRetention retention) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in delIndexInfo::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	           return vws.delIndexInfo(session.room, sid, retention);
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
		}
		
		/**
		 * isRetentionNameFound - It is used to check for the duplication of the retentionName
		 * @param sid
		 * @param retentionName
		 * @return
		 */
		public int isRetentionNameFound(int sid, String retentionName){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in isRetentionNameFound::"+sid);
		    if(session==null) return invalidSessionId;
		    VWS vws = getServer(session.server);
		    if (vws == null) return ServerNotFound;
		    int retentionId =0;
		    try{        	
		    	retentionId = vws.isRetentionNameFound(session.room, sid, retentionName);        	
		        //printToConsole("isRetentionNameFound " + retentionId);        	
		    }catch(Exception ex){}
			return retentionId;
		}
		
		/**
		 * getRetentionCompletedDocs - It is used to get the info of processed document for the selected retention id
		 * @param sid
		 * @param retentionId
		 * @param retentionSettings
		 * @return
		 */
		public int getRetentionCompletedDocs(int sid, int retentionId, Vector retentionSettings)
	    {
	        Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getRetentionCompletedDocs::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = 
	                   vws.getRetentionCompletedDocs(session.room, sid, retentionId, retentionSettings);
	            //System.out.println("Size of return value of vector is : "+ret.size()); 
	            retentionSettings.addAll(ret);
	            return NoError;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
		public int getNotifyDocumentList(int sid, Vector notifyDocs) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getNotifyDocumentList::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = 
	                   vws.getNotifyDocumentList(session.room, sid, notifyDocs);
	            VWSLog.add("ret is : "+ret);
	            VWSLog.add("notifyDocs is : "+notifyDocs);
	            notifyDocs.addAll(ret);
	            VWSLog.add("After assigning notifyDocs is : "+notifyDocs);
	            return NoError;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
		}		    	  
		public int getGroupMembers(int sid, String groupId, Vector memberList) {
	       Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getGroupMembers::"+sid);
	       if(session==null) return invalidSessionId;
	       VWS vws = getServer(session.server);
	       if (vws == null) return ServerNotFound;
	       try{
			   Vector result = new Vector();
			   result = vws.getGroupMembers(session.room, sid, groupId);
			   memberList.addAll(result);
	       }catch(Exception ex){
		   return Error;
	       }
	       return NoError;
	   }
		public int delEmptyUsers(int sid) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in delEmptyUsers::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try
			{
				int result = vws.deleteEmptyUsers(session.room);
				return result;

			}catch(Exception e)
			{
				return Error;
			} 
		}
		public int isDSSActive(int sid){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in isDSSActive::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try{
				int result = vws.isDSSActive(session.room, sid);
				return result;
			}catch(Exception ex){
				//VWSLog.add("exception in isDSSActive :: "+ex.getMessage());
				return Error;
			}
		}
		
		public int getPurgeDocs(int sid, Vector purgeDocsFlag){
			
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getPurgeDocs::"+sid);
	 		if(session==null) return invalidSessionId;
	 		VWS vws = getServer(session.server);
	 		if (vws == null) return ServerNotFound;
	 		try
	 		{
	 			boolean flag = vws.getPurgeDocs(session.room, sid, purgeDocsFlag);
	 			if(flag)
	 				purgeDocsFlag.add("true");
	 			else
	 				purgeDocsFlag.add("false");
	 		 	 VWClient.printToConsole("Active Error in getPurgeDocs--->"+vws.getActiveError());
	            //return vws.getActiveError();
	 		 	 if (sid>0 && vws.getActiveError()<0 ){
	 	        	   return NoError;
	 	           } else {
	 	        	   return vws.getActiveError();
	 	           }
	 		}
	 		catch(Exception e)
	 		{
	 			return Error;
	 		}
		}
		
		public int getKeepDocType(int sid, Vector keepdoctypeFlag){
			
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getKeepDocType::"+sid);
	 		if(session==null) return invalidSessionId;
	 		VWS vws = getServer(session.server);
	 		if (vws == null) return ServerNotFound;
	 		try
	 		{
	 			boolean flag = vws.getKeepDocType(session.room, sid, keepdoctypeFlag);
	 			if(flag)
	 				keepdoctypeFlag.add("true");
	 			else
	 				keepdoctypeFlag.add("false");
	 			VWClient.printToConsole("Active Error in getKeepDocType--->"+vws.getActiveError());
	           //return vws.getActiveError();
	 			 if (sid>0 && vws.getActiveError()<0 ){
	 	        	   return NoError;
	 	           } else {
	 	        	   return vws.getActiveError();
	 	           }
	 		}
	 		catch(Exception e)
	 		{
	 			return Error;
	 		}
		}
		
		public int setPurgeDocs(int sid, String purgeDocsFlag){
			
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in setPurgeDocs::"+sid);
	 		if(session==null) return invalidSessionId;
	 		VWS vws = getServer(session.server);
	 		if (vws == null) return ServerNotFound;
	 		try
	 		{
	 			int  ret = vws.setPurgeDocs(session.room, sid, purgeDocsFlag);
	 			VWClient.printToConsole("Active Error in setPurgeDocs--->"+vws.getActiveError());
	           // return vws.getActiveError();
	 			 if (sid>0 && vws.getActiveError()<0 ){
	 	        	   return NoError;
	 	           } else {
	 	        	   return vws.getActiveError();
	 	           }
	 		}
	 		catch(Exception e)
	 		{
	 			return Error;
	 		}
		}
		public int setKeepDocType(int sid, String keepDocTypeFlag) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in setKeepDocType::"+sid);
	 		if(session==null) return invalidSessionId;
	 		VWS vws = getServer(session.server);
	 		if (vws == null) return ServerNotFound;
	 		try
	 		{
	 			int  ret = vws.setKeepDocType(session.room, sid, keepDocTypeFlag);
	 			VWClient.printToConsole("Active Error in setKeepDocType--->"+vws.getActiveError());
	            //return vws.getActiveError();
	 			 if (sid>0 && vws.getActiveError()<0 ){
	 	        	   return NoError;
	 	           } else {
	 	        	   return vws.getActiveError();
	 	           }
	 		}
	 		catch(Exception e)
	 		{
	 			return Error;
	 		}
		}

		public int revokeDocSignature(int sid, int documentId){

			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in revokeDocSignature::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try
			{
				int  ret = vws.revokeDocSignature(session.room, sid, documentId);
				VWClient.printToConsole("Active Error in revokeDocSignature--->"+vws.getActiveError());
				//return vws.getActiveError();
				 if (sid>0 && vws.getActiveError()<0 ){
		        	   return NoError;
		           } else {
		        	   return vws.getActiveError();
		           }
			}
			catch(Exception e)
			{
				return Error;
			}
		}
		
		public int getARSServer(int sid){
			
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getARSServer::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try
			{
				int  ret = vws.getARSServer(session.room, sid);
				return ret;
			}
			catch(Exception e)
			{
				return Error;
			}
		}

		public int getTreeDetails(int sid, Vector treeDetails){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getTreeDetails::"+sid);
			if(session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if(vws==null) return ServerNotFound;

			try{
				Vector treeView = new Vector();
				treeView = vws.getTreeDetails(session.room, sid);				
				treeDetails.addAll(treeView);    		
			}catch(Exception ex){
				return Error;
			}
			return NoError;
		}
		public void openRecoverDocuments(VWClient vwc, int sid, String roomName) {
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			try {
				if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type) {
	        		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} else {
					//UIManager.setLookAndFeel(plasticLookandFeel);
				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
			VWRecoverDocument recover = new VWRecoverDocument(vwc, sid, roomName);
			new Thread(recover).start();	
			
		}
		
		/*
		 * 
		 * 
		 * Notification Service Methods
		 * 
		 */

		public int openNotificationSettings(int sid, Node node)
	    {
			return openNotificationSettings(sid, node, null);
	    }
	    public int openNotificationSettings(int sid, Node node, JDialog parent)
	    {
	        try 
	        {
	        	if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type) {
	        		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	        	} else {
	        		String plasticLookandFeel = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
	        		//UIManager.setLookAndFeel(plasticLookandFeel);
	        	}
	        }
	        catch(Exception e) 
	        {}

	        VWFolderNotification vwFolderNotification = new VWFolderNotification(this, sid, node, parent);
			new Thread(vwFolderNotification).start();
	        return NoError;
	    }
	    public int openNotificationSettings(int sid, Document doc)
	    {
	    	return openNotificationSettings(sid, doc, null);
	    }
	    
	    public int openNotificationSettings(int sid, Document doc, JDialog parent)
	    {
	        try 
	        {
	        	if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type) {
	        		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	        	} else {
	        		String plasticLookandFeel = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
	        		//UIManager.setLookAndFeel(plasticLookandFeel);
	        	}	        	
	        }
	        catch(Exception e) 
	        {}
	        
	        VWDocNotification vwDocNotification = new VWDocNotification(this, sid, doc, parent);
			new Thread(vwDocNotification).start();	
	        return NoError;
	    }

	    
	    public int saveNotificationSettings(int sid, NotificationSettings settings) {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in saveNotificationSettings::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try {
	    		vws.saveNotificationSettings(session.room, sid, settings);
	    	} catch (Exception ex) {
	    		return Error;
	    	}
	    	return NoError;
	    }
		
	    public int loadNotificationSettings(int sid, int nodeId, String userName, String module, NotificationSettings settings) {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in loadNotificationSettings::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;

	    	try {
	    		int isAdmin = isAdmin(sid);
	    		Vector output = new Vector();
	    		output = vws.loadNotificationSettings(session.room, sid, nodeId, isAdmin, userName, module);
	    		vectorToNotificationSettings(output, settings);	    		
	    	} catch (Exception ex) {
	    		return Error;
	    	}
	    	return NoError;
	    }

	    public int getAllNotifications(int sid, int isAdmin, String userName, String module, Vector settings) {
	    	Session session = getSession(sid);
	       	VWClient.printToConsole("sessionId in getAllNotifications::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;

	    	try {
	    		Vector output = new Vector();
	    		output = vws.loadNotificationSettings(session.room, sid, 0, isAdmin, userName, module);
	    		settings.addAll(vectorToNotificationSettingsList(output));
	    	} catch (Exception ex) {
	    		return Error;
	    	}
	    	return NoError;
	    }

	    private Vector vectorToNotificationSettingsList(Vector v)
	    {
	    	//1     10    1     5     Documents is moved      Document	1     1     1     vwadmin     10.4.8.145  11      srikanth.a@syntaxsoft.com;pandiya.m@syntaxsoft.com
	    	//2     10    1     6     Documents is added      Document	1     1     1     vwadmin     10.4.8.145  11      srikanth.a@syntaxsoft.com;pandiya.m@syntaxsoft.com
	    	
	    	Vector output = new Vector();	    		    	 
	    	int count = 0;
	    	String  currentId ="";
	    	String  previousId ="";
	    	
	    	String currentType = "";
	    	String previousType = "";
	    	Vector data = new Vector();
	    	try{
		    	for (int i = 0; i < v.size(); i++)
		        {	
		        	StringTokenizer st = new StringTokenizer((String) v.elementAt(i), "\t");
		        	st.nextToken();//id
		        	currentId = st.nextToken();//nodeid
		        	st.nextToken(); // nodeName
		        	currentType = st.nextToken();//nodetype 0 folder , 1 - document
		        	NotificationSettings settings = new NotificationSettings();
		        	if ((currentId != null && (previousId.length() == 0) || previousId.equals(currentId)) 
		        			&& (currentType != null && (previousType.length() == 0) || previousType.equals(currentType))){
		        		previousId = currentId;
		        		previousType = currentType;
		        		data.add(v.get(i));	        		
		        	}else if (data.size() > 0){
		        		settings = new NotificationSettings();
		        		vectorToNotificationSettings(data, settings);
		        		output.add(settings);
		        		data = new Vector();
		        		i--;
		        		previousId ="";
		        		previousType = "";
		        	}/*else{
	        		data.add(v.get(i));
	        		NotificationSettings settings = new NotificationSettings();
	        		vectorToNotification(data, settings);
	        		output.add(settings);	        		
	        	}*/

		        	if(i == (v.size()-1)){
		        		settings = new NotificationSettings();
		        		vectorToNotificationSettings(data, settings);
		        		output.add(settings);
		        		data = new Vector();
		        	}
		        }
	    	}catch(Exception e){
	    		System.out.println("Exception vectorToNotificationList() :: "+e.getMessage());
	    		e.printStackTrace();
	    	}
	        return output;
	    }

	    
	    private int vectorToNotificationSettings(Vector v, NotificationSettings settings)
	    {
	    	//1     10    1     5     Documents is moved      Document	1     1     1     vwadmin     10.4.8.145  11      srikanth.a@syntaxsoft.com;pandiya.m@syntaxsoft.com
	    	//2     10    1     6     Documents is added      Document 	1     1     1     vwadmin     10.4.8.145  11      srikanth.a@syntaxsoft.com;pandiya.m@syntaxsoft.com
	    	//1		280	  122	1	1	Any changes to this Document	Document	1	0	0	vwadmin	10.4.8.149	11	vijaykumar.s@syntaxsoft.com;dd	-
	    	Vector docs = new Vector();	    	
	    	Notification[] notificationList = new Notification[v.size()]; 
	    	try{
		        for (int i = 0; i < v.size(); i++)
		        {	        	
		        	 StringTokenizer st = new StringTokenizer((String) v.elementAt(i), "\t");
		        	 int settingsId = Util.to_Number(st.nextToken());
		        	 settings.setReferenceId(Util.to_Number(st.nextToken()));//nodeId
		        	 settings.setNodeName(st.nextToken());
		        	 settings.setNodeType(Util.to_Number(st.nextToken()));
		        	 Notification notification = new Notification(Util.to_Number(st.nextToken()), st.nextToken(), st.nextToken(), settingsId);
		        	 notificationList[i] = notification;
		        	 settings.setNotifyCaptureType(Util.to_Number(st.nextToken()));
		        	 settings.setProcessType(Util.to_Number(st.nextToken()));
		        	 settings.setFolderInherit(Util.to_Number(st.nextToken()));
		        	 settings.setDocumentInherit(Util.to_Number(st.nextToken()));
		        	 settings.setUserName( st.nextToken());
		        	 settings.setIpAddress( st.nextToken());
		        	 settings.setAttachment(st.nextToken());
		        	 settings.setEmails( st.nextToken());
		        	 settings.setLastNotifyTime(st.nextToken()); // For Last Alert
		        }
		        settings.setNotifications(notificationList);
	    	}catch(Exception e){
	    		System.out.println("Exception in vectorToNotification() :: "+e.getMessage());
	    		e.printStackTrace();
	    	}
	        return NoError;
	    }
	    
	    public int readNotificationDetails(String room, int sid, String FailedFlag, String NotificationRetries, Vector notificationHistoryVector){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in readNotificationDetails::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) {
	    		VWClient.printToConsole("returning ServerNotFound from readNotificationDetails method....");
	    		return ServerNotFound;
	    	}
	    	
	    	try {
	    		Vector notificationDetails = new Vector();
	    		notificationDetails = vws.readNotificationDetails(room, sid, FailedFlag, NotificationRetries);
	    		
	    		Vector notificationHistory = vectorToNotificationHistoryList(notificationDetails);
	    		
	    		notificationHistoryVector.addAll(notificationHistory);
	    	} catch (Exception ex) {
	    		return Error;
	    	}
	    	return NoError;
	    }
	    
		private Vector vectorToNotificationHistoryList(Vector notificationDetails) {
			// TODO Auto-generated method stub
			Vector notifyHistoryVector= new Vector();
			
			if(notificationDetails!=null && notificationDetails.size()>0){
				
				for(int i=0; i<notificationDetails.size(); i++){
					String dbstring = notificationDetails.get(i).toString();
					NotificationHistory notifyHistory = new NotificationHistory(dbstring);
					notifyHistoryVector.add(notifyHistory);
				}
			}
			/*if(notifyHistoryVector!=null && notifyHistoryVector.size()>0)
				//VNSLog.add("notifyHistoryVector : "+notifyHistoryVector.size());
			else
				//VNSLog.add("notifyHistoryVector is null");
*/			return notifyHistoryVector;
		}
		
		public int getLoggedInUserMailId(int sid, Vector loggedInUserMailId)
	 	{
	 		Session session = getSession(sid);
	 	  	VWClient.printToConsole("sessionId in getLoggedInUserMailId::"+sid);
	 		if(session==null) return invalidSessionId;
	 		VWS vws = getServer(session.server);
	 		if (vws == null) return ServerNotFound;
	 		try
	 		{
	 			Vector mailId = new Vector();
	 			mailId = vws.getLoggedInUserMailId(session.room, sid);
	 			loggedInUserMailId.addAll(mailId);
	 			return vws.getActiveError();
	 		}
	 		catch(Exception e)
	 		{
	 			return Error;
	 		}
	 	}	
		
		public int deleteNotificationSettings(int sid, NotificationSettings settings){
			Session session = getSession(sid);
		  	VWClient.printToConsole("sessionId in deleteNotificationSettings::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	
	    	try {
	    		return vws.deleteNotificationSettings(session.room, sid, settings);	    		
	    	} catch (Exception ex) {
	    		return Error;
	    	}

		}
	    
		public int addNotificationHistory(int sid, int nodeId, int nodeType, int notifyId, String message){
			Session session = getSession(sid);
		 	VWClient.printToConsole("sessionId in addNotificationHistory::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try {
	    		vws.addNotificationHistory(session.room, sid, nodeId, nodeType, notifyId, session.user, "-", "-", message);
	    	}catch(Exception ex){
	    		return Error;
	    	}
			return NoError;
		}
		
		public int openExistingNotifications(int sid)
	    {
	        try 
	        {
	        	if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type) {
	        		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	        	} else {
	        		/*String plasticLookandFeel = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
	        		UIManager.setLookAndFeel(plasticLookandFeel);*/
	        	}
	        }
	        catch(Exception e) 
	        {
	        	printToConsole("Exception is : "+e.getMessage());
	        }

	        VWExistingNotifications vwExistingNotifications = new VWExistingNotifications(this, sid);
			new Thread(vwExistingNotifications).start();
	        return NoError;
	    }
		public int setNotificationHistory(int sid, int folderId, int nodeId, int nodeType, int settingsId, String userName, String message){
			Session session = getSession(sid);
		 	VWClient.printToConsole("sessionId in setNotificationHistory::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try {
	    		vws.setNotificationHistory(session.room, sid, folderId, nodeId, nodeType, settingsId, "-", "-", userName, message);
	    	}catch(Exception ex){
	    		return Error;
	    	}
			return NoError;
		}
		
		public int CheckNotificationExist(int sid, int nodeId, int nodeType, int notifyId, String userName){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in CheckNotificationExist::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	int settingsId = -1;
	    	try {
	    		settingsId = vws.CheckNotificationExist(session.room, sid, nodeId, nodeType, notifyId, userName);
	    	}catch(Exception ex){
	    		return Error;
	    	}
			return settingsId;
		}
		public int getNotificationListByModule(int sid, String module, Vector output) {
		Session session = getSession(sid);
		VWClient.printToConsole("sessionId in getNotificationListByModule::"+sid);
		if (session == null) return invalidSessionId;
		VWS vws = getServer(session.server);
		if (vws == null) return ServerNotFound;
		try {
			if (notificationList != null && notificationList.size() == 0) {
				getNotificationList(sid, notificationList);
			}
			for (int count = 0; count < notificationList.size(); count++) {
				Notification notification = (Notification) notificationList.get(count);
				if (notification.getModule().equalsIgnoreCase(module.trim())) {
					output.add(notification);
				}
			}
		} catch (Exception ex) {
			output = new Vector();
			return Error;
		}

		return NoError;
	}

		
		public int getNotificationList(int sid, Vector output){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getNotificationList::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try {
	    		Vector result = vws.getNotificationList(session.room, sid);
	    		output.addAll(vectorToNotificationList(result));
	    	}catch(Exception ex){
	    		return Error;
	    	}
			
			return NoError;	
		}
		public Vector vectorToNotificationList(Vector srcNotification){
			Vector result = new Vector();
			for (int i = 0; i < srcNotification.size(); i++){
				Notification notification = new Notification(srcNotification.get(i).toString());
				result.add(notification);
			}
			return result;			
		}
		public Vector notificationList = new Vector();
		
		public int setActiveClient(int sid){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in setActiveClient::"+sid);
			if(session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if(vws==null) return ServerNotFound;
			try{
				vws.setActiveClient(session.room, sid);
			}catch(Exception ex){
				return Error;
			}
			return NoError;
		}
		
		public int getExtractedDocInfo(int sid, int docId, String destinationPath, Vector vwDocInfo){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getExtractedDocInfo::"+sid);
			if(session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if(vws==null) return ServerNotFound;
			try{
				Document doc = new Document(docId);
				//int ret = getDocFile(sid, doc, destinationPath);
				// printToConsole("ret : "+ret);
				VWPageInfo pageInfo = new VWPageInfo();
				ViewerInfo viewerInfo = new ViewerInfo();
				viewerInfo = pageInfo.getPageInfo(docId, destinationPath);
				
				vwDocInfo.add(viewerInfo);
				
			}catch(Exception ex){
				return Error;
			}
			return NoError;
		}
		
		public int getSignSettings(int sid, Vector signSettings)
	    {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getSignSettings::"+sid);
	        VWS vws = getServer(session.server);
	        if (vws == null) return Error;
	        try
	        {
	          Vector result = vws.getSignSettings(session.room, sid);
	          signSettings.addAll(result);
	          VWClient.printToConsole("Active Error in getSignSettings--->"+vws.getActiveError());
	          return vws.getActiveError();
	        }
	        catch(Exception e)
	        {
	        	return Error;
	        }
	        
	    }
		
		public int getSignTitle(int sid, Vector result)
	    {	
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getSignTitle::"+sid);
	        VWS vws = getServer(session.server);
	        if (vws == null) return Error;
	        try
	        {
	          Vector ret = vws.getSignTitle(session.room, sid);
	          result.addAll(ret);
	          VWClient.printToConsole("Active Error in getSignTitle--->"+vws.getActiveError());
	          return vws.getActiveError();
	        }
	        catch(Exception e)
	        {
	        	return Error;
	        }	        
	    }
		public int getIndexId(int sid, Index index) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getIndexId::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	        	return vws.getIndexId(session.room, sid, index);	            
	        }
	        catch(Exception e) 
	        {
	            return Error;
	        }
		}
		
		/**
		 * 
		 * public int getExtractedDocInfo(int sid, int docId, String destinationPath, Vector vwDocInfo){
			Session session = getSession(sid);
			if(session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if(vws==null) return ServerNotFound;
			try{
				Document doc = new Document(docId);
				
				String allDotZipLoc = "c:\\temp\\" + docId;
				getDocFile(sid, doc, allDotZipLoc);
				File fAllDotZip = new File(allDotZipLoc +"/all.zip");
				
				if (fAllDotZip.exists()){
	            	Util.CopyFile(fAllDotZip, new File(destinationPath + File.separator + DOC_CONTAINER));
	            }else{
	            	System.out.println("all.zip not exist");
	            }
				
				//getDocFile(sid, doc, destinationPath);
				VWPageInfo pageInfo = new VWPageInfo();
				ViewerInfo viewerInfo = new ViewerInfo();
				viewerInfo = pageInfo.getPageInfo(docId, destinationPath);
				
				vwDocInfo.add(viewerInfo);
				
			}catch(Exception ex){
				return Error;
			}
			return NoError;
		}
		 */
		
		public int getOptionSettings(int sid, Vector result){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getOptionSettings::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	        	  Vector ret = vws.getOptionSettings(session.room, sid, result);
		          result.addAll(ret);
		          VWClient.printToConsole("Active Error in getOptionSettings--->"+vws.getActiveError());
		          return vws.getActiveError();
		          
	        }
	        catch(Exception e) 
	        {
	            return Error;
	        }			
		}
		
		public int setOptionSettings(int sid, String result){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in setOptionSettings::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	        	  int ret = vws.setOptionSettings(session.room, sid, result);		          
		          return ret;
		          
	        }
	        catch(Exception e) 
	        {
	            return Error;
	        }			
		}
		
		public int getWebAccessUrl(int sid, Vector result)
		{	
			try
			{
				Session session = getSession(sid);
				VWClient.printToConsole("sessionId in getWebAccessUrl::"+sid);
				VWS vws = getServer(session.server);
				if (vws == null) return Error;

				Vector ret = vws.getWebAccessUrl(session.room, sid);
				result.addAll(ret);
				  VWClient.printToConsole("Active Error in getWebAccessUrl--->"+vws.getActiveError());
				//return vws.getActiveError();
				  if (sid>0 && vws.getActiveError()<0 ){
					  return NoError;
				  } else {
					  return vws.getActiveError();
				  }
			}
			catch(Exception e)
			{
				return Error;
			}	        
		}
		
		public int validateRouteDocument(int sid, int routeId, int docId, Vector result)
		{	
			try
			{
				Session session = getSession(sid);
				VWClient.printToConsole("sessionId in validateRouteDocument::"+sid);
				VWS vws = getServer(session.server);
				if (vws == null) return Error;

				Vector ret = vws.validateRouteDocument(session.room, sid, routeId, docId);
				result.addAll(ret);
				 VWClient.printToConsole("Active Error in validateRouteDocument--->"+vws.getActiveError());
				//return vws.getActiveError();
				 if (sid>0 && vws.getActiveError()<0 ){
		        	   return NoError;
		           } else {
		        	   return vws.getActiveError();
		           }
			}
			catch(Exception e)
			{
				return Error;
			}	        
		}
		public int getDocIndicesInRoute(int sid, int docId, Vector routeIndices){
			try
			{
				Session session = getSession(sid);
				VWClient.printToConsole("sessionId in getDocIndicesInRoute::"+sid);
				VWS vws = getServer(session.server);
				if (vws == null) return Error;

				Vector ret = vws.getDocIndicesInRoute(session.room, sid, docId);
				routeIndices.addAll(ret);
				VWClient.printToConsole("Active Error in getDocIndicesInRoute--->"+vws.getActiveError());
				//return vws.getActiveError();
				 if (sid>0 && vws.getActiveError()<0 ){
		        	   return NoError;
		           } else {
		        	   return vws.getActiveError();
		           }
			}
			catch(Exception e)
			{
				return Error;
			}	   
	    }
		
		/**
		 * Functionality : ChangeUserPWD() is used to change the user password. 
		 * Input : sessionid, principal Object, oldPassword, newPassword, confirmPassword.
		 * Return Value : 0 on success, -1 if any errors.
		 */
		public int ChangeUserPWD(int sid, Principal principal, String oldPassword, String newPassword, String confirmPassword) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in ChangeUserPWD::"+sid);
			if (session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			
			try {
				return  vws.changeUserPWD(session.room, sid, principal, oldPassword, newPassword, confirmPassword);
			} catch (Exception e) {
				return Error;
			}
		}
		
		
		public int resetUserPWD(int sid, Principal principal, String oldPassword, String newPassword, String confirmPassword) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in ChangeUserPWD::"+sid);
			if (session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			
			try {
				return  vws.resetUserPWD(session.room, sid, principal, oldPassword, newPassword, confirmPassword);
			} catch (Exception e) {
				return Error;
			}
		}
		
		public boolean getHostedAdmin(int sid){
			try {
				Session session = getSession(sid);
				VWClient.printToConsole("sessionId in getHostedAdmin::"+sid);
				VWS vws = getServer(session.server);
				if (vws == null) return false;

				return vws.getHostedAdmin(sid);
			}
			catch(Exception ex) {
				return false;
			}		
		}
		public boolean isServerMachine(String serverName){
			try{
				if (serverName == null) return false;
				VWS vws = getServer(serverName);
				if (vws == null) return false;

				ServerSchema schema = vws.getSchema();
				String serverIP = schema.getAddress();

				InetAddress serverInetAddress = InetAddress.getByName(serverIP);
				InetAddress clientInetAddress = InetAddress.getLocalHost();

				boolean isServerMachine = false;
				if(clientInetAddress.equals(serverInetAddress)){
					isServerMachine = true;
				}else{
					Enumeration<NetworkInterface> n = NetworkInterface.getNetworkInterfaces();
					for (; n.hasMoreElements();)
					{
						NetworkInterface e = n.nextElement();
						//VWClient.printToConsole("Interface: " + e.getName());
						Enumeration<InetAddress> a = e.getInetAddresses();
						for (; a.hasMoreElements();)
						{
							InetAddress clientNetworkInetAddr = a.nextElement();
							//VWClient.printToConsole("inetaddr : "+clientNetworkInetAddr);

							if(clientNetworkInetAddr.equals(serverInetAddress)){
								isServerMachine = true;
							}
						}
					}
				}
				return isServerMachine;
			}
			catch (Exception e) {
				return false;
			}
		}
		/* Enhancement:-Document keyField Changes
		 *  Method checkKeyIndex is added to do the procedure call 
		 * If the index value is empty it returns the index name 
		 * otherwise returns zero.
		 *  Release :Build5-001
		 * Added By Madhavan
		 */
		public int checkKeyIndex(int sid, DocType docType, Vector indiceslist, int count, Vector result) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in checkKeyIndex::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	        	Vector ret=new Vector();
	        	ret=vws.checkKeyIndex(session.room, sid, docType, indiceslist, count);
	        	result.addAll(ret);
	        	VWClient.printToConsole("Active Error in checkKeyIndex--->"+vws.getActiveError());
	        	//return vws.getActiveError();
	        	 if (sid>0 && vws.getActiveError()<0 ){
	          	   return NoError;
	             } else {
	          	   return vws.getActiveError();
	             }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
		}
		/*
		 * Enhancement:-Document keyField Changes
		 * updateDocTypeKeyField to set the current key field.
		 * Implemented for the document key field change.
		 * Release :Build5-001
		 * Added By Madhavan
		 */
		public int updateDocTypeKeyField(int sid, int oldKeyIndexId, int newkeyIndexId, DocType docType) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in updateDocTypeKeyField::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {	        
	        	return vws.updateDocTypeKeyField(session.room, sid, oldKeyIndexId, newkeyIndexId, docType);
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
		}
		/**Method is used  to check the indices in both  Backup set and indices from database before restoring the document(s). 
		 * @param sid
		 * @param docType
		 * @param dtindexCount
		 * @param restoreIndiceCheck
		 * @param resultVector
	    @return returns error code on exception and 0 on success
		 **/
		public int checkRestoreIndices(int sid,DocType docType, int dtindexCount,
				Vector restoreIndiceCheck, Vector resultVector) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in checkRestoreIndices::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try
			{	     
				Vector ret = new Vector();


				ret=vws.restoreIndicesCheck(session.room, sid,docType,dtindexCount, restoreIndiceCheck);
				resultVector.addAll(ret);
			  	VWClient.printToConsole("Active Error in checkRestoreIndices--->"+vws.getActiveError());
				//return  vws.getActiveError();
			  	 if (sid>0 && vws.getActiveError()<0 ){
		        	   return NoError;
		           } else {
		        	   return vws.getActiveError();
		           }
			}
			catch(Exception e)
			{
				return Error;
			}
		}
		/**
		 * Enhancment:-TakeOWnership
		 * Method added to create the ownership lock when user clicks the 
		 * release ownership menu on right click of folder or document by passing 
		 * node id to the db
		 * @param sid
		 * @param node
		 * @param type
		 * @return
		 */
		public int createOwnership(int sid, int type, Vector nodes,Vector createownerretval)
	    {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in createOwnership::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server); 
			if (vws == null) return ServerNotFound;
			try
			{	
				Vector ret=new Vector();
				String nodeNames = "";
				String nodeIds = "";
				if(nodes != null && nodes.size() > 0){
					for(int i=0; i<nodes.size(); i++){
						Node node = (Node) nodes.get(i);
						nodeNames += node.getName() + ",";
						nodeIds += node.getId() + ",";		
						VWClient.printToConsole("node name :  "+nodeNames+" : node id :  " +nodeIds);
					}
					
					if(nodeNames.endsWith(",")){
						nodeNames = nodeNames.substring(0, nodeNames.length() - 1);
					}
					if(nodeIds.endsWith(",")){
						nodeIds = nodeIds.substring(0, nodeIds.length() - 1);
					}
				}
				String sessionName = "user <"+session.user+"> has taken ownership on <"+nodeNames+"> <Id-"+nodeIds+">"; 				
				ret=vws.createOwnership(session.room, sid, sessionName, type, nodes);	
				createownerretval.addAll(ret);
				VWClient.printToConsole("Active Error in createOwnership--->"+vws.getActiveError());
				//return  vws.getActiveError();
				 if (sid>0 && vws.getActiveError()<0 ){
		        	   return NoError;
		           } else {
		        	   return vws.getActiveError();
		           }
			}
			catch(Exception e)
			{
				return Error;
			}
	    }
		/**
		 * Enhancment:-TakeOWnership
		 * Method added to release the ownership lock when user clicks the 
		 * release ownership menu on right click of folder or document'
		 * by passing node id to the db
		 * @param sid
		 * @param node
		 * @param type
		 * @return
		 */
		public int releaseOwnership(int sid, Node node, int type)
	    {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in releaseOwnership::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try
			{	     
				
				
				int ret=vws.releaseOwnership(session.room, sid, node, type);
				
				return  ret;
			}
			catch(Exception e)
			{
				return Error;
			}
	    }
		/**
		 * Enhancment:-TakeOWnership
		 * When user right clicks the folder or document and if the return value 
		 * from this method is 
		 * (0,0)-->both TakeOWnerShip,Release ownership Menu's are  in  disable state
		 * (0,1)-->TakeOwnership-Disable,ReleaseOwnership-Enable
		 * (1,0)-->TakeOwnership-Enable,ReleaseOwnership-disable
		 * (1,1)-->TakeOwnership-Enable,ReleaseOwnership-Enable
		 * @param sid
		 * @param type
		 * @param nodes
		 * @param createReleaseOwnVector
		 * @return
		 */
		public int checkOwnership(int sid, int type, Vector nodes, Vector createReleaseOwnVector)
	    {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in checkOwnership::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try
			{	  
				Vector retVector = vws.checkOwnership(session.room, sid,  type, nodes);				
				createReleaseOwnVector.addAll(retVector);
				return NoError;
			}
			catch(Exception e)
			{
				return Error;
			}	
	    }
		/**
		 * Used to get the minimum document id to 
		 * establish connection with the dss location
		 * (Implemented for DSSLowDiskSpace Enhancement)
		 * @param sid
		 * @param result
		 * @return
		 */
		public  int getLatestDocId(int sid,Vector result)
		{			
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getLatestDocId::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try{

				Vector retVector = vws.getLatestDocId(session.room,sid);
				retVector.get(0);
				result.addAll(retVector);	
			}
			catch(Exception ex)
			{
				return Error;
			}
			return NoError;
			
		}
		/**
		 * This method returns the children nodes(includes all the docIds present in the node) for the passed nodeid through 
		 * the result vector and it is called by copyNode method to get the childrens.
		 * @param sid
		 * @param nodeid
		 * @param result
		 * @returns error code on exception and sid on success
		 */
		public int getChildNodes(int sid,int nodeid,Vector result){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getChildNodes::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try{
				Vector retVector = vws.getChildNodes(session.room,sid,nodeid);
				result.addAll(retVector);	
			}
			catch(Exception ex)
			{
				return Error;
			}
			return NoError;
			
		}
		/**
		 * Used to check the all.zip size from the DSS location
		 * (Implemented for DSSLowDiskSpace Enhancement)
		 * @param folder
		 * @return folder size in long
		 */
		 public static long getFileSize(File folder) {
		   	 int totalFolder = 0;
		   	 int totalFile=0;
		       totalFolder++;
		       long foldersize = 0;
		       File[] filelist = folder.listFiles();
		    
		       for (int i = 0; i < filelist.length; i++) {
		           if (filelist[i].isDirectory()) {
		        	   foldersize += getFileSize(filelist[i]);
		        	  
		           } else {
		               totalFile++;
		               String ext=filelist[i].toString();
		               if(ext.endsWith("all.zip")){
		               foldersize += filelist[i].length();
		               }
		           }
		       }
		      return foldersize;
		   }
		 /**
		  * Method implemented for DSS low disk space enhancement to send email notification if the available disk space is less than the registry space
		  * @param availableSpace specifies the space which is available on the specified ImageStore
		  * @param sid is the session id generated by the login method
		  * @param doc is the document object
		  * @returns error code on exception and sid on success
		  */
		 public int SendEmailFromDSS(long availableSpace,int sid,Document doc){
			 	ServerSchema dssSchema = new ServerSchema();
		    	Session session = getSession(sid);
		    	if(session==null) return invalidSessionId;
		    	VWS vws = getServer(session.server);
		    	if (vws == null) return ServerNotFound;
		    	try
		    	{
		    		ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
		    		dssSchema.set(ss);
		    	}
		    	catch(RemoteException re)
		    	{
		    		connectedVWSs.remove(session.server);
		    		return Error;
		    	}
			 DSS ds=null;
			 ds = (DSS) Util.getServer(dssSchema);
	      try {
	    	  ds.SendEmailNotification(availableSpace,sid,session.room);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sid;

			 
		 }
		 /**
		  * Method added for Force Fts enhancement(Calls when user clicks the force FTS button from DTC or WebTop ) from general tab.
		  * @param sid is the session id generated by the login method
		  * @param nodeId is the id of the selected document
		  *  @return 0 for success or negative value error code
		  */
		public int ForceFTS(int sid, int nodeId)
	    {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in ForceFTS::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try
			{	  
				return  vws.ForceFTS(session.room, sid, nodeId);
			}
			catch(Exception e)
			{
				return Error;
			}	
	    }
		/**
		 * This method is Not in use
		 * @param v
		 * @param ver
		 * @return
		 */
		private Vector vectorToDocumentsWithVersions(Vector v, String ver)
	    {
	        Vector docs = new Vector();
	        for (int i = 0; i < v.size(); i++)
	        {
	            Document doc = new Document((String) v.elementAt(i), ver);
	            docs.addElement(doc);
	        }
	        return docs;
	    }
		/**Method Used to get the document version for the currently selected document
		  *(Currently this method is not in use) 
	      * @param sid Session id obtained by login
	      * @param doc The Document for which to return document vesrion
	      * @param vers A document of perticular version
	      * @return 0 for success or negative value error code
	    **/
	    public int getDocumentVersions(int sid, Document doc, Vector vers)
	    {
	        Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in getDocumentVersions::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	           Vector ret = vws.getDocumentVersions(session.room, sid, doc.getId(),doc.getVersion());
	           vers.addAll(vectorToDocumentsWithVersions(ret, "DTC"));
	           VWClient.printToConsole("Active Error in getDocumentVersions--->"+vws.getActiveError());
	           return vws.getActiveError(); 
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    /**
	     * Enhancement :-Locked document type
	     * Used to launch the Lock DocumentType (Custom document type) Dialog       
	     * which lists all the document types.
	     * @param sid Session id obtained by login
	     * @param mode is the mode need to load dialog
	     * @param nodeId is the ID of the Node
	     * @return 0 for success or negative value error code
	     **/
	    
	    public int setDocumentTypeDlg(int sid, int mode, int nodeId){
	        try 
	        {
	        	if (type == 1 || type == 9)
	            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	        }
	        catch(Exception e){
	        	return Error;
	        }
	        VWDocTypeListDlg vwDocTypeListDlg = new VWDocTypeListDlg(this, sid, mode, nodeId);
	        new Thread(vwDocTypeListDlg).start();
	    	return NoError;
	    } 
	    /**
	     *  Enhancement :-Locked document type
	     * Method to check whether logged in user has permission to set custom document types on right click of a cabinet. 
	     *  permission is set it returns  1 else it returns 0 
	     * @param sid is the session id generated by the login method
	     * @param nodeid is the document id
	     * @@return 0 on success and -1 on failure 
	     */
	    public int CheckLockDocTypePerm(int sid,int nodeid){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in CheckLockDocTypePerm::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try{
	    		return vws.CheckLockDocTypePerm(session.room,sid,nodeid);
	    	}
	    	catch(Exception ex)
	    	{
	    		return Error;
	    	}

	    }
	    /**
	     *  Enhancement :-Locked document type
	     * this method is used to return the documenttype details 
	     * for listing the doctype in Lock DocumentType Dialog from the database
	     * @param sid
	     * @param indexId
	     * @param indices
	     * @return 1 if the folder is having permission to copy or else 0
	     */
	    public int getDocumentTypes(int sid, int indexId, Vector indices)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in getDocumentTypes::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret = vws.getDocumentTypes(session.room, sid, indexId, indices);
	    		indices.addAll(ret);
	    		VWClient.printToConsole("Active Error in getDocumentTypes--->"+vws.getActiveError());
	    		//return vws.getActiveError();
	    		 if (sid>0 && vws.getActiveError()<0 ){
	          	   return NoError;
	             } else {
	          	   return vws.getActiveError();
	             }
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    /**
	     *  Enhancement :-Locked document type
	     * Used to get  the list of document type ids that are set for the given node id. 
	     * Locked document type dialog through Vector
	     * @param sid
	     * @param nodeId
	     * @param indexInfo
	     * @return 0 on success and -1 on failure 
	     */
	    public int getDocumentIndexIdsFromRegistry(int sid,int nodeId, Vector indexInfo){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in getDocumentIndexIdsFromRegistry::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try{
	    		Vector result = new Vector();
	    		result = vws.getDocumentTypeList(sid, session.room,indexInfo, nodeId);
	    		indexInfo.addAll(result);

	    	}catch(Exception e){
	    		return Error;
	    	}
	    	return NoError;

	    }
	    /**
	     *  Enhancement :-Locked document type
	     * Used to pass the user selected document ids that are set in custom  document type dialog
	     * and save it in the datbase
	     * @param sid
	     * @param indexIds
	     * @param nodeId
	     * @return 1 if the folder is having permission to copy or else 0
	     */
	    public int setDocumentIndexIdsInRegistry(int sid, String indexIds, int nodeId,Vector docTypes){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setDocumentIndexIdsInRegistry::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try{
	    		addDocumentTypeList(sid,indexIds, nodeId,docTypes);
	    	}catch(Exception ex){
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**
	     * Enhancement :-Locked document type
	      * Method used to add the document types which the user want to lock
	      * this method is called by setDocumentIndexIdsInRegistry
	      * @param session id generated by login method
	      * @param settingsValue,document type id(s)
	      * @param nodeId,id of the cabinet on which user want to lock the document types
	      * @returns 1 on success,0 on failure
	      */
	    public int addDocumentTypeList(int sid, String settingsValue, int nodeId,Vector docTypes){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in addDocumentTypeList::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	String user = session.user;                
	    	try{ 
	    		Vector ret= new Vector();
	    		ret=vws.addDocumentTypeList(sid, session.room, user,settingsValue, nodeId);
	    		docTypes.addAll(ret);
	    	}catch(Exception ex){
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**
	     * Enhancement :-Locked document type
	     * clears or resets custom document types. 
	     * @param sid
	     * @param nodeId
	     * @return
	     */
	    public int resetDocumentTypeList(int sid, int nodeId){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in resetDocumentTypeList::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try{        	
	    		vws.resetDocumentTypeList(sid, session.room, nodeId);        	
	    	}catch(Exception ex){
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**
	     * Enhancement Change Document Type
	     * Returns the Document types to be displayed in the Combobox of change
	     * document type in properties tab
	     * @param sid
	     * @param nodeId
	     * @param documentTypes
	     * @returns 0 on success, -1 on exception
	     */
	    public int VWNodeGetDocTypes(int sid, int nodeId, Vector documentTypes)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in VWNodeGetDocTypes::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret = vws.VWNodeGetDocTypes(sid,session.room, nodeId, documentTypes);
	    		documentTypes.addAll(vectorToDocTypes(ret));
	    		VWClient.printToConsole("Active Error in VWNodeGetDocTypes--->"+vws.getActiveError());
	    		//return vws.getActiveError();
	    		 if (sid>0 && vws.getActiveError()<0 ){
	          	   return NoError;
	             } else {
	          	   return vws.getActiveError();
	             }
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    /**
	     * Enhancement :-Locked document type
	     * checks whether the node is having permission to copy the document type into destination node.
	     * If the returnvalue is not equal to 1 then DocumentType Not Permitted alert message will be displayed.
	     * @param sid
	     * @param flag
	     * @param sourceNodeId
	     * @param destinationNodeId
	     * @returns 2 means permission is not available,1 means can continue the copy process
	     */
	   
	    public int ValidateNodeDocType(int sid,int flag, int sourceNodeId,int destinationNodeId,Vector validate)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in ValidateNodeDocType::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		 
	    		Vector ret = vws.ValidateNodeDocType(sid,session.room, flag, sourceNodeId,destinationNodeId);
	    		validate.addAll(ret);
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**Enhancment:-Dynamic User 
	     ** Returns a list of available Creators (users) for dynamic users from the database through vector
	     * @param sid Session id obtained by login
	     * @param creators The list of creators returned
	     * @return 0 for success or negative value error code
	     */
	    public int getRouteUsers(int sid, Vector creators) 
	    {
	        Session session = getSession(sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = vws.getRouteUsers(session.room, sid, creators);
	            creators.addAll(ret);
	            VWClient.printToConsole("Active Error in getRouteUsers--->"+vws.getActiveError());
	           // return vws.getActiveError();
	            if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    /**
	     *  Used to check the dynamic user is assigned for the second task if not 
	     *  it stops the  returns the alert message from the procedure call
	     * @param sid
	     * @param docId
	     * @param routeId
	     * @param action
	     * @param routeTaskSequence
	     * @param result
	     *   @return 0 for success or negative value error code
	     */
	    public int drsValidateTaskDocument(int sid,int docId,int routeId,int action,String routeTaskSequence, Vector result) 
	    {
	        Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in drsValidateTaskDocument::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = vws.drsValidateTaskDocument(session.room, sid,docId,routeId,action,routeTaskSequence);
	            result.addAll(ret);
	            VWClient.printToConsole("Active Error in drsValidateTaskDocument--->"+vws.getActiveError());
	            //return vws.getActiveError();
	            if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    

	    /**
	     * Returns the task indices in route,added for dynamic user enhancement
	     * @param sid
	     * @param docId
	     * @param taskIndices
	     * @return 0 for success and negative value for any exception or error.
	     */
	    public int getTaskIndicesInRoute(int sid, int docId, Vector taskIndices){
			try
			{
				Session session = getSession(sid);
				VWClient.printToConsole("sessionId in getTaskIndicesInRoute::"+sid);
				VWS vws = getServer(session.server);
				if (vws == null) return Error;

				Vector ret = vws.getTaskIndicesInRoute(session.room, sid, docId);
				taskIndices.addAll(ret);
				VWClient.printToConsole("Active Error in getTaskIndicesInRoute--->"+vws.getActiveError());
				//return vws.getActiveError();
				 if (sid>0 && vws.getActiveError()<0 ){
		            	return NoError;
		            } else {
		            	return vws.getActiveError();
		            }
			}
			catch(Exception e)
			{
				return Error;
			}	   
	    }
	    /**
	     * Enhancment:-Dynamic User
	     * This method is used to the named user list in acending order from the
	     * database and set it in the properties table
	     * @param sid
	     * @param previousValue
	     * @param currentValue
	     * @param UserNameInOrder
	     * @return 0 for success and negative value for any exception or error.
	     */
	    public int getUserNameInOrder(int sid, String previousValue,String currentValue, Vector UserNameInOrder){
			try
			{
				Session session = getSession(sid);
				VWClient.printToConsole("sessionId in getUserNameInOrder::"+sid);
				VWS vws = getServer(session.server);
				if (vws == null) return Error;

				Vector ret = vws.getUserNameInOrder(session.room, sid, previousValue,currentValue);
				UserNameInOrder.addAll(ret);
				VWClient.printToConsole("Active Error in getUserNameInOrder--->"+vws.getActiveError());
				//return vws.getActiveError();
				 if (sid>0 && vws.getActiveError()<0 ){
		            	return NoError;
		            } else {
		            	return vws.getActiveError();
		            }
			}
			catch(Exception e)
			{
				return Error;
			}	   
	    }
	    
	    /**
	     * Method added for Unique to Room Enhancement
	     * Return 1 if the indexid is sequence type and used in only one document types having no documents created. "Unique to room" not set 
		 * (Unique to room should display in Unique property)
         * Return 2 if the indexid is sequence type and Unique property is set to "Unique to Room" 
         * (Unique to room should display in Unique property)
         * Return 3 if the indexid is sequence type and used in multiple document types and none of the document types have documents created 
         * (Unique to room should display in Unique property)
         * Return 4 if the indexid is sequence type and used in multiple document types and one or more document types have documents created 
         * (Unique to room should not display in Unique property)
	     **/
	    
	  
	    public int getValidateSeqIndex(int sid, int indexid) {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in getValidateSeqIndex::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret=new Vector();
	    		ret=	vws.checkValidateSeqIndex(session.room, sid, indexid);
	    		if (ret != null && ret.size() > 0){
	    			VWClient.printToConsole("The value is --->"+ret.get(0));
	    			return Integer.parseInt(ret.get(0).toString());	    			
	    		}

	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**
	     *  Method added for Sequence Unique to Room Enhancement
	     *  Used in Aip to avoid Validate duplicate sequence 
	     * @param sid
	     * @param doctypeId
	     * @param retVector
	     * @return
	     */
	    public int checkValidateUniqueSeq(int sid, int doctypeId,Vector retVector) {
	    	Session session = getSession(sid);
	      	VWClient.printToConsole("sessionId in checkValidateUniqueSeq::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret=new Vector();
	    		ret=vws.checkValidateUniqueSeq(session.room, sid, doctypeId);
	    		retVector.addAll(ret);
	    		return Integer.parseInt(ret.get(0).toString());	    		

	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    /**
	     *  Method added for Sequence Unique to Room Enhancement
	     *  Used to Validate the unique violation during copy paste in dtc
	     * @param sid
	     * @param node
	     * @param nodeid
	     * @return
	     */
	    public int checkValidateCopySeq(int sid,Node node, int nodeid) {
	    	Session session = getSession(sid);
	      	VWClient.printToConsole("sessionId in checkValidateCopySeq::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret=new Vector();
	    		ret=	vws.checkValidateCopySeq(session.room, sid,node,nodeid);
	    		if (ret != null && ret.size() > 0){
	    			return Integer.parseInt(ret.get(0).toString());	       			
	    		}

	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**
	     * Enhancment:-WorkFlow Resubmit
	     * This method is used to call the procedure  when user selects Re-submit menu option from DTC
	     * Used from dtc  
	     * @param sid
	     * @param doc
	     * @return
	     */
	    public int resubmitDoctoWorkFlow(int sid,String doc){
	    	Session session = getSession(sid);
	     	VWClient.printToConsole("sessionId in resubmitDoctoWorkFlow::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		vws.resubmitDoctoWorkFlow(session.room,sid,doc);
	    		VWClient.printToConsole("Active Error in resubmitDoctoWorkFlow--->"+vws.getActiveError());
	    		//return vws.getActiveError();
	    		 if (sid>0 && vws.getActiveError()<0 ){
	             	return NoError;
	             } else {
	             	return vws.getActiveError();
	             }
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }

	    /**
	     *  Enhancment:-WorkFlow Resubmit
	     *  This method is used to validate and returns a value 1 or 0. If 1 enable Re-submit,
	     *   0 disable Re-submit menu option
	     * @param sid
	     * @param doc
	     * @return
	     */
	    public int showhideDoctoWorkFlow(int sid,String doc){
	    	Session session = getSession(sid);
	       	VWClient.printToConsole("sessionId in showhideDoctoWorkFlow::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret=new Vector();
	    		ret=vws.showhideDoctoWorkFlow(session.room,sid,doc);
	    		if (ret != null && ret.size() > 0){
	    			return Integer.parseInt(ret.get(0).toString());	  
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**
		 * Ehancement:Sow2096_891-Add Default Index with users and groups
		 * Code added get the selection Enableslection registry value to C++.
		 */
	    public int  getEnableSelect(int sid,boolean isEnable){
	    	Session session = getSession(sid);
	     	VWClient.printToConsole("sessionId in getEnableSelect::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try{
	    		String registryValue="";
	    		vws.getEnableSelect(session.room,sid,registryValue);
	    		if(registryValue.equalsIgnoreCase("true")){
	    			isEnable=true;
	    		}else{
	    			isEnable=false;
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**
	     * Method add for new procedure call to get all the cabinets for right
	     * fax Enhancement
	     * @param sid
	     * @param nodesData
	     * @return
	     */
	    public int  getAllCabinets(int sid,Vector nodesData){
	    	try
	    	{
	    		Session session = getSession(sid);
	    		VWClient.printToConsole("sessionId in getAllCabinets::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return Error;
	    		Vector ret = vws.getAllCabinets(session.room, sid);
	    		nodesData.addAll(ret);
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}	   
	    	return NoError;
	    }
	    /**
	     * Method add for new procedure call to get all the DocumentType for right
	     * fax Enhancement
	     * @param sid
	     * @param faxDocType Vector
	     * @return
	     */
	    public int  getFaxDocType(int sid,Vector faxDocType){
	    	try
	    	{
	    		Session session = getSession(sid);
	      		VWClient.printToConsole("sessionId in getFaxDocType::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return Error;
	    		Vector ret = vws.getFaxDocType(session.room, sid);
	    		faxDocType.addAll(ret);
	    		VWClient.printToConsole("faxDocType"+faxDocType);
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}	   
	    	return NoError;
	    }
	    /*public int getTodoListResubmit(int sid,String doc,RouteMasterInfo routeMasterInfo,int action,String documentName){
	    //	VWClient.printToConsole("Inside first line of getTodoListResubmit");
	    //	try
	    //	{
	    //		Session session = getSession(sid);
	    //		VWClient.printToConsole("sessionId in getTodoListResubmit::"+sid);
	    //		if(session==null) return invalidSessionId;
	    //		VWS vws = getServer(session.server);
	    //		if (vws == null) return ServerNotFound;

	    //		VWClient.printToConsole("docId:::"+doc+"routeMasterInfo.getRouteHistoryId()::"+routeMasterInfo.getRouteHistoryId());
	    //		vws.getTodoListResubmit(session.room,sid,doc,routeMasterInfo.getRouteHistoryId());
	    //		VWClient.printToConsole("Active Error in setRouteStatusResubmit--->"+vws.getActiveError());
	    //		return vws.getActiveError();
	    //	}
	    //	catch(Exception e)
	    //	{
	    //		VWClient.printToConsole("Exception in getTodoListResubmit:::"+e.getMessage());
	    //		return Error;
	    //	}
	    }*/

	    public int setRouteStatus1(int sid, int docId, RouteMasterInfo routeMasterInfo, int action, String documentName)
	    {
	    	VWClient.printToConsole("sessionId in setRouteStatus1::"+sid);
	    	try
	    	{
	    		String doc = '1' + Util.SepChar + docId;
	    		Session session = getSession(sid);
	    		VWClient.printToConsole("sessionId in getTodoListResubmit::"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;

	    		VWClient.printToConsole("docId:::"+doc+"routeMasterInfo.getRouteHistoryId()::"+routeMasterInfo.getRouteHistoryId());
	    		vws.getTodoListResubmit(session.room,sid,doc,routeMasterInfo.getRouteHistoryId());
	    		VWClient.printToConsole("Active Error in setRouteStatusResubmit--->"+vws.getActiveError());
	    		//return vws.getActiveError();
	    		 if (sid>0 && vws.getActiveError()<0 ){
	          	   return NoError;
	             } else {
	          	   return vws.getActiveError();
	             }
	    	}
	    	catch(Exception e)
	    	{
	    		VWClient.printToConsole("Exception in getTodoListResubmit:::"+e.getMessage());
	    		return Error;
	    	}
	    }
	    /**
	     * Added to get the maintanance flag from procedure call
	     * and display warining message in DRS server.
	     * Review Update button in adminwise 
	     * Enhancement:-Update WorkFlow 
	     * Version :- CV83B3
	     * @param sid
	     * @return
	     */
	    
	    public int getDWSMaintenanceFlag(int sid){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		VWClient.printToConsole("sessionId in getDWSMaintenanceFlag::"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getDWSMaintenanceFlag(session.room,sid);
	    		if(ret.size()>0){
	    			returnVal=Integer.parseInt(ret.get(0).toString());
	    		}
	    		return returnVal;
	    	}catch(Exception e){
	    		return Error;
	    	}

	    }
	    /**
	     * Added to perform the RevieUpdate task from the procedure
	     * once the user clicks the review update button from adminwise
	     * Enhancement:-Update WorkFlow 
	     * Version :- CV83B3
	     * @param sid
	     * @return
	     */
	    
	    public int getDRSReviewUpdWorkflow(int sid){
	    	try{
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in getDRSReviewUpdWorkflow::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getDRSReviewUpdWorkflow(session.room,sid);
	    		return vws.getActiveError();
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    /**
	     * Added to get the flag from procedure call to enable or disable
	     * Review Update button in adminwise. 
	     * Enhancement:-Update WorkFlow 
	     * Version :- CV83B3
	     * @param sid
	     * @return
	     */
	    
	    public int getDRSGetReviewUpdFlag(int sid){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in getDRSGetReviewUpdFlag::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getDRSGetReviewUpdFlag(session.room,sid);
	    		if(ret.size()>0){
	    			returnVal=Integer.parseInt(ret.get(0).toString());
	    		}
	    		return returnVal;
	    	}catch(Exception e){
	    		return Error;
	    	}

	    }
	    //Added for WorkFlow update Enhancement after cv83b3
	    public int getDRSGetWorkflowStatus(int sid,Vector statusResult){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in getDRSGetWorkflowStatus::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getDRSGetWorkflowStatus(session.room,sid);
	    		if(ret.size()>0){
	    			statusResult.addAll(ret);	    			
	    		}
	    		return returnVal;
	    	}catch(Exception e){
	    		return Error;
	    	}

	    }
	    /**
	     * Method added to send Email From WorkFlow Update.
	     * Enhancement :- WorkFlow Update CV83B5 Date:-22/12/2015
	     * @param sid
	     * @param Result
	     * @return
	     */
	    public int workFlowUpdateSendEmail(int sid,Vector Result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.workFlowUpdateSendEmail(session.room,sid);
	    		if(ret.size()>0){
	    			Result.addAll(ret);	    			
	    		}
	    		return returnVal;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    /**
	     * Sending workFlow update mail with multiple users in to address 
	     * Method added to send Email From WorkFlow Update.
	     * Enhancement :- WorkFlow Update CV83B5 Date:-22/12/2015
	     * @param sid
	     * @param docId
	     * @param docName
	     * @param routeId
	     * @param routeName
	     * @param taskSequence
	     * @param refId
	     * @return
	     */
	   /* public int sendWorkFlowUpdateMail(int sid, int docId, String docName, int routeId, String routeName, int taskSequence,int refId){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in sendWorkFlowUpdateMail::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	Vector vTaskInfo = new Vector();
	    	try
	    	{
	    		Vector ret = vws.getRouteTaskInfo(session.room, sid, routeId, taskSequence, vTaskInfo);
	    		vTaskInfo.addAll(vectorToRouteTask(ret));
	    		if(vTaskInfo!=null&& vTaskInfo.size()>0)
	    		{
	    			RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(0);
	    			int routeTaskId = routeTaskInfo.getRouteTaskId();
	    			int TaskSeq = routeTaskInfo.getTaskSequence();
	    			Vector vRouteUsers = new Vector();
	    			ret = vws.getRouteUsers(session.room, sid, routeTaskId, vRouteUsers, docId);
	    			ArrayList listRouteUsers = vectorToRouteUsersArrayList(ret);
	    			String UserMailIds="";
	    			if(listRouteUsers!=null && listRouteUsers.size()>0){    		
	    				for(int k=0; k<listRouteUsers.size(); k++)
	    				{
	    					RouteUsers routeUser = (RouteUsers)listRouteUsers.get(k);
	    					int routeUserId =routeUser.getRouteUserId();
	    					String groupName = routeUser.getGroupName();
	    					String userName = "";
	    					try{
	    						Vector retMailIds=new Vector();
	    						retMailIds=vws.getMailId(session.room,sid,routeUser.getUserName());
	    						if(routeUser.getSendEmail() == 1){
	    							UserMailIds=UserMailIds+retMailIds.get(0).toString().trim()+";";
	    						}
	    					}catch(Exception e){

	    					}
	    				}
	    				try{
	    					Vector usermailVect=new Vector();
	    					usermailVect.add(UserMailIds);
	    					vws.sendMailWithAttachment(session.room, session.server, sid, "", docName, routeName, docId, usermailVect,null, Route_Status_Pending, null, null, "");
	    				}catch(Exception ex){
	    					VWClient.printToConsole("exception while sending mail from Workflow Update::"+ex.getMessage());
	    				}finally{
	    					vws.delUpdateWorkFlow(session.room, sid, refId);
	    				}
	    			}
	    		}
	    	}catch(Exception e){
	    		return Error;
	    	}
	    	return NoError;
	    }*/
	    /**
	     * Notify User when document becomes editable
	     * @param sid
	     * @param nodeId
	     * @param nodeName
	     * @return
	     */
	    public int setDocstoNotify(int sid,int docId,String UserName){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in setDocstoNotify::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.setDocstoNotify(session.room,sid,docId,UserName);
	    		return NoError;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    /**
	     * Enhacement:-Notify User when document becomes editable
	     * called by dtc
	     * @param sid
	     * @param doc
	     * @return
	     */
	    public int getUsertoNotifyDocs(int sid,Document doc){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in getUsertoNotifyDocs::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getUsertoNotifyDocs(session.room,sid,doc.getId());
	    		VWClient.printToConsole("ret value inside getUsertoNotifyDocs:::"+ret);
	    		if(ret.size()>0){
	    			//	String mailMessage=NotifyMessage1+" "+doc.getName()+" "+NotifyMessage2;
	    			int sentMail=vws.sendNotififyDocMail(session.room, sid, VWSConstants.VWS,"Document"+" "+doc.getName()+" "+"is available for editing",ret);
	    			if(sentMail>0)
	    				vws.delNotifiedDocs(session.room,sid,doc.getId());
	    		}
	    		return returnVal;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    
	 
	    public int getDRSValidateToDoList(int sid,String UserName,int nodeId,String receivedDate){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in getDRSValidateToDoList::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getDRSValidateToDoList(session.room,sid,UserName,nodeId,receivedDate);
	    		if(ret.size()>0){
	    			return	Integer.valueOf(ret.get(0).toString());
	    		}
	    		else{
	    			return NoError;
	    		}
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
		//Enhancement CV10:- Method added to get the color code from db for document list and hitlist in dtc
	    public int getGetDocsInRouteColor(int sid,int colorType,Vector result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in getGetDocsInRouteColor::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getGetDocsInRouteColor(session.room,sid,colorType);
	    		result.addAll(ret);
	    	}catch(Exception e){
	    		return Error;
	    	}
			return NoError;
	    }
	    public int getWorkFlowReportOptions( int sid, int type,String routeId,Vector result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	     		VWClient.printToConsole("sessionId in getWorkFlowReportOptions::"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getWorkFlowReportOptions(session.room,sid,type,routeId);
	    		if(ret.size()>0)
	    		result.addAll(ret);
	    		return NoError;
	    		
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    
	    
	    public int generateWFReport( int sid, int mode,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date,Vector result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in generateWFReport::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.generateWFReport(session.room, sid, mode, availableIds, tasks, workFlowIds, taskIds, userGroupNames, date);
	    		if(ret.size()>0)
	    		result.addAll(ret);
	    		return NoError;
	    		
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    /*CV2019- Merges from CV10.2-----------------------------------*/
	    public int saveWFReport( int sid, int mode,String reportName,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date,Vector result){
	    	return saveWFReport(sid, mode, reportName, availableIds, tasks, workFlowIds, taskIds, userGroupNames, date, "", result);
	    }
	    /*End of CV10.2 merges----------------------------------------*/
	    public int saveWFReport( int sid, int mode,String reportName,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date, String reportLocation, Vector result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in saveWFReport::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.saveWFReport(session.room, sid, mode,reportName,availableIds, tasks, workFlowIds, taskIds, userGroupNames, date);
	    		if(ret.size()>0)
	    		result.addAll(ret);
	    		return NoError;
	    		
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    
	    public int checkWFReport( int sid,String reportName,Vector result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in checkWFReport::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.checkWFReport(session.room,sid,reportName);
	    		if(ret.size()>0)
	    		result.addAll(ret);
	    		return NoError;
	    		
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    
	    public int getWFReport( int sid,int flag,String reportName,Vector result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		VWClient.printToConsole("sessionId in getWFReport::"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getWFReport(session.room,sid,flag,reportName);
	    		if(ret.size()>0)
	    		result.addAll(ret);
	    		return NoError;
	    		
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    public int deleteWFReport( int sid,String reportName){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		VWClient.printToConsole("sessionId in deleteWFReport::"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.deletWFReport(session.room,sid,reportName);
	    		return NoError;
	    		
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    public int getAgregateReport( int sid, int mode,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date,Vector result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		VWClient.printToConsole("sessionId in getAgregateReport::"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getAgregateReport(session.room, sid, mode, availableIds, tasks, workFlowIds, taskIds, userGroupNames, date);
	    		if(ret.size()>0)
	    		result.addAll(ret);
	    		return NoError;
	    		
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    /**
	     * CV10 Enhancement getAzureStorageCredentials for azure storage enhancement.
	     * @param sid
	     * @param docId
	     * @param storageCredentials
	     * @return
	     */
	    public int getAzureStorageCredentials(int sid,int docId,Vector storageCredentials){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		VWClient.printToConsole("sessionId in getAzureStorageCredentials::"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getAzureStorageCredentials(session.room, sid, docId);
	    		VWClient.printToConsole("After getting Storage details from DB::"+ret);
	    		VWClient.printToConsole("checking for client type after getting storage details::"+type);
				if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type) {
	    			VWClient.printToConsole("inside if...........");
	    			if(ret!=null&&ret.size()>0){
						String storageArray[] = ret.get(0).toString().split(Util.SepChar);
	    				storageCredentials.addAll(ret);
						if (storageArray.length > 0) {
							if (storageArray[7].equalsIgnoreCase("On Premises")) {
								VWClient.printToConsole("Inside On Premises condition before returning 1");
								return 1;
							} else if (storageArray[7].equalsIgnoreCase("Azure Storage")) {
								VWClient.printToConsole("Inside On Premises condition before returning 2");
								return 2;
							}
	
						}
	    			}
			} else if (ret != null && ret.size() > 0) {
				VWClient.printToConsole("inside elseif...........");
				storageCredentials.addAll(ret);
			}
	    		return NoError;

	    	}catch(Exception e){
	    		 VWClient.printToConsole("Exception in getAzureStorageCredentials :"+e.getMessage());
				 return Error;
			 }
		 }
	    
	    
	    public int validateDynamicIndex(int sid,String indexValues,Vector result){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.validateDynamicIndex(session.room,sid,indexValues);
	    		if((ret.size()>0&&ret!=null)){
	    			result.addAll(ret);
	    		}
	    		return NoError;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    //CV10 Issue Fix JNI call to get the zip page count validation 
	    public int getZipPageCount(int sid,String tempPath){
	    	try{
	    		int returnVal=0;
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		int pageCount=0;
	    		File tempFile=new File(tempPath);
	    		if(tempFile.exists()){
	    			ZipFile zfile = new ZipFile(tempFile);
	    			java.util.Enumeration e = zfile.entries();
	    			while (e.hasMoreElements())
	    			{
	    				ZipEntry zipEntry = (ZipEntry) e.nextElement();
	    				VWClient.printToConsole("Zip file contents are ::"+zipEntry.getName());
	    				if ( (zipEntry.getName().endsWith(".vw"))|| (zipEntry.getName().endsWith(".info"))  ) {
	    					pageCount++;
	    				}
	    			}
	    			zfile.close();
	    			if(pageCount>0){
	    				VWClient.printToConsole("Pagecount in getZipPageCount :"+pageCount);
	    				return pageCount;
	    			}
	    		}
	    		return NoError;
	    	}catch(Exception e){
	    		VWClient.printToConsole("Exception while getZipPageCount:::::: "+e.getMessage());
	    		return Error;
	    	}
	    }
	    
	    /**
	     * Added for VW Web Access dialog advanced find 
	     */
	    
	    public int populateSelectionIndexWS(int sid, int index, Vector selectValues)
	    {
	    	System.out.println("populateSelectionIndexWS::::"+index);
	        Session session = getSession(sid);
	        //System.out.println("index.getid::::"+index.getId());
	        System.out.println("sessionId in populateSelectionIndex::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = new Vector();
	            ret = vws.populateSelectionIndex(session.room, sid, index, ret);
	            System.out.println("ret Vector inside VWClient::::"+ret);
	           // index.setDef(stripIds(ret));
	            System.out.println("Active Error in populateSelectionIndex--->"+vws.getActiveError());
	            	if(ret.size()>0 || ret != null){
	            		selectValues.addAll(ret);
	            	}
	            //return vws.getActiveError();
	            if (sid>0 && vws.getActiveError()<0 ){
	         	   return NoError;
	            } else {
	         	   return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    /**
	     * Fetches the hot folder location path from registry
	     * @param sid
	     * @return
	     */
	    public int getHotFolderLocationFromRegistry(String server, Vector hotFolderPath){
	    	VWClient.printToConsole("Inside VWClient.getHotFolderLocationFromRegistry !!!!");
	    	String path = "";
	    	VWS vws = getServer(server);
	    	VWClient.printToConsole("vws VWClient.getHotFolderLocationFromRegistry ::::"+vws);
	    	try {
	    		path = vws.gethotFolderPath();
	    		VWClient.printToConsole("path VWClient.getHotFolderLocationFromRegistry ::::"+path);
	    		hotFolderPath.add(path);
	    		VWClient.printToConsole("hotFolderPath VWClient.getHotFolderLocationFromRegistry ::::"+hotFolderPath);
	    		if (hotFolderPath.size() >=0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}

	    	} catch (Exception ex) {
	    		return Error;
	    	}
	    }

	    /**
	     * CV10.1 Enhancement :- 2114 Document Templates
	     * @param sid
	     * @param selectValues
	     * @return
	     */
	    public int setCVDTForm(int sid,int docTypeId,String formName)
	    {
	    	try{
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		vws.setCVDTForm(session.room,sid,docTypeId,formName);
	    		return NoError;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    /**
	     * CV10.1 Enhancement :- 2114 Document Templates
	     * @param sid
	     * @param selectValues
	     * @return
	     */
	    public int getCVDTForm(int sid,int docTypeId,Vector result)
	    {
	    	try{
	    		VWClient.printToConsole("getCVDTForm:::::::::"+sid);
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector ret=vws.getCVDTForm(session.room,sid,docTypeId);
	    		VWClient.printToConsole("ret value is :::::"+ret);
	    		if((ret.size()>0&&ret!=null)){
	    			result.addAll(ret);
	    		}
	    		return NoError;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	   public int getCVDTemplateFile(int sid,int docTypeId,int docId,Vector result){
		   try{
			   VWClient.printToConsole("Inside getCVDTemplateFile :::::::::");
			   VWClient.printToConsole("sid :::::::::"+sid);
			   VWClient.printToConsole("docTypeId :::::::::"+docTypeId);
			   VWClient.printToConsole("docId :::::::::"+docId);
			   Session session = getSession(sid);
			   if(session==null) return invalidSessionId;
			   VWS vws = getServer(session.server);
			   if (vws == null) return ServerNotFound;
			   VWClient.printToConsole("Inside getCVDTemplateFile :::::");
			   ServerSchema dssSchema = new ServerSchema();
			   VWClient.printToConsole("After serverschema :::::");
			   Document doc=new Document(docId);
			   VWClient.printToConsole("After docid object :::::");
			   ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc,dssSchema);
			   VWClient.printToConsole("After server schema object :::::");
			   if (ss != null && !ss.path.equals(""))
			   {
				   dssSchema.set(ss);
			   }
			   else
			   {
				   if (!doc.getVersion().equals(""))
					   return versionNotFound;
				   else
					   return Error;
			   }
			   Vector ret=vws.getCVDTForm(session.room,sid,docTypeId);
			   VWClient.printToConsole("ret value is :::::"+ret);
			   if((ret.size()>0&&ret!=null)){
				   result.addAll(ret);
			   }
			   if (isDSSUsed())
			   {
				   VWClient.printToConsole("Inside is dssued");
				   DSS ds = (DSS) Util.getServer(dssSchema);
				   if (ds == null) {
					   return dssInactive;
				   }
				   VWClient.printToConsole("Inside second line is dssued");
				   boolean insideIdFolder=false;
				   String serverHome=vws.getServerHome(session.room,sid);
				   String sourcePath=serverHome+ Util.pathSep+"forms"+Util.pathSep+result.get(0).toString().trim();
				   VWClient.printToConsole("sourcePath ::::::"+sourcePath);
				   VWClient.printToConsole("result.get(0).toString().trim() ::::"+result.get(0).toString().trim());
				   String destPath=session.cacheFolder + Util.pathSep +result.get(0).toString().trim();
				   VWClient.printToConsole("destPath ::::::"+destPath);
				   VWDoc vwdoc = ds.getTemplateFile(doc, session.room, sourcePath,destPath);
				   if (vwdoc == null) return documentHasNoPage;
				   doc.setVWDoc(vwdoc);    
				   vwdoc.writeContents();  
				   while(vwdoc.getBytesWritten()<vwdoc.getSize()){
					   vwdoc = ds.getNextTemplateFile(dssSchema, doc);
					   doc.setVWDoc(vwdoc);              	
					   vwdoc.writeContents();
				   }
			   }
			   VWClient.printToConsole("lastline");


		   }catch(Exception e){
	    		VWClient.printToConsole("Inside exception :::::"+e.getMessage());
	    		return Error;
	    	}
	    	return NoError;
	    }
	 	  
	   /**
	     * CV 10.1: Enhancement. Added for checking pending documents on route to send notification
	     * @param sid
	     * @param retDocsList
	     * @return
	     */
	    public int checkTodoDocsToNotify(int sid){
	    	try
	    	{
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector retDocsList= new Vector();
	    		Vector ret = vws.checkTodoDocsToNotify(session.room, sid);
	    		if(ret!=null&&ret.size()>0){
	    		retDocsList.addAll(ret);
	    		}
	    		VWClient.printToConsole("retDocsList in VWClient.checkTodoDocsToNotify ::::"+retDocsList);
	    		if(retDocsList.size()>0 && retDocsList != null){
	    			for(int i=0; i<retDocsList.size(); i++){
	    				StringTokenizer st= new StringTokenizer(retDocsList.get(i).toString(), "\t");
	    				if(st.hasMoreTokens()){
	    					int routeMasterId= Integer.parseInt(st.nextToken());
	    					int docId= Integer.parseInt(st.nextToken());
	    					String docName= st.nextToken();
	    					int routeId= Integer.parseInt(st.nextToken());
	    					String routeName= st.nextToken();
	    					int taskSequence= Integer.parseInt(st.nextToken());
	    					int actionPermission= Integer.parseInt(st.nextToken());
	    					String emailId= st.nextToken();
	    					if(actionPermission == 1){
	    						vws.sendMailWithAttachment(session.room, session.server, sid, emailId, docName, routeName, 
	    								docId, new Vector(), null, Route_Status_Pending, null, null, "");
	    						VWClient.printToConsole("Pending Mail Sent Successfully !!!!");
	    						vws.checkUpdNotificationDocs(session.room, sid, routeMasterId, taskSequence);
	    					} else if(actionPermission == 0){
	    						vws.sendMailWithAttachment(session.room, session.server, sid, emailId, docName, routeName, 
	    								docId, new Vector(), null, Route_Status_Review, null, null, "");
	    						vws.checkUpdNotificationDocs(session.room, sid, routeMasterId, taskSequence);
	    						VWClient.printToConsole("Review Mail Sent Successfully !!!!");
	    					}
	    				}
	    			}

	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}	   
	    	return NoError;
	    }
	  
	   
	  public int  getIndexInfoTemplate(int sid,int retentionId,Vector result){
	    	try
	    	{
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	      		VWClient.printToConsole("sessionId in getIndexInfoTemplate::::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector retDocsList= new Vector();
	    		Vector ret = vws.getIndexInfoTemplate(session.room,sid,retentionId);
	    		if(ret!=null&&ret.size()>0){
	    			result.addAll(ret);
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}	   
	    	return NoError;
	  }
	  public int getRetentionDoctypes(int sid,int retentionId,Vector result){
	    	try
	    	{
	    		Session session = getSession(sid);
	    		if(session==null) return invalidSessionId;
	    		VWClient.printToConsole("sessionId in getRetentionDoctypes::::"+sid);
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		Vector retDocsList= new Vector();
	    		Vector ret = vws.getRetentionDoctypes(session.room,sid,retentionId);
	    		if(ret!=null&&ret.size()>0){
	    			result.addAll(ret);
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}	   
	    	return NoError;
	  }
	  
	  public int isRetentionDocTypeFound(int sid, String docType)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in isDocTypeFound::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            return vws.isRetentionDocTypeFound(session.room, sid, docType);
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }

	  public int isRetentionDTIndexFound(int sid, String doctypeId,String indexName)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("isRetentionDTIndexFound"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            return vws.isRetentionDTIndexFound(session.room, sid, doctypeId,indexName);
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	  /*CV2019 merges from SIDBI line------------------------------------------------***/
	  public int addCVWFReportPdf(int sessionId,File pdfFile,Document doc){
		  Vector templateNameListVector = new Vector();
		  String srcFile = "";
		  int addPagesRet=1;
		  srcFile = pdfFile.getAbsolutePath();
		  DRSLog.dbg("addCVWFReportPdf VWClient :::: "+srcFile);
		  String srcArray[] = srcFile.split(";");
		  Hashtable<Integer, byte[]> byteArray = new Hashtable<Integer, byte[]>();
		  DocInfo attachment[] = new DocInfo[srcArray.length];
		  for (int srcArrayKey = 0; srcArrayKey < srcArray.length; srcArrayKey++) {
			  DRSLog.dbg("srcArray["+srcArrayKey+"] :::: "+srcArray[srcArrayKey]);
			  FileInputStream stream =null;
			  try {
				  File f = new File(srcArray[srcArrayKey]);
				  stream = new FileInputStream(f);
				  byteArray.put(srcArrayKey, new byte[stream.available()]);
				  stream.read(byteArray.get(srcArrayKey));

			  } catch (IOException e) {
				 VWClient.printToConsole("Exception in addCVWFReportPdf::: "+e.getMessage());
			  }finally{
				  try {
					  stream.close();
				  } catch (IOException e) {
					  // TODO Auto-generated catch block
						 VWClient.printToConsole("Exception two in addCVWFReportPdf::: "+e.getMessage());
				  }
			  }

		  }
		  Set<Integer> keys = byteArray.keySet();
		  for (Integer key : keys) {
			  DocInfo attach = new DocInfo();
			  // document id
			  attach.setDocId(doc.getId());
			  //CV10.1 Issue fix to bring the html file extension. 
			  attach.setDocName(pdfFile.getName());
			  
			  // data as byte array
			  attach.setData(byteArray.get(key));
			  attach.setNewPage(false);

			  attachment[key] = attach;
		  }

		  // calling addMorePages to add multiple pages
		  if (doc.getId() > 0) {
			  addPagesRet = addMorePagesNew(sessionId, attachment,2,pdfFile.getName());
			  DRSLog.dbg("Return value from addMorePages: " + addPagesRet);
		  }
		  DRSLog.dbg("\n\n\n");
		  return addPagesRet;
	  }
	  /*------------------------End of SIDBI merges---------------------------------*/
	  
	  /**
	     * CV10.1 Enhancement : This method is created for calling CVCreateNodePath method of 
	     * Rampage Server for creating folder for saving custom HTML reports of Recycled, AT 
	     * and Retention panels
	     * @param sid
	     * @param panelName
	     * @return
	     */
	    public int cvCreateNodePath(int sid, String panelName, String path, String htmlReportName, String folderPath, String documentNameinDTC){
	    	Document retDoc =null;
	    	try
	    	{
	    		Session session = getSession(sid);
	    		//VWClient.printToConsole("isRetentionDTIndexFound"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		int docTypeId= 0;
	    		Vector docTypeInfo = new Vector();
	    		InputStream inStream = null;
	    		OutputStream outStream = null;
	    		Document doc = new Document();
	    		VWClient.printToConsole("\n\n\n");
	    		VWClient.printToConsole("Inside cvCreateNodePath !!!!");
	    		String storageType="";
	    		Vector storageVect=new Vector();
	    		getAzureStorageCredentials(sid,doc.getId(),storageVect);
	    		if(storageVect!=null&&storageVect.size()>0){
	    			String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
	    			storageType=storageArray[7];
	    		}
	    		VWClient.printToConsole("Storage type in cvCreateNodePath ::::"+storageType);
	    		if(storageType.equals("On Premises")){
	    			Vector result=new Vector();
	    			DSS ds = null;
	    			getLatestDocId(sid,result);
	    			int maxDocId=Integer.parseInt(result.get(0).toString());
	    			VWClient.printToConsole("maxDocId in cvCreateNodePath ::::"+maxDocId);
	    			Document maxdoc=new Document(maxDocId);
	    			Vector size=new Vector();
	    			int retval=checkAvailableSpaceInDSS(sid,maxdoc,"",size);
	    			long availableSpace=(Long) size.elementAt(0);
	    			VWClient.printToConsole("availableSpace in cvCreateNodePath ::::"+availableSpace);
	    			if(retval==dssCriticalError){
	    				SendEmailFromDSS(availableSpace,sid,maxdoc);
	    				return dssCriticalError;  
	    			}
	    		}
	    		Vector retnodePath= new Vector();
	    		Vector ret = vws.cvCreateNodePath(session.room, sid, panelName, retnodePath);
	    		VWClient.printToConsole("ret in VWClient.cvCreateNodePath ::::"+ret);
	    		DocType dt= new DocType("CVReports");
	    		if(ret!=null&&ret.size()>0){
	    			dt.setName("CVReports");
	    			/**
	    			 * isDocTypeFound method is calling for checking whether "CVReports" doctype exists
	    			 * or not
	    			 */
	    			docTypeId=isDocTypeFound(sid,dt);
	    			VWClient.printToConsole("docTypeId in VWClient.cvCreateNodePath ::::"+docTypeId);
	    		}
	    		if(docTypeId > 0){
	    			getDocTypeInfo(sid, docTypeId, docTypeInfo);
	    			VWClient.printToConsole("docTypeInfo in VWClient.cvCreateNodePath ::::"+docTypeInfo);
	    		}else {
	    			//-171 error code is used for not finding CVReports Document type.
	    			return -171;
	    		}
	    		//[17	CVReports	ReportName	1	0	 	 	 ]
	    		if(docTypeInfo.size()>0 && docTypeInfo!=null){
	    			StringTokenizer st= new StringTokenizer(docTypeInfo.get(0).toString(), "\t");
	    			int newDocypeId= Integer.parseInt(st.nextToken());
	    			String docTypeName= st.nextToken();
	    			String indexName= st.nextToken();
	    			Index idxName= new Index(indexName);
	    			idxName.setName(indexName);
	    			int indexId=getIndexId(sid, idxName);
	    			VWClient.printToConsole("indexId in VWClient.cvCreateNodePath ::::"+indexId);
	    			
	    			if(indexId > 0 && indexName.equals("ReportName")) {
	    				
	    			StringTokenizer st1= new StringTokenizer(ret.get(0).toString(), "\t");
	    			int parentId= Integer.parseInt(st1.nextToken());
	    			 retDoc = vws.createCVReportsDocument(session.room, sid, newDocypeId, parentId, 1, indexName, doc, dt, panelName, indexId, documentNameinDTC);
	    			
	    			doc.set(retDoc);
	    			doc.setDocType(dt);
	    			doc.setParentId(parentId);
	    			VWClient.printToConsole("doc.getId() After createCVReportsDocument    ::::"+doc.getId());
	    			//DocInfo docInfo = new DocInfo();
	    			// document id
//	    			docInfo.setDocId(doc.getId());
//	    			// file name
//	    			docInfo.setDocName(documentNameinDTC);
//	    			File f = new File(path);
//	    			FileInputStream stream = new FileInputStream(f);
//	    			System.out.println("stream::::"+stream);
//	    			byte[] b = new byte[stream.available()];
//	    			stream.read(b);
//	    			docInfo.setNewPage(true);
//	    			docInfo.setData(b);
	    			
	    			
	    			Vector templateNameListVector = new Vector();
	    			String srcFile = "";
	    			
					File folder = new File(folderPath.trim());
					VWClient.printToConsole("Folder Inside VWClient ::::" + folder);
					
					File[] listOfFiles = folder.listFiles();
					VWClient.printToConsole("listOfFiles ::::" + listOfFiles);

					String ext = null; // FilenameUtils.getExtension("/path/to/file/foo.txt");

					for (int i = 0; i < listOfFiles.length; i++) {
						VWClient.printToConsole("File ::::" + listOfFiles[i].getPath());
						if (listOfFiles[i].isFile()) {
							VWClient.printToConsole("File ::::" + listOfFiles[i].getName());
							/**CV10.2 - CSV file format has added for administration workflow report****/
							if (listOfFiles[i].getName().toLowerCase().endsWith(".html")
									|| listOfFiles[i].getName().toLowerCase().endsWith(".csv")
									|| listOfFiles[i].getName().toLowerCase().endsWith(".xls")
									|| listOfFiles[i].getName().toLowerCase().endsWith(".pdf")) {
								templateNameListVector.add(listOfFiles[i].getName()+";");
							}
						}
					}
					for(int j=0; j<templateNameListVector.size(); j++){
						srcFile = srcFile+folderPath+templateNameListVector.get(j).toString();
					}
					VWClient.printToConsole("srcFile Inside VWClient :::: "+srcFile);
					
					
	    			String srcArray[] = srcFile.split(";");
	    			Hashtable<Integer, byte[]> byteArray = new Hashtable<Integer, byte[]>();
	    			DocInfo attachment[] = new DocInfo[srcArray.length];
	    			for (int srcArrayKey = 0; srcArrayKey < srcArray.length; srcArrayKey++) {
	    				VWClient.printToConsole("srcArray["+srcArrayKey+"] :::: "+srcArray[srcArrayKey]);
	    				File f = new File(srcArray[srcArrayKey]);
	    				FileInputStream stream = new FileInputStream(f);
	    				byteArray.put(srcArrayKey, new byte[stream.available()]);
	    				stream.read(byteArray.get(srcArrayKey));
	    				stream.close();
	    			}
	    			/********CV10.2 Workflow report enhancement changes***/
	    			VWClient.printToConsole("documentNameinDTC :"+documentNameinDTC);
	    			VWClient.printToConsole("htmlReportName :"+htmlReportName);
	    			String outputFormat = ".html";
	    			if (htmlReportName.toLowerCase().endsWith("csv"))
	    				outputFormat = ".csv";
	    			else if (htmlReportName.toLowerCase().endsWith("xls"))
	    				outputFormat = ".xls";
	    			else if (htmlReportName.toLowerCase().endsWith("pdf"))
	    				outputFormat = ".pdf";
					/***End of workfolow report enhancement changes******/
	    			Set<Integer> keys = byteArray.keySet();
	    			for (Integer key : keys) {
	    				DocInfo attach = new DocInfo();
	    				// document id
	    				attach.setDocId(doc.getId());
	    				//CV10.1 Issue fix to bring the html file extension. 
	    				attach.setDocName(documentNameinDTC+"_"+(key+1)+outputFormat);

	    				// data as byte array
	    				attach.setData(byteArray.get(key));
	    				attach.setNewPage(true);

	    				attachment[key] = attach;
	    			}
	    		//}
	    			
	    			// calling addMorePages to add multiple pages
	    			if (doc.getId() > 0) {
	    				int retVal = addMorePages(sid, attachment);
	    				VWClient.printToConsole("Return value from addMorePages: " + retVal);
	    			}
	    			VWClient.printToConsole("\n\n\n");
	    			
	    			//addPages(sid, docInfo);
	    			
	    		} else {
	    			return -172;
	    		}
	    	}
	    	}
	    	catch(Exception e)
	    	{
	    		VWClient.printToConsole("Exception occured at cvCreateNodePath  ::::"+e.getMessage());
	    		return Error;
	    	}	
	    	/********CV10.2 Workflow report enhancement changes***/
	    	if(panelName.equals("Report Panel")){
	    		if(retDoc!=null) {
	    			return	retDoc.getId();
	    		}
	    	}
			return NoError;
	    }
	    
	    public int addMorePages(int sid, DocInfo attachment[]){
			VWClient.printToConsole("Executing addMore Pages ...");
			saveUploadedDocs(sid, attachment);
			uploadDocsToVW(sid, attachment[0]);   	
			return 0;
		}
	    
	    /**
	     * CV2019 merges from SIDBI line
	     * @param sid
	     * @param attachment
	     * @param flag
	     * @param pdfFilName
	     * @return
	     */
	    //,boolean flag,int flag
	    public int addMorePagesNew(int sid, DocInfo attachment[],int flag,String pdfFilName){
			VWClient.printToConsole("Executing addMore Pages ...");
			saveUploadedDocs(sid, attachment);
			uploadDocsToVWNew(sid, attachment[0],flag,pdfFilName);   	
			return 0;
		}
	    public boolean saveUploadedDocs(int sid, DocInfo[] attach){

	    	DocInfo attachment = new DocInfo();
			attachment = attach[0];
			int docId = attachment.getDocId();  
			VWClient.printToConsole("docId in saveUploadedDocs :::: "+docId);

			for (int i = 0 ; i < attach.length; i++){
				String docName = attach[i].getDocName();
				VWClient.printToConsole("docName in saveUploadedDocs :::: "+docName);
				File srcFile = new File("/attachments/" + sid + "/temp/" + docId + "/" + docName);
				VWClient.printToConsole("srcFile in saveUploadedDocs :::: "+srcFile);
				Utils.writeToFile(srcFile.getAbsolutePath(), docName, attach[i].getData());
			}
			VWClient.printToConsole("Writing upload file to the attachment folder !!!!");
			return true;
		}
	    
	    /**
	     * CV2019 merges from SIDBI line
	     * @param sid
	     * @param attachment
	     * @param flag
	     * @param pdfFileName
	     * @return
	     */
	    public boolean uploadDocsToVWNew(int sid, DocInfo attachment,int flag,String pdfFileName){
			int result = 0;
			File curPdFFile=null;
			int docId = attachment.getDocId();
			String docName =  attachment.getDocName();
			boolean isNew = attachment.isNewPage();
			File dstFile = new File("/attachments/" + sid +"/"+ docId +"/");
			VWClient.printToConsole("dstFile in uploadDocToVW :::"+dstFile);
			File srcFile = new File("/attachments/" + sid + "/temp/" + docId + "/" + docName);
			VWClient.printToConsole("srcFile in uploadDocToVW :::"+srcFile);

			
			if (!dstFile.exists()) dstFile.mkdirs();  
			try{
				Document doc = new Document(docId);
				if (!isNew){
					//commented for adding pdf file for sidbi
					//result = publishDirect(sid, doc, srcFile.getParentFile().getAbsolutePath(), 0, 0);			
					getDocFile(sid,doc,srcFile.getParentFile().getAbsolutePath(),false);
				}
				File[] allFiles = srcFile.getParentFile().listFiles();
				List totalFilesList = Arrays.asList(allFiles); 
				if(pdfFileName.length()>0){
				 curPdFFile=new File(srcFile.getParentFile().getAbsolutePath()+"\\"+pdfFileName);
				}
				String fileList = "";
				File allFile=new File(srcFile.getParentFile().getAbsolutePath()+"\\"+"all.zip");
				if(allFile.exists()){
					DRSLog.dbg("allFile as  ::"+allFile.exists());
				}
				DRSLog.dbg("curPdFFile.getAbsolutePath() :::: " + curPdFFile.getAbsolutePath());
				result = wrapFile(sid,srcFile.getParentFile().getAbsolutePath()+"\\"+"all.zip", curPdFFile.getAbsolutePath());
				VWClient.printToConsole("Wrap functionality return ::::" + result);
				VWClient.printToConsole("Updated all.zip created in  ::::" + dstFile.getAbsolutePath());
				VWDoc docObj = new VWDoc();
				docObj.setDstFile(dstFile.getAbsolutePath());
				doc.setVWDoc(docObj);			
				//VWClient.printToConsole("setDocFile " + dstFile.getAbsolutePath());
				result = setDocFile(sid, doc,srcFile.getParentFile().getAbsolutePath(), false, false);
				VWClient.printToConsole("After setdoc file flag value is  "+flag);
				if(flag==2){
					VWClient.printToConsole("before calling generateallweb"+srcFile.getParentFile().getAbsolutePath());
					File pdfFolderFiles = new File("/attachments/" + sid +"/"+ docId +"/"+"generatedpdfs");
					if(!pdfFolderFiles.exists()){
						pdfFolderFiles.mkdirs();
					}
					VWClient.printToConsole("pdfFolderFiles.getAbsolutePath() ::"+pdfFolderFiles.getAbsolutePath());
					ConvertDocToPDFDRS(sid,   pdfFolderFiles.getAbsolutePath(),dstFile.getAbsolutePath()+"\\"+"all.zip");
					VWClient.printToConsole("pdfFolderFiles.getAbsolutePath() ::"+pdfFolderFiles.listFiles());
					File tempFile=new File(pdfFolderFiles.getAbsolutePath()+"\\temp");
					if(tempFile.exists()){
						generateAllWeb( sid,docId,pdfFolderFiles.getAbsolutePath()+"\\temp");
						Utils.deleteDirectory(tempFile.getParent());
					}
					VWClient.printToConsole("after calling generateallweb");
				}
				VWClient.printToConsole("setDocFile is return :::: " + result);

				String docIdStr= String.valueOf(docId);
				DocInfo[] di= getDocumentAttachment3(sid, docIdStr, "", false, "empty");
				//System.out.println("ret : "+ret);
				//VWClient.printToConsole("uploading the pages " + result);
				if(dstFile.exists()){
					Utils.deleteDirectory(dstFile.getAbsolutePath());
				}
				
			}catch(Exception ex){
				//System.out.println("ex is : "+ex.getMessage());
			}    	
			return true;
		}
	    
	    
	    public boolean uploadDocsToVW(int sid, DocInfo attachment){
			int result = 0;

			int docId = attachment.getDocId();
			String docName =  attachment.getDocName();
			boolean isNew = attachment.isNewPage();
			File dstFile = new File("/attachments/" + sid +"/"+ docId +"/");
			File srcFile = new File("/attachments/" + sid + "/temp/" + docId + "/" + docName);

			//Utils.writeToFile(srcFile.getAbsolutePath(), docName, attachment.getData());
			if (!dstFile.exists()) dstFile.mkdirs();  
			try{
				Document doc = new Document(docId);
				if (!isNew){
					result = publishDirect(sid, doc, srcFile.getParentFile().getAbsolutePath(), 0, 0);			
					VWClient.printToConsole("publishDirect :::: " + result);
					if (result == 0 ){
						//deleting  all.zip from the cache folder
//						Utils.deleteDirectory(client.getSession(sid).cacheFolder + File.separator + docId);
					}
					VWClient.printToConsole("Uploaded file save into ::::" + srcFile.getAbsolutePath());
					//VWClient.printToConsole("srcFile.getPath() " + srcFile.getParentFile().getAbsolutePath());
				}

				File[] allFiles = srcFile.getParentFile().listFiles();
				String fileList = "";
				if (allFiles.length > 1){
					for (int i = 0; i < allFiles.length; i++){
						fileList += allFiles[i].getAbsolutePath() + ";";
					}
				}else
					fileList =  allFiles[0].getAbsolutePath();
				VWClient.printToConsole("Total files need to update into ViewWise :::: " + fileList);

				result = wrapFile(sid, dstFile.getAbsolutePath(), fileList);
				VWClient.printToConsole("Wrap functionality return ::::" + result);
				VWClient.printToConsole("Updated all.zip created in  ::::" + dstFile.getAbsolutePath());
				VWDoc docObj = new VWDoc();
				docObj.setDstFile(dstFile.getAbsolutePath());
				doc.setVWDoc(docObj);			
				//VWClient.printToConsole("setDocFile " + dstFile.getAbsolutePath());
				result = setDocFile(sid, doc, dstFile.getAbsolutePath(), false, false);
				VWClient.printToConsole("setDocFile is return :::: " + result);
//				getDocument(sid, docId);
				String docIdStr= String.valueOf(docId);
				DocInfo[] di= getDocumentAttachment3(sid, docIdStr, "", false, "empty");
				//System.out.println("ret : "+ret);
//				if (result == 0) 
//				{
//					removeUploadedDocFolders(String.valueOf(sid));
//					VWClient.printToConsole("Removed the temp folder");
//				}
				//VWClient.printToConsole("uploading the pages " + result);
				
			}catch(Exception ex){
				//System.out.println("ex is : "+ex.getMessage());
			}    	
			return true;
		}
	    
	    public int wrapFile(int sid, String dstPath, String srcPath){
			/*loadLibrary("VWView.dll");
			loadLibrary("VWDialog.dll");
			loadLibrary("ziplib.dll");*/

			VWClient wsClient = new VWClient(dllLocation);
			VWClient.printToConsole("******* WrapFile - wsClient.dllLocation :::: "+wsClient.dllLocation);
			int ret = wrap(sid, dstPath, srcPath);
			VWClient.printToConsole("Return value from Wrap  :::: " + ret);
			return ret;
		}
	    public DocInfo[] getDocumentAttachment3(int sid, java.lang.String docId, String docVersion, boolean forceUpdate, String path){
	    	VWClient.printToConsole("Executing the getDocumentAttachment Method : sid:  " + sid + " - doc Id : " + docId + " path  :::: " + path);
			/**
			 * Added for Tomcat8.0 issue which was creating pdf files in webapps/VWWAAttachment folder,
			 * Added to change location to webaaps/VWWA/Attachment,Gurumurthy.T.S
			 */
			if(path.contains("Attachment")){
				path = path.replace("Attachment", "VWWA\\Attachment");
			}
			VWClient.printToConsole("Attachment path in getDocumentAttachment3 ::::  "+ path);
			if (sid <= 0 ) {
				VWClient.printToConsole("Invalid Session Id :::: " + sid + " - doc Id :::: " + docId);
				return null;
			}
//			try {
//				((VWClientInfo) sessionTable.get(sid)).setLastActivateDate(System.currentTimeMillis());
//			} catch (Exception ex) {
//
//			}
			if (docId == null || docId.trim().length() == 0) return null;
			DocInfo[] attachment = null;
			FileOutputStream fos =null;
			try {
				int documentId = Integer.parseInt(docId);
				com.computhink.common.Document doc = new Document(documentId);
				doc.setVersion(docVersion);
				String allDotZipLoc = "c:\\temp";
				//CV10 Issue fix document not opening in azure storage.
				String azureAllDocZipLoc = "c:\\temp";

				File file = null;
				Vector storageVect=new Vector();
				String storageType="";
				getAzureStorageCredentials(sid, documentId, storageVect);
				//CV10 Issue fix document not opening in azure storage.
				if(storageVect!=null&&storageVect.size()>0){
					VWClient.printToConsole("storageVect inside getdocumentAttchment:::: "+storageVect.get(0).toString());
					String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
					VWClient.printToConsole("Stroage type from storage array in getdocumentAttchment ::::  "+storageArray[7]);
					storageType=storageArray[7];
				}
				
				if(docVersion != null && docVersion.trim().length() > 0){
					if(storageType.equals("On Premises")){
					VWClient.printToConsole("Inside onpremises first file location::::"+file.getAbsolutePath());
					file = new File(allDotZipLoc + "\\" + documentId + "\\" + docVersion + "\\all.zip");
					VWClient.printToConsole("Inside onpremises second file location::::"+file.getAbsolutePath());
					}
				}else{
					
					if(storageType.equals("On Premises")){
						VWClient.printToConsole("Inside onpremesis of webaccess !!!! ");
						azureAllDocZipLoc = azureAllDocZipLoc + "\\" + documentId;
						VWClient.printToConsole("allDotZipLoc :::: "+azureAllDocZipLoc);
					}
					
					allDotZipLoc = allDotZipLoc + "\\" + documentId;
					file = new File(allDotZipLoc + "\\all.zip");
				}

				VWClient.printToConsole("getDocumentAttachment Method :::: all.zip location ::::  " + sid + " - " +file.getAbsolutePath() );

				boolean needToDownload = false;
				boolean writeStampFile = false;
				String svrDateTime = "";
				File pdfFile = null;
				if (path == null||path.equalsIgnoreCase("empty"))
					pdfFile = new File("/attachments/" + documentId + "/" + documentId + ".pdf");
				else if(path.equals("")){
					path = System.getProperty("catalina.base")+"\\webapps\\VWWA\\Attachment";
					pdfFile = new File(path + File.separator + documentId + File.separator + documentId + ".pdf");
					VWClient.printToConsole("path inside blank is::::::::::"+pdfFile);
				}
				else
					pdfFile = new File(path + File.separator + documentId + File.separator + documentId + ".pdf");
				VWClient.printToConsole("getDocumentAttachment Method : pdf file location :  " + sid + " - " +pdfFile.getAbsolutePath() );
				if (!file.getParentFile().exists())
					file.getParentFile().mkdirs();
				if (!pdfFile.getParentFile().exists())
					pdfFile.getParentFile().mkdirs();

				File timeStamp = new File(pdfFile.getParentFile().getAbsolutePath() + File.separator + "VWTimeStamp.txt");
				 fos = new FileOutputStream(timeStamp, true);

				getLastModifyDate(sid, doc);
				svrDateTime = String.valueOf(doc.getVWDoc().getModifyDate());		    
				if (pdfFile.exists() && timeStamp.exists() && pdfFile.getParentFile().list().length > 1 && !forceUpdate)
				{	
					if (timeStamp.exists()) {
						String tempTimeStamp = Utils.extractContents(timeStamp.getAbsolutePath());
						if (!tempTimeStamp.equalsIgnoreCase(svrDateTime)){
							VWClient.printToConsole("getDocumentAttachment Method : all.zip is having latest version in DSS " + sid);
							fos.close();
							timeStamp.delete();
							writeStampFile = true;
							needToDownload = true;
						}
						else{
							writeStampFile = false;
							needToDownload = false;			
						}			
					} else {
						writeStampFile = true;
						needToDownload = true;
						fos.close();
					}

				}else{
					needToDownload = true;
				}

				if (needToDownload){
					//System.out.println("Last Modified Date " + doc.getVWDoc().getModifyDate());
					VWClient.printToConsole("getDocumentAttachment Method : downloading all.zip method :  " + sid );
					doc = new Document(documentId);
					doc.setVersion(docVersion);
					VWClient.printToConsole("getDocumentAttachment Method : allDotZipLoc:  " + allDotZipLoc );
					VWClient.printToConsole("getDocumentAttachment Method : doc.getid():  " + doc.getId());
					getDocFile(sid, doc, azureAllDocZipLoc );
					writeStampFile = true;
				}
				if (writeStampFile){
					if (timeStamp.exists()){
						try{
							if (fos != null)
								fos.close();
						}catch(Exception ex){fos.close();}
						timeStamp.delete();	
					}
					/**
					 * Added this new fos.close to handle memory leak in webaccess CV83B5
					 * Date  17-12-2015
					 */
					
					fos.close();
					fos = new FileOutputStream(timeStamp, true);
					fos.write(String.valueOf(svrDateTime).getBytes());
			
					
				}	    

				/*	    System.out.println("file abs Path " + file.getAbsolutePath());
		    System.out.println("PDF file abs Path " + pdfFile.getAbsolutePath());

				 */	    
				Vector result = new Vector();
				try{
					if (needToDownload || pdfFile.getParentFile().list().length <= 1){
						VWClient.printToConsole("getDocumentAttachment Method : calling convert to PDF " + sid);
						VWClient.printToConsole("Calling Convert2PDF : " + pdfFile.getAbsolutePath() + " : : " + file.getAbsolutePath() + " : " + dllLocation);

						VWClient wsClient = new VWClient(dllLocation);
						VWClient.printToConsole("------- getDocumentAttachment - Load : "+wsClient);

						//int error = client.convertToPDF(file.getAbsolutePath(), pdfFile.getAbsolutePath(), dllLocation, pwd, overLay, whiteOut, color, depth,  result);
						String redActionPwd = "vw";
						if(redActionPwd != null && redActionPwd.contains(";")){
							redActionPwd = redActionPwd.replace(";", "\t");
						}
						//int error = wsClient.convertToPDF(file.getAbsolutePath(), pdfFile.getAbsolutePath(), dllLocation, redActionPwd, overLay, whiteOut, color, depth, orientation,  result);
//						VWClient.printToConsole("getDocumentAttachment Method : return value for convert to PDF " + error);
						if (result != null && result.size() > 0)
							VWClient.printToConsole("getDocumentAttachment Method : convert to PDF returns as:  " + sid + " : " + result.get(0));
					}
				}catch(Exception ex){
					VWClient.printToConsole("Error in getDocumentAttachment3 :::: " +ex.getMessage());
				}
				int attachmentSize = pdfFile.getParentFile().list().length - 1 ;
				attachment = new DocInfo[attachmentSize];
				String[] fileList = pdfFile.getParentFile().list();
				int cnt =0;
				for (int i = 0; i < fileList.length; i++) {
					DocInfo attach = new DocInfo();		
					File selectedFile = new File(pdfFile.getParentFile().getAbsolutePath() + "\\" + fileList[i]);
					if (selectedFile.getName().equalsIgnoreCase("VWTimeStamp.txt")) continue;			
					DataHandler handler = new DataHandler(new FileDataSource(selectedFile.getAbsolutePath()));
					byte[] data = Utils.getBytesFromDataHandler(handler);
					attach.setDocName(selectedFile.getName());
					attach.setDocId(documentId);
					attach.setData(data);
					attachment[cnt++] = attach;

				}

			} catch (Exception ex) {
				VWClient.printToConsole("Error while executing the getDocumentAttachment Method : Doc id :::: " + docId +" - sid ::::  " + sid +"EXCEPTION :::: "+ ex.getMessage());	    
				return null;
			}//Added to handle memory leak CV83B5 17-12-2015
			finally{
				try {
					fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			VWClient.printToConsole("getDocumentAttachment Method : completing the PDF conversion and attachment " + sid);
			return attachment;
}
	    
	    /**
	     * CV10.1 Enhancement: Created for saving form template details
	     * @author apurba.m
	     * @param sid
	     * @param templateName
	     * @param formName
	     * @param sourceLocation
	     * @param destinationLocation
	     * @param result
	     * @return
	     */
	    public int CVSetDTFormTemplate(int sid,int templateId,String templateName, String formName, String sourceLocation, String destinationLocation, Vector result)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("CVSetDTFormTemplate :::: "+sid);
	    	int retValue=0;
	    	Vector ret= new Vector();
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		result=  vws.CVSetDTFormTemplate(session.room, sid,templateId,templateName,formName,sourceLocation,destinationLocation,ret);
	    		VWClient.printToConsole("result inside CVSetDTFormTemplate :::: "+result);
	    		if(result!=null && result.size()>0){
	    			StringTokenizer st= new StringTokenizer(result.get(0).toString(), "\t");
	    			retValue= Integer.parseInt(st.nextToken());
	    			return retValue;
	    		} else {
	    			retValue=0;
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	return retValue;
	    }
	    
	    
	    /**
	     * CV10.1 Enhancement: Fetches all template details
	     * @author apurba.m
	     * @param sid
	     * @param templateList
	     * @return
	     */
	    public int CVGetDTFormTemplate(int sid, Vector templateList) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in CVGetDTFormTemplate :::: "+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = 
	                   vws.CVGetDTFormTemplate(session.room, sid, templateList);
	            VWClient.printToConsole("ret inside VWClient.CVGetDTFormTemplate :::: "+ret);
	            VWClient.printToConsole("templateList is :::: "+templateList);
	            templateList.addAll(ret);
	            VWClient.printToConsole("After assigning templateList is :::: "+templateList);
	            return NoError;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
		}
	    
	    /**
	     * Cv10.1 Enhancement: Fetches details of a template based on template name
	     * @author apurba.m
	     * @param sid
	     * @param templateName
	     * @param templateList
	     * @return
	     */
	    public int CVGetDTFormTemplateDetails(int sid, String templateName, Vector templateDetails) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in CVGetDTFormTemplateDetails :::: "+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = 
	                   vws.CVGetDTFormTemplateDetails(session.room, sid, templateName, templateDetails);
	            VWClient.printToConsole("ret inside VWClient.CVGetDTFormTemplateDetails :::: "+ret);
	            VWClient.printToConsole("templateDetails is :::: "+templateDetails);
	            templateDetails.addAll(ret);
	            VWClient.printToConsole("After assigning templateDetails is :::: "+templateDetails);
	            return NoError;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
		}	
	    
	    /**
	     * Fetches Contentverse server location
	     * @author apurba.m
	     * @param sid
	     * @param serverPath
	     * @return
	     */
	    public int getServerHome(int sid, Vector serverPath){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sid in VWClient.getServerHome :::: "+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws= getServer(session.server);
	    	if(vws == null) return ServerNotFound;
	    	try {
	    		ServerSchema schema = vws.getSchema();
	    		String serverIP = schema.getAddress();
	    		VWClient.printToConsole("serverIP VWClient.getServerHome :::: "+serverIP);
	    		String serverHome= vws.getServerHome(session.room, sid);
	    		VWClient.printToConsole("serverHome inside VWClient.getServerHome :::: "+serverHome);
	    		serverPath.add(serverIP);
	    		VWClient.printToConsole("serverPath after adding path :::: "+serverPath);
	    		return NoError;
	    	} catch(Exception ex){
	    		return Error;
	    	}
	    }
	    
	    /**
	     * CV10.1 Enhancement: Deletes selected form form template details
	     * @author apurba.m
	     * @param sid
	     * @param templateName
	     * @param result
	     * @return
	     */
	    public int CVDelDTFormTemplate(int sid, String templateName, Vector result)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("CVDelDTFormTemplate :::: "+sid);
	    	int retValue=0;
	    	Vector ret= new Vector();
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		result=  vws.CVDelDTFormTemplate(session.room, sid, templateName, ret);
	    		VWClient.printToConsole("result inside CVDelDTFormTemplate :::: "+result);
	    		if(result.get(0).toString().equals("1")){
	    			StringTokenizer st= new StringTokenizer(result.get(0).toString(), "\t");
	    			retValue= Integer.parseInt(st.nextToken());
	    			return retValue;
	    		} else {
	    			retValue=0;
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	return retValue;
	    }
	    
	    /**
	     * CV10.1 Enhancement : Checks for duplicate template name
	     * @author apurba.m
	     * @param sid
	     * @param templateName
	     * @param result
	     * @return
	     */
	    public int CVCheckForTemplateExist(int sid, String templateName, Vector result)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("CVCheckForTemplateExist :::: "+sid);
	    	int retValue=0;
	    	Vector ret= new Vector();
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		result=  vws.CVCheckForTemplateExist(session.room, sid, templateName, ret);
	    		VWClient.printToConsole("result inside CVCheckForTemplateExist :::: "+result);
	    		StringTokenizer st= new StringTokenizer(result.get(0).toString(), "\t");
	    		retValue= Integer.parseInt(st.nextToken());
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	return retValue;
	    }
	    
	    public int VWNodeGetDocTypesWS(int sid, int nodeId, Vector documentTypes)
	    {
	    	Session session = getSession(sid);
	    	//VWClient.printToConsole("sessionId in VWNodeGetDocTypesWS::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret = vws.VWNodeGetDocTypes(sid,session.room, nodeId, documentTypes);
	    		documentTypes.addAll(vectorToDocTypes(ret));
	    		 if (sid>0 && vws.getActiveError()<0 ){
	          	   return NoError;
	             } else {
	          	   return vws.getActiveError();
	             }
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    public int getDocTypeIndicesWithRulesWS(int sid, int nodeId, DocType dt, Vector indices){
	    	Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getDocTypeIndicesWithRulesWS::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
//	    	Vector indices = new Vector();
	    	if (vws == null) return ServerNotFound;       
	    	try
	    	{
	    		Vector ret = 
	    				vws.getDocTypeIndicesWithRules(session.room, sid, nodeId, dt.getId(), indices);
	    		indices.addAll(vectorToIndices(ret));
	    		dt.setIndices(indices);
	    		ret.removeAllElements();
	    		for (int i=0; i < indices.size(); i++)
	    		{
	    			Index idx = (Index) indices.get(i);
	    			if ( idx.getType() == Index.IDX_SELECTION )
	    			{
	    				ret = vws.populateSelectionIndex(session.room, 
	    						sid, idx.getId(), ret);
	    				String defVal=idx.getDefaultValue();
	    				idx.setDef(stripIds(ret));
	    				idx.setDef(defVal);
	    				ret.removeAllElements();
	    			}
	    			/**
	    			 * Ehancement:Sow2096_891-Add Default Index with users and groups
	    			 * Code added get the selection list in dtc.
	    			 */
	    			if(idx.getSelectValue()==1&&idx.getType()==Index.IDX_TEXT){
	    				ret = vws.populateSelectionIndex(session.room, sid,-1, ret);	
	    				String defVal=idx.getDefaultValue();
	    				idx.setDef(stripIds(ret));
	    				idx.setDef(defVal);
	    				ret.removeAllElements();
	    			}
	    			if(idx.getSelectValue()==2&&idx.getType()==Index.IDX_TEXT){
	    				ret = vws.populateSelectionIndex(session.room, sid,-2, ret);	
	    				String defVal=idx.getDefaultValue();
	    				idx.setDef(stripIds(ret));
	    				idx.setDef(defVal);
	    				ret.removeAllElements();
	    			}
	    		}
	    		  VWClient.printToConsole("Active Error in getDocTypeIndicesWithRulesWS--->"+vws.getActiveError());
	    		  if (sid>0 && vws.getActiveError()<0 ){
	              	return NoError;
	              } else {
	              	return vws.getActiveError();
	              }
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    public int getNodeParentsForWS(int sid, Node node, Vector nodes)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getNodeParentsForWS::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = vws.getNodeParents(session.room, sid, node.getId(), 
	                                                                         nodes);
	            VWClient.printToConsole("ret inside getNodeParentsForWS :::: "+ret);
	            nodes.addAll(ret);
	            VWClient.printToConsole("Active Error in getNodeParentsForWS--->"+vws.getActiveError());
	        	if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
		
	    /** Creates a temporary or permanent Search for the session's user for new web access
	     * @author apurba.m
	     * @param sid Session id obtained by login
	     * @param srch The Search to create
	     * @return 0 for success or negative value error code
	     */
	    public int createSearch1(int sid, Search srch) 
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in createSearch1::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            int ret = vws.createSearch1(session.room, sid, srch);
	            srch.setId(ret);
	            return ret;
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    //Method created for CVWeb DbLookup
	    public int getIndexMappingWS(int sid, DocType dt,Vector result) 
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in getIndexMappingWS::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector result1 = vws.getIndexMapping(session.room, sid, dt);	
	    		if(result1.size()>0&&result1!=null)
	    			result.addAll(result1);
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    /**
	     * CV2019 merges from CV10.2 line
	     * @param sid
	     * @param optionKey
	     * @param result
	     * @return
	     */
	    public int getStatisticsInfo(int sid, String optionKey, Vector result)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getStatisticsInfo::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = vws.getStatisticsInfo(session.room, sid, optionKey, result);
	            VWClient.printToConsole("ret inside getStatisticsInfo :::: "+result);
	            result.addAll(ret);
	            VWClient.printToConsole("Active Error in getStatisticsInfo--->"+vws.getActiveError());
	        	if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    /**
	     * Method added to check the all.zip size,files list from the dss for AIP.
	     * @param sid
	     * @param doc
	     * @param dssAllZipFileVect
	     * @return
	     */
	    public int checkUploadedDocumentInfo(int sid, Document doc,Vector dssAllZipFileVect){
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in checkUploadedDocumentInfo::"+sid);
	    	if(session==null) return invalidSessionId;
	    	if (doc == null || doc.getId() <= 0) return invalidParameter;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	String storageType="";
	    	String azureUserName="";
	    	String azurePassword="";
	    	ServerSchema dssSchema = new ServerSchema();
	    	try
	    	{
	    		ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
	    		dssSchema.set(ss);
	    		Vector storageVect=new Vector();
	    		getAzureStorageCredentials(sid,doc.getId(),storageVect);
	    		if(storageVect!=null&&storageVect.size()>0){
	    			String storageArray[]=storageVect.get(0).toString().split(Util.SepChar);
	    			storageType=storageArray[7];
	    			azureUserName=storageArray[8];
	    			azurePassword=storageArray[9];
	    			VWClient.printToConsole("storageType in checkUploadedDocumentInfo:::::"+storageType);
	    		}
	    	}
	    	catch(RemoteException re)
	    	{
	    		connectedVWSs.remove(session.server);
	    		return Error;
	    	}

	    	if (isDSSUsed()){
	    		try{
	    			if(storageType.equals("On Premises")){
	    				DSS ds = (DSS) Util.getServer(dssSchema);
	    				if (ds == null) return dssInactive;
	    				VWClient.printToConsole("dssSchema.path  in checkUploadedDocumentInfo :"+dssSchema.path);
	    				ArrayList list = ds.getAllZipFilesList(dssSchema.path);
	    				if(list!=null){
	    					dssAllZipFileVect.addAll(list);
	    				}
	    			}

	    		}catch(Exception e){
	    			VWClient.printToConsole("Exception  in checkUploaddedDocument"+e.getMessage());
	    		}
	    	}
	    	return NoError;
	    }
	    
	    /**
	     * getMailDetails for CVWeb
	     * Modified Date:- 28_09_2017
	     * Modified By:-Madhavan
	     * @param server
	     * @param rooms
	     * @return
	     */
	    public int getMailDetails(int sid,Vector mailSettings)
	    {
	    	try{
	    		Session session = getSession(sid);
		    	VWClient.printToConsole("sessionId in getIndexMappingWS::"+sid);
		    	if(session==null) return invalidSessionId;
		    	VWS vws = getServer(session.server);
		    	if (vws == null) return ServerNotFound;
	    		Vector mailConfiguration=new Vector();
	    		String serverName=vws.getEmailServerName();
	    		String emailId=vws.getFromEmailId();
	    		String port=vws.getEmailServerPort();
	    	    String userName=vws.getEmailServerUsername();
	    	    String password=vws.getEmailServerPassword();
	    	    String mailDetails=serverName+Util.SepChar+emailId+Util.SepChar+port+Util.SepChar+userName+Util.SepChar+password;
	    	    mailSettings.add(mailDetails);
	    		
	    	}catch(Exception e){
	    		return Error;
	    	}
			return NoError;
	    }
	    
	    /**
		 * Method is added to generate XML file for new web access
		 * @author apurba.m
		 * @param sid
		 * @param finalParams
		 * @param result
		 * @return
		 */
		public int generateXmlfile(int sid, String finalParams, Vector result) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in generateXmlfile ::::" + sid);
			System.out.println("sessionId in generateXmlfile ::::" + sid);
			if (session == null)
				return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null)
				return ServerNotFound;
			try {
				Vector result1 = vws.generateXmlfile(session.room, sid, finalParams);
				if (result1.size() > 0 && result1 != null)
					result.addAll(result1);
				if (sid > 0 && vws.getActiveError() < 0) {
					return NoError;
				} else {
					return vws.getActiveError();
				}
			} catch (Exception e) {
				return Error;
			}
		}
	    
	    public int setCVWebDownloadPage(int sid,int docId, String docName,String pageId)
	    {
	    	Session session = getSession(sid);
	    	System.out.println("sessionId in vwclient setCVWebDownloadPage::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector retVect= new Vector();
	    		int ret = vws.setCVWebDownloadPage(session.room, sid, docId, docName,pageId);
	    		System.out.println("ret inside vwclient setCVWebDownloadPage :::: "+ret);
	    		VWClient.printToConsole("Active Error in setCVWebDownloadPage--->"+vws.getActiveError());
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	  
	    
	    public int syncLockDocument(int sid,int docId,int type)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setCVWebDownloadPage::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		//Vector retVect= new Vector();
	    		vws.syncLockDocument(session.room, sid, docId,type);
	    		//result.addAll(retVect);
	    		VWClient.printToConsole("Active Error in setCVWebDownloadPage--->"+vws.getActiveError());
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	  
	    
	    public int isDocumentLocked(int sid, int docId,Vector name) {

	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setCVWebDownloadPage::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector retVect= new Vector();
	    		vws.isDocumentLocked(session.room,sid, docId, name);
	    		retVect.addAll(name);
	    		VWClient.printToConsole("Active Error in setCVWebDownloadPage--->"+vws.getActiveError());
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}

	    	/*boolean locked = false;
	         activeError = getServer(room).isDocumentLocked(sid, did, name);
	         if (activeError == -1)
	         	locked = false;
	         else if (activeError == 0)
	         	locked = true;;
	         return locked;      */
	    }
    
	    /** Sets a stickynotecomment for a Document
	     * @param sid Session id obtained by login
	     * @param docComment The docComment referencing the Document for which to set the comment
	     * @return 0 for success or negative value error code
	     */
	    public int setStickyNote(int sid, DocComment docComment)
	    {
	        return setStickyNote(sid, docComment, false);
	    }
	    /**
	     * 
	     * @param sid
	     * @param docComment
	     * @param withCheck
	     * @return
	     *
	     *
	     */
	    public int setStickyNote(int sid, DocComment docComment, boolean withCheck)
	    {
	        return setStickyNote(sid, docComment, false,0);
	    }
	    
	    
	  public int setStickyNote(int sid, DocComment docComment, boolean withCheck,int docRestoreFlag )
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in setDocumentComment::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            if(docComment.getComment() !=null &&  
	                        docComment.getComment().length() > MAX_COMMENT_LENGTH)
	              docComment.setComment
	                      (docComment.getComment().substring(0,MAX_COMMENT_LENGTH));
	           Vector ret = vws.setStickyNote(session.room, sid,  docComment,withCheck,docRestoreFlag);                     
	           if(ret !=null && ret.size() > 0)
	           {
	                StringTokenizer st = new StringTokenizer
	                                            ((String) ret.get(0), Util.SepChar);
	              docComment.setId(Util.to_Number(st.nextToken()));
	              docComment.setUser(st.nextToken());
	              docComment.setTimeStamp(st.nextToken());
	           }
	           return docComment.getId();
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }      
	    }
	
	
	  
	    public int docDelStickyNote(int sid,int docId)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setCVWebDownloadPage::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		vws.docDelStickyNote(session.room, sid, docId);
	    		VWClient.printToConsole("Active Error in setCVWebDownloadPage--->"+vws.getActiveError());
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    public int setSecureLink( int sid, int docId,String recepientEmail,String password,int modifyPermission,String deliveryOn,String epiresOn )
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setSecureLink::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		System.out.println("session server  :::"+session.server);
	    		VWClient.printToConsole("session.server :::"+session.server);
	    		String url="&roomName="+session.room+"&documentId="+docId;
	    		System.out.println("url in setSecureLink :::"+url);
	    		VWClient.printToConsole("url in setSecureLink :::"+url);
	    		int retVal=vws.setSecureLink(session.room, sid, docId,recepientEmail,password,modifyPermission,deliveryOn,epiresOn,url);
	    		VWClient.printToConsole("Active Error in setSecureLink--->"+vws.getActiveError());
	    		return retVal;
	    		/*	if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}*/
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	
	    public int setUpdateSecureLink( int sid, int docId,String recepientEmail,String password,int modifyPermission,String deliveryOn,String epiresOn )
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setSecureLink::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		vws.setUpdateSecureLink(session.room, sid, docId,recepientEmail,password,modifyPermission,deliveryOn,epiresOn);
	    		VWClient.printToConsole("Active Error in setSecureLink--->"+vws.getActiveError());
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    
	    public int getSecureLink( int sid, int docId,int flag,String  recepientEmail)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setSecureLink::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		vws.getSecureLink(session.room, sid, docId, flag, recepientEmail);
	    		VWClient.printToConsole("Active Error in setSecureLink--->"+vws.getActiveError());
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    
	    
	    public int readSecureLinkDetails(String room, int sid, Vector secureMailList){
	    	VWClient.printToConsole("inside first line of readSecureLinkDetails");
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in readNotificationDetails::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	
	    	try {
	    		Vector secureInfo = new Vector();
	    		secureInfo = vws.readSecureLinkDetails(room, sid);
	    		VWClient.printToConsole("secureInfo VWClient readSecureLinkDetails::::::::::::::"+secureInfo);
	    		//Vector secureMailInfo = vectorToSecureLinkList(secureInfo);
	    		
	    		secureMailList.addAll(secureInfo);
	    	} catch (Exception ex) {
	    		return Error;
	    	}
	    	return NoError;
	    }
	    
	    public int readSecureLinkUrl(String room, int sid, Vector secureMailList){
	    	VWClient.printToConsole("inside first line of readSecureLinkDetails");
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in readNotificationDetails::"+sid);
	    	if (session == null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	
	    	try {
	    		Vector secureInfo = new Vector();
	    		secureInfo = vws.readSecureLinkUrl(room, sid);
	    		VWClient.printToConsole("secureInfo VWClient readSecureLinkDetails::::::::::::::"+secureInfo);
	    		//Vector secureMailInfo = vectorToSecureLinkList(secureInfo);
	    		
	    		secureMailList.addAll(secureInfo);
	    	} catch (Exception ex) {
	    		return Error;
	    	}
	    	return NoError;
	    }
	    
	    private Vector vectorToSecureLinkList(Vector secureMailLink) {
	    	// TODO Auto-generated method stub
	    	Vector secureLinkVector= new Vector();

	    	if(secureMailLink!=null && secureMailLink.size()>0){

	    		for(int i=0; i<secureMailLink.size(); i++){
	    			String dbstring = secureMailLink.get(i).toString();
	    			NotificationHistory notifyHistory = new NotificationHistory(dbstring);
	    			secureLinkVector.add(notifyHistory);
	    		}
	    	}
	    	return secureLinkVector;

	    }
	    
	    
	    public int updateExpiredLink(String room,int sid)throws RemoteException{
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in updateExpiredLink::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		vws.updateExpiredLink(session.room, sid);
	    		VWClient.printToConsole("Active Error in updateExpiredLink--->"+vws.getActiveError());
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    //Committed for session handling in cvweb
		public int setActiveClientCVWeb(int sid){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in setActiveClient::"+sid);
			if(session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if(vws==null) return ServerNotFound;
			int ret=0;
			try{
				ret=vws.setActiveClient(session.room, sid);
			}catch(Exception ex){
				return Error;
			}
			return ret;
		}
		public int  getLogedInClientType(int sid) {
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in setActiveClient::"+sid);
			if(session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if(vws==null) return ServerNotFound;
			int ret=0;
			try{
				ret=vws.getLogedInClientType(session.room, sid);
			}catch(Exception ex){
				return Error;
			}
			return ret;
		}

		
		
		public int getSecureLinkPermissionModify(int sid,String documentId,String userName, String password,Vector secureMailList){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in setActiveClient::"+sid);
			if(session == null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if(vws==null) return ServerNotFound;
			Vector ret=null;
			try{
				ret=vws.getSecureLinkPermissionModify(session.room, documentId,sid,userName,password);
				secureMailList.addAll(ret);
			}catch(Exception ex){
				return Error;
			}
			return NoError;
		}
		
		/**
	     * Method is added to fetch page information for web access
	     * @author apurba.m
	     * @param file
	     * @return
	     */
		public String getAuditInfoWS(File file)	{
			String pageInfo = "";
			File infoFile = new File(file.getParent(), "database.info");
			if(!infoFile.exists() || !infoFile.canRead() || infoFile.length()==0)
				return "";

			try 
			{
				RandomAccessFile raf = new RandomAccessFile(infoFile,"r");
				pageInfo = raf.readLine();
				raf.close();
			}
			catch(IOException e) {
				return "";
			}
			return pageInfo;
}
		
		/**
	     * @param sid Session id obtained by login
	     * @param doc The document for which to copy the file
	     * @param source The source folder path
	     * @param withId false - do not assume id folder, true - assume id folder
	     * @param newDoc true - newDoc , false - old Doc
	     * @param pageInfo page information
	     * @param indexerFlag true - add document to indexer queue.
	     * @return 0 for success or negative value error code
	     */
	    public int setDocFile(int sid, Document doc, String source, boolean withId, 
				boolean newDoc, String pageInfo, boolean indexerFlag, boolean retry, boolean isBackupDoc)
	    {
	    	VWClient.printToConsole("setDocFile 9.......");
	    	System.out.println();
	    	System.out.println();

	    	if (doc == null || doc.getId() <= 0) return invalidParameter;
	    	Session session = getSession(sid);
	    	long registrySize=0;
	    	long availableSpace=0;
	    	VWClient.printToConsole("sessionId in setDocFile::"+sid);
	    	System.out.println("sessionId in setDocFile::"+sid);
	    	if(session==null) return invalidSessionId;
	    	DSS ds = null;
	    	Document maxDoc=null;
	    	String azureUserName="";
	    	String azurePassword="";
	    	String dbStoragePath="";
	    	String storageType="";
	    	CloudStorageAccount storageAccount=null;
	    	CloudFileClient fileClient =null;
	    	CloudFile cloudFile=null;
	    	CloudFileShare share=null;
	    	CloudFileDirectory rootDir=null;
	    	CloudFileDirectory sampleDir =null;
	    	try{
	    		Vector storageVect=new Vector();
	    		VWClient.printToConsole("Before calling azure credentials in setDocFile::::::::::::"+Util.getNow(2));
	    		System.out.println("Before calling azure credentials in setDocFile::::::::::::"+Util.getNow(2));
	    		getAzureStorageCredentials(sid,doc.getId(),storageVect);
	    		VWClient.printToConsole("After calling azure credentials setDocFiles::::::::::::"+Util.getNow(2));
	    		System.out.println("After calling azure credentials setDocFiles::::::::::::"+Util.getNow(2));
	    		if(storageVect!=null&&storageVect.size()>0){
	    			StringTokenizer st = new StringTokenizer(storageVect.get(0).toString(), Util.SepChar); 
	    			String storageid=st.nextToken();
	    			String systemName=st.nextToken();
	    			String ipaddress=st.nextToken();
	    			String port=st.nextToken();
	    			String hostIp=st.nextToken();
	    			dbStoragePath=st.nextToken();
	    			String Uncpath=st.nextToken();
	    			String azureStroageType=st.nextToken();
	    			storageType=azureStroageType;
	    			VWClient.printToConsole("storageType from database::::::"+azureStroageType);
	    			System.out.println("storageType from database::::::"+azureStroageType);

	    			azureUserName=st.nextToken();
	    			VWClient.printToConsole("storageType from azureUserName::::::"+azureUserName);
	    			System.out.println("storageType from azureUserName::::::"+azureUserName);
	    			azurePassword=st.nextToken();
	    			VWClient.printToConsole("storageType from azurePassword::::::"+azurePassword);
	    			System.out.println("storageType from azurePassword::::::"+azurePassword);
	    		}
	    	}catch(Exception e){
	    		VWClient.printToConsole("Exception while getting the storage type in setdocFile:::::::"+e.getMessage());
	    		System.out.println("Exception while getting the storage type in setdocFile:::::::"+e.getMessage());
	    	}

	    	if(storageType.equals("On Premises")){
	    		Vector resultvect=new Vector();
	    		getLatestDocId(sid,resultvect);
	    		//CV10 :- Issue fix maxdocId is commented and we are passing the current doc object
	    		//	int maxDocId=Integer.parseInt(resultvect.get(0).toString());
	    		//	maxDoc=new Document(maxDocId);
	    		//CV10 Issue fix modified from maxdocid from procedure to docID
	    		Vector size=new Vector();
	    		VWClient.printToConsole("Before calling checkavailable space in dss::::::::::::"+Util.getNow(2));
	    		System.out.println("Before calling checkavailable space in dss::::::::::::"+Util.getNow(2));
	    		checkAvailableSpaceInDSS(sid,doc,"",size);
	    		VWClient.printToConsole("After calling checkavailable space in dss::::::::::::"+Util.getNow(2));
	    		System.out.println("After calling checkavailable space in dss::::::::::::"+Util.getNow(2));
	    		availableSpace= (long) size.get(0); 
	    		availableSpace=availableSpace*1024;
	    		registrySize=(long)size.get(1);
	    		registrySize=registrySize*1024;
	    	}
	    	File file = null;
	    	if (source == null)
	    	{
	    		VWClient.printToConsole("Inside source is null");
	    		System.out.println("Inside source is null");
	    		file = new File(session.cacheFolder + Util.pathSep + doc.getId() + 
	    				Util.pathSep + DOC_CONTAINER);
	    		VWClient.printToConsole("file path"+file.getAbsolutePath());
	    		System.out.println("file path"+file.getAbsolutePath());


	    	}
	    	else
	    	{
	    		VWClient.printToConsole("Inside else with source withId:::"+withId);
	    		System.out.println("Inside else with source withId:::"+withId);
	    		if (!source.endsWith(Util.pathSep)) source += Util.pathSep;
	    		if (withId)
	    		{

	    			file = new File(source + doc.getId() + Util.pathSep 
	    					+ DOC_CONTAINER);
	    			VWClient.printToConsole("inside withId if condition:::"+file.getAbsolutePath());
	    			System.out.println("inside withId if condition:::"+file.getAbsolutePath());
	    		}
	    		else
	    		{
	    			file = new File(source + DOC_CONTAINER);
	    			VWClient.printToConsole("inside else of ::::"+file.getAbsolutePath());
	    			System.out.println("inside else of ::::"+file.getAbsolutePath());
	    		}
	    	}
	    	if (!file.exists()){
	    		VWClient.printToConsole("Inside document not found");
	    		System.out.println("Inside document not found");
	    		return documentNotFound;
	    	}
	    	/*long fileSize = file.length();
	    	VWClient.printToConsole("All.zip file size in source location :"+fileSize);
	    	VWClient.printToConsole("File size in KB :"+(fileSize/1024));
	    	if ((fileSize / 1024) < 5) {
	    		VWClient.printToConsole("Invalid file size......");
	    		System.out.println("Invalid file size......");
	    		return InvalidAllDotZip;
	    	}
	    	boolean isValid = Util.isValid(file.getPath());
	    	VWClient.printToConsole("Is valid zip :"+isValid);
	    	if (!isValid) {
	    		VWClient.printToConsole("all.zip corrupted in source location....");
	    		System.out.println("all.zip corrupted in source location....");
	    		return ClientZipCorrupted;
	    	}*/
	    	VWClient.printToConsole("pageInfo in set docFile:::"+pageInfo);
	    	System.out.println("pageInfo in set docFile:::"+pageInfo);
	    	if(pageInfo == null) {
	    		VWClient.printToConsole("Before calling getAuditInfo ::::::::::::"+Util.getNow(2));
	    		System.out.println("Before calling getAuditInfo ::::::::::::"+Util.getNow(2));
	    		pageInfo = getAuditInfo(file);   
	    		VWClient.printToConsole("after calling getAuditInfo ::::::::::::"+Util.getNow(2));
	    		System.out.println("after calling getAuditInfo ::::::::::::"+Util.getNow(2));
	    		VWClient.printToConsole("Page info from database info file:::"+pageInfo);
	    		System.out.println("Page info from database info file:::"+pageInfo);
	    	}
	    	ServerSchema dssSchema = new ServerSchema();
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		VWClient.printToConsole("Before calling getDSSforDoc:::::"+"session.room::"+session.room+"sid:::"+"docId:::"+doc.getId()+"dssSchema:::"+dssSchema.getPath());
	    		System.out.println("Before calling getDSSforDoc:::::"+"session.room::"+session.room+"sid:::"+"docId:::"+doc.getId()+"dssSchema:::"+dssSchema.getPath());
	    		ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
	    		dssSchema.set(ss);
	    	}
	    	catch(RemoteException re)
	    	{
	    		connectedVWSs.remove(session.server);
	    		VWClient.printToConsole("Inside remote exception :::::"+re.getMessage());
	    		System.out.println("Inside remote exception :::::"+re.getMessage());
	    		return Error;
	    	}
	    	/* Purpose:  Direct DSS call changed. And it is through VWS
	    	 *  Created by: C.Shanmugavalli			Date: 09 Oct 2006
	    	 */        
	    	//1 Direct DSS call changed - Valli
	    	//DSS ds = (DSS) Util.getServer(dssSchema);
	    	//if (ds == null) return dssInactive;
	    	VWClient.printToConsole("calling set doc file next version:::");
	    	System.out.println("calling set doc file next version:::");
	    	Vector lv = new Vector();
	    	VWClient.printToConsole("calling checkNextDocVersion:::::");
	    	System.out.println("calling checkNextDocVersion:::::");
	    	checkNextDocVersion(sid, doc, pageInfo, newDoc, lv);
	    	if(lv.size() > 0) doc.setVersion( (String) lv.get(0));
	    	VWClient.printToConsole("calling set doc file next version:::doc.getVersion()"+doc.getVersion());
	    	System.out.println("calling set doc file next version:::doc.getVersion()"+doc.getVersion());
	    	VWDoc vwdoc = doc.getVWDoc();
	    	VWClient.printToConsole("setting to vwdoc source file:::"+file.getPath());
	    	System.out.println("setting to vwdoc source file:::"+file.getPath());
	    	VWClient.printToConsole("setting to vwdoc dss file path:::"+dssSchema.path);
	    	System.out.println("setting to vwdoc dss file path:::"+dssSchema.path);
	    	System.out.println("vwdoc :::: "+vwdoc);
	    	vwdoc.setSrcFile(file.getPath());
	    	vwdoc.setDstFile(dssSchema.path);
	    	try{
	    		if(storageType.equals("On Premises")){
	    			File destFile=new File(dssSchema.path);
	    			long destFileSize=getFileSize(destFile);
	    			destFileSize=destFileSize/1024;
	    			long sourceFileSize=vwdoc.getSize();
	    			sourceFileSize=sourceFileSize/1024;
	    			if(registrySize>(availableSpace-(sourceFileSize-destFileSize))){
	    				SendEmailFromDSS(availableSpace/1024,sid,maxDoc);
	    				return dssCriticalError;
	    			}
	    		}
	    	}
	    	catch(Exception dssexp){
	    		VWClient.printToConsole("DSS Space Check inside setDoc :"+dssexp.getMessage());
	    		System.out.println("DSS Space Check inside setDoc :"+dssexp.getMessage());
	    	}

	    	try
	    	{
	    		java.util.Properties props = System.getProperties();
	    		String backupPath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + PRODUCT_NAME + File.separator + session.room + File.separator + doc.getId();
	    		File backupLocation = new File(backupPath);
	    		boolean ret = false;
	    		/*
	    		 *  This condition will be checking the  DSS is used or not
	    		 */
	    		VWClient.printToConsole("isDSSUsed::::"+isDSSUsed());
	    		if (isDSSUsed()) {
	    			VWClient.printToConsole("Inside DSS");
	    			ds = (DSS) Util.getServer(dssSchema);
	    			/*if (ds == null) {
	        			vws.sendNotification(session.room, sid, VWSConstants.DSS, resourceManager.getString("DSSInactive.Mail"));
	        			return dssInactive;      
	        		}*/
	    			VWClient.printToConsole("Before calling setDocument inside if condition of dssused:::isBackupDoc"+isBackupDoc);
	    			System.out.println("Before calling setDocument inside if condition of dssused:::isBackupDoc"+isBackupDoc);
	    			VWClient.printToConsole("doc.getid() : " + doc.getId());
	    			System.out.println("doc.getid() : " + doc.getId());
	    			VWClient.printToConsole("isBackupDoc : " + isBackupDoc);
	    			System.out.println("isBackupDoc : " + isBackupDoc);
	    			vwdoc = ds.setDocument(doc, session.room, isBackupDoc);
	    			VWClient.printToConsole("After calling setDocument inside if condition of dssused:::");   //last checked
	    			System.out.println("After calling setDocument inside if condition of dssused:::");
	    			if(storageType.equals("On Premises")){
	    				System.out.println("Before WHILE !!!! ");
	    				while (vwdoc.hasMoreChunks() && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE)
	    				{
	    					VWClient.printToConsole("Inside while <vdoc.hasMoreChunks:true>");
	    					System.out.println("INSIDE WHILE !!!! ");
	    					doc.setVWDoc(vwdoc);
	    					VWClient.printToConsole("Inside while before calling setdocument");
	    					System.out.println("Inside while before calling setdocument");
	    					vwdoc = ds.setDocument(doc, session.room, isBackupDoc);	          
	    					VWClient.printToConsole("Inside while after calling setdocument");
	    					System.out.println("Inside while after calling setdocument");
	    				}
	    				System.out.println("AFTER WHILE !!!! ");
	    				VWClient.printToConsole("AFTER WHILE !!!! ");
	    			}
	    		}else {
	    			// Pass the schema to displayed the client address when upload the document
	    			VWClient.printToConsole("Inside else before calling set documents");
	    			System.out.println("Inside else before calling set documents");
	    			VWClient.printToConsole("dssSchema.path : " + dssSchema.path);
	    			System.out.println("dssSchema.path : " + dssSchema.path);
	    			VWClient.printToConsole("doc.getid() : " + doc.getId());
	    			System.out.println("doc.getid() : " + doc.getId());
	    			VWClient.printToConsole("file : " + file);
	    			System.out.println("file : " + file);
	    			vwdoc = vws.setDocument(session.room, dssSchema, doc, file, mySchema);
	    			VWClient.printToConsole("Inside else after calling set documents");
	    			System.out.println("Inside else after calling set documents");
	    			//vwdoc = ds.setDocument(doc, session.room);
	    			//Checks whether the document has more chunks(Each chunk is of size 4MB)
	    			if(storageType.equals("On Premises")){ 
	    				while (vwdoc.hasMoreChunks() && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE)
	    				{
	    					VWClient.printToConsole("Inside while <vdoc.hasMoreChunks:true>");
	    					VWClient.printToConsole("Inside else of while looop before calling set document");
	    					System.out.println("Inside else of while looop before calling set document");
	    					doc.setVWDoc(vwdoc);
	    					vwdoc = vws.setDocument(session.room, dssSchema, doc, file, mySchema);
	    				}
	    				VWClient.printToConsole("AFTER WHILE !!!! ");
	    			}
	    		}
	    		if(storageType.equals("On Premises")){
	    			System.out.println("On Premises !!!! ");

	    			if ((!vwdoc.isVaildZip() || !vwdoc.getIntegrity()) && !retry && vwdoc.getDocStatus() != VWDoc.DISKSPACE_NOT_AVAILABLE){
	    				addATRecord(vws, session.room, sid, doc.getId(),
	    						VWATEvents.AT_OBJECT_DOC,
	    						VWATEvents.AT_DOC_FILEUPDATE, 
	    						VWATEvents.AT_DOC_FILEUPDATE_FAIL_DESC);
	    				System.out.println("After addATRecord !!!! ");
	    				vwdoc.setVaildZip(true);
	    				return setDocFile(sid, doc, source, withId, newDoc, pageInfo, indexerFlag, true);

	    			}
	    			if ((!vwdoc.isVaildZip() || !vwdoc.getIntegrity()) && (retry || vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE)){
	    				//copied all.zip and moved to Application Data\ViewWise\Recover\Room\docId location.  
	    				System.out.println("copied all.zip and moved to Application Data location");
	    				try{
	    					if (!backupLocation.exists()) backupLocation.mkdirs();
	    					VWClient.printToConsole("backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER::::0"+backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER);
	    					System.out.println("backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER::::0"+backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER);
	    					boolean result = VWCUtil.copyFile(file, new File(backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER));
	    					// To create a document.properties file in the backup location
	    					VWClient.printToConsole("before calling createDocProperties:::"+backupPath);
	    					System.out.println("before calling createDocProperties:::"+backupPath);
	    					createDocProperties(sid, doc, backupPath);
	    					VWClient.printToConsole("after calling createDocProperties:::"+backupPath);
	    					System.out.println("after calling createDocProperties:::"+backupPath);
	    				}catch (Exception e) {
	    					VWClient.printToConsole("Inside corrupted 1:::::"+e.getMessage());
	    					System.out.println("Inside corrupted 1:::::"+e.getMessage());
	    					return DSSZipCorrupted;
	    				}
	    				if (vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE){
	    					System.out.println("DISKSPACE_NOT_AVAILABLE !!!!");
	    					return DSSDiskSpaceNotAvailable;
	    				}else{
	    					VWClient.printToConsole("Inside corrupted 2:::::");
	    					System.out.println("Inside corrupted 2:::::");
	    					return DSSZipCorrupted;
	    				}

	    			}
	    			if (vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE){
	    				System.out.println("DISKSPACE_NOT_AVAILABLE 2 !!!!");
	    				return DSSDiskSpaceNotAvailable;
	    			}
	    		}//CV10 Enhancement added for Azure storage file upload
	    		else if(storageType.equals("Azure Storage")){
	    			System.out.println("Azure Storage !!!! ");
	    			if (!backupLocation.exists()) 
	    				backupLocation.mkdirs();

	    			boolean result = VWCUtil.copyFile(file, new File(backupLocation.getAbsolutePath() + File.separator + DOC_CONTAINER));
	    			VWClient.printToConsole("result in Azure Storage::::"+result);
	    			System.out.println("result in Azure Storage::::"+result);

	    			try {
	    				String accountName="AccountName="+azureUserName+";";
	    				String password="AccountKey="+azurePassword;
	    				String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
	    				System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
	    				System.out.println("Azure Storage try !!!! ");
	    				
	    				storageAccount = CloudStorageAccount.parse(storageConnectionString1);
	    				VWClient.printToConsole("storageAccount connection established :::"+storageAccount.getFileStorageUri());
	    				System.out.println("storageAccount connection established :::"+storageAccount.getFileStorageUri());
	    				fileClient = storageAccount.createCloudFileClient();
	    				VWClient.printToConsole("File client storage Uri ::"+fileClient.getStorageUri());
	    				System.out.println("File client storage Uri ::"+fileClient.getStorageUri());
	    				String pattern = Pattern.quote(System.getProperty("file.separator"));
	    				String dbpath[]=dbStoragePath.split(pattern);

	    				for(int i=0;i<dbpath.length;i++){
	    					if(i==2)
	    					{
	    						share = fileClient.getShareReference(dbpath[i]);
	    						VWClient.printToConsole("Inside loop count 2 share :"+share.getUri());
	    						System.out.println("Inside loop count 2 share :"+share.getUri());
	    						if(!share.exists())
	    							share.create();
	    					}
	    					else if(i==3)
	    					{
	    						sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
	    						VWClient.printToConsole("Inside loop count 3 share :"+share.getUri());
	    						System.out.println("Inside loop count 3 share :"+share.getUri());
	    						if(!sampleDir.exists()){
	    							sampleDir.create();

	    						} 
	    						rootDir = sampleDir;
	    						sampleDir = null;
	    					}
	    					else if(i>=4)
	    					{
	    						VWClient.printToConsole("Inside loop count 4 share :"+share.getUri());
	    						System.out.println("Inside loop count 4 share :"+share.getUri());
	    						if ((i+1) != dbpath.length) {
	    							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
	    							if(!sampleDir.exists()){
	    								sampleDir.create();
	    							}
	    							rootDir = sampleDir;
	    							sampleDir = null;
	    						}
	    						else if ((i+1) == dbpath.length) {
	    							sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
	    							if(!sampleDir.exists()){
	    								sampleDir.create();
	    							}
	    							rootDir = sampleDir;

	    							sampleDir = null;
	    						}
	    					}

	    				}
	    				sampleDir =	rootDir.getDirectoryReference(String.valueOf(doc.getId()));
	    				VWClient.printToConsole("sampleDir.geturi:  " + sampleDir.getUri());
	    				System.out.println("sampleDir.geturi:  " + sampleDir.getUri());
	    				if(!sampleDir.exists()){
	    					sampleDir.create();

	    				}
	    				cloudFile = sampleDir.getFileReference("all.zip");

	    				if (sampleDir.exists()) {
	    					/*
	    					 * CV10 Azure set active version issue fix.Upload document after setactive version or properties update
	    					 * Zip creation was not hapenning
	    					 */
	    					if(cloudFile.exists()){
	    						String newVersion = "";
	    						Vector newVersVector = new Vector();
	    						DocType dt = doc.getDocType();
	    						VWClient.printToConsole("Azure Before getDTVersionSettings call");
	    						System.out.println("Azure Before getDTVersionSettings call");
	    						getDTVersionSettings(sid, dt);
	    						if (dt!=null && dt.getVREnable().equals("1")){
	    							VWClient.printToConsole("Azure Before checkNextDocVersion call");
	    							System.out.println("Azure Before checkNextDocVersion call");
	    							checkNextDocVersion(sid, doc, pageInfo, newDoc, newVersVector);
	    							if(newVersVector!=null && newVersVector.size() > 0) {
	    								newVersion =  (String) newVersVector.get(0);

	    							}
	    							if (newVersion != null && !newVersion.equals(""))
	    							{
	    								CloudFile currentvercloudfile = sampleDir.getFileReference(doc.getVersion());
	    								if(!currentvercloudfile.exists()){
	    									currentvercloudfile.startCopy(cloudFile);
	    								}
	    							}
	    						}/*else{
									VWClient.printToConsole("Inside else before delete docversion :::::::::::::"+doc.getVersion());
									if(!doc.getVersion().equals("")){
										CloudFile neVercloudfile = sampleDir.getFileReference(doc.getVersion());
										VWClient.printToConsole("neVercloudfile geturi::::::::::::"+neVercloudfile.getUri());
										if(!neVercloudfile.exists()){
											neVercloudfile.startCopy(cloudFile);
										}
									}
								}*/
	    						cloudFile.delete();

	    					}
	    					VWClient.printToConsole("RoomName getEncryption in setdocfile::::::::::"+session.room);
	    					System.out.println("RoomName getEncryption in setdocfile::::::::::"+session.room);
	    					String key=vws.getEncryptionKey(session.room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
	    					DSSCipher cipher = new DSSCipher(key);
	    					if ((cipher != null)&&(!key.equals("")))  
	    					{
	    						String fileSrcPath=file.getPath();
	    						cipher.encryptFile(fileSrcPath);
	    						cloudFile.uploadFromFile(fileSrcPath);
	    						VWClient.printToConsole("Azure file upload with encryption");
	    						System.out.println("Azure file upload with encryption");
	    					}else{
	    						VWClient.printToConsole("Azure file upload without encryption");
	    						System.out.println("Azure file upload without encryption");
	    						cloudFile.uploadFromFile(file.getPath());
	    					}
	    					if(cloudFile.exists())
	    						VWClient.printToConsole("Document with "+" "+ doc.getId()+" "+"Uploaded SucessFully to Azure Storage");
	    					System.out.println("Document with "+" "+ doc.getId()+" "+"Uploaded SucessFully to Azure Storage");
	    				} 

	    				if(indexerFlag){
	    					addIndexerDoc(vws, session.room, sid, doc.getId(),3);
	    				}

	    			}catch (Exception e) {
	    				VWClient.printToConsole("Failed to save document to cloud :::"+e.getMessage());
	    				System.out.println("Failed to save document to cloud :::"+e.getMessage());
	    				String errorMessage=e.getMessage().toString();
	    				VWClient.printToConsole("errorMessage:::::"+errorMessage);
	    				System.out.println("errorMessage:::::"+errorMessage);
	    				if(errorMessage.equals("Storage Key is not a valid base64 encoded string.")){
	    					VWClient.printToConsole("Inside if condition of exception");
	    					System.out.println("Inside if condition of exception");
	    					return ViewWiseErrors.invalidCredentials;
	    				}else{
	    					VWClient.printToConsole("Inside else condition of exception");
	    					System.out.println("Inside else condition of exception");
	    					return ViewWiseErrors.uploadFailed;
	    				}

	    			}




	    			// To create a document.properties file in the backup location
	    			createDocProperties(sid, doc, backupPath);

	    			/*
						Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
						Created by: <Pandiya Raj.M>
						Date: <6 Jun 2006>
	    			 */
	    			/*
	    			 * Issue No / Purpose:  Indexer issue # 633 this method not sending indexOption value '3'>
	    			 * i.e the new status '3' is not going as param to DB Whenever add/modify a page in a document in DTC,
	    			 * Created by:	C.Shanmugavalli	Date: 2 July 2006
	    			 */
	    			// Check that document modified, if modified it will delete the page reference in indexer table and store the '3' for 'qc' column for that document            	                
	    			int indexOption = 3;
	    			boolean flag = false;
	    			if (pageInfo != null && pageInfo.length() > 0){
	    				ArrayList listPage = new ArrayList();
	    				StringTokenizer sToken = new StringTokenizer(pageInfo,",");
	    				String pageNo,pageID,operation = null;
	    				while(sToken.hasMoreTokens()){listPage.add(sToken.nextToken());}
	    				int count = 1;
	    				for(int index=0;index<(listPage.size()/3);index++)
	    				{
	    					try {
	    						pageNo = String.valueOf(listPage.get(count++));
	    						pageID = String.valueOf(listPage.get(count++));
	    						operation = String.valueOf(listPage.get(count++));                			
	    						if ("Y".equals(operation.toUpperCase()) || "D".equals(operation.toUpperCase())){ // Y means Modified.
	    							int pageid = 0;
	    							if (pageID!= null) {pageid =  Integer.parseInt(pageID);}

	    							VWClient.printToConsole("Inside azure Before callind DBRemovePageText::::pageid"+pageid);
	    							System.out.println("Inside azure Before callind DBRemovePageText::::pageid"+pageid);
	    							vws.DBRemovePageText(session.room,doc.getId(),pageid);
	    							flag =true;
	    						}
	    						//W means page added. D means deleted page
	    						// N means Not deleted page. Y means page modified
	    						if(("W".equals(operation.toUpperCase()))){ 
	    							//if(!("W".equals(operation.toUpperCase()))){ // commented for Issue # 633 
	    							flag =true;
	    						}
	    					}catch(Exception exception){}
	    				}
	    			}

	    			if(indexerFlag){
	    				addIndexerDoc(vws, session.room, sid, doc.getId(),3);
	    			}
	    			if(isBackupDoc){
	    				addATRecord(vws, session.room, sid, doc.getId(), 
	    						VWATEvents.AT_OBJECT_DOC,
	    						VWATEvents.AT_DOC_FILEUPDATE, 
	    						VWATEvents.AT_REC_DOC_FILEUPDATE_DESC);
	    				if(backupLocation.exists()){
	    					VWClient.printToConsole("before delete backuplocaion::::"+backupLocation);
	    					System.out.println("before delete backuplocaion::::"+backupLocation);
	    					ret = deleteBackupLocation(backupLocation);		
	    				}
	    			}else{
	    				addATRecord(vws, session.room, sid, doc.getId(), 
	    						VWATEvents.AT_OBJECT_DOC,
	    						VWATEvents.AT_DOC_FILEUPDATE, 
	    						VWATEvents.AT_DOC_FILEUPDATE_DESC);
	    			}
	    			//After uploaded the all.zip; we need to make a database call to get the current version 
	    			//Create the method �updateVersionRevision()� which will create a copy of all.zip with current version 
	    			int result1 =  	vws.setDocAuditInfo(session.room, sid, doc, (indexerFlag?newDoc:false), pageInfo);
	    			String updatedVersion = "";
	    			Vector output = new Vector();
	    			DocType dt = doc.getDocType();
	    			VWClient.printToConsole("Azure Before getDTVersionSettings call");
	    			System.out.println("Azure Before getDTVersionSettings call");
	    			getDTVersionSettings(sid, dt);
	    			if (dt!=null && dt.getVREnable().equals("1")){
	    				VWClient.printToConsole("Azure Before checkNextDocVersion call");
	    				System.out.println("Azure Before checkNextDocVersion call");
	    				checkNextDocVersion(sid, doc, pageInfo, newDoc, output);
	    				if(output!=null && output.size() > 0) {
	    					updatedVersion =  (String) output.get(0);
	    					if (updatedVersion != null && updatedVersion.trim().length() >0){
	    						if (isDSSUsed()) {

	    							VWClient.printToConsole("Azure In if dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
	    							System.out.println("Azure In if dssused updateversion revision:::::updatedVersion::::"+updatedVersion);

	    							if (updatedVersion != null && !updatedVersion.equals(""))
	    							{   //CV10 Enhancement update document to azure with version revision commented.

	    								CloudFile cloudFile1 = sampleDir.getFileReference(updatedVersion);
	    								if(cloudFile.exists()){
	    									try {
	    										VWClient.printToConsole("Before start copy :::::");
	    										System.out.println("Before start copy :::::");
	    										cloudFile1.startCopy(cloudFile.getUri());
	    										VWClient.printToConsole("After start copy :::::");
	    										System.out.println("After start copy :::::");
	    										cloudFile1.abortCopy(cloudFile1.getProperties().getCopyState().getCopyId());
	    									}
	    									catch (StorageException e) {
	    										VWClient.printToConsole("Inside storage exception e::::"+e.getMessage());
	    										System.out.println("Inside storage exception e::::"+e.getMessage());
	    										if (!e.getErrorCode().contains("NoPendingCopyOperation")) {
	    											return  ViewWiseErrors.renameAzureDoc;
	    										}
	    									}

	    								}
	    							}	

	    						}
	    						else{
	    							VWClient.printToConsole("Azure In else of dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
	    							System.out.println("Azure In else of dssused updateversion revision:::::updatedVersion::::"+updatedVersion);
	    							vws.updateVersionRevision(session.room, dssSchema, doc,  updatedVersion);   
	    						}
	    					}
	    				}
	    			}
	    			return result1;
	    		}//End of azure file upload

	    		if (vwdoc.getIntegrity())
	    		{            	
	    			/*
					Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
					Created by: <Pandiya Raj.M>
					Date: <6 Jun 2006>
	    			 */
	    			/*
	    			 * Issue No / Purpose:  Indexer issue # 633 this method not sending indexOption value '3'>
	    			 * i.e the new status '3' is not going as param to DB Whenever add/modify a page in a document in DTC,
	    			 * Created by:	C.Shanmugavalli	Date: 2 July 2006
	    			 */
	    			// Check that document modified, if modified it will delete the page reference in indexer table and store the '3' for 'qc' column for that document            	                
	    			int indexOption = 3;
	    			boolean flag = false;
	    			if (pageInfo != null && pageInfo.length() > 0){
	    				ArrayList listPage = new ArrayList();
	    				StringTokenizer sToken = new StringTokenizer(pageInfo,",");
	    				String pageNo,pageID,operation = null;
	    				while(sToken.hasMoreTokens()){listPage.add(sToken.nextToken());}
	    				int count = 1;
	    				for(int index=0;index<(listPage.size()/3);index++)
	    				{
	    					try {
	    						pageNo = String.valueOf(listPage.get(count++));
	    						pageID = String.valueOf(listPage.get(count++));
	    						operation = String.valueOf(listPage.get(count++));                			
	    						if ("Y".equals(operation.toUpperCase()) || "D".equals(operation.toUpperCase())){ // Y means Modified.
	    							int pageid = 0;
	    							if (pageID!= null) {pageid =  Integer.parseInt(pageID);}
	    							VWClient.printToConsole("azure before dbremovepagetText:::"+pageid);
	    							System.out.println("azure before dbremovepagetText:::"+pageid);
	    							vws.DBRemovePageText(session.room,doc.getId(),pageid);
	    							flag =true;
	    						}
	    						//W means page added. D means deleted page
	    						// N means Not deleted page. Y means page modified
	    						if(("W".equals(operation.toUpperCase()))){ 
	    							//if(!("W".equals(operation.toUpperCase()))){ // commented for Issue # 633 
	    							flag =true;
	    						}
	    					}catch(Exception exception){}
	    				}
	    			}
	    			/*                if(flag){
	                	indexOption = 3;
	                } */  

	    			if(indexerFlag){
	    				addIndexerDoc(vws, session.room, sid, doc.getId(),3);
	    			}
	    			if(isBackupDoc){
	    				addATRecord(vws, session.room, sid, doc.getId(), 
	    						VWATEvents.AT_OBJECT_DOC,
	    						VWATEvents.AT_DOC_FILEUPDATE, 
	    						VWATEvents.AT_REC_DOC_FILEUPDATE_DESC);
	    				if(backupLocation.exists())
	    					ret = deleteBackupLocation(backupLocation);					
	    			}else{
	    				addATRecord(vws, session.room, sid, doc.getId(), 
	    						VWATEvents.AT_OBJECT_DOC,
	    						VWATEvents.AT_DOC_FILEUPDATE, 
	    						VWATEvents.AT_DOC_FILEUPDATE_DESC);
	    			}
	    			//After uploaded the all.zip; we need to make a database call to get the current version 
	    			//Create the method �updateVersionRevision()� which will create a copy of all.zip with current version 

	    			int result =  	vws.setDocAuditInfo(session.room, sid, doc, (indexerFlag?newDoc:false), pageInfo);
	    			String updatedVersion = "";
	    			Vector output = new Vector();
	    			DocType dt = doc.getDocType();
	    			getDTVersionSettings(sid, dt);
	    			if (dt!=null && dt.getVREnable().equals("1")){
	    				VWClient.printToConsole("Inside azure before calling checknextdocversion:::::"+pageInfo+"newDoc::::"+newDoc+"output:::"+output);
	    				System.out.println("Inside azure before calling checknextdocversion:::::"+pageInfo+"newDoc::::"+newDoc+"output:::"+output);
	    				checkNextDocVersion(sid, doc, pageInfo, newDoc, output);
	    				if(output!=null && output.size() > 0) {
	    					updatedVersion =  (String) output.get(0);
	    					if (updatedVersion != null && updatedVersion.trim().length() >0){
	    						if (isDSSUsed()) {
	    							VWClient.printToConsole("Inside azure if isDSSUsed updateVersionRevision:::::"+updatedVersion);
	    							System.out.println("Inside azure if isDSSUsed updateVersionRevision:::::"+updatedVersion);

	    							ds.updateVersionRevision(doc, updatedVersion);
	    						}
	    						else{
	    							VWClient.printToConsole("Inside azure else isDSSUsed updateVersionRevision:::::"+updatedVersion);
	    							System.out.println("Inside azure else isDSSUsed updateVersionRevision:::::"+updatedVersion);

	    							vws.updateVersionRevision(session.room, dssSchema, doc,  updatedVersion);     
	    						}
	    					}
	    				}
	    			}
	    			return result;
	    		}
	    		else
	    			return documentNotSet;
	    	}
	    	catch(Exception e)
	    	{
	    		VWClient.printToConsole("Error while set docFile::::"+e.getMessage());
	    		System.out.println("Error while set docFile::::"+e.getMessage());
	    		return Error;

	    	}

	    }
	    
	    /**
	     * Below methods are created to send document to workflow from new web access
	     * @author apurba.m
	     * @param sid
	     * @param docId
	     * @param docName
	     * @param routeId
	     * @param routeName
	     * @param taskSequence
	     * @return
	     */
	    public int sendDocInManualRouteWS(int sid, int docId, String docName, int routeId, String routeName, int taskSequence){
	    	return sendDocInManualRouteWS(sid, docId, docName, routeId, routeName, taskSequence, 0);
	    }
	    public int sendDocInManualRouteWS(int sid, int docId, String docName, int routeId, String routeName, int taskSequence, int signId){
	    	
	    	/*CV2019 changes - Modified by - vanitha***/
	    	Session session = getSession(sid);
	    	Vector secureEmailUrlVector = new Vector();
	    	VWClient.printToConsole("sessionId in VWClient.sendDocInManualRoute::::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        
	        int retVal=readSecureLinkUrl(session.room, sid,  secureEmailUrlVector);
			System.out.println("secureEmailUrlVector after fetching from AdminWise :::: "+secureEmailUrlVector);
			
			String secureurl="";
			if((secureEmailUrlVector!=null)&&(secureEmailUrlVector.size()>0)) {
				secureurl=secureEmailUrlVector.get(0).toString();
			}
			System.out.println("secureurl after fetching from AdminWise :::: "+secureurl);
			return sendDocInManualRoute(sid, docId, docName, routeId, routeName, taskSequence, signId, secureurl);
			/*------------------------End of the CV2019 changes--------------------------------------*/
	    }
	    
	    /*public int sendDocInManualRouteWS(int sid, int docId, String docName, int routeId, String routeName, int taskSequence, int signId){
	    	System.out.println();
	    	System.out.println();
	    	
	    	Session session = getSession(sid);
	    	Vector secureEmailUrlVector = new Vector();
	    	VWClient.printToConsole("sessionId in VWClient.sendDocInManualRoute::::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	    	Vector vTaskInfo = new Vector();
	           	try
	            {
	    		Vector ret = vws.getRouteTaskInfo(session.room, sid, routeId, taskSequence, vTaskInfo);
	    		vTaskInfo.addAll(vectorToRouteTask(ret));
	    		if(vTaskInfo!=null&& vTaskInfo.size()>0)
	    		{
		    		RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(0);
		    		int routeTaskId = routeTaskInfo.getRouteTaskId();
		    		int TaskSeq = routeTaskInfo.getTaskSequence();
		    		Vector vRouteUsers = new Vector();
		    		ret = vws.getRouteUsers(session.room, sid, routeTaskId, vRouteUsers, docId);
		    		ArrayList listRouteUsers = vectorToRouteUsersArrayList(ret);
		    		if(listRouteUsers!=null && listRouteUsers.size()>0){
	    				for(int k=0; k<listRouteUsers.size(); k++)
	    				{
	    					RouteUsers routeUser = (RouteUsers)listRouteUsers.get(k);
	    					int routeUserId =routeUser.getRouteUserId();
	    					String groupName = routeUser.getGroupName();
	    					String userName = "";
	    					//Manual route will be called only by Client and not AdminWise, so sending empty string for userName
	    					vws.setRouteHistory(session.room, sid, docId, routeId, TaskSeq, routeUserId, "", userName, userName, signId, 0, groupName);//routeMasterId = 0
	    					int retVal=readSecureLinkUrl(session.room, sid,  secureEmailUrlVector);
	    					System.out.println("secureEmailUrlVector after fetching from AdminWise :::: "+secureEmailUrlVector);
	    					
	    					String secureurl="";
	    					if((secureEmailUrlVector!=null)&&(secureEmailUrlVector.size()>0)) {
	    						secureurl=secureEmailUrlVector.get(0).toString();
	    					}
	    					System.out.println("secureurl after fetching from AdminWise :::: "+secureurl);
	    					try{
	    						System.out.println("routeUser.getSendEmail() sendDocInManualRouteWS ::::  "+routeUser.getSendEmail());
	    						if(routeUser.getSendEmail() == 1){
	    							vws.sendMailWithAttachmentWS(session.room, session.server, sid, routeUser.getUserName(), docName, routeName, docId, new Vector(),
	    									null, Route_Status_Pending, null, null, "", secureurl);
	    						}
	    					}catch(Exception ex){
	    						
	    					}
	    				}
		    		}
	    		}
	            }catch(Exception e){
	            	 return Error;
	            }
	    	return NoError;
	    }*/
	    
	    /**
	     * These methods are created to send mail to task user from new web access on accept and reject of document
	     * @author apurba.m
	     * @param sid
	     * @param routeMaster
	     * @param status
	     * @param comments
	     * @param selectedTaskSeq
	     * @param notificationReq
	     * @param signId
	     * @return
	     */
	    public int moveDocOnApproveRejectWS(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq,String clientType){
	    	return moveDocOnApproveRejectWS(sid,  routeMaster, status, comments, selectedTaskSeq, true, 0,clientType);
	    }
	    public int moveDocOnApproveRejectWS(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq){
	    	return moveDocOnApproveRejectWS(sid,  routeMaster, status, comments, selectedTaskSeq, true, 0,"");
	    }
	    public int moveDocOnApproveRejectWS(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq, boolean notificationReq, int signId,String clientType)
	    {
	    	/*CV2019 changes - Modified by - vanitha***/
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in moveDocOnApproveRejectWS ::::"+sid);
	    	
	    	Vector secureEmailUrlVector = new Vector();
	    	
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	
	    	int retVal=readSecureLinkUrl(session.room, sid,  secureEmailUrlVector);
			System.out.println("secureEmailUrlVector after fetching from AdminWise moveDocOnApproveRejectWS :::: "+secureEmailUrlVector);
			
			String secureurl = "";
			if((secureEmailUrlVector!=null)&&(secureEmailUrlVector.size()>0)) {
				secureurl=secureEmailUrlVector.get(0).toString();
			}
			System.out.println("secureurl after fetching from AdminWise moveDocOnApproveRejectWS :::: "+secureurl);
	    	return moveDocOnApproveReject(sid,  routeMaster, status, comments, selectedTaskSeq, notificationReq, signId, 0, clientType, secureurl);
	    	/*------------------------End of the CV2019 changes--------------------------------------*/
	    }
	    /*public int moveDocOnApproveRejectWS(int sid,  RouteMasterInfo routeMaster, String status, String comments, int selectedTaskSeq, boolean notificationReq, int signId,String clientType)
	    {
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in moveDocOnApproveRejectWS ::::"+sid);
	    	
	    	Vector secureEmailUrlVector = new Vector();
	    	
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		String userName = "";
	    		if(type == Client.Adm_Client_Type)
	    			userName = session.user;
	    		//printToConsole(" Before update routehistory");      
	    		if(drsValidateTaskDocumentVwctor!=null&&!drsValidateTaskDocumentVwctor.isEmpty()&&drsValidateTaskDocumentVwctor.size()>0&&((String) drsValidateTaskDocumentVwctor.get(0)).trim().equalsIgnoreCase("-1") && userSelectedValue!=0){
	    			drsValidateTaskDocumentVwctor =new Vector();
	    			drsValidateTaskDocument(sid, routeMaster.getDocId(), routeMaster.getRouteId(),2,String.valueOf(userSelectedValue), drsValidateTaskDocumentVwctor);
	    			if(drsValidateTaskDocumentVwctor!=null&&!drsValidateTaskDocumentVwctor.isEmpty()&&drsValidateTaskDocumentVwctor.size()>0 ){
	    				String msg = drsValidateTaskDocumentVwctor.get(0).toString();
	    				JOptionPane.showMessageDialog(null,msg);
	    				VWMessage.showMessage(msg);
	    				return 0;
	    			}
	    		}
	    		vws.updateRouteHistory(session.room, sid, routeMaster.getDocId(), routeMaster.getRouteId(), 
	    				routeMaster.getRouteUserId(), status, comments, routeMaster.getStatus(), routeMaster.getRouteMasterId(),routeMaster.getRouteHistoryId(), userName, userName, signId);

	    		//Added by Srikanth to get the email originator status for given routeid.  
	    		Vector getRouteInfoVec = new Vector();
	    		getRouteInfo(sid, routeMaster.getRouteId(), getRouteInfoVec);
	    		RouteInfo routeInfo = (RouteInfo) getRouteInfoVec.get(0);

	    		int retVal=readSecureLinkUrl(session.room, sid,  secureEmailUrlVector);
				System.out.println("secureEmailUrlVector after fetching from AdminWise moveDocOnApproveRejectWS :::: "+secureEmailUrlVector);
				
				String secureurl="";
				if((secureEmailUrlVector!=null)&&(secureEmailUrlVector.size()>0)) {
					secureurl=secureEmailUrlVector.get(0).toString();
				}
				System.out.println("secureurl after fetching from AdminWise moveDocOnApproveRejectWS :::: "+secureurl);

	    		Vector vRouteTaskInfo = new Vector();
	    		int currentTaskSequence = routeMaster.getLevelSeq();
	    		//printToConsole(" currentTaskSequence "+currentTaskSequence);
	    		Vector ret = vws.getRouteTaskInfo(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, vRouteTaskInfo);
	    		vRouteTaskInfo.addAll(vectorToRouteTask(ret));
	    		if(vRouteTaskInfo!=null && vRouteTaskInfo.size()>0){
	    			RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vRouteTaskInfo.get(0);
	    			int minApprovalCount = vws.getTaskApproversCount(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId());
	    			//printToConsole(" minApprovalCount "+minApprovalCount);
	    			int nextRouteTaskSeq = -1;
	    			if(status.equalsIgnoreCase(Route_Status_Approve))
	    			{
	    				int ApprrovedCount = vws.getApprovedCountForTask(session.room, sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId(),routeMaster.getRouteUserId());
	    				//printToConsole(" ApprrovedCount "+ApprrovedCount);
	    				//Code modified for task escalation enhancement
	    				if(minApprovalCount <= ApprrovedCount){
	    					int routeTaskCount = vws.getRouteTaskCount(session.room, sid, routeMaster.getRouteId());
	    					nextRouteTaskSeq = currentTaskSequence+1;
	    					//printToConsole(" routeTaskCount="+routeTaskCount+" nextRouteTaskSeq="+nextRouteTaskSeq);
	    					if(nextRouteTaskSeq <= routeTaskCount){
	    						vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnApprove);
	    						moveDocToAnotherTaskWS(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
	    								routeMaster.getRouteName(), routeMaster.getDocName(), status, secureurl);
	    					}else{
	    						// Need to update tasks completed status, so final action will takes place.
	    						vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence,Route_Status_OnApproveOnLastTask);
	    						vws.updateRouteCompletedDocs(session.room, sid, routeMaster.getRouteMasterId(), "TASK");
	    						if (routeInfo.getEmailOriginator()==1) {
	    							vws.sendMailWithAttachmentWS(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), 
	    									routeMaster.getRouteName(), routeMaster.getDocId(), new Vector(), null, status, null, null, "", secureurl);
	    						}
	    					}
	    				}
	    			}else if(status.equalsIgnoreCase(Route_Status_Reject)){
	    				if(selectedTaskSeq == 0){// moving to originator
	    					vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnReject);
	    					nextRouteTaskSeq = 0;
	    					moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(), 
	    							routeMaster.getRouteName(), routeMaster.getDocName(), status);
	    					//sending mail to other users of route
	    					notificationOnReject(sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
	    							routeMaster.getRouteName(), routeMaster.getDocName(), status,clientType);
	    					//sending mail to originator
	    					vws.sendMailWithAttachmentWS(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), routeMaster.getRouteName(),
	    							routeMaster.getDocId(), new Vector(), null, status, null, null, "", secureurl);
	    				}else if(selectedTaskSeq == -2){ // End route selected from adminwise reject task drop down or from Client ToDoList
	    					vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnRejectOnLastTask);
	    					//vws.updateRouteCompletedDocs(session.room, sid, routeMaster.getRouteMasterId(), "TASK");
	    					/**Implementation change for Reject criteria->User Selects task / Select task from UI-> End workflow should not take final action. 
	    					 * As requested by Rajesh after CV8.3.3
	    					 * Request Received Date:-06-Nov-2015
	    					 */        				
	    					/*nextRouteTaskSeq = 0;
	    					moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(), 
	    							routeMaster.getRouteName(), routeMaster.getDocName(), status);
	    					vws.sendMailWithAttachmentWS(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), 
	    							routeMaster.getRouteName(), routeMaster.getDocId(), new Vector(), null, status, null, null, "", secureurl);
	    				}else if(selectedTaskSeq > 0){
	    					nextRouteTaskSeq = selectedTaskSeq;
	    					vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnReject);
	    					moveDocToAnotherTask(sid, routeMaster.getRouteId(), nextRouteTaskSeq, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
	    							routeMaster.getRouteName(), routeMaster.getDocName(), status);
	    				}
	    			}else if(status.equalsIgnoreCase(Route_Status_Review)){
	    			}else if(status.equalsIgnoreCase(Route_Status_End)){ 
	    				vws.updateStatusOfOutOfTskUsrs(session.room, sid, routeMaster.getRouteMasterId(), currentTaskSequence, Route_Status_OnEnd);
	    				//sending mail to other users of route
	    				if (notificationReq){
	    					notificationOnReject(sid, routeMaster.getRouteId(), currentTaskSequence, routeMaster.getDocId(), routeMaster.getRouteMasterId(),
	    							routeMaster.getRouteName(), routeMaster.getDocName(), status,"");
	    					//sending mail to originator
	    					vws.sendMailWithAttachmentWS(session.room, session.server, sid, routeMaster.getRouteInitiatedBy(), routeMaster.getDocName(), 
	    							routeMaster.getRouteName(), routeMaster.getDocId(), new Vector(), null, status, null, null, "", secureurl);
	    				}
	    			}
	    		}
	    		return NoError;
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}      
	    }*/
	    
	    private int moveDocToAnotherTaskWS(int sid, int routeId, int routeTaskSeq, int docId, int routeMasterId, 
	    		String routeName, String docName, String status, String openDocUrl){
	    	return moveDocToAnotherTaskWS(sid, routeId, routeTaskSeq, docId, routeMasterId, routeName, docName, status, 0, openDocUrl);
	    }

	    private int moveDocToAnotherTaskWS(int sid, int routeId, int routeTaskSeq, int docId, int routeMasterId, 
	    		String routeName, String docName, String status, int signId, String openDocUrl){
	    	/*CV2019 changes - Modified by - vanitha***/
	    	return moveDocToAnotherTask(sid, routeId, routeTaskSeq, docId, routeMasterId, 
	        		routeName, docName, status, signId, openDocUrl);
	    	/*------------------------End of the CV2019 changes--------------------------------------*/
	    }
	    	
	    /*private int moveDocToAnotherTaskWS(int sid, int routeId, int routeTaskSeq, int docId, int routeMasterId, 
	    		String routeName, String docName, String status, int signId, String openDocUrl){
	    	return moveDocToAnotherTask(sid, routeId, routeTaskSeq, docId, routeMasterId, 
	        		routeName, docName, status, signId, openDocUrl);
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in moveDocToAnotherTask::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		String userName = "";
	    		if(routeTaskSeq>0){
	    			Vector vTaskInfo = new Vector();
	    			Vector ret = vws.getRouteTaskInfo(session.room, sid, routeId, routeTaskSeq, vTaskInfo);
	    			vTaskInfo.addAll(vectorToRouteTask(ret));
	    			// There is no need to call setRouteHistory and send mail if the status is end route
	    			if(vTaskInfo!=null&& vTaskInfo.size()>0 && !status.equalsIgnoreCase(Route_Status_End))
	    			{
	    				RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(0);
	    				int routeTaskId = routeTaskInfo.getRouteTaskId();
	    				Vector vRouteUser = new Vector();
	    				ret = vws.getRouteUsers(session.room, sid, routeTaskId, vRouteUser, docId);
	    				ArrayList listRouteUsers = vectorToRouteUsersArrayList(ret);
	   					for(int k=0; k<listRouteUsers.size(); k++)
	    				{
	    					RouteUsers routeUser = (RouteUsers)listRouteUsers.get(k);
	    					int routeUserId =routeUser.getRouteUserId();
	    					String groupName = routeUser.getGroupName();
	    					
	    					if (type == Client.Adm_Client_Type){
	    						if(isAdmin(sid)==1){
	    							userName = session.user;
	    						}
	    					}
	    					else if (type==Client.Web_Client_Type || type== Client.NWeb_Client_Type){
	    						userName = "";
	    					}

	    					vws.setRouteHistory(session.room, sid, docId, routeId, routeTaskInfo.getTaskSequence(), routeUserId, "", userName, userName, signId, routeMasterId, groupName);
	    					
	    					try{
	    						if(routeUser.getSendEmail() == 1)
	    							vws.sendMailWithAttachmentWS(session.room, session.server, sid, routeUser.getUserName(), docName, routeName, 
	    									docId, new Vector(), null, Route_Status_Pending, null, null, "", openDocUrl);
	    					}catch(Exception ex){
	    					}
	    				}
	    			}
	    		}else if(routeTaskSeq == 0){// move to originator on Reject
	    			if (type == Client.Adm_Client_Type){
						if(isAdmin(sid)==1){
							userName = session.user;
						}
					}
	    			vws.setRouteHistory(session.room, sid, docId, routeId, routeTaskSeq, 0, "", userName, userName, signId, routeMasterId); //routeUserId is 0 here
	    		}
	    	}catch(Exception e){
	    		return Error;
	    	}
	    	return NoError;
	    	
	    }*/
	    /**
	     * Method is used for CVWeb  to encrypt vw file from java side.
	     * @param aFile
	     * @param destinationPath
	     * @param resFileVect
	     * @return
	     */
	    public int encryptVWFile(String aFile, String destinationPath,Vector resFileVect) {
	    	try{
	    		CryptographyUtils utils = new CryptographyUtils();
	    		utils.encryptFile(aFile, destinationPath);
	    		utils = null;
	    		resFileVect.add(destinationPath);
	    	} catch(Exception ex){
	    		System.out.println("EXCEPTION while encryptVWFile ::::  "+ex.getMessage());
	    		VWClient.printToConsole("EXCEPTION while encryptVWFile ::::  "+ex.getMessage());
	    		return Error;
	    	}
	    	return NoError; 
	    }
	    
	    
	    /**
		 *  Method is used for CVWeb  to encrypt vw file from java side.
		 * @param aFile
		 * @param destinationPath
		 * @param index
		 * @return
		 */
	    public int encryptVWFile(String aFile, String destinationPath) {
	    	try{
	    		VWClient.printToConsole("encryptVWFile................");
	    		CryptographyUtils utils = new CryptographyUtils();
	    		VWClient.printToConsole("after creating CryptographyUtils object.....");
	    		utils.encryptFile(aFile, destinationPath);
	    		utils = null;
	    	} catch(Exception ex){
	    		System.out.println("EXCEPTION while encryptVWFile ::::  "+ex.getMessage());
	    		VWClient.printToConsole("EXCEPTION while encryptVWFile ::::  "+ex);
	    		return Error;
	    	}
	    	return NoError;
	    }
	    
	    
	    /**
		 * Method to encrypty tw file
		 * @param aFile
		 * @param destinationPath
		 * @param index
		 * @return
		 */
	    public int encryptTWFile (String aFile, String destinationPath, Vector resThumbnailFileVect) {
	    	try {
	    		CryptographyUtils utils = new CryptographyUtils();
	    		utils.encryptTWFile(aFile, destinationPath);
	    		utils = null;
	    		if (resThumbnailFileVect != null) {
	    			resThumbnailFileVect.add(destinationPath);
	    		}
	    	} catch (Exception ex) {
	    		System.out.println("EXCEPTION while encryptTWFile ::::  "+ex.getMessage());
	    		VWClient.printToConsole("EXCEPTION while encryptTWFile ::::  "+ex.getMessage());
	    		return Error;
	    	}
	    	return NoError;
	    }
	    
	    public int decryptFile(String fileIn, String fileOut) {
	    	try {
	    		CryptographyUtils utils = new CryptographyUtils();
	    		utils.decryptFile(fileIn, fileOut);
	    	} catch (Exception ex) {
	    		System.out.println("EXCEPTION while decryptFile ::::  "+ex.getMessage());
	    		VWClient.printToConsole("EXCEPTION while decryptFile ::::  "+ex.getMessage());
	    		return Error;
	    	}
	    	return NoError;
	    }
	    
	    public int decryptTWFile(String fileIn, String fileOut) {
	    	try {
	    		CryptographyUtils utils = new CryptographyUtils();
	    		utils.decryptTWFile(fileIn, fileOut);
	    	} catch (Exception ex) {
	    		System.out.println("EXCEPTION while decryptTWFile ::::  "+ex.getMessage());
	    		VWClient.printToConsole("EXCEPTION while decryptTWFile ::::  "+ex.getMessage());
	    		return Error;
	    	}
	    	return NoError;
	    }
	    
	    /** 
	     * Implemented for Enhanced Web Access
	     * Whether a session is active or not
	     * @author apurba.m
	     */
	    public int assertSessionWS(int sid)
	    {
	    	Session session = getSession(sid);
	    	System.out.println("sessionId in assertSessionWS :::: "+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		return (vws.assertSessionWS(session.room, sid))? 1:0;

	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}  
	    }
	    
	    
	    public int docDeleteAllPrevComments(int sid, int documentId, String userName){
	    	Session session = getSession(sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		return (vws.docDeleteAllPrevComments(session.room, sid, documentId, userName));

	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}  
	    }
	    
	    /**
	     * Created for EWA to sort page info vector
	     * @author apurba.m
	     * @param sessionId
	     * @param documentId
	     * @param pageInfoVecElem
	     * @return
	     */
	    public int setPagesInfo(int sessionId, int documentId, String pageInfoVecElem){
	    	Session session = getSession(sessionId);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		return (vws.setPagesInfo(session.room, sessionId, documentId, pageInfoVecElem));

	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}  
	    }
	    
	    /**
	     * Created for EWA to get sorted page info vector
	     * @author apurba.m
	     * @param sessionId
	     * @param documentId
	     * @param pageInfoVecElem
	     * @return
	     */
	    public int getPagesInfo(int sessionId, int documentId, Vector sortedPageInfoVec){
	    	Session session = getSession(sessionId);
	    	Vector ret = new Vector();
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		ret = vws.getPagesInfo(session.room, sessionId, documentId, sortedPageInfoVec);
	    		System.out.println("ret inside VWClient.getPagesInfo :::: "+ret);
	    		if(ret != null && ret.size() > 0){
	    			sortedPageInfoVec.addAll(ret);
	    		}
	    		return NoError;
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}  
	    }
	    
	    /**
	     * Created for EWA to delete sort page info vector
	     * @author apurba.m
	     * @param sessionId
	     * @param documentId
	     * @param pageInfoVecElem
	     * @return
	     */
	    public int deletetPagesInfo(int sessionId, int documentId){
	    	Session session = getSession(sessionId);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		return (vws.deletetPagesInfo(session.room, sessionId, documentId));

	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}  
	    }
	    
	    /**
	     * CV2019 merges from SIDBI line
	     * @param sid
	     * @param param
	     * @param result
	     * @return
	     */
	    public int getSettingsInfo(int sid, String param, Vector result){
	    	VWClient.printToConsole("getSettingsInfo sid ::::  "+sid);
	    	Session session = getSession(sid);
	    	if(session==null){
	    		return invalidSessionId;
	    	}
	    	VWS vws = getServer(session.server);
	    	if (vws == null){
	    		return ServerNotFound;
	    	}
	    	try
	    	{
	    		Vector ret = vws.getSettingsInfo(session.room, sid, param, result);
	    		if(ret != null && ret.size() > 0){
	    			result.addAll(ret);
	    		}
	    		return NoError;
	    	}
	    	catch(Exception e)
	    	{
	    		VWClient.printToConsole("EXCEPTION inside getSettingsInfo :::: "+e.getMessage());
	    		return Error;
	    	}  
	    }
	   
	    /**
	     * This method is used to generate summary report html file for DTC export
	     * @param sid
	     * @param doc
	     * @return
	     */
		public int generateRouteSummaryReport(int sid, Document doc) {
			int flag = NoError;
			VWClient.printToConsole("getRouteSummary sid ::::  " + sid);
			Session session = getSession(sid);
			if (session == null) {
				return invalidSessionId;
			}
			VWS vws = getServer(session.server);
			if (vws == null) {
				return ServerNotFound;
			}
			try {
				File hFile = new File(getCacheFolder(sid) + Util.pathSep + doc.getId() + Util.pathSep + "WFS.html");
				VWClient.printToConsole("generateRouteSummaryReport document id ::::  " + doc.getId() + "Summary Report File Path :"+hFile.getPath());
				ArrayList<String> htmlContents = generateRouteSummaryHTML(vws, sid, session, doc.getId());
				VWClient.printToConsole("htmlContents ::::  " + htmlContents);
				if (htmlContents != null) {
					VWCUtil.writeArrayListToFile(htmlContents, hFile.getPath(), "", "UTF-8");
				}
				VWClient.printToConsole("Is summary html created :" + hFile.exists());
			} catch (Exception e) {
				VWClient.printToConsole("EXCEPTION inside generateRouteSummary :::: " + e.getMessage());
				flag = Error;
			}
			return flag;
		}
		/**
		 * CV2019 enhancement
		 * This method is used to generate summary report html file for DTC export
		 * @param docId
		 * @param mode
		 * @param path
		 * @return
		 */
		public ArrayList<String> generateRouteSummaryHTML(VWS vws, int sessionId, Session session, int docId) {
			VWClient.printToConsole("inside generateRouteSummaryHTML.......");
			ArrayList<String> htmlContents = null;
			int mode = 0;
			try {
				/*Vector documentInRoute = vws.getDocumentsInRoute(roomName, sessionId, docId);
				VWClient.printToConsole("documentInRoute value in generateRouteSummaryHTML ......."+documentInRoute);
				if(documentInRoute != null && documentInRoute.size() > 0 && documentInRoute.get(0).equals("1")){
					mode = 1;
		        }//Else if condition added for enhancement  Sow :-2117_1155 Clearly identify documents that are in workflow in a folder by color in DTC.
		        else if(documentInRoute != null && documentInRoute.size() > 0 && documentInRoute.get(0).equals("2")){
		        	mode = 2;
		        } else if(documentInRoute != null && documentInRoute.size() > 0 && documentInRoute.get(0).equals("3")){
		        	mode = 1;
		        }*/
				mode = 1;
				VWClient.printToConsole("getRouteSummary param docId: "+docId+", mode: "+mode);
				boolean isSidbiDocument = getSIDBIDocumentTypeFlag(sessionId, docId);
				Vector<String> routeDetails = null;
				if (!isSidbiDocument) {
					routeDetails = vws.getRouteSummary(session.room, sessionId, docId, mode);
					VWClient.printToConsole("routeDetails......."+routeDetails);
					Vector<String> indices = vws.getDocumentProperties(session.room, sessionId, docId, new Vector());
					VWClient.printToConsole("indices......."+indices);
					VWRouteSummary summary = new VWRouteSummary();
					VWClient.printToConsole("Ater creating instance for VWRouteSummary");
					htmlContents = summary.generateRouteSummaryHTMLContent(routeDetails, indices);
					VWClient.printToConsole("final summary......."+htmlContents);
					summary = null;
				} else {
					routeDetails = vws.getRouteNewSummary(session.room, sessionId, docId, mode);
					VWClient.printToConsole("routeDetails......."+routeDetails);
					VWNewRouteSummary routeSummaryDlg = new VWNewRouteSummary();
					htmlContents = routeSummaryDlg.generateNewSummaryReportHtmlContent(routeDetails);
					VWClient.printToConsole("final summary......."+htmlContents);
				}
			} catch (Exception e) {
				VWClient.printToConsole("Exception in generateRouteSummaryHTML :"+e.getMessage());				
			}
			return htmlContents;
		}
					
	    /**
	     * This method is used to apply java decryption for all.zip files
	     * @param zipFileLocation
	     * @param tempFolderLocation
	     * @return
	     */
	    public boolean applyJavaDecryptionForCheckoutDocuments(String zipFileLocation, String tempFolderLocation) {
	    	boolean isDecrypted = false;
	    	VWClient.printToConsole("inside decryptAllDotZipFile......");
	    	//System.out.println("inside decryptAllDotZipFile......");
	    	File sourceFileDir = null;
	    	try {
	    		//fileIn - extracted file from all.zip, fileOut - java decrypted files
	    		String fileIn = null, fileOut = null;
	    		String fileName = null, fileExtension = null, fileDecryptionLocation = null;
		    	VWClient.printToConsole("zipFileLocation........"+zipFileLocation);
		    	//System.out.println("zipFileLocation........"+zipFileLocation);
		    	File zipFile = new File(zipFileLocation);
			    if (zipFile.exists()) {
			    	String unZipLocation = tempFolderLocation+File.separatorChar+"ExtractedFiles";
			    	Util.unzip(zipFileLocation, unZipLocation);
			    	fileDecryptionLocation = tempFolderLocation;
			    	VWClient.printToConsole("File Decryption Location........"+fileDecryptionLocation);
			    	//System.out.println("File Decryption Location........"+fileDecryptionLocation);
			    	
			    	sourceFileDir = new File(unZipLocation);
			    	File[] listOfFiles = sourceFileDir.listFiles();
			    	VWClient.printToConsole("listOfFiles....."+listOfFiles);
			    	System.out.println("listOfFiles....."+listOfFiles);
			    	if (listOfFiles != null && listOfFiles.length > 0) {
			    		File[] decryptedFiles = new File[listOfFiles.length];
				    	File javaDecryptionCheck = new File (unZipLocation +File.separatorChar+"cripjed.txt");
				    	VWClient.printToConsole("javaDecryptionCheck....."+javaDecryptionCheck.exists());	
				    	//System.out.println("javaDecryptionCheck....."+javaDecryptionCheck.exists());
				    	if (javaDecryptionCheck.exists()) {
				    		VWClient.printToConsole("all.zip files size........"+listOfFiles.length);
				    		//System.out.println("all.zip files size........"+listOfFiles.length);
				    		try {
						    	for (int i = 0; i < listOfFiles.length; i++) {
						    		  if (listOfFiles[i].isFile()) {
						    			  fileName = listOfFiles[i].getName();
						    			  fileExtension = fileName.substring(fileName.indexOf(".")+1, fileName.length());				    			  
						    			  VWClient.printToConsole("File " + fileName);
						    			  VWClient.printToConsole("File extension : "+fileExtension);
						    			  //System.out.println("File " + fileName);
						    			  //System.out.println("File extension : "+fileExtension);
						    		      fileIn = unZipLocation+File.separatorChar+fileName;
						    		      VWClient.printToConsole("fileIn" + fileIn);
						    		      //System.out.println("fileIn" + fileIn);
					    		    	  fileOut = fileDecryptionLocation+File.separatorChar+fileName;
					    		    	  VWClient.printToConsole("fileOut " + fileOut);
					    		    	  //System.out.println("fileOut " + fileOut);
					    		    	  if (fileExtension.equals("vw")) {
					    		    		  decryptFile(fileIn, fileOut);
					    		    	 } else if (fileExtension.equals("tw")) {			    		    		  
					    		    		  decryptTWFile(fileIn, fileOut);					    		    		  
					    		    	  } else {
					    		    		  //System.out.println("Other files");
					    		    		  VWCUtil.CopyFile(new File(fileIn), new File(fileOut));
					    		    	  }
					    		    	  decryptedFiles[i] = new File(fileOut);
					    		    	  
						    		  } 
						    	}
						    	Util.deleteDirectory(sourceFileDir);
						    	VWClient.printToConsole("Unzipped folder deleted");
					    		//System.out.println("Unzipped folder deleted");
						    	File zipFileName = new File(fileDecryptionLocation+File.separatorChar+"all.zip");
						    	ZipUtil.compress(zipFileName, decryptedFiles);
						    	isDecrypted = true;
						    	
				    		} catch (Exception e) {
				    			VWClient.printToConsole("EXCEPTION while decryptAllDotZipFile 2 ::::  "+e.getMessage());
				    			//System.out.println("EXCEPTION while decryptAllDotZipFile 2 ::::  "+e.getMessage());
				    			isDecrypted = false;
				    		}
				    		
				    	}
			    	}
		    	}
	    	}catch (Exception e) {
	    		VWClient.printToConsole("EXCEPTION while decryptAllDotZipFile 1 ::::  "+e.getMessage());
	    		//System.out.println("EXCEPTION while decryptAllDotZipFile 1 ::::  "+e.getMessage());
	    		isDecrypted = false;
	    	}
	    	if (sourceFileDir != null && sourceFileDir.exists()) {
	    		Util.deleteDirectory(sourceFileDir);
    		}
	    	return isDecrypted;
	    }
	    
	    /**
	     * This method is used to apply java decryption and dll encryption for all.zip files
	     * @param zipFileLocation
	     * @param tempFolderLocation
	     * @return
	     */
	    public boolean encryptAllDotZipFile(String zipFileLocation, String tempFolderLocation) {
	    	boolean isEncrypted = false;
	    	VWClient.printToConsole("inside decryptAllDotZipFile......");
	    	File sourceFileDir = null, decryptedFileDir = null;
	    	try {
	    		//fileIn - extracted file from all.zip, fileOut - java decrypted file, dtcFileOut - dtc encrypted files
	    		String fileIn = null, fileOut = null, dtcFileOut = null;
	    		String fileName = null, fileExtension = null, fileDecryptionLocation = null;
		    	VWClient.printToConsole("zipFileLocation........"+zipFileLocation);
		    	File zipFile = new File(zipFileLocation);
			    if (zipFile.exists()) {
			    	String unZipLocation = tempFolderLocation+File.separatorChar+"ExtractedFiles";
			    	Util.unzip(zipFileLocation, unZipLocation);
			    	fileDecryptionLocation = tempFolderLocation+File.separatorChar+"DecryptedFiles";
			    	VWClient.printToConsole("File Decryption Location........"+fileDecryptionLocation);
			    	decryptedFileDir = new File(fileDecryptionLocation);
			    	if (!decryptedFileDir.exists()) {
			    		decryptedFileDir.mkdir();
		    		}	

			    	sourceFileDir = new File(unZipLocation);
			    	File[] listOfFiles = sourceFileDir.listFiles();
			    	VWClient.printToConsole("listOfFiles....."+listOfFiles);
			    	if (listOfFiles != null && listOfFiles.length > 0) {
			    		File[] decryptedFiles = new File[listOfFiles.length];
				    	File javaDecryptionCheck = new File (unZipLocation +File.separatorChar+"cripjed.txt");
				    	VWClient.printToConsole("javaDecryptionCheck....."+javaDecryptionCheck.exists());	
				    	if (javaDecryptionCheck.exists()) {
				    		VWClient.printToConsole("all.zip files size........"+listOfFiles.length);
				    		try {
						    	for (int i = 0; i < listOfFiles.length; i++) {
						    		  if (listOfFiles[i].isFile()) {
						    			  fileName = listOfFiles[i].getName();
						    			  fileExtension = fileName.substring(fileName.indexOf(".")+1, fileName.length());				    			  
						    			  VWClient.printToConsole("File " + fileName);
						    			  VWClient.printToConsole("File extension : "+fileExtension);
						    			  fileIn = unZipLocation+File.separatorChar+fileName;
						    		      VWClient.printToConsole("fileIn" + fileIn);
						    		      fileOut = fileDecryptionLocation+File.separatorChar+(fileName.substring(0, fileName.indexOf(".")+1)+"tmp");
					    		    	  dtcFileOut = fileDecryptionLocation+File.separatorChar+fileName;
					    		    	  VWClient.printToConsole("fileOut " + fileOut);
					    		    	  VWClient.printToConsole("dtcFileOut " + dtcFileOut);
					    		    	  if (fileExtension.equals("vw")) {
					    		    		  decryptFile(fileIn, fileOut);
					    		    		  VWClient.printToConsole("Before calling sendDecryptedFileForCheckOutDoc ");
					    		    		  sendDecryptedFileForDllEncryption(dtcFileOut, fileOut);
					    		    		  VWClient.printToConsole("After calling sendDecryptedFileForCheckOutDoc ");
					    		    	  } else if (fileExtension.equals("tw")) {			    		    		  
					    		    		  decryptTWFile(fileIn, fileOut);
					    		    		  VWClient.printToConsole("Before calling sendDecryptedFileForCheckOutDoc ");
					    		    		  sendDecryptedFileForDllEncryption(dtcFileOut, fileOut);
					    		    		  VWClient.printToConsole("After calling sendDecryptedFileForCheckOutDoc ");	
					    		    	  } else {
					    		    		  VWCUtil.CopyFile(new File(fileIn), new File(dtcFileOut));
					    		    	  }
					    		    	  new File(fileOut).delete();
					    		    	  decryptedFiles[i] = new File(dtcFileOut);
					    		    	  isEncrypted = true;
						    		  } 
						    	}
						    	Util.deleteDirectory(sourceFileDir);
					    		File zipFileName = new File(tempFolderLocation+File.separatorChar+"all.zip");
						    	ZipUtil.compress(zipFileName, decryptedFiles);
						    	Util.deleteDirectory(decryptedFileDir);
						    } catch (Exception e) {
				    			VWClient.printToConsole("EXCEPTION while decryptAllDotZipFile ::::  "+e.getMessage());
				    			isEncrypted = false;
				    		}
				    		
				    	}
			    	}
		    	}
	    	}catch (Exception e) {
	    		VWClient.printToConsole("EXCEPTION while decryptAllDotZipFile ::::  "+e.getMessage());
	    		isEncrypted = false;
	    	}
	    	if (sourceFileDir != null && sourceFileDir.exists()) {
	    		Util.deleteDirectory(sourceFileDir);
    		}
	    	if (decryptedFileDir != null && decryptedFileDir.exists()) {
	    		Util.deleteDirectory(decryptedFileDir);
	    	}
	    	return isEncrypted;
	    }
	   
	    /**
	     * This method is used to send the java decrypted file for DLL encryption
	     * @param filePath
	     * @return
	     */
	    public int sendDecryptedFileForDllEncryption(String dtcFileOut, String dtcFileIn)
	    {
	    	int returnValue = 0;
	    	try{
	    		returnValue = VWCUtil.sendDecryptedFileForDllEncryption(dtcFileOut, dtcFileIn);
	    	}catch(Exception e){
	    		returnValue = Error;
	    	}
	    	return returnValue;
	    }
	    
	    /**
	     * This method is used to apply java encryption and dll decryption for all.zip files 
	     * @param zipFileLocation
	     */
	   
	    public boolean decryptAllDotZipFile(String zipFileLocation) {
	    	boolean isDecrypted = false;
	    	try {
	    		String fileIn = null, fileOut = null, dtcFileOut = null;
	    		String fileName = null, fileExtension = null;		    		
		    	File zipFile = new File(zipFileLocation);
			    if (zipFile.exists()) {
			    	String unZipLocation = zipFile.getParent()+"\\"+"ExtractedFiles";
			    	Util.unzip(zipFileLocation, unZipLocation);
			    	String encryptFileLocation = zipFile.getParent()+"\\"+"DecryptedFiles";
			    	VWClient.printToConsole("encryptFileLocation........"+encryptFileLocation);
			    	File encryptedFileDir = new File(encryptFileLocation);
			    	if (!encryptedFileDir.exists()) {
			    		encryptedFileDir.mkdir();
		    		}
			    	zipFile.delete();
			    	File sourceFileDir = new File(unZipLocation);
			    	File[] listOfFiles = sourceFileDir.listFiles();
			    	File[] encryptedFiles = new File[listOfFiles.length];
			    	File javaEncryptionCheck = new File (encryptFileLocation + "\\cripjed.txt");
			    	if (javaEncryptionCheck.exists()) {
				    	for (int i = 0; i < listOfFiles.length; i++) {
				    		  if (listOfFiles[i].isFile()) {
				    			  fileName = listOfFiles[i].getName();
				    			  fileExtension = fileName.substring(fileName.indexOf(".")+1, fileName.length());
				    			  VWClient.printToConsole("File " + fileName);
				    			  VWClient.printToConsole("File extension : "+fileExtension);
				    		      fileIn = unZipLocation+"\\"+fileName;
				    		      VWClient.printToConsole("fileIn" + fileIn);
				    		      fileOut = encryptFileLocation+"\\"+(fileName.substring(0, fileName.indexOf(".")+1)+"tmp");
				    		      dtcFileOut = encryptFileLocation+"\\"+fileName;
			    		    	  VWClient.printToConsole("fileOut " + fileOut);
			    		    	  VWClient.printToConsole("dtcFileOut " + dtcFileOut);
			    		    	  if (fileExtension.equals("vw")) {
			    		    		  encryptVWFile(fileIn, fileOut);
			    		    		  VWClient.printToConsole("Before calling sendEncryptedFileForCheckInDoc ");
			    		    		  sendEncryptedFileForDllDecryption(dtcFileOut, fileOut);
			    		    		  VWClient.printToConsole("After calling sendEncryptedFileForCheckInDoc ");
			    		    	  } else if (fileExtension.equals("tw")) {
			    		    		  encryptTWFile(fileIn, fileOut, null);
			    		    		  VWClient.printToConsole("Before calling sendEncryptedFileForCheckInDoc ");
			    		    		  sendEncryptedFileForDllDecryption(dtcFileOut, fileOut);
			    		    		  VWClient.printToConsole("After calling sendEncryptedFileForCheckInDoc ");
			    		    	  } else {
			    		    		  VWCUtil.CopyFile(new File(fileIn), new File(dtcFileOut));
			    		    	 }
			    		    	  new File(fileOut).delete();
			    		    	  encryptedFiles[i] = new File(dtcFileOut);
				    		  } 
				    	}
				    	Util.deleteDirectory(sourceFileDir);
				    	File zipFileName = new File(encryptedFileDir.getParent()+"\\"+"all.zip");
				    	ZipUtil.compress(zipFileName, encryptedFiles);
				    	Util.deleteDirectory(encryptedFileDir);
			    	}
		    	}
	    	}catch (Exception e) {
	    		VWClient.printToConsole("EXCEPTION while encryptAllDotZipFile ::::  "+e.getMessage());
	    	}
	    	return isDecrypted;
	    }
	    
	    /**
	     * This method is used to send the encrypted file for DLL decryption
	     * @param filePath
	     * @return
	     */
	    public int sendEncryptedFileForDllDecryption(String dtcFileOut, String dtcFileIn)
	    {
	    	int returnValue = 0;
	    	try{
	    		returnValue = VWCUtil.sendEncryptedFileForDllDecryption(dtcFileOut, dtcFileIn);
	    	}catch(Exception e){
	    		returnValue = Error;
	    	}
	    	return returnValue;
	    }
	    /**
	     * Created for EWA to set redaction information 
	     * @param sid
	     * @param doc
	     * @param redactions
	     * @return
	     */
	    public int setRedactionsEWA(int sid, Document doc, String redactions) {              
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in setRedactionsEWA :::: "+sid);
	    	System.out.println("sessionId in setRedactionsEWA :::: "+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;

	    	try{        
	    		if(redactions.trim().length() > 0){
	    			vws.setRedactions(session.room, sid, doc.getId(),  redactions);
	    		}
	    	} catch(Exception e) {
	    		return Error;
	    	}
	    	return NoError;
	    }
	    
	    /**
	     * Method is added to insert all pages information of a document
	     * Added by @author apurba.m
	     * @param sid
	     * @param docId
	     * @param oldFileName
	     * @param fileName
	     * @param pagesInformation
	     * @return
	     */
	    public int setPagesInformation(int sid, String docId, String oldFileName, String fileName, String pagesInformation){
	    	Session session = getSession(sid);
	    	System.out.println("sessionId in setPagesInformation :::: "+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	
	    	try {
				vws.setPagesInformation(session.room, sid, docId, oldFileName, fileName, pagesInformation);
			} catch (Exception e) {
				System.out.println("EXCEPTION inside VWClient.setPagesInformation :::: "+e.getMessage());
				return Error;
			}
	    	return NoError;
	    }
	    
	    /**
	     * Method is added to get all pages information of a document
	     * Added by @author apurba.m
	     * @param sid
	     * @param docId
	     * @param oldFileName
	     * @param fileName
	     * @param pagesInformation
	     * @return
	     */
	    public int getDocumentPagesInformation(int sessionId, int documentId, Vector<String> resultPagesInfo){
	    	System.out.println("documentId inside VWClient.getDocumentPagesInformation ::::  "+documentId);
	    	Session session = getSession(sessionId);
	    	Vector<String> ret = new Vector<String>();
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		ret = vws.getDocumentPagesInformation(session.room, sessionId, documentId, resultPagesInfo);
	    		System.out.println("ret inside VWClient.getDocumentPagesInformation :::: "+ret);
	    		if(ret != null && ret.size() > 0){
	    			resultPagesInfo.addAll(ret);
	    		}
	    		return NoError;
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	} 
	    }
	    
	    /*
	     * Method added for sidbi customization.
	     * Modified by madhavan Date :- 26-06-2018
	     */
	    public int getCustomUsers(int sid,int param, Vector creators) 
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getCustomUsers::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = vws.getCustomUsers(session.room, sid, String.valueOf(param), creators);
	            if(ret!=null){
	            	VWClient.printToConsole("Inside ret value if condition");	
	            	creators.addAll(ret);
	            }
	            //getCreators(session.room, sid, creators);
	            //creators.addAll(vectorToCreators(ret));
	            VWClient.printToConsole("Active Error in getCustomUsers--->"+vws.getActiveError());
	          //  return vws.getActiveError();
	            if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    public int setRouteOTPInfo(int sid,int recId,int docId,String userName,String OTP) 
	    {
	    	int retVal=0;
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in SetRouteOTPInfo::"+sid);
	        VWClient.printToConsole("sid ::::::::"+sid+":::recId::"+recId+":::docId:::"+docId+":::userName:::"+userName+":::OTP:::"+OTP);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	        	Vector retVect=new Vector();
	        	if(recId>0){
	        		 addATRecord(vws, session.room, sid, docId, 
		                     VWATEvents.AT_OBJECT_OTPREGENERATE,
		                     VWATEvents.AT_OTP,"OTP resent");	
	        	}
	        	retVal = vws.setRouteOTPInfo(session.room,sid,recId,docId,userName,OTP,retVect);
	        	if(retVect.size()>0&&retVect!=null){
	        		VWClient.printToConsole("retVect.get(0).toString():::::::::"+retVect.get(0).toString());
	        		return Integer.parseInt(retVect.get(0).toString());
	        	}
	            VWClient.printToConsole("Active Error in getCreators--->"+vws.getActiveError());
	            if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    
	    public int sendRouteOTPMail(int sid,int docId,String userName,String docName,String OTP,String loggedUserMailId,String action)
	    {
	    	int retVal=0;
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in sendRouteOTPMail::"+sid);
	        //VWClient.printToConsole("sid ::::::::"+sid+":::+":::docId:::"+docId+":::userName:::"+userName+":::OTP:::"+OTP);
	        VWClient.printToConsole("sid::::"+sid+":::::docId"+docId+":::userName:::"+userName+":::OTP:::"+OTP);
	        VWClient.printToConsole("docName ::"+docName+"loggedUserMailId::::"+loggedUserMailId+":::::action"+action);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	        	Vector retVect=new Vector();
	        	if(loggedUserMailId.equals("")||loggedUserMailId.length()==0){
	        		Vector mailIds=new Vector();
	        		getUserMail(sid, session.user,mailIds) ;
	        		System.out.println("vector mailIds "+mailIds);
	        		if(mailIds!=null&&mailIds.size()>0){
	        			loggedUserMailId=mailIds.get(0).toString();
	        			System.out.println("loggedUserMailId :::"+loggedUserMailId);
	        		}	        
	        	
	        	}
	        	VWClient.printToConsole("Before calling sendRouteOTPMail server call ");
	        	retVal = vws.sendRouteOTPMail(session.room,sid,docId,userName,docName,OTP,loggedUserMailId,action);
	        	VWClient.printToConsole("After calling sendRouteOTPMail server call ");

	        	if(retVect.size()>0&&retVect!=null){
	        		VWClient.printToConsole("retVect.get(0).toString():::::::::"+retVect.get(0).toString());
	        		return Integer.parseInt(retVect.get(0).toString());
	        	}
	            VWClient.printToConsole("Active Error in getCreators--->"+vws.getActiveError());
	            if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	        	VWClient.printToConsole("Exception in sendRouteOTPMail :"+e.getMessage());
	            return Error;
	        }
	    }

	    
	    public int checkNextTaskExist(int sid,int docId,int routeId,String taskName,Vector retVal) 
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in checkNextTaskExist::"+sid);
	        VWClient.printToConsole("sid ::::::::"+sid+":::docId::"+docId+":::routeId:::"+routeId+":::taskName:::"+taskName);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	        	Vector nextTaskVal=new Vector();
	        	nextTaskVal=	vws.checkNextTaskExist(session.room,sid,docId,routeId,taskName);
	        	VWClient.printToConsole("nextTaskVal :::::::"+nextTaskVal);
	        	if(nextTaskVal!=null&&nextTaskVal.size()>0) {
	        		retVal.addAll(nextTaskVal);
	        		VWClient.printToConsole("retVal ::::::::"+retVal);
	        	}
	            if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	        	VWClient.printToConsole("Exception in checkNextTaskExist::::::::"+e.getMessage());
	            return Error;
	        }
	    }
	    
	    
	    public int getBranchInfo(int sid,String flag,String branchCode,Vector branchInfo) 
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getBranchInfo::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	            Vector ret = vws.getBranchInfo(session.room, sid,flag,branchCode,branchInfo);
	            branchInfo.addAll(ret);
	            VWClient.printToConsole("Active Error in getBranchInfo--->"+vws.getActiveError());
	            if (sid>0 && vws.getActiveError()<0 ){
	            	return NoError;
	            } else {
	            	return vws.getActiveError();
	            }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    public int checkWFOTPFlag(int sid,int docId){
	    	int retVal=0;
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in checkWFOTPFlag::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Document doc=new Document(docId);
	    		Vector doctypes=new Vector();
	    		Vector regDoctypes=new Vector();
	    		getDocumentInfo(sid,doc,doctypes);
	    		Document doc1=(Document)doctypes.get(0);
	    		getWorkflowDoctype(sid,regDoctypes);
	    		if(regDoctypes!=null&&regDoctypes.size()>0){
	    		String wfDoctypStr=(String) regDoctypes.get(0);
	    		String[] wfdoctyparray = wfDoctypStr.split(";");
	    		List<String> wfdoctypeList=new ArrayList<String>();
	    		Collections.addAll(wfdoctypeList, wfdoctyparray);
	    		if(wfdoctypeList.contains(doc1.getDocType().getName())){
	    			
	    			VWClient.printToConsole("Inside if of 1");
	    			retVal= 1;
	    		}else{
	    			retVal= 0;
	    		}
	    		}else{
	    			retVal= 0;
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    	VWClient.printToConsole("retvalue in checkwfotp::::"+retVal);
			return retVal;
	    }
	    
	    
	    
	    
	    public int getWorkflowDoctype(int sid, Vector regVal) 
	    {
	    	int retVal=0;
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("getWorkflowDoctype:::::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret = vws.getWorkflowDoctype(session.room, sid, regVal);
	    		VWClient.printToConsole("ret int getWorkflowDoctype:::::::::::"+ret);
	    		//VWClient.printToConsole("regVal in getWorkflowDoctype :::"+regVal);
	    		if(ret!=null&&ret.size()>0){
	    		regVal.addAll(ret);
	    		}
	    		VWClient.printToConsole("regVal in workflow doctype--->"+regVal);
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    
	    public int setEncryptionType(int sid,int docId,int encryptionType,Vector result) throws RemoteException {
	    	int retVal=0;
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("setEncryptionType:::::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		int ret = vws.setEncryptionType(session.room, sid, docId, encryptionType, result);
	    		VWClient.printToConsole("ret int setEncryptionType:::::::::::"+ret);
	    		//VWClient.printToConsole("regVal in getWorkflowDoctype :::"+regVal);
	    		//if(ret!=null&&ret.size()>0){
	    		//regVal.addAll(ret);
	    		
	    		VWClient.printToConsole("regVal in setEncryptionType--->"+ret);
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
		  public int getEncryptionType(int sid,int docId,Vector result) throws RemoteException {
			  VWClient.printToConsole("Inside first line of getEncryptionType :");
		    	int retVal=0;
		    	Session session = getSession(sid);
		    	VWClient.printToConsole("getEncryptionType sid:::::"+sid);
		    	if(session==null) return invalidSessionId;
		    	VWS vws = getServer(session.server);
		    	if (vws == null) return ServerNotFound;
		    	try
		    	{
		    		Vector ret = vws.getEncryptionType(session.room, sid, docId, result);
		    		VWClient.printToConsole("ret int getEncryptionType:::::::::::"+ret);
		    		if(ret!=null&&ret.size()>0){
		    			result.addAll(ret);

		    			VWClient.printToConsole("regVal in getEncryptionType--->"+result);
		    			if (sid>0 && vws.getActiveError()<0 ){
		    				return NoError;
		    			} else {
		    				return vws.getActiveError();
		    			}
		    		}
		    	}
		    	catch(Exception e)
		    	{
		    		return Error;
		    	}
				return NoError;
		    }
	    
	    public int getWARoomCache(int sid){
	    	int retVal=0;
	    	try {
	    		 Session session = getSession(sid);
	 	        VWClient.printToConsole("sessionId in getWARoomCache::"+sid);
	 	        if(session==null) return invalidSessionId;
	 	        VWS vws = getServer(session.server);
	 	        if (vws == null) return ServerNotFound;
	    		boolean retFlag=false;
	    		retFlag=vws.getWARoomCache(sid);
	    		if(retFlag==true){
	    			retVal= 1;
	    		}else{
	    			retVal= 0;
	    		}
	    	}
			catch(Exception ex) {
				return Error;
			}
			return retVal;		
		}
	    
	    public int getNativeWrap(int sid){
	    	int retVal=0;
	    	try {
	    		 Session session = getSession(sid);
	 	        VWClient.printToConsole("sessionId in getNativeWrap::"+sid);
	 	        if(session==null) return invalidSessionId;
	 	        VWS vws = getServer(session.server);
	 	        if (vws == null) return ServerNotFound;
	    		boolean retFlag=false;
	    		retFlag=vws.getNativeWrap();
	    		System.out.println("retFlag getNativeWrap::::"+retFlag);
	    		if(retFlag==true){
	    			System.out.println("before returning:::"+retFlag);
	    			retVal= 1;
	    			
	    		}else{
	    			retVal= 0;
	    		}
	    	}
			catch(Exception ex) {
				System.out.println("Exception ::"+ex.getMessage());
				return Error;
			}
			return retVal;		
		}
	    //Generation allweb.zip in java
	    public int generateAllWeb(int sid,int docId,String pdfFolderPath){
	    	try{
	    		Session session = getSession(sid);
	    		VWClient.printToConsole("sessionId in generateAllWeb::"+sid);
	    		if(session==null) return invalidSessionId;
	    		VWS vws = getServer(session.server);
	    		if (vws == null) return ServerNotFound;
	    		VWClient.printToConsole("");
	    		com.computhink.vwc.CryptographyUtils utils = new com.computhink.vwc.CryptographyUtils();
	    		VWClient.printToConsole("pdfFolderPath ::::::::::::"+pdfFolderPath);
	    		File encryptCheck=null;
	    		File uploadFile=new File(pdfFolderPath);
	    		VWClient.printToConsole("uploadFile.exists() ::::::::::::"+uploadFile.exists());
	    		if(uploadFile.exists()){
	    			File[] files = uploadFile.listFiles();
	    			VWClient.printToConsole("files :"+files.length);
	    			List<File> encrypteFilesList=new ArrayList<File>();
	    			FileOutputStream fos =null;
	    			ZipOutputStream zos =null;
	    			if(files!=null){
	    				for(File f: files){
	    					File convertedFiles=new File(f.getAbsolutePath());
	    					String fileNameWithotExtion=convertedFiles.getName().substring(0,convertedFiles.getName().lastIndexOf("."));
	    					VWClient.printToConsole("convertedFiles.getParent() ::::"+convertedFiles.getParent());
	    					VWClient.printToConsole("convertedFiles.getParent() ::::"+convertedFiles.getParent());
	    					System.out.println("convertedFiles.getParent() ::::"+convertedFiles.getParent());
	    					VWClient.printToConsole("convertedFiles.getParent() ::::"+convertedFiles.getParent());
	    					File vwFile=new File(convertedFiles.getParent()+"\\"+fileNameWithotExtion+".vw");
	    					System.out.println("vwFile absolute path:::::"+vwFile.getAbsolutePath());
	    					VWClient.printToConsole("convertedFiles.getParent() ::::"+convertedFiles.getParent());
	    					System.out.println("convertedFiles.getAbsolutePath() :::"+convertedFiles.getAbsolutePath());
	    					VWClient.printToConsole("convertedFiles.getAbsolutePath() :::"+convertedFiles.getAbsolutePath());
	    					System.out.println("convertedFiles.getName() :::"+convertedFiles.getName());
	    					VWClient.printToConsole("convertedFiles.getName() :::"+convertedFiles.getName());
	    					System.out.println("removed file extension to add vw:::"+ convertedFiles.getParent()+"\\"+fileNameWithotExtion);
	    					VWClient.printToConsole("removed file extension to add vw:::"+ convertedFiles.getParent()+"\\"+fileNameWithotExtion);
	    					utils.encryptFile(convertedFiles.getAbsolutePath(), vwFile.getAbsolutePath());
	    					 encryptCheck=new File(convertedFiles.getParent()+Util.pathSep+"crypt.txt");
	    					System.out.println("encryptCheck :::"+encryptCheck);
	    					VWClient.printToConsole("encryptCheck :::"+encryptCheck);
	    					if(!encryptCheck.exists()){
	    						encryptCheck.createNewFile();
	    						encrypteFilesList.add(encryptCheck.getAbsoluteFile());
	    					}
	    					if(vwFile.exists()){
	    						encrypteFilesList.add(vwFile);
	    					}

	    				}
	    				if(encrypteFilesList!=null&&encrypteFilesList.size()>0){
	    					addToZipFile(encrypteFilesList,uploadFile);
	    					File allwebFile=new File(uploadFile.getAbsolutePath()+Util.pathSep+"allweb.zip");
	    					File zerowebFile=new File(uploadFile.getAbsolutePath()+Util.pathSep+"0.0");
	    					if(allwebFile.exists()){
	    						try{	    		
	    							boolean flag=allwebFile.renameTo(zerowebFile);
	    							System.out.println("rename to zero in jni...."+flag);
	    							File[] subFolders = uploadFile.listFiles();
	    							if(subFolders != null && subFolders.length > 0){
	    								for(int i=0; i<subFolders.length; i++){
	    									if(!(subFolders[i].getName().equals(zerowebFile.getName()))){
	    										
	    											Utils.deleteDirectory(subFolders[i].getAbsolutePath());
	    										
	    									}
	    								}
	    							}
	    							int retFlat=setDocFolder(sid,new Document(docId), uploadFile.getAbsolutePath());
	    							if(retFlat==0){
	    								Vector result1=new Vector();
	    								vws.setEncryptionType(session.room, sid, docId, 1, result1);
	    								VWClient.printToConsole("uploadFile ::::"+uploadFile);
	    							//	File partenFile=new File(uploadFile.getParent()).getParentFile();
	    								//File tempFolder=new File(new File(partenFile.getParent()).getParent());
	    								System.out.println("uploadFile.getParent()....."+uploadFile.getParent());
	    								//VWClient.printToConsole("partenFiletemp folder .........."+partenFile);
	    								Utils.deleteDirectory(uploadFile.getAbsolutePath());
	    							}
	    						}catch(Exception e){
	    							System.out.println("Exception in deleting and moving allweb.zip ::::"+e.getMessage());
	    							
	    						}
	    					}
	    					
	    				}
	    			}
	    		}
	    	}catch(Exception e){
	    		return Error;
	    	}
			return NoError;
	    }
	    
	    //Ziping the encrypted vw files in java.
	    private  void addToZipFile(List<File> list, File uploadFile)  {
			try{
				byte[] bytes = new byte[1024];
				FileOutputStream 	fos = new FileOutputStream(uploadFile.getAbsolutePath()+"\\"+"allweb.zip");
				ZipOutputStream zos = new ZipOutputStream(fos);
				for(int i=0;i<list.size();i++){
				System.out.println("Writing '" + list.get(i) + "' to zip file");
				VWClient.printToConsole("Writing '" + list.get(i) + "' to zip file");
				FileInputStream fis = new FileInputStream(list.get(i));
				ZipEntry zipEntry = new ZipEntry(list.get(i).getName());
				zos.putNextEntry(zipEntry);
				int length;
				while ((length = fis.read(bytes)) >= 0) {
					zos.write(bytes, 0, length);
				}
				zos.closeEntry();
				fis.close();
				}
				zos.close();
			}
			catch(Exception  e){
			  System.out.println("Exception while generating allweb.zip....."+e.getMessage());
			}

		}
	    
	    public int oTPValidationAudit(int sid,int docId,String desc)
	    {
	    	 Session session = getSession(sid);
	         if(session==null) return invalidSessionId;
	         VWS vws = getServer(session.server);
	         if (vws == null) return ServerNotFound;
	         VWClient.printToConsole("oTPValidationAudit description for docid = 0:::::"+desc);
	         try {
	        	 if (docId == 0) {
		        	 addATRecord(vws, session.room, sid, sid, 
		        			 VWATEvents.AT_OBJECT_LOGIN, 
		     				VWATEvents.AT_LOGIN,desc);
	        	 } else {
		        	 addATRecord(vws, session.room, sid, docId, 
		                     VWATEvents.AT_OBJECT_OTPVAL_FAILED,
		                     VWATEvents.AT_OTP,desc);
	        	 }
	         }catch(Exception ex) {
					System.out.println("Exception ::"+ex.getMessage());
					return Error;
				}
			return NoError;
	    }
	    
	    
	    public int getRouteTaskByName(int sid,int docId,int routeId,String taskName,Vector result)  {
	    	int retVal=0;
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("getRouteTaskByName:::::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret=vws.getRouteTaskByName(session.room, sid, routeId, docId, taskName, result);
	    		  if(ret!=null){
		            	VWClient.printToConsole("Inside ret value if condition");	
		            	result.addAll(ret);
		            	 VWClient.printToConsole("ret int getRouteTaskByName:::::::::::"+result);
		            }
	    		 
		    		VWClient.printToConsole("regVal in getRouteTaskByName--->"+result);
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    public int getCVCurrentFinancialYearPath(int sid,Vector result) throws RemoteException {
	    	int retVal=0;
	    	Session session = getSession(sid);
	    	VWClient.printToConsole("getCVCurrentFinancialYearPath:::::"+sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector ret=vws.getCVCurrentFinancialYearPath(session.room, sid, result);
	    		  if(ret!=null){
		            	VWClient.printToConsole("Inside ret value if condition");	
		            	result.addAll(ret);
		            	 VWClient.printToConsole("ret int getRouteTaskByName:::::::::::"+result);
		            }
	    		 
		    		VWClient.printToConsole("regVal in getRouteTaskByName--->"+result);
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    public int getDocumentIndicesWS(int sid, Document doc,Vector indices)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getDocumentIndicesWS::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	           Vector ret = vws.getDocumentIndices
	                    (session.room, sid, doc.getId(), doc.getVersion(), new Vector());
	           
	           if(ret!=null&&ret.size()>0){
	        	   indices.addAll(ret);
	           }
	         
	           //return vws.getActiveError();
	           if (sid>0 && vws.getActiveError()<0 ){
	           	return NoError;
	           } else {
	           	return vws.getActiveError();
	           }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    
	    public int getHRMSUserInfo(int sid, String userName,Vector retInfo)
	    {
	        Session session = getSession(sid);
	        VWClient.printToConsole("sessionId in getHRMSUserInfo::"+sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	           Vector ret = vws.getHRMSUserInfo(session.room, sid,userName, retInfo);
	           
	           if(ret!=null&&ret.size()>0){
	        	   retInfo.addAll(ret);
	           }
	         
	           //return vws.getActiveError();
	           if (sid>0 && vws.getActiveError()<0 ){
	           	return NoError;
	           } else {
	           	return vws.getActiveError();
	           }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    
	    
	    public int getCVGetLoginInfo(int sid,String userName,Vector retInfo)
	    {
	    	VWClient.printToConsole("sessionId in getCVGetLoginInfo"+sid);
	        Session session = getSession(sid);
	        if(session==null) return invalidSessionId;
	        VWS vws = getServer(session.server);
	        if (vws == null) return ServerNotFound;
	        try
	        {
	           Vector ret = vws.getCVGetLoginInfo(session.room, sid,userName, retInfo);
	           
	           if(ret!=null&&ret.size()>0){
	        	   retInfo.addAll(ret);
	           }
	           if (sid>0 && vws.getActiveError()<0 ){
	           	return NoError;
	           } else {
	           	return vws.getActiveError();
	           }
	        }
	        catch(Exception e)
	        {
	            return Error;
	        }
	    }
	    private boolean checkVWClientDisplayFlag(){
	    	Preferences clientPref =  null;        	
	    	boolean displayFlag = false;
	    	try{
	    		clientPref =  Preferences.userRoot().node("/computhink/" + PRODUCT_NAME.toLowerCase() + "/VWClient");
	    		displayFlag  = clientPref.getBoolean("debug",false);
	    	}catch(Exception ex){
	    		displayFlag = false;
	    	}
	    	return displayFlag;
	    }
	    
	    /**
	     * Create checkDocViewPerm to return the view permission for DTC optimization.
	     * If return value 1 means permission is their else 2 means permission is not their.
	     * @param sid
	     * @param docId
	     * @return
	     */
	    public int checkDocViewPerm(int sid,int docId)
	    {
	    	VWClient.printToConsole("checkDocViewPerm sid:::"+sid+ "docId ::"+docId);
	    	Session session = getSession(sid);
	    	if(session==null) return invalidSessionId;
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	try
	    	{
	    		Vector retPermInfo=new Vector();
	    		Vector ret=vws.checkDocViewPerm(session.room, sid, session.user, docId, retPermInfo);
	    		if(ret!=null&&ret.size()>0){
	    			int permVal=Integer.parseInt(ret.get(0).toString());
	    			if(permVal==1){
	    				return 1;
	    			}
	    			else if(permVal==0){
	    				return 2;
	    			}
	    		}
	    		if (sid>0 && vws.getActiveError()<0 ){
	    			return NoError;
	    		} else {
	    			return vws.getActiveError();
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		return Error;
	    	}
	    }
	    
	    public int encryptKey(String text, Vector result) {
	    	VWClient.printToConsole("encryptKey text   :"+text);
	    	if(text == null || text.length() == 0) return -1;
	    	try {
	    		String crypt = Base64Coder.encode(text);
	    		VWClient.printToConsole("encryptKey crypt :"+crypt);
	    		result.add(crypt);
	    	} catch (Exception e) {
	    		VWClient.printToConsole("EXCEPTION inside encryptKey :::: "+e.getMessage());
	    		return Error;
	    	}
	    	return NoError; 
	    }
	    
	    public int getServerSettingsSystemInfo(int sid, String roomName, String param, Vector result){
	    	VWClient.printToConsole("sid getServerSettingsSystemInfo ::::  "+sid);
	    	Session session = getSession(sid);
	    	if(session==null){
	    		return invalidSessionId;
	    	}
	    	VWS vws = getServer(session.server);
	    	if (vws == null){
	    		return ServerNotFound;
	    	}
	    	try
	    	{
	    		boolean sidbiCustomization = vws.getSidbiCustomizationFlag(roomName, sid);
	    		if(sidbiCustomization){
	    			Vector ret = vws.getSettingsInfo(session.room, sid, param, result);
	    			if(ret != null && ret.size() > 0){
	    				result.addAll(ret);
	    			}
	    		}
	    		return NoError;
	    	}
	    	catch(Exception e)
	    	{
	    		VWClient.printToConsole("EXCEPTION inside getServerSettingsSystemInfo :::: "+e.getMessage());
	    		return Error;
	    	}  
	    }
	    /**
	     * Method to get msi version number for DTC
	     * @param sid
	     * @return
	    */
		public int getMsiVersionNumber(int sid, String version, Vector versionNumber) {
			VWClient.printToConsole("inside getMsiVersionNumber :"+sid);
			Session session = getSession(sid);
			if (session == null) {
				return invalidSessionId;
			}
			VWS vws = getServer(session.server);
			if (vws == null) {
				return ServerNotFound;
			}
			try {
				String verNumber = vws.getMsiVersionInfo();
				if (verNumber != null && verNumber.trim().length() > 0)
					versionNumber.add(verNumber);
				VWClient.printToConsole("versionNumber :::: " + versionNumber);
				return NoError;
			} catch (Exception e) {
				VWClient.printToConsole("EXCEPTION inside getMsiVersionNumber :::: " + e.getMessage());
				return Error;
			}
		}
		    
	    /**
	     * Method to get msi url for DTC
	     * @param sid
	     * @return
	     */	    
		public int getMsiURL(int sid, String url, Vector msiURL) {
			VWClient.printToConsole("inside getMsiURL :"+sid);
			Session session = getSession(sid);
			if (session == null) {
				return invalidSessionId;
			}
			VWS vws = getServer(session.server);
			if (vws == null) {
				return ServerNotFound;
			}
			try {
				String urlVal = vws.getMsiUrlInfo();
				if (urlVal != null && urlVal.trim().length() > 0)
					msiURL.add(urlVal);
				VWClient.printToConsole("msiURL :::: " + msiURL);
				return NoError;
			} catch (Exception e) {
				VWClient.printToConsole("EXCEPTION inside getMsiURL :::: " + e.getMessage());
				return Error;
			}
		}
		
		 /**
	     * Method to get userName, password and domainName for DTC
	     * @param sid
	     * @return
	     */	    
		public int getMsiServerSettingsInfo(int sid, Vector msiSettingsInfo) {
			VWClient.printToConsole("inside getMsiServerSettingsInfo :"+sid);
			Session session = getSession(sid);
			if (session == null) {
				return invalidSessionId;
			}
			VWS vws = getServer(session.server);
			if (vws == null) {
				return ServerNotFound;
			}
			try {
				Vector ret = vws.getMsiServerSettingsInfo();
				VWClient.printToConsole("getMsiServerSettingsInfo return value :::: " + ret);
				if (ret != null) {
					msiSettingsInfo.addAll(ret);
				}
				VWClient.printToConsole("msiSettingsInfo :::: " + msiSettingsInfo);
				return NoError; 
			} catch (Exception e) {
				VWClient.printToConsole("Exception in getMsiServerSettingsInfo method :::: " + e.getMessage());
				return Error;
			}
		}
		
		public int checkDocIsInRoute(int sid, int nodeId, boolean flagRoute)
		{
			Session session = getSession(sid);
			System.out.println("sessionId in checkDocIsInRoute  ::::"+sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
			if (vws == null) return ServerNotFound;
			try
			{
				return vws.checkDocIsInRoute(sid, session.room, nodeId, flagRoute);
			}
			catch(Exception e)
			{
				return Error;
			}    
		}
		
		/**
		 * This method is used to get SIDBI Customization flag for To do list action
		 * @param sid
		 * @return
		*/
		public boolean getSidbiCustomizationFlag(int sid) {
			VWClient.printToConsole("sid getServerSettingsSystemInfo ::::  " + sid);
			boolean sidbiCustomizationFlag = false;
			Session session = getSession(sid);
			if (session != null) {
				VWS vws = getServer(session.server);
				if (vws != null) {
					try {
						sidbiCustomizationFlag = vws.getSidbiCustomizationFlag(session.room, sid);
					} catch (Exception e) {
						VWClient.printToConsole("EXCEPTION inside getSidbiCustomizationFlag :::: " + e.getMessage());
					}
				}
			}
			VWClient.printToConsole("getSidbiCustomizationFlag return value from VWClient...." + sidbiCustomizationFlag);
			return sidbiCustomizationFlag;
		}
		
		/**
		 * This method is used to check whether the selected document type is SIDBI document type
		 * @param sid
		 * @param docId
		 * @return
		 */
		public boolean getSIDBIDocumentTypeFlag(int sid, int docId) {
			VWClient.printToConsole("getSIDBIDocumentType sid :"+sid);
			boolean isSidbiDocumentType = false;
			Session session = getSession(sid);
			if (session != null) {
				VWS vws = getServer(session.server);
				if (vws != null) {
					try {
						boolean sidbiCustomizationFlag = vws.getServerSettingsInfo(session.room, sid, "SIDBI Customization");
						if (sidbiCustomizationFlag) {
							Vector<Document> doctypes = null;
							Document doc = null, document = null;
							String wfDoctypStr = null;
							Vector<String> wfDoctype =  new Vector<String>();
							getWorkflowDoctype(sid, wfDoctype);
							VWClient.printToConsole("wfDoctype :"+wfDoctype);
				        	if (wfDoctype.size() > 0) {
				        		doctypes =new Vector<Document>();
				        		doc = new Document(docId);
								getDocumentInfo(sid, doc, doctypes);
				        		document =(Document)doctypes.get(0);
				            	//VWClient.printToConsole("doc 1 object id is ::::"+doc1.getId());
				            	VWClient.printToConsole("Selected document :"+document.getDocType().getName());
								wfDoctypStr = (String) wfDoctype.get(0);
								if (wfDoctypStr.length() > 0 && wfDoctypStr != null) {
					        		String[] wfdoctyparray = wfDoctypStr.split(";");
									List<String> wfdoctypeList = new ArrayList<String>();
					        		Collections.addAll(wfdoctypeList, wfdoctyparray);
					        		if(wfdoctypeList.contains(document.getDocType().getName())){
					        			isSidbiDocumentType = true;
					        		}
					        	}
				        	}
						}
					} catch (Exception e) {
						VWClient.printToConsole("getSIDBIDocumentTypeFlag :"+e.getMessage());
					}
				}
	        	VWClient.printToConsole("isSidbiDocumentType :"+isSidbiDocumentType);
			}
			return isSidbiDocumentType;
		}
		
		/**
		 * CV10.2 Merges - Document multi-selection enhancement
		 * @param sid
		 * @param docId
		 * @param RouteInfo
		 * @param action
		 * @param DocName
		 * @return
		 */
		public int setRouteStatusForTodoMultiSelect(int sid, String docId, Vector RouteInfo, int action, String DocName) {
			
			try {
				VWClient.printToConsole("docId inside setRouteStatusFotTodoMultiSelect::::" + docId+" action :"+action+" RouteInfo:"+RouteInfo);
	
				if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type)
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (Exception e) {
			}
			Session session = getSession(sid);
			VWRouteAction routeActionDlg = new VWRouteAction(this, sid, docId, RouteInfo, action, DocName);
			new Thread(routeActionDlg).start();
			return NoError;
		}
	    
		public boolean getDrsValidateTaskDocumentInfo(int sid, int action, RouteMasterInfo routeMasterInfo) {
			boolean isValid = true;
			if (action == 1 && routeMasterInfo.getRouteUserSignType() >= 1) {
				isValid = false;
			} else {
				drsValidateTaskDocumentVwctor = new Vector();
				String routeTaskSequence = "";
				routeTaskSequence = (action == 1 ? "0" : String.valueOf(routeMasterInfo.getLevelSeq()));
				drsValidateTaskDocument(sid, routeMasterInfo.getDocId(), routeMasterInfo.getRouteId(), action, "0",
						drsValidateTaskDocumentVwctor);
				VWClient.printToConsole("drsValidateTaskDocumentVwctor return value :" + drsValidateTaskDocumentVwctor);
				if (drsValidateTaskDocumentVwctor != null && !drsValidateTaskDocumentVwctor.isEmpty()
						&& drsValidateTaskDocumentVwctor.size() > 0
						&& !((String) drsValidateTaskDocumentVwctor.get(0)).trim().equalsIgnoreCase("-1")
						&& (action == 1 || action == 2)) {
					isValid = false;
				}
			}
			VWClient.printToConsole("getDrsValidateTaskDocumentInfo :" + isValid);
			return isValid;
		}
	 /**
	    * CV10.2 - This method is used to generate static jasper report for administration report module
	    * @param sid
	    * @param selectedReport
	    * @param outputFormat
	    * @param dateFrom
	    * @param dateTo
	    * @param reportLocation
	    * @return
	    */
	   public boolean generateJasperReport(int sid, String selectedReport, String outputFormat, String dateFrom, String dateTo, String reportLocation, String reportName, int staticReportID, int nodeId) {
			boolean isReportGenerated = false;
			Session session = getSession(sid);
			VWClient.printToConsole("generateJasperReport :"+session);
			if (session != null) {
				VWS vws = getServer(session.server);
				if (vws != null) {
					try {
						String serverLocation = VWCUtil.getHome()+ Util.pathSep +"Jasper Reports" + Util.pathSep + reportName;
						isReportGenerated = vws.generateStaticJasperReport(sid, session.room, selectedReport, outputFormat, dateFrom, dateTo, serverLocation, staticReportID, nodeId);
						if (isReportGenerated) {
							VWClient.printToConsole("Server Location :"+serverLocation);
							VWClient.printToConsole("Client path :"+reportLocation);
							File clientJasperFile = new File(reportLocation);
							File srvJasperFile = new File(serverLocation);
							// This is added bz client and server and client systems are different means the html/csv file will be creating in the server location. Have to move the file to client system
							VWClient.printToConsole("jasperFile.exists() :"+srvJasperFile.exists());
							if (srvJasperFile.exists()) { 
								VWCUtil.CopyFile(srvJasperFile, clientJasperFile);	
								boolean isSrvFileDeleted = srvJasperFile.delete();
								VWClient.printToConsole("is Server FileDeleted >>>>>> "+isSrvFileDeleted);
							} else {
								VWDoc vwdoc = vws.getServerJasperContents(sid, session.room, serverLocation);	
								printToConsole("Is client report exist BF:"+clientJasperFile.getParentFile().exists());
								if (!clientJasperFile.getParentFile().exists())
									clientJasperFile.getParentFile().mkdirs();
								vwdoc.setDstFile(reportLocation);
								int ret = vwdoc.writeContents();
								VWClient.printToConsole("Server file copied to client :"+ret);
							}
							VWClient.printToConsole("is jasper file created in client location :"+clientJasperFile.exists());
						}
					} catch (Exception e) {
						VWClient.printToConsole("Exception in generate jasper report...."+e.getMessage());
					}
				}
			}
			return isReportGenerated;
		}  	  
		
		/**
		 * CV10.2 - This method is used to get the DB connection object for Workflow - Jasper reports
		 * @param sid
		 * @author vanitha.s
		 * @return
		*/
		public DBConnectionBean getDBConnection(int sid) {
			DBConnectionBean conectionBean = null;
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in getDBConnection  ::::"+session);
			if (session != null) {
				VWS vws = getServer(session.server);
				if (vws != null) {
					try {
						VWClient.printToConsole("before calling getDBConnection method :"+conectionBean);
						conectionBean = vws.getDBConnection(sid, session.room);
						VWClient.printToConsole("connection object in getDBConnection method :"+conectionBean);
					} catch (Exception e) {
						VWClient.printToConsole("Exception in getDBConnection :"+e.getMessage());
					}
				}
			}
			return conectionBean;
		}
		
		/**
		 * CV10.2 - This method used to get static report names list for administration workflow reports
		 * @param sid
		 * @author Vanitha.s
		 * @return
		 */
		
		public ArrayList<String> getStaticReportsList(int sid) {
			ArrayList<String> staticReportList = null;
			Session session = getSession(sid);
			if (session != null) {
				VWS vws = getServer(session.server);
				if (vws != null) {
					try {
						VWClient.printToConsole("before calling getStaticReportList method ");
						staticReportList = vws.getStaticReportsList(sid, session.room);
						//VWClient.printToConsole("result from getStaticReportList method :"+staticReportList);
					} catch (Exception e) {
						VWClient.printToConsole("Exception in getDBConnection :"+e.getMessage());
					}
				}
			}
			return staticReportList;
		}
		/**
		 * CV10.2 - This method used to check whether selected report is static or workflow report
		 * @param sid
		 * @param selecteValue
		 * @author vanitha.s
		 * @return
		 */
		
		public ArrayList<String> checkStaticReport(int sid, String selectedValue) {
			ArrayList<String>  staticReportDetails = null;
			Session session = getSession(sid);
			if (session != null) {
				VWS vws = getServer(session.server);
				if (vws != null) {
					try {
						VWClient.printToConsole("before calling checkStaticReport method ");
						Vector<String> result = vws.checkStaticReport(sid, session.room, selectedValue);
						//VWClient.printToConsole("result from checkStaticReport method :"+result);
						if (result != null && result.size() > 0) {
							staticReportDetails = new ArrayList<String>(result);							
						}
					} catch (Exception e) {
						VWClient.printToConsole("Exception in getDBConnection :"+e.getMessage());
					}
				}
			}
			return staticReportDetails;
		}
		 
		/***
		 * CV10.2 - This method used to get static report resultset to generate html/csv report
		 * @param sid
		 * @param reportID
		 * @param fromDate
		 * @param toDate
		 * @author vanitha.s
		 * @return
		*/
		public ArrayList getStaticReportResult(int sid, int reportID, String fromDate, String toDate) {
			ArrayList result = null;
			Session session = getSession(sid);
	    	VWClient.printToConsole("sessionId in getStaticReportResult::"+sid);
	    	if(session != null) {	    	
		    	VWS vws = getServer(session.server);
		    	if (vws != null) {
			    	try {
			    		Vector ret = vws.getStaticReportResult(sid, session.room, reportID, fromDate, toDate);
			    		VWClient.printToConsole("getStaticReportResult ret value :"+ret);
			    		if (ret != null && ret.size() > 0) {
			    			result = new ArrayList(ret);
			    			//VWClient.printToConsole("getStaticReportResult result value :"+result);	
			    		}
			    	} catch (Exception e) {
			    		VWClient.printToConsole("Exception in getStaticReportResult :"+e.getMessage());
			    	}
		    	}
	    	}
			return result;
		}
		/**
		 * SIDBI - Added for WebService Call
		 * @param sid
		 * @param settingsName
		 * @author apurba.m
		 * @return
		*/
		public int checkCustomizationFlagValues(int sid, String settingsName){
			Session session = getSession(sid);
			VWClient.printToConsole("sessionId in checkCustomizationFlagValues ::::"+sid);
			int customizationFlag = 0;
			if(session != null) {
				VWS vws = getServer(session.server);
				if (vws != null) {
					try {
						boolean flg = vws.getServerSettingsInfo(session.room, sid, settingsName);
						if(flg){
							customizationFlag = 1;
				    	} 
						VWClient.printToConsole("customizationFlag :: "+customizationFlag);
						return customizationFlag;
					} catch (Exception e) {
						VWClient.printToConsole("Exception in checkCustomizationFlagValues :::: "+e.getMessage());
						return Error;
					}
				} else {
					return ServerNotFound;
				}
			} else {
				return invalidSessionId;
			}
		}
		
		/**
		 * This method used to rename the corrupted all.zip file to invalidall.zip and then it create the new all.zip
		 * @param sid
		 * @param docID
		 * @param source
		 * @return
		 */
		public int validateInvalidDssDocument(int sid, int docID, String source) {
			VWClient.printToConsole("inside validateInvalidDssDocument......"+source);
			int returnValue = NoError;
			Session session = getSession(sid);
			ServerSchema dssSchema = new ServerSchema();
	    	VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
	    	Document doc = new Document(docID);
	    	try
	    	{
	    		VWClient.printToConsole("Before calling getDSSforDoc .....");
	    		ServerSchema ss = vws.getDSSforDoc(session.room, sid, doc, dssSchema);
	    		VWClient.printToConsole("After calling getDSSforDoc .....");
	    		dssSchema.set(ss);
	    		VWClient.printToConsole("DSS Path >>> "+dssSchema.path);
	    		File destFile=new File(dssSchema.path+Util.pathSep+"all.zip");
	    		VWClient.printToConsole("is all.zip exist :"+destFile.exists());
	    		if (destFile.exists()) {
	    			VWClient.printToConsole("all.zip size >>>>"+destFile.length()+"bytes");
	    			File fileRenameTo = new File(dssSchema.path+Util.pathSep+"invalidall.zip");
	    			destFile.renameTo(fileRenameTo);					
					VWClient.printToConsole("checking for invalidall.zip exist :"+fileRenameTo.exists());
					if (fileRenameTo.exists()) {
						VWClient.printToConsole("invalidall.zip file size :"+fileRenameTo.length()+"bytes");											
					}
					source = source.substring(0,source.indexOf("all.zip"));
					VWClient.printToConsole("source location  >>> "+source);
					//source = source.replaceAll("all.zip", "");					
					returnValue = setDocFile(sid, doc, source, false, false, false);
					//returnValue = setDocFile(sid, doc, null, false);
	    		} else {
	    			returnValue = ViewWiseErrors.allDotZipDoesNotExist;
	    		}
	    	} catch(RemoteException re) {
	    		VWClient.printToConsole("Exception in validateInvalidDssDocument method  :::::"+re.getMessage());	    		
	    		returnValue = Error;
	    	}
	    	return returnValue;
		}
		
		/**
		 * This method is used to generate dynamic jasper report for administration report module
		 * @param sid
		 * @param reportLocation
		 * @param resultSet
		 * @param columnDescription
		 * @param selectedReport
		 * @param reportName
		 * @param JrxmlPath
		 * @param isPreviewSelected
		 * @param outputformat
		 * @return
		 */
		public boolean generateDynamicJasperReport(int sid, String reportLocation,Vector<String> resultSet, String columnDescription,String selectedReport,String reportName,String JrxmlPath,boolean isPreviewSelected,String outputformat) {
			boolean isReportGenerated = false;
			Session session = getSession(sid);
			VWClient.printToConsole("generateDynamicJasperReport :"+session);
			if (session != null) {
				VWS vws = getServer(session.server);
				if (vws != null) {
					try {						
						VWDoc vwdoc = new VWDoc();
						vwdoc.setSrcFile(JrxmlPath);	
						String serverLocation = VWCUtil.getHome()+ Util.pathSep +"Jasper Reports" + Util.pathSep + reportName;
						isReportGenerated = vws.generateDynamicJasperReport(sid, session.room, vwdoc, serverLocation, resultSet,
								columnDescription, selectedReport, JrxmlPath, isPreviewSelected, outputformat);
						vwdoc = null;
						VWClient.printToConsole("isReportGenerated :"+isReportGenerated);
						if (isReportGenerated && !isPreviewSelected) {
							VWClient.printToConsole("Server Location :"+serverLocation);
							VWClient.printToConsole("Client path :"+reportLocation);
							File clientJasperFile = new File(reportLocation);
							File srvJasperFile = new File(serverLocation);
							// This is added bz client and server and client systems are different means the html/csv file will be creating in the server location. Have to move the file to client system
							VWClient.printToConsole("jasperFile.exists() :"+srvJasperFile.exists());
							if (srvJasperFile.exists()) { 
								VWCUtil.CopyFile(srvJasperFile, clientJasperFile);	
								boolean isSrvFileDeleted = srvJasperFile.delete();
								VWClient.printToConsole("is Server FileDeleted >>>>>> "+isSrvFileDeleted);
							} else {							
								vwdoc = vws.getServerJasperContents(sid, session.room, serverLocation);	
								VWClient.printToConsole("Is client report exist BF:"+clientJasperFile.getParentFile().exists());
								if (!clientJasperFile.getParentFile().exists())
									clientJasperFile.getParentFile().mkdirs();
								vwdoc.setDstFile(reportLocation);
								int ret = vwdoc.writeContents();
								VWClient.printToConsole("Server file copied to client :"+ret);
							}
							VWClient.printToConsole("is jasper file created in client location :"+clientJasperFile.exists());
						}
					} catch (Exception e) {
						VWClient.printToConsole("Exception while generating dynamic report...."+e.getMessage());
					}
				}
			}
			return isReportGenerated;
		}
		
	    /**
	     * This method is used to check DSS availability for EWA
	     * @param sid
	     * @param nodeID
	     * @author vanitha.s
	     * @return
	     */
		public int checkStorageLocationForEWA(int sid, int nodeID) {
			VWClient.printToConsole("nodeID inside checkStorageLocationForEWA "+nodeID);
			Session session = getSession(sid);
			if(session==null) return invalidSessionId;
			VWS vws = getServer(session.server);
	    	if (vws == null) return ServerNotFound;
			try {
				ServerSchema dssSchema = new ServerSchema();
				ServerSchema ss = vws.checkStorageLocationForEWA(session.room, sid, nodeID, dssSchema);			
				if (vws.getActiveError() >= 0 && ss != null) {		
					dssSchema.set(ss);
					VWClient.printToConsole("Before checking disk space.....");
					try {
						VWClient.printToConsole("dssSchema.address :"+dssSchema.address);
						VWClient.printToConsole("dssSchema.comport :"+dssSchema.comport);
						VWClient.printToConsole("dssSchema.type :"+dssSchema.type);
			    		DSS ds = (DSS) Util.getServer(dssSchema);
			    		VWClient.printToConsole("DSS dss >>>>>"+ds);
			    		if (ds == null) {
			    			return dssInactive;   
			    		}
						long registrySpace = ds.getStoragelimit();
						long availableSpace = vws.checkAvailableSpace(dssSchema, dssSchema.path);
						VWClient.printToConsole("Available disk space in MB " + availableSpace);
						VWClient.printToConsole("Size from Registry in MB " + registrySpace);
						if (registrySpace > availableSpace) {
							VWClient.printToConsole("returning low disk space error");
							return dssCriticalError;
						}
					} catch (Exception e) {
						VWClient.printToConsole("Exception while checking disk space "+e);
						return Error;
					}					
				} else {
					VWClient.printToConsole("No DSS Configured....");
					return vws.getActiveError();
				}
			} catch (Exception e) {
				VWClient.printToConsole("Exception while checking available storage location for EWA  :"+e);
				return Error;
			}
			return NoError;
		}
	}