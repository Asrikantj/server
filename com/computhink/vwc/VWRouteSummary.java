package com.computhink.vwc; 

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTable.PrintMode;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.drs.server.DRSConstants;
//import com.computhink.common.util.VWPrint;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.image.Images;
import com.computhink.vws.server.Client;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;


public class VWRouteSummary extends JPanel implements Runnable, Constants
{
	
	 public static boolean isWebClient = false; 
	 private static ResourceManager connectorManager =ResourceManager.getDefaultManager();
	 //JToggleButton showAll = new JToggleButton();
	 JLabel showAll = null;
	 int left = 0, top = 0, height = 0, width = 0;
	 boolean isShowAllEnabled = false;
	 boolean isDocumentInfoEnabled = false;
	 VWRouteSummaryInfoTable indicesTable;
	 
	 public VWRouteSummary() {
		 if (dlgMain != null) {
			 dlgMain.setVisible(false);
		 }
	 }
	 
	 public VWRouteSummary(int mode)
     {    
        new Images();
        initComponents();
        //loadSummaryDetails(mode);
        dlgMain.setVisible(true);
        keyBuffer.start();       
     }
     
     public VWRouteSummary(VWClient vwc, int session, int docId, int mode, String documentName)
     {
        if (active)
        {
        	if(session == prevSID && docId == prevDocId){
        		dlgMain.requestFocus();
        		dlgMain.toFront();
        		return;
        	}
        	else{
        		closeDialog();
        	}
        }    	 
    	this.vwc = vwc; 
    	
    	if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
			isWebClient = true;
    	
    	this.session = session;
    	prevSID = session;
    	this.docId = docId;
    	prevDocId =docId;
    	this.documentName = documentName;
    	VWClient.printToConsole("before calling initComponents........");    	
        initComponents();
        VWClient.printToConsole("after calling initComponents........");
        loadSummaryDetails(mode);
        VWClient.printToConsole("after calling loadSummaryDetails........");
        active = true;        
        keyBuffer.start();
        dlgMain.setVisible(true);    
     }
     
     private void initComponents() 
     {	
	 	new com.computhink.manager.image.Images();    	
    	buttonPanel = new JPanel();
    	routeMain = new JPanel();
    	propertiesPanel = new JPanel();
    	dlgMain = new JFrame();
    	lblRouteNameDesc = new JLabel(); 
        lblDocumentNameDesc = new JLabel();
        lblCreatedByDesc = new JLabel();
        lblTypeDesc = new JLabel();       
        btnClose = new JButton(connectorManager.getString("routesummary.close"));
        btnPrint = new JButton(connectorManager.getString("routesummary.print"));
        btnInfo = new JLabel();
        dlgMain.getContentPane().setLayout(null);
        dlgMain.setTitle(connectorManager.getString("WORKFLOW_MODULE_NAME") +" "+connectorManager.getString("routesummary.title")+" "+ documentName);
        dlgMain.setIconImage(com.computhink.manager.image.Images.app.getImage());
    	dlgMain.getContentPane().add(buttonPanel);
    	dlgMain.getContentPane().add(routeMain);
    	
    	
    	left = 12; top = 15; height = 20; width =100;
    
    	/****CV2019 merges from SIDBI line--------------------------------***/
    	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    	int screenHeight = screenSize.height;
    	int screenWidth = screenSize.width;
    	VWClient.printToConsole("Screen width & Height......."+screenWidth +" , "+screenHeight);
    	dlgMain.setSize((int)(screenWidth*0.9f), 390);    
    	/*------------------End of SIDBI merges----------------------------*/
    	/****CV2019 - Enhancement--------------------------------***/
    	VWClient.printToConsole("Width before reducing :"+dlgMain.getWidth());
    	width = (int)(dlgMain.getWidth()*0.98f);
        VWClient.printToConsole("buttonPanel top :"+top);
    	VWClient.printToConsole("Width after reducing :"+width);
    	buttonPanel.setBounds(0, top, width, 25);
    	buttonPanel.setLayout(null);
    	showAll = new JLabel();
    	showAll.setBounds(left, 0, 180, 20);
    	showAll.setText(connectorManager.getString("RouteSummaryBtn.showAll"));
    	showAll.setVerticalAlignment(SwingConstants.BOTTOM);
    	showAll.setBackground(Color.WHITE);
    	showAll.setForeground(Color.BLUE.darker());
    	showAll.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	buttonPanel.add(showAll);
    	
    	btnInfo.setBounds(left+192, 0, 150, 20);
    	btnInfo.setText(connectorManager.getString("RouteSummaryBtn.info"));
    	btnInfo.setVerticalAlignment(SwingConstants.BOTTOM);
    	btnInfo.setBackground(Color.WHITE);
    	btnInfo.setForeground(Color.BLUE.darker());
    	btnInfo.setCursor(new Cursor(Cursor.HAND_CURSOR));
    	buttonPanel.add(btnInfo);
    	
        /*------End of summary info enhancement changes-----------*/
    	top = top+buttonPanel.getHeight()+5;
    	height = dlgMain.getHeight()- (90);

    	VWClient.printToConsole("routeMain top :"+top);
    	VWClient.printToConsole("routeMain width :"+width);
    	VWClient.printToConsole("routeMain height :"+height);
    	routeMain.setBounds(0, top, width, height);
    	routeMain.setLayout(null);
    	
        scRoutePane = new JScrollPane();
        Table = new VWRouteSummaryTable(this, documentName);        
        scRoutePane.setViewportView(Table);
        
        if(!isWebClient)
        	scRoutePane.getViewport().setBackground(Color.WHITE);
        VWClient.printToConsole("scRoutePane :"+(height - 12));
        scRoutePane.setBounds(left, 0, width-25, height - 12);
        routeMain.add(scRoutePane);
        lblComments = new JLabel(connectorManager.getString("routesummary.comments"));
        lblComments.setBounds(left, 253, 80, 20);
        taComments = new JTextArea(2,300);
        taComments.setEditable(false);
        taComments.setLineWrap(true);        
        Font labelFont = lblComments.getFont();
        taComments.setFont(labelFont);        
        spComments = new JScrollPane(taComments,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        spComments.setBounds(left, 273, width-20, 36);
        btnPrint.setBounds(698, 324, 80, 21);
        btnClose.setBounds(782, 324, 80, 21);
        
        VWRouteSummaryInfo info = new VWRouteSummaryInfo(this.vwc);
        propertiesPanel.setLayout(null);
        propertiesPanel.setBounds(0, (buttonPanel.getHeight()+routeMain.getHeight()+15), (int)(width-(width*0.5f)), 215);
        indicesTable = info.initRouteInfoComponents(propertiesPanel, this.docId, (int)(width-(width*0.55f)), 200, 0, this.session, left);
		
   	 	propertiesPanel.setVisible(false);
   	 	dlgMain.add(propertiesPanel);
        /****CV2019 merges from SIDBI line--------------------------------***/
        dlgMain.setLocation(40, (screenHeight-600)/2);
        /*------------------End of SIDBI merges----------------------------*/
        dlgMain.setResizable(true);  //set to true by srikanth to make summary dialog to maximize on 16 Nov 2015
        
        /**Added for Ctrl+P(Print) key event. Added by Vanitha****/
        SummaryReportKeyEvents summaryRepKey = new SummaryReportKeyEvents();
        addKeyListener(summaryRepKey);
        dlgMain.addKeyListener(summaryRepKey);
        dlgMain.setFocusable(true);
        
        //Component listener added for resizing the RouteSummary dialog and resize the table and columns accordingly
        ComponentListener Clistener = new ComponentAdapter() {
        	public void componentResized(ComponentEvent evt) {
        		VWClient.printToConsole("ComponentListener ");
        		Component c = (Component) evt.getSource();
        		Dimension screenSize1 = c.getSize();
        		dlgMain.setSize(screenSize1);
        		top = 15;
        		if (isDocumentInfoEnabled == false)
        			height = dlgMain.getHeight()- (90); // buttonPanel height : 25
        		else
        			height = dlgMain.getHeight()- (90+210);
            	width = (int)(dlgMain.getWidth()*0.98f);
        		buttonPanel.setBounds(0, top, width, 25);
        		buttonPanel.repaint();
        		/****CV2019 merges from SIDBI line--------------------------------***/        		
        		top = top + buttonPanel.getHeight()+5;
        		routeMain.setBounds(0, top, width, height);
        		scRoutePane.setBounds(left, 0, width-15, height - 12);
        		
        		propertiesPanel.setBounds(0, (buttonPanel.getHeight()+routeMain.getHeight()+15), (int)(width-(width*0.4f)), 215);
        		dlgMain.repaint();
        		routeMain.repaint();
        		propertiesPanel.repaint();
        		Table.setBounds(0, 0, width-12, height - 15);
        		/*------------------End of SIDBI merges----------------------------*/
        		Table.repaint();
        	}
          };

          dlgMain.addComponentListener(Clistener);
          
          //Window state listener added for checking the window state is maximized then resize the table and columns accordingly
          WindowStateListener WSlistener = new WindowAdapter() {
        	  public void windowStateChanged(WindowEvent evt) {
        		  int oldState = evt.getOldState();
        		  int newState = evt.getNewState();
        		  if ((oldState & dlgMain.MAXIMIZED_BOTH) == 0 && (newState & dlgMain.MAXIMIZED_BOTH) != 0) {
        			  dlgMain.setResizable(true);  //set to true by srikanth to make summary dialog to maximize on 16 Nov 2015
        			  dlgMain.setExtendedState(dlgMain.MAXIMIZED_BOTH);
        			  /****CV2019 merges from SIDBI line--------------------------------***/
        			  top = 15;
	        		  if (isDocumentInfoEnabled == false)
	              		height = dlgMain.getHeight()- (90); // buttonPanel height : 25
	              	  else
	              		  height = dlgMain.getHeight()- (90+210);
	                  width = (int)(dlgMain.getWidth()*0.98f);
	              	  buttonPanel.setBounds(0, top, width, 25);
	              	  buttonPanel.repaint();
	              	  
	              	  top = top+buttonPanel.getHeight()+5;
        			  routeMain.setBounds(0, top, width, height);
        			  scRoutePane.setBounds(left, 0, width-15, height-12);
        			  /*------------------End of SIDBI merges----------------------------*/
        			  routeMain.repaint();
        			  scRoutePane.repaint();
        			  propertiesPanel.repaint();
        		  } 

        	  }
          };
          
        dlgMain.addWindowStateListener(WSlistener);

        dlgMain.addWindowListener(new WindowAdapter(){
            public void windowDeactivated(WindowEvent evt) {
            	if(active){
            		dlgMain.requestFocus();
            		dlgMain.toFront();
            	}
            }
             public void windowClosing(WindowEvent e) {
            	 closeDialog();
             }
        });
        
        btnPrint.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
                   doPrint();
                }
            });
        
        btnClose.addActionListener(new ActionListener(){
	        public void actionPerformed(ActionEvent evt) {
	               closeDialog();
            }
        });
        
        SelectionListener listener = new SelectionListener(Table);
        Table.getSelectionModel().addListSelectionListener(listener);
        Table.getColumnModel().getSelectionModel().addListSelectionListener(listener);
        /****CV2019 - Enhancement--------------------------------***/
        showAll.addMouseListener(new MouseAdapter() {
        	 
            @Override
            public void mouseClicked(MouseEvent e) {
                loadSummaryDetails();
            }
 
            @Override
            public void mouseExited(MouseEvent e) {
            	if (isShowAllEnabled == false) { 
            		showAll.setText(connectorManager.getString("RouteSummaryBtn.showAll"));
            	} else {
            		showAll.setText(connectorManager.getString("RouteSummaryBtn.actionsOnly"));
            	}
            }
 
            @Override
            public void mouseEntered(MouseEvent e) {
            	if (isShowAllEnabled == false) { 
            		showAll.setText("<html><a href=''>" + connectorManager.getString("RouteSummaryBtn.showAll") + "</a></html>");
            	} else {
            		showAll.setText("<html><a href=''>" + connectorManager.getString("RouteSummaryBtn.actionsOnly") + "</a></html>");
            	}
            }
 
        });
        
        btnInfo.addMouseListener(new MouseAdapter() {
       	 
            @Override
            public void mouseClicked(MouseEvent e) {
            	showDocumetProperties();
            }
 
            @Override
            public void mouseExited(MouseEvent e) {
            	btnInfo.setText(connectorManager.getString("RouteSummaryBtn.info"));
            }
 
            @Override
            public void mouseEntered(MouseEvent e) {
            	if (isDocumentInfoEnabled == false) { 
            		btnInfo.setText("<html><a href=''>" + connectorManager.getString("RouteSummaryBtn.info") + "</a></html>");
            	} else {
            		btnInfo.setText("<html><a href='' color='red'>" + connectorManager.getString("RouteSummaryBtn.info") + "</a></html>");
            	}
            }
 
        });
        /*---------------------------------------------------------*/
     }
     public void run()
     {
    	 VWClient.printToConsole("Inside run before show of route summary.....");
         show();
     }
     public void show()
     {
         if (vwc == null) return;
         /*
       	 * new frame.show(); method is replaced with new frame.setVisible(true); 
       	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
       	 */
         dlgMain.setVisible(true);
     }
     
     public void closeDialog() 
     {
    	 try{
	         active = false;
	         keyBuffer.kill();
	         dlgMain.setVisible(false);
	         dlgMain.dispose();
    	 }catch(Exception ex){
    		 //System.out.println("Err in close Dlg " + ex.getMessage());
    		 //ex.printStackTrace();
    	 }
     }
     
     /*private void loadSummaryDetails(int mode){
    	 int ret = 0;    	 
    	 ret = vwc.getRouteSummary(session, docId, mode, routeSummary);
    	 String rowData = "";
    	 //routeSummary.add("1	649	Test	1	Pandiya	4	RouteInfo1	Task3	test	2007-01-1805:51:08	Approved	2007-01-18 05:51:46	-	2007-01-18 05:51:46	-	test");
    	 //routeSummary.add("1	2	3	4	5	6	7	8	9	10	11	12	 test	14");
    	 //System.out.println("result route summary" + routeSummary);
    	 if (routeSummary != null && routeSummary.size() > 0){
    		 getSummaryDetails(routeSummary.get(0).toString());
    		 setSummaryDetails();
    	 }
    	 Vector data = new Vector();
    	 Table.clearData();
    	 for (int i = 0; i< routeSummary.size() ; i++){
    		 rowData = "";    		 
    		 String value = routeSummary.get(i).toString();
    		 StringTokenizer tokens = new StringTokenizer(value, Util.SepChar);
    		 try{
    			 rowData += tokens.nextToken() + Util.SepChar;// S.No
    			 tokens.nextToken(); // Document Id
    			 tokens.nextToken(); // Document Name
    			 tokens.nextToken(); // Route Id    		 
    			 rowData += tokens.nextToken() + Util.SepChar;// User Name    		 
    			 tokens.nextToken(); // RouteUser Id
    			 rowData += tokens.nextToken() + Util.SepChar;// Route Name
    			 rowData += tokens.nextToken() + Util.SepChar;// Task Name
    			 rowData += tokens.nextToken() + Util.SepChar;// Task Desc Name
    			 rowData += tokens.nextToken() + Util.SepChar;// Received On
    			 rowData += tokens.nextToken() + Util.SepChar;// Status
    			 //rowData += tokens.nextToken() + Util.SepChar;// Processed On
    			 tokens.nextToken();// Processed On
    			 rowData += tokens.nextToken() + Util.SepChar;// Comments
    			 //tokens.nextToken();// Comments
    			 rowData += tokens.nextToken() + Util.SepChar;// Comments    			 
    			 tokens.nextToken();// approvedBy
    			 tokens.nextToken();// approvedUserName
    			 tokens.nextToken();// authorizedBy
    			 tokens.nextToken();// authorizedUserName
    			 tokens.nextToken();// authorizedUserName    			 
    			 rowData += tokens.nextToken() + Util.SepChar;// routeUserSignType
    			 rowData += tokens.nextToken() + Util.SepChar;//userStatus
    		 }catch(Exception ex){
    			 //System.out.println("Exception in loadSummaryDetails" + ex.getMessage());
    		 }
    		 data.addElement(rowData);
    	 }
    	 Table.addData(data);
    	 Table.setLastProcessedRowSelected();
     }*/

     private void loadSummaryDetails(int mode){
    	 
    	 VWClient.printToConsole("inside loadSummaryDetails........");
    	 vwc.getRouteSummary(session, docId, mode, routeSummary);
    	 if (routeSummary != null && routeSummary.size() > 0){
    		 getSummaryDetails(routeSummary.get(0).toString());
    		 setSummaryDetails();
    	 }
    	 loadSummaryData(false);
     }
     
     private void loadSummaryData(Boolean showAllEnabled) {
    	 String rowData = null;
    	 Vector data = new Vector();
    	 Table.clearData();
    	 String[] tokens = null;
    	 for (int i = 0; i< routeSummary.size() ; i++){
    		 rowData = null;    		 
    		 String value = routeSummary.get(i).toString();
    		 tokens = value.split(Util.SepChar);
    		 VWClient.printToConsole("Summary info tokens length :"+tokens);
    		 VWClient.printToConsole("showAllEnabled :"+showAllEnabled);
    		 try{
    			 if (tokens.length > 0) {
    				 if (showAllEnabled) {
    					 rowData = getTableRowData(tokens);
    				 } else {
    					 if (checkActions(tokens[8])) {
    						 rowData = getTableRowData(tokens);
    					 }
    				 }
    			 }    			 
    		 }catch(Exception ex){
    			 VWClient.printToConsole("Exception in loadSummaryDetails" + ex.getMessage());
    		 }
    		 VWClient.printToConsole("rowData ....."+rowData);
    		 VWClient.printToConsole("condition ....."+(rowData != null && rowData.length() > 0));
    		 if (rowData != null && rowData.length() > 0)
    			 data.addElement(rowData);    		 
    	 }
    	 VWClient.printToConsole("final data ....."+data);
    	 Table.addData(data);
    	 //Table.setLastProcessedRowSelected();
     }
     
     private String getTableRowData(String[] tokens) {
    	 VWClient.printToConsole("tokens[0] :"+tokens[0]);
    	 String rowData = tokens[0]+ Util.SepChar;//tokens.nextToken() + Util.SepChar;// S.No
		 rowData += tokens[4] + Util.SepChar;// Route Name    		 
		 VWClient.printToConsole("tokens[6] :"+tokens[6]);
		 rowData += tokens[6] + Util.SepChar;// User Name
		 VWClient.printToConsole("tokens[7] :"+tokens[7]);
		 rowData += tokens[7] + Util.SepChar;// Received on
		 VWClient.printToConsole("tokens[8] :"+tokens[8]);
		 rowData += tokens[8] + Util.SepChar;// Status
		 VWClient.printToConsole("tokens[9] :"+tokens[9]);
		 rowData += tokens[9] + Util.SepChar;// Processed On
		 VWClient.printToConsole("tokens[10] :"+tokens[10]);
		 rowData += tokens[10] + Util.SepChar;// Comments
		 VWClient.printToConsole("Task Name :"+tokens[12]);
		 /***CV10.2 - Added TaskName as Originator for TaskInitiator/Originator****/
		 if (tokens[8] != null && tokens[8].equalsIgnoreCase("Originator"))
			 rowData += "Originator" + Util.SepChar;// Task Name
		 else
			 rowData += tokens[12] + Util.SepChar;// Task Name
		 /*****End of CV10.2 Merges************************/
		 VWClient.printToConsole("tokens[13] :"+tokens[13]);
		 rowData += tokens[13] + Util.SepChar;// TaskSeqID    			 
		 VWClient.printToConsole("tokens[19] :"+tokens[19]);
		 rowData += tokens[19] + Util.SepChar;// routeUserSignType
		 VWClient.printToConsole("tokens[20] :"+tokens[20]);
		 rowData += tokens[20] + Util.SepChar;//userStatus
		 return rowData;
     }
     
     private boolean checkActions(String status) {
    	 boolean flag = false;
    	 VWClient.printToConsole("status in checkAction :"+status);
		if (!status.contains("Approved, Task completed") && !status.contains("Approved, Workflow completed")
				&& !status.contains("Ended, Workflow ceased") && !status.contains("Rejected, Task completed")
				&& !status.contains("Rejected, Workflow completed") 
				&& !status.contains("Escalated entry added by DWSAdmin during Syncronization process")
				&& !status.startsWith("Removed User"))
			flag = true;
		VWClient.printToConsole("checkAction flag :"+flag);
    	 return flag;
     }
     
     private void loadSummaryDetails() {
    	 if (isShowAllEnabled == false) { 
    		 showAll.setText("<html><a href=''>" + connectorManager.getString("RouteSummaryBtn.actionsOnly") + "</a></html>");
    		 isShowAllEnabled = true;
    	 } else {
    		 showAll.setText("<html><a href=''>" + connectorManager.getString("RouteSummaryBtn.showAll") + "</a></html>");
    		 isShowAllEnabled = false;
    	 }
    	 loadSummaryData(isShowAllEnabled);
     }
     
     private void showDocumetProperties() {
    	 if (isDocumentInfoEnabled == false) {
    		 btnInfo.setForeground(Color.RED.darker());
    		 btnInfo.setText("<html><a href='' color='red'>" + connectorManager.getString("RouteSummaryBtn.info") + "</a></html>");
    		 isDocumentInfoEnabled = true;
    	 } else {
    		 btnInfo.setForeground(Color.BLUE.darker());
    		 btnInfo.setText("<html><a href=''>" + connectorManager.getString("RouteSummaryBtn.info") + "</a></html>");
    		 isDocumentInfoEnabled = false;
    	 }
    	 
    	 propertiesPanel.setVisible(isDocumentInfoEnabled);
    	 VWClient.printToConsole("Before resizing the routemain panel size....");
    	 Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
     	 int screenWidth = screenSize.width;
     	 
     	top = 0;
     	if (isDocumentInfoEnabled == false) {
			dlgMain.setSize((int)(screenWidth*0.9f), 390);
			dlgMain.repaint();
		} else {
			dlgMain.setSize((int)(screenWidth*0.9f), 625);
			dlgMain.repaint();
		}
		VWClient.printToConsole("isDocumentInfoEnabled...."+isDocumentInfoEnabled);   	 	
     }
     
     private void setSummaryDetails(){
    	 lblDocumentNameDesc.setText(this.documentName);
    	 lblRouteNameDesc.setText(this.routeName);
    	 lblCreatedByDesc.setText("");
    	 lblTypeDesc.setText("");
     }
     
     private void getSummaryDetails(String value){
    	 try{
	    	 String[] tokens = value.split(Util.SepChar);
	    	 VWClient.printToConsole("tokens in getSummaryDetails >>>>"+value);
	    	 this.routeName = tokens[4];
    	 }catch(Exception ex){
    		 VWClient.printToConsole("Exception in getSummaryDetails");
    		 VWClient.printToConsole(docId + " : " + documentName + " : " + routeId + " : " + routeName);
    	 }
     }
     
     public String getRouteData(int column){
	 return Table.m_data.getValueAt(Table.getSelectedRow(), column).toString();	 
     }
     
    private class KeyBuffer extends Thread
    {
        private String prefix = "";
        private boolean run = true;
        public KeyBuffer()
        {
            setPriority(Thread.MIN_PRIORITY);
        }
        public void appendKey(char c)
        {
            prefix+=c;
        }
        public void kill()
        {
            run = false;
        }
        public void run()
        {
            while (run)
            {
                Util.sleep(100);
                if (prefix.length() == 0) continue;
                prefix = "";
            }
        }
    }
    private JScrollPane scRoutePane;
    private VWRouteSummaryTable Table;
    private JButton btnClose;
    
    private JLabel lblRouteNameDesc;
    private JLabel lblDocumentNameDesc;
    private JLabel lblCreatedByDesc;
    private JLabel lblTypeDesc;
    private JButton btnPrint;
    //private JButton btnInfo;
    private JLabel btnInfo;
    private KeyBuffer keyBuffer = new KeyBuffer();;
    private JPanel routeMain, buttonPanel, propertiesPanel;
        
 
    private static VWClient vwc;
    private int session;
    private int docId;
    private int routeId;
    
    private String documentName = "";
    private String routeName = "";
    
    private static boolean active = false;
    private Vector routeSummary = new Vector();
    public static JFrame dlgMain;
    
    private static JTextArea taComments; 
    private static JScrollPane spComments;
    private static JLabel lblComments;
    
    public static int prevSID = 0;
    public static int prevDocId = 0;
  
    /**CV2020 - Code modified for summary report and Document properties print.Modified by Vanitha**/
    public void doPrint() {
    	active = false;
    	/**Following codes are added for printing multiple JTables as a single file. Added by Vanitha on 22/01/2020**/
		try {
			ArrayList<JTable> tableList = new ArrayList<JTable>();
			tableList.add(Table);
			if (isDocumentInfoEnabled == true && indicesTable != null)
				tableList.add(indicesTable);
			PrinterJob pj = PrinterJob.getPrinterJob();
			PageFormat pf = pj.defaultPage();
			pf.setOrientation(PageFormat.LANDSCAPE);
			Paper paper = new Paper();
			double margin = 36; // half inch
			paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
			pf.setPaper(paper);

			Book printJob = new Book();
			int totalPages = 0, count = 0;
			String title = null;
			for (JTable t : tableList) {
				VWClient.printToConsole("Table :" + t);
				try {
					int pages = getNumberOfPages(t.getPrintable(PrintMode.FIT_WIDTH, null, null), pf);
					if (count == 0)
						title = connectorManager.getString("WORKFLOW_MODULE_NAME") +" "+connectorManager.getString("routesummary.title")+" "+ documentName;
					else
						title = connectorManager.getString("RouteSummaryInfo.Title");
					MessageFormat headerFormat = new MessageFormat(title);
			        VWClient.printToConsole("pages :" + pages);
					Printable p = t.getPrintable(PrintMode.FIT_WIDTH, headerFormat, null);
					VWClient.printToConsole("before calling summaryprint  totalPages:" + totalPages);					
					printJob.append(new VWSummaryPrint(p, totalPages), pf, pages);
					totalPages += pages;
					count++;
				} catch (Exception e) {
					VWClient.printToConsole("Exception while printing the summary tables :" + e.getMessage());
				}
			}
			VWClient.printToConsole("Before setPagable......");
			pj.setPageable(printJob);
			VWClient.printToConsole("Before print......");
			if (pj.printDialog())
				pj.print();
		} catch (Exception e) {
			VWClient.printToConsole("Exception while printing the summary report :" + e.getMessage());
		}
    	/**End of multiple JTable print****/
    }
    
    public void doExport() {
		active = false;
		String[][] data = Table.getData();
		String[][] indicesData = null;
		if (isDocumentInfoEnabled == true && indicesTable != null) {
			indicesData = indicesTable.getData();
		}
		ArrayList<String> htmlContents = generateSummaryReportHtmlContent(data, indicesData, this.documentName, this.routeName);
		VWClient.printToConsole("htmlContents >>>>>"+htmlContents);
		try {
		    JFileChooser fileChooser = new JFileChooser();
		    File file = new File("c:\\"+ WORKFLOW_MODULE_NAME +"Summary.html");
		    fileChooser.setSelectedFile(file);
		    int returnVal = fileChooser.showSaveDialog(dlgMain);
	
		    String savePath = "";
		    if (returnVal == JFileChooser.APPROVE_OPTION) {
				savePath = fileChooser.getSelectedFile().getPath();
				fileChooser.setVisible(false);
				fileChooser = null;
		    }
		    File reportFile = new File(savePath);
		    boolean isFileSaved = VWCUtil.writeArrayListToFile(htmlContents, reportFile.getPath(), "", "UTF-8");
		    VWClient.printToConsole("Summary report created >>>>>"+isFileSaved);
	
		} catch (Exception ex) {
			VWClient.printToConsole("Exception while doing export :"+ex);
		}
		active=true;
    }
    
    	
    public class SelectionListener implements ListSelectionListener {
        JTable table;
        int ttlColumns = 0;
        int summaryColumn = 0;
        // It is necessary to keep the table since it is not possible
        // to determine the table from the event's source
        SelectionListener(JTable table) {
            this.table = table;
            
            ttlColumns = table.getColumnModel().getColumnCount();
    		summaryColumn = Table.m_data.COL_COMMENTS;
        }
        public void valueChanged(ListSelectionEvent e) {
        	try{
	            // If cell selection is enabled, both row and column change events are fired
	            if (e.getSource() == table.getSelectionModel() && table.getRowSelectionAllowed()) {
	                // Column selection changed
	                VWRouteSummary.taComments.setText(String.valueOf(table.getValueAt(table.getSelectedRow(), summaryColumn)));
	            } else if (e.getSource() == table.getColumnModel().getSelectionModel() && table.getColumnSelectionAllowed() ){
	                // Row selection changed
	                VWRouteSummary.taComments.setText(String.valueOf(table.getValueAt(table.getSelectedRow(),summaryColumn)));
	            }	    
	            if (e.getValueIsAdjusting()) {
	                // The mouse button has not yet been released
	            }
        	}catch(Exception ex){
        		vwc.printToConsole(" Error in valueChanged :"+ex.toString());
        	}
        }
    }
    /**Added for summary report print(CTRL+P)***/
    class SummaryReportKeyEvents extends java.awt.event.KeyAdapter {
        public void keyPressed(java.awt.event.KeyEvent event) {
        	VWClient.printToConsole("inside keypress event.....");
            AWTKeyStroke ak = AWTKeyStroke.getAWTKeyStrokeForEvent(event);
            if(ak.equals(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_P,InputEvent.CTRL_MASK)))
            {
            	VWClient.printToConsole("ctrl+p key pressed >>>>>:"+ak.getKeyCode());
          	  	doPrint();
            }
        }
    }
    public int getNumberOfPages(Printable delegate, PageFormat pageFormat) throws PrinterException 
    {
        Graphics g = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB).createGraphics();
        int numPages = 0;
        while (true) {
            int result = delegate.print(g, pageFormat, numPages);
            if (result == Printable.PAGE_EXISTS) {
                ++numPages;
            } else {
                break;
            }
        }

        return numPages;
    }
    /******End of ctrl+p(print) enhancement***/  
    
    /**
     * Added for Summary report export
     * @param data
     * @param indicesData
     * @param docName
     * @param routeName
     * @return
     */
    public ArrayList<String> generateSummaryReportHtmlContent(String[][] data, String[][] indicesData, String docName, String routeName) {
    	ArrayList<String> htmlContents = new ArrayList<String>();
    	String[] caption = DRSConstants.ExportReportColumnNames;
    	String[] colWidth = DRSConstants.ExportColumnWidths;
    	int rowCount = data.length;
    	String title = connectorManager.getString("WORKFLOW_NAME") +" "+connectorManager.getString("routesummary.report") ;
    	htmlContents.add("<html>");
    	htmlContents.add("<head>");
    	htmlContents.add("<title>" + title + "</title>");
    	htmlContents.add("<style>");
    	htmlContents.add("TH {");
    	htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 12px;COLOR='#336699'; font-weight:bold");
    	htmlContents.add("}");
    	htmlContents.add("TD {");
    	htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 11px");
    	htmlContents.add("}");
    	htmlContents.add("</style>");
    	htmlContents.add("</head>");
    	htmlContents.add("<body BGCOLOR='#F7F7E7'>");
    	htmlContents.add("<p align='center'><span lang='en-us'><font size='2'></font></span></p>");
    	htmlContents.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
    	htmlContents.add("<table border=0 cellspacing=1 width='92%!important' id='AutoNumber1' cellpadding=2>");
        htmlContents.add("<tr><td ><B>Document Name :</B>");
        htmlContents.add("		"+docName+"</td></tr>");
        if (routeName != null) {
	        htmlContents.add("<tr><td ><B>"+ Constants.WORKFLOW_MODULE_NAME +" Name :</B>");
	        htmlContents.add("		"+routeName+"</td></tr>");
        }
        htmlContents.add("</table>");
        htmlContents.add("<table border=1 bordercolor='grey' cellpadding='2' cellspacing='0' style= 'width:92%!important; border-collapse:collapse;' id='AutoNumber1' word-wrap='break-word'>");    	
    	htmlContents.add("<tr BGCOLOR=#cccc99>");
        for(int i=0;i<caption.length;i++) {
            htmlContents.add("<td width='" + colWidth[i] + "%' style='padding-left:2px;'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>" + caption[i] + "</B></td>");
        }
        htmlContents.add("</tr>");
        for(int i=0;i<data.length;i++)
        {
            htmlContents.add("<tr>");
            for(int j=0;j<caption.length;j++) {
                htmlContents.add("<td width='" + colWidth[j] + "%' style='padding-left:2px;'>" + data[i][j] + "</td>");
            }
            htmlContents.add("</tr>");
        }
    	htmlContents.add("</table>");
    	VWClient.printToConsole("htmlContents before printing document properties :"+htmlContents);
        try {
        	if (indicesData != null) {
    			rowCount = indicesData.length;
    			title = connectorManager.getString("routesummary.docProperties");
    			VWClient.printToConsole("getDocumentProperties table value : "+data);
    	        VWClient.printToConsole("rowCount :"+rowCount);
    		  	if (rowCount > 0) {
    	  		    String[] docPropertiesCaptions = DRSConstants.MetaDataColumnNames;
    	  		    String[] docPropertiesColWidth = DRSConstants.MetaDataColumnWidths;
    	  		 	htmlContents.add("<p><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
    	  		 	htmlContents.add("<table border=1 bordercolor='grey' cellpadding='2' cellspacing='0' style='border-collapse:collapse;' width='50%' id='AutoNumber1' word-wrap='break-word'>");
    		        htmlContents.add("<tr>");
    		        for(int i=0;i<docPropertiesCaptions.length;i++) {
    		            htmlContents.add("<td width='" + docPropertiesColWidth[i] + "%' style='padding-left:2px;' BGCOLOR='#cccc99'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>" + docPropertiesCaptions[i] + "</B></FONT></td>");
    		        }
    		        htmlContents.add("</tr>");
    			  	int serialNumber = 1;
    		        for (int x = 0; x < rowCount; x++){
    		        	htmlContents.add("<tr>"); 
    		        	htmlContents.add("<td  width='" + docPropertiesColWidth[0] + "%' style='padding-left:2px;'>" + serialNumber  + "</td>");
    		        	htmlContents.add("<td  width='" + docPropertiesColWidth[1] + "%' style='padding-left:2px;'>" + (indicesData[x][0].equalsIgnoreCase("-")?"&nbsp;":indicesData[x][0]) + "</td>");
    		        	htmlContents.add("<td  width='" + docPropertiesColWidth[2] + "%' style='padding-left:2px;'>" + (indicesData[x][1].equalsIgnoreCase("-")?"&nbsp;":indicesData[x][1]) + "</td>");
    	        		htmlContents.add("</tr>");
    	        		serialNumber++;
    	        	}
    		        htmlContents.add("</table>");
    		  	}
        	}
        } catch (Exception e) {
        	VWClient.printToConsole("Exception while printing document properties :"+e);
        }
        htmlContents.add("<BR>");
        htmlContents.add("<p align='left'><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></p>");
    	htmlContents.add("</body></html>");
    	VWClient.printToConsole("htmlContents after printing document properties :"+htmlContents);
    	return htmlContents;
    }
    
    public ArrayList<String> generateRouteSummaryHTMLContent(Vector<String> docRouteSummary, Vector<String> indices) {
    	VWClient.printToConsole("inside VWRouteSummary generateRouteSummaryHTMLContent....");
    	ArrayList<String> htmlContents = null;
    	String[] stDocInfos = null;	  	
	  	String status = null, routeName = null, signature = null, docName = null, tempSign = null, taskName = null, value = null;
        String tokens[] = null;
	  	try {
			VWClient.printToConsole("docRouteSummary......."+docRouteSummary);
			if (docRouteSummary != null && docRouteSummary.size() > 0) {
				int row = docRouteSummary.size();
				int indicesRowCount = indices.size();
				VWClient.printToConsole("docRouteSummary.size()......."+docRouteSummary.size());
			  	int column = DRSConstants.ExportReportColumnNames.length;
			  	VWClient.printToConsole("column >>>>>> "+column);
			  	int indicesColumn = DRSConstants.MetaDataColumnNames.length;
			  	String[][] data = new String[row][column+1];
			  	String[][] indicesData = new String[indicesRowCount][indicesColumn];
			  	int j = 0;
				for (int i = 0; i < row; i++) {
			  		try {
			  			VWClient.printToConsole("docRouteSummary data >>> "+docRouteSummary.get(i).toString());
				  		stDocInfos = (docRouteSummary.get(i).toString()).split("\t");
				  		docName = stDocInfos[2];
				  		routeName = stDocInfos[4];
				  		status = stDocInfos[8];
				  		if (status.equalsIgnoreCase("Originator")) {
				  			taskName = "Originator";
				  		} else {
				  			taskName = stDocInfos[12];
				  		}
				  		j = 0;
						signature = stDocInfos[19];
						data[i][j++] = stDocInfos[0];
						data[i][j++] = stDocInfos[6];
						data[i][j++] = taskName;
						data[i][j++] = stDocInfos[10];
						data[i][j++] = stDocInfos[13];
						data[i][j++] = status;
						data[i][j++] = stDocInfos[7];
						data[i][j++] = stDocInfos[9];
						data[i][j++] = stDocInfos[4];
						tempSign = "-";
						if(status.contains("Rejected"))
							signature = "-";
						tempSign = (signature.trim().equals("1")?"Authorized":(signature.trim().equals("2")?"Certified":"-"));
						
						data[i][j++] = tempSign;
						VWClient.printToConsole("userStatus >>>>>>>>>>>>>>>>>>>>"+stDocInfos[20]);//AuthorizedByName
						data[i][j++] = stDocInfos[20];
			  		} catch (Exception e) {
			  			VWClient.printToConsole("Exception while generating route summary array :"+e.getMessage());//AuthorizedByName
			  		}
			  	}
			  	docName = VWCUtil.fixFileName(docName);
			  	routeName = VWCUtil.fixFileName(routeName);
			  	if (indices != null && indices.size() > 0) {
					for (int index = 0; index < indices.size(); index++) {
						value = indices.get(index).toString();
						VWClient.printToConsole("Indices value :" + value);
						tokens = value.split(Util.SepChar);
						indicesData[index][0] = tokens[0];
						indicesData[index][1] = tokens[1];
						
					}
			  	}
			  	VWClient.printToConsole("before calling generateSummaryReportHtmlContent method >>>> "+data);
			  	VWClient.printToConsole("before calling generateSummaryReportHtmlContent indicesData >>>> "+indicesData);
			  	VWClient.printToConsole("before calling generateSummaryReportHtmlContent docName >>>> "+docName);
			  	VWClient.printToConsole("before calling generateSummaryReportHtmlContent routeName >>>> "+routeName);
			  	htmlContents = generateSummaryReportHtmlContent(data, indicesData, docName, routeName);
			}
		} catch (Exception e) {
			VWClient.printToConsole("Exception in generateRouteSummaryHTML :"+e.getMessage());
		}
    	return htmlContents;
    }
    
    public static void main(String[] args) {
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);//UIManager.getSystemLookAndFeelClassName());
			//UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			//VWRouteSummary a = new VWRouteSummary(1);
			//a.initComponents();
		}catch(Exception ex){}
		new Thread(new VWRouteSummary(1)).start();		
	} 
}