/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/
/**
 * VWBackupConnector<br>
 *
 * @version     $Revision: 1.11 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
package com.computhink.vwc;

import java.util.Collection;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.Hashtable;
import java.io.File;
import com.computhink.common.*;
import com.computhink.common.login.VWLoginConstant;
import com.computhink.vwc.resource.ResourceManager;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;
import java.io.*;

//------------------------------------------------------------------------------
public class VWDocBackup implements Constants, ViewWiseErrors
{
    final static public int BACKUP_TYPE=0;
    final static public int CHECKOUT_TYPE=1;
    final static public int COPYTYPE_TYPE=2;
    //Added for Change document type enhancement,Gurumurthy.T.S
    public static Vector docIndi=new Vector();
    
    ///private Vector backupInfo=new Vector();
    private VWClient vwClient=null;
    int retVal=0;
    static OutputStreamWriter osw = null;
    private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
    public int backupDocument(int sid, VWClient vwClient, Document doc, 
        boolean withHTML, boolean withVR, boolean withAT, boolean withReferences, 
        boolean withMiniViewer , boolean withUnencryptedPages, 
        boolean withPageText, String path, int backupType,String password)
    {
        File backupDocsPath=new File(path);
        backupDocsPath.mkdirs();
        if(!backupDocsPath.canWrite()) return DesFolderWriteErr;
        this.vwClient=vwClient;
        Session session = vwClient.getSession(sid);
        if(session==null) return invalidSessionId;
        String fileError ="";
        ///backupInfo=new Vector();
        try
        {
        	//Modified for Change Document Type Enhancement
        	Vector docs=new Vector();
            if(doc.getName()==null || doc.getName().equals(""))
            {
                vwClient.getDocumentInfo(sid,doc,docs);
            }
            //Added for Change Document Type Enhancement
            else
            	docs.addElement(doc);
            File backupDocPath=new File(backupDocsPath,"Document"+" ["+session.server+"."+session.room+"]["+doc.getId()+"]");
            backupDocPath.mkdirs();
            initFile((new File(backupDocPath,BackupFileName)).getPath(),fileError);
            try
            {
            	//Modified to pass the Vector for Change Document Type Enhancement,Gurumurthy.T.S
                getDocBackupInfo(sid, docs, withHTML, withVR, withAT, 
                    withReferences, backupType, backupDocPath,password);
            }
            catch(Exception e)
            {
                return getVWDocErr;
            }
            ///VWCUtil.writeListToFile(backupInfo,(new File(backupDocPath,BackupFileName)).getPath(),"");
            closeFile();
            
            if(withHTML)
            {
                initFile((new File(backupDocPath,BackupHTMLFileName)).getPath(),fileError);
                getHTMLFile();
                closeFile();
                ///VWCUtil.writeListToFile(getHTMLFile(),(new File(backupDocPath,BackupHTMLFileName)).getPath(),"");
            }
            //CV10 Enhancement Pareter withUnencryptedPages newly added to check the condition for encryption 
            VWSLog.add("Before calling copyPages.......");
            retVal = copyPages(sid, doc, new File(backupDocPath, BackupPageFolderName),withVR,withUnencryptedPages);
            VWSLog.add("afeter calling copyPages.......");
            /*
             * This code added for checking if DSS inactive not to continue the process of Backup. 
             * Vijaypriya.B.K - Feb 25, 2011 
             */
            if(retVal<0)
            	return retVal;
            
            if(withPageText)
            {
                retVal=vwClient.getDocPageText(sid, doc,
                    (new File(backupDocPath, BackupTextFolderName)).getPath());
            }
            /* Issue #761_01: When exception is got like 'Document has no text' system shouldn't retain the folders
            * This fix should work only for BackUp not for Check out documents 
            * C.Shanmugavalli 20 Nov 2006
            */            
            if(retVal != 0 && backupType == BACKUP_TYPE){
            	if (retVal == documentNotFound  || retVal == DocTextNotFoundErr) retVal = 0;
            	File textFolder = new File(backupDocPath.getPath() + File.separatorChar + "Text");
            	if (textFolder.listFiles().length == 0)
            	if(textFolder.exists()){
            		//If exists need to delete 'pages' folder, Document.vwd and Document.html files
                    VWCUtil.deleteDirectory(textFolder.toString());
                    retVal = 0;
            	}
            }
            if(withMiniViewer) copyMiniViewer(backupDocsPath.getParentFile());
            return retVal;
        }
        catch(Exception e){}
        finally {
            ///backupInfo.removeAllElements();
            ///backupInfo=null;
            closeFile();
        }
        return NoError;
}
//------------------------------------------------------------------------------
//Modified for Change Document Type Enhancement,Gurumurthy.T.S
private int getDocBackupInfo(int sid, Vector docs,boolean withHTML,
    boolean withVR ,boolean withAT, boolean withReferences,
        int backupType, File backupPath,String password)
{
    Session session=vwClient.getSession(sid);
    Hashtable indicesInfo=new Hashtable();
   //Added for Change Document Type Enhancement,Gurumurthy.T.S
    Document doc = null;
    doc=(Document)docs.get(0);
    // Comment : Read the encoding type from server registry . UTF-8 is default encoding type.
    if(backupType==0){
    String encoding = "";
    encoding = vwClient.getEncoding(sid);
    writeToFile("<?xml version='1.0' encoding='"+ encoding +"'?>");
    writeToFile("<VWDocument id='"+doc.getId()+"' >");
    writeToFile("<Version>"+ connectorManager.getString("ViewWise.Version") +"</Version>"); 
    writeToFile("<BackupType>"+String.valueOf(backupType)+"</BackupType>");
    writeToFile("<Room>"+VWCUtil.fixXMLString(session.room)+"</Room>");
    writeToFile("<Name>"+VWCUtil.fixXMLString(doc.getName())+"</Name>");
    writeToFile("<Path>"+VWCUtil.fixXMLString(getDocPath(sid,doc.getId()))+"</Path>");
    writeToFile("<PathIDs>"+VWCUtil.fixXMLString(getDocPathIDs(sid,doc.getId()))+"</PathIDs>");
    writeToFile("<Creator>"+VWCUtil.fixXMLString(doc.getCreator())+"</Creator>");
    writeToFile("<Creation-Date>"+VWCUtil.fixXMLString(doc.getCreationDate())+"</Creation-Date>");
    writeToFile("<Modified-Date>"+VWCUtil.fixXMLString(doc.getModifyDate())+"</Modified-Date>");
    writeToFile("<PageCount>"+String.valueOf(doc.getPageCount())+"</PageCount>");
    getDocComment(sid,doc);
    //Added for Change Document Type Enhancement,Gurumurthy.T.S
    writeToFile("<DocTypeCount>"+docs.size()+"</DocTypeCount>");
    for(int i=0;i<docs.size();i++){
    doc=(Document)docs.get(i);
    int k=i+1;
    //Modified for Change Document Type Enhancement,Gurumurthy.T.S
    writeToFile("<DocType id='"+(k)+"'>");
    
    Vector docTypes=new Vector();
    DocType docType=doc.getDocType();
    vwClient.getDocType(sid, docType, docTypes);
    if(docTypes!=null && docTypes.size()>0)
    {
        docType=(DocType)docTypes.get(0);
      //Modified for Change Document Type Enhancement,Gurumurthy.T.S
        writeToFile("<Name id='"+(k)+"'>"+VWCUtil.fixXMLString(docType.getName())+"</Name>");
        writeToFile("<EnableSIC id='"+(k)+"'>"+docType.getEnableTextSearch()+"</EnableSIC>");
        writeToFile("<AutoMail_To id='"+(k)+"'>"+VWCUtil.fixXMLString(docType.getTo())+"</AutoMail_To>");
        writeToFile("<AutoMail_Cc id='"+(k)+"'>"+VWCUtil.fixXMLString(docType.getCc())+"</AutoMail_Cc>");
        writeToFile("<AutoMail_Subject id='"+(k)+"'>"+VWCUtil.fixXMLString(docType.getSubject())+"</AutoMail_Subject>");
        getDocTypeVerRev(sid, docType,k);
        getDocTypeIndices(sid, docType,k);
    }
    writeToFile("</DocType>");
    }
    //Added for Change Document Type Enhancement,Gurumurthy.T.S
    doc=(Document)docs.get(0);
    getDocIndices(sid,doc);
    getDocRedactions(sid, doc);
    if(withVR)
        getDocVerIndices(sid, doc);
    if(withAT)
        getDocATData(sid, doc);
    if(withReferences)
        getDocReferences(sid, doc);
    getDocSignatures(sid, doc, backupPath);
    writeToFile("</VWDocument>");
    } else if(backupType==1||backupType==2){
        String encoding = "";
        encoding = vwClient.getEncoding(sid);
        writeToFile("<?xml version='1.0' encoding='"+ encoding +"'?>");
        writeToFile("<VWDocument id='"+doc.getId()+"' >");
        writeToFile("<Version>"+ connectorManager.getString("ViewWise.Version") +"</Version>"); 
        writeToFile("<BackupType>"+String.valueOf(backupType)+"</BackupType>");
        writeToFile("<Room>"+session.room+"</Room>");
        writeToFile("<Name>"+doc.getName()+"</Name>");
        writeToFile("<Path>"+getDocPath(sid,doc.getId())+"</Path>");
        writeToFile("<PathIDs>"+getDocPathIDs(sid,doc.getId())+"</PathIDs>");
        //CV10 Enhancement Password tag added only for dtc
        if(password.length()>0)
        writeToFile("<PassWord>"+password+"</PassWord>");
        writeToFile("<Creator>"+doc.getCreator()+"</Creator>");
        writeToFile("<Creation-Date>"+doc.getCreationDate()+"</Creation-Date>");
        writeToFile("<Modified-Date>"+doc.getModifyDate()+"</Modified-Date>");
        writeToFile("<PageCount>"+doc.getPageCount()+"</PageCount>");
        getDocComment(sid,doc);
        //Added for Change Document Type Enhancement,Gurumurthy.T.S
        writeToFile("<DocTypeCount>"+docs.size()+"</DocTypeCount>");
        for(int i=0;i<docs.size();i++){
        doc=(Document)docs.get(i);
        int k=i+1;
        //Modified for Change Document Type Enhancement,Gurumurthy.T.S
        writeToFile("<DocType id='"+(k)+"'>");
        
        Vector docTypes=new Vector();
        DocType docType=doc.getDocType();
        vwClient.getDocType(sid, docType, docTypes);
        if(docTypes!=null && docTypes.size()>0)
        {
            docType=(DocType)docTypes.get(0);
          //Modified for Change Document Type Enhancement,Gurumurthy.T.S
            writeToFile("<Name id='"+(k)+"'>"+docType.getName()+"</Name>");
            writeToFile("<EnableSIC id='"+(k)+"'>"+docType.getEnableTextSearch()+"</EnableSIC>");
            writeToFile("<AutoMail_To id='"+(k)+"'>"+docType.getTo()+"</AutoMail_To>");
            writeToFile("<AutoMail_Cc id='"+(k)+"'>"+docType.getCc()+"</AutoMail_Cc>");
            writeToFile("<AutoMail_Subject id='"+(k)+"'>"+docType.getSubject()+"</AutoMail_Subject>");
            getDocTypeVerRev(sid, docType,k);
            getDocTypeIndices(sid, docType,k);
        }
        writeToFile("</DocType>");
        }
        //Added for Change Document Type Enhancement,Gurumurthy.T.S
        doc=(Document)docs.get(0);
        getDocIndices(sid,doc);
        getDocRedactions(sid, doc);
        if(withVR)
            getDocVerIndices(sid, doc);
        if(withAT)
            getDocATData(sid, doc);
        if(withReferences)
            getDocReferences(sid, doc);
        getDocSignatures(sid, doc, backupPath);
        writeToFile("</VWDocument>");
        
    }
   
    return NoError;
}
//------------------------------------------------------------------------------
private String getDocPath(int sid,int nodeId)
{
    Vector nodes=new Vector();
    vwClient.getNodeParents(sid, new Node(nodeId), nodes);
    String docPath="";
    if(nodes==null||nodes.size()==0) return docPath;
    int count =nodes.size();
    for(int i =0;i<count;i++)   docPath=((Node)nodes.get(i)).getName()+Util.SepChar+docPath;
    return docPath.substring(0,docPath.length()-1);
}
//------------------------------------------------------------------------------
private String getDocPathIDs(int sid,int nodeId)
{
    String docPath="";
    try
    {
        Vector nodes=new Vector();
        vwClient.getNodeParents(sid, new Node(nodeId), nodes);
        if(nodes==null || nodes.size()==0)  return docPath;
        int count=nodes.size();
        docPath="";
        for(int i=0;i<count;i++)
        {
            Node node=(Node)nodes.get(i);
            if(node.getId()!=0) docPath=String.valueOf(node.getId())
                +Util.SepChar+docPath;
        }
        docPath=docPath.substring(0,docPath.length()-1);
    }
    catch(Exception e){
        docPath="";}
    return docPath;
}
//------------------------------------------------------------------------------
private void getDocComment(int sid, Document doc)
{
    writeToFile("<DocComments>");
    Vector comments=new Vector();
    vwClient.getDocumentComment(sid, doc, comments);
    if(comments==null)
    {
        writeToFile("<CommentCount>0</CommentCount>");
    }
    else
    {
        int count = comments.size();
        writeToFile("<CommentCount>"+String.valueOf(count)+"</CommentCount>");
        for(int i=0;i<count;i++)
        {
            DocComment docComment=(DocComment)comments.get(i);
            writeToFile("<DocComment id='"+(i+1)+"' >");
            writeToFile("<Comment id='"+(i+1)+"'>"+VWCUtil.fixXMLString(docComment.getComment())+"</Comment>");
            writeToFile("<UserName id='"+(i+1)+"'>"+VWCUtil.fixXMLString(docComment.getUser())+"</UserName>");
            writeToFile("<Created id='"+(i+1)+"'>"+VWCUtil.fixXMLString(docComment.getTimeStamp())+"</Created>");
            writeToFile("<Scope id='"+(i+1)+"'>"+VWCUtil.fixXMLString(String.valueOf(docComment.getPublic()))+"</Scope>");
            writeToFile("</DocComment>");
        }
    }
    writeToFile("</DocComments>");
}
//------------------------------------------------------------------------------
private void getDocIndices(int sid, Document doc)
{
    writeToFile("<Indices>");
    doc.setVersion("");
    vwClient.getDocumentIndices(sid, doc);
    //Added for Change Document Type Enhancement,Gurumurthy.T.S
    String str = (String) docIndi.get(0);
    String[] str1 = str.split("\t");
    int docTypeId=0;
    if(str1!=null&&str1.length>0)
    	docTypeId=Integer.valueOf(str1[3]);
    //
    if(doc.getDocType().getIndices()!=null && doc.getDocType().getIndices().size()>0)
    {
        Vector docIndices=doc.getDocType().getIndices();
        writeToFile("<Count>"+docIndices.size()+"</Count>");
        for(int i=0;i<docIndices.size();i++)
        {  
        	Index index=(Index)docIndices.get(i);
        	writeToFile("<DocIndex id='"+(i+1)+"' >");
        	writeToFile("<Name id='"+(i+1)+"'>"+VWCUtil.fixXMLString(index.getName())+"</Name>");
        	writeToFile("<Value id='"+(i+1)+"'>"+VWCUtil.fixXMLString(index.getValue())+"</Value>");
        	try{
        		writeToFile("<ActValue id='"+(i+1)+"'>"+VWCUtil.fixXMLString(index.getActValue())+"</ActValue>");
        	}
        	catch(Exception e1){writeToFile("<ActValue id='"+(i+1)+"'></ActValue>");};
        	writeToFile("</DocIndex>");
        	//Added for Change Document Type Enhancement,Gurumurthy.T.S
        	if(i==docIndices.size()-1){
        		writeToFile("<DocTypeId>"+docTypeId+"</DocTypeId>");
        		String docTypename = getDocTypeInfo(vwClient,sid,docTypeId);
        		/*
        		 * Added VWCUtil.fixXMLString() to doctypeName to replace the special 
        		 * character during document backup.Fix added after CV83B3
        		 * 
        		 */
        		writeToFile("<DocTypeName>"+VWCUtil.fixXMLString(docTypename)+"</DocTypeName>");
        	}
        	//
        }
    }
    writeToFile("</Indices>");
}
//------------------------------------------------------------------------------
private void getDocRedactions(int sid,Document doc)
{
    writeToFile("<Redactions>");
    Vector result=new Vector();
    vwClient.getAllRedactions(sid, doc, result);
    if(result!=null && result.size()>0)
    {
        writeToFile("<Count>"+result.size()+"</Count>");
        for(int i=0;i<result.size();i++)
        {       
            writeToFile("<Redaction id='"+(i+1)+"' >");
            StringTokenizer tokens=new StringTokenizer((String)result.get(i),Util.SepChar,false);
            writeToFile("<PageID id='"+(i+1)+"' >"+tokens.nextToken()+"</PageID>");
            writeToFile("<FrameID id='"+(i+1)+"' >"+tokens.nextToken()+"</FrameID>");
            writeToFile("<RedactionID id='"+(i+1)+"' >"+tokens.nextToken()+"</RedactionID>");
            writeToFile("<UserName id='"+(i+1)+"' >"+VWCUtil.fixXMLString(tokens.nextToken())+"</UserName>");
            writeToFile("<Password id='"+(i+1)+"' >"+VWCUtil.fixXMLString(tokens.nextToken())+"</Password>");
            writeToFile("<VerRev id='"+(i+1)+"' >"+tokens.nextToken()+"</VerRev>");
            writeToFile("</Redaction>");
        }
    }
    else
    {
        writeToFile("<Count>0</Count>");
    }
    writeToFile("</Redactions>");
}
//------------------------------------------------------------------------------
private void getDocReferences(int sid, Document doc)
{
    writeToFile("<References>");
    Vector refs=new Vector();
    vwClient.getRefs(sid, doc, refs);
    if(refs!=null && refs.size()>0)
    {
        writeToFile("<Count>"+refs.size()+"</Count>");
        for(int i=0;i<refs.size();i++)
        {       
            writeToFile("<Reference id='"+(i+1)+"' >");
            Document refDoc=(Document)refs.get(i);
            writeToFile("<NodeRefID id='"+(i+1)+"' >"+refDoc.getId()+"</NodeRefID>");
            writeToFile("<NodeRefPath id='"+(i+1)+"' >"+VWCUtil.fixXMLString(getDocPath(sid,refDoc.getId()))+"</NodeRefPath>");
            writeToFile("<NodeRefPathIDs id='"+(i+1)+"' >"+VWCUtil.fixXMLString(getDocPathIDs(sid,refDoc.getId()))+"</NodeRefPathIDs>");
            writeToFile("</Reference>");
        }
    }
    else
    {
        writeToFile("<Count>0</Count>");
    }
    writeToFile("</References>");
}
//------------------------------------------------------------------------------
private void getDocSignatures(int sid,Document doc, File backupPath)
{
    writeToFile("<Signatures>");
    Vector result=new Vector();
    vwClient.getDocumentSigns( sid, doc, result);
    if(result!=null && result.size()>0)
    {
        writeToFile("<Count>"+result.size()+"</Count>");
        for(int i=0;i<result.size();i++)
        {
            Signature sign=(Signature)result.get(i);
            writeToFile("<Signature id='"+(i+1)+"' >");
            writeToFile("<UserName id='"+(i+1)+"' >"+sign.getUserName()+"</UserName>");
            writeToFile("<Type id='"+(i+1)+"' >"+String.valueOf(sign.getType())+"</Type>");
            writeToFile("<Status id='"+(i+1)+"' >"+String.valueOf(sign.getStatus())+"</Status>");
            writeToFile("<SignDate id='"+(i+1)+"' >"+VWCUtil.fixXMLString(sign.getSignDate())+"</SignDate>");
            writeToFile("</Signature>");
            copyUserSignature(sid,sign, backupPath);
        }
    }
    else
    {
        writeToFile("<Count>0</Count>");
    }
    writeToFile("</Signatures>");
}
//------------------------------------------------------------------------------
private void copyUserSignature(int sid, Signature sign, File backupPath)
{
    File file= sign.makeSignFilePath( (new File(backupPath,BackupSignatureFolderName)).getPath(),
                sign.getUserName(),sign.getType());
            sign.setSignFilePath(file.getPath());
    if(!file.exists())  vwClient.getUserSign(sid, sign);
}
//------------------------------------------------------------------------------
private void getDocVerIndices(int sid, Document doc)
{
    writeToFile("<VerRev>");
    Vector vers=new Vector();
    vwClient.getDocumentVers(sid, doc, vers);
    //Added for Change Document Type Enhancement,Gurumurthy.T.S
    int vectorCount=0;
    if(vers==null || vers.size()==0)
    {
        writeToFile("<Count>0</Count>");
    }
    else
    {
        writeToFile("<Count>"+vers.size()+"</Count>");
        for(int i=0;i<vers.size();i++)
        {
            writeToFile("<VRDoc id='"+(i+1)+"' >");
            Document verDoc=(Document)vers.get(i);
            writeToFile("<Value id='"+(i+1)+"' >"+verDoc.getVersion()+"</Value>");
            writeToFile("<Creator id='"+(i+1)+"' >"+VWCUtil.fixXMLString(verDoc.getCreator())+"</Creator>");
            writeToFile("<CreationDate id='"+(i+1)+"' >"+VWCUtil.fixXMLString(verDoc.getCreationDate())+"</CreationDate>");
            try{
                writeToFile("<Comment id='"+(i+1)+"' >"+VWCUtil.fixXMLString(verDoc.getVRComment())+"</Comment>");
            }
            catch(Exception e){writeToFile("<Comment id='"+(i+1)+"' ></Comment>");}
            try
            {
                writeToFile("<DocFileName id='"+(i+1)+"' >"+VWCUtil.fixXMLString
                    (verDoc.getVersionDocFile())+"</DocFileName>");
            }
            catch(Exception e){writeToFile("<DocFileName id='"+(i+1)+"' > </DocFileName>");}
            vwClient.getDocumentIndices(sid, verDoc);
            if(verDoc.getDocType().getIndices()!=null && 
                verDoc.getDocType().getIndices().size()>0)
            {
            	 //Added for Change Document Type Enhancement,Gurumurthy.T.S
            	String docummentTypeName=getDocTypeInfo(vwClient,sid,verDoc.getDocType().getId());
            	VWClient.printToConsole("docummentTypeName : :"+docummentTypeName);
                writeToFile("<Count id='"+(i+1)+"' >"+verDoc.getDocType().getIndices().size()+"</Count>");
                //Added for Change Document Type Enhancement,Gurumurthy.T.S
                writeToFile("<VerdocTypeId id='"+(i+1)+"' >"+verDoc.getDocType().getId()+"</VerdocTypeId>");
                /*
        		 * Added VWCUtil.fixXMLString() to docummentTypeName to replace the special 
        		 * character during document backup.Fix added after CV83B3
        		 * 
        		 */
                writeToFile("<VerdocTypeName id='"+(i+1)+"' >"+VWCUtil.fixXMLString(docummentTypeName)+"</VerdocTypeName>");
                writeToFile("<Index id='"+(i+1)+"' >");
                for(int j=0;j<verDoc.getDocType().getIndices().size();j++)
                {
                    Index vrIndex=(Index)verDoc.getDocType().getIndices().get(j);
                    writeToFile("<Name id='"+(j+1)+"' >"+VWCUtil.fixXMLString(vrIndex.getName())+"</Name>");
                    writeToFile("<VRIndexKey id='"+(j+1)+"' >"+vrIndex.isKey()+"</VRIndexKey>");
                    writeToFile("<Value id='"+(j+1)+"' >"+VWCUtil.fixXMLString(vrIndex.getValue())+"</Value>");
                    try
                    {
                        writeToFile("<ActValue id='"+(j+1)+"' >"+VWCUtil.fixXMLString(vrIndex.getActValue())+"</ActValue>");
                    }
                    catch(Exception e){writeToFile("<ActValue id='"+(j+1)+"' ></ActValue>");};
                }
                writeToFile("</Index>");
            }
            writeToFile("</VRDoc>");
        }
    }
    writeToFile("</VerRev>");
}
//------------------------------------------------------------------------------
private void getDocATData(int sid, Document doc)
{
    writeToFile("<AuditTrail>");
    Vector result=new Vector();
    Vector filter = new Vector();
    filter.add(String.valueOf(doc.getId()));
    filter.add("2");
    filter.add(".");
    filter.add(".");
    filter.add(".");
    filter.add(".");
    filter.add("0");
    filter.add("0");
    filter.add("0");
    vwClient.getATData(sid, filter, result);
    if(result!=null || result.size()>0)
    {
        writeToFile("<Count>"+result.size()+"</Count>");
        for(int i=0;i<result.size();i++)
        {
            writeToFile("<ATDoc id='"+(i+1)+"' >");
            StringTokenizer tokens=new StringTokenizer((String)result.get(i),Util.SepChar,false);
            tokens.nextToken();tokens.nextToken();tokens.nextToken();
            writeToFile("<NodeName id='"+(i+1)+"' >"+VWCUtil.fixXMLString(tokens.nextToken())+"</NodeName>");
            writeToFile("<UserName id='"+(i+1)+"' >"+VWCUtil.fixXMLString(tokens.nextToken())+"</UserName>");
            writeToFile("<ClientIP id='"+(i+1)+"' >"+tokens.nextToken()+"</ClientIP>");
            writeToFile("<DActionDate id='"+(i+1)+"' >"+VWCUtil.fixXMLString(tokens.nextToken())+"</DActionDate>");
            writeToFile("<ActionType id='"+(i+1)+"' >"+tokens.nextToken()+"</ActionType>");
            writeToFile("<Location id='"+(i+1)+"' >"+VWCUtil.fixXMLString(tokens.nextToken())+"</Location>");
            writeToFile("<Description id='"+(i+1)+"' >"+VWCUtil.fixXMLString(tokens.nextToken())+"</Description>");
            try
            {
                writeToFile("<ActionDate id='"+(i+1)+"' >"+VWCUtil.fixXMLString(tokens.nextToken())+"</ActionDate>");
            }
            catch (Exception e){
            writeToFile("<ActionDate id='"+(i+1)+"' ></ActionDate>");};
            writeToFile("</ATDoc>");
        }
    }
    writeToFile("</AuditTrail>");
}
//------------------------------------------------------------------------------
private int copyPages(int sid, Document doc, File pageFolder, boolean withVR,boolean withUnencryptedPages)
{ 
	int retVal=0;
    if(withVR)
    {
    	//CV10 Enchancment backup with room level encryption method modified with extra parameter withUnencryptedPages
        retVal=vwClient.getDocFolderBackup(sid,doc,pageFolder.getPath(),withUnencryptedPages);
    }
    else
    {
    	//CV10 Enchancment backup with room level encryption method modified with extra parameter withUnencryptedPages
        retVal=vwClient.getDocFileBackup(sid,doc,pageFolder.getPath(),withUnencryptedPages,false,0);
    }
	/****CV2019 merges - DTC checkout enhancement***/
    /*VWSLog.dbg("Before doing java decryption and dll encryption.....");
    if (retVal != -1) {
    	//pageFolder = new File("D:\\CheckOut\\Pages");
    	File fileIn = null, fileOut = null;
    	File pageDir = new File(pageFolder.getPath());
    	if (pageDir.exists()) {
    		VWSLog.dbg("pageDir location ......"+pageDir.getPath());
    		File[] listOfFiles = pageDir.listFiles();
    		if (listOfFiles != null && listOfFiles.length > 0) {
    			File tempDir = new File(pageFolder.getPath()+File.separatorChar+"TempPages");
    			VWSLog.dbg("TempPages location ......"+tempDir.getPath());
		    	if (!tempDir.exists()) {
		    		tempDir.mkdir();
	    		}
		    	VWSLog.dbg("Files length ......"+listOfFiles.length);
		    	for (int i = 0; i < listOfFiles.length; i++) {
		    		VWSLog.dbg("file "+listOfFiles[i].getName()+" isFile  :"+listOfFiles[i].isFile());
    				if (listOfFiles[i].isFile()) {
    					VWSLog.dbg("java decryption and dll encryption started.......");    					
    					/**encryptAllDotZipFile method is used to apply java decryption and dll encryption for all.zip files **/
    					//boolean isEncrypted = vwClient.encryptAllDotZipFile(listOfFiles[i].getPath(), tempDir.getPath());
    					
    					/**applyJavaDecryptionForCheckoutDocuments method is used to apply only java decryption for all.zip files **/
    					/*boolean isEncrypted = vwClient.applyJavaDecryptionForCheckoutDocuments(listOfFiles[i].getPath(), tempDir.getPath());
    					if (isEncrypted) {
    						Util.deleteDirectory(listOfFiles[i]);
    						fileIn = new File(tempDir.getPath()+File.separatorChar+"all.zip");
    						fileOut = new File(listOfFiles[i].getPath());
    						boolean copied = VWCUtil.CopyFile(fileIn, fileOut);
    						VWSLog.dbg("Dll encrypted file moved from temp location to pages location :"+copied);
    						Util.deleteDirectory(fileIn);
    					}
    					VWSLog.dbg("End of java decryption and dll encryption.......");
    				}
    			}
		    	Util.deleteDirectory(tempDir);
    		}
    	}
    }*/
    return retVal;
}
//------------------------------------------------------------------------------
public int getHTMLFile()
{
    ///Vector lstHTML= new Vector();
    writeToFile("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>"+
    "<HTML>"+
    "<HEAD>"+
    "<TITLE> " + PRODUCT_NAME + " Document</TITLE>"+
    "</HEAD>"+
    "<BODY>"+
    "<H1 align='center'>Computhink Inc.</H1>"+
    "<H1 align='center'> " + PRODUCT_NAME + " Document Information</H1>"+
    "<HR>"+
    /*
    "<TABLE BORDER=1 WIDTH=100% bgcolor='#FFFF00' style='border-collapse: collapse' bordercolor='#111111' cellpadding='0' cellspacing='0'><TR>"+
    "<TH bgcolor='#FF3300'>&nbsp;<TH bgcolor='#3399FF'>&nbsp;<TH bgcolor='#009999'>&nbsp;<TH bgcolor='#FFFF99'>&nbsp;<TH bgcolor='#FF66FF'>&nbsp;<TH bgcolor='#993333'>&nbsp;</TABLE>"+
    */
     "<p>&nbsp;</p>"+
    "<SCRIPT LANGUAGE='JavaScript'>");
    ///lstHTML.add(strHTMLFile);
    writeToFile("<!--");
    writeToFile("xmlDoc=new ActiveXObject('microsoft.xmldom');"+
    "xmlDoc.load('Document.vwd');"+
    "var xmlDocument;"+
    "var xmlDocName;"+
    "var xmlRoomName;"+
    "var xmlPath;"+
    "var xmlCreator;"+
    "var xmlCreationDate;"+
    "var xmlModifiedDate;"+
    "var xmlComment;"+
    "var xmlDocIndices;"+
    "var intCount;"+
    "var xmlDocIndex;"+
    "var xmlDocVerRev;"+
    "xmlDocument = xmlDoc.documentElement;"+
    "xmlRoomName = xmlDocument.childNodes.item(2);"+
    "xmlDocName= xmlDocument.childNodes.item(3);"+
    "xmlPath = xmlDocument.childNodes.item(4);"+
    "xmlCreator= xmlDocument.childNodes.item(6);"+
    "xmlCreationDate = xmlDocument.childNodes.item(7);"+
    "xmlModifiedDate = xmlDocument.childNodes.item(8);"+
    "xmlComment=xmlDocument.childNodes.item(10);"+
    "xmlDocType= xmlDocument.childNodes.item(11);"+
    "xmlDocIndices= xmlDocument.childNodes.item(12);"+
    "xmlDocVerRev= xmlDocument.childNodes.item(14);"+
    "document.write("+DoubleQuotation+"<table border='0' cellpadding='0' cellspacing='0' style='border-collapse: collapse' bordercolor='#111111' width='100%' id='AutoNumber1'>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<tr><td width='25%' bgcolor='#CCCCCC'><b>Room Name :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%' bgcolor='#CCCCCC'>"+DoubleQuotation+");"+
    "document.write(xmlRoomName.text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%'><b>Document Name :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%'>"+DoubleQuotation+");"+
    "document.write(xmlDocName.text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%' bgcolor='#CCCCCC'><b>Document Path :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%' bgcolor='#CCCCCC'>"+DoubleQuotation+");"+
    "document.write(xmlPath.text.replace(/	/gi,'\\\\'));"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%'><b>Document Creator :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%'>"+DoubleQuotation+");"+
    "document.write(xmlCreator.text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%' bgcolor='#CCCCCC'><b>Document Creation Date :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%' bgcolor='#CCCCCC'>"+DoubleQuotation+");"+
    "document.write(xmlCreationDate.text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%'><b>Document Modified Date :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%'>"+DoubleQuotation+");"+
    "document.write(xmlModifiedDate.text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%' bgcolor='#CCCCCC'><b>Document Comment :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%' bgcolor='#CCCCCC'>"+DoubleQuotation+");"+
    "document.write(xmlComment.text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%'><b>Document Type Name :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%'>"+DoubleQuotation+");"+
    "document.write(xmlDocType.childNodes.item(0).text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%' bgcolor='#CCCCCC'><b>Enable search in contains :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%' bgcolor='#CCCCCC'>"+DoubleQuotation+");"+
    "document.write(xmlDocType.childNodes.item(1).text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%'><b>AutoMail To :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%'>"+DoubleQuotation+");"+
    "document.write(xmlDocType.childNodes.item(2).text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%' bgcolor='#CCCCCC'><b>AutoMail Cc :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%' bgcolor='#CCCCCC'>"+DoubleQuotation+");"+
    "document.write(xmlDocType.childNodes.item(3).text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%'><b>AutoMail Subject :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%'>"+DoubleQuotation+");"+
    "document.write(xmlDocType.childNodes.item(4).text);"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%' bgcolor='#CCCCCC'><b>Number of indices :&nbsp;</b></td>"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<td width='75%' bgcolor='#CCCCCC'>"+DoubleQuotation+");"+
    "if(xmlDocType.childNodes.item(5).tagName=='IndicesCount'){"+//This is having header, so checking with 1
    "document.write(xmlDocType.childNodes.item(5).text);}"+
    "else {"+
    "document.write(xmlDocType.childNodes.item(14).text);}"+
    "document.write('</td></tr>');"+
    "document.write("+DoubleQuotation+"<tr><td width='25%'><b>&nbsp;</b></td><td width='75%'></td></tr>"+DoubleQuotation+");"+
    "document.write('</TABLE>');"+
    "document.write("+DoubleQuotation+"<H3 <font color='#0000FF'>Document Properties</font></H3>"+DoubleQuotation+");"+
    "document.write('<TABLE BORDER=1 WIDTH=100%>');"+
    "document.write('<TR>');"+
    "document.write("+DoubleQuotation+"<TH bgColor='#FFFFCC'> Property Name"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<TH WIDTH='33%' bgColor='#FFFFCC'> Value"+DoubleQuotation+");"+
    "document.write('<TR>');"+
    "indicesCount=xmlDocIndices.childNodes.length;"+
    "for (intCount=1;intCount<indicesCount;intCount++){"+
    "xmlDocIndex=xmlDocIndices.childNodes.item(intCount);"+
    "document.write('<TR><TD>');"+
    "document.write(xmlDocIndex.childNodes.item(0).text);"+
    "document.write('<TD>');"+
    "document.write(xmlDocIndex.childNodes.item(1).text);}"+
    "document.write('</TABLE>');"+
    "verRevCount=xmlDocVerRev.childNodes.length;"+
    "if(xmlDocVerRev.tagName=='VerRev'){"+
    "document.write("+DoubleQuotation+"<H3 <font color='#0000FF'>Document Version/Revision</font></H3>"+DoubleQuotation+");"+
    "document.write('<TABLE BORDER=1 WIDTH=100%>');"+
    "document.write('<TR>');"+
    "document.write("+DoubleQuotation+"<TH bgColor='#FFFFCC'> V/R"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<TH bgColor='#FFFFCC'> Creator"+DoubleQuotation+");"+
    "document.write("+DoubleQuotation+"<TH bgColor='#FFFFCC'> Creation Date"+DoubleQuotation+");"+    
    "document.write("+DoubleQuotation+"<TH bgColor='#FFFFCC'> Comment"+DoubleQuotation+");"+
    "document.write('<TR>');"+
    "for (intCount=1;intCount<verRevCount;intCount++){"+
    "xmlDocVR=xmlDocVerRev.childNodes.item(intCount);document.write('<TR><TD>');document.write(xmlDocVR.childNodes.item(0).text);document.write('<TD>');"+
    "document.write(xmlDocVR.childNodes.item(1).text);document.write('<TD>');"+
    "document.write(xmlDocVR.childNodes.item(2).text);document.write('<TD>');"+
    "document.write(xmlDocVR.childNodes.item(3).text);}}document.write('</TABLE>');");
    //lstHTML.add(strHTMLFile);
    writeToFile("-->");
    writeToFile("</SCRIPT>"+
    "</TABLE>"+
    "<p>&nbsp;</p>"+
    "<TABLE BORDER=1 WIDTH=100%>"+
    "<TR>"+
    "<TH bgcolor='#000000'>&nbsp;<TH bgcolor='#333333'>&nbsp;<TH bgcolor='#666666'>&nbsp;<TH bgcolor='#999999'>&nbsp;<TH bgcolor='#CCCCCC'>&nbsp;<TH bgcolor='#FFFFFF'>&nbsp;</TABLE>"+
    "</BODY>"+
    "</HTML>");
    ///lstHTML.add(strHTMLFile);
    return NoError;
}
//------------------------------------------------------------------------------
//Modified for Change Document Type Enhancement,Gurumurthy.T.S
    private void getDocTypeVerRev(int sid, DocType docType,int indexNum)
    {
        Vector result=new Vector();
        vwClient.getDTVersionSettings(sid, docType);
        if(docType.getVREnable().equalsIgnoreCase("1"))
        {
           //Modified for Change Document Type Enhancement,Gurumurthy.T.S
            writeToFile("<VREnable id='"+(indexNum)+"'>"+docType.getVREnable()+"</VREnable>");
            writeToFile("<VRInitVersion id='"+(indexNum)+"'>"+docType.getVRInitVersion()+"</VRInitVersion>");
            writeToFile("<VRInitRevision id='"+(indexNum)+"'>"+docType.getVRInitRevision()+"</VRInitRevision>");
            writeToFile("<VRPagesChange id='"+(indexNum)+"'>"+docType.getVRPagesChange()+"</VRPagesChange>");
            writeToFile("<VRIncPagesChange id='"+(indexNum)+"'>"+docType.getVRIncPagesChange()+"</VRIncPagesChange>");
            writeToFile("<VRPageChange id='"+(indexNum)+"'>"+docType.getVRPageChange()+"</VRPageChange>");
            writeToFile("<VRIncPageChange id='"+(indexNum)+"'>"+docType.getVRIncPageChange()+"</VRIncPageChange>");
            writeToFile("<VRPropertiesChange id='"+(indexNum)+"'>"+docType.getVRPropertiesChange()+"</VRPropertiesChange>");
            writeToFile("<VRIncPropertiesChange id='"+(indexNum)+"'>"+docType.getVRIncPropertiesChange()+"</VRIncPropertiesChange>");
        }
    }
//--------------------------------------------------------------
//Modified for Change Document Type Enhancement,Gurumurthy.T.S
    private void getDocTypeIndices(int sid,DocType docType,int k)
    {
        vwClient.getDocTypeIndices(sid,docType);
        int indicesCount=docType.getIndices().size();
        //Modified for Change Document Type Enhancement,Gurumurthy.T.S
        writeToFile("<IndicesCount id='"+k+"' >"+indicesCount+"</IndicesCount>");
        for(int i=1;i<=indicesCount;i++)
        {
        	 //Modified for Change Document Type Enhancement,Gurumurthy.T.S
            writeToFile("<Index id='"+(i)+(k)+"' >");
            Index index=(Index)docType.getIndices().get(i-1);
            //Modified for Change Document Type Enhancement,Gurumurthy.T.S
            writeToFile("<NewMask id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString(index.getMask())+"</NewMask>");
            writeToFile("<NewDefault id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString((String)index.getDef().get(0))+"</NewDefault>");
            writeToFile("<Required id='"+(i)+(k)+"'>"+index.isRequired()+"</Required>");
            writeToFile("<IndexOrder id='"+(i)+(k)+"'>"+i+"</IndexOrder>");
            writeToFile("<NewInfo id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString(index.getInfo())+"</NewInfo>");
            writeToFile("<NewDescription id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString(index.getDescription())+"</NewDescription>");
            writeToFile("<DTKey id='"+(i)+(k)+"'>"+index.isKey()+"</DTKey>");
            
            index=getIndex(sid,index);
            //Modified for Change Document Type Enhancement,Gurumurthy.T.S
            writeToFile("<Name id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString(index.getName())+"</Name>");
            writeToFile("<Type id='"+(i)+(k)+"'>"+index.getType()+"</Type>");
            writeToFile("<Mask id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString(index.getMask())+"</Mask>");
            writeToFile("<Default id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString((String)index.getDef().get(0))+"</Default>");
            writeToFile("<SVid id='"+(i)+(k)+"'>"+index.getSystem()+"</SVid>");
            writeToFile("<Info id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString(index.getInfo())+"</Info>");
            writeToFile("<Description id='"+(i)+(k)+"'>"+VWCUtil.fixXMLString(index.getDescription())+"</Description>");
            if(index.getType()==4) /*Selection Type*/
            {
                String strValues="";
                for(int sel=0;sel<index.getDef().size()-1;sel++)
                    strValues+=((String)index.getDef().get(sel)).trim() + "|";
                strValues+=((String)index.getDef().get(index.getDef().size()-1)).trim();

                writeToFile("<SelectionValues id='"+(i)+"'>"+
                VWCUtil.fixXMLString(strValues)+"</SelectionValues>");
            }
            writeToFile("</Index>");
        }
    }
    //-----------------------------------------------------------
    private Index getIndex(int sid, Index index)
    {
        Vector result=new Vector();
        vwClient.getIndices(sid, index.getId(), result);
        if(result==null || result.size()==0) return index;
        StringTokenizer st = new StringTokenizer((String)result.get(0), "\t");
        try
        {
            st.nextToken();
            index.setName(st.nextToken());
            index.setType(Util.to_Number(st.nextToken()));
            index.setMask (st.nextToken());
            index.setDef(st.nextToken());
            st.nextToken();
            index.setCounter(Util.to_Number(st.nextToken()));
            st.nextToken();
            index.setSystem(Util.to_Number(st.nextToken()));
            index.setInfo(st.nextToken());
            index.setDescription(st.nextToken());
        }
        catch (Exception e){}
        return index;
    }
    //-----------------------------------------------------------
    public static int copyMiniViewer(File path)
    {
        File destFile=new File(path,"VWMiniWise");
        if(! destFile.exists())
        {
                VWCUtil.CopyDirectory(new File(VWCUtil.getHome(),"VWMiniWise"), destFile);
        }
        else
        {
            File dMiniWiseFile=new File(destFile, "Miniwise.exe");
            File sMiniWiseFile=new File(new File(VWCUtil.getHome(),"VWMiniWise"), "Miniwise.exe");
            if(dMiniWiseFile.lastModified()!=sMiniWiseFile.lastModified())
            {
                VWCUtil.CopyDirectory(new File(VWCUtil.getHome(),"VWMiniWise"), destFile);
                dMiniWiseFile.setLastModified(sMiniWiseFile.lastModified());
            }
        }
        
        return NoError;
    }
    
    public static boolean initFile(String path,String error)
    {
        try
        {
            File filePath = new File(path);
            filePath.getParentFile().mkdirs();
            if(filePath.exists())
            {
                filePath.renameTo(new File(path+Util.getNow(1)));
            }
            FileOutputStream ufos = new FileOutputStream(path);
            osw = new OutputStreamWriter(ufos);
        }
        catch(IOException e)
        {
            error=e.getLocalizedMessage();
            return false;
        }
        return true;
    }
    
    public static boolean closeFile()
    {
        try
        {
            osw.close();
        }
        catch(IOException e)
        {
            return false;
        }
        return true;
    }
    
    public static boolean writeToFile(String data)
    {
        try
        {
            osw.write(data+Constants.NewLineChar);
        }
        catch(IOException e)
        {
            return false;
        }
        return true;
    }
    public  static String getDocTypeInfo(VWClient vwClient,int sid,int docTypeId)
    { String documentTypeName="";
    Vector docTypeInfo = new Vector();
    vwClient.getDocTypeInfo(sid, docTypeId, docTypeInfo);
    String docTypename = (String)docTypeInfo.get(0);
    String[] arrayDocTypeName = docTypename.split("\t");
    documentTypeName = arrayDocTypeName[1];
    return documentTypeName;

    }
}