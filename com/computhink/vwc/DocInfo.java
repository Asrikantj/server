package com.computhink.vwc;

/**
 * VWAttachment.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */


import com.computhink.vwc.DocInfo;

public class DocInfo  implements java.io.Serializable {
    private byte[] data;

    private int docId;

    private java.lang.String docName;

    private boolean newPage;

    public DocInfo() {
    }

    public DocInfo(
           byte[] data,
           int docId,
           java.lang.String docName,
           boolean newPage) {
           this.data = data;
           this.docId = docId;
           this.docName = docName;
           this.newPage = newPage;
    }


    /**
     * Gets the data value for this VWAttachment.
     * 
     * @return data
     */
    public byte[] getData() {
        return data;
    }


    /**
     * Sets the data value for this VWAttachment.
     * 
     * @param data
     */
    public void setData(byte[] data) {
        this.data = data;
    }


    /**
     * Gets the docId value for this VWAttachment.
     * 
     * @return docId
     */
    public int getDocId() {
        return docId;
    }


    /**
     * Sets the docId value for this VWAttachment.
     * 
     * @param docId
     */
    public void setDocId(int docId) {
        this.docId = docId;
    }


    /**
     * Gets the docName value for this VWAttachment.
     * 
     * @return docName
     */
    public java.lang.String getDocName() {
        return docName;
    }


    /**
     * Sets the docName value for this VWAttachment.
     * 
     * @param docName
     */
    public void setDocName(java.lang.String docName) {
        this.docName = docName;
    }


    /**
     * Gets the newPage value for this VWAttachment.
     * 
     * @return newPage
     */
    public boolean isNewPage() {
        return newPage;
    }


    /**
     * Sets the newPage value for this VWAttachment.
     * 
     * @param newPage
     */
    public void setNewPage(boolean newPage) {
        this.newPage = newPage;
    }

}
