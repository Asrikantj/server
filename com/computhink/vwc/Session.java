/*
 * Session.java
 *
 * Created on December 21, 2003, 12:34 PM
 */

package com.computhink.vwc;
import com.computhink.common.Util;
/**
 *
 * @author  Administrator
 */
public class Session 
{
    public int sessionId = 0;
    public String server = "";
    public String room = "";
    public String cacheFolder = "";
    public String user = "";

    public Session() 
    {
    }

    public Session(int sessionId, String server, String room, String path, 
                                                                     String usr)
    {
        this.sessionId = sessionId;
        this.server = server;
        this.room = room;
        path = (path.endsWith("\\")? path : path + Util.pathSep); 
        this.cacheFolder = path + room; 
        this.user = usr;
    }
}