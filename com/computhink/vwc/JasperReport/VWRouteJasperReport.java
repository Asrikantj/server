package com.computhink.vwc.JasperReport;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import com.computhink.common.Util;
import com.computhink.common.VWDoc;
import com.computhink.database.Database;

import net.sf.jasperreports.engine.DefaultJasperReportsContext;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRField;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
//import net.sf.jasperreports.engine.JasperExportManager; // This import is for pdf output format
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.HtmlExporter;
import net.sf.jasperreports.engine.export.JRCsvExporter;
import net.sf.jasperreports.engine.export.JRCsvMetadataExporter;
import net.sf.jasperreports.engine.export.JRExporterContext;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRXlsAbstractExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterContext;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.type.OrientationEnum;
import net.sf.jasperreports.engine.util.FileBufferedOutputStream;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.Exporter;
import net.sf.jasperreports.export.OutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleCsvExporterConfiguration;
import net.sf.jasperreports.export.SimpleCsvMetadataExporterConfiguration;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleHtmlExporterOutput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimpleWriterExporterOutput;
import net.sf.jasperreports.export.SimpleXlsExporterConfiguration;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;
import net.sf.jasperreports.export.XlsExporterConfiguration;
import net.sf.jasperreports.export.XlsReportConfiguration;
import net.sf.jasperreports.view.JasperViewer;

public class VWRouteJasperReport {
public static String home = null;
	static 
    {
        findHome();
    }
	
	public static String getHome()
    {
        return home;
    }
	public VWRouteJasperReport() {
		
	}
	private static void findHome() {
		String urlPath = "";
		String indexerHome = "";
		String res = "com/computhink/vwc/image/images/vwc.gif";
		String jarres = "/lib/viewwise.jar!";

		int startIndex = 0;
		int endIndex = 0;
		try {
			URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
			urlPath = url.getPath();
		} catch (Exception e) {
			urlPath = "";
		}
		if (!urlPath.equalsIgnoreCase("")) {
			if (urlPath.startsWith("file")) {
				startIndex = 6;
				endIndex = urlPath.length() - res.length() - jarres.length();
			} else {
				startIndex = 1;
				endIndex = urlPath.length() - res.length();
			}
			indexerHome = urlPath.substring(startIndex, endIndex - 1);
			
			if (Util.getEnvOSName().indexOf("Linux") == -1)
				indexerHome = indexerHome.replace('/', '\\');
			else if (Util.getEnvOSName().indexOf("Linux") != -1 && (!indexerHome.startsWith("/"))) {
				indexerHome = "/" + indexerHome;
			}
		}
		home = cleanUP(indexerHome);
	}
    private static String cleanUP(String str) // removes %20    
    {
       String cleanStr = str.replace ("%20", " ");
       return (cleanStr.equalsIgnoreCase("")? str : cleanStr.trim());
    }

    public boolean generateStaticJasperReport(int sessionID, Connection con, String selectedReport, String outputFormat, String dateFrom, String dateTo, String path, Database db) {
		printToConsole("inside generateStaticJasperReport"); 
		boolean isReportGenerated = false;
		File jasperFile = null;
		String filePath = null;
		String engine = null;
		try {
			printToConsole("sessionID ...."+sessionID);
			engine = db.getEngineName();
			
		} catch (Exception e) {
			printToConsole("Exception in DB connection...."+e);
		}		
		System.out.println("Connection object....."+con);
		printToConsole("Connection object....."+con);
		//filePath = "C:"+Util.pathSep+"Program Files (x86)" + Util.pathSep+"Contentverse Client" + Util.pathSep + "Reports" + Util.pathSep;
		filePath = home + Util.pathSep + "Reports" + Util.pathSep;
		String folderName = null;
		if (engine.equalsIgnoreCase("Oracle")) {
			folderName = "oracle";			
		} else if (engine.equalsIgnoreCase("SQLServer")) {
			folderName = "mssql";
		}
		filePath = filePath + folderName + Util.pathSep;
		System.out.println("filePath....."+filePath);
		printToConsole("filePath....."+filePath);
		filePath = filePath.replace("Contentverse Client", "Contentverse");
		
		
		//path = filePath + "JasperReport";
		File jasperPath = new File(path);
		if (!jasperPath.getParentFile().exists())
			jasperPath.getParentFile().mkdirs();
		
		//path = path + Util.pathSep + reportName;
		printToConsole("path >>>>>"+path);
		if (con != null) {
			//System.out.println("Connection established with DB.....");
			printToConsole("Connection established with DB.....");
			try {
				printToConsole("Before reading jrxml file...");
				//System.out.println("Before reading jrxml file..."+selectedReport);
				InputStream inputFile =  null;
				printToConsole("selectedReport :"+selectedReport);
				System.out.println("selectedReport :"+selectedReport);
				String fileName = selectedReport +".jrxml";
				printToConsole("Jasper report fileName :"+fileName);
				System.out.println("Jasper report fileName :"+fileName);
				filePath = filePath + fileName;
				printToConsole("Jasper report file path :"+filePath);
				System.out.println("Jasper report file path :"+filePath);
				jasperFile = new File(filePath);
				printToConsole("Is japer file exists :"+jasperFile.exists());
				System.out.println("Is japer file exists :"+jasperFile.exists());
				if (jasperFile.exists()) {
					//inputFile = this.getClass().getResourceAsStream(filePath);
					inputFile = new FileInputStream(jasperFile);
				} else {
					System.out.println("inside else ... ");
					String resource = fileName;
					URL res = getClass().getResource(resource);
					printToConsole("resource  :"+resource);
					printToConsole("res  :"+res);
					if (res != null)
						printToConsole("URL  :"+res.toURI());
					inputFile = this.getClass().getResourceAsStream(resource);
					System.out.println("inputFile ... "+inputFile);
					if (inputFile != null && inputFile.available() > 0) {
						try {
			                InputStream input = getClass().getResourceAsStream(resource);
			                OutputStream out = new FileOutputStream(jasperFile);
			                int read;
			                byte[] bytes = new byte[1024];

			                while ((read = input.read(bytes)) != -1) {
			                    out.write(bytes, 0, read);
			                }
			                out.flush();
			                out.close();
			                input.close();
			            } catch (IOException ex) {
			                ex.printStackTrace();
			            }
					}
				}
				printToConsole("After reading jrxml file..."+inputFile.available());
				System.out.println("After reading jrxml file..."+inputFile.available());
				HashMap parameter = null;
				if (!selectedReport.equalsIgnoreCase("Pendency Report")) {
					if (dateFrom != null && dateFrom.trim().length() > 0 && dateTo != null && dateTo.trim().length() > 0) {
						parameter = new HashMap();
						parameter.put("FROM_DATE", dateFrom);
						parameter.put("TO_DATE", dateTo);
						printToConsole("From Date :"+parameter.get("FROM_DATE")+" To Date :"+parameter.get("TO_DATE"));
					}
				}
				//System.out.println("After reading jrxml file...");
				JasperReport jasperReport = JasperCompileManager.compileReport(inputFile);
				printToConsole("After compiling jrxml file...");
				System.out.println("After compiling jrxml file...");
				printToConsole("parameter >>>>>"+parameter);
			    JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, parameter, con);
				printToConsole("After fillReport....");
				printToConsole("Report Location :"+path);
				printToConsole("outputFormat :"+outputFormat);
				System.out.println("Report Location :"+path);
			    System.out.println("After fillReport....");
				if (outputFormat.equalsIgnoreCase("pdf")) {
					//path = path+".pdf";
					exportReport(jprint, path);
			    } else if (outputFormat.equalsIgnoreCase("html")) {
			    	//path = path+".html";
			    	exportReportHtml(jprint, path);
			    } else if (outputFormat.equalsIgnoreCase("xls")) {
			    	//path = path+".xls";
			    	exportReportXls(jprint, path);
			    } else if (outputFormat.equalsIgnoreCase("csv")) {
			    	exportReportCsv(jprint, path);
			    }else {
			    	JasperViewer.viewReport(jprint);
			    }
				printToConsole("After viewreport....");
				System.out.println("After viewreport....");
				isReportGenerated = true;
			} catch (Exception e) {
				printToConsole("Exception in jasper generation:::::" + e.getMessage());
				isReportGenerated = false;
			} finally {
				try {
					jasperFile = null;
					if (con != null)
						con.close();
				} catch(Exception e) {
					
				}
			}
		}
		return isReportGenerated;
	}
    
    /**
     * This method used to generate static jasper report for General selection 
     * @param selectedReport
     * @param outputFormat
     * @param path
     * @param resultSet
     * @author vanitha.s
     * @return
     */
    public boolean generateStaticJasperReportFromResultSet(String selectedReport, String outputFormat, String path, ArrayList<String> resultSet) {
    	boolean isReportGenerated = false;
    	String filePath = home + Util.pathSep + "Reports" + Util.pathSep;		
		printToConsole("filePath....."+filePath);
		filePath = filePath.replace("Contentverse Client", "Contentverse");
		
		printToConsole("path >>>>>"+path);
		File jasperPath = new File(path);
		if (!jasperPath.getParentFile().exists()) {
			jasperPath.getParentFile().mkdirs();
    	}
		
		try {
			printToConsole("Before reading jrxml file...");
			//System.out.println("Before reading jrxml file..."+selectedReport);
			InputStream inputFile =  null;
			printToConsole("selectedReport :"+selectedReport);
			String fileName = selectedReport +".jrxml";
			printToConsole("Jasper report fileName :"+fileName);
			filePath = filePath + fileName;
			printToConsole("Jasper report file path :"+filePath);
			File jasperFile = new File(filePath);
			printToConsole("Is japer file exists :"+jasperFile.exists());
			if (jasperFile.exists()) {
				inputFile = new FileInputStream(jasperFile);
			} else {
				System.out.println("inside else ... ");
				String resource = fileName;
				URL res = getClass().getResource(resource);
				printToConsole("resource  :"+resource);
				printToConsole("res  :"+res);
				if (res != null)
					printToConsole("URL  :"+res.toURI());
				inputFile = this.getClass().getResourceAsStream(resource);
				System.out.println("inputFile ... "+inputFile);
				if (inputFile != null && inputFile.available() > 0) {
					try {
		                InputStream input = getClass().getResourceAsStream(resource);
		                OutputStream out = new FileOutputStream(jasperFile);
		                int read;
		                byte[] bytes = new byte[1024];

		                while ((read = input.read(bytes)) != -1) {
		                    out.write(bytes, 0, read);
		                }
		                out.flush();
		                out.close();
		                input.close();
		            } catch (IOException ex) {
		                ex.printStackTrace();
		            }
				}
			}
			printToConsole("After reading jrxml file..." + inputFile.available());
			if (resultSet != null) {
				String[] columnDesc = resultSet.get(0).split("\t");
				resultSet.remove(0);
				printToConsole("resultSet.size() after removing the first index :" + resultSet.size());

				String[] columnsList = new String[columnDesc.length];
				for (int clmnCount = 0; clmnCount < columnDesc.length; clmnCount ++) {
					columnsList[clmnCount] = "Column"+(clmnCount+1);
				}
				printToConsole("columnList >>>> " + columnsList);
				List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();
				Map<String, Object> mapObject = null;
				String[] rowValue = null;
				for (int i = 0; i < resultSet.size(); i++) {
					mapObject = new HashMap<String, Object>();
					rowValue = resultSet.get(i).split("\t");
					for (int j = 0; j < columnsList.length; j++) {
						mapObject.put(columnsList[j], rowValue[j]);
					}
					printToConsole("mapObject["+i+"] >>>>>>>>>>>>>>>>>> "+mapObject);
					mapList.add(mapObject);
				}
				printToConsole("mapList >>>>>>>>>>>>>>>>>> "+mapList);
				printToConsole("size of the list >>>>>>>>>>>>>>>>>> "+mapList.size());		
				if (mapList.size() > 0) {
					Map<String, Object> parameters = new HashMap<String, Object>();
					JRBeanCollectionDataSource itemsJRBean = new JRBeanCollectionDataSource(mapList);
					parameters.put("CollectionBeanParameter", itemsJRBean);
					JasperDesign jasperDesign = JRXmlLoader.load(inputFile);
					printToConsole("After loading the jrxml file...");
					JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
					printToConsole("After compiling the jrxml file...");
				    JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
					printToConsole("After fillReport....");
					printToConsole("Report Location :"+path);
					printToConsole("outputFormat :"+outputFormat);
					System.out.println("Report Location :"+path);
				    System.out.println("After fillReport....");
					if (outputFormat.equalsIgnoreCase("pdf")) {
						exportReport(jprint, path);
				    } else if (outputFormat.equalsIgnoreCase("html")) {
				    	exportReportHtml(jprint, path);
				    } else if (outputFormat.equalsIgnoreCase("xls")) {
				    	exportReportXls(jprint, path);
				    } else if (outputFormat.equalsIgnoreCase("csv")) {
				    	exportReportCsv(jprint, path);
				    }else {
				    	JasperViewer.viewReport(jprint);
				    }
					printToConsole("After viewreport....");
					System.out.println("After viewreport....");
					isReportGenerated = true;
				}
			}

			/*if (beanFactory != null) {
				printToConsole("beanFactory.getReportTypeID() >>>>>"+beanFactory.getReportTypeID());
				Map<String, Object> parameters = new HashMap<String, Object>();
				JRBeanCollectionDataSource itemsJRBean = null;
				if (beanFactory.getReportTypeID() == 1 && beanFactory.getWorkflowCompletedList() != null) {
					printToConsole("beanFactory.getReportTypeID() >>>>>"+beanFactory.getReportTypeID());
					ArrayList<WorkFlowCompletedBean> completedDocList = beanFactory.getWorkflowCompletedList();
					printToConsole("completedDocList size >>>>> "+completedDocList.size());
					itemsJRBean = new JRBeanCollectionDataSource(beanFactory.getWorkflowCompletedList());
					printToConsole("itemsJRBean >>>>> "+itemsJRBean);
				}
				if (itemsJRBean != null) {
					parameters.put("CollectionBeanParameter", itemsJRBean);
					JasperDesign jasperDesign = JRXmlLoader.load(inputFile);
					printToConsole("After loading the jrxml file...");
					JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);
					printToConsole("After compiling the jrxml file...");
				    JasperPrint jprint = (JasperPrint) JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
					printToConsole("After fillReport....");
					printToConsole("Report Location :"+path);
					printToConsole("outputFormat :"+outputFormat);
					System.out.println("Report Location :"+path);
				    System.out.println("After fillReport....");
					if (outputFormat.equalsIgnoreCase("pdf")) {
						exportReport(jprint, path);
				    } else if (outputFormat.equalsIgnoreCase("html")) {
				    	exportReportHtml(jprint, path);
				    } else if (outputFormat.equalsIgnoreCase("xls")) {
				    	exportReportXls(jprint, path);
				    } else if (outputFormat.equalsIgnoreCase("csv")) {
				    	exportReportCsv(jprint, path);
				    }else {
				    	JasperViewer.viewReport(jprint);
				    }
					printToConsole("After viewreport....");
					System.out.println("After viewreport....");
					isReportGenerated = true;
				}
			}*/
		} catch (Exception e) {
			printToConsole("Exception in jasper generation:::::" + e.getMessage());
			isReportGenerated = false;
		} finally {
			
		}

    	return isReportGenerated;
    }
	
    /**
     * This method used to generate dynamic column jasper report for general selection
     * @param sessionID
     * @param vwdoc
     * @param reportLocation
     * @param resultSet
     * @param columnDescription
     * @param selectedReport
     * @param JrxmlPath
     * @param isPreviewSelected
     * @param outputformat
     * @author Nikith
     * @return
     */
	public static boolean generateDynamicJasperReport(int sessionID, VWDoc vwdoc, String reportLocation,Vector<String> resultSet, String columnDescription,String selectedReport,String JrxmlPath,boolean isPreviewSelected,String outputformat) {
		boolean isReportGenerated=false;
		try {
		    printToConsole("Inside Generate Dynamic Reports");
			printToConsole("Value of xml path name"+JrxmlPath);
			printToConsole("is preview enabled ::::"+isPreviewSelected);			
			Iterator value = resultSet.iterator();
			String arr = null;
			String[] finarr=new String[resultSet.size()];
			List<String[]> finval=new ArrayList<>();//list of string objects to get each column value
			
			
			String filePath = home + Util.pathSep + "Reports" + Util.pathSep;
			String folderName = null;
			printToConsole("filePath....."+filePath);
			filePath = filePath.replace("Contentverse Client", "Contentverse");
			
			String[] columnsSelected=columnDescription.split("\t");
			String[] JrxmlTemplate= {"JasperReportwith1Column.jrxml","JasperReportwith2Column.jrxml","JasperReportwith3Column.jrxml","JasperReportwith4Column.jrxml","JasperReportwith5Column.jrxml","JasperReportwith6Column.jrxml","JasperReportwith7Column.jrxml","JasperReportwith8Column.jrxml","JasperReportwith9Column.jrxml","JasperReportwith10Column.jrxml"};
			if(JrxmlPath==null||JrxmlPath.length()==0||JrxmlPath.equalsIgnoreCase("<Default>")) {
				String JrxmlTemplateName=null;
				if(columnsSelected.length>0) {
					JrxmlTemplateName=JrxmlTemplate[(columnsSelected.length-1)];
				}
				filePath=filePath+JrxmlTemplateName;
				printToConsole("file path if jrxml name is not passed "+filePath);
			}else {
				filePath = filePath + "UserSelectedReport" + Util.pathSep + "JasperReport.jrxml";
				printToConsole("file path if jrxml name is passed "+filePath);
				File dstFile = new File(filePath);
				printToConsole("Is UserSelectedReport report exist BF:"+dstFile.getParentFile().exists());
				if (!dstFile.getParentFile().exists())
					dstFile.getParentFile().mkdirs();
				printToConsole("Is UserSelectedReport report exist AF:"+dstFile.getParentFile().exists());
				String srvFile = checkPath(dstFile);
				printToConsole("Server path :"+srvFile);
				vwdoc.setDstFile(srvFile);
				int ret = vwdoc.writeContents();
				printToConsole("Client file copied to server :"+ret);
			}
			
			while (value.hasNext()) { 
	    		
	            //arr= (String) value.next();//storing each row into string
				finarr=value.next().toString().split("\t");
				finval.add(finarr);//adding the each row to list
	            //printToConsole("after split of resultset object "+finval.toString());
	        } 
			
			printToConsole("columnDescription >>>>" + columnDescription);
			String[] col=columnDescription.split("\t");			
			List<Map<String,Object>> m=new ArrayList<>(resultSet.size());
			printToConsole("resultSet.size() ::::::: "+resultSet.size());
			
			//Map<String,Object>[] ls=new Map[resultSet.size()];
			Map<String,Object> ls = null;
			
			String[] columnsList= {"Column1","Column2","Column3","Column4","Column5","Column6","Column7","Column8","Column9","Column10"};
			for(int i=0;i<finval.size();i++) {
				printToConsole("i >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+i);				
				ls=VWRouteJasperReport.getMapObject();
				String[] temp=finval.get(i);
				for(int j=0;j<col.length;j++) {
					//String[] Temp=finval.get(i);
					if (temp != null && temp.length > j) {
						ls.put(columnsList[j],temp[j]);
					}
				}				
				m.add(ls);
			}
			/*for(int i=0;i<m.size();i++) {
				printToConsole("values in map object"+m.get(i));
			}*/
			
			printToConsole("output format selected "+outputformat);
			JRDataSource ds = new JRBeanCollectionDataSource(m);	
			String exportPath=reportLocation;
			Map<String,Object> para=new HashMap<>();
			para.put("CollectionBeanParameter", ds);
			for(int i=0;i<col.length;i++) {
				printToConsole("column name "+col[i]);
				para.put("ColumnName"+(i+1), col[i]);
			}
			InputStream input = null;
			try {
				printToConsole("filePath :"+filePath);
				printToConsole("is jrxml file exist  :"+new File(filePath).exists());
				input = new FileInputStream(new File(filePath));
				//input = new FileInputStream(new File("C:\\Test\\JasperReport.jrxml"));
				printToConsole("input file with jrxml path :::::"+filePath);
				
				//input=JRLoader.getResourceInputStream("C:\\Program Files (x86)\\Contentverse\\Reports\\mssql\\JasperReport.jrxml");
				printToConsole("input :"+input);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				printToConsole("Exception while reading file  :"+e);
			}
			
			JasperDesign jd = null;
			try {
				jd = JRXmlLoader.load(input);
				printToConsole("JasperDesign object :"+jd);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				printToConsole("Exception while loading the file  :"+e);
			}
			JasperReport jr = null;
			try {
				jr = JasperCompileManager.compileReport(jd);
				//jr=JasperCompileManager.compileReport(new FileInputStream(new File("C:\\Program Files (x86)\\Contentverse\\Reports\\mssql\\JasperReport\\JasperReport.jrxml")));
				printToConsole("After calling compileReport :"+jr);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				printToConsole("Exception while compiling general report :"+e);
				e.printStackTrace();
			}
			
			JasperPrint jp = null;
			try {
				printToConsole("before calling jasper print object :::::"+jr);
				jp = JasperFillManager.fillReport(jr, para, new JREmptyDataSource());
				printToConsole("jasper print object :::::"+jp);
				//jp.setOrientation(OrientationEnum.LANDSCAPE);
			} catch (JRException e) {
				// TODO Auto-generated catch block
				printToConsole("exception at jasper repoos :::::"+e.getMessage());
				e.printStackTrace();
			}
			//JasperViewer.viewReport(jp);
			try {
				if(isPreviewSelected) {
					printToConsole("inside jasper viewer ");				
					jasperPreview(jp);
				}else if (outputformat.equalsIgnoreCase("pdf")) {
					//path = path+".pdf";
					//exportReport(jprint, path);
					printToConsole("file name for pdf export "+exportPath);
					exportReport(jp,exportPath);
			    } else if (outputformat.equalsIgnoreCase("html")) {
			    	//path = path+".html";
			    	printToConsole("file name for html export "+exportPath);
			    	exportReportHtml(jp, exportPath);
			    } else if (outputformat.equalsIgnoreCase("xls")) {
			    	//path = path+".xls";
			    	printToConsole("file name for excel export "+exportPath);
			    	exportReportXls(jp, exportPath);
			    } else if (outputformat.equalsIgnoreCase("csv")) {
			    	printToConsole("file name for csv export "+exportPath);
			    	exportReportCsv(jp, exportPath);
			    }else {
			    	jasperPreview(jp);
			    }
				//JasperExportManager.exportReportToPdfFile(jp,"C:\\Users\\nkgdusari\\AppData\\Roaming\\Contentverse\\Tempo.pdf");
			}
				
			 catch (JRException | FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} finally {
				try {
					if (input != null)
						input.close();
				} catch (Exception e) {
				}
			}
			if(jp!=null) {
				isReportGenerated=true;
			}
		} catch (Exception e) {
			printToConsole("Exception in dynamic jasper generation :"+e);
		}
		return isReportGenerated;
		
	}
	
	public static void jasperPreview(JasperPrint jp) {
		printToConsole("jasper viewr method is called ");
		//JasperViewer jv = new JasperViewer( jp, false );
		JasperViewer jViewer = new JasperViewer(jp, false);
		if (jp.getPages().isEmpty()) {
		    // JasperViewer constructor displayed "the document has no pages" dialog
		    // so rather than displaying the empty JasperViewer simply return.
			printToConsole("jasper view checking if pages available "+jp.getPages().isEmpty());
		    		}
		jViewer.setVisible(true);
	}
	public static Map<String,Object> getMapObject(){
    	return new HashMap<String,Object>();
    }
	public static void exportReportHtml(JasperPrint jp, String path) throws JRException, FileNotFoundException {
		//exportReport(jp,path);
		
		File outputFile = new File(path);
	    File parentFile = outputFile.getParentFile();
	    if (parentFile != null)
	        parentFile.mkdirs();
	    FileOutputStream fos = new FileOutputStream(outputFile);
	    PrintWriter out = new PrintWriter(fos);
	    HtmlExporter exporter = new HtmlExporter();
	    exporter.setExporterInput(new SimpleExporterInput(jp));
		
	    exporter.setExporterOutput(new SimpleHtmlExporterOutput(out));
	
		/*SimpleHtmlExporterConfiguration exporterConfig = new SimpleHtmlExporterConfiguration();
		exporterConfig.setBetweenPagesHtml("");
		exporter.setConfiguration(exporterConfig);
		SimpleHtmlReportConfiguration reportConfig = new SimpleHtmlReportConfiguration();
		reportConfig.setRemoveEmptySpaceBetweenRows(true);*/
		//exporter.setConfiguration(reportConfig);
	    //exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN,  false);
	    //exporter.setParameter(JRExporterParameter.JASPER_PRINT, jp);
	    //exporter.setParameter(JRExporterParameter.OUTPUT_WRITER, out);
	    exporter.exportReport();
	    out.flush();
	    out.close();
	    System.out.println("HTML Report exported: " + path);
	    printToConsole("HTML Report exported: " + path);
    }
	
	public static void exportReport(JasperPrint jp, String path) throws JRException, FileNotFoundException {
		printToConsole("Exporting pdf report to: " + path);
		String p=path.replace(".html", ".pdf");
    	//JasperExportManager.exportReportToPdf(jp);
        JRPdfExporter exporter = new JRPdfExporter(DefaultJasperReportsContext.getInstance());
        try {
	        File outputFile = new File(p);
	        File parentFile = outputFile.getParentFile();
	        if (parentFile != null)
	            parentFile.mkdirs();
	        printToConsole("parentFile.isExist : " + parentFile.exists());
	        FileOutputStream fos = new FileOutputStream(outputFile);
	
	        SimpleExporterInput simpleExporterInput = new SimpleExporterInput(jp);
	        OutputStreamExporterOutput simpleOutputStreamExporterOutput = new SimpleOutputStreamExporterOutput(fos);
	        exporter.setExporterInput(simpleExporterInput);
	        exporter.setExporterOutput(simpleOutputStreamExporterOutput);
	
	        exporter.exportReport();
        } catch (Exception e) {
        	System.out.println("Exception in pdf generation...."+e.getMessage());
        }
        printToConsole("Report exported: " + path);
    }
	
	

	public static void exportReportXls(JasperPrint jp, String path) throws JRException, FileNotFoundException {
       /* JRXlsExporter exporter = new JRXlsExporter();

        File outputFile = new File(path);
        File parentFile = outputFile.getParentFile();
        if (parentFile != null)
            parentFile.mkdirs();
        FileOutputStream fos = new FileOutputStream(outputFile);

        exporter.setConfiguration(configuration);

        SimpleExporterInput simpleExporterInput = new SimpleExporterInput(jp);
        OutputStreamExporterOutput simpleOutputStreamExporterOutput = new SimpleOutputStreamExporterOutput(fos);

        exporter.setExporterInput(simpleExporterInput);
        exporter.setExporterOutput(simpleOutputStreamExporterOutput);

        exporter.exportReport();*/
		
		/*File outputFile = new File(path); this is working code
        SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
        configuration.setOnePagePerSheet(true);
        configuration.setIgnoreGraphics(false);

        try (ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
             OutputStream fileOutputStream = new FileOutputStream(outputFile)) {
            Exporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(jp));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArrayOutputStream));
            exporter.setConfiguration(configuration);
            exporter.exportReport();
            byteArrayOutputStream.writeTo(fileOutputStream);
        } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		//ByteArrayOutputStream xlsReport = new ByteArrayOutputStream();
	      ByteArrayOutputStream output = new ByteArrayOutputStream();
		JRXlsxExporter exporter = new JRXlsxExporter();
		exporter.setExporterInput(new SimpleExporterInput(jp));
		//File outputFile = new File(path);
		OutputStream outputfile= new FileOutputStream(new File(path));
		exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputfile));
		//exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(xlsReport));
		SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration(); 
		configuration.setDetectCellType(true);//Set configuration as you like it!!
		configuration.setCollapseRowSpan(false);
		exporter.setConfiguration(configuration);
		exporter.exportReport();
		 try {
			outputfile.write(output.toByteArray());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	protected static JRXlsExporter getXlsExporter()
	{
		return new JRXlsExporter(DefaultJasperReportsContext.getInstance());
	}
	//public static void exportReportXls(JasperPrint jp, String path) throws JRException, FileNotFoundException {
       
		
		/*SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();
        configuration.setDetectCellType(true);
        configuration.setWhitePageBackground(false);
        configuration.setIgnoreGraphics(false);
        configuration.setIgnorePageMargins(true);

        exportReportXls(jp, path, configuration);*/
	//}
	/*public static void exportReportCsv(JasperPrint jp, String path) {
		try {
			JRCsvExporter exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jp));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(new File(path)));
			SimpleCsvExporterConfiguration configuration = new SimpleCsvExporterConfiguration();
			configuration.setRecordDelimiter("\r\n");
			exporter.setConfiguration(configuration);
			exporter.exportReport();
		} catch(Exception e) {
			printToConsole("Exception in exportReportCsv :"+e.getMessage());
		}
	}*/

	
	
	
	public static void exportReportCsv(JasperPrint jp, String path) {
		try {
			JRCsvExporter exporter = new JRCsvExporter();
			exporter.setExporterInput(new SimpleExporterInput(jp));
			exporter.setExporterOutput(new SimpleWriterExporterOutput(new File(path)));
			SimpleCsvExporterConfiguration configuration = new SimpleCsvExporterConfiguration();
			configuration.setRecordDelimiter("\r\n");
			exporter.setConfiguration(configuration);
			exporter.exportReport();
		} catch(Exception e) {
			printToConsole("Exception in exportReportCsv :"+e.getMessage());
		}
	}
	
	public static void main(String[] args) {
		System.out.println("inside main method....");
		//VWRouteJasperReport a = new VWRouteJasperReport();
		try {
	        String dt = "04/01/2009 02:39:09 PM";
	        SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
	        Date date = format1.parse(dt);
	        System.out.println("date >>>>> "+date);
			System.out.println("xxxx :"+new SimpleDateFormat("YYYY-MM-DD HH24:MI:SS").format(date));
		} catch (Exception e) {
			System.out.println("Exception in date formate "+e);
		}
		//a.generateJasperReport();
		//a.generatePendencyJasperReport(0);
		//a.generateStaticJasperReport(0, null, "Workflow completed report", "html", null, null, "D:\\JasperReportOutput", null, "Workflow completed report.html");
	}
	
	public static void printToConsole(String msg)
	{
    	try
    	{
        	
        		String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss.SSS").format(Calendar.getInstance().getTime());
				msg = now + " : " + msg + " \n";    			
				java.util.Properties props = System.getProperties(); 
		     	String filepath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + "Contentverse" + File.separator + "VWClient.log";
		     	filepath = "C:\\jasperLogs.log";
				File log = new File(filepath);        				                                    
		        FileOutputStream fos = new FileOutputStream(log, true);
		        fos.write(msg.getBytes());
		        fos.close();            
		        fos.close();
		        log = null;  
 
        	
        }catch (Exception e)
        {
        	System.out.println( "printToConsole");	
        }
	}
	
	public static String checkPath(File file){
		String dstFile = file.getAbsolutePath();
		if (dstFile.indexOf("\\") != -1 && dstFile.indexOf("/") != -1) {
			String newdstFile = "";
			try {
				newdstFile = dstFile;
				/*
				 * temp.replaceAll("\\", "/"); This replaceAll will not work and throws
				 * PatternSyntaxException To resolve this problem is it must be
				 * temp.replaceAll("\\\\","/");
				 */
				if ((File.separatorChar == '/') && newdstFile.indexOf("\\") != -1) {
					newdstFile = newdstFile.replaceAll("\\\\", "/");
				}
				if ((File.separatorChar == '\\') && newdstFile.indexOf("/") != -1) {
					newdstFile = newdstFile.replaceAll("/", "\\\\");
				}
				dstFile = newdstFile;
			} catch (Exception ex) {
				
			}
		}
		return dstFile;
	 }
}
