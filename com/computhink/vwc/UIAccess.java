/*
 * UIPrincipal.java
 *
 * Created on March 18, 2004, 11:49 AM
 */
package com.computhink.vwc;
import com.computhink.common.VWAccess;
import com.computhink.vwc.image.*;
import com.computhink.resource.ResourceManager;


/**
 *
 * @author  Administrator
 */
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;

public class UIAccess implements VWCConstants
{
    private Vector lblPerms = new Vector();
    private VWAccess access = new VWAccess();
    private ResourceManager connectorManager = ResourceManager.getDefaultManager();
    //Enhancement for Security - No 32
    public UIAccess(boolean document, boolean withListener)
    {
        int x = 15; int y = 1; int w = 200; int h = 30;
        int gap = 7;
        JLabel perm = null;
        for (int i = 0; i < 13; i++) //for (int i = 0; i < 7; i++)
        {   
            switch(i)
            {
                case 0: 
                    perm = new JLabel(connectorManager.getString("security.Assign")); 
                    break;
                case 1: 
                    perm = new JLabel(connectorManager.getString("security.Delete"));
                    //x+=30;
                    break;
                case 2: 
                    perm = new JLabel(connectorManager.getString("security.Share")); 
                    break;
                case 3: 
                    perm = new JLabel(connectorManager.getString("security.Launch"));
                    break;                    
                case 4: 
                    perm = new JLabel(connectorManager.getString("security.Export"));
                    break;
                case 5: 
                    perm = new JLabel(connectorManager.getString("security.Print"));
                    break;
                case 6: 
                    perm = new JLabel(connectorManager.getString("security.Mail"));
                    break;
                case 7: 
                    perm = new JLabel(connectorManager.getString("security.Send_To"));
                    break;    
                case 8: 
                    perm = new JLabel(connectorManager.getString("security.Check_In_Out"));
                    break;                      
                case 9: 
                    perm = new JLabel(connectorManager.getString("security.Modify")); 
                    break;
                case 10: 
                    perm = new JLabel(connectorManager.getString("security.Create"));
                    if (document) perm.setEnabled(false);
                    break;
                case 11: 
                    perm = new JLabel(connectorManager.getString("security.View")); 
                    break;
                case 12: 
                    perm = new JLabel(connectorManager.getString("security.Navigate"));
                    if (document) perm.setEnabled(false);
                    break;
            }
            perm.setFont(new Font("Arial", 1, 10));
            perm.setIconTextGap(gap);
            x = 15;//(i > 2 && i < 9 ?40:15);
            perm.setBounds(x, y, 16, h);
           	y+=19;  
            lblPerms.addElement(perm);
            if (document && (i == 10 || i == 12)) continue;//if (document && (i==4 || i==6)) continue;
            
            if (withListener)
            {            	
                perm.addMouseListener(new MouseAdapter() {
                    public void mouseClicked(MouseEvent evt) {
                        resolvePermissions(evt);
                    }
                });
            }else{
            	perm.setForeground(Color.GRAY);
            }
        }
        quickAssign("Navigator");
    }
    private void resolvePermissions(MouseEvent evt) 
    {
        JLabel permission = (JLabel) evt.getSource();
        String pName = permission.getText();
        String state = permission.getToolTipText();
  
        if (state.equals(connectorManager.getString("security.Granted"))) deny(permission);
        else
        if (state.equals(connectorManager.getString("security.Denied"))) idle(permission);
        else
        if (state.equals(connectorManager.getString("security.Resolve"))) grant(permission);
        
        state = permission.getToolTipText();
        if (pName.equals(connectorManager.getString("security.Assign")) && state.equals(connectorManager.getString("security.Granted")))
        {
            for (int i = 1; i < 13; i++)  grant((JLabel) lblPerms.get(i)); //for (int i = 1; i < 7; i++)  
            return;
        }
        if (pName.equals(connectorManager.getString("security.Assign")) && state.equals(connectorManager.getString("security.Denied")))
        {
            for (int i = 1; i < 13; i++)  deny((JLabel) lblPerms.get(i)); //for (int i = 1; i < 7; i++)  
            return;
        }
        if (pName.equals(connectorManager.getString("security.Navigate")) && state.equals(connectorManager.getString("security.Denied")))
        {
            for (int i = 0; i < 13; i++)  deny((JLabel) lblPerms.get(i));//for (int i = 0; i < 7; i++)  
            return;
        }
        if (pName.equals(connectorManager.getString("security.View")) && state.equals(connectorManager.getString("security.Denied")))
        {
            for (int i = 10; i > 0; i--)  deny((JLabel) lblPerms.get(i)); //for (int i = 4; i > 0; i++)  
            return;
        }       
        if ( pName.equals(connectorManager.getString("security.Share")) && state.equals(connectorManager.getString("security.Denied")))
        {
        		for (int i = 3; i < 9; i++)  deny((JLabel) lblPerms.get(i)); 
                return;
        }
        if ( pName.equals(connectorManager.getString("security.Share")) && state.equals(connectorManager.getString("security.Granted")))
        {
        		for (int i = 3; i < 9; i++)  grant((JLabel) lblPerms.get(i));
                for (int i = 11; i < 13; i++)  grant((JLabel) lblPerms.get(i)); // for (int i = 5; i < 7; i++)

        		return;
        }
        if (!pName.equals(connectorManager.getString("security.Share")) && checkShare()) {
        	//((JLabel) lblPerms.get(2)).setEnabled(false);
        	grant((JLabel) lblPerms.get(2));
        }
        if(!pName.equals(connectorManager.getString("security.Share")) && !checkShare()) deny((JLabel) lblPerms.get(2));
        
        if (!pName.equals(connectorManager.getString("security.Navigate")) && state.equals(connectorManager.getString("security.Granted")))
        {
            for (int i = 11; i < 13; i++)  grant((JLabel) lblPerms.get(i)); // for (int i = 5; i < 7; i++)
            return;
        }
        if ( !pName.equals(connectorManager.getString("security.Assign")) && state.equals(connectorManager.getString("security.Denied")))
        {
                deny((JLabel) lblPerms.get(0));
                return;
        }

    }
    private boolean checkShare(){
    	boolean flag = false;
    	for (int i = 3; i < 9; i++)
    	if(((JLabel) lblPerms.get(i)).getToolTipText().equals(connectorManager.getString("security.Granted"))){
    		flag = true;    		
    	}
    	return flag;
    }
    private void setState(JLabel perm, int state)
    {
        if (perm.getText().equals(connectorManager.getString("security.Navigate"))) access.setNavigate(state);
        else 
        if (perm.getText().equals(connectorManager.getString("security.View"))) access.setView(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Create"))) access.setCreate(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Modify"))) access.setModify(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Share"))) access.setShare(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Delete"))) access.setDelete(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Assign"))) access.setAssign(state);
        /// Added for Enhancement 32
        else
        if (perm.getText().equals(connectorManager.getString("security.Launch"))) access.setLaunch(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Export"))) access.setExport(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Print"))) access.setPrint(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Mail"))) access.setMail(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Send_To"))) access.setSendTo(state);
        else
        if (perm.getText().equals(connectorManager.getString("security.Check_In_Out"))) access.setCheckInOut(state);

        // Added for Enhancement 32
        	
    }
    private void grant(JLabel perm)
    {
        setState(perm, 1);
        perm.setIcon(Images.grn);
        perm.setToolTipText(connectorManager.getString("security.Granted"));
    }
    private void idle(JLabel perm)
    {
        setState(perm, 2);
        perm.setIcon(Images.idl);
        perm.setToolTipText(connectorManager.getString("security.Resolve"));
    }
    private void deny(JLabel perm)
    {
        setState(perm, 0);
        perm.setIcon(Images.dny);
        perm.setToolTipText(connectorManager.getString("security.Denied"));
    }

    public Vector getPermissions()
    {
        return this.lblPerms;
    }
    public VWAccess getAccess()
    {
        return this.access;
    }
    public void setAccess(VWAccess access)
    {
        /*set((JLabel) lblPerms.get(0), access.canAssign());
        set((JLabel) lblPerms.get(1), access.canDelete());
        set((JLabel) lblPerms.get(2), access.canShare());
        set((JLabel) lblPerms.get(3), access.canModify());
        set((JLabel) lblPerms.get(4), access.canCreate());
        set((JLabel) lblPerms.get(5), access.canView());
        set((JLabel) lblPerms.get(6), access.canNavigate());*/

        set((JLabel) lblPerms.get(0), access.canAssign());
        set((JLabel) lblPerms.get(1), access.canDelete());
        set((JLabel) lblPerms.get(2), access.canShare());
        // New
        set((JLabel) lblPerms.get(3), access.canLaunch());
        set((JLabel) lblPerms.get(4), access.canExport());
        set((JLabel) lblPerms.get(5), access.canPrint());
        set((JLabel) lblPerms.get(6), access.canMail());
        set((JLabel) lblPerms.get(7), access.canSendTo());
        set((JLabel) lblPerms.get(8), access.canCheckInOut());
        // New
        set((JLabel) lblPerms.get(9),  access.canModify());
        set((JLabel) lblPerms.get(10), access.canCreate());
        set((JLabel) lblPerms.get(11), access.canView());
        set((JLabel) lblPerms.get(12), access.canNavigate());
    	
    }
    private void set(JLabel perm, int state)
    {
        switch (state)
        {
            case 0: deny(perm); break;
            case 1: grant(perm); break;
            case 2: idle(perm); break;
        }
    }
    public void quickAssign(String assign)
    {
        if (assign.equals(AA_SUPERVISOR))
        {
            /*for (int i = 6; i >= 0; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }*/
            for (int i = 11; i >= 0; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
        }
        else
        if (assign.equals(AA_FACCESS_DENYOTHERS))
        {
            /*for (int i = 6; i >= 1 ; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
            deny((JLabel) lblPerms.get(0));*/
        	 for (int i = 11; i >= 1 ; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
            deny((JLabel) lblPerms.get(0)); 
        }
        else
        if (assign.equals(AA_FACCESS_LEAVEOTHERS))
        {
            /*for (int i = 6; i >= 1 ; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
            idle((JLabel) lblPerms.get(0));*/
        	 for (int i = 11; i >= 1 ; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
            idle((JLabel) lblPerms.get(0)); 
        	
        }
        else
        if (assign.equals(AA_INPUT_DENYOTHERS))
        {
           /* for (int i = 6; i >= 3; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
            deny((JLabel) lblPerms.get(2)); 
            deny((JLabel) lblPerms.get(1)); 
            deny((JLabel) lblPerms.get(0));*/
        	for (int i = 10; i >= 6; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
        	deny((JLabel) lblPerms.get(8)); 
        	deny((JLabel) lblPerms.get(7)); 
        	deny((JLabel) lblPerms.get(6)); 
        	deny((JLabel) lblPerms.get(5)); 
        	deny((JLabel) lblPerms.get(4)); 
        	deny((JLabel) lblPerms.get(3)); 
            deny((JLabel) lblPerms.get(2)); 
            deny((JLabel) lblPerms.get(1)); 
            deny((JLabel) lblPerms.get(0)); 
        	
        }
        else
        if (assign.equals(AA_INPUT_LEAVEOTHERS))
        {
           /* for (int i = 6; i >= 3; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
            idle((JLabel) lblPerms.get(2)); 
            idle((JLabel) lblPerms.get(1)); 
            idle((JLabel) lblPerms.get(0));*/
        	for (int i = 10; i >= 6; i--)
            {
                grant((JLabel) lblPerms.get(i)); 
            }
        	idle((JLabel) lblPerms.get(8));
        	idle((JLabel) lblPerms.get(7));
        	idle((JLabel) lblPerms.get(6)); 
	    	idle((JLabel) lblPerms.get(5)); 
	    	idle((JLabel) lblPerms.get(4)); 
	       	idle((JLabel) lblPerms.get(3)); 
            idle((JLabel) lblPerms.get(2)); 
            idle((JLabel) lblPerms.get(1)); 
            idle((JLabel) lblPerms.get(0)); 
        	
        }
        else
        if (assign.equals(AA_BROWSE_DENYOTHERS))
        {
        	//It should grant Navigate and View, and deny all others
            /*for (int i = 0; i <= 4; i++)
            {
                deny((JLabel) lblPerms.get(i)); 
            }
            grant((JLabel) lblPerms.get(5)); 
            grant((JLabel) lblPerms.get(6));*/
        	for (int i = 0; i <= 10; i++)
            {
                deny((JLabel) lblPerms.get(i)); 
            }
            grant((JLabel) lblPerms.get(11)); 
            grant((JLabel) lblPerms.get(12));
        	
        }
        else
        if (assign.equals(AA_BROWSE_LEAVEOTHERS))
        {
        	//It should grant Navigate and View, and set all others to resolve. 
            /*for (int i = 0; i <= 4; i++)
            {
                idle((JLabel) lblPerms.get(i)); 
            }
            grant((JLabel) lblPerms.get(5)); 
            grant((JLabel) lblPerms.get(6));*/
	    	for (int i = 0; i <= 10; i++)
	        {
	            idle((JLabel) lblPerms.get(i)); 
	        }
	        grant((JLabel) lblPerms.get(11)); 
	        grant((JLabel) lblPerms.get(12));         	
        }
        else
        if (assign.equals("Navigator"))
        {
        	// It should be Navigate granted, all others denied.
        	
            /*for (int i = 0; i <= 5; i++)
            {
                deny((JLabel) lblPerms.get(i)); 
            }
            grant((JLabel) lblPerms.get(6));*/
	      	for (int i = 0; i <= 11; i++)
            {
                deny((JLabel) lblPerms.get(i)); 
            }
	        grant((JLabel) lblPerms.get(12));
        	
        }
    }

}


