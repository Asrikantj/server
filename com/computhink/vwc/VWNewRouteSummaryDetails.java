package com.computhink.vwc;

import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import com.computhink.resource.ResourceManager;
import com.computhink.common.Constants;
import com.computhink.vwc.image.Images;

public class VWNewRouteSummaryDetails extends JDialog implements VWCConstants{
    
    JLabel lblRouteName, lblUserName, lblReceivedOn, lblProcessedOn, lblComments, lblTaskName, lblRouteNameVal, lblUserNameVal, lblReceivedOnVal, lblProcessedOnVal, lblTaskNameVal, lblVersion, lblVersionVal ,lblReference ,lblReferenceVal;
    JButton bOk;
    JTextArea txtComments;
    JScrollPane scComments;
    VWNewRouteSummary summaryInfo;
    private static ResourceManager resourceManager=ResourceManager.getDefaultManager();
    JPanel pnlData = new JPanel(); 
    public VWNewRouteSummaryDetails() {
	// TODO Auto-generated constructor stub
	super();
	setModal(true);
	initComponents();
	//setSize(500, 350);
	setVisible(true);
	setDefaultCloseOperation(DISPOSE_ON_CLOSE);	
    }	
    VWNewRouteSummaryDetails(JFrame parent, VWNewRouteSummary parent2){
	super(parent, true);
	this.summaryInfo  = parent2;
	initComponents();
	//setTitle(resourceManager.getString("NewRouteSummary.Title") + parent2.getRouteData(COL_ROUTENAME) + ": " + parent2.getRouteData(COL_TASK));
	setTitle(resourceManager.getString("routeaction.RouteNewColNamesDlg5") + "  " + resourceManager.getString("routeaction.RouteNewColNamesDlg6")  + parent2.getRouteData(COL_TASK));
	setIconImage(Images.task.getImage());
	setSize(600, 550);
	//setLocation(parent.getX(), parent.getY());
	setLocation(parent.getWidth()/2, (parent.getHeight()/2)-100);
	setResizable(false);
	setVisible(true);
	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	
    }
    public void initComponents(){
	getContentPane().add(pnlData);
	pnlData.setLayout(null);
	
	lblRouteName = new JLabel(resourceManager.getString("WORKFLOW_NAME") +" "+resourceManager.getString("RouteSummary.Name")+" ");
	lblRouteName.setBounds(7, 8,350, 24);
	//pnlData.add(lblRouteName);

	
	lblRouteNameVal = new JLabel("	"+resourceManager.getString("WORKFLOW_NAME") +" "+resourceManager.getString("RouteSummary.NameVal")+" ");
	lblRouteNameVal.setBounds(100, 8, 300, 24);
	//pnlData.add(lblRouteNameVal);
	lblTaskName = new JLabel(resourceManager.getString("routeaction.RouteNewColNamesDlg1")+"  "+resourceManager.getString("routeaction.RouteNewColNamesDlg2"));
	lblTaskName.setBounds(8, 8,420, 24);
	pnlData.add(lblTaskName);
	//Font font = lblTaskName.getFont();
	lblTaskNameVal = new JLabel(" "+resourceManager.getString("RouteSummaryLbl.TaskNameVal"));
	lblTaskNameVal.setBounds(8,24, 400, 24);
	pnlData.add(lblTaskNameVal);


	lblReceivedOn = new JLabel(resourceManager.getString("routeaction.RouteNewColNamesDlg3")+"   "+resourceManager.getString("routeaction.RouteNewColNamesDlg4"));
	lblReceivedOn.setBounds(8,54,400, 24);
	pnlData.add(lblReceivedOn);

	lblReceivedOnVal  = new JLabel(resourceManager.getString("RouteSummaryLbl.ReceivedOnVal"));
	lblReceivedOnVal.setBounds(8,70, 164, 24);
	pnlData.add(lblReceivedOnVal);

	
	lblProcessedOn = new JLabel(resourceManager.getString("routeaction.RouteNewColNamesDlg7")+"   "+resourceManager.getString("routeaction.RouteNewColNamesDlg8"));
	lblProcessedOn.setBounds(8, 100, 400, 24);
	pnlData.add(lblProcessedOn);
	
	lblProcessedOnVal = new JLabel(resourceManager.getString("RouteSummaryLbl.ProcessedOnVal"));
	lblProcessedOnVal.setBounds(8, 116, 164, 24);
	pnlData.add(lblProcessedOnVal);

	
	lblUserName = new JLabel(resourceManager.getString("routeaction.RouteNewColNamesDlg9")+"   "+resourceManager.getString("routeaction.RouteNewColNamesDlg10"));
	lblUserName.setBounds(8, 146, 400, 24);
	pnlData.add(lblUserName);
	
	lblUserNameVal = new JLabel(resourceManager.getString("RouteSummaryLbl.UserNameVal"));
	lblUserNameVal.setBounds(8, 162, 400, 24);
	pnlData.add(lblUserNameVal);

	
	lblVersion = new JLabel(resourceManager.getString("routeaction.RouteNewColNamesDlg13")+"   "+resourceManager.getString("routeaction.RouteNewColNamesDlg14"));
	lblVersion.setBounds(8, 192, 400, 24);
	pnlData.add(lblVersion);
	
	lblVersionVal = new JLabel(resourceManager.getString("RouteSummaryLbl.VersionVal"));
	lblVersionVal.setBounds(8, 208, 400, 24);
	pnlData.add(lblVersionVal);
	
	lblReference = new JLabel(resourceManager.getString("routeaction.RouteNewColNamesDlg15")+"   "+resourceManager.getString("routeaction.RouteNewColNamesDlg16"));
	lblReference.setBounds(8, 238, 400, 24);
	pnlData.add(lblReference);
	
	lblReferenceVal = new JLabel(resourceManager.getString("RouteSummaryLbl.ReferenceVal"));
	lblReferenceVal.setBounds(8, 254, 400, 24);
	pnlData.add(lblReferenceVal);

	
	lblComments = new JLabel(resourceManager.getString("routeaction.RouteNewColNamesDlg11")+"   "+resourceManager.getString("routeaction.RouteNewColNamesDlg12"));
	lblComments.setBounds(8, 284, 400, 24);
	pnlData.add(lblComments);
	lblComments.setAutoscrolls(true);
	
	txtComments = new JTextArea();
	//txtComments.setText("A strapping fast bowler born in Australia, Johnston's career began at New South Wales where he played alongside the likes of Mark Taylor, Michael Slater and Brett Lee, before choosing to represent Ireland. A positive captain who led from the front, he had little problem motivating his side - as demonstrated when Ireland bounced back from a disappointing World Cricket League in Kenya, where they finished 5th, to demolish the United Arab Emirates and qualify for the final of the Intercontinental Cup. He tore the heart out of the UAE batting line up in their second innings taking three for eight from his eight overs as Ireland recorded a morale-boosting victory by an innings and 170 runs. He led Ireland to a thrilling tie against Zimbabwe in their first match of the 2007 World Cup before beating Pakistan - Johnston hit the winning runs - to record one of the biggest upsets in history.");
	
	txtComments.setLineWrap(true);
	txtComments.setEditable(false);
	txtComments.setBackground(getBackground());
	txtComments.setAutoscrolls(true);
	
	
	scComments = new JScrollPane();
	scComments.setViewportView(txtComments);
	scComments.setBounds(8, 300, 550, 600);
	scComments.setBorder(null);
	scComments.setAutoscrolls(true);
	pnlData.add(scComments);
	
	/*lblRouteName.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblTaskName.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblReceivedOn.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblUserName.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblProcessedOn.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblComments.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));*/
	
	bOk = new JButton(resourceManager.getString("RouteSummaryBtn.Ok"));
	bOk.setBounds(370, 190, 65, 24);
	//pnlData.add(bOk);
	
	
	bOk.addActionListener(new ActionListener(){
	    public void actionPerformed(ActionEvent e){
		setVisible(false);
		dispose();
	    }
	});
	setDetails();
    }
    
    public void setDetails(){
	lblRouteNameVal.setText(summaryInfo.getRouteData(9));
	lblTaskNameVal.setText(summaryInfo.getRouteData(0));
	lblReceivedOnVal.setText(summaryInfo.getRouteData(1));
	lblUserNameVal.setText(summaryInfo.getRouteData(6));
	lblProcessedOnVal.setText(summaryInfo.getRouteData(3));	
	lblVersionVal.setText(summaryInfo.getRouteData(5));
	lblReferenceVal.setText(summaryInfo.getRouteData(7));
	txtComments.setText(summaryInfo.getRouteData(4));
	
    }
    public static void main(String[] args) {
	try{
	    String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
	    String windowsLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
	    UIManager.setLookAndFeel(plasticLookandFeel);        	
	    
	}catch(Exception ex){
	    
	}
	new VWNewRouteSummaryDetails(); 
    }
}
