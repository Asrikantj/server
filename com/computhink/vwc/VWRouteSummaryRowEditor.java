package com.computhink.vwc;

import java.util.EventObject;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.DefaultCellEditor;
import javax.swing.table.TableCellEditor;
import javax.swing.event.CellEditorListener;

public class VWRouteSummaryRowEditor implements TableCellEditor{
	
	protected TableCellEditor editor,defaultEditor;
	protected VWRouteSummaryTable gTable=null;
//	------------------------------------------------------------------------------
	public VWRouteSummaryRowEditor(VWRouteSummaryTable table){
		JTextField text=new JTextField();
		text.setBorder(null);
		defaultEditor = new DefaultCellEditor(text);
		gTable=table;
	}
//	------------------------------------------------------------------------------
	public Component getTableCellEditorComponent(JTable table,
			Object value, boolean isSelected, int row, int column){    
		return defaultEditor.getTableCellEditorComponent(table,value,isSelected,row,column);
	}
//	------------------------------------------------------------------------------
	public Object getCellEditorValue(){
		return editor.getCellEditorValue();
	}
//	------------------------------------------------------------------------------
	public boolean stopCellEditing(){
		return editor.stopCellEditing();
	}
//	------------------------------------------------------------------------------
	public void cancelCellEditing(){
		editor.cancelCellEditing();
	}
//	------------------------------------------------------------------------------
	public boolean isCellEditable(EventObject anEvent){
		return false;
	}
//	------------------------------------------------------------------------------
	public void addCellEditorListener(CellEditorListener l){
		editor.addCellEditorListener(l);
	}
//	------------------------------------------------------------------------------
	public void removeCellEditorListener(CellEditorListener l){
		editor.removeCellEditorListener(l);
	}
//	------------------------------------------------------------------------------
	public boolean shouldSelectCell(EventObject anEvent){
		return editor.shouldSelectCell(anEvent);
	}
//	------------------------------------------------------------------------------
}

