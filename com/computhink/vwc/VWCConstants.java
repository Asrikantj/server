/*
 * VWCConstants.java
 *
 * Created on 22 ����� ������, 2003, 03:23 �
 */
package com.computhink.vwc;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;

/**
 *
 * @author  Administrator
 */
public interface VWCConstants 
{
     public static final String VWC_PREF_ROOT = "/computhink/" + Constants.PRODUCT_NAME.toLowerCase() + "/vwc";
     public static final String VWC_HTTP_AGENT = Constants.PRODUCT_NAME + " Client";
     public static final int DEFAULT_DATA_PORT = 6001;
     public static final int DEFAULT_COM_PORT = 6000;
     public static final int DEFAULT_PROXY_PORT = 8080;
     
     public static final String AA_BROWSE_DENYOTHERS = ResourceManager.getDefaultManager().getString("security.AA_BROWSE_DENYOTHERS");
     public static final String AA_BROWSE_LEAVEOTHERS = ResourceManager.getDefaultManager().getString("security.AA_BROWSE_LEAVEOTHERS");
     
     public static final String AA_INPUT_DENYOTHERS = ResourceManager.getDefaultManager().getString("security.AA_INPUT_DENYOTHERS");
     public static final String AA_INPUT_LEAVEOTHERS = ResourceManager.getDefaultManager().getString("security.AA_INPUT_LEAVEOTHERS");
     
     public static final String AA_FACCESS_DENYOTHERS = ResourceManager.getDefaultManager().getString("security.AA_FACCESS_DENYOTHERS");
     public static final String AA_FACCESS_LEAVEOTHERS = ResourceManager.getDefaultManager().getString("security.AA_FACCESS_LEAVEOTHERS");
     
     public static final String AA_SUPERVISOR = ResourceManager.getDefaultManager().getString("security.AA_SUPERVISOR");
     
     public static final String LIB_VIEWER = "VWView.dll";
     public static final String LIB_SIGN = "VWSign.dll";
     public static final String LIB_CLIENT = "VWJClient.dll";
     public static final String LIB_OUTPUT = "VWOutput.dll";
     /**CV2019 - Added for Final action pdf convertion issue****/
     public static final String LIB_DATABASE = "VWDatabase.dll";
     public static final String LIB_LEAD = "VWLead.dll";
     public static final String LIB_DIALOGENU = "VWDialogENU.dll";
     public static final String LIB_VWDJVU = "VWDJVU.dll";
     public static final String LIB_VWINDEX = "VWIndex.dll";
     public static final String LIB_VWMAGIK = "VWMagik.dll";
     public static final String LIB_VWPDF = "VWPDF.dll";
     
     public static final String LIB_LTRIM1 = "LTIMGCLRU.dll";
     public static final String LIB_LTRIM2 = "LTIMGCORU.dll";
     public static final String LIB_LTRIM3 = "LTIMGEFXU.dll";
     public static final String LIB_LTDLGKRNU = "LTDLGKRNU.dll";
     public static final String LIB_LTDLGIMGEFXU = "LTDLGIMGEFXU.dll";
     public static final String LIB_LTDLGCLRU = "LTDLGCLRU.dll";
     /****End of pdf convertion changes*************************/
     // Document Route Summary Constant     
     String[] RouteColNames={ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_0"), ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_1"), ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_2"),
    		 ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_3"), ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_4"),ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_5"),
    		 ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_6"), ResourceManager.getDefaultManager().getString("WORKFLOW_MODULE_NAME")+" "+ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_7"),ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_8"), ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_9"), ResourceManager.getDefaultManager().getString("routeaction.RouteColNames_10")};
    /**CV2019 merges from SIDBI line***/
     String[] NewRouteColNames={ResourceManager.getDefaultManager().getString("routeaction.RouteNewColNames_0"), ResourceManager.getDefaultManager().getString("routeaction.RouteNewColNames_1"), ResourceManager.getDefaultManager().getString("routeaction.RouteNewColNames_2"),
        		 ResourceManager.getDefaultManager().getString("routeaction.RouteNewColNames_3"), ResourceManager.getDefaultManager().getString("routeaction.RouteNewColNames_4"),ResourceManager.getDefaultManager().getString("routeaction.RouteNewColNames_5"),
        		 ResourceManager.getDefaultManager().getString("routeaction.RouteNewColNames_6"), ResourceManager.getDefaultManager().getString("WORKFLOW_MODULE_NAME")+" "+ResourceManager.getDefaultManager().getString("routeaction.RouteNewColNames_7")};
     /**CV2019 merges - RouteSummary info enhancement***/
     String[] RouteInfoColNames={ResourceManager.getDefaultManager().getString("RouteSummaryInfo.ColNames_0"), ResourceManager.getDefaultManager().getString("RouteSummaryInfo.ColNames_1")};
         
     public static int TASK_DESCRIPTION_COL_INDEX=4;     
     String BTN_RESET_NAME = "Reset";
     String BTN_SAVE_NAME = "Save";
     String BTN_CANCEL_NAME = "Cancel";
     String AdminWise_CurrentDate_Format = "0000";
     String[] YesNoData={"","Yes","No"};
     public static int defaultMsgLifeTime = 48;
     
     public static final int COL_SERIALNUMBER = 0;
     public static final int COL_USERNAME = 1;
     public static final int COL_TASK= 2;
     public static final int COL_COMMENTS= 3;
     public static final int COL_TASKDESC= 4;
     public static final int COL_STATUS = 5;
     public static final int COL_RECEIVEDON = 6;
     public static final int COL_PROCESSEDON = 7;
     public static final int COL_ROUTENAME = 8;	
     
     //DRS
     public static final int INVALIDROUTE = 1;
     public static final int VALIDROUTE = 0;
     
     String[] NotificationColumnNames={ResourceManager.getDefaultManager().getString("notificationpanel.NotificationColumnNames_0"),ResourceManager.getDefaultManager().getString("notificationpanel.NotificationColumnNames_1"),
    		 ResourceManager.getDefaultManager().getString("notificationpanel.NotificationColumnNames_2"),ResourceManager.getDefaultManager().getString("notificationpanel.NotificationColumnNames_3")};
     
     String DEFAULT_ACCEPT_LABEL = "Accept";
     String DEFAULT_REJECT_LABEL = "Reject";
}


