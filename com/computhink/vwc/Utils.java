package com.computhink.vwc;


import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Vector;

import javax.activation.DataHandler;

import com.computhink.common.Comparitor;

public class Utils {
	
    private static final int BUFFER_SIZE = 4096;	
    public static final String SepChar = "\t";
    public static int to_Number(String str)
    {
        try 
        {
            return Integer.parseInt(str);
        }
        catch(Exception e){return 0;}
    }
    
	public static byte[] getBytesFromDataHandler(DataHandler data) throws IOException {
		InputStream in = null;
		byte out[] = null;
		in = data.getInputStream();
		if(in != null) {
			out = new byte[in.available()];
			in.read(out);
		} else {
			out = new byte[0];
		}
		in.close();		
		return out;
	}

	public static boolean writeToFile(String strFilePath, String docName, byte[] data){
		try{
				
			VWClient.printToConsole("Uploaded data length " + data.length);
			VWClient.printToConsole("data strFilePath " + strFilePath);
			File strFile = new File(strFilePath);	
			strFile.getParentFile().mkdirs();
			FileOutputStream fos = new FileOutputStream(strFile);			
			fos.write(data);
			fos.close();
		}catch(Exception ex){
			VWClient.printToConsole("Exception in writeToFile " + ex.getMessage());
			return false;
		}
		return true;
	}
    /**
     * Loads the contents of a file into a java.lang.String
     *
     * @param fileName the file to load into a string
     * @return the contents of the file loaded into String format
     */
    public static String extractContents(String fileName) throws IOException {
        StringBuffer buffer = new StringBuffer();

        Reader reader = new FileReader(fileName);
        char[] cbuf = new char[BUFFER_SIZE];
        try {
            while (reader.read(cbuf) > 0) {
                buffer.append(cbuf);
                // memory is cheap - nullify the old cbuf so GC will collect & create new memory in a 4k chunk
                cbuf = null;
                cbuf = new char[BUFFER_SIZE];
            }
            reader.close();
            return buffer.toString().trim();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Throwable t) {
                // do nothing
            }
        }
    }
    
    public static void deleteDirectory(String directoryName)
    {
        //if(directoryName.length()<3) return;
        File directoryFile = new File(directoryName);
        File[] files = directoryFile.listFiles();
        int fileCount=0;
        if(files!=null) fileCount=files.length;
        for (int i=0;i<fileCount;i++)
        {
            if (files[i].isDirectory())
            {
                deleteDirectory(files[i].getAbsolutePath());
            }
            else
            {
                //Log.verboseDebug("Deleting "+files[i].getAbsolutePath());
                files[i].delete();
            }
        }
        // delete the directory
        directoryFile.delete();        
    }    
	
    public static void maintainLogFiles(String path,int maxCount){
    	
    	String receviedDir = path;
    	String correctDir ="VWWSLog.log";
    	if(receviedDir.equalsIgnoreCase(correctDir)){
    		String dir ="c:\\VWWS\\VWWSLog.log";		
    		File logDir = new File(dir);
    		String[] filenames = logDir.list();       
    		int count=0;
    		HashMap hData = new HashMap();
    		
    		for(int i=0;i<filenames.length;i++){
    			
    			if(filenames[i].startsWith("VWWSLog.log")){
    				if(filenames[i].equalsIgnoreCase("VWWSLog.log"))continue;
    				File f = new File(dir+filenames[i]);
    				count++;
    				try{				         	
    					hData.put(filenames[i],String.valueOf(f.lastModified()));				         	
    				}
    				catch(Exception e){ 			         	
    					
    				}
    			} 
    		}  
    		if(!hData.isEmpty()){
    			ArrayList listManager = new ArrayList(hData.entrySet());
    			Collections.sort(listManager,new Comparitor());
    			if (maxCount!=0){     	
    				
    				if (maxCount<listManager.size()){
    					
    					int i=listManager.size()-1;
    					
    					while(count>maxCount){
    						
    						String fileName = (String)((Map.Entry)listManager.get(i)).getKey();
    						File managerFile = new File(dir+fileName);
    						managerFile.delete();
    						i--;
    						count--;
    						
    					}
    				} 
    			}
    		}
    	}
    }    
    /**
     * getDocPDFList - This method will give the list of pdf files name
     * @param did
     * @param path
     * @return
     */
    public static Vector getDocPDFList(String did, String path){
	Vector<String> docPDFList = new Vector<String>();
	/**
	 * Added for Tomcat8.0 issue which was creating pdf files in webapps/VWWAAttachment folder,
	 * Added to change location to webaaps/VWWA/Attachment,Gurumurthy.T.S
	 */
	if(path.contains("VWWAAttachment")){
		path = path.replace("VWWAAttachment", "VWWA\\Attachment");
	}
	File directoryFile = new File(path + File.separator + did);
	
        File[] files = directoryFile.listFiles();
        int fileCount=0;
        if(files!=null) fileCount=files.length;
        for (int i=0; i<fileCount; i++)
        {
            String fileNameWithExt = files[i].getName();
           // int fileExtInt = fileNameWithExt.indexOf(".");
           // String fileExtention = fileNameWithExt.substring(fileExtInt+1, fileNameWithExt.length());
            //String fileName = fileNameWithExt.substring(0, fileExtInt);
            if (!"VWTimeStamp.txt".equalsIgnoreCase(fileNameWithExt))//("pdf".equalsIgnoreCase(fileExtention))
            {
        	docPDFList.add(fileNameWithExt);
            }
        }
        //System.out.println("docPDFList " + docPDFList.size());
	return docPDFList;
    }
    
/*    public static Vector getLoginProperty() {
	String allIndex = null;
	String userName = null;
	String password = null;
	String server = null;
	String room = null;
	Vector loginProperty = null;
	try {
	    loginProperty = new Vector();
            System.out.println("Coming inside - getProperty()");
            //InputStream ins	= Utilities.class.getResourceAsStream("C:/VWWS/convert2pdf.properties");
             String filePath = "c:\\vwws\\convert2pdf.properties";
             File props = new File(filePath);
             if(props.exists()){    	    
    		BufferedReader reader = new BufferedReader(new FileReader(props));
            System.out.println("After getting the property");
            Properties pro = new Properties();
            pro.load(reader.ge);
            reader.close();
            //allIndex = pro.getProperty("AllIndex");
            userName = pro.getProperty("username");
            password = pro.getProperty("password");
            server = pro.getProperty("server");
            room = pro.getProperty("room");
            System.out.println(" userName is : "+userName);
            System.out.println(" password is : "+password);
            System.out.println(" server is : "+server);
            System.out.println(" server is : "+server);
            
            if((userName!=null && !userName.equals("")) && (password!=null && !password.equals("")) && 
        	    (server!=null && !server.equals("")) && (room!=null && !room.equals("")) ){
                loginProperty.add(userName);
                loginProperty.add(password);
                loginProperty.add(server);
                loginProperty.add(room);
            }
            System.out.println("Value of allIndex is : "+allIndex);
        
        } catch (Exception e) {
            System.out.println("Failed to getProperty "+e.getMessage());
        }

        return loginProperty;
    }*/   
    
    public static boolean CopyFile(File fromFile,File toFile)
    {   
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        try
        {
            in = new BufferedInputStream(new FileInputStream(fromFile));
            out = new BufferedOutputStream(new FileOutputStream(toFile));
            int bufferSize = Math.min((int)fromFile.length(), 1024000);
            byte[] buffer = new byte[bufferSize];
            int numRead;
            while ((numRead = in.read(buffer, 0, buffer.length)) != -1)
            {
                out.write(buffer, 0, numRead);
            }
            out.flush();
            in.close();
        }
        catch (IOException e)
        {//Added to Fix the memoty leak CV83B5 17-12-2015
        	try {
				out.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
            return false;
        }
        finally
        {
            try
            {
                if (in != null) in.close();
                if (out != null) out.close();
            }
            catch (IOException e)
            {//Added to Fix the memoty leak CV83B5 17-12-2015
            	try {
					out.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
            }
        }
        return true;
    }

}
