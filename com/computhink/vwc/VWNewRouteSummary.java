package com.computhink.vwc; 

import java.awt.AWTKeyStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.image.BufferedImage;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import java.util.Map;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.JTable.PrintMode;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.drs.server.DRSConstants;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.image.Images;
import com.computhink.vws.server.Client;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;


public class VWNewRouteSummary extends JPanel implements Runnable, Printable, Constants
{
	
	 public static boolean isWebClient = false;
	 private static ResourceManager connectorManager =ResourceManager.getDefaultManager();
	 private static JPanel routeMain = null, buttonPanel = null;
	 private static JPanel subjectPanel = null;
	 public JFrame dlgMain = null;
	 private JScrollPane scRoutePane = null;
	 private JLabel officeNoteTitleLbl = null;
     private JLabel officeNoteSubjectLbl = null;
     private JLabel officeNoteVerticalLbl = null;
     private JLabel officeNoteDocumentIdLbl = null;
     private boolean isThreadCall = false;
     private com.computhink.manager.image.Images img = null;
     private JButton btnInfo;
     private VWNewRouteSummaryTable Table;
     
     public VWNewRouteSummary() {
    	 if (dlgMain != null) {
			 dlgMain.setVisible(false);
		 } 
     }
	 
	 public VWNewRouteSummary(int type) {
		 VWClient.printToConsole("before loading JPanel in thread call....");
		 try {
			 isThreadCall = true;
			 routeMain = new JPanel();	
			 buttonPanel = new JPanel();
			 subjectPanel = new JPanel();		 
			 dlgMain = new JFrame();
			 VWClient.printToConsole("before loading images.....");   
			 img = new com.computhink.manager.image.Images(1);
			 scRoutePane = new JScrollPane();
			 VWClient.printToConsole("before loading setLookAndFeel.....");      
	     	 if (type == Client.Fat_Client_Type || type == Client.NFat_Client_Type)
	     		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	     	 VWClient.printToConsole("after loading setLookAndFeel....."); 
	     	 new VWNewRouteSummaryTable();
			 VWClient.printToConsole("after loading JPanel in thread call...."+routeMain);
			 
		 } catch (Exception e) {
			 VWClient.printToConsole("Exception in initializing VWNewRouteSummary objects : "+e.getMessage());
		 }
	 }
     /*public VWNewRouteSummary(int mode)
     {    
    	new Images();
        initComponents();
        loadSummaryNewDetails(mode);
        keyBuffer.start();       
     }*/
     
     public VWNewRouteSummary(VWClient vwc, int session, int docId, int mode, String documentName)
     {
    	 isThreadCall = false;
    	 if (active)
        {
        	if(session == prevSID && docId == prevDocId){
        		dlgMain.requestFocus();
        		dlgMain.toFront();
        		return;
        	}
        	else{
        		closeDialog();
        	}
        }    	 
    	this.vwc = vwc; 
    	
    	if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
			isWebClient = true;
    	
    	this.session = session;
    	prevSID = session;
    	this.docId = docId;
    	prevDocId =docId;
    	this.documentName = documentName;
    	VWClient.printToConsole("before init componenets ....");
        initComponents();
      	VWClient.printToConsole("after init componenets ....");
      	
        loadSummaryNewDetails(mode);
        VWClient.printToConsole("dlgMain.isVisible().....3 :"+dlgMain.isVisible());
        //dlgMain.setVisible(true);
        VWClient.printToConsole("dlgMain.isVisible().....4 :"+dlgMain.isVisible());
        VWClient.printToConsole("After load new route summary .....");
      //  loadSummaryDetails(mode);
        active = true;        
        keyBuffer.start();         
     }
     
  /*   public VWNewRouteSummary(VWNewRouteSummary vwNewRouteSummary, String documentName2) {
		// TODO Auto-generated constructor stub
	}*/

	private void initComponents() 
     {		
		if (img == null)
			img = new com.computhink.manager.image.Images(1);
    	int left = 12, top = 10, height = 20;
    	if (routeMain == null)
    		routeMain = new JPanel();   
    	if (buttonPanel == null)
    		buttonPanel = new JPanel();
    	if (subjectPanel == null)
    		subjectPanel = new JPanel();
    	if (dlgMain == null)
    		dlgMain = new JFrame();
    	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    	int screenHeight = screenSize.height;
    	int screenWidth = screenSize.width;
    	VWClient.printToConsole("Screen width & Height......."+screenWidth +" , "+screenHeight);
    	VWClient.printToConsole("dlgMain.isVisible().....2 :"+dlgMain.isVisible());
    	dlgMain.setSize((screenWidth-75), 600);    	
    	
    	buttonPanel.setBounds(0, 0, dlgMain.getWidth()-10, 35);
    	buttonPanel.setLayout(null);
    	btnInfo = new JButton();
    	btnInfo.setBounds(dlgMain.getWidth()-50, top, 20, 35);
    	btnInfo.setBackground(buttonPanel.getBackground());
    	btnInfo.setBorder(BorderFactory.createEmptyBorder(4, 4, 4, 4));
    	btnInfo.setContentAreaFilled(false);
        //btnInfo.setBorderPainted(false);
        btnInfo.setToolTipText(connectorManager.getString("routesummary.infoBtnToolTip"));
        btnInfo.setFocusPainted(false);
        btnInfo.setIcon(Images.infoIcon);        
        buttonPanel.add(btnInfo);
        dlgMain.getContentPane().add(buttonPanel);
    	
        btnClose = new JButton(connectorManager.getString("routesummary.close"));
        btnPrint = new JButton(connectorManager.getString("routesummary.print"));
    	dlgMain.getContentPane().setLayout(null);
        VWClient.printToConsole("before loading the subjectPanel");
        subjectPanel.setBounds(0, 35, 870, 150);
        dlgMain.getContentPane().add(subjectPanel);
        routeMain.setBounds(0, 35, 870, 370);
    	dlgMain.getContentPane().add(routeMain);
    	//dlgMain.setSize(980, 600);     	
    	
    	routeMain.setLayout(null);
    	subjectPanel.setLayout(null);
    	//subjectPanel.setBackground(Color.red);
    	//routeMain.setBackground(Color.green);
    	if (scRoutePane == null)
    		scRoutePane = new JScrollPane();
    	//scRoutePane.setBounds(left, top+60, 850, 328);
    	scRoutePane.setBounds(left, top+60, dlgMain.getWidth()-25, dlgMain.getHeight()-245);
    	//VWClient.printToConsole("before loading the subjectPanel :"+(dlgMain.getWidth()-10)+ " height :"+(routeMain.getHeight()-250));
        //scRoutePane.setPreferredSize(new Dimension(dlgMain.getWidth()-10,routeMain.getHeight()-350));
        Table = new VWNewRouteSummaryTable(this, documentName);
        VWClient.printToConsole("inside init componenets ....after calling VWNewRouteSummaryTable...");
        //Table.setDefaultRenderer(String.class, new WrappableCellRenderer1());
        
        dlgMain.getContentPane().setLayout(null);
        dlgMain.setTitle(connectorManager.getString("WORKFLOW_MODULE_NAME") +" "+connectorManager.getString("routesummary.title")+" "+ documentName);
        dlgMain.setIconImage(com.computhink.manager.image.Images.app.getImage());
        scRoutePane.setViewportView(Table);
        VWClient.printToConsole("inside init componenets ....after setViewportView...");
        
        if(!isWebClient)
        	scRoutePane.getViewport().setBackground(Color.WHITE);
        //scRoutePane.setBounds(left, top+60, 850, 328);
        //scRoutePane.setBounds(12, 10, dlgMain.getWidth()-100, (dlgMain.getHeight()-subjectPanel.getHeight()-100));
        routeMain.add(scRoutePane);
        lblComments = new JLabel(connectorManager.getString("routesummary.comments"));
        lblComments.setBounds(left, 253, 80, 20);
        routeMain.add(lblComments);
        officeNoteTitleLbl=new JLabel(connectorManager.getString("routesummary.officeNoteTitleLbl"));
        officeNoteTitleLbl.setFont((new Font("", Font.BOLD, 16)));
        officeNoteTitleLbl.setBounds(15+120,20, 600, height);
        subjectPanel.add(officeNoteTitleLbl);
        officeNoteSubjectLbl=new JLabel(connectorManager.getString("routesummary.officeNoteSubjectLbl"));
       // officeNoteTitleLbl.setFont((new Font("", Font.BOLD, 11)));
        officeNoteSubjectLbl.setBounds(15, 50, 900, height+20);
        subjectPanel.add(officeNoteSubjectLbl);
        officeNoteVerticalLbl=new JLabel(connectorManager.getString("routesummary.officeNoteVerticalLbl"));
       // officeNoteVerticalLbl.setFont((new Font("", Font.BOLD, 11)));
        officeNoteVerticalLbl.setBounds(15,80+5, 500, height);
        subjectPanel.add(officeNoteVerticalLbl);
        officeNoteDocumentIdLbl=new JLabel(connectorManager.getString("routesummary.officeNoteDocumentLbl"));
        officeNoteDocumentIdLbl.setBounds(15, 110, 500, height);
        subjectPanel.add(officeNoteDocumentIdLbl);
        taComments = new JTextArea(2,300);
        taComments.setEditable(false);
        taComments.setLineWrap(true);        
        Font labelFont = lblComments.getFont();
        taComments.setFont(labelFont);        
        spComments = new JScrollPane(taComments,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        //routeMain.add(spComments);
        spComments.setBounds(left, 273, 850, 36);
        //routeMain.add(btnPrint);
        btnPrint.setBounds(698, 324, 80, 21);
        //routeMain.add(btnClose);
        btnClose.setBounds(782, 324, 80, 21);
        //Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();        
      //  dlgMain.setLocation((screenSize.width)/2,(screenSize.height)/2);
        dlgMain.setLocation(40, (screenHeight-600)/2);
        dlgMain.setLocationRelativeTo(null);
        
        /**Added for Ctrl+P(Print) key event. Added by Vanitha****/
        SummaryReportKeyEvents summaryRepKey = new SummaryReportKeyEvents();
        addKeyListener(summaryRepKey);
        dlgMain.addKeyListener(summaryRepKey);
        
        dlgMain.setResizable(true);  //set to true by srikanth to make summary dialog to maximize on 16 Nov 2015        
        //scRoutePane.setVisible(true);
        VWClient.printToConsole("inside init componenets ....after dlgMain setResize...");
        //Component listener added for resizing the RouteSummary dialog and resize the table and columns accordingly
        ComponentListener Clistener = new ComponentAdapter() {
        	public void componentResized(ComponentEvent evt) {
        		Component c = (Component) evt.getSource();
        		Dimension screenSize1 = c.getSize();
        		dlgMain.setSize(screenSize1);
        		subjectPanel.setBounds(0,35, dlgMain.getWidth()-10,150);
        		routeMain.setBounds(0,151, dlgMain.getWidth()-10, dlgMain.getHeight()-30);
        		//scRoutePane.setBounds(12, 10, dlgMain.getWidth()-30, dlgMain.getHeight()-210);
        		scRoutePane.setBounds(5, 45, dlgMain.getWidth()-25, dlgMain.getHeight()-240);
        		officeNoteTitleLbl.setBounds(((dlgMain.getWidth()-600)/2),20, 600, 20);
        		dlgMain.repaint();
        		routeMain.repaint();
        		scRoutePane.repaint();
        		Table.setBounds(0, 0, dlgMain.getWidth()-100, dlgMain.getHeight()-100);
        		//Table.setBackground(Color.red);
        		Table.repaint();
        	}
          };
          VWClient.printToConsole("inside init componenets ....after ComponentListener...");
          dlgMain.addComponentListener(Clistener);
          
          //Window state listener added for checking the window state is maximized then resize the table and columns accordingly
          WindowStateListener WSlistener = new WindowAdapter() {
        	  public void windowStateChanged(WindowEvent evt) {
        		  int oldState = evt.getOldState();
        		  int newState = evt.getNewState();
        		  if ((oldState & dlgMain.MAXIMIZED_BOTH) == 0 && (newState & dlgMain.MAXIMIZED_BOTH) != 0) {
        			  dlgMain.setResizable(true);  //set to true by srikanth to make summary dialog to maximize on 16 Nov 2015
        			  dlgMain.setExtendedState(dlgMain.MAXIMIZED_BOTH);
        			  Dimension screenSize1 = Toolkit.getDefaultToolkit().getScreenSize();
        			  routeMain.setBounds(0, 0, screenSize1.width-10, screenSize1.height-30);
        			  scRoutePane.setBounds(12, 45, screenSize1.width-30, screenSize1.height-95);
        			  routeMain.repaint();
        			  scRoutePane.repaint();
        		  } 

        	  }
          };
          VWClient.printToConsole("inside init componenets ....after WindowStateListener...");
          
        dlgMain.addWindowStateListener(WSlistener);

         
          
        dlgMain.addWindowListener(new WindowAdapter(){
            public void windowDeactivated(WindowEvent evt) {
            	if(active){
            		dlgMain.requestFocus();
            		dlgMain.toFront();
            	}
            }
             public void windowClosing(WindowEvent e) {
            	 closeDialog();
             }
        });
        VWClient.printToConsole("inside init componenets ....after dlgMain addWindowListener...");
        btnPrint.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
                   doPrint();
                }
            });
        
        btnClose.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent evt) {
               closeDialog();
            }
        });  
        
        btnInfo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            	showNewRouteSummaryInfoDialog();
            }
        });
        
	        SelectionListener listener = new SelectionListener(Table);
	        Table.getSelectionModel().addListSelectionListener(listener);
	        Table.getColumnModel().getSelectionModel().addListSelectionListener(listener);
	        VWClient.printToConsole("End of the init componenets....");
        
     }
     public void run()
     {
    	 VWClient.printToConsole("Inside run show OF NEW ROUTE SUMMARAY");
         show();
     }
     public void show()
     {
    	 VWClient.printToConsole("Inside first line of show OF NEW	ROUTe summary......");
         if (vwc == null) {
        	 VWClient.printToConsole("Inside vwc is null.........");
        	 return;
         }
         /*
       	 * new frame.show(); method is replaced with new frame.setVisible(true); 
       	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
       	 */
         VWClient.printToConsole("inside show before setvisible ......."+isThreadCall);
         if (!isThreadCall)
        	 dlgMain.setVisible(true);
     }
     
     public void closeDialog() 
     {
    	 try{
	         active = false;
	         keyBuffer.kill();
	         dlgMain.setVisible(false);
	         dlgMain.dispose();
    	 }catch(Exception ex){
    		 //System.out.println("Err in close Dlg " + ex.getMessage());
    		 //ex.printStackTrace();
    	 }
     }

	private void loadSummaryNewDetails(int mode) {
		try {
			VWClient.printToConsole("Inside load summary new .....");
			vwc.getRouteNewSummary(session, docId, mode, routeSummary);
			String rowData = "";
			if (routeSummary != null && routeSummary.size() > 0) {
				getSummaryDetails(routeSummary.get(0).toString());
				setSummaryDetails();
			}
			Vector data = new Vector();

			Table.clearData();
			// Commented the log added.
			/*for (int j = 0; j < routeSummary.size(); j++) {
				rowData = "";
				String value = routeSummary.get(j).toString();
				StringTokenizer tokens = new StringTokenizer(value, Util.SepChar);
				VWClient.printToConsole("tokens.countTokens() :::" + tokens.countTokens());
				try {
					VWClient.printToConsole("1::::::" + tokens.nextToken());// S.No
					VWClient.printToConsole("2::::::" + tokens.nextToken()); // Document
																				// Id
					VWClient.printToConsole("3::::::" + tokens.nextToken()); // Document
																				// Name
					VWClient.printToConsole("4::::::" + tokens.nextToken()); // Route
																				// Id
					VWClient.printToConsole("5::::::" + tokens.nextToken());// Route
																			// Name
					VWClient.printToConsole("6::::::" + tokens.nextToken()); // RouteUser
																				// Id
					VWClient.printToConsole("7::::::" + tokens.nextToken());// Orginator/initiated
																			// by
					VWClient.printToConsole("8::::::" + tokens.nextToken());// Orginator/initiated
																			// by
					VWClient.printToConsole("9::::::" + tokens.nextToken());// Orginator/initiated
																			// by
					VWClient.printToConsole("10::::::" + tokens.nextToken());// Orginator/initiated
																				// by
					VWClient.printToConsole("11::::::" + tokens.nextToken());// Orginator/initiated
																				// by
					VWClient.printToConsole("12::::::" + tokens.nextToken());// task
																				// sequence
					VWClient.printToConsole("13::::::" + tokens.nextToken());// taskname
					VWClient.printToConsole("14::::::" + tokens.nextToken());// task
																				// desc
					VWClient.printToConsole("15::::::" + tokens.nextToken());// sendMail
																				// status
					VWClient.printToConsole("16::::::" + tokens.nextToken());// Approved
																				// by
					VWClient.printToConsole("17::::::" + tokens.nextToken());// Approved
																				// by
																				// userName
					VWClient.printToConsole("18::::::" + tokens.nextToken());// Authorized
																				// by
					VWClient.printToConsole("19::::::" + tokens.nextToken());// authorizedUserName
					// comment by madhavan on 27-Aug-2018
					VWClient.printToConsole("20::::::" + tokens.nextToken());// routeUserSignType
					VWClient.printToConsole("21::::::" + tokens.nextToken());// workflowStatus
					VWClient.printToConsole("22::::::" + tokens.nextToken());// workflowStatus
					VWClient.printToConsole("23::::::" + tokens.nextToken());// workflowStatus
					VWClient.printToConsole("24::::::" + tokens.nextToken());// workflowStatus
					VWClient.printToConsole("25::::::" + tokens.nextToken());// workflowStatus
					VWClient.printToConsole("26::::::" + tokens.nextToken());// subject
					VWClient.printToConsole("27::::::" + tokens.nextToken());// officenote
				} catch (Exception ex) {
					VWClient.printToConsole("Exception in new route summry :::" + ex.getMessage());
					// System.out.println("Exception in loadSummaryDetails" +
					// ex.getMessage());
				}

			}*/

			long start = System.nanoTime();
			Map<Integer, String> dataMap = null;
			for (int i = 0; i < routeSummary.size(); i++) {
				dataMap = new HashMap<Integer, String>();
				rowData = "";
				String value = routeSummary.get(i).toString();
				String[] tokens = value.split(Util.SepChar);
				if (tokens.length > 0) {
					try {
						dataMap.put(2, tokens[7]);// Received On
						VWClient.printToConsole("tokens[7] ::" + tokens[7]);
						dataMap.put(3, tokens[8]);// Action
						VWClient.printToConsole("tokens[8] ::" + tokens[8]);
						dataMap.put(4, tokens[9]);// Date of action
						VWClient.printToConsole("tokens[9] ::" + tokens[9]);
						dataMap.put(5, tokens[10]);// comments
						VWClient.printToConsole("tokens[10] ::" + tokens[10]);
						dataMap.put(1, tokens[21]);// userinfo
						dataMap.put(6, tokens[22]);// docversion
						dataMap.put(7, tokens[23]);// forwarded to
						dataMap.put(8, tokens[24]);// reference count
						subject = tokens[25];// subject
						officeNoteVertical = tokens[26];// officenote
					} catch (Exception ex) {
						VWClient.printToConsole("Exception in new route summry :::" + ex.getMessage());
					}
				}
				/**
				 * This is commented bz StringTokenizer is taking time to loop
				 * routSummary data
				 **/
				/*StringTokenizer tokens = new StringTokenizer(value,
				// Util.SepChar);
				// VWClient.printToConsole("tokens.countTokens()
				// :::"+tokens.countTokens());
				try {
					tokens.nextToken();// S.No
					tokens.nextToken(); // Document Id
					tokens.nextToken(); // Document Name
					tokens.nextToken(); // Route Id
					tokens.nextToken();// Route Name
					tokens.nextToken(); // RouteUser Id
					tokens.nextToken();// Orginator/initiated by
					dataMap.put(2, tokens.nextToken());// Received On 7
					dataMap.put(3, tokens.nextToken());// Action 8
					dataMap.put(4, tokens.nextToken());// Date of action 9
					dataMap.put(5, tokens.nextToken());// comments 10
					tokens.nextToken();// task sequence
					tokens.nextToken();// taskname
					tokens.nextToken();// task desc
					tokens.nextToken();// sendMail status
					tokens.nextToken();// Approved by
					tokens.nextToken();// Approved by userName
					tokens.nextToken();// Authorized by
					tokens.nextToken();// authorizedUserName
					// comment by madhavan on 27-Aug-2018
					tokens.nextToken();// routeUserSignType
					tokens.nextToken();// workflowStatus 20

					dataMap.put(1, tokens.nextToken());// userinfo 21
					dataMap.put(6, tokens.nextToken());// docversion 22
					dataMap.put(7, tokens.nextToken());// forwarded to 23
					dataMap.put(8, tokens.nextToken());// reference count 24
					subject = tokens.nextToken();// subject 25
					officeNoteVertical = tokens.nextToken();// officenote 26
				} catch (Exception ex) {
					VWClient.printToConsole("Exception in new route summry :::" + ex.getMessage());
					// System.out.println("Exception in loadSummaryDetails" +
					// ex.getMessage());
				} */
				// VWClient.printToConsole("dataMap :::::"+dataMap);
				rowData = dataMap.get(1).toString() + Util.SepChar + dataMap.get(2) + Util.SepChar + dataMap.get(3)
						+ Util.SepChar + dataMap.get(4) + Util.SepChar + dataMap.get(5) + Util.SepChar + dataMap.get(6)
						+ Util.SepChar + dataMap.get(7) + Util.SepChar + dataMap.get(8) + Util.SepChar;

				data.addElement(rowData); 
			}
			long time = System.nanoTime() - start;
			// VWClient.printToConsole("StringTokenizer time taken to loop routeSummary details :"+time);
			VWClient.printToConsole("String Split time taken to loop  routeSummary details :" + time);
			Table.addData(data);

		} catch (Exception e) {
			VWClient.printToConsole("Exception in load summary details new :::" + e.getMessage());
		}
	}
     
     

     
     private void setSummaryDetails(){
    	 if(!subject.equals("-")){
    		 if(subject.length()>160){
    			 String subject1= "<html>"+officeNoteSubjectLbl.getText()+" "+subject.substring(0,160)+"<br>"+subject.substring(160,subject.length())+"</html>";
    			 officeNoteSubjectLbl.setText(subject1);
    			 officeNoteVerticalLbl.setBounds(15,80+15, 500, 20);
    			 officeNoteDocumentIdLbl.setBounds(15, 110+10, 500, 20);
    		 }else{
    			 officeNoteSubjectLbl.setText(officeNoteSubjectLbl.getText()+" "+subject);
    		 }
    	 }
    	 if(!officeNoteVertical.equals("-"))
    	 officeNoteVerticalLbl.setText(officeNoteVerticalLbl.getText()+" "+officeNoteVertical);
    	 officeNoteDocumentIdLbl.setText(officeNoteDocumentIdLbl.getText()+" "+String.valueOf(docId));
    	
     }
     
	private void getSummaryDetails(String value) {
		try {
			// System.out.println("value " + value);
			//long start = System.nanoTime();
			String[] tokens = value.split(Util.SepChar);
			if (tokens.length > 0) {
				docId = Integer.parseInt(tokens[1]); // Document Id
				subject = tokens[25];// subject
				officeNoteVertical = tokens[26];// offcinote
			}
			/**This is commented bz StringTokenizer is taking time to get next token details **/
			/*StringTokenizer tokens = new StringTokenizer(value, Util.SepChar);
			if (tokens.countTokens() > 0) {
				tokens.nextToken();// S.No
				docId = Integer.parseInt(tokens.nextToken()); // Document Id
				tokens.nextToken(); // Document Name
				tokens.nextToken(); // Route Id
				tokens.nextToken();// Route Name
				tokens.nextToken(); // RouteUser Id
				tokens.nextToken();// Orginator/initiated by
				tokens.nextToken();// Received On
				tokens.nextToken();// Action
				tokens.nextToken();// Date of action
				tokens.nextToken();// comments

				tokens.nextToken();// task sequence
				tokens.nextToken();// taskname
				tokens.nextToken();// task desc
				tokens.nextToken();// sendMail status
				tokens.nextToken();// Approved by
				tokens.nextToken();// Approved by userName
				tokens.nextToken();// Authorized by
				tokens.nextToken();// authorizedUserName
				tokens.nextToken();// routeUserSignType
				tokens.nextToken();// workflowStatus
				tokens.nextToken();// userinfo
				tokens.nextToken();// doc version
				tokens.nextToken();// forwarded to
				tokens.nextToken();// reference count
				subject = tokens.nextToken();// subject
				officeNoteVertical = tokens.nextToken();// offcinote
			}*/
			/*long time = System.nanoTime() - start;
			VWClient.printToConsole("StringTokenizer time taken to load  getSummaryDetails :"+time);*/
		} catch (Exception ex) {
			System.out.println("Exception in getSummaryDetails");
			System.out.println(docId + " : " + documentName + " : " + routeId + " : " + routeName);
		}

	}
     
     public String getRouteData(int column){
	 return Table.m_data.getValueAt(Table.getSelectedRow(), column).toString();	 
     }
     
    private class KeyBuffer extends Thread
    {
        private String prefix = "";
        private boolean run = true;
        public KeyBuffer()
        {
            setPriority(Thread.MIN_PRIORITY);
        }
        public void appendKey(char c)
        {
            prefix+=c;
        }
        public void kill()
        {
            run = false;
        }
        public void run()
        {
            while (run)
            {
                Util.sleep(100);
                if (prefix.length() == 0) continue;
                prefix = "";
            }
        }
    }        
    private JButton btnClose;
    
    /*private JLabel lblRouteName;
    private JLabel lblRouteNameDesc;
    private JLabel lblDocumentName;
    private JLabel lblDocumentNameDesc;
    private JLabel lblCreatedBy;
    private JLabel lblCreatedByDesc;
    private JLabel lblType;
    private JLabel lblTypeDesc;*/    
    private JButton btnPrint;
    private KeyBuffer keyBuffer = new KeyBuffer();
    
    private static VWClient vwc;
    private int session;
    private int docId;
    private int routeId;
    private String subject="";
    private String officeNoteVertical="";
    private String documentName = "";
    private String routeName = "";
    /*private String createdBy = "";
    private String routeType = "";
    
    private String userName = "";
    private String receivedOn = "";
    private String processedOn = "";
    private String status = "";
    private String comments = "";*/
    
   // private Vector data = new Vector();
    private static boolean active = false;
    private Vector routeSummary = new Vector();    
    
    private static JTextArea taComments; 
    private static JScrollPane spComments;
    private static JLabel lblComments;
    
    public static int prevSID = 0;
    public static int prevDocId = 0;
    
  
    public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {
    	Graphics2D g2 = (Graphics2D)g;
        g2.setColor(Color.black);
        int fontHeight = g2.getFontMetrics().getHeight();
        int fontDesent = g2.getFontMetrics().getDescent();
        //leave room for page number...
        double pageHeight = pageFormat.getImageableHeight() - fontHeight;
        double pageWidth = pageFormat.getImageableWidth();
        double tableWidth = (double)Table.getColumnModel().getTotalColumnWidth();
        double scale = 1;
        if (tableWidth >= pageWidth) {
            scale = pageWidth / tableWidth;
        }
        double headerHeightOnPage = Table.getTableHeader().getHeight() * scale;
        double tableWidthOnPage = tableWidth * scale;
        double oneRowHeight = (Table.getRowHeight() + Table.getRowMargin()) * scale;
        int numRowsOnAPage = (int)((pageHeight - headerHeightOnPage) / oneRowHeight);
        double pageHeightForTable = oneRowHeight * numRowsOnAPage;
        int totalNumPages = (int)Math.ceil(((double)Table.getRowCount()) / numRowsOnAPage);
        if (pageIndex >= totalNumPages) {
            return NO_SUCH_PAGE;
        }

        g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        
        //Caption Route Summary...
        //g2.drawString(officeNoteTitleLbl.getText()+" "+ documentName,5,10);
        		//connectorManager.getString("WORKFLOW_MODULE_NAME") +" "+connectorManager.getString("routesummary.title")+" "+ documentName,5,10);
        
        g2.translate(0f, headerHeightOnPage);
        g2.translate(0f, -pageIndex * pageHeightForTable);
        if (pageIndex + 1 == totalNumPages) {
            int lastRowPrinted = numRowsOnAPage * pageIndex;
            int numRowsLeft = Table.getRowCount() - lastRowPrinted;
            g2.setClip(0, (int)(pageHeightForTable * pageIndex), (int)Math.ceil(tableWidthOnPage), (int)Math.ceil(oneRowHeight * numRowsLeft));
        }
        else {
            g2.setClip(0, (int)(pageHeightForTable * pageIndex), (int)Math.ceil(tableWidthOnPage),(int)Math.ceil(pageHeightForTable));
        }
        g2.scale(scale, scale);
        Table.paint(g2);
        g2.drawString("Page: " + (pageIndex + 1), (int)pageWidth / 2 - 35, (int) (pageHeight + fontHeight - fontDesent));
        g2.translate(0f, headerHeightOnPage);
        g2.translate(0f, -pageIndex * pageHeightForTable);
        if (pageIndex + 1 == totalNumPages) {
            int lastRowPrinted = numRowsOnAPage * pageIndex;
            int numRowsLeft = Table.getRowCount() - lastRowPrinted;
            g2.setClip(0, (int)(pageHeightForTable * pageIndex), (int)Math.ceil(tableWidthOnPage), (int)Math.ceil(oneRowHeight * numRowsLeft));
        }
        else {
            g2.setClip(0, (int)(pageHeightForTable * pageIndex), (int)Math.ceil(tableWidthOnPage),(int)Math.ceil(pageHeightForTable));
        }
        g2.scale(scale, scale);
        
        Table.paint(g2);
        g2.scale(1 / scale, 1 / scale);
        g2.translate(0f, pageIndex * pageHeightForTable);
        g2.translate(0f, -headerHeightOnPage);
        g2.setClip(0, 0, (int)Math.ceil(tableWidthOnPage), (int)Math.ceil(headerHeightOnPage));
        g2.scale(scale, scale);
        Table.getTableHeader().paint(g2);
        return Printable.PAGE_EXISTS;
    }

    public void doPrint() {
    	active = false;
    	/**Following codes are added for printing multiple JTables as a single file. Added by Vanitha on 22/01/2020**/
		try {
			ArrayList<JTable> tableList = new ArrayList<JTable>();
			tableList.add(Table);			
			PrinterJob pj = PrinterJob.getPrinterJob();
			PageFormat pf = pj.defaultPage();
			pf.setOrientation(PageFormat.LANDSCAPE);
			Paper paper = new Paper();
			double margin = 36; // half inch
			paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
			pf.setPaper(paper);

			Book printJob = new Book();
			int totalPages = 0;
			String title = null;
			for (JTable t : tableList) {
				VWClient.printToConsole("Table :" + t);
				try {
					int pages = getNumberOfPages(t.getPrintable(PrintMode.FIT_WIDTH, null, null), pf);
					title = connectorManager.getString("WORKFLOW_MODULE_NAME") +" "+connectorManager.getString("routesummary.title")+" "+ documentName;
					MessageFormat headerFormat = new MessageFormat(title);
			        VWClient.printToConsole("pages :" + pages);
					Printable p = t.getPrintable(PrintMode.FIT_WIDTH, headerFormat, null);
					VWClient.printToConsole("before calling summaryprint  totalPages:" + totalPages);					
					printJob.append(new VWSummaryPrint(p, totalPages), pf, pages);
					totalPages += pages;
				} catch (Exception e) {
					VWClient.printToConsole("Exception while printing the summary tables :" + e.getMessage());
				}
			}
			VWClient.printToConsole("Before setPagable......");
			pj.setPageable(printJob);
			VWClient.printToConsole("Before print......");
			if (pj.printDialog())
				pj.print();
		} catch (Exception e) {
			VWClient.printToConsole("Exception while printing the summary report :" + e.getMessage());
		}
    	/**End of multiple JTable print****/
    }
        
    public void doPrint_old() {
    	active = false;
    /*    private JLabel officeNoteTitleLbl;
        private JLabel officeNoteSubjectLbl;
        private JLabel officeNoteVerticalLbl;
        private JLabel officeNoteDocumentLbl;*/
    	String printTitle=officeNoteTitleLbl.getText().trim();
    	String printSubject=officeNoteSubjectLbl.getText().trim();
    	String printNoteVeritical=officeNoteVerticalLbl.getText().trim();
    	String printOffNoteDocument=officeNoteDocumentIdLbl.getText().trim();

    	//new VWPrint(Table,printTitle);
    	new VWPrintSummary(Table,printTitle,printSubject,printNoteVeritical,printOffNoteDocument);
    	/*
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setPrintable(this);
        if (pj.printDialog()) {
            try {
                pj.print();
            }
            catch (PrinterException ee) {
                System.out.println(ee);
            }
            active = true;
        }else{
        	active = true;
        }
        */
    }
    public void doExport() {
    	VWClient.printToConsole("inside new route report summary report.....");
		active = false;
		int rowCount = Table.getRowCount();
		String[][] data = Table.getData();
		try {
			ArrayList<String> htmlContents = generateNewSummaryReportHtmlContent(data, this.documentName, this.routeName);
			/*String err = "";
			String title = PRODUCT_NAME + " "+ connectorManager.getString("WORKFLOW_MODULE_NAME") +" "+connectorManager.getString("routesummary.report") ;
			htmlContents.add("<html>");
			htmlContents.add("<head>");
			htmlContents.add("<title>" + title + "</title>");
			htmlContents.add("<style>");
			htmlContents.add("TH {");
			htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 12px;COLOR='#336699'; font-weight:bold");
			htmlContents.add("}");
			htmlContents.add("TD {");
			htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 11px");
			htmlContents.add("}");
			htmlContents.add("</style>");
			htmlContents.add("</head>");
			htmlContents.add("<body BGCOLOR='#F7F7E7'>");
			htmlContents.add("<table border=1 width=100%  cellspacing=0 cellpadding=0 ><tr><td align=center><font size='4'><b>"+ title + "</b></font></td></tr></table>");
			htmlContents.add("<p></p>");    	
			htmlContents.add("<table cellspacing =0 cellpadding = 1 border =1 width=100%>");
			
			htmlContents.add("<tr BGCOLOR=#cccc99>");
			htmlContents.add("");
			htmlContents.add("</tr>");
			for (int x = 0; x < rowCount; x++){
				htmlContents.add("<tr>" + 
				"<td>" + (data[x][0].equalsIgnoreCase("-")?"&nbsp;":data[x][0]) + "</td>" +
				"<td>" + (data[x][1].equalsIgnoreCase("-")?"&nbsp;":data[x][1]) + "</td>" + 
				"<td>" + (data[x][2].equalsIgnoreCase("-")?"&nbsp;":data[x][2]) + "</td>" + 
				"<td>" + (data[x][3].equalsIgnoreCase("-")?"&nbsp;":data[x][3]) + "</td>" + 
				"<td>" + (data[x][4].equalsIgnoreCase("-")?"&nbsp;":data[x][4]) + "</td>" + 
				"<td>" + (data[x][5].equalsIgnoreCase("-")?"&nbsp;":data[x][5]) + "</td>" + 
				"<td>" + (data[x][6].equalsIgnoreCase("-")?"&nbsp;":data[x][6]) + "</td>" + 
				"<td>" + (data[x][7].equalsIgnoreCase("-")?"&nbsp;":data[x][7]) + "</td>" + 
				"<td>" + (data[x][8].equalsIgnoreCase("-")?"&nbsp;":data[x][8]) + "</td>" +
				"<td>" + (data[x][9].equalsIgnoreCase("-")?"&nbsp;":data[x][9]) + "</td>" + 
				"<td>" + (data[x][10].equalsIgnoreCase("-")?"&nbsp;":data[x][10]) + "</td>" +
				"</tr>");
			}
			htmlContents.add("</table></body></html>");*/
			try {
			    JFileChooser fileChooser = new JFileChooser();
			    File file = new File("c:\\"+ WORKFLOW_MODULE_NAME +"Summary.html");
			    fileChooser.setSelectedFile(file);
			    int returnVal = fileChooser.showSaveDialog(dlgMain);
		
			    String savePath = "";
			    if (returnVal == JFileChooser.APPROVE_OPTION) {
				savePath = fileChooser.getSelectedFile().getPath();
				fileChooser.setVisible(false);
				fileChooser = null;
			    }
			    File reportFile = new File(savePath);
			    boolean isFileSaved = VWCUtil.writeArrayListToFile(htmlContents, reportFile.getPath(), "", "UTF-8");
			    VWClient.printToConsole("Summary report created >>>>>"+isFileSaved);
		
			} catch (Exception ex) {
		
			}
			active=true;
		} catch (Exception e) {
			VWClient.printToConsole("Exception while exporting summary report :"+e);
		}
    }
    
    	
    public class SelectionListener implements ListSelectionListener {
        JTable table;
        int ttlColumns = 0;
        int summaryColumn = 0;
        // It is necessary to keep the table since it is not possible
        // to determine the table from the event's source
        SelectionListener(JTable table) {
            this.table = table;
            
            ttlColumns = table.getColumnModel().getColumnCount();
            //Comment by madhavan on 24-August-2018
    		//summaryColumn = Table.m_data.COL_COMMENTS;
        }
        public void valueChanged(ListSelectionEvent e) {
        	try{
	            // If cell selection is enabled, both row and column change events are fired
	            if (e.getSource() == table.getSelectionModel() && table.getRowSelectionAllowed()) {
	                // Column selection changed
	                int first = e.getFirstIndex();
	                int last = e.getLastIndex();
	                VWNewRouteSummary.taComments.setText(String.valueOf(table.getValueAt(table.getSelectedRow(), summaryColumn)));
	            } else if (e.getSource() == table.getColumnModel().getSelectionModel() && table.getColumnSelectionAllowed() ){
	                // Row selection changed
	                int first = e.getFirstIndex();
	                int last = e.getLastIndex();
	                VWNewRouteSummary.taComments.setText(String.valueOf(table.getValueAt(table.getSelectedRow(),summaryColumn)));
	            }	    
	            if (e.getValueIsAdjusting()) {
	                // The mouse button has not yet been released
	            }
        	}catch(Exception ex){
        		vwc.printToConsole(" Error in valueChanged :"+ex.toString());
        	}
        }
    }

    private void showNewRouteSummaryInfoDialog() {
    	VWClient.printToConsole("inside showInfoDialog.....");
    	new VWRouteSummaryInfo(dlgMain, true, this.vwc, this.session, this.docId, documentName);
    }
    
    /**Added for summary report print(CTRL+P)***/
    class SummaryReportKeyEvents extends java.awt.event.KeyAdapter {
        public void keyPressed(java.awt.event.KeyEvent event) {
        	VWClient.printToConsole("inside keypress event.....");
            AWTKeyStroke ak = AWTKeyStroke.getAWTKeyStrokeForEvent(event);
            if(ak.equals(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_P,InputEvent.CTRL_MASK)))
            {
            	VWClient.printToConsole("ctrl+p key pressed >>>>>:"+ak.getKeyCode());
          	  	doPrint();
            }
        }
    }
    public int getNumberOfPages(Printable delegate, PageFormat pageFormat) throws PrinterException 
    {
        Graphics g = new BufferedImage(1, 1, BufferedImage.TYPE_INT_RGB).createGraphics();
        int numPages = 0;
        while (true) {
            int result = delegate.print(g, pageFormat, numPages);
            if (result == Printable.PAGE_EXISTS) {
                ++numPages;
            } else {
                break;
            }
        }

        return numPages;
    }
    /******End of ctrl+p(print) enhancement***/
    public ArrayList<String> generateNewSummaryReportHtmlContent(String[][] data, String docName, String routeName) {
    	ArrayList<String> htmlContents = new ArrayList<String>();
    	String[] caption = VWCConstants.NewRouteColNames;
    	int rowCount = data.length;
    	String title = connectorManager.getString("WORKFLOW_NAME") +" "+connectorManager.getString("routesummary.report") ;
    	htmlContents.add("<html>");
    	htmlContents.add("<head>");
    	htmlContents.add("<title>" + title + "</title>");
    	htmlContents.add("<style>");
    	htmlContents.add("TH {");
    	htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 12px;COLOR='#336699'; font-weight:bold");
    	htmlContents.add("}");
    	htmlContents.add("TD {");
    	htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 11px");
    	htmlContents.add("}");
    	htmlContents.add("</style>");
    	htmlContents.add("</head>");
    	htmlContents.add("<body BGCOLOR='#F7F7E7'>");
    	htmlContents.add("<p align='center'><span lang='en-us'><font size='2'></font></span></p>");
    	htmlContents.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");
    	htmlContents.add("<table border=0 cellspacing=1 width='92%!important' id='AutoNumber1' cellpadding=2>");
        htmlContents.add("<tr><td ><B>Document Name :</B>");
        htmlContents.add("		"+docName+"</td></tr>");
        if (routeName != null) {
	        htmlContents.add("<tr><td ><B>"+ Constants.WORKFLOW_MODULE_NAME +" Name :</B>");
	        htmlContents.add("		"+routeName+"</td></tr>");
        }
        htmlContents.add("</table>");
        htmlContents.add("<table border=1 bordercolor='grey' cellpadding='2' cellspacing='0' style= 'width:92%!important; border-collapse:collapse;' id='AutoNumber1' word-wrap='break-word'>");    	
    	htmlContents.add("<tr BGCOLOR=#cccc99>");
        for(int i=0;i<caption.length;i++) {
            htmlContents.add("<td style='padding-left:2px;'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>" + caption[i] + "</B></td>");
        }
        htmlContents.add("</tr>");
        for(int i=0;i<data.length;i++)
        {
            htmlContents.add("<tr>");
            for(int j=0;j<caption.length;j++) {
                htmlContents.add("<td style='padding-left:2px;'>" + data[i][j] + "</td>");
            }
            htmlContents.add("</tr>");
        }
    	htmlContents.add("</table>");
    	
        htmlContents.add("<BR><BR>");
        htmlContents.add("<p align='left'><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></p>");
    	htmlContents.add("</body></html>");
    	VWClient.printToConsole("htmlContents :"+htmlContents);
    	return htmlContents;
    }
    
    public ArrayList<String> generateNewSummaryReportHtmlContent(Vector<String> docRouteSummary) {
    	VWClient.printToConsole("inside VWRouteSummary generateRouteSummaryHTMLContent....");
    	ArrayList<String> htmlContents = null;	
	  	String routeName = null, docName = null;
	  	try {
			VWClient.printToConsole("docRouteSummary......."+docRouteSummary);
			if (docRouteSummary != null && docRouteSummary.size() > 0) {
				int row = docRouteSummary.size();
				int column = VWCConstants.NewRouteColNames.length;
				VWClient.printToConsole("column >>>>>> " + column);
				String[][] data = new String[row][column + 1];
				int j = 0;
				String[] tokens = null;
				for (int i = 0; i < row; i++) {
					try {
						VWClient.printToConsole("docRouteSummary data >>> " + docRouteSummary.get(i).toString());
						tokens = (docRouteSummary.get(i).toString()).split("\t");
						if (i == 0) {
							docName = tokens[2];
							routeName = tokens[4];
						}
						j = 0;
						data[i][j++] = tokens[21];
						data[i][j++] = tokens[7];
						data[i][j++] = tokens[8];
						data[i][j++] = tokens[9];
						data[i][j++] = tokens[10];
						data[i][j++] = tokens[22];
						data[i][j++] = tokens[23];
						data[i][j++] = tokens[24];
					} catch (Exception e) {
						VWClient.printToConsole("Exception while generating route summary array :" + e.getMessage());// AuthorizedByName
					}
				}
				docName = VWCUtil.fixFileName(docName);
				routeName = VWCUtil.fixFileName(routeName);
				VWClient.printToConsole("before calling generateNewSummaryReportHtmlContent method >>>> " + data);
				VWClient.printToConsole("before calling generateNewSummaryReportHtmlContent docName >>>> " + docName);
				VWClient.printToConsole("before calling generateNewSummaryReportHtmlContent routeName >>>> " + routeName);
				htmlContents = generateNewSummaryReportHtmlContent(data, docName, routeName);
			}
		} catch (Exception e) {
			VWClient.printToConsole("Exception in generateRouteSummaryHTML :"+e.getMessage());
		}
    	return htmlContents;
    }
}