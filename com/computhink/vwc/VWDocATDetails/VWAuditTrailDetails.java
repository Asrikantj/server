/**
 * 
 */
package com.computhink.vwc.VWDocATDetails;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.image.Images;
import com.computhink.vws.server.Client;

/**
 * @author Nishad Nambiar
 *
 */

public class VWAuditTrailDetails implements Runnable, Printable
{
	
	public static boolean isWebClient = false;
	public static ResourceManager connectorManager = ResourceManager.getDefaultManager();
	
     public VWAuditTrailDetails(int mode)
     {     
        new Images();
        initComponents();
        loadAuditDetails(mode);
        dlgMain.setVisible(true);
        keyBuffer.start();
     }
     
     public VWAuditTrailDetails(VWClient vwc, int session, int docId, int mode, String documentName)
     {
        if (active)
        {
        	if(session == prevSID && docId == prevDocId){
        		dlgMain.requestFocus();
        		dlgMain.toFront();
        		return;
        	}
        	else{
        		closeDialog();
        	}
        }    
        this.vwc = vwc;
        
        if(vwc != null && (vwc.getClientType() == Client.Web_Client_Type || vwc.getClientType() == Client.WebL_Client_Type ||
				vwc.getClientType() == Client.NWeb_Client_Type || vwc.getClientType() == Client.NWebL_Client_Type))
			isWebClient = true;
        
        this.session = session;
        prevSID = session;
        this.docId = docId;
        prevDocId = docId;
    	this.documentName = documentName;
        initComponents();
        dlgMain.setVisible(true);
        loadAuditDetails(mode);
        active = true;        
        keyBuffer.start();
     }
     
     private void initComponents() 
     {	int left = 10, top = 10;
     
    	routeMain = new JPanel();
    	dlgMain = new JFrame();
        
        btnClose = new JButton(connectorManager.getString("audit.close"));
        btnPrint = new JButton(connectorManager.getString("audit.print"));
        
    	dlgMain.getContentPane().setLayout(null);
    	dlgMain.getContentPane().add(routeMain);
    	dlgMain.setSize(650, 250);
    	routeMain.setBounds(0, 0, 650, 250);
    	routeMain.setLayout(null);
        scRoutePane = new JScrollPane();
        Table = new VWDocumentAuditTrailsDetailsTable();
        dlgMain.getContentPane().setLayout(null);
        dlgMain.setTitle(connectorManager.getString("audit.details")+" "+ documentName);
        scRoutePane.setViewportView(Table);
        scRoutePane.setBounds(left, top, 620, 162);
        routeMain.add(scRoutePane);
        routeMain.add(btnPrint);
        btnPrint.setBounds(484, 183, 70, 25);
        routeMain.add(btnClose);
        btnClose.setBounds(562, 183, 70, 25);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();        
        dlgMain.setLocation((screenSize.width-440)/2,(screenSize.height-290)/2);
        //dlgMain.setResizable(true);
        dlgMain.setResizable(true);  //srikanth on 19 Nov 2015 allowing window to resize
        
        //Component listener added for resizing the RouteSummary dialog and resize the table and columns accordingly
        ComponentListener Clistener = new ComponentAdapter() {
        	public void componentResized(ComponentEvent evt) {
        		Component c = (Component) evt.getSource();
        		Dimension screenSize1 = c.getSize();
        		dlgMain.setSize(screenSize1);
        		routeMain.setBounds(0, 0, dlgMain.getWidth(), dlgMain.getHeight()-30);
        		scRoutePane.setBounds(10, 10, dlgMain.getWidth()-30, dlgMain.getHeight()-92);
                routeMain.add(btnPrint);
                btnPrint.setBounds(screenSize1.width-166, screenSize1.height-67, 70, 25);
                routeMain.add(btnClose);
                btnClose.setBounds(screenSize1.width-88, screenSize1.height-67, 70, 25);
        		dlgMain.repaint();
        		routeMain.repaint();
        		scRoutePane.repaint();
        		Table.setBounds(0, 0, dlgMain.getWidth()-20, dlgMain.getHeight()-45);
        		Table.repaint();
        		
        	}
          };

          dlgMain.addComponentListener(Clistener);
          
          //Window state listener added for checking the window state is maximized then resize the table and columns accordingly
          WindowStateListener WSlistener = new WindowAdapter() {
        	  public void windowStateChanged(WindowEvent evt) {
        		  int oldState = evt.getOldState();
        		  int newState = evt.getNewState();
        		  if ((oldState & dlgMain.MAXIMIZED_BOTH) == 0 && (newState & dlgMain.MAXIMIZED_BOTH) != 0) {
        			  dlgMain.setResizable(true);  //set to true by srikanth to make summary dialog to maximize on 16 Nov 2015
        			  dlgMain.setExtendedState(dlgMain.MAXIMIZED_BOTH);
        			  Dimension screenSize1 = Toolkit.getDefaultToolkit().getScreenSize();
        			  routeMain.setBounds(0, 0, screenSize1.width-10, screenSize1.height-60);
        			  scRoutePane.setBounds(10, 10, screenSize1.width-30, screenSize1.height-92);
					  routeMain.add(btnPrint);
		                btnPrint.setBounds(screenSize1.width-160, screenSize1.height-62, 70, 25);
		                routeMain.add(btnClose);
		                btnClose.setBounds(screenSize1.width-82, screenSize1.height-62, 70, 25);
        			  routeMain.repaint();
        			  scRoutePane.repaint();
        			  Table.repaint();
        		  } 

        	  }
          };
          
        dlgMain.addWindowStateListener(WSlistener);
        
        
        dlgMain.addWindowListener(new WindowAdapter(){
        	public void windowLostFocus(WindowEvent e){
            	if(active){
	            	dlgMain.requestFocus();
	                dlgMain.toFront();
            	}
            }
            public void windowDeactivated(WindowEvent e) {
            	if(active){
            		dlgMain.requestFocus();
            		dlgMain.toFront();
            	}
            }
            public void windowStateChanged(WindowEvent e) {
            	if(active){	
            		dlgMain.toFront();
            	}
            }
             public void windowDeiconified(WindowEvent e) {
            	 if(active){
            		 dlgMain.toFront();
            	 }
            }           
             public void windowClosing(WindowEvent e) {         
            	 closeDialog();
             }
     
        });
        
        btnPrint.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
                   doPrint();
                }
            });
        
        btnClose.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent evt) {
               closeDialog();
            }
        });        
     }
     public void run()
     {
         show();
     }
     public void show()
     {
         if (vwc == null) return;
         /*
      	 * new frame.show(); method is replaced with new frame.setVisible(true); 
      	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
      	 */
             dlgMain.setVisible(true);
     }
     
     private void closeDialog() 
     {
    	 try{
	         active = false;
	         keyBuffer.kill();
	         dlgMain.setVisible(false);
	         dlgMain.dispose();
    	 }catch(Exception ex){
    	 }
     }

     private void loadAuditDetails(int mode){
    	 int ret = 0;
    	 ret = vwc.getDocumentAuditDetails(session, docId, mode, auditDetails);
    	 
    	 if (auditDetails != null && auditDetails.size() > 0){
    		 getSummaryDetails(auditDetails.get(0).toString());
    	 }
    	 Vector data = new Vector();
    	 Table.clearData();
    	 VWDocumentHistoryInfo vwdocumentHistoryInfo =null;
    	 for (int i = 0; i< auditDetails.size() ; i++){
    		 String value = auditDetails.get(i).toString();
    		 StringTokenizer tokens = new StringTokenizer(value, Util.SepChar);
    		 try{
    			 
    			 vwdocumentHistoryInfo = new VWDocumentHistoryInfo();
    			 tokens.nextToken();
    			 tokens.nextToken();
    			 tokens.nextToken();
    			 vwdocumentHistoryInfo.setAuditObjectType(connectorManager.getString("audit.document"));
    			 vwdocumentHistoryInfo.setAuditObjectName(tokens.nextToken());
    			 vwdocumentHistoryInfo.setAuditUserName(tokens.nextToken());
    			 vwdocumentHistoryInfo.setAuditClientHost(tokens.nextToken());
    			 vwdocumentHistoryInfo.setAuditDate(tokens.nextToken());
    			 tokens.nextToken();
    			 vwdocumentHistoryInfo.setAuditLocation(tokens.nextToken());    			
    			 vwdocumentHistoryInfo.setAuditDescription(tokens.nextToken());
    			 tokens.nextToken();
    			 vwdocumentHistoryInfo.setAuditEvent(tokens.nextToken());
    			 data.add(vwdocumentHistoryInfo);
    			 tokens = null;
    		 }catch(Exception ex){
    			 vwc.printToConsole("Exception in loadAudit Details" + ex.getMessage());
    		 }
    	 }
    	 Table.addData(data,session,"VW_Room");
     }
     
     private void getSummaryDetails(String value){
    	 try{
    	 StringTokenizer tokens = new StringTokenizer(value, Util.SepChar);
    	 tokens.nextToken();    	 //docId = Util.to_Number(tokens.nextToken());
    	 documentName = tokens.nextToken();tokens.nextToken();
    	 }catch(Exception ex){
    		 vwc.printToConsole("Exception in getSummaryDetails");
    	 }
 	 
     }
     
    private class KeyBuffer extends Thread
    {
        private String prefix = "";
        private boolean run = true;
        public KeyBuffer()
        {
            setPriority(Thread.MIN_PRIORITY);
        }
        public void appendKey(char c)
        {
            prefix+=c;
        }
        public void kill()
        {
            run = false;
        }
        public void run()
        {
            while (run)
            {
                Util.sleep(100);
                if (prefix.length() == 0) continue;
                prefix = "";
            }
        }
    }
    private JScrollPane scRoutePane;
    private VWDocumentAuditTrailsDetailsTable Table;
    private JButton btnClose;
    
    private JButton btnPrint;
    private KeyBuffer keyBuffer = new KeyBuffer();;
    private JPanel routeMain;
        
 
    private static VWClient vwc;
    private int session;
    private int docId;
    
    private String documentName = "";
    private static boolean active = false;
    private Vector auditDetails = new Vector();
    private static JFrame dlgMain;
    public static int prevSID = 0;
    public static int prevDocId = 0;
  
    public int print(Graphics g, PageFormat pageFormat, int pageIndex) throws PrinterException {
    	Graphics2D g2 = (Graphics2D)g;
        g2.setColor(Color.black);
        int fontHeight = g2.getFontMetrics().getHeight();
        int fontDesent = g2.getFontMetrics().getDescent();
        //leave room for page number...
        double pageHeight = pageFormat.getImageableHeight() - fontHeight;
        double pageWidth = pageFormat.getImageableWidth();
        double tableWidth = (double)Table.getColumnModel().getTotalColumnWidth();
        double scale = 1;
        if (tableWidth >= pageWidth) {
            scale = pageWidth / tableWidth;
        }
        double headerHeightOnPage = Table.getTableHeader().getHeight() * scale;
        double tableWidthOnPage = tableWidth * scale;
        double oneRowHeight = (Table.getRowHeight() + Table.getRowMargin()) * scale;
        int numRowsOnAPage = (int)((pageHeight - headerHeightOnPage) / oneRowHeight);
        double pageHeightForTable = oneRowHeight * numRowsOnAPage;
        int totalNumPages = (int)Math.ceil(((double)Table.getRowCount()) / numRowsOnAPage);
        if (pageIndex >= totalNumPages) {
            return NO_SUCH_PAGE;
        }

        g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        
        //Caption Route Summary...
        g2.drawString(connectorManager.getString("audit.details")+" "+ documentName,5,10);
        
        g2.translate(0f, headerHeightOnPage);
        g2.translate(0f, -pageIndex * pageHeightForTable);
        if (pageIndex + 1 == totalNumPages) {
            int lastRowPrinted = numRowsOnAPage * pageIndex;
            int numRowsLeft = Table.getRowCount() - lastRowPrinted;
            g2.setClip(0, (int)(pageHeightForTable * pageIndex), (int)Math.ceil(tableWidthOnPage), (int)Math.ceil(oneRowHeight * numRowsLeft));
        }
        else {
            g2.setClip(0, (int)(pageHeightForTable * pageIndex), (int)Math.ceil(tableWidthOnPage),(int)Math.ceil(pageHeightForTable));
        }
        g2.scale(scale, scale);
        Table.paint(g2);
        g2.drawString("Page: " + (pageIndex + 1), (int)pageWidth / 2 - 35, (int) (pageHeight + fontHeight - fontDesent));
        g2.translate(0f, headerHeightOnPage);
        g2.translate(0f, -pageIndex * pageHeightForTable);
        if (pageIndex + 1 == totalNumPages) {
            int lastRowPrinted = numRowsOnAPage * pageIndex;
            int numRowsLeft = Table.getRowCount() - lastRowPrinted;
            g2.setClip(0, (int)(pageHeightForTable * pageIndex), (int)Math.ceil(tableWidthOnPage), (int)Math.ceil(oneRowHeight * numRowsLeft));
        }
        else {
            g2.setClip(0, (int)(pageHeightForTable * pageIndex), (int)Math.ceil(tableWidthOnPage),(int)Math.ceil(pageHeightForTable));
        }
        g2.scale(scale, scale);
        
        Table.paint(g2);
        g2.scale(1 / scale, 1 / scale);
        g2.translate(0f, pageIndex * pageHeightForTable);
        g2.translate(0f, -headerHeightOnPage);
        g2.setClip(0, 0, (int)Math.ceil(tableWidthOnPage), (int)Math.ceil(headerHeightOnPage));
        g2.scale(scale, scale);
        Table.getTableHeader().paint(g2);
        return Printable.PAGE_EXISTS;
    }

    public void doPrint() {
    	active = false;
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setPrintable(this);
        if (pj.printDialog()) {
            try {
                pj.print();
            }
            catch (PrinterException ee) {
                System.out.println(ee);
            }
            active = true;
        }
    }

    /*public static void main(String[] args) {
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}catch(Exception ex){}
		new Thread(new VWRouteSummary(1)).start();		
	} */

}



/*public class AuditTrail {
	public static void main(String[] args) {
		
		JFrame f=new JFrame();
		
		f.getContentPane().setLayout(null);
		VWDocumentAuditTrailsDetailsTable table = new VWDocumentAuditTrailsDetailsTable();
		JScrollPane sp=new JScrollPane(table);
		table.setBackground(Color.white);
		
		
		VWDocumentHistoryInfo info = new VWDocumentHistoryInfo();
		info.setAuditEvent("Event1");
		info.setAuditDate("Date1");
		info.setAuditUserName("UserName1");
		
		Vector documentHistory = new Vector();
		documentHistory.add(info);
		table.addData(documentHistory, 2, "");
		
		
		sp.setBounds(20,20,400,200);
		f.getContentPane().add(sp);
		f.setSize(600,300);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
		

	}

}
*/