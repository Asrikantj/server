package com.computhink.vwc.VWDocATDetails;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import javax.swing.AbstractCellEditor;
import javax.swing.DefaultCellEditor;
import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.event.TableModelEvent;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

//import ViewWise.AdminWise.AdminWise;
//import ViewWise.WebClient.VWConstant;
//import ViewWise.WebClient.VWImages.VWImages;
//import ViewWise.WebClient.VWTable.VWTableCellRenderer;


import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWTableResizer;
import com.computhink.common.Document;
import com.computhink.common.RouteMasterInfo;
import com.computhink.vwc.VWCConstants;
/**
 * @author Nishad Nambiar
 *
 */



public class VWDocumentAuditTrailsDetailsTable extends JTable implements Transferable, VWCConstants
{
	public static boolean emailIconFlag = false;
	public static boolean actionIconFlag = false;
	private static ResourceManager resourceManager=null;
    public static VWDocumentAuditTrailsDetailsTableData m_data;
    TableColumn[] ColModel=new TableColumn[10];
    static boolean[] visibleCol={true, true, true, true, true, true, true};
    Timer keyTimer;    
    //VWCheckBox checkBox = new VWCheckBox();
    
    public boolean isCellEditable(int row, int column) {
    	return false;
    }
    
    public VWDocumentAuditTrailsDetailsTable(){
        super();
        m_data = new VWDocumentAuditTrailsDetailsTableData(this);
        setAutoCreateColumnsFromModel(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        
        JTextField readOnlyText=new JTextField();
        readOnlyText.setEditable(true);
        Dimension tableWith = getPreferredScrollableViewportSize();
        setFont(readOnlyText.getFont());
        for (int k = 0; k < VWDocumentAuditTrailsDetailsTableData.m_columns.length; k++) {
	        TableCellRenderer renderer;
	        
	        DefaultTableCellRenderer textRenderer;
	        System.out.println("VWAuditTrailDetails.isWebClient :: "+VWAuditTrailDetails.isWebClient);
	        if(VWAuditTrailDetails.isWebClient)
	        	textRenderer =new DefaultTableCellRenderer();
	        else
	        	textRenderer =new VWTableCellRenderer();
	        
	        textRenderer.setHorizontalAlignment(VWDocumentAuditTrailsDetailsTableData.m_columns[k].m_alignment);
	       	renderer = textRenderer;
	       	VWDocumentAuditTrailsDetailsRowEditor editor=new VWDocumentAuditTrailsDetailsRowEditor(this);
	        TableColumn column = new TableColumn
	            (k,Math.round(tableWith.width*VWDocumentAuditTrailsDetailsTableData.m_columns[k].m_width), 
	                renderer, editor);
	       /* if( (k==0) ){
	        	column.setCellRenderer(new VWCheckBoxRenderer());
	        	column.setCellEditor(new EmailCheckBoxEditor());
	        }
	        
	        if( (k==1) ){
	        	column.setCellRenderer(new VWActionCheckBoxRenderer());
	        	column.setCellEditor(new ActionCheckBoxEditor1());
	        }*/
	        addColumn(column);   
	        ColModel[k]=column;
        }
        
        JTableHeader header = getTableHeader();
        header.setUpdateTableInRealTime(false);
        SymMouse aSymMouse = new SymMouse();
        addMouseListener(aSymMouse);
        //header.addMouseListener(m_data.new ColumnListener(this));
        header.setReorderingAllowed(true);
       // setVisibleCols();
        
        if(!VWAuditTrailDetails.isWebClient)
        	setBackground(java.awt.Color.white);
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);
        keyTimer = new Timer(500, new keyTimerAction());
        keyTimer.setRepeats(false);
        //setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		//setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS); //added by Srikanth on 19 Nov 2015 for resizing all column

        setDragEnabled(true); 
    
        
        getColumnModel().getColumn(0).setPreferredWidth(100);
        getColumnModel().getColumn(1).setPreferredWidth(100);
		getColumnModel().getColumn(2).setPreferredWidth(100);
		getColumnModel().getColumn(3).setPreferredWidth(100);
		getColumnModel().getColumn(4).setPreferredWidth(100);
		getColumnModel().getColumn(5).setPreferredWidth(100);
		getColumnModel().getColumn(6).setPreferredWidth(100);
		getColumnModel().getColumn(7).setPreferredWidth(100);
		
		//getColumnModel().getColumn(3).setPreferredWidth(104);
	//	getColumnModel().getColumn(4).setPreferredWidth(104);
	//	getColumnModel().getColumn(5).setPreferredWidth(72);
		
		
		getColumnModel().getColumn(0).setResizable(true);
		getColumnModel().getColumn(1).setResizable(true);
		
		setShowGrid(false);
		//setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS); //added by Srikanth on 19 Nov 2015 for resizing all column
	//	getColumnModel().getColumn(2).setResizable(true);
	//	getColumnModel().getColumn(3).setResizable(true);
		/*
		 * CV10.1 Enhancement 2149
		 * Allow user to change order of columns in document history windows. 
		 * getTableHeader().setReorderingAllowed(false); modified to true
		 */
		getTableHeader().setReorderingAllowed(true);
		
		//Column E-Signature hidden.
//		getColumnModel().getColumn(6).setPreferredWidth(0); 
//		getColumnModel().getColumn(6).setMinWidth(0);
//		getColumnModel().getColumn(6).setMaxWidth(0);
//		setBackground(Color.white);

		//Column Approver ID hidden.
	//	getColumnModel().getColumn(7).setPreferredWidth(0); 
	//	getColumnModel().getColumn(7).setMinWidth(0);
	//	getColumnModel().getColumn(7).setMaxWidth(0);
		if(!VWAuditTrailDetails.isWebClient)
			setBackground(Color.white);
		setRowHeight(20);
		
        
		ApproversColumnHeaderToolTips tips = new ApproversColumnHeaderToolTips();
	    
	    // Assign a tooltip for each of the columns
	    for (int c=0; c<getColumnCount(); c++) {
	        TableColumn col = getColumnModel().getColumn(c);
	        if(c==0){
	        	tips.setToolTip(col, "Object Name");
	        }
	        else if(c==1){
	        	tips.setToolTip(col, "Object Type");
	        } 
	        else if(c==2){
	        	tips.setToolTip(col, "Event");
	        } 
/*	        else if(c==3){
	        	tips.setToolTip(col, "Task");
	        } 
	        else if(c==4){
	        	tips.setToolTip(col, "Task Description");
	        } 
	        else if(c==5){
	        	tips.setToolTip(col, "Task Length");
	        }
*/	        
	    }
	    header.addMouseMotionListener(tips);
	    
	    getColumnModel().getColumn(0).setPreferredWidth(0); 
		getColumnModel().getColumn(0).setMinWidth(0);
		getColumnModel().getColumn(0).setMaxWidth(0);
		
	    getColumnModel().getColumn(1).setPreferredWidth(0); 
		getColumnModel().getColumn(1).setMinWidth(0);
		getColumnModel().getColumn(1).setMaxWidth(0);
		
		setTableResizable();
  }
    
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
    
    public class ApproversColumnHeaderToolTips extends MouseMotionAdapter {
        // Current column whose tooltip is being displayed.
        // This variable is used to minimize the calls to setToolTipText().
        TableColumn curCol;
    
        // Maps TableColumn objects to tooltips
        Map tips = new HashMap();
    
        // If tooltip is null, removes any tooltip text.
        public void setToolTip(TableColumn col, String tooltip) {
            if (tooltip == null) {
                tips.remove(col);
            } else {
                tips.put(col, tooltip);
            }
        }
    
        public void mouseMoved(MouseEvent evt) {
            TableColumn col = null;
            JTableHeader header = (JTableHeader)evt.getSource();
            JTable table = header.getTable();
            TableColumnModel colModel = table.getColumnModel();
            int vColIndex = colModel.getColumnIndexAtX(evt.getX());
    
            // Return if not clicked on any column header
            if (vColIndex >= 0) {
                col = colModel.getColumn(vColIndex);
            }
    
            if (col != curCol) {
                header.setToolTipText((String)tips.get(col));
                curCol = col;
            }
        }
    }
    
//------------------------------------------------------------------------------
  public void addData(Vector docs, int roomId,String roomName)
  {
        m_data.setData(docs,roomId,roomName);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public int getRowCount()
  {
      if (m_data==null) return 0;
      return m_data.getRowCount();
  }
//------------------------------------------------------------------------------
  /*public void insertData(Vector docs,int roomId,String roomName)
  {
     if(docs==null || docs.size()==0)
        return;
    for(int i=0;i<docs.size();i++)
    {
    	VWApproverInfo doc=(VWApproverInfo)docs.get(i);
        m_data.insert(doc);
    }
     m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public void deleteRow(int row)
  {
        m_data.delete(row);
        m_data.fireTableDataChanged();
  }
//------------------------------------------------------------------------------
  public Vector getData()
  {
	  	m_data.fireTableDataChanged();
        return m_data.getData();
  }
//------------------------------------------------------------------------------
  public VWApproverInfo getRowData(int rowNum)
  {
        return m_data.getRowData(rowNum);
  }
//------------------------------------------------------------------------------
  public String getRowApproverId(int rowNum)
  {
	  	VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverId();
  }
//------------------------------------------------------------------------------
  public String getRowRoomId(int rowNum)
  {
	  	VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverId();
        
  }
//------------------------------------------------------------------------------
  public String getRowApproverName(int rowNum)
  {
        VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverName();
  }
//------------------------------------------------------------------------------
  public String getRowApproverAction(int rowNum)
  {
        VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverAction();
  }
//------------------------------------------------------------------------------
  public String getRowApproverEmail(int rowNum)
  {
        VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverEmail();
  }
//------------------------------------------------------------------------------
  public String getRowApproverTaskDescription(int rowNum)
  {
        VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverTaskDescription();
  }
//------------------------------------------------------------------------------
  public String getRowApproverTaskLength(int rowNum)
  {
        VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverTaskLength();
  }
  
//------------------------------------------------------------------------------
  public String getRowApproverESignature(int rowNum)
  {
        VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverESignature();
  }

//------------------------------------------------------------------------------
  public String getRowApproverEmailIcon(int rowNum)
  {
        VWApproverInfo row=m_data.getRowData(rowNum);
        if(row==null) return "";
        return row.getApproverESignature();
  }*/
  
//------------------------------------------------------------------------------
  public DataFlavor[] getTransferDataFlavors()
  {
      int i=0;
      int[] selRows=getSelectedRows();
      int count = selRows.length;
      DataFlavor[] data= new DataFlavor[count];
      for(i=0;i<count;i++)
          data[i]=new DataFlavor(getDocument(selRows[i]).getClass(),"Document");
      return data;
  }
  public Object getTransferData(DataFlavor flavor) throws UnsupportedFlavorException,IOException
  {
      /*int selRow=getSelectedRow();
      if(selRow>=0)
        return this.getDocument(selRow);*/
      return null;
  }
    //--------------------------------------------------------------------------
  public boolean isDataFlavorSupported(DataFlavor flavor)
  {
      int selRow=getSelectedRow();
      if(selRow>=0) return true;
      return false;
  }

  public RouteMasterInfo getDocument(int rowNum)
  {
        return m_data.getDocument(rowNum);
  }

//------------------------------------------------------------------------------ 
  private void setVisibleCols()
  {
    TableColumnModel model=getColumnModel();
    int count =visibleCol.length;
    int i=0;
    while(i<count)
    {
        if (!visibleCol[i])
        {
            TableColumn column = model.getColumn(i);
            model.removeColumn(column);
            count--;
        }
        else
        {
            i++;
        }
    }
    if(count==0)return;
    Dimension tableWith = getPreferredScrollableViewportSize();
    int colWidth=(int)Math.round(tableWith.getWidth()/count);
    for (i=0;i<count;i++)
    {
        model.getColumn(i).setPreferredWidth(colWidth);
    }
    setColumnModel(model);
}
//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyPressed(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if (object instanceof JTable)
                    Table_keyPressed(event);
        }
    }
//------------------------------------------------------------------------------
    void Table_keyPressed(java.awt.event.KeyEvent event)
	{
            //int selRow=getSelectedRow();
            if(event.getKeyCode()==event.VK_DOWN)
            {
                keyTimer.stop();
                keyTimer.start();
            }   
            else if(event.getKeyCode()==event.VK_UP)
            {
                keyTimer.stop();
                keyTimer.start();
            }
            else if(event.getKeyCode()==event.VK_ENTER)
            {
                LoadRowData(getSelectedRow());
                event.setKeyCode(event.VK_UNDEFINED);
            }
	}
//------------------------------------------------------------------------------
    protected class keyTimerAction implements java.awt.event.ActionListener {
    public void actionPerformed(java.awt.event.ActionEvent e) {
        LoadPropData(getSelectedRow());
        keyTimer.stop();
        }
    }
//------------------------------------------------------------------------------
class SymMouse extends java.awt.event.MouseAdapter{
    public void mouseClicked(java.awt.event.MouseEvent event)
    {
    	  Point origin = event.getPoint();
    	    int row = rowAtPoint(origin);
    	    int column = columnAtPoint(origin);
    	    System.out.println("SymMouse : Row :"+row);
    	    System.out.println("SymMouse : Column :"+column);
    	    if (row == -1 || column == -1)
    	       return; // no cell found
    	    if(event.getClickCount() == 1)
    	    lSingleClick(event,row,column);
    }
  }
//------------------------------------------------------------------------------
void VWToDoListTable_LeftMouseClicked(java.awt.event.MouseEvent event)
{
	
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    System.out.println("Row Selected :"+row);
    System.out.println("Column Selected :"+column);
    if (row == -1 || column == -1)
       return; // no cell found
    
    if(event.getClickCount() == 1)
    	lSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
        lDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
void VWToDoListTable_RightMouseClicked(java.awt.event.MouseEvent event)
{
    Point origin = event.getPoint();
    int row = rowAtPoint(origin);
    int column = columnAtPoint(origin);
    if (row == -1 || column == -1)
       return; // no cell found
    if(event.getClickCount() == 1)
        rSingleClick(event,row,column);
    else if(event.getClickCount() == 2)
            rDoubleClick(event,row,column);
}
//------------------------------------------------------------------------------
private void rSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
 /*
   
    VWMenu menu=null;
    //int menuId=VWConstant.Document_TYPE;
    int eventY=event.getY();
    int roomId=getRowRoomId(row);
    int docId=getRowDocId(row);
    if(this.getSelectedRowCount()==0) return;
    menu=new VWMenu(VWConstant.DocumentRouteToDoList_TYPE);    
    menu.show(this,event.getX(),eventY);
 */
	
}
//------------------------------------------------------------------------------
private void rDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
}
//------------------------------------------------------------------------------
private void lSingleClick(java.awt.event.MouseEvent event,int row,int col)
{
	/*if (row >= getData().size()) return;
	VWApproverInfo vwApproverInfo = (VWApproverInfo)getData().elementAt(row);
	if(col==0){
		AdminWise.printToConsole(" email "+vwApproverInfo.getApproverEmailFlag());
		if(vwApproverInfo.getApproverEmailFlag())
    		vwApproverInfo.setApproverEmailFlag(false);
    	else
    		vwApproverInfo.setApproverEmailFlag(true);
		AdminWise.printToConsole("email "+vwApproverInfo.getApproverEmailFlag());
	}
	else if(col==1){
		AdminWise.printToConsole(" Action "+vwApproverInfo.getApproverActionFlag());
		if(vwApproverInfo.getApproverActionFlag())
    		vwApproverInfo.setApproverActionFlag(false);
    	else
    		vwApproverInfo.setApproverActionFlag(true);
		AdminWise.printToConsole(" Action "+vwApproverInfo.getApproverActionFlag());		
	}*/
	repaint();
}
//------------------------------------------------------------------------------
private void lDoubleClick(java.awt.event.MouseEvent event,int row,int col)
{
	/*
    LoadRowData(row);
    LoadPropData(row);
    */
 }
//------------------------------------------------------------------------------
 private void LoadPropData(int row)
{
	 /*
	 RouteMasterInfo routeMasterInfo =getDocument(row);
	    int docId=getRowDocId(row);
	    int roomId=getRowRoomId(row);
	    if (roomId>0 && docId>0)
	    {
	    	Document doc = new Document(docId);
	    	doc.setRoomId(roomId);
	        VWWeb.webPanel.propertiesPanel.loadDocProperties(doc);
	        VWWeb.webPanel.commentPanel.loadDocComments(roomId,docId);
	    }
	*/
	 
}
//------------------------------------------------------------------------------
private void LoadRowData(int row)
{
	/*
    int docId=getRowDocId(row);
    int roomId=getRowRoomId(row);
    //String searchString=getRowSearchString(row);
   IconData idata = (IconData)getValueAt(row,1);
    String docName = (String)idata.m_data;
    ///VWTreeConnector.setTreeNodeSelection(roomId,docId,false,3,false);
    //String docVerRev=getRowVerRev(row);
    String docVerRev="";
    if(docVerRev==null || docVerRev.equals("") || docVerRev.equals(".")) docVerRev="";
    VWTableConnector.getDocFile(roomId,docId,docName,VWTreeConnector.getFixTreePath(),
    		docVerRev,"");
    VWTreeConnector.setTreeNodeSelection(roomId,docId,false,3,false);
   	VWWeb.webPanel.documentPanel.Table.highlightDocumentTableRow(roomId, docId);
   	*/
	
}

	public void updateRow(int rowId, boolean freeze){
		//m_data.updateFreezeRow(rowId,freeze);
		m_data.fireTableDataChanged();
	}
}
//------------------------------------------------------------------------------
class ToDoListColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public ToDoListColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
//------------------------------------------------------------------------------
class VWDocumentAuditTrailsDetailsTableData extends DefaultTableModel
{

	public static final ToDoListColumnData m_columns[] = {
	    new ToDoListColumnData(VWAuditTrailDetails.connectorManager.getString("DocAuditTrailCol.ObjName"),0.1F,JLabel.LEFT),
	    new ToDoListColumnData(VWAuditTrailDetails.connectorManager.getString("DocAuditTrailCol.ObjTyp"),0.1F,JLabel.LEFT),
	    new ToDoListColumnData(VWAuditTrailDetails.connectorManager.getString("DocAuditTrailCol.Event"),0.1F,JLabel.LEFT ),
	    new ToDoListColumnData(VWAuditTrailDetails.connectorManager.getString("DocAuditTrailCol.UserName"),0.2F, JLabel.LEFT),
	    new ToDoListColumnData(VWAuditTrailDetails.connectorManager.getString("DocAuditTrailCol.Description"),0.1F,JLabel.LEFT),
	    new ToDoListColumnData(VWAuditTrailDetails.connectorManager.getString("DocAuditTrailCol.Date"),0.2F,JLabel.LEFT ),
	    new ToDoListColumnData(VWAuditTrailDetails.connectorManager.getString("DocAuditTrailCol.Location"),0.1F,JLabel.LEFT),    
	    new ToDoListColumnData(VWAuditTrailDetails.connectorManager.getString("DocAuditTrailCol.ClientHost"),0.1F,JLabel.LEFT),
	  };
    public static final int COL_OBJECT_NAME=0;
    public static final int COL_OBJECT_TYPE=1;  
    public static final int COL_AUDIT_EVENT=2;
    public static final int COL_AUDIT_DATE=5;//3
    public static final int COL_AUDIT_USERNAME=3;//4
    
    public static final int COL_CLIENT_HOST=7;//5
    public static final int COL_LOCATION=6;
    public static final int COL_DESCRIPTION=4;//7
  
  protected VWDocumentAuditTrailsDetailsTable m_parent;
  protected Vector m_vector;
  protected int m_sortCol = 0;
  protected boolean m_sortAsc = true;

  public VWDocumentAuditTrailsDetailsTableData(VWDocumentAuditTrailsDetailsTable parent) {
    m_parent = parent;
    m_vector = new Vector();
  }
  
  public Class getColumnClass(int columnIndex) {	
	  //if(columnIndex == 0)
		//  return Boolean.class;
	  //else
		  return Object.class;
  }
  
//------------------------------------------------------------------------------
public void setData(Vector docs,int roomId,String roomName) {
    m_vector.removeAllElements();
    if(docs==null || docs.size()==0)    return;
    int count =docs.size();
    int freezed = -1;
    for(int i=0;i<count;i++)
    {
    	VWDocumentHistoryInfo historyInfo=(VWDocumentHistoryInfo)docs.get(i);
        m_vector.addElement(historyInfo);
    }
  }

public RouteMasterInfo getDocument(int rowNum) {
    return (RouteMasterInfo)m_vector.elementAt(rowNum);
}
public Document getDocumentObject(int rowNum) {
	Document doc = new Document(((RouteMasterInfo)m_vector.elementAt(rowNum)).getDocId());    
    return doc;
}

//------------------------------------------------------------------------------
public Vector getData() {
    return m_vector;
  }
//------------------------------------------------------------------------------
/*public VWApproverInfo getRowData(int rowNum) {
    
    try
    {
        return (VWApproverInfo)m_vector.elementAt(rowNum);
    }
    catch(Exception e)
    {
        return null;
    } 
  }*/

//------------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
//------------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
//------------------------------------------------------------------------------
  public String getColumnName(int column) { 
    ///return m_columns[column].m_title; 
      String str = m_columns[column].m_title;
    if (column==m_sortCol)
      str += m_sortAsc ? " �" : " �";
    return str;
  }
//------------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
	  return false;
  }
//------------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    VWDocumentHistoryInfo row = (VWDocumentHistoryInfo)m_vector.elementAt(nRow);
    switch (nCol) {
    	case COL_AUDIT_EVENT: return row.getAuditEvent() ;
    	case COL_AUDIT_DATE: return row.getAuditDate();
    	case COL_AUDIT_USERNAME: return  row.getAuditUserName();
    	case COL_OBJECT_NAME: return row.getAuditObjectName();              
	    case COL_OBJECT_TYPE: return row.getAuditObjectType();
	    case COL_CLIENT_HOST: return row.getAuditClientHost();
	    case COL_LOCATION: return row.getAuditLocation();
	    case COL_DESCRIPTION: return row.getAuditDescription();
   
	
    }
    return "";
  }
  //-----------------------------------------------------------
  
  
  /*private ImageIcon getEmailIcon(VWApproverInfo row)
	{
	  if(row.getApproverEmailFlag())
		  return VWImages.OkIcon;
	  else
		  return VWImages.DelIcon;
	}
  	private ImageIcon getActionIcon(VWApproverInfo row)
	{
	  if(row.getApproverActionFlag())
		  return VWImages.OkIcon;
	  else
		  return VWImages.DelIcon;
	}*/
  
  private ImageIcon getDocIcon(int signId, int status, boolean flag)
  {
	  
  //Fix Start : Issue 496 ; Change the Icon when the document is check out
//  Fix Start : New issue 537 ; Change the Icon when the document is opened
 // 	 if(flag){
//  		 System.out.println("ICON 2");
  	 	//return VWImages.CopyIcon;
 // 		 return VWImages.UpIcon;
 // 	  }else {//if(status > 0 && signId == -2){
  	  		//return VWImages.DelIcon;
  //		System.out.println("ICON 3");
 // 		return VWImages.DownIcon;
  		  return new ImageIcon("");
  	 }
  	//Fix End
// Fix End
      /*if(signId==-1)
      {
          //return VWImages.AddIcon;
    	  System.out.println("ICON 4");
    	  return new ImageIcon("D:\\Projects\\ViewWise_MainLine\\IXRc\\ViewWise-DEV\\Source\\Build\\Source\\Java\\AdminWise\\ViewWise\\AdminWise\\VWImages\\images\\_UserAdd.gif");
      }
      else if(signId==0)
      {
    	  // Issue No 40 : Wrong image was loaded Created By : Valli Date 11 Oct 2006 
    	//  return VWImages.AllRoomIcon;
          //return VWImages.DocBCollapsedIcon;
    	  System.out.println("ICON 5");
    	  return new ImageIcon("D:\\Projects\\ViewWise_MainLine\\IXRc\\ViewWise-DEV\\Source\\Build\\Source\\Java\\AdminWise\\ViewWise\\AdminWise\\VWImages\\images\\_UserRemove.gif");
      }
      else if(signId>0)
      {
       //   return VWImages.BackupIcon;
    	  System.out.println("ICON 6");
    	  if(VWToDoListTable.emailIconFlag){
    		  return new ImageIcon("C:\\Inetpub\\wwwroot\\Save.gif");
    	  }else{
    		  return new ImageIcon("C:\\Inetpub\\wwwroot\\Ok.gif");
    	  }
    	  
    	  
      }
      
      
      
//    Fix Start : New issue 537 ; Change the Icon when the document is opened
      else if(signId == -2)
      {
         // return VWImages.AdvancedFindImage;
    	  System.out.println("ICON 7");
    	  return new ImageIcon("D:\\Projects\\ViewWise_MainLine\\IXRc\\ViewWise-DEV\\Source\\Build\\Source\\Java\\AdminWise\\ViewWise\\AdminWise\\VWImages\\images\\DelIcon.gif");
      }*/
      
      
      
      //Fix end
      //return null;
 // }
    //-----------------------------------------------------------
//------------------------------------------------------------------------------
/*  public void setValueAt(Object value, int nRow, int nCol, int x) {
    if (nRow < 0 || nRow >= getRowCount())
      return;
    
     VWApproverInfo row = (VWApproverInfo)m_vector.elementAt(nRow);
    //Document row = (Document)m_vector.elementAt(nRow);
    
    String svalue = value.toString();

    if(nCol==COL_APPROVERNAME)row.setApproverName(svalue);    

 //-----------------------------------------------------------------------------
  }*/
	
  
  public void setValueAt(Object value, int nRow, int nCol){
	  if (nRow < 0 || nRow >= getRowCount())
	      return;
	//    VWApproverInfo row = (VWApproverInfo)m_vector.elementAt(nRow);
	  VWDocumentHistoryInfo row = (VWDocumentHistoryInfo)m_vector.elementAt(nRow);
	    String svalue = value.toString();
	    switch (nCol) {
	    case COL_AUDIT_EVENT: 
	    	row.setAuditEvent("svalue");
	        break;
	    case COL_AUDIT_DATE:
	    	row.setAuditDate("svalue");
	        break;
	    
	     case COL_AUDIT_USERNAME:
	        row.setAuditUserName("svalue");
	        break;
	    case COL_OBJECT_NAME:
	        row.setAuditObjectName("svalue");  
	        break;
	    case COL_OBJECT_TYPE:
	        row.setAuditObjectType("svalue");
	        break;
	    case COL_CLIENT_HOST:
	        row.setAuditClientHost("svalue");
	        break;
	    case COL_LOCATION:
	        row.setAuditLocation("svalue");
	        break;
	    case COL_DESCRIPTION:
	        row.setAuditDescription("svalue");
	        break;
	    }  
  }
//----------------------------------------------------------------------------
 /* public void insert(int row,VWApproverInfo approverinfo){
    if (row < 0)
      row = 0;
    if (row > m_vector.size())
      row = m_vector.size();
    m_vector.insertElementAt(approverinfo, row);
  }
  //----------------------------------------------------------------------------
  public void insert(VWApproverInfo rowData) {
    int count=m_vector.size();
    boolean found=false;
    for(int i=0;i<count;i++)
    {
    	VWApproverInfo tmpRowData=(VWApproverInfo)m_vector.get(i);
        if(tmpRowData.getApproverId()==rowData.getApproverId() && 
            tmpRowData.getApproverId() == rowData.getApproverId())
        {
            found=true;
            break;
        }
    }
    if(!found) m_vector.addElement(rowData);
  }*/
//----------------------------------------------------------------------------
  public boolean delete(int row) {
    if (row < 0 || row >= m_vector.size())
      return false;
    m_vector.remove(row);
      return true;
  }
  //----------------------------------------------------------------------------
public void clear(){
    m_vector.removeAllElements();
  }
//------------------------------------------------------------------------------
   class ColumnListener extends MouseAdapter
  {
    protected VWDocumentAuditTrailsDetailsTable m_table;
//------------------------------------------------------------------------------
    public ColumnListener(VWDocumentAuditTrailsDetailsTable table){
      m_table = table;
    }
//------------------------------------------------------------------------------
    public void mouseClicked(MouseEvent e){
      
        if(e.getModifiers()==e.BUTTON3_MASK)
            selectViewCol(e);
	else if(e.getModifiers()==e.BUTTON1_MASK)
            sortCol(e);
    }
//------------------------------------------------------------------------------
    private void sortCol(MouseEvent e)
    {
        TableColumnModel colModel = m_table.getColumnModel();
        int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
        int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();

        if (modelIndex < 0)
        return;
        if (m_sortCol==modelIndex)
        m_sortAsc = !m_sortAsc;
        else
        m_sortCol = modelIndex;

        for (int i=0; i < colModel.getColumnCount();i++) {
        TableColumn column = colModel.getColumn(i);
        column.setHeaderValue(getColumnName(column.getModelIndex()));    
        }
        m_table.getTableHeader().repaint();  

        Collections.sort(m_vector, new 
        ToDoListComparator(modelIndex, m_sortAsc));
        m_table.tableChanged(
        new TableModelEvent(VWDocumentAuditTrailsDetailsTableData.this)); 
        m_table.repaint();  
    }
//------------------------------------------------------------------------------
    private void selectViewCol(MouseEvent e)
    {
    	/*
        javax.swing.JPopupMenu menu=new javax.swing.JPopupMenu("View");
        for (int i=0; i < m_table.ToDoListColumnNames.length; i++){
         javax.swing.JCheckBoxMenuItem subMenu=new javax.swing.JCheckBoxMenuItem(
                m_table.ToDoListColumnNames[i],m_table.visibleCol[i]);
        TableColumn column = m_table.ColModel[i];
        subMenu.addActionListener(new ColumnKeeper(column,VWToDoListTableData.m_columns[i]));
        menu.add(subMenu);
        }
        menu.show(m_table,e.getX(),e.getY());
        */
    }
  class ColumnKeeper implements java.awt.event.ActionListener
  {
    protected TableColumn m_column;
    protected ToDoListColumnData  m_colData;

    public ColumnKeeper(TableColumn column,ToDoListColumnData colData){
      m_column = column;
      m_colData = colData;
    }

    public void actionPerformed(java.awt.event.ActionEvent e) {
    	/*
    	javax.swing.JCheckBoxMenuItem item=(javax.swing.JCheckBoxMenuItem)e.getSource();
      TableColumnModel model = m_table.getColumnModel();
      boolean found=false;
      int i=0;
      int count=m_table.ToDoListColumnNames.length;
      int colCount=model.getColumnCount();
      while (i<count && !found){
        if(m_table.ToDoListColumnNames[i].equals(e.getActionCommand()))
            found=true;
        i++;
        }
      i--;
      if (item.isSelected()) {
            m_table.visibleCol[i]=!m_table.visibleCol[i];
            model.addColumn(m_column);
      }
      else {
        if(colCount>1)
        {
            m_table.visibleCol[i]=!m_table.visibleCol[i];
            model.removeColumn(m_column);
        }
      }
      m_table.tableChanged(new javax.swing.event.TableModelEvent(m_table.m_data)); 
      m_table.repaint();
      
      */
    }
    
  }
  
  }
//------------------------------------------------------------------------------
class ToDoListComparator implements Comparator
{
    protected int     m_sortCol;
    protected boolean m_sortAsc;
//------------------------------------------------------------------------------
    public ToDoListComparator(int sortCol, boolean sortAsc) {
    m_sortCol = sortCol;
    m_sortAsc = sortAsc;
    }
//------------------------------------------------------------------------------
    //Not in use - Valli
    public int compare(Object o1, Object o2) {
    if(!(o1 instanceof RouteMasterInfo) || !(o2 instanceof RouteMasterInfo))
      return 0;
    RouteMasterInfo s1 = (RouteMasterInfo)o1;
    RouteMasterInfo s2 = (RouteMasterInfo)o2;
    /*if(!(o1 instanceof Document) || !(o2 instanceof Document))
        return 0;
      Document s1 = (Document)o1;
      Document s2 = (Document)o2;*/
    int result = 0;
    //double d1, d2;
    switch (m_sortCol) {
      case 0:
        result = s1.getRoomName().compareTo(s2.getRoomName());
        break;
      case 1:
        result = s1.getDocName().compareTo(s2.getDocName());
        break;
      case 2:
          result = s1.getRouteName().compareTo(s2.getRouteName());
          break;  
      /*case 3:
        result = s1.getDocType().getName().compareTo(s2.getDocType().getName());
        break;
      case 4:
        result = s1.getVersion().compareTo(s2.getVersion());
        break;*/
      case 3:
        result = s1.getCreatorName().compareTo(s2.getCreatorName());
        break;
      case 4:
        result = s1.getCreatedDate().compareTo(s2.getCreatedDate());
        break;
      case 5:
        result = s1.getModifiedDate().compareTo(s2.getModifiedDate());
        break;
      case 6:
          result = s1.getReceivedDate().compareTo(s2.getReceivedDate());
          break;
      /*case 7:
        result = String.valueOf(s1.getPageCount()).compareTo(String.valueOf(s2.getPageCount()));
        break;
      case 8:
        result = String.valueOf(s1.getCommentCount()).compareTo(String.valueOf(s2.getCommentCount()));
        break;*/
      /*case 9:
        result = String.valueOf(s1.getRefCount()).compareTo(String.valueOf(s2.getRefCount()));
        break;*/
    }
    if (!m_sortAsc)
      result = -result;
    return result;
    }
//------------------------------------------------------------------------------
    public boolean equals(Object obj) {
    if (obj instanceof ToDoListComparator) {
    	ToDoListComparator compObj = (ToDoListComparator)obj;
      return (compObj.m_sortCol==m_sortCol) && 
        (compObj.m_sortAsc==m_sortAsc);
    }
    return false;
    }
}

}




class EmailCheckBoxCellRenderer extends JCheckBox implements TableCellRenderer,MouseListener {
	JCheckBox check = null;
    public EmailCheckBoxCellRenderer() {
    	check = new JCheckBox();
   }
    
    public void mouseClicked(MouseEvent me){
  		System.out.println("Clicked");
  			if(VWDocumentAuditTrailsDetailsTable.actionIconFlag){
  				VWDocumentAuditTrailsDetailsTable.actionIconFlag = false;
  			}
  			else{
  				VWDocumentAuditTrailsDetailsTable.actionIconFlag = true;
  			}
  	}
    
    public void mouseEntered(MouseEvent me){

    }
	public void mouseExited(MouseEvent me){
	    	
    }
	public void mouseReleased(MouseEvent me){
		
	}
	public void mousePressed(MouseEvent me){
		
	}
    
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      setText(value.toString());
	  
      super.setPreferredSize(new Dimension (100, 20));
      setSize(table.getColumnModel().getColumn(column).getWidth(), getPreferredSize().height);
      if (table.getRowHeight(row) != getPreferredSize().height) {
    	  table.setRowHeight(row, getPreferredSize().height);
      }
      return check;
  }
} 

class EmailCheckBoxEditor extends AbstractCellEditor implements TableCellEditor,ActionListener {
	protected JCheckBox check;
	String txt="Hello";
	
	public Object getCellEditorValue () {
		return Boolean.valueOf (check.isSelected ());
	}
	
	public EmailCheckBoxEditor() {
		check = new JCheckBox();
		check.addActionListener(this);
		check.setEnabled(true);
	}
	
	public void actionPerformed(ActionEvent ae){
		{
		  			if(VWDocumentAuditTrailsDetailsTable.emailIconFlag){
		  				VWDocumentAuditTrailsDetailsTable.emailIconFlag = false;
		  			}
		  			else{
		  				VWDocumentAuditTrailsDetailsTable.emailIconFlag = true;
		  			}
		  	}
	}
	public Component getTableCellEditorComponent(javax.swing.JTable table, Object value, boolean isSelected, int row, int column) {
		txt = value.toString();
		return check;
	}
}

class ActionCheckBoxEditor1 extends AbstractCellEditor implements TableCellEditor,ActionListener {
	protected JCheckBox check;
	String txt="Hello";
	
	public Object getCellEditorValue () {
		return Boolean.valueOf (check.isSelected ());
	}

	public ActionCheckBoxEditor1() {
		check = new JCheckBox();
		check.addActionListener(this);
		check.setEnabled(true);
	}
	
	public void actionPerformed(ActionEvent ae){
		{
		  			if(VWDocumentAuditTrailsDetailsTable.actionIconFlag){
		  				VWDocumentAuditTrailsDetailsTable.actionIconFlag = false;
		  			}
		  			else{
		  				VWDocumentAuditTrailsDetailsTable.actionIconFlag = true;
		  			}
		  	}
	}

	public Component getTableCellEditorComponent(javax.swing.JTable table, Object value, boolean isSelected, int row, int column) {
		txt = value.toString();
		return check;
	}
}
