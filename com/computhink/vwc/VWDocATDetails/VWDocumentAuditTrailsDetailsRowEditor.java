package com.computhink.vwc.VWDocATDetails;

import java.awt.Component;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.EventObject;

import javax.swing.DefaultCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.CellEditorListener;
import javax.swing.table.TableCellEditor;

//import ViewWise.AdminWise.VWUtil.VWComboBox;

/**
 * @author Nishad Nambiar
 *
 */

public class VWDocumentAuditTrailsDetailsRowEditor implements TableCellEditor {
	  
	  protected TableCellEditor editor, defaultEditor, checkboxEditor;
	  protected VWDocumentAuditTrailsDetailsTable gTable=null;
	 // VWCheckBox checkbox = new VWCheckBox();
	  
	  
	  /**
	   * Constructs a EachRowEditor.
	   * create default editor 
	   *
	   * @see TableCellEditor
	   * @see DefaultCellEditor
	   */ 
	  public VWDocumentAuditTrailsDetailsRowEditor(VWDocumentAuditTrailsDetailsTable table) {
	    defaultEditor = new DefaultCellEditor(new JTextField());
	    //checkboxEditor = new DefaultCellEditor(checkbox);
	    gTable=table;
	  }
	  
	  /**
	   * @param row    table row
	   * @param editor table cell editor
	   */
	  
	  public Component getTableCellEditorComponent(JTable table,
	      Object value, boolean isSelected, int row, int column) {
				 editor=defaultEditor;     
		    return editor.getTableCellEditorComponent(table,
		             value, isSelected, row, column);
		    
	  }

	  public Object getCellEditorValue() {
	    return editor.getCellEditorValue();
	  }
	  public boolean stopCellEditing() {
	    return editor.stopCellEditing();
	  }
	  public void cancelCellEditing() {
	    editor.cancelCellEditing();
	  }
	  public boolean isCellEditable(EventObject anEvent) {
		  return false;
	  }
	  public void addCellEditorListener(CellEditorListener l) {
	    editor.addCellEditorListener(l);
	  }
	  public void removeCellEditorListener(CellEditorListener l) {
	    editor.removeCellEditorListener(l);
	  }
	  public boolean shouldSelectCell(EventObject anEvent) {
	    return editor.shouldSelectCell(anEvent);
	  }

}
