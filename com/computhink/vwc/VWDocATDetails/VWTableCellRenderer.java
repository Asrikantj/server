package com.computhink.vwc.VWDocATDetails;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.*;
//import javax.swing.table.TableCellRenderer;
//import javax.swing.border.*;
import java.awt.Component;
import java.awt.Color;
//import java.awt.Rectangle;
/**
 * @author Nishad Nambiar
 *
 */

public class VWTableCellRenderer extends DefaultTableCellRenderer
{
    public Component getTableCellRendererComponent(JTable table,Object value,
        boolean isSelected,boolean hasFocus,int row,int column) 
    {
        if(row%2==0)
        {
            setBackground(new Color(15658734));
            ///setBackground(Color.lightGray);
        }
        else
        {
            setBackground(Color.white);
        }
       
        /*
        if (value instanceof IconData) 
        {
          IconData ivalue = (IconData)value;
          setIcon(ivalue.m_icon);
          setText(ivalue.m_data.toString());
          if (ivalue.foreColor != null )
        	  setForeground(ivalue.foreColor);  
        }
        if (value instanceof ColorData) 
        {
        	ColorData ivalue = (ColorData)value;
        	setForeground(ivalue.getFColor());
        	setText(ivalue.toString());
        }        
*/
        
        return super.getTableCellRendererComponent(table,value,isSelected,false,row,column);
    }
/*
    public void setSelectedRow(JTable table,int row)
    {
        getTableCellRendererComponent(table,"",true,false,row,0);
    }
 */
}