/**
 * 
 */
package com.computhink.vwc.VWDocATDetails;

/**
 * @author Nishad Nambiar
 *
 */
public class VWDocumentHistoryInfo {
	public  String auditEvent="";
	public  String auditDate="";
	public  String auditUserName="";
	
	public  String auditObjectName="";
	public  String auditObjectType="";
	public  String auditClientHost="";
	public  String auditLocation="";
	public  String auditDescription="";
	/**
	 * @return Returns the auditDate.
	 */
	public  String getAuditDate() {
		return auditDate;
	}
	/**
	 * @param auditDate The auditDate to set.
	 */
	public  void setAuditDate(String auditDate) {
		this.auditDate = auditDate;
	}
	/**
	 * @return Returns the auditEvent.
	 */
	public  String getAuditEvent() {
		return auditEvent;
	}
	/**
	 * @param auditEvent The auditEvent to set.
	 */
	public  void setAuditEvent(String auditEvent) {
		this.auditEvent = auditEvent;
	}
	/**
	 * @return Returns the auditUserName.
	 */
	public  String getAuditUserName() {
		return auditUserName;
	}
	/**
	 * @param auditUserName The auditUserName to set.
	 */
	public  void setAuditUserName(String auditUserName) {
		this.auditUserName = auditUserName;
	}
	/**
	 * @return Returns the auditClientHost.
	 */
	public String getAuditClientHost() {
		return auditClientHost;
	}
	/**
	 * @param auditClientHost The auditClientHost to set.
	 */
	public void setAuditClientHost(String auditClientHost) {
		this.auditClientHost = auditClientHost;
	}
	/**
	 * @return Returns the auditDescription.
	 */
	public String getAuditDescription() {
		return auditDescription;
	}
	/**
	 * @param auditDescription The auditDescription to set.
	 */
	public void setAuditDescription(String auditDescription) {
		this.auditDescription = auditDescription;
	}
	/**
	 * @return Returns the auditLocation.
	 */
	public String getAuditLocation() {
		return auditLocation;
	}
	/**
	 * @param auditLocation The auditLocation to set.
	 */
	public void setAuditLocation(String auditLocation) {
		this.auditLocation = auditLocation;
	}
	/**
	 * @return Returns the auditObjectName.
	 */
	public String getAuditObjectName() {
		return auditObjectName;
	}
	/**
	 * @param auditObjectName The auditObjectName to set.
	 */
	public void setAuditObjectName(String auditObjectName) {
		this.auditObjectName = auditObjectName;
	}
	/**
	 * @return Returns the auditObjectType.
	 */
	public String getAuditObjectType() {
		return auditObjectType;
	}
	/**
	 * @param auditObjectType The auditObjectType to set.
	 */
	public void setAuditObjectType(String auditObjectType) {
		this.auditObjectType = auditObjectType;
	}
	
}
