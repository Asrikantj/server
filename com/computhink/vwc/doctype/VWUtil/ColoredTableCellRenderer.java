package com.computhink.vwc.doctype.VWUtil;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.*;

import java.awt.Component;
import java.awt.Color;

public class ColoredTableCellRenderer extends DefaultTableCellRenderer
{
  
  public void setValue(Object value) {
	
    if(isSelectedFlag)
    {
        setForeground(Color.white);
        super.setValue(value);
        return;
    }
    if (value instanceof ColorData) 
    {
        ColorData cvalue = (ColorData)value;
        setForeground(cvalue.getFColor());
        setIcon(null);
        setText(cvalue.getData().toString());
    }
    else if (value instanceof IconData) 
    {
        IconData ivalue = (IconData)value;
        setIcon(ivalue.m_icon);
        setText(ivalue.m_data.toString());
    }
    else{
        super.setForeground(UIManager.getColor("Table.focusCellForeground"));
        super.setValue(value);
    }
  }
//------------------------------------------------------------------------------
  public Component getTableCellRendererComponent(JTable table,Object value,
    boolean isSelected,boolean hasFocus,int row,int column) 
    {
	  		if(row%2==0)
	  			setBackground(new Color(15658734));
	  		else
	  			setBackground(Color.white);
        return super.getTableCellRendererComponent(table,value,isSelected,false,row,column);
    }
    private boolean isSelectedFlag=false;
}
