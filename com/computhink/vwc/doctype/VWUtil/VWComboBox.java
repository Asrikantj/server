package com.computhink.vwc.doctype.VWUtil;

//import java.awt.CardLayout;
//import java.awt.BorderLayout;
import java.util.LinkedList;
import java.util.List;
import javax.swing.plaf.basic.BasicComboBoxRenderer;
import  javax.swing.JList;
import java.awt.Component;

import javax.swing.JComboBox;

public class VWComboBox extends JComboBox
{
    public VWComboBox()
    {
        super();
        setRenderer(new VWComboBoxRenderer());
    }    
    public VWComboBox(Object[] items)
    {
        super(items);
        setRenderer(new VWComboBoxRenderer());
        for(int i=0;i<items.length;i++)
        {
            cmbToolTips.add(items[i].toString().trim());
        }
    }
    
    //--------------------------------------------------------------------------
    public VWComboBox(List items)
    {
        this(listTo1DArrOfString(items));
    }    
    public static String[] listTo1DArrOfString(List list)	
  {
      if(list==null || list.size()==0) return null;
      int listCount=list.size();
      String[] data=new String[listCount];
      for(int i=0;i<listCount;i++)
              data[i]=(String)list.get(i);
      return data;
    }
    public void removeItemAt(int anIndex)
    {
        super.removeItemAt(anIndex);
        cmbToolTips.remove(anIndex);
    }
    public void removeAllItems()
    {
        super.removeAllItems();
        cmbToolTips=new LinkedList();
    }
    public void insertItemAt(Object anObject,int index)
    {
        super.insertItemAt(anObject,index);
        cmbToolTips.add(index,anObject.toString().toString().trim());
    }
    public void addItem(Object anObject)
    {
        super.addItem(anObject);
        cmbToolTips.add(anObject.toString().trim());
    }
    
  class VWComboBoxRenderer extends BasicComboBoxRenderer {
    public Component getListCellRendererComponent( JList list, 
           Object value, int index, boolean isSelected, boolean cellHasFocus) {
      if (isSelected) {
        setBackground(list.getSelectionBackground());
        setForeground(list.getSelectionForeground());      
        if (-1 < index) {
          list.setToolTipText((String)cmbToolTips.get(index));
        }
      } else {
        setBackground(list.getBackground());
        setForeground(list.getForeground());
      } 
      setFont(list.getFont());
      setText((value == null) ? "" : value.toString());     
      return this;
    }  
  }
private List cmbToolTips=new LinkedList();
}
