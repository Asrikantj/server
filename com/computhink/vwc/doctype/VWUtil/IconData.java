package com.computhink.vwc.doctype.VWUtil;

import java.awt.Color;
import javax.swing.ImageIcon;

public class IconData
{
  public ImageIcon  m_icon;
  public Object m_data;
  public Color foreColor;

  public IconData(ImageIcon icon, Object data) {
    m_icon = icon;
    m_data = data;
  }
  public IconData(ImageIcon icon, Object data, Color color) {
	    m_icon = icon;
	    m_data = data;    
	    foreColor = color;
	  }
  public String toString() {
    return m_data.toString();
  }
}