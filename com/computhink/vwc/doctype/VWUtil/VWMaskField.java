package com.computhink.vwc.doctype.VWUtil;

import javax.swing.JFormattedTextField;
import javax.swing.text.MaskFormatter;

import com.computhink.vwc.VWUtil;

import java.util.Vector;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
/*
    System.out.println(NumberFormat.getCurrencyInstance().getCurrency().getSymbol());
    System.out.println(NumberFormat.getCurrencyInstance().getCurrency().getDefaultFractionDigits());
 
 */
public class VWMaskField extends JFormattedTextField {
    static {
        loadSymbols();
   }
    private static void loadSymbols() {
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        thousandsSepChar=new Character(symbols.getGroupingSeparator()).toString();
        percentageChar=new Character(symbols.getPercent()).toString();
        decimalChar=new Character(symbols.getDecimalSeparator()).toString();
        ///symbols.getMinusSign();
        ///System.out.println(symbols.getCurrencySymbol());
        currencySymbols=new Vector();
        Locale[] locals = java.util.Locale.getAvailableLocales();
        for(int i=0;i<locals.length;i++)
        {
            String curStr=new DecimalFormatSymbols(locals[i]).getCurrencySymbol();
            if(!currencySymbols.contains(curStr))
                currencySymbols.add(curStr);
        }
        currencySymbols.remove(symbols.getCurrencySymbol());
        currencySymbols.add(0,symbols.getCurrencySymbol());
        /*
        if(!currencySymbols.contains("$"))
            currencySymbols.add("$");
        if(!currencySymbols.contains("�"))
            currencySymbols.add("�");
        if(!currencySymbols.contains("�"))
            currencySymbols.add("�");
        if(!currencySymbols.contains("�"))
            currencySymbols.add("�");
        if(!currencySymbols.contains("�"))
            currencySymbols.add("�");
        if(!currencySymbols.contains("�"))
            currencySymbols.add("�");
         */
    }
    public VWMaskField()
    {  
    	setCaretPosition(0);
    }
    public VWMaskField(String mask,int type,boolean noBorder) {
        String newMask="";
        setCaretPosition(0);
        if(type==NUMBER_TYPE) {
            gOriginalMask=mask;
            gType=NUMBER_TYPE;
            maskFormatter.setValidCharacters(numberString+".-+");
            newMask=ConvertNumberMask();
        }
        else if(type==TEXT_TYPE) {
        	gType = TEXT_TYPE;
            gOriginalMask=mask;
            newMask=ConvertTextMask();
        }
        try{
            maskFormatter.setMask(newMask);
        }
        catch(java.text.ParseException e){}
        maskFormatter.install(this);
        if(noBorder)setBorder(null);
        setFocusLostBehavior(JFormattedTextField.COMMIT);
        SymFocus aSymFocus = new SymFocus();
        addFocusListener(aSymFocus);
    }
    //--------------------------------------------------------------------------
    public VWMaskField(int type,int length,boolean noBorder) {
        String mask="";
        if(type==USNUMBER_TYPE) {
            for(int i=0;i<length;i++)
                mask+="#";
            gOriginalMask=mask;
            maskFormatter.setValidCharacters(numberString);
        }
        else if(type==DATE_TYPE) {
            maskFormatter.setValidCharacters("yMd/-");
        }
        else if(type==SEQUENCE_TYPE) {
            for(int i=0;i<length;i++)
                mask+="*";
            gOriginalMask=mask;
            maskFormatter.setInvalidCharacters("~");
        }
        try{
            maskFormatter.setMask(mask);
        }
        catch(java.text.ParseException e){}
        maskFormatter.install(this);
        setFocusLostBehavior(JFormattedTextField.COMMIT);
        if(noBorder)setBorder(null);
        SymFocus aSymFocus = new SymFocus();
        addFocusListener(aSymFocus);
    }
    //--------------------------------------------------------------------------
    class SymFocus extends java.awt.event.FocusAdapter {
        public void focusLost(java.awt.event.FocusEvent event) {
            Object object = event.getSource();
            if (object instanceof VWMaskField)
                VWMask_focusLost(event);
        }
        //--------------------------------------------------------------------------
        public void focusGained(java.awt.event.FocusEvent event) {
            Object object = event.getSource();
            if (object instanceof VWMaskField)
                VWMask_focusGained(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWMask_focusLost(java.awt.event.FocusEvent event) {
        if(gMaskUsedIn==USEDINTEXT_TYPE) {
            try {
                commitEdit();
            }
            catch(java.text.ParseException e){}
            setText(getFormatedValue());
        }
    }
    //--------------------------------------------------------------------------
    void VWMask_focusGained(java.awt.event.FocusEvent event) {
        setSelectionStart(0);
        setSelectionEnd(getText().trim().length());
        // Set the cursor position to end of the text.
        if(gType == TEXT_TYPE){
        	int position = getCursorPosition(getFormatedValue());
        	setCaretPosition(position);        
        }
        else{
        setCaretPosition(getText().trim().length());
    }
    }
    //--------------------------------------------------------------------------
    /**
     * Desc   :Below method getCursorPosition() returns the cursor position, where cursor has to be set 
     */
    private int getCursorPosition(String formatedValue){
    	int position = 0;
    	boolean empty = true;    	
    	for(int count=0;count<formatedValue.length();count++){
    		char curChar = formatedValue.charAt(count);
    		int curCharInt = (int) curChar;
    		if( (curCharInt>=65 && curCharInt <=90) || (curCharInt>=97 && curCharInt <=122) || (curCharInt>=48 && curCharInt <=57) ){
    			empty = false;
    			break;
    		}
    	}
    	if(!empty) position = getText().trim().length();
    	return position; 
    }
    
    //--------------------------------------------------------------------------
    MaskFormatter maskFormatter = new MaskFormatter();
    //--------------------------------------------------------------------------
    public String getFormatedValue() {
        if(gType==NUMBER_TYPE) {
        	// Trim the trailing blank Spaces
            return getDisplayNumberValue(getText().trim());
        }
        return getText();
    }
    //--------------------------------------------------------------------------
    private String getDisplayNumberValue(String originalValue) {
        String strValue=fixNumber(originalValue.trim());
        gActValue=strValue;
        String actValue=strValue;
        int pointIndex=strValue.indexOf(decimalChar);
        String strValueWithOutPoint=strValue;
        if(pointIndex > 0) {
            strValueWithOutPoint=strValue.substring(0,pointIndex);
        }
        if(thousandsSepFlag) {
            int index=strValueWithOutPoint.length();
            actValue="";
            while(index>3) {
                actValue=thousandsSepChar+strValueWithOutPoint.substring(index-3,index) +actValue;
                index-=3;
            }
            actValue=strValueWithOutPoint.substring(0,index)+actValue;
            if(pointIndex>0) actValue=actValue+strValue.substring(pointIndex,strValue.length());
        }
        if(strValue.indexOf("-")==0) {
            if(nigativesFlag) {
                actValue="(" + actValue.substring(1) + ")";
            }
        }
        if(percentageFlag) {        	
            actValue=actValue.trim();
            actValue= actValue.equals("")?"0":actValue;
            actValue= actValue +percentageChar ;
        }
        if(!currencySymbol.equals("")) {
            actValue=actValue.trim();
            actValue=currencySymbol+(actValue.trim().equals("")?"0":actValue.trim());
        }
        if(scientificFlag) {
            actValue=fixScientific(actValue);
        }
        return actValue;
    }
    //--------------------------------------------------------------------------
    private String fixScientific(String value) {
        String retValue=value;
        int maskDigitCount=gOriginalMask.indexOf(".");
        int valueDigitCount=value.indexOf(decimalChar);
        int valuePointIndex=valueDigitCount;
        //int defDigitCount=0;
        if(maskDigitCount<=0 && valueDigitCount<=0) {
            retValue+="E+0";
        }
        else if(maskDigitCount<=0 && valueDigitCount>0) {
            retValue=value.substring(0,valuePointIndex);
            retValue+=value.substring(valuePointIndex+1,value.length());
            retValue+="E-" + (valueDigitCount);
            ///retValue=retValue.substring(0,valueDigitCount)+ "E+0";
        }
        else if(maskDigitCount>0 && valueDigitCount<=0) {
            maskDigitCount=gOriginalMask.indexOf("E") - gOriginalMask.indexOf(".")-1;
            if(maskDigitCount>value.length())
                while (maskDigitCount>value.length())
                    value="0"+value;
            retValue=value.substring(0,value.length()-maskDigitCount);
            retValue+=decimalChar + value.substring(value.length()-maskDigitCount,value.length());
            retValue+="E+" + maskDigitCount;
            if(retValue.substring(0,1).equals(decimalChar)) retValue="0"+retValue;
        }
        else if(maskDigitCount>0 && valueDigitCount>0) {
            maskDigitCount=gOriginalMask.indexOf("E") - gOriginalMask.indexOf(".")-1;
            valueDigitCount=value.length()-valueDigitCount-1;
            if(maskDigitCount>=valueDigitCount) {
                retValue=value.substring(0,valuePointIndex);
                retValue+=value.substring(valuePointIndex+1,value.length());
                value=retValue;
                if(maskDigitCount>value.length())
                    while (maskDigitCount>value.length())
                        value="0"+value;
                retValue=value.substring(0,value.length()-maskDigitCount);
                retValue+=decimalChar + value.substring(value.length()-maskDigitCount,value.length());
                retValue+="E+" + (maskDigitCount-valueDigitCount);
                if(retValue.substring(0,1).equals(decimalChar)) retValue="0"+retValue;
            }
            else {
                retValue=value.substring(0,valuePointIndex);
                retValue+=value.substring(valuePointIndex+1,value.length());
                value=retValue;
                retValue=value.substring(0,value.length()-maskDigitCount);
                retValue+=decimalChar + value.substring(value.length()-maskDigitCount,value.length());
                retValue+="E-" + (valueDigitCount-maskDigitCount);
                if(retValue.substring(0,1).equals(decimalChar)) retValue="0"+retValue;
            }
        }
        return retValue;
    }
    //--------------------------------------------------------------------------
    private String fixNumber(String value) {
        String retValue="";
        int count = value.length();
        for(int i=0;i<count;i++) {
            String chr=value.substring(i,i+1);
            
            if(chr.equals("-") && retValue.equals("")) {
                retValue+=chr;
            }
            if(chr.equals(decimalChar) && retValue.lastIndexOf(decimalChar)<0) {
                if(retValue.equals("")) retValue="0";
                retValue+=chr;
            }
            else if(numberString.indexOf(chr)>=0) {
                retValue+=chr;
            }
        }
        String tmpMask=gOriginalMask;
        if(tmpMask.startsWith("("))
            tmpMask=tmpMask.substring(1,tmpMask.length()-1);
        if(tmpMask.endsWith(")"))
            tmpMask=tmpMask.substring(0,tmpMask.length()-1);
        int maskDigitCount=tmpMask.lastIndexOf(".");
        int valueDigitCount=retValue.lastIndexOf(decimalChar);
        if(maskDigitCount>0 && valueDigitCount>0) {
            maskDigitCount=tmpMask.length()-maskDigitCount;
            valueDigitCount=retValue.length() - valueDigitCount;
            int def=maskDigitCount-valueDigitCount;
            if(def<0)   retValue=retValue.substring(0,retValue.length()+def);
        }
        else if(maskDigitCount<0 && valueDigitCount>0) {
            retValue=retValue.substring(0,valueDigitCount);
        }
        if(tmpMask.endsWith("%")) tmpMask=tmpMask.substring(0,tmpMask.length()-1);
        maskDigitCount=0;
        if(tmpMask.lastIndexOf(".")>=0)
            maskDigitCount=tmpMask.length()-tmpMask.lastIndexOf(".");
        if(retValue.lastIndexOf(decimalChar)<0) retValue+=decimalChar;
        valueDigitCount=retValue.length()- retValue.lastIndexOf(decimalChar);
        while(maskDigitCount > valueDigitCount) {
            retValue+="0";
            valueDigitCount++;
        }
        if(retValue.endsWith(decimalChar)) retValue=retValue.substring(0,retValue.length()-1);
        return retValue;
    }
    //--------------------------------------------------------------------------
    private String ConvertNumberMask() {
        if(gOriginalMask.indexOf(thousandsSepMask)>=0) {
            thousandsSepFlag=true;
        }
        if(gOriginalMask.indexOf(scientificMask)>=0) {
            scientificFlag=true;
        }
        if(gOriginalMask.indexOf(nigativesMask)>=0) {
            nigativesFlag=true;
        }
        if(gOriginalMask.indexOf(percentageMask)>=0) {
            percentageFlag=true;
        }
        return "******************************";
    }
    //--------------------------------------------------------------------------
    private String ConvertTextMask() {
        String newMask=gOriginalMask;
        if(newMask.equals("<"))
            return "LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL";
        if(newMask.equals(">"))
            return "UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU";
        newMask=newMask.replace('\\','\'');
        newMask=replaceMaskChar(newMask,"9","#",true);
        newMask=replaceMaskChar(newMask,"a","A",true);
        newMask=replaceMaskChar(newMask,"&","*",true);
        newMask=replaceMaskChar(newMask,"C","*",true);
        newMask=replaceMaskChar(newMask,"U","'U",false);
        newMask=replaceMaskChar(newMask,"L","'L",false);
        newMask=replaceMaskChar(newMask,"H","'H",false);
        newMask=fixSmallLetters(newMask);
        newMask=fixCapitalLetters(newMask);
        return newMask;
    }
    //--------------------------------------------------------------------------
    private String replaceMaskChar(String mask,String orgChar,String newChar,boolean ignoreCase) {
        String newMask="";
        int count=mask.length();
        String str="";
        for(int i=0;i<count;i++) {
            str=mask.substring(i,i+1);
            if(ignoreCase && ((i==0 && str.equalsIgnoreCase(orgChar))||
            (str.equalsIgnoreCase(orgChar) && !mask.substring(i-1,i).equals("'"))))
                newMask+=newChar;
            else if(!ignoreCase && ((i==0 && str.equals(orgChar))||
            (str.equals(orgChar) && !mask.substring(i-1,i).equals("'"))))
                newMask+=newChar;
            else
                newMask+=str;
        }
        return newMask;
    }
    //--------------------------------------------------------------------------
    private String fixSmallLetters(String mask) {
        String newMask=mask;
        String str="";
        boolean smallLetter=true;
        int index=mask.indexOf('<');
        if( index>=0)
            if(index==0 || !mask.substring(index-1,index).equals("'")) {
                newMask=mask.substring(0,index);
                for(int i=index+1;i<mask.length();i++) {
                    str=mask.substring(i,i+1);
                    if(str.equals(">")) {
                        smallLetter=false;
                    }
                    else if(str.equals("<")) {
                        smallLetter=true;
                        continue;
                    }
                    if(smallLetter && ((str.equalsIgnoreCase("A") && !mask.substring(i-1,i).equals("'"))
                    || (str.equalsIgnoreCase("?")&& !mask.substring(i-1,i).equals("'"))))
                        newMask+="L";
                    else
                        newMask+=mask.substring(i,i+1);
                }
            }
        return newMask;
    }
    //--------------------------------------------------------------------------
    private String fixCapitalLetters(String mask) {
        String newMask=mask;
        String str="";
        boolean capitalLetter=true;
        int index=mask.indexOf('>');
        if( index>=0)
            if(index==0 || !mask.substring(index-1,index).equals("'")) {
                newMask=mask.substring(0,index);
                for(int i=index+1;i<mask.length();i++) {
                    str=mask.substring(i,i+1);
                    if(str.equals("<")) {
                        capitalLetter=false;
                    }
                    else if(str.equals(">")) {
                        capitalLetter=true;
                        continue;
                    }
                    if(capitalLetter && ((str.equalsIgnoreCase("A") && !mask.substring(i-1,i).equals("'"))
                    || (str.equalsIgnoreCase("?")&& !mask.substring(i-1,i).equals("'"))))
                        newMask+="U";
                    else
                        newMask+=mask.substring(i,i+1);
                }
            }
        return newMask;
    }
    //--------------------------------------------------------------------------
    public static String getActualValue(Object value) {
    	//System.out.println("getActualValue " + value.toString());
        String displayValue=value.toString();
        if(displayValue.indexOf(",")>0) {
            displayValue=VWUtil.removeCharFromString(displayValue,",");
        }
        if(displayValue.indexOf(nigativesMask)==0) {
            displayValue=VWUtil.removeCharFromString(displayValue,")");
            displayValue=VWUtil.removeCharFromString(displayValue,"(");
            displayValue="-"+displayValue;
        }
        if(displayValue.indexOf(percentageMask)>=0) {
            displayValue=VWUtil.removeCharFromString(displayValue,percentageChar);
        }
        for(int i=0;i<currencySymbols.size();i++) {
            if(displayValue.indexOf((String)currencySymbols.get(i)) >= 0) {
                displayValue=VWUtil.removeCharFromString(displayValue,(String)currencySymbols.get(i));
                break;
            }
        }
        if(displayValue.indexOf("e+")>0) {
            int pointIndex=displayValue.indexOf(decimalChar);
        }
        while(displayValue.endsWith("0"))
            displayValue=displayValue.substring(0,displayValue.length()-1);
        if(displayValue.endsWith(decimalChar))
            displayValue=displayValue.substring(0,displayValue.length()-1);
        return displayValue.trim();
    }
    //--------------------------------------------------------------------------
    public void setUsedIn(int usedIn) {
        gMaskUsedIn=usedIn;
    }
    //--------------------------------------------------------------------------
    public static Vector getCurrencySymbols() {
        return currencySymbols;
    }
    //--------------------------------------------------------------------------
    public void setCurrencySymbol(String symbol) {
        currencySymbol=symbol;
    }
    public static String retainCurrency(String currency) {
  	  if (currency.startsWith("("))
  		  currency = currency.substring(1, currency.length());
  	  if (currency.endsWith(")"))
  		  currency = currency.substring(0, currency.length() - 1);
  	  currency = currency.replace("0.0", "");
  	  currency = currency.replace("0", "");
  	  currency = currency.replaceAll("#,##", "");
  	  return currency;
    }
    //--------------------------------------------------------------------------
    public static final int USNUMBER_TYPE=0;
    public static final int NUMBER_TYPE=1;
    public static final int DATE_TYPE=2;
    public static final int TEXT_TYPE=3;
    public static final int SEQUENCE_TYPE=4;
    public static final int USEDINGRID_TYPE=0;
    public static final int USEDINTEXT_TYPE=1;
    private static final String thousandsSepMask="#,##";
    private static final String scientificMask="E+00";
    private static final String nigativesMask="(";
    private static final String percentageMask="%";
    private static String percentageChar="%";
    private static final String numberString="0123456789";
    private boolean thousandsSepFlag=false;
    private static String thousandsSepChar=",";
    private static String decimalChar=".";
    private boolean scientificFlag=false;
    private boolean nigativesFlag=false;
    private boolean percentageFlag=false;
    private String currencySymbol="";
    private String gOriginalMask="";
    private String gOriginalValue="";
    private String gActValue="";
    private int gType=-1;
    private int gMaskUsedIn=USEDINGRID_TYPE;
    private static Vector currencySymbols;
}