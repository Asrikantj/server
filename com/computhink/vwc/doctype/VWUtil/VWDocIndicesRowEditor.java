package com.computhink.vwc.doctype.VWUtil;
import java.util.*;
import java.awt.*;
import javax.swing.*;
import javax.swing.table.*;
import java.awt.event.MouseEvent;
import javax.swing.event.CellEditorListener;

import com.computhink.vwc.VWCConstants;
import com.computhink.vwc.VWUtil;
//import com.computhink.vwc.doctype.VWUtil.VWComboBox;
//import com.computhink.vwc.doctype.VWUtil.VWDateComboBox;

import com.computhink.common.Index;
import com.computhink.common.util.VWComboBox;
import com.computhink.common.util.VWDateComboBox;
import com.computhink.vwc.doctype.VWDocIndicesTable;

import com.computhink.vwc.doctype.VWUtil.VWMaskField;
/**
  * each row TableCellEditor
  *
  * @version 1.0 10/20/98
  * @author <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
  */

public class VWDocIndicesRowEditor implements TableCellEditor,VWCConstants{
  
  protected TableCellEditor editor, defaultEditor;
  protected VWDocIndicesTable gTable=null;
  private VWMaskField gMaskEdit=null;
  
  /**
   * Constructs a EachRowEditor.
   * create default editor 
   *
   * @see TableCellEditor
   * @see DefaultCellEditor
   */
  public VWDocIndicesRowEditor(VWDocIndicesTable table) {
  JTextField text=new JTextField();
    text.setBorder(null);
    defaultEditor = new DefaultCellEditor(text);
    gTable=table;
  }
  
  /**
   * @param row    table row
   * @param editor table cell editor
   */
  
  public Component getTableCellEditorComponent(JTable table,
    Object value,boolean isSelected,int row,int column) {
        gMaskEdit=null;
        String displayValue=value.toString().trim();
        int indexId = gTable.getRowIndexId(row);
        Index rowIndex=getIndex(row);
        if(rowIndex.getType()==Index.IDX_TEXT) //text type
        {
            editor=defaultEditor;
            String mask=rowIndex.getMask();   
            
            if(!mask.trim().equals(""))
                editor= new DefaultCellEditor(new VWMaskField(mask,VWMaskField.TEXT_TYPE,true));// defaultVal field
            else{
            value = value.toString().trim();
            }
        }else if(rowIndex.getType()==Index.IDX_BOOLEAN) //Boolean type
        {
            editor=new DefaultCellEditor(new VWComboBox(YesNoData)); // Default value field
        }else if(rowIndex.getType()==Index.IDX_DATE) {
            VWDateComboBox dateCombo=new VWDateComboBox();
            dateCombo.setDateFormat(rowIndex.getMask());
            editor=new DefaultCellEditor(dateCombo);// defaultVal field
        }else if(rowIndex.getType()==Index.IDX_SELECTION) {
        	Vector arrValues = rowIndex.getDef();
        	//Blank entry was missing in selection list - added 7 Dec 2007	
            VWComboBox vwComboBox = new VWComboBox(arrValues);
            if(!rowIndex.getActDef().trim().equalsIgnoreCase("") && !rowIndex.getDefaultValue().trim().equalsIgnoreCase("")){
            	int selectedItem  = 1;
            	Vector arrValuesNew = new Vector();
            	String temp = new String();
            	arrValuesNew.add(0, temp);
            	arrValuesNew.addAll(arrValues);
            	for(int i = 0; i<arrValuesNew.size(); i++){
            		if(!displayValue.trim().equals("")){
            			if(displayValue.trim().equals((String)arrValuesNew.get(i)))
            				selectedItem = i;
            		}
            	}
            	vwComboBox = new VWComboBox(arrValuesNew);
            	vwComboBox.setSelectedIndex(selectedItem);
            	arrValuesNew = null;
            }
            editor=new DefaultCellEditor(vwComboBox);          
        }else if(rowIndex.getType()==Index.IDX_NUMBER) {
            String info=rowIndex.getInfo();
            String subType="";
            int valueType=0;
            if(!info.equals("") && !info.equals("."))
            {
            try{
                subType=info.substring(2,3);
                valueType=VWUtil.to_Number(info.substring(3,4));
            }
            catch(Exception e){
                subType="";
                valueType=0;
            };
            }            
            if(subType.equals("0")) {
            gMaskEdit=new VWMaskField(rowIndex.getMask(),VWMaskField.NUMBER_TYPE,true);
            editor= new DefaultCellEditor(gMaskEdit);// Default Value
            displayValue=gMaskEdit.getActualValue(value);            
            gMaskEdit.setText(displayValue);
            }else if(subType.equals("1")) {
                gMaskEdit=new VWMaskField(rowIndex.getMask(),VWMaskField.NUMBER_TYPE,true);
                editor= new DefaultCellEditor(gMaskEdit);// Default Value
                displayValue=gMaskEdit.getActualValue(value);
                gMaskEdit.setText(displayValue);
            }else if(subType.equals("2")) {
            	//currency symbol with dot are Sfr.	Kr.	py6.	rbB.	piB. - giving problem in decimal places and Cur.symbol is not coming 
            	//Issue No:VW 6.10378 and VW 6.10389 fixed
/*            	if(rowIndex.getMask().indexOf(".")!=-1){
            		int dotIndex = rowIndex.getMask().indexOf(".");
            		String mask = rowIndex.getMask().substring(dotIndex+1);
            		if(mask.indexOf(".")!=-1){
            	      gMaskEdit=new VWMaskField(mask,VWMaskField.NUMBER_TYPE,true);
            		}else
            			gMaskEdit=new VWMaskField(rowIndex.getMask(),VWMaskField.NUMBER_TYPE,true);
            	}else*/
            		gMaskEdit=new VWMaskField(rowIndex.getMask(),VWMaskField.NUMBER_TYPE,true);
/*                if(rowIndex.getMask()!= null && !rowIndex.getMask().equals("")){
                	if(rowIndex.getMask().indexOf("0")!=-1){
                		String currency = "";
                		try{
	                		int position = rowIndex.getMask().indexOf("#") == -1 ? rowIndex.getMask().indexOf("0"):rowIndex.getMask().indexOf("#"); 
		                	currency = rowIndex.getMask().substring(0,position);
		                	if (currency != null && currency.indexOf("(") == 0){
		                		currency = currency.substring(1,currency.length());	                		
		                	}
                		}catch(Exception ex){}
	                    gMaskEdit.setCurrencySymbol(currency);}
                }*/
                String currency = VWMaskField.retainCurrency(rowIndex.getMask());
                gMaskEdit.setCurrencySymbol(currency);
                editor= new DefaultCellEditor(gMaskEdit);// Default Value
                displayValue=gMaskEdit.getActualValue(value);
                gMaskEdit.setText(displayValue);
            }else if(subType.equals("3")) {
                gMaskEdit=new VWMaskField(rowIndex.getMask(),VWMaskField.NUMBER_TYPE,true);
                editor= new DefaultCellEditor(gMaskEdit);// Default Value
                displayValue=gMaskEdit.getActualValue(value);
                gMaskEdit.setText(displayValue);
            }else if(subType.equals("4")) {
                gMaskEdit=new VWMaskField(rowIndex.getMask(),VWMaskField.NUMBER_TYPE,true);
                editor= new DefaultCellEditor(gMaskEdit);// Default Value
                displayValue=gMaskEdit.getActualValue(value);
                gMaskEdit.setText(displayValue);
            }
        } else if(rowIndex.getType()==Index.IDX_SEQUENCE){           
            int count=VWUtil.getCharCount(rowIndex.getMask(),"~");
            gMaskEdit=new VWMaskField(VWMaskField.SEQUENCE_TYPE,count,true);
            editor= new DefaultCellEditor(gMaskEdit);
            displayValue=gMaskEdit.getActualValue(value);
            gMaskEdit.setText(displayValue.trim());                        
        }        
        return editor.getTableCellEditorComponent(table,value,isSelected,row,column);
    }
   //-----------------------------------------------------------
    public Object getCellEditorValue(){
        if(gMaskEdit !=null) {
            try {
                gMaskEdit.commitEdit();
            }
            catch(java.text.ParseException e){}
            String retValue=gMaskEdit.getFormatedValue();
            return retValue;
        }
        return editor.getCellEditorValue();
    }
    public boolean stopCellEditing() {
        return editor.stopCellEditing();
    }
    public void cancelCellEditing() {
        editor.cancelCellEditing();
    }
    public boolean isCellEditable(EventObject anEvent){      
        int column=0, row=0, indexId=0;
        if (anEvent instanceof MouseEvent) {
            Point point=((MouseEvent)anEvent).getPoint();
            column = gTable.columnAtPoint(point);
            row = gTable.rowAtPoint(point);
            if (column == 0) return false;
        }
        
        return true;

        //if(column==0) return false;
        /*Index rowIndex=getIndex(row);
        //if(rowIndex==null || rowIndex.isReadOnly()) return false;
        //if(rowIndex.getType()==Index.IDX_SEQUENCE) return false;*/
        //return true;	
    }
    public void addCellEditorListener(CellEditorListener l) {
        editor.addCellEditorListener(l);
    }
    public void removeCellEditorListener(CellEditorListener l) {
        editor.removeCellEditorListener(l);
    }
    public boolean shouldSelectCell(EventObject anEvent) {
        return editor.shouldSelectCell(anEvent);
    }
    private Index getIndex(int row)
    {
        return gTable.getRowData(row);
    }
}