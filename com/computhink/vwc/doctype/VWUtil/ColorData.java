package com.computhink.vwc.doctype.VWUtil;
import java.awt.Color;
public class ColorData
{
  private Color  m_fColor;
  private Color  m_bColor;
  private Object m_data;
  public static Color GREEN = new Color(0, 128, 0);
  public static Color RED = Color.red;
  public static Color BLACK = Color.black;
  public static Color BLUE = new java.awt.Color(0,128,192);
  public static Color WHITE  = Color.white;
  public static Color GRAY  = Color.gray;

//------------------------------------------------------------------------------
  public ColorData(Object data,Color color) {
    m_fColor = color;
    m_data  = data;
  }  
//------------------------------------------------------------------------------
  public void setFColor(Color color) {
    m_fColor = color;
  }  
//------------------------------------------------------------------------------
  public Color getFColor() {
    return m_fColor;
  }  
//------------------------------------------------------------------------------
  public void setBColor(Color color) {
    m_bColor = color;
  }  
//------------------------------------------------------------------------------
  public Color getBColor() {
    return m_bColor;
  }  
//------------------------------------------------------------------------------
  public void setData(Object data) {
    m_data = data;
  }  
//------------------------------------------------------------------------------
  public Object getData() {
    return m_data;
  }  
//------------------------------------------------------------------------------
  public String toString() {
    return m_data.toString();
  }
}
