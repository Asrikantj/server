package com.computhink.vwc.doctype;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.Preferences;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.border.EtchedBorder;

import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWCConstants;
import com.computhink.vwc.VWClient;
//import com.computhink.vwc.doctype.VWUtil.VWComboBox;
import com.computhink.common.Index;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.common.NodeIndexRules;
import com.computhink.common.Util;


import javax.swing.JDialog;
//import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;

public class VWRulesDlgSettings implements VWCConstants, Runnable {
	int nTop, nLeft ,nHeight, nWidth, nCtrlDiff, nLblDiff;
	public VWClient vwc;
	private static JDialog dlgMain;
	private int nodeId;
	private String nodeName = "";
	public static int selectedDocTypeId = -1;
	public static int prevRoomId = 0;
	public static int prevNodeId = 0;
	public static Vector currentIndices = new Vector();
	private WindowAdapter secAdapter;
	private static ResourceManager connectorManager = ResourceManager.getDefaultManager();
	public VWRulesDlgSettings (VWClient vwc, int roomId) {
	        if (active)
	        {
	        	if(roomId == prevRoomId){
	        		dlgMain.requestFocus();
	        		dlgMain.toFront();
	        		return;
	        	}
	        	else{
	        		closeDialog();
	        	}
	        }
	        this.vwc = vwc;
	        this.roomId=roomId;
	        prevRoomId = roomId;
	        setupUI();
	        loadTabData(roomId);
	        active = true;
	        dlgMain.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}
	
    public VWRulesDlgSettings (VWClient vwc, int roomId, int nodeId, String nodeName) {
    	//super(parent,"Rules Settings",true);
    	this.nodeId = nodeId;
    	this.nodeName = nodeName;
        if (active)
        {
        	if(roomId == prevRoomId && nodeId == prevNodeId){
        		dlgMain.requestFocus();
        		dlgMain.toFront();
        		return;
        	}
        	else{
        		closeDialog();
        	}
        }
    	this.vwc = vwc;
        this.roomId=roomId;
        prevRoomId = roomId;
        prevNodeId = nodeId;
        setupUI();
        loadTabData(roomId);
        active = true;        
    }
    public VWRulesDlgSettings () {        
        setupUI();
        loadTabData(roomId);
    }
    public void run()
    {
        show();
    }
    public void show()
    {
        if (vwc == null) return;
        /*
       	 * new frame.show(); method is replaced with new frame.setVisible(true); 
       	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
       	 */
        dlgMain.setVisible(true);
    }
    private void closeDialog() 
    {
        active = false;
        dlgMain.getOwner().dispose();
    }
    //--------------------------------------------------------------------------
    public void setupUI() {
         ///if(UILoaded) return;
    	dlgMain = new JDialog();
    	Table.getParent().setBackground(java.awt.Color.white);
    	dlgMain.setTitle(connectorManager.getString("rule.defaultproperty")+" "+ nodeName);        
    	dlgMain.setSize(442, 425);
    	dlgMain.setLayout(new BorderLayout());
        VWNewDocPanel.setLayout(null);
        dlgMain.add(VWNewDocPanel,BorderLayout.CENTER);
        VWNewDocPanel.setBounds(0,0,500,400);
        PanelCommand.setLayout(null);
        //VWNewDocPanel.add(PanelCommand);
        //PanelCommand.setBackground(java.awt.Color.white);
        PanelCommand.setBounds(0,0,168,432);
       
 //       nTop = 10; nLeft = -6; nWidth = 70; nHeight = 22;
        nTop = 10; nLeft = 10; nWidth = 68; nHeight = 22;
        

        VWNewDocPanel.add(JLblDocumentType);
        //JLblDocumentType.setBackground(java.awt.Color.white);
        JLblDocumentType.setText(connectorManager.getString("rule.doctype"));
        JLblDocumentType.setBounds(nLeft, nTop, 100, 22);
        TxtDocTypeName.setVisible(false);
        
        VWNewDocPanel.add(CmbDocType);
        CmbDocType.setBorder(etchedBorder);
        nLeft = nLeft + 90 + 4;   //212    
        CmbDocType.setBounds(nLeft, nTop, 320, nHeight); 
        TxtDocTypeName.setVisible(false);
        
        VWNewDocPanel.add(TxtDocTypeName);
        TxtDocTypeName.setBackground(java.awt.Color.white);
        TxtDocTypeName.setBorder(etchedBorder);
        TxtDocTypeName.setBounds(12,160,144,22);

        PanelTable.setBounds(11, 48, 414, 292);
        VWNewDocPanel.add(PanelTable);
        PanelTable.setLayout(new BorderLayout());
        //PanelTable.add(JLabel1,BorderLayout.NORTH);
        PanelTable.setBackground(java.awt.Color.white);
        Table.setBackground(java.awt.Color.white);
        PanelTable.add(SPIndicesTable,BorderLayout.CENTER);
        
        VWNewDocPanel.add(BtnReset);
        BtnReset.setText(connectorManager.getString("general.reset"));
        BtnReset.setActionCommand("Reset");        
        nWidth = 70; nHeight = 22;
        nTop = nTop + 350 ;
        nLeft = 103+10 + nWidth+10;        
        BtnReset.setBounds(nLeft-183, 360, 70, 22);
        

        VWNewDocPanel.add(BtnRefresh);
        BtnRefresh.setText(connectorManager.getString("general.refresh"));
        BtnRefresh.setActionCommand("Refresh");

        BtnRefresh.setBounds(nLeft-10, nTop, nWidth+10, nHeight);

        
        VWNewDocPanel.add(BtnSave);
        BtnSave.setText(connectorManager.getString("general.save"));
        //BtnSave.setBackground(java.awt.Color.white); 
        nLeft = nLeft +nWidth + 10;
        BtnSave.setBounds(nLeft, nTop, nWidth, nHeight);
        //BtnSave.setIcon(VWImages.SaveIcon);
               
        BtnCancel.setText(connectorManager.getString("general.cancel"));
        BtnCancel.setActionCommand(BTN_CANCEL_NAME);
        //BtnCancel.setIcon(VWImages.CancelIcon);
        VWNewDocPanel.add(BtnCancel);
        //BtnCancel.setBackground(java.awt.Color.white);
        nLeft = nLeft + nWidth+10;
        BtnCancel.setBounds(nLeft, nTop, nWidth, nHeight);

        
        //SymComponent aSymComponent = new SymComponent();
        //dlgMain.addComponentListener(aSymComponent);
        //Pic.setIcon(VWImages.NewDocImage);
        SymAction lSymAction = new SymAction();
        BtnSave.addActionListener(lSymAction);
        BtnReset.addActionListener(lSymAction);
        BtnCancel.addActionListener(lSymAction);
        BtnRefresh.addActionListener(lSymAction);
        CmbDocType.addActionListener(lSymAction);
        Table.setBackground(java.awt.Color.white);
        SPIndicesTable.setBackground(java.awt.Color.white);
        
        dlgMain.setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        dlgMain.setModal(true);
        dlgMain.setResizable(false);
        UILoaded=true;
        getDlgOptions();
        try{
        	CmbDocType.setSelectedIndex(0);
        }catch(Exception ex){
        	
        }
        //loadDocTypeData();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        dlgMain.setLocation((screenSize.width-450)/2,(screenSize.height-450)/2);
        dlgMain.setResizable(false);
        secAdapter = new WindowAdapter(){
            public void windowClosing(WindowEvent evt){
                closeDialog();
            }
            public void windowLostFocus(WindowEvent e){
            	if(windowActive){
            		dlgMain.requestFocus();
            		dlgMain.toFront();
            	}
            }
            public void windowDeactivated(WindowEvent e) {
            	if(windowActive){
            		dlgMain.requestFocus();
            		dlgMain.toFront();
            	}
            }
            public void windowStateChanged(WindowEvent e) {
                dlgMain.toFront();
            }
             public void windowDeiconified(WindowEvent e) {
                dlgMain.toFront();
            }
        };
        dlgMain.addWindowListener(secAdapter);  
    }
    class SymAction implements java.awt.event.ActionListener {
        public void actionPerformed(java.awt.event.ActionEvent event) {
            Object object = event.getSource();
            if (object == BtnSave)
                BtnSave_actionPerformed(event);
            else if (object == BtnReset)
                BtnReset_actionPerformed(event);
            else if (object == BtnCancel)
                BtnCancel_actionPerformed(event);
            else if (object == BtnRefresh)
            	BtnRefresh_actionPerformed(event);
            else if (object == CmbDocType)
                CmbDocType_actionPerformed(event);
        }
    }
    //--------------------------------------------------------------------------
    void BtnReset_actionPerformed(java.awt.event.ActionEvent event) {
    	
    	int answer=-2;
    	windowActive = false;
    	answer = JOptionPane.showConfirmDialog(dlgMain, connectorManager.getString("rule.jmsg1"),connectorManager.getString("rule.title1"),
                JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
    	windowActive = true;
    	if(answer!=-2){
    		dlgMain.requestFocus();
    		dlgMain.toFront();
    	}
    	if(answer == JOptionPane.YES_OPTION){
        	vwc.deleteNodeRules(roomId, nodeId);
        	Table.clearData();
        	loadDocTypes();
            loadDocTypeData();
    	}
    }
    //--------------------------------------------------------------------------
    void BtnSave_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        //Saving the Rules to NodeIndexRules Object
        //CmbDocType.getSelectedItem()
        DocType selectedDocType = (DocType) CmbDocType.getSelectedItem();
        NodeIndexRules rulesObject = new NodeIndexRules();
        rulesObject.setNodeId(nodeId);
        //System.out.println("Node Id " + nodeId);
        rulesObject.setDocTypeId(selectedDocType.getId());
        //System.out.println("DocType Id " + rulesObject.getDocTypeId());
        int countIndex = 0;
        ArrayList indicesRulesList = new ArrayList();
        for(int count = 0; count < Table.getRowCount(); count++){
        	Index idx = Table.getRowData(count);
        	boolean modified = true;
        	/* Node Properties enhancement. - Inherent index rules from parent is implemented
        	*  So here only modified values sent to procedure.  19 Feb 2008.
        	*/
    		if (actualIndexRulesList.containsKey(idx.getId())){
    			String actualIndex =  actualIndexRulesList.get(idx.getId()).toString();
    			if((actualIndex!=null && actualIndex.trim().length() > 0) && (idx!=null && idx.getValue().length() == 0) ){
    				countIndex++;
            		indicesRulesList.add(idx.getId()+ Util.SepChar + "VWBlank:");
    			}
    			if (actualIndex.trim().equals(idx.getValue().trim())) modified = false;
    		}  		
        	if(idx.getValue().trim().length() > 0){
        		if (modified){
        			countIndex++;
        			indicesRulesList.add(idx.getId()+ Util.SepChar + ((idx.getValue().trim().length() == 0)?((idx.getDefaultValue().trim().length() > 0)?idx.getDefaultValue():""):idx.getValue()));
        		}
        		//System.out.println("indicesRulesList Id: " +idx.getId()+ Util.SepChar + "::" +  idx.getValue() + ":" + idx.getDefaultValue());
        	}
        	idx = null;
        }
        rulesObject.setIndicesRulesList(indicesRulesList);
        rulesObject.setIndicesCount(countIndex);
        /**
         * CV10 Issue fix Validate node properties alert message.
         */
        Vector result=new Vector();
        String totalListValues="";
        String finalValues="";
        for(int i=0;i<indicesRulesList.size();i++){
        	totalListValues=totalListValues.concat(Util.SepChar+indicesRulesList.get(i).toString());
        }
        finalValues=countIndex+totalListValues;
        vwc.validateDynamicIndex(roomId,finalValues,result);
        if(result!=null&&result.size()>0){
        	if(!(result.get(0).toString().equals("1"))){
        		Util.Msg(dlgMain, result.get(0).toString(),connectorManager.getString("DLGTITLE.NodeProperties"));
        		return;
        	}
        }//End of validate node properties
        if(inherit){
        	int answer=-2;
        	windowActive = false;
        	answer = JOptionPane.showConfirmDialog(dlgMain,connectorManager.getString("rule.jmsg2"),connectorManager.getString("rule.title1"),
                    JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
        	windowActive = true;
        	if(answer!=-2){
        		dlgMain.requestFocus();
        		dlgMain.toFront();
        	}
        	if(answer == JOptionPane.YES_OPTION){
            	vwc.updateNodeIndexRules(roomId, rulesObject);
        	}
        }else{
        	vwc.updateNodeIndexRules(roomId, rulesObject);
        }
        actualIndexRulesList = null;
        //System.out.println("Update the Index Rules");
        saveDlgOptions();
        cancel=false;
        active = false;
        dlgMain.setVisible(false);
    }
    //--------------------------------------------------------------------------
    void BtnRefresh_actionPerformed(java.awt.event.ActionEvent event) {
        stopGridEdit();
        loadDocTypeData();
    }
    //--------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
        cancel=true;
        active = false;
        saveDlgOptions();
        closeDialog();
    }
    //--------------------------------------------------------------------------
    void CmbDocType_actionPerformed(java.awt.event.ActionEvent event) {
        //if(!docTypeLoaded) return;
    	
        try{
            dlgMain.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.WAIT_CURSOR));
            
            //loadDocTypeData();
            Vector indicesList = new Vector();
            DocType selDocType=(DocType) CmbDocType.getSelectedItem();
            //System.out.println("roomid  " + roomId);
            int ret=vwc.getDocTypeIndices(roomId, selDocType);
            //System.out.println("ret  " + ret);
        	selectedDocTypeId = selDocType.getId();
        	//System.out.println("docType id  " + selectedDocTypeId);
        	//System.out.println("Indices size " + selDocType.getIndices().size());
        	currentIndices = new Vector();
        	for(int i =0;i < selDocType.getIndices().size(); i++){
        		currentIndices.add(((Index)selDocType.getIndices().get(i)).getId());
        	}
        	//System.out.println("currentIndices " + currentIndices);
        	dlgMain.setCursor(java.awt.Cursor.getPredefinedCursor(java.awt.Cursor.DEFAULT_CURSOR));        	
        	Table.m_data.sortNodeProperties();        	
        	Table.m_data.fireTableDataChanged();
        }
        catch(Exception e){
        	//System.out.println("Ex in " + e.getMessage());
        }
        ///finally{AdminWise.adminPanel.setDefaultPointer();}
    }
    //--------------------------------------------------------------------------
    public Document getDocument()
    {
    	
        Document doc = new Document(0);
        doc.setDocType((DocType) CmbDocType.getSelectedItem());
        doc.getDocType().setIndices(Table.getData());
        for(int i=0;i<Table.getRowCount();i++)
        {
            Index index = (Index)Table.getRowData(i);
            if(index.isKey())
            {
                doc.setName(index.getValue());
                break;
            }
        }
        return doc;
    }
    //--------------------------------------------------------------------------
    javax.swing.JPanel VWNewDocPanel = new javax.swing.JPanel();
    javax.swing.JPanel PanelCommand = new javax.swing.JPanel();
    javax.swing.JPanel PanelTable = new javax.swing.JPanel();
    javax.swing.JLabel Pic = new javax.swing.JLabel();
    JButton BtnSave = new JButton();
    JButton BtnReset = new JButton();
    JButton BtnCancel = new JButton();
    JButton BtnRefresh = new JButton();
    public  JComboBox CmbDocType = new JComboBox();
    public VWDocIndicesTable Table = new VWDocIndicesTable();
    javax.swing.JScrollPane SPIndicesTable = new javax.swing.JScrollPane(Table);
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.JLabel JLblDocumentType = new javax.swing.JLabel();
    javax.swing.JTextField TxtDocTypeName = new javax.swing.JTextField();
    EtchedBorder etchedBorder = new EtchedBorder
    (EtchedBorder.LOWERED,java.awt.Color.white,java.awt.Color.darkGray);
    //javax.swing.JCheckBox ChkEnableSIC = new javax.swing.JCheckBox();
    //}}
    //--------------------------------------------------------------------------
   /* class SymComponent extends java.awt.event.ComponentAdapter {
        public void componentResized(java.awt.event.ComponentEvent event) {
            Object object = event.getSource();
            if (object instanceof VWRulesDlgSettings)
                VWNewDocPanel_componentResized(event);
        }
    }
    //--------------------------------------------------------------------------
    void VWNewDocPanel_componentResized(java.awt.event.ComponentEvent event) {
        if(event.getSource() instanceof VWRulesDlgSettings) {
            resizePanel();
        }
    }
    private void resizePanel()
    {
        Dimension mainDimension=dlgMain.getSize();
        Dimension PanelDimension=mainDimension;
        Dimension commandDimension=PanelCommand.getSize();
        commandDimension.height=mainDimension.height;
        PanelDimension.width=mainDimension.width-commandDimension.width;
        PanelDimension.height=mainDimension.height;
        PanelCommand.setSize(commandDimension);
        PanelTable.setSize(PanelDimension);
        dlgMain.doLayout();
    }*/
    //--------------------------------------------------------------------------
    public void loadTabData(int roomId) {
        Table.clearData();
        loadDocTypes();
        loadDocTypeData();
    }
    //--------------------------------------------------------------------------
    public void unloadTabData() {
        stopGridEdit();
        Table.clearData();
        CmbDocType.removeAllItems();
    }
    void loadDefaultDocTypeData(){
    	Vector indices = new Vector();
    	Index idx = null;
    	idx = new Index("233	Address Line	1	0	data	0	1	0000	Address Line	1" , 0);
    	indices.add(idx);
    	idx = new Index("233	Address Line	1	0	data1	0	1	0000	Address Line	1" , 0);
    	indices.add(idx);
    	idx = new Index("233	Address Line	1	0	data2	0	1	0000	Address Line	1" , 0);
    	indices.add(idx);
    	idx = new Index("233	Address Line	1	0	data3	0	1	0000	Address Line	1" , 0);
    	indices.add(idx);
    	idx = new Index("233	Address Line	1	0	data4	0	1	0000	Address Line	1" , 0);
    	indices.add(idx);
    	Table.addData(indices);
        if(Table.getRowCount()>0) {
            Table.setRowSelectionInterval(0,0);
            setMode(true);
        }  

    }
    //--------------------------------------------------------------------------
    private void loadDocTypeData() {
        stopGridEdit();        
        
        Vector result = new Vector(); 
        vwc.checkNodeIndexRules(roomId, nodeId, result);
    	//vwc.getNodeIndexRules(roomId, nodeId, result);
        int rulesExistNodeId = 0;
        int defaultDocTypeId = 0;
    	if (result.size() > 0){
    		StringTokenizer tokens = new StringTokenizer(result.get(0).toString(), Util.SepChar);
    		rulesExistNodeId = Util.to_Number(tokens.nextToken());
    		defaultDocTypeId = Util.to_Number(tokens.nextToken());
    		
    		//System.out.println("Data --- " + rulesExistNodeId + " : " + defaultDocTypeId);    		
    		//indexRules = new NodeIndexRules(result.get(0).toString());
    	//indexRules.updateIndicesList();
    	}
        Vector indicesList = new Vector();
        //NodeIndexRules indexRules = new NodeIndexRules();
        // Pass the Current Node id for get the indices with inherited values
        int ret=vwc.getAllDocTypeIndices(roomId, nodeId, indicesList);
        if (nodeId != rulesExistNodeId)
        	inherit = true;
        else
        	inherit = false;
        Table.addData(indicesList); 
        for (int count = 0; count < CmbDocType.getItemCount(); count++){
        	if (((DocType)CmbDocType.getItemAt(count)).getId() == defaultDocTypeId){
        			CmbDocType.setSelectedIndex(count);
        			break;
        	}        	
        }
        //CmbDocType.setSelectedItem(new DocType(indexRules.getDocTypeId()));        
        if(Table.getRowCount()>0) {
            Table.setRowSelectionInterval(0,0);
            setMode(true);
        }    	
    }
    //--------------------------------------------------------------------------
    private void loadDocTypes() {
        docTypeLoaded=false;
        CmbDocType.removeAllItems();
        CmbDocType.addItem(new DocType(-1,""));
        Vector docTypes=new Vector();
        int ret=vwc.getDocTypes(roomId,docTypes);       
        if(ret<0)
        {
            //VWMessage.showMessage(VWWeb.gConnector.ViewWiseClient.getErrorDescription(ret));
        }
        if (docTypes!=null)
            for (int i=0;i<docTypes.size();i++){
                CmbDocType.addItem((DocType)docTypes.get(i));
            }
        
   //     CmbDocType.addItem(new DocType(-2,LBL_REFRESH_NAME));
        docTypeLoaded=true;
    }
    //--------------------------------------------------------------------------
    public void clearDocTypeDetails() {
        stopGridEdit();
        Table.clearData();
    }
    //--------------------------------------------------------------------------
    public int getSelDTId() {
        DocType selDocType=(DocType) CmbDocType.getSelectedItem();
        if(selDocType==null) return 0;
        return selDocType.getId();
    }    
    //--------------------------------------------------------------------------
    private void stopGridEdit() {
        try{
            //Table.getCellEditor().stopCellEditing();
        }
        catch(Exception e){};
    }
    //--------------------------------------------------------------------------
    private void setMode(boolean enabled)
    {
        BtnSave.setEnabled(enabled);
        BtnReset.setEnabled(enabled);
    }
    //--------------------------------------------------------------------------

    private void saveDlgOptions() {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        prefs.putInt("x", dlgMain.getX());
        prefs.putInt("y", dlgMain.getY());
    }
    //--------------------------------------------------------------------------
    private void getDlgOptions() {
/*        Preferences prefs = Preferences.userNodeForPackage(getClass());
        Dimension d =VWUtil.getScreenSize();
        int x=prefs.getInt("x",d.width/4);
        int y=prefs.getInt("y",d.height/3);
        dlgMain.setLocation(x,y);
*/    }    
    //--------------------------------------------------------------------------
    private static int curMode=-1;
    private boolean docTypeLoaded=false;
    private boolean UILoaded=false;
    public boolean cancel=true;
    private int roomId=0;
    private static boolean active = false;
    public static boolean inherit = false;
    public static Hashtable actualIndexRulesList = new Hashtable();
    private boolean windowActive=true;
    //--------------------------------------------------------------------------
	public static void main(String[] args) {
		try{
			//String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception ex){System.out.println("Err in LF " + ex.getMessage());}
		VWClient vwc = new VWClient("0B9B5177-1011-4BBF-B617-455288043D4A","c:\\temp",1);	
		int ret = vwc.login("Syntaxwkt1200", "TestRoom","vwadmin","vw");
        VWRulesDlgSettings rulesDialog = new VWRulesDlgSettings(vwc, ret, 4022, "Restore");
        new Thread(rulesDialog).start();
        vwc.logout(ret);

	}    
}
