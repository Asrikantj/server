/*
                      Copyright (c) 1997-2001
                         Computhink Software

                      All rights reserved.
*/

/**
 * VWIndexTable<br>
 *
 * @version     $Revision: 1.2 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
**/
package com.computhink.vwc.doctype;

import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.event.TableModelEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableColumnModel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import com.computhink.common.Index;
import com.computhink.vwc.doctype.VWUtil.ColoredTableCellRenderer;
import com.computhink.vwc.doctype.VWUtil.ColorData;
import com.computhink.vwc.doctype.VWUtil.VWDocIndicesRowEditor;
import com.computhink.vwc.VWTableResizer;
import com.computhink.resource.ResourceManager;
import java.util.Date;
import java.text.Format;
import java.text.SimpleDateFormat;

public class VWDocIndicesTable extends JTable
{
    protected VWDocIndicesTableData m_data;
    final static int COL_COUNT=5;
    public VWDocIndicesTable(){
        super();
        TableColumn column = null;
        JTextField readOnlyText=new JTextField();
        setFont(readOnlyText.getFont());
        //setBorder(new EtchedBorder());
        setVisible(true);
        m_data = new VWDocIndicesTableData(this);
        setAutoCreateColumnsFromModel(false);
        setCellSelectionEnabled(false);
        setModel(m_data); 
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        readOnlyText.setEditable(false);
        Dimension tableWith = getPreferredScrollableViewportSize();
        for (int k = 0; k < VWDocIndicesTableData.m_columns.length; k++) {
        TableCellRenderer renderer;
        DefaultTableCellRenderer textRenderer =new ColoredTableCellRenderer();
        textRenderer.setHorizontalAlignment(VWDocIndicesTableData.m_columns[k].m_alignment);
        renderer = textRenderer;
        VWDocIndicesRowEditor editor=new VWDocIndicesRowEditor(this);
        column = new TableColumn(k,Math.round(tableWith.width*VWDocIndicesTableData.m_columns[k].m_width), 
                renderer, editor);
        column.setPreferredWidth((getPreferredScrollableViewportSize().width / 2)-28);
        addColumn(column);    
        }
        getTableHeader().setReorderingAllowed(false);
/*        //Set the height and width for setting space between Table Cells.        
        int columnWidth = 2;
        int columnHeight = 0;
        this.setIntercellSpacing(new Dimension(columnWidth, columnHeight));
*/        setRowHeight(18);
		//this property will set the value on lost focus of field - earlier user have to clicks on enter key
		putClientProperty("terminateEditOnFocusLost", Boolean.TRUE);
        JTableHeader header = getTableHeader();        
        //header.setUpdateTableInRealTime(true);
        header.addMouseListener(m_data.new ColumnListener(this));
        setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        setTableResizable();
        setSurrendersFocusOnKeystroke(true);
        InputMap im = getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        
        //  Have the enter key work the same as the tab key
 
        KeyStroke tab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0);
        KeyStroke shiftTab = KeyStroke.getKeyStroke(KeyEvent.VK_TAB , 1);
        KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0);
        im.put(enter, im.get(tab));
        
        //  Override the default tab behaviour
        //  Tab to the next editable cell. When no editable cells goto next cell.
 
        final Action oldTabAction = getActionMap().get(im.get(tab));
        final Action oldShiftTabAction = getActionMap().get(im.get(shiftTab));
        
        Action tabAction = new AbstractAction()
        {
            public void actionPerformed(ActionEvent e)
            {
            	if (e.getModifiers() == 0)
            		oldTabAction.actionPerformed(e);
            	if (e.getModifiers() ==1)
            		oldShiftTabAction.actionPerformed(e);
                JTable table = (JTable)e.getSource();               
                int row = table.getSelectedRow();
                int column = 1 ;
                if (e.getModifiers() == 1){
                	--row;
                	if (row <= -1){
                		row = table.getRowCount() - 1;
                	}	
                } 
                try{
	                setEditingRow(row);
	                setEditingColumn(column);
	                editCellAt(row, column);
	                table.changeSelection(row, 1, false, false);
                }catch(Exception ex){
                	//System.out.println("Ex in 170 " + ex.getMessage());
                }
                                 
            }
        };
        getActionMap().put(im.get(tab), tabAction);
        getActionMap().put(im.get(shiftTab), tabAction);

  }


    public void paintComponent(){
    	
    }
    public void setTableResizable() {
		// Resize Table
    	new VWTableResizer(this);
	}
    //--------------------------------------------------------------------------
  public void addData(Vector list)
  {
        if (list==null || list.size()==0)
            return ;
        m_data.setData(list);
        m_data.fireTableDataChanged();
  }
    //--------------------------------------------------------------------------
  public Vector getData()
  {
        return m_data.getData();
  }
    //--------------------------------------------------------------------------
  public Index getRowData(int rowNum)
  {
      return m_data.getRowData(rowNum);
  }
    //--------------------------------------------------------------------------
  public int getRowType(int rowNum)
  {
        Index row=m_data.getRowData(rowNum);
        return row.getType();
  }
    //--------------------------------------------------------------------------
  public int getRowIndexId(int rowNum)
  {
        Index row=(Index)m_data.getRowData(rowNum);
        return row.getId();
  }
    //--------------------------------------------------------------------------
    public int getRowDocTypeId(int rowNum)
  {
        Index row=m_data.getRowData(rowNum);
        return 0;
  }
  //----------------------------------------------------------------------------
  public void clearData()
  {
      m_data.clear();
      m_data.fireTableDataChanged();
  }
  //------------------------------------------------------------------------------
  public int getRowCount()
  {
      if (m_data==null) return 0;
      return m_data.getRowCount();
  }
    //--------------------------------------------------------------------------
  public String getKeyValue()
  {
      if (m_data==null) return "";
      return m_data.getKeyValue();
  }
    //--------------------------------------------------------------------------
}
class DocIndicesRowData
{
  public Object  m_name;
  public Object  m_value;
  public Index m_index;
  public DocIndicesRowData() {
    m_name = "";
    m_value="";
    m_index=null;
  }
    //--------------------------------------------------------------------------
  public DocIndicesRowData(Index index)
  {
	  String mvalue = "";
	  m_name = index.getName();
	  //The below code checks weather value MASK is number or not. If number removes all 0's starting with and displays the value.
	  String value = index.getDefaultValue();
	  if(index.getMask().trim().equals("0")){
		  for(int count = 0; count < value.length(); count++){
			  if(value.charAt(count) == '0'){
			  }
			  else{
				  mvalue = value.substring(count,value.length());
				  break;
			  }
		  }
		  m_value = mvalue;
	  }    
	  else{
		  m_value = index.getDefaultValue();	
	  }
	  /* It was not displaying sequence no issue fixed.
	  * And it was not displaying prefix and sufix is fixed
	  * If the no of digit is more than 1 than many ~ will there in mask*/ 
      if(index.getType()==Index.IDX_SEQUENCE){
    	  String prefix = "";
    	  String sufix = "";
    	  if(index.getMask().indexOf('~')!=-1){
    		  prefix = index.getMask().substring(0, index.getMask().indexOf('~'));
    		  sufix = index.getMask().substring(index.getMask().lastIndexOf('~')+1);
    	  }
    	  m_value = prefix+String.valueOf(index.getCounter())+sufix;
      }
	  if(index.getSystem()==1)
	  {
		  	Format formatter;
		  	Date date=new Date();
	        formatter = new SimpleDateFormat(index.getMask());
	        m_value= formatter.format(date);
	  }
	  m_index=index;
	  if(index.isKey()){
		  m_name = m_name + "*";
		  m_name=new ColorData(m_name,ColorData.RED);
	  }
	  else if (index.isRequired()){
		  m_name = m_name + "*";
		  m_name=new ColorData(m_name,ColorData.BLUE);
	  }
	  else{
		  m_name=new ColorData(m_name,ColorData.BLACK);
	  }
	  m_value=new ColorData(m_value,index.isKey()?ColorData.RED:ColorData.BLACK);
	  //System.out.println("M_value " + m_value +  " : ");
  	}
}
    //--------------------------------------------------------------------------
class DocIndicesColumnData
{
  public String  m_title;
  float m_width;
  int m_alignment;

  public DocIndicesColumnData(String title, float width, int alignment) {
    m_title = title;
    m_width = width;
    m_alignment = alignment;
  }
}
    //--------------------------------------------------------------------------
class VWDocIndicesTableData extends AbstractTableModel
{
  public static final DocIndicesColumnData m_columns[] = {
    new DocIndicesColumnData(ResourceManager.getDefaultManager().getString("DocIndicesColName.IndexName"),0.3F,JLabel.LEFT),
    new DocIndicesColumnData(ResourceManager.getDefaultManager().getString("DocIndicesColName.DefaultIndexVal"),0.6249F,JLabel.LEFT)
  };

  public static final int COL_NAME = 0;
  public static final int COL_VALUE = 1;

  protected VWDocIndicesTable m_parent;
  protected Vector m_vector;
  protected int m_sortCol = 0;
  protected boolean m_sortAsc = true;  
  public String sortColumn ="";
  public VWDocIndicesTableData(VWDocIndicesTable parent) {
    m_parent = parent;
    m_vector = new Vector();    
  }
    //--------------------------------------------------------------------------
public void setData(Vector data){
    m_vector.removeAllElements();
    int count =data.size();
    //Keeping old value in Hashtable for comparison. 19 Feb 2008
    VWRulesDlgSettings.actualIndexRulesList = new Hashtable();
    for(int i=0;i<count;i++)
    {
    	Index idx = (Index)data.get(i);
  	  	if (idx.getValue().trim().length() == 0 && idx.getDefaultValue().trim().length() > 0){
  	  	idx.setValue(idx.getDefaultValue());
  	  	
  	  	if (idx.getValue().trim().length() >  0 ){
  	  	  	VWRulesDlgSettings.actualIndexRulesList.put(idx.getId(), idx.getValue());
  	  	}
  	  	
	  }  	  	
        m_vector.addElement(new DocIndicesRowData(idx));
    }
  }
    //--------------------------------------------------------------------------
public void insertRow(DocIndicesRowData rowdata) {
    m_vector.addElement(rowdata);
}
    //--------------------------------------------------------------------------
public Vector getData() {
    int count=getRowCount();
    Vector data=new Vector();
    for(int i=0;i<count;i++)
    {
        DocIndicesRowData row=(DocIndicesRowData)m_vector.elementAt(i);
        data.add(row.m_index);
    }
    return data;
  }
    //--------------------------------------------------------------------------
public String getKeyValue() {
    int count=getRowCount();
    for(int i=0;i<count;i++)
    {
        DocIndicesRowData row=(DocIndicesRowData)m_vector.elementAt(i);
        ColorData colorData=(ColorData)row.m_value;
        if(colorData.getFColor()==ColorData.RED)
            return row.m_value.toString();
    }
    return "";
  }
    //--------------------------------------------------------------------------
 public Index getRowData(int rowNum) {
    DocIndicesRowData row=(DocIndicesRowData)m_vector.elementAt(rowNum);
    return row.m_index;
  }
    //--------------------------------------------------------------------------
  public int getRowCount() {
    return m_vector==null ? 0 : m_vector.size(); 
  }
    //--------------------------------------------------------------------------
  public int getColumnCount() { 
    return m_columns.length; 
  } 
    //--------------------------------------------------------------------------
  public String getColumnName(int column) { 
    return m_columns[column].m_title; 
  }
    //--------------------------------------------------------------------------
  public boolean isCellEditable(int nRow, int nCol) {
	  if(nCol!=0)
		  return true;
	  else
		  return false;
  }

  //Desc   :The method 'sortNodeProperties()' sorts the Indexes belongs to the selected Document Type. It is called when
  //		DocType selection is changed in DocType Combo Box.
  //Author :Nishad Nambiar
  //Date   :12-Dec-2007
  public void sortNodeProperties(){
	  Vector selectedDoctypeIndex = new Vector();
	  Vector otherDoctypeIndex = new Vector();
	  for(int count=0;count<getRowCount();count++){
		  DocIndicesRowData row = (DocIndicesRowData)m_vector.elementAt(count);
		  if(VWRulesDlgSettings.currentIndices.contains(row.m_index.getId())){
			  selectedDoctypeIndex.add(row);
		  }
		  else{
			  otherDoctypeIndex.add(row);
		  }
	  }
	  m_vector.removeAllElements();
	  m_vector.addAll(selectedDoctypeIndex);
	  m_vector.addAll(otherDoctypeIndex);
 }
  
    //--------------------------------------------------------------------------
  public Object getValueAt(int nRow, int nCol) {
    if (nRow < 0 || nRow>=getRowCount())
      return "";
    DocIndicesRowData row = (DocIndicesRowData)m_vector.elementAt(nRow);
    //System.out.println(" DT id "+ VWRulesDlgSettings.currentIndices.indexOf(row.m_index.getId()));
   /* if(row.m_value.toString().trim().equalsIgnoreCase("VWBlank:"))
    	row.m_value = "";*/
    switch (nCol) {
    	
      case COL_NAME:return new ColorData(row.m_name, VWRulesDlgSettings.currentIndices.contains(row.m_index.getId()) ?Color.BLUE:Color.BLACK); 
    	  //return row.m_name;
      case COL_VALUE: return new ColorData(row.m_value, row.m_index.isInherited()?Color.gray:Color.BLACK);
    	  //return row.m_value;
    }
    return "";
  }
  public void setValueAt(Object value, int nRow, int nCol) {
	    if (nRow < 0 || nRow >= getRowCount())
	      return;
	    DocIndicesRowData row = (DocIndicesRowData)m_vector.elementAt(nRow);
	    String svalue = (value== null)?"":value.toString();
	    
	    switch (nCol) {
	      case COL_NAME:
	          row.m_name = svalue;
	        break;
	      case COL_VALUE:
	        row.m_value = svalue;
	        row.m_index.setValue(svalue);
	        break;
	    }
  }
    //--------------------------------------------------------------------------
  public void clear(){
    m_vector.removeAllElements();
  }
  class ColumnListener extends MouseAdapter
  {
    protected VWDocIndicesTable m_table;      
    //-----------------------------------------------------------
    public ColumnListener(VWDocIndicesTable table){
      m_table = table;
    }
    //-----------------------------------------------------------
    public void mouseClicked(MouseEvent e){
      
        if(e.getModifiers()==e.BUTTON1_MASK){
			try{
				sortCol(e);
				//System.out.println("mouseClicked for sorting.");
				}catch(Exception ex){
			}
		}
    }    
    //-----------------------------------------------------------
    //Changed to capture which column sorted. 
    private void sortCol(MouseEvent e)
    {
    	/*when table cell is in editable mode, click of header the value 
    	 * will not be moved with the Column name.Value will be in the same row for the 
    	 * other column. 
    	 * To avoid the above scenario, stop the cell editing on click of header for sorting. 
    	 * In otherwords stop the cell editing while sorting.
    	 * --PR-- 
    	 */
    	try{
    		if (m_table.getCellEditor() != null){    			
    			m_table.getCellEditor().stopCellEditing();
    		}
    	}catch(Exception ec){}
    	try{
	        TableColumnModel colModel = m_table.getColumnModel();
	        int columnModelIndex = colModel.getColumnIndexAtX(e.getX());
	        int modelIndex = colModel.getColumn(columnModelIndex).getModelIndex();
	    	
	        try{
	        sortColumn = getColumnName(columnModelIndex);
	        }catch(Exception ex){
	        	//System.out.println("Exception in sortColumn " + ex.getMessage());
	        	return;
	        }	        
	        if (modelIndex < 0)
	        return;
	        if (m_sortCol==modelIndex)
	        	m_sortAsc = !m_sortAsc;
	        else
	        	m_sortCol = modelIndex;

	        for (int i=0; i < colModel.getColumnCount();i++) {
	        TableColumn column = colModel.getColumn(i);
	        try{
	        column.setHeaderValue(getColumnName(column.getModelIndex()));
	        }catch(Exception ex){
	        	//System.out.println("ex in header " + ex.getMessage());
	        	return;
	        }
	        }
	        m_table.getTableHeader().repaint();
	        Collections.sort(m_vector, new IndexListComparator(modelIndex, m_sortAsc));
	        m_table.tableChanged(
	        new TableModelEvent(VWDocIndicesTableData.this)); 
	        m_table.repaint();
    	}catch(Exception ex){
    		//System.out.println("Exception in Sorting " + ex.getMessage());
    	}
    }
  
    //--------------------------------------------------------------------------

    //--------------------------------------------------------------------------
  }
  class IndexListComparator implements Comparator
  {
    protected int     m_sortCol;
    protected boolean m_sortAsc;
      //-----------------------------------------------------------
    public IndexListComparator(int sortCol, boolean sortAsc) {
      m_sortCol = sortCol;
      m_sortAsc = sortAsc;
    }
      //-----------------------------------------------------------
    public int compare(Object o1, Object o2) {
      if(!(o1 instanceof DocIndicesRowData) || !(o2 instanceof DocIndicesRowData))
        return 0;
      DocIndicesRowData s1 = (DocIndicesRowData)o1;
      DocIndicesRowData s2 = (DocIndicesRowData)o2;
      int result = 0;       
      switch (m_sortCol) {    
        case COL_NAME: //0:
          result = s1.m_name.toString().compareTo(s2.m_name.toString());
          break;
        case COL_VALUE: //1:
            result = s1.m_value.toString().compareTo(s2.m_value.toString());
            break;
      }
      if (!m_sortAsc)
        result = -result;
      return result;
    }
      //-----------------------------------------------------------
    public boolean equals(Object obj) {
      if (obj instanceof IndexListComparator) {
        IndexListComparator compObj = (IndexListComparator)obj;
        return (compObj.m_sortCol==m_sortCol) && 
          (compObj.m_sortAsc==m_sortAsc);
      }
      return false;
    }
  }}
