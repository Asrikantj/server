package com.computhink.vwc;

import java.awt.Font;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import com.computhink.resource.ResourceManager;
import com.computhink.common.Constants;
import com.computhink.vwc.image.Images;

public class VWRouteSummaryDetails extends JDialog implements VWCConstants{
    
    JLabel lblRouteName, lblUserName, lblReceivedOn, lblProcessedOn, lblComments, lblTaskName, lblRouteNameVal, lblUserNameVal, lblReceivedOnVal, lblProcessedOnVal, lblTaskNameVal;
    JButton bOk;
    JTextArea txtComments;
    JScrollPane scComments;
    VWRouteSummary summaryInfo;
    private static ResourceManager resourceManager=ResourceManager.getDefaultManager();
    JPanel pnlData = new JPanel(); 
    public VWRouteSummaryDetails() {
	// TODO Auto-generated constructor stub
	super();
	setModal(true);
	initComponents();
	/**CV2019 merges from SIDBI line***/
	setSize(500, 400);
	setVisible(true);
	setDefaultCloseOperation(DISPOSE_ON_CLOSE);	
    }	
    VWRouteSummaryDetails(JFrame parent, VWRouteSummary summaryInfo){
	super(parent, true);
	this.summaryInfo  = summaryInfo;
	initComponents();
	setTitle(resourceManager.getString("RouteSummary.Title") + summaryInfo.getRouteData(COL_ROUTENAME) + "): " + summaryInfo.getRouteData(COL_TASK));
	setIconImage(Images.task.getImage());
	/**CV2019 merges from SIDBI line***/
	setSize(500, 400);
	setLocation(parent.getX(), parent.getY());
	setResizable(false);
	setVisible(true);
	setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	
    }
    public void initComponents(){
	getContentPane().add(pnlData);
	pnlData.setLayout(null);
	
	lblRouteName = new JLabel(resourceManager.getString("WORKFLOW_NAME") +" "+resourceManager.getString("RouteSummary.Name")+" ");
	lblRouteName.setBounds(7, 8,350, 24);
	pnlData.add(lblRouteName);
	Font font = lblRouteName.getFont();
	
	lblRouteNameVal = new JLabel("	"+resourceManager.getString("WORKFLOW_NAME") +" "+resourceManager.getString("RouteSummary.NameVal")+" ");
	lblRouteNameVal.setBounds(120, 8, 300, 24);
	pnlData.add(lblRouteNameVal);
	
	lblTaskName = new JLabel(resourceManager.getString("RouteSummaryLbl.TaskName")+" ");
	lblTaskName.setBounds(8, 36, 44, 24);
	pnlData.add(lblTaskName);
	
	lblTaskNameVal = new JLabel(" "+resourceManager.getString("RouteSummaryLbl.TaskNameVal"));
	lblTaskNameVal.setBounds(48, 36, 164, 24);
	pnlData.add(lblTaskNameVal);


	lblReceivedOn = new JLabel(resourceManager.getString("RouteSummaryLbl.ReceivedOn")+" ");
	lblReceivedOn.setBounds(212, 36, 96, 24);
	pnlData.add(lblReceivedOn);

	lblReceivedOnVal  = new JLabel(resourceManager.getString("RouteSummaryLbl.ReceivedOnVal"));
	lblReceivedOnVal.setBounds(310, 36, 250, 24);
	pnlData.add(lblReceivedOnVal);

	
	lblUserName = new JLabel(resourceManager.getString("RouteSummaryLbl.UserName")+" ");
	lblUserName.setBounds(8, 60, 100, 24);
	pnlData.add(lblUserName);
	
	lblUserNameVal = new JLabel(resourceManager.getString("RouteSummaryLbl.UserNameVal"));
	lblUserNameVal.setBounds(48, 60, 164, 24);
	pnlData.add(lblUserNameVal);

	
	lblProcessedOn = new JLabel(resourceManager.getString("RouteSummaryLbl.ProcessedOn")+" ");
	lblProcessedOn.setBounds(212, 60, 100, 24);
	pnlData.add(lblProcessedOn);
	
	lblProcessedOnVal = new JLabel(resourceManager.getString("RouteSummaryLbl.ProcessedOnVal"));
	lblProcessedOnVal.setBounds(310, 60, 250, 24);
	pnlData.add(lblProcessedOnVal);

	
	lblComments = new JLabel(resourceManager.getString("RouteSummaryLbl.Comments")+" ");
	lblComments.setBounds(8, 84, 80, 24);
	pnlData.add(lblComments);
	lblComments.setAutoscrolls(true);
	
	txtComments = new JTextArea();
	txtComments.setText("A strapping fast bowler born in Australia, Johnston's career began at New South Wales where he played alongside the likes of Mark Taylor, Michael Slater and Brett Lee, before choosing to represent Ireland. A positive captain who led from the front, he had little problem motivating his side - as demonstrated when Ireland bounced back from a disappointing World Cricket League in Kenya, where they finished 5th, to demolish the United Arab Emirates and qualify for the final of the Intercontinental Cup. He tore the heart out of the UAE batting line up in their second innings taking three for eight from his eight overs as Ireland recorded a morale-boosting victory by an innings and 170 runs. He led Ireland to a thrilling tie against Zimbabwe in their first match of the 2007 World Cup before beating Pakistan - Johnston hit the winning runs - to record one of the biggest upsets in history.");
	
	txtComments.setLineWrap(true);
	txtComments.setEditable(false);
	txtComments.setBackground(getBackground());
	txtComments.setAutoscrolls(true);
	
	
	scComments = new JScrollPane();
	scComments.setViewportView(txtComments);
	/**CV2019 merges from SIDBI line***/
	scComments.setBounds(8, 104, 425, 200);
	scComments.setBorder(null);
	scComments.setAutoscrolls(true);
	pnlData.add(scComments);
	
	lblRouteName.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblTaskName.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblReceivedOn.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblUserName.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblProcessedOn.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	lblComments.setFont(new Font(font.getFontName(), Font.BOLD, font.getSize()));
	
	bOk = new JButton(resourceManager.getString("RouteSummaryBtn.Ok"));
	/**CV2019 merges from SIDBI line***/
	bOk.setBounds(370, 310, 65, 24);
	pnlData.add(bOk);
	
	
	bOk.addActionListener(new ActionListener(){
	    public void actionPerformed(ActionEvent e){
		setVisible(false);
		dispose();
	    }
	});
	setDetails();
    }
    
    public void setDetails(){
    /****CV2019 merges from SIDBI line--------------------------------***/   	
    lblRouteNameVal.setText(summaryInfo.getRouteData(8));
	lblTaskNameVal.setText(summaryInfo.getRouteData(2));
	lblReceivedOnVal.setText(summaryInfo.getRouteData(6));
	lblUserNameVal.setText(summaryInfo.getRouteData(1));
	lblProcessedOnVal.setText(summaryInfo.getRouteData(7));	
	txtComments.setText(summaryInfo.getRouteData(3));
	/*------------------End of SIDBI merges----------------------------*/
    }
    public static void main(String[] args) {
	try{
	    String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
	    String windowsLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
	    UIManager.setLookAndFeel(plasticLookandFeel);       
	    VWRouteSummaryDetails d = new VWRouteSummaryDetails();
	    d.initComponents();
	    
	}catch(Exception ex){
	    
	}
	new VWRouteSummaryDetails(); 
    }
}
