/* JAVA Port by Robert Neild November 1999 */

/* PC1 Cipher Algorithm ( Pukall Cipher 1 ) */
/* By Alexander PUKALL 1991 */
/* free code no restriction to use */
/* please include the name of the Author in the final software */
/* the Key is 128 bits */

/* Only the K zone change in the two routines */
/* You can create a single routine with the two parts in it */
package com.computhink.vwc;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.StringTokenizer;

import javax.crypto.Cipher;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptographyUtils {
	
    private int ax,bx,cx,dx,res,i,inter,cfc,cfd,compte;    
    
    private byte cle[] = new byte [17];               // holds key
    
    private int x1a0[] = new int [8];
    
    private int max_limit = 1024 * 1024;        
    
    private static final String DEFAULT_KEY = "D36083DE-284C-4d62-8B4A-0F1537A654B0";
    
    private static final String DEFAULT_SIGNATURE = "18f39b10-b4c7-11d9-9669-0800200c9a66";

 /*   public static void main(String[] args) {
    	CryptographyUtils utils1 = new CryptographyUtils();
    	String encryptData=desEncryptData("sa");
    	System.out.println("encryptedData :::"+encryptData);
    	//String EncryptData=desEncryptData("sa");
    	String decrypted=utils1.desDecryptData("217-23-224-75-232-54-99-91");
    	System.out.println("decrypted :::"+decrypted);
    }*/
    
    
    /*public static void main(String[] args) {

    	String s0 = "C:\\Users\\balamurugan.j\\Desktop\\logo.gif";
    	String s1 = "C:\\Users\\balamurugan.j\\Desktop\\logo.vw";
    	System.out.println("Test of CryptographyUtils1.");

    	System.out.println();
    	System.out.println(System.currentTimeMillis());
    	CryptographyUtils utils1 = new CryptographyUtils();
    	utils1.encryptFile(s0, s1);
    	utils1 = null;		 
    	System.out.println(System.currentTimeMillis());		 
    	System.out.println("done");   	    	    	
    	
    	String d0 = "C:\\Users\\balamurugan.j\\Desktop\\logo.vw";
    	String d1 = "C:\\Users\\balamurugan.j\\Desktop\\logoXXX.gif";
    	System.out.println("Test of CryptographyUtils2.");

    	System.out.println();
    	System.out.println(System.currentTimeMillis());
    	CryptographyUtils utils2 = new CryptographyUtils();
    	utils2.decryptFile(d0, d1);
    	utils2 = null;		 
    	System.out.println(System.currentTimeMillis());		 
    	System.out.println("done");
    	
	}*/
    
	/**
	 * This method is  used to DES encrypt a string
	 * @param data
	 * @return String
	 */
    public static String desEncryptData(String data) {
    	try {
    		DESKeySpec desKeySpec = new DESKeySpec(DEFAULT_KEY.getBytes());
    		SecretKeySpec secretKey = new SecretKeySpec(desKeySpec.getKey(), "DES");
    		Cipher desEncryptCipher = Cipher.getInstance("DES/ECB/PKCS5Padding"); 
    		desEncryptCipher.init(Cipher.ENCRYPT_MODE, secretKey);
    		byte[] encryptedData = desEncryptCipher.doFinal(data.getBytes());
    		return getString(encryptedData);
    	} catch( Exception e ) {
    		e.printStackTrace();
    		return null;
    	}
    }
    
	/**
	 * This method is  used to DES decrypt a string
	 * @param encryptedData
	 * @return String
	 */
	public static String desDecryptData( String encryptedData ) {
		try {
			DESKeySpec desKeySpec = new DESKeySpec(DEFAULT_KEY.getBytes());
			SecretKeySpec secretKey = new SecretKeySpec(desKeySpec.getKey(), "DES");
			Cipher desDecryptCipher = Cipher.getInstance("DES/ECB/PKCS5Padding"); 
			desDecryptCipher.init(Cipher.DECRYPT_MODE, secretKey); 
			byte[] decryptedData = desDecryptCipher.doFinal(getBytes(encryptedData));
			return new String(decryptedData);
		} catch( Exception e ) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * @param bytes
	 * @return String
	 */
	private static String getString( byte[] bytes ) {
		StringBuffer sb = new StringBuffer();
		for( int i=0; i<bytes.length; i++ ) {
			byte b = bytes[ i ];
			sb.append( ( int )( 0x00FF & b ) );
			if( i+1 <bytes.length ) {
				sb.append( "-" );
			}
		}
		return sb.toString();
	}
	
	/**
	 * @param str
	 * @return byte
	 */
	private static byte[] getBytes( String str ) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		StringTokenizer st = new StringTokenizer( str, "-", false );
		while( st.hasMoreTokens() ) {
			int i = Integer.parseInt( st.nextToken() );
			bos.write( ( byte )i );
		}
		return bos.toByteArray();
	}
	
    
	/**
	 * This method is used to encrypt a file
	 * @param fileIn
	 * @param fileOut
	 */
	public void encryptFile(String fileIn, String fileOut) 
	{								
		FileChannel fis = null, fos = null;		
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		ByteBuffer buffer = null, signature = null;
		File encryptFile = null, encryptedFile = null;
		try {
			if (fileIn!=null && fileOut!=null) {
				if (!fileIn.trim().equals("") && !fileOut.trim().equals("")) {
					saveKey(DEFAULT_KEY.getBytes());
					encryptFile = new File(fileIn);
					if (encryptFile.isFile() && encryptFile.exists()) {
						inputStream = new FileInputStream(encryptFile);
						fis = inputStream.getChannel();
						buffer = ByteBuffer.allocate((int) encryptFile.length());
						@SuppressWarnings("unused")
						long checkSum = 0L;
						int nRead;
						while ((nRead = fis.read(buffer)) != -1) {
							if (nRead == 0)
								continue;
							buffer.position(0);
							buffer.limit(nRead);
							while (buffer.hasRemaining())
								checkSum += buffer.get();
							buffer.clear();							
						}							
						fis.close();
						inputStream.close();						
						System.out.println(">>>>>>>>>>>>>>>>>>>>>1 : " + buffer.limit());
						cfc = 0; cfd = 0;
						for (int index=0; index<buffer.limit(); index++) {
							if (index<=max_limit)
								buffer.put(index, (byte)encrypt(buffer.get(index)));
							else
								break;
						}
						encryptedFile = new File(fileOut);	
						outputStream = new FileOutputStream(encryptedFile); 
						fos = outputStream.getChannel();
						signature = ByteBuffer.wrap(getFileSignature().getBytes());
						fos.write(signature);						
						fos.write(buffer);
						fos.close();		
						outputStream.close();
					}
				}
			}			
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			signature = null;			
			fis = null; fos = null; buffer = null;
			inputStream  = null; outputStream = null;
			encryptFile = null; encryptedFile = null;						
		}
	}
	
	/**
	 * This method is used to decrypt a file
	 * @param fileIn
	 * @param fileOut
	 */
	public void decryptFile(String fileIn, String fileOut) {								
		FileChannel fis = null, fos = null;
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		ByteBuffer buffer = null, signature = null;
		File encryptFile = null, encryptedFile = null;
		try {
			if (fileIn!=null && fileOut!=null) {
				if (!fileIn.trim().equals("") && !fileOut.trim().equals("")) {
					saveKey(DEFAULT_KEY.getBytes());
					encryptFile = new File(fileIn);
					if (encryptFile.isFile() && encryptFile.exists()) {
						inputStream = new FileInputStream(encryptFile);
						fis = inputStream.getChannel();			
						buffer = ByteBuffer.allocate((int) encryptFile.length());
						@SuppressWarnings("unused")
						long checkSum = 0L;
						int nRead;
						while ((nRead = fis.read(buffer)) != -1) {
							if (nRead == 0)
								continue;
							buffer.position(0);
							buffer.limit(nRead);
							while (buffer.hasRemaining())
								checkSum += buffer.get();
							buffer.clear();
							
						}						
						fis.close();	
						inputStream.close();
						// signature verification
						int bIndex = 0;
						String fileSignature = getFileSignature();
						boolean validateSign = true;
						if (buffer.limit()>71 && fileSignature!=null) {
							for (int cIndex=0; cIndex<buffer.limit(); cIndex++) {
								if (cIndex>71 && validateSign==true) {
									if (signature==null) {
										signature = ByteBuffer.allocate((int) (buffer.limit()-72));
									}
									signature.put(bIndex, buffer.get(cIndex));
									bIndex++;
								} else {															
									System.out.print((char)buffer.get(cIndex));
									if (((char)buffer.get(cIndex)==fileSignature.charAt(cIndex))==false) {
										validateSign = false;								
									}		
								}
							}
						}
						System.out.println(">>>>>>>>>>>>>>>>>>>>>2 : " + buffer.limit());
						System.out.println(">>>>>>>>>>>>>>>>>>>>>3 : " + signature.limit());
						cfc = 0; cfd = 0;
						for (int index=0; index<signature.limit(); index++) {
							if (index<=max_limit)
								signature.put(index, (byte)decrypt(signature.get(index)));
							else
								break;
						}			
						encryptedFile = new File(fileOut);
						outputStream = new FileOutputStream(encryptedFile);
						fos = outputStream.getChannel();						
						fos.write(signature);
						fos.close();		
						outputStream.close();
					}
				}
			}			
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			signature = null;
			buffer = null; fis = null; fos = null;
			inputStream  = null; outputStream = null;
			encryptFile = null; encryptedFile = null;						
		}
	}
	
	/**
	 * This method is used to encrypt
	 * @param c
	 * @return
	 * @throws IOException
	 */
	private int encrypt(int c) throws Exception 
	{    	  		
		assemble();
		cfc=(int) (inter>>8);
		cfd=(int) (inter&255); /* cfc^cfd = random byte */

		/* K ZONE !!!!!!!!!!!!! */
		/* here the mix of c and cle[compte] is before the encryption of c */

		for (compte=0;compte<=15;compte++)
		{
			/* we mix the plaintext byte with the key */  	    
			cle[compte]=(byte) (cle[compte]^c);
		}

		c = c ^ (cfc^cfd);
  	 
		return c;  /* we write the crypted byte */
	}            
      
    /**
     * This method is sued to decrypt
     * @param c
     * @return
     * @throws IOException
     */
    private int decrypt(int c) throws Exception 
    {   
    	assemble();
    	cfc=(int) (inter>>8);
    	cfd=(int) (inter&255); /* cfc^cfd = random byte */
  	    
    	/* K ZONE !!!!!!!!!!!!! */
    	/* here the mix of c and cle[compte] is after the decryption of c */
  	    
    	c = c ^ (cfc^cfd);
  	   
    	for (compte=0;compte<=15;compte++)
    	{
    		/* we mix the plaintext byte with the key */
    		cle[compte]=(byte) (cle[compte]^ c);
    	}

    	return c;      /* write the decrypted byte */
    }
      
    /**
     * This method is used to assemble
     */
    private void assemble() throws Exception
    {	 
    	x1a0[0]= (int) ( ( cle[0]*256 )+ cle[1]);
    	code();
    	inter=res;
		 
    	x1a0[1]= (int) (x1a0[0] ^ ( (cle[2]*256) + cle[3]));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[2]= (int) (x1a0[1] ^ ( (cle[4]*256) + cle[5]));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[3]= (int) (x1a0[2] ^ ( (cle[6]*256) + cle[7] ));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[4]= (int) (x1a0[3] ^ ( (cle[8]*256) + cle[9] ));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[5]= (int) (x1a0[4] ^ ( (cle[10]*256) + cle[11] ));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[6]= (int) (x1a0[5] ^ ( (cle[12]*256) + cle[13] ));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[7]= (int) (x1a0[6] ^ ( (cle[14]*256) + cle[15] ));
    	code();
    	inter=(int) (inter^res);
		 
    	i=0;
    }
    
    /**
     * This method is used to code
     */
    private void code() throws Exception
    {
    	int si = 0, tmp = 0, x1a2 = 0;
    	
    	dx=(int) (x1a2+i);
    	ax=x1a0[i];
    	cx=0x015a;
    	bx=0x4e35;
	 
    	tmp=ax;
    	ax=si;
    	si=tmp;
	 
    	tmp=ax;
    	ax=dx;
    	dx=tmp;
	 
    	if (ax!=0) {
    		ax=(int) (ax*bx);
    	}

    	tmp=ax;
    	ax=cx;
    	cx=tmp;
	 
    	if (ax!=0)  {
    		ax=(int) (ax*si);
    		cx=(int) (ax+cx);
    	}

    	tmp=ax;
    	ax=si;
    	si=tmp;
    	ax=(int) (ax*bx);
    	dx=(int) (cx+dx);
	 
    	ax=(int) (ax+1);
	 
    	x1a2=dx;
    	x1a0[i]=ax;
	 
    	res=(int) (ax^dx);
    	i=(int) (i+1);
    }
        
    /**
     * @return
     */
    private String getFileSignature() throws Exception 
    {
    	String formattedSignature = "";
    	for (int idx=0; idx<DEFAULT_SIGNATURE.length(); idx++) {    		
    		formattedSignature += DEFAULT_SIGNATURE.charAt(idx);
    		formattedSignature += Character.toString('\0');
    	}    	
    	return formattedSignature;
    }
    
    /**
     * This method is used to save the key
     * @param key
     */
    private void saveKey(byte[] key) throws Exception
    { 
        for (int idx=0; idx<Math.min(16, key.length); idx++){ 
          cle[idx] = key[idx]; 
        } 
    }        
}