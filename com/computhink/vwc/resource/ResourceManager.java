/*
 * ResourceManager.java
 *
 * Created on Aug 18, 2008
 * Author :Nishad Nambiar
 * 
 */

package com.computhink.vwc.resource;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.MissingResourceException;
/**
 * A class that manages resource bundles.
 * Follows the singleton pattern.
 * @author  Nishad Nambiar
 */
public class ResourceManager 
{
    /** stores singleton instances of managers */
    private static HashMap managers = new HashMap();
    
    /** current resource bundle */
    private ResourceBundle bundle;
    
    /** default resource bundle */
    private static final String defaultBundlePath = "com.computhink.vwc.resource.Connector";
    
    /** Creates a new instance of ResourceManager */
    private ResourceManager(ResourceBundle bundle) 
    {
        this.bundle = bundle;
    }
    
    /** takes key string as an argument and returns associated value */
    public String getString(String key) {
        String value = null;
        try 
        {
            value = bundle.getString(key);
        } catch (MissingResourceException e) 
        {
            //System.out.println("java.util.MissingResourceException: Couldn't find value for: " + key);
        }
        if(value == null) {
            value = "  ";
        }
        return value;
    }
    
    /** returns manager instance according to bundle supplied */
    public static ResourceManager getManager(String bundlePath) 
    {
        ResourceManager manager = (ResourceManager)managers.get(bundlePath);
        if (manager == null)
        {
            manager = new ResourceManager(ResourceBundle.getBundle(bundlePath));
            managers.put(bundlePath, manager);
        }
        return manager;
    }    
    
    /** returns default manager instance */
    public static ResourceManager getDefaultManager()
    {
        return getManager(defaultBundlePath);
    }
}
