/*
                      Copyright (c) 1997-2003
                         Computhink Software

                      All rights reserved.
*/

package com.computhink.database;
/**
 * ConnectionAdapter.<br>
 *
 * @version     $Revision: 1.3 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
import java.io.*;
import java.sql.*;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public final class ConnectionAdapter implements DatabaseConstants
{
    private Database database;
    private Connection sqlConnection;
    private int sqlConnectionsUsed;
    private int referenceCount;
    {
        database = null;
        sqlConnection = null;
        sqlConnectionsUsed = 0;
        referenceCount = 0;
    }
    ConnectionAdapter(Connection connection)
    {
        this.sqlConnection = connection;
    }
    ConnectionAdapter(Database database)
    {
        this.database = database;
    }
    synchronized void ensureOpen() throws SQLException
    {
        ensureConnectionIsOpen();
    }
    int aquireReference()
    {
        return ++referenceCount;
    }
    int releaseReference()
    {
        return --referenceCount;
    }
    boolean inUse()
    {
        return referenceCount > 0;
    }
    private void ensureConnectionIsValid() throws SQLException
    {
        if (sqlConnection == null)
        {
            throw new SQLException("ConectionAdapter has no valid connection");
        }
    }
    private void ensureConnectionIsOpen() throws SQLException
    {
        if (sqlConnection != null && !sqlConnection.isClosed())
        {
            // already open and ready to be used;
            return;
        }
        sqlConnection = getConnectionFromDriverManager();
        sqlConnectionsUsed++;
    }
    void closeAfterDelay() throws SQLException
    {
        // ensure transaction was closed
        sqlConnection.rollback();
        // make sure the connection is still not being used
        if (inUse()) return;
        // throw away our reference to the timer
        try
        {
            closeNow();
            //if (LOG_VERBOSE_DEBUG) Log.verboseDebug("Closed Connection");
        }
        catch (SQLException e)
        {
            //if (LOG_DEBUG) Log.SQLException(DEBUG_TYPE,
                   //                         "unable to close connection", e);
        }
    }
    private void closeNow() throws SQLException
    {
        if (!sqlConnection.isClosed())
        {
            // ensure last transaction has completed
            sqlConnection.commit();
            sqlConnection.close();
        }
        sqlConnection = null;
    }
    private Connection getConnectionFromDriverManager() throws SQLException
    {
        String url = database.jdbcUrl;
        String user = database.jdbcUser;
        String password = database.jdbcPassword;
        String jdbcUrlService=database.jdbcUrlService;
        String engine=database.engineName;
        SQLServerDataSource dataSource = database.dataSource;

        Connection connection =null;
        if (dataSource == null)
        	DriverManager.setLoginTimeout(10);
        Database.printToConsole("*********getConnectionFromDriverManager***************");
        Database.printToConsole("user -> "+user);
        Database.printToConsole("dataSource object -> "+dataSource);
		if (user == null || user.length() == 0) {			
			if (engine.equalsIgnoreCase("oracle")) {
				try {
					connection = DriverManager.getConnection(jdbcUrlService);

				} catch (Exception e) {
					try {
						connection = DriverManager.getConnection(url);

					} catch (Exception exp) {

						throw exp;

					}
				}

			} else {
				try {
					if (dataSource != null) {
						connection = dataSource.getConnection();
						Database.printToConsole("connection object by using sqldatasource -> "+connection);
					} else {
						connection = DriverManager.getConnection(url);
						Database.printToConsole("connection object by using drivermanager -> "+connection);
					}
				} catch (Exception e) {
					throw e;
				}
			}
		} else {
			if (engine.equalsIgnoreCase("oracle")) {
				try {
					connection = DriverManager.getConnection(jdbcUrlService, user, password);
				} catch (Exception e) {
					try {
						connection = DriverManager.getConnection(url, user, password);

					} catch (Exception exp) {

						throw exp;

					}
				}

			} else {
				try {
					if (dataSource != null) {
						connection = dataSource.getConnection();
						Database.printToConsole("connection object by using sqldatasource -> "+connection);
					} else {
						connection = DriverManager.getConnection(url, user, password);
						Database.printToConsole("connection object by using drivermanager -> "+connection);
					}
				} catch (Exception e) {
					throw e;
				}
			}

		}

        // we need to control the transactions
        if (!database.getEngineName().equalsIgnoreCase("DSN"))
            connection.setAutoCommit(false);

        /*
        // make sure we get no Dirty, non-repeatable or phantom reads
        if (connection.getMetaData().supportsTransactionIsolationLevel(
                connection.TRANSACTION_SERIALIZABLE))
        {
            connection.setTransactionIsolation(
                connection.TRANSACTION_SERIALIZABLE);
        }
*/
// make sure we get no Dirty, non-repeatable or phantom reads
        if (connection.getMetaData().supportsTransactionIsolationLevel(
                connection.TRANSACTION_READ_UNCOMMITTED))
        {
            connection.setTransactionIsolation(
                connection.TRANSACTION_READ_UNCOMMITTED);
        }
        return connection;
    }
    public Connection getConnection()
    {
        return sqlConnection;
    }
    public String getStatistics()
    {
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);

        //writer.println();
        //writer.println("Connection Adapter Statistics\t-> " + this);
        //writer.println("SQL Connections Used\t\t-> " + sqlConnectionsUsed);
        //writer.println("Current Reference Count\t\t-> " + referenceCount);

        try{stringWriter.close();}
        catch(IOException e){}
        
        return stringWriter.toString();
    }
    public int getReferenceCount(){
    	return referenceCount;
    }

}