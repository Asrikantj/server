package com.computhink.database;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public class DSNConnectionManager implements DatabaseConstants {
	private DSNDatabase database;
    private int minPoolSize;
    private int maxPoolSize;
    private int connectionsInUse;

    private LinkedList connectionPool;
    private LinkedList availableConnections;
    private HashMap contextToConnection;
    //private HashMap connectionToContext;

    // statistics
    /*private int maxConnectionsUsed;
    private int numberOfNewContextConnections;
    private int numberOfSameContextConnections;
    private int numberOfConnectionWaits;*/
    {
        minPoolSize = 0;
        maxPoolSize = 1;
        connectionsInUse = 0;
        connectionPool = new LinkedList();
        availableConnections = new LinkedList();
        contextToConnection = new HashMap();
        //connectionToContext = new HashMap();
        /*maxConnectionsUsed = 0;
        numberOfNewContextConnections = 0;
        numberOfSameContextConnections = 0;
        numberOfConnectionWaits = 0;*/
    }

    DSNConnectionManager(DSNDatabase database)
    {
        this.database = database;
        maxPoolSize = database.connectionMaxPoolSize;
        minPoolSize = database.connectionMinPoolSize;
        DSNDatabase.printToConsole("maxPoolSize DSNConnectionManager >>>> "+minPoolSize);
        DSNDatabase.printToConsole("minPoolSize DSNConnectionManager >>>> "+maxPoolSize);
        DSNDatabase.printToConsole("Existing availableConnections >>>> "+availableConnections);
        if (maxPoolSize < 0)
        {
            maxPoolSize = DEFAULT_CONNECTION_MAX_POOL_SIZE;
        }

        if (minPoolSize < 0)
        {
            minPoolSize = DEFAULT_CONNECTION_MIN_POOL_SIZE;
        }
        DSNDatabase.printToConsole("minPoolSize DSNConnectionManager constructor >>>> "+minPoolSize);
        /*for (int i=0; i < minPoolSize; i++)
        {*/
            DSNConnectionAdapter connectionAdapter = new DSNConnectionAdapter(database);
            connectionPool.add(connectionAdapter);
            availableConnections.add(connectionAdapter);            
        	DSNDatabase.printToConsole("DSN added in availableConnections list :"+database.getDsnName()+", connectionAdapter :"+connectionAdapter);
		//}
        DSNDatabase.printToConsole("connectionPool >>>>"+connectionPool);
        DSNDatabase.printToConsole("availableConnections >>>>"+availableConnections);
    }
    public synchronized Connection aquireConnection() throws  SQLException
    {
        return aquireConnection(null);
    }
    public synchronized Connection aquireConnection(Object context) throws  SQLException
    {
    	DSNDatabase.printToConsole("Available connections in aquireconnection >>> "+availableConnections);
    	DSNConnectionAdapter connectionAdapter = null;
        if (availableConnections.size() > 0){
        	connectionAdapter = (DSNConnectionAdapter) availableConnections.getLast();
			DSNDatabase.printToConsole("availableConnections.getLast() >>>"+availableConnections.getLast());
        	DSNDatabase.printToConsole("connectionAdapter >>>>"+connectionAdapter);
        	if (connectionAdapter != null)
        	availableConnections.removeLast();          	
        }
        DSNDatabase.printToConsole("Final available connections >>> "+availableConnections);
        if (connectionAdapter == null)
        {
            synchronized(this)
            {
                while (connectionAdapter == null)
                {
                    connectionAdapter = findAvailableConnectionAdapter(context);
                    DSNDatabase.printToConsole("connectionAdapter in loop  >>>>"+connectionAdapter);
                    if (connectionAdapter == null)
                    {
						try {
							//numberOfConnectionWaits++;
							this.wait();
						} catch (InterruptedException e) {
						}
                        continue;
                    }
                }
                //numberOfNewContextConnections++;
                availableConnections.remove(connectionAdapter);
                DSNDatabase.printToConsole("availableConnections after remove :"+availableConnections);
                //DSNDatabase.printToConsole("maxConnectionsUsed   >>>>"+maxConnectionsUsed);
                DSNDatabase.printToConsole("connectionsInUse   >>>>"+connectionsInUse);
                /*if (connectionsInUse > maxConnectionsUsed)
                {
                    maxConnectionsUsed = connectionsInUse;
                }
                DSNDatabase.printToConsole("maxConnectionsUsed   >>>>"+maxConnectionsUsed);*/
            }
        }
        /*else
        {
            numberOfSameContextConnections++;
        }*/
        DSNDatabase.printToConsole("connectionAdapter  >>>>"+connectionAdapter);
        DSNDatabase.printToConsole("before ensureOpen call");
        // open the connection if necessary
        connectionAdapter.ensureOpen();
        DSNDatabase.printToConsole("after ensureOpen call");
        // increment reference count
        connectionAdapter.aquireReference();
        DSNDatabase.printToConsole("contextToConnection before  >>>>"+contextToConnection);
        contextToConnection.put(connectionAdapter.getConnection(), connectionAdapter);
        DSNDatabase.printToConsole("contextToConnection after  >>>>"+contextToConnection);
        connectionsInUse++;
        return connectionAdapter.getConnection();
    }

    private DSNConnectionAdapter findAvailableConnectionAdapter(Object context)
    {
        // add another connection to the pool if necessary
        if (availableConnections.size() == 0)
        {
            if (connectionPool.size() < maxPoolSize)
            {
                // Adding an Adapter to the pool
                // create another connection adapter
                DSNConnectionAdapter connectionAdapter = 
                                                new DSNConnectionAdapter(database);
                connectionPool.add(connectionAdapter);
                availableConnections.add(connectionAdapter);
            }
            else
            {
                //Pool size is at max
            }
        }
        DSNDatabase.printToConsole("connectionPool in findAvailableConnectionAdapter :"+connectionPool);
        DSNDatabase.printToConsole("availableConnections in findAvailableConnectionAdapter :"+availableConnections);
        // note: there may not be any available
        DSNConnectionAdapter connectionAdapter = null;
        try
        {
            connectionAdapter = (DSNConnectionAdapter) availableConnections.getLast();
        }
        catch (NoSuchElementException e) { }
        DSNDatabase.printToConsole("connectionAdapter >>>>"+connectionAdapter);
        return connectionAdapter;
    }
    public synchronized void releaseConnection(Connection connection) throws SQLException
    {
    	//Database.printToConsole("Release Connection " + connection);
        if (connection == null) return;
        try{
        //ConnectionAdapter connectionAdapter = new ConnectionAdapter(connection);
        DSNDatabase.printToConsole("releaseConnection >>>>"+connection);
        DSNConnectionAdapter connectionAdapter =  (DSNConnectionAdapter) contextToConnection.get(connection);	
        // release this connection, keep open if pool size <= to min pool size
        synchronized(this)
        {
            // is the connectionAdapter still in use?
            connectionAdapter.releaseReference();            
            if (!connectionAdapter.inUse())
            {
                // not in use
                //Object context = connectionToContext.get(connection);
                contextToConnection.remove(connection);
                //connectionToContext.remove(connection);
                connectionsInUse--;                
                availableConnections.addLast(connectionAdapter);               
                // let others know of the available connection
                this.notify();
                DSNDatabase.printToConsole("availableConnections.size() >>>>"+availableConnections.size()+" minPoolSize >>>>"+minPoolSize);
                if (availableConnections.size() > minPoolSize)
                {   
                	availableConnections.removeLast();
                	connectionPool.remove(connectionAdapter);
                	//Database.printToConsole("Database Connection is closed for " + connectionAdapter.hashCode());
                    // Pool is big enough
                    // removing connection from pool
                    connectionAdapter.closeAfterDelay();
                }
                else
                {
                    // keeping connection in pool
                }
            }
            else
            {
                // connection still in use
            }
            
        }
        }catch(Exception ex){
        	//Database.printToConsole("Exception in release connection " + ex.getMessage());	
        }
    }
    public String getStatistics()
    {
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);

        // add the connection manager statistics

        //JDK1.3
        try{stringWriter.close();}
        catch(IOException e){}        
        StringBuffer buffer = stringWriter.getBuffer();
        // now add each adapter's statistics
        Iterator iterator = connectionPool.iterator();
        while (iterator.hasNext())
        {
            DSNConnectionAdapter adapter = (DSNConnectionAdapter) iterator.next();
            buffer.append(adapter.getStatistics());
        }
        return buffer.toString();
    }
    
}
