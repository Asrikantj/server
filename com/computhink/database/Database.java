/*
 * Database.java
 *
 * Created on November 10, 2003, 10:49 AM
 */
package com.computhink.database;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.locks.ReentrantLock;
import java.util.prefs.Preferences;

import com.computhink.common.Room;
import com.computhink.vws.server.VWSPreferences;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class Database implements DatabaseConstants 
{
    String name;
    String engineName;
    int connectionCloseDelay = -1;
    int connectionMinPoolSize = -1;
    int connectionMaxPoolSize = -1;
    String jdbcDriverClass;
    String jdbcUrl;
    String jdbcUrlService;
    String jdbcUser;
    String jdbcPassword;
    public String serverIP;
    public int serverPort;
    private int countDBFail =0;
    public int dbFailed = 0;
    public boolean dbConnected = true;    
    public static final ReentrantLock locks = new ReentrantLock();

    private ConnectionManager connectionManager;
    private Connection connection;

    private long        lastActivateDate;
    SQLServerDataSource dataSource = null;
    public Database(Room room, String logger)
    {    	
    	DBLog.setLogger(logger);
    	this.name = room.getName();
    	// Issue No 501 , to pass the IP address and Port from room object to database object
    	this.serverIP = room.getVWS().address;
    	this.serverPort = room.getVWS().comport;
    	// End of Fix
    	this.engineName = room.getDatabaseEngineName();
    	this.connectionCloseDelay = DEFAULT_CONNECTION_CLOSE_DELAY;
    	this.connectionMinPoolSize = room.getMinPoolSize();
    	this.connectionMaxPoolSize = room.getMaxPoolSize();
    	DatabaseEngineProfile ep = new DatabaseEngineProfile(engineName);
    	this.jdbcDriverClass = ep.getJdbcDriverClassName();
    	this.jdbcUrl = ep.createJdbcUrl(
    			room.getDatabaseHost(), 
    			room.getDatabaseName(),
    			room.getDatabasePort());
    	printToConsole("Connection String url in Database.java :"+this.jdbcUrl);
    	this.jdbcUrlService=ep.createJdbcUrlServiceName(
    			room.getDatabaseHost(), 
    			room.getDatabaseName(),
    			room.getDatabasePort());
    	this.jdbcUser = room.getDatabaseUser();
    	this.jdbcPassword = room.getDatabasePassword();
    	this.countDBFail = room.getCountDBFail();
    	if(this.engineName.equalsIgnoreCase("sqlserver")) {
			this.dataSource = ep.createSQLServerDataSource(this.name, room.getDatabaseHost(), room.getDatabasePort(),
					room.getDatabaseName(), this.jdbcUser, this.jdbcPassword);
    	}
    	initialize();
    }
    public Database(String roomName, String dsnName, String userName, String pwd){
    	this.name = roomName;
    	this.engineName = "DSN";
    	this.connectionCloseDelay = DEFAULT_CONNECTION_CLOSE_DELAY;
    	this.connectionMinPoolSize = VWSPreferences.getDSNMinPool();
    	this.connectionMaxPoolSize = VWSPreferences.getDSNMaxPool();
    	DatabaseEngineProfile ep = new DatabaseEngineProfile(engineName);
    	this.jdbcDriverClass = "sun.jdbc.odbc.JdbcOdbcDriver";
    	this.jdbcUrl = ep.createJdbcUrl(dsnName, "", 0);
    	printToConsole("Connection String url in Database.java :"+this.jdbcUrl);
    	this.jdbcUrlService=ep.createJdbcUrlServiceName(dsnName, "", 0);
    	this.jdbcUser = userName;
    	this.jdbcPassword = pwd;
    	//printToConsole("Database constructed " + roomName + " : " + dsnName + " : " + userName + " : " + pwd);
    	initialize();
    }
    public void initialize()
    {
        try
        {
        	//When concurrently loading multiple driver (different database) leads to ViewWise server not starting 
        	//To Avoid that allow to load driver one by one using ReentrantLock
        	
        	//locks.lock();
            Class driverClass = Class.forName(jdbcDriverClass);
            DriverManager.registerDriver((Driver) driverClass.newInstance());
        }
        catch (ClassNotFoundException e)
        {
            DBLog.err("Driver Class not Found '" + jdbcDriverClass + "'");
        }
        catch (InstantiationException e)
        {
            DBLog.err("Driver Class could not be instantiated'" + 
                                                         jdbcDriverClass + "'");
        }
        catch (IllegalAccessException e)
        {
            DBLog.err("Driver Class could not be instantiated'" + 
                                                         jdbcDriverClass + "'");
        }
        catch (SQLException e)
        {
           DBLog.err("Driver Class could not be registered'" + 
                                                          jdbcDriverClass + "'");
        }finally{
        	//locks.unlock();
        }
        connectionManager = new ConnectionManager(this);
    }
    public synchronized Connection aquireConnection() throws SQLException
    {
        return aquireConnection(null);
    }
    public Connection aquireConnection(Object context) throws SQLException
    {
        Connection connection = connectionManager.aquireConnection(context);
        return connection;
    }
    public void releaseConnection() throws SQLException
    {
        if (connection != null && connection.isClosed())
        {
            connection.close();
        }
        connection = null;
    }
    public void closeConnection(Connection connection) throws SQLException
    {
	try{
            if (connection != null)
            {
        	//printToConsole("Release Connection - closeConnection " + connection.isClosed() + " : " + connection.hashCode());
            connectionManager.releaseConnection(connection);
            connection.close();
            }
	}catch(Exception ex){
	    //printToConsole("Error while closing the connection!!! " + ex.getMessage());
	}
        connection = null;
    }
    public void releaseConnection(Connection connection) throws SQLException
    {
    	try{
        connectionManager.releaseConnection(connection);
    	}catch(Exception ex){
    		Database.printToConsole("R1 Err 	" + ex.getMessage() + " ::: "  + connection.hashCode());
    	}
    }
    public String getStatistics()
    {
        return connectionManager.getStatistics();
    }
    public String getName()
    {
        return name;
    }
    public String getEngineName()
    {
        return engineName;
    }
    public int getConnectionCloseDelay()
    {
        return connectionCloseDelay;
    }
    public int getConnectionMinPoolSize()
    {
        return connectionMinPoolSize;
    }
    public int getConnectionMaxPoolSize()
    {
        return connectionMaxPoolSize;
    }
    public String getJdbcDriverClass()
    {
        return jdbcDriverClass;
    }
    public String getJdbcUrl()
    {
        return jdbcUrl;
    }
    public String getJdbcUser()
    {
        return jdbcUser;
    }
    public String getJdbcPassword()
    {
        return jdbcPassword;
    }
	/**
	 * @return Returns the countDBFail.
	 */
	public int getCountDBFail() {
		return countDBFail;
	}
	/**
	 * @param countDBFail The countDBFail to set.
	 */
	public void setCountDBFail(int countDBFail) {
		this.countDBFail = countDBFail;
	}
	/**
	 * @return Returns the dbConnected.
	 */
	public boolean isDbConnected() {
		return dbConnected;
	}
	/**
	 * @param dbConnected The dbConnected to set.
	 */
	public void setDbConnected(boolean dbConnected) {
		this.dbConnected = dbConnected;
	}
	/**
	 * @return Returns the lastActivateDate.
	 */
	public long getLastActivateDate() {
		return lastActivateDate;
	}
	/**
	 * @param lastActivateDate The lastActivateDate to set.
	 */
	public void setLastActivateDate() {
		this.lastActivateDate = System.currentTimeMillis();
	}
	public static void printToConsole(String msg)
	{
		try {
			String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
			msg = now + " : " + msg + " \n";    			
			String filepath = "c:\\VWDB.log";
			File log = new File(filepath);       
			if (!log.exists()) log.createNewFile();
	        FileOutputStream fos = new FileOutputStream(log, true);
	        fos.write(msg.getBytes());
	        fos.close();            
	        fos.close();
	        log = null;  
		} catch (Exception e) {
			System.out.println("printToConsole");
		}
	}
	public synchronized void assertConnection(Connection con)
	{
	    if (con == null) return;
	    try
	    {
		con.commit();                       
		releaseConnection(con);
	    }
	    catch (Exception e)
	    {     
		// Close the Room If Db is not available. To Solve the Issue No 501		
	    }
	}

	public static void main(String[] args) {
	    try{
		Database db = new Database("LDAPNovell", "VWSQLDSN", "sa", "sa");
		System.out.println("Engine Name " + db.getEngineName());
		System.out.println("getJdbcDriverClass " + db.getJdbcDriverClass());
		System.out.println("getJdbcUrl Name " + db.getJdbcUrl());
		for (int i = 0 ; i < 10; i++){
		    Connection con = db.aquireConnection();	    	    
		    //PreparedStatement st = con.prepareStatement("Select 33, EName From Employee Where EId = 2");
		    Statement st = con.createStatement();


		    ResultSet rs = st.executeQuery("Select 33, EName From Employee Where EId = 2" );
		    //ResultSet rs = st.executeQuery("select Name from UserTable");
		    /*	    while(rs.next()){
		System.out.println("Result : " +  rs.getObject(1));
	    }
		     */	    
		    ResultSetMetaData resultMeta = rs.getMetaData();
		    int colCount = resultMeta.getColumnCount();
		    if(rs.next()){
			for (int count =1; count < colCount; count++){
			    int IndexId = rs.getInt(count);
			    String value = rs.getString(++count);			      
			    System.out.println(i + "::: Data " + IndexId + " : " + value);
			}
		    }
		    System.out.println("conn :" + con);
		    //DoDBAction.assertConnection(db, con);
		    db.releaseConnection();
		}
	    }catch(Exception ex){
		System.out.println("Err " + ex.getMessage());
		ex.printStackTrace();

	    }
	}
}