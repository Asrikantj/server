package com.computhink.database;

import java.io.File;
import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.locks.ReentrantLock;

import com.computhink.vws.server.VWSPreferences;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

public class DSNDatabase implements DatabaseConstants {
	String name;
	String engineName;
	String dsnName;
	int connectionCloseDelay = -1;
	int connectionMinPoolSize = -1;
	int connectionMaxPoolSize = -1;
	String jdbcDriverClass;
	String jdbcUrl;
	String jdbcUrlService;
	String jdbcUser;
	String jdbcPassword;
	public String serverIP;
	public int serverPort;
	//private int countDBFail = 0;
	public int dbFailed = 0;
	public boolean dbConnected = true;
	SQLServerDataSource dataSource = null;
	public static final ReentrantLock locks = new ReentrantLock();

	private DSNConnectionManager connectionManager;
	private Connection connection;

	//private long lastActivateDate;
	
	public DSNDatabase(String roomName, String dsnName, String userName, String pwd){
    	this.name = roomName;
    	DSNDatabase.printToConsole("/n/n/nRoomName ------------------------------------------------>"+this.name);
    	this.dsnName = dsnName;
    	DSNDatabase.printToConsole("dsnName ------------------------------------------------>"+dsnName);
    	this.engineName = "DSN";
    	this.connectionCloseDelay = DEFAULT_CONNECTION_CLOSE_DELAY;
    	this.connectionMinPoolSize = VWSPreferences.getDSNMinPool();
    	this.connectionMaxPoolSize = VWSPreferences.getDSNMaxPool();
    	DSNDatabase.printToConsole("Minimum pool size----->: "+connectionMinPoolSize+" connectionMaxPoolSize----->: "+connectionMaxPoolSize);
    	DatabaseEngineProfile ep = new DatabaseEngineProfile(engineName);
    	this.jdbcDriverClass = "sun.jdbc.odbc.JdbcOdbcDriver";
    	this.jdbcUrl = ep.createJdbcUrl(dsnName, "", 0);
    	DSNDatabase.printToConsole("jdbcUrl ------------------------------------------------>"+this.jdbcUrl);
    	this.jdbcUrlService=ep.createJdbcUrlServiceName(dsnName, "", 0);
    	DSNDatabase.printToConsole("jdbcUrlService ------------------------------------------------>"+this.jdbcUrlService);
    	this.jdbcUser = userName;
    	DSNDatabase.printToConsole("jdbcUser ------------------------------------------------>"+this.jdbcUser);
    	this.jdbcPassword = pwd;    
    	initialize();
    }
	
	public void initialize() {
		try {
			Class driverClass = Class.forName(jdbcDriverClass);
			DriverManager.registerDriver((Driver) driverClass.newInstance());
		} catch (ClassNotFoundException e) {
			DBLog.err("Driver Class not Found '" + jdbcDriverClass + "'"+e);
			DBLog.err("Driver Class not Found '" + jdbcDriverClass + "'");
		} catch (InstantiationException e) {
			DBLog.err("Driver Class could not be instantiated'" + jdbcDriverClass + "'");
		} catch (IllegalAccessException e) {
			DBLog.err("Driver Class could not be instantiated'" + jdbcDriverClass + "'");
		} catch (SQLException e) {
			DBLog.err("Driver Class could not be registered'" + jdbcDriverClass + "'");
		} finally {
			// locks.unlock();
		}
		connectionManager = new DSNConnectionManager(this);
	}

	public synchronized Connection aquireConnection() throws SQLException {
		return aquireConnection(null);
	}

	public Connection aquireConnection(Object context) throws SQLException {
		Connection connection = connectionManager.aquireConnection(context);
		return connection;
	}
	public String getEngineName()
    {
        return engineName;
    }
	public void releaseConnection() throws SQLException
    {
        if (connection != null && connection.isClosed())
        {
            connection.close();
        }
        connection = null;
    }
	public void releaseConnection(Connection connection) throws SQLException
    {
    	try{
    		connectionManager.releaseConnection(connection);
    	}catch(Exception ex){
    		printToConsole("R1 Err 	" + ex.getMessage() + " ::: "  + connection.hashCode());
    	}
    }
	
	public static void printToConsole(String msg)
	{
    	try
    	{
        	String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
			msg = now + " : " + msg + " \n";    			
			String filepath = "c:\\VWDB.log";
			File log = new File(filepath);       
			if (!log.exists()) log.createNewFile();
	        FileOutputStream fos = new FileOutputStream(log, true);
	        fos.write(msg.getBytes());
	        fos.close();            
	        fos.close();
	        log = null;    
    	}catch (Exception e)
        {
        	System.out.println( "printToConsole");	
        }
	}

	public String getDsnName() {
		return dsnName;
	}

	public void setDsnName(String dsnName) {
		this.dsnName = dsnName;
	}
}
