/*
                      Copyright (c) 1997-2003
                         Computhink Software

                      All rights reserved.
*/
package com.computhink.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import com.computhink.vws.server.VWSPreferences;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
/**
 * DatabaseEngineProfile<br>
 *
 * @version     $Revision: 1.5 $
 * @author      <a href="mailto:Fadish@bigfoot.com">Fadi Shehadeh</a>
**/

public class DatabaseEngineProfile implements DatabaseConstants
{
    private long attributesSupported = 0x0000000000000000;
    private HashMap keywords = new HashMap();
    private HashMap errors = new HashMap();
    private String databaseEngineName = "";
    
    public DatabaseEngineProfile(Database database)
    {
        new DatabaseEngineProfile(database.getEngineName());
    }
    public DatabaseEngineProfile(String engineName)
    {
        databaseEngineName=engineName;
        if(databaseEngineName.equalsIgnoreCase("oracle"))
        {
            addSupportedAttribute(PRIMARY_KEY, "PRIMARY KEY");
            addSupportedAttribute(FOREIGN_KEY, "");
            addSupportedAttribute(VARCHAR_LENGTH);
            addSupportedAttribute(UPPER_FUNCTION);
        }
        else if(databaseEngineName.equalsIgnoreCase("sqlserver"))
        {
            addSupportedAttribute(PRIMARY_KEY, "PRIMARY KEY");
            addSupportedAttribute(FOREIGN_KEY);
            addSupportedAttribute(NULL, "NULL");
            addSupportedAttribute(VARCHAR_LENGTH);
            addSupportedAttribute(UPPER_FUNCTION);
            addSupportedAttribute(DROP_COLUMN);
        }
        else if(databaseEngineName.equalsIgnoreCase("DSN"))
        {
            addSupportedAttribute(PRIMARY_KEY, "PRIMARY KEY");
            addSupportedAttribute(FOREIGN_KEY);
            addSupportedAttribute(NULL, "NULL");
            addSupportedAttribute(VARCHAR_LENGTH);
            addSupportedAttribute(UPPER_FUNCTION);
            addSupportedAttribute(DROP_COLUMN);
        }
    }
    public final void addSupportedAttribute(long attribute, String keyword)
    {
        attributesSupported |= attribute;
        addKeyword(attribute, keyword);
    }
    public String getJdbcDriverClassName()
    {
        if(databaseEngineName.equalsIgnoreCase("Oracle"))
            return "oracle.jdbc.driver.OracleDriver";
        else if(databaseEngineName.equalsIgnoreCase("SQLServer")){
        	if (VWSPreferences.isCheckSQL2005()){
        		return "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        	}
        	else{
        		return "com.microsoft.jdbc.sqlserver.SQLServerDriver";
        	}
        }else if (databaseEngineName.equalsIgnoreCase("DSN")){
            return "sun.jdbc.odbc.JdbcOdbcDriver";
        }
        return "";
    }
    public String createJdbcUrl(String host, String databaseName, int port)
    {
    	if(databaseEngineName.equalsIgnoreCase("oracle"))   
    	{
    		return "jdbc:oracle:thin:@" + host + ":" + port + ":" + databaseName;
    	}
    	else if(databaseEngineName.equalsIgnoreCase("sqlserver"))
    	{   
			if (VWSPreferences.isCheckSQL2005()) {
				return "jdbc:sqlserver://" + host + ":" + port + ";DatabaseName=" + databaseName
						+ ";SelectMethod=direct;";
			} else {
				return "jdbc:microsoft:sqlserver://" + host + ":" + port + ";DatabaseName=" + databaseName
						+ ";SelectMethod=Cursor;";
			}
    	}
    	else if(databaseEngineName.equalsIgnoreCase("DSN"))   
    	{            
    		return "jdbc:odbc:" + host;
    	}
    	return "";
    }
    public final void addSupportedAttribute(long attribute)
    {
        attributesSupported |= attribute;
        addKeyword(attribute, "");
    }
    public final void setAttributesSupported(long attributes)
    {
        attributesSupported = attributes;
    }
    public final Iterator getAttributesSupported()
    {
        return keywords.keySet().iterator();
    }
    public final Iterator getKeywords()
    {
        ArrayList keywords = new ArrayList();
        Iterator iterator = this.keywords.values().iterator();
        while (iterator.hasNext())
        {
            String value = (String) iterator.next();

            if (value.length() > 0)
            {
                keywords.add(value);
            }
        }
        return keywords.iterator();
    }
    public final boolean supports(long attributes)
    {
        return ((attributes & attributesSupported) != 0);
    }
    private final void addKeyword(long attribute, String keyword)
    {
        keywords.put(new Long(attribute), keyword);
    }
    public final String getKeyword(long attribute)
    {
        return (String)keywords.get(new Long(attribute));
    }
    public final void addError(int errorCode, String error)
    {
        errors.put(new Integer(errorCode), error);
    }
    public final String getError(int errorCode)
    {
        return (String)errors.get(new Integer(errorCode));
    }
    public String createJdbcUrlServiceName(String host, String databaseName, int port)
    {
    	if(databaseEngineName.equalsIgnoreCase("oracle"))   
    	{
    		return "jdbc:oracle:thin:@//" + host + ":" + port + "/" + databaseName;
    	}
    	return "";
    }
    
    /**
     * This method is used to create datasource object to connect with SQL DB over SQL/Windows authentication
     * @param roomName
     * @param dbHost
     * @param dbPort
     * @param databaseName
     * @param dbUser
     * @param dbPassword
     * @return
     */
	public SQLServerDataSource createSQLServerDataSource(String roomName, String dbHost, int dbPort, String databaseName, String dbUser, String dbPassword) {
		SQLServerDataSource dataSource = null;
		String sqlConnectionString = VWSPreferences.getSQLConnectionString(roomName);
		String authenticationMode = VWSPreferences.getAuthenticationMode(roomName);
		try {
			Database.printToConsole("sqlConnectionString :"+sqlConnectionString);
			Database.printToConsole("authenticationMode :"+authenticationMode);
    		if (sqlConnectionString.trim().length() == 0 && authenticationMode != null && authenticationMode.equalsIgnoreCase("windows")) {
    			Database.printToConsole("inside SQLServerDataSource object creation.....");
    			dataSource = new SQLServerDataSource();
    			dataSource.setServerName(dbHost);
    			dataSource.setPortNumber(dbPort);
				boolean integratedSecurity = VWSPreferences.getIntegratedSecurity(roomName);
				dataSource.setIntegratedSecurity(integratedSecurity);
				dataSource.setAuthenticationScheme(VWSPreferences.getAuthenticationScheme(roomName));
				dataSource.setDomain(VWSPreferences.getDomain(roomName));
				dataSource.setUser(dbUser);
				dataSource.setPassword(dbPassword);
				dataSource.setDatabaseName(databaseName);
				dataSource.setServerSpn(VWSPreferences.getServerSpn(roomName));
    		} 
		} catch (Exception e) {
			Database.printToConsole("Exception while creating the SQLServerDataSource :"+e);
		}
		return dataSource;
    }
}