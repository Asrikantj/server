package com.computhink.database;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DSNConnectionAdapter implements DatabaseConstants {
	private DSNDatabase database;
    private Connection sqlConnection;
    private int referenceCount;
    {
        database = null;
        sqlConnection = null;
        referenceCount = 0;
    }
    DSNConnectionAdapter(DSNDatabase database)
    {
        this.database = database;
    }
    synchronized void ensureOpen() throws SQLException
    {
        ensureConnectionIsOpen();
    }
    private void ensureConnectionIsOpen() throws SQLException
    {
    	DSNDatabase.printToConsole("ensureConnectionIsOpen >>> "+sqlConnection);
        if (sqlConnection != null && !sqlConnection.isClosed())
        {
            // already open and ready to be used;
            return;
        }
        sqlConnection = getConnectionFromDriverManager();
    }
    private Connection getConnectionFromDriverManager() throws SQLException
    {
        String url = database.jdbcUrl;
        String user = database.jdbcUser;
        String password = database.jdbcPassword;
        String jdbcUrlService=database.jdbcUrlService;
        String engine=database.engineName;
        DSNDatabase.printToConsole("url in getConnectionFromDriverManager >>>"+url);
        DSNDatabase.printToConsole("user in getConnectionFromDriverManager >>>"+user);
        DSNDatabase.printToConsole("password in getConnectionFromDriverManager >>>"+password);
        DSNDatabase.printToConsole("jdbcUrlService in getConnectionFromDriverManager >>>"+jdbcUrlService);
        DSNDatabase.printToConsole("engine in getConnectionFromDriverManager >>>"+engine);
        Connection connection =null;
        /**Following line is commented for DBLookup dsn connection failed customer issue.Commented by Vanitha on 13/01/2019**/
        //DriverManager.setLoginTimeout(10);
        if (user == null || user.length() == 0)
        {
			if (engine.equalsIgnoreCase("oracle")) {
				try {
					connection = DriverManager.getConnection(jdbcUrlService);
				} catch (Exception e) {
					try {
						connection = DriverManager.getConnection(url);
					} catch (Exception exp) {
						throw exp;
					}
				}
			} else {
				connection = DriverManager.getConnection(url);
			}
        } else {
			if (engine.equalsIgnoreCase("oracle")) {
				try {
					connection = DriverManager.getConnection(jdbcUrlService, user, password);
				} catch (Exception e) {
					try {
						connection = DriverManager.getConnection(url, user, password);
					} catch (Exception exp) {
						throw exp;
					}
				}
			} else {
				connection = DriverManager.getConnection(url, user, password);
			}
		}
        if (!database.getEngineName().equalsIgnoreCase("DSN"))
            connection.setAutoCommit(false);

        // make sure we get no Dirty, non-repeatable or phantom reads
        if (connection.getMetaData().supportsTransactionIsolationLevel(
                connection.TRANSACTION_READ_UNCOMMITTED))
        {
            connection.setTransactionIsolation(
                connection.TRANSACTION_READ_UNCOMMITTED);
        }
        return connection;
    }
    int aquireReference()
    {
        return ++referenceCount;
    }
    public Connection getConnection()
    {
    	DSNDatabase.printToConsole("sqlConnection in getConnection method :"+sqlConnection);
        return sqlConnection;
    }
    int releaseReference()
    {
        return --referenceCount;
    }
    boolean inUse()
    {
        return referenceCount > 0;
    }
    void closeAfterDelay() throws SQLException
    {
        // ensure transaction was closed
        sqlConnection.rollback();
        // make sure the connection is still not being used
        if (inUse()) return;
        // throw away our reference to the timer
        try
        {
            closeNow();
            //if (LOG_VERBOSE_DEBUG) Log.verboseDebug("Closed Connection");
        }
        catch (SQLException e)
        {
            //if (LOG_DEBUG) Log.SQLException(DEBUG_TYPE, "unable to close connection", e);
        }
    }
    private void closeNow() throws SQLException
    {
    	DSNDatabase.printToConsole("sqlConnection.isClosed() >>>>"+sqlConnection.isClosed());
        if (!sqlConnection.isClosed())
        {
            // ensure last transaction has completed
            sqlConnection.commit();
            sqlConnection.close();
        }
        sqlConnection = null;
        DSNDatabase.printToConsole("Connection released.....");
    }
    public String getStatistics()
    {
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);
        try {
        	stringWriter.close();
        } catch(IOException e) {
        	
        }
        return stringWriter.toString();
    }
}