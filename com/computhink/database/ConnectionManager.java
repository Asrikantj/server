/*
                      Copyright (c) 1997-2003
                         Computhink Software

                      All rights reserved.
*/

package com.computhink.database;
/**
 * ConnectionManager manages connections to the database.<br>
 * This is a singleton.
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@bigfoot.com">Fadi Shehadeh</a>
**/
import java.io.*;
import java.sql.*;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;

public final class ConnectionManager implements DatabaseConstants
{
    private Database database;
    private int minPoolSize;
    private int maxPoolSize;
    private int connectionsInUse;

    private LinkedList connectionPool;
    private LinkedList availableConnections;
    private HashMap contextToConnection;
    private HashMap connectionToContext;

    // statistics
    private int maxConnectionsUsed;
    private int numberOfNewContextConnections;
    private int numberOfSameContextConnections;
    private int numberOfConnectionWaits;
    {
        minPoolSize = 0;
        maxPoolSize = 1;
        connectionsInUse = 0;
        connectionPool = new LinkedList();
        availableConnections = new LinkedList();
        contextToConnection = new HashMap();
        connectionToContext = new HashMap();
        maxConnectionsUsed = 0;
        numberOfNewContextConnections = 0;
        numberOfSameContextConnections = 0;
        numberOfConnectionWaits = 0;
    }

    ConnectionManager(Database database)
    {
        this.database = database;
        maxPoolSize = database.connectionMaxPoolSize;
        minPoolSize = database.connectionMinPoolSize;

        if (maxPoolSize < 0)
        {
            maxPoolSize = DEFAULT_CONNECTION_MAX_POOL_SIZE;
        }

        if (minPoolSize < 0)
        {
            minPoolSize = DEFAULT_CONNECTION_MIN_POOL_SIZE;
        }

        for (int i=0; i < minPoolSize; i++)
        {
            ConnectionAdapter connectionAdapter = 
                                                new ConnectionAdapter(database);
            connectionPool.add(connectionAdapter);
            availableConnections.add(connectionAdapter);            
        }
    }
    public synchronized Connection aquireConnection() throws  SQLException
    {
        return aquireConnection(null);
    }
    public synchronized Connection aquireConnection(Object context) throws  SQLException
    {
    	/*try{*/
        ConnectionAdapter connectionAdapter = null;
        /*if (context == null)
        {
            context = Thread.currentThread();
            Database.printToConsole("Thread Object Id " + Thread.currentThread().hashCode());
        }*/
        if (availableConnections.size() > 0){
        	connectionAdapter = (ConnectionAdapter) availableConnections.getLast();
        	if (connectionAdapter != null)
        	availableConnections.removeLast();          	
        }
//       connectionAdapter = (ConnectionAdapter) contextToConnection.get(context);
        if (connectionAdapter == null)
        {
            synchronized(this)
            {
                while (connectionAdapter == null)
                {
                    connectionAdapter = findAvailableConnectionAdapter(context);
                    if (connectionAdapter == null)
                    {
                        try
                        {
                            //Not enough connections - waiting for a release
                            numberOfConnectionWaits++;
                            //Database.printToConsole("Waiting for Connections....");
                            this.wait();
                            //Notified - connection maybe available
                        }
                        catch (InterruptedException e)
                        {
                        }
                        continue;
                    }
                }
                // increment the new context count
                numberOfNewContextConnections++;
                // Aquired a connection
                //contextToConnection.put(context, connectionAdapter);
                //connectionToContext.put(connectionAdapter, context);
                availableConnections.remove(connectionAdapter);
                //connectionsInUse++;
                if (connectionsInUse > maxConnectionsUsed)
                {
                    maxConnectionsUsed = connectionsInUse;
                }
            }
        }
        else
        {
            // increment same context count
            numberOfSameContextConnections++;
        }
  
        // open the connection if necessary
        connectionAdapter.ensureOpen();
        // increment reference count
        connectionAdapter.aquireReference();
        /*Database.printToConsole("Aquire -  ConnectionAdapter Hashcode " + connectionAdapter.hashCode());
        Database.printToConsole("Aquire -  Connection Hashcode " + connectionAdapter.getConnection().hashCode());
        Database.printToConsole("Aquire -  Reference Count " + connectionAdapter.getReferenceCount());
        Database.printToConsole("Aquire -  Connection in Use " + connectionsInUse);
        Database.printToConsole("Aquire -  numberOfSameContextConnections " + numberOfSameContextConnections);
        Database.printToConsole("Aquire -  numberOfNewContextConnections " + numberOfNewContextConnections);
        Database.printToConsole("Aquire -  AvailableConnections Size " + availableConnections.size());
        Database.printToConsole("Aquire -  ConnectionPool Size " + connectionPool.size());*/
        //Adding the connection adapter which is aquired by last request.
        contextToConnection.put(connectionAdapter.getConnection(), connectionAdapter);
        connectionsInUse++;
        return connectionAdapter.getConnection();
/*    	}catch(Exception ex){
    		//Database.printToConsole("Aquire Exception " + ex.getMessage() + " :::: " +  ex.getLocalizedMessage());	
    		return null;
    	}*/
    }

    private ConnectionAdapter findAvailableConnectionAdapter(Object context)
    {
        // add another connection to the pool if necessary
        if (availableConnections.size() == 0)
        {
            if (connectionPool.size() < maxPoolSize)
            {
                // Adding an Adapter to the pool
                // create another connection adapter
                ConnectionAdapter connectionAdapter = 
                                                new ConnectionAdapter(database);
                connectionPool.add(connectionAdapter);
                availableConnections.add(connectionAdapter);
            }
            else
            {
                //Pool size is at max
            }
        }

        // note: there may not be any available
        ConnectionAdapter connectionAdapter = null;
        try
        {
            connectionAdapter = (ConnectionAdapter) 
                                                 availableConnections.getLast();
        }
        catch (NoSuchElementException e) { }
        return connectionAdapter;
    }
    public synchronized void releaseConnection(Connection connection) throws SQLException
    {
    	//Database.printToConsole("Release Connection " + connection);
        if (connection == null) return;
        try{
        //ConnectionAdapter connectionAdapter = new ConnectionAdapter(connection);
        ConnectionAdapter connectionAdapter =  (ConnectionAdapter) contextToConnection.get(connection);	
        // release this connection, keep open if pool size <= to min pool size
        synchronized(this)
        {
            // is the connectionAdapter still in use?
            connectionAdapter.releaseReference();            
            if (!connectionAdapter.inUse())
            {
                // not in use
                //Object context = connectionToContext.get(connection);
                contextToConnection.remove(connection);
                //connectionToContext.remove(connection);
                connectionsInUse--;                
                availableConnections.addLast(connectionAdapter);               
                // let others know of the available connection
                this.notify();
                if (availableConnections.size() > minPoolSize)
                {   
                	availableConnections.removeLast();
                	connectionPool.remove(connectionAdapter);
                	//Database.printToConsole("Database Connection is closed for " + connectionAdapter.hashCode());
                    // Pool is big enough
                    // removing connection from pool
                    connectionAdapter.closeAfterDelay();
                }
                else
                {
                    // keeping connection in pool
                }
            }
            else
            {
                // connection still in use
            }
            
        }
        
        /*Database.printToConsole("Release ConnectionAdapter Hashcode " + connectionAdapter.hashCode());
        Database.printToConsole("Release Connection Hashcode " + connectionAdapter.getConnection().hashCode());
        Database.printToConsole("Release Reference " + connectionAdapter.getReferenceCount());        
        Database.printToConsole("Release Connection in Use " + connectionsInUse);
        Database.printToConsole("Release AvailableConnections Size " + availableConnections.size());*/
        }catch(Exception ex){
        	//Database.printToConsole("Exception in release connection " + ex.getMessage());	
        }
    }
    public String getStatistics()
    {
        StringWriter stringWriter = new StringWriter();
        PrintWriter writer = new PrintWriter(stringWriter);

        // add the connection manager statistics
        /*
        writer.println();
        writer.println("Connection Manager Statistics");
        writer.println("Max Connections Used\t\t-> " + maxConnectionsUsed);
        writer.println("Current Connections In Use\t-> " + connectionsInUse);
        writer.println("Connection requests\t\t-> " +
                       (numberOfNewContextConnections+
                        numberOfSameContextConnections));
        writer.println("New Context Connections\t\t-> " +
                       numberOfNewContextConnections);
        writer.println("Same Context Connections\t-> " +
                       numberOfSameContextConnections);
        writer.println("Number of Connection Waits\t-> " +
                       numberOfConnectionWaits);
         */

        //JDK1.3
        try{stringWriter.close();}
        catch(IOException e){}        
        StringBuffer buffer = stringWriter.getBuffer();
        // now add each adapter's statistics
        Iterator iterator = connectionPool.iterator();
        while (iterator.hasNext())
        {
            ConnectionAdapter adapter = (ConnectionAdapter) iterator.next();
            buffer.append(adapter.getStatistics());
        }
        return buffer.toString();
    }
}