/*
 * DBLog.java
 *
 * Created on December 7, 2003, 4:52 PM
 */

package com.computhink.database;
/**
 *
 * @author  Administrator
 */
import java.lang.reflect.Method;

public class DBLog 
{
    private static Class Log = null;
    private static Method add = null;
    private static Method err = null;
    
    public static void setLogger(String logger)
    {
        if (add != null) return;
        try
        {
            Class[] stringClass = {Class.forName("java.lang.String")};
            Log = Class.forName(logger);
            add = Log.getMethod("add", stringClass);
            err = Log.getMethod("err", stringClass);
        }
        catch (Exception e)
        {
        }
    }
    public static void add(String msg)
    {
        try
        {
            String[] msgs = {msg};
            add.invoke(null, (Object[])msgs);
        }
        catch (Exception e)
        {
        }
    }
    public static void err(String msg)
    {
        try
        {
            String[] msgs = {msg};
            err.invoke(null,(Object[])msgs);
        }
        catch (Exception e)
        {
        }
    }
    
}
