/*
                      Copyright (c) 1997-2003
                         Computhink Software

                      All rights reserved.
*/

package com.computhink.database;

/**
 * DatabaseConstants for this package<br>
 *
 * @version     $Revision: 1.1 $
  * @author      <a href="mailto:Fadish@bigfoot.com">Fadi Shehadeh</a>
**/
public interface DatabaseConstants
{
    static final boolean LOG_STATISTICS = false;
    static final boolean DATABASE_LOG_VERBOSE_DEBUG = false;

    public static final String DATABASE_PREFIX = "com.vws.computhink.database";
    public static final String DATABASE_DB_PREFIX = DATABASE_PREFIX+".db";

    public static final String LOG_DRIVER = DATABASE_PREFIX + ".log.driver";
    public static final String DEFAULT = DATABASE_PREFIX + ".default";

    public static final long VARCHAR_LENGTH     = 0x1000000000000000L;
    public static final long UPPER_FUNCTION     = 0x2000000000000000L;
    public static final long DROP_COLUMN        = 0x4000000000000000L;

    // These need DATABASE_DB_PREFIX prepended
    public static final String ENGINE_NAME =
        ".engine.name"; // used to map an engine profile
    public static final String CONNECTION_MIN_POOL_SIZE =
        ".connection.min.pool.size";
    public static final String CONNECTION_MAX_POOL_SIZE =
        ".connection.max.pool.size";

    public static final String JDBC_DRIVER_CLASS =
        ".jdbc.driver.class";
    public static final String JDBC_URL =
        ".jdbc.url";
    public static final String JDBC_USER =
        ".jdbc.user";
    public static final String JDBC_PASSWORD =
        ".jdbc.password";
    public static final String JDBC_ADD_FOREIGN_KEYS =
        ".add.foreign.keys";
    public static final String JDBC_ADD_CONSTRAINTS =
        ".add.constraints";

    public static final String DATE_FORMAT =
        ".date.format";
    public static final String DATE_FORMAT_LENGTH =
        ".date.format.length";
    public static final String TIME_ZONE =
        ".time.zone";

    // default values
    public static final boolean DEFAULT_LOG_DRIVER = false;

    public static final String DEFAULT_ENGINE_NAME = "";
    public static final int DEFAULT_CONNECTION_CLOSE_DELAY = 1000;
    public static final int DEFAULT_CONNECTION_MIN_POOL_SIZE = 5;
    public static final int DEFAULT_CONNECTION_MAX_POOL_SIZE = 8;

    public static final String DEFAULT_JDBC_DRIVER_CLASS =
        "sun.jdbc.odbc.JdbcOdbcDriver";
    public static final String DEFAULT_JDBC_USER = null;
    public static final String DEFAULT_JDBC_PASSWORD = null;

    public static final boolean DEFAULT_ADD_FOREIGN_KEYS = false;
    public static final boolean DEFAULT_ADD_CONSTRAINTS = false;

    public static final String DEFAULT_DATE_FORMAT =
        "yyyy-MM-dd'T'HH:mm:ss"; // conforms to ISO 8601
    public static final int DEFAULT_DATE_FORMAT_LENGTH =
        DEFAULT_DATE_FORMAT.length() - 2; // minus the ticks
    public static final String DEFAULT_TIME_ZONE = "GMT";
    
    public static final long DEFAULT_ATTRIBUTES = 0x0000000000000000L;
    public static final long PRIMARY_KEY        = 0x0000000000000001L;
    public static final long FOREIGN_KEY        = 0x0000000000000002L;

    public static final long AUTOINCREMENT      = 0x0000000000000000L;
    public static final long NULL               = 0x0000000000010000L;
    public static final long NOT_NULL           = 0x0000000000020000L;
}
