/*
 * SQLQueryParser.java
 *
 * Created on March 17, 2002, 12:17 AM
 */
package com.computhink.database;
/**
 *
 * @author  Fadi
 * @version
 */
import java.util.*;


public class SQLQueryParser
{
    private static final String D_QUOTES=String.valueOf('"');
    private static final String[] QUERY_WORDS={"AND","OR","NOT","NEAR" };

    public static void parseSQLQuery(String query,boolean matchSimilar)
    {
        if(query.equals("") || query.length()==1) return;
        boolean queryError = false;
        String finalQuery = "";
        String element;
        String previousElement;
        String nextElement;
        
        String [] queryElementsArray=getQueryElementsArray(query);
        queryError = true;
        if (queryElementsArray != null) 
        {
            queryError=false;
            if (isQueryWord(queryElementsArray[0]))  
            {
                queryError=true;
            }
            else if (queryElementsArray.length>0)
            {
                if (isQueryWord(queryElementsArray[queryElementsArray.length-1]) )
                {
                    queryError=true;
                }
            }
        }
        if (!queryError)
        {
            for (int i=0;i<queryElementsArray.length;i++)
            {
                element=queryElementsArray[i];
                previousElement="";
                nextElement="";
                if (queryElementsArray.length>0 & i < queryElementsArray.length-1)
                    nextElement=queryElementsArray[i+1];
                if (i>0) previousElement=queryElementsArray[i-1];
                if (isQueryWord(element))
                {
                    if (isQueryWord(nextElement))
                    {
                        if (!(element.equalsIgnoreCase("and") & nextElement.equalsIgnoreCase("not")))
                        {
                            queryError=true;
                            break;
                        }
                    }
                    if (element.equalsIgnoreCase("not") & !isQueryWord(previousElement) )
                    {
                        queryError=true;
                        break;
                    }
                }
                if (matchSimilar & !isQueryWord(element))
                {
                    String parenthesis="";
                    int j=0;
                    if (element.startsWith("(") & !element.endsWith(")"))
                    {
                        while (element.substring(j).startsWith("("))
                        {
                            parenthesis=parenthesis+"(";
                            queryError=!queryError;
                            j++;
                        }
                        element=parenthesis+"FORMSOF(INFLECTIONAL,"+element.substring(j)+")";
                    }
                    else if (element.endsWith(")") & !element.startsWith("("))
                    {
                        j=element.length();
                        while (element.substring(0,j).endsWith(")"))
                        {
                            parenthesis=parenthesis + ")";
                            queryError=!queryError;
                            j--;
                        }
                        element="FORMSOF(INFLECTIONAL,"+element.substring(0,j)+")"+parenthesis;
                    }
                    else
                    {
                        element="FORMSOF(INFLECTIONAL,"+element+")";
                    }
                }
                finalQuery=finalQuery+element;
                if (nextElement=="") break;
                finalQuery=finalQuery+((isQueryWord(element)|isQueryWord(nextElement))?" ":" AND ");
            }
        }    
        if (queryError) 
            query = "";
        else
            query = finalQuery;
    }
//------------------------------------------------------------------------------
    private static String[] getQueryElementsArray(String str)
    {
        Vector vector=new Vector();
        String element="";
        String nextLetter="";
        boolean openQuotes=false;
        char[] letter=str.toCharArray();
        for (int i=0;i<letter.length;i++)
        {
            if (i<letter.length-1) nextLetter=Integer.toString(letter[i]);
            if (nextLetter.equals(D_QUOTES))
            {
                openQuotes=!openQuotes;
                element=element+letter[i];
                if (openQuotes|nextLetter!=" ") continue;
            }
            else if (String.valueOf(letter[i]).equals(" "))
            {
                if (openQuotes) 
                {
                    element=element+letter[i];
                    continue;
                } 
            }
            else
            {
                element=element+letter[i];
                continue;
            }
            if (element.length()>0) vector.add(element.trim());
            element="";
        }
        if (element.length()>0) vector.add(element.trim());
        if (openQuotes) return null;
        String[] ret=new String[vector.size()];
        vector.copyInto(ret);
        return ret;
    }
//------------------------------------------------------------------------------    
    /*
    private static boolean isValidWord(String word)
    {
        java.util.regex.Pattern pattern=java.util.regex.Pattern.compile("[a-zA-Z_0-9]");
        java.util.regex.Matcher matcher =null;
        char[] chars=word.toCharArray();
        for (int i=0;i<chars.length;i++)
        {
            matcher=pattern.matcher(Integer.toString(chars[i]));
            if (matcher.matches()) return true;
        } 
        return false;
    }
     **/
//------------------------------------------------------------------------------
    private static boolean isQueryWord(String word)
    {
        boolean ret = false;
        for (int i=0;i<QUERY_WORDS.length; i++)
        {
            ret = ret | word.equalsIgnoreCase(QUERY_WORDS[i]);
            if (ret) break;
        }
        return ret;
    }
//------------------------------------------------------------------------------
    public static void parseOracleQuery(String query, boolean matchSimilar)
    {
        if(query.equals("") || query.length()==1) return;
        boolean queryError=false;
        String finalQuery="";
        String element;
        String previousElement;
        String nextElement;
        String [] queryElementsArray=getQueryElementsArray(query);
        queryError=true;
        if (queryElementsArray!=null) 
        {
            queryError=false;
            if (isQueryWord(queryElementsArray[0]))  
            {
                queryError=true;
            }
            else if (queryElementsArray.length > 0)
            {
                if (isQueryWord(queryElementsArray[queryElementsArray.length-1]) )
                {
                    queryError=true;
                }
            }
        }
        if (!queryError)
        {
            for (int i=0;i<queryElementsArray.length;i++)
            {
                element=queryElementsArray[i];
                previousElement = "";
                nextElement = "";
                if (queryElementsArray.length>0 & i<queryElementsArray.length-1)
                               nextElement=queryElementsArray[i+1];
                if (i>0)previousElement=queryElementsArray[i-1];
                if (isQueryWord(element))
                {
                    if (isQueryWord(nextElement))
                    {
                        if (!(element.equalsIgnoreCase("and") & nextElement.equalsIgnoreCase("not")))
                        {
                            queryError=true;
                            break;
                        }
                    }
                    if ( element.equalsIgnoreCase("not") & !isQueryWord(previousElement) )
                    {
                        queryError=true;
                        break;
                    }
                }
                if (matchSimilar & !isQueryWord(element))
                {
                    String parenthesis = "";
                    int j = 0;
                    if (element.startsWith("(") & !element.endsWith(")"))
                    {
                        while(element.substring(j).startsWith("("))
                        {
                            parenthesis=parenthesis+"(";
                            queryError=!queryError;
                            j++;
                        }
                        element=parenthesis+"About("+element.substring(j)+")";
                    }
                    else if (element.endsWith(")") & !element.startsWith("("))
                    {
                        j=element.length();
                        while (element.substring(0,j).endsWith(")"))
                        {
                            parenthesis=parenthesis + ")";
                            queryError=!queryError;
                            j--;
                        }
                        element="About("+element.substring(0,j)+")"+parenthesis;
                    }
                    else
                    {
                        element="About("+element+")";
                    }
                }
                finalQuery=finalQuery+element;
                if (nextElement=="") break;
                finalQuery=finalQuery+((isQueryWord(element)|isQueryWord(nextElement))?" ":" & ");
            }
        }    
        if (queryError)
            query = "";
        else
            query = finalQuery;
    }
//------------------------------------------------------------------------------
}