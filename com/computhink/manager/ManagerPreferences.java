/*
 * DSSPreferences.java
 *
 * Created on November 8, 2003, 12:13 PM
 */

package com.computhink.manager;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import com.computhink.ars.server.ARSConstants;
import com.computhink.common.Constants;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.drs.server.DRSConstants;
import com.computhink.vns.server.VNSConstants;
import com.computhink.vws.csserver.CSSConstants;

public class ManagerPreferences implements ManagerConstants
{
    private static Preferences ManagerPref;
    
    static
    {
        ManagerPref = Preferences.systemRoot().node(MANAGER_PREF_ROOT);
    }
    private static Preferences getNode(String node)
    {
        return ManagerPref.node(node);
    }
    public static void setProxy(boolean b)
    {
        getNode("proxy").putBoolean("enabled", b);
        flush();
    }
    public static void setProxyHost(String server)
    {
        getNode("proxy").put("host", server);
        flush();
    }
    public static void setProxyPort(int port)
    {
        getNode("proxy").putInt("port", port);
        flush();
    }
    public static boolean getProxy()
    {
        return getNode("proxy").getBoolean("enabled", false);
    }
    public static String getProxyHost()
    {
        return getNode("proxy").get("host", "");
    }
    public static int getProxyPort()
    {
        return getNode("proxy").getInt("port", 0);
    }
    public static void setActiveServer(String server)
    {
        getNode("active").put("name", server);
        flush();
    }
    public static ServerSchema getActiveServer(boolean detailed)
    {
        String sname = getNode("active").get("name", "");
        if (detailed)
            return getServer(sname);
        ServerSchema ss = new ServerSchema();
        ss.name = sname;
        return ss;
    }
    public static boolean isActiveServer(String server)
    {
        if (getNode("active").get("name", "").equals(server)) return true;
        return false;
    }
    public static void addServer(String server) 
    {
        StringTokenizer st =  new StringTokenizer(server, "|");
        Preferences serverPref  = getNode("servers/" + st.nextToken());
        serverPref.put("address", st.nextToken());
        serverPref.put("port", st.nextToken());
        serverPref.put("type", st.nextToken());
        flush();
    }
    public static void removeServer(String server) 
    {
        StringTokenizer st =  new StringTokenizer(server, "|") ;
        Preferences serverPref  = getNode("servers/" + st.nextToken());
        try
        {
            serverPref.removeNode() ;
        }
        catch(BackingStoreException e){}
    }
    public static void removeActiveServer(String server) 
    {
        Preferences activeServer  = getNode("active");
        try
        {
            activeServer.removeNode() ;
        }
        catch(BackingStoreException e){}
    }
    public static void setManager(String sname, String man, String pas)
    {
        pas = Util.encryptKey(pas);
        Preferences serverPref = getNode("servers/" + sname);
        serverPref.put("manager", man);
        serverPref.put("manpass", pas);
        flush();
    }
    public static String getManager(String sname)
    {
        Preferences serverPref = getNode("servers/" + sname);
        String man = serverPref.get("manager", "");
        String pas = serverPref.get("manpass", "");
        if (!pas.equals("manager")) pas = Util.decryptKey(pas);
        return man + "||" + pas;
    }
    /* Purpose:	Master Switch off added ARS in registry
    * Created By: C.Shanmugavalli		Date:	20 Sep 2006
    */
    public static String getARSNode()
    {
    	String temp = getNode("ars").get("arsenable","");
    	return temp;
    }
    
    public static void setARSEnable(String  arsenable)
    {
         Preferences genPref = getNode("ars"); 
         genPref.put("arsenable",arsenable);
    }
    public static String getDRSNode()
    {
    	String temp = getNode("dws").get("dwsenable","");
    	return temp;
    }
    
    public static void setDRSEnable(String  drsenable)
    {
         Preferences genPref = getNode("dws"); 
         genPref.put("dwsenable",drsenable);
    }
    public static String getARSEnable()
    {
    	String arsenable = getNode("ars").get("arsenable", "");
    	/*ARS enable will be always true regardless of value set in registry as per request. C.Shanmugavalli, 10 Nov 2006*/
    	arsenable = "true";
    	if(arsenable!=null)
    		return arsenable;
    	else
    		return new String();
    }
    // ARS Master switch end
    // DRS master switch off added 20 Dec @006 Valli
    public static String getDRSEnable()
    {
    	String drsenable = getNode("dws").get("dwsenable", "");
        	if(drsenable!=null)
    		return drsenable;
    	else
    		return new String();
    }
    
    public static String getVNSEnable(){
    	String vnsenable = getNode("vns").get("vnsenable", "");
    	vnsenable = "true";
    	if(vnsenable!=null)
    		return vnsenable;
    	else
    		return new String();
    }
    
    
    public static String getCSSEnable(){
    	String cssenable = getNode("css").get("cssenable", "");
    	cssenable = "true";
    	if(cssenable!=null)
    		return cssenable;
    	else
    		return new String();
    }
    
    public static String getPass(String sname)
    {
        Preferences serverPref = getNode("servers/" + sname);
        return Util.decryptKey(serverPref.get("manpass", ""));
    }
    public static ServerSchema getServer(String sname)
    {
        Preferences serverPref = getNode("servers/" + sname);
        ServerSchema ss = new ServerSchema();
        ss.name = sname;
        ss.address = serverPref.get("address", "");
        ss.comport = serverPref.getInt("port", 0);
        ss.type = serverPref.get("type", "");
        
        if(ss.type.equalsIgnoreCase("ViewWise Server")){
        	ss.type = Constants.SERVER_VWS;
        	serverPref.put("type", ss.type);
        }else if(ss.type.equalsIgnoreCase("Document Routing Server")){
        	ss.type = Constants.SERVER_DRS;
        	serverPref.put("type", ss.type);
        } 
        return ss;
    }
    public static Vector getServers(boolean detailed) 
    {
        Preferences serverPref = getNode("servers");
        String[] servers = null;
        Vector vServers = new Vector();
        try
        {
            servers = serverPref.childrenNames();
        }
        catch(BackingStoreException e){}
 
        for (int i=0; i < servers.length; i++)
        {
            Preferences server = serverPref.node(servers[i]); 
            /* Purpose:	Master Switch off added ARS in registry
            * Created By: C.Shanmugavalli		Date:	20 Sep 2006
            */
            if( (server.getInt("port", 0) == ARSConstants.DEFAULT_COM_PORT )&& getARSEnable().equalsIgnoreCase("false")
            	||(server.getInt("port", 0) == DRSConstants.DEFAULT_COM_PORT )&& getDRSEnable().equalsIgnoreCase("false")	
            	||(server.getInt("port", 0) == VNSConstants.DEFAULT_COM_PORT )&& getVNSEnable().equalsIgnoreCase("false")
            	||(server.getInt("port", 0)== CSSConstants.DEFAULT_COM_PORT)& getCSSEnable().equalsIgnoreCase("false")
            	)
            	continue;
            if (detailed)
                vServers.addElement(getServer(servers[i]));
            else
                vServers.addElement(servers[i]);
        }
        return vServers;
    }
    static private void flush()
    {
        try
        {
            ManagerPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }
}