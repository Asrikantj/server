/*
 * IXRSettings.java
 *
 * Created on Decembaer 21, 2003, 4:24 PM
 */
package com.computhink.manager;
import com.computhink.common.*;
import com.computhink.ixr.server.*;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWS;
/**
 *
 * @author  Administrator
 */
import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.awt.Toolkit.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.rmi.*;
import java.io.*;
import java.util.*;

public class IXRSettings extends JDialog implements ManagerConstants 
{
    private JButton bOK, bCancel, bConnect;
    private JPanel pPorts, pProxy, pVWS, pGeneral, pLang;
    private JLabel lDataPort, lProxyPort, lProxyHost, lComPort, lRoom, 
                              lVWSPort, lVWSHost,lNote, lLang, lCodePage, lSnooze, lEngine, lblIdleTimeout, lRooms;
    private JTextField tProxyPort, tComPort, tDataPort, tProxyHost, 
                                                        tVWSHost, tVWSPort, txtIdleTimeout;
    private JCheckBox cbProxy, cbLog;
    private JComboBox cmbLang, cmbEngine, cmbCodePage;
    private ServerSchema vwsSchema;
    private Vector<String> vLanguages = new Vector<String>();
    private Vector<String> vEngines = new Vector<String>();
    private Vector<String> vCodePages = new Vector<String>();
    private JSpinner spSnooze;
    private IXR ixr;
    private static ResourceManager resourceManager=null;
    private VWS vws = null;
    private VWCheckList roomsList = new VWCheckList();
    private static final int width = 500;
    private static final int height = 510;

    private Vector roomsVector = new Vector();
    public IXRSettings(){
        loadLanguages();
        loadEngines();
        loadCodePage();
        initComponents();
        //getCurrentSettings();
    
    }
    public IXRSettings(JFrame parent, boolean modal, CIServer server) 
    {
        super(parent, modal);
        setTitle(ResourceManager.getDefaultManager().getString("IXRSettingsTitle"));
        this.ixr = (IXR) server;
        loadLanguages();
        loadEngines();
        loadCodePage();
        initComponents();
        getCurrentSettings();
    }
    private void loadEngines()
    {
        vEngines.addElement("AUTO");
        vEngines.addElement("LEGACY");
        vEngines.addElement("STANDARD");
        vEngines.addElement("FAST");
    }
    private void loadLanguages()
    {
        vLanguages.addElement("Afrikaans");
        vLanguages.addElement("Albanian");
        vLanguages.addElement("Aymara");
        vLanguages.addElement("Basque");
        vLanguages.addElement("Bemba");
        vLanguages.addElement("Blackfoot");
        vLanguages.addElement("Breton");
        vLanguages.addElement("Bugotu");
        vLanguages.addElement("Bulgarian");
        vLanguages.addElement("Byelorussian");
        vLanguages.addElement("Catalan");
        vLanguages.addElement("Chamorro");
        vLanguages.addElement("Chechen. Cyrillic alphabet");
        vLanguages.addElement("Chinese Simplified");
        vLanguages.addElement("Chinese Traditional");
        vLanguages.addElement("Corsican");
        vLanguages.addElement("Croatian");
        vLanguages.addElement("Crow");
        vLanguages.addElement("Czech");
        vLanguages.addElement("Danish");
        vLanguages.addElement("Dutch");
        vLanguages.addElement("English");
        vLanguages.addElement("Eskimo");
        vLanguages.addElement("Esperanto");
        vLanguages.addElement("Estonian");
        vLanguages.addElement("Faroese");
        vLanguages.addElement("Fijian");
        vLanguages.addElement("Finnish");
        vLanguages.addElement("French");
        vLanguages.addElement("Frisian");
        vLanguages.addElement("Friulian");
        vLanguages.addElement("Gaelic Irish");
        vLanguages.addElement("Gaelic Scottish");
        vLanguages.addElement("Ganda (Luganda)");
        vLanguages.addElement("German");
        vLanguages.addElement("Greek");
        vLanguages.addElement("Guarani");
        vLanguages.addElement("Hani");
        vLanguages.addElement("Hawaiian");
        vLanguages.addElement("Hungarian");
        vLanguages.addElement("Icelandic");
        vLanguages.addElement("Ido");
        vLanguages.addElement("Indonesian");
        vLanguages.addElement("Interlingua");
        vLanguages.addElement("Italian");
        vLanguages.addElement("Japanese");
        vLanguages.addElement("Kabardian. Cyrillic alphabet");
        vLanguages.addElement("Kasub");
        vLanguages.addElement("Kawa");
        vLanguages.addElement("Kikuyu");
        vLanguages.addElement("Kongo");
        vLanguages.addElement("Korean");
        vLanguages.addElement("Kpelle");
        vLanguages.addElement("Kurdish (Latin alphabet only)");
        vLanguages.addElement("Latin");
        vLanguages.addElement("Latvian");
        vLanguages.addElement("Lithuanian");
        vLanguages.addElement("Luba");
        vLanguages.addElement("Luxembourgian");
        vLanguages.addElement("Macedonian");
        vLanguages.addElement("Malagasy");
        vLanguages.addElement("Malay");
        vLanguages.addElement("Malinke");
        vLanguages.addElement("Maltese");
        vLanguages.addElement("Maori");
        vLanguages.addElement("Mayan");
        vLanguages.addElement("Miao (Latin alphabet only)");
        vLanguages.addElement("Minankabaw");
        vLanguages.addElement("Mohawk");
        vLanguages.addElement("Moldavian");
        vLanguages.addElement("Nahuatl");
        vLanguages.addElement("Norwegian");
        vLanguages.addElement("Nyanja");
        vLanguages.addElement("Occidental");
        vLanguages.addElement("Ojibway");
        vLanguages.addElement("Papiamento");
        vLanguages.addElement("Pigin Englis");
        vLanguages.addElement("Polish");
        vLanguages.addElement("Portuguese");
        vLanguages.addElement("Portuguese (Brazilian)");
        vLanguages.addElement("Provencal");
        vLanguages.addElement("Quechua");
        vLanguages.addElement("Rhaetic");
        vLanguages.addElement("Romanian");
        vLanguages.addElement("Romany");
        vLanguages.addElement("Ruanda");
        vLanguages.addElement("Rundi");
        vLanguages.addElement("Russian");
        vLanguages.addElement("Samoan");
        vLanguages.addElement("Sardinian");
        vLanguages.addElement("Serbian");
        vLanguages.addElement("Shona");
        vLanguages.addElement("Sioux");
        vLanguages.addElement("Slovak");
        vLanguages.addElement("Slovenian");
        vLanguages.addElement("Somali");
        vLanguages.addElement("Sorbian (Wend)");
        vLanguages.addElement("Sotho");
        vLanguages.addElement("Spanish");
        vLanguages.addElement("Sundanese");
        vLanguages.addElement("Swahili");
        vLanguages.addElement("Swazi");
        vLanguages.addElement("Swedish");
        vLanguages.addElement("Tagalog");
        vLanguages.addElement("Tahitian");
        vLanguages.addElement("Tinpo");
        vLanguages.addElement("Tongan");
        vLanguages.addElement("Tswana (Chuana)");
        vLanguages.addElement("Tun (Latin alphabet only)");
        vLanguages.addElement("Turkish");
        vLanguages.addElement("Ukrainian");
        vLanguages.addElement("Visayan");
        vLanguages.addElement("Welsh");
        vLanguages.addElement("Wolof");
        vLanguages.addElement("Xhosa");
        vLanguages.addElement("Zapotec");
        vLanguages.addElement("Zulu");

    }
    private void loadCodePage(){
    	vCodePages.addElement("Code Page 437");
    	vCodePages.addElement("Code Page 850");
    	vCodePages.addElement("Code Page 852");
    	vCodePages.addElement("Code Page 860");
    	vCodePages.addElement("Code Page 863");
    	vCodePages.addElement("Code Page 865");
    	vCodePages.addElement("Code Page 866");
    	vCodePages.addElement("CWI Magyar");
    	vCodePages.addElement("Greek-ELOT");
    	vCodePages.addElement("Greek-MEMOTEK");
    	vCodePages.addElement("Icelandic");
    	vCodePages.addElement("IVKAM C-S");
    	vCodePages.addElement("Latin 1");
    	vCodePages.addElement("Mac Central EU");
    	vCodePages.addElement("Mac INSO Latin 2");
    	vCodePages.addElement("Mac Primus CEu");
    	vCodePages.addElement("Macintosh");
    	vCodePages.addElement("Magyar Ventura");
    	vCodePages.addElement("Maltese");
    	vCodePages.addElement("Mazowia Polish");
    	vCodePages.addElement("OCR");
    	vCodePages.addElement("Roman 8");
    	vCodePages.addElement("Sloven & Coat");
    	vCodePages.addElement("Turkish");
    	vCodePages.addElement("Unicode");
    	vCodePages.addElement("UTF-8");
    	vCodePages.addElement("Windows ANSI");
    	vCodePages.addElement("Windows Baltic");
    	vCodePages.addElement("Windows Cyrilic");
    	vCodePages.addElement("Windows Eastern");
    	vCodePages.addElement("Windows Esperant");
    	vCodePages.addElement("Windows Greek");
    	vCodePages.addElement("Windows Sami");
    	vCodePages.addElement("Windows Turkish");
    	vCodePages.addElement("WordPerfect");
    	vCodePages.addElement("WordPerfect No");
    	vCodePages.addElement("WordPerfect Old");
    }
     private void initComponents() 
    {
        pPorts = new JPanel();
        pVWS = new JPanel();
        pProxy = new JPanel();
        pGeneral = new JPanel();
        pLang = new JPanel();
        cbProxy = new JCheckBox();
        resourceManager= ResourceManager.getDefaultManager();
        cbLog = new JCheckBox(resourceManager.getString("IXRSettingsChK.LogInfo"));
        bOK = new JButton(resourceManager.getString("IXRSettingsBtn.Ok"));
        bCancel = new JButton(resourceManager.getString("IXRSettingsBtn.Cancel"));

        tComPort = new JTextField();
        tDataPort = new JTextField();
        tProxyHost = new JTextField();
        tProxyPort = new JTextField();
        tVWSHost = new JTextField();
        tVWSPort = new JTextField();
        txtIdleTimeout=new JTextField();

        lProxyHost = new JLabel(resourceManager.getString("IXRSettingsLbl.ProxyHost"));
        lProxyPort = new JLabel(resourceManager.getString("IXRSettingsLbl.Port"));
        lVWSHost = new JLabel(resourceManager.getString("IXRSettingsLbl.VWSHost"));
        lVWSPort = new JLabel(resourceManager.getString("IXRSettingsLbl.VWSComPort"));
        lComPort = new JLabel(resourceManager.getString("IXRSettingsLbl.ComPort"));
        lDataPort = new JLabel(resourceManager.getString("IXRSettingsLbl.Dataport"));
        lLang = new JLabel(resourceManager.getString("IXRSettingsLbl.OCRLanguage"));
        lEngine = new JLabel(resourceManager.getString("IXRSettingsLbl.OCREngine"));
        lCodePage = new JLabel(resourceManager.getString("IXRSettingsLbl.OCRCode"));
        lNote = new JLabel(resourceManager.getString("IXRSettingsLbl.Note"));
        
        lSnooze = new JLabel(resourceManager.getString("IXRSettingsLbl.Snooze"));
        spSnooze = new JSpinner(new SpinnerNumberModel(1, 1, 300, 1));

        lblIdleTimeout = new JLabel(resourceManager.getString("IXRSettingsLbl.IdleTimeout")); 
        
        cmbEngine = new JComboBox(vEngines);
        cmbLang = new JComboBox(vLanguages);
        cmbCodePage = new JComboBox(vCodePages);

        bConnect = new JButton(resourceManager.getString("IXRSettingsBtn.Connect"));
        lRooms = new JLabel(resourceManager.getString("IXRSettingsLbl.Rooms"));
        getContentPane().setLayout(null);
        setResizable(false);
        
        pPorts.setLayout(null);
        pPorts.setBorder(new TitledBorder(resourceManager.getString("IXRSettingsTitleBorder.ServPorts")));
        pPorts.setFocusable(false); getContentPane().add(pPorts);
        pPorts.setBounds(20, 20, 450, 50);
        pPorts.add(lComPort); lComPort.setBounds(15, 20, 120, 16);
        pPorts.add(lDataPort); lDataPort.setBounds(300, 20, 60, 16);
        pPorts.add(tComPort); tComPort.setBounds(150, 20, 50, 20);
        pPorts.add(tDataPort); tDataPort.setBounds(370, 20, 50, 20);
        
        pProxy.setLayout(null);
        pProxy.setBorder(new TitledBorder(resourceManager.getString("IXRSettingsTitleBorder.HttpProxy")));
        pProxy.setFocusable(false);  getContentPane().add(pProxy);
        pProxy.setBounds(20, 75, 450, 50);
        pProxy.add(lProxyHost); lProxyHost.setBounds(15, 20, 70, 16);
        pProxy.add(lProxyPort); lProxyPort.setBounds(325, 20, 30, 16);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 21);
        pProxy.add(tProxyHost); tProxyHost.setBounds(90, 20, 110, 20);
        pProxy.add(tProxyPort); tProxyPort.setBounds(370, 20, 50, 20);
        
        pVWS.setLayout(null);
        pVWS.setBorder(new TitledBorder(PRODUCT_NAME +" "+resourceManager.getString("IXRSettingsTitleBorder.Server")));
        pVWS.setFocusable(false); getContentPane().add(pVWS);
        pVWS.setBounds(20, 130, 450, 150);
        pVWS.add(lVWSHost); lVWSHost.setBounds(15, 20, 70, 16);
        pVWS.add(tVWSHost); tVWSHost.setBounds(90, 20, 110, 20);
        pVWS.add(lVWSPort); lVWSPort.setBounds(250, 20, 120, 16);
        pVWS.add(tVWSPort); tVWSPort.setBounds(370, 20, 50, 20);
        /*Vector<String> roomDetails = new Vector<String>();
        roomDetails.add("Room1");
        roomDetails.add("Room2");
        roomDetails.add("Room3");
        roomDetails.add("Room4");
        roomDetails.add("Room5");
        roomDetails.add("Room6");
        roomsList.loadData(roomDetails);*/       
        roomsList.disableAllItems();
        pVWS.add(lRooms); lRooms.setBounds(15, 55, 70, 16);
        pVWS.add(roomsList); roomsList.setBounds(90, 55, 328, 80);
        
        //pVWS.add(bConnect); bConnect.setBounds(345, 107, 80, 23);
        
        pLang.setLayout(null);
        pLang.setBorder(new TitledBorder(resourceManager.getString("IXRSettingsTitleBorder.General")));
        pLang.setFocusable(false); getContentPane().add(pLang);
        pLang.setBounds(20, 285, 450, 128);
        pLang.add(lLang); lLang.setBounds(15, 20, 100, 16);
        pLang.add(cmbLang); cmbLang.setBounds(125, 20, 120, 20);
        pLang.add(lEngine); lEngine.setBounds(15, 55, 80, 16);
        pLang.add(cmbEngine); cmbEngine.setBounds(125, 55, 120, 20);
        pLang.add(lSnooze); lSnooze.setBounds(260, 20, 120, 16);
        pLang.add(spSnooze); spSnooze.setBounds(370, 16, 45, 26);
        pLang.add(lblIdleTimeout); lblIdleTimeout.setBounds(260, 55, 120, 16);
        pLang.add(txtIdleTimeout); txtIdleTimeout.setBounds(370, 55, 45, 20);
        pLang.add(lCodePage); lCodePage.setBounds(15, 90, 120, 16); 
        pLang.add(cmbCodePage); cmbCodePage.setBounds(125, 90, 120, 20);


        getContentPane().add(bOK); bOK.setBounds(292, 430, 80, 23);
        getContentPane().add(bCancel); bCancel.setBounds(388, 430, 80, 23);
        getContentPane().add(cbLog); cbLog.setBounds(23, 410, 450, 20);
        bOK.setToolTipText(lNote.getText());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(width, height));
        setLocation((screenSize.width - width)/2,(screenSize.height-height)/2);
        //-------------Listeners------------------------------------------------
        addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });
        bOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
        bConnect.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                ConnecttoVWS();
            }
        });        
    }
// Enhancement for Indexer - selected room will process
    private boolean ConnecttoVWS()
    {
        applyVWSSettings();
        vws = (VWS) Util.getServer(vwsSchema);
        if (vws != null)
        {
            populateRooms();
            roomsList.loadData(roomsVector);
            setSelectedRooms();
            roomsList.disableAllItems();
            return true;
        }
        else
        {
            return false;
        }
    }
    private void applyVWSSettings()
    {
    	try{
	        vwsSchema.address = tVWSHost.getText();
	        vwsSchema.comport = Util.to_Number(tVWSPort.getText());
	        ixr.setVWS(vwsSchema);
    	}catch(Exception ex){}
    }
    private void populateRooms()
    {
        try
        {
        	roomsVector.clear();
            Vector ret = vws.getRoomNames();
            roomsVector.addAll(ret);
        }
        catch(Exception e){}
    }
    private String getServerName()
    {
        try
        {
            return ixr.getSchema().name;
        }
        catch(RemoteException re)
        {
            return "";
        }
    }
// Enhancement for Indexer - selected room will process
    private String getSelectedRooms(){
    	StringBuffer selectedRooms = new StringBuffer("");
    	List rooms = roomsList.getSelectedItems();
    	for(int item=0; item < rooms.size(); item++){
    		selectedRooms.append(rooms.get(item).toString() + ";");
    	}
    	return selectedRooms.toString();
    }
    private void setSelectedRooms(){
    	String selectedRooms = IXRPreferences.getSelectedRooms();
    	Vector roomsVector = new Vector();
    	StringTokenizer tokens = new StringTokenizer(selectedRooms, ";");
    	while(tokens.hasMoreTokens()){
    		roomsVector.add(tokens.nextToken());
    	}
    	roomsList.setCheckItems(roomsVector);   	
    }
    
    
    private void setSettings()
    {
        try
        {
            ixr.setComPort(Integer.parseInt(tComPort.getText()));
            ixr.setDataPort(Integer.parseInt(tDataPort.getText()));
            ixr.setProxy(cbProxy.getSelectedObjects() != null? true : false);
            ixr.setProxyHost(tProxyHost.getText());
            String pPort = tProxyPort.getText();
            if (pPort.equals(""))
                ixr.setProxyPort(0);
            else
                ixr.setProxyPort(Integer.parseInt(pPort));
            vwsSchema.address = tVWSHost.getText();
            vwsSchema.comport = Util.to_Number(tVWSPort.getText());
            ixr.setVWS(vwsSchema);
            ixr.setLogInfo(cbLog.isSelected());
            ixr.setLanguage((String) cmbLang.getSelectedItem());
            ixr.setSnooze( ((Integer) spSnooze.getValue()).intValue());
            ixr.setIdleTimeout( Integer.parseInt(this.txtIdleTimeout.getText()));
            ixr.setEngine((String) cmbEngine.getSelectedItem());
            ixr.setCodePage((String) cmbCodePage.getSelectedItem());
        }
        catch(Exception e) {}
    }
    private void getCurrentSettings()
    {
        try
        {
            tComPort.setText(String.valueOf(ixr.getComPort()));
            tDataPort.setText(String.valueOf(ixr.getDataPort()));
            cbProxy.setSelected(ixr.getProxy());
            tProxyHost.setText(ixr.getProxyHost());
            int pPort = ixr.getProxyPort();
            if (pPort > 0)
                tProxyPort.setText(String.valueOf(pPort));
            else
                tProxyPort.setText("");
            vwsSchema = ixr.getVWS();
            tVWSHost.setText(vwsSchema.address);
            int vPort = vwsSchema.comport;
            if (vPort > 0)
                tVWSPort.setText(String.valueOf(vPort));
            else
                tVWSPort.setText("");
            cbLog.setSelected(ixr.getLogInfo());
            cbProxyStateChanged();
            cmbLang.setSelectedItem(ixr.getLanguage());
            spSnooze.setValue(new Integer(ixr.getSnooze()));
            cmbEngine.setSelectedItem(ixr.getEngine());
            cmbCodePage.setSelectedItem(ixr.getCodePage());
              this.txtIdleTimeout.setText(String.valueOf(IXRPreferences.getIdleTimeout()));
              ConnecttoVWS();
        }
        catch(RemoteException re)
        {
        }
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    private void closeDialog() 
    {
        setVisible(false);
        dispose();
    }
    public static void main(String args[]) 
    {
    	try{
        String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
		UIManager.setLookAndFeel(plasticLookandFeel);
    	}catch(Exception ex){}    	
    	/*
    	 * new IXRSettings().show(); method is replaced with new IXRSettings().setVisible(true); 
    	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
    	 */
        new IXRSettings().setVisible(true);
    }
    
}
