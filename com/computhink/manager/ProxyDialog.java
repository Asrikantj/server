/*
 * DSSProxyDialog.java
 *
 * Created on September 27, 2003, 11:51 AM
 */

package com.computhink.manager;

import com.computhink.common.*;
import com.computhink.resource.ResourceManager;

import java.awt.*;
import javax.swing.border.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
//import org.netbeans.lib.awtextra.*;
import java.awt.Toolkit.*;
/**
 *
 * @author  Saad
 */
public class ProxyDialog extends JDialog implements ManagerConstants
{
    private JTextField tPort;
    private JPanel pProxy;
    private JLabel lAddress;
    private JTextField tAddress;
    private JCheckBox cbProxy;
    private JLabel lPort;
    private JButton bCancel;
    private JButton bOK;
    private static ResourceManager resourceManager= null;
    public ProxyDialog(JFrame parent, boolean modal) 
    {
        super(parent, modal);
        initComponents();
    }
    private void initComponents() 
    {
    	resourceManager=ResourceManager.getDefaultManager();
        bOK = new JButton(resourceManager.getString("ProxyDlgBtn.OK"));
        bCancel = new JButton(resourceManager.getString("ProxyDlgBtn.Cancel"));
        cbProxy = new JCheckBox();
        pProxy = new JPanel();
        lAddress = new JLabel(resourceManager.getString("ProxyDlgLbl.Address"));
        tAddress = new JTextField(ManagerPreferences.getProxyHost());
        lPort = new JLabel(resourceManager.getString("ProxyDlgLbl.Port"));
        tPort = new JTextField(
                            Integer.toString(ManagerPreferences.getProxyPort()));

        getContentPane().setLayout(null);
        setTitle(resourceManager.getString("ProxyDlg.Title"));
        pProxy.setLayout(null);
        
        getContentPane().add(bOK);  bOK.setBounds(278, 110, 90, 26);
        getContentPane().add(bCancel);  bCancel.setBounds(378, 110, 90, 26);
        pProxy.add(cbProxy);  cbProxy.setBounds(10, 0, 20, 20);
        pProxy.setBorder(new TitledBorder("         "+resourceManager.getString("ProxyDlg.TitleBorder")));
        pProxy.add(lAddress);  lAddress.setBounds(25, 30, 120, 16);
        pProxy.add(tAddress);  tAddress.setBounds(90, 30, 140, 20);
        pProxy.add(lPort);  lPort.setBounds(300, 30, 120, 16);
        pProxy.add(tPort); tPort.setBounds(340, 30, 80, 20);
        getContentPane().add(pProxy); pProxy.setBounds(20, 20, 450, 70);
        
        cbProxy.setSelected(ManagerPreferences.getProxy());
        cbProxyStateChanged(); 
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new java.awt.Dimension(505, 185));
        setLocation((screenSize.width-505)/2,(screenSize.height-185)/2);
        //-------------Listeners------------------------------------------------
        addWindowListener(new java.awt.event.WindowAdapter() 
        {
            public void windowClosing(java.awt.event.WindowEvent evt) 
            {
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
        bOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                updateProxy();
                closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                closeDialog();
            }
        });
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tAddress.setEnabled(b);
        tPort.setEnabled(b);
    }
    private void updateProxy()
    {
        ManagerPreferences.setProxy(
                      (cbProxy.getSelectedObjects() == null ? false : true));
        ManagerPreferences.setProxyHost(tAddress.getText());
        ManagerPreferences.setProxyPort(Integer.parseInt(tPort.getText()));
    }
    private void closeDialog() 
    {
        setVisible(false);
        dispose();
    }
    
}


