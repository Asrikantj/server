package com.computhink.manager;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ToolTipManager;
import javax.swing.border.LineBorder;
import javax.swing.event.MouseInputAdapter;
import javax.swing.text.html.HTMLEditorKit;

import com.computhink.common.CIServer;
import com.computhink.common.Constants;
import com.computhink.common.RoomProperty;
import com.computhink.common.Util;
import com.computhink.manager.image.Images;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.license.License;
import com.computhink.vws.license.LicenseException;
import com.computhink.vws.license.LicenseManager;
import com.computhink.vws.license.LicenseManagerImpl;
import com.computhink.vws.license.SWLicense;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;
import com.computhink.vws.server.VWSUtil;

public class VWGeneralPanel extends VWPanel implements Constants, ManagerConstants{
	  private static ResourceManager resourceManager= null;
	public VWGeneralPanel(CIServer server) {
		super();
		vws = (VWS) server;
		if (vws == null)
			prefs = new VWSPreferences();
		initComponents();		
		getGeneral();
		//setLicenseInfo();
		setCVLicenseInfo();
		addDocListeners();
	}
	public VWGeneralPanel() {
		// TODO Auto-generated constructor stub
		super();
		prefs = new VWSPreferences();
		vws = null;
		initComponents();
		addDocListeners();
	}
	private void initComponents() 
	{   
		resourceManager=ResourceManager.getDefaultManager();
		setLayout(null);
		pLicense = new VWPanel();
		pRRooms = new VWPanel();
		pLicense.setLayout(null);
		lLicenseTitile=new JLabel("<html><u>"+resourceManager.getString("LicenseLbl.Title")+"</u></html>");
		lLicenseTitile.setFont(new Font("", Font.PLAIN, 12));
		lLicenseTitile.setForeground(Color.BLUE);
		lLicense = new JLabel(resourceManager.getString("LicenseLbl.LicenseFile"));
		lLicense.setFont(new Font("", Font.PLAIN, 11));
		lNamedUsers = new JLabel(resourceManager.getString("LicenseLbl.NamedGroup"));
		lNamedUsers.setFont(new Font("", Font.PLAIN, 11));
		lNamedUsers.setForeground(Color.red);
		lNamedDesktop=new JLabel(resourceManager.getString("LicenseLbl.Desktop"));
		lNamedDesktop.setFont(new Font("", Font.PLAIN, 11));
		lNamedDesktop.setForeground(Color.black);
		lNamedOnline=new JLabel(resourceManager.getString("LicenseLbl.Online"));
		lNamedOnline.setFont(new Font("", Font.PLAIN, 11));
		lNamedOnline.setForeground(Color.black);
		lPublicWA=new JLabel(resourceManager.getString("LicenseLbl.PublicWA"));
		lPublicWA.setFont(new Font("", Font.PLAIN, 11));
		lPublicWA.setForeground(Color.black);
		lPublicWA.setVisible(false);
		lConcurrentDesktop=new JLabel(resourceManager.getString("LicenseLbl.Desktop"));
		lConcurrentDesktop.setFont(new Font("", Font.PLAIN, 11));
		lConcurrentDesktop.setForeground(Color.black);
		lConcurrentOnline=new JLabel(resourceManager.getString("LicenseLbl.Online"));
		lConcurrentOnline.setFont(new Font("", Font.PLAIN, 11));
		lConcurrentOnline.setForeground(Color.black);
		
		
		lLineBorder=new JLabel();
		lProfessionalNamedUsers=new JLabel(resourceManager.getString("LicenseLbl.ProfessionalNamedGroup"));
		lProfessionalNamedUsers.setFont(new Font("", Font.PLAIN, 11));
		lProfessionalNamedUsers.setForeground(Color.red);
		lSecurityAdmin=new JLabel(resourceManager.getString("LicenseLbl.SecurityAdministrator"));
		lSecurityAdmin.setFont(new Font("", Font.PLAIN, 11));
		lSecurityAdmin.setForeground(Color.black);
		lEnterpriceConcurrent=new JLabel(resourceManager.getString("LicenseLbl.EnterpriceConcurrent"));
		lProfessionalConcurrent=new JLabel(resourceManager.getString("LicenseLbl.ProfessionalConcurrent"));
		lBatchInput=new JLabel(resourceManager.getString("LicenseLbl.BatchInput"));
		lBatchInput.setFont(new Font("", Font.PLAIN, 11));
		lBatchInput.setForeground(Color.black);
		lLicenseSummary = new JLabel("<html><u>"+resourceManager.getString("LicenseLbl.LicenseSummary")+"</u></html>");
		lLicenseSummary.setFont(new Font("", Font.PLAIN, 12));
		
			
		lLicenseSummary.setForeground(Color.BLUE);
		lGeneralHelp = new JLabel(" Double-click on Room icon to bring/take Room online/offline.");

		tNamedUsers = new JTextField();
		tNamedOnline=new JTextField();
		tConcurrentOnline=new JTextField();
		tProfessionalNamedUsers=new JTextField();
		tPublicWA=new JTextField();
		tSecurityAdmin=new JTextField();
		tEnterpriceConcurrent=new JTextField();
		tProfessionalConcurrent=new JTextField();
		tLicense = new JTextField();
		tBatchInput = new JTextField();

		bBrowse = new JButton(); 
		bRegister1 = new JButton(resourceManager.getString("LicenseBtn.Apply")); 
		bInformation=new JButton();
		spRooms = new JScrollPane();
		spRRooms = new JScrollPane();

		/*epLogo = new JEditorPane();
        epLogo.setEditorKit(new HTMLEditorKit()); 
        epLogo.setEditable(false);
        epLogo.setBorder(new LineBorder(Color.gray, 1));*/

		epCVLicense = new JEditorPane();
		epCVLicense.setEditorKit(new HTMLEditorKit()); 
		epCVLicense.setEditable(false);
		//epCVLicense.setBorder(new LineBorder(Color.gray, 1));

		lLicense.setBounds(20+20,20+25, 160, 16); pLicense.add(lLicense); 
		lLicenseTitile.setBounds(20,20,160,16);pLicense.add(lLicenseTitile);
		tLicense.setBounds(75,20+25, 275+118, 20);pLicense.add(tLicense);
		tLicense.setEnabled(false);
		bBrowse.setBounds(470,20+25,20, 20);pLicense.add(bBrowse);
		lLineBorder.setBounds(20,160,480, 1);pLicense.add(lLineBorder); 
		bBrowse.setIcon(Images.bro);
		bInformation.setBounds(470,120,20, 20); pLicense.add(bInformation);
		bInformation.setBorderPainted(false);
		bInformation.setBackground(new Color(238, 242, 244));
		//bInformation.setToolTipText("License Information");
		bInformation.setFocusPainted(false);
		bInformation.setIcon(Images.info);
		
		bInformation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));        
		bInformation.setContentAreaFilled(false);
		bInformation.addMouseMotionListener(new MouseMotionAdapter(){
		      public void mouseMoved(MouseEvent e){         
		    	  setCVLicenseToolTip();
		      }       
		    });
	
		
		/*bInformation.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				//browseLicenseFile();
				bInformation.setBackground(new Color(238, 242, 244));
				
			}
		});
		*/
		bBrowse.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				browseLicenseFile();
			}
		});
		bRegister1.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				registerGeneral();
				bRegister1.setEnabled(false);
			}
		});
	
	//	LineBorder boder=new LineBorder(Color.gray, 1);
		lLineBorder.setBorder(new LineBorder(new Color(107, 106, 104)));
		//lLineBorder.setBorder(boder);
		lLineBorder.setVisible(true);
		lNamedUsers.setBounds(20,56+150, 150, 16); pLicense.add(lNamedUsers);
		lNamedDesktop.setBounds(50+110,56+150, 150, 16);pLicense.add(lNamedDesktop);
		lNamedOnline.setBounds(50+110,56+150+25, 150, 16);pLicense.add(lNamedOnline);
		tNamedUsers.setBounds(190+30,56+150, 300-30, 20); pLicense.add(tNamedUsers);
		tNamedOnline.setBounds(190+30,56+150+25, 300-30, 20);pLicense.add(tNamedOnline);
		lProfessionalNamedUsers.setBounds(20,56+150+25+25, 170, 16); pLicense.add(lProfessionalNamedUsers);
		lEnterpriceConcurrent.setBounds(20,56+150+25+25+35,170,16);pLicense.add(lEnterpriceConcurrent);
		lEnterpriceConcurrent.setFont(new Font("", Font.PLAIN, 11)); 
		lEnterpriceConcurrent.setForeground(Color.red);
		lConcurrentDesktop.setBounds(50+110,56+150+25+35+25,170,16);pLicense.add(lConcurrentDesktop);
		lConcurrentOnline.setBounds(50+110,56+150+25+25+35+25, 150, 16);pLicense.add(lConcurrentOnline);
		lEnterpriceConcurrent.setVisible(true);
		lProfessionalConcurrent.setBounds(20,56+150+25+25+35+25+25,170,16);pLicense.add(lProfessionalConcurrent);
		lProfessionalConcurrent.setFont(new Font("", Font.PLAIN, 11));
		lProfessionalConcurrent.setForeground(Color.red);
		lProfessionalConcurrent.setVisible(true);
		lPublicWA.setBounds(20,56+150+25+25+35+25+25+35,170,16);pLicense.add(lPublicWA);
		lPublicWA.setVisible(false);
		lSecurityAdmin.setBounds(20,376,170,16);pLicense.add(lSecurityAdmin);
		lSecurityAdmin.setVisible(true);
		lBatchInput.setBounds(20,401,170,16);pLicense.add(lBatchInput);
		lBatchInput.setVisible(true);
		tProfessionalNamedUsers.setBounds(190+30,56+150+25+25, 300-30, 20); pLicense.add(tProfessionalNamedUsers);
		tEnterpriceConcurrent.setBounds(190+30,56+150+25+25+35, 300-30, 20); pLicense.add(tEnterpriceConcurrent);
		tConcurrentOnline.setBounds(190+30,56+150+25+25+35+25, 300-30, 20);pLicense.add(tConcurrentOnline);
		tProfessionalConcurrent.setBounds(190+30,56+150+25+25+35+25+25, 300-30, 20); pLicense.add(tProfessionalConcurrent);
		tPublicWA.setBounds(190+30,56+150+25+25+35+25+25+35, 300-30, 20);pLicense.add(tPublicWA);
		tPublicWA.setVisible(false);
		tSecurityAdmin.setBounds(190+30,376, 300-30, 20); pLicense.add(tSecurityAdmin);
		tBatchInput.setBounds(190+30,401, 300-30, 20); pLicense.add(tBatchInput);
		lLicenseSummary.setBounds(20, 175, 160, 16); pLicense.add(lLicenseSummary);

		/*pLicense.add(epLogo); 
        epLogo.setBounds(20, 128, 470, 220);        
        epLogo.setBackground(this.getBackground());*/

        pLicense.add(epCVLicense); 
        epCVLicense.setBounds(25,70,440,80);        
        epCVLicense.setBackground(this.getBackground());
        
        SymMouse symMouse = new SymMouse();
        epCVLicense.addMouseListener(symMouse);

        bRegister1.setBounds(iRegLeft, iRegTop-10, iRegWidth, iRegHeight); pLicense.add(bRegister1); 
        add(pLicense);
        bRegister1.setBackground(this.getBackground());
        pLicense.setBounds(0, 0, 500, 700);

	}
	
	class SymMouse extends MouseAdapter{		
		public void mouseClicked(MouseEvent event) {
			Object object = event.getSource();
			if (object instanceof JEditorPane)
				if(event.getModifiers()==event.BUTTON3_MASK)
					VWEditorPanelLicense_RightMouseClicked(event);
		}
	}
	
	public void VWEditorPanelLicense_RightMouseClicked(MouseEvent event) {
		System.out.println("VWEditorPanelLicense_RightMouseClicked...");
		if(event.getClickCount() == 1)
			rSingleClick(event);
		else if(event.getClickCount() == 2)
			rDoubleClick(event);
	}
	
	private void rSingleClick(MouseEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	private void rDoubleClick(MouseEvent event) {
		System.out.println("rDoubleClick...");
		setCVLicenseToolTip();
	}
	void setRoomLevelNamed(){
		try
		{
			RoomProperty[] rooms = null;
			rooms = VWSPreferences.getRegRooms();
			for (int i=0; i < rooms.length; i++)
			{
				String named=VWSPreferences.getRoomsNamed(rooms[i].getName());
				System.out.println("VWSPreferences.getNamedUsers() inside for"+VWSPreferences.getNamedUsers());
				if(named.equalsIgnoreCase(VWSPreferences.getNamedUsers())){
					System.out.println("named inside for loop"+named);
					String roomNamed=rooms[i]+"#"+tNamedUsers.getText();
					System.out.println("roomNamed"+roomNamed);
					VWSPreferences.setRoomsNamed(roomNamed);
				}
			}
		}
		catch(Exception e){}
	}
	
	void setRoomLevelNamedOnline(){
		try
		{
			RoomProperty[] rooms = null;
			rooms = VWSPreferences.getRegRooms();
			for (int i=0; i < rooms.length; i++)
			{
				String named=VWSPreferences.getRoomsNamedOnline(rooms[i].getName());
				System.out.println("VWSPreferences.getNamedUsers() inside for"+VWSPreferences.getNamedOnline());
				if(named.equalsIgnoreCase(VWSPreferences.getNamedOnline())){
					System.out.println("named inside for loop"+named);
					String roomNamed=rooms[i]+"#"+tNamedUsers.getText();
					System.out.println("roomNamed"+roomNamed);
					VWSPreferences.getRoomsNamedOnline(roomNamed);
				}
			}
		}
		catch(Exception e){}
	}
	
	void setRoomLevelProfessional(){
		try
		{
			RoomProperty[] rooms = null;
			rooms = VWSPreferences.getRegRooms();
			for (int i=0; i < rooms.length; i++)
			{
				RoomProperty rp =  new RoomProperty(rooms[i].getName());
				String named= 	VWSPreferences.getRoomsProfessionalNamed(rooms[i].getName());
				System.out.println("prof named inside for loop"+named);
				System.out.println("VWSPreferences.getNamedUsers() inside for"+VWSPreferences.getProfessionalNamedUsers());
				if(named.equalsIgnoreCase(VWSPreferences.getProfessionalNamedUsers())){
					String roomProfessional=rooms[i]+"#"+tProfessionalNamedUsers.getText();
					VWSPreferences.setRoomsProfessionalNamed(roomProfessional);
				}
			}

		}
		catch(Exception e){}
	}
	
	private void registerGeneral()
	{
		try{
			if (!checkRegisteredGroupsAreValid(tBatchInput, resourceManager.getString("RoomMessage.BatchInputAdminGroup"))) return;			
			if (vws == null){
				if (VWSPreferences.getRoomLevelNamed()) {
					VWSPreferences.setLicenseFile(tLicense.getText());
					setRoomLevelNamed();
					setRoomLevelNamedOnline();
					setRoomLevelProfessional();
					setRoomLevelBatchInputAdministrator();
					VWSPreferences.setNamedUsers(tNamedUsers.getText());
					VWSPreferences.setNamedOnline(tNamedOnline.getText());
					VWSPreferences.setProfessionalNamedUsers(tProfessionalNamedUsers.getText());
					VWSPreferences.setEnterpriseConcurrent(tEnterpriceConcurrent.getText());
					VWSPreferences.setConcurrentOnline(tConcurrentOnline.getText());
					VWSPreferences.setProfessionalConcurrent(tProfessionalConcurrent.getText());
					VWSPreferences.setPublicWebAccess(tPublicWA.getText());
					VWSPreferences.setSecurityAdministrator(tSecurityAdmin.getText());
					VWSPreferences.setBatchInputAdministrator(tBatchInput.getText());
				} else {
					VWSPreferences.setLicenseFile(tLicense.getText());
					VWSPreferences.setNamedUsers(tNamedUsers.getText());
					VWSPreferences.setNamedOnline(tNamedOnline.getText());
					VWSPreferences.setProfessionalNamedUsers(tProfessionalNamedUsers.getText());
					VWSPreferences.setEnterpriseConcurrent(tEnterpriceConcurrent.getText());
					VWSPreferences.setConcurrentOnline(tConcurrentOnline.getText());
					VWSPreferences.setProfessionalConcurrent(tProfessionalConcurrent.getText());
					VWSPreferences.setPublicWebAccess(tPublicWA.getText());
					VWSPreferences.setSecurityAdministrator(tSecurityAdmin.getText());
					VWSPreferences.setBatchInputAdministrator(tBatchInput.getText());
				}
			} else {
				if (VWSPreferences.getRoomLevelNamed()) {
					setRoomLevelNamed();
					setRoomLevelNamedOnline();
					setRoomLevelProfessional();
					setRoomLevelBatchInputAdministrator();
					vws.setProfessionalNamedUsers(tProfessionalNamedUsers.getText());
					vws.setLicenseFile(tLicense.getText());
					vws.setNamedUsers(tNamedUsers.getText());
					vws.setNamedOnlineUsers(tNamedOnline.getText());
					VWSPreferences.setEnterpriseConcurrent(tEnterpriceConcurrent.getText());
					VWSPreferences.setConcurrentOnline(tConcurrentOnline.getText());
					VWSPreferences.setProfessionalConcurrent(tProfessionalConcurrent.getText());
					VWSPreferences.setPublicWebAccess(tPublicWA.getText());
					vws.setSecurityAdministrator(tSecurityAdmin.getText());
					vws.setBatchInputAdministrator(tBatchInput.getText());
				} else {
					vws.setProfessionalNamedUsers(tProfessionalNamedUsers.getText());
					vws.setLicenseFile(tLicense.getText());
					vws.setNamedUsers(tNamedUsers.getText());
					vws.setNamedOnlineUsers(tNamedOnline.getText());
					VWSPreferences.setEnterpriseConcurrent(tEnterpriceConcurrent.getText());
					VWSPreferences.setConcurrentOnline(tConcurrentOnline.getText());
					VWSPreferences.setProfessionalConcurrent(tProfessionalConcurrent.getText());
					VWSPreferences.setPublicWebAccess(tPublicWA.getText());
					vws.setSecurityAdministrator(tSecurityAdmin.getText());
					vws.setBatchInputAdministrator(tBatchInput.getText());
				}

			}
		}catch(Exception e){

		}
	}

	private void browseLicenseFile()
	{
		String dFolder = VWSUtil.getHome() + Util.pathSep + "License";
		JFileChooser fc = new JFileChooser(new File(dFolder));
		fc.setFileFilter(new LicenseFilter());
		//fc.showDialog(this, "Register");
		fc.showOpenDialog(this);
		File lic = fc.getSelectedFile();
		if (lic != null) tLicense.setText(lic.getPath());
	}

	private class LicenseFilter extends javax.swing.filechooser.FileFilter 
	{
		public boolean accept(File f) 
		{
			if (f.isDirectory()) return true;
			String extension = getExtension(f);
			if (extension != null && extension.equals("lic")) return true;
			return false;
		}
		private String getExtension(File f) 
		{
			String ext = null;
			String s = f.getName();
			int i = s.lastIndexOf('.');

			if (i > 0 &&  i < s.length() - 1) 
				ext = s.substring(i+1).toLowerCase();
			return ext;
		}
		public String getDescription() 
		{
			return "License Files";
		}
	}

	private void getGeneral()
	{
		try
		{
			if (vws == null){
				tLicense.setText(VWSPreferences.getLicenseFile());
				tNamedUsers.setText(VWSPreferences.getNamedUsers());
				tNamedOnline.setText(VWSPreferences.getNamedOnline());
				tProfessionalNamedUsers.setText(VWSPreferences.getProfessionalNamedUsers());
				tEnterpriceConcurrent.setText(VWSPreferences.getEnterpriseConcurrent());
				tConcurrentOnline.setText(VWSPreferences.getConcurrentOnline());
				tProfessionalConcurrent.setText(VWSPreferences.getProfessionalConcurrent());
				tPublicWA.setText(VWSPreferences.getPublicWebAccess());
				tSecurityAdmin.setText(VWSPreferences.getSecurityAdministrator());
				tBatchInput.setText(VWSPreferences.getBatchInputAdministrator());
			}else{
				tLicense.setText(vws.getLicenseFile());
				tNamedUsers.setText(vws.getNamedUsers()); 
				tNamedOnline.setText(VWSPreferences.getNamedOnline());
				tProfessionalNamedUsers.setText(vws.getProfessionalNamedUsers());
				tEnterpriceConcurrent.setText(VWSPreferences.getEnterpriseConcurrent());
				tConcurrentOnline.setText(VWSPreferences.getConcurrentOnline());
				tProfessionalConcurrent.setText(VWSPreferences.getProfessionalConcurrent());
				tPublicWA.setText(VWSPreferences.getPublicWebAccess());
				tSecurityAdmin.setText(vws.getSecurityAdministrator());
				tBatchInput.setText(vws.getBatchInputAdministrator());
			}
		}
		catch(Exception e){e.printStackTrace();}
		bRegister1.setEnabled(false);
	}
	
/*
 *Added in CV83B2 For getting the Max of Enterprice Named License  	
 *Added for UI Changes 
 */
	private int getMaxEnterpriceNamed(int desktopNamed,int webTopNamed,int webLiteNamed,int webaccessSeats){
		if(desktopNamed>webTopNamed&&desktopNamed>webLiteNamed&&desktopNamed>webaccessSeats)
			return desktopNamed;
		else if(webTopNamed>webLiteNamed&&webTopNamed>webaccessSeats)
			return webTopNamed;
		else if(webLiteNamed>webaccessSeats)
			return webLiteNamed;
		else
			return webaccessSeats;
	}
	
	private int getMaxEnterpriceConcurrent(int desktopConcurrent,int webTopConcurrent,int webLiteConcurrent,int webaccessConcurrentSeats){
		if(desktopConcurrent>webTopConcurrent&&desktopConcurrent>webLiteConcurrent&&desktopConcurrent>webaccessConcurrentSeats)
			return desktopConcurrent;
		else if(webTopConcurrent>webLiteConcurrent&&webTopConcurrent>webaccessConcurrentSeats)
			return webTopConcurrent;
		else if(webLiteConcurrent>webaccessConcurrentSeats)
			return webLiteConcurrent;
		else
			return webaccessConcurrentSeats;
	}
	
	
	
/**
 * Method added to get the SignWiseLicense
 * @return
 */
	private int getSigWiseLicense(){
		LicenseManager lm = null;
		License lc1 = null;
		String license="";
		lm =  LicenseManagerImpl.getInstance();

		try {
			lc1 = lm.getLicense();
		} catch (LicenseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (lm == null)
		{
			return 0;
		}
		try
		{
			license = lc1.getCompanyName();
			SWLicense lc = lm.getSignWiseLicense();
			if (lc != null && license.equals(lc.getCompanyName()))
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}
		catch(LicenseException le)
		{
			//VWSLog.war(le.getMessage());
		}
		return 0;

}
	//Modified for CV83B2 License UI Changes 
	private void setCVLicenseToolTip() {
		try{
			License lic = null;
			if(vws == null){
				LicenseManager lm = LicenseManagerImpl.getInstance();
				if(lm != null){
					lic = lm.getLicense();
				}
			}else{
				lic = vws.getLicense();				
			}
			
			if(lic != null){
				/**
				 * Professional Seats = Server Seats - Shared Named Seats(DTC, WebTop, WebLite and WebAccess).
				 * where, Professional Seats : Named WebAccess Seats.
				 * 		  ServerSeats : Shared Named seats + Professional seats.
				 * Consumed Professional Seats = Consumed WebAccess seats - Named WebAccess Seats() 
				 */
				int consumedWebAccessSeats =0;
				int totalCLWebAccessUsers =0;
				int professionalSeats = lic.getProfessionalWebAccessSeats();
				int webaccessSeats=lic.getWebAccessSeats();
				/*if(lic.getWebAccessSeats() > 0 && lic.getWebAccessSeats() > lic.getDesktopSeatsNamed()){
					professionalSeats = lic.getServerSeats() - lic.getDesktopSeatsNamed();
				}*/
				int sharedServerSeats = lic.getServerSeats() - professionalSeats;
				int consumedProfessionalSeats = 0;
				
				int totalConsumedSeats = lic.getConsumed(Client.NFat_Client_Type) + lic.getConsumed(Client.NWeb_Client_Type) + lic.getConsumed(Client.NWebL_Client_Type) + lic.getConsumed(Client.sdk_Client_Type) + lic.getConsumed(Client.WebAccess_Client_Type) + lic.getConsumed(Client.Professional_WebAccess_Client_Type);;
				//System.out.println("totalConsumedSeats :: "+totalConsumedSeats);
				//System.out.println("sharedServerSeats :: "+sharedServerSeats);
				/*if(totalConsumedSeats > sharedServerSeats){
					consumedProfessionalSeats = totalConsumedSeats - sharedServerSeats;					
				}else if(lic.getConsumed(Client.WebAccess_Client_Type) > lic.getWebAccessSeats()){
					consumedProfessionalSeats = lic.getConsumed(Client.WebAccess_Client_Type) - lic.getWebAccessSeats();				
				}*/
		     	int totalSharedConsumedSeats = totalConsumedSeats-lic.getConsumed(Client.Professional_WebAccess_Client_Type);
				int sharedWebaccessSeats = lic.getWebAccessSeats();
				/*if(lic.getWebAccessSeats() > 0 && lic.getWebAccessSeats() > lic.getDesktopSeatsNamed()){
					sharedWebaccessSeats = lic.getWebAccessSeats() - professionalSeats;
				}else{
					sharedWebaccessSeats = lic.getWebAccessSeats();
				}*/
				
				/*
				 * if(lic.getConsumed(Client.WebAccess_Client_Type) <= professionalSeats){
					consumedProfessionalSeats = lic.getConsumed(Client.WebAccess_Client_Type);
					
				}
				else if(lic.getConsumed(Client.WebAccess_Client_Type) > professionalSeats){
					consumedProfessionalSeats = professionalSeats;
					consumedWebAccessSeats = lic.getConsumed(Client.WebAccess_Client_Type) - consumedProfessionalSeats;
				}
				 */
				
				
				if(lic.getConsumed(Client.Professional_WebAccess_Client_Type) <= professionalSeats){
					consumedProfessionalSeats = lic.getConsumed(Client.Professional_WebAccess_Client_Type);
				}
				else if(lic.getConsumed(Client.Professional_WebAccess_Client_Type) >professionalSeats){
					consumedProfessionalSeats = professionalSeats;
					//consumedWebAccessSeats = lic.getConsumed(Client.WebAccess_Client_Type) - consumedProfessionalSeats;
					
				}

				if(lic.getConsumed(Client.WebAccess_Client_Type) <= lic.getWebAccessSeats()){
					consumedWebAccessSeats = lic.getConsumed(Client.WebAccess_Client_Type);
				}
				else if(lic.getConsumed(Client.WebAccess_Client_Type) > lic.getWebAccessSeats()){
					consumedWebAccessSeats = lic.getWebAccessSeats();
					//consumedWebAccessSeats = lic.getConsumed(Client.WebAccess_Client_Type) - consumedProfessionalSeats;
				}

				
				/*code added by Madhavan
				if(lic.getConsumed(Client.WebAccess_Client_Type) <= sharedWebaccessSeats){
					//consumedProfessionalSeats = professionalSeats;
					consumedWebAccessSeats =lic.getConsumed(Client.WebAccess_Client_Type);
					System.out.println("Consumed webAccess seats::::----"+consumedWebAccessSeats);
				}
				if(lic.getConsumed(Client.Professional_WebAccess_Client_Type) <= professionalSeats){
					//System.out.println("Inside staement of professional");
					consumedProfessionalSeats = lic.getConsumed(Client.Professional_WebAccess_Client_Type);
					
				}
				*/
				
			    /**
				 * 1 CustomLabelWebAccessSeat = 25 ConcurrentUsers.
				 * For Custom label web access, 1 seat = 25 concurrent users. i.e. group of 25 users will consume 1 web access seat.
				 */
				int consumedCLWebAccessUsers = lic.getConsumed(Client.Custom_Label_WebAccess_Client_Type);
			
					totalCLWebAccessUsers = lic.getCustomLabelWebAccessSeats(); //based on registry for single publicwebaccess.
				/*}else{*/
					//totalCLWebAccessUsers = lic.getCustomLabelWebAccessSeats() * 25; //change to 25
				//}
				VWSLog.dbg("consumedCLWebAccessUsers :::::::::"+consumedCLWebAccessUsers);
				int consumedCLWebAccessSeats = (consumedCLWebAccessUsers > 0) ? consumedCLWebAccessUsers : 0; // change to 25
				int maxEnterpriseNamed=getMaxEnterpriceNamed(lic.getDesktopSeatsNamed(),lic.getWebtopSeatsNamed(),lic.getWebliteSeatsNamed(),sharedWebaccessSeats);
				int maxEnterpriseConcurrent=getMaxEnterpriceConcurrent(lic.getDesktopSeatsConcurrent(),lic.getWebliteSeatsNamed(),lic.getWebliteSeatsConcurrent(),lic.getWebaccessSeatsConcurrent());
				int totalConsumedEnterpriseNamed=lic.getConsumed(Client.NFat_Client_Type) +lic.getConsumed(Client.NWeb_Client_Type)+lic.getConsumed(Client.NWebL_Client_Type)+lic.getConsumed(Client.WebAccess_Client_Type);
				int totalConsumedEnterpriseConcurrent=lic.getConsumed(Client.Fat_Client_Type)+lic.getConsumed(Client.Web_Client_Type)+lic.getConsumed(Client.WebL_Client_Type)+lic.getConsumed(Client.Concurrent_WebAccess_Client_Type);
				int desktopConcurrent=0;
				String SignWiseName="";
				if(lic.getDesktopSeatsConcurrent() > 0 )
					desktopConcurrent=lic.getConsumed(Client.Fat_Client_Type);
				if(getSigWiseLicense()==1)
					SignWiseName=resourceManager.getString("SignWiseEnabled");
				else 
					SignWiseName=resourceManager.getString("SignWiseDisabled");
					
					
	
				String licenseInfo = "<html>"
									+ "<body style='margin-left:8px;margin-top:8px;'>"
									+ "<span style='font-size:11.0pt;font-family:Arial;'>"+resourceManager.getString("LicenseToolTip.Licensedfor")+"</span> "
									+ "<span style='font-size:11.0pt;font-family:Arial;color:#333333'> " + lic.getCompanyName() + "<br>" 
									+ "<table border='0' style='font-size:11.0pt;font-family:Arial;color:#333333' width='300px'>"
									+ "<tr>"
										+ "<td width='20%' style='font-weight:bold;'>"+resourceManager.getString("LicenseToolTip.Servers")+"</td>"
										+ "<td width='10%'>" + 	lic.getConsumed(Client.All_Client_Type) + " / " + lic.getServerSeats() + "</td>"
									+ "</tr>" 
									+ "<tr>"
										+ "<td width='35%' style='color:red;'>"+resourceManager.getString("LicenseToolTip.EnterpriseNamed")+"</td>"
										+ "<td width='16%'>" + "</td>"
										+ "<td width='36%'>"+resourceManager.getString("LicenseToolTip.Administration")+"</td>"
										+ "<td width='13%'>"   +lic.getConsumed(Client.Adm_Client_Type) + " / " +  lic.getAdminWiseSeats()  + "</td>"
									+ "</tr>" 
									+ "<tr>"
										/*+ "<td width='35%' style='text-align:center;font-weight:bold;'>"+resourceManager.getString("LicenseToolTip.Desktop")*/
										+ "<td width='35%' style='margin-left:50px;'>"+resourceManager.getString("LicenseToolTip.Desktop")
										+ "<td width='16%' >" + lic.getConsumed(Client.NFat_Client_Type) + " / " +lic.getDesktopSeatsNamed()+ "</td>"
										+ "<td width='36%'>"+resourceManager.getString("LicenseToolTip.SubAdministrator")+"</td>"
										+ "<td width='13%'>"   + lic.getConsumed(Client.SubAdm_Client_Type) +  " / " + lic.getSubAdminSeats() +  "</td>" 
									+ "</tr>"
									+ "<tr>"
									/*+"<td width='35%' style='text-align:center;font-weight:bold;'>"+resourceManager.getString("LicenseToolTip.Online")+"</td>"*/
									+"<td width='35%' style='margin-left:50px;'>"+resourceManager.getString("LicenseToolTip.Online")+"</td>"
									+ "<td width='16%'>"   +lic.getConsumed(Client.NamedOnline_WebAccess_Client_Type) + " / " +  lic.getNamedOnlineWebaccessSeats()  + "</td>"
									+ "<td width='36%'>"+resourceManager.getString("LicenseToolTip.ARS")+"</td>"
									+ "<td width='13%'>"   + lic.getConsumed(Client.Ars_Client_Type) +  " / " + lic.getArsSeats() + "</td>"
									+ "</tr>"
									+ "<tr>"
										+ "<td width='35%' style='color:red;'>"+resourceManager.getString("LicenseToolTip.Professional")+"</td>"
										+ "<td width='16%'>"   + consumedProfessionalSeats + " / " +  professionalSeats + "</td>" 
										+"<td width='36%'>"+resourceManager.getString("LicenseToolTip.Indexer")+"</td>"
										+ "<td width='13%'>"   + lic.getConsumed(Client.IXR_Client_Type) +  " / " + lic.getIndexerSeats()+ "</td>"
									+ "</tr>" 
									+ "<tr>"
										+ "<td width='35%'>"+"</td>"
										+ "<td width='16%'>" + "</td>"
										+ "<td width='36%'>"+resourceManager.getString("LicenseToolTip.Notification")+"</td>"
										+ "<td width='13%'>"   +lic.getConsumed(Client.Notification_Client_Type) + " / " +  lic.getNotificationSeats()  + "</td>"
									+ "</tr>"
									+ "<tr>"
										+ "<td width='35%'>"+"</td>"
										+ "<td width='16%'></td>"
										+ "<td width='36%'>"+resourceManager.getString("LicenseToolTip.DWS")+"</td>"
										+ "<td width='13%'>"  + (lic.getDrsSeats() > 0 ? (totalConsumedEnterpriseNamed+consumedProfessionalSeats+desktopConcurrent+consumedCLWebAccessSeats)+ " / " + (maxEnterpriseNamed+professionalSeats+lic.getDesktopSeatsConcurrent()+lic.getCustomLabelWebAccessSeats()) : "0 / 0") + "</td>" 
									+ "</tr>" 
									+ "<tr>"
										+ "<td width='35%' style='color:red;'>"+resourceManager.getString("LicenseToolTip.EnterpriseConcurrent")+"</td>"
										+ "<td width='16%'>" + "</td>"
										+ "<td width='36%'>"+resourceManager.getString("LicenseToolTip.CSServer")+"</td>"
										+ "<td width='13%'>"   + lic.getConsumed(Client.ContentSentinel_client_Type) +  " / " + lic.getCsserverSeats()+ "</td>"
									//"<tr><td width='45%'>"+resourceManager.getString("LicenseToolTip.EnterpriseConcurrent")+"</td><td width='15%'>" + totalConsumentEnterpriseConcurrent+ " / " +  maxEnterpriseConcurrent + "</td>" +
									// CV83 B2"<td width='25%'>"+resourceManager.getString("LicenseToolTip.DWS")+"</td><td width='25%'>"  + (lic.getDrsSeats() > 0 ? totalSharedConsumedSeats + " / " + sharedServerSeats : "0 / 0") + "</td>" + "</tr>"+
									+ "</tr>"
									+ "<tr>"
										+ "<td width='35%' style='margin-left:50px;'>"+resourceManager.getString("LicenseToolTip.Desktop")	
										+ "<td width='16%'>" + lic.getConsumed(Client.Fat_Client_Type) + " / " +lic.getDesktopSeatsConcurrent()+ "</td>"
										+ "<td width='36%'>"+"</td>"
										+ "<td width='13%'></td>"
									+ "</tr>" 
									+ "<tr>"
										+ "<td width='35%' style='margin-left:50px;'>"+resourceManager.getString("LicenseToolTip.Online")+"</td>"
										+ "<td width='16%'>"   +lic.getConsumed(Client.Concurrent_WebAccess_Client_Type) + " / " +  lic.getWebaccessSeatsConcurrent()  + "</td>"
										+ "<td width='36%'>"+"</td>"
										+ "<td width='13%'></td>"
									+ "</tr>" 
									+ "<tr>"
										+ "<td width='40%' style='color:red;'>"+resourceManager.getString("LicenseToolTip.ProfConcrWebAccess")+"</td>"
										+ "<td width='16%'>"   + lic.getConsumed(Client.Professional_Conc_WebAccess_Client_Type) +  " / " + lic.getProfessionalConcWebAccessSeats()+ "</td>" 
										+ "<td width='31%'>"+"</td>"
										+ "<td width='13%'></td>"
									+ "</tr>"
									+ "<tr>"
										+ "<td width='35%'>"+ "</td>"
										+ "<td width='16%'>"+ "</td>"
										+ "<td width='36%'>"+ "</td>"
										+ "<td width='13%'>"+ "</td>"
									+ "</tr>" 
									+ "<tr>"
										+ "<td width='35%' >"+resourceManager.getString("LicenseToolTip.CSWebaccess")+"</td>"
										+ "<td width='16%'>"   + lic.getConsumed(Client.CSWebaccess_client_Type) +  " / " + lic.getCsswebaccessSeat()+ "</td>"
										+ "<td width='36%'>"+resourceManager.getString("LicenseToolTip.AIP")+"</td>"
										+ "<td width='13%'>"   + lic.getConsumed(Client.AIP_Client_Type) +  " / " + lic.getAipSeats()  + "</td>"
									+ "</tr>" 
									//(lic.getDesktopSeatsConcurrent() > 0 ? 
									+ "<tr>"
										+ "<td width='35%'>"+resourceManager.getString("LicenseToolTip.CustomLabelWebAccess")+"</td>"
										+ "<td width='16%'>"   + consumedCLWebAccessSeats +  " / " + totalCLWebAccessUsers+ "</td>" 
										/*+ "<td width='35%'>"+resourceManager.getString("LicenseToolTip.SDK")+"</td>"
										+ "<td width='20%'>"  + lic.getConsumed(Client.sdk_Client_Type) +  " / " + lic.getSdkSeats() + "</td>"*/ 
										+ "<td width='36%'>"+resourceManager.getString("LicenseToolTip.EAS")+"</td>"
										+ "<td width='13%'>"  + lic.getConsumed(Client.EAS_Client_Type) +  " / " + lic.getEasSeats() + "</td>"
									+ "</tr>"
									+ "<tr>"
										+ "<td width='35%'>"+ "</td>"
										+ "<td width='16%'>"+ "</td>"
										+ "<td width='36%'>"+ "</td>"
										+ "<td width='13%'>"+ "</td>"
									+ "</tr>" 
									+ "<tr>"
										+ "<td widht='50%'>"+SignWiseName+"</td>"
									+ "</tr>";
									//"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.AIP")+"</td><td width='15%'>"   + lic.getConsumed(Client.AIP_Client_Type) +  " / " + lic.getAipSeats()  + "</td>" +
								    //"<td width='35%'>"+resourceManager.getString("LicenseToolTip.WebAccess")+"</td><td width='15%'>"   + consumedWebAccessSeats +  " / " + sharedWebaccessSeats + "</td></tr>" +
									//"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.WebTopNamed")+"</td><td width='15%'>"  +lic.getConsumed(Client.NWeb_Client_Type) + " / " + lic.getWebtopSeatsNamed()  + "</td>" +
									//"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.WebLiteNamed")+"</td><td width='15%'>"  +lic.getConsumed(Client.NWebL_Client_Type) + " / " + lic.getWebliteSeatsNamed()  + "</td>" +
									//(lic.getWebtopSeatsConcurrent() > 0 ? "<td width='25%'>"+resourceManager.getString("LicenseToolTip.WebTopConcurrent")+"</td><td width='25%'>"   + lic.getConsumed(Client.Web_Client_Type) + " / " + lic.getWebtopSeatsConcurrent() +  "</td></tr>" : "</tr>") +
									//(lic.getWebliteSeatsConcurrent() > 0 ? "<td width='25%'>"+resourceManager.getString("LicenseToolTip.WebLiteConcurrent")+"</td><td width='25%'>"   + lic.getConsumed(Client.WebL_Client_Type) + " / " + lic.getWebliteSeatsConcurrent() +  "</td></tr>" : "</tr>");
									//	epCVLicense.setToolTipText(licenseInfo);
				bInformation.setToolTipText(licenseInfo);
			
				int dismissDelay = ToolTipManager.sharedInstance().getDismissDelay();

			    // Keep the tool tip showing
			    dismissDelay = Integer.MAX_VALUE;
			    ToolTipManager.sharedInstance().setDismissDelay(dismissDelay);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	

	private void setLicenseInfo()
	{
		License lic = null;
		//epLogo.setText("");
		if (vws == null ) {
			LicenseManager lm =  LicenseManagerImpl.getInstance();
			if (lm == null)
			{
				tLicense.setToolTipText("Invalid " + PRODUCT_NAME + " License");
				return;
			}
			// this will force a license file read
			LicenseManagerImpl.setLicense();
			try
			{
				if (!lm.hasValidLicense())
				{
					tLicense.setToolTipText("License has expired");
					return;
				}
			}
			catch(LicenseException le)
			{
				tLicense.setToolTipText(le.getMessage());
				return;
			}
			try
			{
				lic = lm.getLicense();
			}
			catch(LicenseException le){}
		}else {
			try
			{
				lic = vws.getLicense();
			}        
			catch(Exception e){e.printStackTrace();}
		}
		if (lic == null)
		{
			tLicense.setToolTipText("Invalid " + PRODUCT_NAME + " License");
			//epLogo.setText("Invalid " + PRODUCT_NAME + " License");
			return;
		}
		String licenseInfo = "<html><body style='margin-left:8px;margin-top:8px;'><span style='font-size:11.0pt;font-family:Arial;'>"+resourceManager.getString("LicenseToolTip.Licensedfor")+"</span> <span style='font-size:11.0pt;font-family:Arial;color:#333333'> " + lic.getCompanyName() + "<br>" +
		"<table border='0' style='font-size:11.0pt;font-family:Arial;color:#333333' width='90%'><tr><td width='25%'>"+resourceManager.getString("LicenseToolTip.Servers")+"</td><td width='25%'>" + 	lic.getConsumed(Client.All_Client_Type) + " / " + lic.getServerSeats() + "</td><td width='25%'></td>" + 
		"<tr><td width='35%'>Desktop Concurrent:</td><td width='15%'>" + lic.getConsumed(Client.Fat_Client_Type) + " / " + lic.getDesktopSeatsConcurrent() + "</td>"+
		"</td>"+resourceManager.getString("LicenseToolTip.DesktopNamed")+"</td><td width='25%'>" + lic.getConsumed(Client.NFat_Client_Type) + " / " +  lic.getDesktopSeatsNamed() + "</td></tr>"+ 
		"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.WebTopConcurrent")+"</td><td width='15%'>"  +lic.getConsumed(Client.Web_Client_Type) + " / " + lic.getWebtopSeatsConcurrent()  + "</td>" +
		"<td width='25%'>"+resourceManager.getString("LicenseToolTip.WebTopNamed")+"</td><td width='25%'>"   + lic.getConsumed(Client.NWeb_Client_Type) + " / " + lic.getWebtopSeatsNamed() +  "</td></tr>" +
		"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.WebLiteConcurrent")+"</td><td width='15%'>"  +lic.getConsumed(Client.WebL_Client_Type) + " / " + lic.getWebliteSeatsConcurrent()  + "</td>" +
		"<td width='25%'>"+resourceManager.getString("LicenseToolTip.WebLiteNamed")+"</td><td width='25%'>"   + lic.getConsumed(Client.NWebL_Client_Type) + " / " + lic.getWebliteSeatsNamed() +  "</td></tr>" +        	              
		"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.Administration")+"</td><td width='15%'>"   +lic.getConsumed(Client.Adm_Client_Type) + " / " +  lic.getAdminWiseSeats()  + "</td>" +
		"<td width='25%'>"+resourceManager.getString("LicenseToolTip.Indexer")+"</td><td width='25%'>"   + lic.getConsumed(Client.IXR_Client_Type) +  " / " + lic.getIndexerSeats() + "</td></tr>" +
		"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.AIP")+"</td><td width='15%'>"   + lic.getConsumed(Client.AIP_Client_Type) +  " / " + lic.getAipSeats()  + "</td>" +
		"<td width='25%'>"+resourceManager.getString("LicenseToolTip.EAS")+"</td><td width='25%'>"  + lic.getConsumed(Client.EAS_Client_Type) +  " / " + lic.getEasSeats() + "</td></tr>" +
		"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.ARS")+"</td><td width='15%'>"   + lic.getConsumed(Client.Ars_Client_Type) +  " / " + lic.getArsSeats() +  "</td>" +
		"<td width='25%'>"+resourceManager.getString("LicenseToolTip.DWS")+"</td><td width='25%'>"  + lic.getConsumed(Client.Drs_Client_Type) +  " / " + lic.getDrsSeats() + "</td></tr>" +      	              
		"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.WebAccess")+"</td><td width='15%'>"   + lic.getConsumed(Client.WebAccess_Client_Type) +  " / " + lic.getWebAccessSeats()+ "</td>" +
		"</td>"+resourceManager.getString("LicenseToolTip.SDK")+"</td><td width='25%'>"  + lic.getConsumed(Client.sdk_Client_Type) +  " / " + lic.getSdkSeats() + "</td></tr>" +
		"<tr><td width='35%'>"+resourceManager.getString("LicenseToolTip.Notification")+"</td><td width='15%'>"   + lic.getConsumed(Client.Notification_Client_Type) +  " / " + lic.getNotificationSeats()+ "</td></tr>" + 
		"</table></span></html>";
		tLicense.setToolTipText(licenseInfo);
		//epLogo.setText(licenseInfo);
	}

	/**
	 * setCVLicenseInfo() : is to show the license info for Contentverse License.
	 *
	 */
	private void setCVLicenseInfo()
	{
		License lic = null;
		//epLogo.setText("");
		if (vws == null ) {
			LicenseManager lm =  LicenseManagerImpl.getInstance();
			if (lm == null)
			{
				tLicense.setToolTipText("Invalid " + PRODUCT_NAME + " License");
				return;
			}
			// this will force a license file read
			LicenseManagerImpl.setLicense();
			try
			{
				if (!lm.hasValidLicense())
				{
					tLicense.setToolTipText("License has expired");
					return;
				}
			}
			catch(LicenseException le)
			{
				tLicense.setToolTipText(le.getMessage());
				return;
			}
			try
			{
				lic = lm.getLicense();
			}
			catch(LicenseException le){}
		}else {
			try
			{
				lic = vws.getLicense();
			}        
			catch(Exception e){e.printStackTrace();}
		}
		if (lic == null)
		{
			tLicense.setToolTipText("Invalid " + PRODUCT_NAME + " License");
			//epLogo.setText("Invalid " + PRODUCT_NAME + " License");
			return;
		}
		
		//int professionalSeats = lic.getServerSeats() - lic.getDesktopSeatsNamed();
		int professionalSeats = lic.getProfessionalWebAccessSeats();
		int publicWebaccessSeats=lic.getCustomLabelWebAccessSeats();
		int sharedServerSeats = lic.getServerSeats() - professionalSeats;
		int consumedProfessionalSeats = 0;
		int totalConsumedSeats = lic.getConsumed(Client.NFat_Client_Type) + lic.getConsumed(Client.NWeb_Client_Type) + lic.getConsumed(Client.NWebL_Client_Type) + lic.getConsumed(Client.sdk_Client_Type) + lic.getConsumed(Client.WebAccess_Client_Type);
		/*if(totalConsumedSeats > sharedServerSeats){
			consumedProfessionalSeats = totalConsumedSeats - sharedServerSeats;
		}*/
		int totalSharedConsumedSeats = totalConsumedSeats ;
		//String licenseInfo = "<html><body style='margin-left:9px;margin-top:8px;'><span style='font-size:11.0pt;font-family:Arial;'><b>"+resourceManager.getString("LicenseLbl.Licensedfor")+"</b></span> <span style='font-size:11.0pt;font-family:Arial;color:#333333'> " + lic.getCompanyName() + "<br>" +
		String licenseInfo = "<html><body style='margin-left:9px;margin-top:8px;'><span style='font-size:11.0pt;font-family:Arial;'>"+resourceManager.getString("LicenseLbl.Licensedfor")+"</span> <span style='font-size:11.0pt;font-family:Arial;color:#333333'> " + lic.getCompanyName() + "<br>" +
		"<table border='0' style='font-size:11.0pt;font-family:Arial;color:#333333' width='90%'>" +
		"<tr> <td width='30%' style='padding-left: 60px;'> "+resourceManager.getString("LicenseLbl.Users")+"</td><td width='70%'>" + lic.getConsumed(Client.All_Client_Type) + " / " + lic.getServerSeats() + "</td></tr>" + 
		//Commented and modified to display the workflow seats same as server seats,Gurumurthy.T.S 22-05-2014
		//"<tr><td width='30%'>"+resourceManager.getString("LicenseLbl.Workflow")+"</td><td width='70%'>"  + (lic.getDrsSeats() > 0 ? totalSharedConsumedSeats + " / " + sharedServerSeats : "0 / 0") + "</td></tr>" +
		
		//"<tr><td width='30%'>"+resourceManager.getString("LicenseLbl.Workflow")+"</td><td width='70%'>" + lic.getConsumed(Client.All_Client_Type) + " / " + lic.getServerSeats() + "</td></tr>" +
		"<tr><td width='60%' style='padding-left: 60px;'>"+resourceManager.getString("LicenseLbl.AutoDataImport")+"</td><td width='70%'>"   + lic.getConsumed(Client.AIP_Client_Type) +  " / " + lic.getAipSeats()  + "</td></tr>" +
		"</table></span></html>";

		epCVLicense.setText(licenseInfo);
		//tLicense.setToolTipText(licenseInfo);
		//epLogo.setText(licenseInfo);
	}


	private void addDocListeners()
	{
		tLicense.getDocument().addDocumentListener(new DocListener(bRegister1));
		// Email Server changes 30 Jan 2007 
		tNamedUsers.getDocument().addDocumentListener(new DocListener(bRegister1));
		tNamedOnline.getDocument().addDocumentListener(new DocListener(bRegister1));
		tProfessionalNamedUsers.getDocument().addDocumentListener(new DocListener(bRegister1));
		tEnterpriceConcurrent.getDocument().addDocumentListener(new DocListener(bRegister1));
		tConcurrentOnline.getDocument().addDocumentListener(new DocListener(bRegister1));
		tProfessionalConcurrent.getDocument().addDocumentListener(new DocListener(bRegister1));
		tPublicWA.getDocument().addDocumentListener(new DocListener(bRegister1));
		tSecurityAdmin.getDocument().addDocumentListener(new DocListener(bRegister1));
		tBatchInput.getDocument().addDocumentListener(new DocListener(bRegister1));
	}
	
	private boolean checkRegisteredGroupsAreValid(JTextField groupField, String msg) {
    	if (groupField.getText().trim().length() > 0) {
    		if ((tNamedUsers.getText().trim().length() > 0 && tNamedUsers.getText().equalsIgnoreCase(groupField.getText()))
					|| (tNamedOnline.getText().trim().length() > 0 && tNamedOnline.getText().equalsIgnoreCase(groupField.getText()))
					|| (tProfessionalNamedUsers.getText().trim().length() > 0 && tProfessionalNamedUsers.getText().equalsIgnoreCase(groupField.getText()))
					|| (tEnterpriceConcurrent.getText().trim().length() > 0 && tEnterpriceConcurrent.getText().equalsIgnoreCase(groupField.getText())
					|| (tConcurrentOnline.getText().trim().length() > 0 && tConcurrentOnline.getText().equalsIgnoreCase(groupField.getText())))
					|| (tProfessionalConcurrent.getText().trim().length() > 0 && tProfessionalConcurrent.getText().equalsIgnoreCase(groupField.getText()))) {
	            Util.Msg(this, msg, MANAGER_APP); 
	            groupField.grabFocus(); 
	            return false;
    		} else 
    			return true;
        } else
            return true;
    }

	private void setRoomLevelBatchInputAdministrator() {
		try {
			RoomProperty[] rooms = VWSPreferences.getRegRooms();
			if (rooms != null && rooms.length > 0) {
				for (int i = 0; i < rooms.length; i++) {
					String batchInputAdminGrp = VWSPreferences.getRoomsBatchInputAdministrator(rooms[i].getName());
					System.out.println("batchInputAdminGrp" + batchInputAdminGrp);
					if (batchInputAdminGrp.equalsIgnoreCase(VWSPreferences.getBatchInputAdministrator())) {
						System.out.println("batchInputAdministrator inside for loop" + batchInputAdminGrp);
						String roomBatchAdmin = rooms[i] + "#" + tBatchInput.getText();
						System.out.println("room batch input administrator :" + roomBatchAdmin);
						VWSPreferences.setRoomsBatchInputAdministrator(roomBatchAdmin);
					}
				}
			}
		} catch (Exception e) {
		}
	}

	int iRegLeft = 400, iRegTop = 463, iRegWidth = 90, iRegHeight = 23;

	//private JEditorPane epLogo;
	private JEditorPane epCVLicense;
	private VWPanel pLicense, pRRooms; 
	JLabel lLicenseTitile,lLicense,lNamedDesktop,lConcurrentDesktop,lNamedOnline,lConcurrentOnline,lPublicWA, lNamedUsers,lProfessionalNamedUsers,lSecurityAdmin,lEnterpriceConcurrent,lProfessionalConcurrent,lGeneralHelp, lLicenseSummary,lLineBorder,lBatchInput;
	private JTextField tLicense,tNamedUsers,tNamedOnline,tConcurrentOnline,tProfessionalNamedUsers,tSecurityAdmin,tPublicWA,tEnterpriceConcurrent,tProfessionalConcurrent,tBatchInput;
	private JButton bBrowse, bRegister1,bInformation;
	private JScrollPane spRooms, spRRooms;
	private VWS vws;
	VWSPreferences prefs = null;
}
