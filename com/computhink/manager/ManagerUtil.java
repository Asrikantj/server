/*
 * VWSUtil.java
 *
 * Created on 22 ����� ������, 2003, 03:28 �
 */
package com.computhink.manager;
import com.computhink.common.*;
/**
 *
 * @author  Administrator
 */
import java.util.*;
import java.io.File;
import java.net.*;

public class ManagerUtil 
{
    public static String home = "";
    public static native int serviceSwitch
                                       (int state, String host, String service);
    
    static
    {
        findHome();
        if (Util.getEnvOSName().startsWith("Windows")) loadNativeLibrary();
    }
    private static boolean loadNativeLibrary()
    {
        try
        {
        	File utilDll = new File(home + Util.pathSep 
                    + "system" + Util.pathSep + "VWJUtil.dll");
        	if (utilDll.exists())
        		System.load(home + Util.pathSep 
                                     + "system" + Util.pathSep + "VWJUtil.dll");
        	else
        		return false; 
        }
        catch (Exception e)
        {
            return false;
        }
         return true;
    }
    public static String getHome()
    {
        return home;
    }
    public static int switchWindowsService(int state, String host, String svcName)
    {
        if (!Util.getEnvOSName().startsWith("Windows")) return -1;
        return serviceSwitch(state, host, svcName);
    }
    private static void findHome()
    {
        String  urlPath = "";
        String  indexerHome = "";
        String res = "com/computhink/manager/image/images/mgr.gif";
        String jarres = "/lib/ViewWise.jar!";
        
        int startIndex = 0;
        int endIndex = 0;
        try
        {
            URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
            urlPath = url.getPath();
        }
        catch(Exception e){urlPath="";}
        if (!urlPath.equalsIgnoreCase(""))
        {
            if (urlPath.startsWith("file"))
            {
                //file:/c:/program%20files/viewwise%20indexer/lib/viewwiseindexer.jar!/image/indexer.gif
                startIndex = 6;
                endIndex = urlPath.length() - res.length() - jarres.length();
            }
            else
            {
                ///c:/program%20files/viewwise%20indexer/Classes/indexer.gif
                startIndex = 1;
                endIndex = urlPath.length() - res.length();
            }
            indexerHome = urlPath.substring(startIndex, endIndex-1);  
            indexerHome = indexerHome.replace('/', '\\');
        }
        System.out.println("room name in managerUtil, indexerHome is : "+indexerHome);
        home = cleanUP(indexerHome);
    }
    private static String cleanUP(String str) // removes %20    
    {
       String cleanStr = ""; 
       StringTokenizer st = new StringTokenizer(str, "%20");
       while (st.hasMoreElements())
       {
           cleanStr = cleanStr + st.nextToken() + " ";
       }
       return (cleanStr.equalsIgnoreCase("")? str : cleanStr.trim());
    }
}
