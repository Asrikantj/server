package com.computhink.manager;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.NumberFormatter;
import javax.swing.text.html.HTMLEditorKit;

import com.computhink.common.CIServer;
import com.computhink.common.RoomProperty;
import com.computhink.common.Util;
import com.computhink.manager.image.Images;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWCreateDB;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSPreferences;

public class VWRoomPanel extends VWPanel implements ManagerConstants {
    VWSPreferences prefs = null;
    boolean showRoomLevelNamedGrp = false;
    private static ResourceManager resourceManager=null;    
    public VWRoomPanel(CIServer server) {
		// TODO Auto-generated constructor stub
		super();
		this.vws = (VWS) server;
		if (vws == null){
		    prefs = new VWSPreferences();
		}
		initComponents();		
		addDocListeners();
		loadDBEngines();
		//System.out.println("synchronizing the Rooms");
		synchronizeRooms();
	}

    public VWRoomPanel() {
		// TODO Auto-generated constructor stub
		super();
		prefs = new VWSPreferences();
		vws = null;
		initComponents();
		addDocListeners();
	}
    private void initComponents() 
    {
    	showRoomLevelNamedGrp = VWSPreferences.getRoomLevelNamed();
    	resourceManager=ResourceManager.getDefaultManager();
    	setLayout(null);
    	pRooms = new VWPanel();
    	pRRooms = new VWPanel();
    	pRooms.setLayout(null);
    	pDatabase = new VWPanel();
    	pBypass = new VWPanel();

    	cbBypass = new JCheckBox(resourceManager.getString("RoomChk.BypassSecurity"));
    	spRooms = new JScrollPane();
    	//spRRooms = new JScrollPane();
    	lstRooms = new JList();
    	lstRooms.setCellRenderer(new VWListCellRenderer());
    	lstRooms.addMouseListener(new MouseAdapter(){
    	    public void mouseClicked(MouseEvent e) {
    		if (e.getClickCount() > 1){
    		    int position = lstRooms.getSelectedIndex();
    		    switchRoomState(position);
    		}
    	    }
    	});
    	/*
    	 * setting values for the JList with image icon
    	 */
    	Vector rooms = new Vector();
    	/*Vector rooms = new Vector(); 
    	rooms.add("VWRoom1");
    	rooms.add("VWRoom2");
    	Images im = new Images();
    	
    	JPanel jp1 = new JPanel();
    	jp1.add(new JLabel(im.app));
    	jp1.add(new JLabel(rooms.get(0).toString()));
    	vRooms.addElement(jp1);

    	JPanel jp2 = new JPanel();
    	jp2.add(new JLabel(im.stop));
    	jp2.add(new JLabel(rooms.get(1).toString()));
    	vRooms.addElement(jp2);*/
    	
        //-------
        
    	tPass = new JPasswordField();

    	bAdd = new JButton(resourceManager.getString("RoomBtn.Add"));
    	bRemove = new JButton(resourceManager.getString("RoomBtn.Remove"));
    	bRegister3 = new JButton(resourceManager.getString("RoomBtn.Apply"));
    	bCreateDB = new JButton();
    	//bBrowse = new JButton(); 

    	tHost = new JTextField();
    	tUser = new JTextField();
    	tAdmins = new JTextField();
    	tSubAdmins = new JTextField();
    	tNamed = new JTextField();
    	tNamedOnline=new JTextField();
    	tProfessionalNamed = new JTextField();
    	tEnterPriseConcurrent = new JTextField();
    	tProfessionalConcurrent = new JTextField();
    	tConcurrentOnline=new JTextField();
    	tName = new JTextField();
    	tBatchInput = new JTextField();
    	tDBName = new JTextField();
    	tKey = new JTextField();    	

    	lMinutes = new JLabel(resourceManager.getString("RoomLbl.Minutes"));
    	lIdle = new JLabel(resourceManager.getString("RoomLbl.IdleTimeout"));
    	lPass = new JLabel(resourceManager.getString("RoomLbl.Password"));
    	lEngine = new JLabel(resourceManager.getString("RoomLbl.DBEngine"));
    	lHost = new JLabel(resourceManager.getString("RoomLbl.DBHost"));
    	lPort = new JLabel(resourceManager.getString("RoomLbl.DBPort"));
    	lUser = new JLabel(resourceManager.getString("RoomLbl.DBUser"));
    	lKey = new JLabel(resourceManager.getString("RoomLbl.DocEncryptKey"));
    	lDBName = new JLabel(resourceManager.getString("RoomLbl.DBName"));
    	lAdmins = new JLabel(resourceManager.getString("RoomLbl.AdminGroup"));
    	lSubAdmins = new JLabel(resourceManager.getString("RoomLbl.SubAdminGroup"));
    	lNamed = new JLabel(resourceManager.getString("RoomLbl.NamedGroup"));
    	lNamedOnline = new JLabel(resourceManager.getString("RoomLbl.NamedOnlineGroup"));
    	
    	lProfessionalNamed = new JLabel(resourceManager.getString("RoomLbl.ProfessionalNamedGroup"));
    	lEnterPriseConcurrent= new JLabel(resourceManager.getString("RoomLbl.EnterpriseConcurrent"));
    	lConcurrentOnline=new JLabel(resourceManager.getString("RoomLbl.EnterpriseConcurrentOneline"));
    	lProfessionalConcurrent= new JLabel(resourceManager.getString("RoomLbl.professionalConcurrent"));
    	lBatchInput = new JLabel(resourceManager.getString("RoomLbl.BatchInput"));
    	lName = new JLabel(resourceManager.getString("RoomLbl.RoomName"));
    	lDBScheduler = new JLabel(resourceManager.getString("RoomLbl.ResetDBCon"));
    	lRegRooms = new JLabel(resourceManager.getString("RoomLbl.RegisteredRooms"));
    	comEngine = new JComboBox(vDBEngines);
    	epLogo = new JEditorPane();
    	epLogo.setEditorKit(new HTMLEditorKit()); 
    	epLogo.setEditable(false);
    	epLogo.setBorder(null);

    	minuteSpinner = new JSpinner();
    	jSpinner = new JSpinner();
    	tPort = new JFormattedTextField(getNF());
    	tPort.setText("1433");
    	
    	tIdle = new JFormattedTextField(getNF());
    	tIdle.setText("22");
    	// Database scheduler controls
    	lMinute = new JLabel (resourceManager.getString("RoomLbl.Minutes"));
    	ChkOpt1 = new JRadioButton();
    	ChkOpt2 = new JRadioButton();
    	group1 = new ButtonGroup(); 

    	group1.add(ChkOpt1);
    	group1.add(ChkOpt2);
    	ChkOpt1.setSelected(true);
    	dateModel = new SpinnerDateModel();
    	minuteModel = new SpinnerNumberModel(1, 1, 999, 1);

    	dateModel.setCalendarField(Calendar.HOUR_OF_DAY);
    	jSpinner = new JSpinner(dateModel);
    	//jSpinner.setBounds(280,55, 100, 20);
    	JSpinner.DateEditor editor2 = new JSpinner.DateEditor(jSpinner, "HH:mm:ss");
    	jSpinner.setEditor(editor2);		    
    	minuteSpinner = new JSpinner(minuteModel);
    	//minuteSpinner.setBounds(344,30,36, 20);
    	//	  Get the date formatter
    	JFormattedTextField tf = ((JSpinner.DefaultEditor)jSpinner.getEditor()).getTextField();
    	tf.setEditable(true);
    	//
    	lRegRooms.setBounds(10, 5, 100, 20);
    	pRooms.add(lRegRooms);
    	//lstRooms.setBackground(new Color(255, 255, 204));
    	lRegRooms.setForeground(Color.BLUE);
    	pRooms.add(spRooms); 

		if(showRoomLevelNamedGrp){	
			spRooms.setBounds(10, 25, 190, 270);
		}else{
			spRooms.setBounds(10, 25, 190, 230);
		}
    	spRooms.setViewportView(lstRooms);
    	//lstRooms.setBackground(new Color(255, 255, 204));
        //pRooms.add(spRooms); spRooms.setBounds(10, 25, 190, 210);
        //spRooms.setViewportView(lstRooms);

    	pDatabase.setLayout(null);
    	pDatabase.setBorder(new TitledBorder("  "+resourceManager.getString("RoomTitleBorder.Database")+" "+resourceManager.getString("RoomTitleBorder.Settings")+"  "));
    	iLeft = 15; iTop = 20; iHeight = 16; iWidth = 70;
		if(showRoomLevelNamedGrp){
    		pDatabase.setBounds(230, 227, 390, 208); 
		}else{
			pDatabase.setBounds(230, 177, 390, 215);
		}
    	lEngine.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lEngine);
    	iTop += 25;
    	lDBName.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lDBName);
    	iTop += 25;
    	
    /*	if(showRoomLevelNamedGrp){
			iTop1 = 0;
		}else{
			iTop1 = 5;
		}*/
    	
    	lHost.setBounds(iLeft, iTop+iTop1, iWidth, iHeight); pDatabase.add(lHost);
    	//iTop += 25;

    	lPort.setBounds(iLeft+210+34, iTop+iTop1, iWidth, iHeight); pDatabase.add(lPort);
    	iTop += 25;
    	lUser.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lUser);
    	iTop += 25;
    	lPass.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lPass);
    	//iLeft = iLeft + iWidth + 5;
    	iLeft = iLeft + iWidth;
    	iTop = 20; iHeight = 20; iWidth = 155;
    	comEngine.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(comEngine);
    	iTop += 25;
    	tDBName.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(tDBName);
    	iTop += 25;
    	tHost.setBounds(iLeft, iTop+iTop1, iWidth, iHeight); pDatabase.add(tHost);        
    	bCreateDB.setBounds(iLeft + iWidth + 4 + 115, iTop+iTop1, 18, 18); pDatabase.add(bCreateDB);
    	bCreateDB.setIcon(Images.rooms);
    	bCreateDB.setBorderPainted(false);
    	bCreateDB.setBackground(getBackground());
    	//bCreateDB.setBounds(iLeft + iWidth + 2, iTop, 20, 20); pDatabase.add(bCreateDB);
    	//bCreateDB.setIcon(Images.cdb);
    	//bCreateDB.setToolTipText("New Room Database");
    	//iTop += 25;
    	tPort.setBounds(iLeft+ iWidth+51, iTop+iTop1, 65, iHeight); pDatabase.add(tPort);
    	iTop += 25;
    	tUser.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(tUser);
    	iTop += 25;
    	tPass.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(tPass);
    	if(showRoomLevelNamedGrp){
    		iTop += 25;
    	}
    	else {
    		iTop += 45;	
    	}
    	
    	iLeft = 15;
    	comEngine.setBackground(new Color(255, 255, 204));  
    	iWidth = iWidth - 20;
    	if(showRoomLevelNamedGrp){
    		iTop = iTop;
		}else{
			iTop = iTop - 15;
		}
    	
    	lDBScheduler.setBounds(iLeft, iTop  , iWidth, iHeight);
    	ChkOpt1.setText(resourceManager.getString("RoomChk.RoomIdle")+" ");
    	ChkOpt1.setBackground(getVWColor());
    	ChkOpt2.setText(resourceManager.getString("RoomChk.Daily")+" ");
    	ChkOpt2.setBackground(getVWColor());
    	//iTop += 30;
    	iLeft = iLeft + iWidth;
    	ChkOpt1.setBounds(iLeft + 5, iTop, iWidth, iHeight);
    	ChkOpt2.setBounds(iLeft + 5, iTop+30, iWidth, iHeight);
    	iLeft = iLeft + iWidth - 4 ;  
    	minuteSpinner.setBounds(iLeft+10, iTop, 50, iHeight);
    	lMinute.setBounds(iLeft +10 + 52, iTop, 50, iHeight);
    	
    	iTop += 30;
    	//ChkOpt2.setBounds(iLeft, iTop+5, iWidth, iHeight);
    	//iLeft = iLeft + iWidth ; 
    	jSpinner.setBounds(iLeft + 10, iTop, 85, iHeight);
    	//iTop = iTop - 27; 
    	
    	
    	//iTop = iTop - 27;
    	 
    	

    	pDatabase.add(lDBScheduler);
    	pDatabase.add(ChkOpt1);
    	pDatabase.add(ChkOpt2);
    	pDatabase.add(minuteSpinner);
    	pDatabase.add(jSpinner);
    	pDatabase.add(lMinute);

    	pRooms.add(pDatabase);
    	pBypass.setLayout(null);
		
		if(showRoomLevelNamedGrp){
			pBypass.setBounds(10, 328, 210, 110); 
		}else{
			pBypass.setBounds(10, 285, 210, 110); 
		}
		pRooms.add(pBypass);
    	cbBypass.setBounds(0, 10, 120, 21); pBypass.add(cbBypass);
    	cbBypass.setBackground(getVWColor());
    	//CV10 Bypass Security Enhancement
    	cbBypass.setVisible(false);
    	lIdle.setBounds(0, 38, 70, 16); pBypass.add(lIdle);
    	tIdle.setBounds(70, 38, 37, 20); pBypass.add(tIdle);    	
    	lMinutes.setBounds(110, 38, 60, 16); pBypass.add(lMinutes);

    	lKey.setBounds(0, 68, 168, 16);pBypass.add(lKey);         
    	tKey.setBounds(0, 86, 190, 20);pBypass.add(tKey);


    	lName.setBounds(230, 10, 80, 16); pRooms.add(lName);        
    	tName.setBounds(230, 25, 190, 20); pRooms.add(tName);
    	lName.setForeground(Color.BLUE);
    	if(showRoomLevelNamedGrp){    
    		bAdd.setBounds(10, 305, 85, 23); 
    	}else{
    		bAdd.setBounds(10, 260, 85, 23);
    	}
    	pRooms.add(bAdd); 

    	if(showRoomLevelNamedGrp){
    		bRemove.setBounds(115, 305, 85, 23); 
    	}else{
    		bRemove.setBounds(115, 260, 85, 23);
    	}
    	pRooms.add(bRemove);   
    	if(showRoomLevelNamedGrp) {
    		iRegTop = 450;
    	}
    	bRegister3.setBounds(iRegLeft, iRegTop, iRegWidth, iRegHeight); pRooms.add(bRegister3); 
    	bRegister3.setBackground(getBackground());
    	
    	lAdmins.setForeground(Color.RED);
    	tAdmins.setForeground(Color.RED);
    	lAdmins.setBounds(430, 10, 168, 16); pRooms.add(lAdmins);
    	tAdmins.setBounds(430, 25, 190, 20); pRooms.add(tAdmins);
    	
    	//lSubAdmins.setForeground(Color.RED);
    	//tSubAdmins.setForeground(Color.RED);
    	lSubAdmins.setBounds(230, 50, 168, 16); pRooms.add(lSubAdmins);
    	lSubAdmins.setForeground(Color.BLUE);
    	tSubAdmins.setBounds(230, 65, 190, 20); pRooms.add(tSubAdmins);
    	
    	if(showRoomLevelNamedGrp) {
    		
        	//Batch Input Administrator Group
	        lBatchInput.setBounds(430, 50, 168, 16);
	        pRooms.add(lBatchInput);
	        tBatchInput.setBounds(430, 65, 190, 20);
	        pRooms.add(tBatchInput);
	        
	        //Named Group
	        lNamed.setBounds(230, 90+5, 168, 16);
	        pRooms.add(lNamed);
	        tNamed.setBounds(230, 105+5, 190, 20);
	        pRooms.add(tNamed);
	        
	        lNamedOnline.setBounds(230,130+5, 168, 16);
	        pRooms.add(lNamedOnline);
	        tNamedOnline.setBounds(230, 145+5, 190, 20); 
	        pRooms.add(tNamedOnline);
	        
	        //Professional Named Group
	        lProfessionalNamed.setBounds(230, 170+5, 168, 16);
	        pRooms.add(lProfessionalNamed);
	        tProfessionalNamed.setBounds(230, 185+5, 190, 20); 
	        pRooms.add(tProfessionalNamed);
	        
	        //Enterprise Concurrent Group
	        lEnterPriseConcurrent.setBounds(430, 90+5, 168, 16);
	        pRooms.add(lEnterPriseConcurrent);
	        tEnterPriseConcurrent.setBounds(430, 105+5, 190, 20); 
	        pRooms.add(tEnterPriseConcurrent);
	        
	        //lConcurrentOnline
	        
	        lConcurrentOnline.setBounds(430, 130+5, 168, 16);
	        pRooms.add(lConcurrentOnline);
	        tConcurrentOnline.setBounds(430, 145+5, 190, 20); 
	        pRooms.add(tConcurrentOnline);
	        
	      //Professional Concurrent Group
	        lProfessionalConcurrent.setBounds(430, 170+5, 168, 16);
	        pRooms.add(lProfessionalConcurrent);
	        tProfessionalConcurrent.setBounds(430, 185+5, 190, 20);
	        pRooms.add(tProfessionalConcurrent);
    	}
    	
        
    	bRegister3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                registerRoom();
                bRegister3.setEnabled(false);
            }
        });
    	cbBypass.addChangeListener(new ChangeListener(){
    		public void stateChanged(ChangeEvent e){
    			bRegister3.setEnabled(true);    			
    			cbBypassStateChanged();
    		}
    	});
        bAdd.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                synchronizeRooms();
                addRoom();
            }
        });
        bRemove.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                removeRoom();
            }
            
        });
        lstRooms.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                roomChanged();
            }
        });
        bCreateDB.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                createDB();
            }
        });
        
        ActionListener changeAction = new ActionListener(){
            public void actionPerformed(ActionEvent e){
        	bRegister3.setEnabled(true);
            }
        };
        ChkOpt1.addActionListener(changeAction);
        ChkOpt2.addActionListener(changeAction);
    	add(pRooms);
    	spRooms = new JScrollPane();
    	pRooms.setBounds(0, 0, 620, 750);

    }
    private void createDB()
    {
        try
        {
            setCursor(new Cursor(Cursor.WAIT_CURSOR));
            
            String engine = (String) comEngine.getSelectedItem();
            VWCreateDB cdb = new VWCreateDB(SwingUtilities.getWindowAncestor(this), this, false, engine);
            cdb.setVisible(true);
            cdb.show(tDBName.getText(), tHost.getText(), tPort.getText(),
                              tUser.getText(), new String(tPass.getPassword()));
            
            setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
        catch(Exception e){e.printStackTrace();}
    }
    
    public void makeRoomsOnline(boolean status){
	    bCreateDB.setEnabled(status);
    }
    private void loadDBEngines()
    {
        vDBEngines.addElement(DBENGINE_SQL);
        vDBEngines.addElement(DBENGINE_ORA);
    }
    
    private boolean compEmpty(JTextField comp, String msg)
    {
        if (comp.getText().equals(""))
        {
            Util.Msg(this, msg, MANAGER_APP); 
            comp.grabFocus(); 
            return true;
        }
        else
            return false;
    }
    private boolean checkRegisteredGroupsAreValid(JTextField groupField1, String msg, JTextField groupField2) {
    	if (groupField1.getText().trim().length() > 0) {
    		if ((tAdmins.getText().trim().length() > 0 && tAdmins.getText().equalsIgnoreCase(groupField1.getText()))
					|| (tNamed.getText().trim().length() > 0 && tNamed.getText().equalsIgnoreCase(groupField1.getText()))
					|| (tNamedOnline.getText().trim().length() > 0 && tNamedOnline.getText().equalsIgnoreCase(groupField1.getText()))
					|| (tProfessionalNamed.getText().trim().length() > 0 && tProfessionalNamed.getText().equalsIgnoreCase(groupField1.getText()))
					|| (tEnterPriseConcurrent.getText().trim().length() > 0 && tEnterPriseConcurrent.getText().equalsIgnoreCase(groupField1.getText())
					|| (tConcurrentOnline.getText().trim().length() > 0 && tConcurrentOnline.getText().equalsIgnoreCase(groupField1.getText())))
					|| (tProfessionalConcurrent.getText().trim().length() > 0 && tProfessionalConcurrent.getText().equalsIgnoreCase(groupField1.getText()))
					|| (groupField2.getText().trim().length() > 0 && groupField2.getText().equalsIgnoreCase(groupField1.getText()))) {
	            Util.Msg(this, msg, MANAGER_APP); 
	            groupField1.grabFocus(); 
	            return false;
    		} else 
    			return true;
        } else
            return true;
    }
    private void clearRoomInfo()
    {
        tName.setText("");
        comEngine.setSelectedIndex(-1);
        tDBName.setText("");
        tHost.setText("");
        tPort.setValue(new Integer(0));
        tUser.setText("");
        tPass.setText("");
        cbBypass.setSelected(false);
        tAdmins.setText("");
        tNamed.setText("");
        tNamedOnline.setText("");
        tProfessionalNamed.setText("");
        tEnterPriseConcurrent.setText("");
        tConcurrentOnline.setText("");
        tProfessionalConcurrent.setText("");
        tBatchInput.setText("");
        tIdle.setValue(new Integer(0));
        tKey.setText("");
    }
    private void addRoom()
    {
    	try{
    		String name = "Room_" + (vRooms.size()+1);
    		//vRooms.addElement(name);
    		vRoomInfo.addElement(new RoomProperty(name));
    		VWRoomItem room = new VWRoomItem(name, false);
    		vRooms.addElement(room);    	        
    		lstRooms.setListData(vRooms);
    		try{
    			lstRooms.setSelectedValue(room, true);
    		}catch (Exception e) { }
    		tName.setText(name);
    		tNamed.setText(VWSPreferences.getNamedUsers());
    	    tNamedOnline.setText(VWSPreferences.getNamedOnline());
			//tName.grabFocus();
    		tProfessionalNamed.setText(VWSPreferences.getProfessionalNamedUsers());
    		tEnterPriseConcurrent.setText(VWSPreferences.getEnterpriseConcurrent());
    	    tConcurrentOnline.setText(VWSPreferences.getConcurrentOnline());
    		tProfessionalConcurrent.setText(VWSPreferences.getProfessionalConcurrent());
    		tBatchInput.setText(VWSPreferences.getBatchInputAdministrator());
    		tName.grabFocus();
    	}catch (Exception e) {
    		//VWClient.printToConsole("EXCEPTION  : "+e.getMessage());
    	}
    }
    private void removeRoom()
    {
        if (!Util.Ask(this,  resourceManager.getString("RoomMessage.RemoveSelRoom1") , 
                                    "" + PRODUCT_NAME+" "+resourceManager.getString("RoomMessage.RemoveSelRoom2"))) return;
        /*
     	 * new lstRooms.getSelectedValues(); method is replaced with new lstRooms.getSelectedValuesList().toArray(); 
     	 * as JList.getSelectedValues() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
     	 */
        Object[] rooms = lstRooms.getSelectedValuesList().toArray();
        //VWRoomItem[] rooms = (VWRoomItem[])lstRooms.getSelectedValues();
        System.out.println("removeRoom " + rooms.length);
        if (rooms == null) return;
        for (int i=0; i < rooms.length; i++)
        {
            
            String room = ((VWRoomItem)rooms[i]).getName();
            System.out.println("selected removeRoom " + room);
            
            try
            {
        	if (vws == null)
        	    VWSPreferences.removeRoom(room);
        	else
        	    vws.removeRoom(room);
            }
            catch(Exception e){e.printStackTrace();}
        }
        synchronizeRooms();
    }
    private void registerRoom()
    {
	if (compEmpty(tName,resourceManager.getString("RoomMessage.RoomNameReq"))) return;
	if (compEmpty(tDBName,resourceManager.getString("RoomMessage.DBNameReq"))) return;
	if (comEngine.getSelectedIndex() < 0)
	{
	    Util.Msg(this, resourceManager.getString("RoomMessage.DBEngineReq"), MANAGER_APP); 
	    comEngine.grabFocus(); 
	    return;
	}
	if (compEmpty(tHost,resourceManager.getString("RoomMessage.DBHostReq"))) return;
	if (compEmpty(tPort, resourceManager.getString("RoomMessage.DBPortReq"))) return;
	if (compEmpty(tUser, resourceManager.getString("RoomMessage.DBUserReq"))) return;
	if (compEmpty(tAdmins,resourceManager.getString("RoomMessage.AdminGroup"))) return;
	if (!checkRegisteredGroupsAreValid(tSubAdmins,resourceManager.getString("RoomMessage.SubAdminGroup"), tBatchInput)) return;
	if (!checkRegisteredGroupsAreValid(tBatchInput,resourceManager.getString("RoomMessage.BatchInputAdminGroup"), tSubAdmins)) return;

	RoomProperty rp = new RoomProperty((String) tName.getText());

	rp.setDatabaseEngineName((String) comEngine.getSelectedItem());
	rp.setDatabaseName(tDBName.getText());
	rp.setDatabaseHost(tHost.getText());
	rp.setDatabasePort(((Number) tPort.getValue()).intValue());
	rp.setDatabaseUser(tUser.getText());
	rp.setDatabasePassword( new String(tPass.getPassword()));
	rp.setBypass(cbBypass.isSelected());
	rp.setAdmins(tAdmins.getText());
	rp.setSubAdmins(tSubAdmins.getText());
	rp.setNamedGroup(tNamed.getText().trim().length() == 0 ? "-" : tNamed.getText()); //Storing '-' for empty named group.
	rp.setNamedOnlineGroup(tNamedOnline.getText().trim().length() == 0 ? "-" : tNamedOnline.getText());
	rp.setProfessionalNamedGroup(tProfessionalNamed.getText().trim().length() == 0 ? "-" : tProfessionalNamed.getText()); //Storing '-' for empty professional named group.
	rp.setEnterpriseConcurrentGroup(tEnterPriseConcurrent.getText().trim().length()==0 ? "-" :tEnterPriseConcurrent.getText());
	rp.setConcurrentOnlineGroup(tConcurrentOnline.getText().trim().length() == 0 ? "-" : tConcurrentOnline.getText());
	rp.setProfessionalConcurrentGroup(tProfessionalConcurrent.getText().trim().length()==0 ? "-" :tProfessionalConcurrent.getText());
	rp.setBatchInputAdministrator(tBatchInput.getText().trim().length()==0 ? "-" :tBatchInput.getText());
	rp.setIdle(((Number) tIdle.getValue()).intValue());
	//CV10 Encryption Util.encryptKey added to encrypt and store the key in registry for room level encryption
	rp.setKey(Util.encryptKey(new String(tKey.getText()))); //code modified by madhavan
	rp.setIsEncryptedKey(true);
	try
	{	 
		RoomProperty ap =  null;
	    if (vws == null) {
	    	rp.setEnabled(VWSPreferences.getRoomState(rp.getName()));
	    	ap = VWSPreferences.getRoomAuthenticationProperties(rp.getName());
	    } else {
	    	rp.setEnabled(vws.getRoomState(rp.getName()));
	    	ap = vws.getRoomAuthenticationProperties(rp.getName());
	    }
	    try{
	    	if (ap != null) {
		    	rp.setSqlConnectionString(ap.getSqlConnectionString());
		    	rp.setAuthenticationMode(ap.getAuthenticationMode());
		    	rp.setIntegratedSecurity(ap.isIntegratedSecurity());
		    	rp.setAuthenticationScheme(ap.getAuthenticationScheme());
		    	rp.setDomain(ap.getDomain());
		    	rp.setServerSpn(ap.getServerSpn());
	    	}
			rp.setConnectionResetScheduler(ChkOpt1.isSelected()?0:1);
			if (ChkOpt1.isSelected())
			    rp.setSchedulerTime(minuteSpinner.getValue().toString());
			else{
			    Date dailyFreq = dateModel.getDate();
			    String dailyFrequency = new SimpleDateFormat("HH:mm:ss").format(dailyFreq);
			    rp.setSchedulerTime(dailyFrequency);
			}
	    }catch(Exception ex){		
	    }	    
	    if (vws == null){
		VWSPreferences.removeRoom(((VWRoomItem) lstRooms.getSelectedValue()).getName());
		VWSPreferences.addRoom(rp);
		if (Util.getEnvOSName().indexOf("Linux")!=-1 && VWSPreferences.getCheckViewWiseJar().trim().equals("true")){
		    VWSPreferences.setCheckViewWiseJar("false");
		}    		    
	    }else{
		vws.removeRoom(((VWRoomItem) lstRooms.getSelectedValue()).getName());
		vws.addRoom(rp);
	    }

	}
    	catch(Exception e){}
	synchronizeRooms();
    }
    public void synchronizeRooms()
    {
	//System.out.println("synchronizeRooms");
        RoomProperty[] rooms = null;
        try
        {
            if (vws == null)
        	rooms = VWSPreferences.getRegRooms();
            else
        	rooms = vws.getRegRooms();
        }
        catch(Exception e){}
        vRoomInfo.removeAllElements();
        vRooms.removeAllElements();
        pRRooms.removeAll();
        if (rooms != null && rooms.length > 0)
        {
            for (int i=0; i < rooms.length; i++)
            {
                vRooms.addElement(new VWRoomItem(rooms[i].getName(), rooms[i].isEnabled()));
                vRoomInfo.addElement(rooms[i]);
                //createRoomIcon(rooms[i]);
            }
            lstRooms.setListData(vRooms);
            if (vRooms.size() > 0) lstRooms.setSelectedIndex(0);
        }
    }
    private void addDocListeners()
    {
    	tName.getDocument().addDocumentListener(new DocListener(bRegister3));
        tDBName.getDocument().addDocumentListener(new DocListener(bRegister3));
        tHost.getDocument().addDocumentListener(new DocListener(bRegister3));
        tPort.getDocument().addDocumentListener(new DocListener(bRegister3));
        tUser.getDocument().addDocumentListener(new DocListener(bRegister3));
        tPass.getDocument().addDocumentListener(new DocListener(bRegister3));
        tIdle.getDocument().addDocumentListener(new DocListener(bRegister3));
        tKey.getDocument().addDocumentListener(new DocListener(bRegister3));
        tAdmins.getDocument().addDocumentListener(new DocListener(bRegister3));
        tSubAdmins.getDocument().addDocumentListener(new DocListener(bRegister3));
        tNamed.getDocument().addDocumentListener(new DocListener(bRegister3));
        tNamedOnline.getDocument().addDocumentListener(new DocListener(bRegister3));
        tConcurrentOnline.getDocument().addDocumentListener(new DocListener(bRegister3));
        tProfessionalNamed.getDocument().addDocumentListener(new DocListener(bRegister3));
        tEnterPriseConcurrent.getDocument().addDocumentListener(new DocListener(bRegister3));
        tProfessionalConcurrent.getDocument().addDocumentListener(new DocListener(bRegister3));
        tBatchInput.getDocument().addDocumentListener(new DocListener(bRegister3));
    }
    private void roomChanged()
    {
        clearRoomInfo();
        if (lstRooms.getSelectedIndices().length == 1)
        {
            getRoomInfo(lstRooms.getSelectedIndex());
            bRegister3.setEnabled(false);
            return;
        }
        clearRoomInfo();
        bRegister3.setEnabled(false);
    }
    private void cbBypassStateChanged()
    {
        if (cbBypass.getSelectedObjects() == null)
            switchGroups(true);
        else
            switchGroups(false);
    }
    private void switchGroups(boolean b)
    {	//CV10 Enhancement if condition added for byePass security
    	if((VWSPreferences.getSecurityCheck()==true)&&(VWSPreferences.getDebug()==true))
        tAdmins.setEnabled(b);
    }
    private void getRoomInfo(int i)
    {
        if ( vRoomInfo.size() == 0) return;
        RoomProperty rp = (RoomProperty) vRoomInfo.elementAt(i);
        tName.setText(rp.getName()); 
        comEngine.setSelectedItem(rp.getDatabaseEngineName());
        tDBName.setText(rp.getDatabaseName());
        tHost.setText(rp.getDatabaseHost());
        tPort.setValue(new Integer(rp.getDatabasePort()));
        tUser.setText(rp.getDatabaseUser());
        tPass.setText(rp.getDatabasePassword());
        cbBypass.setSelected(rp.getBypass());
        tAdmins.setText(rp.getAdmins());
        tSubAdmins.setText(rp.getSubAdmins());
        String namedGroup = rp.getNamedGroup().length() == 0 ? VWSPreferences.getNamedUsers() : rp.getNamedGroup();
        if(namedGroup.equalsIgnoreCase("-"))
        	namedGroup = "";
        tNamed.setText(namedGroup);
        
     
      String namedOnlineGroup = rp.getNamedOnlineGroup().length() == 0 ? VWSPreferences.getNamedOnline() : rp.getNamedOnlineGroup();
        if(namedOnlineGroup.equalsIgnoreCase("-"))
        	namedOnlineGroup = "";
        tNamedOnline.setText(namedOnlineGroup);

        String professionalNamedGroup = rp.getProfessionaNamedGroup().length() == 0 ? VWSPreferences.getProfessionalNamedUsers() : rp.getProfessionaNamedGroup();
        if(professionalNamedGroup.equalsIgnoreCase("-"))
        	professionalNamedGroup = "";
        tProfessionalNamed.setText(professionalNamedGroup);
        
        

       String enterpriseConcurrentGroup = rp.getEnterpriseConcurrentGroup().length() == 0 ? VWSPreferences.getEnterpriseConcurrent() : rp.getEnterpriseConcurrentGroup();
        if(enterpriseConcurrentGroup.equalsIgnoreCase("-"))
        	enterpriseConcurrentGroup = "";
        tEnterPriseConcurrent.setText(enterpriseConcurrentGroup);
        
       
        String concurrentOnlineGroup = rp.getConcurrentOnlineGroup().length() == 0 ? VWSPreferences.getConcurrentOnline() : rp.getConcurrentOnlineGroup();
        if(concurrentOnlineGroup.equalsIgnoreCase("-"))
        	concurrentOnlineGroup = "";
        tConcurrentOnline.setText(concurrentOnlineGroup);
        

        String professionalConcurrentGroup = rp.getProfessionalConcurrentGroup().length() == 0 ? VWSPreferences.getProfessionalConcurrent() : rp.getProfessionalConcurrentGroup();
        if(professionalConcurrentGroup.equalsIgnoreCase("-"))
        	professionalConcurrentGroup = "";
        tProfessionalConcurrent.setText(professionalConcurrentGroup);
        
        String batchInputAdministrator = rp.getBatchInputAdministrator().length() == 0 ? VWSPreferences.getBatchInputAdministrator() : rp.getBatchInputAdministrator();
        if(batchInputAdministrator.equalsIgnoreCase("-"))
        	batchInputAdministrator = "";
        tBatchInput.setText(batchInputAdministrator);
        
       
        tIdle.setValue(new Integer(rp.getIdle()));
        String key = rp.getKey();
        if (key != null && key.length() > 0)
        {
            tKey.setText("************");
            tKey.setEnabled(false);
        }
        else
        {
            tKey.setEnabled(true);
        }
        try{
	        if (rp.getConnectionResetScheduler() == 0){
	        	ChkOpt1.setSelected(true);
	        	minuteModel.setValue(Integer.parseInt(rp.getSchedulerTime()));
	        }else{        	
	        	ChkOpt2.setSelected(true);
	        	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	        	Date date = timeFormat.parse(rp.getSchedulerTime());
	        	dateModel.setValue(date);
	        }
        }catch(Exception ex){
        	
        }
    }
    private void createRoomIcon(RoomProperty rp)
    {
/*        final JLabel room = new JLabel();
        room.setIcon(Images.ron);
        room.setText(rp.getName());
        room.setDisabledIcon(Images.rof);
        room.setIconTextGap(10);
        room.setHorizontalTextPosition(SwingConstants.CENTER);
        room.setVerticalTextPosition(SwingConstants.BOTTOM);
        room.addMouseListener(new MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                 if (evt.getClickCount() > 1) 
                     switchRoomState(room);
            }
        });
        boolean state = false;
        try
        {
            state = vws.getRoomState(room.getText());
        }
        catch(Exception e){}
        room.setEnabled(state);
        pRRooms.add(room);*/
        final VWRoomItem room = new VWRoomItem(rp.getName(), rp.isEnabled());
/*        room.addMouseListener(new MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                 if (evt.getClickCount() > 1) 
                     switchRoomState(room);
            }
        });*/
        boolean state = false;
        try
        {
            if (vws == null)
        	state = VWSPreferences.getRoomState(room.getName());
            else
        	state = vws.getRoomState(room.getName());
        }
        catch(Exception e){}
        room.setEnabled(state);
        //pRRooms.add(room);
       vRooms.addElement(room);
       lstRooms.setListData(vRooms);  
    }
    private void switchRoomState(int position)
    {	
	VWRoomItem room = (VWRoomItem) vRooms.get(position);
        String rname = " room '" + room.getName() + "' ";
        String question = (room.isEnabled()? "Take" + rname + " Offline?" :
                                             "Bring" + rname + "Online?");
        if ( Util.Ask(SwingUtilities.getWindowAncestor(this) ,  question , PRODUCT_NAME + " Server"))
        {
            room.setEnabled(!room.isEnabled());
            try
            {
        	if (vws == null)
        	    VWSPreferences.setRoomState(room.getName(), room.isEnabled());
        	else
        	    vws.setRoomState(room.getName(), room.isEnabled());
            }
            catch(Exception e) {
        	e.printStackTrace();
            }           
            vRooms.remove(position);
            boolean state = false;
            try
            {
        	if (vws == null)
        	    state = VWSPreferences.getRoomState(room.getName());
        	else
        	    state = vws.getRoomState(room.getName());
            }
            catch(Exception e){}
            
            room.setEnabled(state);            
            vRooms.insertElementAt(room, position);            
            lstRooms.setListData(vRooms);
            lstRooms.setSelectedIndex(position);
        }
    }
   /* private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
            switchProxyData(true);
        else
            switchProxyData(false);
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }*/
    private NumberFormatter getNF()
    {
    	DecimalFormat decimalFormat = new DecimalFormat("####");
    	NumberFormatter numberFormatter = new NumberFormatter(decimalFormat);
    	numberFormatter.setOverwriteMode(true);
    	numberFormatter.setAllowsInvalid(false);
    	return(numberFormatter);
    }
    
    public void setUser(String user)
    {
        tUser.setText(user);
    }
    public void setDatabaseName(String dbname)
    {
        tDBName.setText(dbname);
    }
    public void setPass(String pass)
    {
        tPass.setText(pass);
    }

    int iRegLeft = 400, iRegTop = 425, iRegWidth = 90, iRegHeight = 23;
    int iLeft = 0, iTop = 0, iHeight = 0, iWidth = 0, iPortWidth =0, iTop1 =0;;
    private JEditorPane epLogo;
    private VWPanel pRooms, pDatabase, pBypass, pRRooms; 
    JLabel lEngine, lDBName, lHost, lPort, lUser, lPass, 
    lDBScheduler, lMinute, lIdle, lMinutes, lKey, lName, lAdmins,lNamed,lNamedOnline, lProfessionalNamed, lEnterPriseConcurrent,lConcurrentOnline, lProfessionalConcurrent, lRegRooms, lSubAdmins, lBatchInput;
    private JList lstRooms;
    private JComboBox comEngine;
    public JTextField tDBName, tHost, tUser, tKey, tName, tAdmins,tNamed,tNamedOnline, tConcurrentOnline,tProfessionalNamed, tEnterPriseConcurrent, tProfessionalConcurrent, tSubAdmins, tBatchInput ;
    private JPasswordField tPass;
    private JFormattedTextField tIdle, tPort;
    
    public  JButton bAdd, bRemove, bRegister3, bCreateDB;
    private JCheckBox cbBypass;
    private JRadioButton ChkOpt1, ChkOpt2;
    private JScrollPane spRooms;
    private JSpinner minuteSpinner, jSpinner;
    private ButtonGroup group1;
    private VWS vws;
    private SpinnerDateModel dateModel;
    private SpinnerNumberModel minuteModel;
    private Vector vRoomInfo = new Vector();
    private Vector vRooms = new Vector();
    private Vector vDBEngines = new Vector();
    
    public static Color getVWColor(){
    	return new Color(238, 242, 244);
    }
    
    public static void main(String args[]){

    	try{
    		String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
    		UIManager.setLookAndFeel(plasticLookandFeel);
    	}catch(Exception ex){
    		System.out.println("ex :: "+ex.getMessage());
    	}

    	JFrame frame = new JFrame();
		VWRoomPanel panel = new VWRoomPanel();
		frame.add(panel);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/*
     	 * new frame.reSize(800, 600); method is replaced with new frame.setSize(800, 600); 
     	 * as reSize() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
     	 */
		frame.setSize(800, 630);
    }
}
