/*
 * ConnectorTask.java
 *
 * Created on September 14, 2003, 3:10 PM
 */

package com.computhink.manager;

import java.util.TimerTask;
import java.rmi.RemoteException;
import java.io.*;

import com.computhink.resource.ResourceManager;
import com.computhink.common.*;
//import com.computhink.dss.server.*;

/**
 *
 * @author  Saad
 */
public class ServerConnector extends TimerTask
{
    private ManagerFrame frame;
    private String host;
    private String type;
    private int port;
    private static ResourceManager resourceManager=ResourceManager.getDefaultManager();
    public ServerConnector
                        (ManagerFrame frame, String host, int port, String type)
    {
        this.frame = frame;
        this.host = host;
        this.port = port;
        this.type = type;
    }
    public void run() 
    {
        CIServer server = (CIServer) Util.getServer(type, host, port);
        String run = resourceManager.getString("ServConnector.RunningStr")+"- \\\\" + host + ":" + port;
        String stop =resourceManager.getString("ServConnector.StopedStr") +"- \\\\" + host + ":" + port;
        String current = frame.getStatus();
        if ( current.equals(run) )
        {
            if (server != null) return;
            frame.setServer(server);
            frame.setStatus(stop);
            return;
        }
        else if ( current.equals(stop) )
        {
            if (server == null) return;
            frame.setServer(server);
            frame.setStatus(run);
            return;
        }
        else
        {
            if (server == null) 
                frame.setStatus(stop);
            else
            {
                frame.setStatus(run);
                frame.setServer(server);
            }
        }
    }
}
