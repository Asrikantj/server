/*
 * RegisterDialog.java
 *
 * Created on November 6, 2003, 3:07 PM
 */

package com.computhink.manager;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;
/**
 *
 * @author  Administrator
 */
public class RegisterDialog extends JDialog implements ManagerConstants  
{
    private JButton bAdd, bRemove, bClose, bRegister;
    private JLabel lLine1,lLine2, lServers, lAddress, lPort, lName, lType, 
                                                                   lUser, lPass; 
    private JComboBox comType;
    private JScrollPane spServers;
    private JTextField tName, tPort, tAddress, tUser;
    private JPasswordField tPass;
    private JList lstServers;
    private JPanel pServer;
    private Vector vServers = new Vector();
    private Vector vServerInfo = new Vector();
    private Vector vTypes = new Vector();
    private boolean newRegisteration = false;
    private static ResourceManager resourceManager= null;
    public RegisterDialog(Frame parent, boolean modal) 
    {
        super(parent, modal);
        loadTypes();
        initComponents();
        synchronizeServers();
    }
    private void loadTypes()
    {
        vTypes.addElement(SERVER_VWS);
        vTypes.addElement(SERVER_IXR);
        vTypes.addElement(SERVER_DSS);
        /* Purpose:	Master Switch off added for ARS in registry
        * Created By: C.Shanmugavalli		Date:	20 Sep 2006
        */
        if(ManagerPreferences.getARSEnable().equalsIgnoreCase("true"))
        vTypes.addElement(SERVER_ARS);
        // DRS master switch off Valli 20 Dec 2006
        if(ManagerPreferences.getDRSEnable().equalsIgnoreCase("true"))
            vTypes.addElement(SERVER_DRS);
        if(ManagerPreferences.getVNSEnable().equalsIgnoreCase("true"))
            vTypes.addElement(SERVER_VNS);
        if(ManagerPreferences.getCSSEnable().equalsIgnoreCase("true"))
            vTypes.addElement(SERVER_CSS);
    }
    private void initComponents() 
    {
    	resourceManager=ResourceManager.getDefaultManager();
        spServers = new JScrollPane();
        lstServers = new JList();
        comType = new JComboBox(vTypes);
        pServer = new JPanel();

        tAddress = new JTextField();
        tPort = new JTextField();
        tName = new JTextField();
        
        tUser = new JTextField();
        tPass = new JPasswordField();

        lLine1 = new JLabel("_________________________________");
        lLine2 = new JLabel("_________________________________");
        lType = new JLabel(resourceManager.getString("RegDlgLbl.Type"));
        lPort = new JLabel(resourceManager.getString("RegDlgLbl.CommPort"));
        lName = new JLabel(resourceManager.getString("RegDlgLbl.Name"));
        lServers = new JLabel(resourceManager.getString("RegDlgLbl.Registered") +" "+ resourceManager.getString("PRODUCT_NAME")+" "+resourceManager.getString( "RegDlgLbl.Servers"));
        lAddress = new JLabel(resourceManager.getString("RegDlgLbl.Adderess"));
        lUser = new JLabel(resourceManager.getString("RegDlgLbl.User"));
        lPass = new JLabel(resourceManager.getString("RegDlgLbl.Password"));
        
        bAdd = new JButton(resourceManager.getString("RegDlgBtn.Add"));
        bRemove = new JButton(resourceManager.getString("RegDlgBtn.Remove"));
        bClose = new JButton(resourceManager.getString("RegDlgBtn.Close"));
        bRegister = new JButton(resourceManager.getString("RegDlgBtn.Register"));

        getContentPane().setLayout(null);
        setTitle(resourceManager.getString("RegDlgTitle.Manage")+" "+ resourceManager.getString("CVProduct.Name")+" "+resourceManager.getString("RegDlgTitle.Servers"));
        setResizable(false);
        spServers.setViewportBorder(new SoftBevelBorder(BevelBorder.RAISED));
        spServers.setViewportView(lstServers);
        lstServers.setBackground(new Color(255, 255, 204));
        spServers.setBounds(30, 40, 180, 270); getContentPane().add(spServers);
        lServers.setBounds(30, 20, 180, 16); getContentPane().add(lServers);
        pServer.setLayout(null);
        pServer.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
        
        lAddress.setBounds(30, 30, 70, 16); pServer.add(lAddress);
        tAddress.setBounds(90, 30, 170, 20); pServer.add(tAddress);
        lPort.setBounds(30, 55, 120, 16); pServer.add(lPort);
        tPort.setBounds(210, 55, 50, 20); pServer.add(tPort);
        lLine1.setBounds(30, 65, 240, 20); pServer.add(lLine1);
        lName.setBounds(30, 95, 70, 16); pServer.add(lName);
        tName.setBounds(80, 95, 180, 20); pServer.add(tName);
        lType.setBounds(30, 120, 70, 20); pServer.add(lType);
        comType.setBounds(80, 120, 180, 20); pServer.add(comType);
        comType.setBackground(new Color(255, 255, 204));
        lLine2.setBounds(30, 140, 240, 20); pServer.add(lLine2);
        lUser.setBounds(30, 170, 70, 16); pServer.add(lUser);
        tUser.setBounds(100, 170, 160, 20); pServer.add(tUser);
        lPass.setBounds(30, 195, 70, 16); pServer.add(lPass);
        tPass.setBounds(100, 195, 160, 20); pServer.add(tPass);
        
        
        
        bRegister.setBounds(170, 230, 90, 26); pServer.add(bRegister);
        bAdd.setBounds(30, 320, 80, 26); getContentPane().add(bAdd);
        bRemove.setBounds(130, 320, 80, 26); getContentPane().add(bRemove);
        bClose.setBounds(420, 320, 80, 26); getContentPane().add(bClose);
        pServer.setBounds(220, 40, 280, 270); getContentPane().add(pServer);
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new java.awt.Dimension(538, 405));
        setLocation((screenSize.width-538)/2,(screenSize.height-405)/2);
        
        bRegister.setEnabled(false);
        comType.setSelectedIndex(-1);
        //-------------Listeners------------------------------------------------
        tAddress.addFocusListener(new FocusListener(){
            public void focusGained(FocusEvent e){} 
            public void focusLost(FocusEvent e)
            {
                if (bRegister.isEnabled()) guessServerName(tAddress.getText());
            }
        });
        tAddress.getDocument().addDocumentListener(new DocListener(bRegister));
        tPort.getDocument().addDocumentListener(new DocListener(bRegister));
        tName.getDocument().addDocumentListener(new DocListener(bRegister));
        tUser.getDocument().addDocumentListener(new DocListener(bRegister));
        tPass.getDocument().addDocumentListener(new DocListener(bRegister));
        comType.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if (comType.getSelectedIndex() >= 0) bRegister.setEnabled(true);
            }
        });
        bAdd.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                synchronizeServers();
                addServer();
            }
        });
        bRemove.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                removeServer();
            }
        });
        bRegister.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                newRegisteration = true;
                registerServer();
            }
        });
        lstServers.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                if (e.getValueIsAdjusting()) return;
                selectionChanged();
            }
        });
        bClose.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        //----------------------------------------------------------------------
    }
    private void addServer()
    {
        String name = "NoName_" + (vServers.size()+1);
        vServers.addElement(name);
        vServerInfo.addElement(name + "| | | ");  //Name-Addr-Port-type
        lstServers.setListData(vServers);
        lstServers.setSelectedIndex(vServers.size()-1);
        tAddress.grabFocus();
        tAddress.setText(getLocalAddress());
        tPort.grabFocus();
    }
    private void removeServer()
    {
        Object[] servers = lstServers.getSelectedValuesList().toArray();
        for (int i=0; i < servers.length; i++)
        {
            String server = (String) servers[i];
            ManagerPreferences.removeServer(server);
             if (ManagerPreferences.isActiveServer(server))
                 ManagerPreferences.removeActiveServer(server);
        }
        synchronizeServers();
    }
    private void registerServer()
    {
        if (tAddress.getText().equals(""))
        {Util.Msg
         (null, resourceManager.getString("RegMSG.AddressReq"), MANAGER_APP); tAddress.grabFocus(); return;}
        if (tPort.getText().equals(""))
        {Util.Msg
         (null, resourceManager.getString("RegMSG.PortReq"), MANAGER_APP); tPort.grabFocus(); return;}
        if (tName.getText().equals(""))
        {Util.Msg
         (null, resourceManager.getString("RegMSG.NameReq"), MANAGER_APP); tName.grabFocus(); return;}
        if (comType.getSelectedIndex() < 0)
        {Util.Msg
         (null,resourceManager.getString("RegMSG.TypeReq"), MANAGER_APP); comType.grabFocus(); return;}
        
        if (vServers.size() > 0)
        ManagerPreferences.removeServer((String) lstServers.getSelectedValue());
        
        ManagerPreferences.addServer(tName.getText() + "|" + 
                                        tAddress.getText() + "|" + 
                                        tPort.getText() + "|" + 
                                        (String) comType.getSelectedItem());
        ManagerPreferences.setManager(tName.getText(), tUser.getText(),
                                               new String(tPass.getPassword()));
        synchronizeServers();
    }
    private void selectionChanged()
    {
        clearServerInfo();
        if (lstServers.getSelectedIndices().length == 1)
        {
            getServerInfo(lstServers.getSelectedIndex());
            bRegister.setEnabled(false);
            return;
        }
        bRegister.setEnabled(false);
    }
    private void switchAddRemove(boolean b)
    {
        bAdd.setEnabled(b);
        bRemove.setEnabled(b);
    }
    private void clearServerInfo()
    {
        tAddress.setText("");
        tPort.setText("");
        tName.setText("");
        comType.setSelectedIndex(-1);
        tUser.setText("");
        tPass.setText("");
    }
    private void synchronizeServers()
    {
        vServerInfo.removeAllElements();
        vServerInfo = ManagerPreferences.getServers(true);
        if (vServerInfo != null)
        {
            vServers.removeAllElements();
            for (int i=0; i < vServerInfo.size(); i++)
            {
                String server = ((ServerSchema) vServerInfo.elementAt(i)).name;
                vServers.addElement(server);
            }
            lstServers.setListData(vServers);
            if (vServers.size() > 0) lstServers.setSelectedIndex(0);
        }
    }
    private String getLocalAddress()
    {
        try
        {
            return InetAddress.getLocalHost().getHostAddress();
        }
        catch(UnknownHostException uhe)
        {
            return "";
        }
    }
    private void getServerInfo(int i)
    {
        try
        {
            ServerSchema ss = (ServerSchema) vServerInfo.elementAt(i);
            tName.setText(ss.name);
            tAddress.setText(ss.address);
            tPort.setText(String.valueOf(ss.comport));
            comType.setSelectedItem(ss.type);
            String credentials = ManagerPreferences.getManager(ss.name);
            if (credentials != null)
            {
                StringTokenizer st = new StringTokenizer(credentials, "||");
                if (st.hasMoreTokens()) tUser.setText( (String) st.nextToken());
                if (st.hasMoreTokens()) tPass.setText( (String) st.nextToken());
            }
        }
        catch(Exception e){}
    }
    private void guessServerName(String Addr)
    {
        String name;
        setCursor(new Cursor(Cursor.WAIT_CURSOR));
        try
        {
            name = InetAddress.getByName(Addr).getHostName();
            tName.setText(name);
        }
        catch(UnknownHostException e){}
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }
    private void closeDialog() 
    {
        setVisible(false);
        dispose();
    }
    public boolean fire()
    {
    	/*
     	 * new super.show(); method is replaced with new super.setVisible(true); 
     	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
     	 */
        super.setVisible(true); 
        return newRegisteration;
    }
}

