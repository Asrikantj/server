/*
 * ManagerFrame.java
 *
 * Created on Sep 21, 2003, 12:14 AM
 */
package com.computhink.manager;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.MenuBar;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.rmi.RemoteException;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.FontUIResource;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.rtf.RTFEditorKit;

import com.computhink.ars.server.ARSPreferences;
import com.computhink.common.CIServer;
import com.computhink.common.Constants;
import com.computhink.common.LogMessage;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.drs.server.DRSPreferences;
import com.computhink.manager.DRSSettings;
import com.computhink.manager.configuration.LoginManagementConfiguration;
import com.computhink.manager.configuration.VWConfiguration;
import com.computhink.manager.image.Images;
import com.computhink.vns.server.VNSPreferences;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.csserver.CSSPreferences;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;

public class ManagerFrame extends JFrame implements ManagerConstants 
{
    private JButton bClose, bClear, bSave, bProxy, 
                                  bRegister, bHelp, bAbout, bSettings, bManager,bLoginManagement,bStart, bStop;
    private JPanel pActive, pLog;    
    private JTextPane Logger;
    private JScrollPane ScLogger;
    private JLabel statusBar, lActive, lPrivilege;
    private JComboBox comActive;
    private JList clientList;
    private CIServer server;
    private TimerTask worker;
    private Timer workerTimer = new Timer();
    private Timer connector = new Timer();
    private ServerConnector serverConnector;
    private int btnWidth = 104, btnHeight = 22, btnTop = 8;
    private String host;
    private String runningServer = "";
    private int port;
    
    private JMenuBar mBar = new JMenuBar();
    
    private static ResourceManager resourceManager= null;
    /***CV2019 code merges from CV10.2 line. Added to increase Server manager height and width */
    private static final int width = 1200;//800
    private static final int height = 710;//560
    
    private static boolean authorized = false;
    private static ServerSchema mySchema = null;

    public ManagerFrame() 
    {
        super(MANAGER_APP);
        new Images();
        initComponents();
        resetLog();
        refreshServers();
        workerTimer.schedule(new LogWorker(), 0, 10);
        mySchema = Util.getMySchema();
        setBackground(new Color(238, 242, 244));
    }
    private void initComponents() 
    {
    	resourceManager=ResourceManager.getDefaultManager();
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        ScLogger = new JScrollPane();
        Logger = new JTextPane();
        pActive = new JPanel();
        pLog  = new JPanel();
        statusBar = new JLabel();
        lActive = new JLabel(PANEL_TITLE);
        lPrivilege = new JLabel("Unauthorized");
        bRegister = new JButton();
        bClose = new JButton();
        bClear = new JButton();
        bSave = new JButton();
        bProxy = new JButton(); 
        bAbout = new JButton();  
        bHelp = new JButton(); 
        bSettings = new JButton();
        bStart = new JButton();
        bStop = new JButton();
        bStart.setIcon(Images.startService);
        bStop.setIcon(Images.stopService);
        bManager= new JButton(); 
        bLoginManagement=new JButton();
        comActive = new JComboBox();
        
        getContentPane().setLayout(null);
        setSize(new Dimension(width, height));
        
        pLog.setLayout(null);
        pLog.setBorder(new BevelBorder(BevelBorder.RAISED));
        /***CV2019 code merges from CV10.2 line*/
        pLog.setBounds(10, 56, 1175, 574);
        getContentPane().add(pLog);
        ScLogger.setBounds(3, 3, 1169, 568);
        /**End of CV10.2 code merges***/
        ScLogger.setViewportView(Logger);
        pLog.add(ScLogger);
        pActive.setBackground(new Color(238, 242, 244));
        pLog.setBackground(new Color(238, 242, 244));
        getContentPane().setBackground(new Color(238, 242, 244));
        
        pActive.setLayout(null);
        TitledBorder border = new TitledBorder(PANEL_TITLE);
        border.setBorder(new BevelBorder(BevelBorder.RAISED));
        //pActive.setBorder(border);
        /***CV2019 code merges from CV10.2 line**/
        pActive.setBounds(10, 24, 1180, 50); getContentPane().add(pActive);
        lActive.setBounds(5, btnTop, 94, 22); pActive.add(lActive);
        comActive.setBounds(98, btnTop, 200, btnHeight); pActive.add(comActive);
        comActive.setBackground(new Color(255, 255, 204));
        int iLeft = 302, hGap = 21; 
        
        bStart.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bStart);
        iLeft += hGap;
        bStop.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bStop);
        /***End of CV10.2 code merges**/
        bStart.setBorderPainted(false);
        bStop.setBorderPainted(false);
        bStart.setBackground(pActive.getBackground());
        bStop.setBackground(pActive.getBackground());
        bStart.setToolTipText(resourceManager.getString("BtnToolTip.StartService"));
        bStop.setToolTipText(resourceManager.getString("BtnToolTip.StopService"));
        bStart.setFocusPainted(false);
        bStop.setFocusPainted(false);
        
        bStart.setContentAreaFilled(false);
        bStop.setContentAreaFilled(false);        
        bSettings.setContentAreaFilled(false);
        bRegister.setContentAreaFilled(false);
        bProxy.setContentAreaFilled(false);
        bManager.setContentAreaFilled(false);
        bLoginManagement.setContentAreaFilled(false);
        bSave.setContentAreaFilled(false);
        bClear.setContentAreaFilled(false);
        bAbout.setContentAreaFilled(false);
        
        bStart.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bStop.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bSettings.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bRegister.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bProxy.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bManager.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bLoginManagement.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bSave.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bClear.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        bAbout.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));        
        
        
        iLeft += hGap + 23;        
        bSettings.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bSettings);
        bSettings.setBorderPainted(false);
        bSettings.setBackground(pActive.getBackground());
        bSettings.setToolTipText(BTN_SETTINGS);
        bSettings.setFocusPainted(false);
        bSettings.setIcon(Images.settings);
        
        iLeft += hGap + 18;        
        bRegister.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bRegister);
        bRegister.setBorderPainted(false);
        bRegister.setBackground(pActive.getBackground());
        bRegister.setToolTipText(BTN_MANAGE);
        bRegister.setFocusPainted(false);
        bRegister.setIcon(Images.regServer);

        
        iLeft += hGap;        
        bProxy.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bProxy);
        bProxy.setBorderPainted(false);
        bProxy.setBackground(pActive.getBackground());
        bProxy.setToolTipText(BTN_PROXY);
        bProxy.setFocusPainted(false);
        bProxy.setIcon(Images.proxyServer);

        
        iLeft += hGap + 18;        
        bManager.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bManager);
        bManager.setBorderPainted(false);
        bManager.setBackground(pActive.getBackground());
        bManager.setToolTipText(BTN_MANAGER);
        bManager.setFocusPainted(false);
        bManager.setIcon(Images.mgrSetup);
        
        iLeft += hGap + 18;        
        bLoginManagement.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bLoginManagement);
        bLoginManagement.setBorderPainted(false);
        bLoginManagement.setBackground(pActive.getBackground());
        bLoginManagement.setToolTipText(BTN_LOGIN_MANAGER);
        bLoginManagement.setFocusPainted(false);
        bLoginManagement.setIcon(Images.login);

        iLeft += hGap + 18;        
        bSave.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bSave);
        bSave.setBorderPainted(false);
        bSave.setBackground(pActive.getBackground());
        bSave.setToolTipText(BTN_SAVE_LOG);
        bSave.setFocusPainted(false);
        bSave.setIcon(Images.saveLog);
        
        
        iLeft += hGap;        
        bClear.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bClear);
        bClear.setBorderPainted(false);
        bClear.setBackground(pActive.getBackground());
        bClear.setToolTipText(BTN_CLEAR_LOG);
        bClear.setFocusPainted(false);
        bClear.setIcon(Images.clearLog);

        
/*        
        iLeft += hGap;        
        bClose.setBounds(iLeft, btnTop, 20, btnHeight); pActive.add(bClose);
        bClose.setBorderPainted(false);
        bClose.setBackground(pActive.getBackground());
        bClose.setToolTipText(BTN_CLOSE_MANAGER);
        bClose.setFocusPainted(false);
        bClose.setIcon(Images.close);*/
        
        //iLeft += hGap + hGap + 254; values before cv10       
        /***CV2019 code merges from CV10.2 line*/
        iLeft += hGap + hGap + 540;     
        bAbout.setBounds(iLeft, 4, 20, btnHeight); pActive.add(bAbout);
        bAbout.setBorderPainted(false);
        bAbout.setBackground(pActive.getBackground());
        bAbout.setToolTipText(BTN_ABOUT);
        bAbout.setFocusPainted(false);
        bAbout.setIcon(Images.about);

        /*bSettings.setBounds(316, btnTop, btnWidth, btnHeight); pActive.add(bSettings);
        bSettings.setMnemonic('S');
        bRegister.setBounds(452, btnTop, btnWidth, btnHeight); pActive.add(bRegister);
        bRegister.setMnemonic('R');
        bProxy.setBounds(560, btnTop, btnWidth, btnHeight); pActive.add(bProxy);
        bProxy.setMnemonic('P');

        bAbout.setBounds(682, btnTop, 90, btnHeight); pActive.add(bAbout);
        bAbout.setMnemonic('A');*/
        btnTop = 452;
        
        /* bManager.setBounds(12, btnTop, btnWidth, btnHeight); getContentPane().add(bManager);
        bManager.setMnemonic('M');
        bClear.setBounds(466, btnTop, btnWidth, btnHeight); getContentPane().add(bClear);
        bClear.setMnemonic('l');
        bSave.setBounds(574, btnTop, btnWidth, btnHeight); getContentPane().add(bSave);
        bSave.setMnemonic('v');
        bClose.setBounds(694, btnTop, 90, btnHeight); getContentPane().add(bClose);
        bClose.setMnemonic('C');*/
        

        statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
        getContentPane().add(statusBar);
        /***CV2019 code merges from CV10.2 line*/
        statusBar.setBounds(10, 635, 1174, 35);
        getContentPane().add(lPrivilege);
        
        /***CV2019 code merges from CV10.2 line*/
        lPrivilege.setBounds(680, 645, 80, 15);
        lPrivilege.setForeground(Color.red);
        lPrivilege.setVisible(false);
        
        //bHelp.setBounds(635, 340, 130, 25); getContentPane().add(bHelp);
        setIconImage(Images.MGRApp.getImage());
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setLocation((screenSize.width-width)/2,(screenSize.height-height)/2);
        setResizable(false);
        /* Purpose:	Master Switch off added ARS in registry
        * Created By: C.Shanmugavalli		Date:	20 Sep 2006
        * Default is changed to true for Enhancement line 16 Oct 2006
        * For 57 'arsenable' value will be always true. 8 Nov 2006
        */
        if("".equals(ManagerPreferences.getARSNode()) || "false".equals(ManagerPreferences.getARSNode()))
        	ManagerPreferences.setARSEnable("true");
        if("".equals(ManagerPreferences.getDRSNode()))
        	ManagerPreferences.setDRSEnable("true");
       
        //-------------Listeners------------------------------------------------
        bStart.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
        	switchService();
            }
        });
        bStop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
        	switchService();
            }
        });
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                exit();
            }
        });
        bRegister.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt){
                showRegisterDialog();
            }
        });
        bClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt){
                clearLog();
            }
        });
        bSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                saveLog();
            }
        });
        bHelp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
            }
        });
        bSettings.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (server == null) return;
                showSettingsDialog();
            }
        });
        bManager.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (server == null) return;
                setupManager();
            }
        });    
        bLoginManagement.addActionListener(new ActionListener(){
        	public void actionPerformed(ActionEvent evt){
        		 if (server == null) return;
        		 showLoginManagementDialog();
        	}
        });
        bAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                showAboutDialog();
            }
        });
        bClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                exit();
            }
        });
        bProxy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                setProxy();
            }
        });
        statusBar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                if (evt.getClickCount() > 1) switchService();
                    
            }
        });
        loadMenuBar();
    }//init components
    
    
    public void loadMenuBar(){
	JMenu fileMenu = new JMenu(resourceManager.getString("menu.FileMenu"));
	JMenu editMenu = new JMenu(resourceManager.getString("menu.EditMenu"));
	//JMenu viewMenu = new JMenu("Settings");
	JMenu helpMenu = new JMenu(resourceManager.getString("menu.helpMenu"));
	
	JMenuItem clearLog = new JMenuItem(resourceManager.getString("menuItem.clearLog"), Images.clearLog);
	JMenuItem clearSaveLog = new JMenuItem(resourceManager.getString("menuItem.SaveLog"), Images.saveLog);
	
	JMenuItem regServer = new JMenuItem(resourceManager.getString("menuItem.Register"), Images.regServer);
	JMenuItem settingsServer = new JMenuItem(resourceManager.getString("menuItem.Settings"), Images.settings);
	
	JMenuItem proxyServer = new JMenuItem(resourceManager.getString("menuItem.Proxy"), Images.proxyServer);
	JMenuItem managerSetup = new JMenuItem(resourceManager.getString("menuItem.ManagerSetup"), Images.mgrSetup);
	
	JMenuItem exitServer = new JMenuItem(resourceManager.getString("menuItem.Close") ,Images.close);
	JMenuItem aboutServer = new JMenuItem(resourceManager.getString("menuItem.About"), Images.about);
	
	fileMenu.setMnemonic('M');
	editMenu.setMnemonic('S');
	helpMenu.setMnemonic('H');
	
	clearLog.setMnemonic('C');
	clearSaveLog.setMnemonic('A');
	regServer.setMnemonic('R');
	settingsServer.setMnemonic('E');
	proxyServer.setMnemonic('P');
	managerSetup.setMnemonic('N');
	exitServer.setMnemonic('L');
	aboutServer.setMnemonic('B');
	
	
	fileMenu.add(clearSaveLog);
	fileMenu.add(clearLog);	
	fileMenu.add(exitServer);
	
	editMenu.add(settingsServer);
	editMenu.add(regServer);
	editMenu.add(proxyServer);
	editMenu.add(managerSetup);
	
	helpMenu.add(aboutServer);
	
	
	JLabel lblContentverse = new JLabel(resourceManager.getString("powered_by_contentverse"), SwingConstants.RIGHT);
	lblContentverse.setForeground(Color.GRAY);
	Font font = lblContentverse.getFont();
	lblContentverse.setFont(new Font(font.getName(), Font.PLAIN, 10));	
	
	mBar.add(fileMenu);
	mBar.add(editMenu);
	mBar.add(helpMenu);
	mBar.add(Box.createHorizontalGlue());		
	mBar.add(lblContentverse);
	mBar.add(Box.createHorizontalStrut(12));
	/***CV2019 code merges from CV10.2 line*/
	mBar.setBounds(0, 0, 1200, 24);
	mBar.setBackground(new Color(238, 242, 244));
	mBar.setBorderPainted(false);
	clearLog.setBackground(new Color(238, 242, 244));
	clearSaveLog.setBackground(new Color(238, 242, 244));
	regServer.setBackground(new Color(238, 242, 244));
	settingsServer.setBackground(new Color(238, 242, 244));
	proxyServer.setBackground(new Color(238, 242, 244));
	managerSetup.setBackground(new Color(238, 242, 244));
	exitServer.setBackground(new Color(238, 242, 244));
	aboutServer.setBackground(new Color(238, 242, 244));
	
	add(mBar);
	
	ActionListener menuActions = new ActionListener() {
		public void actionPerformed(ActionEvent e) {             
			JMenuItem menuItem = (JMenuItem) e.getSource();            	
			String selectedText =menuItem.getText();
			if (selectedText.equalsIgnoreCase(resourceManager.getString("menuItem.Settings"))){
		                if (server == null) return;
		                showSettingsDialog();
			} else if(selectedText.equalsIgnoreCase(resourceManager.getString("menuItem.Register"))){
			    showRegisterDialog();
			    
			}else if(selectedText.equalsIgnoreCase(resourceManager.getString("menuItem.Proxy"))){
			    setProxy();
			}else if(selectedText.equalsIgnoreCase(resourceManager.getString("menuItem.ManagerSetup"))){
			    if (server == null) return;
			    setupManager();
			}else if(selectedText.equalsIgnoreCase(resourceManager.getString("menuItem.Close"))){
			    exit();
			}else if(selectedText.equalsIgnoreCase(resourceManager.getString("menuItem.About"))){
			    showAboutDialog();
			}else if(selectedText.equalsIgnoreCase(resourceManager.getString("menuItem.clearLog"))){
			    clearLog();
			}else if(selectedText.equalsIgnoreCase(resourceManager.getString("menuItem.SaveLog"))){
			    saveLog();
			}
		}};
		
		clearLog.addActionListener(menuActions);
		clearSaveLog.addActionListener(menuActions);
		regServer.addActionListener(menuActions);
		settingsServer.addActionListener(menuActions);
		proxyServer.addActionListener(menuActions);
		managerSetup.addActionListener(menuActions);
		exitServer.addActionListener(menuActions);
		aboutServer.addActionListener(menuActions);	
    }
    private void switchService()
    {
        if (authorized | server == null)
            eSwitchService();
        else
            Util.Msg(this, resourceManager.getString("MSG.AcessDenied"), MANAGER_APP);
    }
    private void addServersListener()
    {
        comActive.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                connectToServer((String) comActive.getSelectedItem());
            }
        });
    }
    private void clearLog()
    {
    	boolean flag = Util.Ask(this,"   "+ resourceManager.getString("MSG.ClearLog")+"   ", MANAGER_APP);
        if (Logger.getDocument().getLength() > 0 && flag){
        	saveLog();
        	Logger.setText("");
        }else if(Logger.getDocument().getLength() > 0 && !flag){
        	Logger.setText("");
        }
    }
    private void saveLog()
    {
        Document doc = Logger.getDocument();
        try
        {
            /**
             * Linux Path handle on saveing the log file on click on Clear & Save button on the ViewWise Manager
             */
            String linuxPath = assertLogFolder();
            linuxPath = linuxPath.replaceAll("\\\\", "/");
            
            if(Util.getEnvOSName().indexOf("Linux") == -1)
        	linuxPath = linuxPath.replace('/', '\\');
            else if(Util.getEnvOSName().indexOf("Linux")!=-1 && (!linuxPath.startsWith("/"))){
        	linuxPath = "/"+linuxPath;
            }
            File logFile = new File(linuxPath + LOG_FILE_PREFIX + 
                                                       Util.getNow(1) + ".rtf");
            FileOutputStream fos = new FileOutputStream(logFile);
            new RTFEditorKit().write(fos, doc, 0, doc.getLength());
            fos.close();
            /*Issue 562
             * 
             * 
             * 
             * */   
           int maxFileCount =VWSPreferences.getLogFileCount();
           Util.maintainLogFiles(linuxPath,LOG_FILE_PREFIX,maxFileCount);           
           //End of fix 562 
        }
        catch(Exception e){}
        //Logger.setText("");
		
    }
    private String assertLogFolder()
    {
        File folder = new File(ManagerUtil.getHome() + Util.pathSep + LOG_FOLDER);
        try
        {
            if (!folder.exists()) folder.mkdir();
            return folder.getPath() + Util.pathSep;
        }
        catch(Exception e){}
        return "c:" + Util.pathSep;
    }
    private void connectToServer(String server)
    {
    	try{
    		if (server == null || server.equals("") || server.equals(runningServer)) 
    			return;
        if (serverConnector != null) serverConnector.cancel();
    		ManagerPreferences.setActiveServer(server);
    		ServerSchema ss = ManagerPreferences.getServer(server);
    		host = ss.address;
    		port = ss.comport;
    		String type = ss.type;
    		setStatus(resourceManager.getString("ManagerFrame.LookingStr")+" "+ server);
        if (!runningServer.equals("")) writeLogMarker(server);
    		serverConnector = new ServerConnector(this, host, port, type);
    		connector.schedule(serverConnector, 0, 10);
    		comActive.setToolTipText(type);
    		runningServer = server;
    	}catch (Exception e) {
    		System.out.println("Exception in connectToServer :: "+e.getMessage());
    		e.printStackTrace();
    	}
    }
    private void writeLogMarker(String server)
    {
        if (Logger.getDocument().getLength() == 0) return;
        SimpleAttributeSet sas = new SimpleAttributeSet();
        StyleConstants.setForeground(sas, Color.black);
        StyleConstants.setBackground(sas, Color.yellow);
        writeLog(server + "\r\n" , sas);
    }
    private void refreshServers()
    {
        comActive.removeAllItems();
        Vector v = ManagerPreferences.getServers(false);
        int size = v.size();
        for (int i = 0; i < size; i++)
        {
            comActive.addItem(v.elementAt(i));
        }
        if (noneRegistered())
        {
            if (serverConnector != null) serverConnector.cancel();
            Util.sleep(500);
            setStatus(" "+resourceManager.getString("ManagerFrame.NoRegServ"));
            switchServerButtons(false);
            return;
        }
        comActive.setSelectedIndex(-1);
        addServersListener();
        String s = ((ServerSchema) ManagerPreferences.getActiveServer(false)).
                                                                           name;
        comActive.setSelectedItem(s);
    }
    private boolean noneRegistered()
    {
        return (comActive.getItemCount() == 0 ? true : false); 
    }
    public String getStatus()
    {
        return statusBar.getText();
    }
    public void setStatus(String text) 
    {
        if (text.startsWith(resourceManager.getString("ManagerFrame.RunStr")))
        {   
            //switchServerButtons(true);
            statusBar.setIcon(Images.run);
            bStart.setEnabled(false);
            bStop.setEnabled(true);
        }
        else if(text.startsWith(resourceManager.getString("ManagerFrame.StopStr"))) 
        {
            switchServerButtons(false);
            statusBar.setIcon(Images.stop);
            bStart.setEnabled(true);
            bStop.setEnabled(false);
            
        }
        else if (text.startsWith(resourceManager.getString("ManagerFrame.LookStr"))) 
        {
            switchServerButtons(false);
            statusBar.setIcon(Images.look);
            bStart.setEnabled(true);
            bStop.setEnabled(false);
        }
        else {statusBar.setIcon(null);
        bStart.setEnabled(false);
        bStop.setEnabled(false);
        }
        statusBar.setText(text);
    }
    private void exit() 
    {
        if (Logger.getDocument().getLength() > 0)
        {
            int ret = Util.Option(this,"   "+resourceManager.getString("MSG.SaveLogBeforeClose")+"   ",MANAGER_APP);
            switch (ret)
            {
                case JOptionPane.CANCEL_OPTION: return;
                case JOptionPane.YES_OPTION: saveLog(); break;
                case JOptionPane.NO_OPTION:
            }
            quit();
        }
        else
            if (Util.Ask
                (this," "+resourceManager.getString("MSG.CloseServManager"), MANAGER_APP)) quit();
    }
    private void quit()
    {
        try{ if (server != null) server.managerEnd();}
        catch(RemoteException re){}
        System.exit(0);
    }
    private void setProxy()
    {
        new ProxyDialog(this, true).setVisible(true);
    }
    private void showRegisterDialog()
    {
        if (new RegisterDialog(this, true).fire())
        {
            refreshServers();
            setServer(server); 
        }
    }
    private void showAboutDialog()
    {
    	/*
     	 * new frame.show(); method is replaced with new frame.setVisible(true); 
     	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
     	 */
        new AboutManager(this, true).setVisible(true);
    }
    /**
     * Method modified for 
     * Enhancement:- 2083 Easy access to locked  users
     * To call the lock dialog seperately using the loginmanagement buttomd
     * 
     * 
     */
    private void showLoginManagementDialog()
    {
    	 String type = comActive.getToolTipText();
         if (type.equals(SERVER_VWS))
        	 new LoginManagementConfiguration(this, server, SERVER_VWS);
    }
    private void showSettingsDialog()
    {
        String type = comActive.getToolTipText();
        if (type.equals(SERVER_VWS))
                //new VWSSettings(this, true, server).show();
            	new VWConfiguration(this, server, SERVER_VWS);
        else if (type.equals(SERVER_DSS))
                //new DSSSettings(this, true, server).show();
            	new VWConfiguration(this, server, SERVER_DSS);
        else if(type.equals(SERVER_IXR))
                //new IXRSettings(this, true, server).show();
            new VWConfiguration(this, server, SERVER_IXR);
        //Updated for ARS, 20 July 2006, Valli
        else if(type.equals(SERVER_ARS))
        		//new ARSSettings(this, true, server).show();
            new VWConfiguration(this, server, SERVER_ARS);
        else if(type.equals(SERVER_DRS))
    		//new DRSSettings(this, true, server).show();
            new VWConfiguration(this, server, SERVER_DRS);
        else if(type.equals(SERVER_VNS)){
            new VWConfiguration(this, server, SERVER_VNS);
        }  else if(type.equals(SERVER_CSS)){
            new VWConfiguration(this, server, SERVER_CSS);
        }
        
    }
    private void eSwitchService() 
    {
        String msg;
        String svc = "";
        int state;
        String serverOS = null;
        String server = (String) comActive.getSelectedItem();
        String type = comActive.getToolTipText();
        if (type.equals(SERVER_VWS))        svc = SERIVCE_VWS;
        else if (type.equals(SERVER_DSS))   svc = SERIVCE_DSS;
        else if(type.equals(SERVER_IXR))    svc = SERIVCE_IXR;
        //Updated for ARS, 20 July 2006, Valli 
        else if(type.equals(SERVER_ARS))    svc = SERIVCE_ARS;
        //Updated for DRS, 19 Dec 2006, Valli 
        else if(type.equals(SERVER_DRS))    svc = SERIVCE_DRS;
        else if(type.equals(SERVER_VNS))    svc = SERIVCE_VNS;
        else if(type.equals(SERVER_CSS))    svc=SERIVCE_CSS;
        /* Issue 658: DSS is not able to stop through ViewWise manager. Has to kill through java - kill
         * Additional issue: Not able to start service through VW manager
         * Start server was not implemented for Netware, and implemented now
         * Author: C.Shanmugavalli Date: 8 Nov 2006
         */
        if(type.equals(SERVER_DRS)){
        	ServerSchema vwss = DRSPreferences.getVWS();
        	 if (vwss.address.trim() == "" || vwss.comport == 0) {
        		 Util.Msg(this, resourceManager.getString("MSG.DRSSeting1")+" "+ PRODUCT_NAME +" "+resourceManager.getString("MSG.DRSSeting2"), MANAGER_APP);
                 return;
        	 }
        }
        if(type.equals(SERVER_ARS)){
        	ServerSchema vwss = ARSPreferences.getVWS();
        	 if (vwss.address.trim() == "" || vwss.comport == 0) {
        		 Util.Msg(this,  resourceManager.getString("MSG.ARSSeting1")+" "+ PRODUCT_NAME +" "+resourceManager.getString("MSG.ARSSeting2"), MANAGER_APP);
                 return;
        	 }
        }
        if(type.equals(SERVER_VNS)){
        	ServerSchema vwss = VNSPreferences.getVWS();

        	if (vwss.address.trim() == "" || vwss.comport == 0) {
        		Util.Msg(this, resourceManager.getString("MSG.VNSSetting1")+" "+ PRODUCT_NAME+" "+resourceManager.getString("MSG.VNSSetting2"), MANAGER_APP);
        		return;
        	}
        }
        if(type.equals(SERVER_CSS)) {
        	ServerSchema vwss = CSSPreferences.getVWS();
        	if (vwss.address.trim() == "" || vwss.comport == 0) {
        		Util.Msg(this, resourceManager.getString("MSG.CSSetting1")+" "+ PRODUCT_NAME+" "+resourceManager.getString("MSG.CSSetting2"), MANAGER_APP);
        		return;
        	}
        
        }
        serverOS = Util.getEnvOSName();
        if ( statusBar.getText().startsWith(resourceManager.getString("ManagerFrame.RunStr")))
        {
            msg = resourceManager.getString("MSG.StopServ1")+" "+server+" "+ resourceManager.getString("MSG.StopServ2");
            state = 0;
            //serverOS = getServerOS();
        }
        else
        {
            msg = resourceManager.getString("MSG.StartServ1")+" " +  server +" " + resourceManager.getString("MSG.StopServ2");
            state = 1;
            //serverOS = getServerOS();
            //serverOS = Util.getEnvOSName();
        }
        if(serverOS != null && !serverOS.startsWith("Windows") && (serverOS.startsWith("NetWare")||serverOS.indexOf("Linux")!=-1)) 
        {   //server OS is Netware or Linux...requesting start server/ shutdown server
        	
            if (Util.Ask(this, msg, MANAGER_APP)){ 
            	if(msg.startsWith(resourceManager.getString("MSG.StopServ1")))
            		shutDownServer();
            	else if(msg.startsWith(resourceManager.getString("MSG.StartServ1")))
            		startServer(type, serverOS);
            }
            //return;
        }
        String managerOS = Util.getEnvOSName();
        if (managerOS.startsWith("Windows"))
        {   //  Server OS Unknown...requesting startup or shutdown
            //  assuming Server OS is Windows
            if (Util.Ask(this, msg, MANAGER_APP))
            {
                int ret = ManagerUtil.switchWindowsService(state, host, svc);
                switch (ret)
                {
                    case 5: Util.Msg
                              (this, resourceManager.getString("MSG.Error5"), MANAGER_APP); 
                }
            }
        }
    }
    private String getServerOS()
    {
        try
        {
            return server.getServerOS();
        }
        catch(Exception e){return "";}
    }
    private void shutDownServer()
    {
        try
        {
            server.exit();
        }
        catch(Exception e){}
    }
    /*Added for issue # 658 additional issue*/
    private void startServer(String type, String serverOS){
    	try
        {
    		//java -Xms256m -Xmx512m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.vws.server.ViewWiseServer
    		//java -Xms128m -Xmx512m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.dss.server.ViewWiseDSS
    		if(serverOS == null || serverOS.startsWith("Windows") ) return;
    		Runtime runtime = Runtime.getRuntime();
	    	if (type.equals(SERVER_VWS)){
	            Process process = runtime.exec("java -Xms512m -Xmx768m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.vws.server.ViewWiseServer");
	    	}else if (type.equals(SERVER_DSS)){
	    		Process process = runtime.exec("java -Xms512m -Xmx768m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.dss.server.ViewWiseDSS");
	    	}else if (type.equals(SERVER_IXR)){
	    		//Process process = runtime.exec("java -Xms128m -Xmx512m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.ixr.server.ViewWiseIndexer");
	    		Process process = runtime.exec("java -Xms512m -Xmx768m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.ixr.server.ViewWiseIndexer");
	    	}else if (type.equals(SERVER_ARS)){
	    		Process process = runtime.exec("java -Xms512m -Xmx768m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.ixr.server.ViewWiseARS");
	    	}else if (type.equals(SERVER_DRS)){
	    		Process process = runtime.exec("java -Xms512m -Xmx768m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.ixr.server.ViewWiseDRS");
	    	}else if (type.equals(SERVER_VNS)){
	    		Process process = runtime.exec("java -Xms512m -Xmx768m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.vns.server.ViewWiseVNS");
	    	}else if (type.equals(SERVER_CSS)){
	    																								
	    		Process process = runtime.exec("java -Xms512m -Xmx768m -DVIEWWISE_HOME=$VIEWWISE_HOME com.computhink.vws.csserver.ViewWiseCSS");
	    	}
        }
        catch(Exception e){
        	//Util.Msg(this, "Error in starting server "+e.getMessage() , "Info");
        	//VWSLog.err("Error in starting server "+e.getMessage());
        }
    
    }
    private void resetLog()
    {
        Logger.setDocument(new DefaultStyledDocument());
        Logger.setFont(new Font("Arial", Font.PLAIN, 12));
        Logger.setEditable(false);
        Logger.setRequestFocusEnabled(false);
    }
    private void switchServerButtons(boolean b)
    {
        bSettings.setEnabled(b);
        bManager.setEnabled(b);
    }
    private boolean authenticateManager(CIServer svr) 
    {
        try
        {
            if (ManagerPreferences.getManager(runningServer).equals
                                         (svr.getManager()))        return true;
        }
        catch(Exception e){}
        return false;
    }
    public void setServer(CIServer svr) 
    {
        if (svr == null)
        {
            lPrivilege.setVisible(false);
            server = null;
            switchServerButtons(false);
            return;
        }
        if (!authenticateManager(svr))
        {
            lPrivilege.setVisible(true);
            authorized = false;
            switchServerButtons(false);
            return;
        }
        authorized = true;
        lPrivilege.setVisible(false);
        switchServerButtons(true);
        try
        {
            String msg = svr.managerBegin(mySchema);
            if (msg.equals(""))
            {
                server = svr;
                return;
            }
            else
            {
                Util.Msg(this, msg, MANAGER_APP);
            }
        }
        catch(RemoteException re){}
        server = null;
    }
    private void writeLog(String msg, SimpleAttributeSet sas)
    {
        Document doc = Logger.getDocument();
        try
        {
            doc.insertString(doc.getLength(), msg ,sas);
            Logger.repaint();
            Logger.setCaretPosition(Logger.getDocument().getLength());
        }
        catch (Exception e){}
    }
    private void setupManager()
    {
        new ManagerCredentials(this, true).setVisible(true);
        setServer(server); 
    }
    private class LogWorker extends TimerTask 
    {
        public LogWorker() 
        {
        }
        public void run() 
        {
            if (server == null) return;
            SimpleAttributeSet sas = new SimpleAttributeSet();
            try
            {
                for (int x = 0; x < server.getLogCount(); x++)
                {
                   LogMessage lm = server.getLogMsg();
                   StyleConstants.setForeground(sas, lm.color);
                   writeLog(lm.fullMsg , sas);
                }
            }
            catch(Exception e){}
        }
    }
    private class ManagerCredentials extends JDialog 
    {
         private JTextField tManager;
         private JPasswordField tPass, tConfirm;
         private JLabel lManager, lPass, lConfirm; 
         private JButton bRegister, bClose;
         
         private static final int WIDTH = 350;
         private static final int HEIGHT = 200;
         
         public ManagerCredentials(JFrame parent, boolean modal)
         {
            super(parent, modal);
            initComponents();
            getCredentials();
         }
         private void initComponents() 
         {
             tManager = new JTextField();
             tPass = new JPasswordField();
             tConfirm = new JPasswordField();
             lManager = new JLabel(resourceManager.getString("ManagerCreLbl.ManagerID"));
             lPass = new JLabel(resourceManager.getString("ManagerCreLbl.NewPassword"));
             lConfirm = new JLabel(resourceManager.getString("ManagerCreLbl.ConfirmPassword"));
             bRegister = new JButton(resourceManager.getString("ManagerCreLbl.Register"));
             bClose = new JButton(resourceManager.getString("ManagerCreLbl.Close"));
             
             getContentPane().setLayout(null);
             setTitle(resourceManager.getString("ManagerTitle.ManagerSetup")+" "+ runningServer);
             lManager.setBounds(20, 20, 100, 16); getContentPane().add(lManager);
             lPass.setBounds(20, 50, 100, 16); getContentPane().add(lPass);
             lConfirm.setBounds(20, 80, 110, 16); getContentPane().add(lConfirm);
             
             tManager.setBounds(140, 20, 170, 20); getContentPane().add(tManager);
             tPass.setBounds(140, 50, 170, 20); getContentPane().add(tPass);
             tConfirm.setBounds(140, 80, 170, 20); getContentPane().add(tConfirm);
             
             bRegister.setBounds(110, 120, 90, 26); getContentPane().add(bRegister);
             bClose.setBounds(220, 120, 90, 26); getContentPane().add(bClose);
             
             Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
             setSize(new java.awt.Dimension(WIDTH, HEIGHT));
             setLocation((screenSize.width-WIDTH)/2,(screenSize.height-HEIGHT)/2);
             setResizable(false);
             
              bClose.addActionListener(new ActionListener(){
              public void actionPerformed(ActionEvent evt) {
                   closeDialog();
                }
              });
              bRegister.addActionListener(new ActionListener(){
              public void actionPerformed(ActionEvent evt) {
                   setCredentials();
                }
              });
         }
         private void getCredentials()
         {
             if (server == null)
             {
                  Util.Msg(this, resourceManager.getString("MSG.LogConn"), MANAGER_APP);
                  return;
             }
             try
             {
                 String credentials = server.getManager();
                 if (credentials != null)
                 {
                     StringTokenizer st = new StringTokenizer(credentials, "||");
                     if (st.hasMoreTokens()) 
                         tManager.setText( (String) st.nextToken());
                     if (st.hasMoreTokens())
                     {
                         tPass.setText( (String) st.nextToken());
                         tConfirm.setText(new String(tPass.getPassword()));
                     }
                 }
             }
             catch(Exception e){}
         }
         private void setCredentials()
         {
             if (server == null)
             {
                  Util.Msg(this, resourceManager.getString("MSG.LogConn"), MANAGER_APP);
                  return;
             }
             String p1 = new String(tPass.getPassword());
             String p2 = new String(tConfirm.getPassword());
             if (!p1.equals(p2))
             {
                  Util.Msg(this, resourceManager.getString("MSG.PassworNotConfirm"), MANAGER_APP);
                  return;
             }
             try
             {
                 if (server.setManager(tManager.getText(), 
                                               new String(tPass.getPassword())))
                     Util.Msg(this, resourceManager.getString("MSG.CredentialsUpdated"), MANAGER_APP);
                 else
                     Util.Msg(this,resourceManager.getString("MSG.ErrorUpdateCredentials"), MANAGER_APP);
                     
             }
             catch(Exception e){}
         }
         private void closeDialog() 
         {
             setVisible(false);
             dispose();
         }
        
    }
    public static void main(String[] args) {
		new ManagerFrame();
		setUIFont(new FontUIResource(Manager.fontName, Font.PLAIN, Manager.fontSize));
	}
    /*
  	 * Added for localization to set font
  	 * Modified by Madhavan
  	 */
    public static void setUIFont(javax.swing.plaf.FontUIResource f) {
    	java.util.Enumeration keys = UIManager.getDefaults().keys();
    	while (keys.hasMoreElements()) {
    	    Object key = keys.nextElement();
    	    Object value = UIManager.get(key);
    	    
    	    if (value instanceof javax.swing.plaf.FontUIResource){		
    		UIManager.put(key, f);
    	    }
    	}
        }
}
