/*
 * DSSSettings.java
 *
 * Created on November 30, 2003, 4:24 PM
 */
package com.computhink.manager;
import com.computhink.common.*;
import com.computhink.dss.server.*;
import com.computhink.vws.server.*;
/**
 *
 * @author  Administrator
 */
import javax.swing.*;

import java.awt.*;
import java.awt.Toolkit.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.rmi.*;
import java.io.*;
import java.util.*;
import java.net.InetAddress;

public class DSSSettings extends JDialog implements ManagerConstants, 
                                                                  ViewWiseErrors 
{
    private JPanel pPorts, pProxy, pVWS, pStores;
    // Issue 511
    private JLabel lComPort, lDataPort, lProxyHost, lProxyPort,
                                      lVWSPort, lVWSHost, lRoom, lNote1, lNote2, lDSSSvrName, lDSSSvrDisplay;
   // Issue 511
    private JTextField tComPort, tDataPort, tProxyHost, tProxyPort, tVWSPort, 
                                                                       tVWSHost;
    private JComboBox comRooms;
    private JButton bOK, bCancel, bAdd, bRemove, bConnect;
    private JList lstStores;
    private JScrollPane spFolders;
    private JCheckBox cbProxy, cbLog;

    private DSS dss;
    private VWS vws;
    private ServerSchema vwsSchema;
    private Vector vStores = new Vector();
    private String selectedRoom = "";
    private String myAddress = "";
    
    private static final int width = 500;
    private static final int height = 550;
    // Issue 511
    ListSelectionModel listSelectionModel;
    //Issue 511
    public DSSSettings(JFrame parent, boolean modal, CIServer server) 
    {
        super(parent, modal);
        this.dss = (DSS) server;
        getLocalAddress();
        initComponents();
        getCurrentSettings();
        if (ConnecttoVWS()) refreshStores();
    }
    private void getLocalAddress()
    {
        try
        {
            myAddress = InetAddress.getLocalHost().getHostAddress();
        }
        catch(Exception e){}
    }
    private void initComponents() 
    {
        pPorts = new JPanel();
        pProxy = new JPanel();
        pVWS = new JPanel();
        pStores = new JPanel();

        tComPort = new JTextField();
        tDataPort = new JTextField();
        tProxyHost = new JTextField();
        tProxyPort = new JTextField();
        tVWSHost = new JTextField();
        tVWSPort = new JTextField();
        comRooms = new JComboBox();

        cbProxy = new JCheckBox();
        cbLog = new JCheckBox("Log Information");
        spFolders = new JScrollPane();
        lstStores = new JList();
        
        bAdd = new JButton("Add");
        bRemove = new JButton("Remove");
        bOK = new JButton("OK");
        bCancel = new JButton("Cancel");
        bConnect = new JButton("Connect");
        
        lProxyHost = new JLabel("Proxy Host:");
        lProxyPort = new JLabel("Port:");
        lVWSHost = new JLabel("Host:");
        lVWSPort = new JLabel("Communication Port:");
        lComPort = new JLabel("Communication Port:");
        lDataPort = new JLabel("Data Port:");
        lRoom = new JLabel("Room:");
        lNote1 = new JLabel
        ("Server must be restarted for the new settings to take effect.");
        lNote2 = new JLabel
        (PRODUCT_NAME + " Server settings will be applied.");
		// Issue 511
        lDSSSvrName = new JLabel("Host : Port -");
        lDSSSvrDisplay = new JLabel(""); 
        // Issue 511
        getContentPane().setLayout(null);
        setTitle("DSS Settings - " + getServerName());
        setResizable(false);
        
        pPorts.setLayout(null);
        pPorts.setBorder(new TitledBorder("Server Ports"));
        pPorts.setFocusable(false); getContentPane().add(pPorts);
        pPorts.setBounds(20, 10, 450, 70);
        pPorts.add(lComPort); lComPort.setBounds(30, 30, 120, 16);
        pPorts.add(lDataPort); lDataPort.setBounds(300, 30, 60, 16);
        pPorts.add(tComPort); tComPort.setBounds(160, 30, 50, 20);
        pPorts.add(tDataPort); tDataPort.setBounds(370, 30, 50, 20);
        
        pProxy.setLayout(null);
        pProxy.setBorder(new TitledBorder("         Use Http Proxy"));
        pProxy.setFocusable(false); getContentPane().add(pProxy);
        pProxy.setBounds(20, 95, 450, 70);
        pProxy.add(lProxyHost); lProxyHost.setBounds(30, 30, 70, 16);
        pProxy.add(lProxyPort); lProxyPort.setBounds(300, 30, 30, 16);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 21);
        pProxy.add(tProxyHost); tProxyHost.setBounds(110, 30, 110, 20);
        pProxy.add(tProxyPort);tProxyPort.setBounds(370, 30, 50, 20);

        pVWS.setLayout(null);
        pVWS.setBorder(new TitledBorder(PRODUCT_NAME + " Server"));
        pVWS.setFocusable(false); getContentPane().add(pVWS);
        pVWS.setBounds(20, 180, 450, 70);
        pVWS.add(lVWSHost); lVWSHost.setBounds(30, 30, 70, 16);
        pVWS.add(lVWSPort); lVWSPort.setBounds(240, 30, 120, 16);
        pVWS.add(tVWSHost); tVWSHost.setBounds(80, 30, 110, 20);
        pVWS.add(tVWSPort); tVWSPort.setBounds(370, 30, 50, 20);

        pStores.setLayout(null);
        pStores.setBorder(new TitledBorder("Document Stores"));
        pStores.setFocusable(false); getContentPane().add(pStores);
        //Issue 511
		listSelectionModel = lstStores.getSelectionModel();
        listSelectionModel.addListSelectionListener(
                new ListSelectionHandler());       
        pStores.setBounds(20, 265, 450, 200); 
        // Issue 511     
        spFolders.setViewportView(lstStores);
        pStores.add(lRoom); lRoom.setBounds(30, 30, 60, 16);
        pStores.add(comRooms); comRooms.setBounds(80, 30, 110, 20);
        pStores.add(spFolders); spFolders.setBounds(30, 60, 390, 65);
		// Issue 511
        pStores.add(lDSSSvrName); lDSSSvrName.setBounds(30, 135, 70, 20);
        pStores.add(lDSSSvrDisplay); lDSSSvrDisplay .setBounds(95, 135, 150, 20);
        lDSSSvrName.setVisible(false);
	    lDSSSvrDisplay.setVisible(false);         
        pStores.add(bAdd); bAdd.setBounds(220, 160, 90, 26);
        pStores.add(bRemove); bRemove.setBounds(330, 160, 90, 26);
        // Issue 511
        lstStores.setBackground(new Color(255, 255, 204));
        
        bAdd.setEnabled(false);
        bRemove.setEnabled(false);
		// Issue 511
        getContentPane().add(bOK); bOK.setBounds(239, 475, 90, 26);
        getContentPane().add(bCancel); bCancel.setBounds(349, 475, 90, 26);
        // Issue 511
        getContentPane().add(cbLog); cbLog.setBounds(20, 475, 200, 20);
        bOK.setToolTipText(lNote1.getText());
        bConnect.setToolTipText(lNote2.getText());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(width, height));
        setLocation((screenSize.width - width)/2,(screenSize.height -height)/2);
        //-------------Listeners------------------------------------------------
        addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });
        bOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
        bAdd.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                addStore();
            }
        });
        bRemove.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                removeStore();
            }
        });
        bConnect.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                ConnecttoVWS();
            }
        });
        comRooms.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if (comRooms.getSelectedIndex() >= 0)
                {
                    selectedRoom = (String) comRooms.getSelectedItem();
                    refreshStores();
                }
            }
        });
        //----------------------------------------------------------------------
    }
    private boolean ConnecttoVWS()
    {
        applyVWSSettings();
        vws = (VWS) Util.getServer(vwsSchema);
        if (vws != null)
        {
            switchStores(true);
            populateRooms();
            refreshStores();
            return true;
        }
        else
        {
            switchStores(false);
            return false;
        }
    }
    private void populateRooms()
    {
        try
        {
            Vector ret = vws.getRoomNames();
            comRooms.removeAllItems();
            for (int i = 0; i < ret.size(); i++)
            {
                comRooms.addItem(ret.get(i));
                comRooms.setSelectedIndex(i);
            }
        }
        catch(Exception e){}
    }
    private void applyVWSSettings()
    {
        vwsSchema.address = tVWSHost.getText();
        vwsSchema.comport = Util.to_Number(tVWSPort.getText());
        try
        {
            dss.setVWS(vwsSchema);
        }
        catch(Exception e){}
    }
    private void switchStores(boolean b)
    {
        if (!vwsSchema.address.equals(myAddress)) return;
        bAdd.setEnabled(b);
        bRemove.setEnabled(b);
    }
    private String getServerName()
    {
        try
        {
            return dss.getSchema().name;
        }
        catch(RemoteException re)
        {
            return "";
        }
    }
    private void removeStore()
    {
        String store = (String) lstStores.getSelectedValue();
        if (store != null)
        {
             if (!Util.Ask(this, "Remove path '" + store + "'?", getTitle())) 
                                                                        return;
             if (removeStoreFromServer(store))
             {
                 refreshStores();
             }
        }
    }
    private void addStore()
    {
        if (selectedRoom.equals("")) return;
        String store = getFolder();
        if (store != null)
        {
            if (storeExists(store))
            {
                Util.Msg(this, "Path already registered", this.getTitle());
                return;
            }
            if (storeRoot(store))
            {
                Util.Msg(this, "Root path not allowed", this.getTitle());
                return;
            }
            if (addStoreToServer(store))  
            {
                refreshStores();
            }
        }
    }
    private ServerSchema createStoreSchema(String store)
    {
        ServerSchema mySchema = Util.getMySchema();
        try
        {
            mySchema.comport = dss.getComPort();
        }
        catch(Exception e){}
        mySchema.path = store;
        mySchema.room = selectedRoom;
        return mySchema;
    }
    private void getDSSStores(Vector stores)
    {
        try
        {
            Vector ret = vws.getDSSStores(createStoreSchema(""), stores);
            stores.addAll(ret);
        }
        catch(Exception e){}
    }
    // Issue 511 
    private String getDSSIPPort(String location)
    {
        try
        {        	
        	Vector ret = vws.getDSSIPPort(createStoreSchema(""), location);
            return ret.get(0).toString();
        }        
        catch(Exception e){
        DSSLog.err("Error in Database - getDSSIPPort");	
        }
        return "";
    }
    //Issue 511
    private boolean addStoreToServer(String store)
    {
        try
        {
            if ( vws.addDSSStore(createStoreSchema(store)) > 0) return true;
        }
        catch(Exception e){}
        return false;
    }
    private boolean removeStoreFromServer(String store)
    {
        try
        {
            if ( vws.removeDSSStore(createStoreSchema(store)) > 0 ) return true;
        }
        catch(Exception e){}
        return false;
    }
    private boolean storeExists(String path)
    {
        if (vStores.contains(path))
           return true;
        else
           return false;
    }
    private boolean storeRoot(String path)
    {
        if (new File(path).getParent() == null)
            return true;
        else
            return false;
    }
    private void refreshStores()
    {
        vStores.removeAllElements();
        getDSSStores(vStores);
        lstStores.setListData(vStores);
        lstStores.setSelectedIndex(vStores.size()-1);
    }
    private void setSettings()
    {
        try
        {
            dss.setComPort(Integer.parseInt(tComPort.getText()));
            dss.setDataPort(Integer.parseInt(tDataPort.getText()));
            dss.setProxy(cbProxy.getSelectedObjects() != null? true : false);
            dss.setProxyHost(tProxyHost.getText());
            String pPort = tProxyPort.getText();
            if (pPort.equals(""))
                dss.setProxyPort(0);
            else
                dss.setProxyPort(Integer.parseInt(pPort));
            vwsSchema.address = tVWSHost.getText();
            vwsSchema.comport = Util.to_Number(tVWSPort.getText());
            dss.setVWS(vwsSchema);
            dss.setLogInfo(cbLog.isSelected());
        }
        catch(Exception e){}
    }
    private void getCurrentSettings()
    {
        try
        {
            tComPort.setText(String.valueOf(dss.getComPort()));
            tDataPort.setText(String.valueOf(dss.getDataPort()));
            cbProxy.setSelected(dss.getProxy());
            tProxyHost.setText(dss.getProxyHost());
            int pPort = dss.getProxyPort();
            if (pPort > 0)
                tProxyPort.setText(String.valueOf(pPort));
            else
                tProxyPort.setText("");
            vwsSchema = dss.getVWS();
            tVWSHost.setText(vwsSchema.address);
            int vPort = vwsSchema.comport;
            if (vPort > 0)
                tVWSPort.setText(String.valueOf(vPort));
            else
                tVWSPort.setText("");
            cbLog.setSelected(dss.getLogInfo());
            cbProxyStateChanged();
        }
        catch(RemoteException re)
        {
        }
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    private void closeDialog() 
    {
        setVisible(false);
        dispose();
    }
    private String getFolder()
    {
    	File folder = null;
        JFileChooser fc = new JFileChooser(new File("c:\\"));        
        fc.setFileSelectionMode(fc.DIRECTORIES_ONLY);
        int returnVal = fc.showDialog(this, "select");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	folder = fc.getSelectedFile();
        }
        if (folder != null)
            return folder.getPath();
        else
            return null;
    }
	// Issue 511
    class ListSelectionHandler implements ListSelectionListener {
    	public void valueChanged(ListSelectionEvent e) { 
  		  String location =(String)lstStores.getSelectedValue();
  		  if(location != null){
  		  String IPPort = getDSSIPPort(location);
  		  if(!IPPort.equalsIgnoreCase("")){
		      lDSSSvrDisplay.setText(IPPort.substring(0,IPPort.indexOf(":"))+" : "+IPPort.substring(IPPort.indexOf(":")+1,IPPort.length()));
  		  	  lDSSSvrName.setVisible(true);
			  lDSSSvrDisplay.setVisible(true); 
  		   }
  		  } 
  		  else{
  		  lDSSSvrName.setVisible(false);
  		  lDSSSvrDisplay.setVisible(false);   		  
  		  }  
  	}    
    } 
	// Issue 	511
}
