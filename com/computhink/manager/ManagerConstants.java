/*
 * ManagerConstants.java
 *
 * Created on November 9, 2003, 3:24 PM
 */

package com.computhink.manager;
import com.computhink.common.*;
import com.computhink.resource.ResourceManager;

/**
 *
 * @author  Administrator
 */
public interface ManagerConstants extends Constants
{
    public static final String MANAGER_PREF_ROOT = "/computhink/" + PRODUCT_NAME.toLowerCase() + "/manager";
    public static final String MANAGER_APP = ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ResourceManager.getDefaultManager().getString("ManagerApp.ServManager");
    public static final String PANEL_TITLE =ResourceManager.getDefaultManager().getString("PANEL_TITLE.ActiveServer");  
    public static final String BTN_CLOSE_MANAGER = ResourceManager.getDefaultManager().getString("BtnToolTip.Close");
    public static final String BTN_CLEAR_LOG = ResourceManager.getDefaultManager().getString("BtnToolTip.ClearLog");
    public static final String BTN_SAVE_LOG = ResourceManager.getDefaultManager().getString("BtnToolTip.SaveLog");
    public static final String BTN_PROXY = ResourceManager.getDefaultManager().getString("BtnToolTip.ProxySettings");
    public static final String BTN_MANAGE = ResourceManager.getDefaultManager().getString("BtnToolTip.RegisterServers");
    public static final String BTN_ABOUT = ResourceManager.getDefaultManager().getString("BtnToolTip.About");
    public static final String BTN_HELP = ResourceManager.getDefaultManager().getString("Help");
    public static final String BTN_SETTINGS = ResourceManager.getDefaultManager().getString("BtnToolTip.Settings");
    public static final String BTN_MANAGER =ResourceManager.getDefaultManager().getString("BtnToolTip.ManagerSetup");
    public static final String BTN_LOGIN_MANAGER=ResourceManager.getDefaultManager().getString("BtnToolTip.LoginManagement");

    public static final String LOG_FILE_PREFIX = "ManagerLog-";
    public static final String LOG_FOLDER = "log";
    
    public static final String SERIVCE_VWS = PRODUCT_NAME + "Service";
    public static final String SERIVCE_IXR = PRODUCT_NAME + "Indexer";
    public static final String SERIVCE_DSS = PRODUCT_NAME + "Storage";
    // Constant added for ARS, 20 July 2006, Valli
    public static final String SERIVCE_ARS = PRODUCT_NAME + "Retention";
    // Constant added for DRS, 20 Dec 2006, Valli
    public static final String SERIVCE_DRS = PRODUCT_NAME + WORKFLOW_MODULE_NAME;
    public static final String SERIVCE_VNS = PRODUCT_NAME + "Notification";
    public static final String SERIVCE_CSS = PRODUCT_NAME + "Content Sentinel";

}
