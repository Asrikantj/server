package com.computhink.manager;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.computhink.drs.server.DRS;
import com.computhink.drs.server.DRSConstants;
import com.computhink.drs.server.DRSPreferences;
import com.computhink.common.CIServer;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWClient;
import com.computhink.vws.server.VWS;


public class DRSSettingsPanel extends VWPanel implements ManagerConstants, DRSConstants
{
    private JButton bOK, bCancel;
    private VWPanel pPorts, pProxy, pVWS, pLang, pPdfSettings;
    private JLabel lDataPort, lProxyPort, lProxyHost, lComPort, 
                              lVWSPort, lVWSHost,lNote,  lSnooze, lMinute, lBurnAnn, lRedWhiteout, lColor, lDepth;
    private JTextField tProxyPort, tComPort, tDataPort, tProxyHost, tVWSHost, tVWSPort;
    private JCheckBox cbProxy, cbLog, burnAnnCb, redWhiteoutCb, colorCb; 
    JComboBox depthComboBox;
    String depthCombo[]={"8","24"};   
    
    JRadioButton ChkOpt1 = new JRadioButton();
    JRadioButton ChkOpt2 = new JRadioButton();
    ButtonGroup group1 = new ButtonGroup(); 
    //SpinnerNumberModel hr =  new SpinnerNumberModel(0, 0, 23, 1);
    //SpinnerNumberModel min =  new SpinnerNumberModel(0, 0, 59, 1);
    //SpinnerNumberModel sec =  new SpinnerNumberModel(0, 0, 59, 1);
    //JSpinner hrSpin = new JSpinner(hr);
    //JSpinner minSpin = new JSpinner(min);
    //JSpinner secSpin = new JSpinner(sec);
    JSpinner jSpinner;
    JSpinner minuteSpinner;
    SpinnerDateModel dateModel = new SpinnerDateModel();
    SpinnerNumberModel minuteModel = new SpinnerNumberModel(5, 1, 59, 1);
    
    private static final int width = 500;
    private static final int height = 510;
    private static final int schedStartByAuto = 0;
    private static final int schedStartByFrequency= 1;
    private static ResourceManager resourceManager=null;
        
    private DRS drs;
    private VWS vws;
    private ServerSchema vwsSchema;
    private Vector vStores = new Vector();
    private String selectedRoom = "";
    private String myAddress = "";
    

    public DRSSettingsPanel(){
	super();
	initComponents();
	setBackground(new Color(238, 242, 244));
    }
    public DRSSettingsPanel(CIServer server) 
    {
        super();
        this.drs = (DRS) server;
        initComponents();
        getCurrentSettings();
    }
    /*
    public ARSSettings(JFrame parent, boolean modal) 
    {
        super(parent, modal);
        initComponents();
        getCurrentSettings();
    }
    */
     private void initComponents() 
     {
    	//setTitle("Auto Retention Server Settings "); 
    	//setTitle(SETTINGS);
    	 resourceManager=ResourceManager.getDefaultManager();
        pPorts = new VWPanel();
        pVWS = new VWPanel();
        pProxy = new VWPanel();
        pLang = new VWPanel();
        pPdfSettings = new VWPanel();
        cbProxy = new JCheckBox();
        cbLog = new JCheckBox(resourceManager.getString("DRSChK.LogInfo"));
        burnAnnCb = new JCheckBox(resourceManager.getString("DRSLbl.BurnAnnotation"));
        colorCb = new JCheckBox(resourceManager.getString("DRSLbl.Color"));
        redWhiteoutCb = new JCheckBox(resourceManager.getString("DRSLbl.RedWhiteOut"));
        depthComboBox = new JComboBox(depthCombo);   
        bOK = new JButton(resourceManager.getString("DRSBtn.Apply"));
        bCancel = new JButton(resourceManager.getString("DRSBtn.Cancel"));
        
        cbProxy.setBackground(this.getBackground());
        cbLog.setBackground(this.getBackground());
        burnAnnCb.setBackground(this.getBackground());
        colorCb.setBackground(this.getBackground());
        redWhiteoutCb.setBackground(this.getBackground());
        depthComboBox.setBackground(this.getBackground());
        bOK.setBackground(this.getBackground());
        bCancel.setBackground(this.getBackground());
        ChkOpt1.setBackground(this.getBackground());
        ChkOpt2.setBackground(this.getBackground());

        tComPort = new JTextField();
        tDataPort = new JTextField();
        tProxyHost = new JTextField();
        tProxyPort = new JTextField();
        tVWSHost = new JTextField();
        tVWSPort = new JTextField();

        lProxyHost = new JLabel(resourceManager.getString("DRSLbl.ProxyHost"));
        lProxyPort = new JLabel(resourceManager.getString("DRSLbl.Port"));
        lVWSHost = new JLabel(resourceManager.getString("DRSLbl.VWSHost"));
        lVWSPort = new JLabel(resourceManager.getString("DRSLbl.VWSComPort"));
        lBurnAnn = new JLabel(resourceManager.getString("DRSLbl.BurnAnnotation"));
        lColor = new JLabel(resourceManager.getString("DRSLbl.Color"));
        lRedWhiteout = new JLabel(resourceManager.getString("DRSLbl.RedWhiteOut"));
        lDepth = new JLabel(resourceManager.getString("DRSLbl.Depth"));
        lComPort = new JLabel(resourceManager.getString("DRSLbl.ComPort"));
        lDataPort = new JLabel(resourceManager.getString("DRSLbl.Dataport"));
        lNote = new JLabel(resourceManager.getString("DRSLbl.Note"));
        lSnooze = new JLabel (resourceManager.getString("DRSLbl.Snooze"));
        lMinute = new JLabel (resourceManager.getString("DRSLbl.minutes"));
//        lBurnAnn = new JLabel (resourceManager.getString("DRSLbl.BurnAnnotation"));
        setLayout(null);
        //setResizable(false);
        
        pPorts.setLayout(null);
        pPorts.setBorder(new TitledBorder(resourceManager.getString("DRSTitleBorder.ServPorts")));
        pPorts.setFocusable(false); 
        add(pPorts);
        pPorts.setBounds(20, 10, 470, 60);
        pPorts.add(lComPort); lComPort.setBounds(30, 30, 120, 16);
        pPorts.add(lDataPort); lDataPort.setBounds(300, 30, 60, 16);
        pPorts.add(tComPort); tComPort.setBounds(160, 30, 50, 20);
        pPorts.add(tDataPort); tDataPort.setBounds(370, 30, 50, 20);
        
        pProxy.setLayout(null);
        pProxy.setBorder(new TitledBorder("         "+resourceManager.getString("DRSTitleBorder.HttpProxy")));
        pProxy.setFocusable(false);  
        add(pProxy);
        pProxy.setBounds(20, 80, 470, 60);
        pProxy.add(lProxyHost); lProxyHost.setBounds(30, 30, 70, 16);
        pProxy.add(lProxyPort); lProxyPort.setBounds(325, 30, 30, 16);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 21);
        pProxy.add(tProxyHost); tProxyHost.setBounds(110, 30, 110, 20);
        pProxy.add(tProxyPort); tProxyPort.setBounds(370, 30, 50, 20);
        
        pVWS.setLayout(null);
        pVWS.setBorder(new TitledBorder(PRODUCT_NAME +" "+resourceManager.getString("DRSTitleBorder.Server")));
        pVWS.setFocusable(false); 
        add(pVWS);
        pVWS.setBounds(20, 150, 470, 80);
        pVWS.add(lVWSHost); lVWSHost.setBounds(30, 30, 70, 16);
        pVWS.add(lVWSPort); lVWSPort.setBounds(240, 30, 120, 16);
        pVWS.add(tVWSHost); tVWSHost.setBounds(70, 30, 110, 20);
        pVWS.add(tVWSPort); tVWSPort.setBounds(370, 30, 50, 20);
        
        pPdfSettings.setLayout(null);
        pPdfSettings.setBorder(new TitledBorder(PRODUCT_NAME +" "+resourceManager.getString("DRSTitleBorder.PDFSettingsInfo")));
        pPdfSettings.setFocusable(false); 
        add(pPdfSettings);
        pPdfSettings.setBounds(20, 240, 470, 80);
        pPdfSettings.add(burnAnnCb); burnAnnCb.setBounds(30, 30, 15, 15);
        pPdfSettings.add(lBurnAnn); lBurnAnn.setBounds(50, 30, 80, 15);
        pPdfSettings.add(redWhiteoutCb); redWhiteoutCb.setBounds(300, 30, 15, 15);
        pPdfSettings.add(lRedWhiteout); lRedWhiteout.setBounds(320, 30, 80, 15);
        pPdfSettings.add(colorCb); colorCb.setBounds(30, 50, 15, 15);
        pPdfSettings.add(lColor); lColor.setBounds(50, 50, 80, 15);
        pPdfSettings.add(depthComboBox); depthComboBox.setBounds(300, 50, 40, 20);
        pPdfSettings.add(lDepth); lDepth.setBounds(350, 50, 80, 15);
        
        
        pLang.setLayout(null);
        pLang.setBorder(new TitledBorder(resourceManager.getString("DRSTitleBorder.SheduleInfo")));
        pLang.setFocusable(false); 
        add(pLang);
        pLang.setBounds(20, 330, 470, 120);
        pLang.add(ChkOpt1); ChkOpt1.setBounds(30, 30, 160, 16);
        pLang.add(lSnooze); lSnooze.setBounds(190, 30, 45, 16);
        pLang.add(lMinute); lMinute.setBounds(310, 30, 50, 16);
        pLang.add(ChkOpt2); ChkOpt2.setBounds(30, 65, 215, 16);
        ChkOpt1.setText(resourceManager.getString("DRSChkOpt1.StartDWS"));
        ChkOpt2.setText(resourceManager.getString("DRSChkOpt2.DailyFreq"));
        burnAnnCb.setText(resourceManager.getString("DRSLbl.BurnAnnotation"));
        colorCb.setText(resourceManager.getString("DRSLbl.Color"));
        group1.add(ChkOpt1);
        group1.add(ChkOpt2);
        ChkOpt1.setSelected(true);
        ChkOpt1.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent evt) 
            {            	
            	toggleTime(evt.getSource());
            }
        });
        ChkOpt2.addActionListener(new ActionListener() 
                {
                    public void actionPerformed(ActionEvent evt) 
                    {            	
                    	toggleTime(evt.getSource());
                    }
        });

        //lHour = new JLabel("Hr");
        //lMinutes = new JLabel("Min");
        //lSeconds = new JLabel("Sec");
        //pLang.add(lHour); lHour.setBounds(265, 55, 45, 20);
        //pLang.add(lMinutes); lMinutes.setBounds(308, 55, 45, 20);
        //pLang.add(lSeconds); lSeconds.setBounds(353, 55, 45, 20);

        //pLang.add(hrSpin); hrSpin.setBounds(260, 75, 45, 20);
        //pLang.add(minSpin); minSpin.setBounds(305, 75, 45, 20);
        //pLang.add(secSpin); secSpin.setBounds(350, 75, 45, 20);
        
        dateModel.setCalendarField(Calendar.HOUR_OF_DAY);
	    jSpinner = new JSpinner(dateModel);jSpinner.setBounds(265,65, 100, 20);
	    JSpinner.DateEditor editor2 = new JSpinner.DateEditor(jSpinner, "HH:mm:ss a");
	    jSpinner.setEditor(editor2);
	    minuteSpinner = new JSpinner(minuteModel);minuteSpinner.setBounds(265,30,36, 20);

//	  Get the date formatter
	    JFormattedTextField tf = ((JSpinner.DefaultEditor)jSpinner.getEditor()).getTextField();
	    //DefaultFormatterFactory factory = (DefaultFormatterFactory)tf.getFormatterFactory();
	    tf.setEditable(false);
	    //DateFormatter formatter = (DateFormatter)factory.getDefaultFormatter();
	    
	    // Change the date format to only show the hours
	    //formatter.setFormat(new SimpleDateFormat("HH:MM:ss a"));

	    pLang.add(minuteSpinner);
		pLang.add(jSpinner);
        enableTime(false);
        add(bOK); bOK.setBounds(400, 455, 90, 23);
        //add(bCancel); bCancel.setBounds(378, 430, 90, 26);
        add(cbLog); cbLog.setBounds(20, 455, 150, 20);
        bOK.setToolTipText(lNote.getText());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(width, height));
        setLocation((screenSize.width - width)/2,(screenSize.height-height)/2);
        //-------------Listeners------------------------------------------------
/*        addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });*/
        bOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                //closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
    }
     private void toggleTime(Object btn){
    	 if (btn == ChkOpt1){
    		 enableTime(false);
    	 } else if (btn == ChkOpt2){
    		 enableTime(true);
    	 } 
     }
     private void enableTime(boolean flag){
     	jSpinner.setEnabled(flag);
     	minuteSpinner.setEnabled(!flag);
    	 //hrSpin.setEnabled(flag);
    	 //minSpin.setEnabled(flag);
    	 //secSpin.setEnabled(flag);
     }
    private void setSettings()
    {
    	try
    	{
    		/**
        	 * setting of burn Annotation, redwhiteout value, color and depth values are added by
        	 * @author apurba.m
        	 */
    		
    		if(burnAnnCb.isSelected()){
    			DRSPreferences.setBurnAnnotation("1");
    		} else {
    			DRSPreferences.setBurnAnnotation("0");
    		}    	

    		if(redWhiteoutCb.isSelected()){
    			DRSPreferences.setWhiteoutValue("1");
    		} else {
    			DRSPreferences.setWhiteoutValue("0");
    		}

    		if(colorCb.isSelected()){
    			DRSPreferences.setColorValue("1");
    		} else {
    			DRSPreferences.setColorValue("0");
    		}

    		DRSPreferences.setDepthValue(depthComboBox.getSelectedItem().toString());

    		drs.setComPort(Integer.parseInt(tComPort.getText()));
    		drs.setDataPort(Integer.parseInt(tDataPort.getText()));
    		drs.setProxy(cbProxy.getSelectedObjects() != null? true : false);
    		drs.setProxyHost(tProxyHost.getText());
    		String pPort = tProxyPort.getText();
    		if (pPort.equals(""))
    			drs.setProxyPort(0);
    		else
    			drs.setProxyPort(Integer.parseInt(pPort));
    		vwsSchema.address = tVWSHost.getText();
    		vwsSchema.comport = Util.to_Number(tVWSPort.getText());
    		drs.setVWS(vwsSchema);
    		drs.setLogInfo(cbLog.isSelected());
    		if(ChkOpt1.isSelected()){
    			drs.setSchedulerStartInfo(schedStartByAuto);
    			DRSPreferences.setSnoozeTime(minuteModel.getNumber().toString());
    		}
    		if(ChkOpt2.isSelected()){
    			drs.setSchedulerStartInfo(schedStartByFrequency);
    			Date dailyFrequency = dateModel.getDate();
    			String dailyFreq = new SimpleDateFormat("HH:mm:ss").format(dailyFrequency);
    			//String dailyFreq = hr.getValue().toString()+":"+min.getValue().toString()+
    			//					":"+sec.getValue().toString();
    			drs.setDialyFrequency(dailyFreq);
    		}
    		//Vector temp = new Vector();
    		//ars.getRetentionDocs(temp);
    		//Util.Msg(this, "retentionDoc: "+temp.toString(), "Msg");

    	}
    	catch(Exception e) {}
    }
    private void getCurrentSettings()
    {
    	try
    	{
    		/**
        	 * setting of burn Annotation, redwhiteout value, color and depth values are added by
        	 * @author apurba.m
        	 */
    		String burnAnnotationRegistry = DRSPreferences.getBurnAnnotation();
    		String whiteoutValRegistry = DRSPreferences.getWhiteoutValue();
    		String depthValRegistry = DRSPreferences.getDepthValue();
    		String colorValRegistry = DRSPreferences.getColorValue();

    		if(Integer.parseInt(burnAnnotationRegistry) == 1){
    			burnAnnCb.setSelected(true);
    		} else {
    			burnAnnCb.setSelected(false);
    		}
    		
    		if(Integer.parseInt(whiteoutValRegistry) == 1){
    			redWhiteoutCb.setSelected(true);
    		} else {
    			redWhiteoutCb.setSelected(false);
    		}
    		
    		if(Integer.parseInt(colorValRegistry) == 1){
    			colorCb.setSelected(true);
    		} else {
    			colorCb.setSelected(false);
    		}
    		
    		if(Integer.parseInt(depthValRegistry) == 24){
    			depthComboBox.setSelectedItem("24");
    		} else {
    			depthComboBox.setSelectedItem("8");
    		}
    		
    		tComPort.setText(String.valueOf(drs.getComPort()));
    		tDataPort.setText(String.valueOf(drs.getDataPort()));
    		cbProxy.setSelected(drs.getProxy());
    		tProxyHost.setText(drs.getProxyHost());
    		int pPort = drs.getProxyPort();
    		if (pPort > 0)
    			tProxyPort.setText(String.valueOf(pPort));
    		else
    			tProxyPort.setText("");
    		vwsSchema = drs.getVWS();
    		tVWSHost.setText(vwsSchema.address);
    		int vPort = vwsSchema.comport;
    		if (vPort > 0)
    			tVWSPort.setText(String.valueOf(vPort));
    		else
    			tVWSPort.setText("");
    		cbLog.setSelected(drs.getLogInfo());
    		cbProxyStateChanged();
    		if(drs.getSchedulerStartInfo() == 0){
    			ChkOpt1.setSelected(true);
    			Integer intValue = new Integer(DRSPreferences.getSnoozeTime());
    			minuteModel.setValue(intValue);
    			minuteSpinner.setEnabled(true);
    		}
    		if(drs.getSchedulerStartInfo() == 1){
    			ChkOpt2.setSelected(true);
    			jSpinner.setEnabled(true);
    			String dailyFreq = null;
    			dailyFreq = drs.getDialyFrequency();
    			SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    			Date date = timeFormat.parse(dailyFreq);
    			//Date date = new Date(dailyFreq);
    			dateModel.setValue(date);
    			//hrSpin.setEnabled(true);
    			//minSpin.setEnabled(true);
    			//secSpin.setEnabled(true);
    			//String dailyFreq = null;
    			//dailyFreq = ars.getDialyFrequency();
    			//StringTokenizer stDailyFreq = new StringTokenizer(ars.getDialyFrequency(), ":");
    			//if(stDailyFreq != null && stDailyFreq.countTokens() == 3){
    			//hrSpin.setValue(new Integer(stDailyFreq.nextToken()));
    			//minSpin.setValue(new Integer(stDailyFreq.nextToken()));
    			//secSpin.setValue(new Integer(stDailyFreq.nextToken()));
    			//}else{
    			//stDailyFreq = new StringTokenizer(ARSConstants.DEFAULT_DAILYFREQUENCY, ":");
    			//hrSpin.setValue(stDailyFreq.nextToken());
    			//minSpin.setValue(stDailyFreq.nextToken());
    			//secSpin.setValue(stDailyFreq.nextToken());
    			//}
    		}


    	}catch(RemoteException re){

    	}
    	catch(Exception re)
    	{

    	}
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    private void closeDialog() 
    {
        setVisible(false);
        //dispose();
    }
   
    /*public static void main(String a[]){
    	// come later
    	new DRSSettings(null, true, null).show();
    }*/
   
}
