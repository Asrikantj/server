/**
 * 
 */
package com.computhink.manager;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSPreferences;
import com.computhink.vws.server.mail.VWMail;

/**
 * @author Administrator
 *
 */
public class EmailServerSettings extends VWPanel implements ActionListener, Constants{
	JLabel lableCaption=new JLabel("");
	private int TOP = 10;
	private int LEFT = 24;
	private int WIDTH = 160;
	private int HEIGHT = 20;
	private int  iPortWidth = 50;
	private static ResourceManager resourceManager=null;
	int iRegLeft = 400, iRegTop = 455, iRegWidth = 90, iRegHeight = 23;
	
	  public JTextField txtEmailServer,txtEmailServerPort,txtEmailServerUsername, txtFromEmailId, txtVWSEmailId,txtDSSEmailId, txtDRSEmailId, txtARSEmailId, txtVNSEmailId, txtEmailServerPassword;
	  //public JPasswordField txtEmailServerPassword;
	  public JLabel lblEmailServer;
	  public JLabel lblEmailServerPort;
	  public JLabel lblEmailServerUsername;
	  public JLabel lblEmailServerPassword;
	  public JLabel lblFromEmailId;
	  public  JLabel sslcheckLabel;
	  
	  JCheckBox chkSendMailToVWS = new JCheckBox();
	  JCheckBox chkSendMailToDSS=new JCheckBox();
	  JCheckBox chkSendMailToDRS = new JCheckBox();
	  JCheckBox chkSendMailToARS = new JCheckBox();
	  JCheckBox chkSendMailToVNS = new JCheckBox();
	  JCheckBox chkEnableSSL = new JCheckBox();
	  public JLabel lblEmailNotification;
	  public JLabel lblEmailNotificationLine;
	  public JLabel lblVWSEmailId;
	  public JLabel lblDSSEmailId;
	  public JLabel lblDRSEmailId;
	  public JLabel lblARSEmailId;
	  public JLabel lblVNSEmailId;
	  
	  public JButton btnRegister;
	  public JButton btnTestEmail;
	  VWSPreferences vws=new VWSPreferences(); 
	  public EmailServerSettings(VWSPreferences vws){
	      this();
	      this.vws = vws;
	  }
	    public EmailServerSettings(){
		//this.vws = vws;
	    	resourceManager=ResourceManager.getDefaultManager();
	    	lableCaption.setBorder(new TitledBorder(" "+resourceManager.getString("Email.TitlBorder")+" "));
	    	//lableCaption.setText("Email Server Settings");
	    	lableCaption.setBounds(10, 5, 490, 360);//(10, 5, 490, 300);
	        add(lableCaption);
	    	
	        TOP += 20;
	    	lblEmailServer=new JLabel(resourceManager.getString("EmailLbl.Server"));
	    	lblEmailServer.setBounds(LEFT, TOP, WIDTH, HEIGHT);
	    	add(lblEmailServer);
	    	
	        lblEmailServerPort=new JLabel(resourceManager.getString("EmailLbl.Port"));       
	        LEFT += 304; 
	        lblEmailServerPort.setBounds(LEFT, TOP, 150, HEIGHT);
	        add(lblEmailServerPort);

	        
	    	txtEmailServer = new JTextField();
	        LEFT = 24; TOP += 20;
	        txtEmailServer.setBounds(LEFT, TOP, WIDTH, HEIGHT);
	        add(txtEmailServer);
	        
	        
	        
	        txtEmailServerPort = new JTextField();
	        LEFT += 304;
	        txtEmailServerPort.setBounds(LEFT, TOP, iPortWidth, HEIGHT);
	        add(txtEmailServerPort);

	        
	        lblEmailServerUsername=new JLabel(resourceManager.getString("EmailLbl.Username"));
	        LEFT = 24; TOP += 30;
	        lblEmailServerUsername.setBounds(LEFT, TOP, WIDTH, HEIGHT);
	        add(lblEmailServerUsername);
	        
	        lblEmailServerPassword=new JLabel(resourceManager.getString("EmailLbl.Password"));
	        LEFT += 304;
	        lblEmailServerPassword.setBounds(LEFT, TOP, WIDTH, HEIGHT);
	        add(lblEmailServerPassword);
	        
	        txtEmailServerUsername = new JTextField();
	        LEFT = 24; TOP += 20;
	        txtEmailServerUsername.setBounds(LEFT, TOP, WIDTH + 110, HEIGHT);
	        add(txtEmailServerUsername);

	        txtEmailServerPassword = new JPasswordField();
	        LEFT += 304;
	        txtEmailServerPassword.setBounds(LEFT, TOP, WIDTH , HEIGHT);
	        add(txtEmailServerPassword);
	        
	        LEFT = 24; TOP += 30;
	        lblFromEmailId = new JLabel(resourceManager.getString("EmailLbl.FromEmailId"));
	        lblFromEmailId.setBounds(LEFT, TOP, WIDTH, HEIGHT);
	        add(lblFromEmailId);	                        
	        
	        TOP += 20;
	        txtFromEmailId=new JTextField();
	        txtFromEmailId.setBounds(LEFT, TOP, WIDTH + 110, HEIGHT);
	        add(txtFromEmailId);
	        
	        LEFT = 24; TOP += 30;
	        //Enhancement - to send the exception as Emails
	        lblEmailNotification = new JLabel(resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("EmailLbl.Notifications"));
	        lblEmailNotificationLine = new JLabel("");
	        Border border = BorderFactory.createEtchedBorder(Color.white, Color.gray);		
	        lblEmailNotificationLine.setBorder(border);
	        lblEmailNotification.setBounds(LEFT, TOP, 220, HEIGHT);
	        lblEmailNotificationLine.setBounds(LEFT+180, TOP+10, 284, 2);
	        add(lblEmailNotification);
	        add(lblEmailNotificationLine);
	        
	        LEFT = 24; TOP += 30;
	        chkSendMailToVWS.setBounds(LEFT, TOP, 20, HEIGHT);
	        add(chkSendMailToVWS);
	        chkSendMailToVWS.setSelected(false);
	        
	        lblVWSEmailId = new JLabel(resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("EmailLbl.NotifiServer"));
	        lblVWSEmailId.setBounds(LEFT + 20, TOP, WIDTH + 110, HEIGHT);
	        add(lblVWSEmailId);	                        
	        
	        //TOP += 20;
	        txtVWSEmailId=new JTextField();
	        txtVWSEmailId.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 133, HEIGHT);
	        txtVWSEmailId.setToolTipText(resourceManager.getString("EamilToolTip.ARSDelimit"));
	        add(txtVWSEmailId);
	        
	        LEFT = 24; TOP += 30;
	        chkSendMailToDSS.setBounds(LEFT, TOP, 20, HEIGHT);
	        add(chkSendMailToDSS);
	        chkSendMailToDSS.setSelected(false);
	        
	        lblDSSEmailId = new JLabel(resourceManager.getString("EmailLbl.NotifiDSS"));
	        lblDSSEmailId.setBounds(LEFT + 20, TOP, WIDTH + 110, HEIGHT);
	        add(lblDSSEmailId);	    
	        
	        txtDSSEmailId=new JTextField();
	        txtDSSEmailId.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 133, HEIGHT);
	        txtDSSEmailId.setToolTipText(resourceManager.getString("EamilToolTip.ARSDelimit"));
	        add(txtDSSEmailId);

	        LEFT = 24; TOP += 30;
	        chkSendMailToDRS.setBounds(LEFT, TOP, 20, HEIGHT);
	        add(chkSendMailToDRS);
	        chkSendMailToDRS.setSelected(false);
	        
	        lblDRSEmailId = new JLabel(resourceManager.getString("EmailLbl.NotifiDocServer1")+" "+ resourceManager.getString("WORKFLOW_NAME")+" "+resourceManager.getString("EmailLbl.NotifiDocServer2"));
	        lblDRSEmailId.setBounds(LEFT + 20, TOP, WIDTH + 110, HEIGHT);
	        add(lblDRSEmailId);
	        
	        //TOP += 20;
	        txtDRSEmailId=new JTextField();
	        txtDRSEmailId.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 133, HEIGHT);
	        txtDRSEmailId.setToolTipText(resourceManager.getString("EamilToolTip.DRSDelimit"));
	        add(txtDRSEmailId);
	        //
	        LEFT = 24; TOP += 30;
	        chkSendMailToARS.setBounds(LEFT, TOP, 20, HEIGHT);
	        add(chkSendMailToARS);
	        chkSendMailToARS.setSelected(false);
	        
	        chkSendMailToVWS.setBackground(getBackground());
	        chkSendMailToDSS.setBackground(getBackground());
	        chkSendMailToDRS.setBackground(getBackground());
	        chkSendMailToARS.setBackground(getBackground());
	        chkSendMailToVNS.setBackground(getBackground());
	        
	        lblARSEmailId = new JLabel(resourceManager.getString("EmailLbl.NotifiARS"));
	        lblARSEmailId.setBounds(LEFT + 20, TOP, WIDTH + 110, HEIGHT);
	        add(lblARSEmailId);	                        
	        
	        //TOP += 20;
	        txtARSEmailId=new JTextField();
	        txtARSEmailId.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 133, HEIGHT);
	        txtARSEmailId.setToolTipText(resourceManager.getString("EamilToolTip.ARSDelimit"));
	        add(txtARSEmailId);

	         LEFT = 24; TOP += 30;
	        chkSendMailToVNS.setBounds(LEFT, TOP, 20, HEIGHT);
	        add(chkSendMailToVNS);
	        chkSendMailToVNS.setSelected(false);
	        
	        lblVNSEmailId = new JLabel(resourceManager.getString("EmailLbl.NotificationServer"));
	        lblVNSEmailId.setBounds(LEFT + 20, TOP, WIDTH + 110, HEIGHT);
	        add(lblVNSEmailId);	                        
	        
	        //TOP += 20;
	        txtVNSEmailId=new JTextField();
	        txtVNSEmailId.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 133, HEIGHT);
	        txtVNSEmailId.setToolTipText(resourceManager.getString("EamilToolTip.VNSDelimit"));
	        add(txtVNSEmailId);
	        
	        //
	        chkEnableSSL.setBounds(30,iRegTop,20, iRegHeight);
	        add(chkEnableSSL);
	        chkEnableSSL.setSelected(false);
	        chkEnableSSL.setBackground(getBackground());
	        sslcheckLabel = new JLabel(resourceManager.getString("EmailLbl.EnableSSL"));
	        sslcheckLabel.setBounds(50, iRegTop,60, iRegHeight);
	        add(sslcheckLabel);	   
	           
	        
	        
	        btnTestEmail = new JButton(resourceManager.getString("EmailBtn.TestEmail"));
	        btnTestEmail.setBounds(iRegLeft-94, iRegTop, iRegWidth, iRegHeight);
	       // btnTestEmail.addActionListener(this);
	        add(btnTestEmail);
	        
	        btnRegister = new JButton(resourceManager.getString("EmailBtn.Apply"));
	        btnRegister.setBounds(iRegLeft, iRegTop, iRegWidth, iRegHeight);
	        //btnRegister.addActionListener(this);
	        add(btnRegister);
	        btnTestEmail.setBackground(getBackground());
	        btnRegister.setBackground(getBackground());
	        this.setLayout(null);
	   
	      //-------------Listeners------------------------------------------------
	        btnRegister.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent evt){

		            vws.setEmailServerName(txtEmailServer.getText());
		            vws.setEmailServerPort(txtEmailServerPort.getText());
		            vws.setEmailServerUsername(txtEmailServerUsername.getText());
		           // VWSLog.err("this.txtEmailServerPassword.getPassword().toString()111111" + txtEmailServerPassword.getText());
		            vws.setEmailServerPassword(txtEmailServerPassword.getText());
		            
		        	/* * new txtEmailServerPassword.getText(); method is replaced with txtEmailServerPassword.getPassword().toString(); 
		        	 * as Jpasswordfield.getText(); method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001**/
		        	 
		            vws.setFromEmailId(txtFromEmailId.getText());
		            //
		            vws.setVWSEmailId(txtVWSEmailId.getText());
		            vws.setDSSEmailId(txtDSSEmailId.getText());
		            vws.setDRSEmailId(txtDRSEmailId.getText());
		            vws.setARSEmailId(txtARSEmailId.getText());
		            vws.setVNSEmailId(txtVNSEmailId.getText());
		            
		            if(chkSendMailToVWS.isSelected())
		            	vws.setVWSEmailIdSet("true");
		            else
		            	vws.setVWSEmailIdSet("false");
		            if(chkSendMailToDSS.isSelected())
		            	vws.setDSSEmailIdSet("true");
		            else
		            	vws.setDSSEmailIdSet("false");
		            if(chkSendMailToDRS.isSelected())
		            	vws.setDRSEmailIdSet("true");
		            else
		            	vws.setDRSEmailIdSet("false");
		            if(chkSendMailToARS.isSelected())
		            	vws.setARSEmailIdSet("true");
		            else
		            	vws.setARSEmailIdSet("false");
		            
		            if(chkSendMailToVNS.isSelected())
		            	vws.setVNSEmailIdSet("true");
		            else
		            	vws.setVNSEmailIdSet("false");
		            if(chkEnableSSL.isSelected())
		            	vws.setSSLEnable("true");
		            else
		            	vws.setSSLEnable("false");
				}
			});
	        btnTestEmail.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent evt){

	    			//Test Mail...
	    			try{
		    			String message = resourceManager.getString("EamilMsg.TestMsg")+" "+ resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("EamilMsg.Server");
		    			String subject = resourceManager.getString("EamilMsg.TestMail")+" " + resourceManager.getString("CVProduct.Name");
		    			VWMail.sendTestMail(txtEmailServer.getText().trim(), txtEmailServerPort.getText().trim(), txtFromEmailId.getText().trim(), txtFromEmailId.getText().trim(),txtEmailServerUsername.getText().trim(),txtEmailServerPassword.getText().trim(),"EmailServer",subject.trim(), message.trim());
	    			}catch(Exception ex){
	    			}
	    		
				}
			});	        
	        restoreEmailSettings();
	    }
	    
	    public void restoreEmailSettings(){
	    	try{
	        	this.txtEmailServer.setText(vws.getEmailServerName());
	        	this.txtEmailServerPassword.setText(vws.getEmailServerPassword());
	        	this.txtEmailServerPort.setText(vws.getEmailServerPort());
	        	this.txtEmailServerUsername.setText(vws.getEmailServerUsername());
	        	this.txtFromEmailId.setText(vws.getFromEmailId());
	        	//
	        	this.txtVWSEmailId.setText(vws.getVWSEmailId());
	        	this.txtDSSEmailId.setText(vws.getDSSEmailId());
	        	this.txtDRSEmailId.setText(vws.getDRSEmailId());
	        	this.txtARSEmailId.setText(vws.getARSEmailId());
	        	this.txtVNSEmailId.setText(vws.getVNSEmailId());
	        	
	        	if(vws.getVWSEmailIdSet().equalsIgnoreCase("true"))
	        		this.chkSendMailToVWS.setSelected(true);
	        	if(vws.getDSSEmailIdSet().equalsIgnoreCase("true"))
	        		this.chkSendMailToDSS.setSelected(true);
	        	if(vws.getDRSEmailIdSet().equalsIgnoreCase("true"))
	        		this.chkSendMailToDRS.setSelected(true);
	        	if(vws.getARSEmailIdSet().equalsIgnoreCase("true"))
	        		this.chkSendMailToARS.setSelected(true);
	        	if(vws.getVNSEmailIdSet().equalsIgnoreCase("true"))
	        		this.chkSendMailToVNS.setSelected(true);
	        	if(vws.getSSLEnable().equalsIgnoreCase("true"))
	        		this.chkEnableSSL.setSelected(true);
	        	//
	        }catch(Exception e){
	        	
	        }
	    }
	    public void actionPerformed(ActionEvent ae){
	    	try
	         {
	    		if(ae.getSource()==btnRegister){
		            vws.setEmailServerName(this.txtEmailServer.getText());
		            vws.setEmailServerPort(this.txtEmailServerPort.getText());
		            vws.setEmailServerUsername(this.txtEmailServerUsername.getText());
		            vws.setEmailServerPassword(this.txtEmailServerPassword.getText().trim());
		            /*
		        	 * new txtEmailServerPassword.getText(); method is replaced with txtEmailServerPassword.getPassword().toString(); 
		        	 * as Jpasswordfield.getText(); method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
		        	 */
		            vws.setFromEmailId(this.txtFromEmailId.getText());
		            //
		            vws.setVWSEmailId(this.txtVWSEmailId.getText());
		            vws.setDSSEmailId(this.txtDSSEmailId.getText());
		            vws.setDRSEmailId(this.txtDRSEmailId.getText());
		            vws.setARSEmailId(this.txtARSEmailId.getText());
		            vws.setVNSEmailId(this.txtVNSEmailId.getText());
		            
		            if(this.chkSendMailToVWS.isSelected())
		            	vws.setVWSEmailIdSet("true");
		            else
		            	vws.setVWSEmailIdSet("false");
		            if(this.chkSendMailToDSS.isSelected())
		            	vws.setDSSEmailIdSet("true");
		            else
		            	vws.setDSSEmailIdSet("false");
		            if(this.chkSendMailToDRS.isSelected())
		            	vws.setDRSEmailIdSet("true");
		            else
		            	vws.setDRSEmailIdSet("false");
		            if(this.chkSendMailToARS.isSelected())
		            	vws.setARSEmailIdSet("true");
		            else
		            	vws.setARSEmailIdSet("false");
		            
		            if(this.chkSendMailToVNS.isSelected())
		            	vws.setVNSEmailIdSet("true");
		            else
		            	vws.setVNSEmailIdSet("false");
		            if(this.chkEnableSSL.isSelected())
		            	vws.setSSLEnable("true");
		            else
		            	vws.setSSLEnable("false");
		            //
	    		}
	    		else if(ae.getSource() == btnTestEmail){
	    			//Test Mail...
	    			try{
		    			String message = resourceManager.getString("EamilMsg.TestMsg")+" "+ resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("EamilMsg.Server");
		    			String subject = resourceManager.getString("EamilMsg.TestMail")+" " + resourceManager.getString("CVProduct.Name");
		    			VWMail.sendTestMail(this.txtEmailServer.getText().trim(), this.txtEmailServerPort.getText().trim(), this.txtFromEmailId.getText().trim(), this.txtFromEmailId.getText().trim(),this.txtEmailServerUsername.getText().trim(),this.txtEmailServerPassword.getText().trim(),"EmailServer",subject.trim(), message.trim());
	    			}catch(Exception ex){
	    			}
	    		}
	         }
	         catch(Exception e){
	         }
	    
	    }
	    /**
	     * @param args
	     */
/*
	    public static void main(String[] args) {
	    	JFrame f=new JFrame();
	    	f.add(new EmailServerSettings(vws));
	    	f.setSize(600,600);
	    	f.setVisible(true);

	    }
*/
}
