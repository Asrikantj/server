/*
 * AboutManager.java
 *
 * Created on November 13, 2003, 10:58 AM
 */

package com.computhink.manager;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;

import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.vwc.resource.ResourceManager;
import com.computhink.manager.image.Images;

public class AboutManager extends JDialog implements Constants// ActionListener 
{
    private AboutManagerPanel panel = new AboutManagerPanel();
    private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
    int isWebClient = 2;//1 for web lite and 2 for Web top
    String webTitle = "";
    public static ImageIcon webClientAboutIcon = null;
    
    public AboutManager(Window parent, boolean modal, int mode, ImageIcon aboutIcon) 
    {
        super(parent, "About " + PRODUCT_NAME);
        setModal(true);
        new Images();
        webClientAboutIcon = aboutIcon;
        panel = new AboutManagerPanel(mode);
        isWebClient = mode;        
        if (mode == 0){
            webTitle = "Web Top";
        }else if (mode == 1 ){
            webTitle = "Web Lite";
        }
        setTitle("About " + PRODUCT_NAME + " Online - " + webTitle);
        initComponents();
    }
    public AboutManager(JFrame parent, boolean modal) 
    {
        super(parent, modal);
        new Images();
        setTitle("About Server Manager");
        initComponents();
    }
    private void initComponents() 
    {
	getContentPane().setLayout(new BorderLayout());
	getContentPane().add(panel, BorderLayout.CENTER);
	setIconImage(Images.app.getImage());
	
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new java.awt.Dimension(600, 360));
        setResizable(false);
        setLocation((screenSize.width-600)/2,(screenSize.height-360)/2);

        
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent evt){
                closeDialog();
            }
        });
        

        //-------------------------Listeners------------------------------------
    }
    private void closeDialog() 
    {
        setVisible(false);
        dispose();
    }
    public static void main(String args[]) {
    	new AboutManager(null, true).setVisible(true);
    }
}
