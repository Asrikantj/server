/*
  *
 * Created on Decembaer 21, 2003, 4:24 PM
 */
package com.computhink.manager;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.computhink.ars.server.ARS;
import com.computhink.ars.server.ARSConstants;
import com.computhink.common.CIServer;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.vws.server.VWS;
import com.computhink.resource.ResourceManager;

public class ARSSettingsPanel extends VWPanel implements ManagerConstants, ARSConstants
{
    private JButton bOK, bCancel;
    private JPanel pPorts, pProxy, pVWS, pLang;
    private JLabel lDataPort, lProxyPort, lProxyHost, lComPort, 
                              lVWSPort, lVWSHost,lNote, lHour, lMinutes, lSeconds, lSnooze, lMinute;
    private JTextField tProxyPort, tComPort, tDataPort, tProxyHost, 
                                                        tVWSHost, tVWSPort;
    private JCheckBox cbProxy, cbLog;
    private static ResourceManager resourceManager=null;
    JRadioButton ChkOpt1 = new JRadioButton();
    JRadioButton ChkOpt2 = new JRadioButton();
    ButtonGroup group1 = new ButtonGroup(); 
    //SpinnerNumberModel hr =  new SpinnerNumberModel(0, 0, 23, 1);
    //SpinnerNumberModel min =  new SpinnerNumberModel(0, 0, 59, 1);
    //SpinnerNumberModel sec =  new SpinnerNumberModel(0, 0, 59, 1);
    //JSpinner hrSpin = new JSpinner(hr);
    //JSpinner minSpin = new JSpinner(min);
    //JSpinner secSpin = new JSpinner(sec);
    JSpinner jSpinner;
    JSpinner minuteSpinner;
    SpinnerDateModel dateModel = new SpinnerDateModel();
    SpinnerNumberModel minuteModel = new SpinnerNumberModel(5, 1, 59, 1);
    
    private static final int width = 500;
    private static final int height = 510;
    private static final int schedStartByAuto = 0;
    private static final int schedStartByFrequency= 1;
        
    private ARS ars;
    private VWS vws;
    private ServerSchema vwsSchema;
    private Vector vStores = new Vector();
    private String selectedRoom = "";
    private String myAddress = "";
    

    public ARSSettingsPanel(){
	super();
	initComponents();
	setBackground(new Color(238, 242, 244));
    }
    public ARSSettingsPanel(CIServer server) 
    {
        super();
        this.ars = (ARS) server;
        initComponents();
        getCurrentSettings();
    }
    /*
    public ARSSettings(JFrame parent, boolean modal) 
    {
        super(parent, modal);
        initComponents();
        getCurrentSettings();
    }
    */
     private void initComponents() 
     {
    	//setTitle("Auto Retention Server Settings "); 
    	//setTitle(SETTINGS);
    	 resourceManager=ResourceManager.getDefaultManager();
        pPorts = new VWPanel();
        pVWS = new VWPanel();
        pProxy = new VWPanel();
        pLang = new VWPanel();
        cbProxy = new JCheckBox();
        cbLog = new JCheckBox(resourceManager.getString("ARSChK.LogInfo"));
        bOK = new JButton(resourceManager.getString("ARSBtn.Apply"));
        bCancel = new JButton(resourceManager.getString("ARSBtn.Cancel"));

        cbProxy.setBackground(this.getBackground());
        cbLog.setBackground(this.getBackground());
        bOK.setBackground(this.getBackground());
        bCancel.setBackground(this.getBackground());
        ChkOpt1.setBackground(this.getBackground());
        ChkOpt2.setBackground(this.getBackground());
        
        tComPort = new JTextField();
        tDataPort = new JTextField();
        tProxyHost = new JTextField();
        tProxyPort = new JTextField();
        tVWSHost = new JTextField();
        tVWSPort = new JTextField();

        lProxyHost = new JLabel(resourceManager.getString("ARSLbl.ProxyHost"));
        lProxyPort = new JLabel(resourceManager.getString("ARSLbl.Port"));
        lVWSHost = new JLabel(resourceManager.getString("ARSLbl.VWSHost"));
        lVWSPort = new JLabel(resourceManager.getString("ARSLbl.VWSComPort"));
        lComPort = new JLabel(resourceManager.getString("ARSLbl.ComPort"));
        lDataPort = new JLabel(resourceManager.getString("ARSLbl.Dataport"));
        lNote = new JLabel(resourceManager.getString("ARSLbl.Note"));
        lSnooze = new JLabel (resourceManager.getString("ARSLbl.Snooze"));
        lMinute = new JLabel (resourceManager.getString("ARSLbl.minutes"));
        setLayout(null);
        //setResizable(false);
        
        pPorts.setLayout(null);
        pPorts.setBorder(new TitledBorder(resourceManager.getString("ARSTitleBorder.ServPorts")));
        pPorts.setFocusable(false); add(pPorts);
        pPorts.setBounds(20, 20, 470, 80);
        pPorts.add(lComPort); lComPort.setBounds(30, 30, 120, 16);
        pPorts.add(lDataPort); lDataPort.setBounds(300, 30, 60, 16);
        pPorts.add(tComPort); tComPort.setBounds(160, 30, 50, 20);
        pPorts.add(tDataPort); tDataPort.setBounds(370, 30, 50, 20);
        
        pProxy.setLayout(null);
        pProxy.setBorder(new TitledBorder("         "+resourceManager.getString("ARSTitleBorder.HttpProxy")));
        pProxy.setFocusable(false);  
        add(pProxy);
        pProxy.setBounds(20, 110, 470, 80);
        pProxy.add(lProxyHost); lProxyHost.setBounds(30, 30, 70, 16);
        pProxy.add(lProxyPort); lProxyPort.setBounds(325, 30, 30, 16);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 21);
        pProxy.add(tProxyHost); tProxyHost.setBounds(110, 30, 110, 20);
        pProxy.add(tProxyPort); tProxyPort.setBounds(370, 30, 50, 20);
        
        pVWS.setLayout(null);
        pVWS.setBorder(new TitledBorder(PRODUCT_NAME +" "+resourceManager.getString("ARSTitleBorder.Server")));
        pVWS.setFocusable(false); 
        add(pVWS);
        pVWS.setBounds(20, 200, 470, 80);
        pVWS.add(lVWSHost); lVWSHost.setBounds(30, 30, 70, 16);
        pVWS.add(lVWSPort); lVWSPort.setBounds(240, 30, 120, 16);
        pVWS.add(tVWSHost); tVWSHost.setBounds(70, 30, 110, 20);
        pVWS.add(tVWSPort); tVWSPort.setBounds(370, 30, 50, 20);
        
        pLang.setLayout(null);
        pLang.setBorder(new TitledBorder(resourceManager.getString("ARSTitleBorder.SheduleInfo")));
        pLang.setFocusable(false); 
        add(pLang);
        pLang.setBounds(20, 290, 470, 140);
        pLang.add(ChkOpt1); ChkOpt1.setBounds(30, 30, 260, 16);
//    	Modified , due to change in GUI, (implementation of Spinner in Snooze time), Mallikarjuna,       
        pLang.add(lSnooze); lSnooze.setBounds(290, 30, 45, 16);
        pLang.add(lMinute); lMinute.setBounds(388, 30, 50, 16);
        pLang.add(ChkOpt2); ChkOpt2.setBounds(30, 65, 215, 16);
        ChkOpt1.setText(resourceManager.getString("ARSChkOpt1.StartARS"));
        ChkOpt2.setText(resourceManager.getString("ARSChkOpt2.DailyFreq"));
        group1.add(ChkOpt1);
        group1.add(ChkOpt2);
        ChkOpt1.setSelected(true);
        ChkOpt1.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent evt) 
            {            	
            	toggleTime(evt.getSource());
            }
        });
        ChkOpt2.addActionListener(new ActionListener() 
                {
                    public void actionPerformed(ActionEvent evt) 
                    {            	
                    	toggleTime(evt.getSource());
                    }
        });

        //lHour = new JLabel("Hr");
        //lMinutes = new JLabel("Min");
        //lSeconds = new JLabel("Sec");
        //pLang.add(lHour); lHour.setBounds(265, 55, 45, 20);
        //pLang.add(lMinutes); lMinutes.setBounds(308, 55, 45, 20);
        //pLang.add(lSeconds); lSeconds.setBounds(353, 55, 45, 20);

        //pLang.add(hrSpin); hrSpin.setBounds(260, 75, 45, 20);
        //pLang.add(minSpin); minSpin.setBounds(305, 75, 45, 20);
        //pLang.add(secSpin); secSpin.setBounds(350, 75, 45, 20);
        
        dateModel.setCalendarField(Calendar.HOUR_OF_DAY);
	    jSpinner = new JSpinner(dateModel);jSpinner.setBounds(279,65, 100, 20);
	    JSpinner.DateEditor editor2 = new JSpinner.DateEditor(jSpinner, "HH:mm:ss a");
	    jSpinner.setEditor(editor2);
//    	Modified , due to change in GUI, (implementation of Spinner in Snooze time), Mallikarjuna, 	    
	    minuteSpinner = new JSpinner(minuteModel);minuteSpinner.setBounds(344,30,36, 20);
	    jSpinner.setBackground(Color.white);
	    editor2.setBackground(Color.white);
	    minuteSpinner.setBackground(Color.white);
//	  Get the date formatter
	    JFormattedTextField tf = ((JSpinner.DefaultEditor)jSpinner.getEditor()).getTextField();
	    //DefaultFormatterFactory factory = (DefaultFormatterFactory)tf.getFormatterFactory();
	    tf.setEditable(false);
	    //DateFormatter formatter = (DateFormatter)factory.getDefaultFormatter();
	    
	    // Change the date format to only show the hours
	    //formatter.setFormat(new SimpleDateFormat("HH:MM:ss a"));

	    pLang.add(minuteSpinner);
		pLang.add(jSpinner);
        enableTime(false);
        add(bOK); bOK.setBounds(400, 455, 90, 23);
        //add(bCancel); bCancel.setBounds(378, 430, 90, 26);
        add(cbLog); cbLog.setBounds(20, 455, 150, 20);
        bOK.setToolTipText(lNote.getText());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(width, height));
        setLocation((screenSize.width - width)/2,(screenSize.height-height)/2);
        //-------------Listeners------------------------------------------------
/*        addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });*/
        bOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                //closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
    }
     private void toggleTime(Object btn){
    	 if (btn == ChkOpt1){
    		 enableTime(false);
    	 } else if (btn == ChkOpt2){
    		 enableTime(true);
    	 } 
     }
     private void enableTime(boolean flag){
     	jSpinner.setEnabled(flag);
     	minuteSpinner.setEnabled(!flag);
    	 //hrSpin.setEnabled(flag);
    	 //minSpin.setEnabled(flag);
    	 //secSpin.setEnabled(flag);
     }
    private void setSettings()
    {
        try
        {
            ars.setComPort(Integer.parseInt(tComPort.getText()));
            ars.setDataPort(Integer.parseInt(tDataPort.getText()));
            ars.setProxy(cbProxy.getSelectedObjects() != null? true : false);
            ars.setProxyHost(tProxyHost.getText());
            String pPort = tProxyPort.getText();
            if (pPort.equals(""))
                ars.setProxyPort(0);
            else
                ars.setProxyPort(Integer.parseInt(pPort));
            vwsSchema.address = tVWSHost.getText();
            vwsSchema.comport = Util.to_Number(tVWSPort.getText());
            ars.setVWS(vwsSchema);
            ars.setLogInfo(cbLog.isSelected());
            if(ChkOpt1.isSelected()){
            	ars.setSchedulerStartInfo(schedStartByAuto);
//            	Modified , due to change in GUI, (implementation of Spinner in Snooze time), Mallikarjuna,            	
            	ars.setSnoozeTime(minuteModel.getNumber().toString());
            }
            if(ChkOpt2.isSelected()){
            	ars.setSchedulerStartInfo(schedStartByFrequency);
            	Date dailyFrequency = dateModel.getDate();
            	String dailyFreq = new SimpleDateFormat("HH:mm:ss").format(dailyFrequency);
            	//String dailyFreq = hr.getValue().toString()+":"+min.getValue().toString()+
				//					":"+sec.getValue().toString();
            	ars.setDialyFrequency(dailyFreq);
            }
            //Vector temp = new Vector();
            //ars.getRetentionDocs(temp);
            //Util.Msg(this, "retentionDoc: "+temp.toString(), "Msg");
            
        }
        catch(Exception e) {}
    }
    private void getCurrentSettings()
    {
        try
        {
            tComPort.setText(String.valueOf(ars.getComPort()));
            tDataPort.setText(String.valueOf(ars.getDataPort()));
            cbProxy.setSelected(ars.getProxy());
            tProxyHost.setText(ars.getProxyHost());
            int pPort = ars.getProxyPort();
            if (pPort > 0)
                tProxyPort.setText(String.valueOf(pPort));
            else
                tProxyPort.setText("");
            vwsSchema = ars.getVWS();
            tVWSHost.setText(vwsSchema.address);
            int vPort = vwsSchema.comport;
            if (vPort > 0)
                tVWSPort.setText(String.valueOf(vPort));
            else
                tVWSPort.setText("");
            cbLog.setSelected(ars.getLogInfo());
            cbProxyStateChanged();        
            if(ars.getSchedulerStartInfo() == 0){            	
            	ChkOpt1.setSelected(true);
//     	Modified , due to change in GUI, (implementation of Spinner in Snooze time), Mallikarjuna,	
            	Integer intValue = new Integer(ars.getSnoozeTime());
            	minuteModel.setValue(intValue);
            	minuteSpinner.setEnabled(true);
            }
//        	Modified , due to change in GUI, (implementation of Spinner in Snooze time), Mallikarjuna,            
            if(ars.getSchedulerStartInfo() == 1){
            	ChkOpt2.setSelected(true);
            	jSpinner.setEnabled(true);
            	String dailyFreq = null;
            	dailyFreq = ars.getDialyFrequency();
            	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            	Date date = timeFormat.parse(dailyFreq);
            	//Date date = new Date(dailyFreq);
            	dateModel.setValue(date);
            	minuteSpinner.setEnabled(false);
            	//hrSpin.setEnabled(true);
    	    	//minSpin.setEnabled(true);
    	    	//secSpin.setEnabled(true);
            	  	//String dailyFreq = null;
	            	//dailyFreq = ars.getDialyFrequency();
		            //StringTokenizer stDailyFreq = new StringTokenizer(ars.getDialyFrequency(), ":");
		            //if(stDailyFreq != null && stDailyFreq.countTokens() == 3){
		            	//hrSpin.setValue(new Integer(stDailyFreq.nextToken()));
						//minSpin.setValue(new Integer(stDailyFreq.nextToken()));
						//secSpin.setValue(new Integer(stDailyFreq.nextToken()));
		            //}else{
		            	//stDailyFreq = new StringTokenizer(ARSConstants.DEFAULT_DAILYFREQUENCY, ":");
		            	//hrSpin.setValue(stDailyFreq.nextToken());
						//minSpin.setValue(stDailyFreq.nextToken());
						//secSpin.setValue(stDailyFreq.nextToken());
		            //}
	            }


        }catch(RemoteException re){
        	
        }
        catch(Exception re)
        {
        	
        }
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    private void closeDialog() 
    {
        setVisible(false);
        //dispose();
    }
   
   /* public static void main(String a[]){
    	// come later
    	new ARSSettings(null, true, null).show();
    }*/
   
}
