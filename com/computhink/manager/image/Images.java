/*
 * Images.java
 *
 * Created on Sep 21, 2003, 12:14 AM
 */
package com.computhink.manager.image;

import java.awt.Image;
import java.io.File;
import java.net.URL;
import java.util.prefs.Preferences;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import com.computhink.common.Constants;
import com.computhink.vwc.VWClient;
/**
 *
 * @author  Saad
 * @version 
 */

public class Images
{
	public static ImageIcon app;
	public static ImageIcon run;
	public static ImageIcon stop;
	public static ImageIcon look;
	public static ImageIcon ron;
	public static ImageIcon rof;
	public static ImageIcon logo;
	public static ImageIcon bro;
	public static ImageIcon server;
	public static ImageIcon vws;
	public static ImageIcon dss;
	public static ImageIcon ixr;
	public static ImageIcon drs;
	public static ImageIcon ars;
	public static ImageIcon email;
	public static ImageIcon security;
	public static ImageIcon license;
	public static ImageIcon rooms;
	public static ImageIcon dvws;
	public static ImageIcon ddss;
	public static ImageIcon dixr;
	public static ImageIcon ddrs;
	public static ImageIcon dars;
	public static ImageIcon demail;
	public static ImageIcon dsecurity;
	public static ImageIcon dlicense;
	public static ImageIcon drooms;
	public static ImageIcon online;
	public static ImageIcon offline;
	public static ImageIcon startService;
	public static ImageIcon stopService;
	public static ImageIcon dvns;
	public static ImageIcon dcss;
	public static ImageIcon about;
	public static ImageIcon info;
	public static ImageIcon clearLog;
	public static ImageIcon saveLog;
	public static ImageIcon regServer;
	public static ImageIcon proxyServer;
	public static ImageIcon mgrSetup;
	public static ImageIcon settings;
	public static ImageIcon close;
	public static ImageIcon login;
	public static ImageIcon vns;
	public static ImageIcon css;
	public static ImageIcon hotfolder;

	public static ImageIcon MGRApp;
	public static ImageIcon DRSApp;
	public static ImageIcon ARSApp;
	public static ImageIcon IXRApp;
	public static ImageIcon DSSApp;
	public static ImageIcon VWSApp;    
	public static ImageIcon VNSApp;
	public static ImageIcon CSSApp;

	public Images()
	{
		try
		{
			online = new ImageIcon(getClass().getResource("images/Online.png"));
			offline = new ImageIcon(getClass().getResource("images/Offline.png"));

			app = new ImageIcon(getClass().getResource("images/VWLogo.png"));
			MGRApp = new ImageIcon(getClass().getResource("images/ServerManager.png"));
			DRSApp = new ImageIcon(getClass().getResource("images/DRSSettings.png"));
			ARSApp = new ImageIcon(getClass().getResource("images/ARSSettings.png"));
			IXRApp = new ImageIcon(getClass().getResource("images/IndexerSettings.png"));
			DSSApp = new ImageIcon(getClass().getResource("images/DSSSettings.png"));
			VWSApp = new ImageIcon(getClass().getResource("images/ServerConfig.png"));
			VNSApp = new ImageIcon(getClass().getResource("images/NotificationSettings.png"));
			CSSApp= new ImageIcon(getClass().getResource("images/conentsentinel.png"));

			run = new ImageIcon(getClass().getResource("images/run.png"));
			stop = new ImageIcon(getClass().getResource("images/Stopsignal.png"));
			look = new ImageIcon(getClass().getResource("images/look.png"));
			rof = new ImageIcon(getClass().getResource("images/rof.gif"));
			ron = new ImageIcon(getClass().getResource("images/ron.gif"));
			logo = getAboutImage();
			bro = new ImageIcon(getClass().getResource("images/bro.gif"));
			server = new ImageIcon(getClass().getResource("images/Root.png"));
			vws = new ImageIcon(getClass().getResource("images/Server.png"));
			ixr = new ImageIcon(getClass().getResource("images/Indexer.png"));
			dss = new ImageIcon(getClass().getResource("images/DocStorage.png"));
			drs = new ImageIcon(getClass().getResource("images/Routing.png"));
			ars = new ImageIcon(getClass().getResource("images/Retention.png"));
			email = new ImageIcon(getClass().getResource("images/Email.png"));
			security = new ImageIcon(getClass().getResource("images/Connection.png"));
			license = new ImageIcon(getClass().getResource("images/License.png"));
			rooms = new ImageIcon(getClass().getResource("images/Rooms.png"));
			login = new ImageIcon(getClass().getResource("images/Login.png"));
			hotfolder =new ImageIcon(getClass().getResource("images/HotFolder.png"));
			vns = new ImageIcon(getClass().getResource("images/Notification.png"));
			css = new ImageIcon(getClass().getResource("images/conentsentinel.png"));

			dvws = new ImageIcon(getClass().getResource("images/DServer.png"));
			dixr = new ImageIcon(getClass().getResource("images/DIndexer.png"));
			ddss = new ImageIcon(getClass().getResource("images/DDocStorage.png"));
			ddrs = new ImageIcon(getClass().getResource("images/DRouting.png"));
			dars = new ImageIcon(getClass().getResource("images/DRetention.png"));
			demail = new ImageIcon(getClass().getResource("images/DEmail.png"));
			dsecurity = new ImageIcon(getClass().getResource("images/DConnection.png"));
			dlicense = new ImageIcon(getClass().getResource("images/DLicense.png"));
			drooms = new ImageIcon(getClass().getResource("images/DRooms.png"));
			dvns = new ImageIcon(getClass().getResource("images/DNotification.png"));
			dcss=new ImageIcon(getClass().getResource("images/DContentSentinel.png"));

			startService = new ImageIcon(getClass().getResource("images/Start.png"));
			stopService = new ImageIcon(getClass().getResource("images/Stop.png"));
			about = new ImageIcon(getClass().getResource("images/About.png"));
			info=new ImageIcon(getClass().getResource("images/Info.png"));
			clearLog = new ImageIcon(getClass().getResource("images/ClearLog.png"));
			saveLog = new ImageIcon(getClass().getResource("images/SaveLog.png"));
			regServer = new ImageIcon(getClass().getResource("images/RegServer.png"));
			proxyServer = new ImageIcon(getClass().getResource("images/ProxyServer.png"));
			mgrSetup = new ImageIcon(getClass().getResource("images/MgrSetup.png"));
			settings = new ImageIcon(getClass().getResource("images/Settings.png"));
			close = new ImageIcon(getClass().getResource("images/Close.png"));



		}
		catch (Exception e)
		{
		}
	}
	
	public Images(int mode) {
		app = new ImageIcon(getClass().getResource("images/VWLogo.png"));
	}
	
	private ImageIcon getAboutImage(){
		ImageIcon aboutImage = null;
		try{
			Preferences imagePrefs = Preferences.systemRoot().node("/computhink/" + Constants.PRODUCT_NAME.toLowerCase() +"/imagepath");
			String imagePath = imagePrefs.get("srvmanagerabout", "");

			if(imagePath != null && imagePath.trim().length() > 0){
				try{
					if(imagePath.startsWith("http:")){
						if(imagePath.contains("http:\\")){
							imagePath = imagePath.replace("http:\\", "http://");
						}
						URL url = new URL(imagePath);
						Image image = ImageIO.read(url);
						aboutImage = new ImageIcon(image);
					}else{
						File file = new File(imagePath);
						Image image = ImageIO.read(file);
						aboutImage = new ImageIcon(image);
					}
				}catch (Exception e) {
					aboutImage = new ImageIcon(getClass().getResource("images/logo.png"));
				}
			}else{
				aboutImage = new ImageIcon(getClass().getResource("images/logo.png"));
			}
		}catch (Exception e) {
			aboutImage = new ImageIcon(getClass().getResource("images/logo.png"));
		}
		return aboutImage;
	}
}