package com.computhink.manager;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.computhink.common.CIServer;
import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.manager.image.Images;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;
/**
 * CV10.1 Enhancement Added for HotFolder Location UI 
 * @author apurba.m
 *
 */

public class VWFolderLocationPanel extends VWPanel implements Constants {
	private static ResourceManager resourceManager = null;
	/****CV2019 merges from SIDBI line (Hot Folder Location tab changes)--------------------------------***/
	JLabel lableCaptionFolder = new JLabel("");
	JLabel lablePatchMsi = new JLabel("");
	private VWPanel pLicense1;
	JLabel lLicense1, lMSIVersion, lMSILocation, lMSIUsername, lMSIPassword, lMSIDomain;
	private JTextField tLicense1, tMSIVersion, tMSILocation, tUserName, tPassword, tDomain; 
	private JButton bBrowse1, bRegisterFolder;
	private VWS vws1;
	VWSPreferences prefs = new VWSPreferences();
	private int TOP = 10;
	private int LEFT = 24;
	private int WIDTH = 160;
	private int HEIGHT = 20;
	int iRegLeft = 400, iRegTop = 455, iRegWidth = 90, iRegHeight = 23;
	/*------------------End of SIDBI merges----------------------------*/
	public VWFolderLocationPanel(CIServer server) {
		super();
		vws1 = (VWS) server;
		initComponents();
		addDocListeners();
	}

	public VWFolderLocationPanel() {
		// TODO Auto-generated constructor stub
		super();
		vws1 = null;
		initComponents();
		addDocListeners();
	}

	private void initComponents() {
		/****CV2019 Merges - SIDBI Hot Folder Location enhancement changes***/
		resourceManager = ResourceManager.getDefaultManager();
		setLayout(null);
		pLicense1 = new VWPanel();
		pLicense1.setLayout(null);
		lableCaptionFolder.setBorder(new TitledBorder("\t" + resourceManager.getString("FolderLocation.MainTitle") + " "));
		lableCaptionFolder.setBounds(10, 5, 490, 100);// (10, 5, 490, 300);
		add(lableCaptionFolder);
		
		bBrowse1 = new JButton();
		bRegisterFolder = new JButton(resourceManager.getString("LicenseBtn.Apply"));
		
		TOP += 20;
		lLicense1 = new JLabel(resourceManager.getString("FolderLocation.Folder"));				
		lLicense1.setBounds(LEFT, TOP, WIDTH, HEIGHT);
		pLicense1.add(lLicense1);
		
		TOP += 20;
		tLicense1 = new JTextField();
		tLicense1.setBounds(LEFT, TOP, WIDTH+270, HEIGHT);
		pLicense1.add(tLicense1);
		
		bBrowse1.setBounds(WIDTH+LEFT+270, TOP, 20, HEIGHT);
		pLicense1.add(bBrowse1);
		bBrowse1.setIcon(Images.bro);
		
		TOP = 120;
		lablePatchMsi.setBorder(new TitledBorder("\t" + resourceManager.getString("FolderLocation.SubTitle") + " "));
		lablePatchMsi.setBounds(10, TOP, 490, 200);//
		add(lablePatchMsi);
		
		TOP += 30; WIDTH = 80; 
		lMSIVersion = new JLabel(resourceManager.getString("FolderLocation.MSIVersion"));
		lMSIVersion.setBounds(LEFT, TOP, WIDTH, HEIGHT);
		pLicense1.add(lMSIVersion);		
		tMSIVersion = new JTextField();
		tMSIVersion.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 280, HEIGHT);
		pLicense1.add(tMSIVersion);
		
		TOP += 30;
		lMSILocation = new JLabel(resourceManager.getString("FolderLocation.MSILocation"));
		lMSILocation.setBounds(LEFT, TOP, WIDTH, HEIGHT);
		pLicense1.add(lMSILocation);
		tMSILocation = new JTextField();
		tMSILocation.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 280, HEIGHT);
		pLicense1.add(tMSILocation);
		
		TOP += 30;
		lMSIUsername = new JLabel(resourceManager.getString("FolderLocation.MSIUsername"));
		lMSIUsername.setBounds(LEFT, TOP, WIDTH, HEIGHT);
		pLicense1.add(lMSIUsername);
		tUserName = new JTextField();
		tUserName.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 280, HEIGHT);
		pLicense1.add(tUserName);
		
		TOP += 30;
		lMSIPassword = new JLabel(resourceManager.getString("FolderLocation.MSIPassword"));
		lMSIPassword.setBounds(LEFT, TOP, WIDTH, HEIGHT);
		pLicense1.add(lMSIPassword);
		tPassword = new JPasswordField();
		tPassword.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 280, HEIGHT);
		pLicense1.add(tPassword);
		
		TOP += 30;
		lMSIDomain = new JLabel(resourceManager.getString("FolderLocation.MSIDomain"));
		lMSIDomain.setBounds(LEFT, TOP, WIDTH, HEIGHT);
		pLicense1.add(lMSIDomain);
		tDomain = new JTextField();
		tDomain.setBounds(LEFT + WIDTH + 11, TOP, WIDTH + 280, HEIGHT);
		pLicense1.add(tDomain);
		
		
		bBrowse1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				browseLicenseFile();
			}
		});
		bRegisterFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				registerHotFolderSettings();
				bRegisterFolder.setEnabled(false);
			}
		});

		bRegisterFolder.setBounds(iRegLeft, iRegTop, iRegWidth, iRegHeight);
		pLicense1.add(bRegisterFolder);
		add(pLicense1);
		bRegisterFolder.setBackground(this.getBackground());
		pLicense1.setBounds(0, 0, 500, 700);
		bRegisterFolder.setEnabled(false);
		
		try {
			/*if(tLicense1.getText().length() ==0 && vws1.gethotFolderPath().length() >0){
				tLicense1.setText(vws1.gethotFolderPath());
			}else 
				*/
			if(tLicense1.getText().length()==0&&prefs.getHotFolderPath().length()>0){
				tLicense1.setText(prefs.getHotFolderPath());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			VWSLog.dbg("Exception while getting hotfolder path :"+e.getMessage());
		}
		loadMsiInfo();
		/*------------------End of SIDBI merges----------------------------*/
	}

	class SymMouse extends MouseAdapter {
		public void mouseClicked(MouseEvent event) {
			Object object = event.getSource();
			if (object instanceof JEditorPane)
				if (event.getModifiers() == event.BUTTON3_MASK)
					VWEditorPanelLicense_RightMouseClicked(event);
		}
	}

	public void VWEditorPanelLicense_RightMouseClicked(MouseEvent event) {
		System.out.println("VWEditorPanelLicense_RightMouseClicked...");
		if (event.getClickCount() == 1)
			rSingleClick(event);
		else if (event.getClickCount() == 2)
			rDoubleClick(event);
	}

	private void rSingleClick(MouseEvent event) {
		// TODO Auto-generated method stub

	}

	private void rDoubleClick(MouseEvent event) {
		System.out.println("rDoubleClick...");
	}

	private void registerHotFolderSettings() {
		/****CV2019 Merges - SIDBI Hot Folder Location enhancement changes***/
		try {
			String password = tPassword.getText();
			if (password != null && password.trim().length() > 0) {					
				password = Util.encryptKey_MSI(password);					
			}
			if (vws1 == null) {				
				VWSPreferences.setHotFolderPath(tLicense1.getText());
				VWSPreferences.setMsiVersion(tMSIVersion.getText());
				VWSPreferences.setMsiURL(tMSILocation.getText());
				VWSPreferences.setUserName(tUserName.getText());				
				VWSPreferences.setUserPassword(password);
				VWSPreferences.setDomainName(tDomain.getText());
			} else {
				vws1.setHotFolderPath(tLicense1.getText());
				vws1.setMsiServerSettingsInfo(tMSIVersion.getText(), tMSILocation.getText(), tUserName.getText(),
						password, tDomain.getText());
			}
		} catch (Exception e) {
			VWSLog.add("Exception in registerHotFolderSettings :"+e);
		}
		/*------------------End of SIDBI merges----------------------------*/
	}

	private void browseLicenseFile() {
		String dFolder = "C:\\";
		JFileChooser fc = new JFileChooser(new File(dFolder));
		fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fc.showOpenDialog(this);
		File lic = fc.getSelectedFile();
		if (lic != null)
			tLicense1.setText(lic.getPath());
	}

	private void addDocListeners() {
		tLicense1.getDocument().addDocumentListener(new DocListener(bRegisterFolder));
		/****CV2019 Merges - SIDBI Hot Folder Location enhancement changes***/
		tMSIVersion.getDocument().addDocumentListener(new DocListener(bRegisterFolder));
		tMSILocation.getDocument().addDocumentListener(new DocListener(bRegisterFolder));
		tUserName.getDocument().addDocumentListener(new DocListener(bRegisterFolder));
		tPassword.getDocument().addDocumentListener(new DocListener(bRegisterFolder));
		tDomain.getDocument().addDocumentListener(new DocListener(bRegisterFolder));
		/*------------------End of SIDBI merges----------------------------*/
	}
	
	/**
	 * CV2019 Merges - SIDBI Hot Folder Location enhancement changes
	 */
	private void loadMsiInfo() {
		try {
			String msiURL = null, msiVersion = null, userName = null, password = null, domainName = null;
			if (vws1 == null) {
				msiVersion = VWSPreferences.getMsiVersion1();
				msiURL = VWSPreferences.getMsiURL1();
				userName = VWSPreferences.getUserName();
				password = VWSPreferences.getUserPassword();
				if (password != null && password.trim().length() > 0) {
					password = Util.decryptKey_MSI(password);
				}
				domainName = VWSPreferences.getDomainName();
				
				
			} else {
				msiURL = vws1.getMsiUrlInfo();				
				msiVersion = vws1.getMsiVersionInfo();
				Vector<String> msiInfo = vws1.getMsiServerSettingsInfo();
				userName = msiInfo.get(0);
				//Password already decrypted in getMsiServerSettingsInfo method
				password = msiInfo.get(1); 
				domainName = msiInfo.get(2);				
			}
			tMSIVersion.setText(msiVersion);
			tMSILocation.setText(msiURL);
			tUserName.setText(userName);			
			tPassword.setText(password);
			tDomain.setText(domainName);
		} catch (Exception e) {
			VWSLog.add("Exception in loadMsiInfo :"+e);
		}
	}
	/*------------------End of SIDBI merges----------------------------*/
}
