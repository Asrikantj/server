package com.computhink.manager;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JList;

import com.computhink.manager.image.Images;

public class VWListCellRenderer extends DefaultListCellRenderer {
    public Component getListCellRendererComponent(JList list, Object value,
	    int index, boolean isSelected, boolean cellHasFocus) {
	JLabel label = new JLabel();
	if (value instanceof VWRoomItem) {
	    VWRoomItem room = (VWRoomItem) value;
	    label = (JLabel) super.getListCellRendererComponent(list, value,
		    index, isSelected, cellHasFocus);
	    label.setIconTextGap(10);
	    label.setText(room.getName());
	    if (room.isEnabled()) {
		label.setIcon(Images.online);
	    } else {
		label.setIcon(Images.offline);
	    }
	}
	return label;
    }
}
