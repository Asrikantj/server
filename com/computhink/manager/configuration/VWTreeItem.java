package com.computhink.manager.configuration;

import java.awt.Font;
import java.util.Locale;

import javax.swing.plaf.FontUIResource;
import javax.swing.tree.DefaultMutableTreeNode;

import com.computhink.manager.Manager;
import com.computhink.vwc.VWCPreferences;
import com.computhink.vws.server.VWSSettings;

public class VWTreeItem extends DefaultMutableTreeNode {
    int id;
    String name;
    int type;
    boolean enabled = false;
    public VWTreeItem(int id, String name){
	super(name);
	this.id = id;
	this.name = name;	
    }
    public VWTreeItem(int id, String name, boolean enabled){
	super(name);
	this.id = id;
	this.name = name;	
	this.enabled = enabled;
	setEnabled(enabled);
	Manager.setFont();
    }   
    public VWTreeItem(int id, String name, int type) {
	super(name);
	this.id = id;
	this.name = name;
	this.type = type;
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the type
     */
    public int getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(int type) {
        this.type = type;
    }
    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return enabled;
    }
    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
