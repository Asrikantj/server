package com.computhink.manager.configuration;

import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Hashtable;
import java.util.Stack;
import java.util.prefs.Preferences;

import javax.swing.JTree;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.TreeUI;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeModel;
import com.computhink.resource.ResourceManager;
import com.computhink.ars.server.ARSPreferences;
import com.computhink.common.Constants;
import com.computhink.common.ServerSchema;
import com.computhink.drs.server.DRSPreferences;
import com.computhink.dss.server.DSSPreferences;
import com.computhink.ixr.server.IXRPreferences;
import com.computhink.vns.server.VNSPreferences;
import com.computhink.vws.csserver.CSSPreferences;
import com.computhink.vws.server.VWSPreferences;


public class VWNavigation extends JTree implements Constants{
	private static ResourceManager resourceManager= null;
    public VWNavigation(String serverType, boolean isOffline) {
	// TODO Auto-generated constructor stub
	this(getDefaultTreeModel(serverType, isOffline));
    }
    public VWNavigation(TreeModel newModel)
    {
        super();
        toggleClickCount = 2;
        setLayout(null);
        rowHeight = 16;
        visibleRowCount = 20;
        rootVisible = true;
        scrollsOnExpand = true;
        setOpaque(true);
        updateUI();
        setModel(newModel);

        MouseMotionListener[] ml = this.getMouseMotionListeners();
        for (int i = 0; i < ml.length; i++)
        {
            this.removeMouseMotionListener(ml[i]);
        }

        MouseListener[] ml2 = this.getMouseListeners();
        for (int i = 0; i < ml2.length; i++)
        {
            this.removeMouseListener(ml2[i]);
        }

        for (int i = 0; i < ml.length; i++)
        {
            this.addMouseMotionListener(ml[i]);
        }
        for (int i = 0; i < ml2.length; i++)
        {
            this.addMouseListener(ml2[i]);
        }
        setVisible(true);        
    }
    protected static TreeModel getDefaultTreeModel(String serverType, boolean isOffline)
    {
    	resourceManager=ResourceManager.getDefaultManager();
        boolean enableNode = false;
        boolean enableAllNode = false;
        if (serverType != null && serverType.equals("ALL")){
            enableAllNode = true;
        }
        if (enableAllNode || (serverType != null && serverType.equals(SERVER_VWS)) || checkServerRegistered(SERVER_VWS, isOffline)){
            enableNode = true;
        }
        
		VWTreeItem root = new VWTreeItem(1, resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("Navigation.Server"));
		root.setEnabled(enableNode);
		VWTreeItem parent;

        
        parent = new VWTreeItem(2,resourceManager.getString("Navigation.Server"), enableNode);
        parent.setEnabled(enableNode);
        root.add(parent);
        if (serverType.equalsIgnoreCase(SERVER_VWS) || enableAllNode || checkServerRegistered(SERVER_VWS, isOffline)){
        parent.add(new VWTreeItem(3, resourceManager.getString("Navigation.License"), enableNode));
        parent.add(new VWTreeItem(4, resourceManager.getString("Navigation.ConnectSecurity"), enableNode));
        parent.add(new VWTreeItem(5, resourceManager.getString("Navigation.RoomDB"), enableNode));
        parent.add(new VWTreeItem(6, resourceManager.getString("Navigation.EmailSettings"), enableNode));
        if (!isOffline)
        parent.add(new VWTreeItem(11, resourceManager.getString("Navigation.LoginManagement"), enableNode));
        parent.add(new VWTreeItem(13, resourceManager.getString("Navigation.FolderLocation"), enableNode));        
        }
        enableNode = false;
        if (enableAllNode || ( serverType != null && serverType.equals(SERVER_DSS) ) || checkServerRegistered(SERVER_DSS, isOffline)){
            enableNode = true;
        }
        parent = new VWTreeItem(7, resourceManager.getString("Navigation.DocStorage"), enableNode);
        parent.setEnabled(enableNode);
        root.add(parent);
        enableNode = false;
        if (enableAllNode || serverType != null && serverType.equals(SERVER_IXR)  || checkServerRegistered(SERVER_IXR, isOffline)){
            enableNode = true;
        }
        parent = new VWTreeItem(8, resourceManager.getString("Navigation.Indexer"), enableNode);
        parent.setEnabled(enableNode);
        root.add(parent);
        enableNode = false;
        if (enableAllNode || serverType != null && serverType.equals(SERVER_DRS)  || checkServerRegistered(SERVER_DRS, isOffline)){
            enableNode = true;
        }        
        parent = new VWTreeItem(9,resourceManager.getString("WORKFLOW_NAME"), enableNode);
        parent.setEnabled(enableNode);        
        root.add(parent);
        enableNode = false;
        if (enableAllNode || serverType != null && serverType.equals(SERVER_ARS)  || checkServerRegistered(SERVER_ARS, isOffline)){
            enableNode = true;
        }        
        parent = new VWTreeItem(10, resourceManager.getString("Navigation.Retention"), enableNode);
        parent.setEnabled(enableNode);
        root.add(parent);
        
        enableNode = false;
        if (enableAllNode || serverType != null && serverType.equals(SERVER_VNS)  || checkServerRegistered(SERVER_VNS, isOffline)){
            enableNode = true;
        }   
        if (enableAllNode || serverType != null && serverType.equals(SERVER_CSS)  || checkServerRegistered(SERVER_CSS, isOffline)){
            enableNode = true;
        }   
        
        parent = new VWTreeItem(12, resourceManager.getString("Navigation.Notification"), enableNode);
        parent.setEnabled(enableNode);
        root.add(parent);
        
        parent = new VWTreeItem(14, resourceManager.getString("Navigation.ContentSentinel"), enableNode);
        parent.setEnabled(enableNode);
        root.add(parent);


        return new DefaultTreeModel(root);
    }
    
    public static boolean checkServerRegistered(String serverType, boolean isOffline){
	if (isOffline){
	    if (serverType.equalsIgnoreCase(SERVER_VWS)){
		if (VWSPreferences.getRooms().length > 0){
		    return true;
		}
	    }
	    if (serverType.equalsIgnoreCase(SERVER_DSS)){
		if (DSSPreferences.getVWS().address.trim().length() > 0){
		    return true;
		}
	    }
	    if (serverType.equalsIgnoreCase(SERVER_IXR)){
		if (IXRPreferences.getVWS().address.trim().length() > 0){
		    return true;
		}
	    }
	    if (serverType.equalsIgnoreCase(SERVER_ARS)){
		if (ARSPreferences.getVWS().address.trim().length() > 0){
		    return true;
		}
	    }
	    if (serverType.equalsIgnoreCase(SERVER_DRS)){
		if (DRSPreferences.getVWS().address.trim().length() > 0){
		    return true;
		}
	    }
	    if (serverType.equalsIgnoreCase(SERVER_VNS)){
	    	if (VNSPreferences.getVWS().address.trim().length() > 0){
	    		return true;
	    	}
	    }if (serverType.equalsIgnoreCase(SERVER_CSS)){
	    	if (CSSPreferences.getVWS().address.trim().length() > 0){
	    		return true;
	    	}
	    }
	}
	return false;
    }
    public void updateUI()
    {
        setUI((TreeUI) UIManager.getUI(this));
        invalidate();
    }
    
    /**
     * Sets the L&F object that renders this component.
     * 
     * @param ui
     *            the <code>TreeUI</code> L&F object
     * @see UIDefaults#getUI
     * @beaninfo bound: true hidden: true attribute: visualUpdate true
     *           description: The UI object that implements the Component's
     *           LookAndFeel.
     */
    public void setUI(TreeUI ui)
    {
        if ((TreeUI) this.ui != ui)
        {
            try
            {
                super.setUI(ui);
            }
            finally
            {

            }
        }
    }

}
