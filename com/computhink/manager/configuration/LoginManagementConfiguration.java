package com.computhink.manager.configuration;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;




import com.computhink.common.CIServer;
import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.manager.AboutManagerPanel;
import com.computhink.manager.VWGeneralPanel;
import com.computhink.manager.VWRoomPanel;
import com.computhink.manager.loginManagement;
import com.computhink.manager.image.Images;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWS;
import com.sun.org.apache.xerces.internal.impl.RevalidationHandler;

/**
 * 
 * Class added for 
 * Enhancement:- 2083 Easy access to locked  users
 * Locked time and lock release time update properly with 
 * current time, release time=curtime+ lock time in minutes
 * @author madhavan.b
 *
 */

public class LoginManagementConfiguration extends JPanel implements Constants{
    private static final int width = 715;
    private static final int roomPanelWidth = 850;
    private static ResourceManager resourceManager=ResourceManager.getDefaultManager();
    private static final int height = 520;
    private String title = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("ConfigTitle.ServerConfig");
    String[] servers = {"ALL", PRODUCT_NAME + " Server", "Document Storage Server", "Indexer Server", "Document "+ WORKFLOW_MODULE_NAME +" Server", "Auto Retention Server", "Notification Server","Content Sentinel Server" };
    private Window containerFrame = null;
    
    public LoginManagementConfiguration(JFrame parent, CIServer server, String SERVER) {
	//super(parent, true);
	containerFrame = new JDialog();
	this.server = server;
	this.serverName = SERVER;
	this.isOffline = false;
	new Images();
	((JDialog)containerFrame).setTitle(PRODUCT_NAME + " Server Configuration");
	showNavigation(treePanel);
	initComponents();
	containerFrame.setIconImage(getApplicationImage(SERVER));
	setDefaultRow(navigation.getSelectionPath());
	((JDialog)containerFrame).setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);	
	containerFrame.setSize(width, height);
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	containerFrame.setLocation((screenSize.width - width)/2,(screenSize.height -height)/2);
	((JDialog)containerFrame).setResizable(false);
	containerFrame.setVisible(true);

    }
    public LoginManagementConfiguration(String SERVER) {
	// TODO Auto-generated constructor stub
	super();
	containerFrame = new JFrame();
	new Images();
	this.isOffline = true;
	this.serverName = SERVER;
	((JFrame)containerFrame).setTitle(resourceManager.getString("CVProduct.Name")+" "+resourceManager.getString("ConfigTitle.ServerConfig"));
	showNavigation(treePanel);
	initComponents();	
	containerFrame.setIconImage(getApplicationImage(SERVER));
	setDefaultRow(navigation.getSelectionPath());
	((JFrame)containerFrame).setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	containerFrame.setSize(width, height);
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	containerFrame.setLocation((screenSize.width - width)/2,(screenSize.height -height)/2);
	((JFrame)containerFrame).setResizable(false);
	containerFrame.setVisible(true);
	//setResizable(false);
    }
    int getRowOfServer(String server){
	for (int i = 0; i < servers.length; i++){
	    if (server.equalsIgnoreCase(servers[i])){
		//System.out.println("Set Default Row " + server + " : " + i);
		return i;
	    }
	}
	return 0;
    }
    public Image getApplicationImage(String SERVER){
	if (SERVER.equalsIgnoreCase(PRODUCT_NAME + " Server"+"LoginManagement")){
	    return Images.VWSApp.getImage();
	}else if(SERVER.equalsIgnoreCase("Document Storage Server")){
	    return Images.DSSApp.getImage();
	}else if(SERVER.equalsIgnoreCase("Indexer Server")){
	    return Images.IXRApp.getImage();
	}else if(SERVER.equalsIgnoreCase("Document "+ WORKFLOW_MODULE_NAME +" Server")){
	    return Images.DRSApp.getImage();
	}else if(SERVER.equalsIgnoreCase("Auto Retention Server")){
	    return Images.ARSApp.getImage();
	}else if(SERVER.equalsIgnoreCase("Notification Server")){
	    return Images.VNSApp.getImage();
	}else if(SERVER.equalsIgnoreCase("Content Sentinel Server")) {
		return Images.CSSApp.getImage();
	}	
	    return Images.VWSApp.getImage();	
    }
    void initComponents(){
	containerFrame.setIconImage(Images.app.getImage());
	setLayout(null);
	treePanel.setBounds(0, 0, 200, 485);
	add(treePanel);
	settingsPanel.setLayout(null);
	settingsPanel.setBounds(201, 0, 850, 800);
	settingsPanel.setBackground(new Color(238, 242, 244));
	add(settingsPanel);	
	containerFrame.addWindowListener(new WindowAdapter(){
	    public void windowClosing(WindowEvent e) {
		if (isOffline){
		    System.exit(0);
		}else{
		    containerFrame.setVisible(false);
		    containerFrame.dispose();
		}
		    
	    }
	});
	containerFrame.add(this);
    }
    void showNavigation(JPanel treePanel){
	//navigation.setSelectionModel(new DefaultCheckboxTreeSelectionModel());
	navigation = new VWNavigation(serverName, isOffline);
	navigation.setCellRenderer(new VWTreeRenderer());
	JScrollPane scrollpane = new JScrollPane(navigation);
	scrollpane.setBounds(0, 0, 100, 100);
	scrollpane.setBorder(new EmptyBorder(1,1,1,1));
	BorderLayout borderLayout = new BorderLayout();
	treePanel.setLayout(borderLayout);
	treePanel.add(scrollpane, BorderLayout.CENTER);
	
	JLabel lblContentverse = new JLabel("   "+resourceManager.getString("powered_by_contentverse"));
	lblContentverse.setForeground(Color.GRAY);
	lblContentverse.setBackground(new Color(239, 243, 247));
	lblContentverse.setOpaque(true);
	lblContentverse.setBounds(0,0,100,20);
	Font font = lblContentverse.getFont();
	lblContentverse.setFont(new Font(font.getName(), Font.PLAIN, 10));
	treePanel.add(lblContentverse, BorderLayout.SOUTH);
	treePanel.setBackground(Color.WHITE);

	navigation.addMouseListener(new MouseAdapter() {
		public void mouseReleased(MouseEvent mouseEvent) {
			try {
				int xPos = mouseEvent.getX();
				int yPos = mouseEvent.getY();
				TreePath selectedPath = navigation.getPathForLocation(xPos,yPos);//jTree1.getLeadSelectionPath();
				int id = ((VWTreeItem)selectedPath.getLastPathComponent()).getId(); 

				if (id == LoginManagement) {//( id > ViewWiseServer && id <= EmailSettings){
					serverName = "Document Storage Server";//PRODUCT_NAME + " Server" + "LoginManagement"; 			
				}else if (id == DocumentServer){
					serverName = "Document Storage Server";
				}else if (id == IndexerServer){
					serverName = "Indexer Server";
				}else if (id == RoutingServer){
					serverName = "Document "+ WORKFLOW_MODULE_NAME +" Server";
				}else if (id == RetentionServer){
					serverName = "Auto Retention Server";
				}else if (id == NotificationServer){
					serverName = "Notification Server";
				}
				else if (id == ContentSentinelServer){
					serverName = "Content Sentinel Server";
				}
				
				
				showSettings(0,selectedPath);
			}catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	});
    }

    public  void showSettings(int id1,TreePath selectedPath){
	try{
	    //if(selectedPath != null){
		int id=0;
		String[] titles = {title, title, title, title +" "+
				resourceManager.getString("ConfigTitle.ForLicense"),title +" "+ 
				resourceManager.getString("ConfigTitle.ForConnection"), title +" "+ 
				resourceManager.getString("ConfigTitle.ForRooms"), title +" "+ 
				resourceManager.getString("ConfigTitle.ForEmail"), title +" "+  
			    resourceManager.getString("ConfigTitle.ForDocStorage")+" ", title +" "+ 
			    resourceManager.getString("ConfigTitle.ForIndexer")+" ", title +" "+ 
			    resourceManager.getString("ConfigTitle.ForDocument")+resourceManager.getString("WORKFLOW_NAME")+" ", 
			    title +" "+resourceManager.getString("ConfigTitle.ForRetention")+" ", title +" "+  
			    resourceManager.getString("ConfigTitle.ForLogin")+" ", title +" "+ 
			    resourceManager.getString("ConfigTitle.ForNotification"), title +" "+ 
					    resourceManager.getString("ConfigTitle.ForContentSentinel") };
		
		

		Object node = selectedPath.getLastPathComponent();
		VWTreeItem selectedItem = (VWTreeItem) node;
		if(id1==11)
		{
			
			id=11;
		}else
		 id = selectedItem.getId();
		if(id!=11)
			id=0;
		//System.out.println("Id is -> " + id + " : Enabled " +selectedItem.isEnabled());
		settingsPanel.removeAll();
		repaint();
		settingsPanel.revalidate();
		if (isOffline)
		    ((JFrame)containerFrame).setTitle(titles[id]);
		else
		    ((JDialog)containerFrame).setTitle(titles[id]);
		System.out.println("Title " + serverName);
		containerFrame.setIconImage(getApplicationImage(serverName));    
		//if (!selectedItem.isEnabled()) return;
		switch(id){

		case License:
			containerFrame.setSize(width, height);
		    com.computhink.manager.VWGeneralPanel general = new com.computhink.manager.VWGeneralPanel(server);
		    general.setBounds(0, 0, 600, 800);		    
		    settingsPanel.add(general);
		    repaint();
		    settingsPanel.revalidate();	
		    break;
		    
		case FolderLocation:
			containerFrame.setSize(width, height);
		    com.computhink.manager.VWFolderLocationPanel folder = new com.computhink.manager.VWFolderLocationPanel(server);
		    folder.setBounds(0, 0, 600, 800);		    
		    settingsPanel.add(folder);
		    repaint();
		    settingsPanel.revalidate();	
		    break;
		    
		case Connection_Security: 
			containerFrame.setSize(width, height);
		    com.computhink.manager.VWConnectionPanel connection = new com.computhink.manager.VWConnectionPanel(server);
		    connection.setBounds(0, 0, 620, 800);
		    settingsPanel.add(connection);
		    repaint();
		    settingsPanel.revalidate();		   
		    break;
		case RoomInfo:
			containerFrame.setSize(roomPanelWidth, height);
		    com.computhink.manager.VWRoomPanel rooms = new com.computhink.manager.VWRoomPanel(server);
		    rooms.makeRoomsOnline(isOffline);
		    rooms.setBounds(0, 0, 850, 800);
		    settingsPanel.add(rooms);
		    repaint();
		    settingsPanel.revalidate();
		    break;
		case EmailSettings: 
			containerFrame.setSize(width, height);
		    com.computhink.manager.EmailServerSettings email = new com.computhink.manager.EmailServerSettings();
		    email.setBounds(0, 0, 600, 800);
		    settingsPanel.add(email);
		    repaint();
		    settingsPanel.revalidate();	
		    break;
		case DocumentServer:
			containerFrame.setSize(width, height);
		    if (isOffline){					    
			com.computhink.dss.server.DSSSettingsPanel dssSettings = new com.computhink.dss.server.DSSSettingsPanel();
			dssSettings.setBounds(0, 0, 600, 800);
			settingsPanel.add(dssSettings);
			repaint();
			settingsPanel.revalidate();
		    }else{
			com.computhink.manager.DSSSettingsPanel dssSettings = new com.computhink.manager.DSSSettingsPanel(server);
			dssSettings.setBounds(0, 0, 600, 800);
			settingsPanel.add(dssSettings);
			repaint();
			settingsPanel.revalidate();					
		    }
		    break;
		case IndexerServer:
			containerFrame.setSize(width, height);
		    if (isOffline){					    
			com.computhink.ixr.server.IXRSettingsPanel ixrSettings = new com.computhink.ixr.server.IXRSettingsPanel();
			ixrSettings.setBounds(0, 0, 600, 800);
			settingsPanel.add(ixrSettings);
			repaint();
			settingsPanel.revalidate();
		    }else{
			com.computhink.manager.IXRSettingsPanel ixrSettings = new com.computhink.manager.IXRSettingsPanel(server);
			ixrSettings.setBounds(0, 0, 600, 800);
			settingsPanel.add(ixrSettings);
			repaint();
			settingsPanel.revalidate();						
		    }
		    
		    break;
		case RoutingServer:
			containerFrame.setSize(width, height);
		    if (isOffline){					    
			com.computhink.drs.server.DRSSettingsPanel drsSettings = new com.computhink.drs.server.DRSSettingsPanel();
			drsSettings.setBounds(0, 0, 600, 800);
			settingsPanel.add(drsSettings);
			repaint();
			settingsPanel.revalidate();
		    }else{
			com.computhink.manager.DRSSettingsPanel drsSettings = new com.computhink.manager.DRSSettingsPanel(server);
			drsSettings.setBounds(0, 0, 600, 800);
			settingsPanel.add(drsSettings);
			repaint();
			settingsPanel.revalidate();					
		    }	
		    break;
		case RetentionServer: 
			containerFrame.setSize(width, height);
		    if (isOffline){					    
			com.computhink.ars.server.ARSSettingsPanel arsSettings = new com.computhink.ars.server.ARSSettingsPanel();
			arsSettings.setBounds(0, 0, 600, 800);
			settingsPanel.add(arsSettings);
			repaint();
			settingsPanel.revalidate();
		    }else{
			com.computhink.manager.ARSSettingsPanel arsSettings = new com.computhink.manager.ARSSettingsPanel(server);
			arsSettings.setBounds(0, 0, 600, 800);
			settingsPanel.add(arsSettings);
			repaint();
			settingsPanel.revalidate();					
		    }	
		    
		    break;
		case LoginManagement:
			containerFrame.setSize(width, height);
		    //if (!isOffline)
		{
		    loginManagement login = new loginManagement((VWS)server);
		    login.setBounds(0, 0, 600, 800);
		    settingsPanel.add(login);
		    repaint();
		    settingsPanel.revalidate();					
		}
		break;
		case NotificationServer: 
			containerFrame.setSize(width, height);
			if (isOffline){
				com.computhink.vns.server.VNSSettingsPanel vnsSettings = new com.computhink.vns.server.VNSSettingsPanel();
				vnsSettings.setBounds(0, 0, 600, 800);
				settingsPanel.add(vnsSettings);
				repaint();
				settingsPanel.revalidate();
			}else{
				com.computhink.manager.VNSSettingsPanel vnsSettings = new com.computhink.manager.VNSSettingsPanel(server);
				vnsSettings.setBounds(0, 0, 600, 800);
				settingsPanel.add(vnsSettings);
				repaint();
				settingsPanel.revalidate();					
			}
		break;
		case ContentSentinelServer: 
			containerFrame.setSize(width, height);
			if (isOffline){
				com.computhink.vws.csserver.CSSSettingsPanel cssSettings = new com.computhink.vws.csserver.CSSSettingsPanel();
				cssSettings.setBounds(0, 0, 600, 800);
				settingsPanel.add(cssSettings);
				repaint();
				settingsPanel.revalidate();
			}else{
				com.computhink.manager.CSSettingsPanel cssSettings = new com.computhink.manager.CSSettingsPanel(server);
				cssSettings.setBounds(0, 0, 600, 800);
				settingsPanel.add(cssSettings);
				repaint();
				settingsPanel.revalidate();					
			}
		break;
		case 1:
		    AboutManagerPanel aboutPanel = new AboutManagerPanel(true);
		    aboutPanel.setBounds(0, 0, 600, 800);
		    settingsPanel.add(aboutPanel);
		    containerFrame.setIconImage(getApplicationImage("ALL"));  
		    repaint();
		    settingsPanel.revalidate();	
		}
	    //}	    
	}catch (Exception e) {
	    // TODO: handle exception
	}
    }
    void setDefaultRow(TreePath selectedPath){	
      	navigation.expandRow(1);
      	navigation.setSelectionInterval(6, 6);
   		showSettings(11,navigation.getSelectionPath());
    }
    JPanel treePanel = new JPanel();
    public JPanel settingsPanel = new JPanel();
    VWNavigation navigation = null;
    CIServer server = null;
    String serverName = "";
    boolean isOffline = true;
    public static void main(String[] args) {
	try 
	{

	    String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
	    String windowsLookandFeel  = "com.jgoodies.looks.windows.WindowsLookAndFeel";
	    UIManager.setLookAndFeel(plasticLookandFeel);        	
	}
	catch(Exception e) 
	{
	}
	LoginManagementConfiguration config = null;
	if (args.length == 0)
	    config = new LoginManagementConfiguration("ALL");
	else
	    config = new LoginManagementConfiguration(args[0]);
	//Util.Msg(config, "data " + args[0] + " : " , "VW");
    }
}
