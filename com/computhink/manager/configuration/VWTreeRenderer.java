package com.computhink.manager.configuration;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

import com.computhink.manager.image.Images;

public class VWTreeRenderer extends DefaultTreeCellRenderer {

	private static final long serialVersionUID = 1L;

	ImageIcon imageList[] = { Images.server, Images.server, Images.vws,
	    Images.license, Images.security, Images.rooms, Images.email,
	    Images.dss, Images.ixr, Images.drs, Images.ars, Images.login,Images.vns,Images.hotfolder,Images.css };

    ImageIcon dImageList[] = { Images.server, Images.server, Images.dvws,
	    Images.dlicense, Images.dsecurity, Images.drooms, Images.demail,
	    Images.ddss, Images.dixr, Images.ddrs, Images.dars , Images.server,Images.dvns,Images.hotfolder,Images.dcss};

    public java.awt.Component getTreeCellRendererComponent(JTree tree,
	    java.lang.Object value, boolean selected, boolean expanded,
	    boolean leaf, int row, boolean hasFocus) {
	if (value instanceof VWTreeItem) {
	    VWTreeItem item = (VWTreeItem) value;
	    int id = item.getId();
	    boolean isEnabled = item.isEnabled();
	    setEnabled(isEnabled);
	    /*if (!isEnabled && id > 1) id += 100;*/
	    if (isEnabled) {
	    	setOpenIcon(imageList[id]);
	    	setClosedIcon(imageList[id]);
	    	setLeafIcon(imageList[id]);
	    } else {
	    	setOpenIcon(dImageList[id]);
	    	setClosedIcon(dImageList[id]);
	    	setLeafIcon(dImageList[id]);
	    }
	}
	Component ret = super.getTreeCellRendererComponent(tree, value,
		selected, expanded, leaf, row, hasFocus);
	return (ret);
    }
}
