package com.computhink.manager;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DecimalFormat;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.text.NumberFormatter;

import com.computhink.common.CIServer;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSPreferences;

public class VWConnectionPanel extends VWPanel implements ManagerConstants{
	private static final long serialVersionUID = 1L;
    private static ResourceManager resourceManager=null;
	public VWConnectionPanel() {
		super();
		initComponents();		
		loadSSchemas();
		loadSecurities();		
		getOfflineCurrentSettings();
		addDocListeners();
	}
	public VWConnectionPanel(CIServer server) {
		super();
		this.vws = (VWS) server;
		initComponents();
		loadSSchemas();
		loadSecurities();
		if (vws == null)
		    getOfflineCurrentSettings();
		else
		    getOnlineCurrentSettings();		    
		addDocListeners();
	}

	
    private void initComponents() 
    {
    	setLayout(null);
    	resourceManager=ResourceManager.getDefaultManager();
    	pConnection = new VWPanel();
    	pPorts = new VWPanel();
    	pProxy = new VWPanel();
    	pSecurity = new VWPanel();
    	
    	pConnection.setLayout(null);
    	pPorts.setLayout(null);
        pSecurity.setLayout(null);
        pProxy.setLayout(null);
        
        pConnection.setBackground(getVWColor());
        pPorts.setBackground(getVWColor());
        pSecurity.setBackground(getVWColor());
        pProxy.setBackground(getVWColor());
        
        
        pProxy.setBorder(new TitledBorder("         "+resourceManager.getString("TitleBorder.HttpProxy")));
        
        pSecurity.setBorder(new TitledBorder(resourceManager.getString("TitleBorder.SecurityServer")));
        
        lSvrHost = new JLabel(resourceManager.getString("ConnectionLbl.SrvHost"));
    	lComPort = new JLabel(resourceManager.getString("ConnectionLbl.ComPort"));
    	lDataPort = new JLabel(resourceManager.getString("ConnectionLbl.DataPort"));
    	lScheme = new JLabel(resourceManager.getString("ConnectionLbl.Scheme"));
    	lTree = new JLabel(resourceManager.getString("ConnectionLbl.Tree"));
    	lLDAPSecurity = new JLabel(resourceManager.getString("ConnectionLbl.LDAPSecurity"));
    	lLoginHost = new JLabel(resourceManager.getString("ConnectionLbl.LoginHost"));
    	lAuthorsHost = new JLabel(resourceManager.getString("ConnectionLbl.AuthorsHost"));
    	lAuthorsContext = new JLabel(resourceManager.getString("ConnectionLbl.AuthorsContext"));
    	ldapPass  = new JPasswordField();
    	lLoginContext = new JLabel(resourceManager.getString("ConnectionLbl.LoginContext"));
    	lLDAPPort = new JLabel(resourceManager.getString("ConnectionLbl.LDAPPort"));
    	lNote1 = new JLabel(resourceManager.getString("ConnectionLbl.Note"));
    	
    	tTree = new JTextField();
    	tLoginHost = new JTextField();
    	tAuthorsHost = new JTextField();
    	tAuthorsContext = new JTextField();
    	tLoginContext = new JTextField();
    	tLDAPPort = new JTextField();
        tSvrHost = new JTextField();
        
    	comSecurity = new JComboBox(vSecurity);
    	comSecurity.setBackground(Color.white);
    	comScheme = new JComboBox(vSSchemas);
    	comScheme.setBackground(Color.WHITE);
    	
    	cbLog = new JCheckBox(resourceManager.getString("Connectionchk.LogInfo"));
    	cbLog.setBackground(getVWColor());
    	cbDebug = new JCheckBox(resourceManager.getString("Connectionchk.DebugInfo"));
    	cbDebug.setBackground(getVWColor());
    	tComPort = new JFormattedTextField(getNF());
    	
    	tProxyPort = new JFormattedTextField(getNF());
        tComPort = new JFormattedTextField(getNF());
        tDataPort = new JFormattedTextField(getNF());
        
        cbProxy = new JCheckBox();
        cbProxy.setBackground(getVWColor());
        lProxyHost = new JLabel(resourceManager.getString("ConnectionLbl.ProxyHost"));
        lProxyPort = new JLabel(resourceManager.getString("ConnectionLbl.ProxyPort"));
        tProxyHost = new JTextField();
        
        bRegister2 = new JButton(resourceManager.getString("ConnectionBtn.Apply"));
        bRegister2.setBackground(getVWColor());
  	
    	pConnection.add(pPorts); pPorts.setBounds(10, 20, 490, 102);
        pPorts.setBorder(new TitledBorder(resourceManager.getString("TitleBorder.ServerSettings")));
        iLeft = 14; iTop = 20; iHeight = 20; iWidth = 120;
        if (vws == null){
            pPorts.add(lSvrHost); lSvrHost.setBounds(iLeft, iTop, iWidth + 80, iHeight);
            iLeft += iWidth + 120;
            pPorts.add(tSvrHost); tSvrHost.setBounds(iLeft, iTop, iWidth + 40, iHeight);
            iTop += 24; iLeft = 14;
    	}else{
    	    iTop = 30;
    	}
        pPorts.add(lComPort); lComPort.setBounds(iLeft, iTop, iWidth + 80, iHeight);
        iLeft += iWidth + 118;
        pPorts.add(lDataPort); lDataPort.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft = 14; iTop += 20;
        pPorts.add(tComPort); tComPort.setBounds(iLeft, iTop, iPortWidth, iHeight);
        iLeft += iWidth + 118;
        pPorts.add(tDataPort); tDataPort.setBounds(iLeft, iTop, iPortWidth, iHeight);
        
        
        
        iLeft = 10; iHeight = 20; iWidth = 130;
        iTop = 132;
        pConnection.add(pProxy); pProxy.setBounds(iLeft, iTop,  490, 72);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 20);
        iLeft = 14;iTop = 20;
        pProxy.add(lProxyHost); lProxyHost.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 108;
        pProxy.add(lProxyPort); lProxyPort.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft = 14;iTop += 20;
        pProxy.add(tProxyHost); tProxyHost.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 108;
        pProxy.add(tProxyPort); tProxyPort.setBounds(iLeft, iTop, iPortWidth, iHeight);
        
        iLeft = 10; iHeight = 20; iWidth = 60;
        iTop = 214; 
        /*LDAP screen setting changes implemented - Valli 13 March 2007*/
        pConnection.add(pSecurity);pSecurity.setBounds(iLeft, iTop, 490, 220);
        iTop = 30;iLeft = 14; 
        pSecurity.add(lScheme);lScheme.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 110; 
        pSecurity.add(lTree);lTree.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 105;
        pSecurity.add(lLDAPSecurity);lLDAPSecurity.setBounds(iLeft, iTop, iWidth, iHeight);
        
        
        iLeft = 14; iTop += 20; iWidth = 130;
        pSecurity.add(comScheme);comScheme.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 40;
        pSecurity.add(tTree);tTree.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 35;
        pSecurity.add(comSecurity);comSecurity.setBounds(iLeft, iTop, iWidth, iHeight);
        
        iLeft = 14; iTop += 30; iWidth = 60;
        pSecurity.add(lLoginHost);lLoginHost.setBounds(iLeft, iTop, iWidth + 20, iHeight);
        iLeft += iWidth + 110; 
        pSecurity.add(lAuthorsHost);lAuthorsHost.setBounds(iLeft, iTop, iWidth + 20, iHeight);
        iLeft += iWidth + 105; 
        pSecurity.add(lAuthorsContext);lAuthorsContext.setBounds(iLeft, iTop, iWidth + 40, iHeight);
        
        iLeft = 14; iTop += 20; iWidth = 130;
        pSecurity.add(tLoginHost);tLoginHost.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 40;
        pSecurity.add(tAuthorsHost);tAuthorsHost.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 35;
        pSecurity.add(ldapPass);ldapPass.setBounds(iLeft, iTop, iWidth, iHeight);
        pSecurity.add(tAuthorsContext);tAuthorsContext.setBounds(iLeft, iTop, iWidth, iHeight);
        
        iLeft = 14; iTop += 30; iWidth = 90;                
        pSecurity.add(lLoginContext);lLoginContext.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 245;
        pSecurity.add(lLDAPPort);lLDAPPort.setBounds(iLeft, iTop, iWidth, iHeight);
        
        iLeft = 14; iTop += 20; iWidth = 90;
        pSecurity.add(tLoginContext);tLoginContext.setBounds(iLeft, iTop, iWidth + 210, iHeight);
        iLeft += iWidth + 245; iWidth = 40;
        pSecurity.add(tLDAPPort);tLDAPPort.setBounds(iLeft, iTop, iPortWidth, iHeight);
        
        
        comScheme.setEnabled(true);
        comScheme.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                 if (comScheme.getSelectedIndex() >= 0) 
                 {
                    bRegister2.setEnabled(true);
                    comSchemeStateChanged((String) comScheme.getSelectedItem());
                 }
            }
        });
        
         
        pConnection.add(cbLog); cbLog.setBounds(14, 455, 120, 20);
        pConnection.add(cbDebug); cbDebug.setBounds(150, 455, 150, 20);
        pConnection.add(bRegister2);bRegister2.setBounds(400, 455, 90, 23);
        bRegister2.setToolTipText(lNote1.getText());
        
        
        /*comScheme.setSelectedIndex(-1);
        comSecurity.setSelectedIndex(-1);*/
        
        add(pConnection);
        pConnection.setBounds(0, 0, 500, 700);

        bRegister2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
        	if (vws == null)
        	    setOfflineSettings();
        	else
        	    setOnlineSettings();
                bRegister2.setEnabled(false);
            }
        });
        ActionListener cbAction = new ActionListener(){
            public void actionPerformed(ActionEvent e){
        	bRegister2.setEnabled(true);
            }
        };
        cbDebug.addActionListener(cbAction);
        cbLog.addActionListener(cbAction);
        cbProxy.addActionListener(cbAction);
    }
    
    
    private void setOnlineSettings()
    {
	try
	{
	    // Email Server changes 30 Jan 2007
	    /*            vws.setLicenseFile(tLicense.getText());
            vws.setEmailServerName(this.ess.txtEmailServer.getText());
            vws.setFromEmailId(this.ess.txtFromEmailId.getText());
            vws.setEmailServerPort(this.ess.txtEmailServerPort.getText());
            vws.setEmailServerUsername(this.ess.txtEmailServerUsername.getText());
            vws.setEmailServerPassword(this.ess.txtEmailServerPassword.getText());
	     */            vws.setComPort(Integer.parseInt(tComPort.getText()));
	     vws.setDataPort(Integer.parseInt(tDataPort.getText()));
	     vws.setProxy(cbProxy.getSelectedObjects() != null? true : false);
	     vws.setProxyHost(tProxyHost.getText());
	     String pPort = tProxyPort.getText();
	     if (pPort.equals(""))
		 vws.setProxyPort(0);
	     else
		 vws.setProxyPort(Integer.parseInt(pPort));
	     vws.setSSType((String) comScheme.getSelectedItem());

	     if( vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_ADS) || 
		     vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_NOVELL)||
		     vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_LINUX) )
	     {
		 vws.setSSLoginHost(tLoginHost.getText());
		 vws.setSSAuthorsHost(tAuthorsHost.getText());
		 vws.setTree(tTree.getText());
		 vws.setLoginContext(tLoginContext.getText());
		 vws.setLdapPort(tLDAPPort.getText());
		 vws.setAdminPass(new String(ldapPass.getPassword()));
		 vws.setLdapSecurity((String)comSecurity.getSelectedItem ());
	     }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_ADS)){
		 vws.setSSLoginHost(tLoginHost.getText());
		 vws.setSSAuthorsHost(tAuthorsHost.getText());
	     }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_NDS)){
		 vws.setTree(tTree.getText());
		 vws.setLoginContext(tLoginContext.getText());
		 vws.setAuthorsContext(tAuthorsContext.getText());
	     }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_SSH)){
		 vws.setSSLoginHost(tLoginHost.getText());
		 vws.setSSAuthorsHost(tAuthorsHost.getText());
	     }

	     vws.setLogInfo(cbLog.isSelected());
	     vws.setDebugInfo(cbDebug.isSelected());
	     cbProxyStateChanged();
	}
	catch(Exception e)
	{
	}
    }
    
    private void setOfflineSettings()
    {
	try
	{
	    // Email Server changes 30 Jan 2007
	    /*            vws.setLicenseFile(tLicense.getText());
            vws.setEmailServerName(this.ess.txtEmailServer.getText());
            vws.setFromEmailId(this.ess.txtFromEmailId.getText());
            vws.setEmailServerPort(this.ess.txtEmailServerPort.getText());
            vws.setEmailServerUsername(this.ess.txtEmailServerUsername.getText());
            vws.setEmailServerPassword(this.ess.txtEmailServerPassword.getText());
	     */
	    VWSPreferences.setHostName(tSvrHost.getText());
	    VWSPreferences.setComPort(Integer.parseInt(tComPort.getText()));
	    VWSPreferences.setDataPort(Integer.parseInt(tDataPort.getText()));
	    VWSPreferences.setProxy(cbProxy.getSelectedObjects() != null ? true: false);
	    VWSPreferences.setProxyHost(tProxyHost.getText());
	     String pPort = tProxyPort.getText();
	     if (pPort.equals(""))
		 VWSPreferences.setProxyPort(0);
	     else
		 VWSPreferences.setProxyPort(Integer.parseInt(pPort));
	     VWSPreferences.setSSType((String) comScheme.getSelectedItem());

	     if( VWSPreferences.getSSType().equalsIgnoreCase(SCHEME_LDAP_ADS) || 
		     VWSPreferences.getSSType().equalsIgnoreCase(SCHEME_LDAP_NOVELL)||
		     VWSPreferences.getSSType().equalsIgnoreCase(SCHEME_LDAP_LINUX) )
	     {
		 VWSPreferences.setSSLoginHost(tLoginHost.getText());
		 VWSPreferences.setSSAuthorsHost(tAuthorsHost.getText());
		 VWSPreferences.setTree(tTree.getText());
		 VWSPreferences.setLoginContext(tLoginContext.getText());
		 VWSPreferences.setLdapPort(tLDAPPort.getText());
		 VWSPreferences.setAdminPass(new String(ldapPass.getPassword()));
		 VWSPreferences.setLdapSecurity((String)comSecurity.getSelectedItem ());
	     }else if(VWSPreferences.getSSType().equalsIgnoreCase(SSCHEME_ADS)){
		 VWSPreferences.setSSLoginHost(tLoginHost.getText());
		 VWSPreferences.setSSAuthorsHost(tAuthorsHost.getText());
	     }else if(VWSPreferences.getSSType().equalsIgnoreCase(SSCHEME_NDS)){
		 VWSPreferences.setTree(tTree.getText());
		 VWSPreferences.setLoginContext(tLoginContext.getText());
		 VWSPreferences.setAuthorsContext(tAuthorsContext.getText());
	     }else if(VWSPreferences.getSSType().equalsIgnoreCase(SSCHEME_SSH)){
		 VWSPreferences.setSSLoginHost(tLoginHost.getText());
		 VWSPreferences.setSSAuthorsHost(tAuthorsHost.getText());
	     }

	     VWSPreferences.setLogInfo(cbLog.isSelected());
	     VWSPreferences.setDebugInfo(cbDebug.isSelected());
	     cbProxyStateChanged();
	}
	catch(Exception e)
	{
	}
    }
    
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
            switchProxyData(true);
        else
            switchProxyData(false);
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    
    private void addDocListeners(){
        tComPort.getDocument().addDocumentListener(new DocListener(bRegister2));
        tDataPort.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tProxyHost.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tProxyPort.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tLoginHost.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tTree.getDocument().addDocumentListener(new DocListener(bRegister2));
        tAuthorsHost.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tLoginContext.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tLDAPPort.getDocument().addDocumentListener(new DocListener(bRegister2));
        
        tAuthorsContext.getDocument().addDocumentListener(
                                                   new DocListener(bRegister2));
    }
    private void getOnlineCurrentSettings(){
    	try{
    	tComPort.setValue(new Integer(vws.getComPort()));
        tDataPort.setValue(new Integer(vws.getDataPort()));
        cbProxy.setSelected(vws.getProxy());
        tProxyHost.setText(vws.getProxyHost());
        tProxyPort.setValue(new Integer(vws.getProxyPort()));
        comScheme.setSelectedItem(vws.getSSType());
        if( vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_ADS) || 
       			vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_NOVELL)||
       			vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_LINUX) )
        {
        	tLoginHost.setText(vws.getSSLoginHost());
            tAuthorsHost.setText(vws.getSSAuthorsHost());
            tTree.setText(vws.getTree());
            comSecurity.setSelectedItem(vws.getLdapSecurity());
            ldapPass.setText(vws.getAdminPass());
            tLoginContext.setText(vws.getLoginContext());
            tLDAPPort.setText(vws.getLdapPort());
        }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_ADS)){
        	tLoginHost.setText(vws.getSSLoginHost());
            tAuthorsHost.setText(vws.getSSAuthorsHost());
        }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_SSH)){
        	tLoginHost.setText(vws.getSSLoginHost());
            tAuthorsHost.setText(vws.getSSAuthorsHost());
        }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_NDS)){
            tTree.setText(vws.getTree());
            tLoginContext.setText(vws.getLoginContext());
            tAuthorsContext.setText(vws.getAuthorsContext());
        }
        cbLog.setSelected(vws.getLogInfo());
        cbDebug.setSelected(vws.getDebugInfo());
        }catch(Exception ex){
        }
        bRegister2.setEnabled(false);
    }
    private void getOfflineCurrentSettings(){
    	try{
    	tSvrHost.setText(VWSPreferences.getHostName());
    	tComPort.setValue(new Integer(VWSPreferences.getComPort()));
        tDataPort.setValue(new Integer(VWSPreferences.getDataPort()));
        cbProxy.setSelected(VWSPreferences.getProxy());
        tProxyHost.setText(VWSPreferences.getProxyHost());
        tProxyPort.setValue(new Integer(VWSPreferences.getProxyPort()));
        comScheme.setSelectedItem(VWSPreferences.getSSType());
        if( VWSPreferences.getSSType().equalsIgnoreCase(SCHEME_LDAP_ADS) || 
       			VWSPreferences.getSSType().equalsIgnoreCase(SCHEME_LDAP_NOVELL)||
       			VWSPreferences.getSSType().equalsIgnoreCase(SCHEME_LDAP_LINUX) )
        {
        	tLoginHost.setText(VWSPreferences.getSSLoginHost());
            tAuthorsHost.setText(VWSPreferences.getSSAuthorsHost());
            tTree.setText(VWSPreferences.getTree());
            comSecurity.setSelectedItem(VWSPreferences.getLdapSecurity());
            ldapPass.setText(VWSPreferences.getAdminPass());
            tLoginContext.setText(VWSPreferences.getLoginContext());
            tLDAPPort.setText(VWSPreferences.getLdapPort());
        }else if(VWSPreferences.getSSType().equalsIgnoreCase(SSCHEME_ADS)){
        	tLoginHost.setText(VWSPreferences.getSSLoginHost());
            tAuthorsHost.setText(VWSPreferences.getSSAuthorsHost());
        }else if(VWSPreferences.getSSType().equalsIgnoreCase(SSCHEME_SSH)){
        	tLoginHost.setText(VWSPreferences.getSSLoginHost());
            tAuthorsHost.setText(VWSPreferences.getSSAuthorsHost());
        }else if(VWSPreferences.getSSType().equalsIgnoreCase(SSCHEME_NDS)){
            tTree.setText(VWSPreferences.getTree());
            tLoginContext.setText(VWSPreferences.getLoginContext());
            tAuthorsContext.setText(VWSPreferences.getAuthorsContext());
        }
        cbLog.setSelected(VWSPreferences.getLogInfo());
        cbDebug.setSelected(VWSPreferences.getDebugInfo());
        }catch(Exception ex){
        }
        bRegister2.setEnabled(false);
    }
    private NumberFormatter getNF()
    {
        DecimalFormat decimalFormat = new DecimalFormat("####");
        NumberFormatter numberFormatter = new NumberFormatter(decimalFormat);
        numberFormatter.setOverwriteMode(true);
        numberFormatter.setAllowsInvalid(false);
        return(numberFormatter);
    }
    private void loadSSchemas()
    {
        vSSchemas.addElement(SSCHEME_ADS);
        vSSchemas.addElement(SSCHEME_NDS);
        vSSchemas.addElement(SSCHEME_SSH);
        //Different LDAP scheme are added here
        vSSchemas.addElement(SCHEME_LDAP_ADS);
        vSSchemas.addElement(SCHEME_LDAP_NOVELL);
        vSSchemas.addElement(SCHEME_LDAP_LINUX);

    }
    private void loadSecurities ()
    {
    	vSecurity.addElement (SECURITY_SIMPLE);
    }
    private void comSchemeStateChanged(String scheme)
    {
        if (scheme == null) return;
        if (scheme.equals(SSCHEME_NDS))
            switchTreeContext(true);
        else if(scheme.equals(SCHEME_LDAP_ADS) ||
        		scheme.equals(SCHEME_LDAP_NOVELL) ||
        		scheme.equals(SCHEME_LDAP_LINUX))
            switchtoLDAPFields();
        else
            switchTreeContext(false);
    }
    private void switchTreeContext(boolean b)
    {
	lTree.setText(resourceManager.getString("ConnectionLbl.Tree"));
	lAuthorsHost.setText(resourceManager.getString("ConnectionLbl.AuthorsHost"));
	lAuthorsContext.setText(resourceManager.getString("ConnectionLbl.AuthorsContext"));
	if(!b)
	{
	    tTree.setText("");
	    tLoginContext.setText("");
	    tLDAPPort.setText("");
	}
	tTree.setEnabled(b);
	tAuthorsContext.setVisible(true);
	tAuthorsContext.setEnabled(b);
	tLoginContext.setEnabled(b);
	tLDAPPort.setEnabled(b);
	tLoginHost.setEnabled(!b);
	tAuthorsHost.setEnabled(!b);
	ldapPass.setEnabled(false);
	ldapPass.setVisible(false);
	ldapPass.setText("");
	comSecurity.setEnabled(false);  

	tLDAPPort.setEnabled(false);
	lLDAPSecurity.setEnabled(false);
	lLDAPPort.setEnabled(false);
	tLDAPPort.setBackground(Color.WHITE);
	if(b){
	    tTree.setBackground(bgHighlightedColor);
	    tAuthorsContext.setBackground(bgHighlightedColor);	
	    tLoginContext.setBackground(bgHighlightedColor);            
	    tLoginHost.setBackground(Color.WHITE);        
	    tAuthorsHost.setBackground(Color.WHITE);

	    lTree.setEnabled(true);
	    lAuthorsContext.setEnabled(true);
	    lLoginContext.setEnabled(true);
	    lLoginHost.setEnabled(false);
	    lAuthorsHost.setEnabled(false);


	}else{
	    tTree.setBackground(Color.WHITE);
	    tAuthorsContext.setBackground(Color.WHITE);	
	    tLoginContext.setBackground(Color.WHITE);
	    tLoginHost.setBackground(bgHighlightedColor);        
	    tAuthorsHost.setBackground(bgHighlightedColor);
	    lTree.setEnabled(false);
	    lAuthorsContext.setEnabled(false);
	    lLoginContext.setEnabled(false);
	    lLoginHost.setEnabled(true);
	    lAuthorsHost.setEnabled(true);                        
	}
    }
    private void switchtoLDAPFields()
    {
    	try{
	        tLoginContext.setEnabled(true);
	        tLoginHost.setEnabled(true);
	        tTree.setEnabled(true);
	        tLDAPPort.setEnabled(true);
	        lTree.setText(resourceManager.getString("ConnectionLbl.Directory"));
	        tAuthorsHost.setEnabled(true);
	        lAuthorsHost.setText(resourceManager.getString("ConnectionLbl.AdminUser"));
        	lAuthorsContext.setText(resourceManager.getString("ConnectionLbl.Password"));
	        tAuthorsContext.setVisible(false);
	        ldapPass.setEnabled(true);
	        ldapPass.setVisible(true);
	        comSecurity.setEnabled(true);
	        if (vws == null){
	            tTree.setText(VWSPreferences.getTree());
	            comSecurity.setSelectedItem(VWSPreferences.getLdapSecurity());
	            ldapPass.setText(VWSPreferences.getAdminPass());
	            tLoginContext.setText(VWSPreferences.getLoginContext());
	            tLDAPPort.setText(VWSPreferences.getLdapPort());
	        }else{
	            tTree.setText(vws.getTree());
	            comSecurity.setSelectedItem(vws.getLdapSecurity());
	            ldapPass.setText(vws.getAdminPass());
	            tLoginContext.setText(vws.getLoginContext());
	            tLDAPPort.setText(vws.getLdapPort());

	        }
        	tLoginContext.setBackground(bgHighlightedColor);
	        tLoginHost.setBackground(bgHighlightedColor);
	        tTree.setBackground(bgHighlightedColor);
		    tLDAPPort.setBackground(bgHighlightedColor);
		    ldapPass.setBackground(bgHighlightedColor);
		    
		    lLoginContext.setEnabled(true);
		    lLoginHost.setEnabled(true);
		    lTree.setEnabled(true);
		    lLDAPPort.setEnabled(true);
		    lAuthorsContext.setEnabled(true);
		    lLDAPSecurity.setEnabled(true);	        
    	}catch(Exception e){}
    }
    
    private VWS vws;
    private VWPanel pConnection;
    private int iHeight, iWidth, iTop, iLeft = 0, iPortWidth = 50;
    private VWPanel pSecurity, pProxy, pPorts;
    private JTextField tLoginHost, tProxyHost, tTree, tAuthorsContext, tAuthorsHost, 
                                                        tLoginContext, tLDAPPort, tSvrHost; //, tLDAPSecurity;
    private JFormattedTextField tProxyPort, tComPort, tDataPort;
    private JButton bRegister2;
    private JLabel lProxyHost, lAuthorsContext, lLoginContext, lTree, lLoginHost, lDataPort, lComPort, lScheme, 
            lProxyPort, lAuthorsHost, lNote1, lLDAPSecurity, lLDAPPort, lSvrHost;
    private JComboBox comScheme, comSecurity;
    private JCheckBox cbProxy, cbLog, cbDebug;
    private JPasswordField ldapPass;
    private Color bgHighlightedColor = new Color(255, 255, 180);//new Color(230, 243, 255);
    
    VWSPreferences vwsPreferences=new VWSPreferences(); 
    EmailServerSettings ess=new EmailServerSettings(vwsPreferences);
    private Vector vSecurity = new Vector();
    private Vector vSSchemas = new Vector();
    public static Color getVWColor(){
    	return new Color(238, 242, 244);
    }
    
    public static void main(String args[]){
    	try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}catch(Exception ex){
			
		}
    	
    	JFrame frame = new JFrame();
    	VWConnectionPanel panel = new VWConnectionPanel();
		frame.add(panel);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		/*
     	 * new frame.reSize(800, 600); method is replaced with new frame.setSize(800, 600); 
     	 * as reSize() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
     	 */
		frame.setSize(800, 600);
    }
}
