package com.computhink.manager;

public class VWRoomItem {
    private int id;
    private String name;
    private boolean enabled;

    public VWRoomItem(String name, boolean enabled) {
	super();	
	this.name = name;
	this.enabled = enabled;
    }
    
    public VWRoomItem(int id, String name, boolean enabled) {
	super();
	this.id = id;
	this.name = name;
	this.enabled = enabled;
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }
    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the status
     */
    public boolean isEnabled() {
        return enabled;
    }
    /**
     * @param status the status to set
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
