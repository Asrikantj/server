/*
 * Manager.java
 *
 * Created on September 14, 2003, 2:41 PM
 */
package com.computhink.manager;
import java.awt.Font;

import javax.swing.JFrame;
import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import com.computhink.common.*;
import com.computhink.vwc.VWCPreferences;
/**
 *
 * @author  Saad
 */
public class Manager extends JFrame implements Constants
{
	public static String fontName="";
    public static int fontSize=14;
    public static void main(String[] args)
    {
       
        try 
        {
			/*
			Enhancement No / Purpose:  <85/Update the UI with JGoodies>
			Created by: <Pandiya Raj.M>
			Date: <03 Aug 2006>
			*/        	
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        	String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
        	String plasticXPLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
        	/*
        	 * Added for localization to the fontname and fontsize from registry
        	 * Modified by Madhavan
        	 */
        	UIManager.setLookAndFeel(plasticLookandFeel); 
        	if (VWCPreferences.getFontName().trim().length()> 0)
        		fontName = VWCPreferences.getFontName();
        	fontSize = VWCPreferences.getFontSize();
        	setUIFont(new FontUIResource(fontName, Font.PLAIN, fontSize));
        	
        }
        catch(Exception e) 
        {
        }
        
       
         if (ManagerPreferences.getProxy())
            Util.RegisterProxy(ManagerPreferences.getProxyHost(), 
                               ManagerPreferences.getProxyPort(),
                                                            PRODUCT_NAME + " Manager");
         ManagerFrame frame = new ManagerFrame();
         /*
     	 * new frame.show(); method is replaced with new frame.setVisible(true); 
     	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
     	 */
         frame.setVisible(true);
    }
    /*
	 * Added for localization to set font
	 * Modified by Madhavan
	 */
    public static void setUIFont(javax.swing.plaf.FontUIResource f) {
    	java.util.Enumeration keys = UIManager.getDefaults().keys();
    	while (keys.hasMoreElements()) {
    		Object key = keys.nextElement();
    		Object value = UIManager.get(key);

    		if (value instanceof javax.swing.plaf.FontUIResource){		
    			UIManager.put(key, f);
    		}
    	}
    }
    public static void setFont()
    {
    	if (VWCPreferences.getFontName().trim().length()> 0){
    		fontName = VWCPreferences.getFontName();
    		fontSize = VWCPreferences.getFontSize();
    	}
        	setUIFont(new FontUIResource(fontName, Font.PLAIN, fontSize));
    }
}
