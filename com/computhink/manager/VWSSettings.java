/*
 * VWSSettings.java
 *
 * Created on November 23, 2003, 2:20 PM
 */

package com.computhink.manager;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.NumberFormatter;
import javax.swing.text.html.HTMLEditorKit;

import com.computhink.common.CIServer;
import com.computhink.common.RoomProperty;
import com.computhink.common.Util;
import com.computhink.vwc.resource.ResourceManager;
import com.computhink.manager.image.Images;
import com.computhink.vws.license.License;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSPreferences;
import com.computhink.vws.server.VWSUtil;

import com.jgoodies.looks.plastic.PlasticXPLookAndFeel;

public class VWSSettings extends JDialog implements ManagerConstants 
{
    private VWS vws;
    private Vector vSSchemas = new Vector();
    private Vector vDBEngines = new Vector();
    private Vector vSecurity = new Vector();
    private Vector vRooms = new Vector();
    private Vector vRoomInfo = new Vector();
    
	private int iHeight, iWidth, iTop, iLeft = 0, iPortWidth = 50;
    private JPanel pDatabase, pConnection, pLicense, pBypass, pSecurity, pProxy, 
            pRooms, pRRooms, pPorts;
    private JTextField tNamedUsers,tProfessionalNamedUsers,tLoginHost, tName,tKey, tProxyHost, 
            tDBName,tHost, tTree, tUser, tAuthorsContext, tAdmins, tAuthorsHost, 
                                                        tLicense, tLoginContext, tLDAPPort; //, tLDAPSecurity;
    private JFormattedTextField tIdle, tPort, tProxyPort, tComPort, tDataPort;
    private JButton bAdd, bRemove, bRegister1, bRegister2, bRegister3, bClose, 
            bBrowse, btnEmailSettingsRegister;
    private JLabel lPass, lEngine, lProxyHost, lMinutes, lPort, lNamedUsers, 
            lAuthorsContext, lLoginContext, lIdle, lUser, lName, lTree, lHost, 
            lLoginHost, lAdmins, lDataPort, lComPort, lKey, lScheme, 
            lProxyPort, lDBName, lAuthorsHost, lLicense, lNote1, lNote2,
            lGeneralHelp, lLDAPSecurity, lLDAPPort, lMinute, lDBScheduler, lRegRooms;
    private JList lstRooms;
    private JComboBox comEngine, comScheme, comSecurity;
    private JScrollPane spRooms, spRRooms;
    private JCheckBox cbBypass, cbProxy, cbLog, cbDebug;
    private JTabbedPane tpSettings;
    private JPasswordField tPass;
    private JPasswordField ldapPass;
    private JEditorPane epLogo;
    
    VWSPreferences vwsPreferences=new VWSPreferences(); 
    EmailServerSettings ess=new EmailServerSettings(vwsPreferences);
    private JRadioButton ChkOpt1, ChkOpt2;
    private ButtonGroup group1;
    private JSpinner jSpinner;
    private JSpinner minuteSpinner;   
    private SpinnerDateModel dateModel;
    private SpinnerNumberModel minuteModel;       

    
    private static final int width = 540;
    private static final int height = 540;
    private Color bgHighlightedColor = new Color(255, 255, 180);//new Color(230, 243, 255);
    int iRegLeft = 420, iRegTop = 395, iRegWidth = 80, iRegHeight = 23;
    private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
    
    public VWSSettings(JFrame parent, boolean modal, CIServer server) 
    {
        super(parent, modal);
        loadSSchemas();
        loadDBEngines();
        loadSecurities();
        vws = (VWS) server;
        initComponents();
        getGeneral();
        getCurrentSettings();
        synchronizeRooms();
        readEnv();
        addDocListeners();
    }
    private void loadSSchemas()
    {
        vSSchemas.addElement(SSCHEME_ADS);
        vSSchemas.addElement(SSCHEME_NDS);
        vSSchemas.addElement(SSCHEME_SSH);
        //Different LDAP scheme are added here
        vSSchemas.addElement(SCHEME_LDAP_ADS);
        vSSchemas.addElement(SCHEME_LDAP_NOVELL);
        vSSchemas.addElement(SCHEME_LDAP_LINUX);

    }
    private void loadDBEngines()
    {
        vDBEngines.addElement(DBENGINE_SQL);
        vDBEngines.addElement(DBENGINE_ORA);
    }
    private void loadSecurities ()
    {
    	vSecurity.addElement (SECURITY_SIMPLE);
    }
    private NumberFormatter getNF()
    {
        DecimalFormat decimalFormat = new DecimalFormat("####");
        NumberFormatter numberFormatter = new NumberFormatter(decimalFormat);
        numberFormatter.setOverwriteMode(true);
        numberFormatter.setAllowsInvalid(false);
        return(numberFormatter);
    }
    private void initComponents() 
    {
        pConnection = new JPanel();
        pLicense = new JPanel();
        pProxy = new JPanel();
        pSecurity = new JPanel();
        pPorts = new JPanel();
        pDatabase = new JPanel();
        pBypass = new JPanel();
        pRooms = new JPanel();
        pRRooms = new JPanel();
        
        cbProxy = new JCheckBox();
        cbBypass = new JCheckBox("Bypass Security");
        cbLog = new JCheckBox("Log Information");
        cbDebug = new JCheckBox("Debug Information");
        tpSettings = new JTabbedPane();
        comScheme = new JComboBox(vSSchemas); 
        comEngine = new JComboBox(vDBEngines);
        comSecurity = new JComboBox(vSecurity); 
        spRooms = new JScrollPane();
        spRRooms = new JScrollPane();
        lstRooms = new JList();
        tPass = new JPasswordField();
        ldapPass  = new JPasswordField();

        bAdd = new JButton("Add");
        bRemove = new JButton("Remove");
        bRegister1 = new JButton("Register");
        btnEmailSettingsRegister = new JButton("Register");
        bRegister2 = new JButton("Register");
        bRegister3 = new JButton("Register");
        bClose = new JButton("Close");
        bBrowse = new JButton(); 

        tLicense = new JTextField();
        tHost = new JTextField();
        tUser = new JTextField();
        tNamedUsers = new JTextField();
        tAdmins = new JTextField();
        tName = new JTextField();
        tDBName = new JTextField();
        tKey = new JTextField();
        tProxyHost = new JTextField();
        tLoginHost = new JTextField();
        tAuthorsHost = new JTextField();
        tAuthorsContext = new JTextField();
        tLoginContext = new JTextField();
        tTree = new JTextField();
        tLDAPPort = new JTextField();
        
        lLicense = new JLabel(PRODUCT_NAME + " License File:");
        lComPort = new JLabel("Communication port");
        lDataPort = new JLabel("Data Port");
        lMinutes = new JLabel("minutes");
        lIdle = new JLabel("Idle Timeout");
        lProxyHost = new JLabel("Proxy Host");
        lProxyPort = new JLabel("Proxy Port");
        lPass = new JLabel("Password:");
        lEngine = new JLabel("DB Engine:");
        lHost = new JLabel("DB Host:");
        lPort = new JLabel("DB Port:");
        lUser = new JLabel("DB User:");
        lKey = new JLabel("Document Encryption Key");
        lDBName = new JLabel("DB Name:");
        lScheme = new JLabel("Scheme");
        lLoginContext = new JLabel("Login Context");
        lLDAPPort = new JLabel("LDAP Port");
        lAuthorsContext = new JLabel("Authors Context");
        lTree = new JLabel("Tree");
        lAdmins = new JLabel("Administrators Group");
        lNamedUsers = new JLabel(" Named Users Group:");
        lName = new JLabel("Room Name");
        lLoginHost = new JLabel("Login Host");
        lAuthorsHost = new JLabel("Authors Host");
        lNote1 = new JLabel
        (" Server must be restarted for the new settings to take effect.");
        lNote2 = new JLabel(" Delimit multiple Contexts with ';' ");
        lGeneralHelp = new JLabel
        (" Double-click on Room icon to bring/take Room online/offline.");
        lLDAPSecurity = new JLabel("Security");
        lDBScheduler = new JLabel("Reset database connection:");
        lRegRooms = new JLabel("Registered Rooms");
        
        epLogo = new JEditorPane();
        epLogo.setEditorKit(new HTMLEditorKit()); 
        epLogo.setEditable(false);
        epLogo.setBorder(null);
        
        tProxyPort = new JFormattedTextField(getNF());
        tComPort = new JFormattedTextField(getNF());
        tDataPort = new JFormattedTextField(getNF());
        tPort = new JFormattedTextField(getNF());
        tIdle = new JFormattedTextField(getNF());
        
        // Database scheduler controls
        lMinute = new JLabel ("minutes");
	    ChkOpt1 = new JRadioButton();
	    ChkOpt2 = new JRadioButton();
	    group1 = new ButtonGroup(); 
	    
        group1.add(ChkOpt1);
        group1.add(ChkOpt2);
        ChkOpt1.setSelected(true);
        
        dateModel = new SpinnerDateModel();
	    minuteModel = new SpinnerNumberModel(1, 1, 999, 1);
	    
	    dateModel.setCalendarField(Calendar.HOUR_OF_DAY);
	    jSpinner = new JSpinner(dateModel);jSpinner.setBounds(280,55, 100, 20);
	    JSpinner.DateEditor editor2 = new JSpinner.DateEditor(jSpinner, "HH:mm:ss");
	    jSpinner.setEditor(editor2);		    
	    minuteSpinner = new JSpinner(minuteModel);minuteSpinner.setBounds(344,30,36, 20);
	    //	  Get the date formatter
	    JFormattedTextField tf = ((JSpinner.DefaultEditor)jSpinner.getEditor()).getTextField();
	    tf.setEditable(true);
	    
        ChkOpt1.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent evt) 
            {            	
            	 bRegister3.setEnabled(true);
            }
        });
        ChkOpt2.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent evt) 
            {            	
            	 bRegister3.setEnabled(true);
            }
        });

        //----------------------------------------------------------------------
        setTitle(PRODUCT_NAME + " Server Settings - online");
        getContentPane().setLayout(null);
        getContentPane().add(bClose); bClose.setBounds(431, 475, 80, 23);
        getContentPane().add(tpSettings); tpSettings.setBounds(10, 10, 513, 460);
        
        pProxy.setLayout(null);
        pPorts.setLayout(null);
        pSecurity.setLayout(null);
        pRooms.setLayout(null);
        pConnection.setLayout(null);
        pLicense.setLayout(null);
        pBypass.setLayout(null);
        
        spRRooms.setBorder(null);
        spRRooms.setViewportBorder(new TitledBorder(
                     new BevelBorder(BevelBorder.LOWERED), "Registered Rooms"));
        
        //spRooms.setBorder(new TitledBorder("Registered Rooms"));
        pProxy.setBorder(new TitledBorder("         Use Http Proxy"));
        //pBypass.setBorder(new TitledBorder("          Bypass Security"));
        pSecurity.setBorder(new TitledBorder("Security Server"));

        tpSettings.setTabPlacement(JTabbedPane.BOTTOM);
        tpSettings.setAutoscrolls(true);
        
        pConnection.add(pPorts); pPorts.setBounds(10, 5, 490, 72);
        pPorts.setBorder(new TitledBorder("Server Settings"));
        iLeft = 14; iTop = 20; iHeight = 20; iWidth = 120;
        pPorts.add(lComPort); lComPort.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 118;
        pPorts.add(lDataPort); lDataPort.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft = 14; iTop += 20;
        pPorts.add(tComPort); tComPort.setBounds(iLeft, iTop, iPortWidth, iHeight);
        iLeft += iWidth + 118;
        pPorts.add(tDataPort); tDataPort.setBounds(iLeft, iTop, iPortWidth, iHeight);

        iLeft = 10; iHeight = 20; iWidth = 130;
        iTop = 86;
        pConnection.add(pProxy); pProxy.setBounds(iLeft, iTop,  490, 72);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 20);
        iLeft = 14;iTop = 20;
        pProxy.add(lProxyHost); lProxyHost.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 108;
        pProxy.add(lProxyPort); lProxyPort.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft = 14;iTop += 20;
        pProxy.add(tProxyHost); tProxyHost.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 108;
        pProxy.add(tProxyPort); tProxyPort.setBounds(iLeft, iTop, iPortWidth, iHeight);
        
        iLeft = 10; iHeight = 20; iWidth = 60;
        iTop = 166; 
        /*LDAP screen setting changes implemented - Valli 13 March 2007*/
        pConnection.add(pSecurity);pSecurity.setBounds(iLeft, iTop, 490, 200);
        iTop = 20;iLeft = 14; 
        pSecurity.add(lScheme);lScheme.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 110; 
        pSecurity.add(lTree);lTree.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 105;
        pSecurity.add(lLDAPSecurity);lLDAPSecurity.setBounds(iLeft, iTop, iWidth, iHeight);
        
        iLeft = 14; iTop += 20; iWidth = 130;
        pSecurity.add(comScheme);comScheme.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 40;
        pSecurity.add(tTree);tTree.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 35;
        pSecurity.add(comSecurity);comSecurity.setBounds(iLeft, iTop, iWidth, iHeight);
        
        iLeft = 14; iTop += 30; iWidth = 60;
        pSecurity.add(lLoginHost);lLoginHost.setBounds(iLeft, iTop, iWidth + 20, iHeight);
        iLeft += iWidth + 110; 
        pSecurity.add(lAuthorsHost);lAuthorsHost.setBounds(iLeft, iTop, iWidth + 20, iHeight);
        iLeft += iWidth + 105; 
        pSecurity.add(lAuthorsContext);lAuthorsContext.setBounds(iLeft, iTop, iWidth + 40, iHeight);
        
        iLeft = 14; iTop += 20; iWidth = 130;
        pSecurity.add(tLoginHost);tLoginHost.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 40;
        pSecurity.add(tAuthorsHost);tAuthorsHost.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 35;
        pSecurity.add(ldapPass);ldapPass.setBounds(iLeft, iTop, iWidth, iHeight);
        pSecurity.add(tAuthorsContext);tAuthorsContext.setBounds(iLeft, iTop, iWidth, iHeight);
        
        iLeft = 14; iTop += 30; iWidth = 90;                
        pSecurity.add(lLoginContext);lLoginContext.setBounds(iLeft, iTop, iWidth, iHeight);
        iLeft += iWidth + 245;
        pSecurity.add(lLDAPPort);lLDAPPort.setBounds(iLeft, iTop, iWidth, iHeight);
        
        iLeft = 14; iTop += 20; iWidth = 90;
        pSecurity.add(tLoginContext);tLoginContext.setBounds(iLeft, iTop, iWidth + 210, iHeight);
        iLeft += iWidth + 245; iWidth = 40;
        pSecurity.add(tLDAPPort);tLDAPPort.setBounds(iLeft, iTop, iPortWidth, iHeight);

        
        /*pConnection.add(pSecurity); pSecurity.setBounds(20, 210, 450, 150);
        pSecurity.add(lScheme);lScheme.setBounds(25, 30, 60, 16);
        pSecurity.add(comScheme);comScheme.setBounds(120, 30, 100, 20);
        pSecurity.add(lLoginHost);lLoginHost.setBounds(230, 30, 80, 16);
        pSecurity.add(tLoginHost);tLoginHost.setBounds(330, 30, 100, 20);
        pSecurity.add(lTree);lTree.setBounds(25, 60, 80, 16);
        pSecurity.add(tTree);tTree.setBounds(120, 60, 100, 20);
        pSecurity.add(lAuthorsHost);lAuthorsHost.setBounds(230, 60, 80, 16);
        pSecurity.add(tAuthorsHost);tAuthorsHost.setBounds(330, 60, 100, 20);
        pSecurity.add(lLDAPSecurity);lLDAPSecurity.setBounds(25, 90, 120, 16);
        pSecurity.add(comSecurity);comSecurity.setBounds(120, 90, 100, 20);      
        pSecurity.add(lAuthorsContext);lAuthorsContext.setBounds(230, 90, 130, 16);
        pSecurity.add(ldapPass);ldapPass.setBounds(330, 90, 100, 20);
        pSecurity.add(tAuthorsContext);tAuthorsContext.setBounds(330, 90, 100, 20);
        pSecurity.add(lLoginContext);lLoginContext.setBounds(25, 120, 130, 16);
        pSecurity.add(tLoginContext);tLoginContext.setBounds(120, 120, 200, 20);
        pSecurity.add(lLDAPPort);lLDAPPort.setBounds(330, 120, 60, 16);
        pSecurity.add(tLDAPPort);tLDAPPort.setBounds(391, 120, 39, 20);
        //comScheme.setBackground(new Color(255, 255, 204));
        
        /*pSecurity.add(lScheme);lScheme.setBounds(25, 30, 60, 16);
        pSecurity.add(comScheme); comScheme.setBounds(110, 30, 100, 20);
        pSecurity.add(lLoginHost); lLoginHost.setBounds(230, 30, 80, 16);
        pSecurity.add(tLoginHost); tLoginHost.setBounds(330, 30, 100, 20);
        pSecurity.add(lAuthorsHost); lAuthorsHost.setBounds(230, 70, 80, 16);
        pSecurity.add(tAuthorsHost); tAuthorsHost.setBounds(330, 70, 100, 20);
        pSecurity.add(lTree); lTree.setBounds(25, 70, 30, 16);
        pSecurity.add(tTree); tTree.setBounds(110, 70, 100, 20);
        pSecurity.add(lLoginContext); lLoginContext.setBounds(25, 110, 130, 16);
        pSecurity.add(tLoginContext); tLoginContext.setBounds(110, 110, 100, 20);
        pSecurity.add(lAuthorsContext); lAuthorsContext.setBounds(230, 110, 130, 16);
        pSecurity.add(tAuthorsContext); tAuthorsContext.setBounds(330, 110, 100, 20);
        comScheme.setBackground(new Color(255, 255, 204));*/
        //---------------------General Tab----------------------------------------
        tpSettings.addTab("General", pLicense);
        pLicense.add(epLogo); epLogo.setBounds(0, 0, 500, 145);
        lLicense.setBounds(20, 160, 160, 16); pLicense.add(lLicense); 
        tLicense.setBounds(190, 160, 285, 20);pLicense.add(tLicense);
        tLicense.setEnabled(false);
        bBrowse.setBounds(480, 160, 20, 20);pLicense.add(bBrowse);
        bBrowse.setIcon(Images.bro);
        lNamedUsers.setBounds(20, 190, 150, 16); pLicense.add(lNamedUsers);
        tNamedUsers.setBounds(190, 190, 310, 20); pLicense.add(tNamedUsers);
        tProfessionalNamedUsers.setBounds(190,226,310,20);pLicense.add(tProfessionalNamedUsers);
        bRegister1.setBounds(iRegLeft, iRegTop, iRegWidth, iRegHeight); pLicense.add(bRegister1); 
        pLicense.add(spRRooms);
        spRRooms.setViewportView(pRRooms);
        spRRooms.setBounds(20, 220, 480, 125);
        pLicense.add(lGeneralHelp); lGeneralHelp.setBounds(20, 360, 420, 26);
        lGeneralHelp.setForeground(Color.blue);
        //---------------------Rooms Tab----------------------------------------
        tpSettings.addTab("Connection & Security", pConnection);
        lRegRooms.setBounds(10, 5, 100, 20);
        pRooms.add(lRegRooms);
        lstRooms.setBackground(new Color(255, 255, 204));
        pRooms.add(spRooms); spRooms.setBounds(10, 25, 190, 210);
        spRooms.setViewportView(lstRooms);
        pDatabase.setLayout(null);
        pDatabase.setBorder(new TitledBorder("Database"));
        iLeft = 10; iTop = 20; iHeight = 16; iWidth = 70;
        pDatabase.setBounds(230, 90, 270, 285); 
        lEngine.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lEngine);
        iTop += 25;
        lDBName.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lDBName);
        iTop += 25;
        lHost.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lHost);
        iTop += 25;
        lPort.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lPort);
        iTop += 30;
        lUser.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lUser);
        iTop += 25;
        lPass.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(lPass);
        iLeft = iLeft + iWidth + 5;
        iTop = 20; iHeight = 20; iWidth = 155;
        comEngine.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(comEngine);
        iTop += 25;
        tDBName.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(tDBName);
        iTop += 25;
        tHost.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(tHost);        
        //bCreateDB.setBounds(iLeft + iWidth + 2, iTop, 20, 20); pDatabase.add(bCreateDB);
        //bCreateDB.setIcon(Images.cdb);
        //bCreateDB.setToolTipText("New Room Database");
        iTop += 25;
        tPort.setBounds(iLeft, iTop, iPortWidth, iHeight); pDatabase.add(tPort);
        iTop += 30;
        tUser.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(tUser);
        iTop += 25;
        tPass.setBounds(iLeft, iTop, iWidth, iHeight); pDatabase.add(tPass);
        iTop += 45;
        iLeft = 10;
        comEngine.setBackground(new Color(255, 255, 204));        
        lDBScheduler.setBounds(iLeft, iTop + 5, iWidth, iHeight);
        ChkOpt1.setText("When room is idle for ");
        ChkOpt2.setText("Daily at ");
        iTop += 30;
        ChkOpt1.setBounds(iLeft, iTop, iWidth, iHeight);
        iTop += 30;
        ChkOpt2.setBounds(iLeft, iTop, iWidth, iHeight);
        iTop = iTop - 27; iLeft = iLeft + iWidth ;  
        minuteSpinner.setBounds(iLeft, iTop, 50, iHeight);
        lMinute.setBounds(iLeft + 52, iTop, iWidth, iHeight); 
        iTop += 27;
        jSpinner.setBounds(iLeft, iTop, 65, iHeight);

        pDatabase.add(lDBScheduler);
        pDatabase.add(ChkOpt1);
        pDatabase.add(ChkOpt2);
        pDatabase.add(minuteSpinner);
        pDatabase.add(jSpinner);
        pDatabase.add(lMinute);
        
        pRooms.add(pDatabase);
        
        pBypass.setBounds(10, 265, 210, 110); pRooms.add(pBypass);
        cbBypass.setBounds(0, 10, 120, 21); pBypass.add(cbBypass);
        lIdle.setBounds(0, 38, 70, 16); pBypass.add(lIdle);
        tIdle.setBounds(67, 38, 37, 20); pBypass.add(tIdle);
        lMinutes.setBounds(110, 38, 60, 16); pBypass.add(lMinutes);
        
        /*lAdmins.setBounds(0, 25, 168, 16); pBypass.add(lAdmins);
        lAdmins.setForeground(Color.RED);
        tAdmins.setForeground(Color.RED);
        tAdmins.setBounds(0, 43, 190, 20); pBypass.add(tAdmins);*/
        
        lKey.setBounds(0, 68, 168, 16);pBypass.add(lKey);         
        tKey.setBounds(0, 86, 190, 20);pBypass.add(tKey);
        

        lName.setBounds(230, 10, 80, 16); pRooms.add(lName);        
        tName.setBounds(230, 25, 190, 20); pRooms.add(tName);
        bAdd.setBounds(10, 240, 85, 23); pRooms.add(bAdd);
        bRemove.setBounds(115, 240, 85, 23); pRooms.add(bRemove);          
        bRegister3.setBounds(iRegLeft, iRegTop, iRegWidth, iRegHeight); pRooms.add(bRegister3); 
        
        lAdmins.setForeground(Color.RED);
        tAdmins.setForeground(Color.RED);
        lAdmins.setBounds(230, 50, 168, 16); pRooms.add(lAdmins);
        tAdmins.setBounds(230, 65, 190, 20); pRooms.add(tAdmins);
        
        //lIdle.setBounds(230, 40, 80, 16); pRooms.add(lIdle);
        //tIdle.setBounds(311, 40, 70, 20); pRooms.add(tIdle);
        //lMinutes.setBounds(383, 40, 60, 16); pRooms.add(lMinutes); 
        //---------------------Rooms Tab---------------------------------------
        tpSettings.addTab("Rooms", pRooms);
        
        bRegister2.setBounds(iRegLeft, iRegTop, iRegWidth, iRegHeight); pConnection.add(bRegister2); 
        pConnection.add(cbLog); cbLog.setBounds(14, 390, 120, 20);
        pConnection.add(cbDebug); cbDebug.setBounds(150, 390, 150, 20);
        bRegister2.setToolTipText(lNote1.getText());
        
        tAuthorsContext.setToolTipText(lNote2.getText());
        lAuthorsContext.setToolTipText(lNote2.getText());
        tLoginContext.setToolTipText(lNote2.getText());
        lLoginContext.setToolTipText(lNote2.getText());
        
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(width, height));
        setLocation((screenSize.width - width)/2,(screenSize.height -height)/2);
        //setIconImage(Images.app.getImage());
        //--------------------------Login Management Tab------------------------
        tpSettings.addTab("Login Management", new loginManagement(vws));
        //--------------------------Listeners-----------------------------------
        
        //--------------------------Email Server Settings Tab------------------------
        tpSettings.addTab("Email Server Settings", new EmailServerSettings(vwsPreferences));

        
        tKey.addKeyListener(new KeyListener()
        {//Limit encryption key
            public void keyTyped(KeyEvent evt){}
            public void  keyPressed(KeyEvent evt){}
            public void  keyReleased(KeyEvent evt){
                if (tKey.getDocument().getLength() > 16)
                {
                    try
                    {
                        tKey.setText(tKey.getDocument().getText(0, 16));
                    }
                    catch(BadLocationException ble){}
                }
            }
        });
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });
        tLicense.addMouseMotionListener(new MouseMotionListener(){
            public void mouseDragged(MouseEvent e) {}
            public void mouseMoved(MouseEvent e) {
                setLicenseInfo();
            }
        });    
        cbBypass.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e){
                    bRegister3.setEnabled(true);
                    cbBypassStateChanged();
            }
        });    
        comEngine.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if (comEngine.getSelectedIndex() >= 0) 
                                bRegister3.setEnabled(true);
            }
        });
        comSecurity.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){              
            }
        });
        
        
        bClose.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        bBrowse.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                browseLicenseFile();
            }
        });
        comScheme.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                 if (comScheme.getSelectedIndex() >= 0) 
                 {
                    bRegister2.setEnabled(true);
                    comSchemeStateChanged((String) comScheme.getSelectedItem());
                 }
            }
        });
        cbProxy.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent evt){
                bRegister2.setEnabled(true);
                cbProxyStateChanged();
            }
        });
        bAdd.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                synchronizeRooms();
                addRoom();
            }
        });
        bRemove.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                removeRoom();
            }
        });
        bRegister1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                registerGeneral();
                bRegister1.setEnabled(false);
            }
        });
        bRegister2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                bRegister2.setEnabled(false);
            }
        });
        bRegister3.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                registerRoom();
                bRegister3.setEnabled(false);
            }
        });

        lstRooms.addListSelectionListener(new ListSelectionListener(){
            public void valueChanged(ListSelectionEvent e){
                roomChanged();
            }
        });
        cbLog.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e){
                    bRegister2.setEnabled(true);
            }
        });    
        cbDebug.addChangeListener(new ChangeListener(){
            public void stateChanged(ChangeEvent e){
                    bRegister2.setEnabled(true);
            }
        });    
        comScheme.setSelectedIndex(-1);
        comEngine.setSelectedIndex(-1);
        comSecurity.setSelectedIndex(-1);
        setResizable(false);
    }
    
    private void addDocListeners()
    {
        tLicense.getDocument().addDocumentListener(new DocListener(bRegister1));
        // Email Server changes 30 Jan 2007 
        tNamedUsers.getDocument().addDocumentListener
                                                  (new DocListener(bRegister1));
        this.ess.txtEmailServer.getDocument().addDocumentListener(new DocListener(ess.txtEmailServer));
        this.ess.txtEmailServerPassword.getDocument().addDocumentListener(new DocListener(ess.txtEmailServer));
        this.ess.txtEmailServerPort.getDocument().addDocumentListener(new DocListener(ess.txtEmailServer));
        this.ess.txtEmailServerUsername.getDocument().addDocumentListener(new DocListener(ess.txtEmailServer));
        tComPort.getDocument().addDocumentListener(new DocListener(bRegister2));
        tDataPort.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tProxyHost.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tProxyPort.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tLoginHost.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tTree.getDocument().addDocumentListener(new DocListener(bRegister2));
        tAuthorsHost.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tLoginContext.getDocument().addDocumentListener
                                                  (new DocListener(bRegister2));
        tLDAPPort.getDocument().addDocumentListener(new DocListener(bRegister2));
        
        tAuthorsContext.getDocument().addDocumentListener(
                                                   new DocListener(bRegister2));
        tName.getDocument().addDocumentListener(new DocListener(bRegister3));
        tDBName.getDocument().addDocumentListener(new DocListener(bRegister3));
        tHost.getDocument().addDocumentListener(new DocListener(bRegister3));
        tPort.getDocument().addDocumentListener(new DocListener(bRegister3));
        tUser.getDocument().addDocumentListener(new DocListener(bRegister3));
        tPass.getDocument().addDocumentListener(new DocListener(bRegister3));
        tIdle.getDocument().addDocumentListener(new DocListener(bRegister3));
        tKey.getDocument().addDocumentListener(new DocListener(bRegister3));
        tAdmins.getDocument().addDocumentListener(new DocListener(bRegister3));
    }
    private void roomChanged()
    {
        clearRoomInfo();
        if (lstRooms.getSelectedIndices().length == 1)
        {
            getRoomInfo(lstRooms.getSelectedIndex());
            bRegister3.setEnabled(false);
            return;
        }
        clearRoomInfo();
        bRegister3.setEnabled(false);
    }
    private void cbBypassStateChanged()
    {
        if (cbBypass.getSelectedObjects() == null)
            switchGroups(true);
        else
            switchGroups(false);
    }
    private void switchGroups(boolean b)
    {
        tAdmins.setEnabled(b);
    }
    private void getRoomInfo(int i)
    {
        if ( vRoomInfo.size() == 0) return;
        RoomProperty rp = (RoomProperty) vRoomInfo.elementAt(i);
        tName.setText(rp.getName()); 
        comEngine.setSelectedItem(rp.getDatabaseEngineName());
        tDBName.setText(rp.getDatabaseName());
        tHost.setText(rp.getDatabaseHost());
        tPort.setValue(new Integer(rp.getDatabasePort()));
        tUser.setText(rp.getDatabaseUser());
        tPass.setText(rp.getDatabasePassword());
        cbBypass.setSelected(rp.getBypass());
        tAdmins.setText(rp.getAdmins());
        tIdle.setValue(new Integer(rp.getIdle()));
        String key = rp.getKey();
        if (key != null && key.length() > 0)
        {
            tKey.setText("************");
            tKey.setEnabled(false);
        }
        else
        {
            tKey.setEnabled(true);
        }
        try{
	        if (rp.getConnectionResetScheduler() == 0){
	        	ChkOpt1.setSelected(true);
	        	minuteModel.setValue(Integer.parseInt(rp.getSchedulerTime()));
	        }else{        	
	        	ChkOpt2.setSelected(true);
	        	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	        	Date date = timeFormat.parse(rp.getSchedulerTime());
	        	dateModel.setValue(date);
	        }
        }catch(Exception ex){
        	
        }

    }
    private void comSchemeStateChanged(String scheme)
    {
        if (scheme == null) return;
        if (scheme.equals(SSCHEME_NDS))
            switchTreeContext(true);
        else if(scheme.equals(SCHEME_LDAP_ADS) ||
        		scheme.equals(SCHEME_LDAP_NOVELL) ||
        		scheme.equals(SCHEME_LDAP_LINUX))
            switchtoLDAPFields();
        else
            switchTreeContext(false);
    }
    /*Added for LDAP SS. Valli*/
    private void switchtoLDAPFields()
    {
    	try{
	        tLoginContext.setEnabled(true);
	        tLoginHost.setEnabled(true);
	        tTree.setEnabled(true);
	        tLDAPPort.setEnabled(true);
	        lTree.setText("Directory");
	        tAuthorsHost.setEnabled(true);
	        lAuthorsHost.setText("Admin User");
        	lAuthorsContext.setText("Password");
	        tAuthorsContext.setVisible(false);
	        ldapPass.setEnabled(true);
	        ldapPass.setVisible(true);
	        comSecurity.setEnabled(true);
	        
	        tTree.setText(vws.getTree());
	        comSecurity.setSelectedItem(vws.getLdapSecurity());
	        ldapPass.setText(vws.getAdminPass());
	        tLoginContext.setText(vws.getLoginContext());
	        tLDAPPort.setText(vws.getLdapPort());
        	tLoginContext.setBackground(bgHighlightedColor);
	        tLoginHost.setBackground(bgHighlightedColor);
	        tTree.setBackground(bgHighlightedColor);
		    tLDAPPort.setBackground(bgHighlightedColor);
		    ldapPass.setBackground(bgHighlightedColor);
		    
		    lLoginContext.setEnabled(true);
		    lLoginHost.setEnabled(true);
		    lTree.setEnabled(true);
		    lLDAPPort.setEnabled(true);
		    lAuthorsContext.setEnabled(true);
		    lLDAPSecurity.setEnabled(true);	        
    	}catch(Exception e){}
    }
    private void setLicenseInfo()
    {
        License lic = null;
        try
        {
            lic = vws.getLicense();
        }
        catch(Exception e){}
        if (lic == null)
        {
            tLicense.setToolTipText("Invalid " + PRODUCT_NAME + " License");
            return;
        }
        String info = "<html><li>" + "Licensed for: " + lic.getCompanyName() +
                "&nbsp;&nbsp;&nbsp;&nbsp;" + 
                "  </li><li>" + "Server seats: " + 
                lic.getConsumed(Client.All_Client_Type) + " / " + lic.getServerSeats() +
                "  </li><li>" + "Desktop Concurrent seats: " + 
                lic.getConsumed(Client.Fat_Client_Type) + " / " + lic.getDesktopSeatsConcurrent() +  
                 "  </li><li>" + "Desktop Named seats: " + 
                lic.getConsumed(Client.NFat_Client_Type) + " / " +  lic.getDesktopSeatsNamed() + 
                "  </li><li>" + "WebTop Concurrent seats: " + 
                lic.getConsumed(Client.Web_Client_Type) + " / " + lic.getWebtopSeatsConcurrent() +//lic.getApplet1SeatsConcurrent() +  
                "  </li><li>" + "WebTop Named seats: " + 
                lic.getConsumed(Client.NWeb_Client_Type) + " / " + lic.getWebtopSeatsNamed() + //lic.getApplet1SeatsNamed() +
                "  </li><li>" + "WebLite Concurrent seats: " + 
                lic.getConsumed(Client.WebL_Client_Type) + " / " + lic.getWebliteSeatsConcurrent() +//lic.getApplet1SeatsConcurrent() +  
                "  </li><li>" + "WebLite Named seats: " + 
                lic.getConsumed(Client.NWebL_Client_Type) + " / " + lic.getWebliteSeatsNamed() +//lic.getApplet1SeatsNamed() +  
                
                "  </li><li>" + "Administration seats: " +
                lic.getConsumed(Client.Adm_Client_Type) + " / " +  lic.getAdminWiseSeats() +
                "  </li><li>" + "AIP seats: " +
                lic.getConsumed(Client.AIP_Client_Type) +  " / " + lic.getAipSeats() +
                "  </li><li>" + "EAS seats: " +
                lic.getConsumed(Client.EAS_Client_Type) +  " / " + lic.getEasSeats() +  
                "  </li><li>" + "Indexer seats: " +
                lic.getConsumed(Client.IXR_Client_Type) +  " / " + lic.getIndexerSeats() +  
                "  </li><li>" + "ARS seats: " +
                lic.getConsumed(Client.Ars_Client_Type) +  " / " + lic.getArsSeats() +  
                "  </li><li>" + "DWS seats: " +
                lic.getConsumed(Client.Drs_Client_Type) +  " / " + lic.getDrsSeats() +  

                //"  </li><li>" + "ARS seats: " +
                //lic.getConsumed(4) +  " / " + lic.getArsSeats() +  
              
                "  </li><li>" + "Others: " +
                lic.getConsumed(Client.Ewk_Client_Type) +  " / " + lic.getEworkSeats() + 
                "  </li><li>" + "SDK seats: " +
                lic.getConsumed(Client.sdk_Client_Type) +  " / " + lic.getSdkSeats() +  
                "  </li></html>";
        tLicense.setToolTipText(info);
    }
    private void getGeneral()
    {
         try
         {
             tLicense.setText(vws.getLicenseFile());
             tNamedUsers.setText(vws.getNamedUsers());
             tProfessionalNamedUsers.setText(vws.getProfessionalNamedUsers());
         }
         catch(Exception e){}
         bRegister1.setEnabled(false);
    }
    private void registerGeneral()
    {
         try
         {
              vws.setLicenseFile(tLicense.getText());
              vws.setNamedUsers(tNamedUsers.getText());
              // Email Server changes 30 Jan 2007
              //vws.setEmailServerName(this.txtEmailServer.getText());
              vws.setEmailServerName(this.ess.txtEmailServer.getText());
              vws.setEmailServerPort(this.ess.txtEmailServerPort.getText());
              vws.setEmailServerUsername(this.ess.txtEmailServerUsername.getText());
              /*
           	 * new JPasswordField().getText(); method is replaced with JPasswordField().getPassword(); 
           	 * as getText() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
           	 */
              vws.setEmailServerPassword(this.ess.txtEmailServerPassword.getText().trim()); //srikanth on 11 Nov 2015
              
              vws.setFromEmailId(this.ess.txtFromEmailId.getText());
         }
         catch(Exception e){}
    }
    private void switchTreeContext(boolean b)
    {
    	lTree.setText("Tree");
        lAuthorsHost.setText("Authors Host");
        lAuthorsContext.setText("Authors Context");
        if(!b)
        {
            tTree.setText("");
            tLoginContext.setText("");
            tLDAPPort.setText("");
        }
        tTree.setEnabled(b);
        tAuthorsContext.setVisible(true);
        tAuthorsContext.setEnabled(b);
        tLoginContext.setEnabled(b);
        tLDAPPort.setEnabled(b);
        tLoginHost.setEnabled(!b);
        tAuthorsHost.setEnabled(!b);
        ldapPass.setEnabled(false);
        ldapPass.setVisible(false);
        ldapPass.setText("");
        comSecurity.setEnabled(false);
        tLDAPPort.setEnabled(false);
        lLDAPSecurity.setEnabled(false);
        lLDAPPort.setEnabled(false);
        tLDAPPort.setBackground(Color.WHITE);
        if(b){
        	tTree.setBackground(bgHighlightedColor);
        	tAuthorsContext.setBackground(bgHighlightedColor);	
            tLoginContext.setBackground(bgHighlightedColor);            
            tLoginHost.setBackground(Color.WHITE);        
            tAuthorsHost.setBackground(Color.WHITE);
            
            lTree.setEnabled(true);
            lAuthorsContext.setEnabled(true);
            lLoginContext.setEnabled(true);
            lLoginHost.setEnabled(false);
            lAuthorsHost.setEnabled(false);


        }else{
        	tTree.setBackground(Color.WHITE);
        	tAuthorsContext.setBackground(Color.WHITE);	
            tLoginContext.setBackground(Color.WHITE);
            tLoginHost.setBackground(bgHighlightedColor);        
            tAuthorsHost.setBackground(bgHighlightedColor);
            lTree.setEnabled(false);
            lAuthorsContext.setEnabled(false);
            lLoginContext.setEnabled(false);
            lLoginHost.setEnabled(true);
            lAuthorsHost.setEnabled(true);                        
        }
    }
    private void clearRoomInfo()
    {
        tName.setText("");
        comEngine.setSelectedIndex(-1);
        tDBName.setText("");
        tHost.setText("");
        tPort.setValue(new Integer(0));
        tUser.setText("");
        tPass.setText("");
        cbBypass.setSelected(false);
        tAdmins.setText("");
        tIdle.setValue(new Integer(0));
        tKey.setText("");
    }
    private void addRoom()
    {
        String name = "Room_" + (vRooms.size()+1);
        vRooms.addElement(name);
        vRoomInfo.addElement(new RoomProperty(name));  
        lstRooms.setListData(vRooms);
        lstRooms.setSelectedValue(name, true);
        tName.setText(name);
        tName.grabFocus();
    }
    private void removeRoom()
    {
        if (!Util.Ask(this,  "Remove selected Room(s)?" , 
                                    ((JFrame) getParent()).getTitle())) return;
        /*
     	 * new JList.getSelectedValues(); method is replaced with new JList.getSelectedValuesList().toArray(); 
     	 * as JList.getSelectedValues() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
     	 */
        Object[] rooms = lstRooms.getSelectedValuesList().toArray();
        if (rooms == null) return;
        for (int i=0; i < rooms.length; i++)
        {
            String room = (String) rooms[i];
            try
            {
                vws.removeRoom(room);
            }
            catch(Exception e){}
        }
        synchronizeRooms();
    }
    private boolean compEmpty(JTextField comp, String msg)
    {
        if (comp.getText().equals(""))
        {
            Util.Msg(this, msg, MANAGER_APP); 
            comp.grabFocus(); 
            return true;
        }
        else
            return false;
    }
    private void registerRoom()
    {
        if (compEmpty(tName, "Room Name required.")) return;
        if (compEmpty(tDBName, "Database Name required.")) return;
        if (comEngine.getSelectedIndex() < 0)
        {
            Util.Msg(this, "Database Engine required.", MANAGER_APP); 
            comEngine.grabFocus(); 
            return;
        }
        if (compEmpty(tHost, "Database Host required.")) return;
        if (compEmpty(tPort, "Database Port required.")) return;
        if (compEmpty(tUser, "Database User required.")) return;
        if (compEmpty(tAdmins, "Administrators Group required.")) return;
        
        RoomProperty rp = new RoomProperty((String) tName.getText());
        
        rp.setDatabaseEngineName((String) comEngine.getSelectedItem());
        rp.setDatabaseName(tDBName.getText());
        rp.setDatabaseHost(tHost.getText());
        rp.setDatabasePort(((Number) tPort.getValue()).intValue());
        rp.setDatabaseUser(tUser.getText());
        rp.setDatabasePassword( new String(tPass.getPassword()));
        rp.setBypass(cbBypass.isSelected());
        rp.setAdmins(tAdmins.getText());
        rp.setIdle(((Number) tIdle.getValue()).intValue());
        rp.setKey(tKey.getText());
        try
        {
            rp.setEnabled(vws.getRoomState(rp.getName()));
        rp.setConnectionResetScheduler(ChkOpt1.isSelected()?0:1);
        if (ChkOpt1.isSelected())
        	rp.setSchedulerTime(minuteSpinner.getValue().toString());
        else{
        	Date dailyFreq = dateModel.getDate();
        	String dailyFrequency = new SimpleDateFormat("HH:mm:ss").format(dailyFreq);
        	rp.setSchedulerTime(dailyFrequency);
        }

            
            vws.removeRoom((String) lstRooms.getSelectedValue());
            vws.addRoom(rp);
        }
        catch(Exception e){}
        synchronizeRooms();
    }
    private void synchronizeRooms()
    {
        RoomProperty[] rooms = null;
        try
        {
            rooms = vws.getRegRooms();
        }
        catch(Exception e){}
        vRoomInfo.removeAllElements();
        vRooms.removeAllElements();
        pRRooms.removeAll();
        if (rooms != null && rooms.length > 0)
        {
            for (int i=0; i < rooms.length; i++)
            {
                vRooms.addElement(rooms[i].getName());
                vRoomInfo.addElement(rooms[i]);
                createRoomIcon(rooms[i]);
            }
            lstRooms.setListData(vRooms);
            if (vRooms.size() > 0) lstRooms.setSelectedIndex(0);
        }
    }
    /*SetSetting changed. Values will be stored in registry based on SSType selection. Valli 14 March 2007*/
    private void setSettings()
    {
        try
        {
        	// Email Server changes 30 Jan 2007
            vws.setLicenseFile(tLicense.getText());
            vws.setEmailServerName(this.ess.txtEmailServer.getText());
            vws.setFromEmailId(this.ess.txtFromEmailId.getText());
            vws.setEmailServerPort(this.ess.txtEmailServerPort.getText());
            vws.setEmailServerUsername(this.ess.txtEmailServerUsername.getText());
            /*
           	 * new JPasswordField().getText(); method is replaced with JPasswordField().getPassword(); 
           	 * as getText() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
           	 */
            vws.setEmailServerPassword(this.ess.txtEmailServerPassword.getText().trim()); //Srikanth on 10 Nov 2015
            vws.setComPort(Integer.parseInt(tComPort.getText()));
            vws.setDataPort(Integer.parseInt(tDataPort.getText()));
            vws.setProxy(cbProxy.getSelectedObjects() != null? true : false);
            vws.setProxyHost(tProxyHost.getText());
            String pPort = tProxyPort.getText();
            if (pPort.equals(""))
                vws.setProxyPort(0);
            else
                vws.setProxyPort(Integer.parseInt(pPort));
            vws.setSSType((String) comScheme.getSelectedItem());
            
           	if( vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_ADS) || 
       			vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_NOVELL)||
       			vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_LINUX) )
           	{
	            vws.setSSLoginHost(tLoginHost.getText());
	            vws.setSSAuthorsHost(tAuthorsHost.getText());
	            vws.setTree(tTree.getText());
	            vws.setLoginContext(tLoginContext.getText());
	            vws.setLdapPort(tLDAPPort.getText());
	            vws.setAdminPass(new String(ldapPass.getPassword()));
	            vws.setLdapSecurity((String)comSecurity.getSelectedItem ());
            }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_ADS)){
	            vws.setSSLoginHost(tLoginHost.getText());
	            vws.setSSAuthorsHost(tAuthorsHost.getText());
            }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_NDS)){
	            vws.setTree(tTree.getText());
	            vws.setLoginContext(tLoginContext.getText());
	            vws.setAuthorsContext(tAuthorsContext.getText());
            }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_SSH)){
	            vws.setSSLoginHost(tLoginHost.getText());
	            vws.setSSAuthorsHost(tAuthorsHost.getText());
            }
            
            vws.setLogInfo(cbLog.isSelected());
            vws.setDebugInfo(cbDebug.isSelected());
            
        }
        catch(Exception e)
        {
        }
    }
    /*GetSettings changed. Values will be retrived from registry based on SSType selection. Valli 14 March 2007*/
    private void getCurrentSettings()
    {
        try
        {
        	// Email Server changes 30 Jan 2007
        	this.ess.txtEmailServer.setText(vws.getEmailServerName());
        	this.ess.txtEmailServerPort.setText(vws.getEmailServerPort());
        	this.ess.txtEmailServerUsername.setText(vws.getEmailServerUsername());
        	this.ess.txtEmailServerPassword.setText(vws.getEmailServerPassword());
        	this.ess.txtFromEmailId.setText(vws.getFromEmailId());
            tLicense.setText(vws.getLicenseFile());
            tComPort.setValue(new Integer(vws.getComPort()));
            tDataPort.setValue(new Integer(vws.getDataPort()));
            cbProxy.setSelected(vws.getProxy());
            tProxyHost.setText(vws.getProxyHost());
            tProxyPort.setValue(new Integer(vws.getProxyPort()));
            comScheme.setSelectedItem(vws.getSSType());
            if( vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_ADS) || 
           			vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_NOVELL)||
           			vws.getSSType().equalsIgnoreCase(SCHEME_LDAP_LINUX) )
            {
            	tLoginHost.setText(vws.getSSLoginHost());
                tAuthorsHost.setText(vws.getSSAuthorsHost());
                tTree.setText(vws.getTree());
                comSecurity.setSelectedItem(vws.getLdapSecurity());
                ldapPass.setText(vws.getAdminPass());
                tLoginContext.setText(vws.getLoginContext());
                tLDAPPort.setText(vws.getLdapPort());
            }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_ADS)){
            	tLoginHost.setText(vws.getSSLoginHost());
                tAuthorsHost.setText(vws.getSSAuthorsHost());
            }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_SSH)){
            	tLoginHost.setText(vws.getSSLoginHost());
                tAuthorsHost.setText(vws.getSSAuthorsHost());
            }else if(vws.getSSType().equalsIgnoreCase(SSCHEME_NDS)){
                tTree.setText(vws.getTree());
                tLoginContext.setText(vws.getLoginContext());
                tAuthorsContext.setText(vws.getAuthorsContext());
            }
            cbLog.setSelected(vws.getLogInfo());
            cbDebug.setSelected(vws.getDebugInfo());
            cbProxyStateChanged();
        }
        catch(RemoteException re)
        {
        }
        bRegister2.setEnabled(false);
    }
    private void createRoomIcon(RoomProperty rp)
    {
        final JLabel room = new JLabel();
        room.setIcon(Images.ron);
        room.setText(rp.getName());
        room.setDisabledIcon(Images.rof);
        room.setIconTextGap(10);
        room.setHorizontalTextPosition(SwingConstants.CENTER);
        room.setVerticalTextPosition(SwingConstants.BOTTOM);
        room.addMouseListener(new MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                 if (evt.getClickCount() > 1) 
                     switchRoomState(room);
            }
        });
        boolean state = false;
        try
        {
            state = vws.getRoomState(room.getText());
        }
        catch(Exception e){}
        room.setEnabled(state);
        pRRooms.add(room);
    }
    private void switchRoomState(JLabel room)
    {
        String rname = " room '" + room.getText() + "' ";
        String question = (room.isEnabled()? "Take" + rname + " Offline?" :
                                             "Bring" + rname + "Online?");
        if ( Util.Ask(this,  question , ((JFrame) getParent()).getTitle()))
        {
            room.setEnabled(!room.isEnabled());
            try
            {
                vws.setRoomState(room.getText(), room.isEnabled());
            }
            catch(Exception e) {}
        }
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
            switchProxyData(true);
        else
            switchProxyData(false);
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    private void closeDialog() 
    {
        setVisible(false);
        dispose();
    }
    private void readEnv() 
    {
    	/* Issue/Purpose:	Changing color code of about dialog
    	* C.Shanmugavalli 		21 Feb 2007
    	*/
    	//String buildVersion = Util.getBuildVersion();
        String aboutText = 
        "<html><body><div align='left'><table width=500 height=175>" +
        
        "<tr><td width=420 height=35 bgcolor=#12468E VALIGN='TOP'>" +
        //"<p align=center><font face=Tahoma size=3 color=#003399>" +        
        "<p align=center><font face=Tahoma size=3 color=#FFFFFF>" +
        "<b>" + PRODUCT_NAME + "<sup><font size=2>�</font>"+ 
        //"</sup> Server Settings (5.6.1.323) </b></p></td></tr>" +
        //"</sup> Server Settings (6.0.0.1500) </b></p></td></tr>" +
        "</sup> Server Settings ("+ connectorManager.getString("ViewWise.Product.Version")+") </b></p></td></tr>" +
        //"</sup> Server Settings ("+buildVersion+") </b></p></td></tr>" +
   
        //"<tr><td width=420 height=140 bgcolor=#336699 VALIGN='TOP'>" +        
        "<tr><td width=420 height=140 bgcolor=#12468E VALIGN='TOP'>" +
        "<font face=Tahoma size=3 color=#FFFFFF>" + 
        "<p style='margin-top: 8; margin-bottom: 1; margin-left: 25'>" +
        "Operating System: " + Util.getEnvOSName() + " " + Util.getEnvOSVer() + 
        " running on " + Util.getEnvOSArch() + "</p>" + 
        "<p style='margin-top: 1; margin-bottom: 1; margin-left: 25'>" +
        "User Directory: " + Util.getEnvUserDir() + "</p>" + 
        "<p style='margin-top: 1; margin-bottom: 1; margin-left: 25'>" +
        "Virtual Machine: " + Util.getEnvVMName() + "</p>" + 
        "<p style='margin-top: 1; margin-bottom: 1; margin-left: 25'>" +
        "Java version: " + Util.getEnvJavaVer() + "</p>" + 
        "<p style='margin-top: 1; margin-bottom: 1; margin-left: 25'>" +
        "Java Vendor: " + Util.getEnvJavaVendor() + "</p>" + "<p></p>" + 
        "</font></td></tr></table></div></body></html>";
        
        epLogo.setText(aboutText);
    }
    private void browseLicenseFile()
    {
        String dFolder = VWSUtil.getHome() + Util.pathSep + "License";
        JFileChooser fc = new JFileChooser(new File(dFolder));
        fc.setFileFilter(new LicenseFilter());
        //fc.showDialog(this, "Register");
        fc.showOpenDialog(this);
        File lic = fc.getSelectedFile();
        if (lic != null) tLicense.setText(lic.getPath());
    }
    
    private class LicenseFilter extends javax.swing.filechooser.FileFilter 
    {
        public boolean accept(File f) 
        {
            if (f.isDirectory()) return true;
            String extension = getExtension(f);
            if (extension != null && extension.equals("lic")) return true;
            return false;
        }
        private String getExtension(File f) 
        {
            String ext = null;
            String s = f.getName();
            int i = s.lastIndexOf('.');

            if (i > 0 &&  i < s.length() - 1) 
                ext = s.substring(i+1).toLowerCase();
            return ext;
        }
        public String getDescription() 
        {
            return "License Files";
        }
    }
    public static void main(String[] args) {
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		}catch(Exception ex){}
		/*
     	 * new frame.show(); method is replaced with new frame.setVisible(true); 
     	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
     	 */
    	new VWSSettings(new JFrame(), true, null).setVisible(true);
	}
}
