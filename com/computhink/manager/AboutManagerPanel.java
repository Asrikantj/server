/*
 * AboutManager.java
 *
 * Created on November 13, 2003, 10:58 AM
 */

package com.computhink.manager;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLEditorKit;

import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.vwc.resource.ResourceManager;
import com.computhink.manager.image.Images;

public class AboutManagerPanel extends JPanel //implements ActionListener 
{
    private JEditorPane tpAbout;
    private JLabel lLogo;
    private boolean fromServerManager = false;
    private static final String WIN_PATH = "rundll32";
    private static final String WIN_FLAG = "url.dll,FileProtocolHandler";
    private static final String LINUX_PATH = "knoqueror";
    private final ResourceManager connectorManager = ResourceManager.getDefaultManager();
    int mode = 2;
    public AboutManagerPanel(boolean fromServerManager) 
    {
	this.fromServerManager = fromServerManager;
        new Images();
        initComponents();
        readEnv();
    }
    public AboutManagerPanel() 
    {
	this(2);
    }
    public AboutManagerPanel(int mode) 
    {
        new Images();
        this.mode = mode;
        initComponents();
        readEnv();
    }

    private void initComponents() 
    {
        lLogo = new JLabel();
        if(mode == 0 || mode == 1){
        	lLogo.setIcon(AboutManager.webClientAboutIcon);
        }else{
        	lLogo.setIcon(Images.logo);
        }
        tpAbout = new JEditorPane();
        tpAbout.setEditorKit(new HTMLEditorKit()); 
        tpAbout.setEditable(false);
        tpAbout.setBorder(null);
        setLayout(null);
       
        //add(lLogo); lLogo.setBounds(0,0,171, 365);
        lLogo.setBorder(null);
        if (fromServerManager){
            add(tpAbout); tpAbout.setBounds(0,0,600,810);
        }
        else {
            add(lLogo); lLogo.setBounds(0,0,171, 365);
            lLogo.setBorder(null);
            add(tpAbout); tpAbout.setBounds(171,0,420,338);                    
        }
            
        tpAbout.addHyperlinkListener(new HyperlinkListener(){
            public void hyperlinkUpdate(HyperlinkEvent e){
                String cmd = "";
  	          if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
                      try
                      {
                          String url = "www.computhink.com";
                          if (Util.getEnvOSName().startsWith("Window"))
                          {
                              cmd = WIN_PATH + " " + WIN_FLAG + " " + url;
                              Process p = Runtime.getRuntime().exec(cmd);
                              return;
                          }
                          if (Util.getEnvOSName().startsWith("Linux"))
                          {
                              
                              cmd = LINUX_PATH + " " + url;
                              Process p = Runtime.getRuntime().exec(cmd);
                              //int exitCode = p.waitFor();
                              return;
                          }
                          if (Util.getEnvOSName().startsWith("netware"))
                              return;
                      }
                      catch(Exception ioe){}
            }
        });
        //-------------------------Listeners------------------------------------
    }
    private void readEnv() 
    {
    	/* Issue/Purpose:	Changing color code of about dialog
    	* C.Shanmugavalli 		21 Feb 2007
    	*/
        //String buildVersion = Util.getBuildVersion();
    	String superScriptText = "<sup><font size=2>�</font></sup>";
    	String aboutText = "<html><body><div align='left'>" +
        "<table width=" + (fromServerManager?"510":"420") + " height=810  cellspacing=0  bgcolor=#556274>" +
        //"<tr><td width=420 height=58 bgcolor=#FF9900>" +
        (fromServerManager?"<tr><td width=420 height=58></td></tr>":"") +
        "<tr><td width=420 height=58>" +
        //"<p align=center><font face=Tahoma color=#003399>" +
        "<p align=center><font face=Tahoma color=#FFFFFF>" +
        "<b>"+Constants.PRODUCT_NAME+ superScriptText + (mode == 2?" Server Manager":((mode == 0)?" Online - Web Top":" Online - Web Lite"))+ "("+ connectorManager.getString("ViewWise.Product.Version")+") </b></p></td></tr>" +
        //"</sup> Server Manager("+buildVersion+") </b></p></td></tr>" +
        
        //"<tr><td width=420 height=25 align=left valign=top bgcolor=#336699>" +
        "<tr><td width=420 height=160 align=left valign=top bgcolor=#556274>" +
        "<font face=Tahoma size=3  color=#FFFFFF>" + "<p></p>" +
        "&nbsp;&nbsp;&nbsp;Operating System: " + Util.getEnvOSName() + " " +
        Util.getEnvOSVer() + " running on " + Util.getEnvOSArch() +
        "<p style='margin-top: 1; margin-bottom: 1'>" +
        "&nbsp;&nbsp;&nbsp;User Directory: " + Util.getEnvUserDir() + "</p>" + 
        "<p style='margin-top: 1; margin-bottom: 1'>" +
        "&nbsp;&nbsp;&nbsp;Virtual Machine: " + Util.getEnvVMName() + "</p>" + 
        "<p style='margin-top: 1; margin-bottom: 1'>" +
        "&nbsp;&nbsp;&nbsp;Java version: " + Util.getEnvJavaVer() + "</p>" + 
        "<p style='margin-top: 1; margin-bottom: 1'>" +
        "&nbsp;&nbsp;&nbsp;Java Vendor: " + Util.getEnvJavaVendor() +
        "</font></td></tr>" +
        
        "<tr><td width=420 height=25 align=left valign=top bgcolor=#556274>" +
        "<p style='word-spacing: 1; margin-top: 1; margin-bottom: 1'>" +
        "<font face=Tahoma size=2 color=#FFFFFF>" +
        "Copyright Computhink Inc. 1994-2020. All rights reserved." +
        "&nbsp;&nbsp;&nbsp;<a href='http://www.computhink.com'>" +
        "www.computhink.com</font></p>" +
        "</a></font></td></tr>" +
        
        "<tr><td width=420 height=250 align=left valign=top bgcolor=#556274>" +
        "<p style='word-spacing: 1; margin-top: 1; margin-bottom: 1'>" +
        //"<p style='margin-left: 3; margin-right: 3'" + 
        "<font face=Tahoma size=2 color=#FFFFFF>" +
        "Warning: This computer program is protected by copyright" +
        "law and international treaties. Unauthorized reproduction and " +
        "distribution of this program, or any portion of it, may result in " +
        "severe civil and criminal penalties, and will be prosecuted to the " +
        "maximum extent allowed under the law. </font></td>" +
        "</tr></table></div></body></html>";
        tpAbout.setText(aboutText);
    }
    public static void main(String args[]) {
    	new AboutManagerPanel().setVisible(true);
    }
}
