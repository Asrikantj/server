/*
 *
 * Created on Nov 18 2011
 */
package com.computhink.manager;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

import com.computhink.resource.ResourceManager;
import com.computhink.vws.csserver.CSS;
import com.computhink.vws.csserver.CSSConstants;
import com.computhink.vws.csserver.CSSPreferences;
import com.computhink.vws.server.mail.VWMail;
import com.computhink.common.CIServer;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;


public class CSSettingsPanel extends VWPanel implements ManagerConstants, CSSConstants
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton bOK, bTestMail;
	private JPanel pPorts, pVWS, pEmail;

	private JLabel lDataPort, lComPort, lVWSPort, lVWSHost,
	lEmailServer, lEmailServerPort, lEmailServerUsername, lEmailServerPassword, lFromEmailId,
	lPolling, lFileSizeLimit, lNotificationRetries;

	private JTextField tComPort, tDataPort, tVWSHost, tVWSPort, 
	tEmailServer, tEmailServerPort, tEmailServerUsername, tEmailServerPassword, tFromEmailId,
	tPolling, tFileSizeLimit, tNotificationRetries;

	private JCheckBox cbLog;
	private JCheckBox chkEnableSSL;
	
	private static final int height = 510;
	private static ResourceManager resourceManager=null;

	private CSS css;
	private ServerSchema vwsSchema;


	public CSSettingsPanel(){
		super();
		initComponents();
		setBackground(new Color(238, 242, 244));
	}
	public CSSettingsPanel(CIServer server) 
	{
		super();
		this.css = (CSS) server;
		initComponents();
		getCurrentSettings();
	}
	private void initComponents() 
	{
		 resourceManager=ResourceManager.getDefaultManager();
		pPorts = new VWPanel();
		pVWS = new VWPanel();
		pEmail = new VWPanel();
		cbLog = new JCheckBox(resourceManager.getString("VNSChk.LogInformation"));
		bOK = new JButton(resourceManager.getString("VNSBtn.Apply"));
		bTestMail = new JButton(resourceManager.getString("VNSBtn.TestMail"));
		chkEnableSSL = new JCheckBox(resourceManager.getString("VNSChk.SSLEnable"));
		tComPort = new JTextField();
		tDataPort = new JTextField();
		tVWSHost = new JTextField();
		tVWSPort = new JTextField();
		tEmailServer = new JTextField();
		tEmailServerPort = new JTextField();
		tEmailServerUsername = new JTextField();
		tEmailServerPassword = new JPasswordField();
		tFromEmailId = new JTextField();
		tPolling = new JTextField();
		tFileSizeLimit = new JTextField();
		tNotificationRetries = new JTextField();

		lVWSHost = new JLabel(resourceManager.getString("VNSLbl.VWSHost"));
		lVWSPort = new JLabel(resourceManager.getString("VNSLbl.VWSComPort"));
		lComPort = new JLabel(resourceManager.getString("VNSLbl.ComPort"));
		lDataPort = new JLabel(resourceManager.getString("VNSLbl.Dataport"));
		lEmailServer = new JLabel(resourceManager.getString("VNSLbl.EmailServ"));
		lEmailServerPort = new JLabel(resourceManager.getString("VNSLbl.EmailServPort"));
		lEmailServerUsername=new JLabel(resourceManager.getString("VNSLbl.Username"));
		lEmailServerPassword=new JLabel(resourceManager.getString("VNSLbl.Password"));
		lFromEmailId = new JLabel(resourceManager.getString("VNSLbl.FromEmailId"));
		lPolling = new JLabel(resourceManager.getString("VNSLbl.Polling"));
		lFileSizeLimit = new JLabel(resourceManager.getString("VNSLbl.FileSize"));
		lNotificationRetries = new JLabel(resourceManager.getString("CSSLbl.NotifiRetries"));

		setLayout(null);
		cbLog.setBackground(this.getBackground());
		chkEnableSSL.setBackground(this.getBackground());
		bOK.setBackground(this.getBackground());
		bTestMail.setBackground(this.getBackground());
		
		pPorts.setLayout(null);
		pPorts.setBorder(new TitledBorder(resourceManager.getString("VNSTitleBorder.ServPorts")));
		pPorts.setFocusable(false); add(pPorts);
		pPorts.setBounds(20, 10, 470, 70);
		pPorts.add(lComPort); lComPort.setBounds(20, 30, 120, 16);
		pPorts.add(lDataPort); lDataPort.setBounds(290, 30, 60, 16);
		pPorts.add(tComPort); tComPort.setBounds(160, 30, 50, 20);
		pPorts.add(tDataPort); tDataPort.setBounds(370, 30, 50, 20);

		/*pProxy.setLayout(null);
        pProxy.setBorder(new TitledBorder("         Use Http Proxy"));
        pProxy.setFocusable(false);  
        add(pProxy);
        pProxy.setBounds(20, 110, 470, 80);
        pProxy.add(lProxyHost); lProxyHost.setBounds(30, 30, 70, 16);
        pProxy.add(lProxyPort); lProxyPort.setBounds(325, 30, 30, 16);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 21);
        pProxy.add(tProxyHost); tProxyHost.setBounds(110, 30, 110, 20);
        pProxy.add(tProxyPort); tProxyPort.setBounds(370, 30, 50, 20);*/

		pVWS.setLayout(null);
		pVWS.setBorder(new TitledBorder(PRODUCT_NAME +" "+ resourceManager.getString("VNSTitleBorder.Server")));
		pVWS.setFocusable(false); 
		add(pVWS);
		pVWS.setBounds(20, 90, 470, 70);
		pVWS.add(lVWSHost); lVWSHost.setBounds(20, 30, 70, 16);
		pVWS.add(lVWSPort); lVWSPort.setBounds(240, 30, 120, 16);
		pVWS.add(tVWSHost); tVWSHost.setBounds(60, 30, 110, 20);
		pVWS.add(tVWSPort); tVWSPort.setBounds(370, 30, 50, 20);

		pEmail.setLayout(null);
		pEmail.setBorder(new TitledBorder(resourceManager.getString("VNSTitleBorder.EmailServ")));
		pEmail.setFocusable(false);
		add(pEmail);
		pEmail.setBounds(20, 170, 470, 275);
		int x = 20, intendX = 120, y = 26, width = 120, lHeight = 16, tHeight = 20, gap = 5;  
		pEmail.add(lEmailServer); lEmailServer.setBounds(x, y, width, lHeight);
		pEmail.add(lEmailServerPort); lEmailServerPort.setBounds(300, y, width, lHeight);
		y = y + 20;
		pEmail.add(tEmailServer); tEmailServer.setBounds(x, y, width+100, tHeight);
		pEmail.add(tEmailServerPort); tEmailServerPort.setBounds(300, y, 50, tHeight);
		y = y + 20 + gap;
		pEmail.add(lEmailServerUsername); lEmailServerUsername.setBounds(x, y, width, lHeight);
		pEmail.add(lEmailServerPassword); lEmailServerPassword.setBounds(300, y, width, lHeight);
		y = y + 20;
		pEmail.add(tEmailServerUsername); tEmailServerUsername.setBounds(x, y, 220, tHeight);
		pEmail.add(tEmailServerPassword); tEmailServerPassword.setBounds(300, y, width+30, tHeight);
		y = y + 20 + gap; 
		pEmail.add(lFromEmailId); lFromEmailId.setBounds(x, y, width, lHeight);
		y = y + 20; 
		pEmail.add(tFromEmailId); tFromEmailId.setBounds(x, y, 220, tHeight);
		gap = gap + 10;
		y = y + 20 + gap;
		pEmail.add(lPolling); lPolling.setBounds(x, y, width-20, lHeight);
		pEmail.add(lFileSizeLimit); lFileSizeLimit.setBounds(intendX+80, y, width, lHeight);
		y = y + 20;
		pEmail.add(tPolling); tPolling.setBounds(x, y, width-60, tHeight);
		pEmail.add(tFileSizeLimit); tFileSizeLimit.setBounds(intendX+80, y, width-40, tHeight);
		y = y + 20 + gap;
		pEmail.add(lNotificationRetries); lNotificationRetries.setBounds(x, y, width, lHeight);
		y = y + 20;
		pEmail.add(tNotificationRetries); tNotificationRetries.setBounds(x, y, width-90, tHeight);

		add(bOK); bOK.setBounds(400, 455, 90, 23);
		add(bTestMail); bTestMail.setBounds(300, 455, 90, 23);
		add(cbLog); cbLog.setBounds(20, 455, 130, 20);
		add(chkEnableSSL); chkEnableSSL.setBounds(150, 455, 100, 20);
		chkEnableSSL.setVisible(true);
		
		
		bOK.setToolTipText(resourceManager.getString("VNSToolTip.Apply"));//lNote.getText()
		bTestMail.setToolTipText(resourceManager.getString("VNSToolTip.TestMail"));

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(new Dimension(width, height));
		setLocation((screenSize.width - width)/2,(screenSize.height-height)/2);
		//-------------Listeners------------------------------------------------
		bOK.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				setSettings();
			}
		});
		bTestMail.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				try{
					String message =resourceManager.getString("CSSMail.Message");
					String subject = resourceManager.getString("VNSMail.Subject1") + PRODUCT_NAME + " -"+ resourceManager.getString("CSSMail.ContentSentinel");
					VWMail.sendTestMail(tEmailServer.getText().trim(), tEmailServerPort.getText().trim(), tFromEmailId.getText().trim(), tFromEmailId.getText().trim(),tEmailServerUsername.getText(),tEmailServerPassword.getText(),"Notification",subject.trim(), message.trim());
				}catch(Exception ex){
				}
			}
		});
	}
	private void setSettings()
	{
		try
		{
			css.setComPort(Integer.parseInt(tComPort.getText()));
			css.setDataPort(Integer.parseInt(tDataPort.getText()));
			vwsSchema.address = tVWSHost.getText();
			vwsSchema.comport = Util.to_Number(tVWSPort.getText());
			css.setVWS(vwsSchema);
			css.setLogInfo(cbLog.isSelected());
			CSSPreferences.setSSL(chkEnableSSL.isSelected());
			css.setEmailServerName(tEmailServer.getText());
			css.setFromEmailId(tFromEmailId.getText());
			css.setEmailServerPort(tEmailServerPort.getText());
			css.setEmailServerUsername(tEmailServerUsername.getText());
			css.setEmailServerPassword(tEmailServerPassword.getText());
			 
			String pollingTime = tPolling.getText();
            if(pollingTime != null && pollingTime.trim().equalsIgnoreCase("0")){
            	Util.Msg(this, resourceManager.getString("VNSMSG.PollingTime"),resourceManager.getString( "VNSMSGTitle"));
            }else{
            	css.setCSPollingTime(pollingTime);
            }
            css.setCSFileSizeLimit(tFileSizeLimit.getText());
        	css.setCSRetries(tNotificationRetries.getText());
        	
		}
		catch(Exception e) {}
	}
	private void getCurrentSettings()
	{
		try
		{
			tComPort.setText(String.valueOf(css.getComPort()));
			tDataPort.setText(String.valueOf(css.getDataPort()));
			vwsSchema = css.getVWS();
			tVWSHost.setText(vwsSchema.address);
			int vPort = vwsSchema.comport;
			if (vPort > 0)
				tVWSPort.setText(String.valueOf(vPort));
			else
				tVWSPort.setText("");

			cbLog.setSelected(css.getLogInfo());
			
			//Email server settings.
			tEmailServer.setText(CSSPreferences.getEmailServerName());
			chkEnableSSL.setSelected(CSSPreferences.getSSL());
			tEmailServerPort.setText(CSSPreferences.getEmailServerPort());
			tEmailServerUsername.setText(CSSPreferences.getEmailServerUsername());
			tEmailServerPassword.setText(CSSPreferences.getEmailServerPassword());
			tFromEmailId.setText(CSSPreferences.getFromEmailId());
			
			tPolling.setText(String.valueOf(css.getCSPollingTime()));
			tFileSizeLimit.setText(css.getCSFileSizeLimit());
			tNotificationRetries.setText(css.getCSRetries());
		}catch(RemoteException re){}
		catch(Exception re){}
	}
}
