/*
 * ServerSettings.java
 *
 * Created on November 13, 2003, 11:38 AM
 */

package com.computhink.manager;

import javax.swing.*;
import java.awt.*;
import javax.swing.border.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.rmi.*;

import com.computhink.common.*;
/**
 *
 * @author  Administrator
 */
public class ServerSettingsDialog extends JDialog 
{
    private JTextField tProxyPort;
    private JPanel pServe;
    private JPanel pProxy;
    private JLabel lDataPort;
    private JLabel lPort;
    private JLabel lComPort;
    private JTextField tComPort;
    private JLabel lSSettings;
    private JTextField tDataPort;
    private JLabel lHost;
    private JTextField tProxyHost;
    private JButton bCancel;
    private JButton bOK;
    private JCheckBox cbProxy;
    
    private CIServer server;

    public ServerSettingsDialog(JFrame parent, boolean modal, CIServer server) 
    {
        super(parent, modal);
        this.server = server;
        initComponents();
        getSettings();
    }
    private void initComponents() 
    {
        pServe = new JPanel();
        pProxy = new JPanel();
        tDataPort = new JTextField();
        tComPort = new JTextField();
        tProxyHost = new JTextField();
        tProxyPort = new JTextField();
        lSSettings = new JLabel("Server Ports");
        lDataPort = new JLabel("Data Port:");
        lComPort = new JLabel("Communication Port:");
        cbProxy = new JCheckBox("Use Http Proxy");
        lPort = new JLabel("Port:");
        lHost = new JLabel("Host:");
        bOK = new JButton("OK");
        bCancel = new JButton("Cancel");

        getContentPane().setLayout(null);
        setTitle("Server Settings");
        setResizable(false);
        
        lSSettings.setBounds(40, 12, 90, 16); getContentPane().add(lSSettings);

        pServe.setLayout(null);
        pServe.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
        tDataPort.setBounds(160, 60, 50, 20); pServe.add(tDataPort);
        tComPort.setBounds(160, 20, 50, 20); pServe.add(tComPort);
        lDataPort.setBounds(20, 60, 70, 16); pServe.add(lDataPort);
        lComPort.setBounds(20, 20, 140, 16); pServe.add(lComPort);
        pServe.setBounds(30, 20, 340, 100); getContentPane().add(pServe);

        cbProxy.setBounds(40, 140, 109, 24); getContentPane().add(cbProxy);
        
        pProxy.setLayout(null);
        pProxy.setBorder(new SoftBevelBorder(BevelBorder.RAISED));
        tProxyHost.setBounds(80, 30, 160, 20); pProxy.add(tProxyHost);
        tProxyPort.setBounds(80, 70, 50, 20);  pProxy.add(tProxyPort);
        lPort.setBounds(20, 70, 70, 16); pProxy.add(lPort);
        lHost.setBounds(20, 30, 70, 16); pProxy.add(lHost);
        pProxy.setBounds(30, 150, 340, 100);getContentPane().add(pProxy);

        bCancel.setBounds(290, 270, 80, 26); getContentPane().add(bCancel);
        bOK.setBounds(200, 270, 80, 26); getContentPane().add(bOK);

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(400, 340));
        setLocation((screenSize.width-400)/2,(screenSize.height-315)/2);
        //-------------Listeners------------------------------------------------
        addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });
        bOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
        //----------------------------------------------------------------------
    }
    private void closeDialog() 
    {
        setVisible(false);
        dispose();
    }
    private void getSettings()
    {
        try
        {
            tComPort.setText(String.valueOf(server.getComPort()));
            tDataPort.setText(String.valueOf(server.getDataPort()));
            cbProxy.setSelected(server.getProxy());
            tProxyHost.setText(server.getProxyHost());
            int pPort = server.getProxyPort();
            if (pPort > 0)
                tProxyPort.setText(String.valueOf(pPort));
            else
                tProxyPort.setText("");
            cbProxyStateChanged();
        }
        catch(RemoteException re)
        {
        }
    }
    private void setSettings()
    {
        try
        {
            server.setComPort(Integer.parseInt(tComPort.getText()));
            server.setDataPort(Integer.parseInt(tDataPort.getText()));
            server.setProxy(cbProxy.getSelectedObjects() != null? true : false);
            server.setProxyHost(tProxyHost.getText());
            String pPort = tProxyPort.getText();
            if (pPort.equals(""))
                server.setProxyPort(0);
            else
                server.setProxyPort(Integer.parseInt(pPort));
        }
        catch(RemoteException re)
        {
        }
        catch(NumberFormatException nfe)
        {
        }
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }

   
}
