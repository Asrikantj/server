/*
 * Created on May 12, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.ars.server;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;
/**
 * @author shanmugavalli.c
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ARSConstants extends Constants {
	
	public static final String ARS_PREF_ROOT  = "/computhink/" + PRODUCT_NAME.toLowerCase() + "/ars/server";
	public static final String ARS_CLASSPATH = "com/computhink/ars/image/images/ars.gif";
	public static final String ARS_JAR = "/lib/ViewWise.jar!";
	public static final String SETTINGS =ResourceManager.getDefaultManager().getString("CVProduct.Name")+ " Retention Server Settings";
	
	public static final int DEFAULT_DATA_PORT = 5001;
	public static final int DEFAULT_COM_PORT = 5000;
	public static final int DEFAULT_PROXY_PORT = 8080;
	public static final int DEFAULT_SCHEDULARSTART = 0;
	public static final String DEFAULT_DAILYFREQUENCY = "6:01:01";
	public static final String DEFAULT_SNOOZETIME = "5";
		
	public static final int DEFAULT_LOG_LINES = 100;
	public static final String ERROR_LOG_FILE = "ARSErrors.log";
	public static final String LOG_FILE_PREFIX = "ARSLog-";
	public static final String LOG_FOLDER = "log";
	public static final String arsAdminUser = "ARSAdmin";
	
	
	//public static final String[] ReportColumnNames = {"Document Name","Location","Retention Period","Retention Period StartsOn","Disposal Action",
	//        "Move Location","User Name"};
	//public static final String[] ColumnWidths = {"20","30","10","10","30","20","20"};
	public static final String[] ReportColumnNames = {
		ResourceManager.getDefaultManager().getString("ReportCol.DocID"), 
		ResourceManager.getDefaultManager().getString("ReportCol.DocName"), 
		ResourceManager.getDefaultManager().getString("ReportCol.DocLoc"),
		ResourceManager.getDefaultManager().getString("ReportCol.RetentionName"),
		ResourceManager.getDefaultManager().getString("ReportCol.RetentionCriteria"),
		ResourceManager.getDefaultManager().getString("ReportCol.UserName")};

	public static final String[] ColumnWidths = {"7","19","28","19","15","15"};
	public static String LBL_ARS_NAME=ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ResourceManager.getDefaultManager().getString("ReportLbl.CycleReport");
	public static String DUMMY_SERVERNAME="SystemSrv";

}
