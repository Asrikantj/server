/*
 * Created on May 24, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.ars.server;

import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.VWClient;

/**
 * @author shanmugavalli.c
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Worker extends Thread implements ViewWiseErrors//TimerTask
{

    private static boolean shutDownRequest = false;
    private static boolean snoozing = false;
    public String roomName;
    private int snooze;
    public int sessionID;
    VWClient vwClient;
    private int schedulerStartInfo;

	public Worker(VWClient vwClient, String roomName, int snooze, int schedulerStartInfo, int sessionID)
    {
		this.vwClient = vwClient;
		this.snooze = snooze;
		this.sessionID = sessionID;
        this.roomName = roomName;
        this.schedulerStartInfo = schedulerStartInfo;
    }

	public void run()
    {
	  	 while (!shutDownRequest)
         {
	    	if(schedulerStartInfo == 0)
	    	{
	    		ARSLog.add("Processing retention documents of the room: '"+roomName+"'");
	    		ARSProcessDocs arsProcessDocs = new ARSProcessDocs();
	    		//arsProcessDocs.getReportDocs(vwClient, this.sessionID,this.roomName);
	    		arsProcessDocs.sendEmailNotify(vwClient, sessionID, roomName);
	    		arsProcessDocs.getRetentionDocs(vwClient, this.sessionID,this.roomName);
	            snoozing = true;
	            try
	            {
	                sleep(snooze);
	            }
	            catch(Exception e){}
	            snoozing = false;
	    		}
         }
	}
	 public void shutDown()
	    {
	 		//ARSLog.add("coming here to shut down");
	        shutDownRequest = true;
	        //try
			//{
	        	//Thread.currentThread().destroy();
	        	//interrupt();
	        	//join();
	        //}catch(Exception e){
	        //}
	    }
}
