package com.computhink.ars.server;

public class MessageDataBean {
	String documentId = null;
	String docName = null;
	String docLocation = null;
	String retentionName = null;
	String retCriteriaCount = null;
	String creator = null;
	String sendMail = null;
	String mailIds = null;
	String indicesCondition = null;

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getDocLocation() {
		return docLocation;
	}

	public void setDocLocation(String docLocation) {
		this.docLocation = docLocation;
	}

	public String getRetentionName() {
		return retentionName;
	}

	public void setRetentionName(String retentionName) {
		this.retentionName = retentionName;
	}

	public String getRetCriteriaCount() {
		return retCriteriaCount;
	}

	public void setRetCriteriaCount(String retCriteriaCount) {
		this.retCriteriaCount = retCriteriaCount;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getSendMail() {
		return sendMail;
	}

	public void setSendMail(String sendMail) {
		this.sendMail = sendMail;
	}

	public String getMailIds() {
		return mailIds;
	}

	public void setMailIds(String mailIds) {
		this.mailIds = mailIds;
	}

	public String getIndicesCondition() {
		return indicesCondition;
	}

	public void setIndicesCondition(String indicesCondition) {
		this.indicesCondition = indicesCondition;
	}
}
