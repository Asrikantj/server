/*
 * Created on May 12, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.ars.server;

import java.rmi.RemoteException;

import com.computhink.common.CIServer;
import com.computhink.common.ServerSchema;
import com.computhink.vwc.VWClientListener;

/**
 * @author shanmugavalli.c
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface ARS extends CIServer {
	
	
	public ServerSchema getVWS() throws RemoteException; 
    public void setVWS(ServerSchema vws) throws RemoteException;
    public void init() throws RemoteException;
    public int getSchedulerStartInfo() throws RemoteException; 
    public void setSchedulerStartInfo(int schedStart) throws RemoteException;
    public String getDialyFrequency() throws RemoteException;
    public void setDialyFrequency(String dialyFrequency) throws RemoteException;
    public String getSnoozeTime() throws RemoteException;
    public void setSnoozeTime(String snoozeTime) throws RemoteException;
    

}
