/*
 * Created on May 12, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.ars.server;

import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.manager.ManagerPreferences;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.mail.VWMail;
import com.computhink.resource.ResourceManager;


/**
 * @author shanmugavalli.c
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ViewWiseARS implements ARSConstants{

	public static void main(String[] args) {
		String message = "";

		//ARSLog.err("prop CLASSPATH "+System.getProperty("CLASSPATH"));
		//ARSLog.err("java.class.path : " + System.getProperty("java.class.path"));
		
	      if (ARSPreferences.getProxy())
            Util.RegisterProxy(ARSPreferences.getProxyHost(),
                               ARSPreferences.getProxyPort(),
                               SERVER_ARS);
        String hostName = ARSPreferences.getHostName();
        if (hostName.length() > 0)
        {
            System.getProperties().put("java.rmi.server.useLocalHostname", "true");
            System.getProperties().put("java.rmi.server.hostname", hostName);
        }
        ServerSchema s = Util.getMySchema();
        s.comport = ARSPreferences.getComPort();
        s.dataport = ARSPreferences.getDataPort();
        s.type = SERVER_ARS;
        ARSLog.add("ARS Info " + ARSPreferences.getComPort()+":"+ARSPreferences.getDataPort()+":"+SERVER_ARS);
        /* Purpose:	Master Switch off added for ARS in registry
        * Created By: C.Shanmugavalli		Date:	20 Sep 2006
        */
        if(ManagerPreferences.getARSEnable().equalsIgnoreCase("true")){
	        int port = Util.createServer(s);
	        if ( port != 0)
	        {
	            ARSLog.add("Started "+ PRODUCT_NAME +" Retention Storage Server @" + hostName +"[" + s.address + "]:" + port);
	            try
	            {
	                //Util.sleep(200);
	                ((ARS) Util.getServer(s)).init();
				}
	            catch(Exception e){}
	        }
	        else{
	            //VWSLog.err("Could not start ViewWise Retention Server on port: " + port);
	        	
	        	message = ResourceManager.getDefaultManager().getString("ARSNotifyMSG.STARTARSONPORT1")+" "+ ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ResourceManager.getDefaultManager().getString("ARSNotifyMSG.STARTARSONPORT2")+" " + port;
				VWMail.sendMailNotificationOnVSM(message, VWSConstants.ARS);
				
	            ARSLog.err("Could not start "+ PRODUCT_NAME +" Retention Server on port: " + port);
	        }
	        
	    }// Master switch off condition
	}
}
