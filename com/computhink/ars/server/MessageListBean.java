package com.computhink.ars.server;

import java.util.List;

public class MessageListBean {
	private int sid = 0;
	private List messageDataList = null;

	public int getSid() {
		return sid;
	}

	public void setSid(int sid) {
		this.sid = sid;
	}

	public List getMessageDataList() {
		return messageDataList;
	}

	public void setMessageDataList(List messageDataList) {
		this.messageDataList = messageDataList;
	}
}
