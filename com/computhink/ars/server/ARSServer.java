/*
 * Created on May 12, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.ars.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import com.computhink.common.Constants;
import com.computhink.common.LogMessage;
import com.computhink.common.SCMEvent;
import com.computhink.common.SCMEventListener;
import com.computhink.common.SCMEventManager;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.VWClient;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWS;

/**
 * @author shanmugavalli.c
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ARSServer extends UnicastRemoteObject implements ARS, SCMEventListener, Constants {


	private Vector clients = new Vector();
	private Hashtable workers = new Hashtable();
    private long stime;
    private ServerSchema mySchema;
    //ARSServer shutdown is not stopping the final action process.
    public static boolean shutDownRequest = false;
    private static ServerSchema  managerSchema = null;
    Timer processTimer;
	private final String SIGN = "0B9B5177-1011-4BBF-B617-455288043D4A";
	VWClient vwClient;
	VWS vws;

    public ARSServer(ServerSchema s) throws RemoteException
    {
        super(s.dataport);
        this.mySchema = s;
        SCMEventManager scm = SCMEventManager.getInstance();
        scm.addSCMEventListener(this);
        stime = System.currentTimeMillis();
    }
    public void handleSCMEvent(SCMEvent event)
    {
        shutDown();
    }
    public void exit() throws RemoteException
    {
        shutDown();
    }
    public String getServerOS() throws RemoteException
    {
        return Util.getEnvOSName();
    }
    public void init() throws RemoteException
    {
        ServerSchema vwss = ARSPreferences.getVWS();
	    vws = (VWS) Util.getServer(vwss);
        if (vws == null)
        {
            ARSLog.add(PRODUCT_NAME + " Server @" + vwss.address + ":" +vwss.comport + " inactive");
            ARSLog.war("Listening to "+ PRODUCT_NAME +" Server...");
            vws = waitForViewWiseServer(vwss);
        }
        if (shutDownRequest) return;
        ARSLog.add("Connected to "+ PRODUCT_NAME +" Server @" + vwss.address + ":" +vwss.comport);
        try
        {
            String serverName = null;
        	//String cashDir = System.getProperty("java.io.tmpdir")+"Temp";;
        	String cashDir = "c:\\temp";
	  		vwClient = new VWClient(SIGN, cashDir,Client.Ars_Client_Type);
            //Vector servers=new Vector();
            //vwClient.getServers(servers);
            //if(servers == null || servers.size() == 0){
            	//ARSLog.err("Couldn't get server Name");
            	//return;
            //}   
            // loop for multiple servers
            //ARS is not for multiple server. It should be for single server
            //for(int j=0; j<servers.size(); j++)
            {
                //serverName = (String) servers.get(0);
                serverName = ARSConstants.DUMMY_SERVERNAME+"ARS";
                Vector rooms =new Vector();
                rooms = vws.getRoomNames();
                //int ret = vwClient.getRooms(serverName, rooms);
                /*if(rooms==null || rooms.size()==0)
                {
                    ARSLog.war("Waiting to get ViewWise Rooms...");
                    rooms=waitForGetViewWiseRooms(vws);
                    //rooms=waitForGetViewWiseRooms(vwClient, serverName);
                }*/
                // loop for multiple rooms
                if(rooms!=null && rooms.size()>0)
                {
                for (int i = 0; i < rooms.size(); i++)
                {
                    String roomName = (String) rooms.get(i);
                	int sessionID = -1;
               		sessionID = vws.login(roomName, Client.Ars_Client_Type, vwss.getAddress(), vwClient.encryptStr(ARSConstants.arsAdminUser), "", vwss.getComport());
                	if(sessionID>0){
                		vwClient.setSession(sessionID, serverName, roomName, ARSConstants.arsAdminUser);	
                	}else{
                		if(sessionID == ViewWiseErrors.NoMoreLicenseSeats){
                			ARSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.NoMoreLicenseSeats));
                		}else if(sessionID==ViewWiseErrors.ServerNotFound)
                        	ARSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.ServerNotFound));
                        else if(sessionID==ViewWiseErrors.checkUserError)
                        	ARSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.checkUserError));
                        else if(sessionID==ViewWiseErrors.NotAllowedConnections) 
                        	ARSLog.war("This Room is Administratively Down."+
                                NewLineChar+"Please contact your "+ PRODUCT_NAME +" Administrator"+
                                NewLineChar+"before attempting to reconnect.");
                        else if(sessionID==ViewWiseErrors.accessDenied)
                        	ARSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.accessDenied));
                        else if(sessionID==ViewWiseErrors.LoginCountExceeded)
                        	ARSLog.war(vwClient.getErrorDescription(ViewWiseErrors.LoginCountExceeded));
                        else if(sessionID==ViewWiseErrors.JarFileMisMatchErr)
                        	ARSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.JarFileMisMatchErr));
        	            else
        	            	ARSLog.war(""+ PRODUCT_NAME +" room may be down.");
                		continue;
                	}
                	int schedulerStartInfo = ARSPreferences.getSchedulerStartInfo();
		         	int snoozeTime = Integer.parseInt(ARSPreferences.getSnoozeTime()) * 60 * 1000;
                    if(schedulerStartInfo == 0){
                    	//int snoozeTime = 10 * 1000;
	                    Worker worker = new Worker(vwClient, roomName, snoozeTime, ARSPreferences.getSchedulerStartInfo(), sessionID);
	                    if (worker == null)
	                    {
	                        ARSLog.war("Cannot start Retention Worker for room " + roomName);
	                        continue;
	                    }
	                    //ARSLog.add("Processing retention documents of the room: '"+serverName+"."+roomName+"'");
	                    //ARSLog.add("Processing retention documents of the room: '"+roomName+"'");
                    	worker.start();
	                    workers.put(roomName, worker);
	                    
                    }else if(schedulerStartInfo == 1){
                    	snoozeTime = 1000*60*60*24;
                        //ARSLog.war(" dailyFrequency "+ARSPreferences.getDialyFrequency());
                    	//Worker class added here also to get room name and sessionId. This is to teminate session on logout
                    	Worker worker = new Worker(vwClient, roomName, snoozeTime, ARSPreferences.getSchedulerStartInfo(), sessionID);
                    	workers.put(roomName, worker);
                        String dailyFrequency = ARSPreferences.getDialyFrequency();
                        StringTokenizer stDailyFreq = null;
                        stDailyFreq = new StringTokenizer(dailyFrequency, ":");
                        
                        Calendar calendar = Calendar.getInstance();
                        if(stDailyFreq != null && stDailyFreq.countTokens() == 3){
                        	calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(stDailyFreq.nextToken()));
                        	calendar.set(Calendar.MINUTE, Integer.parseInt(stDailyFreq.nextToken()));
                        	calendar.set(Calendar.SECOND, Integer.parseInt(stDailyFreq.nextToken()));
                        }else{
                        	calendar.set(Calendar.HOUR_OF_DAY, 0);
                        	calendar.set(Calendar.MINUTE, 0);
                        	calendar.set(Calendar.SECOND, 0);
                        }
                        Date startTime = calendar.getTime();
                        //ARSLog.add("Processing Document: ... timer Started..."+startTime.toString());
                        //String now = new SimpleDateFormat("yy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
                        //ARSLog.add("Now time .."+now);
                        //ARSLog.add("ARS will process retention documents of the room: '"+serverName+"."+roomName+"' at " +startTime.toString());
                        ARSLog.add("ARS will process retention documents of the room: '"+roomName+"' at " +startTime.toString());
                    	processTimer = new Timer();
                    	processTimer.schedule(new SheduledProcess(vwClient, sessionID, roomName), startTime, snoozeTime);
                    }//end for schedulerStartInfo
                } // end for rooms loop
                } //if condition for room null
                
            } // end for servers loop
        }
        catch(Exception e)
        {
            ARSLog.err(PRODUCT_NAME + " Server connection error: " + e.getMessage());
        }

    }

    public String managerBegin(ServerSchema manager) throws RemoteException
    {
        if (managerSchema == null)
        {
            managerSchema = manager;
            return "";
        }
        else
        {
            if (managerSchema.address.equals(manager.address))
                return "";
            else
                return "Cannot monitor server. Manager @"
                                + managerSchema.address + " already connected.";
        }
    }
    public void managerEnd() throws RemoteException
    {
        managerSchema = null;
    }
    private void shutDown()
    {
    	vwClient.shutdown();
    	ARSLog.add("VW client stopped");
    	//vwClient.shutdown() kills the MDSS schema which is not in use.
    	vwClient = null;
    	//ARSServer shutdown is not stopping the final action process.
    	shutDownRequest = true;
    	if(processTimer!=null){
    		processTimer.cancel();
    		processTimer = null;
    	}
        Enumeration wenum = workers.elements();
        while (wenum.hasMoreElements())
        {
            Worker worker = (Worker) wenum.nextElement();
            try{
            	vws.logout(worker.roomName, worker.sessionID);
            }catch(Exception ex){}
            worker.shutDown();
            ARSLog.add("Stopped Retention Worker for Room " + worker.roomName);
        }
        ARSLog.add("Stopping ARS Server...");
        ARSLog.writeLog();
        Util.sleep(500);
        	Util.killServer(mySchema);
        ARSLog.err("Stopped");
    }

    private VWS waitForViewWiseServer(ServerSchema vwss)
    {
        VWS vws = null;
        while (vws == null && !shutDownRequest)
        {
            Util.sleep(2000);
            vws = (VWS) Util.getServer(vwss);
        }
        return vws;
    }

    private Vector waitForGetViewWiseRooms(VWS vws)
    {
        Vector ret=null;
        while ((ret == null || ret.size()==0) && !shutDownRequest)
        {
            try
            {
                Util.sleep(2000);
                ret = vws.getRoomNames();
            }
            catch(Exception e)
            {
            	ARSLog.err("Error in getting "+ PRODUCT_NAME +" rooms "+e.getMessage());
            }
        }
        return ret;
    }
    private Vector waitForGetViewWiseRooms(VWClient vwClient, String serverName)
    {
        Vector ret=null;
        while ((ret == null || ret.size()==0) && !shutDownRequest)
        {
            try
            {
                Util.sleep(2000);
                vwClient.getRooms(serverName,ret);
            }
            catch(Exception e)
            {
            	ARSLog.err("Error in getting "+ PRODUCT_NAME +" rooms "+e.getMessage());
            }
        }
        return ret;
    }

    public ServerSchema getSchema() throws RemoteException
    {
        return this.mySchema;
    }
    public void setComPort(int port) throws RemoteException
    {
        ARSPreferences.setComPort(port);
    }
    public int getComPort() throws RemoteException
    {
        return ARSPreferences.getComPort();
    }
     public void setDataPort(int port) throws RemoteException
    {
        ARSPreferences.setDataPort(port);
    }
    public int getDataPort() throws RemoteException
    {
        return ARSPreferences.getDataPort();
    }
    public void setProxy(boolean b) throws RemoteException
    {
        ARSPreferences.setProxy(b);
    }
    public void setProxyHost(String server) throws RemoteException
    {
        ARSPreferences.setProxyHost(server);
    }
    public void setProxyPort(int port) throws RemoteException
    {
        ARSPreferences.setProxyPort(port);
    }
    public boolean getProxy() throws RemoteException
    {
        return ARSPreferences.getProxy();
    }
    public String getProxyHost() throws RemoteException
    {
        return ARSPreferences.getProxyHost();
    }
    public int getProxyPort() throws RemoteException
    {
        return ARSPreferences.getProxyPort();
    }
    public ServerSchema getVWS() throws RemoteException
    {
        return ARSPreferences.getVWS();
    }
    public void setVWS(ServerSchema vws) throws RemoteException
    {
        ARSPreferences.setVWS(vws);
    }
    public boolean getLogInfo() throws RemoteException
    {
        return ARSPreferences.getLogInfo();
    }
    public void setLogInfo(boolean b) throws RemoteException
    {
        ARSPreferences.setLogInfo(b);
    }

    public boolean getDebugInfo() throws RemoteException
    {
        return ARSPreferences.getDebugInfo();
    }
    public void setDebugInfo(boolean b) throws RemoteException
    {
        ARSPreferences.setDebugInfo(b);
    }
    public boolean setManager(String man, String pas) throws RemoteException
    {
        return ARSPreferences.setManager(man, pas);
    }
    public String getManager() throws RemoteException
    {
    	return ARSPreferences.getManager();
    }


    public long getTimeOn() throws RemoteException
    {
    	return (System.currentTimeMillis() - stime) / 1000;
    }
    public LogMessage getLogMsg() throws RemoteException
    {
        return ARSLog.getLog();
    }
    public Vector getClients() throws RemoteException
    {
        return this.clients;
    }
    public int getLogCount() throws RemoteException
    {
        return ARSLog.getCount();
    }
    /*
     * Not in use check one time and remove - Valli 
     * */
    /*
   	public void setRoomEncryptionKey(String roomName,String key) throws RemoteException
	{
			if (!key.equals("") && !ciphers.containsKey(roomName))
			{
				ARSCipher cipher = new ARSCipher(key);
				ciphers.put(roomName, cipher);
				ARSLog.add("Document encryption set for Room '" +roomName + "'");
			}
			else
			{
				ciphers.put(roomName, null);
			}
	}

	public boolean isEncryptionKeySet(String roomName) throws RemoteException
	{
		return ciphers.containsKey(roomName);
    }
	* */
	 public int getSchedulerStartInfo() throws RemoteException
	 {
	        return ARSPreferences.getSchedulerStartInfo();
	 }
	 public void setSchedulerStartInfo(int schStart) throws RemoteException
	 {
	        ARSPreferences.setSchedulerStartInfo(schStart);
	 }

	 public String getDialyFrequency() throws RemoteException
	 {
	        return ARSPreferences.getDialyFrequency();
	 }
	 public void setDialyFrequency(String dialyFrequency) throws RemoteException
	 {
	        ARSPreferences.setDialyFrequency(dialyFrequency);
	 }
	 
	 
	 public String getSnoozeTime() throws RemoteException{
		 return ARSPreferences.getSnoozeTime();
	 }
	 public void setSnoozeTime(String snoozeTime) throws RemoteException{
		 
		 ARSPreferences.setSnoozeTime(snoozeTime);
		 
	 }

	 class SheduledProcess extends TimerTask {
	   	int ret = -1;
	   	private String roomName;
	   	private int sessionID;
	   	VWClient vwClient;
	   	public SheduledProcess(VWClient vwClient, int sessionID, String roomName){
	   		this.vwClient = vwClient;
	   		this.roomName = roomName;
	   		this.sessionID = sessionID;
	   	}
	   	private void doSheduledProcess(){
    		//ARSLog.add("coming in doSheduledProcess : '"+roomName+"'");
	   		ARSProcessDocs arsProcessDocs = new ARSProcessDocs();
    		//ret = arsProcessDocs.getReportDocs(vwClient, sessionID, roomName);
	   		ret = arsProcessDocs.sendEmailNotify(vwClient, sessionID, roomName);
	   		ret = arsProcessDocs.getRetentionDocs(vwClient, sessionID, roomName);
        	//processTimer.cancel();
        	//processTimer = null;
	   	}
	   	public void run(){
	   		doSheduledProcess();
	  	}
   }
}
