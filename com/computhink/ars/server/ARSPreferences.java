/*
 * Created on May 12, 2006
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.ars.server;

import java.rmi.RemoteException;
import java.util.prefs.Preferences;

import com.computhink.common.RoomProperty;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.vws.server.VWSPreferences;

/**
 * @author shanmugavalli.c
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ARSPreferences implements ARSConstants{
	
	private static Preferences ARSPref;
    
    static
    {
        ARSPref= Preferences.systemRoot().node(ARS_PREF_ROOT);
        
    }
    public static String getHostName()
    {
        return getNode("host").get("name", "");
    }
    public static void setHostName(String name)
    {
        getNode("host").put("name", name);
        flush();
    }
    private static Preferences getNode(String node)
    {
        return ARSPref.node(node);
    }
    public static void setComPort(int port)
    {
        getNode("host").putInt("comport", port);
        flush();
    }
    public static int getComPort()
    {
        return getNode("host").getInt("comport", DEFAULT_COM_PORT);
    }
     public static void setDataPort(int port)
    {
        getNode("host").putInt("dataport", port);
        flush();
    }
    public static int getDataPort()
    {
        return getNode("host").getInt("dataport", DEFAULT_DATA_PORT);
    }
    public static void setProxy(boolean b)
    {
        getNode("proxy").putBoolean("enabled", b);
        flush();
    }
    public static void setProxyHost(String server)
    {
        getNode("proxy").put("host", server);
        flush();
    }
    public static void setProxyPort(int port)
    {
        getNode("proxy").putInt("port", port);
        flush();
    }
    public static boolean getProxy()
    {
        return getNode("proxy").getBoolean("enabled", false);
    }
    public static String getProxyHost()
    {
        return getNode("proxy").get("host", "");
    }
    public static boolean getLogInfo()
    {
        return getNode("general").getBoolean("loginfo", true);
    }
    public static void setLogInfo(boolean b)
    {
        getNode("general").putBoolean("loginfo", b);
        flush();
    }
    
    public static boolean getDebugInfo()
    {
        return getNode("general").getBoolean("debuginfo", false);
    }
    public static void setDebugInfo(boolean b)
    {
        getNode("general").putBoolean("debuginfo", b);
        flush();
    }
    
    
    public static int getProxyPort()
    {
        return getNode("proxy").getInt("port", DEFAULT_PROXY_PORT);
    }
    public static int getFlushLines()
    {
        return getNode("general").getInt("loglines", DEFAULT_LOG_LINES);
    }
    public static void setVWS(ServerSchema vws)
    {
        getNode("vws").put("host", vws.address);
        getNode("vws").putInt("port", vws.comport);
        flush();
    }
    public static ServerSchema getVWS()
    {
        ServerSchema ss = new ServerSchema();
        ss.address = getNode("vws").get("host", "");
        ss.comport = getNode("vws").getInt("port", 0);
        ss.type = SERVER_VWS;
        return ss;
    }
    
    public static String getManager()
    {
        String man = getNode("general").get("manager", "admin");
        String pas = getNode("general").get("manpass", "manager");
        if (!pas.equals("manager")) pas = Util.decryptKey(pas);
        return man + "||" + pas;
    }
    public static boolean setManager(String man, String pas)
    {
        try
        {
            pas = Util.encryptKey(pas);
            getNode("general").put("manager", man);
            getNode("general").put("manpass", pas);
            flush();
            return true;
        }
        catch(Exception e){}
        return false;
    }
    
    static private void flush()
    {
        try
        {
            ARSPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }   
    
    public RoomProperty[] getRooms() throws RemoteException
    {
        return VWSPreferences.getRooms();
    }
    
    public static int getSchedulerStartInfo()
    {
        return getNode("general").getInt("schedularStart", DEFAULT_SCHEDULARSTART);
    }
    public static void setSchedulerStartInfo(int s)
    {
        getNode("general").putInt("schedularStart", s);
        flush();
    }
    
    public static String getDialyFrequency() 
	{
    	return getNode("general").get("DailyFrequency", DEFAULT_DAILYFREQUENCY); 
	}
	public static void setDialyFrequency(String dialyFrequency) 
	{
	 	getNode("general").put("DailyFrequency", dialyFrequency);
        flush();
	}
	
	public static String getSnoozeTime() 
	{
    	return getNode("general").get("SnoozeTime", DEFAULT_SNOOZETIME); 
	}
	public static void setSnoozeTime(String snoozeTime) 
	{
	 	getNode("general").put("SnoozeTime", snoozeTime);
        flush();
	}
	
    public static void setLogFileCount(int count)
    {
        getNode("general").putInt("maxlogfilecount", count);
        flush();
    }    
    public static int getLogFileCount()
    {
        return getNode("general").getInt("maxlogfilecount",100);
    }
    public static boolean getKeepDocType(){
    	return getNode("general").getBoolean("keepdoctype", false);
    }
    public static boolean getPurgeDocs(){
    	return getNode("general").getBoolean("PurgeDocs", false);
    }
    public static void setKeepDocType(String flag) 
	{
	 	getNode("general").put("keepdoctype", flag);
        flush();
	}
    public static void setPurgeDocs(String flag) 
	{
	 	getNode("general").put("PurgeDocs", flag);
        flush();
	}
}
