package com.computhink.common;

import java.util.*;

public class SCMEventManager extends Vector
{
    private static SCMEventManager h = null;

    private SCMEventManager()
    {
        super();
    }
    public static SCMEventManager getInstance()
    {
        if(null == h)  h = new SCMEventManager();
        return h;
    }
    public void dispatchSCMEvent(int eventID)
    {
        SCMEvent event = new SCMEvent(eventID);
        for(int i=0; i < size(); i++)
        {
            Object obj = get(i);
            if (null == obj) continue;
            ((SCMEventListener)obj).handleSCMEvent(event);
        }
    }
    public void addSCMEventListener(SCMEventListener l)
    {
        if (!contains(l)) add(l);
    }
    public void removeSCMEventListener(SCMEventListener l)
    {
            remove(l);
    }
}
        

            

