package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;

public class RouteInvalidTaskInfo extends Object implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int routeId;
	private int taskId;
	private String taskName;
	private String userName;

	public RouteInvalidTaskInfo() {
	}

	public RouteInvalidTaskInfo(String dbstring) {
		StringTokenizer st = new StringTokenizer(dbstring, "\t");
		/* 
		 * Sample Data - [3 Task_1 vw1]
		 */
		
		try {
			this.taskId = Util.to_Number(st.nextToken());
			this.taskName = st.nextToken();
			this.userName = st.nextToken();
							
		} catch (Exception e) {
		}
	}

	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	public int getTaskId() {
		return taskId;
	}

	public void setTaskId(int taskId) {
		this.taskId = taskId;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
}