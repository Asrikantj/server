/*
 * VWRetention.java
 *
 * Created on February 08, 2010
 */

package com.computhink.common;
/**
 *
 * @author  Vijaypriya.B.K
 */
import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;

public class VWRetention implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id = -1;
    private String name;
    //Added for Retention based on folder policy enhancement,Gurumurthy.T.S 22-05-2012
    private String retentionLocation="";
    private String retentionLocationId="";
    private int docTypeId;
    private int status;
    private String createdBy = "";
    private int actionId;
    private String location = "";
    private String ipAddress = "";
    private String createdDate = "";
    
    //
    private int notifyBefore;
    private String reportLocation = "";
    private int sendMail = 0;
    private String mailIds = "";
    private String fromARS = "ARS";
    //
    
    private Vector<IndicesCondition> indicesCondition = new Vector<IndicesCondition>();
    
    public VWRetention(int id) 
    {
    	this.id = id;
    }
    public VWRetention(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
        	/**
             * Name, Id, DocTypeId, EnableRetention, DisposalActionId, MoveDocLocation, PolicyCreatedDate
             */
        	this.name = st.nextToken();
            this.id = Util.to_Number(st.nextToken());
            this.docTypeId = Util.to_Number(st.nextToken());
            this.status = Util.to_Number(st.nextToken());
            this.actionId = Util.to_Number(st.nextToken());
            this.location = st.nextToken();
            this.notifyBefore = Util.to_Number(st.nextToken());
            this.createdDate = st.nextToken();
            this.reportLocation = st.nextToken();
            this.sendMail = Util.to_Number(st.nextToken());
            this.mailIds = st.nextToken();
            //Added for Retention based on folder policy enhancement,Gurumurthy.T.S 22-05-2012
            this.retentionLocationId=st.nextToken();
            this.retentionLocation=st.nextToken();
            
            this.createdBy = st.nextToken();
            this.ipAddress = st.nextToken();
        }
        catch(Exception e){}
    }
    public VWRetention(String dbstring,String userName)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
        	/**
             * Name, Id, DocTypeId, EnableRetention, DisposalActionId, MoveDocLocation, PolicyCreatedDate
             */
        	
            this.id = Util.to_Number(st.nextToken());
            this.name = st.nextToken();
            this.docTypeId = Util.to_Number(st.nextToken());
            this.status = Util.to_Number(st.nextToken());
            this.actionId = Util.to_Number(st.nextToken());
            this.location = st.nextToken();
            this.notifyBefore = Util.to_Number(st.nextToken());
            this.createdDate ="";
            this.reportLocation = st.nextToken();
            this.sendMail = Util.to_Number(st.nextToken());
            this.mailIds = st.nextToken();
           
            //Added for Retention based on folder policy enhancement,Gurumurthy.T.S 22-05-2012
            this.retentionLocationId=st.nextToken();
            st.nextToken();
            this.retentionLocation="0";
            
            this.createdBy = userName;
           // this.ipAddress = st.nextToken();
        }
        catch(Exception e){}
    }
    public VWRetention(int id, String name) 
    {
        this.id = id;
        this.name = name;
    }
    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String toString()
    {
        return this.name;
    }
    
    public void setSettings(Vector settings)
    {
        if(settings==null || settings.size()==0) 
        {
            status=0;
            return;
        }
        StringTokenizer st = new StringTokenizer((String)settings.get(0), "\t");
        try
        {
            status = Util.to_Number(st.nextToken());
            name=st.nextToken();
            docTypeId=Util.to_Number(st.nextToken());
            createdBy=st.nextToken();
            actionId=Util.to_Number(st.nextToken());
            location=st.nextToken();
            ipAddress=st.nextToken();
        }
        catch(Exception e){}
    }

	public int getActionId() {
		return actionId;
	}
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public int getDocTypeId() {
		return docTypeId;
	}
	public void setDocTypeId(int docTypeId) {
		this.docTypeId = docTypeId;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Vector getIndicesCondition() {
		return indicesCondition;
	}
	public void setIndicesCondition(Vector indicesCondition) {
		this.indicesCondition.removeAllElements();
		this.indicesCondition.addAll(indicesCondition);
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getFromARS() {
		return fromARS;
	}
	public void setFromARS(String fromARS) {
		this.fromARS = fromARS;
	}
	public String getMailIds() {
		return mailIds;
	}
	public void setMailIds(String mailIds) {
		this.mailIds = mailIds;
	}
	public String getReportLocation() {
		return reportLocation;
	}
	public void setReportLocation(String reportLocation) {
		this.reportLocation = reportLocation;
	}
	public int getSendMail() {
		return sendMail;
	}
	public void setSendMail(int sendMail) {
		this.sendMail = sendMail;
	}
	public int getNotifyBefore() {
		return notifyBefore;
	}
	public void setNotifyBefore(int notifyBefore) {
		this.notifyBefore = notifyBefore;
	}
	//Added for Retention based on folder policy enhancement,Gurumurthy.T.S 22-05-2012
	public String getRetentionLocation() {
		return retentionLocation;
	}
	public void setRetentionLocation(String retentionLocation) {
		this.retentionLocation = retentionLocation;
	}

	public String getRetentionLocationId() {
		return retentionLocationId;
	}
	public void setRetentionLocationId(String retentionLocationId) {
		this.retentionLocationId = retentionLocationId;
	}
	//
	public boolean isSameObject(Object obj){
		//System.out.println("Checking the retention are same");
		boolean retFlag = false;
		if (obj instanceof VWRetention) {
			VWRetention retObject = (VWRetention) obj;
			if (getName().equals(retObject.getName()) &&
					getDocTypeId() == retObject.getDocTypeId() &&
					getActionId() == retObject.getActionId() &&
					getLocation().equals(retObject.getLocation()) ){
				//System.out.println("Both retention are same");
				retFlag = true;
			}else{
				//System.out.println("Both retention are not same");
				return false;
			}
			if (retFlag){
				Vector objIndicesCondition = retObject.getIndicesCondition();
				Vector objSrcIndicesCondition = getIndicesCondition();
				//System.out.println(" Index condition size " + objIndicesCondition.size() + " :::: " +objSrcIndicesCondition.size());
				if (objIndicesCondition.size() != objSrcIndicesCondition.size()){
					//System.out.println("Both Index condition size are not same");
					return false;
				}
				for (int j = 0; j < objSrcIndicesCondition.size(); j++){
					if ( objSrcIndicesCondition.get(j) instanceof IndicesCondition){
						IndicesCondition srcIdxCondition = (IndicesCondition) objSrcIndicesCondition.get(j);
						boolean isMatching = false;
						for (int i = 0; i < objIndicesCondition.size(); i++){
							if (objIndicesCondition.get(i) instanceof IndicesCondition){
								IndicesCondition modifiedIdxCondition = (IndicesCondition) objIndicesCondition.get(i);
								if (modifiedIdxCondition.isSameObject(srcIdxCondition)){
									isMatching = true;									
								}
							}
						}
						if (!isMatching){
							//System.out.println("Index condition are not same");
							return false;
						}
					}
				}
				//System.out.println("retention are same");
				return true;
			}
		}
		//System.out.println("returning the retention are not same");
		return false;
	}
}