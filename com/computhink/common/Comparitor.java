package com.computhink.common;
import java.util.*;
import com.computhink.vws.server.*;
public class Comparitor implements Comparator 
{
   public int compare(Object o1, Object o2) 
   {
   	long v1=0;
   	long v2=0;
   	try{
      v1 = Long.parseLong((String)((Map.Entry)o1).getValue());
      v2 = Long.parseLong((String)((Map.Entry)o2).getValue());
   	}
   	catch(Exception e){
   	VWSLog.add("Error in parseLong "+e.getMessage());
   	}
      if(v1==v2)
      	return 0;
      else if(v1>v2)
      	return -1;
      else
      	return 1;   	
     
    }
}
