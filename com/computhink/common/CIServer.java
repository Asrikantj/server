package com.computhink.common;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.*;

public interface CIServer extends Remote, Constants 
{
    public ServerSchema getSchema() throws RemoteException;
    public void setComPort(int port) throws RemoteException; 
    public int getComPort() throws RemoteException;
    public void setDataPort(int port) throws RemoteException;
    public int getDataPort() throws RemoteException;
    
    public void setProxy(boolean b) throws RemoteException;
    public void setProxyHost(String server) throws RemoteException;
    public void setProxyPort(int port) throws RemoteException;
    public boolean getProxy() throws RemoteException;
    public String getProxyHost() throws RemoteException;
    public int getProxyPort() throws RemoteException;
    
    public LogMessage getLogMsg() throws RemoteException;
    public Vector getClients() throws RemoteException;
    public int getLogCount() throws RemoteException;
    public long getTimeOn() throws RemoteException; 
    
    public boolean getLogInfo() throws RemoteException; 
    public void setLogInfo(boolean b) throws RemoteException; 
    public boolean getDebugInfo() throws RemoteException; 
    public void setDebugInfo(boolean b) throws RemoteException; 
    
    public String managerBegin(ServerSchema schema) throws RemoteException; 
    public void managerEnd() throws RemoteException; 
    
    public boolean setManager(String man, String pas) throws RemoteException;
    public String getManager() throws RemoteException;
    public void exit() throws RemoteException;
    public String getServerOS() throws RemoteException;
}
