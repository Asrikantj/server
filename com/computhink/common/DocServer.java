/*
 * DocServer.java
 *
 * Created on December 10, 2003, 11:35 AM
 */
package com.computhink.common;
/**
 *
 * @author  Administrator
 *  Note:   classes utilizing file transfer implement this interface
 *          for now: DSS, MDSS and IXR
 */
import java.rmi.*;

public interface DocServer extends Remote, Constants
{
    public boolean getDocument(Document doc, String room, String path, 
               ServerSchema svr, boolean insideIdFolder) throws RemoteException;
    //public boolean setDocument(Document doc, String room) 
    //                                                   throws RemoteException;
    public boolean getDocFolder(Document doc, String room, String path, 
                                       ServerSchema svr) throws RemoteException;
    //public boolean setDocFolder(Document doc, String room) 
    //                                                     throws RemoteException;
    
    public VWDoc setDocument(Document doc, String room) throws RemoteException;
    public VWDoc setDocFolder(Document doc, String room) throws RemoteException;
}
