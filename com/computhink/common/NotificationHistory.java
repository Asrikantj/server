package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;

public class NotificationHistory implements Serializable{

	private static final long serialVersionUID = 1L;
	private int id;
	private int historyId;
	private int folderId;
	private int nodeId;
	private String nodeName;
	private int nodeType;
	private int nodifyId;
	private int captureType;
	private int processType;
	private int inherit;
	private String creator;
	private int attachment;
	private String mailIds;
	private String oldValue;
	private String newValue;
	private String modifiedBy;
	private String desc;
	private String created;
	private String sentDate;
	private int sentFlag;
	private int failedFlag;
	//Future purpose
	private int mailException;
	
	public NotificationHistory(String dbstring) 
	{
		try{
			//4 0 1256	Doc Name	1 4 1 0 0 vwadmin	11 vijaypriya.k@syntaxsoft.com;pandiyaraj.m@syntaxsoft.com - - vwadmin Comment added for document DocOne 2011-12-05 11:53:02 - 0 0
			StringTokenizer st = new StringTokenizer(dbstring, "\t");
			this.id = Util.to_Number(st.nextToken());			//4
			this.historyId = Util.to_Number(st.nextToken());	//4
			this.folderId = Util.to_Number(st.nextToken());		//0
			this.nodeId = Util.to_Number(st.nextToken());		//1256
			this.nodeName = st.nextToken().trim();				//Doc Name
			this.nodeType = Util.to_Number(st.nextToken());		//1
			this.nodifyId = Util.to_Number(st.nextToken());		//4
			this.captureType = Util.to_Number(st.nextToken());	//1
			this.processType = Util.to_Number(st.nextToken());	//0
			this.inherit = Util.to_Number(st.nextToken());		//0
			this.creator = st.nextToken().trim();				//vwadmin
			this.attachment = Util.to_Number(st.nextToken());	//11
			this.mailIds = st.nextToken().trim();				//vijaypriya.k@syntaxsoft.com;pandiyaraj.m@syntaxsoft.com
			this.oldValue = st.nextToken().trim();				//	-
			this.newValue = st.nextToken().trim();				//  -
			this.modifiedBy = st.nextToken().trim();			//vwadmin
			this.desc = st.nextToken().trim();					//Comment added for document DocOne
			this.created = st.nextToken().trim();				//2011-11-25 12:44:00
			this.sentDate = st.nextToken().trim();				//	-
			this.sentFlag = Util.to_Number(st.nextToken());		//	0
			this.failedFlag = Util.to_Number(st.nextToken());	//	0
		}catch(Exception ex){
		}
	}
		            
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public int getFailedFlag() {
		return failedFlag;
	}
	public void setFailedFlag(int failedFlag) {
		this.failedFlag = failedFlag;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getNewValue() {
		return newValue;
	}
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}
	public int getNodeId() {
		return nodeId;
	}
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}
	public String getOldValue() {
		return oldValue;
	}
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}
	public String getSentDate() {
		return sentDate;
	}
	public void setSentDate(String sentDate) {
		this.sentDate = sentDate;
	}
	public int getSentFlag() {
		return sentFlag;
	}
	public void setSentFlag(int sentFlag) {
		this.sentFlag = sentFlag;
	}
	public int getNodifyId() {
		return nodifyId;
	}
	public void setNodifyId(int settingsId) {
		this.nodifyId = settingsId;
	}

	public int getMailException() {
		return mailException;
	}
	public void setMailException(int mailException) {
		this.mailException = mailException;
	}

	public int getFolderId() {
		return folderId;
	}

	public void setFolderId(int folderId) {
		this.folderId = folderId;
	}

	public int getNodeType() {
		return nodeType;
	}

	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}

	public int getAttachment() {
		return attachment;
	}

	public void setAttachment(int attachment) {
		this.attachment = attachment;
	}

	public int getCaptureType() {
		return captureType;
	}

	public void setCaptureType(int captureType) {
		this.captureType = captureType;
	}

	public int getInherit() {
		return inherit;
	}

	public void setInherit(int inherit) {
		this.inherit = inherit;
	}

	public String getMailIds() {
		return mailIds;
	}

	public void setMailIds(String mailIds) {
		this.mailIds = mailIds;
	}

	public int getProcessType() {
		return processType;
	}

	public void setProcessType(int processType) {
		this.processType = processType;
	}

	public int getHistoryId() {
		return historyId;
	}

	public void setHistoryId(int historyId) {
		this.historyId = historyId;
	}

	public String getNodeName() {
		return nodeName;
	}

	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
}
