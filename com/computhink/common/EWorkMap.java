/*
 * EWorkMap.java
 *
 * Created on 12 ����, 2004, 10:50 �
 */

package com.computhink.common;

import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  Administrator
 */
public class EWorkMap implements Serializable {
    
    private int id;
    private int templateId;
    private int indexId;
    private int eWorkFieldId;
    private String eWorkField = "";
    private String eWorkFieldFromForm = "";
    private int eWorkFieldFromId;
    private String eWorkFieldFrom = "";
    
    public EWorkMap() {
    }
    public EWorkMap(int Id) {
        setId(Id);
    }
    
    public EWorkMap(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
            this.id             = Util.to_Number(st.nextToken());
            this.templateId     = Util.to_Number(st.nextToken());
            this.indexId        = Util.to_Number(st.nextToken());
            this.eWorkFieldId   = Util.to_Number(st.nextToken());
            this.eWorkField     = st.nextToken();
            this.eWorkFieldFromForm= st.nextToken();
            this.eWorkFieldFromId = Util.to_Number(st.nextToken());
            this.eWorkFieldFrom     = st.nextToken();
        }
        catch(Exception e){}
    }
    
    public int getId()
    {
        return this.id;
    }
    public void setId(int Id)
    {
        this.id = Id;
    }
    
    public int getTemplateId()
    {
        return this.templateId;
    }
    public void setTemplateId(int templateId)
    {
        this.templateId = templateId;
    }
        
    public int getIndexId()
    {
        return this.indexId;
    }
    public void setIndexId(int indexId)
    {
        this.indexId = indexId;
    }
    
    public int getEWFieldId()
    {
        return this.eWorkFieldId;
    }
    public void setEWFieldId(int eWorkFieldId)
    {
        this.eWorkFieldId = eWorkFieldId;
    }
    
    public String getEWField()
    {
        return this.eWorkField;
    }
    
    public void setEWField(String eWorkField)
    {
        this.eWorkField = eWorkField;
    }
    
    public String getEWorkFieldFromForm()
    {
        return this.eWorkFieldFromForm;
    }
    public void setEWorkFieldFromForm(String eWorkFieldFromForm)
    {
        this.eWorkFieldFromForm = eWorkFieldFromForm;
    }
    
    public int getEWorkFieldFromId()
    {
        return this.eWorkFieldFromId;
    }
    public void setEWorkFieldFromId(int eWorkFieldFromId)
    {
        this.eWorkFieldFromId = eWorkFieldFromId;
    }
    public String getEWorkFieldFrom()
    {
        return this.eWorkFieldFrom;
    }
    public void setEWorkFieldFrom(String eWorkFieldFrom)
    {
        this.eWorkFieldFrom = eWorkFieldFrom;
    }
}
