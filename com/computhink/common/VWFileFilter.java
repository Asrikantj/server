/*
 * VWTFilter.java
 *
 * Created on March 31, 2002, 12:17 AM
 */
package com.computhink.common;
/**
 *
 * @author  Saad
 * @version 
 */
import java.io.*;
import java.util.regex.*;

public class VWFileFilter implements FilenameFilter
{
    private static String filter;
    
    public VWFileFilter(String filter)
    {
        this.filter = filter;
    }
    public boolean accept(File dir, String name) 
    {
        Pattern pattern = Pattern.compile("." + filter, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher((CharSequence) name);
        if (matcher.matches())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
