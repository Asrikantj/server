package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;

public class RouteIndexInfo implements Serializable {
	
	private int routeIndexInfoId = 0;
	private int routeId = 0;
	private int indexId = 0;
	private int indexTypeId = 0;
	private int docTypeId = 0;
	private String indexName = "";
	private String indexValue1 = "";
	private String indexValue2 = "";
	private String comparisonOperator = "";
	private String logicalOperator = "";		
	
    public RouteIndexInfo()
    {
   	
    }
    public RouteIndexInfo(String dbstring) 
    {
    	StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
        	this.routeIndexInfoId = Util.to_Number(st.nextToken());
			this.routeId = Util.to_Number(st.nextToken());
			this.indexId = Util.to_Number(st.nextToken());
			this.indexName = st.nextToken();
			this.indexValue1 = st.nextToken();
			this.indexValue2= st.nextToken().trim();
			this.comparisonOperator = st.nextToken();
			this.logicalOperator = st.nextToken();
			this.docTypeId = Util.to_Number(st.nextToken());
			this.indexTypeId = Util.to_Number(st.nextToken());
        }
        catch(Exception e){
        }
    }
	public String getComparisonOperator() {
		return  comparisonOperator;
	}
	public void setComparisonOperator(String comparisonOperator) {
		this.comparisonOperator = comparisonOperator;
	}
	public int getIndexId() {
		return indexId;
	}
	public void setIndexId(int indexId) {
		this.indexId = indexId;
	}
	public String getIndexName() {
		return indexName;
	}
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}
	public String getIndexValue1() {
		return indexValue1;
	}
	public void setIndexValue1(String indexValue1) {
		this.indexValue1 = indexValue1;
	}
	public String getIndexValue2() {
		return indexValue2;
	}
	public void setIndexValue2(String indexValue2) {
		this.indexValue2 = indexValue2;
	}
	public String getLogicalOperator() {
		return logicalOperator;
	}
	public void setLogicalOperator(String logicalOperator) {
		this.logicalOperator = logicalOperator;
	}
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public int getRouteIndexInfoId() {
		return routeIndexInfoId;
	}
	public void setRouteIndexInfoId(int routeIndexInfoId) {
		this.routeIndexInfoId = routeIndexInfoId;
	}
	public int getDocTypeId() {
		return docTypeId;
	}
	public void setDocTypeId(int docTypeId) {
		this.docTypeId = docTypeId;
	}
	public int getIndexTypeId() {
		return indexTypeId;
	}
	public void setIndexTypeId(int indexTypeId) {
		this.indexTypeId = indexTypeId;
	}

}
