/*
 * VWATEvents.java
 *
 * Created on March 13, 2004, 12:52 PM
 */
package com.computhink.common;
/**
 *
 * @author  Administrator
 */
public class VWATEvents 
{
    public static int AT_OBJECT_DOC = 200;
    public static int AT_OBJECT_NODE = 300;
    public static int AT_OBJECT_FIND = 400;
    public static int AT_OBJECT_LOGIN = 500;
    public static int AT_OBJECT_AT = 600;
    public static int AT_OBJECT_SECURITY = 700;
    public static int AT_OBJECT_ARS = 900;
    public static int AT_OBJECT_DRS = 1000;
    public static int AT_OBJECT_OTP = 6200;
    public static int AT_OBJECT_OTPREGENERATE = 6300;
    public static int AT_OBJECT_OTPVAL_FAILED = 6400;
    private static int AT_FIRST_EVENT = 1;
    
    //Document EVENTS
    public static int AT_DOC_OPEN = AT_FIRST_EVENT++;
    public static int AT_DOC_FILEUPDATE = AT_FIRST_EVENT++;
    public static int AT_DOC_INDEXUPDATE = AT_FIRST_EVENT++;
    public static int AT_DOC_PURGE = AT_FIRST_EVENT++;
    public static int AT_DOC_FILEMOVE = AT_FIRST_EVENT++;   //Archive in DB
    public static int AT_DOC_VRACTIVE = AT_FIRST_EVENT++;
    public static int AT_DOC_VRUPDATE = AT_FIRST_EVENT++;
    public static int AT_DOC_NEW = AT_FIRST_EVENT++;
    public static int AT_DOC_MOVE = AT_FIRST_EVENT++;
    public static int AT_DOC_DELETE = AT_FIRST_EVENT++;
    public static int AT_DOC_FAX = AT_FIRST_EVENT++;
    public static int AT_DOC_EMAIL = AT_FIRST_EVENT++;
    public static int AT_DOC_PRINT = AT_FIRST_EVENT++;
    public static int AT_DOC_EXPORT = AT_FIRST_EVENT++;
    public static int AT_DOC_CHECKOUT = AT_FIRST_EVENT++;
    public static int AT_DOC_CHECKIN = AT_FIRST_EVENT++;
    public static int AT_DOC_BACKUP = AT_FIRST_EVENT++;
    public static int AT_DOC_RESTORE = AT_FIRST_EVENT++;
    public static int AT_DOC_COPY = AT_FIRST_EVENT++;
    // Search EVENTS
    public static int AT_FIND = AT_FIRST_EVENT++;
    // Node EVENTS
    public static int AT_NODE_NEW = AT_FIRST_EVENT++;
    public static int AT_NODE_COPY = AT_FIRST_EVENT++;
    public static int AT_NODE_MOVE = AT_FIRST_EVENT++;
    public static int AT_NODE_RENAME = AT_FIRST_EVENT++;
    public static int AT_NODE_DELETE = AT_FIRST_EVENT++;
    //Document EVENTS
    public static int AT_DOC_CANCELCHECKOUT = AT_FIRST_EVENT++;
    public static int AT_DOC_CANCELCHECKIN = AT_FIRST_EVENT++; //obsolete in 5.6
    
    public static int AT_LOGIN = AT_FIRST_EVENT++;
    public static int AT_LOGOUT = AT_FIRST_EVENT++;
    
    public static int AT_SETTING_CHANGE = AT_FIRST_EVENT++;//30;
    public static int AT_DATA_ARCHIVE = AT_FIRST_EVENT++;//31;
    public static int AT_DATA_DEL = AT_FIRST_EVENT++;//32;
    
    public static int AT_DOC_VRDEL = AT_FIRST_EVENT++;//33;
    public static int AT_DOC_VRCOMMENT = AT_FIRST_EVENT++;//34;
    
    public static int AT_SEC_NEW = AT_FIRST_EVENT++;//35;
    public static int AT_SEC_UPDATE = AT_FIRST_EVENT++;//36;
    public static int AT_SEC_REMOVE = AT_FIRST_EVENT++;//37;
   
    // ARS Document EVENT, 20 July 2006, Valli
    public static int AT_ARS_APPLIED = 42; 
    //DRS Document EVENT, 19 Dec 2006, Valli
    public static int AT_DRS_APPLIED = 45; 
    public static int AT_DRS_NEW_ROUTE = 51;
    public static int AT_DRS_UPDATE_ROUTE = 52;
    public static int AT_LOGIN_SECURE_LINK=67;
	public static int AT_OTP = 2;
    
    
    public static String AT_DOC_FILEUPDATE_DESC = "Pages Updated";
    public static String AT_DOC_FILEUPDATE_FAIL_DESC = "Pages failed to update";
    public static String AT_CREATE_DOC__DESC = "Document Created from";
    public static String AT_DOC_FILEOPEN_DESC = "Document Opened";
    public static String AT_REC_DOC_FILEUPDATE_DESC = "Recovered Document Updated";
}