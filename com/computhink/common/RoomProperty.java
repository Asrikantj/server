/*
 * IndexerservermanagerImpl.java
 *
 * Created on March 17, 2002, 12:14 AM
*/
package com.computhink.common;
/**
 *
 * @author  Administrator
 * @version 
*/

public class RoomProperty implements java.io.Serializable
{
    private String  name;
    private boolean bypass;
    private boolean enabled;
    private String  databaseEngineName;
    private String  databaseName;
    private String  databaseHost;
    private int     databasePort;
    private String  databaseUser;
    private String  databasePassword;
    private String  admins;
    private String  namedGroup;
    private String namedOnlineGroup;
    private String professionalNamedGroup;
    private String enterpriseConcurrentGroup;
    private String concurrentOnlineGroup;
    private String professionalConcurrentGroup;
    private String publicWAGroup;
    private String  managers;
    private int     idle;
    private String  key;
    private boolean isEncryptedKey;
    private int     minPoolSize;
    private int     maxPoolSize;
    private int 	countDBFail;
    private int connectionResetScheduler; 
    private String schedulerTime; 
    private int tabStatus;
    private int togglestatus;
    private String  subAdmins;
    private String batchInputAdministrator;
    private String sqlConnectionString;
    private String authenticationMode;
    private boolean integratedSecurity;
    private String authenticationScheme;
    private String domain;
    private String serverSpn;
    public RoomProperty(String name)
    {
        this.name = name;
    }
    public String getName()
    {
        return name;
    }
    public int getIdle()
    {
        return idle;
    }
    public void setIdle(int idle)
    {
        this.idle = idle;
    }
    public int getDatabasePort()
    {
        return databasePort;
    }
    public void setDatabasePort(int port)
    {
        this.databasePort = port;
    }
    public void setDatabaseEngineName(String databaseEngineName)
    {
        this.databaseEngineName = databaseEngineName;
    }
    public String getDatabaseEngineName()
    {
        return databaseEngineName;
    }
    public void setDatabaseName(String databaseName)
    {
        this.databaseName = databaseName;
    }
    public String getDatabaseName()
    {
        return databaseName;
    }
    public String getDatabaseHost()
    {
        return databaseHost;
    }
    public void setDatabaseHost(String databaseHost)
    {
        this.databaseHost = databaseHost;
    }
    public String getAdmins()
    {
        return admins;
    }
    public void setAdmins(String admins)
    {
        this.admins = admins;
    }
    public String getManagers()
    {
        return managers;
    }
    public void setManagers(String managers)
    {
        this.managers = managers;
    }
    public String getKey()
    {
        return key;
    }
    public void setKey(String key)
    {
        this.key = key;
    }
    public boolean getBypass()
    {
        return bypass;
    }
    public void setBypass(boolean bypass)
    {
        this.bypass = bypass;
    }
    public void setDatabaseUser(String databaseUser)
    {
        this.databaseUser = databaseUser;
    }
    public String getDatabaseUser()
    {
        return databaseUser;
    }
    public void setDatabasePassword(String databasePassword)
    {
        this.databasePassword = databasePassword;
    }
    public String getDatabasePassword()
    {
        return databasePassword;
    }
    public int getMinPoolSize()
    {
        return minPoolSize;
    }
    public void setMinPoolSize(int size)
    {
        this.minPoolSize = size;
    }
    public int getMaxPoolSize()
    {
        return maxPoolSize;
    }
    public void setMaxPoolSize(int size)
    {
        this.maxPoolSize = size;
    }
    public String toString()
    {
        return name;
    }
    public boolean isEnabled() 
    {
        return enabled;
    }
    public void setEnabled(boolean enabled) 
    {
        this.enabled = enabled;
    }
	/**
	 * @return Returns the countDBFail.
	 */
	public int getCountDBFail() {
		return countDBFail;
	}
	/**
	 * @param countDBFail The countDBFail to set.
	 */
	public void setCountDBFail(int countDBFail) {
		this.countDBFail = countDBFail;
	}
	// Added for connection reset scheduler
	/**
	 * @return Returns the connectionResetScheduler.
	 */
	public int getConnectionResetScheduler() {
		return connectionResetScheduler;
	}
	/**
	 * @param connectionResetScheduler The connectionResetScheduler to set.
	 */
	public void setConnectionResetScheduler(int connectionResetScheduler) {
		this.connectionResetScheduler = connectionResetScheduler;
	}
	/**
	 * @return Returns the schedulerTime.
	 */
	public String getSchedulerTime() {
		return schedulerTime;
	}
	/**
	 * @param schedulerTime The schedulerTime to set.
	 */
	public void setSchedulerTime(String schedulerTime) {
		this.schedulerTime = schedulerTime;
	}
	public String getNamedGroup() {
		return namedGroup;
	}
	public void setNamedGroup(String namedGroup) {
		this.namedGroup = namedGroup;
	}
	
	public String getNamedOnlineGroup() {
		return namedOnlineGroup;
	}
	public void setNamedOnlineGroup(String namedOnlineGroup) {
		this.namedOnlineGroup = namedOnlineGroup;
	}
	public String getProfessionaNamedGroup() {
		return professionalNamedGroup;
	}
	public void setProfessionalNamedGroup(String professionalNamedGroup) {
		this.professionalNamedGroup = professionalNamedGroup;
	}
	public String getEnterpriseConcurrentGroup() {
		return enterpriseConcurrentGroup;
	}
	public void setEnterpriseConcurrentGroup(String enterpriseConcurrentGroup) {
		this.enterpriseConcurrentGroup = enterpriseConcurrentGroup;
	}
	public String getConcurrentOnlineGroup() {
		return concurrentOnlineGroup;
	}
	public void setConcurrentOnlineGroup(String concurrentOnlineGroup) {
		this.concurrentOnlineGroup = concurrentOnlineGroup;
	}
	
	public String getProfessionalConcurrentGroup() {
		return professionalConcurrentGroup;
	}
	public void setProfessionalConcurrentGroup(String professionalConcurrentGroup) {
		this.professionalConcurrentGroup = professionalConcurrentGroup;
	}
	

	public String getPublicWebAccess() {
		return publicWAGroup;
	}
	public void setPublicWebAccess(String publicWA) {
		// TODO Auto-generated method stub
		this.publicWAGroup=publicWA;
	}
	/**
	 * Enhancment:- CV10 Added to enable and disable the adminwise backup/restore tabs
	 * @return
	 */
	public int getTabStatus() {
		return tabStatus;
	}
	public void setTabStatus(int tabStatus) {
		this.tabStatus = tabStatus;
	}
	public int getToggleStatus() {
		return togglestatus;
	}
	public void setToggleStatus(int togglestatus) {
		this.togglestatus = togglestatus;
	}
	
	public boolean getIsEncryptedKey() {
		return isEncryptedKey;
	}
	
	public void setIsEncryptedKey(boolean isEncryptedKey) {
		this.isEncryptedKey = isEncryptedKey;
	}
	public String getSubAdmins() {
		return subAdmins;
	}
	public void setSubAdmins(String subAdmins) {
		this.subAdmins = subAdmins;
	}
	public String getBatchInputAdministrator() {
		return batchInputAdministrator;
	}
	public void setBatchInputAdministrator(String batchInputAdministrator) {
		this.batchInputAdministrator = batchInputAdministrator;
	}
	public String getSqlConnectionString() {
		return sqlConnectionString;
	}
	public void setSqlConnectionString(String sqlConnectionString) {
		this.sqlConnectionString = sqlConnectionString;
	}
	public String getAuthenticationMode() {
		return authenticationMode;
	}
	public void setAuthenticationMode(String authenticationMode) {
		this.authenticationMode = authenticationMode;
	}
	public boolean isIntegratedSecurity() {
		return integratedSecurity;
	}
	public void setIntegratedSecurity(boolean integratedSecurity) {
		this.integratedSecurity = integratedSecurity;
	}
	public String getAuthenticationScheme() {
		return authenticationScheme;
	}
	public void setAuthenticationScheme(String authenticationScheme) {
		this.authenticationScheme = authenticationScheme;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getServerSpn() {
		return serverSpn;
	}
	public void setServerSpn(String serverSpn) {
		this.serverSpn = serverSpn;
	}
}
