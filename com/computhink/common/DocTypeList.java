package com.computhink.common;

import java.io.Serializable;

/**
 * @author :Gurumurthy.T.S
 * Desc    : This bean holds the value for DocumentType list.
 */
	public class DocTypeList implements Serializable{
		private int id = 0;
		private String name = "";

		public DocTypeList(int id,String name){
			this.id = id;
			this.name = name;
		}
		
		/**
		 * @return Returns the id.
		 */
		public int getId() {
			return id;
		}
		/**
		 * @param id The id to set.
		 */
		public void setId(int id) {
			this.id = id;
		}
		/**
		 * @return Returns the name.
		 */
		public String getName() {
			return name;
		}
		/**
		 * @param name The name to set.
		 */
		public void setName(String name) {
			this.name = name;
		}
		public String toString(){
			return this.name;
		}
	
	}
