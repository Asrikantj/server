/*
 * DocComment.java
 *
 * Created on February 18, 2004, 4:28 PM
 */

package com.computhink.common;

import java.util.StringTokenizer;
import java.io.Serializable;
/**
 *
 * @author  Administrator
 */
public class DocComment implements Serializable
{
     private int id;
     private int docId;
     private String comment;
     private String user;
     private String timeStamp;
     private boolean publicScope; 
    
    public DocComment() 
    {
    }
    public DocComment(String dbstring) {
        
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        this.id = Util.to_Number(st.nextToken());
        this.docId =Util.to_Number(st.nextToken());
        this.user =st.nextToken();
        this.timeStamp =st.nextToken();
        this.publicScope =(Util.to_Number(st.nextToken()) ==1) ? true : false ;
        this.comment =st.nextToken();
        while(st.hasMoreTokens())
        {
            this.comment +="\t"+st.nextToken();
        }    
    }
    public int getId() {
        return this.id ;
    }
    
   
    public void setId(int id) {
        this.id =id;
    }
    
    
    public int getDocId() {
        return this.docId;
    }
   
    public void setDocId(int docId) {
        this.docId =docId;
    }
    
    
    public String getComment() {
        return this.comment;
    }
    
    
    public void setComment(String comment) {
        this.comment =comment;
    }
  
    public String getTimeStamp() {
        return this.timeStamp;
    }
    
    
    public void setTimeStamp(String timeStamp) {
        this.timeStamp =timeStamp;
    }
    
   
    public String getUser() {
        return this.user;
    }
    
    
    public void setUser(String user) {
        this.user =user;
    }
    
    public boolean getPublic() {
        return this.publicScope;
    }
    
    
    public void setPublic(boolean scope) {
        this.publicScope =scope;
    }
    
}
