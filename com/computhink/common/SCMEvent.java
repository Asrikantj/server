
package com.computhink.common;

public class SCMEvent
{
    public static final int SERVICE_STOPPED = 1;
    
    private int id;

    public SCMEvent(int code)
    {
        id = code;
    }

    public int getID()
    {
        return id;
    }
}
