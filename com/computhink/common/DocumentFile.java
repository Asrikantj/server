
package com.computhink.common;

public class DocumentFile implements java.io.Serializable
{

    private int nChunkSize=0;
    private byte[] fileData=null;
    private boolean bLastPacket=false;
    
    public DocumentFile(int chunkSize)
    {
        nChunkSize=chunkSize;
        bLastPacket=false;
    }
    
    public void setFileData(byte[] data)
    {
        fileData=data;
    }

    public byte[] getFileData()
    {
        return fileData;
    }
    
    public int getChunkSize()
    {
        return nChunkSize;
    }
    
    public void setChunkSize(int newChunkSize)
    {
        nChunkSize=newChunkSize;
    }
    
    public void setLastPacket(boolean isLastPacket)
    {
        bLastPacket=isLastPacket;
    }

    public boolean getLastPacket()
    {
        return bLastPacket;
    }
}
