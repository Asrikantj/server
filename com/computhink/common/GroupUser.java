package com.computhink.common;

public class GroupUser
{
    private int id = 0;
    protected int groupId = 0;
    protected int userId = 0;

    public GroupUser()
    {
    }
    public GroupUser(int id)
    {
        this.id=id;
    }
    public int getGroupId()
    {
        return groupId;
    }
    public void setGroupId(int groupId)
    {
        this.groupId = groupId;
    }
    public int getUserId()
    {
        return userId;
    }
    public void setUserId(int userId)
    {
        this.userId = userId;
    }
    public int getId()
    {
        return id;
    }
}



