
package com.computhink.common;

import java.util.Vector;

public class OutputOptions implements java.io.Serializable {
    private String selection;
    private int defaultAction;
    private Vector docOptions;
    private Document docobj;
    
    public void OutputOptions() {
        
    }
    public java.lang.String getSelection() {
        return selection;
    }
    
    public void setSelection(java.lang.String selection) {
        this.selection = selection;
    }
    
    public int getDefaultAction() {
        return defaultAction;
    }
    /*
     * 0 Export
     * 1 Mail
     * 2 Print/Fax
     */
    public void setDefaultAction(int defaultAction) {
        this.defaultAction = defaultAction;
    }
    
    public Vector getDocOptions() {
        return docOptions;
    }
    
    public void setDocOptions(Vector docOptions) {
        this.docOptions = docOptions;
    }
    public void setDocument(Document obj){
        this.docobj=obj;
    }
    public Document getDocument(){
        return this.docobj;
    }
}
