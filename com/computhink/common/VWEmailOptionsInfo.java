package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;

import com.computhink.common.Util;

/**
 * @author Vijaypriya.k
 * December 07, 2010
 */
public class VWEmailOptionsInfo implements Serializable{
	public int routeId = 0;
	public String userEmailAddress=null;
	public int documentMetaDataFlag=1;
	public int routeSummaryReportFlag=1;
	public int documentPagesFlag=1;
	public String reportLocation=null;
	public int sendMail=0;
	public int documentVwrFlag = 0;
	public boolean isEditableFlag = true;
	
	public VWEmailOptionsInfo(){
		
	}
	public VWEmailOptionsInfo(String rowValue){
		StringTokenizer st = new StringTokenizer(rowValue, ",");
		try{
			this.userEmailAddress = st.nextToken();
			this.documentMetaDataFlag = Util.to_Number(st.nextToken());
			this.routeSummaryReportFlag = Util.to_Number(st.nextToken());
			this.documentPagesFlag = Util.to_Number(st.nextToken());
			this.documentVwrFlag = Util.to_Number(st.nextToken());
		}catch(Exception ex){
			
		}
	}
	/**
	 * @return userEmailAddress
	 */
	public String getUserEmailAddress() {
		return userEmailAddress;
	}
	/**
	 * @param userEmailAddress
	 */
	public void setUserEmailAddress(String userEmailAddress) {
		this.userEmailAddress = userEmailAddress;
	}
	/**
	 * @return documentMetaDataFlag
	 */
	public int getDocumentMetaDataFlag() {
		return documentMetaDataFlag;
	}
	/**
	 * @param documentMetaDataFlag
	 */
	public void setDocumentMetaDataFlag(int documentMetaDataFlag) {
		this.documentMetaDataFlag = documentMetaDataFlag;
	}
	/**
	 * @return documentPagesFlag
	 */
	public int getDocumentPagesFlag() {
		return documentPagesFlag;
	}
	/**
	 * @param documentPagesFlag
	 */
	public void setDocumentPagesFlag(int documentPagesFlag) {
		this.documentPagesFlag = documentPagesFlag;
	}
	/**
	 * @return routeSummaryReportFlag
	 */
	public int getRouteSummaryReportFlag() {
		return routeSummaryReportFlag;
	}
	/**
	 * @param routeSummaryReportFlag
	 */
	public void setRouteSummaryReportFlag(int routeSummaryReportFlag) {
		this.routeSummaryReportFlag = routeSummaryReportFlag;
	}
	public String getReportLocation() {
		return reportLocation;
	}
	public void setReportLocation(String reportLocation) {
		this.reportLocation = reportLocation;
	}
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public int getSendMail() {
		return sendMail;
	}
	public void setSendMail(int sendMail) {
		this.sendMail = sendMail;
	}
	/****CV2019 merges - SIDBI - VWR enhancement changes***/
	public int getDocumentVwrFlag() {
		return documentVwrFlag;
	}
	public void setDocumentVwrFlag(int documentVwrFlag) {
		this.documentVwrFlag = documentVwrFlag;
	}
	public boolean isEditableFlag() {
		return isEditableFlag;
	}
	public void setEditableFlag(boolean isEditableFlag) {
		this.isEditableFlag = isEditableFlag;
	}		
}
