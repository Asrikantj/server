package com.computhink.common.login;

import java.awt.Dimension;
import javax.swing.JDialog;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import java.awt.Frame;
import java.awt.BorderLayout;
import java.awt.Toolkit;

import javax.swing.border.TitledBorder;

import com.computhink.common.RouteMasterInfo;
import com.computhink.common.Signature;
import com.computhink.common.util.VWLinkedButton;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.VWRouteAction;
import com.computhink.vwc.VWUtil;
import com.computhink.vwc.image.Images;
import com.computhink.resource.ResourceManager;
import java.awt.Insets;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.prefs.*;

public class VWLoginScreen extends javax.swing.JDialog implements VWLoginConstant, Runnable
{
	private ResourceManager resourceManager=null;
	public VWLoginScreen(VWClient vwc, int sessionId, String title, String user, int docId, RouteMasterInfo routeMasterInfo, int action, String documentName, int type)
	{
            this.sessionId = sessionId;
            this.vwc = vwc;
            this.routeMasterInfo = routeMasterInfo;
            this.action = action;
            this.documentName = documentName;
            this.type = type;
            this.docId = docId;
            this.user = user;
            setTitle(title);
            new Images();
            initComponents();
            setVisible(true);
            active = true;
	}
	public VWLoginScreen(){
        new Images();
        initComponents();
        setVisible(true);
        active = true;	
	}
	public void initComponents(){
		resourceManager=ResourceManager.getDefaultManager();
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        getContentPane().setLayout(null);
        setSize(405,175);
        setVisible(false);
        PnlGeneral.setBorder(new javax.swing.border.TitledBorder(lbl_Border_Title));
        PnlGeneral.setLayout(null);
        getContentPane().add(PnlGeneral);
        PnlGeneral.setBounds(6,8,388,92);
        JLabel1.setText(lbl_UserName);
        PnlGeneral.add(JLabel1);
        JLabel1.setBounds(12,24,78, 22);
        JLabel2.setText(lbl_Password);
        PnlGeneral.add(JLabel2);
        JLabel2.setBounds(12,52,68,28);
        
        PnlGeneral.add(TxtUserName);
        TxtUserName.setBounds(108,24,272,20);
        TxtUserName.setText(user);
        TxtUserName.setEditable(false);
        PnlGeneral.add(TxtPassword);
        TxtPassword.setBounds(108,56,272,20);
        
        
        BtnLogin.setText(BTN_LOGIN_NAME);
        BtnLogin.setActionCommand(BTN_LOGIN_NAME);
        getContentPane().add(BtnLogin);            
        BtnLogin.setBounds(205,114,88,25);//(89,128,73,25);
        BtnLogin.setIcon(Images.connectIcon);
        
        BtnCancel.setText(BTN_CANCEL_NAME);
        BtnCancel.setActionCommand(BTN_CANCEL_NAME);
        getContentPane().add(BtnCancel);
        BtnCancel.setBounds(300,114,88,25);//(233,128,73,25); //RdoAllRoom.setBounds(296,86,94,18);
        BtnCancel.setIcon(Images.closeIcon);

        SymAction lSymAction = new SymAction();
        BtnLogin.addActionListener(lSymAction);
        BtnCancel.addActionListener(lSymAction);
        SymKey aSymKey = new SymKey();
        addKeyListener(aSymKey);
        TxtUserName.addKeyListener(aSymKey);
        TxtPassword.addKeyListener(aSymKey);
        BtnLogin.addKeyListener(aSymKey);
        BtnCancel.addKeyListener(aSymKey);
        getDlgOptions();
        setResizable(false);
        addWindowListener(new SymWindow());		
	}
	public void run(){
		setVisible(true);
	}
	
	class SymWindow extends WindowAdapter{
        public void windowLostFocus(WindowEvent e){
        	if(active){
            	requestFocus();
                toFront();
        	}
        }
        public void windowDeactivated(WindowEvent e) {
        	if(active){
        		requestFocus();
        		toFront();
        	}
        }
        public void windowStateChanged(WindowEvent e) {
        	if(active){	
        		toFront();
        	}
        }
         public void windowDeiconified(WindowEvent e) {
        	 if(active){
        		toFront();
        	 }
        }
         public void windowClosing(WindowEvent e) {                	 
        	 closeDialog();
         }
         
 }	
    private void closeDialog() 
    {
        active = false;
        setVisible(false);
        dispose();        
    }
	
//------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
        public void keyTyped(java.awt.event.KeyEvent event)
        {
            Object object = event.getSource();
            if(event.getKeyText(event.getKeyChar()).equals("Enter"))
            {
                BtnLogin.requestFocus();
                BtnLogin_actionPerformed(null);
            }
            else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
            {
                BtnCancel.requestFocus();
                BtnCancel_actionPerformed(null);
            }
        }
    }
//------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
        public void actionPerformed(java.awt.event.ActionEvent event)
        {
                Object object = event.getSource();
                if (object == BtnLogin)
                        BtnLogin_actionPerformed(event);
                else if (object == BtnCancel)
                        BtnCancel_actionPerformed(event);			
        }
    }

//------------------------------------------------------------------------------
    void BtnLogin_actionPerformed(java.awt.event.ActionEvent event)
    {
    	String input = new String((char[])TxtPassword.getPassword());
    	if (input.trim().length() == 0){
    		JOptionPane.showMessageDialog(this,resourceManager.getString("LoginMsg.BlankPassword"));
    		vwc.printToConsole("Password should not be blank!");
    	}else{
    		cancelFlag=false;
    		int output = vwc.authenticateUser(sessionId, TxtUserName.getText(), input);
    		vwc.printToConsole("Autheticate the User : " + output);
    		String templateStr = "";
    		int result = 0;
    		if (output == 1){
    			vwc.printToConsole("Authenticate passed");
    			Signature sign = new Signature();
    			if (routeMasterInfo.getRouteUserSignType() == 2){
    				closeDialog();
    				sign.setDocId(docId);
    				sign.setUserName(user);
    				sign.setType(Signature.Template_Type);
    				//if (vwc.isUserHasSign(sessionId, routeMasterInfo.getRouteUserSignType()) >= 1)
    				{
    				vwc.printToConsole("Certify type and get the user sign");
    				vwc.getUserSign(sessionId, sign);
    				templateStr=VWCUtil.getStrFromFile(new File(sign.getSignFilePath()));
    				//sign.setType(Signature.Template_Type);
    				result=VWCUtil.validateSign(templateStr,0);
    				}
/*    				else{
    					result = 0;
    					JOptionPane.showMessageDialog(this, "Signature is not registered for this user.\n Please contact ViewWise Adminstrator");
    				}
*/    				//result = checkCertifySign(sign);
    				vwc.printToConsole("Verified the signature " + result);
    				sign.setType(Signature.Certified_Type);

    			}else{
    				vwc.printToConsole("Authorized USer");
    				sign.setDocId(docId);
    				sign.setUserName(user);
    				sign.setType(Signature.Authorized_Type);
    				result = 1;
    			}
    			if (result == 1){
    				vwc.printToConsole("Opening the "+ WORKFLOW_MODULE_NAME.toLowerCase() +" status");
    				VWRouteAction routeStatus = new VWRouteAction(vwc, sessionId, docId, routeMasterInfo, action, documentName, type, sign);
    				new Thread(routeStatus).start();
    			}    			 
    			closeDialog();
    		}else{
    			JOptionPane.showMessageDialog(this, resourceManager.getString("LoginMsg.FailedToAuthenticate"));
    		}
    	}
    }

	private int checkCertifySign(Signature sign){
		vwc.printToConsole("checkcertify sign");
		sign.setUserName(vwc.getSession(sessionId).user);
		sign.setType(Signature.Template_Type);
		vwc.printToConsole("check user has sign");
		vwc.getUserSign(sessionId, sign);
		String templateStr=VWCUtil.getStrFromFile(new File(sign.getSignFilePath()));
		vwc.printToConsole("template str" + templateStr);
		int result=VWCUtil.validateSign(templateStr,0);
		return result;
	}
	
//------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
        cancelFlag=true;
        setVisible(false);
        closeDialog();
    }
//------------------------------------------------------------------------------
    public String[] getValues()
    {
        String[] values=new String[3];
        values[0]=TxtUserName.getText();
        
        values[1]=String.valueOf(TxtPassword.getPassword());
        /*
    	 * new TxtPassword.getText(); method is replaced with new String.valueOf(TxtPassword.getPassword()); 
    	 * as Jpasswordfield.getText(); method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
    	 */
       // values[2]=RdoThisRoom.isSelected()?"0":"1";
        return values;
    }
//------------------------------------------------------------------------------
    public boolean getCancelFlag()
    {
        return cancelFlag;
    }
//------------------------------------------------------------------------------
    private void saveDlgOptions()
    {
/*        Preferences prefs = Preferences.userNodeForPackage(getClass());
        if(AdminWise.adminPanel.saveDialogPos)
        {
            prefs.putInt( "x", this.getX());
            prefs.putInt( "y", this.getY());
        }
        if(AdminWise.adminPanel.saveLastUserName)
            prefs.put("UserName",TxtUserName.getText());*/
    }
//------------------------------------------------------------------------------
    private void getDlgOptions()
    {
        Preferences prefs = Preferences.userNodeForPackage(getClass());
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();//VWUtil.getScreenSize();
        int x=prefs.getInt( "x",d.width/4);
        int y=prefs.getInt( "y",d.height/3);
        setLocation(x,y);
    }
    public static void main(String[] args) {
    	String title = "test";
/*    	Frame frame = new Frame();
    	frame.setVisible(true);
    	frame.setSize(200, 300);*/
    	String[] values = {"admin","vw","1"};
		VWLoginScreen login = new VWLoginScreen();
	}
      
//  ------------------------------------------------------------------------------
	//{{DECLARE_CONTROLS
	javax.swing.JPanel PnlGeneral = new javax.swing.JPanel();
	javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
	javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
	javax.swing.JTextField TxtUserName = new javax.swing.JTextField(50);
	javax.swing.JPasswordField TxtPassword = new javax.swing.JPasswordField(50);
	JButton BtnLogin = new JButton();
    JButton BtnCancel = new JButton();
    
    boolean cancelFlag=true;
    boolean active = false;    
    public int sessionId = 0;
    String user = "";
    VWClient vwc = null;
    VWRouteAction parent = null;
    RouteMasterInfo routeMasterInfo = null;
    int action = 0;
    String documentName = "";
    int type =0;
    int docId = 0;    
}