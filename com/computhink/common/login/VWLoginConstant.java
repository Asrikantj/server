package com.computhink.common.login;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;
public interface VWLoginConstant extends Constants{
	    String[] YesNoData={"Yes","No"};
	    String LBL_DIALOG_NAME=PRODUCT_NAME + " admin login";
	    String lbl_ViewType_ThisRoom=ResourceManager.getDefaultManager().getString("LoginLbl.ThisRoom");
	    String lbl_ViewType_AllRooms=ResourceManager.getDefaultManager().getString("LoginLbl.AllRooms");
	    String lbl_Border_Title=ResourceManager.getDefaultManager().getString("LoginLbl.Border_Title");
	    String lbl_UseFor=ResourceManager.getDefaultManager().getString("LoginLbl.UserFor");
	    String lbl_UserName=ResourceManager.getDefaultManager().getString("LoginLbl.UserName");
	    String lbl_Password=ResourceManager.getDefaultManager().getString("LoginLbl.Password");
	    String BTN_LOGIN_NAME=ResourceManager.getDefaultManager().getString("LoginBtn.LOGIN");
	    String BTN_CANCEL_NAME=ResourceManager.getDefaultManager().getString("LoginBtn.CANCEL");
}
