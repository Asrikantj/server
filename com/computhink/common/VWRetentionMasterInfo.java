package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;

public class VWRetentionMasterInfo implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int retentionId;
	private String retentionName;
	private int documentId;
	private String documentName;
	private int docTypeId;
	private String docTypeName;
	private String creatorName;
	private String processedDate;
	
	public VWRetentionMasterInfo(int retentionId) {
		this(retentionId, "");
	}
	public VWRetentionMasterInfo(String dbstring) {
		//50 Report2 55 108 3 RetDocType ARSAdmin Mar  2 2010  5:13PM
		System.out.println("Inside VWRetentionMasterInfo : "+dbstring);
		StringTokenizer st = new StringTokenizer(dbstring, "\t");
		try {
			this.retentionId = Util.to_Number(st.nextToken());
			this.retentionName = st.nextToken();
			this.documentId = Util.to_Number(st.nextToken());
			this.documentName = st.nextToken();
			this.docTypeId = Util.to_Number(st.nextToken());
			this.docTypeName = st.nextToken();
			this.creatorName = st.nextToken();
			this.processedDate = st.nextToken();
		} catch (Exception e) {
			System.out.println("Exception in VWRetentionMasterInfo : "+e.getMessage());
			this.documentId = -1;
		}
	}
	public VWRetentionMasterInfo(int retentionId, String docName) {
		this.retentionId = retentionId;
		this.documentName = docName;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public int getDocTypeId() {
		return docTypeId;
	}
	public void setDocTypeId(int docTypeId) {
		this.docTypeId = docTypeId;
	}
	public String getDocTypeName() {
		return docTypeName;
	}
	public void setDocTypeName(String docTypeName) {
		this.docTypeName = docTypeName;
	}
	public int getDocumentId() {
		return documentId;
	}
	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}
	public int getRetentionId() {
		return retentionId;
	}
	public void setRetentionId(int retentionId) {
		this.retentionId = retentionId;
	}
	public String getRetentionName() {
		return retentionName;
	}
	public void setRetentionName(String retentionName) {
		this.retentionName = retentionName;
	}
}
