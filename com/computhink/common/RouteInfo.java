package com.computhink.common;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.Vector;

import com.computhink.vws.server.VWSLog;

public class RouteInfo extends Object implements Serializable, Comparable {
	private int routeId;
	private String routeName;
	private String startAction = "";
	private String startLocation = "";
	private String rejectLocation = "";
	private String processType = "";
	private String routeActionId = "";
	private String actionLocation = "";
	private int routeUsed = 0;
	private String createdDate = "";
	private String modifiedDate = "";
	private ArrayList listOfRouteUsers = null;
	private ArrayList RouteTaskList = null;
	private ArrayList RouteIndexList = null;
	private String routeDescription = "";
	private String routeImage = "";
	private String ImageFilePath = "";
	private int sendEmailOnRptGen = 0;
	private String emailIDs = "";//Old functionality
	private String reportLocation = "";
	private String indexName = "";
	private int indexId = 0;
	private String indexValue = "";
	private int emailOriginator = 0;
	private int compressdoc = 0;
	private int pdfConversion=0;


	private Vector RouteEmailOptionsInfoList = null;

	public RouteInfo(int routeId) {
		this(routeId, "");
	}

	public RouteInfo(String dbstring) {
		
		StringTokenizer st = new StringTokenizer(dbstring, "\t");
		/* Sample Data
		 * [5	Route200	Route200 ddddd	1	-1	0	-	0	2008-03-14 12:50:48	2008-03-14 12:50:48	    org.jhotdraw.samples.javadraw.BouncingDrawing..]
		
		/ * Cast(id AS VARCHAR)			+ CHAR(9) +
			ISNULL(Name,' ')			+ CHAR(9) +
			Case When Len(RouteDescription)>0 Then Rtrim(RouteDescription) Else '-' End	+ CHAR(9) +
			ISNULL(StartAction,' ')			+ CHAR(9) +
			ISNULL(StartLocation,'-')		+ CHAR(9) +
			Cast(RouteActionid AS Varchar)		+ CHAR(9) +
			Case When Len(ActionLocation)>0 Then Rtrim(ActionLocation) Else '-' End		+ CHAR(9) +
			Cast(Used AS Varchar)			+ CHAR(9) +
			ISNULL(Created,' ')                     + CHAR(9) +
			ISNULL(Modified,' ')                    + CHAR(9) +
			ISNULL(RouteImage,'-')			+ CHAR(9) AS Fld
		 * */
		try {
			this.routeId = Util.to_Number(st.nextToken());
			this.routeName = st.nextToken();
			this.routeDescription = st.nextToken();
			if(this.routeDescription.equalsIgnoreCase("-"))
				this.routeDescription = "";
			this.startAction = st.nextToken();
			this.startLocation = st.nextToken();
			if(this.startLocation.equalsIgnoreCase("-"))
				this.startLocation = "";
			//this.processType = st.nextToken();
			this.routeActionId = st.nextToken();
			this.actionLocation = st.nextToken();
			if(this.actionLocation.equalsIgnoreCase("-"))
				actionLocation = "";
			this.routeUsed = Util.to_Number(st.nextToken());
			this.createdDate = st.nextToken();
			this.modifiedDate = st.nextToken();
			this.reportLocation = st.nextToken();
			this.rejectLocation=st.nextToken();
			this.emailOriginator=Util.to_Number(st.nextToken());
			this.compressdoc=Util.to_Number(st.nextToken());
			this.pdfConversion=Util.to_Number(st.nextToken());
			this.sendEmailOnRptGen = Util.to_Number(st.nextToken());
			//this.emailIDs = st.nextToken();//Old functionality
			//Route email option enhancement
			this.RouteEmailOptionsInfoList = new Vector();
			this.RouteEmailOptionsInfoList = getEmailOptionsInfoVector(st.nextToken());						
		} catch (Exception e) {
		}
	}

	public RouteInfo(int routeId, String routeName) {
		this.routeId = routeId;
		this.routeName = routeName;
	}

	public int compareTo(Object rInfo) {
        if(this.getRouteName().compareTo(((RouteInfo)rInfo).getRouteName())>0)
            return 1;
        else if(this.getRouteName().compareTo(((RouteInfo)rInfo).getRouteName())<0)
            return -1;
        else return 0;
    }
	
	public String getActionLocation() {
		return actionLocation;
	}

	public void setActionLocation(String actionLocation) {
		this.actionLocation = actionLocation;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getProcessType() {
		return processType;
	}

	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public String getRouteActionId() {
		return routeActionId;
	}

	public void setRouteActionId(String routeActionId) {
		this.routeActionId = routeActionId;
	}

	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		if(routeName.contains("\t")){
			routeName=routeName.replaceAll("\t"," ");
		}
		this.routeName = routeName;
	}

	public String getStartAction() {
		return startAction;
	}

	public void setStartAction(String startAction) {
		this.startAction = startAction;
	}

	public String getStartLocation() {
		return startLocation;
	}

	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}

	public String getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	/*public RouteUsers getRouteUsers() {
		return routeUsers;
	}

	public void setRouteUsers(RouteUsers routeUsers) {
		this.routeUsers = routeUsers;
	}*/

	public ArrayList getListOfRouteUsers() {
		return this.listOfRouteUsers;
	}

	public void setListOfRouteUsers(ArrayList listOfRouteUsers) {
		/*
		 * if(this.listOfRouteUsers == null){ listOfRouteUsers = new
		 * ArrayList(); } listOfRouteUsers.add(routeUsers);
		 */
		this.listOfRouteUsers = listOfRouteUsers;
	}

	public String toString() {
		return this.routeName;
	}

	public String getRouteDescription() {
		return routeDescription;
	}

	public void setRouteDescription(String routeDescription) {
		if(routeDescription.contains("\t")){
			routeDescription=routeDescription.replaceAll("\t"," ");
		}
		this.routeDescription = routeDescription;
	}

	public String getRouteImage() {
		return routeImage;
	}

	public void setRouteImage(String routeImage) {
		this.routeImage = routeImage;
	}

	public ArrayList getRouteTaskList() {
		return RouteTaskList;
	}

	public void setRouteTaskList(ArrayList routeTaskList) {
		RouteTaskList = routeTaskList;
	}

	public ArrayList getRouteIndexList() {
		return RouteIndexList;
	}

	public void setRouteIndexList(ArrayList routeIndexList) {
		RouteIndexList = routeIndexList;
	}

	public int getRouteUsed() {
		return routeUsed;
	}

	public void setRouteUsed(int routeUsed) {
		this.routeUsed = routeUsed;
	}
	public static File makeRouteImgFilePath(String cashDir ,int routeId)
    {
        if(cashDir==null || cashDir.equals(""))
            cashDir=System.getProperty("java.io.tmpdir");
        cashDir=cashDir+(cashDir.endsWith(Util.pathSep)?"":Util.pathSep);
        File routeImageFolder=new File(cashDir+"RouteImage");
        routeImageFolder.mkdirs();
        File routeImgFile=null;
        routeImgFile=new File(routeImageFolder,"RouteImage.draw");
        return routeImgFile;
    }
	public String getImageFilePath() {
		return ImageFilePath;
	}
	public void setImageFilePath(String imageFilePath) {
		ImageFilePath = imageFilePath;
	}

	/**
	 * @return Returns the emailIDs.
	 */
	public String getEmailIDs() {
		return emailIDs;
	}

	/**
	 * @param emailIDs The emailIDs to set.
	 */
	public void setEmailIDs(String emailIDs) {
		this.emailIDs = emailIDs;
	}

	/**
	 * @return Returns the reportLocation.
	 */
	public String getReportLocation() {
		return reportLocation;
	}

	/**
	 * @param reportLocation The reportLocation to set.
	 */
	public void setReportLocation(String reportLocation) {
		this.reportLocation = reportLocation;
	}

	/**
	 * @return Returns the sendEmailOnRptGen.
	 */
	public int getSendEmailOnRptGen() {
		return sendEmailOnRptGen;
	}

	/**
	 * @param sendEmailOnRptGen The sendEmailOnRptGen to set.
	 */
	public void setSendEmailOnRptGen(int sendEmailOnRptGen) {
		this.sendEmailOnRptGen = sendEmailOnRptGen;
	}

	/**
	 * @return Returns the indexName.
	 */
	public String getIndexName() {
		return indexName;
	}

	/**
	 * @param indexName The indexName to set.
	 */
	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public int getIndexId() {
		return indexId;
	}

	public void setIndexId(int indexId) {
		this.indexId = indexId;
	}

	public String getIndexValue() {
		return indexValue;
	}

	public void setIndexValue(String indexValue) {
		this.indexValue = indexValue;
	}
	/**
	 * @return RouteEmailOptionsInfoList - Bean of VWEmailOptionsInfo
	 */
	public Vector getRouteEmailOptionsInfoList() {
		return RouteEmailOptionsInfoList;
	}
	/**
	 * @param routeEmailOptionsInfoList
	 */
	public void setRouteEmailOptionsInfoList(Vector routeEmailOptionsInfoList) {
		RouteEmailOptionsInfoList = routeEmailOptionsInfoList;
	}
	private Vector<VWEmailOptionsInfo> getEmailOptionsInfoVector(String vwEmailOptionsInfoStr) {
		
		Vector<VWEmailOptionsInfo> vwEmailOptionsList = new Vector<VWEmailOptionsInfo>();
		try{
			String tempEmailOptionsInfoRow[] = vwEmailOptionsInfoStr.split("\\|");
			for(int i=0; i<tempEmailOptionsInfoRow.length; i++){
				VWEmailOptionsInfo vwEmailOptionsInfo = new VWEmailOptionsInfo(tempEmailOptionsInfoRow[i]);
				if(tempEmailOptionsInfoRow.length==1){//This check it to avoid the empty email options 
					if(!vwEmailOptionsInfo.getUserEmailAddress().equals("-")){
						/****CV2019 merges from SIDBI line***/
						if (vwEmailOptionsInfo.getDocumentVwrFlag() == 1) {
							vwEmailOptionsInfo.setEditableFlag(false);
						}
						/****End of SIDBI line--------------------------------***/
						vwEmailOptionsList.add(vwEmailOptionsInfo);
					}
				}else{
					vwEmailOptionsList.add(vwEmailOptionsInfo);
				}
			}
		}catch(Exception ex){

		}
		return vwEmailOptionsList;
	}
	/*
	 * Enhancement:-WorkFlow Resubmit
	 * Added to get the set the Reject location from administration End WorkFlow Properties
	 */
	public String getRejectLocation() {
		return rejectLocation;
	}

	public void setRejectLocation(String rejectLocation) {
		this.rejectLocation = rejectLocation;
	}

	public int getEmailOriginator() {
		return emailOriginator;
	}

	public void setEmailOriginator(int emailOriginator) {
		this.emailOriginator = emailOriginator;
	}

	public int getCompressdoc() {
		return compressdoc;
	}

	public void setCompressdoc(int compressdoc) {
		this.compressdoc = compressdoc;
	}

	public int getPdfConversion() {
		return pdfConversion;
	}

	public void setPdfConversion(int pdfConversion) {
		this.pdfConversion = pdfConversion;
	}

}
