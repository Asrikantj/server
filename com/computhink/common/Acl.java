/*
 * Acl.java
 *
 * Created on January 28, 2004, 5:20 PM
 */
package com.computhink.common;
/**
 *
 * @author  Administrator
 */
import java.util.*;

public class Acl implements java.io.Serializable
{
    private int id;
    private Node node;
    private Vector entries = new Vector();
    private HashMap principalMap = new HashMap();

    public Acl(Node node)
    {
        this.node = node;
        this.id = node.getAclId();
    }
    public Acl(Document doc)
    {
        this.node = new Node(doc.getId());
        this.id = node.getAclId();
        node.setType(Node.DOCUMENT_TYPE);
    }
    public AclEntry newEntry(Principal p)
    {
        AclEntry entry = new AclEntry(this, p); 
        entries.add(entry);
        principalMap.put(p, entry);
        return entry;
    }
    public void addEntry(AclEntry entry)
    {   
        if (entry == null || entry.getPrincipal() == null) return;
        entries.add(entry);
        principalMap.put(entry.getPrincipal(), entry);
    }
    public void removeEntry(AclEntry entry)
    {
        entries.remove(entry);
        Principal principal = entry.getPrincipal();
        principalMap.remove(principal);
    }
    public Node getNode()
    {
        return this.node;
    }
    public int getNodeId()
    {
        return this.node.getId();
    }
    public Vector getEntries()
    {
        return entries;
    }
    public AclEntry getEntry(Principal p)
    {
        AclEntry curAclEntry= (AclEntry) principalMap.get(p);
        if(curAclEntry == null)
        {
            Object[] principals=principalMap.keySet().toArray();
            int count=principals.length;
            for(int i=0;i<count;i++)
            {
                if(((Principal) principals[i]).getName().
                                                  equalsIgnoreCase(p.getName()))
                {
                    curAclEntry=(AclEntry) principalMap.get(principals[i]); 
                    break;
                }
            }
        }
        return curAclEntry;   
    }

    public void set(Acl newAcl)
    {
        this.node = newAcl.getNode();
        entries.clear();
        principalMap.clear();
        Iterator iterator = newAcl.getEntries().iterator();
        while (iterator.hasNext())
        {
            AclEntry entry = (AclEntry) iterator.next();
            addEntry(entry);
        }
    } 
    public int getId()
    {
        return this.id;
    }
    public void setId(int id) 
    {
        this.id = id;
    }
}