/*
 * VWClientListener.java
 *
 * Created on March 11, 2004, 12:28 PM
 */

package com.computhink.common;
/**
 *
 * @author  Pandiya Raj.M
 */
public class VWItemCheckEvent extends java.util.EventObject   
{
    private int itemIndex=0;
    private boolean checked=false;
    private Object object=null;
    public VWItemCheckEvent(Object source) 
    {
        super(source);
    }

    /** Getter for property itemIndex.
     * @return Value of property itemIndex.
     */
    public int getItemIndex() {
        return itemIndex;
    }
    
    /** Setter for property itemIndex.
     * @param itemIndex New value of property itemIndex.
     */
    public void setItemIndex(int itemIndex) {
        this.itemIndex = itemIndex;
    }
    
    /** Getter for property checked.
     * @return Value of property checked.
     */
    public boolean isChecked() {
        return checked;
    }
    
    /** Setter for property checked.
     * @param checked New value of property checked.
     */
    public void setChecked(boolean checked) {
        this.checked = checked;
    }
    
    /** Getter for property object.
     * @return Value of property object.
     */
    public java.lang.Object getObject() {
        return object;
    }
    
    /** Setter for property object.
     * @param object New value of property object.
     */
    public void setObject(java.lang.Object object) {
        this.object = object;
    }
    
}
