/*
 * Index.java
 *
 * Created on December 28, 2003, 11:50 AM
 */

package com.computhink.common;

/**
 *
 * @author  Administrator
 */
import java.util.*;

public class Index implements java.io.Serializable
{
    public static final int IDX_TEXT = 0;
    public static final int IDX_NUMBER = 1;
    public static final int IDX_DATE = 2;
    public static final int IDX_BOOLEAN = 3;
    public static final int IDX_SELECTION = 4;
    public static final int IDX_SEQUENCE = 5;
    private int selectValue = 0;
    private int id = 0;
    private int DTIndexId=0;
    private int order = 0;
    private int type = 0;
    private String name = "";
    private String description = "";
    private String info = "";
    private String value = "";
    private String actValue = "";
    private Vector def = new Vector();
    private String actDef = "";
    private String mask = "";
    private boolean key = false;
    private int counter = 0;
    private boolean required = false;
    private boolean readOnly = false;
    private boolean multiSelect=false;
    private boolean unique = false;
    private int system = 0;
    private int used = 0;
    private boolean isInherited = false;
    private String externalColumn ="";
    /**
     * Following fields are added for DbLookup enhancement - Allowing multiple tigger indices and Data types
     */
    private boolean triggerEnabled;
    private int triggerDataType;
    private String triggerValue;
    private boolean isDynamicIndex= false;
    public Index(int id)
    {
        this.id = id;
    }
    public Index(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
            DTIndexId = Util.to_Number(st.nextToken());
            id = Util.to_Number(st.nextToken());
            name = st.nextToken();
            type = Util.to_Number(st.nextToken());
            mask = st.nextToken();
            def.add(st.nextToken());
            required = (st.nextToken().equals("1")? true : false);
            counter = Util.to_Number(st.nextToken());
            key = (st.nextToken().equals("1")? true : false);
            st.nextToken();
            used=Util.to_Number(st.nextToken());
            system=Util.to_Number(st.nextToken());
            info = st.nextToken();
            if(info != null && !info.equals(""))
            {
                try
                {
                    this.readOnly=info.substring(0,1).equals("1");
                    //this.unique=info.substring(1,2).equals("1");
                    /*
                     * Enhancement:-Sequence Unique to Room 
                     * Condition Modified to set unique as true for both 
                     * Unique yes and Unique to room Property.
                     */
                    if(info.substring(1,2).equals("1")||info.substring(1,2).equals("2")){
                        this.unique=true;
                        }
                }
                catch(Exception e){}
            }
            description = st.nextToken();
            actDef = st.nextToken();
            triggerEnabled = st.nextToken().equals("1");
            isDynamicIndex = st.nextElement().equals("1");
        }
        catch(Exception e){}
    }
    public Index(String dbstring, int mode)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {            
            id = Util.to_Number(st.nextToken());
            name = st.nextToken();
            type = Util.to_Number(st.nextToken());
            mask = st.nextToken();
            def.add(st.nextToken());            
            counter = Util.to_Number(st.nextToken());
            used=Util.to_Number(st.nextToken());
            info = st.nextToken();
            if(info != null && !info.equals(""))
            {
                try
                {
                    this.readOnly=info.substring(0,1).equals("1");
                    /*
                     * Enhancement:-Sequence Unique to Room 
                     * Condition Modified to set unique as true for both 
                     * Unique yes and Unique to room Property.
                     */
                    if(info.substring(1,2).equals("1")||info.substring(1,2).equals("2")){
                    this.unique=true;
                    }
                }
                catch(Exception e){}
            }            
            description = st.nextToken();
            //Add the inherit property to check the indices is inherited or not
            isInherited = st.nextToken().equals("1");
            actDef = "";
            triggerEnabled = st.nextToken().equals("1");
            isDynamicIndex = st.nextElement().equals("1");
        }
        catch(Exception e){}
    }
    public int getId()
    {
        return this.id;
    }
    public int getOrder()
    {
        return this.order;
    }
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name =  name;
    }
    public Vector getDef()
    {
        return this.def;
    }
    public String getMask()
    {
        return this.mask;
    }
    public String getValue()
    {
        return this.value;
    }
    public String getActValue()
    {
        return this.actValue;
    }
    public boolean isKey()
    {
        return this.key;
    }
    public void setKey(boolean b)
    {
        this.key = b;
    }
    public boolean isRequired()
    {
        return this.required;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    public void setActValue(String actValue)
    {
        this.actValue = actValue;
    }
    public void setOrder(int order)
    {
        this.order = order;
    }
    public void setType(int type)
    {
        this.type = type;
    }
    public void setDef(String defStr)
    {
        if(def.contains(defStr)) def.remove(defStr);
        this.def.add(0,defStr);
        this.actDef = defStr;
    }
    public void setDef(Vector defs)
    {
        this.def.removeAllElements();
        this.def.addAll(defs);
    }
    public int getType()
    {
        return this.type;
    }
    public boolean isReadOnly() 
    {
        return this.readOnly;
    }        
    public boolean isUnique() 
    {
        return this.unique;
    }
    public String getDescription() 
    {
        return this.description;
    }    
    public int getSystem() 
    {
        return this.system;
    }
    public String toString()
    {
        return this.name;
    }
    public String getInfo() 
    {
        return info;
    }
    public void setInfo(String info) 
    {
        this.info = info;
    }
    public void setMask(String mask) 
    {
        this.mask = mask;
    }
    public void setRequired(boolean required) 
    {
        this.required = required;
    }
    public void setSystem(int system) 
    {
        this.system = system;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }
    public String getActDef() 
    {
        return actDef;
    }
    public void setActDef(String actDef) 
    {
        this.actDef = actDef;
    }
    public int getDTIndexId() 
    {
        return DTIndexId;
    }
    public void setDTIndexId(int DTIndexId) 
    {
        this.DTIndexId = DTIndexId;
    }
    public int getCounter() 
    {
        return counter;
    }
    public void setCounter(int counter) 
    {
        this.counter = counter;
    }
    public void setId(int id) 
    {
        this.id = id;
    }
    public boolean isSystem()
    {
        return system > 0;
    }
    public String getDefaultValue()
    {
        if(def == null || def.size() == 0) return "";
        return (String)def.get(0);
    }
    public int getUsed() 
    {
        return used;
    }
    public void setUsed(int used) 
    {
        this.used = used;
    }
    /**
     * @return Returns the isInherited.
     */
    public boolean isInherited() {
	return isInherited;
    }
    /**
     * @param isInherited The isInherited to set.
     */
    public void setInherited(boolean isInherited) {
	this.isInherited = isInherited;
    }
    /**
     * @return the externalColumn
     */
    public String getExternalColumn() {
	return externalColumn;
    }
    /**
     * @param externalColumn the externalColumn to set
     */
    public void setExternalColumn(String externalColumn) {
	this.externalColumn = externalColumn;
    }
	public boolean isTriggerEnabled() {
		return triggerEnabled;
	}
	public void setTriggerEnabled(boolean triggerEnabled) {
		this.triggerEnabled = triggerEnabled;
	}
	public int getTriggerDataType() {
		return triggerDataType;
	}
	public void setTriggerDataType(int triggerDataType) {
		this.triggerDataType = triggerDataType;
	}
	public String getTriggerValue() {
		return triggerValue;
	}
	public void setTriggerValue(String triggerValue) {
		this.triggerValue = triggerValue;
	}
	public int getSelectValue() {
		return selectValue;
	}
	public void setSelectValue(int selectValue) {
		this.selectValue = selectValue;
	}
	/**
	 * CV10.1 Enhancement MultiSelect property in selection index
	 * @return
	 */
	public boolean isMultiSelect() 	{
		return this.multiSelect;
	}
	public void setMultiSelect(boolean multiSelect) {
		this.multiSelect = multiSelect;
	}
	/**
	 * CV10.1 Enhancement: Check whether dynamic index or not
	 * @author apurba.m
	 * @return
	 */
	public boolean isDynamicIndex() {
		return isDynamicIndex;
	}
	public void setDynamicIndex(boolean isDynamicIndex) {
		this.isDynamicIndex = isDynamicIndex;
	}
	
	
}