package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;

import com.computhink.vwc.VWCConstants;

public class RouteMasterInfo implements Serializable, VWCConstants {
	
	private int routeMasterId;
	private int docId; 
	private String docName;
	private String routeName;
	private String creatorName;			
	private String createdDate;			
	private String modifiedDate;			
	private int routeId; 
	private int routeUserId; 
	private int levelSeq;
	private String receivedDate;	
	private String processedDate;
	private String status;
	private String comments;	
	private int sessionId;
	private String roomName;
    private int signId=0;
    private int signType=-1;
    private String routeUsername;
    private String startLocation;
    private String processType;
    private int actionPermission;
    private String taskName;
    private String taskDescription;
    private int tasklength;
    private int OnRejectAction;
    private String routeInitiatedBy;
    private int approvedBy;
    private String approvedUserName;
    private int authorizedBy;
    private String authorizedUserName;
    private int routeUserSignType;
    private String allPreviousTasks;
	private String immediateNextTask;
	private int routeValidity;
	
	//Task user escalation enhancement
	private String routeUserGrpName;
	private int EUserId;//Escalation user id
	private String EUserName;
	private String userStatus;
	private String notifyMail;
	private String reminderInterval;
	private int taskReminderEnabled;
	private int routeHistoryId;
	/**
	 * Getting Accept/Reject custom labels
	 */
	private String acceptLabel; // value is '-' from VW_DRSGetOriginator procedure
	private String rejectLabel; // value is '-' from VW_DRSGetOriginator procedure
	
	
	/**
	 * @return Returns the processedDate.
	 */
	public String getProcessedDate() {
		return processedDate;
	}
	/**
	 * @param processedDate The processedDate to set.
	 */
	public void setProcessedDate(String processedDate) {
		this.processedDate = processedDate;
	}
public RouteMasterInfo(int routeId) {
		this(routeId, "");
	}
	public RouteMasterInfo(String dbstring) {
//1	7	Sumathi	Route1	1	Task1	Task Decs	4	vwadmin	2008-03-07 07:20:23	2008-03-07 07:20:36	1	1	2008-03-11 14:22:30	0	Pending	-1	vwadmin	_		
		StringTokenizer st = new StringTokenizer(dbstring, "\t");
		try {
			this.routeMasterId = Util.to_Number(st.nextToken());
			this.docId = Util.to_Number(st.nextToken());
			this.docName = st.nextToken();
			this.routeName = st.nextToken();
			// New DRS enhancement fields added  here.
			this.actionPermission = Util.to_Number(st.nextToken());
			this.taskName = st.nextToken();
			this.taskDescription = st.nextToken();
			this.tasklength = Util.to_Number(st.nextToken());
			this.OnRejectAction = Util.to_Number(st.nextToken());
			this.creatorName = st.nextToken();
			this.createdDate = st.nextToken();
			this.modifiedDate = st.nextToken();
			this.routeId = Util.to_Number(st.nextToken());
			this.routeUserId = Util.to_Number(st.nextToken());
			this.receivedDate = st.nextToken();
			this.levelSeq = Util.to_Number(st.nextToken());
			this.status = st.nextToken();
			this.startLocation = st.nextToken();
			this.routeUsername = st.nextToken();
			this.routeInitiatedBy = st.nextToken();
			//Issue:	Oracle for reject action approver drop down is not coming bcos of processedDate empty
			// So script will always send some value (- or date)
			try{
				this.processedDate = st.nextToken();
			    this.approvedBy = Util.to_Number(st.nextToken());
			    this.approvedUserName = st.nextToken();
			    this.authorizedBy = Util.to_Number(st.nextToken());
			    this.authorizedUserName= st.nextToken(); 
			    this.routeUserSignType = Util.to_Number(st.nextToken());
			    this.routeValidity = Util.to_Number(st.nextToken());
			    //Task escalation enhancement
			    this.reminderInterval = st.nextToken();
			    this.userStatus = st.nextToken();
			    this.taskReminderEnabled = Util.to_Number(st.nextToken());
			    String accLabel = st.nextToken();
			    this.acceptLabel = (accLabel.equals("-") ? DEFAULT_ACCEPT_LABEL : accLabel);
			    String rejLabel = st.nextToken();
			    this.rejectLabel = (rejLabel.equals("-") ? DEFAULT_REJECT_LABEL : rejLabel);
			    
			    if(st.hasMoreTokens()){
					this.allPreviousTasks = st.nextToken();
					this.immediateNextTask = st.nextToken();
				}else{
					this.allPreviousTasks = "~";
					this.immediateNextTask = "~";
				}
				this.routeHistoryId=Util.to_Number(st.nextToken());
			}catch(Exception ex ){
				
			}
		} catch (Exception e) {
			this.docId = -1;
			
		}
	}
	
	//Route task user escalation
	public RouteMasterInfo(String dbstring, int escalatedFlag) {//escalatedFlag - 1
		StringTokenizer st = new StringTokenizer(dbstring, "\t");
		try {
			//Routeid, RouteName, Docid, DocName, RouteMasterid, TaskSeq, RouteUserid, EUserid, EUserName, Status, UserStatus, DocSignid, OnRejectAction, routeInitiatedBy
			this.routeId = Util.to_Number(st.nextToken());
			this.routeName = st.nextToken();
			this.docId = Util.to_Number(st.nextToken());
			this.docName = st.nextToken();
			this.routeMasterId = Util.to_Number(st.nextToken());
			this.levelSeq = Util.to_Number(st.nextToken());
			this.routeUserId = Util.to_Number(st.nextToken());//routeUserGrpId
			this.routeUserGrpName = st.nextToken();
			this.EUserId = Util.to_Number(st.nextToken());
			this.EUserName = st.nextToken();
			this.status = st.nextToken();
			this.userStatus = st.nextToken();
			this.notifyMail = st.nextToken();
			this.OnRejectAction = Util.to_Number(st.nextToken());
			this.routeInitiatedBy = st.nextToken();
		}catch(Exception ex){
			this.docId = -1;
		}
	}
	
	public RouteMasterInfo(int routeId, String docName) {
		this.routeId = routeId;
		this.docName = docName;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCreatorName() {
		return creatorName;
	}
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}
	public int getDocId() {
		return docId;
	}
	public void setDocId(int docId) {
		this.docId = docId;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(String modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getReceivedDate() {
		return receivedDate;
	}
	public void setReceivedDate(String receivedDate) {
		this.receivedDate = receivedDate;
	}
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public int getRouteUserId() {
		return routeUserId;
	}
	public void setRouteUserId(int routeUserId) {
		this.routeUserId = routeUserId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String toString() {
		return this.docName;
	}
	public String getRoomName() {
		return roomName;
	}
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	public int getSessionId() {
		return sessionId;
	}
	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}
	public String getRouteName() {
		return routeName;
	}
	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}
	public int getSignId() {
		return signId;
	}
	public void setSignId(int signId) {
		this.signId = signId;
	}
	public int getSignType() {
		return signType;
	}
	public void setSignType(int signType) {
		this.signType = signType;
	}
	public int getLevelSeq() {
		return levelSeq;
	}
	public void setLevelSeq(int levelSeq) {
		this.levelSeq = levelSeq;
	}
	/**
	 * @return Returns the routeUsername.
	 */
	public String getRouteUsername() {
		return routeUsername;
	}
	/**
	 * @param routeUsername The routeUsername to set.
	 */
	public void setRouteUsername(String routeUsername) {
		this.routeUsername = routeUsername;
	}
	/**
	 * @return Returns the startLocation.
	 */
	public String getStartLocation() {
		return startLocation;
	}
	/**
	 * @param startLocation The startLocation to set.
	 */
	public void setStartLocation(String startLocation) {
		this.startLocation = startLocation;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}
	public int getActionPermission() {
		return actionPermission;
	}
	public void setActionPermission(int actionPermission) {
		this.actionPermission = actionPermission;
	}
	public String getTaskDescription() {
		return taskDescription;
	}
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	public int getTasklength() {
		return tasklength;
	}
	public void setTasklength(int tasklength) {
		this.tasklength = tasklength;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}
	/**
	 * @return Returns the routeMasterId.
	 */
	public int getRouteMasterId() {
		return routeMasterId;
	}
	/**
	 * @param routeMasterId The routeMasterId to set.
	 */
	public void setRouteMasterId(int routeMasterId) {
		this.routeMasterId = routeMasterId;
	}
	public int getOnRejectAction() {
		return OnRejectAction;
	}
	public void setOnRejectAction(int onRejectAction) {
		OnRejectAction = onRejectAction;
	}
	public String getRouteInitiatedBy() {
		return routeInitiatedBy;
	}
	public void setRouteInitiatedBy(String routeInitiatedBy) {
		this.routeInitiatedBy = routeInitiatedBy;
	}
	public int getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(int approvedBy) {
		this.approvedBy = approvedBy;
	}
	public String getApprovedUserName() {
		return approvedUserName;
	}
	public void setApprovedUserName(String approvedUserName) {
		this.approvedUserName = approvedUserName;
	}
	public int getAuthorizedBy() {
		return authorizedBy;
	}
	public void setAuthorizedBy(int authorizedBy) {
		this.authorizedBy = authorizedBy;
	}
	public String getAuthorizedUserName() {
		return authorizedUserName;
	}
	public void setAuthorizedUserName(String authorizedUserName) {
		this.authorizedUserName = authorizedUserName;
	}
	public int getRouteUserSignType() {
		return routeUserSignType;
	}
	public void setRouteUserSignType(int routeUserSignType) {
		this.routeUserSignType = routeUserSignType;
	}
	public String getAllPreviousTasks() {
		return allPreviousTasks;
	}
	public void setAllPreviousTasks(String allPreviousTasks) {
		this.allPreviousTasks = allPreviousTasks;
	}
	public String getImmediateNextTask() {
		return immediateNextTask;
	}
	public void setImmediateNextTask(String immediateNextTask) {
		this.immediateNextTask = immediateNextTask;
	}
	public int getRouteValidity() {
		return routeValidity;
	}
	public void setRouteValidity(int routeValidity) {
		this.routeValidity = routeValidity;
	}
	public String getReminderInterval() {
		return reminderInterval;
	}
	public void setReminderInterval(String reminderInterval) {
		this.reminderInterval = reminderInterval;
	}
	public int getEUserId() {
		return EUserId;
	}
	public void setEUserId(int userId) {
		EUserId = userId;
	}
	public String getEUserName() {
		return EUserName;
	}
	public void setEUserName(String userName) {
		EUserName = userName;
	}
	public String getNotifyMail() {
		return notifyMail;
	}
	public void setNotifyMail(String notifyMail) {
		this.notifyMail = notifyMail;
	}
	public String getRouteUserGrpName() {
		return routeUserGrpName;
	}
	public void setRouteUserGrpName(String routeUserGrpName) {
		this.routeUserGrpName = routeUserGrpName;
	}
	public String getUserStatus() {
		return userStatus;
	}
	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}
	public int getTaskReminderEnabled() {
		return taskReminderEnabled;
	}
	public void setTaskReminderEnabled(int taskReminderEnabled) {
		this.taskReminderEnabled = taskReminderEnabled;
	}
	public String getAcceptLabel() {
		return acceptLabel;
	}
	public void setAcceptLabel(String acceptLabel) {
		this.acceptLabel = acceptLabel;
	}
	public String getRejectLabel() {
		return rejectLabel;
	}
	public void setRejectLabel(String rejectLabel) {
		this.rejectLabel = rejectLabel;
	}
	public int getRouteHistoryId() {
		return routeHistoryId;
	}
	public void setRouteHistoryId(int routeHistoryId) {
		this.routeHistoryId = routeHistoryId;
	}
}
