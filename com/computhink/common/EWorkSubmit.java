/*
 * EWorkSubmit.java
 *
 * Created on 16 ����, 2004, 04:50 �
 */

package com.computhink.common;

import java.util.*;
import java.io.Serializable;

/**
 *
 * @author  Administrator
 */
public class EWorkSubmit implements Serializable {
    
    int id;
    Document doc;
    int submitState;
    EWorkTemplate ewTemplate;
    String dateOfCheckout;
    String ewFolder;
    
    public EWorkSubmit() {
    }
    
    public EWorkSubmit(String dbstring)
    {
        int templateId = 0;
        int docId = 0;
        String eWorkMap="";
        String eWorkMapForm="";
        String dateOfCheckout="";
        String documentName="";
        String eWorkFolderName="";
        String isDocCopy="";
        String isAttachPages="";
        String isSaveAssoc="";
        
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
            this.id             = Util.to_Number(st.nextToken());
            templateId          = Util.to_Number(st.nextToken());
            docId               = Util.to_Number(st.nextToken());
            this.submitState    = Util.to_Number(st.nextToken());
            eWorkMap            = st.nextToken();
            eWorkMapForm        = st.nextToken();
            this.dateOfCheckout = st.nextToken();
            documentName        = st.nextToken();
            this.ewFolder       = st.nextToken();
            isDocCopy           = st.nextToken();
            isAttachPages       = st.nextToken();
            isSaveAssoc         = st.nextToken();
        }
        catch(Exception e){}
        //build doc object
        this.doc = new Document(docId);
        this.doc.setName(documentName);
        //build template object
        this.ewTemplate = new EWorkTemplate(templateId);
        this.ewTemplate.setEWorkMap(eWorkMap);
        this.ewTemplate.setEWorkMapForm(eWorkMapForm);
        this.ewTemplate.setName("");
        this.ewTemplate.setDocCopy(isDocCopy.equals("1")? true: false);
        this.ewTemplate.setAttachPages(isAttachPages.equals("1")? true: false);
        this.ewTemplate.setSaveAssoc(isSaveAssoc.equals("1")? true: false);
    }
    
    public EWorkSubmit(int Id) {
        setId(Id);
    }
    
    public void setId(int Id)
    {
        this.id = Id;
    }
    
    public int getId()
    {
        return this.id;
    }
    
    public void setDoc(Document doc)
    {
        this.doc = doc;
    }
    public Document getDoc()
    {
        return this.doc;
    }
    
    public void setState(int submitState)
    {
        this.submitState = submitState;
    }
    
    public int getState()
    {
        return this.submitState;
    }
    
    public void setEWorkTemplate(EWorkTemplate template)
    {
        this.ewTemplate = template;
    }
    public EWorkTemplate getEWorkTemplate()
    {
        return this.ewTemplate;
    }
    
    public void setDateOfCheckout(String dateOfCheckout)
    {
        this.dateOfCheckout = dateOfCheckout;
    }
    public String getDateOfCheckout()
    {
        return this.dateOfCheckout;
    }
    public void setEWorkFolder(String ewFolder)
    {
        this.ewFolder = ewFolder;
    }
    public String getEWorkFolder()
    {
        return this.ewFolder;
    }
}
