/*
 * VWAccess.java
 *
 * Created on March 28, 2004, 3:39 PM
 */

package com.computhink.common;

/**
 *
 * @author  Administrator
 */
import java.math.BigInteger;

public class VWAccess implements java.io.Serializable
{
    private static final int DENY = 0;
    private static final int GRANT = 1;
    private static final int IDLE = 2;
    
    private static final int NAVIGATE = 10;
    private static final int VIEW = 30;
    private static final int CREATE = 50;
    private static final int MODIFY = 70;
    private static final int SHARE = 90;
    private static final int DELETE = 110;
    private static final int ASSIGN = 130;
    
    private static final int SUPERVISOR = 1000;
    private static final int FULLACCESS = 2000;
    private static final int INDEXER = 3000;
    private static final int BROWSER = 4000;

    private static final int LAUNCH = 150;
    private static final int EXPORT = 170;
    private static final int PRINT = 190;
    private static final int MAIL = 210;
    private static final int SENDTO = 230;
    private static final int CHECKINOUT = 250;

    private BigInteger mask = BigInteger.valueOf(0);
    
    public VWAccess() 
    {
        setNavigate(GRANT);
    }
    public VWAccess(String val)
    {
        mask = new BigInteger(val);
    }
    public VWAccess(int rank) 
    {
        switch (rank)
        {
            case SUPERVISOR:
                setNavigate(GRANT);
                setView(GRANT);
                setCreate(GRANT);
                setModify(GRANT);
                setShare(GRANT);
                setDelete(GRANT);
                setAssign(GRANT);
                break;
           case FULLACCESS:
                setNavigate(GRANT);
                setView(GRANT);
                setCreate(GRANT);
                setModify(GRANT);
                setShare(GRANT);
                setDelete(GRANT);
                break;
            case INDEXER:
                setNavigate(GRANT);
                setView(GRANT);
                setCreate(GRANT);
                setModify(GRANT);
                break;
             case BROWSER:
                setNavigate(GRANT);
                setView(GRANT);
        }
    }
    public String getMask()
    {
        return mask.toString();
    }
    public void setMask(String val)
    {
        this.mask = new BigInteger(val);
    }
    private BigInteger getBigInt()
    {
        return mask;
    }
    public void setNavigate(int state)
    {
        setBits(NAVIGATE, state);
    }
    public void setView(int state)
    {
        setBits(VIEW, state);
    }
    public void setCreate(int state)
    {
        setBits(CREATE, state);
    }
    public void setModify(int state)
    {
        setBits(MODIFY, state);
    }
    public void setShare(int state)
    {
        setBits(SHARE, state);
    }
    public void setDelete(int state)
    {
        setBits(DELETE, state);
    }
    public void setAssign(int state)
    {
        setBits(ASSIGN, state);
    }
    public int canNavigate()
    {
        return testBits(NAVIGATE);
    }
    public int canView()
    {
        return testBits(VIEW);
    }
    public int canCreate()
    {
       return testBits(CREATE);
    }
    public int canModify()
    {
        return testBits(MODIFY);
    }
    public int canShare()
    {
        return testBits(SHARE);
    }
    public int canDelete()
    {
        return testBits(DELETE);
    }
    public int canAssign()
    {
        return testBits(ASSIGN);
    }
// Enhancement for Security - No 32
    public void setLaunch(int state)
    {
        setBits(LAUNCH, state);
    }
    public void setExport(int state)
    {
        setBits(EXPORT, state);
    }
    public void setPrint(int state)
    {
        setBits(PRINT, state);
    }
    public void setMail(int state)
    {
        setBits(MAIL, state);
    }
    public void setSendTo(int state)
    {
        setBits(SENDTO, state);
    }
    public void setCheckInOut(int state)
    {
        setBits(CHECKINOUT, state);
    }
    public int canLaunch()
    {
        return testBits(LAUNCH);
    }
    public int canExport()
    {
        return testBits(EXPORT);
    }    
    public int canPrint()
    {
        return testBits(PRINT);
    }
    public int canMail()
    {
        return testBits(MAIL);
    }
    public int canSendTo()
    {
        return testBits(SENDTO);
    }
    public int canCheckInOut()
    {
        return testBits(CHECKINOUT);
    }
    // End
    
    public boolean inheritsAll()
    {
        if (
            canAssign()     == 2 &&
            canCreate()     == 2 &&
            canModify()     == 2 &&
            canShare()      == 2 &&
            canDelete()     == 2 &&
            canView()       == 2 &&
            canNavigate()   == 2 &&
            /// Security Enhancement 32
            canLaunch()   	== 2 &&
            canPrint()   	== 2 &&
            canMail()   	== 2 &&
            canSendTo()   	== 2 &&
            canExport()   	== 2 &&
            canCheckInOut()	== 2 
            ///
            )
            return true;
        else
            return false;
            
    }
    private void setBits(int index, int state)
    {
        index /= 10;
        switch (state)
        {
            case GRANT:
                mask = mask.setBit(index-1);
                mask = mask.setBit(index);
                return;
            case DENY:
                mask = mask.clearBit(index-1);
                mask = mask.clearBit(index);
                return;
            case IDLE:
                mask = mask.setBit(index-1);
                mask = mask.clearBit(index);
        }
    }
    private int testBits(int index)
    {
        index /= 10;
        if (mask.testBit(index-1) &&  mask.testBit(index)) return GRANT;
        else
        if (!mask.testBit(index-1) &&  !mask.testBit(index)) return DENY;
        else
        if (mask.testBit(index-1) &&  !mask.testBit(index)) return IDLE;
        else
            return DENY;    //fail safe
    }
}
