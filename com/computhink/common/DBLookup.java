package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;

public class DBLookup implements Serializable{

    private int dsnId = 0;
    private String dsnName = "";
    private String userName = "";
    private String passWord = "";
    private String externalTable = "";
    private int dsnStatus = 1;
    private int docTypeId = 0;
    private int triggerIndexId = 0;
    private int triggerIndexDataType = 0; 
    private DocType docType = null;
    private String configQuery = "";
    private int indexId = 0;
    private int dsnType = 0;
    private String indexName = "";
    
    
    private final int dbLookupDSNInfo = 0;
    private final int selListDSNInfo = 1; 
    
    public DBLookup(int dsnId){
	this.dsnId = dsnId;
    }
    
    public DBLookup(int dsnId, String dsnName){
	this.dsnId = dsnId;
	this.dsnName = dsnName;
    }
    /*
     * mode is used to differentiate DSN info for DBLookup and Selection index 
     */
    public DBLookup(String data, int mode){
    	StringTokenizer tokens = new StringTokenizer(data, "\t");
    	switch (mode){
    	case dbLookupDSNInfo:
    		if (tokens.hasMoreTokens()) docTypeId = Util.to_Number(tokens.nextToken().trim()); // DocType Id
    		if (tokens.hasMoreTokens()) dsnId = Util.to_Number(tokens.nextToken().trim()); // DSN Id
    		if (tokens.hasMoreTokens()) dsnName = tokens.nextToken().trim(); // DSN Name
    		if (tokens.hasMoreTokens()) externalTable =  tokens.nextToken().trim(); // External Table Name
    		if (tokens.hasMoreTokens()) dsnStatus =  Util.to_Number(tokens.nextToken().trim()); // DSN Status
    		if (tokens.hasMoreTokens()) userName = tokens.nextToken().trim(); // User Name
    		if (tokens.hasMoreTokens()) passWord = tokens.nextToken().trim(); // Password	
    		if (tokens.hasMoreTokens()) configQuery = tokens.nextToken().trim(); // Saved Query
    		if (tokens.hasMoreTokens()) indexId = Util.to_Number(tokens.nextToken().trim()); // IndexId
    		break;
    	case selListDSNInfo:
    		if (tokens.hasMoreTokens()) dsnId = Util.to_Number(tokens.nextToken().trim()); // DSN Id
    		if (tokens.hasMoreTokens()) docTypeId = Util.to_Number(tokens.nextToken().trim()); // DocType Id
    		if (tokens.hasMoreTokens()) dsnName = tokens.nextToken().trim(); // DSN Name
    		if (tokens.hasMoreTokens()) externalTable =  tokens.nextToken().trim(); // External Table Name
    		if (tokens.hasMoreTokens()) dsnStatus =  Util.to_Number(tokens.nextToken().trim()); // DSN Status
    		if (tokens.hasMoreTokens()) userName = tokens.nextToken().trim(); // User Name
    		if (tokens.hasMoreTokens()) passWord = tokens.nextToken().trim(); // Password	
    		if (tokens.hasMoreTokens()) configQuery = tokens.nextToken().trim(); // Saved Query
    		if (tokens.hasMoreTokens()) indexId = Util.to_Number(tokens.nextToken().trim()); // IndexId
    		if (tokens.hasMoreTokens()) indexName = tokens.nextToken().trim(); // IndexName
    		if (tokens.hasMoreTokens()) dsnType = Util.to_Number(tokens.nextToken());
    		break;
    	}

    }
    /**
     * @return the dsnId
     */
    public int getDsnId() {
        return dsnId;
    }
    /**
     * @param dsnId the dsnId to set
     */
    public void setDsnId(int dsnId) {
        this.dsnId = dsnId;
    }
    /**
     * @return the docTypeId
     */
    public int getDocTypeId() {
        return docTypeId;
    }
    /**
     * @param docTypeId the docTypeId to set
     */
    public void setDocTypeId(int docTypeId) {
        this.docTypeId = docTypeId;
    }
    /**
     * @return the dsnName
     */
    public String getDsnName() {
        return dsnName;
    }
    /**
     * @param dsnName the dsnName to set
     */
    public void setDsnName(String dsnName) {
        this.dsnName = dsnName;
    }
    /**
     * @return the dsnStatus
     */
    public int getDsnStatus() {
        return dsnStatus;
    }
    /**
     * @param dsnStatus the dsnStatus to set
     */
    public void setDsnStatus(int dsnStatus) {
        this.dsnStatus = dsnStatus;
    }
    /**
     * @return the externalTable
     */
    public String getExternalTable() {
        return externalTable;
    }
    /**
     * @param externalTable the externalTable to set
     */
    public void setExternalTable(String externalTable) {
        this.externalTable = externalTable;
    }
    /**
     * @return the passWord
     */
    public String getPassWord() {
        return passWord;
    }
    /**
     * @param passWord the passWord to set
     */
    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }
    /**
     * @return the triggerIndexDataType
     */
    public int getTriggerIndexDataType() {
        return triggerIndexDataType;
    }
    /**
     * @param triggerIndexDataType the triggerIndexDataType to set
     */
    public void setTriggerIndexDataType(int triggerIndexDataType) {
        this.triggerIndexDataType = triggerIndexDataType;
    }
    /**
     * @return the triggerIndexId
     */
    public int getTriggerIndexId() {
        return triggerIndexId;
    }
    /**
     * @param triggerIndexId the triggerIndexId to set
     */
    public void setTriggerIndexId(int triggerIndexId) {
        this.triggerIndexId = triggerIndexId;
    }
    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }
    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * @return the docType
     */
    public DocType getDocType() {
        return docType;
    }
    /**
     * @param docType the docType to set
     */
    public void setDocType(DocType docType) {
        this.docType = docType;
    }

	public String getConfigQuery() {
		return configQuery;
	}

	public void setConfigQuery(String configQuery) {
		this.configQuery = configQuery;
	}

	public int getDsnType() {
		return dsnType;
	}

	public void setDsnType(int dsnType) {
		this.dsnType = dsnType;
	}

	public int getIndexId() {
		return indexId;
	}

	public void setIndexId(int indexId) {
		this.indexId = indexId;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}
}
