
package com.computhink.common;

import java.util.*;
import java.io.File;
public class Signature implements java.io.Serializable
{
    public static final int Authorized_Type = 1;
    public static final int Certified_Type  = 2;
    public static final int Template_Type  = 3;
    public static final int Authorized_Type_EWA = 4;
    private int id;
    private int docId;
    private String userName;
    private int type;
    private int status;
    private String signFilePath="";
    private String signDate;
    public Signature()
    {
    }
    public Signature(String dbstring)
    {   
        setSignatureInfo(dbstring);
    }
    public void setSignatureInfo(String dbstring)
    {   
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
            this.docId = Util.to_Number(st.nextToken());
            this.userName = st.nextToken();
            this.type = Util.to_Number(st.nextToken());
            this.status = Util.to_Number(st.nextToken());
            this.signDate = st.nextToken();
        }
        catch(Exception e){}
    }
    public int getDocId()
    {
        return docId;
    }
    public void setDocId(int docId)
    {
        this.docId = docId;
    }
    public String getUserName()
    {
        return userName;
    }
    public void setUserName(String userName)
    {
        this.userName = userName;
    }
    public String getSignFilePath()
    {
        return signFilePath;
    }
    public void setSignFilePath(String signFilePath)
    {
        this.signFilePath = signFilePath;
    }
    public void setType(int type)
    {
        this.type = type;
    }
    public int getType()
    {
        return type;
    }
    public int hashCode()
    {
        return new Integer(docId).hashCode();
    }
    
    public String toString()
    {
        String str=userName.trim()+(type==Authorized_Type?" [Authorized]":" [Certified]");
        return str;
    }   
    public static File makeSignFilePath(String cashDir ,String userName, int type)
    {
        if(cashDir==null || cashDir.equals(""))
            cashDir=System.getProperty("java.io.tmpdir");
        cashDir=cashDir+(cashDir.endsWith(Util.pathSep)?"":Util.pathSep);
        File signFolder=new File(cashDir+"Sign",userName);
        signFolder.mkdirs();
        File signFile=null;
        switch (type)
        {
            case Authorized_Type:
                signFile=new File(signFolder,userName+"_A.emf");
                break;
            case Certified_Type:
                signFile=new File(signFolder,userName+"_C.emf");
                break;
            case Template_Type:
                signFile=new File(signFolder,userName+"_T.tmp");
                break;
            case Authorized_Type_EWA:
                signFile=new File(signFolder,userName+"_A.png");
                break;
        }
        return signFile;
    }
    
    /** Getter for property signDate.
     * @return Value of property signDate.
     */
    public java.lang.String getSignDate() {
        return signDate;
    }
    
    /** Setter for property signDate.
     * @param signDate New value of property signDate.
     */
    public void setSignDate(java.lang.String signDate) {
        this.signDate = signDate;
    }
    
    /** Getter for property id.
     * @return Value of property id.
     */
    public int getId() {
        return id;
    }
    
    /** Setter for property id.
     * @param id New value of property id.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /** Getter for property status.
     * @return Value of property status.
     */
    public int getStatus() {
        return status;
    }
    
    /** Setter for property status.
     * @param status New value of property status.
     */
    public void setStatus(int status) {
        this.status = status;
    }
    
}