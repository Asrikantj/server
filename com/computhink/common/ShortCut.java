/*
 * ShortCut.java
 *
 * Created on December 30, 2003, 11:52 AM
 */
package com.computhink.common;
/**
 *
 * @author  Administrator
 */
public class ShortCut implements java.io.Serializable
{
    public static final int NODE_SC = 0;
    public static final int DOC_SC  = 1;
    
    private int id;
    private int nodeId;
    private String name;
    private int type;
    private boolean publicScope;
    private int roomId;
    
    public ShortCut(String name, boolean scope)
    {
       this.name = name;
       this.publicScope = scope;
    }
    public ShortCut(int id, int nid, String name, int type)
    {
        this.id = id;
        this.nodeId = nid;
        this.name = name;
        this.type = type;
    }
    public int getId()
    {
        return this.id;
    }
    public int getNodeId()
    {
        return this.nodeId;
    }
    public void setNodeId(int id)
    {
       this.nodeId = id;
    }
    public String getName()
    {
        return this.name;
    }
    public int getType()
    {
        return this.type;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public boolean getPublic() 
    {
        return publicScope;
    }
    public void setPublic(boolean scope) 
    {
        this.publicScope = scope;
    }
    //remove later
    public String getUserName()
    {
        return this.name;
    }
    public void setUser(String name)
    {
        this.name = name;
    }
    public String toString()
    {
        return this.name;
    }
    public void setId(int id) 
    {
        this.id = id;
    }
    
    /** Getter for property roomId.
     * @return Value of property roomId.
     *
     */
    public int getRoomId() {
        return roomId;
    }
    
    /** Setter for property roomId.
     * @param roomId New value of property roomId.
     *
     */
    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }
    
}
