/*
 * Cipher.java
 *
 * Created on September 11, 2002, 7:35 AM
 */
package com.computhink.common;
/**
 *
 * @author  Administrator
 * @version 
 */
import java.io.*;
public abstract class Cipher extends CryptoUtils
{
    public int keySize;
    public abstract void setKey( byte[] key );
    
    public Cipher( int keySize )
    {
        this.keySize = keySize;
    }
    public int keySize()
    {
	    return keySize;
    }
    
    public void setKey( String keyStr )
    {
    	setKey( makeKey( keyStr ) );
    }
    public byte[] makeKey( String keyStr )
    {
        byte[] key;
	    if ( keySize == 0 )
	        key = new byte[keyStr.length()];
	    else
	        key = new byte[keySize];
        
        int i, j;

        for ( j = 0; j < key.length; ++j )
            key[j] = 0;

        for ( i = 0, j = 0; i < keyStr.length(); ++i, j = (j+1) % key.length )
            key[j] ^= (byte) keyStr.charAt( i );

	    return key;
    }
}
