/*
 * EWTemplate.java
 *
 * Created on 12 ����, 2004, 09:42 �
 */

package com.computhink.common;

import java.util.*;
import java.io.Serializable;
/**
 *
 * @author  Administrator
 */
public class EWorkTemplate implements Serializable {
    
    private int id;
    private String name = "";
    private DocType dt;
    private String eWorkMap = "";
    private String eWorkMapForm = "";
    private boolean docCopy = false;
    private boolean attachPages = false;
    private boolean saveAssoc = false;
    
    private Vector mapping = new Vector();
    
    public EWorkTemplate() {
    }
    
    public EWorkTemplate(int id) 
    {
        this(id, "");
    }
    public EWorkTemplate(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
            this.id             = Util.to_Number(st.nextToken());
            this.name           = st.nextToken();
            this.dt             = new DocType(Util.to_Number(st.nextToken()),
            st.nextToken());
            this.eWorkMap       = st.nextToken();
            this.eWorkMapForm   = st.nextToken();
            this.docCopy        = (st.nextToken().equals("1")? true: false);
            this.attachPages    = (st.nextToken().equals("1")? true: false);
            this.saveAssoc      = (st.nextToken().equals("1")? true: false);
        }
        catch(Exception e){}
    }
    public EWorkTemplate(int id, String name) 
    {
        this.id = id;
        this.name = name;
    }
    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getName()
    {
        return this.name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public Vector getMapping()
    {
        return this.mapping;
    }
    
    public void setMapping(Vector mapping)
    {
        this.mapping.removeAllElements();
        this.mapping.addAll(mapping);
    }
    public void setDocType(DocType dt) {
        this.dt = dt;
    }
    public DocType getDocType() {
        return this.dt;
    }
    
    public void setEWorkMap(String eWorkMap)
    {
        this.eWorkMap = eWorkMap;
    }
    
    public String getEWorkMap()
    {
        return this.eWorkMap;
    }
    
    public void setEWorkMapForm(String eWorkMapForm)
    {
        this.eWorkMapForm = eWorkMapForm;
    }

    public String getEWorkMapForm()
    {
        return this.eWorkMapForm;
    }
    
    public void setDocCopy(boolean docCopy)
    {
        this.docCopy = docCopy;
    }
    
    public boolean isDocCopy()
    {
        return this.docCopy;
    }
    
    public void setAttachPages(boolean attachPages)
    {
        this.attachPages = attachPages;
    }
    
    public boolean isAttachPages()
    {
        return this.attachPages;
    }

    public void setSaveAssoc(boolean saveAssoc)
    {
        this.saveAssoc = saveAssoc;
    }
    
    public boolean isSaveAssoc()
    {
        return this.saveAssoc;
    }
}
