package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;

public class RouteUsers implements Serializable {

	private int routeUserId;
	private int routeId;
    private int userId;
    private String userName = "";
    private int routeUserLevel;
    private int sendEmail;
    public Vector routeUsrList;
    private String userEmail = "";
	private int actionPermission;
	private int eSignature=0;
	private String groupName = "";
	private int routeTaskId = 0;
	private String expiryPeriod = "";
	private int eUserid = 0;
	private String eUserName = "";
	private String notifyMail = ""; 
	
    public int getEUserid() {
		return eUserid;
	}
	public void setEUserid(int userid) {
		eUserid = userid;
	}
	public String getEUserName() {
		return eUserName;
	}
	public void setEUserName(String userName) {
		eUserName = userName;
	}
	public String getExpiryPeriod() {
		return expiryPeriod;
	}
	public void setExpiryPeriod(String expiryPeriod) {
		this.expiryPeriod = expiryPeriod;
	}
	public String getNotifyMail() {
		return notifyMail;
	}
	public void setNotifyMail(String notifyMail) {
		this.notifyMail = notifyMail;
	}
	public RouteUsers()
    {
    	
    }
    public RouteUsers(int routeUserId) 
    {
        this.routeUserId = routeUserId;
    }
    public RouteUsers(String dbstring) 
    {
    	 StringTokenizer st = new StringTokenizer(dbstring, "\t");
         try
         {
        	 ///29	3	28	vwuser1	2	0	one@mail.com	0	Review	 Desc	2	0
         	this.routeUserId = Util.to_Number(st.nextToken());
 			this.routeTaskId= Util.to_Number(st.nextToken());
 			this.routeId = Util.to_Number(st.nextToken());
 			this.groupName = st.nextToken().trim();
 			this.userId = Util.to_Number(st.nextToken());
 			this.userName = st.nextToken().trim();
 			this.userEmail = st.nextToken().trim();
 			st.nextToken().trim();
 			this.sendEmail= Util.to_Number(st.nextToken());
 			this.actionPermission = Util.to_Number(st.nextToken());
 			/* Its not bean sent from script.-Task escalation enhancement
 			 * if(st.hasMoreTokens())
 				this.eSignature= Util.to_Number(st.nextToken());*/
 			this.expiryPeriod = st.nextToken();
 			this.eUserid = Util.to_Number(st.nextToken());
 			this.eUserName = st.nextToken();
 			this.notifyMail = st.nextToken();
         }
         catch(Exception e){
         }
    }
    
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public int getRouteUserLevel() {
		return routeUserLevel;
	}
	public void setRouteUserLevel(int routeUserLevel) {
		this.routeUserLevel = routeUserLevel;
	}

	public Vector getRouteUsrList() {
		return routeUsrList;
	}

	public int getRouteUserId() {
		return routeUserId;
	}

	public void setRouteUserId(int routeUserId) {
		this.routeUserId = routeUserId;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String toString() {
		return this.userName;
	}
	public int getSendEmail() {
		return sendEmail;
	}
	public void setSendEmail(int sendEmail) {
		this.sendEmail = sendEmail;
	}
	/**
	 * @return Returns the userEmail.
	 */
	public String getUserEmail() {
		return userEmail;
	}
	/**
	 * @param userEmail The userEmail to set.
	 */
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}
	/**
	 * @return Returns the actionPermission.
	 */
	public int getActionPermission() {
		return actionPermission;
	}
	/**
	 * @param actionPermission The actionPermission to set.
	 */
	public void setActionPermission(int actionPermission) {
		this.actionPermission = actionPermission;
	}
	/**
	 * @return Returns the eSignature.
	 */
	public int getESignature() {
		return eSignature;
	}
	/**
	 * @param signature The eSignature to set.
	 */
	public void setESignature(int signature) {
		eSignature = signature;
	}
	/**
	 * @return Returns the taskDescription.
	 *//*
	public String getTaskDescription() {
		return taskDescription;
	}
	*//**
	 * @param taskDescription The taskDescription to set.
	 *//*
	public void setTaskDescription(String taskDescription) {
		this.taskDescription = taskDescription;
	}
	*//**
	 * @return Returns the taskLength.
	 *//*
	public int getTaskLength() {
		return taskLength;
	}
	*//**
	 * @param taskLength The taskLength to set.
	 *//*
	public void setTaskLength(int taskLength) {
		this.taskLength = taskLength;
	}
	*//**
	 * @return Returns the taskName.
	 *//*
	public String getTaskName() {
		return taskName;
	}
	*//**
	 * @param taskName The taskName to set.
	 *//*
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}*/
	/**
	 * @param routeUsrList The routeUsrList to set.
	 */
	public void setRouteUsrList(Vector routeUsrList) {
		this.routeUsrList = routeUsrList;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public int getRouteTaskId() {
		return routeTaskId;
	}
	public void setRouteTaskId(int routeTaskId) {
		this.routeTaskId = routeTaskId;
	}
}
