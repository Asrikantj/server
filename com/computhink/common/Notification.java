package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;

public class Notification implements Serializable{


	private static final long serialVersionUID = 1L;
	private int id;
	private String module;	//Document, Folder, Route, Retention, Others(Recycle Documents, Signature, Backup Documents, Resotre Documents)
	private String notifyDesc;
	private int settingsId;
	
	public Notification() {
		// TODO Auto-generated constructor stub
	}

	public Notification(int id, String notifyDesc, String module, int settingsId){
		this.id = id;
		this.notifyDesc = notifyDesc;
		this.module = module;
		this.settingsId = settingsId;
	}

	public Notification(int id, String notifyDesc, String module){
		this.id = id;
		this.notifyDesc = notifyDesc;
		this.module = module;
	}

	public Notification(int id, String notifyDesc){
		this.id = id;
		this.notifyDesc = notifyDesc;
	}
	public Notification(String dbString){
		try{
        StringTokenizer st = new StringTokenizer(dbString, "\t");
        this.id = Util.to_Number(st.nextToken());
        this.notifyDesc = st.nextToken();
        this.module = st.nextToken();
		}catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public String getNotifyDesc() {
		return notifyDesc;
	}
	public void setNotifyDesc(String notifyDesc) {
		this.notifyDesc = notifyDesc;
	}

	public int getSettingsId() {
		return settingsId;
	}

	public void setSettingsId(int settingsId) {
		this.settingsId = settingsId;
	}	
}
