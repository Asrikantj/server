/*
 * Constants.java
 *
 * Created on September 11, 2002, 7:35 AM
 */
package com.computhink.common;

import com.computhink.vwc.resource.ResourceManager;

/**
 *
 * @author  Saad
 * @version 
 */
public interface Constants
{
	public static final String PRODUCT_NAME = ResourceManager.getDefaultManager().getString("Product.Name");
	public static final String WORKFLOW_MODULE_NAME = "Workflow";
	public static final String POWERED_BY_CONTENTVERSE = "Powered by Contentverse";
	public static final String CV_PREF_ROOT = "/computhink/" + Constants.PRODUCT_NAME.toLowerCase();
    public static final int  OneMinute = 1000 * 60;
    public static final int  OneHour = 60 * OneMinute;
    public static final long OneDay = 24 * OneHour; 
    public static final int MAX_COMMENT_LENGTH =7000;
    
    public static final String SERVER_VWS  = PRODUCT_NAME + " Server";
    public static final String SERVER_IXR  = "Indexer Server";
    public static final String SERVER_DSS  = "Document Storage Server";
    public static final String SERVER_MDSS = "Mini Storage Server";
    // Constant added for ARS, 20 July 2006, Valli
    public static final String SERVER_ARS  = "Auto Retention Server";
    // Constant added for DRS, 20 Dec 2006, Valli
    public static final String SERVER_DRS  = "Document "+ WORKFLOW_MODULE_NAME +" Server";
    public static final String SERVER_VNS  = "Notification Server";
    public static final String SERVER_CSS="Content Sentinel Server";
    
    public static final String SSCHEME_ADS = "ADS";
    public static final String SSCHEME_NDS = "NDS";
    public static final String SSCHEME_SSH = "SSH";
    
    public static final String DBENGINE_SQL = "SQLServer";
    public static final String DBENGINE_ORA = "Oracle";
    public static final String DBENGINE_SYB = "Sybase";
        
    public static final String DOC_CONTAINER = "all.zip";
    
    public static final String DOC_WEB_CONTAINER = "0.0";
    
    String NewLineChar = (new Character((char)13)).toString() + 
                                            (new Character((char)10)).toString();
    String DoubleQuotation=(new Character((char)34)).toString();
    
    String BackupFileName = "Document.vwd";
    String BackupPageFolderName = "Pages";
    String BackupHTMLFileName = "Document.html";
    String BackupTextFolderName= "Text";
    String BackupSignatureFolderName= "Signature";
    
    /*LDAP constants*/
    public static final String SCHEME_LDAP_ADS = "LDAP(ADS)";
    public static final String SCHEME_LDAP_NOVELL = "LDAP(NOVELL)";
    public static final String SCHEME_LDAP_LINUX = "LDAP(LINUX)";
    public static final String SECURITY_SIMPLE = "Simple";
    String Route_Status_Reject = "Rejected";
    String Route_Status_Approve = "Approved";
    String Route_Status_Pending = "Pending";
    String Route_Status_Review = "Reviewed";
    String Route_Status_End = WORKFLOW_MODULE_NAME +" Ended";
    /**CV2019 merges from SIDBI Line - rtf message changes******/
    String Route_Status_Approve_End = "Approved " + WORKFLOW_MODULE_NAME +" Ended";
    String Route_Status_Approve_PSR="Approved and PSR";
    String Route_Status_PSR_End="PSR "+ WORKFLOW_MODULE_NAME +" Ended";
    String Route_Status_Approve_Intimation="Approved and Intimation";
    /** End of SIDBI Line Merges******/
    String Route_Status_OnReject = "Rejected, Task completed";
    String Route_Status_OnApprove = "Approved, Task completed";
    String Route_Status_OnRejectOnLastTask = "Rejected, "+ WORKFLOW_MODULE_NAME +" completed";
    String Route_Status_OnApproveOnLastTask = "Approved, "+ WORKFLOW_MODULE_NAME +" completed";
    String Route_Status_OnReject_ForOriginator = "Rejected, "+ WORKFLOW_MODULE_NAME +" completed";
    String Route_Status_OnEnd = "Ended, "+ WORKFLOW_MODULE_NAME +" ceased";
    String Route_Status_Notify_OnRptGen = "Send Notification On Report Generation";
    /**CV2019 merges from SIDBI Line - rtf message changes******/
    String Route_Status_Notify_OnVwrRptGen = "Send Notification On VWR Report Generation";
    public static final int SSL_PORT = 636;
    public static final String SECURITY_PROTOCOL_SSL = "ssl";
    int CertifiedSign=1;
    int AuthorizedSign=2;
    String userGroupXMLFile = "UserGroupList.xml";
    
    static int ViewWiseServer = 1;
    static int License = 3;
    static int Connection_Security = 4;    
    static int RoomInfo = 5;
    static int EmailSettings = 6;
    static int LoginManagement = 11;
    static int FolderLocation= 13;


    static int DocumentServer = 7;
    static int IndexerServer = 8;
    static int RoutingServer = 9;
    static int RetentionServer = 10;
    static int NotificationServer = 12;
    static int ContentSentinelServer=14;
    
    static int TEXT_DATA_TYPE = 0;
    static int NUMERIC_DATA_TYPE = 1;
    static int DATE_DATA_TYPE = 2;
    static int SELECTION_DATA_TYPE = 3;
    
    //Escalation enhancement
    int Route_Esc_Status_AutoAccept = -2;
    int Route_Esc_Status_AutoReject = -3;
    String ROUTE_ZERODOC_MESSAGE = ResourceManager.getDefaultManager().getString("AutoWorkFlow.NoDocumentMsg");
    
}
