/*
 * Room.java
 *
 * Created on March 17, 2002, 12:14 AM
 */
package com.computhink.common;
import com.computhink.database.*;
/**
 *
 * @author  Administrator
 * @version 
 */
import java.util.*;

public class Room 
{
    private ServerSchema vws;
    private RoomProperty roomProperty;
    private Database database;
    private String hostIp;
    private long lastAccessTime;
    private int countDBFail; 

    public Room(RoomProperty roomProperty)
    {
        this.roomProperty = roomProperty;
    }
    public RoomProperty getRoomProperty()
    {
        return roomProperty;
    }
    public String getName()
    {
        return roomProperty.getName();
    }
    public String getDatabaseEngineName()
    {
        return roomProperty.getDatabaseEngineName();
    }
    public String getDatabaseName()
    {
        return roomProperty.getDatabaseName();
    }
    public String getDatabaseHost()
    {
        return roomProperty.getDatabaseHost();
    }
    public String getDatabaseUser()
    {
        return roomProperty.getDatabaseUser();
    }
    public String getDatabasePassword()
    {
        return roomProperty.getDatabasePassword();
    }
    public int getMinPoolSize()
    {
        return roomProperty.getMinPoolSize();
    }
    public int getMaxPoolSize()
    {
        return roomProperty.getMaxPoolSize();
    }
    public int getDatabasePort()
    {
        return roomProperty.getDatabasePort();
    }
    public Database getDatabase(String logger)
    {
        if (database == null)
        {
            database = new Database(this, logger);
        }
        return database;
    }
    public Database getDatabase()
    {
        return getDatabase(null);
    }
    public ServerSchema getVWS()
    {
        return this.vws;
    }
    public void setVWS(ServerSchema vws)
    {
        this.vws = vws;
    }
    public long getLastAccessTime()
    {
        return lastAccessTime;
    }
    public void setLastAccessTime(long lastAccessTime)
    {
        this.lastAccessTime = lastAccessTime;
    }
    public boolean getBypass()
    {
        return roomProperty.getBypass();
    }
    public String getAdmins()
    {
        return roomProperty.getAdmins();
    }
    public String getNamedGroup()
    {
        return roomProperty.getNamedGroup();
    }
    public String getNamedOnlineGroup()
    {
        return roomProperty.getNamedOnlineGroup();
    }
    
    public String getConcurrentOnlineGroup()
    {
        return roomProperty.getConcurrentOnlineGroup();
    }
    
    public String getPublicWAGroup()
    {
        return roomProperty.getPublicWebAccess();
    }
    
    
    public String getProfessionalNamedGroup()
    {
        return roomProperty.getProfessionaNamedGroup();
    }
    public String getEnterpriseConcurrentGroup()
    {
        return roomProperty.getEnterpriseConcurrentGroup();
    }
    public String getProfessionalConcurrentGroup()
    {
        return roomProperty.getProfessionalConcurrentGroup();
    }
    
    public String getManagers()
    {
        return roomProperty.getManagers();
    }
    public String getKey()
    {
        return roomProperty.getKey();
    }
    public boolean getISEncryptedKey()
    {
        return roomProperty.getIsEncryptedKey();
    }
    public String toString()
    {
        return roomProperty.getName();
    }
	/**
	 * @return Returns the countDBFail.
	 */
	public int getCountDBFail() {
		return roomProperty.getCountDBFail();
	}
	public String getSubAdmins()
    {
        return roomProperty.getSubAdmins();
    }
	public String getBatchInputAdministrator()
    {
        return roomProperty.getBatchInputAdministrator();
    }
}
