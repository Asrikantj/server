/*
 * DocType.java
 *
 * Created on December 28, 2003, 11:40 AM
 */

package com.computhink.common;
/**
 *
 * @author  Administrator
 */
import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;

public class DocType implements Serializable
{
    private int id;
    private String name;
    private String to = "";
    private String cc = "";
    private String subject = "";
    private Vector indices = new Vector();
    private boolean enableTextSearch = false;
    private boolean used = true;

    private String VREnable="0";
    private String VRInitVersion="0";
    private String VRInitRevision="0";
    private String VRPagesChange="0";
    private String VRIncPagesChange="0";
    private String VRPageChange="0";
    private String VRIncPageChange="0";
    private String VRPropertiesChange="0";
    private String VRIncPropertiesChange="0";
    private String tag="";
    /**
     * Following fields are commented for DbLookup enhancement - Allowing multiple tigger indices and Data types
     */
    /*private int triggerIndexId =0;
    private int triggerIndexDataType = 0;*/
    private boolean DBLookupEnabled = false;
    /*
    * Purpose	 	: Getter and setters added for Retention variables	 
    * Created By	: Valli
    * Date			: 20-Jul-2006
    */
    private String ARSEnable ="0";
	private String ARSRetentionPeriod ="0";
	private String ARSRetentionPeriodStartsID ="0";
	private String ARSDisposalActionID ="0";
	private String ARSMoveDocLocation ="0";
	private String ARSPolicyCreatedDate ="0";
    private String ARStag="";

    public DocType(int id) 
    {
        this(id, "");
    }
    public DocType(String dbstring)
    {
    	StringTokenizer st = new StringTokenizer(dbstring, "\t");
    	try
    	{
    		if(st.countTokens()==1){
    			this.id = Util.to_Number(st.nextToken());
    		}else{
    			this.id = Util.to_Number(st.nextToken());
    			this.name = st.nextToken();
    			this.used = ( st.nextToken().equals("1")? true: false);
    			st.nextToken(); //session id
    			this.enableTextSearch = ( st.nextToken().equals("1")? true: false);
    			String toStr = st.nextToken();
    			this.to=toStr.equals("-") ? "" : toStr;
    			String ccStr = st.nextToken(); 
    			this.cc= ccStr.equals("-") ? "" : ccStr;
    			String subjectStr = st.nextToken();
    			this.subject= subjectStr.equals("-") ? "" : subjectStr;
    			this.DBLookupEnabled = st.nextToken().equals("1");
    			//this.triggerIndexId = Util.to_Number(st.nextToken());
    		}
    	}
    	catch(Exception e){}
    }
    public DocType(int id, String name) 
    {
        this.id = id;
        this.name = name;
    }
    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getName()
    {
        return this.name;
    }
    public Vector getIndices()
    {
        return this.indices;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public void setIndices(Vector indices)
    {
        this.indices.removeAllElements();
        this.indices.addAll(indices);
    }
    public void setEnableTextSearch(boolean b)
    {
        this.enableTextSearch = b;
    }
    public boolean getEnableTextSearch()
    {
        return this.enableTextSearch;
    }
     public void setUsed(boolean b)
    {
        this.used = b;
    }
    public boolean getUsed()
    {
        return this.used;
    }
    public String toString()
    {
        return this.name;
    }
    
    public java.lang.String getTo() {
        return to;
    }
    
    public void setTo(java.lang.String to) {
        this.to = to;
    }
    
    public java.lang.String getCc() {
        return cc;
    }
    
    public void setCc(java.lang.String cc) {
        this.cc = cc;
    }
    
    public java.lang.String getSubject() {
        return subject;
    }

    public void setSubject(java.lang.String subject) {
        this.subject = subject;
    }
    
    public java.lang.String getVREnable() {
        return VREnable;
    }
    
    public void setVREnable(java.lang.String VREnable) {
        this.VREnable = VREnable;
    }
    
    public java.lang.String getVRInitVersion() {
        return VRInitVersion;
    }

    public void setVRInitVersion(java.lang.String VRInitVersion) {
        this.VRInitVersion = VRInitVersion;
    }
    
    public java.lang.String getVRInitRevision() {
        return VRInitRevision;
    }
    
    public void setVRInitRevision(java.lang.String VRInitRevision) {
        this.VRInitRevision = VRInitRevision;
    }
    
    public java.lang.String getVRPagesChange() {
        return VRPagesChange;
    }
    
    public void setVRPagesChange(java.lang.String VRPagesChange) {
        this.VRPagesChange = VRPagesChange;
    }
    
    public java.lang.String getVRIncPagesChange() {
        return VRIncPagesChange;
    }
    
    public void setVRIncPagesChange(java.lang.String VRIncPagesChange) {
        this.VRIncPagesChange = VRIncPagesChange;
    }
    
    public java.lang.String getVRPageChange() {
        return VRPageChange;
    }
    
    public void setVRPageChange(java.lang.String VRPageChange) {
        this.VRPageChange = VRPageChange;
    }
    
    public java.lang.String getVRIncPageChange() {
        return VRIncPageChange;
    }
    
    public void setVRIncPageChange(java.lang.String VRIncPageChange) {
        this.VRIncPageChange = VRIncPageChange;
    }
    
    public java.lang.String getVRPropertiesChange() {
        return VRPropertiesChange;
    }
    
    public void setVRPropertiesChange(java.lang.String VRPropertiesChange) {
        this.VRPropertiesChange = VRPropertiesChange;
    }
    
    public java.lang.String getVRIncPropertiesChange() {
        return VRIncPropertiesChange;
    }
    
    public void setVRIncPropertiesChange(java.lang.String VRIncPropertiesChange) {
        this.VRIncPropertiesChange = VRIncPropertiesChange;
    }
    public void setSettings(Vector settings)
    {
        if(settings==null || settings.size()==0) 
        {
            VREnable="0";
            return;
        }
        StringTokenizer st = new StringTokenizer((String)settings.get(0), "\t");
        try
        {
            VREnable=st.nextToken();
            VRInitVersion=st.nextToken();
            VRInitRevision=st.nextToken();
            VRPagesChange=st.nextToken();
            VRIncPagesChange=st.nextToken();
            VRPageChange=st.nextToken();
            VRIncPageChange=st.nextToken();
            VRPropertiesChange=st.nextToken();
            VRIncPropertiesChange=st.nextToken();
        }
        catch(Exception e){}
    }
    
    /*
    * get, set for ARS
    */

    public String getARSEnable() {
        return ARSEnable;
    }
    public void setARSEnable(String ARSEnable) {
        this.ARSEnable = ARSEnable;
    }

    public String getARSRetentionPeriod() {
        return ARSRetentionPeriod;
    }
    public void setAVRRetentionPeriod(String ARSRetentionPeriod) {
        this.ARSRetentionPeriod = ARSRetentionPeriod;
    }

   	public String getARSRetentionPeriodStartsID() {
        return ARSRetentionPeriodStartsID;
    }
    public void setARSRetentionPeriodStartsID(String ARSRetentionPeriodStartsID) {
        this.ARSRetentionPeriodStartsID = ARSRetentionPeriodStartsID;
    }

   	public String getARSDisposalActionID() {
        return ARSDisposalActionID;
    }
    public void setARSDisposalActionID(String ARSDisposalActionID) {
        this.ARSDisposalActionID = ARSDisposalActionID;
    }

   	public String getARSMoveDocLocation() {
        return ARSMoveDocLocation;
    }
    public void setARSMoveDocLocation(String ARSMoveDocLocation) {
        this.ARSMoveDocLocation = ARSMoveDocLocation;
    }

   	public String getARSPolicyCreatedDate() {
        return ARSPolicyCreatedDate;
    }
    public void setARSPolicyCreatedDate(String ARSPolicyCreatedDate) {
        this.ARSPolicyCreatedDate = ARSPolicyCreatedDate;
    }
	/*get, set ARS end here	*/

    /*
    * set ARS settings
    */
  	public void setARSSettings(Vector settings)
	{
  			if(settings==null || settings.size()==0)
		{
			ARSEnable = "0";
			return;
		}
		StringTokenizer st = new StringTokenizer((String)settings.get(0), "\t");
		try
		{
		    ARSEnable = st.nextToken();
			ARSRetentionPeriod = st.nextToken();
			ARSRetentionPeriodStartsID = st.nextToken();
			ARSDisposalActionID = st.nextToken();
			ARSMoveDocLocation = st.nextToken();
			ARSPolicyCreatedDate  = st.nextToken();
		}
		
		catch(Exception e){}
    }

    /** Getter for property tag.
     * @return Value of property tag.
     */
    public java.lang.String getTag() {
        return tag;
    }
    
    /** Setter for property tag.
     * @param tag New value of property tag.
     */
    public void setTag(java.lang.String tag) {
        this.tag = tag;
    }
    /**
     * @return the triggerIndex
     */
    /*public int getTriggerIndexId() {
        return triggerIndexId;
    }
    *//**
     * @param triggerIndexId the triggerIndexId to set
     *//*
    public void setTriggerIndexId(int triggerIndexId) {
        this.triggerIndexId = triggerIndexId;
    }
    *//**
     * @return the triggerIndexDataType
     *//*
    public int getTriggerIndexDataType() {
        return triggerIndexDataType;
    }
    *//**
     * @param triggerIndexDataType the triggerIndexDataType to set
     *//*
    public void setTriggerIndexDataType(int triggerIndexDataType) {
        this.triggerIndexDataType = triggerIndexDataType;
    }*/
	public boolean isDBLookupEnabled() {
		return DBLookupEnabled;
	}
	public void setDBLookupEnabled(boolean lookupEnabled) {
		DBLookupEnabled = lookupEnabled;
	}
    
}