package com.computhink.common;

import java.awt.image.IndexColorModel;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

import java.util.Hashtable;
/*
 * NodeIndexRules object is used to keeping the indices and its default values
 *  
 */
public class NodeIndexRules implements Serializable{

	/**
	 * @param args
	 */
	private int nodeId;
	private int docTypeId;	
	private int indicesCount;
	private int indexId; 
	private String indexValue;
	private String creatorName;		
	private String createdOn;
	private ArrayList indicesRulesList = new  ArrayList();	
	private Index[] indicesList;	
	
	
	public NodeIndexRules(){
		
	}
	public NodeIndexRules(int nodeId){
		this.nodeId = nodeId;
	}
	
	public NodeIndexRules(int nodeId, int docTypeId){
		this.nodeId = nodeId;
		this.docTypeId = docTypeId;
	}

	public NodeIndexRules(String dbstring){
    	try{
	        StringTokenizer st = new StringTokenizer(dbstring, "\t");
	        setNodeId(Util.to_Number(st.nextToken()));
	        setDocTypeId(Util.to_Number(st.nextToken()));
	        //setCreatorName(st.nextToken());
	        
	        //for(int count=0; count < indicesCount; count++){
	        indicesCount = 0;
	        while(st.hasMoreTokens()){
	        	try{
	        		String id = st.nextToken();
	        		String value =st.nextToken(); 
	        	indicesRulesList.add(id + Util.SepChar + value);
	        	indicesCount++;
	        	}catch(Exception ex){
	        		
	        	}
	        }
	        setIndicesCount(indicesCount);
	        setIndicesRulesList(indicesRulesList);
	        //setCreatorName(st.nextToken());
	        //setCreatedOn(st.nextToken());	    
	        updateIndicesList();
    	}catch(Exception ex){    		
    	}
	}
	
	/**
	 * @return Returns the indicesRulesList.
	 */
	public ArrayList getIndicesRulesList() {
		return indicesRulesList;
	}
	/**
	 * @param indicesRulesList The indicesRulesList to set.
	 */
	public void setIndicesRulesList(ArrayList indicesRulesList) {
		this.indicesRulesList = indicesRulesList;
	}
	public void updateIndicesList(){
		indicesList = new Index[indicesCount];
		Index index = null;
		StringTokenizer tokens = null;
		for(int count=0;count < indicesCount; count++){
			if(indicesRulesList!=null && indicesRulesList.size()>0){
				tokens = new StringTokenizer(indicesRulesList.get(count).toString(), Util.SepChar);
				index = new Index(Util.to_Number(tokens.nextToken()));
				index.setValue(tokens.nextToken());
				indicesList[count] = index;
				tokens = null;
			}
		}
		
	}
	/**
	 * @return Returns the indicesList.
	 */
	public Index[] getIndicesList() {
		return indicesList;
	}
	/**
	 * @param indicesList The indicesList to set.
	 */
	public void setIndicesList(Index[] indicesList) {
		this.indicesList = indicesList;
	}
	/**
	 * @return Returns the createdOn.
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn The createdOn to set.
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return Returns the creatorName.
	 */
	public String getCreatorName() {
		return creatorName;
	}

	/**
	 * @param creatorName The creatorName to set.
	 */
	public void setCreatorName(String creatorName) {
		this.creatorName = creatorName;
	}

	/**
	 * @return Returns the indicesCount.
	 */
	public int getIndicesCount() {
		return indicesCount;
	}
	/**
	 * @param indicesCount The indicesCount to set.
	 */
	public void setIndicesCount(int indicesCount) {
		this.indicesCount = indicesCount;
	}
	/**
	 * @return Returns the docTypeId.
	 */
	public int getDocTypeId() {
		return docTypeId;
	}

	/**
	 * @param docTypeId The docTypeId to set.
	 */
	public void setDocTypeId(int docTypeId) {
		this.docTypeId = docTypeId;
	}

	/**
	 * @return Returns the indexId.
	 */
	public int getIndexId() {
		return indexId;
	}

	/**
	 * @param indexId The indexId to set.
	 */
	public void setIndexId(int indexId) {
		this.indexId = indexId;
	}

	/**
	 * @return Returns the indexValue.
	 */
	public String getIndexValue() {
		return indexValue;
	}

	/**
	 * @param indexValue The indexValue to set.
	 */
	public void setIndexValue(String indexValue) {
		this.indexValue = indexValue;
	}

	/**
	 * @return Returns the nodeId.
	 */
	public int getNodeId() {
		return nodeId;
	}

	/**
	 * @param nodeId The nodeId to set.
	 */
	public void setNodeId(int nodeId) {
		this.nodeId = nodeId;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
