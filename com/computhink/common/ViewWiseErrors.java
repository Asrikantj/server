package com.computhink.common;

import com.computhink.resource.ResourceManager;

public interface ViewWiseErrors
{
	int NoError = 0;
	int Error = -1;

	int True = 1;
	int False = NoError;

	String E1 = ResourceManager.getDefaultManager().getString("ViewWiseERR.GeneralErr");

	int invalidParameter = -99;
	String E99 = ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidArgument");

	int documentNotFound = -199;
	String E199 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DocNotFound");
	int invalidCredentials=-233;
	String E233=ResourceManager.getDefaultManager().getString("ViewWiseERR.azureInvalidCredentials");
	int uploadFailed = -277;
	String E277 = ResourceManager.getDefaultManager().getString("ViewWiseERR.UploadFailed");
	
	int azurecopyFailed=-266;
	String E266=ResourceManager.getDefaultManager().getString("ViewWiseERR.CopyFailed");
	
	int azureActiveVersionFailed=-255;
	String E255=ResourceManager.getDefaultManager().getString("ViewWiseERR.ActiveVersFailed");
	
	int azureChangeDoctypeFailed=-244;
	String E244=ResourceManager.getDefaultManager().getString("ViewWiseERR.ChangeDoctypeFailed");
	
	int azureDelVersionsFailed=-288;
	String E288=ResourceManager.getDefaultManager().getString("ViewWiseERR.DelVersFailed");
	
	int documentNotSet = -299;
	String E299 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DocNotSet");
	
	int renameAzureDoc = -201;
	String E201 = ResourceManager.getDefaultManager().getString("ViewWiseERR.UploadVersionFailed");
	
	int documentHasNoPage = -300;
	String E300 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DocNoPages");

	int dssInactive = -399;
	String E399 = ResourceManager.getDefaultManager().getString("ViewWiseERR.StorageServInactive");

	int storageNotDefined = -400;
	String E400 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoStorageLocDef");

	int parentNotFound = -401;
	String E401 = ResourceManager.getDefaultManager().getString("ViewWiseERR.ParentNode");
	int dssCriticalError=-507;
	String E507=ResourceManager.getDefaultManager().getString("ViewWiseERR.DSSLowDiskSpace");		
	int CopyNodeUniqSeqViolation=-509;
	String E509=ResourceManager.getDefaultManager().getString("ViewWiseERR.CopyUniqeViolation");		
	
	int invalidSessionId = -499;
	String E499 = ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidSessionId1") 
		+ ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidSessionId2")
		+ ResourceManager.getDefaultManager().getString("CVProduct.Name")+" "+ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidSessionId3");

	int destinationRoomServerInactive = -599;
	String E599 = ResourceManager.getDefaultManager().getString("CVProduct.Name")+ ResourceManager.getDefaultManager().getString("ViewWiseERR.DestRoomServInactive");

	int documentAlreadyLocked = -699;
	String E699 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.DocLocked");

	int accessDenied = -799;
	String E799 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.AccessDenied");

	int LoginCountExceeded = -800;
	String E800 = ResourceManager.getDefaultManager().getString("ViewWiseERR.LoginCountExceed");

	int versionNotFound = -899;
	String E899 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DocVerDoesNotExist");

	int invalidAIPComponents = -999;
	String E999 =ResourceManager.getDefaultManager().getString( "ViewWiseERR.InvalidAIP");

	int    checkUserError = -100;
	String E100 = ResourceManager.getDefaultManager().getString("ViewWiseERR.FailedtoAuthenticate");

	int    clientNotFoundError = -101;
	String E101 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.ClientNotFound");

	int    NotAllowed = -102;
	String E102 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NotAllowed");

	int    DelNodeError = -103;
	String E103 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DelNodeError");
	
	int    DelDocVersError = -104;
	String E104 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DelDocVersError");

	int    SetDocVerError = -105;
	String E105 = ResourceManager.getDefaultManager().getString("ViewWiseERR.ErrorWhileActiveDocVer");

	int    NoDataFoundError = -106;
	String E106 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoDataFound");

	int    CreateSearchError = -107;
	String E107 = ResourceManager.getDefaultManager().getString("ViewWiseERR.SavingSearch");

	int    GetDataError = -108;
	String E108 = ResourceManager.getDefaultManager().getString("ViewWiseERR.RetrivingData");

	int    invalidReference = -109;
	String E109 = ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidDocRef");

	int    invalidPassword = -110;
	String E110 = ResourceManager.getDefaultManager().getString("ViewWiseERR.PasswordNotBlank");
	
	int 	NodeInUse = -111;
	String E111=ResourceManager.getDefaultManager().getString("ViewWiseERR.NodeInWorkflow");
	
	int 	NodeInRetention = -112;	
	String E112=ResourceManager.getDefaultManager().getString("ViewWiseERR.NodeInRetention");
	// Database Errors
	int    DatabaseError = -2;
	String E2 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DatabaseFailure");

	int    UnableToStore = -3;
	String E3 = ResourceManager.getDefaultManager().getString("ViewWiseERR.UserDoesNotExist");

	int    AclDoesNotExist = -4;
	String E4 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoAcl");

	int    AclEntryDoesNotExist = -5;
	String E5 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoAclEntry");

	int    AgentDocumentDoesNotExist = -6;
	String E6 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoAgentDoc");

	int    GroupDoesNotExist = -7;
	String E7 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoGroup");

	int    PrincipalDoesNotExist = -8;
	String E8 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoPrincipal");

	int    NodeDoesNotExist = -9;
	String E9 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoNode");

	int    UserDoesNotExist = -10;
	String E10 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NOUser");

	int    NotADocumentNode = -11;
	String E11 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NONodeInDoc");

	int    NotAFolderNode = -12;
	String E12 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NodeisNotFolder");

	// Server Errors
	int    ServerNotFound = -13;
	String E13 = ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ResourceManager.getDefaultManager().getString("ViewWiseERR.ServInactive");

	int    ServerExiting = -14;
	String E14 = ResourceManager.getDefaultManager().getString("ViewWiseERR.SeverExiting");

	int    ServerNotAvailable = -15;
	String E15 = ResourceManager.getDefaultManager().getString("ViewWiseERR.ServerNotAvailable");

	int    LostCommunicationWithServer = -16;
	String E16 = ResourceManager.getDefaultManager().getString("ViewWiseERR.LostCommunication");

	int    RemoteMethodException = -17;
	String E17 = ResourceManager.getDefaultManager().getString("ViewWiseERR.RemoteMethodExcep");

	int    RuntimeException = -18;
	String E18 = ResourceManager.getDefaultManager().getString("ViewWiseERR.RuntimeExcep");

	int    NoMoreLicenseSeats = -19;
	String E19 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoMoreLicense");
	int    UserDoesNotExistINDB = -45;
	String E45 = ResourceManager.getDefaultManager().getString("UserDoesNotExistINDB");
	int    AdminUsersCountExceedsLicenseSeats = -190;
	/*String E191 = "You currently have more users in the "+ Constants.PRODUCT_NAME +" named user group than you are licensed for.\n"   
				 + "Please remove users to at or below your licensed seats or contact your Computhink representative about additional licenses.";*/
	String E190 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.AdminLicenseCountExceed");
	//Added for License changes, Madhavan.B
	int    NamedUsersCountExceedsLicenseSeats = -191;
	/*String E191 = "You currently have more users in the "+ Constants.PRODUCT_NAME +" named user group than you are licensed for.\n"   
				 + "Please remove users to at or below your licensed seats or contact your Computhink representative about additional licenses.";*/
	String E191 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.NamedLicenseCountExceed");
	
	
	int    NamedOnlineCountExceedsLicenseSeats = -195;
	/*String E191 = "You currently have more users in the "+ Constants.PRODUCT_NAME +" named user group than you are licensed for.\n"   
				 + "Please remove users to at or below your licensed seats or contact your Computhink representative about additional licenses.";*/
	String E195 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.NamedOnlineLicenseCountExceed");
	
    //
	int    ProfessionalNamedUsersCountExceedsLicenseSeats = -192;
	String E192=ResourceManager.getDefaultManager().getString("PRODUCT_NAME")+ResourceManager.getDefaultManager().getString("ViewWiseERR.ProfessionalNamedLicenseCountExceed");
	
	int ConcurrentUsersCountExceedsLicenseSeats=-193;
	String E193 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.ConcurrentLicenseCountExceed");
	
	int ConcurrentOnlineCountExceedsLicenseSeats=-196;
	String E196 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.ConcurrentOnlineLicenseCountExceed");
	
	
	int ProfConcrUsersCountExceedsLicenseSeats=-194;
	String E194 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.ProfConcrLicenseCountExceed");
	
	int PublicWACountExceedsLicenseSeats=-197;
	String E197 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.PublicWALicenseCountExceed");

	int SubAdminUsersCountExceedsLicenseSeats = -198;
	String E198 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.SubAdminLicenseCountExceed");
	
	int BatchInputAdminUsersCountExceedsLicenseSeats = -200;
	String E200 = ResourceManager.getDefaultManager().getString("PRODUCT_NAME") + ResourceManager.getDefaultManager().getString("ViewWiseERR.BatchAdminLicenseCountExceed");
	
	int    ParentNodeDoesNotExist = -20;
	String E20 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.NoParentNode");

	int    InvalidPrincipalName = -21;
	String E21 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidPrincipal");

	int    SearchCriteriaNotSpecified = -23;
	String E23 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.NoSearchCriteria");

	int    LockDenied = -24;
	String E24 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.LockDenied");

	int    NotLocked = -25;
	String E25 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.LockDenied");

	int    InvalidDirectoryType = -26;
	String E26 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidDirectory");

	int    GetInitialContextFailed = -27;
	String E27 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.ContextFailed");

	int    ArchiveFailed = -28;
	String E28 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.ArchiveFailed");

	int    DefaultLocationNotFound = -29;
	String E29 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.LocationNotFound");

	int    IPAlreadyConnect = -30;
	String E30 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.CredentialConflict");

	int    DocumentDeleted = -31;
	String E31 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.DocDel");

	int    DisconnectedClient = -32;
	String E32 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.ClientConnected");

	int NotAllowedConnections = -33;
	String E33 =  ResourceManager.getDefaultManager().getString("ViewWiseERR.NoConnectionsAllowed");

	int JarFileMisMatchErr = -34;
	String E34 = ResourceManager.getDefaultManager().getString("CVProduct.Name")+" "+ ResourceManager.getDefaultManager().getString("ViewWiseERR.ClienServVerMisMatch");

	int NativeLibraryErr = -44;
	String E44 =ResourceManager.getDefaultManager().getString("ViewWiseERR.NavtiveLib");

	/*Backup error 601-650*/

	int SomeDocLocked = -601;
	String E601 = ResourceManager.getDefaultManager().getString("ViewWiseERR.SomeDocLocked");

	int NoDocumentToCheckOut = -602;
	String E602 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoDocCheckOut");

	int DesFolderWriteErr = -603;
	String E603 = ResourceManager.getDefaultManager().getString("ViewWiseERR.WritingToDestFold");

	int getVWDocErr = -604;
	String E604 =ResourceManager.getDefaultManager().getString("ViewWiseERR.DocReading1")
			+ ResourceManager.getDefaultManager().getString("CVProduct.Name")
			+ResourceManager.getDefaultManager().getString("ViewWiseERR.DocReading2");

	/*Restore error 701-750*/

	int DocumentInfoErr = -701;
	String E701 = ResourceManager.getDefaultManager().getString("ViewWiseERR.ReadingDocInfo");

	int CreateParentNodeErr = -702;
	String E702 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CreatingParentNode");

	int CreateDocTypeErr = -703;
	String E703 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CreatingDocType");

	int CreateDocumentErr = -704;
	String E704 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CreatingDoc");

	int DocumentAlreadyRestored = -705;
	String E705 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DocAlreadyRestored");

	int IndexAlreadyExists = -706;
	String E706 = ResourceManager.getDefaultManager().getString("ViewWiseERR.IndexAlreadyExists");

	int NoDocumentToCheckIn = -707;
	String E707 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoDocumentToCheckIn");

	int readOnlyDocumentErr = -708;
	String E708 = ResourceManager.getDefaultManager().getString("ViewWiseERR.ReadOnlyDocumentErr");

	int    CreateSignTemplateError = -150;
	String E150 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CreateSignTemplateError");

	int alreadyCheckOutDocument = -709;
	String E709 = ResourceManager.getDefaultManager().getString("ViewWiseERR.AlreadyCheckOutDocument"); 

	int CheckUniquenessIndexErr = -710;
	String E710 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CheckUniquenessIndexErr");

	int checkOutDenied = -715;
	String E715 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CheckOutDenied");
	
	int pageCountMismatch=-716;
	String E716 = ResourceManager.getDefaultManager().getString("ViewWiseERR.PageCountMisMatch");

	int CreateDocFileErr = -500;
	String E500 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CreateDocFileErr");

	int CreateCachErr = -501;
	String E501 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CreateCachErr");

	int AddingPageErr = -502;
	String E502 =ResourceManager.getDefaultManager().getString( "ViewWiseERR.AddingPageErr");

	int FileNotFoundErr = -503;
	String E503 = ResourceManager.getDefaultManager().getString("ViewWiseERR.FileNotFoundErr");

	int DocTextNotFoundErr = -504;

	/* Issue No / Purpose:  761 1.	Non recording of proper error message in case of exception,
	 * Error code was wrong
	 * Created by: C.Shanmugavalli  Date: 20 Nov 2006*/

	String E504 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DocTextNotFoundErr");
	//String E505 = "Document has no text";
	/*
Issue No / Purpose:  <563/Adminwise-Restore>
Created by: <Pandiya Raj.M>
Date: <21 Jul 2006>
	 */

	int CheckDocumentTypeErr = -811;
	String E811 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CheckDocumentTypeErr");

	int CheckDocumentTypeErr1 = -8001;
	String E8001 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CheckDocumentTypeErr1");

	int CheckDocumentTypeErr2 = -8002;
	String E8002 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CheckDocumentTypeErr2");

	int CheckDocumentTypeErr4 = -8004;
	String E8004 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CheckDocumentTypeErr4");

	int CheckDocumentTypeErr8 = -8008;
	String E8008 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CheckDocumentTypeErr8");


	
	int CancelRestoreProcess = -712;
	String E712 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CancelRestoreProcess");

	int CancelCreateDocTypeProcess = -713;
	String E713 = ResourceManager.getDefaultManager().getString("ViewWiseERR.CancelCreateDocTypeProcess");  
	int DocumentTypeNameEmptyErr = -714;
	String E714 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DocumentTypeNameEmptyErr");
	int securityErrorCode=-387;
	String E387 = ResourceManager.getDefaultManager().getString("ViewWiseERR.securityErrorCode");
	int DOCUMENT_TYPE_SUCCESS = 0;   
	int DOCUMENT_TYPE_MISMATCH = 1; 
	int DOCUMENT_TYPE_NOT_EXIST = 2;
	int DOCUMENT_INDEX_NOT_EXIST = 3;
	int DOCUMENT_TYPE_FAILED = 4;
	int DOCUMENT_TYPE_MISMATCH_SKIP_DOCUMENT = 5;
	int DOCUMENT_INDEX_EMPTY= 6;
	int connectionaAvailable = 0;
	int IdleTimeout = 101;          
	int DSSZipCorrupted = -395;	
	String E395 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DocCorrupt");
	
	int DSSDiskSpaceNotAvailable = -396;
	String E396 = ResourceManager.getDefaultManager().getString("ViewWiseERR.DSSDiskSpaceNotAvailable");
	//commented for,Bug 391 - Administration : DO NOT generate a status report at end of workflow if the Location is left blank.
	String E508=ResourceManager.getDefaultManager().getString("ViewWiseERR.DocTypePermission");
	int DocumentTypeNotPermitted = -508;

	int dsnConnError=-779;
	String E779=ResourceManager.getDefaultManager().getString("ViewWiseERR.dsnConnError");
	
	int InvalidDateMask= -113;
	String E113=ResourceManager.getDefaultManager().getString("ViewWiseERR.formatMaskMismatch");
	/**CV10.2 - Added for DB Lookup Connection error message**/
	int dsnLkupConnError = -900;
	String E900=ResourceManager.getDefaultManager().getString("ViewWiseERR.dsnLkupConnError");
	/***End of DBLookup code merges************/
	int allDotZipDoesNotExist = -50;
	String E50=ResourceManager.getDefaultManager().getString("ViewWiseERR.allDotZipDoesNotExistError");
	int syncFailed = -40;
	String E40=ResourceManager.getDefaultManager().getString("ViewWiseERR.syncFailed");
	int storageNotFound = -301;
	String E301=ResourceManager.getDefaultManager().getString("ViewWiseERR.StorageLocationNotAvailable");
	/*int InvalidAllDotZip = -393;
	String E393 = ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidAllDotZip");
	int ClientZipCorrupted = -394;	
	String E394 = ResourceManager.getDefaultManager().getString("ViewWiseERR.ClientDocCorrupt");*/
	int NoSubAdminRolesFound = -22;
	String E22 = ResourceManager.getDefaultManager().getString("ViewWiseERR.NoSubAdminRolesFound");
	int invalidDesktopClientAccess  = -35;
	String E35 = ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidDesktopClientAccess");
	int invalidWebClientAccess  = -36;
	String E36 = ResourceManager.getDefaultManager().getString("ViewWiseERR.InvalidWebClientAccess");
}