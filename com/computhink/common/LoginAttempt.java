/*
 * loginAttempt.java
 *
 * Created on April 23, 2005, 1:37 PM
 */

package com.computhink.common;

import java.text.SimpleDateFormat;
import java.util.Calendar;
/**
 *
 * @author  administrator
 */
import java.util.Date;

import com.computhink.vws.server.VWSPreferences;
public class LoginAttempt implements java.io.Serializable {
    
    private long date;
    private String user;
    private String ip;
    private String room;
    private int type;
    public  String now="";
    public  String releaseDtTime="";
    public LoginAttempt(String room, int type, String ip, String user) 
    {
        this.room = room;
        this.type = type;
        this.ip = ip;
        this.user = user;
    }
    
    public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getNow() {
		return now;
	}

	public void setNow(String now) {
		this.now = now;
	}

	public String getReleaseDtTime() {
		return releaseDtTime;
	}

	public void setReleaseDtTime(String releaseDtTime) {
		this.releaseDtTime = releaseDtTime;
	}

	public java.lang.String getIp() {
        return ip;
    }
    public void setIp(java.lang.String ip) {
        this.ip = ip;
    }
    public java.lang.String getRoom() {
        return room;
    }
    public void setRoom(java.lang.String room) {
        this.room = room;
    }
    public long getDate() {
        return date;
    }
    public void setDate(long date) {
        this.date = date;
    }
    public java.lang.String getName() {
        return user;
    }
    public void setName(java.lang.String user) {
        this.user = user;
    }
    public int getType() {
        return type;
    }
    public void setType(int type) {
        this.type = type;
    }
    /**
     * Method modified for 
     * Enhancement:- 2083 Easy access to locked  users
     * Locked time and lock release time update properly with 
     * current time, release time=curtime+ lock time in minutes
     * 
     */
    Calendar cal = Calendar.getInstance();
    Calendar cal1 = Calendar.getInstance();
    public String toString()
    {

    	java.text.SimpleDateFormat sdf1 = 
    			new java.text.SimpleDateFormat("hh:mm:ss");
    	java.text.SimpleDateFormat sdf2 = 
    			new java.text.SimpleDateFormat("mm:ss");
    	Date lockTime = new Date(System.currentTimeMillis() - date);
    	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
    	if(now.equals(""))
    		now = sdf.format(cal.getTime());
    	int loginlockTime=  VWSPreferences.getLoginLockTime();
    	cal1.add(cal.MINUTE, loginlockTime);
    	if(releaseDtTime.equals(""))
    		releaseDtTime=sdf.format(cal1.getTime());
    	return now +" "+ user + "@" + ip +" "+releaseDtTime;

    }
}
