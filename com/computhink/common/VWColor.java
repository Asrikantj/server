package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;

public class VWColor implements Serializable
{
	private int id = 0;
	private String colorNames = "";
	private String colorCode = "";
	public VWColor(String dbString)
	{
        StringTokenizer st = new StringTokenizer(dbString, "\t");
        try
        {
            this.id = Util.to_Number(st.nextToken());
            this.colorNames = st.nextToken();
            this.colorCode = st.nextToken();
          
        }
        catch(Exception e){}
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getColorNames() {
		return colorNames;
	}
	public void setColorNames(String colorNames) {
		this.colorNames = colorNames;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	
}
