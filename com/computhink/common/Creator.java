/*
 * Creator.java
 *
 * Created on January 3, 2004, 5:54 PM
 */

package com.computhink.common;

/**
 *
 * @author  Administrator
 */
public class Creator implements java.io.Serializable
{
    
    private int id = 0;
    private String name = "";
    private String email = "";
    private String userdn="";
    
    public Creator(int id) 
    {
        this(id, "","","");
    }
    public Creator(int id, String name) 
    {
        this.id = id;
        this.name = name;
    }
    public Creator(int id, String name,String email) 
    {
        this.id = id;
        this.name = name;
        this.email = email;
       
    }
    /**CV2019 - Code merges from CV10.2 ***/
    public Creator(int id, String name,String email,String userdn) 
    {
        this.id = id;
        this.name = name;
        this.email = email;
        this.userdn=userdn;
    }
    /****End of CV10.2 merges************/
	public int getId() 
    {
        return this.id;
    }
    public void setId(int id) 
    {
        this.id = id;
    }
    public String getName() 
    {
        return this.name;
    }
    public void setName(String name) 
    {
         this.name = name;
    }
    public String toString()
    {
        return this.name;
    }
	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**CV2019 - Code merges from CV10.2 ***/
	public String getUserdn() {
		return userdn;
	}
	public void setUserdn(String userdn) {
		this.userdn = userdn;
	}
	/****End of CV10.2 merges************/
}
