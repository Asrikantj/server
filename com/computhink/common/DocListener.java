package com.computhink.common;

import javax.swing.JComponent;
import javax.swing.event.*;

public class DocListener implements DocumentListener
{
    private JComponent component;

    public DocListener(JComponent component)
    {
        this.component = component;
    }
    public void changedUpdate(DocumentEvent e)
    {
       component.setEnabled(true);
    } 
    public void insertUpdate(DocumentEvent e)
    {
       component.setEnabled(true);
    } 
    public void removeUpdate(DocumentEvent e)
    {
        component.setEnabled(true);
    }
}
