/*
 * VWDoc.java
 *
 * Created on September 9, 2003, 11:13 AM
 */

package com.computhink.common;
/**
 *
 * @author  Saad
 */
import java.io.*;
import java.util.*;
import java.nio.*;

import com.computhink.dss.server.DSSLog;

public class VWDoc implements Constants, Serializable
{
    private int id;
    private int pageCount = 0;
    private long modifyDate = 0L;
    private String dstFile;        
    private boolean integrity;
    private boolean srcIsFolder;
    private byte[] bytesToWrite;
    private long bytesWritten;
    private long totalBytesWritten;
    private String currentFile;
    private String sourceFile;
    private long size = 0;
    private static final int chunk = 1048576 * 4;
    private boolean vaildZip = false;
    private int docStatus = 0;
    
    /*
     * Document status is used to find the reason for the invalid zip
     * 0 - Doument is valid
     * 1 - Document is invalid and Diskspace not available 
     * 2 - Document is valid and Diskspace not enough and it is less than 50 MB
     */
    public static final int VALID = 0;
    public static final int DISKSPACE_NOT_AVAILABLE = 1;
    public static final int INSUFFICIENT_DISKSPACE = 2;
    
    
    private Hashtable files = new Hashtable();
    
    public VWDoc()
    {
    }
    public VWDoc(int id)
    {
        this.id = id;
    }
    private void setSize()
    {
        Enumeration enumV = files.elements();
        while(enumV.hasMoreElements())
        {
            Vector chunks = (Vector) enumV.nextElement();
            for (int i = 0; i < chunks.size(); i++)
            {
                size += ((Integer) chunks.elementAt(i)).intValue();
            }
        }
    }
    public long getSize()
    {
        return size;
    }
    public boolean getIntegrity()
    {
        long docSize =  getSize();
        if (docSize > 0 && docSize == totalBytesWritten)
            return true;
        else
            return false;
    }
    public String getSrcFile()
    {
        return sourceFile;
    }
    public String getDstFile()
    {
        return dstFile;
    }
    public boolean starting()
    {
        return (totalBytesWritten == 0);
    }
    public boolean finished()
    {
        return (files.size() == 0);
    }
    public boolean hasMoreChunks()
    {
        removeWritten();
        if (currentFile == null) return false;
        setNextChunk();
        return true;
    }
    public void setDstFile(String file)
    {
        dstFile = file;
    }
    public void setSrcFile(String file)
    {
        sourceFile = file;
        reset();
        File f = new File(file);
        if (f.isFile())
        {
            
            Vector chunks = setContents(file);
            if (chunks != null)
            {
                files.put(file, chunks);
                currentFile = file;
            }
            srcIsFolder = false;
        }
        else if (f.isDirectory())
        {
            File[] fa = f.listFiles();
            if (fa.length > 0)
            {
                setDirectoryContents(fa);
            }
            srcIsFolder = true;
        }
        setSize();
    }
    private void reset()
    {
        files = new Hashtable();
        currentFile = null;
        bytesToWrite = null;
        totalBytesWritten = 0;
    }
    private void setDirectoryContents(File[] directory)
    {
        String fn = directory[0].getPath();
        Vector chunks = setContents(fn);
        if (chunks != null)
        {
            files.put(fn, chunks);
            currentFile = fn;
        }
        for (int i = 1; i < directory.length; i++)
        {
            File file = directory[i];
            int fileSize = (int) file.length();
            chunks = new Vector();
            if (chunk >= fileSize)
            {
                chunks.addElement(new Integer(fileSize));
            }
            else
            {
                int numOfChunks =  fileSize / chunk;                
                if (numOfChunks >= 1)
                {
                    for (int j = 0; j < numOfChunks; j++) 
                    {
                        chunks.addElement(new Integer(chunk));
                    }
                }
                chunks.addElement(new Integer(fileSize - (numOfChunks * chunk)));
            }
            files.put(file.getPath(), chunks);
            if (currentFile == null)  currentFile = file.getPath();
        }
    }
    private Vector setContents(String fn)
    {
        File file = new File(fn);
        if (!file.exists()) return null;
        long fileSize =  file.length();
        if (chunk >= fileSize)
            readChunk(file, 0, (int)fileSize);
        else
            readChunk(file, 0, chunk);
        Vector chunks = new Vector();
        if (chunk >= fileSize)
        {
            chunks.addElement(new Integer((int)fileSize));
            return chunks;
        }
        int numOfChunks =  (int)(fileSize / chunk);
        for (int i = 0; i < numOfChunks; i++) 
        {
            chunks.addElement(new Integer(chunk));
        }
        // Last chunk
        chunks.addElement(new Integer((int)(fileSize - (numOfChunks * chunk))));
        return chunks;
    }
    public int writeContents()
    {
        if (bytesToWrite == null || dstFile == null ) return -1;  
        /* Issue: Restore document didn't work for Linux.
        * Reason - file.getName() gives name with full file path when windows path goes 
        * to Linux box. So getting AbsolutePath 
        * and taking only the file name based on file separator character. 7 March 2008*/
        /* Issue 628: all.zip is created out side store location with file like this 'Image Store\10\all.zip'
        * when DSS is running in linux and vws in windows.  
        * dstFile path was going wrong like this 'usr/Image_Store\15\all.zip' which is changed into 'usr/Image_Store/15/all.zip' */
        String df;
        String tempCurrentFile = "";
        File fileNew = new File(currentFile);
        String fileAbsPath = fileNew.getAbsolutePath();
        
        if(fileAbsPath.indexOf("\\")!=-1  && fileAbsPath.indexOf("/")!=-1  && File.separatorChar == '/'){
        	if(fileAbsPath.indexOf("\\")!=-1 ){
        		int ind = fileAbsPath.lastIndexOf("\\");
            	tempCurrentFile = fileAbsPath.substring(ind+1);
        	}
        }else{
        	tempCurrentFile = new File(currentFile).getName();
        }
    	if (srcIsFolder){
    		if(dstFile.substring(dstFile.length()-1).equalsIgnoreCase(""+File.separatorChar))
    			df = dstFile + tempCurrentFile;
    		else
    			df = dstFile + Util.pathSep + tempCurrentFile;
        }else{
        	if(dstFile.indexOf("\\")!=-1  && dstFile.indexOf("/")!=-1 ){
        		String newdstFile = "";
        		try{
        			newdstFile = dstFile;
        			/* temp.replaceAll("\\", "/"); This replaceAll will not work and throws PatternSyntaxException  
        			* To resolve this problem is it must be temp.replaceAll("\\\\","/"); 
        			*/
        			if((File.separatorChar == '/')&& newdstFile.indexOf("\\")!=-1){
        				newdstFile = newdstFile.replaceAll("\\\\", "/");
        			}
        			if((File.separatorChar == '\\')&& newdstFile.indexOf("/")!=-1){
        				newdstFile = newdstFile.replaceAll("/", "\\\\");
        			}
        			dstFile = newdstFile;
        		}catch(Exception ex){
        			DSSLog.err("Error in writeContents - "+ex.toString());
        			return -1;
        		}
        	}
            df = dstFile;
        }
        return writeChunk(df);
    }
    private int writeChunk(String file)
    {
        File folder = new File(new File(file).getParent());
        DSSLog.dbg("File Size before writing chunk"+folder.length());    
        RandomAccessFile fileOut = null;
        try
        {
            if (!folder.exists()) folder.mkdirs();
            File oldFile = new File(file);
            if (bytesWritten == 0 && oldFile.exists()) oldFile.delete();
            fileOut = new RandomAccessFile(file, "rwd");
            fileOut.seek(bytesWritten);
            fileOut.write(bytesToWrite);
            fileOut.close();
            bytesWritten += bytesToWrite.length;
            totalBytesWritten += bytesToWrite.length;    
            DSSLog.dbg("Total bytes written::"+totalBytesWritten);
        }
        catch (Exception e){
        	DSSLog.err("Error while saving document: "+e.toString());
        	return -1;
        }
        finally{
        	try{
        	if (fileOut != null){
        		fileOut.close();
        	}
        	}catch (Exception e) {
				// TODO: handle exception
			}
        }
        bytesToWrite = null;
        return 0;
    }
    private void removeWritten()
    {
        ((Vector) files.get(currentFile)).removeElementAt(0);
        if (((Vector) files.get(currentFile)).size() == 0)
        {
            files.remove(currentFile);
            if (files.size() > 0) 
            {
                currentFile = (String) files.keys().nextElement();
                bytesWritten = 0;
            }
            else
                currentFile = null;
        }
    }
    private void setNextChunk()
    {
        Vector chunks = (Vector) files.get(currentFile);
        int chunk = ((Integer) chunks.elementAt(0)).intValue();
        readChunk(new File(currentFile), bytesWritten, chunk);
    }
    private void readChunk(File file, long off, int len)
    {
        bytesToWrite = new byte[len];
        RandomAccessFile raf = null;
        try
        {
            raf = new RandomAccessFile(file, "r");
            raf.seek(off);
            raf.read(bytesToWrite);
            raf.close();
            raf = null;             
        }
        catch(Exception e){}
        finally{
        	try{
        	if (raf != null){
        		raf.close();
        	}
        	}catch (Exception e) {
				// TODO: handle exception
			}
        }
    }
    public long getModifyDate() 
    {
        return modifyDate;
    }
    public void setModifyDate(long modifyDate) 
    {
        this.modifyDate = modifyDate;
    }
    public void setSize(long size) 
    {
        this.size = size;
    }
    /*Purpose: Issue # 791 Methods added to fix the issue. "Document is not downloaded if the size is greater than 4mb
    * C.Shanmugavalli 19 Dec 2006 
    */
    public long getBytesWritten()
    {
        return totalBytesWritten;
    }
	public boolean isVaildZip() {
		return vaildZip;
	}
	public void setVaildZip(boolean vaildZip) {
		this.vaildZip = vaildZip;
	}
	public int getDocStatus() {
		return docStatus;
	}
	public void setDocStatus(int docStatus) {
		this.docStatus = docStatus;
	}

}
