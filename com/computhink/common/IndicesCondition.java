/*
* IndicesCondition.java
 *
 * Created on February 08, 2010
 */

package com.computhink.common;
/**
 *
 * @author  Vijaypriya.B.K
 */
import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;

public class IndicesCondition implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int retentionId;
	private int docTypeId;
	private int indexId;
    private String indexName;
    private String comparisonOpert;
    private String indexValue1;
    private String indexValue2;
    private String logicalOpert;
    private int indexTypeId;
        
    public IndicesCondition(int id) 
    {
        this(id, "");
    }
    public IndicesCondition(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
        	//id, Retentionid, Indexid, IndexName, IndexValue1, IndexValue2, ComparisonOper, LogicalOper, DocTypeid, Type
        	this.id = Util.to_Number(st.nextToken());
        	this.retentionId = Util.to_Number(st.nextToken());
        	this.indexId = Util.to_Number(st.nextToken());
        	this.indexName = st.nextToken();
        	this.indexValue1 = st.nextToken();
            this.indexValue2 = st.nextToken();
            this.comparisonOpert = st.nextToken();
            this.logicalOpert = st.nextToken();
        	this.docTypeId = Util.to_Number(st.nextToken());
        	this.indexTypeId = Util.to_Number(st.nextToken());
        }
        catch(Exception e){}
    }
    /**
     * Constructor added for CV10.1 Retention Template Changes.
     * Modified By :- Madhavan
     * @param isReteniton
     * @param dbstring
     */
    public IndicesCondition(boolean isReteniton,String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
        	//id, Retentionid, Indexid, IndexName, IndexValue1, IndexValue2, ComparisonOper, LogicalOper, DocTypeid, Type
        	this.id = Util.to_Number(st.nextToken());
        	this.retentionId = Util.to_Number(st.nextToken());
        	this.indexId = Util.to_Number(st.nextToken());
        	this.indexName = st.nextToken();
        	this.comparisonOpert = st.nextToken();
        	this.indexValue1 = st.nextToken();
            this.indexValue2 = st.nextToken();
            this.logicalOpert = st.nextToken();
        	this.docTypeId = Util.to_Number(st.nextToken());
        	this.indexTypeId = Util.to_Number(st.nextToken());
        }
        catch(Exception e){}
    }
    public IndicesCondition(int id, String indexName) 
    {
        this.id = id;
        this.indexName = indexName;
    }
    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getIndexName()
    {
        return this.indexName;
    }
    public void setIndexName(String indexName)
    {
        this.indexName = indexName;
    }
    public String toString()
    {
        return this.indexName;
    }
    public void setConditions(Vector settings)
    {
        if(settings==null || settings.size()==0) 
        {
            return;
        }
        StringTokenizer st = new StringTokenizer((String)settings.get(0), "\t");
        try
        {
        	this.id = Util.to_Number(st.nextToken());
            this.retentionId = Util.to_Number(st.nextToken());
            this.docTypeId = Util.to_Number(st.nextToken());
            this.indexId = Util.to_Number(st.nextToken());
            this.indexName = st.nextToken();
            this.comparisonOpert = st.nextToken();
            this.indexValue1 = st.nextToken();
            this.indexValue2 = st.nextToken();
            this.logicalOpert = st.nextToken();
        }
        catch(Exception e){}
    }
	public String getComparisonOpert() {
		return comparisonOpert;
	}
	public void setComparisonOpert(String comparisonOpert) {
		this.comparisonOpert = comparisonOpert;
	}
	public int getRetentionId() {
		return retentionId;
	}
	public void setRetentionId(int retentionId) {
		this.retentionId = retentionId;
	}
	public String getIndexValue1() {
		return indexValue1;
	}
	public void setIndexValue1(String indexValue1) {
		this.indexValue1 = indexValue1;
	}
	public String getIndexValue2() {
		return indexValue2;
	}
	public void setIndexValue2(String indexValue2) {
		this.indexValue2 = indexValue2;
	}
	public int getDocTypeId() {
		return docTypeId;
	}
	public void setDocTypeId(int docTypeId) {
		this.docTypeId = docTypeId;
	}
	public int getIndexId() {
		return indexId;
	}
	public void setIndexId(int indexId) {
		this.indexId = indexId;
	}
	public String getLogicalOpert() {
		return logicalOpert;
	}
	public void setLogicalOpert(String logicalOpert) {
		this.logicalOpert = logicalOpert;
	}
	public int getIndexTypeId() {
		return indexTypeId;
	}
	public void setIndexTypeId(int indexTypeId) {
		this.indexTypeId = indexTypeId;
	}
	public boolean isSameObject(Object obj){
		//System.out.println("Indices Condition obj instanceof IndicesCondition -> " + (obj instanceof IndicesCondition));
		if (obj instanceof IndicesCondition){
			IndicesCondition srcIndicesCondition = (IndicesCondition) obj; 
			if (srcIndicesCondition.getIndexName().equals(indexName) &&
					srcIndicesCondition.getIndexTypeId() == indexTypeId &&
					srcIndicesCondition.getIndexValue1().equals(indexValue1) &&
					srcIndicesCondition.getIndexValue2().equals(indexValue2) &&
					srcIndicesCondition.getComparisonOpert().equals(comparisonOpert) &&
					srcIndicesCondition.getLogicalOpert().equals(logicalOpert)){
				//System.out.println("Index Condition are same");
				return true;
			}
		}
		//System.out.println("Index Condition are not same");
		return false;	
	}
}