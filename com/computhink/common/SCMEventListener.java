package com.computhink.common;
public interface SCMEventListener
{
    public void handleSCMEvent(SCMEvent event);
}
