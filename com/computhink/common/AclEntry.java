
package com.computhink.common;

import java.util.*;

public class AclEntry implements java.io.Serializable
{
    private int id;
    private int aclId;
    private int type;
    private Principal principal;
    private VWAccess access;
    private boolean inherited = false;
    
    protected AclEntry(Acl a, Principal p)
    {
        this.principal = p;
        this.aclId = a.getId();
        this.type = a.getNode().getType();
        access = new VWAccess();
    }
    public AclEntry(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
            this.id = Util.to_Number(st.nextToken());
            this.aclId = Util.to_Number(st.nextToken());
            this.type = Util.to_Number(st.nextToken());
            this.principal = new Principal(Util.to_Number(st.nextToken()),
                                                st.nextToken(), 
                                                Util.to_Number(st.nextToken()));
            access = new VWAccess(st.nextToken()); 
        }
        catch(Exception e){}
    }
    public int getId()
    {
        return this.id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public int getAclId()
    {
        return aclId;
    }
    public void setAclId(int id)
    {
        this.aclId = id;;
    }
    public int getType()
    {
        return type;
    }
    public void setType(int type)
    {
        this.type = type;
    }
    public Principal getPrincipal()
    {
        return this.principal;
    }
    public void setPrincipal(Principal p)
    {
        this.principal = p;
    }
    public String getMask() 
    {
        return access.getMask();
    }
    public void setMask(String mask) 
    {
        access.setMask(mask);
    }
    public int canNavigate() 
    {
        return access.canNavigate();
    }
    public int canView()
    {
        return access.canView();
    }
    public int canCreate()
    {
       return access.canCreate();
    }
    public int canModify()
    {
        return access.canModify();
    }
    public int canShare()
    {
        return access.canShare();
    }
    public int canDelete()
    {
        return access.canDelete();
    }
    public int canAssign()
    {
        return access.canAssign();
    }
    public void setNavigate(int state)
    {
        access.setNavigate(state);
    }
    public void setView(int state)
    {
        access.setView(state);
    }
    public void setCreate(int state)
    {
        access.setCreate(state);
    }
    public void setModify(int state)
    {
        access.setModify(state);
    }
    public void setShare(int state)
    {
        access.setShare(state);
    }
    public void setDelete(int state)
    {
        access.setDelete(state);
    }
    public void setAssign(int state)
    {
       access.setAssign(state);
    }
//// Security Enhancement 32
    public int canLaunch() 
    {
        return access.canLaunch();
    }

    public void setLaunch(int state)
    {
        access.setLaunch(state);
    }

    public int canExport() 
    {
        return access.canExport();
    }

    public void setExport(int state)
    {
        access.setExport(state);
    }
    public int canPrint() 
    {
        return access.canPrint();
    }

    public void setPrint(int state)
    {
        access.setPrint(state);
    }
    public int canMail() 
    {
        return access.canMail();
    }

    public void setMail(int state)
    {
        access.setMail(state);
    }
    public int canSendTo() 
    {
        return access.canSendTo();
    }

    public void setSendTo(int state)
    {
        access.setSendTo(state);
    }
    
    public int canCheckInOut() 
    {
        return access.canCheckInOut();
    }

    public void setCheckInOut(int state)
    {
        access.setCheckInOut(state);
    }
    
////    
    public VWAccess getAccess() 
    {
        return access;
    }
    public boolean isInherited() 
    {
        return inherited;
    }
    public void setInherited(boolean inherited) 
    {
        this.inherited = inherited;
    }
    
}