/*
 * LogMessage.java
 *
 * Created on December 11, 2003, 2:06 PM
 */

package com.computhink.common;

/**
 *
 * @author  Administrator
 */
import java.util.Date;
import java.awt.Color;
import java.text.*;
import java.io.Serializable;

public class LogMessage implements Serializable
{
    public static final int MSG_INF = 1;
    public static final int MSG_ERR = 2;
    public static final int MSG_WAR = 3;
    public static final int MSG_DBG = 4;
    
    public Date date;
    public String plainMsg;
    public String fullMsg;
    public Color color;
    public String server;
    public String caller;
    public int type;
    
    public LogMessage(String msg, int type) 
    {
        this.fullMsg = msg;
        this.type = type;
        int l = Util.getNow(2).length();
        if (msg.length() > l) setProperties(l);
    }
    private void setProperties(int l)
    {
        try
        {
            this.date = DateFormat.getDateTimeInstance
                                    (DateFormat.SHORT, DateFormat.MEDIUM).parse
                                                     (fullMsg.substring(0, l));
        }
        catch(ParseException pe){}
        this.plainMsg = this.fullMsg.substring(l+1).trim();
        switch (this.type)
        {
            case MSG_INF:
                this.color = new Color(0, 0, 0/*0, 0, 255*/); break;
            case MSG_WAR:
                this.color = new Color(100, 100, 0); ; break;    
            case MSG_DBG:
                this.color = new Color(0, 100, 100); ; break;    
            case MSG_ERR:
                this.color = new Color(200, 50, 0); 
                fullMsg = fullMsg + "==>" + getErrrorOrigin() + "\r\n";
                break;
        }
    }
    private String getErrrorOrigin()
    {
        String errOrg = "";
        String method = ""; 
        String clazz  = "";
        int lineNo = 0;
        StackTraceElement[] stElements = new Throwable().getStackTrace();

        for (int i=0; i <stElements.length; i++)
        {
            clazz = stElements[i].getClassName();
            clazz = clazz.substring(clazz.lastIndexOf(".") + 1);
            if (!clazz.endsWith("Log")) continue;
            method = stElements[i].getMethodName();
            if (!method.equals("err")) continue;
                //---this is the caller method for it follows xxxLog.err
                clazz = stElements[i + 1].getClassName();
                clazz = clazz.substring(clazz.lastIndexOf(".") + 1);
                method = stElements[i + 1].getMethodName();
                lineNo = stElements[i + 1].getLineNumber();
                errOrg = clazz + "." + method + "(" + lineNo +")";
        }
        return errOrg;
    }
    public boolean equals(LogMessage lm)
    {
        if (this.plainMsg.equals(lm.plainMsg)) return true;
        return false;
    }
}
