
package com.computhink.common;

import java.util.*;

public class Principal implements java.io.Serializable
{
    public static final int GROUP_TYPE = 1;
    public static final int USER_TYPE  = 2;

    private int id;
    private String name;
    private int type;
    private String email="";
    private boolean admin=false;
    private boolean subAdmin = false;
    private boolean hasASign=false;
    private boolean hasCSign=false;
    private int userGroupId;
    private boolean trimType=false;
    private String dirName="";
    private String schemeType = null;
    private String ldapDirectory = null;

    public Principal()
    {
    }
    public Principal(int id)
    {
        this.id = id;
    }
    public Principal(int id, String name, int type)
    {
        this.id = id;
        this.name = name;
        this.type = type;
    }
    public Principal(int id, String name, int type, int userGroupId)
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.userGroupId = userGroupId;
    }
    public Principal(int id, String name, int type,String dirName)
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.dirName=dirName;
    }
    /**
     * CV2019 issue fix - If both domains having common group then in the Add dialog its not differentiating. Second configured domain groups need to show with domain name.
     * @param id
     * @param name
     * @param type
     * @param dirName
     * @param schemeType
     * @param ldapDirectory
     */
    public Principal(int id, String name, int type,String dirName, String schemeType, String ldapDirectory)
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.dirName=dirName;
        this.schemeType = schemeType;
        this.ldapDirectory = ldapDirectory;
    }
    public Principal(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
            this.id = Util.to_Number(st.nextToken());
            this.name = st.nextToken();
            this.type = Util.to_Number(st.nextToken());
            
            if(st.hasMoreTokens())
                this.setAdmin(st.nextToken().equals("1"));
            if(st.hasMoreTokens())
            {
                this.setHasASign(!st.nextToken().equals("0"));
                this.setHasCSign(!st.nextToken().equals("0"));
            }
            String mailid = st.nextToken();
            if(mailid.trim().equals("-")) mailid = "";
            this.email=mailid;
        }
        catch(Exception e){}
    }
    public String getPrincipalInfo()
    {
        return String.valueOf(id)+ Util.SepChar + name + Util.SepChar +
        String.valueOf(type) + Util.SepChar + email;
    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public void setType(int type)
    {
        this.type = type;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public int getType()
    {
        return type;
    }
    public int hashCode()
    {
        return new Integer(id).hashCode();
    }
    public void set(Principal newPrincipal)
    {
        this.id = newPrincipal.getId();
        this.name = newPrincipal.getName();
        this.type = newPrincipal.getType();

        this.admin = newPrincipal.isAdmin();
        this.hasASign = newPrincipal.isHasASign();
        this.hasCSign = newPrincipal.isHasCSign();
    }
    public boolean equals(java.lang.Object object)
    {
        if (!(object instanceof Principal))
        {
            return false;
        }

        Principal principal = (Principal) object;
        boolean idMatch = id == 0 || principal.id == 0 ||
            principal.id == id;
        return idMatch &&
            principal.type == type &&
            principal.name.equals(name);
    }
    public String toString()
    {
        String str = "";
        String[] dirList = null;
        if(trimType)
        	str = name.trim();
        else {
        	/**CV2019 issue fix - If both domains having common group then in the Add dialog its not differentiating. Second configured domain groups need to show with domain name.**/
        	/**@author vanitha.s**/
        	str = name.trim();
        	if (schemeType != null && schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
        		if (ldapDirectory != null) {
        			dirList = ldapDirectory.split(",");
        			if (dirList.length > 1) {
        				if (dirName != null && !dirName.equalsIgnoreCase(dirList[0].trim())) {
        					str = str + "@"+dirName;
        				}
        			}
        		}
        	}
        	str = str +(type==GROUP_TYPE?" [Group]":" [User]");
        	/**End of CV2019 add dialog issue fix***************************/
        }
        return str;
    }
    
    public boolean isGroup()
    {
        return (type == GROUP_TYPE);
    }
    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }
    
    /** Getter for property hasASign.
     * @return Value of property hasASign.
     */
    public boolean isHasASign() {
        return hasASign;
    }
    
    /** Setter for property hasASign.
     * @param hasASign New value of property hasASign.
     */
    public void setHasASign(boolean hasASign) {
        this.hasASign = hasASign;
    }
    
    /** Getter for property hasCSign.
     * @return Value of property hasCSign.
     */
    public boolean isHasCSign() {
        return hasCSign;
    }
    
    /** Setter for property hasCSign.
     * @param hasCSign New value of property hasCSign.
     */
    public void setHasCSign(boolean hasCSign) {
        this.hasCSign = hasCSign;
    }
	/**
	 * @return Returns the email.
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email The email to set.
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	public int getUserGroupId() {
		return userGroupId;
	}
	public void setUserGroupId(int userGroupId) {
		this.userGroupId = userGroupId;
	}
	public boolean isTrimType() {
		return trimType;
	}
	public void setTrimType(boolean trimType) {
		this.trimType = trimType;
	}
	public String getDirName() {
		return dirName;
	}
	public void setDirName(String dirName) {
		this.dirName = dirName;
	}
	public String getSchemeType() {
		return schemeType;
	}
	public void setSchemeType(String schemeType) {
		this.schemeType = schemeType;
	}
	public String getLdapDirectory() {
		return ldapDirectory;
	}
	public void setLdapDirectory(String ldapDirectory) {
		this.ldapDirectory = ldapDirectory;
	}
	public boolean isSubAdmin() {
		return subAdmin;
	}
	public void setSubAdmin(boolean subAdmin) {
		this.subAdmin = subAdmin;
	}
    
}