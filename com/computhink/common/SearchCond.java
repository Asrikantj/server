package com.computhink.common;

import java.util.StringTokenizer;

public class SearchCond implements java.io.Serializable
{
    private int serial;
    private int indexId;
    private int cond;
    private int docTypeId;
    private String value1;
    private String value2;
    private int indexType;
    private String indexMask;
    private String docType;
    private String index;
    private String condString;
    
    public static final int COND_EQUAL = 0;
    public static final int COND_GREATER = 1;
    public static final int COND_LESS = 2;
    public static final int COND_GREATER_OR_EQUAL = 3;
    public static final int COND_LESS_OR_EQUAL = 4;
    public static final int COND_NOT_EQUAL = 5;
    public static final int COND_LIKE = 6;
    public static final int COND_BETWEEN = 7;
    public static final int COND_IN = 8;

    public SearchCond()
    {
    }
    public SearchCond(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, Util.SepChar);
        ///this.serial=Util.to_Number(tokens.nextToken());
        this.indexId = Util.to_Number(st.nextToken());
        this.cond = Integer.parseInt(st.nextToken());
        this.value1 = st.nextToken();
        this.value2 = st.nextToken();
        this.docTypeId = Integer.parseInt(st.nextToken());
    }
     public int getSerial() {
        return this.serial;
    }
    
    public void setSerial(int serial) {
        this.serial =serial;
    }
    
    public int getIndexId() {
        return indexId;
    }
    public void setIndexId(int id) 
    {
        this.indexId =id;
    }
    public int getCond() {
        return this.cond;
    }
    
    public void setCond(int cond) {
        this.cond =cond;
    }
    
    public int getDocTypeId() {
        return this.docTypeId;
    }
    
    public void setDocTypeId(int docTypeId) {
        this.docTypeId =docTypeId;
    }
    
    public String getValue1() {
        return this.value1;
        
    }
    
    public void setValue1(String value1) {
        this.value1 =value1;
    }
    public String getValue2() 
    {
        return value2;
    }
    public void setValue2(String value2) 
    {
        this.value2 =value2;
    }
    public int getIndexType() 
    {
        return indexType;
    }
    public void setIndexType(int indexType) 
    {
        this.indexType = indexType;
    }
    public String getIndexMask() 
    {
        return indexMask;
    }
    public void setIndexMask(String indexMask) 
    {
        this.indexMask = indexMask;
    }
    public String getDocType() 
    {
        return docType;
    }
    public void setDocType(String docType) 
    {
        this.docType = docType;
    }
    public String getIndex() 
    {
        return index;
    }
    public void setIndex(String index) 
    {
        this.index = index;
    }
	public String getCondString(int cond) {
		if(cond == COND_BETWEEN)
			return "between";
		if(cond ==  COND_EQUAL)
			return "=";
		if(cond == COND_GREATER)
			return ">";
		if(cond == COND_GREATER_OR_EQUAL)
			return ">=";
		if(cond == COND_IN)
			return "in";
		if(cond == COND_LESS)
			return "<";
		if(cond == COND_LESS_OR_EQUAL)
			return "<=";
		if(cond == COND_LIKE)
			return "like";
		if(cond == COND_NOT_EQUAL)
			return "!=";
		else	
			return "";
	}
	public void setCondString(String condString) {
		this.condString = condString;
	}
    
}
