package com.computhink.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class RouteTaskInfo implements Serializable {
	
	private int routeTaskId = 0;
	private int routeId = 0;
	private int taskSequence = 0;
	private String taskName = "";
	private String taskDescritpion = "";
	private int tasklength = 0;
	private int ApproversCount = 0;
	private int OnRejectTaskId = 0;
	private ArrayList RouteUsersList = null;
	private int taskFigureId = -1;
	private int acceptSignatureId = 0;
	private String taskReminder = "";
	private int taskReminderEnabled = 0;
	private String acceptLabel = "";
	private String rejectLabel = "";
	private int emailToPrvTaskUsers = 0; 
	
	 public RouteTaskInfo()
     {
    	
     }
	public RouteTaskInfo(String dbstring) 
    {
    	 StringTokenizer st = new StringTokenizer(dbstring, "\t");
         try
         {
        	this.routeTaskId = Util.to_Number(st.nextToken());
         	this.routeId = Util.to_Number(st.nextToken());
 			this.taskSequence = Util.to_Number(st.nextToken());
 			this.taskName = st.nextToken();
 			this.taskDescritpion = st.nextToken();
 			if(this.taskDescritpion.equalsIgnoreCase("-"))
 				this.taskDescritpion = "";
 			this.tasklength = Util.to_Number(st.nextToken());
 			this.ApproversCount= Util.to_Number(st.nextToken().trim());
 			this.OnRejectTaskId = Util.to_Number(st.nextToken());
 			this.taskFigureId = Util.to_Number(st.nextToken());
 			this.acceptSignatureId = Util.to_Number(st.nextToken());
 			this.taskReminder = st.nextToken();
 			this.taskReminderEnabled = Util.to_Number(st.nextToken());
 			this.acceptLabel = st.nextToken();
 			if(this.acceptLabel.equalsIgnoreCase("-"))
 				this.acceptLabel = "";
 			this.rejectLabel = st.nextToken();
 			if(this.rejectLabel.equalsIgnoreCase("-"))
 				this.rejectLabel = "";
			/****CV2019 merges from SIDBI line***/
 			this.emailToPrvTaskUsers = Util.to_Number(st.nextToken());
         }
         catch(Exception e){
         }
    }
	
	
	public int getApproversCount() {
		return ApproversCount;
	}
	public void setApproversCount(int approversCount) {
		ApproversCount = approversCount;
	}
	public int getOnRejectTaskId() {
		return OnRejectTaskId;
	}
	public void setOnRejectTaskId(int onRejectTaskId) {
		OnRejectTaskId = onRejectTaskId;
	}
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public String getTaskDescritpion() {
		return taskDescritpion;
	}
	public void setTaskDescritpion(String taskDescritpion) {
		if(taskDescritpion.contains("\t")){
			taskDescritpion=taskDescritpion.replaceAll("\t"," ");
		}
		this.taskDescritpion = taskDescritpion;
	}
	public int getTaskSequence() {
		return taskSequence;
	}
	public void setTaskSequence(int taskSequence) {
		this.taskSequence = taskSequence;
	}
	public int getTasklength() {
		return tasklength;
	}
	public void setTasklength(int tasklength) {
		this.tasklength = tasklength;
	}
	public String getTaskName() {
		return taskName;
	}
	public void setTaskName(String taskName) {
		if(taskName.contains("\t")){
			taskName=taskName.replaceAll("\t"," ");
		}
		this.taskName = taskName;
	}
	public ArrayList getRouteUsersList() {
		return RouteUsersList;
	}
	public void setRouteUsersList(ArrayList routeUsersList) {
		RouteUsersList = routeUsersList;
	}
	public int getRouteTaskId() {
		return routeTaskId;
	}
	public void setRouteTaskId(int routeTaskId) {
		this.routeTaskId = routeTaskId;
	}
	/**
	 * @return Returns the taskFigureId.
	 */
	public int getTaskFigureId() {
		return taskFigureId;
	}
	/**
	 * @param taskFigureId The taskFigureId to set.
	 */
	public void setTaskFigureId(int taskFigureId) {
		this.taskFigureId = taskFigureId;
	}
	/**
	 * @return Returns the acceptSignatureId.
	 */
	public int getAcceptSignatureId() {
		return acceptSignatureId;
	}
	/**
	 * @param acceptSignatureId to set.
	 */
	public void setAcceptSignatureId(int acceptSignatureId) {
		this.acceptSignatureId = acceptSignatureId;
	}
	public String getTaskReminder() {
		return taskReminder;
	}
	public void setTaskReminder(String taskReminder) {
		this.taskReminder = taskReminder;
	}
	public int getTaskReminderEnabled() {
		return taskReminderEnabled;
	}
	public void setTaskReminderEnabled(int taskReminderEnabled) {
		this.taskReminderEnabled = taskReminderEnabled;
	}
	public String getAcceptLabel() {
		return acceptLabel;
	}
	public void setAcceptLabel(String acceptLabel) {
		this.acceptLabel = acceptLabel;
	}
	public String getRejectLabel() {
		return rejectLabel;
	}
	public void setRejectLabel(String rejectLabel) {
		this.rejectLabel = rejectLabel;
	}
	/****CV2019 merges from SIDBI line--------------------------------***/
	public int getEmailToPrvTaskUsers() {
		return emailToPrvTaskUsers;
	}
	public void setEmailToPrvTaskUsers(int emailToPrvTaskUsers) {
		this.emailToPrvTaskUsers = emailToPrvTaskUsers;
	}
	/****End of SIDBI line--------------------------------***/
}
