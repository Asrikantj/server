package com.computhink.common;

public class VWEvent implements java.io.Serializable
{
    public static final int ADMIN_DISCONNECT = 100;
    public static final int IDLE_DISCONNECT = 101;
    public static final int ADD_SERVER = 102;
 
    private String server;
    private String room;
    private int id;
    private int sessionId;
    private ServerSchema vws;
     
    public VWEvent(String server, String room, int id, int sid) 
    {
        this.server = server;
        this.room = room;
        this.id = id;
        this.sessionId = sid;
    }
    public java.lang.String getServer() 
    {
        return server;
    }
    public java.lang.String getRoom() 
    {
        return room;
    }
    public int getId() 
    {
        return id;
    }
    public int getSessionId() 
    {
        return sessionId;
    }
    
    public void setSessionId(int sessionId) 
    {
        this.sessionId = sessionId;
    }
    public ServerSchema getVWS() 
    {
        return vws;
    }
    public void setVWS(ServerSchema vws) 
    {
        this.vws = vws;
    }
    
    /** Setter for property id.
     * @param id New value of property id.
     */
    public void setId(int id) {
        this.id = id;
    }
    
}

