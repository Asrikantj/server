package com.computhink.common;

import java.util.prefs.Preferences;

public class CVPreferences implements Constants {

	private static Preferences CVPrefs = null;

	static {
		try {
			CVPrefs = Preferences.systemRoot().node(CV_PREF_ROOT);
		} catch (Exception e) {

		}
	}

	private static Preferences getNode(String node) {
		return CVPrefs.node(node);
	}

	static private void flush() {
		try {
			CVPrefs.flush();
		} catch (java.util.prefs.BackingStoreException e) {

			System.out.println("Exception in flush " + e.getMessage());
		}
	}

	public static void setHosting(boolean hosting) {
		CVPrefs.putBoolean("hosting", hosting);
		flush();
	}

	public static boolean getHosting() {
		return CVPrefs.getBoolean("hosting", false);
	}
}
