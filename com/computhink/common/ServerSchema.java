/*
 * server.java
 *
 * Created on November 23, 2003, 4:26 PM
 */
package com.computhink.common;
/**
 *
 * @author  Administrator
 */
import java.util.*;

public class ServerSchema implements java.io.Serializable
{
    public int    id;
    public String name = "";
    public String address = "";
    public String type = "";
    public String path = "";
    public String room = "";
    public int comport = 0;
    public int dataport = 0;
    public boolean used;
    public boolean isDefault;
    public String hostName = "";
    public String proxyHost  = "";
    public boolean isProxySet  = false;
/*
Issue No / Purpose:  <650/Terminal Services Client>
Created by: <Pandiya Raj.M>
Date: <19 Jul 2006>
Include the new variable "stubIPAddress" to hold the ipaddress of stub object created.
*/        
    public String stubIPaddress = "";
    public String uncPath = "";
    public String storageType = "";
    public String userName = "";
    public String password = "";
    public ServerSchema() 
    {
    }
    public ServerSchema(String dbstring)
    {
        try{
	    	StringTokenizer st = new StringTokenizer(dbstring, "\t");
	        this.id = Util.to_Number(st.nextToken());
	        this.name = st.nextToken();
	        this.address = st.nextToken();
	        this.comport = Util.to_Number(st.nextToken());
	        this.dataport = Util.to_Number(st.nextToken());
	        this.path = st.nextToken();
	        this.uncPath = st.nextToken();
	        this.storageType=st.nextToken();
	        this.userName=st.nextToken();
	        this.password=st.nextToken();
	        try
	        {
	            this.used=st.nextToken().equals("1");
	        }
	        catch(Exception e)
	        {
	            this.used=false;
	        }
	        try
	        {
	            this.isDefault=st.nextToken().equals("1");
	        }
	        catch(Exception e)
	        {
	            this.isDefault=false;
	        }
	        try
	        {
	            this.hostName=st.nextToken();
	        }
	        catch(Exception e)
	        {
	            this.hostName="";
	        }
	        /* If there is some issue in getting storage location. Eg. for some of the cabinet storage 
	        * location is specified and for some it is not specified. Then, ServerSchema constructor is
	        * throwing exception. So catching it and returing -1 for that.*/
        }catch(Exception ex){
        	this.id = -1;
        	this.hostName="";
        }
    }
    public void set(ServerSchema ss)
    {
        this.name = ss.name;
        this.address = ss.address;
        this.comport = ss.comport;
        this.dataport = ss.dataport;
        this.path = ss.path;
        this.type = ss.type;
        this.id=ss.id;
        this.isDefault=ss.isDefault;
        this.hostName=ss.hostName;
        this.stubIPaddress=ss.stubIPaddress;
        this.userName=ss.userName;
        this.password=ss.password;
        this.storageType=ss.storageType;
        		
    }
    public boolean equals(ServerSchema ss)
    {
        if ( this.address == ss.address &&
             this.comport == ss.comport &&
             this.type == ss.type) return true;
        return false;
    }

    public java.lang.String getName() {
        return name;
    }

    public void setName(java.lang.String name) {
        this.name = name;
    }

    public java.lang.String getAddress() {
        return address;
    }

    public void setAddress(java.lang.String address) {
        this.address = address;
    }

    public java.lang.String getType() {
        return type;
    }
    
    public void setType(java.lang.String type) {
        this.type = type;
    }
    
    public java.lang.String getPath() {
        return path;
    }
    
    public void setPath(java.lang.String path) {
        this.path = path;
    }
    
    public java.lang.String getRoom() {
        return room;
    }
    
    public void setRoom(java.lang.String room) {
        this.room = room;
    }
    
    public int getComport() {
        return comport;
    }
    
    public void setComport(int comport) {
        this.comport = comport;
    }
    
    public int getDataport() {
        return dataport;
    }
    
    public void setDataport(int dataport) {
        this.dataport = dataport;
    }
    
    public boolean isUsed() {
        return used;
    }
    
    public void setUsed(boolean used) {
        this.used = used;
    }
    
    public boolean isIsDefault() {
        return isDefault;
    }
    
    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public String toString()
    {
        if(path!=null)
            return name+" : " + path;
         else
            return name;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public java.lang.String getHostName() {
        return hostName;
    }
    
    public void setHostName(java.lang.String hostName) {
        this.hostName = hostName;
    }
    
    public java.lang.String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(java.lang.String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public boolean isIsProxySet() {
        return isProxySet;
    }
    
    public void setIsProxySet(boolean isProxySet) {
        this.isProxySet = isProxySet;
    }
    public java.lang.String getStubIPAddress() {
        return stubIPaddress;
    }
    
    public void setStubIPAddress(java.lang.String stubIPaddress) {
        this.stubIPaddress = stubIPaddress;
    }    
    public java.lang.String getStorageType() {
        return storageType;
    }
    public void setStorageType(java.lang.String storageType) {
        this.storageType = storageType;
    }

    public java.lang.String getUserName() {
        return userName;
    }
    public void setUserName(java.lang.String userName) {
        this.userName = userName;
    }
    public java.lang.String getPassword() {
        return password;
    }
    public void setPassword(java.lang.String password) {
        this.password = password;
    }
    
}
