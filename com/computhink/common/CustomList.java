/**
 * 
 */
package com.computhink.common;

import java.io.Serializable;

/**
 * @author :Nishad Nambiar
 * Desc    : This bean holds the value for custom list.
 */
	public class CustomList implements Serializable{
		private int id = 0;
		private String name = "";
		private String type = "0";

		public CustomList(int id,String name){
			this.id = id;
			this.name = name;
		}
		
		public CustomList(int id,String name, String type){
			this.id = id;
			this.name = name;
			this.type = type;
		}
		
		/**
		 * @return Returns the id.
		 */
		public int getId() {
			return id;
		}
		/**
		 * @param id The id to set.
		 */
		public void setId(int id) {
			this.id = id;
		}
		/**
		 * @return Returns the name.
		 */
		public String getName() {
			return name;
		}
		/**
		 * @param name The name to set.
		 */
		public void setName(String name) {
			this.name = name;
		}
		public String toString(){
			return this.name;
		}

		/**
		 * @return Returns the type.
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type The type to set.
		 */
		public void setType(String type) {
			this.type = type;
		}
	
	}
