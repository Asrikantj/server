/*
 * Search.java
 *
 * Created on December 30, 2003, 2:06 PM
 */
package com.computhink.common;
/**
 *
 * @author  Administrator
 */
import java.util.StringTokenizer;
import java.util.Vector;
    
public class Search implements java.io.Serializable
{
    private int id = 0;
    private int nodeId = 0;
    private String name = "";
    private String index = "";
    private String contents = "";
    private boolean temp = true;
    private boolean previousVersions = false;
    private boolean searchInComment =  false;
    private String treePath = " ";
    private String creationDate1 = "";
    private String creationDate2 = "";
    private String modifiedDate1 = "";
    private String modifiedDate2 = "";
    private int hitListid = 0;
    private int rowNo = 0;
    private int lastDocId = 0;
    private int searchAuthorCount = 0;
    private int searchDTCount = 0;
    private int searchDTCondCount = 0;
    /*findResultWith is added to do search(index) like in advanced google search. 28 Aug 2007 */
    private int findResultWith = 1;
    //Text search
    private int TextResultWith = 1;
    private int SearchOnlyCond = 1;

    private Vector conds = new Vector();
    private Vector docTypes = new Vector();
    private Vector creators = new Vector();
    
    public static final int ATLEAST_ONE_WORD = 2;
    public static final int ALL_THE_WORDS = 0;
    public static final int EXACT_WORDS = 1;
    /**CV10.2 - Added for Workflow report***/
    private boolean workflowReport = false;


    public Search()
    {
    }
    public Search(int id, String name)
    {
        this.id = id;
        this.name = name;
    }
    public Search(String searchInfo)
    {
    	try{
	        StringTokenizer st = new StringTokenizer(searchInfo, Util.SepChar);
	        this.id = Util.to_Number(st.nextToken());
	        this.name = st.nextToken();
	        this.nodeId = Util.to_Number(st.nextToken());
	        this.index = st.nextToken();
	        this.contents = st.nextToken();
	        this.previousVersions = st.nextToken().equals("1");
	        this.searchInComment = st.nextToken().equals("1");
	        /*  If the ceated date and modified date is having some space some where it is picking 
	        * current date and search result is going wrong because of that. So trim is added here
	        C.Shanmugavalli 			27 Feb 2007*/
	        this.creationDate1 = st.nextToken().trim();
	        this.creationDate2 = st.nextToken().trim();
	        this.modifiedDate1 = st.nextToken().trim();
	        this.modifiedDate2 = st.nextToken().trim();
	        this.treePath = st.nextToken();
	        this.searchAuthorCount = Util.to_Number(st.nextToken());
	        this.searchDTCount = Util.to_Number(st.nextToken());
	        this.searchDTCondCount = Util.to_Number(st.nextToken());
	        st.nextToken(); //Logged in Username
	        this.findResultWith = Util.to_Number(st.nextToken());
	        //Text search
	        this.TextResultWith = Util.to_Number(st.nextToken());	        
	        //this.SearchOnlyCond = Util.to_Number(st.nextToken());
    	}catch(Exception e){
    	}
    }
    public int getId() 
    {
        return this.id;
    }    
    public void setId(int Id) 
    {
        this.id =Id;
    }    
    public int getNodeId() 
    {
        return this.nodeId;
    }
    public void setNodeId(int nodeId) 
    {
        this.nodeId = nodeId;
    }
    public String getName() 
    {
        return this.name;
    }
    public void setName(String name) 
    {
        this.name =name;
    }    
    public String getIndex() 
    {
        return this.index;
    }
    public void setIndex(String index) 
    {
        this.index =index;
    }
    public String getContents() {
        return this.contents;
    }
    
    public void setContents(String contents) {
        this.contents =contents;
    }    
    
    public boolean isTemp() {
        return this.temp;
    }
    
    public void setTemp(boolean temp) {
        this.temp =temp;
    }
    
    public boolean isPreviousVersions() {
        return this.previousVersions;
    }
    
    public void setPreviousVersions(boolean previousVersions) {
        this.previousVersions =previousVersions;
    }
    
    public boolean isSearchInComment() {
        return this.searchInComment;
    }
        
    public void setSearchInComment(boolean searchInComment) {
        this.searchInComment =searchInComment;
    }
        
    public String getTreePath() {
        return this.treePath;
    }
    
    public void setTreePath(String treePath) {
        this.treePath =treePath;
    }
    
    public String getCreationDate1() {
        return this.creationDate1;
    }
    
    public void setCreationDate1(String creationDate1) {
        this.creationDate1 =creationDate1;
    }
    
    public String getCreationDate2() {
        return this.creationDate2;
    }
    
    public void setCreationDate2(String creationDate2) {
        this.creationDate2 =creationDate2;
    }
    
    public String getModifiedDate1() {
        return modifiedDate1;
    }
    
    public void setModifiedDate1(String modifiedDate1) {
        this.modifiedDate1 =modifiedDate1;
    }
    
    public String getModifiedDate2() {
        return this.modifiedDate2;
    }
    
    public void setModifiedDate2(String modifiedDate2) {
        this.modifiedDate2 =modifiedDate2;
    }
    
    public int getHitListId() {
        return this.hitListid;
    }
    
    public void setHitListId(int hitListId) {
        this.hitListid = hitListId;
    }
    
    public int getRowNo() {
        return this.rowNo;
    }
    
    public void setRowNo(int rowNo) {
        this.rowNo =rowNo;
    }
    
    public int getSearchAuthorCount() {
        return this.searchAuthorCount;
    }
    
    public void setSearchAuthorCount(int searchAuthorCount) {
        this.searchAuthorCount =searchAuthorCount;
    }
    
    public int getSearchDTCount() {
        return this.searchDTCount;
    }
    
    public void setSearchDTCount(int searchDTCount) {
        this.searchDTCount =searchDTCount;
    }
    
    public int getSearchDTCondCount() {
        return this.searchDTCondCount;
    }
    
    public void setSearchDTCondCount(int searchDTCondCount) {
        this.searchDTCondCount =searchDTCondCount;
    }
    
    public Vector getConds() {
        return conds;
    }
    
    public void setConds(Vector conds) {
        this.conds.clear();
        this.conds.addAll(conds);
    }
    
    public Vector getDocTypes() {
        return this.docTypes;
    }
    
    public void setDocTypes(Vector docTypes) {
        this.docTypes.clear();
        this.docTypes.addAll(docTypes);
    }
    
    public Vector getCreators() {
        return creators;
    }
    
    public void setCreators(Vector creators) {
        this.creators.clear();
        this.creators.addAll(creators);
    }
    public void set(Search newSearch)
    {
        id = newSearch.id;
        nodeId = newSearch.nodeId;
        name = newSearch.name;
        index = newSearch.index;
        contents = newSearch.contents;
        temp = newSearch.temp;
        previousVersions = newSearch.previousVersions;
        searchInComment =  newSearch.searchInComment;
        treePath = newSearch.treePath;
        creationDate1 = newSearch.creationDate1;
        creationDate2 = newSearch.creationDate2;
        modifiedDate1 = newSearch.modifiedDate1;
        modifiedDate2 = newSearch.modifiedDate2;
        hitListid = newSearch.hitListid;
        rowNo = newSearch.rowNo;
        searchAuthorCount =newSearch.searchAuthorCount ;
        searchDTCount = newSearch.searchDTCount;
        searchDTCondCount = newSearch.searchDTCondCount;
        conds = newSearch.conds;
        docTypes = newSearch.docTypes;
        creators = newSearch.creators;
        findResultWith = newSearch.findResultWith;
        
        //Text search
        TextResultWith = newSearch.TextResultWith;
        //SearchOnlyCond = newSearch.SearchOnlyCond;
    }    
    public int getLastDocId() 
    {
        return lastDocId;
    }
    public void setLastDocId(int lastDocId) 
    {
        this.lastDocId = lastDocId;
    }
    public String toString()
    {
        return this.name;
    }
	public int getFindResultWith() {
		return findResultWith;
	}
	public void setFindResultWith(int findResultWith) {
		this.findResultWith = findResultWith;
	}
	
	//Text search
	public int getTextResultWith() {
		return TextResultWith;
	}
	public void setTextResultWith(int TextResultWith) {
		this.TextResultWith = TextResultWith;
	}
	public int getSearchOnlyCond() {
		return SearchOnlyCond;
	}
	public void setSearchOnlyCond(int SearchOnlyCond) {
		this.SearchOnlyCond = SearchOnlyCond;
	}
	/**CV10.2 - Added for Workflow report***/
	public boolean isWorkflowReport() {
		return workflowReport;
	}
	public void setWorkflowReport(boolean workflowReport) {
		this.workflowReport = workflowReport;
	}
	/**End of CV10.2 merges****************/
}
