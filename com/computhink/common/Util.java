/*
 * DSSMisc.java
 *
 * Created on September 14, 2003, 2:15 PM
 */
package com.computhink.common;
import java.awt.Component;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;

import com.computhink.ars.server.ARS;
import com.computhink.ars.server.ARSServer;
import com.computhink.drs.server.DRS;
import com.computhink.drs.server.DRSConstants;
import com.computhink.drs.server.DRSServer;
import com.computhink.dss.server.DSS;
import com.computhink.dss.server.DSSServer;
import com.computhink.ixr.server.IXR;
import com.computhink.ixr.server.IXRServer;
import com.computhink.mdss.MDSS;
import com.computhink.mdss.MDSSServer;
import com.computhink.vns.server.VNS;
import com.computhink.vns.server.VNSServer;
import com.computhink.vws.csserver.CSS;
import com.computhink.vws.csserver.CSServer;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSServer;
import com.computhink.vws.server.VWSUtil;
public class Util implements Constants
{
    private  static String envUserDir;
    private  static String envVMName;
    private  static String envJavaVer;
    private  static String envJavaVendor;
    private  static String envOSName;
    private  static String envOSArch;
    private  static String envOSVer;
    
    public static final String pathSep = String.valueOf(File.separatorChar);
    public static final String SepChar = "\t";
    
    private static final String AlMoftah = new String("Ykg4#~");
    private static HashMap remoteServers = new HashMap();
    private static final String INTERNAL_KEY =
    //"ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e1055d84
    //d0e7d5c97b935d2237b854af379b0b00964b87401fadbe7d06443c21d";
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d"+
    "ce26dad8d501b5738eb5dfabf810852afbf917a6f5227867d2918a6f40c68d2e06443c21d";
    
    
    //static { Util.readEnvironment(); }
    public static String getFixedValue(String str,String defaultVal) {
        if(str==null || str.trim().equals("") || str.trim().equals("."))
            return defaultVal;
        return str;
    }
    public static String GetFixedString(String str, int len)    
    {
        if(str==null) str = " ";
        while(str.length() < len) str +=" ";
        return str;
    }
    public static String GetFixedString(int i,int len)    
    {
        String str =Integer.toString(i);
        while(str.length() < len) str +=" ";
        return str;
    }
    public static String getOneSpace(String str)    
    {
    	if(str == null || str!=null && str.equals(""))
    		str = " ";
        return str;
    }
    public static String encryptKey(String text) 
    {
        if(text == null || text.length() == 0) return "";
        String crypt = "";
        int depth = 1;
        String key = "321654";
        while(key.length()<text.length()+1) key+=key;
        for(int i=0;i<text.length();i++)
            crypt +=(char)(((text.charAt(i)+key.charAt(i)-key.charAt(i+1))^INTERNAL_KEY.charAt(i))^key.charAt(i));
        return crypt; 
    }
    public static String decryptKey(String text) 
    {
        if(text == null || text.length() == 0) return "";
        String crypt="";
        int depth=1;
        String key="321654";
        while(key.length()<text.length()+1) key+=key;
        for(int i=0;i<text.length();i++)
            crypt +=(char)(((text.charAt(i)^key.charAt(i))^INTERNAL_KEY.charAt(i))+key.charAt(i+1)-key.charAt(i));
        return crypt;  
    }
    /**CV2019 enhancement***/
    public static String encryptKey_MSI(String text) 
    {
        if(text == null || text.length() == 0) return "";
        String crypt = "";
        int depth = 1;
        String key = "abc111222333444555";
        while(key.length()<text.length()+1) key+=key;
        for(int i=0;i<text.length();i++) 
            crypt +=(char)(((text.charAt(i)+key.charAt(i)-key.charAt(i+1))^INTERNAL_KEY.charAt(i))^key.charAt(i));
        return crypt; 
    }
    public static String decryptKey_MSI(String text) 
    {
        if(text == null || text.length() == 0) return "";
        String crypt="";
        int depth=1;
        String key="abc111222333444555";
        while(key.length()<text.length()+1) key+=key;
        for(int i=0;i<text.length();i++)
            crypt +=(char)(((text.charAt(i)^key.charAt(i))^INTERNAL_KEY.charAt(i))+key.charAt(i+1)-key.charAt(i));
        return crypt;  
    }
    public static void RegisterProxy(String addr, int port, String agent)
    {
        System.getProperties().put("http.proxySet", "true");
        System.getProperties().put("http.proxyHost", addr);
        System.getProperties().put("http.proxyPort", String.valueOf(port));
        System.getProperties().put("http.agent", agent);
    }
    public static ServerSchema getMySchema()
    {
    	ServerSchema ss = new ServerSchema();
        try
        {
            ss.address = InetAddress.getLocalHost().getHostAddress();
            ss.name = InetAddress.getLocalHost().getHostName();
        }
        catch(Exception e){}
        return ss;
    }
    public static void sleep(int ms) 
    {
        try 
        {
            Thread.currentThread().sleep(ms);
        }
        catch (InterruptedException ie) 
        {
        }
    }
    public static int createServer(String type)
    {
        ServerSchema s = getMySchema();
        s.comport = getAnyPort();
        s.dataport = getAnyPort();
        s.type = type;
        return createServer(s);
    }
    
    /*
    Purpose:  Following method changed for ARS to create rmiregistry outside java env
    Created by: Shanmugavalli.C
    Date: <20 July 2006>
    */   
    public static int createServer(ServerSchema s)
    {
        String rmiName = "";
        try
        {
            if (s == null || s.type == null) return 0;
            String hostName = System.getProperty("java.rmi.server.hostname");
            if (hostName != null){
                rmiName = "//" + "127.0.0.1:" + s.comport + "/" + 
                                                           getClassName(s.type);
            }else{
                rmiName = "//" + s.address + ":" + s.comport + "/" + 
                                                           getClassName(s.type);
            }
            Remote remote =  getServerObject(s);
            if (remote == null) return 0;
            //if(s.comport != 2000 && s.comport != 3000 && s.comport != 4000 && s.comport != 5000)
            /*
            if(s.comport == 5000)
        	{
        		Runtime runtime = Runtime.getRuntime();
                Process process = runtime.exec("rmiregistry "+s.comport);
                LocateRegistry.getRegistry(s.comport);
        	}
        	else
        	*/
        	{
           		Registry reg = LocateRegistry.createRegistry(s.comport);
        	}
           	/*
           	 * Valli-Need to look in again. It catches connection exception first and rebinds second time for port 5000 -ARS
           	 */
       		try{
       			Naming.rebind(rmiName, remote);
       		}catch(Exception ex){
       			if(s.comport == 5000)
            	{
            		try{Naming.rebind(rmiName, remote);}
            		catch(Exception e){
            			//VWSLog.err("Error in rebind 2 "+e.getMessage());
            		}
            	}
       		}
            remoteServers.put(new Integer(s.comport), remote);
            return s.comport;
        }
        catch (Exception e) 
        {
        	//VWSLog.err("Error in create Server for "+rmiName+" "+e.getMessage());
        	//if DSS port is caught in exception we are rebinding the object
        	if (s.comport == 3000){
        		try{        	
        			Remote remote =  getServerObject(s);
        			Naming.rebind(rmiName, remote);
        		}catch (Exception e11) {
        			// TODO: handle exception
        			e11.printStackTrace();
        			return 0;
        		}
        		return s.comport;
        	}else{
        		return 0;
        	}
        }
    }
    private static int getAnyPort()
    {
        int port = 0;
        try
        {
            ServerSocket ss = new ServerSocket(0);
            port = ss.getLocalPort();
            ss.close();
            ss = null;
        }
        catch(Exception e){}
        return port;
        
    }
    public static Remote getServer(String type, String host, int port)
    {
        ServerSchema s = new ServerSchema();
        s.address = host;
        s.comport = port;
        s.type = type;
        return getServer(s);
    }
    public static Remote getServer(ServerSchema s)
    {
        Remote server = null;
        
        String name = "//" + s.address + ":" + s.comport + "/" + 
                                                           getClassName(s.type);
        try 
        {
            server = Naming.lookup(name);
        }
        catch (Exception e) 
        {
        }
        return server;
    }
    public static boolean killServer(ServerSchema s)
    {
        String name = "//" + s.address + ":" + s.comport + "/" + 
                                                           getClassName(s.type);
        try
        {
            Remote server = (Remote) remoteServers.get(new Integer(s.comport));
            Naming.unbind(name);
            UnicastRemoteObject.unexportObject(server, true);
            remoteServers.remove(new Integer(s.comport));
        }
        catch (Exception e)
        {
            return false;
        }
        return true;
    }
    private static String getClassName(String type)
    {
        if (type == null || type.equals("")) return "";
        if (type.equals(SERVER_VWS)) return VWS.class.getName();
        if (type.equals(SERVER_IXR)) return IXR.class.getName();
        if (type.equals(SERVER_DSS)) return DSS.class.getName();
        if (type.equals(SERVER_MDSS)) return MDSS.class.getName();
        // Added for ARS , 20 July 2006, Valli
        if (type.equals(SERVER_ARS)) return ARS.class.getName();
        // Added for DRS , 20 Dec 2006, Valli
        if (type.equals(SERVER_DRS)) return DRS.class.getName();
        if (type.equals(SERVER_VNS)) return VNS.class.getName();
        if(type.equals(SERVER_CSS))return CSS.class.getName();
        return "";
    }
    private static Remote getServerObject(ServerSchema s)
    {
        try
        {
            if (s.type.equals(SERVER_VWS)) return new VWSServer(s);
            if (s.type.equals(SERVER_IXR)) return new IXRServer(s);
            if (s.type.equals(SERVER_DSS)) return new DSSServer(s);
            if (s.type.equals(SERVER_MDSS)) return new MDSSServer(s);
            //Added for ARS , 20 July 2006, Valli
            if (s.type.equals(SERVER_ARS)) return new ARSServer(s);
            //Added for DRS , 20 Dec 2006, Valli
            if (s.type.equals(SERVER_DRS)) return new DRSServer(s);
            if (s.type.equals(SERVER_VNS)) return new VNSServer(s);
            if(s.type.equals(SERVER_CSS)) return new CSServer(s);
        }
        catch(RemoteException re){}
        return null;
    }
    public static String getNow(int style)
    {
        String now = "";
        switch (style)
        {
            case 1:
                now = new SimpleDateFormat("yyMMddHHmm").format(
                                              Calendar.getInstance().getTime());
                break;
            case 2:
                now = DateFormat.getDateTimeInstance
                            (DateFormat.SHORT, DateFormat.MEDIUM).
                                      format(Calendar.getInstance().getTime());
                break;
            case 3:
                now = new SimpleDateFormat("yyMMdd").format(
                                              Calendar.getInstance().getTime());
                break;                
        }
        return now;
    }
    public static int to_Number(String str)
    {
        try 
        {
            return Integer.parseInt(str);
        }
        catch(Exception e){return 0;}
    }
    public static int getCharCount(String str,String chr)
    {
        int charCount=0;
        int count=str.length();
        for(int i=0;i<count;i++)
        {
            if(str.substring(i,i+1).equalsIgnoreCase(chr)) charCount++;
        }
        return charCount;
    }
    public static URL fileToURL(File file) 
    {
      String path = file.getAbsolutePath();
      String fSep = System.getProperty("file.separator");
      if (fSep != null && fSep.length() == 1)
         path = path.replace(fSep.charAt(0), '/');
      if (path.length() > 0 && path.charAt(0) != '/')
         path = '/' + path;
      try  
      {
         return new URL("file", null, path);
      }
      catch (java.net.MalformedURLException e) 
      {
         throw new Error("unexpected MalformedURLException");
      }
    }
    public static boolean Ask(java.awt.Container comp, String q, String title) 
    {
        JOptionPane optionPane = new JOptionPane();
        int answer = optionPane.showConfirmDialog(comp, q, title,
                        JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
        return ( answer == JOptionPane.YES_OPTION ? true : false);
    }
    public static boolean Ask(java.awt.Window comp, String q, String title) 
    {
        JOptionPane optionPane = new JOptionPane();
        int answer = optionPane.showConfirmDialog(comp, q, title,
                        JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
        return ( answer == JOptionPane.YES_OPTION ? true : false);
    }
    public static boolean Ask(java.awt.Window comp, JScrollPane msgPanel, String title) 
    {
        JOptionPane optionPane = new JOptionPane();
        int answer = optionPane.showConfirmDialog(comp, msgPanel, title,
                        JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
        return ( answer == JOptionPane.YES_OPTION ? true : false);
    }
    public static void Msg(java.awt.Container comp, String msg, String title) 
    {
        JOptionPane optionPane = new JOptionPane();
        optionPane.showMessageDialog(comp, msg, title,
                                               JOptionPane.INFORMATION_MESSAGE);
    }
    
    public static boolean Ask(Component parent,String msg, String title)
    {
    	int result = JOptionPane.showConfirmDialog(parent,msg,title,
    			JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
    	return ( result == JOptionPane.YES_OPTION ? true : false);
    }
    public static void Msg(java.awt.Window comp, String msg, String title) 
    {
        JOptionPane optionPane = new JOptionPane();
        optionPane.showMessageDialog(comp, msg, title,
                                               JOptionPane.INFORMATION_MESSAGE);
    }
    public static void Msg(Component parent, String msg, String title) 
    {
        JOptionPane optionPane = new JOptionPane();
        optionPane.showMessageDialog(parent, msg, title,
                                               JOptionPane.INFORMATION_MESSAGE);
    }
    public static int Option(java.awt.Window comp, String q, String title) 
    {
        JOptionPane optionPane = new JOptionPane();
        int answer = optionPane.showConfirmDialog(comp, q, title,
                 JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);
        return answer;
    }
    public static String decriptStr(String str)
    {
        if(str == null || str.equals("")) return "";
        Rc4Cipher cipher = new Rc4Cipher(AlMoftah);
        byte[] decryptedText = cipher.decrypt(str.getBytes());
        return new String(decryptedText);
    }
    public static String encriptStr(String str)
    {
        if(str==null || str.equals("")) return "";
        Rc4Cipher cipher = new Rc4Cipher(AlMoftah);
        byte[] cipherText = cipher.encrypt(str.getBytes());
        return new String(cipherText);
    }
    public static String readConFile(String path) 
    {
        String str="";
        try 
        {
            File filePath=new File(path);
            if(!filePath.exists() || !filePath.canRead()) return str;
            RandomAccessFile raf = new RandomAccessFile(filePath,"r");
            str = raf.readLine();
            str=decryptKey(str);
            raf.close();
        }
        catch(IOException e) {
            return "";
        }
        return str;
    }
    public static String readRegistrationFile(String path) 
    {
        String str="";
        String returnStr="";
        try 
        {
            File filePath=new File(path);
            if(!filePath.exists() || !filePath.canRead()) return str;
            RandomAccessFile raf = new RandomAccessFile(filePath,"r");
            str = raf.readLine();
            while(str!= null ) {
            	returnStr += str;
            	str = raf.readLine();
            	if (str != null)returnStr+="\r";
            }
            returnStr = Util.decryptKey(returnStr);
            raf.close();
        }
        catch(IOException e) {
            return "";
        }
        return returnStr;
    }    
    private static void readEnvironment()
    {
        envUserDir = System.getProperty("user.home");
        envVMName = System.getProperty("java.vm.name");
        envJavaVer = System.getProperty("java.version");
        envJavaVendor = System.getProperty("java.vendor");
        envOSName = System.getProperty("os.name");
        envOSArch = System.getProperty("os.arch");
        envOSVer = System.getProperty("os.version");
    }
    public static String fixURLPath(String urlPath) 
    {
        String sFixPath="";
        if(urlPath==null || urlPath.equals("")) return sFixPath;
        int count=urlPath.length();
        for(int i=0;i<count;i++) {
            if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equals("0")) {
                sFixPath+=" ";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equalsIgnoreCase("B")) {
                sFixPath+="+";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equalsIgnoreCase("F")) {
                sFixPath+="/";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("3") &&
            urlPath.substring(i+2,i+3).equalsIgnoreCase("F")) {
                sFixPath+="?";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equals("5")) {
                sFixPath+="%";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equals("3")) {
                sFixPath+="#";
                i=i+2;
            }
            else if(urlPath.substring(i,i+1).equals("%") &&
            urlPath.substring(i+1,i+2).equals("2") &&
            urlPath.substring(i+2,i+3).equals("6")) {
                sFixPath+="&";
                i=i+2;
            }
            else {
                sFixPath+=urlPath.substring(i,i+1);
            }
        }
        return sFixPath;
    }
	public static void CreateEmptyAllZip(String zipPath, String fileName){
	    byte[] buf = new byte[1024];
	    try {
	        // Create the ZIP file
	        String outFilename = zipPath + pathSep + fileName;
	        String infoFileName = zipPath + pathSep + "folder_database.info";
	        File infoFile = new File(infoFileName);
	        if (!infoFile.exists()) infoFile.createNewFile();	        
	        
	        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(outFilename));
	        // Compress the files
	        
	            FileInputStream in = new FileInputStream(infoFileName);
	            // Add ZIP entry to output stream.
	            out.putNextEntry(new ZipEntry(infoFile.getName()));
	            // Transfer bytes from the file to the ZIP file
	            int len;
	            while ((len = in.read(buf)) > 0) {
	                out.write(buf, 0, len);
	            }
	            // Complete the entry
	         out.closeEntry();
	         in.close();	        
	        // Complete the ZIP file
	        out.close();	        
	       infoFile.delete();
	    } catch (IOException e) {}
	    catch (Exception ex) {}
	}    
    public static boolean CopyFile(File fromFile,File toFile)
    {   
        BufferedInputStream in = null;
        BufferedOutputStream out = null;
        try
        {
            in = new BufferedInputStream(new FileInputStream(fromFile));
            out = new BufferedOutputStream(new FileOutputStream(toFile));
            int bufferSize = Math.min((int)fromFile.length(), 1024000);
            byte[] buffer = new byte[bufferSize];
            int numRead;
            while ((numRead = in.read(buffer, 0, buffer.length)) != -1)
            {
                out.write(buffer, 0, numRead);
            }
            out.flush();
            in.close();
        }
        catch (IOException e)
        {
            return false;
        }
        finally
        {
            try
            {
                if (in != null) in.close();
                if (out != null) out.close();
            }
            catch (IOException e)
            {
            }
        }
        return true;
    }
    public static String getEnvUserDir() 
    {
        return System.getProperty("user.home");
    }
    public static String getEnvVMName() 
    {
        return System.getProperty("java.vm.name");
    }
    public static String getEnvJavaVer() 
    {
        return System.getProperty("java.version");
    }
    public static String getEnvJavaVendor() 
    {
        return System.getProperty("java.vendor");
    }
    public static String getEnvOSName() 
    {
        return System.getProperty("os.name");
    }
    public static String getEnvOSArch() 
    {
        return System.getProperty("os.arch");
    }
    public static String getEnvOSVer() 
    {
        return System.getProperty("os.version");
    }
    /*Issue 562
     * 
     * Nebu
     * 
     */ 
    public static void maintainLogFiles(String path,String filePrefix,int maxCount){
    	
    	String dir = path;
    	File logDir = new File(dir);
        String[] filenames = logDir.list();       
        int count=0;
        HashMap hData = new HashMap();
        
        for(int i=0;i<filenames.length;i++){
        	
	        	if(filenames[i].startsWith(filePrefix)){
	        		
		        	File f = new File(dir+filenames[i]);
		            count++;
				         try{				         	
				         	hData.put(filenames[i],String.valueOf(f.lastModified()));				         	
				         	}
				         catch(Exception e){
				         	
				         	VWSLog.err("Error - Maintain Log Files");
				         	
				            }
	           } 
        }
        //     int maxCount = VWSPreferences.getLogFileCount();
        if(!hData.isEmpty()){
        	ArrayList listManager = new ArrayList(hData.entrySet());
        	Collections.sort(listManager,new Comparitor());
        	if (maxCount!=0){     	
        		
        		if (maxCount<listManager.size()){
        			
        			int i=listManager.size()-1;
        			
        			while(count>maxCount){
        				
        				String fileName = (String)((Map.Entry)listManager.get(i)).getKey();
        				File managerFile = new File(dir+fileName);
        				managerFile.delete();
        				i--;
        				count--;
        				
        			}
        		} 
        	}
        }
    }
    // End of fix 562
    
	 /*	Enhancement No:66-To read build time of server ViewWise.jar from manifext file
  	 *	Created by: <Shanmugavalli.C>	Date: <04 Aug 2006>
  	 *	@param: jar file name with path	@return Built-DateTime:
  	 */
    public static String getJarManifestInfo(String jarName){
    	
    	String buildTime = new String();
      	//VWSLog.err("jarName "+jarName);
    	try {
			File file = new File(jarName);
			if(file.exists()){
				JarFile jar = new JarFile(file);
				//FileInputStream fin = null;
			    //fin = new FileInputStream(new File(jarName));
				Enumeration enumeration = jar.entries();
	            while(enumeration.hasMoreElements()) {
	                JarEntry entry = (JarEntry) enumeration.nextElement();
	                InputStream in = jar.getInputStream( entry );
	                if(entry.getName().equals("META-INF/MANIFEST.MF")) {
	                	BufferedReader rdr = new BufferedReader(new InputStreamReader(in));
	                	StringBuffer buffer = new StringBuffer();
	                	String line = "";
	                	// Built-DateTime: added in Manifest file.
	                	while( (line = rdr.readLine()) != null) {
	                		if(line.startsWith("Built-DateTime:")){
	                			buffer.append(line);
	                			buffer.append(System.getProperty("line.separator"));
	                			break;
	                		}
	                	}
	                	buildTime = buffer.toString();
	                	//VWSLog.err("mf details: "+buildTime);
	                }
	            }
			}
		}catch(Exception e) {
			//VWSLog.err("error "+e.getMessage());
			return null;
		}
		return buildTime;
    }
    /**
     * CV10.1
     * Added for checking the build version for jar mismatch
     * @return
     */
    public static String getBuildVersion(String jarName){

    	String buildTime = new String();
    	//VWSLog.err("jarName "+jarName);
    	try {
    		File file = new File(jarName);
    		if(file.exists()){
    			JarFile jar = new JarFile(file);
    			Enumeration enumeration = jar.entries();
    			while(enumeration.hasMoreElements()) {
    				JarEntry entry = (JarEntry) enumeration.nextElement();
    				InputStream in = jar.getInputStream( entry );
    				if(entry.getName().equals("META-INF/MANIFEST.MF")) {
    					BufferedReader rdr = new BufferedReader(new InputStreamReader(in));
    					StringBuffer buffer = new StringBuffer();
    					String line = "";
    					// Built-DateTime: added in Manifest file.
    					while( (line = rdr.readLine()) != null) {
    						if(line.startsWith("Build-Version:")){
    							buffer.append(line);
    							buffer.append(System.getProperty("line.separator"));
    							break;
    						}
    					}
    					buildTime = buffer.toString();
    				}
    			}
    		}
    	}catch(Exception e) {
    		//VWSLog.err("error "+e.getMessage());
    		return null;
    	}
    	return buildTime;
    }

        
    
    
    
    
    
    
 /*	Enhancement : By reading build time of server ViewWise.jar from manifext file, build version is generated
  	 *	Created by: <Shanmugavalli.C>	Date: <22 Feb 2007>
  	 *
  	 *	@param: 	@return: Build versions
  	 *
  	 *	This method is not used and we replace product version from the properties file using Resource bundle -- PR 
  	 */
    public static String getBuildVersion(){
    	String svrJarLocation = VWSUtil.getHome()+"\\lib\\ViewWise.jar";
		String svrJarBuildTime = getJarManifestInfo(svrJarLocation);
		String buildVersion = "";
   	 	if(svrJarBuildTime!=null && !svrJarBuildTime.equals(""))
   	 		buildVersion = "6.0.0."+svrJarBuildTime.substring(19,21)+svrJarBuildTime.substring(16,18);
   	 	else
   	 		buildVersion = "6.0.0.1201";
  		 return buildVersion; 
    }
    
    public static String getFileWithCorrectedFileSep(String dstFile){
		String newdstFile = "";
    	if(dstFile.indexOf("\\")!=-1  && dstFile.indexOf("/")!=-1 ){
    		try{
    			newdstFile = dstFile;
    			/* temp.replaceAll("\\", "/"); This replaceAll will not work and throws PatternSyntaxException  
    			* To resolve this problem is it must be temp.replaceAll("\\\\","/"); 
    			*/
    			if((File.separatorChar == '/')&& newdstFile.indexOf("\\")!=-1){
    				newdstFile = newdstFile.replaceAll("\\\\", "/");
    			}
    			if((File.separatorChar == '\\')&& newdstFile.indexOf("/")!=-1){
    				newdstFile = newdstFile.replaceAll("/", "\\\\");
    			}
    			dstFile = newdstFile;
    		}catch(Exception ex){
    			System.out.println("Error in getFileWithCorrectedFileSep - "+ex.toString());
    		}
    	}
    	return dstFile;
    }

    public static String replaceSplCharFromTempTable(String tempTable){    
	tempTable = tempTable.replaceAll("-", ".");
	return tempTable;
    }
    public static String convertDateFormat(String inputDate, String actualFormat, String reqFormat){
    	try{
    		String convertDate = "";
    		actualFormat = actualFormat.replaceAll("dddd", "EEEE");
    		actualFormat = actualFormat.replaceAll("ddd", "EEE");
    		reqFormat = reqFormat.replaceAll("dddd", "EEEE");
    		reqFormat = reqFormat.replaceAll("ddd", "EEE");

    		SimpleDateFormat formatter = new SimpleDateFormat(actualFormat);
    		Date dateObj = formatter.parse(inputDate); 
    		formatter = new SimpleDateFormat(reqFormat);
    		convertDate = formatter.format(dateObj);
    		return convertDate;
    	}catch(Exception ex){
    		return inputDate;
    	}
    }
    /*    public static String checkPath(String file){
	return checkPath(new File(file));
    }
     */    public static String checkPath(String file){
    	 String dstFile = file;// file.getAbsolutePath();
    	 if(dstFile.indexOf("\\")!=-1  && dstFile.indexOf("/")!=-1 ){
    		 String newdstFile = "";
    		 try{
    			 newdstFile = dstFile;
    			 /* temp.replaceAll("\\", "/"); This replaceAll will not work and throws PatternSyntaxException  
    			  * To resolve this problem is it must be temp.replaceAll("\\\\","/"); 
    			  */
    			 if((File.separatorChar == '/')&& newdstFile.indexOf("\\")!=-1){
    				 newdstFile = newdstFile.replaceAll("\\\\", "/");
    			 }
    			 if((File.separatorChar == '\\')&& newdstFile.indexOf("/")!=-1){
    				 newdstFile = newdstFile.replaceAll("/", "\\\\");
    			 }
    			 dstFile = newdstFile;
    		 }catch(Exception ex){
    			 //DSSLog.err("Error in writeContents - "+ex.toString());
    		 }
    	 }	
    	 return dstFile;
     }
     public static Vector<VWEmailOptionsInfo> getEmailOptionsInfoVector(String vwEmailOptionsInfoStr) {
 		Vector<VWEmailOptionsInfo> vwEmailOptionsList = new Vector<VWEmailOptionsInfo>();
 		try{
 			String tempEmailOptionsInfoRow[] = vwEmailOptionsInfoStr.split("\\|");
 			for(int i=0; i<tempEmailOptionsInfoRow.length; i++){
 				VWEmailOptionsInfo vwEmailOptionsInfo = new VWEmailOptionsInfo(tempEmailOptionsInfoRow[i]);
 				vwEmailOptionsList.add(vwEmailOptionsInfo);
 			}
 		}catch(Exception ex){
 			//Document.printToConsole("Exception in getEmailOptionsInfoVector, Util.java and ex is : "+ex.getMessage());
 		}
 		return vwEmailOptionsList;
 	}
    public static Vector<String> getHtmlMetaDataContents(Vector documentIndices, Vector documentInfo){
    	Vector<String> htmlMetaDataContents = new Vector<String>();
    	//Vector<Index> docMetaData = new Vector<Index>();
    	String docName = "";
    	String docTypeName = "";
    	String creator = "";
    	String createdDate = "";
    	int pageCount = 0;
    	
    	if(documentIndices!=null && documentIndices.size()>0 && documentInfo!=null && documentInfo.size()>0){
    		String dbString = documentInfo.get(0).toString();
    		StringTokenizer st = new StringTokenizer(dbString, "\t");
    		st.nextToken();//Document id
    		docName = st.nextToken();
    		st.nextToken();//Document type id
    		docTypeName = st.nextToken();
    		creator = st.nextToken();
    		createdDate = st.nextToken();
    		st.nextToken();//ModifiedDate
    		st.nextToken();//sessionid
    		pageCount = to_Number(st.nextToken());
    		//comment count
    		//reference count

    		Vector<String> htmlContent =new Vector<String>();
    		htmlContent.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
    		htmlContent.add("<html>");
    		htmlContent.add("<head>");
    		htmlContent.add("<title>"+ DRSConstants.LBL_DOC_META_DATA +" ["+docName+"]</title>");
    		htmlContent.add("<style>");
    		htmlContent.add("TH {");
    		htmlContent.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699'; font-weight:bold");
    		htmlContent.add("}");
    		htmlContent.add("TD {");
    		htmlContent.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
    		htmlContent.add("}");
    		htmlContent.add("</style>");
    		htmlContent.add("</head>");	        
    		htmlContent.add("<body BGCOLOR='#F7F7E7'>");
    		htmlContent.add("<p align='center'><span lang='en-us'><font size='2'></font></span></p>");
    		htmlContent.add("<p align='center'><b><span lang='en-us'><font size='4'>"+DRSConstants.LBL_DOC_META_DATA+" ["+docName+"]</font></span></b></p>");

    		
	    	htmlContent.add("<Table>");
	    	htmlContent.add("<tr><td><b>Document Type </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "+docTypeName+"	</td></tr>");
	    	htmlContent.add("<tr><td><b>Creator </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "+creator+" </td></tr>");
	    	htmlContent.add("<tr><td><b>Creation Date </b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: "+createdDate+" </td></tr>");
	    	htmlContent.add("<tr><td><b>Number of page(s) </b>&nbsp;: "+pageCount+"	</td></tr>");
	    	htmlContent.add("</Table>");
    		

    		htmlContent.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
    		htmlContent.add("<tr>");
    		for(int i=0;i<DRSConstants.MetaDataColumnNames.length;i++)
    			htmlContent.add("<td width='" + DRSConstants.MetaDataColumnWidths[i] + "%' align='center' BGCOLOR='#cccc99'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>" + DRSConstants.MetaDataColumnNames[i] + "</B></FONT></td>");
    		htmlContent.add("</tr>");
    		int serialNumber = 1;
    		String tempValue = "";
    		if(documentIndices!=null && documentIndices.size()>0){
    			for(int count=0;count<documentIndices.size();count++)
    			{
    				Index idx = (Index)documentIndices.get(count);
    				if(idx.getValue()==null || idx.getValue().trim().equals(""))
						tempValue = "&nbsp;";
					else
						tempValue = idx.getValue();
    				
    				htmlContent.add("<tr>");
    				htmlContent.add("<td width='" + DRSConstants.MetaDataColumnWidths[0] + "%'>" +
    						serialNumber + "</td>");//SNo
    				htmlContent.add("<td width='" + DRSConstants.MetaDataColumnWidths[1] + "%'>" +
    						idx.getName() + "</td>");//Index Name
    				htmlContent.add("<td width='" + DRSConstants.MetaDataColumnWidths[2] + "%'>" +
    						tempValue + "</td>");//Index Value
    				htmlContent.add("</tr>");
    				serialNumber++;
    			}
    		}
    		htmlContent.add("</table>");
    		htmlContent.add("<p align='left'><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></p>");
    		htmlContent.add("</body>");
    		htmlContent.add("</html>");

    		htmlMetaDataContents.addAll(htmlContent);
    	}
    	return htmlMetaDataContents;
    }
    /**
     * CV10.1 Enhancment:- To fetch the template from contentverse folder.
     * @param templateLocation
     * @return
     */
	public static File[] getPdfTemplates(String templateLocation) {
		File folder = new File(templateLocation);
		File[] listOfFiles = folder.listFiles();

		    for (int i = 0; i < listOfFiles.length; i++) {
		      if (listOfFiles[i].isFile()) {
		        System.out.println("File " + listOfFiles[i].getName());
		      } else if (listOfFiles[i].isDirectory()) {
		        System.out.println("Directory " + listOfFiles[i].getName());
		      }
		    }
		    return listOfFiles;
	}
	/**
	 * CV10.1 ValidateDateMonthYear to check the invalid date format and throw error from create document for aip.
	 * @param mask
	 * @param date
	 * @return
	 */
	
	  public static boolean validateDateMonthYear(String mask, String date) {

			if (mask.startsWith("dddd,"))
				mask = mask.replace("dddd", "EEEE");

			String maskSeperator = "/";
			String dateSeperator = "/";

			if (mask.contains(","))
				maskSeperator = ",";
			else if (mask.contains("-"))
				maskSeperator = "-";
			else if (mask.contains("/"))
				maskSeperator = "/";

			if (date.contains(","))
				dateSeperator = ",";
			else if (date.contains("-"))
				dateSeperator = "-";
			else if (date.contains("/"))
				dateSeperator = "/";

			if (!maskSeperator.equals(dateSeperator)) {
				return false;
			}
			String[] maskElements = mask.split(maskSeperator);
			String[] dateElements = date.split(dateSeperator);
			for (int i = 0; i < maskElements.length; i++) {
			try {
				String key1 = maskElements[i].trim();
				String key2 = dateElements[i].trim();
				if (key1.contains("d") &&  key1.trim().length() == 7) {
					String[] inMaskElement = key1.split(" ");
					String[] inDatekElement = key2.split(" ");
					for(int n=0; n<inMaskElement.length; n++){
						String inKey1 = inMaskElement[n];
						String inKey2 = inDatekElement[n];
						if(inKey1.contains("d")){
							if(inKey2.length() > 2 )
								return false;
							if(Integer.parseInt(inKey2) == 0) 
								return false;
						}
					}
				}
				if (key1.trim().contains("d") && key1.trim().length() <= 2) {
					if(key2.length() > 2 )
						return false;
					if(Integer.parseInt(key2) == 0)
						return false;
				}
				if(key1.contains("M") && key1.length() <= 2){
					if(!(key2.length() <= 2)) 
						return false;
					else if(Integer.valueOf(key2) < 1) 
						return false;
				}
				if(key1.contains("M") && key1.length() == 3){
					if(key1.length() != key2.length())
						return false;
				}
				if(key1.equals("EEEE")){
					key2 = key2.toUpperCase();
					Pattern p = Pattern.compile(String.format("\\b(%s)\\w*\\b", "SUNDAY|MONDAY|TUESDAY|WEDNESDAY|THURSDAY|FRIDAY|SATURDAY"));
					if(!(p.matcher(key2).find()))
						return false;
				}
				if (key1.contains("y")) {
					if (key1.length() != key2.length())
						return false;
					System.out.println("key2............."+key2);
					if(Integer.parseInt(key2) == 0) 
						return false;
				}
			} catch (Exception e) {
				VWSLog.dbg("Exception 1 datemask validation ::::"+e.getMessage());
				return false;
			}
			}
			SimpleDateFormat format = new SimpleDateFormat(mask);
			try {
				Date parse = format.parse(date);
				parse.getTime();
			} catch (ParseException e) {
				VWSLog.dbg("Exception 2 datemask validation ::::"+e.getMessage());
				return false;
			}
			return true;
		}
	  
	  public static File[] listAttachmentFiles(String templateLocation) {
			File folder = new File(templateLocation);
			File[] listOfFiles = folder.listFiles();

			    for (int i = 0; i < listOfFiles.length; i++) {
			      if (listOfFiles[i].isFile()) {
			        System.out.println("File " + listOfFiles[i].getName());
			      } else if (listOfFiles[i].isDirectory()) {
			        System.out.println("Directory " + listOfFiles[i].getName());
			      }
			    }
			    return listOfFiles;
		}
	  	/**
	  	 * CV2019 enhancement
	     * Size of the buffer to read/write data
	     */
	    private static final int BUFFER_SIZE = 4096;
	    /**
	     * Extracts a zip file specified by the zipFilePath to a directory specified by
	     * destDirectory (will be created if does not exists)
	     * @param zipFilePath
	     * @param destDirectory
	     * @throws IOException
	     */
	    public static void unzip(String zipFilePath, String destDirectory) throws IOException {
	        File destDir = new File(destDirectory);
	        if (!destDir.exists()) {
	            destDir.mkdir();
	        }
	        ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath));
	        ZipEntry entry = zipIn.getNextEntry();
	        // iterates over entries in the zip file
	        while (entry != null) {
	            String filePath = destDirectory + File.separator + entry.getName();
	            if (!entry.isDirectory()) {
	                // if the entry is a file, extracts it
	                extractFile(zipIn, filePath);
	            } else {
	                // if the entry is a directory, make the directory
	                File dir = new File(filePath);
	                dir.mkdir();
	            }
	            zipIn.closeEntry();
	            entry = zipIn.getNextEntry();
	        }
	        zipIn.close();
	    }
	    /**
	     * Extracts a zip entry (file entry)
	     * @param zipIn
	     * @param filePath
	     * @throws IOException
	     */
	    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
	        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
	        byte[] bytesIn = new byte[BUFFER_SIZE];
	        int read = 0;
	        while ((read = zipIn.read(bytesIn)) != -1) {
	            bos.write(bytesIn, 0, read);
	        }
	        bos.close();
	    }
	    
		public static void deleteDirectory(File file) {
			if (file.exists()) {
				if (file.isDirectory()) {
					File[] files = file.listFiles();
					if (files != null && files.length > 0) {
						for (int i = 0; i < files.length; i++) {
							if (files[i].isDirectory()) {
								deleteDirectory(files[i]);
							} else {
								files[i].delete();
							}
						}
					}
				}
				file.delete();
			}
		}
}
