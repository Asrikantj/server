/*
 * Group.java
 *
 * Created on January 4, 2004, 6:50 PM
 */

package com.computhink.common;

/**
 *
 * @author  Administrator
 */
public class Group 
{
    private int id = 0;
    protected String name = "";

    public Group()
    {
    }
    public Group(int id)
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public void setName(String name)
    {
        this.name = (name == null ? "" : name);
    }
    public int getId()
    {
        return id;
    }
    public String toString()
    {
        return this.name;
    }
}

