package com.computhink.common;

import java.io.Serializable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.computhink.vwc.VWClient;
import com.computhink.vwc.notification.VWNotificationConstants;

public class RouteReportSettings implements Serializable, VWNotificationConstants{

	private static final long serialVersionUID = 1L;
	private int id;
	private int referenceId; // DocumentId or FolderId or DocumentTypeId, RetentionId or RouteId
	private int nodeType;// For Folder = 0, Document = 1, DocumentType = 2, StorageMangement = 3, AuditTrail = 4, RecycleDocs=5, Restore=6, RedAction=7, Retention=8, Route=9
	private String nodeName;
	//private String typeDesc;
	private Notification[] notifications;
	private String userName;
	private String ipAddress;
	private String attachment;	//Include Reference,History (00 = Both unchecked, 11 = Both checked, 01 = Only History checked, 10 = Only Reference checked)
	private String emails;
	private String created;
	private String modified;
	private int folderInherit;	//set only for Folder
	private int documentInherit;	//set only for Folder
	private int notifyCaptureType; //Notify type 0 - one time 1 - Continuous
	private int ProcessType;	//Bulk / Individual
	private String lastNotifyTime;
	
	public String getAttachment() {
		return attachment;
	}
	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}
	public String getCreated() {
		return created;
	}
	public void setCreated(String created) {
		this.created = created;
	}
	public String getEmails() {
		return emails;
	}
	public void setEmails(String emails) {
		this.emails = emails;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getIpAddress() {
		return ipAddress;
	}
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	public String getModified() {
		return modified;
	}
	public void setModified(String modified) {
		this.modified = modified;
	}
	public int getReferenceId() {
		return referenceId;
	}
	public void setReferenceId(int referenceId) {
		this.referenceId = referenceId;
	}
	public Notification[] getNotifications() {
		return notifications;
	}
	public void setNotifications(Notification[] notifications) {
		this.notifications = notifications;
	}
	public int getNodeType() {
		return nodeType;
	}
	public void setNodeType(int nodeType) {
		this.nodeType = nodeType;
	}
	/*public String getTypeDesc() {
		return typeDesc;
	}
	public void setTypeDesc(String typeDesc) {
		this.typeDesc = typeDesc;
	}*/
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public int getNotifyCaptureType() {
		return notifyCaptureType;
	}
	public void setNotifyCaptureType(int notifyCaptureType) {
		this.notifyCaptureType = notifyCaptureType;
	}
	public int getProcessType() {
		return ProcessType;
	}
	public void setProcessType(int processType) {
		ProcessType = processType;
	}
	
	public String getAllNotificationEvents(){
		String notificationEvents = "";
		for (int i = 0; i < notifications.length; i++){
			notificationEvents += notifications[i].getNotifyDesc() + ";";
		}
		return notificationEvents;
	}
	
	public void setNotifications(int nodeType, String notificationString) {

		Vector<Notification> temp = new Vector<Notification>();
		Notification[] notifications = null;
		try{
			StringTokenizer st = new StringTokenizer(notificationString, ";");
			while(st.hasMoreTokens()){
				String desc = st.nextToken();

				Notification notification = new Notification();
				notification.setId(getNotifyId(nodeType, desc));
				notification.setNotifyDesc(desc);
				notification.setModule(NotificationModules[nodeType]);

				temp.add(notification);				
			}
			
			if(temp != null && temp.size() > 0){
				notifications = new Notification[temp.size()];
				for(int i=0; i<temp.size(); i++){
					notifications[i] = temp.get(i);
				}
			}

			this.notifications = notifications;			
		}catch(Exception e){
			VWClient.printToConsole("Exception while setting notifications : "+e.getMessage());
		}
	}
	
	private int getNotifyId(int nodeType, String desc) {
		int notifyId = 0;
		try{
			Notification[] notificationList = getNotificationList(nodeType);	
			if(notificationList != null && notificationList.length > 0){
				for(int i=0; i<notificationList.length; i++){
					if(notificationList[i].getNotifyDesc().equalsIgnoreCase(desc)){
						notifyId = notificationList[i].getId();
						break;
					}
				}
			}
		}catch(Exception e){
			VWClient.printToConsole("Exception while getting notify id : "+e.getMessage());
		}
		return notifyId;
	}
	
	private Notification[] getNotificationList(int nodeType) {
		Notification[] notifications = null;
		/*try{
			switch(nodeType){
			case FOLDER: notifications = FOLDER_NOTIFICATION_LIST; break; 
			case DOCUMENT: notifications =  DOCUMENT_NOTIFICATION_LIST; break;
			case DOCUMENT_TYPE: {
				//notifications =  DOCUMENT_TYPE_NOTIFICATION_LIST; 
				break;
			}
			case STORAGE_MANAGEMENT: notifications =  STORAGE_MANAGEMENT_NOTIFICATION_LIST; break;
			case AUDIT_TRAIL: notifications =  AUDIT_TRAIL_NOTIFICATION_LIST;  break;
			case RECYCLE_DOCUMENTS: notifications =  RECYCLE_NOTIFICATION_LIST;  break;
			case RESTORE_DOCUMENTS: notifications =  RESTORE_NOTIFICATION_LIST;  break;
			case REDACTIONS: notifications =  REDACTION_NOTIFICATION_LIST;  break;
			case RETENTION: notifications =  RETENTION_NOTIFICATION_LIST;  break;
			case ROUTE: notifications =  ROUTE_NOTIFICATION_LIST;  break;
			}
		}catch(Exception e){
			VWClient.printToConsole("Exception while getting Notification List : "+e.getMessage());
		}*/
		return notifications;		
	}

	public String getNotificationModule(){
		String module = "";
/*		if(notifications != null && notifications.length > 0){
			module = notifications[0].getModule();
		}
*/
		module = NotificationModules[nodeType];
		return module;
	}
	public String getNodeName() {
		return nodeName;
	}
	public void setNodeName(String nodeName) {
		this.nodeName = nodeName;
	}
	public String getLastNotifyTime() {
		return lastNotifyTime;
	}
	public void setLastNotifyTime(String lastNotifyTime) {
		this.lastNotifyTime = lastNotifyTime;
	}
	public int getDocumentInherit() {
		return documentInherit;
	}
	public void setDocumentInherit(int documentInherit) {
		this.documentInherit = documentInherit;
	}
	public int getFolderInherit() {
		return folderInherit;
	}
	public void setFolderInherit(int folderInherit) {
		this.folderInherit = folderInherit;
	}
}
