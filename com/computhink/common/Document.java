package com.computhink.common;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Vector;

public class Document implements Serializable 
{
    private int id;
    private int sessionId;
    /**
     * Added to Check the type of the lock type,if lockType is 1 chekout related functionalities,
     * If lockType is 4 create ownership related Functionalities,Gurumurthy.T.S 01-03-2014
     */
    private int lockType;
    private int roomId;
    private String roomName;
    private int parentId;
    private String VRComment = "";
    private String version = "";
    private String versionDocFile = "";
    private DocType dt;
    private VWDoc vwdoc = new VWDoc();
    private int pageCount=-1;
    private String name;
    private String modifyDate;
    private String creationDate;
    private String creator;
    private String tag;
    private int commentCount=0;
    private int refCount=0;
    private String location="";
    private int signId=0;
    private int signType=-1;
    // Add this properties to identified the document is freezed or not
    private int freezed = 0;
    private Vector customIndexValues = new Vector();
    //Purpose: nodeType added for newSearch which will list folder/cabinet/drawer/document
    private String nodeType = "";
    private String colorCode = "";
    private int commentInclude = 0;
    private int status = 0;
    private int resultType = 0;//0 for index contains; 1 for text contains
    private int routeUserSignType = 0;
    private int notificationExist = 0;
    private String desc = "";
    private String deletedUser = "";
    private String deletedHost = "";
    private String deletedTime = "";
    private String deletedLocation ="";
    private int docPendingStatus = 0;
    public int getDocPendingStatus() {
		return docPendingStatus;
	}
	public void setDocPendingStatus(int docPendingStatus) {
		this.docPendingStatus = docPendingStatus;
	}
	private int scope;
    	
    public Document() 
    {
    }
    public Document(int id) 
    {
        this.id = id;
    }
    public void setParentId(int id) 
    {
        this.parentId = id;
    }
    public int getParentId() 
    {
        return this.parentId;
    }
    public void setId(int id) 
    {
        this.id = id;
    }
    public int getId() 
    {
        return this.id;
    }
    public String getName() {
        return this.name;
    }
    public Document(String dbstring, boolean ver) 
    {
    	try{
    		//2914	1	2	Doctors	vwadmin	2007-10-31 10:06:35	2007-10-31 10:06:35	0	0	0	0	0	-1	0, 
    		//3005	nodeName	-	-	.	-	-	0	-	-	-	0	-1	0 
	        StringTokenizer st = new StringTokenizer(dbstring, "\t");
	        this.id = Util.to_Number(st.nextToken());
	        if (ver){
	        	this.version = st.nextToken();
	        }
	        else 
	        {

	            this.name = st.nextToken();
	            try{
		            this.dt = new DocType(Util.to_Number(st.nextToken()),  st.nextToken());
	            }catch(Exception ex){
	            }
	        }
	        this.creator = st.nextToken();
	        this.creationDate = st.nextToken();
	        if (!ver) 
	        {
	            this.modifyDate = st.nextToken();
	            this.sessionId = Util.to_Number(st.nextToken());
	            this.pageCount=Util.to_Number(st.nextToken());
	            this.commentCount=Util.to_Number(st.nextToken());
	            this.refCount=Util.to_Number(st.nextToken());
	            try
	            {
	                this.signType=Util.to_Number(st.nextToken());
	                this.signId=Util.to_Number(st.nextToken());
	            }
	            catch(Exception e)
	            {
	                this.signType=-1;
	                this.signId=0;
	            }
	        }
	        else 
	        {
	            this.VRComment = st.nextToken();
	            this.versionDocFile = st.nextToken();
	        }
	        this.freezed = Util.to_Number(st.nextToken());
	        this.colorCode = st.nextToken();
	        this.scope = Util.to_Number(st.nextToken());
	        this.status = Util.to_Number(st.nextToken());
	        if (!ver)
	        	this.routeUserSignType = Util.to_Number(st.nextToken());
	        this.notificationExist = Util.to_Number(st.nextToken());
	        //Added to check the lock type
	        this.lockType = Util.to_Number(st.nextToken());
	        this.docPendingStatus = Util.to_Number(st.nextToken());
	        while(st.hasMoreTokens()){
	        	customIndexValues.add((String)st.nextToken());
	        }
	        
    	}catch(Exception ex){  
    	}
    }
    /*
    * new constructor added for Document List enhancement. This is to get values for custom index fields
    * If HitList = 1 then it is for search result list.
    * If HitList = 0 then it is DocList
    * Valli 9 May 2007 */
    // HitList 1 is from the DTC, AdminWise, QC Indexer and if Hitlist = 2 then it is call from Web Access
    public Document(String dbstring, int HitList) 
    {
    	try{
    		System.out.println("dbstring Document :::: "+dbstring);
    		//1236	Sodexo	5	Announcement	vwadmin	2011-11-24 20:42:24	2011-11-25 11:31:38	0	3	1	0	0	-1	0	0,0,0	0	0	1
	        StringTokenizer st = new StringTokenizer(dbstring, "\t");
	        this.id = Util.to_Number(st.nextToken());
	        this.name = st.nextToken();
            this.dt = new DocType(Util.to_Number(st.nextToken()),st.nextToken());
            this.creator = st.nextToken();
            this.creationDate = st.nextToken();
            this.modifyDate = st.nextToken();
            if(HitList >= 1)
            	this.version = 	st.nextToken();
    	    this.sessionId = Util.to_Number(st.nextToken());
    	    this.pageCount = Util.to_Number(st.nextToken());
    	    this.commentCount = Util.to_Number(st.nextToken());
    	    this.refCount =  Util.to_Number(st.nextToken());
    	    if(HitList >= 1)
    	    	this.location = st.nextToken();
    	    this.signType = Util.to_Number(st.nextToken());
    	    this.signId=Util.to_Number(st.nextToken());
    	    try{
            if(HitList >= 1)
            	this.commentInclude = Integer.parseInt(st.nextToken());
    	    }catch(Exception ex){
    	    	
    	    }
    	    this.freezed =  Util.to_Number(st.nextToken());
    	    //Purpose: nodeType added for newSearch which will list folder/cabinet/drawer/document
    	    if(HitList >= 1){
    	    	try{
    	    		this.nodeType =  st.nextToken();
    	    	}catch(Exception ex){}
    	    }
    	    this.colorCode = st.nextToken();
    	    this.scope = Util.to_Number(st.nextToken());
    	    this.status = Util.to_Number(st.nextToken());
    	    this.routeUserSignType = Util.to_Number(st.nextToken());
    	    this.notificationExist = Util.to_Number(st.nextToken());
    	  //Added to check the lock type
    	    this.lockType = Util.to_Number(st.nextToken());
    	    this.docPendingStatus = Util.to_Number(st.nextToken());
    	    if (HitList == 2){//webaccess 
    	    	try{
    	    	resultType = Util.to_Number(st.nextToken());
    	    	}catch(Exception ex){
    	    		
    	    	}
    	    }
	        while(st.hasMoreTokens()){
	        	customIndexValues.add((String)st.nextToken());
	        }
    	}catch(Exception ex){ 
    		this.id = -1;
    	}
    }
    
    public Document(String dbstring, String ver) 
    {
    	try{
    		StringTokenizer st = new StringTokenizer(dbstring, "\t");
    		this.id = Util.to_Number(st.nextToken());
    		this.version = st.nextToken();

    		try{
    			this.dt = new DocType(Util.to_Number(st.nextToken()),  st.nextToken());
    		}catch(Exception ex){
    		}
    		this.creator = st.nextToken();
    		this.creationDate = st.nextToken();
    		this.VRComment = st.nextToken();
    		this.versionDocFile = st.nextToken();

    	}catch(Exception ex){  
    	}
    }
    public void setDocType(DocType dt) 
    {
        this.dt = dt;
    }
    public DocType getDocType() 
    {
        return this.dt;
    }
    public void setName(String name) 
    {
        this.name = name;
    }
    public void setCreator(String creator) 
    {
        this.creator = creator;
    }
    public void setCreationDate(String creationDate) 
    {
        this.creationDate = creationDate;
    }
    public void setModifyDate(String modifyDate) 
    {
        this.modifyDate = modifyDate;
    }
    public int getSessionId() 
    {
        return this.sessionId;
    }
    public void setSessionId(int sid) 
    {
        this.sessionId = sid;
    }
    public String getCreationDate() 
    {
        return this.creationDate;
    }
    public String getCreator() 
    {
        return this.creator;
    }
    public String getVersion() 
    {
        return this.version;
    }
    public void setVersion(String ver) 
    {
        this.version = ver;
    }
    public String getModifyDate() 
    {
        return this.modifyDate;
    }
    public void set(Document doc) 
    {
        this.id = doc.id;
        this.name = doc.name;
        this.creator = doc.creator;
        this.creationDate = doc.creationDate;
        this.modifyDate = doc.modifyDate;
        this.dt = doc.getDocType();
        this.freezed = doc.getFreezed();
    }
    public VWDoc getVWDoc() 
    {
        return this.vwdoc;
    }
    public void setVWDoc(VWDoc vwdoc) 
    {
        this.vwdoc = vwdoc;
    }
    public int getRoomId() 
    {
        return roomId;
    }
    public void setRoomId(int roomId) 
    {
        this.roomId = roomId;
    }
    public String getVRComment() 
    {
        return VRComment;
    }
    public void setVRComment(String VRComment) 
    {
        this.VRComment = VRComment;
    }
    public String getVersionDocFile() 
    {
        return versionDocFile;
    }
    public void setVersionDocFile(String versionDocFile) 
    {
        this.versionDocFile = versionDocFile;
    }
    public int getPageCount() 
    {
        return pageCount;
    }
    public void setPageCount(int pageCount) 
    {
        this.pageCount = pageCount;
    }
    public String getRoomName() 
    {
        return roomName;
    }
    public void setRoomName(String roomName) 
    {
        this.roomName = roomName;
    }
    public long getLastModifyDate()
    {
        return vwdoc.getModifyDate();
    }
    public String getTag() 
    {
        return tag;
    }
    public void setTag(String tag) 
    {
        this.tag = tag;
    }
    public int getCommentCount() 
    {
        return commentCount;
    }
    public void setCommentCount(int commentCount) 
    {
        this.commentCount = commentCount;
    }
    public int getRefCount() 
    {
        return refCount;
    }
    public void setRefCount(int refCount) 
    {
        this.refCount = refCount;
    }
    public String getLocation() 
    {
        return location;
    }
    public void setLocation(String location) 
    {
        this.location = location;
    }
    public long getSize() 
    {
        return vwdoc.getSize();
    }
    
    /** Getter for property signType.
     * @return Value of property signType.
     */
    public int getSignType() {
        return signType;
    }
    
    /** Setter for property signType.
     * @param signType New value of property signType.
     */
    public void setSignType(int signType) {
        this.signType = signType;
    }
    
    /** Getter for property signId.
     * @return Value of property signId.
     */
    public int getSignId() {
        return signId;
    }
    
    /** Setter for property signId.
     * @param signId New value of property signId.
     */
    public void setSignId(int signId) {
        this.signId = signId;
    }
    /** get the document is freezed or not.
      * @return Value of property freezed.
     */
	public boolean isFreezed() {
		return (freezed == 1)?true:false;
	}
	/** Setter for property signId.
     * @param freezed New value of property signId.
     */
	public void setFreezed(int freezed) {
		this.freezed = freezed;
	}
	public void setFreezed(boolean freezed) {
		this.freezed = (freezed?1:0);
	}
	public int getFreezed() {
		return this.freezed;
	}
	public Vector getCustomIndexValues() {
		return customIndexValues;
	}
	public void setCustomIndexValues(Vector customIndexValues) {
		this.customIndexValues = customIndexValues;
	}
	public String getNodeType() {
		return nodeType;
	}
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}
	public static void printToConsole(String msg)
	{
    	try
    	{
    		String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
			msg = now + " : " + msg + " \n";    			
			java.util.Properties props = System.getProperties(); 
	     	String filepath =props.getProperty("user.home")+ File.separator + "Application Data"+ File.separator + Constants.PRODUCT_NAME + File.separator + "document.log";
			File log = new File(filepath);        				                                    
	        FileOutputStream fos = new FileOutputStream(log, true);
	        fos.write(msg.getBytes());
	        fos.close();            
	        fos.close();
	        log = null;              
    	}catch (Exception e)
        {
        	System.out.println( "printToConsole");	
        }
	}
	/**
	 * @return Returns the commentInclude.
	 */
	public int getCommentInclude() {
		return commentInclude;
	}
	/**
	 * @param commentInclude The commentInclude to set.
	 */
	public void setCommentInclude(int commentInclude) {
		this.commentInclude = commentInclude;
	}
	public String getColorCode() {
		return colorCode;
	}
	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
	    return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
	    this.status = status;
	}
	public int getResultType() {
		return resultType;
	}
	public void setResultType(int resultType) {
		this.resultType = resultType;
	}
	public int getRouteUserSignType() {
		return routeUserSignType;
	}
	public void setRouteUserSignType(int routeUserSignType) {
		this.routeUserSignType = routeUserSignType;
	}
	public boolean isNotificationExist() {
		return (notificationExist > 0);
	}
	public void setNotificationExist(int notificationExist) {
		this.notificationExist = notificationExist;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public String getDeletedHost() {
		return deletedHost;
	}
	public void setDeletedHost(String deletedHost) {
		this.deletedHost = deletedHost;
	}
	public String getDeletedLocation() {
		return deletedLocation;
	}
	public void setDeletedLocation(String deletedLocation) {
		this.deletedLocation = deletedLocation;
	}
	public String getDeletedTime() {
		return deletedTime;
	}
	public void setDeletedTime(String deletedTime) {
		this.deletedTime = deletedTime;
	}
	public String getDeletedUser() {
		return deletedUser;
	}
	public void setDeletedUser(String deletedUser) {
		this.deletedUser = deletedUser;
	}	
	public int getScope() {
		return scope;
	}
	public void setScope(int scope) {
		this.scope = scope;
	}
	
	public int getLockType() {
		return lockType;
	}
	public void setLockType(int lockType) {
		this.lockType = lockType;
	}
	public static void main(String[] args) {
		/*String data = "786	111	2	Purchase Order	admin	2011-07-25 14:01:17	2011-07-25 14:01:17	.	-1	1	0	0	SYNTAXWKTBLR024:E:\\Storage StdDemo	0	-1	0	0	Document	0,0,0	0	0	-1	admin	10.4.8.151	2013-03-04 16:11:25	Export\\Large\\Docs\\111";
		Document doc = new Document(data, 1);
		
		Vector searchData = new Vector();
		searchData.add(data);
		Vector result =  VWClient.vectorToSearchDocuments(searchData, "StandarDemo", 1);
		Document docResult = (Document) result.get(0);
		System.out.println("Del Info " + docResult.getDeletedHost() + "\n" + docResult.getDeletedLocation() + "\n" + docResult.getDeletedTime() + "\n" + docResult.getDeletedUser());
		*/				
	}
}