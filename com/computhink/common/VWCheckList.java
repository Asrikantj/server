/*
                      Copyright (c) 1997-2007
                         Computhink Software
 
                      All rights reserved.
 */

/**
 * VWCheckList<br>
 *
 * @version     $Revision: 1.1 $
 * @author      <a href="mailto:fadish@yqi.com">Fadi Shehadeh</a>
 **/

package com.computhink.common;

import java.util.List;
import java.util.LinkedList;
import java.util.Vector;
import java.awt.Component;
import java.awt.Dimension;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;
import javax.swing.UIManager;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import com.computhink.common.Principal;
import com.computhink.common.AclEntry;
import com.computhink.common.Creator;
import com.computhink.common.DocType;
import com.computhink.vwc.VWUtil;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
//--------------------------------------------------------------------------
public class VWCheckList extends JPanel {
    
    public VWCheckList() {
    	BorderLayout border = new BorderLayout();
    	border.setHgap(200);
        setLayout(border);
        setBackground(java.awt.Color.white);
        
        listDescription.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        listDescription.addKeyListener(new KeyAdapter() {
        public void keyReleased(KeyEvent ke) {
            if(ke.getKeyCode() == KeyEvent.VK_SPACE)
            {
                int selectedIndex = listDescription.getSelectedIndex();
                if (selectedIndex < 0)  return;
                CheckBoxItem item = (CheckBoxItem)listCheckBox.getModel().getElementAt(selectedIndex);
                boolean checked=!item.isChecked();
                if(item.isEnabled())
                {
                    item.setChecked(checked);
                    itemCheckChanded(checked,selectedIndex);
                    listCheckBox.repaint();
                }
            }
        }
    });
       listDescription.addMouseListener(new MouseAdapter(){
        public void mouseClicked(MouseEvent me) {
            if (me.getClickCount() != 2)    return;
            int selectedIndex = listDescription.locationToIndex(me.getPoint());
            if (selectedIndex < 0)  return;
            CheckBoxItem item = (CheckBoxItem)listCheckBox.getModel().getElementAt(selectedIndex);
            boolean checked=!item.isChecked();
            if(item.isEnabled())
            {
                item.setChecked(checked);
                itemCheckChanded(checked,selectedIndex);
                listCheckBox.repaint();
            }
        }
        });
            listCheckBox.setCellRenderer(new CheckBoxRenderer());
            listDescription.setCellRenderer(new DescriptionListRenderer());
            listCheckBox.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            listCheckBox.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent me) {
                    int selectedIndex = listCheckBox.locationToIndex(me.getPoint());
                    if (selectedIndex < 0)
                        return;
                    CheckBoxItem item = (CheckBoxItem)listCheckBox.getModel().getElementAt(selectedIndex);
                    boolean checked=!item.isChecked();
                    item.setChecked(checked);
                    selected[selectedIndex]=checked;
                    itemCheckChanded(checked,selectedIndex);
                    if(selectedIndex==0)
                    {
                        listDescription.getSelectionModel().clearSelection();
                        item.setChecked(checked);
                    }
                    listDescription.setSelectedIndex(selectedIndex);
                    listCheckBox.repaint();
                }
            });
            JScrollPane scrollPane = new JScrollPane();            
            scrollPane.setRowHeaderView(listCheckBox);
            scrollPane.setViewportView(listDescription);
            listDescription.setFixedCellHeight(20);
            listCheckBox.setFixedCellHeight(listDescription.getFixedCellHeight());
            listCheckBox.setFixedCellWidth(30);
            add(scrollPane,BorderLayout.CENTER);
    }
    public void addVWEventListener(VWCheckListListener listener) {
            listenerList.add(VWCheckListListener.class, listener);
        }
        // This methods allows classes to unregister for MyEvents
        public void removeVWEventListener(VWCheckListListener listener) {
            listenerList.remove(VWCheckListListener.class, listener);
        }
        // This private class is used to fire MyEvents
        void fireVWEvent(VWItemCheckEvent evt) {
            Object[] listeners = listenerList.getListenerList();
            // Each listener occupies two elements - the first is the listener class
            // and the second is the listener instance
            for (int i=0; i<listeners.length; i+=2) {
                if (listeners[i]==VWCheckListListener.class) {
                    if(evt.isChecked())
                        ((VWCheckListListener)listeners[i+1]).VWItemChecked(evt);
                    else
                        ((VWCheckListListener)listeners[i+1]).VWItemUnchecked(evt);
                }
            }
        }
    //--------------------------------------------------------------------------
    public VWCheckList(Object[] listData) {
        this();
        loadData(listData);
    }
    //--------------------------------------------------------------------------
    public void loadData(Object[] listData) {
        Vector data=new Vector();
        loadData(data);
    }
    //--------------------------------------------------------------------------
    public void loadData(Vector listData) {
        int count = listData.size();
        listItems=new Object[count];
        listCheckBox.setListData(buildCheckBoxItems(count));
        listDescription.setListData(listData);
        selected=new boolean[count];
        for(int i =0;i<count;i++) {
            listItems[i]=listData.get(i);
            selected[i]=false;
        }
    }
    //--------------------------------------------------------------------------
    private CheckBoxItem[] buildCheckBoxItems(int totalItems) {
        CheckBoxItem[] checkboxItems = new CheckBoxItem[totalItems];
        for (int counter=0;counter<totalItems;counter++)
        {
            checkboxItems[counter] = new CheckBoxItem();
            checkboxItems[counter].setEnabled(true);
        }
        return checkboxItems;
    }
    //--------------------------------------------------------------------------
    /* Inner class to hold data for JList with checkboxes */
    class CheckBoxItem {
        private boolean isChecked;
        private boolean isEnabled;
        public CheckBoxItem() {
            isChecked = false;
            isEnabled = true;
        }
        public boolean isChecked(){
            return isChecked;
        }
        public void setChecked(boolean value) {
            isChecked = value;
        }
        public boolean isEnabled(){
            return isEnabled;
        }
        public void setEnabled(boolean value) {
            isEnabled = value;
        }
    }
    //--------------------------------------------------------------------------
    /* Inner class that renders JCheckBox to JList*/
    class CheckBoxRenderer extends JCheckBox implements ListCellRenderer {
        
        public CheckBoxRenderer() {
            setBackground(UIManager.getColor("List.textBackground"));
            setForeground(UIManager.getColor("List.textForeground"));
        }
        public Component getListCellRendererComponent(JList listBox,Object obj,
        int currentindex,boolean isChecked, boolean hasFocus) {
            setSelected(((CheckBoxItem)obj).isChecked());
            setEnabled(((CheckBoxItem)obj).isEnabled());
            return this;
        }
    }
    //--------------------------------------------------------------------------
    class DescriptionListRenderer extends javax.swing.DefaultListCellRenderer{
        public DescriptionListRenderer() {
            super();
        }
        public Component getListCellRendererComponent(JList listBox,Object obj,
        int currentindex,boolean isChecked, boolean hasFocus) {
            Component component=super.getListCellRendererComponent(listBox,obj,currentindex,
            isChecked,hasFocus);
            if(listItems==null || listItems.length==0) return component;
            Object index=listItems[currentindex];
            return component;
        }
    }
    //--------------------------------------------------------------------------
    public Object getItem(int index) {
        if(listItems==null)
            return null;
        return listItems[index];
    }
    //--------------------------------------------------------------------------
    public boolean getItemIsCheck(int index) {
        if(index<0) return false;
        CheckBoxItem item = (CheckBoxItem)listCheckBox.getModel().getElementAt(index);
        return item.isChecked();
    }
    //--------------------------------------------------------------------------
    public void setItemCheck(int index,boolean value,int itemId) {
        if(listCheckBox.getModel().getSize()==0) return;
        if(itemId==0) {
            CheckBoxItem item =
            (CheckBoxItem)listCheckBox.getModel().getElementAt(index);
            item.setChecked(value);
            selected[index]=value;
        }
        else {
            int itemIndex=-1;
            for(int i=0;i<listItems.length;i++) {
                if (itemId==getItemId(listItems[i])) {
                    itemIndex=i;
                    break;
                }
            }
            if(itemIndex>=0) setItemCheck(itemIndex,true);
        }
    }
    //--------------------------------------------------------------------------
    public void setItemEnable(int index,boolean enable) {    	
        if(listCheckBox.getModel().getSize()==0) return;
        CheckBoxItem item =
        (CheckBoxItem)listCheckBox.getModel().getElementAt(index);        
        item.setEnabled(enable);        
    }
    //--------------------------------------------------------------------------
    public void setItemCheck(int index,boolean value) {
        setItemCheck(index,value,0);
    }
    //--------------------------------------------------------------------------
    public void removeItems() {
        listCheckBox.removeAll();
        listDescription.removeAll();
        listItems=null;
        selected=null;
    }
    //--------------------------------------------------------------------------
    private void itemCheckChanded(boolean value,int index) {
        ///if(source<0) return;
        CheckBoxItem item =
            (CheckBoxItem)listCheckBox.getModel().getElementAt(index);
        if(!item.isEnabled()) 
        {
            setItemCheck(index,!value);
            return;
        }
        VWItemCheckEvent evt=new VWItemCheckEvent(this);
        evt.setItemIndex(index);
        evt.setObject(listItems[index]);
        evt.setChecked(value);
        fireVWEvent(evt);
    }
    //--------------------------------------------------------------------------
    public List getSelectedItems()
    {
       if(listItems==null) return null;
        List list=new LinkedList();
        int count=listItems.length;
        for(int i=0;i<count;i++)
            if(((CheckBoxItem)listCheckBox.getModel().getElementAt(i)).isChecked())
                list.add(listItems[i]);
        return list;
    }
    //--------------------------------------------------------------------------
    public int getselectedItemsCount() {
        int count=selected.length,ret=0;
        for(int i=0;i<count;i++) {
            if(selected[i]) ret++;
        }
        return ret;
    }
    //--------------------------------------------------------------------------
    /*public void setCheckItems(VWRecord[] listData) {
        deSelectAllItems();
        if (listData==null) return;
        int count = listData.length;
        
        for(int i=0;i<count;i++) {
            for(int j=0;j<listItems.length;j++) {
                if (getItemId(listItems[j])==listData[i].getId()) {
                    setItemCheck(j,true);
                    break;
                }
            }
        }
        listCheckBox.repaint();
    }*/
    public void setCheckItems(Vector listData) {
        deSelectAllItems();
        if (listData==null || (listData != null && listData.size() == 0)) {
        	setCheckAllItems(0);
        	return;
        }
        	
        int count = listData.size();
        
        for(int i=0;i<count;i++) {
            for(int j=0;j<listItems.length;j++) {
                if (listItems[j].equals(listData.get(i))) {
                    setItemCheck(j,true);
                    break;
                }
            }
        }
        listCheckBox.repaint();
    }    
    //--------------------------------------------------------------------------
    public void setCheckItems(int[] listData) {
        deSelectAllItems();
        if (listData==null) return;
        int count = listData.length;
        
        for(int i=0;i<count;i++) {
            for(int j=0;j<listItems.length;j++) {
                if (getItemId(listItems[j])==listData[i]) {
                    setItemCheck(j,true);
                    break;
                }
            }
        }
        listCheckBox.repaint();
    }
    //--------------------------------------------------------------------------
    public void setCheckAllItems(int beginWith) {
        if(listItems==null) return;
        int count = listItems.length;
        for(int i=beginWith;i<count;i++)
            setItemCheck(i,true);
        listCheckBox.repaint();
    }
    //--------------------------------------------------------------------------
    public void setCheckItems(List listData) {
        deSelectAllItems();
        if (listData==null) return;
        int count = listData.size();
        int itemId=0;
        for(int i=0;i<count;i++) {
            Object item=listData.get(i);
            if(item instanceof Creator)
                itemId=((Creator)item).getId();
            else if(item instanceof DocType)
                itemId=((DocType)item).getId();
            else if(item instanceof AclEntry)
                itemId=((AclEntry)item).getPrincipal().getId();
            else if(item instanceof Principal)
                itemId=((Principal)item).getId();
            else
                itemId=VWUtil.to_Number((String)item);
            if(listItems!=null) {
                for(int j=0;j<listItems.length;j++) {
                    if(itemId==getItemId(listItems[j])) {
                        setItemCheck(j,true);
                        break;
                    }
                }
            }
        }
        listCheckBox.repaint();
    }
    //--------------------------------------------------------------------------
    public List getSelectedItemIds() {
        if(listItems==null) return null;
        List list=new LinkedList();
        int count=listItems.length;
        for(int i=0;i<count;i++)
            if(((CheckBoxItem)listCheckBox.getModel().getElementAt(i)).isChecked())
                list.add(Integer.toString(getItemId(listItems[i])));
        return list;
    }
    //--------------------------------------------------------------------------
    private int getItemId(Object item) {
        if (item instanceof DocType)
            return ((DocType) item).getId();
        else if (item instanceof Creator)
            return ((Creator) item).getId();
        else if (item instanceof Principal)
            return ((Principal) item).getId();
    return 0;
    }
    //--------------------------------------------------------------------------
    public void deSelectAllItems() {
        for(int i=0;i<listDescription.getModel().getSize();i++) {
            setItemCheck(i,false);
        }
        listCheckBox.repaint();
    }
    public void disableAllItems() {    	
        for(int index=0; index < listDescription.getModel().getSize();index++) {
            setItemEnable(index, false);            
        }
    }

    //--------------------------------------------------------------------------
    public void selectAllItems() {
        for(int i=0;i<listDescription.getModel().getSize();i++) {
            setItemCheck(i,true);
        }
        listCheckBox.repaint();
    }
    //--------------------------------------------------------------------------
    public int getItemsCount() {
        return listDescription.getModel().getSize();
    }
    //--------------------------------------------------------------------------
    public JList getList() {
        return listDescription;
    }
    //--------------------------------------------------------------------------
    protected JList listCheckBox = new JList();
    protected JList listDescription = new JList();
    private boolean[] selected =null;
    private Object[] listItems=null;
    protected javax.swing.event.EventListenerList listenerList =
            new javax.swing.event.EventListenerList();

    //--------------------------------------------------------------------------
}