package com.computhink.common;

import java.io.Serializable;
import java.util.*;

public class Node implements Serializable
{
    public static final int FOLDER_TYPE = 0;
    public static final int DOCUMENT_TYPE = 1;
    public static final int CABINET_TYPE = 2;
    public static final int DRAWER_TYPE = 3;
    public static final int SUB_FOLDER_TYPE = 4;

    public static final int FOLDER = FOLDER_TYPE;
    public static final int DOCUMENT = DOCUMENT_TYPE;
    
    public static final int LEVEL_CAB = 10;
    public static final int LEVEL_DRW = 20;
    public static final int LEVEL_FOL = 30;

    private int id;
    private String name;
    private String description;
    private int level;
    private int sessionId;
    /**
     * Added to Check the type of the lock type,if lockType is 1 chekout related functionalities,
     * If lockType is 4 create ownership related Functionalities,Gurumurthy.T.S 01-03-2014
     */
    private int lockType;
    private int type;
    private int parentId;
    private int storageId;
    private int serverId;
    private int aclId = 0;
    private int countAclEntries = 0;
    private String path = "";     
    private Vector userGroupSecurityList = new Vector();  
    
    private String defaultDocType;
    private int countNodeProperties  =0;
    private Vector nodePropertiesList = new Vector();
    private String colorCode = "";
    private int scope = 0;
    private int notificationExist = 0;
     public Node(String dbstring)
    {
        StringTokenizer st = new StringTokenizer(dbstring, "\t");
        try
        {
            this.id = Util.to_Number(st.nextToken());
            this.name = st.nextToken();
            this.parentId = Util.to_Number(st.nextToken());
            this.sessionId = Util.to_Number(st.nextToken());
            this.aclId = Util.to_Number(st.nextToken());
            this.colorCode = st.nextToken();
            this.scope = Util.to_Number(st.nextToken());
            this.notificationExist = Util.to_Number(st.nextToken());
            //Added to check the type type of lock
            this.lockType = Util.to_Number(st.nextToken());
            if(st.hasMoreTokens()){
            	int nodeType = getNodeTypeInt(st.nextToken());
            	this.type = nodeType; 
            }
        }
        catch(Exception e){}
    }

     private int getNodeTypeInt(String nodeTypestr) {
    	 int nodeType = -1;
    	 try{
    		 if(nodeTypestr.equalsIgnoreCase("Cabinet")){
    			 nodeType = CABINET_TYPE;
    		 }
    		 else if(nodeTypestr.equalsIgnoreCase("Drawer")){
    			 nodeType = DRAWER_TYPE;
    		 }
    		 else if(nodeTypestr.equalsIgnoreCase("Folder")){
    			 nodeType = FOLDER_TYPE;
    		 }
    		 else if(nodeTypestr.equalsIgnoreCase("Sub Folder")){
    			 nodeType = SUB_FOLDER_TYPE;
    		 }
    		 else if(nodeTypestr.equalsIgnoreCase("Document")){
    			 nodeType = DOCUMENT_TYPE;
    		 }
    	 }catch (Exception e) {}
    	 return nodeType;
     }

	public Node(String dbstring, boolean mode)
     {
         StringTokenizer st = new StringTokenizer(dbstring, "\t");
         try
         {
        	 //6-1 EAS Archives-2 0-3 0-4 2-aclid 1  VWAdmin 67108863 
             this.id = Util.to_Number(st.nextToken()); // nodeid
             this.name = st.nextToken(); // node name
             this.parentId = Util.to_Number(st.nextToken());  //node Parent id
             this.sessionId = Util.to_Number(st.nextToken()); // if it is checkout , session id is > 0
             this.aclId = Util.to_Number(st.nextToken());
             this.colorCode = st.nextToken();
             this.scope = Util.to_Number(st.nextToken());
             this.notificationExist = Util.to_Number(st.nextToken());
             //Added to check the lock type
             this.lockType = Util.to_Number(st.nextToken());
             this.countAclEntries = Util.to_Number(st.nextToken());
             if (countAclEntries != 0){
            	 int aclCount = 0;
	             while(countAclEntries != aclCount){
	            	 // principalid, type, security access value
	            	 String userName = st.nextToken();
	            	 String permission = st.nextToken();
	            	 if (userName != null && userName.trim().length() > 0 && permission != null && permission.trim().length() > 0){
	            		 userGroupSecurityList.add(userName + Util.SepChar + permission);
	            	 }
            		 aclCount++;
	             }
             }
             if(st.hasMoreTokens()){
            	 this.defaultDocType = st.nextToken();
	             this.countNodeProperties= Util.to_Number(st.nextToken());
	             if (countNodeProperties != 0){
	            	 int propertiesCount = 0;
		             while(countNodeProperties != propertiesCount){
		            	 // Index name , Index value
		            	 String indexName = st.nextToken();
		            	 String indexVal = st.nextToken();
		            	 if(indexName != null && indexName.trim().length() > 0 && indexVal != null && indexVal.trim().length() > 0){
		            		 nodePropertiesList.add(indexName + Util.SepChar + indexVal);
		            	 }
		            	 propertiesCount++;
		             }
	             }
             }
         }
         catch(Exception e){}
     }
    public Node(int id)
    {
        this.id = id;
    }
    public Node(int id, String name)
    {
        this(id, name, FOLDER_TYPE, 0, 0, 0);
    }
    public Node(String name, int type)
    {
        this(0, name, type, 0, 0, 0);
    }
    public Node(String name, int type, int parentId)
    {
        this(0, name, type, parentId, 0, 0);
    }

    public Node(String name, int type, int parentId, int serverId)
    {
        this(0, name, type, parentId, 0, serverId);
    }

    public Node(int id, String name, int type)
    {
        this(id, name, type, 0, 0, 0);
    }
    public Node(int id, String name, int type, int parentId,
                int storageId, int serverId)
    {
        this.id = id;
        this.name = name;
        this.type = type;
        this.parentId = parentId;
        this.storageId = storageId;
        this.serverId = serverId;
    }
    public int getId()
    {
        return id;
    }
    public void setId(int id)
    {
        this.id = id;
    }
    public String getName()
    {
        return name;
    }
    public byte[] getNameBytes()
    {
        return name.getBytes();
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String description)
    {
        this.description = description;
    }
    public int getType()
    {
        return type;
    }
    public void setType(int type)
    {
        this.type = type;
    }
    public int getParentId()
    {
        return parentId;
    }
    public void setParentId(int parentId)
    {
        this.parentId = parentId;
    }
    public int getStorageId()
    {
        return storageId;
    }
    public void setStorageId(int storageId)
    {
        this.storageId = storageId;
    }
    public int getServerId()
    {
        return serverId;
    }
    public void setServerId(int serverId)
    {
        this.serverId = serverId;
    }
    public int getAclId()
    {
        return aclId;
    }
    public void setAclId(int aclId)
    {
        this.aclId = aclId;
    }
    public int getSessionId()
    {
        return this.sessionId;
    }
    public void setSessionId(int sid)
    {
        this.sessionId = sid;
    }
    public boolean isDocument()
    {
        if (type == DOCUMENT_TYPE) return true;
        return false;
    }
    public int getLevel() 
    {
        return level;
    }
    public void setLevel(int level) {
        this.level = level;
    }
    public void set(Node node) 
    {
        this.name = node.name;
        this.type = node.type;
        this.parentId = node.parentId;
        this.storageId = node.storageId;
        this.aclId = node.aclId;
        this.colorCode = node.colorCode;
    }

	/**
	 * @return Returns the userGroupSecurityList.
	 */
	public Vector getUserGroupSecurityList() {
		return userGroupSecurityList;
	}

	/**
	 * @param userGroupSecurityList The userGroupSecurityList to set.
	 */
	public void setUserGroupSecurityList(Vector userGroupSecurityList) {
		this.userGroupSecurityList = userGroupSecurityList;
	}

	/**
	 * @return Returns the nodePropertiesIndexList.
	 */
	public Vector getNodePropertiesList() {
		return nodePropertiesList;
	}

	/**
	 * @param nodePropertiesIndexList The nodePropertiesIndexList to set.
	 */
	public void setNodePropertiesList(Vector nodePropertiesIndexList) {
		this.nodePropertiesList = nodePropertiesIndexList;
	}

	/**
	 * @return Returns the defaultDocType.
	 */
	public String getDefaultDocType() {
		return defaultDocType;
	}

	/**
	 * @param defaultDocType The defaultDocType to set.
	 */
	public void setDefaultDocType(String defaultDocType) {
		this.defaultDocType = defaultDocType;
	}

	/**
	 * @return Returns the path.
	 */
	public String getPath() {
		return path;
	}

	/**
	 * @param path The path to set.
	 */
	public void setPath(String path) {
		this.path = path;
	}

	public String getColorCode() {
		return colorCode;
	}

	public void setColorCode(String colorCode) {
		this.colorCode = colorCode;
	}
	public boolean isNotificationExist() {
		return (notificationExist > 0);
	}
	public void setNotificationExist(int notificationExist) {
		this.notificationExist = notificationExist;
	}
	public int getScope() {
		return scope;
	}

	public void setScope(int scope) {
		this.scope = scope;
	}

	public int getLockType() {
		return lockType;
	}

	public void setLockType(int lockType) {
		this.lockType = lockType;
	}
	
}
