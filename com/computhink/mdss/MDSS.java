package com.computhink.mdss;
import com.computhink.common.*;
import com.computhink.vwc.VWClientListener;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;



public interface MDSS extends Remote, Constants
{
    public void addListener(VWClientListener l) throws RemoteException;
    public void dispatchEvent(VWEvent event, int clientType) throws RemoteException;
    public boolean ping() throws RemoteException;
}
