package com.computhink.mdss;
import com.computhink.common.*;
import com.computhink.dss.server.*;
import com.computhink.vwc.*;
import com.computhink.vws.server.Client;

import java.net.*;
import java.util.*;
import java.rmi.registry.*;
import java.rmi.server.*;
import java.rmi.*;
import java.io.*;

public class MDSSServer extends UnicastRemoteObject implements MDSS//, DocServer
{
    private ServerSchema schema;
    private VWClientListener vcwListener = null;
    
    public MDSSServer(ServerSchema s) throws RemoteException 
    {
        super(s.dataport);
        this.schema = s;
    }
    public ServerSchema getSchema() throws RemoteException 
    {
        return this.schema;
    }
    /*
     *  Comment the following methods. all methods are moved to DSS.
     */
   /* public VWDoc setDocFolder(Document doc, String room) throws RemoteException 
    {
        return setDocument(doc, room);
    }
    public VWDoc setDocument(Document doc, String room) throws RemoteException 
    {
        VWDoc vwdoc = doc.getVWDoc();
        vwdoc.writeContents();
        return vwdoc;
    }
    public boolean getDocument(Document doc, String room, String file, 
                          ServerSchema s, boolean dummy) throws RemoteException 
    {
        VWDoc vwdoc = doc.getVWDoc();
        try
        {
            vwdoc.setSrcFile(file);
            vwdoc.setDstFile(s.path + Util.pathSep + doc.getId() + 
                                                  Util.pathSep + DOC_CONTAINER);
            DocServer ds = (DocServer) Util.getServer(s);
            vwdoc = ds.setDocument(doc, room);
            while (vwdoc.hasMoreChunks())
            {
                vwdoc = ds.setDocument(doc, room);
                doc.setVWDoc(vwdoc);
            }
            if (vwdoc.getIntegrity())
                return true;
            else
                return false;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    public boolean getDocFolder(Document doc, String room, String path, 
                                          ServerSchema s) throws RemoteException 
    {
        VWDoc vwdoc = doc.getVWDoc();
        try
        {
            vwdoc.setSrcFile(path);
            vwdoc.setDstFile(s.path + doc.getId());
            DocServer ds = (DocServer) Util.getServer(s);
            vwdoc = ds.setDocFolder(doc, room);
            while (vwdoc.hasMoreChunks())
            {
                vwdoc = ds.setDocument(doc, room);
                doc.setVWDoc(vwdoc);
            }
            if (vwdoc.getIntegrity())
                return true;
            else
                return false;
        }
        catch(Exception e)
        {
            return false;
        }
    }*/
    public void addListener(VWClientListener vwcl) throws RemoteException
    {
        this.vcwListener = vwcl;
    }
    public void dispatchEvent(VWEvent event,int clientType) throws RemoteException
    {
        if (vcwListener != null)  vcwListener.eventHandler(event);
        // Fix Start For Issue 485 and 494, Check for the Client Type Admin Wise and Web Client
        //if(clientType == 1 || clientType == 12 || clientType == 2 || clientType == 3 || clientType == 4) {
	    // client type added for ARS, 20 July 2006, Valli
        if (clientType == Client.Adm_Client_Type
				|| clientType == Client.Fat_Client_Type
				|| clientType == Client.NFat_Client_Type
				|| clientType == Client.NWeb_Client_Type
				|| clientType == Client.Web_Client_Type
				|| clientType == Client.AIP_Client_Type
				|| clientType == Client.All_Client_Type
				|| clientType == Client.sdk_Client_Type
				|| clientType == Client.Ars_Client_Type 
        		|| clientType == Client.Drs_Client_Type
        		|| clientType == Client.Fax_Client_Type) {
		// Fix End        	
            VWCUtil.sendDisconnect(event.getSessionId(), event.getId()); 
        }
    }
    public boolean ping() throws RemoteException 
    {
        return true;
    }
    
}
