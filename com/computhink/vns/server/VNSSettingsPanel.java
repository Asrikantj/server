/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vns.server;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

import com.computhink.resource.ResourceManager;
import com.computhink.vns.image.Images;
import com.computhink.vws.server.mail.VWMail;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.manager.ManagerConstants;
import com.computhink.manager.VWPanel;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VNSSettingsPanel extends VWPanel implements ManagerConstants, VNSConstants {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton bOK, bTestMail;
	private VWPanel pPorts, pVWS, pEmail;
	private JLabel lDataPort, lComPort, lVWSPort, lVWSHost,
	lEmailServer, lEmailServerPort, lEmailServerUsername, lEmailServerPassword, lFromEmailId,
	lPolling, lFileSizeLimit, lNotificationRetries;
	private JTextField tComPort, tDataPort, tVWSHost, tVWSPort, 
	tEmailServer, tEmailServerPort, tEmailServerUsername, tEmailServerPassword, tFromEmailId,
	tPolling, tFileSizeLimit, tNotificationRetries;
	private JCheckBox cbLog;
	private JCheckBox chkEnableSSL;
    private ResourceManager resourceManager=null;
	SpinnerNumberModel minuteModel = new SpinnerNumberModel(5, 1, 59, 1);

	private static final int height = 510;

	private ServerSchema vwsSchema;


	public VNSSettingsPanel() 
	{
		new Images();
		initComponents();
		getCurrentSettings();
	}
	private void initComponents() 
	{
		resourceManager=ResourceManager.getDefaultManager();
		pPorts = new VWPanel();
		pVWS = new VWPanel();
		pEmail = new VWPanel();
		cbLog = new JCheckBox(resourceManager.getString("VNSChk.LogInformation"));
		bOK = new JButton(resourceManager.getString("VNSBtn.Apply"));
		bTestMail = new JButton(resourceManager.getString("VNSBtn.TestMail"));
		chkEnableSSL = new JCheckBox(resourceManager.getString("VNSChk.SSLEnable"));
		tComPort = new JTextField();
		tDataPort = new JTextField();
		tVWSHost = new JTextField();
		tVWSPort = new JTextField();
		tEmailServer = new JTextField();
		tEmailServerPort = new JTextField();
		tEmailServerUsername = new JTextField();
		tEmailServerPassword = new JPasswordField();
		tFromEmailId = new JTextField();
		tPolling = new JTextField();
		tFileSizeLimit = new JTextField();
		tNotificationRetries = new JTextField();

		lVWSHost = new JLabel(resourceManager.getString("VNSLbl.VWSHost"));
		lVWSPort = new JLabel(resourceManager.getString("VNSLbl.VWSComPort"));
		lComPort = new JLabel(resourceManager.getString("VNSLbl.ComPort"));
		lDataPort = new JLabel(resourceManager.getString("VNSLbl.Dataport"));
		lEmailServer = new JLabel(resourceManager.getString("VNSLbl.EmailServ"));
		lEmailServerPort = new JLabel(resourceManager.getString("VNSLbl.EmailServPort"));
		lEmailServerUsername=new JLabel(resourceManager.getString("VNSLbl.Username"));
		lEmailServerPassword=new JLabel(resourceManager.getString("VNSLbl.Password"));
		lFromEmailId = new JLabel(resourceManager.getString("VNSLbl.FromEmailId"));
		lPolling = new JLabel(resourceManager.getString("VNSLbl.Polling"));
		lFileSizeLimit = new JLabel(resourceManager.getString("VNSLbl.FileSize"));
		lNotificationRetries = new JLabel(resourceManager.getString("VNSLbl.NotifiRetries"));

		setLayout(null);
		cbLog.setBackground(this.getBackground());
		chkEnableSSL.setBackground(this.getBackground());
		bOK.setBackground(this.getBackground());
		bTestMail.setBackground(this.getBackground());

		pPorts.setLayout(null);
		pPorts.setBorder(new TitledBorder(resourceManager.getString("VNSTitleBorder.ServPorts")));
		pPorts.setFocusable(false); add(pPorts);
		pPorts.setBounds(20, 10, 470, 70);
		pPorts.add(lComPort); lComPort.setBounds(20, 30, 120, 16);
		pPorts.add(lDataPort); lDataPort.setBounds(290, 30, 60, 16);
		pPorts.add(tComPort); tComPort.setBounds(160, 30, 50, 20);
		pPorts.add(tDataPort); tDataPort.setBounds(370, 30, 50, 20);

		pVWS.setLayout(null);
		pVWS.setBorder(new TitledBorder(resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VNSTitleBorder.Server")));
		pVWS.setFocusable(false); add(pVWS);
		pVWS.setBounds(20, 90, 470, 70);
		pVWS.add(lVWSHost); lVWSHost.setBounds(20, 30, 70, 16);
		pVWS.add(lVWSPort); lVWSPort.setBounds(240, 30, 120, 16);
		pVWS.add(tVWSHost); tVWSHost.setBounds(60, 30, 110, 20);
		pVWS.add(tVWSPort); tVWSPort.setBounds(370, 30, 50, 20);


		pEmail.setLayout(null);
		pEmail.setBorder(new TitledBorder(resourceManager.getString("VNSTitleBorder.EmailServ")));
		pEmail.setFocusable(false);
		add(pEmail);
		pEmail.setBounds(20, 170, 470, 275);
		int x = 20, intendX = 120, y = 26, width = 120, lHeight = 16, tHeight = 20, gap = 5;  
		pEmail.add(lEmailServer); lEmailServer.setBounds(x, y, width, lHeight);
		pEmail.add(lEmailServerPort); lEmailServerPort.setBounds(300, y, width, lHeight);
		y = y + 20;
		pEmail.add(tEmailServer); tEmailServer.setBounds(x, y, width+100, tHeight);
		pEmail.add(tEmailServerPort); tEmailServerPort.setBounds(300, y, 50, tHeight);
		y = y + 20 + gap;
		pEmail.add(lEmailServerUsername); lEmailServerUsername.setBounds(x, y, width, lHeight);
		pEmail.add(lEmailServerPassword); lEmailServerPassword.setBounds(300, y, width, lHeight);
		y = y + 20;
		pEmail.add(tEmailServerUsername); tEmailServerUsername.setBounds(x, y, 220, tHeight);
		pEmail.add(tEmailServerPassword); tEmailServerPassword.setBounds(300, y, width+30, tHeight);
		y = y + 20 + gap; 
		pEmail.add(lFromEmailId); lFromEmailId.setBounds(x, y, width, lHeight);
		y = y + 20; 
		pEmail.add(tFromEmailId); tFromEmailId.setBounds(x, y, 220, tHeight);
		gap = gap + 10;
		y = y + 20 + gap;
		pEmail.add(lPolling); lPolling.setBounds(x, y, width-20, lHeight);
		pEmail.add(lFileSizeLimit); lFileSizeLimit.setBounds(intendX+80, y, width, lHeight);
		y = y + 20;
		pEmail.add(tPolling); tPolling.setBounds(x, y, width-60, tHeight);
		pEmail.add(tFileSizeLimit); tFileSizeLimit.setBounds(intendX+80, y, width-40, tHeight);
		y = y + 20 + gap;
		pEmail.add(lNotificationRetries); lNotificationRetries.setBounds(x, y, width, lHeight);
		y = y + 20;
		pEmail.add(tNotificationRetries); tNotificationRetries.setBounds(x, y, width-90, tHeight);

		add(bOK); bOK.setBounds(400, 455, 90, 23);
		add(bTestMail); bTestMail.setBounds(300, 455, 90, 23);
		add(cbLog); cbLog.setBounds(20, 455, 130, 20);
		add(chkEnableSSL); chkEnableSSL.setBounds(150, 455, 100, 20);
		bOK.setToolTipText(resourceManager.getString("VNSToolTip.Apply"));
		bTestMail.setToolTipText(resourceManager.getString("VNSToolTip.TestMail"));

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(new Dimension(width, height));
		setLocation((screenSize.width - width)/2,(screenSize.height-height)/2);
		//-------------Listeners------------------------------------------------
		bOK.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				setSettings();
			}
		});
		bTestMail.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt){
				try{
					String message =resourceManager.getString("VNSMail.Message");
					String subject = resourceManager.getString("VNSMail.Subject1") + resourceManager.getString("CVProduct.Name") + " -"+ resourceManager.getString("VNSMail.Notification");
					System.out.println("Before sendTestMail() from VMail");
					VWMail.sendTestMail(tEmailServer.getText().trim(), tEmailServerPort.getText().trim(), tFromEmailId.getText().trim(), tFromEmailId.getText().trim(),tEmailServerUsername.getText(),tEmailServerPassword.getText(),"Notification",subject.trim(), message.trim());
				}catch(Exception ex){

				}
			}
		});
	}
	private void setSettings()
	{
		try
		{
			VNSPreferences.setComPort(Integer.parseInt(tComPort.getText()));
			VNSPreferences.setDataPort(Integer.parseInt(tDataPort.getText()));
			vwsSchema.address = tVWSHost.getText();
			vwsSchema.comport = Util.to_Number(tVWSPort.getText());
			VNSPreferences.setVWS(vwsSchema);
			VNSPreferences.setLogInfo(cbLog.isSelected());
			VNSPreferences.setSSL(chkEnableSSL.isSelected());
			VNSPreferences.setEmailServerName(tEmailServer.getText());
        	VNSPreferences.setFromEmailId(tFromEmailId.getText());
        	VNSPreferences.setEmailServerPort(tEmailServerPort.getText());
        	VNSPreferences.setEmailServerUsername(tEmailServerUsername.getText());
        	VNSPreferences.setEmailServerPassword(tEmailServerPassword.getText());
        	
        	String pollingTime = tPolling.getText();
            if(pollingTime != null && pollingTime.trim().equalsIgnoreCase("0")){
            	Util.Msg(this, resourceManager.getString("VNSMSG.PollingTime"),resourceManager.getString("VNSMSGTitle"));
            }else{
            	VNSPreferences.setNotificationPollingTime(pollingTime);
            }        	
        	VNSPreferences.setNotificationFileSizeLimit(tFileSizeLimit.getText());
        	VNSPreferences.setNotificationRetries(tNotificationRetries.getText());


		}
		catch(Exception e) {}
	}
	private void getCurrentSettings()
	{
		try
		{
			tComPort.setText(String.valueOf(VNSPreferences.getComPort()));
			tDataPort.setText(String.valueOf(VNSPreferences.getDataPort()));
			vwsSchema = VNSPreferences.getVWS();
			tVWSHost.setText(vwsSchema.address);
			int vPort = vwsSchema.comport;
			if (vPort > 0)
				tVWSPort.setText(String.valueOf(vPort));
			else
				tVWSPort.setText("");
			cbLog.setSelected(VNSPreferences.getLogInfo());
			chkEnableSSL.setSelected(VNSPreferences.getSSL());
			tEmailServer.setText(VNSPreferences.getEmailServerName());
			tEmailServerPort.setText(VNSPreferences.getEmailServerPort());
			tEmailServerUsername.setText(VNSPreferences.getEmailServerUsername());
			tEmailServerPassword.setText(VNSPreferences.getEmailServerPassword());
			tFromEmailId.setText(VNSPreferences.getFromEmailId());
			
			tPolling.setText(String.valueOf(VNSPreferences.getNotificationPollingTime()));
			tFileSizeLimit.setText(VNSPreferences.getNotificationFileSizeLimit());
			tNotificationRetries.setText(VNSPreferences.getNotificationRetries());

		}
		catch(Exception re)
		{
		}
	}
	public static void main(String a[]){
		try{
			String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
		}catch(Exception ex){

		}	        	
	}
}
