/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vns.server;

import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.VWClient;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Worker extends Thread implements ViewWiseErrors
{
	private static boolean shutDownRequest = false;
    private static boolean snoozing = false;
    public String roomName;
    private int snooze;
    public int sessionID;
    VWClient vwClient;
    private int schedulerStartInfo;

	public Worker(VWClient vwClient, String roomName, int snooze, int schedulerStartInfo, int sessionID)
    {
		this.vwClient = vwClient;
		this.snooze = snooze;
		this.sessionID = sessionID;
        this.roomName = roomName;
        this.schedulerStartInfo = schedulerStartInfo;
    }

	public void run()
    {
	  	 while (!shutDownRequest)
         {
	    	if(schedulerStartInfo == 0)
	    	{
	    		VNSLog.add("Processing Notification for the room: '"+roomName+"'");
	    		VNSProcessEmail vnsProcessEmail = new VNSProcessEmail();
	    		vnsProcessEmail.getProcessDetails(vwClient, sessionID, this.roomName);
	    		snoozing = true;
	    		try
	    		{
	    			sleep(snooze);
	    		}
	    		catch(Exception e){}
	    		snoozing = false;
	    	}
         }
	}
	 public void shutDown()
	    {
	        shutDownRequest = true;
	    }
}
