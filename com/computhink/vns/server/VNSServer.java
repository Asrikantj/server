/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vns.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TimerTask;
import java.util.Vector;

import com.computhink.common.LogMessage;
import com.computhink.common.SCMEvent;
import com.computhink.common.SCMEventListener;
import com.computhink.common.SCMEventManager;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.VWClient;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSPreferences;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VNSServer extends UnicastRemoteObject implements VNS, SCMEventListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Vector clients = new Vector();
	private Hashtable  workers = new Hashtable();
	private long stime;
	private ServerSchema mySchema;
	//VNSServer shutdown is not stopping the final action process.
	public static boolean shutDownRequest = false;
	private static ServerSchema  managerSchema = null;
	//Timer processTimer;
	private final String SIGN = "0B9B5177-1011-4BBF-B617-455288043D4A";

	VWClient vwClient;
	VWS vws;

	public VNSServer(ServerSchema s) throws RemoteException
	{
		super(s.dataport);
		this.mySchema = s;
		SCMEventManager scm = SCMEventManager.getInstance();
		scm.addSCMEventListener(this);
		stime = System.currentTimeMillis();
	}
	public void handleSCMEvent(SCMEvent event)
	{
		shutDown();
	}
	public void exit() throws RemoteException
	{
		shutDown();
	}
	public String getServerOS() throws RemoteException
	{
		return Util.getEnvOSName();
	}
	public void init() throws RemoteException
	{
		ServerSchema vwss = VNSPreferences.getVWS();
		vws = (VWS) Util.getServer(vwss);
		if (vws == null)
		{
			VNSLog.add(PRODUCT_NAME + " Server @" + vwss.address + ":" +vwss.comport + " inactive");
			VNSLog.war("Listening to " + PRODUCT_NAME + " Server...");
			vws = waitForViewWiseServer(vwss);
		}
		if (shutDownRequest) return;
		VNSLog.add("Connected to " + PRODUCT_NAME + " Server @" + vwss.address + ":" +vwss.comport);
		try
		{
			String serverName = null;
			String cacheDir = "c:\\temp";
			vwClient = new VWClient(SIGN, cacheDir,Client.Notification_Client_Type);
			{
				serverName = VNSConstants.DUMMY_SERVERNAME+"VNS";
				Vector rooms =new Vector();
				rooms = vws.getRoomNames();
				// loop for multiple rooms
				if(rooms!=null && rooms.size()>0)
				{
					for (int i = 0; i < rooms.size(); i++)
					{
						String roomName = (String) rooms.get(i);
						int sessionID = -1;
						sessionID = vws.login(roomName, Client.Notification_Client_Type, vwss.getAddress(), vwClient.encryptStr(VNSConstants.vnsAdminUser), "", vwss.getComport());
						
						if(sessionID>0){
							vwClient.setSession(sessionID, serverName, roomName, VNSConstants.vnsAdminUser);	
						}else{
							if(sessionID == ViewWiseErrors.NoMoreLicenseSeats){
								VNSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.NoMoreLicenseSeats));
							}else if(sessionID==ViewWiseErrors.ServerNotFound)
								VNSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.ServerNotFound));
							else if(sessionID==ViewWiseErrors.checkUserError)
								VNSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.checkUserError));
							else if(sessionID==ViewWiseErrors.NotAllowedConnections) 
								VNSLog.war("This Room is Administratively Down."+
										NewLineChar+"Please contact your " + PRODUCT_NAME + " Administrator"+
										NewLineChar+"before attempting to reconnect.");
							else if(sessionID==ViewWiseErrors.accessDenied)
								VNSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.accessDenied));
							else if(sessionID==ViewWiseErrors.LoginCountExceeded)
								VNSLog.war(vwClient.getErrorDescription(ViewWiseErrors.LoginCountExceeded));
							else if(sessionID==ViewWiseErrors.JarFileMisMatchErr)
								VNSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.JarFileMisMatchErr));
							else
								VNSLog.war(""+ PRODUCT_NAME +" room may be down.");
							continue;
						}
						int pollingTime = Integer.parseInt(VNSPreferences.getNotificationPollingTime()) * 60 * 1000;
						int schedulerStartInfo = 0; //This is nothing but service or application
						if(schedulerStartInfo == 0){
							Worker worker = new Worker(vwClient, roomName, pollingTime, 0, sessionID);
							if (worker == null)
							{
								VNSLog.war("Cannot start Notification Worker for room " + roomName);
								continue;
							}
							worker.start();
							workers.put(roomName, worker);
						}
					} // end for rooms loop
				} //if condition for room null
			} // end for servers loop
		}
		catch(Exception e)
		{
			VNSLog.err(PRODUCT_NAME + " Server connection error: " + e.getMessage());
		}
	}

	public String managerBegin(ServerSchema manager) throws RemoteException
	{
		if (managerSchema == null)
		{
			managerSchema = manager;
			return "";
		}
		else
		{
			if (managerSchema.address.equals(manager.address))
				return "";
			else
				return "Cannot monitor server. Manager @"
				+ managerSchema.address + " already connected.";
		}
	}
	public void managerEnd() throws RemoteException
	{
		managerSchema = null;
	}
	private void shutDown()
	{
		vwClient.shutdown();
		VNSLog.add("VW client stopped");
		//vwClient.shutdown() kills the MDSS schema which is not in use.
		vwClient = null;
		//VNSServer shutdown is not stopping the final action process.
		shutDownRequest = true;
		Enumeration wenum = workers.elements();
		while (wenum.hasMoreElements())
		{
			Worker worker = (Worker) wenum.nextElement();
			try{
				vws.logout(worker.roomName, worker.sessionID);
			}catch(Exception ex){}
			worker.shutDown();
			VNSLog.add("Stopped Notfication Worker for Room " + worker.roomName);
		}
		VNSLog.add("Stopping VNS Server...");
		VNSLog.writeLog();
		Util.sleep(500);
		Util.killServer(mySchema);
		VNSLog.err("Stopped");
	}

	private VWS waitForViewWiseServer(ServerSchema vwss)
	{
		VWS vws = null;
		while (vws == null && !shutDownRequest)
		{
			Util.sleep(2000);
			vws = (VWS) Util.getServer(vwss);
		}
		return vws;
	}

	private Vector waitForGetViewWiseRooms(VWS vws)
	{
		Vector ret=null;
		while ((ret == null || ret.size()==0) && !shutDownRequest)
		{
			try
			{
				Util.sleep(2000);
				ret = vws.getRoomNames();
			}
			catch(Exception e)
			{
				VNSLog.err("Error in getting " + PRODUCT_NAME + " rooms "+e.getMessage());
			}
		}
		return ret;
	}
	private Vector waitForGetViewWiseRooms(VWClient vwClient, String serverName)
	{
		Vector ret=null;
		while ((ret == null || ret.size()==0) && !shutDownRequest)
		{
			try
			{
				Util.sleep(2000);
				vwClient.getRooms(serverName,ret);
			}
			catch(Exception e)
			{
				VNSLog.err("Error in getting " + PRODUCT_NAME + " rooms "+e.getMessage());
			}
		}
		return ret;
	}

	public ServerSchema getSchema() throws RemoteException
	{
		return this.mySchema;
	}
	public void setComPort(int port) throws RemoteException
	{
		VNSPreferences.setComPort(port);
	}
	public int getComPort() throws RemoteException
	{
		return VNSPreferences.getComPort();
	}
	public void setDataPort(int port) throws RemoteException
	{
		VNSPreferences.setDataPort(port);
	}
	public int getDataPort() throws RemoteException
	{
		return VNSPreferences.getDataPort();
	}
	public ServerSchema getVWS() throws RemoteException
	{
		return VNSPreferences.getVWS();
	}
	public void setVWS(ServerSchema vws) throws RemoteException
	{
		VNSPreferences.setVWS(vws);
	}
	

	public boolean getDebugInfo() throws RemoteException
	{
		return VNSPreferences.getDebugInfo();
	}
	public void setDebugInfo(boolean b) throws RemoteException
	{
		VNSPreferences.setDebugInfo(b);
	}
	public boolean setManager(String man, String pas) throws RemoteException
	{
		return VNSPreferences.setManager(man, pas);
	}
	public String getManager() throws RemoteException
	{
		return VNSPreferences.getManager();
	}


	public long getTimeOn() throws RemoteException
	{
		return (System.currentTimeMillis() - stime) / 1000;
	}
	public LogMessage getLogMsg() throws RemoteException
	{
		return VNSLog.getLog();
	}
	public Vector getClients() throws RemoteException
	{
		return this.clients;
	}
	public int getLogCount() throws RemoteException
	{
		return VNSLog.getCount();
	}
	

	class SheduledProcess extends TimerTask {
		int ret = -1;
		private String roomName;
		private int sessionID;
		VWClient vwClient;
		public SheduledProcess(VWClient vwClient, int sessionID, String roomName){
			this.vwClient = vwClient;
			this.roomName = roomName;
			this.sessionID = sessionID;
		}
		private void doSheduledProcess(){
			//VNSLog.add("coming in doSheduledProcess : '"+roomName+"'");
			//VNSProcessNotification vnsProcessNotification = new VNSProcessNotification();
			//ret = vnsProcessDocs.getReportDocs(vwClient, sessionID, roomName);
			//ret = vnsProcessNotification.sendEmailNotify(vwClient, sessionID, roomName);
			for(int i=0; i<1000; i++){
				System.out.println(""+i);
			}
			//processTimer.cancel();
			//processTimer = null;
		}
		public void run(){
			doSheduledProcess();
		}
	}

	public boolean ping() throws RemoteException
    {
        return true;
    }
	//Need to Verify...
	public boolean getProxy() throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}
	public String getProxyHost() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	public int getProxyPort() throws RemoteException {
		// TODO Auto-generated method stub
		return 0;
	}
	public void setProxy(boolean b) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	public void setProxyHost(String server) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	public void setProxyPort(int port) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	//
	
	
	
	
	//--------------
	//Need to take from VWS email settings.
	public String getEmailServerName() throws RemoteException{
		return VWSPreferences.getEmailServerName();
	}
	public void setEmailServerName(String emailServerName) throws RemoteException {
		VNSPreferences.setEmailServerName(emailServerName);
	}
	public String getEmailServerPort() throws RemoteException{
		return VWSPreferences.getEmailServerPort();
	}
	public void setEmailServerPort(String emailServerPort) throws RemoteException {
		VNSPreferences.setEmailServerPort(emailServerPort);
	}
	public String getEmailServerUsername() throws RemoteException{
		return VWSPreferences.getEmailServerName();
	}
	public void setEmailServerUsername(String emailServerUsername) throws RemoteException {
		VNSPreferences.setEmailServerUsername(emailServerUsername);
	}
	public String getEmailServerPassword() throws RemoteException{
		return VWSPreferences.getEmailServerPassword();
	}
	public void setEmailServerPassword(String emailServerPassword) throws RemoteException {
		VNSPreferences.setEmailServerPassword(emailServerPassword);
	}
	public String getFromEmailId() throws RemoteException {
		return VWSPreferences.getFromEmailId();
	}
	public void setFromEmailId(String emailId) throws RemoteException {
		VNSPreferences.setFromEmailId(emailId);
	}
	
	//--------------	
	public String getNotificationPollingTime() throws RemoteException{
		return VNSPreferences.getNotificationPollingTime();
	}
	public void setNotificationPollingTime(String pollingTime) throws RemoteException{
		VNSPreferences.setNotificationPollingTime(pollingTime);
	}
	
	public String getNotificationFileSizeLimit() throws RemoteException {
		return VNSPreferences.getNotificationFileSizeLimit();
	}
	public void setNotificationFileSizeLimit(String notificationFileSizeLimit) throws RemoteException {
		VNSPreferences.setNotificationFileSizeLimit(notificationFileSizeLimit);
	}
	
	public String getNotificationRetries() throws RemoteException {
		return VNSPreferences.getNotificationRetries();
	}
	public void setNotificationRetries(String notificationRetries) throws RemoteException {
		VNSPreferences.setNotificationRetries(notificationRetries);
	}
	
	public String getNotificationEmailId() throws RemoteException{
		return VNSPreferences.getNotificationEmailId();
	}
	public void setNotificationEmailId(String notificationEmailId) throws RemoteException {
		VNSPreferences.setNotificationEmailId(notificationEmailId);
	}
	//--------------
	public boolean getLogInfo() throws RemoteException
	{
		return VNSPreferences.getLogInfo();
	}
	public void setLogInfo(boolean b) throws RemoteException
	{
		VNSPreferences.setLogInfo(b);
	}

}
