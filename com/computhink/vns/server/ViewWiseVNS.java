/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vns.server;

import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.manager.ManagerPreferences;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.mail.VWMail;
import com.computhink.resource.ResourceManager;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ViewWiseVNS implements VNSConstants{

	public static void main(String[] args) {
		String message = "";

		//VNSLog.err("prop CLASSPATH "+System.getProperty("CLASSPATH"));
		//VNSLog.err("java.class.path : " + System.getProperty("java.class.path"));
		
	      /*if (VNSPreferences.getProxy())
            Util.RegisterProxy(VNSPreferences.getProxyHost(),
                               VNSPreferences.getProxyPort(),
                               SERVER_VNS);*/
        String hostName = VNSPreferences.getHostName();
        if (hostName.length() > 0)
        {
            System.getProperties().put("java.rmi.server.useLocalHostname", "true");
            System.getProperties().put("java.rmi.server.hostname", hostName);
        }
        ServerSchema s = Util.getMySchema();
        s.comport = VNSPreferences.getComPort();
        s.dataport = VNSPreferences.getDataPort();
        s.type = SERVER_VNS;
        //VNSLog.add("VNS Info " + VNSPreferences.getComPort()+":"+VNSPreferences.getDataPort()+":"+SERVER_VNS);
        /* 
		 *	Master Switch off added for VNS in registry
         */
        System.out.println("ManagerPreferences.getVNSEndable() : "+ManagerPreferences.getVNSEnable());
        if(ManagerPreferences.getVNSEnable().equalsIgnoreCase("true")){
        	
	        int port = Util.createServer(s);
	        System.out.println("port is : "+port);
	        if ( port != 0)
	        {
	            VNSLog.add("Started " + PRODUCT_NAME + " Notification Server @" + hostName +"[" + s.address + "]:" + port);
	            try
	            {
	                ((VNS) Util.getServer(s)).init();
				}
	            catch(Exception e){}
	        }
	        else{
	        	message = ResourceManager.getDefaultManager().getString("ViewWiseVNs.NotStartNotifyServOnPort1")+" "+ PRODUCT_NAME +" "+
	        			ResourceManager.getDefaultManager().getString("ViewWiseVNs.NotStartNotifyServOnPort2")+ port;
	        	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VNS);

	        	VNSLog.err("Could not start " + PRODUCT_NAME + " Notification Server on port: " + port);
	        }
	        
	    }// Master switch off condition
	}
}
