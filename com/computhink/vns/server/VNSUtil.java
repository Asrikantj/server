/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vns.server;

import java.net.URL;
import java.util.StringTokenizer;

import com.computhink.common.Util;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class VNSUtil implements VNSConstants {

	private static String home = "";
    
    static
    {
        findHome();
    }
    public static String getHome()
    {
        return home;
    }
    private static void findHome()
    {
        String  urlPath = "";
        String  serverHome = "";
        String res = "com/computhink/vns/image/images/vns.gif";
        String jarres = "/lib/viewwise.jar!";
        
        int startIndex = 0;
        int endIndex = 0;
        try
        {
            URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
            urlPath = url.getPath();
        }
        catch(Exception e){}
        if (!urlPath.equalsIgnoreCase(""))
        {
            if (urlPath.startsWith("file"))
            {
                if (Util.getEnvOSName().startsWith("Windows"))
                    startIndex = 6;
                else
                    startIndex = 5;
                endIndex = urlPath.length() - res.length() - jarres.length();
            }
            else
            {
                startIndex = 1;
                endIndex = urlPath.length() - res.length();
            }
            serverHome = urlPath.substring(startIndex, endIndex-1);  
            serverHome = serverHome.replace('/', java.io.File.separatorChar);
        }
        home = cleanUP(serverHome);
    }
    private static String cleanUP(String str) // removes %20    
    {
       String cleanStr = ""; 
       StringTokenizer st = new StringTokenizer(str, "%20");
       while (st.hasMoreElements())
       {
           cleanStr = cleanStr + st.nextToken() + " ";
       }
       return (cleanStr.equalsIgnoreCase("")? str : cleanStr.trim());
    }
}
