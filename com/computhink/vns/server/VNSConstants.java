/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vns.server;

import com.computhink.common.Constants;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface VNSConstants extends Constants {
	
	public static final String VNS_PREF_ROOT  = "/computhink/" + PRODUCT_NAME.toLowerCase() + "/vns/server";
	public static final String VNS_CLASSPATH = "com/computhink/vns/image/images/vns.gif";
	public static final String VNS_JAR = "/lib/ViewWise.jar!";
	public static final String SETTINGS = PRODUCT_NAME + " Notification Server Settings";
	
	public static final int DEFAULT_DATA_PORT = 9001;
	public static final int DEFAULT_COM_PORT = 9000;
	/*public static final int DEFAULT_PROXY_PORT = 8080;*/
	public static final String DEFAULT_POLLINGTIME = "5";
		
	public static final int DEFAULT_LOG_LINES = 100;
	public static final String ERROR_LOG_FILE = "VNSErrors.log";
	public static final String LOG_FILE_PREFIX = "VNSLog-";
	public static final String LOG_FOLDER = "log";
	public static final String vnsAdminUser = "VNSAdmin";

	public static String DUMMY_SERVERNAME="SystemSrv";
	public static final int MAIL_SINGLE = 1;
	public static final int MAIL_BUILK = 2;
	
	public static final String DOCUMENT_ALERT = "DOCUMENT_ALERT";
	public static final String FOLDER_ALERT = "FOLDER_ALERT";
	public static final String BACKUP_ALERT = "BACKUP_ALERT";
	public static final String RECYCLE_ALERT = "RECYCLE_ALERT";
	public static final String RETENTION_ALERT = "RETENTION_ALERT";
	public static final String ROUTE_ALERT = WORKFLOW_MODULE_NAME.toUpperCase() +"_ALERT";
	public static int Folder_TYPE = 4;
	
	
	//For Notification emails
	//Document Module
	public static final String str_AllDocChanges = "Any changes to Document(s)";
	public static final String str_Doc_PropModified = "Properties (Index value) are Modified";
	public static final String str_Doc_PagesModified = "Document Pages added/updated/removed";
	public static final String str_Doc_CommentModified = "Comments are added/modified";
	public static final String str_Doc_Moved = "Document is moved";
	public static final String str_Doc_Freeze = "Freeze Document";
	public static final String str_Doc_UnFreeze = "Un-Freeze Document";
	public static final String str_Doc_Signature = "Signature applied";
	public static final String str_Doc_Indexed = "Document Indexed";
	public static final String str_Doc_PermissionMod = "Permission is Modified";
	public static final String str_Doc_Checkout = "Document Check Out";
	public static final String str_Doc_CheckIn = "Document Check In / Release Session";
	public static final String str_Doc_RouteEnded = WORKFLOW_MODULE_NAME +" Ended";
	
	public static final int notify_AllDocChanges = 1;
	public static final int notify_Doc_PropModified = 2;
	public static final int notify_Doc_PagesModified = 3;
	public static final int notify_Doc_CommentModified = 4;
	public static final int notify_Doc_Moved = 5;
	public static final int notify_Doc_Freeze = 6;
	public static final int notify_Doc_UnFreeze = 7;
	public static final int notify_Doc_Signature = 8;
	public static final int notify_Doc_Indexed = 9;
	public static final int notify_Doc_PermissionMod = 10;
	public static final int notify_Doc_Checkout = 11;
	public static final int notify_Doc_CheckIn = 12;
	public static final int notify_Doc_RouteEnded = 13;
	
	//Folder Module
	public static final String str_AllFolChanges = "Any changes to folder";
	public static final String str_Fol_AddedDoc = "Document /  Sub Folder is created";
	public static final String str_Fol_DocRemoved = "Document / Sub Folder is removed";
	public static final String str_Fol_PermissionMod = "Permission is modified";
	public static final String str_Fol_NodeProp = "Node Properties";
	public static final String str_Fol_CustomCols = "Custom columns";
	public static final String str_Fol_Checkout = "Folder Check Out";
	public static final String str_Fol_CheckIn = "Folder Check In";
	
	public static final int notify_AllFolChanges = 31;
	public static final int notify_Fol_AddedDoc = 32;
	public static final int notify_Fol_DocRemoved = 33;
	public static final int notify_Fol_PermissionMod = 34;
	public static final int notify_Fol_NodeProp = 35;
	public static final int notify_Fol_CustomCols = 36;
	public static final int notify_Fol_Checkout = 37;
	public static final int notify_Fol_CheckIn = 38;
	
	//Document Type Module
	public static final String str_DT_VREnabled = "Version revision Enabled / Disabled for Doc type";
	public static final String str_DT_AssignRoute = "Assigned "+ WORKFLOW_MODULE_NAME.toLowerCase() +" for Doc type";
	public static final String str_DT_ExternalDB = "External DB for Doc type";
	public static final String str_DT_DSNValue = "Selection value update from DSN";
	
	public static final int notify_DT_VREnabled = 51;
	public static final int notify_DT_AssignRoute = 52;
	public static final int notify_DT_ExternalDB = 53;
	public static final int notify_DT_DSNValue = 54;
	
	//Storage Management
	public static final String str_Storage_LocationMod = "Storage location changed";
	public static final String str_Storage_LocationMov = "Storage location Moved";

	public static final int notify_Storage_LocationMod = 71;
	public static final int notify_Storage_LocationMov = 72;
	
	//Audit Trail
	public static final String str_AT_UpdateSet = "Update setting";
	
	public static final int notify_AT_UpdateSet = 76;
	
	//Recycle Documents
	public static final String str_Recycle_Empty = "Empty Recycle";
	public static final String str_Recycle_purgeDocs = "Purge documents";
	public static final String str_Recycle_RestoreDocs = "Restore documents";
	
	public static final int notify_Recycle_Empty = 81;
	public static final int notify_Recycle_purgeDocs = 82;
	public static final int notify_Recycle_RestoreDocs = 83;
	
	//Restore Documents
	public static final String str_Restore_NewDT = "Doc type created while restoring";
	public static final String str_Restore_NewIndex = "Index created while restoring";
	
	public static final int notify_Restore_NewDT = 86;
	public static final int notify_Restore_NewIndex = 87;
	
	//Redaction
	public static final String str_Redaction_RepPassword = "Replace Passwords";
	public static final int notify_Redaction_RepPassword = 91;
	
	//Retention
	public static final String str_Retention_DocExpired = "Documents Expired";
	public static final String str_Retention_Update = "Update Retention";
	public static final String str_Retention_Delete = "Delete Retention";
	
	public static final int notify_Retention_DocExpired = 96;
	public static final int notify_Retention_Update = 97;
	public static final int notify_Retention_Delete = 98;
		
	//Route
	public static final String str_Route_Comp = WORKFLOW_MODULE_NAME +" completed";
	public static final String str_Route_Valid_to_Invalid = "Valid to Invalid "+ WORKFLOW_MODULE_NAME;
	public static final String str_Route_Invalid_to_Valid = "Invalid to Valid "+ WORKFLOW_MODULE_NAME;
	public static final String str_Route_Delete = "Delete "+ WORKFLOW_MODULE_NAME;
	
	public static final int notify_Route_Comp = 110;
	public static final int notify_Route_Valid_to_Invalid = 111;
	public static final int notify_Route_Invalid_to_Valid = 112;
	public static final int notify_Route_Delete = 113;
	
	
	
	
	//Module and Node Type
	public static final String module_Folder = "Folder";
	public static final String module_Document = "Document";
	public static final String module_DT = "DocumentType";
	public static final String module_Storage = "Storage Management";
	public static final String module_AT = "AuditTrail";
	public static final String module_RecycledDocs = "Recycle Documents";
	public static final String module_RestoreDocs = "Restore Documents";
	public static final String module_Redactions = "Redactions";
	public static final String module_Retention = "Retention";
	public static final String module_Route = WORKFLOW_MODULE_NAME;

	//Module Node type
	public static final int nodeType_Folder = 0;
	public static final int nodeType_Document = 1;
	public static final int nodeType_DT = 2;
	public static final int nodeType_Storage = 3;
	public static final int nodeType_AT = 4; 
	public static final int nodeType_RecycledDocs = 5;
	public static final int nodeType_RestoreDocs = 6;
	public static final int nodeType_Redaction =  7;
	public static final int nodeType_Retention = 8;
	public static final int nodeType_Route=9;


}
