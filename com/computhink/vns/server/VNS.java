/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vns.server;

import java.rmi.RemoteException;
import com.computhink.common.CIServer;
import com.computhink.common.ServerSchema;


/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface VNS extends CIServer {

	public ServerSchema getVWS() throws RemoteException; 
	public void setVWS(ServerSchema vws) throws RemoteException;
	public void init() throws RemoteException;
	
    public String getEmailServerName() throws RemoteException;
	public void setEmailServerName(String emailServerName)throws RemoteException;
	public String getFromEmailId() throws RemoteException;
    public void setFromEmailId(String emailId)throws RemoteException;
    public String getEmailServerPort() throws RemoteException;
    public void setEmailServerPort(String emailServerPort)throws RemoteException;
    public String getEmailServerUsername() throws RemoteException;
	public void setEmailServerUsername(String emailServerUsername)throws RemoteException;
    public String getEmailServerPassword() throws RemoteException;
	public void setEmailServerPassword(String emailServerPassword)throws RemoteException;

	public String getNotificationPollingTime() throws RemoteException;
	public void setNotificationPollingTime(String notificationPollingTime) throws RemoteException;
	public String getNotificationFileSizeLimit() throws RemoteException;
	public void setNotificationFileSizeLimit(String notificationFileSizeLimit) throws RemoteException;
	public String getNotificationRetries() throws RemoteException;
	public void setNotificationRetries(String notificationRetries) throws RemoteException;
	public String getNotificationEmailId() throws RemoteException;
	public void setNotificationEmailId(String notificationEmailId) throws RemoteException;
	
	public void setLogInfo(boolean flag) throws RemoteException;
	public boolean getLogInfo() throws RemoteException;
	public boolean ping() throws RemoteException;
}
