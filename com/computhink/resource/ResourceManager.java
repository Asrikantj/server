/*
 * ResourceManager.java
 *
 * Created on JAN 8, 2014
 * Author :Gurumurthy.T.S
 * 
 */

package com.computhink.resource;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.MissingResourceException;
import java.util.Vector;

/**
 * A class that manages resource bundles.
 * Follows the singleton pattern.
 * @author  Gurumurthy.T.S
 */
public class ResourceManager 
{
    /** stores singleton instances of managers */
    private static HashMap managers = new HashMap();
    
    /** current resource bundle */
    private ResourceBundle bundle;
    
    /** default resource bundle */
    private static final String defaultBundlePath = "com.computhink.resource.CVResource";
    private static final Locale locale = Locale.getDefault();
    private static String reskey="";
    public static Vector usedLocales = new Vector();
    
    /** Creates a new instance of ResourceManager */
    private ResourceManager(ResourceBundle bundle) 
    {
        this.bundle = bundle;
    }
    
    /** takes key string as an argument and returns associated value */
    public String getString(String key) {
        String value = null;
        try 
        {
            value = bundle.getString(key);
        } catch (MissingResourceException e) 
        {
            //System.out.println("java.util.MissingResourceException: Couldn't find value for: " + key);
        }
        if(value == null) {
            value = "  ";
        }
        return value;
    }
    /*
     * To check the system default Locale,and if Locale is other than the Used
     * Locale's Default en_US Locale will be returning
     */
    public static Locale getLocale() {
    	try{
    		usedLocales.add("en_US");
    		usedLocales.add("en_GB");
    		usedLocales.add("de_DE");
    		usedLocales.add("fr_CA");
    		usedLocales.add("zh_CN");
    		usedLocales.add("nl_NL");
    		if (usedLocales.contains(locale.toString())) {
    			return locale;
    		} else
    			return new Locale("en","US");
    	}
    	catch(MissingResourceException ex)
    	{
    		return new Locale("en","US");
    	}

    }
    
    /** returns manager instance according to bundle supplied */
    public static ResourceManager getManager(String bundlePath) 
    {
    	reskey = bundlePath + "_" + getLocale();
    	ResourceManager manager = (ResourceManager)managers.get(reskey);
    	if (manager == null)
    	{
    		try{
    			manager = new ResourceManager(ResourceBundle.getBundle(bundlePath,getLocale()));
    		}
    		catch(Exception ex)
    		{
    			System.out.println("inside catch : ");
    			manager = new ResourceManager(ResourceBundle.getBundle(bundlePath,new Locale("en","US")));
    		}
    		managers.put(reskey, manager);
    	}
    	return manager;
    }    
    
    /** returns default manager instance */
    public static ResourceManager getDefaultManager()
    {
        return getManager(defaultBundlePath);
    }
}
