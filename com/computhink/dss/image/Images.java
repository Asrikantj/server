 /*
 * Images.java
 *
 * Created on Sep 21, 2003, 12:14 AM
 */
package com.computhink.dss.image;
/**
 *
 * @author  Saad
 * @version 
 */
import javax.swing.ImageIcon;

public class Images
{
    public static ImageIcon app;

    public Images()
    {
        try
        {
            app = new ImageIcon(getClass().getResource("images/dss.gif"));
        }
        catch (Exception e){}
    }
}