/*
 * ViewWiseDSS.java
 *
 * Created on December 9, 2003, 6:03 PM
 */
package com.computhink.dss.server;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.TimerTask;

import com.computhink.common.*;
import com.computhink.vws.server.VWSLog;
/**
 *
 * @author  Administrator
 */
public class ViewWiseDSS implements DSSConstants
{
    public static void main(String args[]) 
    {
        if (DSSPreferences.getProxy())
            Util.RegisterProxy(DSSPreferences.getProxyHost(), 
                               DSSPreferences.getProxyPort(),
                               SERVER_DSS);
        //String hostName = DSSPreferences.getHostName();
	       String hostNameWithIP = DSSPreferences.getHostName();
	       String hostName = "";
	       String forceBindIP = "";
	       try{
	        int pos = -1;	       
	        if (hostNameWithIP != null && hostNameWithIP.trim().length() > 0){
	        	pos = hostNameWithIP.indexOf("|");
	        }
	        
	        if (pos > 0){
	        	forceBindIP = hostNameWithIP.substring(pos+1,hostNameWithIP.length());
	        	hostName = hostNameWithIP.substring(0,pos);
	        }else{
	        	hostName = hostNameWithIP;
	        }
	        if (hostName.length() > 0)
	        {
	            if (forceBindIP != null && forceBindIP.trim().length() > 0){
	            	System.setProperty("java.rmi.server.hostname", forceBindIP);
	            }else{
	            	System.setProperty("java.rmi.server.hostname", hostName);
	            }
	        }
	        }catch (Exception e) {
				// TODO: handle exception
	        	//System.out.println("Error :::: " + e.getMessage());
	        	e.printStackTrace();
	        	
		}        
        if (hostName.length() > 0)
        {
            System.setProperty("java.rmi.server.useLocalHostname", "true");
            System.setProperty("java.rmi.server.hostname", hostName);
        }
        ServerSchema s = Util.getMySchema();
        s.comport = DSSPreferences.getComPort();
        s.dataport = DSSPreferences.getDataPort();
        s.type = SERVER_DSS;
        if (forceBindIP != null && forceBindIP.trim().length() > 0){
    		s.address = forceBindIP;
    		System.setProperty("java.rmi.server.hostname", forceBindIP);       	
        }else if (!hostName.equals(s.address)){
            //If multiple IP address are existing in server environment, forced to bind the server given host name.
        	try{
        	InetAddress localhost = InetAddress.getLocalHost();
        	InetAddress[] allIps = InetAddress.getAllByName(localhost.getCanonicalHostName());       	
        	  if (allIps != null && allIps.length > 1) {
        	    for (int i = 0; i < allIps.length; i++) {        	    	
        	    	if (hostName.equals(allIps[i].getHostAddress())){
        	    		s.address = allIps[i].getHostAddress();
        	    		System.setProperty("java.rmi.server.hostname", hostName);
        	    		VWSLog.add("Binding the Document Storage Server to " + hostName);
        	    	}
        	    }
        	  }
        	}catch (Exception e) {
				// TODO: handle exception
			}
        }      
        if (args != null
				&& args.length > 0
				&& args[0].equalsIgnoreCase("shutdown"))
         {
            DSSLog.add("Stop DSS Server @" + hostName + 
                                                 "[" + s.address + "]");
             try
             {   
                ((DSS) Util.getServer(s)).exit();
                return;
             }
         catch(Exception e){}
         }
        
        int port = Util.createServer(s);
        if ( port != 0)
        {
            DSSLog.add("Started Document Storage Server @" + hostName + 
                                                 "[" + s.address + "]:" + port);

            try
            {
                ((DSS) Util.getServer(s)).init();
            }
            catch(Exception e){}            
            if (args == null  || (args != null && args[0].equalsIgnoreCase("start")) && DSSPreferences.isRestartDSS())
	            createRestartScheduler();
        }
        else
            DSSLog.err
                   ("Could not start Document Storage Server on port: " + port);
    }

    public static void createRestartScheduler(){
    	//System.out.println("createRestartScheduler");
    	java.util.Timer firstTimer = new java.util.Timer();		
	    String dailyFrequency  = DSSPreferences.getRestartDSSTime();		        
	    StringTokenizer stDailyFreq = null;
	    stDailyFreq = new StringTokenizer(dailyFrequency, ":");
	    //System.out.println("Next Restart Timing is "+ dailyFrequency);
	    Calendar calendar = Calendar.getInstance();
	    try{	
        if (stDailyFreq == null || stDailyFreq.countTokens() < 2) return;	    	
		if(stDailyFreq != null && stDailyFreq.countTokens() >= 2){
			int dd = 0;
			int mins = 0;
			try{
				dd = Integer.parseInt(stDailyFreq.nextToken());
				mins = Integer.parseInt(stDailyFreq.nextToken());

				if (dd > 0 && dd <= 24 &&
						mins >= 0 && mins <=59 ){
					calendar.set(Calendar.HOUR_OF_DAY, dd);
					calendar.set(Calendar.MINUTE, mins);
					calendar.set(Calendar.SECOND, 0);
				}else{
					return;
				}
			}catch (Exception e) {
			
				return;
				// TODO: handle exception
			}
		}else{
		    calendar.set(Calendar.HOUR_OF_DAY, 0);
		    calendar.set(Calendar.MINUTE, 0);
		    calendar.set(Calendar.SECOND, 0);
		}
	    }catch(Exception ex){
	    //	ex.printStackTrace();
	    }
	    Date startTime = calendar.getTime();
	    //System.out.println("Selected Time " + startTime.getTime());
	    long currentTime = new Date().getTime();
	    //System.out.println("Current Time " + currentTime);
	    //System.out.println("Time diff " + (currentTime > startTime.getTime()));
	    if (currentTime > startTime.getTime()){
	    	calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);	    	
	    	startTime = calendar.getTime();
	    }
	    //System.out.println("Scheduled " + startTime);
	    firstTimer.schedule(new ViewWiseDSS().new ScheduleProcess(), startTime, 1000 * 24 * 60 * 60);
    }
    
    
    class ScheduleProcess extends TimerTask{
    	public void restartDSS(){
    		//System.out.println("Document Storage Server restarted by scheduler");
    		try{
    			if (!DSSPreferences.isRestartDSS()) return;
    			String hostName = DSSPreferences.getHostName();
    			ServerSchema s = Util.getMySchema();
    			s.comport = DSSPreferences.getComPort();
    			s.dataport = DSSPreferences.getDataPort();
    			s.type = SERVER_DSS;
    			DSS ds = null;
    			try
    			{
    				ds = (DSS) Util.getServer(s);
    				if (ds == null) return;
    				ds.exit();
    			}
    			catch(Exception e){
    				//System.out.println("Ex 1");
    				//e.printStackTrace();
    			}
    			try
    			{
    		    	int port = Util.createServer(s);
    		    	//System.out.println("Creating server startDSS " + port);
    		        if ( port != 0)
    		        {
    		            DSSLog.add("Started Document Storage Server @" + hostName + 
    		                                                 "[" + s.address + "]:" + port);
    		            try
    		            {
    		            	ds = (DSS) Util.getServer(s);
    	    				if (ds == null) {
    	    					DSSLog.err("Could not get Document Storage Server : " + port);
    	    				}else{
    	    					ds.init();
    	    				}
    		            }
    		            catch(Exception e){}
    		        }
    		        else
    		            DSSLog.err
    		                   ("Could not start Document Storage Server on port: " + port);    				    				
    			}
    			catch(Exception e){

    			}

    		}catch (Exception e) {
    			// TODO: handle exception
    		}
    	}

    	public void run(){
    		restartDSS();
    	}
    }
}
