package com.computhink.dss.server;

import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.file.*;
/**
 * CV10 Enhancement onpremisestoAzureCopy copy/Move documents
 * @author madhavan.b
 *
 */
public  class OnpremisesAzureCopy {
	CloudStorageAccount storageAccount=null;
	CloudFileClient fileClient =null;
	CloudFile cloudFile=null;
	CloudFileShare share=null;
	CloudFileDirectory rootDir=null;
	CloudFileDirectory sampleDir =null;
	ListFileItem fileItem=null;
	boolean isMoved=false;
	public OnpremisesAzureCopy(){

	}
	public boolean onpremisesAzureCopyDocument(String sourceFile,String destinationFile,String azureUserName,String azurePassword,int docID,boolean delete,boolean withID) {
		try {
			String accountName="AccountName="+azureUserName+";";
			String password="AccountKey="+azurePassword;
			String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
			System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
			storageAccount = CloudStorageAccount.parse(storageConnectionString1);
			fileClient = storageAccount.createCloudFileClient();
			String pattern = Pattern.quote(System.getProperty("file.separator"));
			String destpath[]=destinationFile.split(pattern);
			for(int i=0;i<destpath.length;i++){
				if(i==2)
				{
					share = fileClient.getShareReference(destpath[i]);
					if(!share.exists())
						share.create();
				}
				else if(i==3)
				{
					sampleDir=	share.getRootDirectoryReference().getDirectoryReference(destpath[i]);
					if(!sampleDir.exists()){
						sampleDir.create();
					} 
					rootDir = sampleDir;
					sampleDir = null;
				}
				else if(i>=4)
				{
					if ((i+1) != destpath.length) {
						sampleDir =	rootDir.getDirectoryReference(destpath[i]);
						if(!sampleDir.exists()){
							sampleDir.create();
						}
						rootDir = sampleDir;
						sampleDir = null;
					}
					else if ((i+1) == destpath.length) {
						sampleDir =	rootDir.getDirectoryReference(destpath[i]);
						if(!sampleDir.exists()){
							sampleDir.create();
						}
						rootDir = sampleDir;
						sampleDir = null;
					}
				}
			}
			File sourceDocFile=new File(sourceFile);
			sampleDir=rootDir;
			DSSLog.dbg("samplDir.geturi onpremisesAzureCopyDocument:::::::"+sampleDir.getUri());
			if(!sampleDir.exists())
				sampleDir.create();
			ArrayList backupList=getAzureBackupList(sourceDocFile.getAbsolutePath());
			for(int i=0;i<backupList.size();i++){
				String backupListFilePath=backupList.get(i).toString();
				String backupDocName=backupListFilePath.substring(backupListFilePath.lastIndexOf("\\")+1,+backupListFilePath.length());
				cloudFile=sampleDir.getFileReference(backupDocName);
				cloudFile.uploadFromFile(backupListFilePath);
				if(delete==true){
					File completeSrcFile=new File(sourceDocFile.getAbsoluteFile()+"\\"+backupDocName);
					if(cloudFile.exists()&&completeSrcFile.exists()){
						completeSrcFile.delete();
						if(sourceDocFile.exists())
							sourceDocFile.delete();
						isMoved=true;
					}

				}

			}


		}

		catch (Exception e) {
			DSSLog.err("Error while copy/paste from onpremises to azure storage :"+e.getMessage());
		}
		return isMoved;
	}

	/**
	 * Enhancement :- CV10 Azure restore document to 
	 * Method added to get the list of backup file locations
	 * @param srcPath
	 * @return
	 */
	private ArrayList getAzureBackupList(String srcPath) {
		ArrayList fileList = new ArrayList();
		try
		{
			String modifedSrcPath = srcPath;
			File path = new File(modifedSrcPath);
			if (path.isDirectory())
			{
				File[] fa = path.listFiles();
				if (fa.length > 0)
					for (int i = 0; i < fa.length; i++)
					{
						fileList.add(fa[i].getPath());
					}
			} 
		}        
		catch(Exception e){return null;}
		return fileList;
	}
}


