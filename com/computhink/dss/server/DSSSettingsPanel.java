/*
 * DSSSettings.java
 *
 * Created on November 30, 2003, 4:24 PM
 */

package com.computhink.dss.server;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPasswordField;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.dss.image.Images;
import com.computhink.manager.VWPanel;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWS;
/**
 *
 * @author  Administrator
 */
public class DSSSettingsPanel extends VWPanel implements DSSConstants, ViewWiseErrors 
{
    private VWPanel pPorts, pProxy, pVWS, pStores;
    //Issue 511 
    private JLabel lComPort, lDataPort, lProxyHost, lProxyPort,
                            lVWSPort, lVWSHost, lRoom, lNote1, lNote2, lSvrHost, lDSSSvrName, lDSSSvrDisplay,lMinSize, lMB, lStorageType, lUserName, lPassword;
     // Issue 511                       
    private JTextField tComPort, tDataPort, tProxyHost, tProxyPort, 
                                                   tVWSPort, tVWSHost, tSvrHost, minSize,  tUserName;       
    private JPasswordField tPassword;
    
    private JComboBox comRooms, comStorageType;
    private JButton bOK, bCancel, bAdd, bRemove, bConnect;
    private JList lstStores;
    private JScrollPane spFolders;
    private JCheckBox cbProxy, cbLog;

    private VWS vws = null;
    private ServerSchema vwsSchema;
    private Vector vStores = new Vector();
    private Vector azureStore=new Vector();
    private String selectedRoom = "";
    private String selectedStorageType = "";
    
    private static final int width = 500;
    private static final int height = 550;
	private static ResourceManager resourceManager=null;
    // Issue 511
    ListSelectionModel listSelectionModel;
    //Issue 511
    public DSSSettingsPanel() 
    {
        super();
        new Images();
        initComponents();
        getCurrentSettings();
        switchStores(false);
    }
    private void initComponents() 
    {
    	resourceManager=ResourceManager.getDefaultManager();
        pPorts = new VWPanel();
        pProxy = new VWPanel();
        pVWS = new VWPanel();
        pStores = new VWPanel();

        tSvrHost = new JTextField();
        tComPort = new JTextField();
        tDataPort = new JTextField();
        tProxyHost = new JTextField();
        tProxyPort = new JTextField();
        tVWSHost = new JTextField();
        tVWSPort = new JTextField();
        tUserName = new JTextField();
        tPassword = new JPasswordField();
        minSize=new JTextField();
        comRooms = new JComboBox();
        comStorageType = new JComboBox();

        cbProxy = new JCheckBox();
        cbLog = new JCheckBox(resourceManager.getString("DSSChK.LogInfo"));
        spFolders = new JScrollPane();
        lstStores = new JList();
        
        bAdd = new JButton(resourceManager.getString("DSSBtn.Add"));
        bRemove = new JButton(resourceManager.getString("DSSBtn.Remove"));
        bOK = new JButton(resourceManager.getString("DSSBtn.Apply"));
        bCancel = new JButton(resourceManager.getString("DSSBtn.Cancel"));
        bConnect = new JButton(resourceManager.getString("DSSBtn.Connect"));
        lMinSize=new JLabel(resourceManager.getString("DSSLbl.MinSize"));
        lMB=new JLabel("MB");
        lProxyHost = new JLabel(resourceManager.getString("DSSLbl.ProxyHost"));
        lProxyPort = new JLabel(resourceManager.getString("DSSLbl.Port"));
        lSvrHost = new JLabel(resourceManager.getString("DSSLbl.SrvHost"));
        lVWSHost = new JLabel(resourceManager.getString("DSSLbl.VWSHost"));
        lVWSPort = new JLabel(resourceManager.getString("DSSLbl.VWSCommPort"));
        lComPort = new JLabel(resourceManager.getString("DSSLbl.CommPort"));
        lDataPort = new JLabel(resourceManager.getString("DSSLbl.DataPort"));
        lRoom = new JLabel(resourceManager.getString("DSSLbl.Room"));
        lStorageType = new JLabel(resourceManager.getString("DSSLbl.StorageType"));
        lUserName = new JLabel(resourceManager.getString("DSSLbl.UserName"));
        lPassword = new JLabel(resourceManager.getString("DSSLbl.Password"));
        lNote1 = new JLabel(resourceManager.getString("DSSLbl.Note1"));
        lNote2 = new JLabel(resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("DSSLbl.Note2"));
		// Issue 511
        lDSSSvrName = new JLabel(resourceManager.getString("DSSLbl.SvrName"));
		lDSSSvrDisplay = new JLabel("");
		 // Issue 511
        setLayout(null);
        //setResizable(false);
        
        cbProxy.setBackground(this.getBackground());
        cbLog.setBackground(this.getBackground());
        bAdd.setBackground(this.getBackground());
        bCancel.setBackground(this.getBackground());
        bConnect.setBackground(this.getBackground());
        bOK.setBackground(this.getBackground());
        bRemove.setBackground(this.getBackground());
        lstStores.setBackground(this.getBackground());
        
        pPorts.setLayout(null);
        pPorts.setBorder(new TitledBorder(resourceManager.getString("DSSTitleBorder.ServPorts")));
        pPorts.setFocusable(false); add(pPorts);
        pPorts.setBounds(20, 10, 470, 83);  //(20,10,470,100)
        
        pPorts.add(lSvrHost); lSvrHost.setBounds(20, 20, 120, 16);  //(30,30,120,16)
        pPorts.add(tSvrHost); tSvrHost.setBounds(130, 20, 100, 20); //(160,30,260,20)
        pPorts.add(lComPort); lComPort.setBounds(20, 50, 120, 16); //(30,60,120,16)
        pPorts.add(lDataPort); lDataPort.setBounds(230, 50, 60, 16); //(300,60,60,16)
        pPorts.add(tComPort); tComPort.setBounds(130, 50, 50, 20);   //(160,60,60,20)
        pPorts.add(tDataPort); tDataPort.setBounds(300, 50, 50, 20);  //(370,60,50,20)
        
        pProxy.setLayout(null);
        pProxy.setBorder(new TitledBorder("         "+resourceManager.getString("DSSTitleBorder.HttpProxy")));
        pProxy.setFocusable(false); add(pProxy);
        pProxy.setBounds(20, 97, 470, 53);  //(20, 115, 470, 70);
        pProxy.add(lProxyHost); lProxyHost.setBounds(20, 20, 70, 16);  //(30, 30, 70, 16);
        pProxy.add(lProxyPort); lProxyPort.setBounds(230, 20, 30, 16);  //(300, 30, 30, 16);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 21);
        pProxy.add(tProxyHost); tProxyHost.setBounds(80, 20, 110, 20);  //(110, 30, 110, 20);
        pProxy.add(tProxyPort);tProxyPort.setBounds(300, 20, 50, 20);  //(370, 30, 50, 20);

        pVWS.setLayout(null);
        pVWS.setBorder(new TitledBorder(resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("DSSTitleBorder.Server")));
        pVWS.setFocusable(false); add(pVWS);
        pVWS.setBounds(20, 155, 470, 53);  //(20, 190, 470, 70); 
        pVWS.add(lVWSHost); lVWSHost.setBounds(20, 20, 70, 16);  //(30, 30, 70, 16);
        pVWS.add(lVWSPort); lVWSPort.setBounds(200, 20, 120, 16);  //(240, 30, 120, 16);
        pVWS.add(tVWSHost); tVWSHost.setBounds(80, 20, 110, 20);  //(80, 30, 110, 20);
        pVWS.add(tVWSPort); tVWSPort.setBounds(300, 20, 50, 20);  //(370, 30, 50, 20);

        pStores.setLayout(null);
        pStores.setBorder(new TitledBorder(resourceManager.getString("DSSTitleBorder.DocStores")));
        pStores.setFocusable(false); add(pStores);
        // Start of fix 511
        pStores.setBounds(20, 210, 470, 225);  //(20, 265, 470, 180);           
		listSelectionModel = lstStores.getSelectionModel();
        listSelectionModel.addListSelectionListener(
                new ListSelectionHandler()); 
        // End of fix 511
        spFolders.setViewportView(lstStores);
        pStores.add(lRoom); lRoom.setBounds(20, 20, 60, 16);  //(30, 30, 60, 16);
        pStores.add(comRooms); comRooms.setBounds(80, 20, 110, 20);        //(80, 30, 110, 20);

        pStores.add(lStorageType); lStorageType.setBounds(20, 55, 60, 16);  //(30, 30, 60, 16);
        pStores.add(comStorageType); comStorageType.setBounds(80, 55, 110, 20);        //(80, 30, 110, 20);
        
        comStorageType.addItem("On Premises");
        comStorageType.addItem("Azure Storage");
       // comStorageType.addItem("Google Storage");
        //comStorageType.addItem("Amazon Storage");
        comStorageType.setSelectedItem("On Premises");
        
        pStores.add(lUserName); lUserName.setBounds(20, 85, 70, 16);  //(30, 30, 70, 16);
        pStores.add(lPassword); lPassword.setBounds(215, 85, 120, 16);  //(240, 30, 120, 16);
        pStores.add(tUserName); tUserName.setBounds(80, 85, 110, 20);  //(80, 30, 110, 20);
        pStores.add(tPassword); tPassword.setBounds(300, 85, 150, 20);  //(370, 30, 50, 20);
        
        if (comStorageType.getSelectedItem().equals("On Premises")) {
        	tUserName.setEnabled(false);
        	tPassword.setEnabled(false);
        } else {
        	tUserName.setEnabled(true);
        	tPassword.setEnabled(true);
        }
              
        pStores.add(spFolders); spFolders.setBounds(20, 115, 430, 65);  //(30, 60, 390, 65);
        comRooms.setBackground(Color.white);
        // Start of fix - 511
        pStores.add(lDSSSvrName); lDSSSvrName.setBounds(215, 20, 70, 20);  //(200, 30, 70, 20);
        pStores.add(lDSSSvrDisplay); lDSSSvrDisplay .setBounds(300, 20, 160, 20); //(265, 30, 150, 20);
        lDSSSvrName.setBackground(new Color(51, 51, 51));
        lDSSSvrDisplay.setBackground(new Color(51, 51, 51));
        spFolders.setBackground(getBackground());
		lDSSSvrName.setVisible(false);
		lDSSSvrDisplay.setVisible(false);		
        pStores.add(bAdd); bAdd.setBounds(240, 190, 90, 23);  //(220, 135, 90, 23);
        pStores.add(bRemove); bRemove.setBounds(360, 190, 90, 23);  //(330, 135, 90, 23);
        pStores.add(bConnect); bConnect.setBounds(20, 190, 140, 23);  //(30, 135, 140, 23);
        // End of fix Issue 511
        lstStores.setBackground(new Color(255, 255, 204));
        //Start of fix Issue 511
        add(bOK); bOK.setBounds(400, 455, 90, 23);
        //add(bCancel); bCancel.setBounds(349,455, 90, 26);
        add(cbLog); cbLog.setBounds(20, 455, 120, 20);
        add(lMinSize); lMinSize.setBounds(140,455,130, 20);
        add(minSize); minSize.setBounds(240,455,60, 20);
        add(lMB); lMB.setBounds(311,455,25,20);
        if(minSize.getText().equals("")){
     	   minSize.setText(Long.toString(DSSPreferences.getStorageSpace()));
        }
        // End of fix 511
        bOK.setToolTipText(lNote1.getText());
        bConnect.setToolTipText(lNote2.getText());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(width, height));
        setLocation((screenSize.width - width)/2,(screenSize.height -height)/2);
        //setIconImage(Images.app.getImage());
        //-------------Listeners------------------------------------------------
/*        addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });
*/        bOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                //closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
        bAdd.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                addStore();
            }
        });
        bRemove.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                removeStore();
            }
        });
        bConnect.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                ConnecttoVWS();
            }
        });
        comRooms.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if (comRooms.getSelectedIndex() >= 0)
                {
                    selectedRoom = (String) comRooms.getSelectedItem();
                    refreshStores();
                    try {
                    	azureStore.removeAllElements();
                    	getAzureStores(azureStore);
                    	if(azureStore!=null&&azureStore.size()>0){
                    		String storageArray[]=azureStore.get(0).toString().split(Util.SepChar);
                    		String storageType=storageArray[7];
                    		String storageUserName=storageArray[8];
                    		String storagePassword=storageArray[9];
                    		if(storageType.length()>0){
                    			comStorageType.setSelectedItem(storageType);	
                    		}
                    		if(storageUserName.length()>0&&(!storageUserName.equals("-"))){
                    			tUserName.setText(storageUserName);

                    		}else if(storageUserName.equals("-")){
                    			tUserName.setText("");
                    		}
                    		if(storagePassword.length()>0&&(!storagePassword.equals("-"))){
                    			tPassword.setText(storagePassword);

                    		}else if(storagePassword.equals("-")){
                    			tPassword.setText("");
                    		}
                    	}else{
                      	   	tUserName.setText("");
                          	tPassword.setText("");
                            comStorageType.setSelectedItem("On Premises");
                      	}
                    } catch (Exception ex) {
            			// TODO Auto-generated catch block
                    	ex.printStackTrace();
            		}
                  
                }
            }
        });
        comStorageType.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if (comStorageType.getSelectedIndex() >= 0)
                {
                    selectedStorageType = (String) comStorageType.getSelectedItem();
                    if (comStorageType.getSelectedItem().equals("On Premises")) {
                    	tUserName.setEnabled(false);
                    	tPassword.setEnabled(false);
                    } else {
                    	tUserName.setEnabled(true);
                    	tPassword.setEnabled(true);
                    }
                    //refreshStores();
                }
            }
        });
    }
    private boolean ConnecttoVWS()
    {
        applyVWSSettings();
        vws = (VWS) Util.getServer(vwsSchema);
        if (vws != null)
        {
            switchStores(true);
            populateRooms();
            refreshStores();
            return true;
        }
        else
        {
            switchStores(false);
            return false;
        }
    }
    private void applyVWSSettings()
    {
    	try{
    		vwsSchema.address = tVWSHost.getText();
    		vwsSchema.comport = Util.to_Number(tVWSPort.getText());
    		vwsSchema.name=tUserName.getText();
    		vwsSchema.password=tPassword.getText();
    		vwsSchema.storageType=comStorageType.getSelectedItem().toString();
    		DSSPreferences.setVWS(vwsSchema);
    		 String dssPath= (String) lstStores.getSelectedValue();
    		if(dssPath.equals(" ")){
    			Util.Msg(this, resourceManager.getString("Storage_Select.MSG"),resourceManager.getString("Storage_Select.Title"));	
    			return;
    		}
    		  String storageId=getStorageId(dssPath);
    		  int storagIdVal=Integer.parseInt(storageId);
    		  if(storagIdVal>0){
    			  String storageValues=getStorageFromId(storageId);
    			  String storageUserName=tUserName.getText();
    			  String storagePassword=tPassword.getText();
    			  vws.updateAzureStorageInfo(selectedRoom,storageValues,storageUserName,storagePassword);
    		  }
    		
    	}catch(Exception e){
    		
    	}
    }
    private void switchStores(boolean b)
    {
        bAdd.setEnabled(b);
        bRemove.setEnabled(b);
        bConnect.setEnabled(!b);
    }
    private void removeStore()
    {
        String store = (String) lstStores.getSelectedValue();
        if (store != null)
        {
             if (!Util.Ask((Window)this.getParent(), resourceManager.getString("DSSMSG.RemovePath1")+ " '"+ store + "'?",resourceManager.getString("DSSMSG.RemovePath2"))) 
                                                                        return;
             if (removeStoreFromServer(store))
             {
                 refreshStores();
             }
        }
    }
    private void addStore()
    {
        if (selectedRoom.equals("")) return;
        String store = getFolder();
        if (store != null)
        {
            if (storeExists(store)) 
            {
                Util.Msg((Window)this.getParent(), resourceManager.getString("DSSMSG.PathReg"), SETTINGS);
                return;
            }
            if (storeRoot(store))
            {
                Util.Msg((Window)this.getParent(),resourceManager.getString("DSSMSG.RootPath"), SETTINGS);
                return;
            }
            if (addStoreToServer(store))  
            {
                refreshStores();
            }
        }
    }
    private ServerSchema createStoreSchema(String store)
    {
        ServerSchema mySchema = Util.getMySchema();
        mySchema.comport = DSSPreferences.getComPort();
        mySchema.path = store;
        mySchema.room = selectedRoom;
        mySchema.storageType=comStorageType.getSelectedItem().toString();
        mySchema.userName=tUserName.getText();
        mySchema.password=tPassword.getText();
       /* mySchema.storageType=DSSPreferences.getDSSStorageType();
        mySchema.userName=DSSPreferences.getDSSUserName();
        mySchema.password=DSSPreferences.getDSSPassword();*/
        return mySchema;
    }
    
    private void getAzureStores(Vector azureStore)
    {
        try
        {
            Vector ret = vws.getAzureStore(createStoreSchema(""),azureStore);
            azureStore.addAll(ret);
        }
        catch(Exception e){}
    }
	
    private void getDSSStores(Vector stores)
    {
        try
        {
            Vector ret = vws.getDSSStores(createStoreSchema(""), stores);
            stores.addAll(ret);
        }
        catch(Exception e){}
    }
    private void populateRooms()
    {
        try
        {
            Vector ret = vws.getRoomNames();
            for (int i = 0; i < ret.size(); i++)
            {
                comRooms.addItem(ret.get(i));
                comRooms.setSelectedIndex(i);
            }
        }
        catch(Exception e){}
    }
    private boolean addStoreToServer(String store)
    {
        try
        {
            if ( vws.addDSSStore(createStoreSchema(store)) > 0) return true;
        }
        catch(Exception e){switchStores(false);}
        return false;
    }
    private boolean removeStoreFromServer(String store)
    {
        try
        {
            if ( vws.removeDSSStore(createStoreSchema(store)) > 0) return true;
        }
        catch(Exception e){switchStores(false);}
        return false;
    }
    private boolean storeExists(String path)
    {
         if (vStores.contains(path))
           return true;
        else
           return false;
    }
    private boolean storeRoot(String path)
    {
        if (new File(path).getParent() == null)
            return true;
        else
            return false;
    }
    private void refreshStores()
    {
        vStores.removeAllElements();
        getDSSStores(vStores);  // = DSSPreferences.getStores();
        lstStores.setListData(vStores);
        lstStores.setSelectedIndex(vStores.size()-1);
    }
    private void setSettings()
    {
        DSSPreferences.setHostName(tSvrHost.getText());
        DSSPreferences.setComPort(Integer.parseInt(tComPort.getText()));
        DSSPreferences.setDataPort(Integer.parseInt(tDataPort.getText()));
        DSSPreferences.setProxy(cbProxy.getSelectedObjects() != null? true : false);
        DSSPreferences.setProxyHost(tProxyHost.getText());
      /*  DSSPreferences.setDSSStorageType(comStorageType.getSelectedItem().toString());
        DSSPreferences.setDSSUserName(tUserName.getText());
        DSSPreferences.setDSSPassword(tPassword.getText());*/
        DSSPreferences.setStorageSpace(Long.parseLong(minSize.getText()));
        String pPort = tProxyPort.getText();
        if (pPort.equals(""))
            DSSPreferences.setProxyPort(0);
        else
            DSSPreferences.setProxyPort(Integer.parseInt(pPort));
        applyVWSSettings();
        DSSPreferences.setLogInfo(cbLog.isSelected());
    }
    private void getCurrentSettings()
    {
        tSvrHost.setText(DSSPreferences.getHostName());
        tComPort.setText(String.valueOf(DSSPreferences.getComPort()));
        tDataPort.setText(String.valueOf(DSSPreferences.getDataPort()));
        cbProxy.setSelected(DSSPreferences.getProxy());
        tProxyHost.setText(DSSPreferences.getProxyHost());
       /* tUserName.setText(DSSPreferences.getDSSUserName());
        tPassword.setText(DSSPreferences.getDSSPassword());
        comStorageType.setSelectedItem(DSSPreferences.getDSSStorageType());*/
        int pPort = DSSPreferences.getProxyPort();
        if (pPort > 0)
            tProxyPort.setText(String.valueOf(pPort));
        else
            tProxyPort.setText("");
        vwsSchema = DSSPreferences.getVWS();
        tVWSHost.setText(vwsSchema.address);
        int vPort = vwsSchema.comport;
        if (vPort > 0)
            tVWSPort.setText(String.valueOf(vPort));
        else
            tVWSPort.setText("");
        cbLog.setSelected(DSSPreferences.getLogInfo());
        cbProxyStateChanged();
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    private void closeDialog() 
    {
        System.exit(0);
    }
    private String getFolder()
    {
    	File folder = null;
        JFileChooser fc = new JFileChooser(new File("c:\\"));
        fc.setFileSelectionMode(fc.DIRECTORIES_ONLY);
        int returnVal = fc.showDialog(this, "select");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	folder = fc.getSelectedFile();
        }
        if (folder != null)
            return folder.getPath();
        else
            return null;
    }
    //Start of fix Issue 511 
    private String getDSSIPPort(String location)
    {
        try
        {        	
        	Vector ret = vws.getDSSIPPort(createStoreSchema(""), location);
            return ret.get(0).toString();
        }
        catch(Exception e){
        	DSSLog.add("Error occured in getDSSIPPort");
        	}
        return "";
    }
    //CV10 Issue Fix added to get the storage id from database 
    private String getStorageId(String location)
    {
    	try
    	{        	
    		Vector ret = vws.getStorageId(createStoreSchema(""), location);
    		DSSLog.dbg("Return vector from getStorageId :"+ret);
    		return ret.get(0).toString();
    	}
    	catch(Exception e){
    		DSSLog.err("Error occured in getDSSIPPort");
    	}
    	return "";
    }
    private String getStorageFromId(String storageId)
    {
        try
        {        	
        	Vector ret = vws.getStorageFromId(createStoreSchema(""), storageId);
        	DSSLog.dbg("Return vector from  getStorageFromId :"+ret);
        	if(ret!=null &&ret.size()>0)
            return ret.get(0).toString();
        }
        catch(Exception e){
        	DSSLog.err("Error occured in getStorageFromId");
        	}
		return "";
       
    }
    //End of fix 511 
    public static void main(String args[]) 
    {
    	try{
            String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
    		UIManager.setLookAndFeel(plasticLookandFeel);
        	}catch(Exception ex){}    	
    	/*
    	 * new DSSSettings().show(); method is replaced with new DSSSettings().setVisible(true); 
    	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
    	 */
        new DSSSettings().setVisible(true);
    }

  //Start of fix Issue 511 
    class ListSelectionHandler implements ListSelectionListener {
    	public void valueChanged(ListSelectionEvent e) { 
    		  String location =(String)lstStores.getSelectedValue();
    		  if(location != null){
    		  String IPPort = getDSSIPPort(location);
    		  
    		  //CV10 Enhancment condition added to reload the azure settings based on selected locaion
    		  String storageId=getStorageId(location);
      		  int storagIdVal=Integer.parseInt(storageId);
      		  if(storagIdVal>0){
      			  String storageValues=getStorageFromId(storageId);
      			  String storageArray[]=storageValues.split(Util.SepChar);
      			  if(storageArray[7].equals("Azure Storage")){
      				  comStorageType.setSelectedItem("Azure Storage");
      			  }else if(storageArray[7].equals("On Premises")||storageArray[7].equals("-")){
      				  comStorageType.setSelectedItem("On Premises");
      			  }
      			  if(storageArray[8].equals("-")){
      				  tUserName.setText("");
      			  }else{
      				  tUserName.setText(storageArray[8]);
      			  }
      			  if(storageArray[9].equals("-")){
      				  tPassword.setText("");
      			  }else{
      				  tPassword.setText(storageArray[9]);
      			  }
      		  }    		  
    		  if(!IPPort.equalsIgnoreCase("")){
	    	      lDSSSvrDisplay.setText(IPPort.substring(0,IPPort.indexOf(":"))+" : "+IPPort.substring(IPPort.indexOf(":")+1,IPPort.length()));
    		  	  lDSSSvrName.setVisible(true);
	  	  		  lDSSSvrDisplay.setVisible(true); 
    		  	}
    		  } 
    		  else{
    		  	lDSSSvrName.setVisible(false);
    	  		lDSSSvrDisplay.setVisible(false);  		  	
    	  	      }    	  		 
    	} 
    }    	
    //End of fix 511 
}
