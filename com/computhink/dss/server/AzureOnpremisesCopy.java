package com.computhink.dss.server;

import java.io.File;
import java.util.regex.Pattern;

import com.microsoft.azure.storage.*;
import com.microsoft.azure.storage.file.*;
/**
 *  CV10 Enhancement AzuretoOnpremisesCopy copy/Move documents
 * @author madhavan.b
 *
 */
public  class AzureOnpremisesCopy {
	CloudStorageAccount storageAccount=null;
	CloudFileClient fileClient =null;
	CloudFile cloudFile=null;
	CloudFileShare share=null;
	CloudFileDirectory rootDir=null;
	CloudFileDirectory sampleDir =null;
	ListFileItem fileItem=null;
	boolean isMoved=false;
	public AzureOnpremisesCopy(){

	}
	public boolean azureOnpremisesDocument(String sourceFile,String destinationFile,String azureUserName,String azurePassword,int docID,boolean delete,boolean withID) {
		try {
			String accountName="AccountName="+azureUserName+";";
			String password="AccountKey="+azurePassword;
			String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
			System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
			storageAccount = CloudStorageAccount.parse(storageConnectionString1);
			fileClient = storageAccount.createCloudFileClient();
			String pattern = Pattern.quote(System.getProperty("file.separator"));
			String sourcepath[]=sourceFile.split(pattern);
			String destpath[]=destinationFile.split(pattern);
			for(int i=0;i<sourcepath.length-1;i++){
				if(i==2)
				{
					share = fileClient.getShareReference(sourcepath[i]);
					if(!share.exists())
						share.create();
				}
				else if(i==3)
				{
					sampleDir=	share.getRootDirectoryReference().getDirectoryReference(sourcepath[i]);
					if(!sampleDir.exists()){
						sampleDir.create();

					} 
					rootDir = sampleDir;
					sampleDir = null;

				}
				else if(i>=4)
				{
					if ((i+1) != sourcepath.length) {
						sampleDir =	rootDir.getDirectoryReference(sourcepath[i]);
						if(!sampleDir.exists()){
							sampleDir.create();
						}
						rootDir = sampleDir;
						sampleDir = null;
					}
					else if ((i+1) == sourcepath.length) {
						sampleDir =	rootDir.getDirectoryReference(sourcepath[i]);
						if(!sampleDir.exists()){
							sampleDir.create();
						}
						rootDir = sampleDir;

						sampleDir = null;
					}
				}

			}
			String srcReference=sourceFile.substring(sourceFile.lastIndexOf(File.separator)+1,sourceFile.length());
			sampleDir=rootDir.getDirectoryReference(srcReference);
			DSSLog.dbg("sampleDirectory Uri while copying from azure to onpremises"+sampleDir.getUri());
			File destFile=new File(destinationFile);
			if(!destFile.exists()){
				destFile.mkdir();
			}

			for ( ListFileItem fileItem1 : sampleDir.listFilesAndDirectories() ) {
				String filesURI=fileItem1.getUri().toString();
				String fileName=filesURI.substring(filesURI.lastIndexOf("/")+1, filesURI.length());
				CloudFile cloudFile1=sampleDir.getFileReference(fileName);
				if(cloudFile1.exists()){
					cloudFile1.downloadToFile(destinationFile+"\\"+fileName);
					if(delete==true){
						cloudFile1.delete();
					}
				}
			}
			if(delete==true){
				if(sampleDir.exists())
					sampleDir.delete();
				isMoved=true;
			}
		}
		catch (Exception e) {
			DSSLog.err("Error while copy/paste from azure to onpremises storage :"+e.getMessage());
		}
		return isMoved;
	}

}


