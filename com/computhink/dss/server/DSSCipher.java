package com.computhink.dss.server;


import java.security.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.io.*;

public final class DSSCipher
{
    public static final int encrypt = Cipher.ENCRYPT_MODE;
    public static final int decrypt = Cipher.DECRYPT_MODE;
    
    private static final String provider = "SunJCE";
    private static final String alg = "Blowfish";
    private static final String mode = "OFB";
    private static final String pad = "PKCS5Padding";
    private static final String extendedAlg = alg + "/" + mode + "/" + pad;
    private static final String marker = "4223DAAB-D393-11D0-9A76-00C05FB68BF7";
    private static final int bSize = 80;

    private Cipher cipher;
    private String key;

    public DSSCipher(String key)
    {
       this.key = key;
    }
    private boolean cipherInit(int cipherMode)
    {
        boolean init = false;
        try
        {
            cipher = Cipher.getInstance(alg, provider);
            cipher.init(cipherMode, new SecretKeySpec(key.getBytes(), alg));
            init = true;
        }
        catch (Exception e){ DSSLog.err(e.getMessage()); }
        return init;
    }
    public byte[] cipher(byte[] b, int mode)
    {
        byte[] ciphered = null;
        
        if (!cipherInit(mode)) return null;
        try
        {
            ciphered = cipher.doFinal(b);
        }
        catch (Exception e) { DSSLog.err(e.getMessage());} 
        return ciphered;
    }
    public boolean encryptFile(String fileName)
    {
        RandomAccessFile raf = null;
        File file = new File(fileName);
        if (isEncrypted(file)) return true;
        int size = (int) file.length();
        try 
        {
            byte[] buffer = new byte[bSize];
            raf = new RandomAccessFile(file, "rwd");
            raf.seek(size-bSize);
            raf.read(buffer);
            if ((buffer = cipher(buffer, encrypt)) == null)
            {
                raf.close();
                return false;
            }
            raf.seek(size - bSize);
            raf.write(buffer);
            raf.write(marker.getBytes());
            byte b = new Integer(buffer.length - bSize).byteValue();
            raf.write(b); 
            raf.close();
            return true;
        }
        catch (Exception e) { DSSLog.err(e.getMessage());} 
        finally{
        	try{
        	if (raf != null){
        		raf.close();
        		raf = null;
        	}
        	}catch (Exception e) {
				// TODO: handle exception
			}
        }
        return false;
    }
    public static boolean isEncrypted(File file)
    {
	String dstFile = file.getAbsolutePath();
	if(dstFile.indexOf("\\")!=-1  && dstFile.indexOf("/")!=-1 ){
		String newdstFile = "";
		try{
			newdstFile = dstFile;
			/* temp.replaceAll("\\", "/"); This replaceAll will not work and throws PatternSyntaxException  
			* To resolve this problem is it must be temp.replaceAll("\\\\","/"); 
			*/
			if((File.separatorChar == '/')&& newdstFile.indexOf("\\")!=-1){
				newdstFile = newdstFile.replaceAll("\\\\", "/");
			}
			if((File.separatorChar == '\\')&& newdstFile.indexOf("/")!=-1){
				newdstFile = newdstFile.replaceAll("/", "\\\\");
			}
			dstFile = newdstFile;
		}catch(Exception ex){
			//DSSLog.err("Error in writeContents - "+ex.toString());
		}
	}
	file = new File(dstFile);
        long size = file.length();
        byte[] buffer = new byte[marker.length()];
        RandomAccessFile raf = null;
        try
        {
        	raf = new RandomAccessFile(file,"r");
            raf.seek(size - 37);
            raf.read(buffer);
            raf.close();
        }
        catch (Exception e) { DSSLog.err(e.getMessage());}
        finally{
        	try{
        		if (raf != null){
        			raf.close();
        			raf = null;
        		}
        	}catch (Exception e) {
        		// TODO: handle exception
        	}
        }
        return (marker.equals(new String(buffer)) ? true : false);
    }
    public boolean decryptFile(String fileName)
    {
        RandomAccessFile raf = null;
        File file = new File(fileName);
                    if (!isEncrypted(file)) return true;
        int size = (int) file.length();
        try
        {
            raf = new RandomAccessFile(file,"rwd");
            raf.seek(size-1);
            int delta = raf.read();
            byte[] buffer = new byte[bSize + delta];
            int encryptedBlock = size - (buffer.length + /* marker + delta*/ 37);
            raf.seek(encryptedBlock);
            raf.read(buffer);
            if ( (buffer = cipher(buffer, decrypt)) == null) 
            {
                raf.close();
                raf = null;
                return false;
            }
            raf.seek(encryptedBlock);
            raf.write(buffer);
            raf.setLength(encryptedBlock + buffer.length);
            raf.close();
            return true;
        }
        catch (Exception e) { DSSLog.err(e.getMessage());} 
        finally{
        	try{
        	if (raf != null){
        		raf.close();
        		raf = null;
        	}
        	}catch (Exception e) {
				// TODO: handle exception
			}
        }
        
        return true;
    }
}




