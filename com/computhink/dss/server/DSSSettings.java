/*
 * DSSSettings.java
 *
 * Created on November 30, 2003, 4:24 PM
 */

package com.computhink.dss.server;
import com.computhink.common.*;
import com.computhink.dss.image.*;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.*;

import javax.swing.*;
import java.awt.*;
import java.awt.Toolkit.*;
import java.awt.event.*;
import javax.swing.event.*;
import javax.swing.border.*;
import java.rmi.*;
import java.io.*;
import java.util.*;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;
/**
 *
 * @author  Administrator
 */
public class DSSSettings extends JFrame implements DSSConstants, ViewWiseErrors 
{
    private JPanel pPorts, pProxy, pVWS, pStores;
    //Issue 511 
    private JLabel lComPort, lDataPort, lProxyHost, lProxyPort,
                            lVWSPort, lVWSHost, lRoom, lNote1, lNote2, lSvrHost, lDSSSvrName, lDSSSvrDisplay;
     // Issue 511                       
    private JTextField tComPort, tDataPort, tProxyHost, tProxyPort, 
                                                   tVWSPort, tVWSHost, tSvrHost;                                                
    private JComboBox comRooms;
    private JButton bOK, bCancel, bAdd, bRemove, bConnect;
    private JList lstStores;
    private JScrollPane spFolders;
    private JCheckBox cbProxy, cbLog;
    private ResourceManager resourceManager=null;
    private VWS vws = null;
    private ServerSchema vwsSchema;
    private Vector vStores = new Vector();
    private String selectedRoom = "";
    
    private static final int width = 500;
    private static final int height = 550;
    // Issue 511
    ListSelectionModel listSelectionModel;
    //Issue 511
    public DSSSettings() 
    {
        super(SETTINGS);
        new Images();
        initComponents();
        getCurrentSettings();
        switchStores(false);
    }
    private void initComponents() 
    {   
    	resourceManager=ResourceManager.getDefaultManager();
        pPorts = new JPanel();
        pProxy = new JPanel();
        pVWS = new JPanel();
        pStores = new JPanel();

        tSvrHost = new JTextField();
        tComPort = new JTextField();
        tDataPort = new JTextField();
        tProxyHost = new JTextField();
        tProxyPort = new JTextField();
        tVWSHost = new JTextField();
        tVWSPort = new JTextField();
        comRooms = new JComboBox();

        cbProxy = new JCheckBox();
        cbLog = new JCheckBox(resourceManager.getString("DSSSettingsChK.LogInfo"));
        spFolders = new JScrollPane();
        lstStores = new JList();
        
        bAdd = new JButton(resourceManager.getString("DSSSettingsBtn.Add"));
        bRemove = new JButton(resourceManager.getString("DSSSettingsBtn.Remove"));
        bOK = new JButton(resourceManager.getString("DSSSettingsBtn.OK"));
        bCancel = new JButton(resourceManager.getString("DSSSettingsBtn.Cancel"));
        bConnect = new JButton(resourceManager.getString("DSSSettingsBtn.Connect"));
        
        lProxyHost = new JLabel(resourceManager.getString("DSSSettingsLbl.ProxyHost"));
        lProxyPort = new JLabel(resourceManager.getString("DSSSettingsLbl.Port:"));
        lSvrHost = new JLabel(resourceManager.getString("DSSSettingsLbl.SrvHost"));
        lVWSHost = new JLabel(resourceManager.getString("DSSSettingsLbl.VWSHost"));
        lVWSPort = new JLabel(resourceManager.getString("DSSSettingsLbl.VWSCommPort"));
        lComPort = new JLabel(resourceManager.getString("DSSSettingsLbl.CommPort"));
        lDataPort = new JLabel(resourceManager.getString("DSSSettingsLbl.DataPort"));
        lRoom = new JLabel(resourceManager.getString("DSSSettingsLbl.Room"));
        lNote1 = new JLabel(resourceManager.getString("DSSSettingsLbl.Note1"));
        lNote2 = new JLabel(resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("DSSSettingsLbl.Note2"));
		// Issue 511
        lDSSSvrName = new JLabel(resourceManager.getString("DSSSettingsLbl.SvrName"));
		lDSSSvrDisplay = new JLabel("");
		 // Issue 511
        getContentPane().setLayout(null);
        setResizable(false);
        
        pPorts.setLayout(null);
        pPorts.setBorder(new TitledBorder(resourceManager.getString("DSSSettingsTitleBorder.ServPorts")));
        pPorts.setFocusable(false); getContentPane().add(pPorts);
        pPorts.setBounds(20, 10, 450, 100);
        
        pPorts.add(lSvrHost); lSvrHost.setBounds(30, 30, 120, 16);
        pPorts.add(tSvrHost); tSvrHost.setBounds(160, 30, 260, 20);
        pPorts.add(lComPort); lComPort.setBounds(30, 60, 120, 16);
        pPorts.add(lDataPort); lDataPort.setBounds(300, 60, 60, 16);
        pPorts.add(tComPort); tComPort.setBounds(160, 60, 50, 20);
        pPorts.add(tDataPort); tDataPort.setBounds(370, 60, 50, 20);
        
        pProxy.setLayout(null);
        pProxy.setBorder(new TitledBorder("         "+resourceManager.getString("DSSTitleBorder.HttpProxy")));
        pProxy.setFocusable(false); getContentPane().add(pProxy);
        pProxy.setBounds(20, 115, 450, 70);
        pProxy.add(lProxyHost); lProxyHost.setBounds(30, 30, 70, 16);
        pProxy.add(lProxyPort); lProxyPort.setBounds(300, 30, 30, 16);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 21);
        pProxy.add(tProxyHost); tProxyHost.setBounds(110, 30, 110, 20);
        pProxy.add(tProxyPort);tProxyPort.setBounds(370, 30, 50, 20);

        pVWS.setLayout(null);
        pVWS.setBorder(new TitledBorder(resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("DSSSettingsTitleBorder.Server")));
        pVWS.setFocusable(false); getContentPane().add(pVWS);
        pVWS.setBounds(20, 190, 450, 70);
        pVWS.add(lVWSHost); lVWSHost.setBounds(30, 30, 70, 16);
        pVWS.add(lVWSPort); lVWSPort.setBounds(240, 30, 120, 16);
        pVWS.add(tVWSHost); tVWSHost.setBounds(80, 30, 110, 20);
        pVWS.add(tVWSPort); tVWSPort.setBounds(370, 30, 50, 20);

        pStores.setLayout(null);
        pStores.setBorder(new TitledBorder(resourceManager.getString("DSSSettingsTitleBorder.DocStores")));
        pStores.setFocusable(false); getContentPane().add(pStores);
        // Start of fix 511
        pStores.setBounds(20, 265, 450, 200);       
		listSelectionModel = lstStores.getSelectionModel();
        listSelectionModel.addListSelectionListener(
                new ListSelectionHandler()); 
        // End of fix 511
        spFolders.setViewportView(lstStores);
        pStores.add(lRoom); lRoom.setBounds(30, 30, 60, 16);
        pStores.add(comRooms); comRooms.setBounds(80, 30, 110, 20);        
        pStores.add(spFolders); spFolders.setBounds(30, 60, 390, 65);
        // Start of fix - 511
        pStores.add(lDSSSvrName); lDSSSvrName.setBounds(30, 135, 70, 20);
        pStores.add(lDSSSvrDisplay); lDSSSvrDisplay .setBounds(95, 135, 150, 20);
       
		lDSSSvrName.setVisible(false);
		lDSSSvrDisplay.setVisible(false);		
        pStores.add(bAdd); bAdd.setBounds(220, 160, 90, 26);
        pStores.add(bRemove); bRemove.setBounds(330, 160, 90, 26);
        pStores.add(bConnect); bConnect.setBounds(30, 160, 140, 26);
        // End of fix Issue 511
        lstStores.setBackground(new Color(255, 255, 204));
        //Start of fix Issue 511
        getContentPane().add(bOK); bOK.setBounds(239, 475, 90, 26);
        getContentPane().add(bCancel); bCancel.setBounds(349,475, 90, 26);
        getContentPane().add(cbLog); cbLog.setBounds(20, 475, 200, 20);
        // End of fix 511
        bOK.setToolTipText(lNote1.getText());
        bConnect.setToolTipText(lNote2.getText());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(width, height));
        setLocation((screenSize.width - width)/2,(screenSize.height -height)/2);
        setIconImage(Images.app.getImage());
        //-------------Listeners------------------------------------------------
        addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });
        bOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
        bAdd.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                addStore();
            }
        });
        bRemove.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                removeStore();
            }
        });
        bConnect.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                ConnecttoVWS();
            }
        });
        comRooms.addItemListener(new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                if (comRooms.getSelectedIndex() >= 0)
                {
                    selectedRoom = (String) comRooms.getSelectedItem();
                    refreshStores();
                }
            }
        });
    }
    private boolean ConnecttoVWS()
    {
        applyVWSSettings();
        vws = (VWS) Util.getServer(vwsSchema);
        if (vws != null)
        {
            switchStores(true);
            populateRooms();
            refreshStores();
            return true;
        }
        else
        {
            switchStores(false);
            return false;
        }
    }
    private void applyVWSSettings()
    {
        vwsSchema.address = tVWSHost.getText();
        vwsSchema.comport = Util.to_Number(tVWSPort.getText());
        DSSPreferences.setVWS(vwsSchema);
    }
    private void switchStores(boolean b)
    {
        bAdd.setEnabled(b);
        bRemove.setEnabled(b);
        bConnect.setEnabled(!b);
    }
    private void removeStore()
    {
        String store = (String) lstStores.getSelectedValue();
        if (store != null)
        {
             if (!Util.Ask(this, resourceManager.getString("DSSSettingsMSG.RemovePath1") +" '"+ store + "'?", resourceManager.getString("DSSSettingsMSG.RemovePath2"))) 
                                                                        return;
             if (removeStoreFromServer(store))
             {
                 refreshStores();
             }
        }
    }
    private void addStore()
    {
        if (selectedRoom.equals("")) return;
        String store = getFolder();
        if (store != null)
        {
            if (storeExists(store)) 
            {
                Util.Msg(this, resourceManager.getString("DSSSettingsMSG.PathReg"), SETTINGS);
                return;
            }
            if (storeRoot(store))
            {
                Util.Msg(this,resourceManager.getString("DSSSettingsMSG.RootPath"), SETTINGS);
                return;
            }
            if (addStoreToServer(store))  
            {
                refreshStores();
            }
        }
    }
    private ServerSchema createStoreSchema(String store)
    {
        ServerSchema mySchema = Util.getMySchema();
        mySchema.comport = DSSPreferences.getComPort();
        mySchema.path = store;
        mySchema.room = selectedRoom;
        return mySchema;
    }
    private void getDSSStores(Vector stores)
    {
        try
        {
            Vector ret = vws.getDSSStores(createStoreSchema(""), stores);
            stores.addAll(ret);
        }
        catch(Exception e){}
    }
    private void populateRooms()
    {
        try
        {
            Vector ret = vws.getRoomNames();
            for (int i = 0; i < ret.size(); i++)
            {
                comRooms.addItem(ret.get(i));
                comRooms.setSelectedIndex(i);
            }
        }
        catch(Exception e){}
    }
    private boolean addStoreToServer(String store)
    {
        try
        {
            if ( vws.addDSSStore(createStoreSchema(store)) > 0) return true;
        }
        catch(Exception e){switchStores(false);}
        return false;
    }
    private boolean removeStoreFromServer(String store)
    {
        try
        {
            if ( vws.removeDSSStore(createStoreSchema(store)) > 0) return true;
        }
        catch(Exception e){switchStores(false);}
        return false;
    }
    private boolean storeExists(String path)
    {
         if (vStores.contains(path))
           return true;
        else
           return false;
    }
    private boolean storeRoot(String path)
    {
        if (new File(path).getParent() == null)
            return true;
        else
            return false;
    }
    private void refreshStores()
    {
        vStores.removeAllElements();
        getDSSStores(vStores);  // = DSSPreferences.getStores();
        lstStores.setListData(vStores);
        lstStores.setSelectedIndex(vStores.size()-1);
    }
    private void setSettings()
    {
        DSSPreferences.setHostName(tSvrHost.getText());
        DSSPreferences.setComPort(Integer.parseInt(tComPort.getText()));
        DSSPreferences.setDataPort(Integer.parseInt(tDataPort.getText()));
        DSSPreferences.setProxy(cbProxy.getSelectedObjects() != null? true : false);
        DSSPreferences.setProxyHost(tProxyHost.getText());
        String pPort = tProxyPort.getText();
        if (pPort.equals(""))
            DSSPreferences.setProxyPort(0);
        else
            DSSPreferences.setProxyPort(Integer.parseInt(pPort));
        applyVWSSettings();
        DSSPreferences.setLogInfo(cbLog.isSelected());
    }
    private void getCurrentSettings()
    {
        tSvrHost.setText(DSSPreferences.getHostName());
        tComPort.setText(String.valueOf(DSSPreferences.getComPort()));
        tDataPort.setText(String.valueOf(DSSPreferences.getDataPort()));
        cbProxy.setSelected(DSSPreferences.getProxy());
        tProxyHost.setText(DSSPreferences.getProxyHost());
        int pPort = DSSPreferences.getProxyPort();
        if (pPort > 0)
            tProxyPort.setText(String.valueOf(pPort));
        else
            tProxyPort.setText("");
        vwsSchema = DSSPreferences.getVWS();
        tVWSHost.setText(vwsSchema.address);
        int vPort = vwsSchema.comport;
        if (vPort > 0)
            tVWSPort.setText(String.valueOf(vPort));
        else
            tVWSPort.setText("");
        cbLog.setSelected(DSSPreferences.getLogInfo());
        cbProxyStateChanged();
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    private void closeDialog() 
    {
        System.exit(0);
    }
    private String getFolder()
    {
    	File folder = null;
        JFileChooser fc = new JFileChooser(new File("c:\\"));
        fc.setFileSelectionMode(fc.DIRECTORIES_ONLY);
        int returnVal = fc.showDialog(this, "select");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
        	folder = fc.getSelectedFile();
        }
        if (folder != null)
            return folder.getPath();
        else
            return null;
    }
    //Start of fix Issue 511 
    private String getDSSIPPort(String location)
    {
        try
        {        	
        	Vector ret = vws.getDSSIPPort(createStoreSchema(""), location);
            return ret.get(0).toString();
        }
        catch(Exception e){
        	DSSLog.add("Error occured in getDSSIPPort");
        	}
        return "";
    }
    //End of fix 511 
    public static void main(String args[]) 
    {
    	try{
            String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
    		UIManager.setLookAndFeel(plasticLookandFeel);
        	}catch(Exception ex){}    	
    	/*
    	 * new DSSSettings().show(); method is replaced with new DSSSettings().setVisible(true); 
    	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
    	 */
        new DSSSettings().setVisible(true);
    }

  //Start of fix Issue 511 
    class ListSelectionHandler implements ListSelectionListener {
    	public void valueChanged(ListSelectionEvent e) { 
    		  String location =(String)lstStores.getSelectedValue();
    		 
    		  if(location != null){
    		  String IPPort = getDSSIPPort(location);
    		  if(!IPPort.equalsIgnoreCase("")){
	    	      lDSSSvrDisplay.setText(IPPort.substring(0,IPPort.indexOf(":"))+" : "+IPPort.substring(IPPort.indexOf(":")+1,IPPort.length()));
    		  	  lDSSSvrName.setVisible(true);
	  	  		  lDSSSvrDisplay.setVisible(true); 
    		  	}
    		  } 
    		  else{
    		  	lDSSSvrName.setVisible(false);
    	  		lDSSSvrDisplay.setVisible(false);  		  	
    	  	      }    	  		 
    	} 
    }    	
    //End of fix 511 
}
