package com.computhink.dss.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Vector;

import com.computhink.common.*;

public interface DSS extends CIServer//, DocServer 
{
    public int getTotalDocuments() throws RemoteException; 
    public double getTotalBytes() throws RemoteException; 
    public double getTotalTime() throws RemoteException;
    public ServerSchema getVWS() throws RemoteException; 
    public void setVWS(ServerSchema vws) throws RemoteException;
    public void init() throws RemoteException; 
    public boolean deleteDocument(Document doc) throws RemoteException;
    public boolean deleteDocumentVersions(Document doc) throws RemoteException;
    public int getPageCount(Document doc, String room) throws RemoteException;
	// Add the one more parameter to check the version/revision have the document or not    
    public int getPageCount(Document doc, String room, Vector versionList) throws RemoteException;
    public long getLastModifyDate(Document doc) throws RemoteException;
    public boolean isAllWebExist(Document doc) throws RemoteException;
    public long getDocSize(Document doc,boolean withVR) throws RemoteException;
    public long getDocSize(Document doc,boolean withVR, String Version) throws RemoteException;
    public boolean setActiveFileVersion(int sid, int nid, String roomName, String path, String oVer, String nVer)
                                                         throws RemoteException;
    public boolean moveDocFolder(Document doc, String room, ServerSchema Sdss, 
                                      ServerSchema Ddss) throws RemoteException;
    public boolean copyDocFolder(Document doc, String room, ServerSchema Sdss, 
                                      ServerSchema Ddss) throws RemoteException;
    public boolean copyDocFolder(Document doc, String room, ServerSchema Sdss, 
                      ServerSchema Ddss, boolean withID) throws RemoteException;
    public VWDoc getRMIDoc(Document doc, String room, String path, 
               ServerSchema svr, boolean insideIdFolder) throws RemoteException;
    public VWDoc getRMIDoc(Document doc, String room, String path, 
            ServerSchema svr, boolean insideIdFolder, Vector versionList) throws RemoteException;
    public VWDoc getRMIDocWeb(Document doc, String room, String path, 
            ServerSchema svr, boolean insideIdFolder, Vector versionList) throws RemoteException;
    
    public VWDoc getNextRMIDoc(ServerSchema dssSchema, Document doc)throws RemoteException;
    public boolean isEncryptionKeySet(String roomName) throws RemoteException;
    public void setRoomEncryptionKey(String roomName,String key) throws RemoteException;
    /// Methods move from MDSS to DSS
    //public VWDoc getDocument(Document doc, String room, String srcPath,
    //		String destPath, boolean insideIdFolder) throws RemoteException;   
    
    public boolean getDocument(Document doc, String room, String path, 
            ServerSchema svr, boolean insideIdFolder) throws RemoteException;
	public boolean getDocFolder(Document doc, String room, String path, 
	                                   ServerSchema svr) throws RemoteException; 
	public VWDoc setDocument(Document doc, String room) throws RemoteException;
	public VWDoc setDocument(Document doc, String room, boolean isBackupDoc) throws RemoteException;
	public VWDoc setDocument(Document doc, String room, ServerSchema svr) throws RemoteException;
	public VWDoc setDocument(Document doc, String room, ServerSchema svr, boolean isBackupDoc) throws RemoteException;
	public VWDoc setDocFolder(Document doc, String room) throws RemoteException;
	public ArrayList getRMIDocList(String srcPath) throws RemoteException;
    public VWDoc getRMIDoc(Document doc, String room, String path, 
            ServerSchema svr) throws RemoteException;   
    public void writeLogMsg(ServerSchema dssSvr, Document doc,String room) throws RemoteException;
    public void updateVersionRevision(Document doc, String newVersion) throws RemoteException;
    public boolean isDSSLocationExists(String location) throws RemoteException;
    public long checkAvailableSpace(String dssLocation)throws RemoteException;
    public long getStoragelimit() throws RemoteException;
    public void SendEmailNotification(long dssLocationFreeSpace,int sid,String room) throws RemoteException;
    public VWDoc getTemplateFile(Document doc, String room, String srcFile,String dstFile) throws RemoteException ;
	public VWDoc getNextTemplateFile(ServerSchema dssSchema, Document doc)throws RemoteException;
	public ArrayList getAllZipFilesList(String srcPath) throws RemoteException;
	

}
