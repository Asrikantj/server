/*
 * DSSPreferences.java
 *
 * Created on November 10, 2003, 10:48 AM
 */
package com.computhink.dss.server;
import com.computhink.common.*;
/**
 *
 * @author  Administrator
 */
import java.util.prefs.*;
import java.util.*;

public class DSSPreferences implements DSSConstants
{
    
   private static Preferences DSSPref;
    
    static
    {
        DSSPref= Preferences.systemRoot().node(DSS_PREF_ROOT);
    }
    public static String getHostName()
    {
        return getNode("host").get("name", "");
    }
    public static void setHostName(String name)
    {
        getNode("host").put("name", name);
        flush();
    }
    private static Preferences getNode(String node)
    {
        return DSSPref.node(node);
    }
    public static void setComPort(int port)
    {
        getNode("host").putInt("comport", port);
        flush();
    }
    public static int getComPort()
    {
        return getNode("host").getInt("comport", DEFAULT_COM_PORT);
    }
     public static void setDataPort(int port)
    {
        getNode("host").putInt("dataport", port);
        flush();
    }
    public static int getDataPort()
    {
        return getNode("host").getInt("dataport", DEFAULT_DATA_PORT);
    }
    public static void setProxy(boolean b)
    {
        getNode("proxy").putBoolean("enabled", b);
        flush();
    }
    public static void setProxyHost(String server)
    {
        getNode("proxy").put("host", server);
        flush();
    }
    public static void setProxyPort(int port)
    {
        getNode("proxy").putInt("port", port);
        flush();
    }
    public static boolean getProxy()
    {
        return getNode("proxy").getBoolean("enabled", false);
    }
    public static String getProxyHost()
    {
        return getNode("proxy").get("host", "");
    }
    public static boolean getLogInfo()
    {
        return getNode("general").getBoolean("loginfo", true);
    }
    public static void setLogInfo(boolean b)
    {
        getNode("general").putBoolean("loginfo", b);
        flush();
    }
    public static boolean getDebugInfo()
    {
        return getNode("general").getBoolean("debuginfo", false);
    }
    public static void setDebugInfo(boolean b)
    {
        getNode("general").putBoolean("debuginfo", b);
        flush();
    }
    public static int getProxyPort()
    {
        return getNode("proxy").getInt("port", DEFAULT_PROXY_PORT);
    }
    public static int getFlushLines()
    {
        return getNode("general").getInt("loglines", DEFAULT_LOG_LINES);
    }
    public static void setVWS(ServerSchema vws)
    {
        getNode("vws").put("host", vws.address);
        getNode("vws").putInt("port", vws.comport);
        getNode("vws").put("dssstoragetype", vws.storageType);
        getNode("vws").put("dssusername", vws.userName);
        getNode("vws").put("dsspassword", vws.password);
        //getNode("vws").put("room", vws.room);
        flush();
    }
    public static ServerSchema getVWS()
    {
        ServerSchema ss = new ServerSchema();
        ss.address = getNode("vws").get("host", "");
        ss.comport = getNode("vws").getInt("port", 0);
        //ss.room = getNode("vws").get("room", "");
        ss.type = SERVER_VWS;
        return ss;
    }
    public static String getManager()
    {
        String man = getNode("general").get("manager", "admin");
        String pas = getNode("general").get("manpass", "manager");
        //man = Util.decryptKey(man);
        if (!pas.equals("manager")) pas = Util.decryptKey(pas);
        return man + "||" + pas;
    }
    public static boolean setManager(String man, String pas)
    {
        try
        {
            pas = Util.encryptKey(pas);
            getNode("general").put("manager", man);
            getNode("general").put("manpass", pas);
            flush();
            return true;
        }
        catch(Exception e){}
        return false;
    }
//  Issue 562
    public static void setLogFileCount(int count)
    {
        getNode("general").putInt("maxlogfilecount", count);
        flush();
    }    
    public static int getLogFileCount()
    {
        return getNode("general").getInt("maxlogfilecount",100);
    }    
  
    public static void setStorageSpace(long minSize){
    	 getNode("general").putLong("storagespace",minSize);
         flush();
    }
    public static long getStorageSpace()
    {
    	return getNode("general").getInt("storagespace",100);
    }
    
    public static void setDateTime(String date)
    {
    	getNode("general").put("DateTime",date);
        flush();
    }

    
    public static String getDateTime()
    {
      	return getNode("general").get("DateTime","");
     
    }
    
    //Issue 562
    /*
    public static Vector getStores()
    {
        Vector stores = new Vector();
        try
        {
            String[] keys = getNode("stores").keys();
            for (int i=0; i < keys.length; i++)
            {
                stores.add(getNode("stores").get(keys[i], ""));
            }
        }
        catch(Exception e){}
        return stores;
    }
    public static void addStore(String path)
    {
        Vector stores = getStores();
        int i = 0;
        for (; i < stores.size(); i++)
        {
            String store = (String) stores.elementAt(i);
            removeStore(store);
            getNode("stores").put(String.valueOf(i+1), store);
        }
        getNode("stores").put(String.valueOf(i+1), path);
    }
    public static boolean storeExists(String path)
    {
        String[] keys;
        try
        {
            keys = getNode("stores").keys();
            for (int i=0; i < keys.length; i++)
            {
                if ((getNode("stores").get(keys[i], "")).equals(path))
                                                                    return true;
            }
        }
        catch(Exception e){}
        return false;
    }
    public static void removeStore(String path)
    {
        String[] keys;
        try
        {
            keys = getNode("stores").keys();
            for (int i=0; i < keys.length; i++)
            {
                if ((getNode("stores").get(keys[i], "")).equals(path))
                {
                    getNode("stores").remove(keys[i]);
                    return;
                }
            }
        }
        catch(Exception e){}
    }
     */
    public static void setRestartDSSTime(String server)
    {
        getNode("general").put("restart", server);
        flush();
    }
    public static String getRestartDSSTime()
    {
        return getNode("general").get("restart", "23:00");
    }
    public static void setRestartDSS(boolean restartdss)
    {
        getNode("general").putBoolean("restartdss", restartdss);
        flush();
    }
    public static boolean isRestartDSS()
    {
        return getNode("general").getBoolean("restartdss", false);
    }   
    
    public static void setDSSStorageType(String storageType)
	{
		 getNode("general").put("dssstoragetype", storageType);
		 flush();
	}
	public static String getDSSStorageType()
	{
		return getNode("general").get("dssstoragetype", "");
	}
	
	public static void setDSSUserName(String userName)
	{
		 getNode("general").put("dssusername", userName);
		 flush();
	}
	public static String getDSSUserName()
	{
		return getNode("general").get("dssusername", "");
	}

	public static void setDSSPassword(String password)
	{
		 getNode("general").put("dsspassword", password);
		 flush();
	}
	public static String getDSSPassword()
	{
		return getNode("general").get("dsspassword", "");
	}

    static private void flush()
    {
        try
        {
            DSSPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }    
}
