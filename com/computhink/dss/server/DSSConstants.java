/*
 * DSSConstants.java
 *
 * Created on November 10, 2003, 10:49 AM
 */
package com.computhink.dss.server;
import com.computhink.common.*;
import com.computhink.resource.ResourceManager;
/**
 *
 * @author  Administrator
 */
public interface DSSConstants extends Constants
{
     public static final String DSS_PREF_ROOT 
                                            = "/computhink/" + PRODUCT_NAME.toLowerCase() + "/dss/server";
     public static final String DSS_CLASSPATH 
                                    = "com/computhink/dss/image/images/dss.gif";
     public static final String DSS_JAR = "/lib/ViewWise.jar!";
     public static final String SETTINGS =ResourceManager.getDefaultManager().getString("DSSMSG.Title");
     
     public static final int DEFAULT_DATA_PORT = 3001;
     public static final int DEFAULT_COM_PORT = 3000;
     public static final int DEFAULT_PROXY_PORT = 8080;
     
     public static final int DEFAULT_LOG_LINES = 100;
     public static final String ERROR_LOG_FILE = "DSSErrors.log";
     public static final String LOG_FILE_PREFIX = "DSSLog-";
     public static final String LOG_FOLDER = "log";
}
