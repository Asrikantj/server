/*
 * VWUtil.java
 *
 * Created on March 17, 2002, 12:17 AM
 */
package com.computhink.dss.server;
import com.computhink.common.*;
import com.computhink.vws.server.*;
/**
 *
 * @author  Saad
 * @version 
 */
import java.net.*;
import java.util.*;

public class DSSUtil implements DSSConstants
{
    private static String home = "";
    
    static
    {
        findHome();
    }
    public static String getHome()
    {
        return home;
    }
    private static void findHome()
    {
        String  urlPath = "";
        String  indexerHome = "";
        String res = DSS_CLASSPATH;
        String jarres = DSS_JAR;
        
        int startIndex = 0;
        int endIndex = 0;
        try
        {
            URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
            urlPath = url.getPath();
        }
        catch(Exception e){urlPath="";}
        if (!urlPath.equalsIgnoreCase(""))
        {
            if (urlPath.startsWith("file"))
            {
                //file:/c:/program%20files/viewwise%20indexer/lib/viewwiseindexer.jar!/image/indexer.gif
                startIndex = 6;
                endIndex = urlPath.length() - res.length() - jarres.length();
            }
            else
            {
                ///c:/program%20files/viewwise%20indexer/Classes/indexer.gif
                startIndex = 1;
                endIndex = urlPath.length() - res.length();
            }
            indexerHome = urlPath.substring(startIndex, endIndex-1);  
            indexerHome = indexerHome.replace('/', '\\');
        }
        home = cleanUP(indexerHome);
    }
    private static String cleanUP(String str) // removes %20    
    {
       String cleanStr = ""; 
       StringTokenizer st = new StringTokenizer(str, "%20");
       while (st.hasMoreElements())
       {
           cleanStr = cleanStr + st.nextToken() + " ";
       }
       return (cleanStr.equalsIgnoreCase("")? str : cleanStr.trim());
    }
}
