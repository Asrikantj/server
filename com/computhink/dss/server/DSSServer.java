/*
 * DSSServer.java
 *
 * Created on December 9, 2003, 6:03 PM
 */
package com.computhink.dss.server;
import com.computhink.vws.server.*;
import com.computhink.common.*;
import com.computhink.drs.server.DRSLog;

/**
 *
 * @author  AdministratorsetSrcFile
 */
import java.net.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.*;
import java.rmi.server.*;
import java.rmi.*;
import java.io.*;

public class DSSServer extends UnicastRemoteObject implements DSS, DocServer,
                                                                SCMEventListener
{
    private Vector clients = new Vector();
    private Vector rooms = new Vector();
    private HashMap ciphers = new HashMap();
    private int totalDocuments = 0;
    private double totalKB = 0;
    private double totalTime = 0;
    private long stime;
    private ServerSchema mySchema;
    private boolean shutDownRequest = false;
    private static ServerSchema  managerSchema = null;
    
    public DSSServer(ServerSchema s) throws RemoteException 
    {
        super(s.dataport);
        this.mySchema = s;
        SCMEventManager scm = SCMEventManager.getInstance();
        scm.addSCMEventListener(this);
        stime = System.currentTimeMillis();
    }
    public void handleSCMEvent(SCMEvent event)
    {
        shutDown();
    }
    public void exit() throws RemoteException
    {
        shutDown();
    }
    public String getServerOS() throws RemoteException
    {
        return Util.getEnvOSName();
    }
    public void init() throws RemoteException 
    {
        ServerSchema vwss = DSSPreferences.getVWS();
        VWS vws = (VWS) Util.getServer(vwss);
        if (vws == null)
        {
        	DSSLog.add(PRODUCT_NAME + " Server @" + vwss.address + ":" + 
                                                    vwss.comport + " inactive");
            DSSLog.war("Listening to "+ PRODUCT_NAME +" Server...");
            vws = waitForViewWiseServer(vwss);
        }
        if (shutDownRequest) return;
        DSSLog.add("Connected to "+ PRODUCT_NAME +" Server @" + vwss.address + ":" + 
                                                                  vwss.comport);
        try
        {
            Vector ret = vws.getRoomNames();
            if(ret==null || ret.size()==0)
            {
                DSSLog.war("Waiting to get "+ PRODUCT_NAME +" Rooms...");
                ret=waitForGetViewWiseRooms(vws);
            }
            rooms.addAll(ret);
            for (int i = 0; i < rooms.size(); i++)
            {
                String roomName = (String) rooms.get(i);
                DSSLog.dbg("Room Name before encryption in DSS init:::::"+roomName);
                String key = vws.getEncryptionKey(roomName, 
                                        "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
                //DSSLog.add("Encryption key inside for loop of DSS init::::"+key);
                //Null Pointer is not handled key!=null - added
                if (key!=null && !key.equals(""))
                {
                    DSSCipher cipher = new DSSCipher(key);
                    ciphers.put(roomName, cipher);
                    DSSLog.add("Document encryption set for Room '" +  roomName + "'");
                }
                else
                {
                    ciphers.put(roomName, null);
                }
                DSSLog.add("DSS for Room '" + roomName + "' Ready...");    
            }
        }
        catch(Exception e)
        {
            DSSLog.err(PRODUCT_NAME + " Server connection error: " + e.getMessage());
        }
    }
    private VWS waitForViewWiseServer(ServerSchema vwss)
    {
        VWS vws = null;
        while (vws == null && !shutDownRequest) 
        { 
            Util.sleep(2000);
            vws = (VWS) Util.getServer(vwss);
        }
        return vws;
    }
    private Vector waitForGetViewWiseRooms(VWS vws)
    {
        Vector ret=null;
        while ((ret == null || ret.size()==0) && !shutDownRequest) 
        { 
            try
            {
                Util.sleep(2000);
                ret = vws.getRoomNames();
            }
            catch(Exception e)
            {
                DSSLog.err(PRODUCT_NAME + " Server connection error.");
            }
        }
        return ret;
    }
    public ServerSchema getSchema() throws RemoteException 
    {
        return this.mySchema;
    }
    public void setComPort(int port) throws RemoteException 
    {
        DSSPreferences.setComPort(port);
    }
    public int getComPort() throws RemoteException
    {
        return DSSPreferences.getComPort();
    }
     public void setDataPort(int port) throws RemoteException
    {
        DSSPreferences.setDataPort(port);
    }
    public int getDataPort() throws RemoteException
    {
        return DSSPreferences.getDataPort();
    }
    public void setProxy(boolean b) throws RemoteException 
    {
        DSSPreferences.setProxy(b);
    }
    public void setProxyHost(String server) throws RemoteException 
    {
        DSSPreferences.setProxyHost(server);
    }
    public void setProxyPort(int port) throws RemoteException 
    {
        DSSPreferences.setProxyPort(port);
    }
    public boolean getProxy() throws RemoteException 
    {
        return DSSPreferences.getProxy();
    }
    public String getProxyHost() throws RemoteException 
    {
        return DSSPreferences.getProxyHost();
    }
    public int getProxyPort() throws RemoteException 
    {
        return DSSPreferences.getProxyPort();
    }
    public ServerSchema getVWS() throws RemoteException 
    {
        return DSSPreferences.getVWS();
    }
    public void setVWS(ServerSchema vws) throws RemoteException 
    {
        DSSPreferences.setVWS(vws);
    }
    public boolean getLogInfo() throws RemoteException
    {
        return DSSPreferences.getLogInfo();
    }
    public void setLogInfo(boolean b) throws RemoteException
    {
        DSSPreferences.setLogInfo(b);
    }
    public boolean getDebugInfo() throws RemoteException
    {
        return DSSPreferences.getDebugInfo();
    }
    public void setDebugInfo(boolean b) throws RemoteException
    {
        DSSPreferences.setDebugInfo(b);
    }
    public boolean setManager(String man, String pas) throws RemoteException
    {
        return DSSPreferences.setManager(man, pas);
    }
    public String getManager() throws RemoteException
    {
        return DSSPreferences.getManager();
    }
    public String getStorageType() throws RemoteException
    {
        return DSSPreferences.getDSSStorageType();
    }
    public boolean deleteDocument(Document doc) throws RemoteException 
    {
        return deleteDocument(doc, false);
    }
    public boolean deleteDocumentVersions(Document doc) throws RemoteException 
    {
        return deleteDocument(doc, true);
    }
    private void deleteFolder(String path)
    {
	
        File folder = new File(checkPath(new File(path)));
        if (!folder.exists()) return;
        File[] files = folder.listFiles();
        for (int i=0; i < files.length; i++)
            files[i].delete();
        folder.delete();
    }
    private boolean deleteDocument(Document doc, boolean versions)
    {
    	DSSLog.dbg("Inside deleteDocument");
    	String room = doc.getRoomName();
    	if (room != null){
    		room = ": " + doc.getRoomName() + " - ";  
    	}else{
    		room = "";
    	}
        File folder = new File(checkPath(new File(doc.getVWDoc().getDstFile())));
        if (!folder.exists()) return true;
        VWFileFilter ff = new VWFileFilter("*");
        String[] filestoDelete = folder.list(ff);
        for (int j=0; filestoDelete != null && j < filestoDelete.length; j++)
        {
            File currfile = new File(folder + Util.pathSep + filestoDelete[j]);
            DSSLog.dbg("current file in deleteDocument :"+currfile.getAbsolutePath());
            if (versions && currfile.getName().equals(DOC_CONTAINER)) continue;
            currfile.delete();
            DSSLog.dbg("After deleting current file in deleteDocument");
        }
        if (versions)
        {
            DSSLog.add(getCaller() + room + " deleted document '" + doc.getId() 
                                                                + "' versions");
            return true;
            
        }
        folder = new File(checkPath(folder));
        DSSLog.dbg("folder path before delete in deleteDocument:"+folder.getAbsolutePath());
        boolean ret = folder.delete();
        if (ret)
            DSSLog.add(getCaller() + room + " deleted document '" + doc.getId() + "'");
        
        else
            DSSLog.add(getCaller() + room + " document '" + doc.getId() 
                                                           + "' delete failed");
        return ret;
    }
    public VWDoc setVWDocument(Document doc, String room) throws RemoteException 
    {
    	//VWSLog.err("info coming in MDSS setDocument ");
        VWDoc vwdoc = doc.getVWDoc();
        int ret = vwdoc.writeContents();
        if (ret < 0){
        	vwdoc.setVaildZip(false);
        }
        return vwdoc;
    }
    public VWDoc setDocument(Document doc, String room) throws RemoteException{
    	return setDocument(doc, room, null);
    }
    
    public VWDoc setDocument(Document doc, String room, ServerSchema svr) throws RemoteException{
    	return setDocument(doc, room, svr, false);
    }
    public VWDoc setDocument(Document doc, String room, boolean isBackupDoc) throws RemoteException{
    	return setDocument(doc, room, null, isBackupDoc);
    }
    public VWDoc setDocument(Document doc, String room, ServerSchema svr, boolean isBackupDoc) throws RemoteException
    {
    	VWDoc vwdoc = doc.getVWDoc();
    	int ret =0;
    	// This following functionality will check the Diskspace and verify with the document size.
    	File dssLocFile = new File(vwdoc.getDstFile());
    	ServerSchema vwss = DSSPreferences.getVWS();
    	VWS vws = (VWS) Util.getServer(vwss);
    	Vector storageVect =vws.getAzureStorageCredentials(room,doc.getSessionId(),doc.getId());
		String stroageType[]=storageVect.get(0).toString().split(Util.SepChar);
		String storageType=stroageType[7];
	
    	if(storageType.equalsIgnoreCase("On Premises")){
        	if (!dssLocFile.exists()){
        		dssLocFile.mkdirs();
        	}
        	long dssLocationFreeSpace = checkAvailableSpace(vwdoc.getDstFile());

        	if (dssLocationFreeSpace == 0){
        		dssLocationFreeSpace = checkAvailableSpace(new File(vwdoc.getDstFile()).getParent());
        	}
        	long fileSize = (vwdoc.getSize()  / (1024 * 1024));

        	if (dssLocationFreeSpace <= 50){
        		vwdoc.setVaildZip(true);
        		vwdoc.setDocStatus(VWDoc.INSUFFICIENT_DISKSPACE);
        	}

        	if (fileSize >= dssLocationFreeSpace){
        		vwdoc.setVaildZip(false);
        		vwdoc.setDocStatus(VWDoc.DISKSPACE_NOT_AVAILABLE);    		
        	}
        	if (fileSize >= dssLocationFreeSpace){
        		return vwdoc;
        	}
        	if (vwdoc.getDocStatus() > 0){
        		String reason = "";
        		if (vwdoc.getDocStatus() == VWDoc.DISKSPACE_NOT_AVAILABLE){
        			reason = "Storage Server disk space is insufficient. ";
        		}else if(vwdoc.getDocStatus() == VWDoc.INSUFFICIENT_DISKSPACE){
        			reason = "Storage Server disk space is insufficient and less than 50 MB. ";    			
        		}    		
        		if (svr == null)
        			DSSLog.err(getCaller() + ": " + room + " - " + reason + doc.getId());
        		else
        			DSSLog.err(svr.address +  ": " + room + " - " + reason  + doc.getId());
        	}
        }
    	File file = null;
    	if (!vwdoc.getDstFile().endsWith(DOC_CONTAINER))
    		vwdoc.setDstFile(doc.getVWDoc().getDstFile() + DOC_CONTAINER);
    	vwdoc.setDstFile(checkPath(new File(doc.getVWDoc().getDstFile())));        
    	String oldDocVersion = doc.getVersion();
    	if (vwdoc.starting())
    	{
    		if(isBackupDoc){
    			DSSLog.dbg("Inside isBackupDoc in setDocFile");
    			String date = new SimpleDateFormat("yyyyMMddHHmmss").format(Calendar.getInstance().getTime());
    			file = new File(vwdoc.getDstFile());
    			if(file.exists()){
    				file.renameTo(new File(vwdoc.getDstFile()+"_"+date));
    				DSSLog.dbg("After renaming the file with Date:::"+file.getName());
    			}
    		}
    		if (oldDocVersion != null && !oldDocVersion.equals(""))
    		{
    			File fAllDotZip = new File(checkPath(new File(doc.getVWDoc().getDstFile())));
    			File fOldVersion = new File(fAllDotZip.getParent() + 
    					Util.pathSep + oldDocVersion);
    			if (fAllDotZip.exists() && !fOldVersion.exists()){
    				fAllDotZip.renameTo(fOldVersion);
    				DSSLog.dbg("If old all.zip renaming::::"+fAllDotZip.getName());
    			}
    		}
    	}
    	DSSLog.dbg("Before writing to chunk for document:::"+doc.getId());
    	if(storageType.equalsIgnoreCase("On Premises")) {
    		ret = vwdoc.writeContents();
    	}
    	if (ret < 0){
    		DSSLog.dbg("Inside valid zip false condition");
    		vwdoc.setVaildZip(false);
    	}
    	DSSLog.dbg("Writing Doc chunk...");
    	DSSLog.dbg("Vwdoc.getIntegrity::::"+vwdoc.getIntegrity());
    	if (vwdoc.getIntegrity())
    	{
    		/*
    		 * Checking the uploaded all.zip is corrupted or not 
    		 */

    		if (isValid(vwdoc.getDstFile())) {
    			DSSLog.dbg("all.zip is valid  after uploading to dss :::");
    			vwdoc.setVaildZip(true);

    		} else {
    			DSSLog.dbg("all.zip is invalid  after uploading to dss :::");
    			vwdoc.setVaildZip(false);
    			if (svr == null)
    				DSSLog.add(getCaller() + ": " + room + " - Uploaded document is corrupted " + doc.getId());
    			else
    				DSSLog.add(svr.address +  ": " + room + " - Uploaded document is corrupted " + doc.getId());
    			return vwdoc;
    		}
    		DSSCipher cipher = (DSSCipher) ciphers.get(room);
    		if (cipher != null)  
    		{
    			cipher.encryptFile(vwdoc.getDstFile());
    		}
    		if (svr == null)
            	DSSLog.add(getCaller() +  ": " + room + " - uploaded document '" + 
                                                             doc.getId() + "'");
    		else
            	DSSLog.add(svr.address +  ": " + room + " - uploaded document '" + 
                        doc.getId() + "'");
    	}
    	return vwdoc;
    }

    public long checkAvailableSpace(String dssLocation) throws RemoteException{
    	File dssLocationFile=null;
    	long dssLocationFreeSpace=0;
    	try{    	
    		dssLocationFile = new File(dssLocation);
    		if(dssLocationFile.exists()){
    			DSSLog.dbg("Storage location(Document Level):"+"<"+dssLocationFile+">");
    			dssLocationFreeSpace = (dssLocationFile.getFreeSpace() / (1024 * 1024));
    			DSSLog.dbg("Available/Free Disk Space on Storage server(Document Level): "+"<"+dssLocationFreeSpace+">");
    		}
    		else {
    			DSSLog.dbg("Storage location(Room Level):"+"<"+dssLocationFile+">");
    			String dsspath=dssLocation.substring(0,dssLocation.lastIndexOf("\\"));
    			String finalpath=dsspath.substring(0,dsspath.lastIndexOf("\\"));
    		    dssLocationFile = new File(finalpath);
    		    DSSLog.dbg("dssLocationFile:::::::::::"+dssLocationFile);
    			dssLocationFreeSpace = (dssLocationFile.getFreeSpace() / (1024 * 1024));
    			DSSLog.dbg("Available/Free Disk Space on Storage Server (Room Level): "+"<"+dssLocationFreeSpace+">");
    		}

    	}
    	catch(Exception exp){
    		DSSLog.dbg("Exception while getting Disk Space from Storage server"+exp.getMessage());
    	}
    	return dssLocationFreeSpace;
    }
	//Create the method “updateVersionRevision()” which will create a copy of all.zip with newVersion 
    public void updateVersionRevision(Document doc, String newVersion) throws RemoteException{
    	 VWDoc vwdoc = doc.getVWDoc();
    	 if (!vwdoc.getDstFile().endsWith(DOC_CONTAINER))
             vwdoc.setDstFile(vwdoc.getDstFile() + DOC_CONTAINER);
	   	 vwdoc.setDstFile(checkPath(new File(vwdoc.getDstFile())));
         if (newVersion != null && !newVersion.equals(""))
         {
             File fAllDotZip = new File(vwdoc.getDstFile());
             File fOldVersion = new File(fAllDotZip.getParent() + 
                                               Util.pathSep + newVersion);
             if (fAllDotZip.exists() && !fOldVersion.exists()){
            	 Util.CopyFile(fAllDotZip, fOldVersion);
             }
             
         }   
    }
    public VWDoc setDocFolder(Document doc, String room) throws RemoteException
    {
        VWDoc vwdoc = doc.getVWDoc();
        vwdoc.setDstFile(checkPath(new File(doc.getVWDoc().getDstFile())));
        int ret = vwdoc.writeContents();
        if (ret < 0){
        	vwdoc.setVaildZip(false);
        }
        return vwdoc;
    }
    public boolean getDocument(Document doc, String room, String path, 
                ServerSchema svr, boolean insideIdFolder) throws RemoteException 
    {
        boolean ret = false;
        String srcFile;
        String dstFile;
    	ServerSchema vwss = DSSPreferences.getVWS();
    	VWS vws = (VWS) Util.getServer(vwss);
    	Vector storageVect =vws.getAzureStorageCredentials(room,doc.getSessionId(),doc.getId());
		String stroageType[]=storageVect.get(0).toString().split(Util.SepChar);
		String storageType=stroageType[7];
        if (doc.getVersion().equals(""))
        {
            srcFile = path + DOC_CONTAINER;
            if (insideIdFolder)
                dstFile = svr.path + Util.pathSep + doc.getId() + 
                                                   Util.pathSep + DOC_CONTAINER;
            else
                dstFile = svr.path + Util.pathSep + DOC_CONTAINER;
        }
        else
        {
            //when getdssfordoc is called for a version it adds the file name
            srcFile = path;         
            dstFile = svr.path + Util.pathSep + doc.getId() + Util.pathSep + 
                                doc.getVersion() + Util.pathSep + DOC_CONTAINER;
        }
        srcFile = checkPath(new File(srcFile));
        //dstFile = checkPath(new File(dstFile));

        DSSCipher cipher = (DSSCipher) ciphers.get(room);
        VWDoc vwdoc = doc.getVWDoc();
        try
        {
            File srcFileExist = new File(srcFile);
            if (!srcFileExist.exists()) return false;
            if (cipher != null)  ret = cipher.decryptFile(srcFile);
            vwdoc.setSrcFile(srcFile);
            vwdoc.setDstFile(dstFile);
            //DSSLog.dbg("Reading Doc chunk...");
            DocServer ds = (DocServer) Util.getServer(svr);
            vwdoc = ds.setDocument(doc, room);
            if(storageType.equals("On Premises")){
            	while (vwdoc.hasMoreChunks())
            	{
            		DSSLog.dbg("Reading Doc chunk...");
            		//vwdoc = ds.setDocument(doc, room);
            		vwdoc = setVWDocument(doc, room);
            		doc.setVWDoc(vwdoc);
            	}
            }
            DSSLog.dbg("Reading Doc completed...");
            if (vwdoc.getIntegrity())
                ret = true;
            else
                ret = false;
        }
        catch(Exception e){}
        if(storageType.equals("On Premises")){
        if (ret)
            DSSLog.add(getCaller() + ": " + room + " -  downloaded document '" + 
                                                             doc.getId() + "'");
        else
            DSSLog.war( getCaller() + ": " + room + " -  document '" + doc.getId() 
                                                         + "' download failed");
        }
        return ret;
    }
	// Add the one more parameter to check the version/revision have the document or not
    public VWDoc getRMIDoc(Document doc, String room, String path, 
            ServerSchema svr, boolean insideIdFolder) throws RemoteException{
    	return getRMIDoc(doc, room, path, svr, insideIdFolder, new Vector());
    }
    public VWDoc getRMIDoc(Document doc, String room, String path, 
                ServerSchema svr, boolean insideIdFolder, Vector versionList) throws RemoteException 
    {
        boolean ret = false;
        String srcFile = null;
        String dstFile = null;
        
        VWDoc vwdoc = doc.getVWDoc();
        try {
        	DSSLog.dbg("getRMIDoc vwdoc ::: " + vwdoc.getDstFile());
	        if (vwdoc.getDstFile() == null)
	        {
	        	DSSLog.dbg("getRMIDoc doc.getVersion() ::: " + doc.getVersion());
	            if (doc.getVersion().equals(""))
	            {
	                srcFile = path + DOC_CONTAINER;
	                if (insideIdFolder)
	                    dstFile = svr.path + Util.pathSep + doc.getId() + 
	                                                   Util.pathSep + DOC_CONTAINER;
	                else
	                    dstFile = svr.path + Util.pathSep + DOC_CONTAINER;	                
	            }
	            else
	            {
	            	DSSLog.dbg("Before calling the getExistingFileForVerRev method ");
	                //when getdssfordoc is called for a version it adds the file name            	
	                srcFile = path;
	                
	                File srcFilePath = new File(srcFile);                
	                srcFilePath = new File(checkPath(srcFilePath));
	                DSSLog.dbg("File path exist : "+srcFilePath.exists()+" versionList size : "+versionList.size());
	                if (!srcFilePath.exists() && versionList.size() > 0){
	                	srcFile = getExistingFileForVerRev(versionList, doc.getVersion(), srcFilePath);                	
	                	//when we opened the document from version tab if version not exist or previous version not available return the NULL 
	                	if (srcFile.equals("")){ 
	                		return null;/*srcFile = srcFilePath.getParent() + File.separator + DOC_CONTAINER;*/
	                	}
	            	}
	                DSSLog.dbg("After calling the getExistingFileForVerRev method "+doc.getId());
		                dstFile = svr.path + Util.pathSep + doc.getId() + Util.pathSep + 
		                                doc.getVersion() + Util.pathSep + DOC_CONTAINER;
	            }
	            srcFile = checkPath(new File(srcFile));
	            DSSLog.dbg("Before entering the decryption process :: "+room);
	            DSSCipher cipher = (DSSCipher) ciphers.get(room);
	            DSSLog.dbg("cipher 1 ::: "+cipher);
	            if (cipher == null) {
	            	if (DSSCipher.isEncrypted(new File(srcFile))) {
	            		DSSLog.dbg("Encrypted file exist ");
	            		ServerSchema vwss = DSSPreferences.getVWS();
	            		VWS vws = (VWS) Util.getServer(vwss);
	            		String key = vws.getEncryptionKey(room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
	            		DSSLog.dbg("Encryption key in getRMIDoc::::"+key);
	            		if (key != null && key.trim().length() > 0) {			
	            			setRoomEncryptionKey(room, key);
	            			DSSLog.add("Document encryption set for Room " + room);
	            			cipher = (DSSCipher) ciphers.get(room);
	            		}
	            	}
	            }
	        	
	            DSSLog.dbg("cipher 2 ::: "+cipher);
	            if (cipher != null)  ret = cipher.decryptFile(srcFile);
	            vwdoc.setSrcFile(srcFile);
	            vwdoc.setDstFile(dstFile);
	            DSSLog.dbg("Decryption process is over ");
	         }
	       // else
	       // {
	            /*ret = vwdoc.hasMoreChunks();            
	            if (!ret)
	            	DSSLog.add(svr.address + " downloaded document '" +doc.getId() + "'");
	            else
	            	DSSLog.war( svr.address  + " document '" + doc.getId()+ "' download failed");*/
	       // }
	        //DSSLog.add("Document Info " + doc.getId() + " ::: " + vwdoc.hasMoreChunks());
        } catch (Exception e) {
        	DSSLog.add("Exception in getRMIDoc : " + e.getMessage());
        }
        return vwdoc;
    }
    
    
    public VWDoc getRMIDocWeb(Document doc, String room, String path, 
            ServerSchema svr, boolean insideIdFolder, Vector versionList) throws RemoteException 
{
    boolean ret = false;
    String srcFile = null;
    String dstFile = null;
    
    VWDoc vwdoc = doc.getVWDoc();
    if (vwdoc.getDstFile() == null)
    {
        if (doc.getVersion().equals(""))
        {
            srcFile = path + DOC_WEB_CONTAINER;
            if (insideIdFolder)
                dstFile = svr.path + Util.pathSep + doc.getId() + 
                                               Util.pathSep + DOC_WEB_CONTAINER;
            else
                dstFile = svr.path + Util.pathSep + DOC_WEB_CONTAINER;
        }
        else
        {
            //when getdssfordoc is called for a version it adds the file name            	
            srcFile = path;
            
            File srcFilePath = new File(srcFile);                
            srcFilePath = new File(checkPath(srcFilePath));
            if (!srcFilePath.exists() && versionList.size() > 0){
            	srcFile = getExistingFileForVerRev(versionList, doc.getVersion(), srcFilePath);                	
            	//when we opened the document from version tab if version not exist or previous version not available return the NULL 
            	if (srcFile.equals("")){ 
            		return null;/*srcFile = srcFilePath.getParent() + File.separator + DOC_CONTAINER;*/
            	}
        	}
                dstFile = svr.path + Util.pathSep + doc.getId() + Util.pathSep + 
                                doc.getVersion() + Util.pathSep + DOC_WEB_CONTAINER;
        }
        srcFile = checkPath(new File(srcFile));
        DSSCipher cipher = (DSSCipher) ciphers.get(room);
        if (cipher == null) {
        	if (DSSCipher.isEncrypted(new File(srcFile))) {
        		ServerSchema vwss = DSSPreferences.getVWS();
        		VWS vws = (VWS) Util.getServer(vwss);
        		String key = vws.getEncryptionKey(room, "4223DAAB-D393-11D0-9A76-00C05FB68BF7");
        		DSSLog.dbg("Encryption key in getRMIDoc::::"+key);
        		if (key != null && key.trim().length() > 0) {			
        			setRoomEncryptionKey(room, key);
        			DSSLog.add("Document encryption set for Room " + room);
        			cipher = (DSSCipher) ciphers.get(room);
        		}
        	}
        }
    	
        
        if (cipher != null)  ret = cipher.decryptFile(srcFile);
        vwdoc.setSrcFile(srcFile);
        vwdoc.setDstFile(dstFile);
     }
   // else
   // {
        /*ret = vwdoc.hasMoreChunks();            
        if (!ret)
        	DSSLog.add(svr.address + " downloaded document '" +doc.getId() + "'");
        else
        	DSSLog.war( svr.address  + " document '" + doc.getId()+ "' download failed");*/
   // }
    //DSSLog.add("Document Info " + doc.getId() + " ::: " + vwdoc.hasMoreChunks());
    return vwdoc;
}
    public String checkPath(File file){
	String dstFile = file.getAbsolutePath();
	if(dstFile.indexOf("\\")!=-1  && dstFile.indexOf("/")!=-1 ){
		String newdstFile = "";
		try{
			newdstFile = dstFile;
			/* temp.replaceAll("\\", "/"); This replaceAll will not work and throws PatternSyntaxException  
			* To resolve this problem is it must be temp.replaceAll("\\\\","/"); 
			*/
			if((File.separatorChar == '/')&& newdstFile.indexOf("\\")!=-1){
				newdstFile = newdstFile.replaceAll("\\\\", "/");
			}
			if((File.separatorChar == '\\')&& newdstFile.indexOf("/")!=-1){
				newdstFile = newdstFile.replaceAll("/", "\\\\");
			}
			dstFile = newdstFile;
		}catch(Exception ex){
			//DSSLog.err("Error in writeContents - "+ex.toString());
		}
	}
	return dstFile;
    }
    /*Purpose: Issue # 791 Methods added to fix the issue. "Document is not downloaded if the size is greater than 4mb
    * C.Shanmugavalli 19 Dec 2006 
    */
    public VWDoc getNextRMIDoc(ServerSchema dssSvr, Document doc) throws RemoteException {
    	
    	try{
    		if (dssSvr == null) return null;
    		VWDoc vwdoc = doc.getVWDoc();
    		vwdoc.hasMoreChunks();
            if(vwdoc.finished())
    			DSSLog.add(dssSvr.address + " downloaded document '" +doc.getId() + "'");
    		return vwdoc;
    		}catch(Exception ex){
    			DSSLog.add(dssSvr.address  + " document '" + doc.getId()+ "' download failed");
    			return null;
    		}
    }
    public void writeLogMsg(ServerSchema mySchema, Document doc,String room) throws RemoteException {
    	try {
	    	VWDoc vwdoc = doc.getVWDoc();
	    	String storageType="";
	    	ServerSchema vwss = DSSPreferences.getVWS();
	    	VWS vws = (VWS) Util.getServer(vwss);
	    	if(vws!=null){
	    		Vector storageVect =vws.getAzureStorageCredentials(room,doc.getSessionId(),doc.getId());
	    		String stroageType[]=storageVect.get(0).toString().split(Util.SepChar);
	    		storageType=stroageType[7];
	    	}
	    
	    	if (storageType.equalsIgnoreCase("On Premises")) {
	    		DSSLog.dbg("Inside if condition of onpremises");
	    		if(vwdoc.getIntegrity()){
	    			//CV10 Enhancement Room level encryption re-encrypting the all.zip after opening in viewer & after backup
	    			DSSLog.dbg("roomName in writeLogMsg "+room);
	    			DSSCipher cipher = (DSSCipher) ciphers.get(room);
	    			if (cipher != null)  {
	    				DSSLog.dbg("Inside dss cipher not null");
	    				if (!DSSCipher.isEncrypted(new File(vwdoc.getSrcFile()))){
	    					DSSLog.dbg("Inside second if condition");
	    					cipher.encryptFile(vwdoc.getSrcFile());
	    					DSSLog.dbg("File Encrypted in writeLogMsg");
	    				}
	    			}
	    			DSSLog.add(mySchema.address + ": " + mySchema.getRoom() + " - downloaded document '" +doc.getId() + "'");
	    		}
	    		else	
	    			DSSLog.add(mySchema.address  + ": " + mySchema.getRoom() + " - document '" + doc.getId()+ "' download failed");
	    	}	    	
    	} catch (Exception e) {
    		DSSLog.dbg("Exception in writeLogMsg : "+e.getMessage());
    	}
    }
// get the list of files from the storage server

    public ArrayList getRMIDocList(String srcPath) throws RemoteException {
    	ArrayList fileList = new ArrayList();
        try
        {
            String modifedSrcPath = checkPath(new File(srcPath));
            File path = new File(modifedSrcPath);
        	 if (path.isDirectory())
             {
                 File[] fa = path.listFiles();
                 if (fa.length > 0)
                     for (int i = 0; i < fa.length; i++)
                     {
                    	 fileList.add(fa[i].getPath());
                     }
             } 
        }        
        catch(Exception e){return null;}
        return fileList;
    }
    /**
     * Added to check the file size from dss and AIP  Temp
     * @param srcPath
     * @return
     * @throws RemoteException
     */
    public ArrayList getAllZipFilesList(String srcPath) throws RemoteException{
    	DSSLog.add("Inside getAllZipFilesList"+srcPath);
    	ArrayList fileList = new ArrayList();
    	ZipFile zfile=null;
    	try{
    		File allZipFile=new File(srcPath+"\\"+"all.zip");
    	
    		if(allZipFile.exists()){
    			long zipSize=allZipFile.length();
    			fileList.add(String.valueOf(zipSize));
    			zfile = new ZipFile(allZipFile);
    			if(zfile!=null){
    				java.util.Enumeration e = zfile.entries();
    				while (e.hasMoreElements())
    				{
    					ZipEntry zipEntry = (ZipEntry) e.nextElement();
    					DSSLog.dbg("Zip file contents are ::"+zipEntry.getName());
    					fileList.add(zipEntry.getName());
    				}
    			}

    		}
    	}catch(Exception e){
    		DSSLog.err("Exception while getting files list");
    		return null;
    	}
    	finally{
    		try {
    			zfile.close();
    		} catch (IOException e) {
    			// TODO Auto-generated catch block
    			DSSLog.err("Exception while getting files list");
    		}
    	}
    	return fileList;
    }    
    public VWDoc getRMIDoc(Document doc, String room, String path, 
            ServerSchema svr) throws RemoteException 
{
        boolean ret = false;
        String srcFile = null;
        String dstFile = null;                
        VWDoc vwdoc = doc.getVWDoc();
        try{
            	path = checkPath(new File(path));
    	 	srcFile = path;
    	 	if (!new File(srcFile).exists()) return null;
    	 	dstFile = svr.path + Util.pathSep + new File(srcFile).getName();
            DSSCipher cipher = (DSSCipher) ciphers.get(room);
            if (cipher != null)  ret = cipher.decryptFile(srcFile);
            vwdoc.setSrcFile(srcFile);
            vwdoc.setDstFile(dstFile);
        }catch(Exception ex){}
        return vwdoc;
}    
    public boolean getDocFolder(Document doc, String room, 
                             String path, ServerSchema s) throws RemoteException 
    {
        VWDoc vwdoc = doc.getVWDoc();
        try
        {
            //if (cipher != null)  ret = cipher.decryptFolder(srcFile);
            vwdoc.setSrcFile(path);
            //if (cipher != null)  ret = cipher.encryptFile(docFile);
            vwdoc.setDstFile(s.path);
            DocServer ds = (DocServer) Util.getServer(s);
            vwdoc = ds.setDocFolder(doc, room);
            while (vwdoc.hasMoreChunks())
            {
                vwdoc = ds.setDocFolder(doc, room);
                doc.setVWDoc(vwdoc);
            }
            if (vwdoc.getIntegrity())
                return true;
            else
                return false;
        }
        catch(Exception e){return false;}
    }
    public boolean moveDocFolder(Document doc, String room, ServerSchema Sdss, 
                                       ServerSchema Ddss) throws RemoteException 
    {
        return moveDocFolder(doc, room, Sdss, Ddss, true, true);
    }
    public boolean copyDocFolder(Document doc, String room, ServerSchema Sdss, 
                                       ServerSchema Ddss) throws RemoteException 
    {
        return moveDocFolder(doc, room, Sdss, Ddss, false, true);
    }
    public boolean copyDocFolder(Document doc, String room, ServerSchema Sdss, 
                       ServerSchema Ddss, boolean withID) throws RemoteException 
    {
        return moveDocFolder(doc, room, Sdss, Ddss, false, withID);
    }
    private boolean moveDocFolder(Document doc, String room, ServerSchema Sdss, 
                              ServerSchema Ddss, boolean delete, boolean withID) 
    {
        boolean ret = false;
        if (Ddss.path.equals("")) return ret;
        if (!Ddss.path.endsWith("/") && !Ddss.path.endsWith("\\")) Ddss.path += Util.pathSep;
        if (withID) Ddss.path = Ddss.path + doc.getId();
        //requester did not set DSS type
        if (Ddss.getType().equals("")) Ddss.setType(Sdss.getType());
        
        VWDoc vwdoc = doc.getVWDoc();
        try
        {
        	ServerSchema vwss = DSSPreferences.getVWS();
        	VWS vws = (VWS) Util.getServer(vwss);
           /* Vector storageVect =vws.getAzureStorageCredentials(room,doc.getSessionId(),doc.getId());
    		String stroageType[]=storageVect.get(0).toString().split(Util.SepChar);
    		String storageType=stroageType[7];*/
            vwdoc.setSrcFile(checkPath(new File(Sdss.path)));
            String sourceStorageType=Sdss.getStorageType();
            DSSLog.dbg("sourceStorageType in movdocfolder :"+sourceStorageType);
            vwdoc.setDstFile(Ddss.path);
            String destStorageType=Ddss.getStorageType();
            DSSLog.dbg("destStorageType in movdocfolder :"+destStorageType);
            doc.setVWDoc(vwdoc);
            if(sourceStorageType.equalsIgnoreCase("On Premises")&&destStorageType.equalsIgnoreCase("On Premises")){
            	if (Ddss.address.equals(mySchema.address))
            	{
            		//same dss diffrenet folder
            		vwdoc = setDocFolder(doc, room);
            		while (vwdoc.hasMoreChunks())
            		{
            			vwdoc = setDocFolder(doc, room);
            			doc.setVWDoc(vwdoc);
            		}
            		if (vwdoc.getIntegrity())
            			ret = true;
            		else
            			ret = false;

            	}
            	else
            	{
            		DocServer ds = (DocServer) Util.getServer(Ddss);
            		vwdoc = ds.setDocFolder(doc, room);
            		while (vwdoc.hasMoreChunks())
            		{
            			vwdoc = ds.setDocFolder(doc, room);
            			doc.setVWDoc(vwdoc);
            		}
            		if (vwdoc.getIntegrity())
            			ret = true;
            		else
            			ret = false;
            	}
            }
        	else if(sourceStorageType.equalsIgnoreCase("Azure Storage")&&destStorageType.equalsIgnoreCase("On Premises")){
        		DSSLog.dbg("Inside azure to onpremesis copy condition");
        		String azureUserName=Sdss.getUserName();
        		String azurePassword=Sdss.getPassword();
        		AzureOnpremisesCopy azureOP=new AzureOnpremisesCopy();
        		String sourceFile=checkPath(new File(Sdss.path));
        		String destinationFile=checkPath(new File(Ddss.path));
        		DSSLog.dbg("Source file inside copy::::::"+sourceFile);
        		DSSLog.dbg("destinationFile inside copy::::::"+destinationFile);
        		ret=azureOP.azureOnpremisesDocument(sourceFile,destinationFile,azureUserName,azurePassword,doc.getId(),delete,withID);
        		delete=false;
        	}
        	else if(sourceStorageType.equalsIgnoreCase("On Premises")&&destStorageType.equalsIgnoreCase("Azure Storage")){
        		DSSLog.dbg("Inside onpremesis to azure copy condition");
        		OnpremisesAzureCopy opAzure=new OnpremisesAzureCopy();
        		String azureUserName=Ddss.getUserName();
        		String azurePassword=Ddss.getPassword();
        		AzureOnpremisesCopy azureOP=new AzureOnpremisesCopy();
        		String sourceFile=checkPath(new File(Sdss.path));
        		String destinationFile=checkPath(new File(Ddss.path));
        		DSSLog.dbg("Source file inside copy::::::"+sourceFile);
        		DSSLog.dbg("destinationFile inside copy::::::"+destinationFile);
        		ret=opAzure.onpremisesAzureCopyDocument(sourceFile, destinationFile, azureUserName, azurePassword,doc.getId(),delete,withID);
        		delete=false;
        	}
        }
        catch(Exception e){return false;}
        if (ret && delete) deleteFolder(Sdss.path);
        return ret;
    }
    public int getPageCount(Document doc, String room) throws RemoteException{
    	return getPageCount(doc, room, new Vector());
    }
    public int getPageCount(Document doc, String room, Vector versionList) throws RemoteException
    {
        //File folder = new File(doc.getVWDoc().getDstFile());
        //if (!folder.exists()) return 0;
        int pageCount = 0;
        File docFile = null;
        try
        {
            if (doc.getVersion().equals(""))
                docFile = new File(doc.getVWDoc().getDstFile() + DOC_CONTAINER);
            else{
            	docFile = new File(doc.getVWDoc().getDstFile());
            	if (!docFile.getPath().endsWith(doc.getVersion())){
            		docFile = new File(docFile.getPath() + File.separator + doc.getVersion());
            	}            
            }
            docFile = new File(checkPath(docFile));    
                
            if ( !docFile.exists() ) {
            	String returnFile = getExistingFileForVerRev(versionList, doc.getVersion(), docFile);
            	// If the first few version of the doc is not having any pages, then zip (1.0)file will not be there
            	// in that location. So returning page count of active file.
            	if (returnFile.equals("")) return 0;/*docFile = new File(docFile.getParent() + File.separator + DOC_CONTAINER);*/ 
            	else docFile = new File(returnFile);            	
            }
            DSSCipher cipher = (DSSCipher) ciphers.get(room);
            if (cipher != null)  cipher.decryptFile(docFile.getPath());
            
            ZipFile zfile = new ZipFile(docFile);
            java.util.Enumeration e = zfile.entries();
            while (e.hasMoreElements())
            {
                if ( ((ZipEntry) e.nextElement()).getName().endsWith(".vw") ) 
                                                                    pageCount++;
            }
            zfile.close();
            if (cipher != null)  cipher.encryptFile(docFile.getPath());
        } 
        catch (IOException e){}
        return pageCount;
    }
	// Identified the version /revision file is available or not
    private String getExistingFileForVerRev(Vector versionList, String Version, File docFile){
    	boolean findFile = false;
    	File selectedFile = null;
    	for(int index= versionList.size()- 1; index >= 0; index--){
    		Document doc = (Document) versionList.get(index);
    		if (doc.getVersion().equals(Version)){
    			findFile = true;
    		}
    		if (findFile){    			
    			selectedFile = new File(docFile.getParent() + File.separator + doc.getVersion());
    			if (selectedFile.exists()){  
    				return selectedFile.getPath();
    			}
    		}
    	}
    	return "";
    }
    public boolean setActiveFileVersion(int sid, int nid, String roomName,
	    String path, String oVer, String nVer) {
	path = checkPath(new File(path));
	if (path != null && !path.endsWith("\\") && !path.endsWith("/")){
	    path += File.separator;
	}
	File currentFile = new File(path + DOC_CONTAINER);
	// document has no file is OK
	if (!currentFile.exists())
	    return true;
	// new file version cannot exist
	File nVerFile = new File(path + nVer);
	// Comment the following code. now whenever create an all.zip
        // corresponding version/revision copy created.

	// if (nVerFile.exists()) return false;
	// current file becomes new version
	// copy(currentFile, nVerFile);
	Util.CopyFile(currentFile, nVerFile);
	// do we have a file for the old ver?
	File oVerFile = new File(path + oVer);
	ServerSchema vwss = DSSPreferences.getVWS();
	VWS vws = (VWS) Util.getServer(vwss);
	if (oVerFile.exists()) {
	    currentFile.delete();
	    Util.CopyFile(oVerFile, currentFile);
	} else {
	    try {
		Vector versionList = new Vector();
		Vector ret = new Vector();
		String returnFile = "";
		ret = vws.getDocumentVers(roomName, sid, nid, versionList);
		if (ret != null && ret.size() > 0) {
		    versionList.addAll(vectorToDocuments(ret, true));
		    returnFile = getExistingFileForVerRev(versionList, oVer,
			    oVerFile);
		}
		if (returnFile != null && !returnFile.equalsIgnoreCase("")) {
		    File existFileObject = new File(returnFile);
		    if (existFileObject.exists()) {
			currentFile.delete();
			Util.CopyFile(existFileObject, currentFile);
		    }
		} else {
		    Util.CreateEmptyAllZip(path, oVer);
		    currentFile.delete();
		    Util.CopyFile(oVerFile, currentFile);
		}
	    } catch (Exception ex) {
		return false;
	    }
	}
	try {
	    Document doc = new Document(nid);
	    VWDoc vwdoc = new VWDoc();
	    vwdoc.setDstFile(path);
	    doc.setVWDoc(vwdoc);
	    int pageCount = getPageCount(doc, roomName);
	    vws.setPageCount(roomName, sid, nid, pageCount);
	} catch (Exception ex) {
	    
	}
	return true;
    }
    private void copy(File fin, File fout) 
    {
        try
        {
            FileReader in = new FileReader(fin);
            FileWriter out = new FileWriter(fout);
            int c;
            while ((c = in.read()) != -1)  out.write(c);
            in.close();
            out.close();
        }
        catch (Exception e){}
    }

    public long getLastModifyDate(Document doc) throws RemoteException
    {
        File folder = new File(doc.getVWDoc().getDstFile());
        if (!folder.exists()) return 0;
        File file = new File(checkPath(new File(doc.getVWDoc().getDstFile() + DOC_CONTAINER)));
        if (file != null && file.exists())
            return file.lastModified();
        else
            return 0;
    }
    
    public boolean isAllWebExist(Document doc) throws RemoteException
    {
    	DSSLog.add("isAllWebExist:::"+doc.getVWDoc().getDstFile());
        File folder = new File(doc.getVWDoc().getDstFile());
        if (!folder.exists()) return false;
        File file = new File(checkPath(new File(doc.getVWDoc().getDstFile() + DOC_WEB_CONTAINER)));
        DSSLog.dbg("file path for allweb zip is ::::"+file.getAbsolutePath());
        if (file != null && file.exists())
            return true;
        else
            return false;
    }
/*
Enhancement No / Purpose:  <63/While Downloading display the downloaded size>
Created by: <Pandiya Raj.M>
Date: <03 Aug 2006>
*/
    public long getDocSize(Document doc, boolean withVR) throws RemoteException
    {
        return getDocSize(doc, withVR,"");        
    }
    public long getDocSize(Document doc, boolean withVR,String Version) throws RemoteException
    {
        long docSize=0;
        File folder = new File(doc.getVWDoc().getDstFile());
        //String returnPath = checkPath(new File(doc.getVWDoc().getDstFile()));
        //doc.getVWDoc().setDstFile(returnPath);
        File file = null;
        if (Version.trim().length() > 0 && doc.getVersion().equals("")){
        	doc.setVersion(Version);
        }
        if (doc.getVersion().equals("")){
            if (doc.getVWDoc().getDstFile().endsWith(File.separator))
        	file = new File(doc.getVWDoc().getDstFile() + DOC_CONTAINER);
            else
        	file = new File(doc.getVWDoc().getDstFile() + File.separator + DOC_CONTAINER);
        }
        else{        	
        	file = new File(doc.getVWDoc().getDstFile());
        	if (!file.getPath().endsWith(doc.getVersion())){
        		if (!file.getPath().endsWith("all.zip")) //if statement added by srikanth on 16 sep 2015 for 0kb issue
        			file = new File(file.getPath() + File.separator + doc.getVersion());
        	}
        }       
        file = new File(checkPath(file));
        if ( !file.exists() ) {
        	ServerSchema vwss = DSSPreferences.getVWS();
        	VWS vws = (VWS) Util.getServer(vwss);
        	Vector versionList = new Vector();
        	Vector ret = new Vector();
        	String returnFile = "";
        	try{
        		ret = vws.getDocumentVers(doc.getRoomName(),doc.getRoomId(),doc.getId(),versionList);
            	if(ret!=null && ret.size()>0){
            		versionList.addAll(vectorToDocuments(ret, true));
            		returnFile = getExistingFileForVerRev(versionList, doc.getVersion(), file);
            	}        	        	
        	}catch(Exception ex){
        		
        	}
        	// If the first few version of the doc is not having any pages, then zip (1.0)file will not be there
        	// in that location. So returning page count of active file.
        	if (returnFile.equals("")) return 0;/*file = new File(file.getParent() + File.separator + DOC_CONTAINER);*/ 
        	
        	else {      
        	    file = new File(checkPath(new File(returnFile)));       
        	}        	
        }
        		docSize = file.length();
        return docSize;
    }
    
    private Vector vectorToDocuments(Vector v, boolean ver)
    {
        Vector docs = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            Document doc = new Document((String) v.elementAt(i), ver);
            docs.addElement(doc);
        }
        return docs;
    }
    
    public long getTimeOn() throws RemoteException 
    {
        return (System.currentTimeMillis() - stime) / 1000;
    }
    public LogMessage getLogMsg() throws RemoteException 
    {
        return DSSLog.getLog();
    }
    public int getLogCount() throws RemoteException
    {
        return DSSLog.getCount();
    }
    public Vector getClients() throws RemoteException 
    {
        return this.clients;
    }
    public int getTotalDocuments() throws RemoteException 
    {
        return this.totalDocuments;
    }
    public double getTotalBytes() throws RemoteException 
    {
        return this.totalKB;
    }
    public double getTotalTime() throws RemoteException 
    {
        return this.totalTime;
    }
    private void appendCaller(String caller)
    {
        if (clients.contains(caller)) return;
        clients.add(caller);
    }
    private String getCaller()
    {
        String caller = "";
        String clrName = "";
        try
        {
            caller = this.getClientHost(); 
            clrName = InetAddress.getByName(caller).getHostName();
        }
        catch (Exception e)
        {
        }
        appendCaller(caller + " (" + clrName + ")");
        return caller;
    }
    public String managerBegin(ServerSchema manager) throws RemoteException 
    {
        if (managerSchema == null) 
        {
            managerSchema = manager;
            return "";
        }
        else
        {
            if (managerSchema.address.equals(manager.address))
                return "";
            else
                return "Cannot monitor server. Manager @" 
                                + managerSchema.address + " already connected.";
        }
    }
    public void managerEnd() throws RemoteException 
    {
        managerSchema = null;
    }
    private void shutDown()
    {
        shutDownRequest = true;
        Set rooms = ciphers.keySet();
        Iterator it = rooms.iterator();
        for (int i = 0; i < rooms.size(); i++)
        {
            DSSLog.add("Stopped DSS Server for Room " + (String) it.next());
        }
        /* Issue 658: DSS is not able to stop through ViewWise manager in Netware. Has to kill through java -kill
        * Author: C.Shanmugavalli Date: 8 Nov 2006
        */
        
        DSSLog.add("Writing Server Log");
        DSSLog.writeLog();
        DSSLog.add("Stopping DSS Server...");
        Util.sleep(500);
        Util.killServer(mySchema);
        
    }
    public void setRoomEncryptionKey(String roomName,String key) throws RemoteException 
    {
	if (ciphers.containsKey(roomName) && ciphers.get(roomName) != null ) return;
        if (!key.equals("")/* && !ciphers.containsKey(roomName)*/)
        {
        	//DSSLog.dbg("Encryption key in setRoomEncryptionKey::::"+key);
            DSSCipher cipher = new DSSCipher(key);
            ciphers.put(roomName, cipher);
            DSSLog.add("Document encryption set for Room '" + 
                                                        roomName + "'");
        }
        else if (key.equals("") && !ciphers.containsKey(roomName))
        {
            ciphers.put(roomName, null);
        }
    }
    public boolean isEncryptionKeySet(String roomName) throws RemoteException 
    {
        return ciphers.containsKey(roomName);
    }
    public boolean isDSSLocationExists(String location) throws RemoteException
    {
    	DSSLog.dbg("In isDSSLocationExists method");	
    	location = checkPath(new File(location));
    	DSSLog.dbg("Location ::  "+location);	
    	File folder = new File(location);
    	DSSLog.dbg("folder path    ::  "+folder.getAbsolutePath());	
    	if (folder.exists()) {
    		DSSLog.dbg("Inside folder exists condition ");	
    		return true;
    	}
    	DSSLog.dbg("folder location not exists ");	
    	return false;
    }
    
	public static boolean isValid(String filePath) {
			File file = new File(filePath); 
		    ZipFile zipfile = null;
		    try {
		        zipfile = new ZipFile(file);
		        return true;
		    } catch (ZipException e) {
		        return false;
		    } catch (IOException e) {
		        return false;
		    } finally {
		        try {
		            if (zipfile != null) {
		                zipfile.close();
		                zipfile = null;
		            }
		        } catch (IOException e) {
		        }
		    }
		}
	/*
	 * Method added for DSS Low Disk Space email alert
	 * 
	 */
	public void SendEmailNotification(long dssLocationFreeSpace,int sid,String room)
	{
		try{
	    	long maxStorageSapce=dssLocationFreeSpace;
	    	String msg="Available/Free Disk Space on Storage server is Low : "+maxStorageSapce+" MB"+" "+"Contact Administrator.";
	    	int retval=0;
	    	String registryDate=DSSPreferences.getDateTime();
	    	Date sysdate=new Date();
	    	SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	    	String currentDate=format.format(sysdate);
	    	if(registryDate.equals("")){
	    		ServerSchema vwss = DSSPreferences.getVWS();
    			VWS vws = (VWS) Util.getServer(vwss);
    			retval=vws.sendNotification(room,sid,1,msg);
    			DSSLog.add("Low Disk Space on Storage server. E-mail Notification sent");
    			DSSPreferences.setDateTime(currentDate);
	     	}
	    	else if(!registryDate.equals("")){
	    	Date d1 = null;
	    	Date d2 = null;
	    	try{
	    		d1 = format.parse(currentDate);
	    		d2 = format.parse(registryDate);
	    	}
	    	catch(Exception e){
	    		VWSLog.dbg("Date Parsing Exception in Notification of DSS"+e.getMessage());
	    	}
	    	
	    	//in milliseconds
	    	long diff = d1.getTime() - d2.getTime();
	    	long diffHours = diff / (60 * 60 * 1000) % 24;
    	 	if(diffHours>=2){
	    			ServerSchema vwss = DSSPreferences.getVWS();
	    			VWS vws = (VWS) Util.getServer(vwss);
	    			retval=vws.sendNotification(room,sid,1,msg);
	    			DSSLog.add("Low Disk Space on Storage server. E-mail Notification sent");
	    			DSSPreferences.setDateTime(currentDate);
	    		}
	    	
	    	}
	    	
	    		
	    	if(retval!=108){
    			
    		}
	    	}
	    	catch(Exception exception)
	    	{
	    		VWSLog.dbg("Exception while sending mail from Storage server"+exception.getMessage());
	    	}
	}
	/*
	 * Method added for DSS Low Disk Space to get the minimum size 
	 * from Registry.
	 * 
	 */
	public long getStoragelimit() throws RemoteException
	{
	long minStorageSpace=DSSPreferences.getStorageSpace();
	return minStorageSpace;
	}
	
	/**
	 * CV10.1 Enhacement to get the next template
	 */
	public VWDoc getNextTemplateFile(ServerSchema dssSvr, Document doc) throws RemoteException {

		try{
			if (dssSvr == null) return null;
			VWDoc vwdoc = doc.getVWDoc();
			vwdoc.hasMoreChunks();
			if(vwdoc.finished())
				DSSLog.add(dssSvr.address + " downloaded document '" +doc.getId() + "'");
			return vwdoc;
		}catch(Exception ex){
			DSSLog.add(dssSvr.address  + " document '" + doc.getId()+ "' download failed");
			return null;
		}
	}

	public VWDoc getTemplateFile(Document doc, String room, String srcFile,String dstFile)
			throws RemoteException {
		VWDoc vwdoc = doc.getVWDoc();
		if (vwdoc.getDstFile() == null&&vwdoc.getSrcFile()==null)
		{
			vwdoc.setSrcFile(srcFile);
			vwdoc.setDstFile(dstFile);
		}
		return vwdoc;
	}

	
}
