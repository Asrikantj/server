/*
 * DSSLog.java
 *
 * Created on September 25, 2003, 12:44 PM
 */

package com.computhink.ixr.server;
import com.computhink.common.*;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;
/**
 *
 * @author  Saad
 */
import java.util.*;
import java.io.*;
import java.awt.Color;
import javax.swing.text.html.*;
import javax.swing.text.Document;
import javax.swing.text.*;
import javax.swing.text.rtf.RTFEditorKit;

public class IXRLog implements IXRConstants
{
    private static Vector Log = new Vector();
    private static LogMessage oldlog = new LogMessage("", LogMessage.MSG_INF);
    private static LogMessage oldwar = new LogMessage("", LogMessage.MSG_WAR);
    private static LogMessage olderr = new LogMessage("", LogMessage.MSG_ERR);
    private static LogMessage olddbg = new LogMessage("", LogMessage.MSG_DBG);


    public static void add(String log)
    {
        if (!IXRPreferences.getLogInfo()) return;
        LogMessage lm = createMsg(log, LogMessage.MSG_INF);
        if (lm.equals(oldlog)) return;
        append(lm);
        oldlog = lm;
        flushWhenFull();
    }
    public static void war(String war)
    {
        LogMessage lw = createMsg(war, LogMessage.MSG_WAR);
        if (lw.equals(oldwar)) return;
        append(lw);
        oldwar = lw;
    }
    public static void dbg(String dbg)
    {
        if (!IXRPreferences.getDebugInfo()) return;
        LogMessage ld = createMsg(dbg, LogMessage.MSG_DBG);
        if (ld.equals(oldwar)) return;
        append(ld);
        olddbg = ld;
    }
    public static void err(String err)
    {
        LogMessage le = createMsg(err, LogMessage.MSG_ERR);
        if (le.equals(olderr)) return;
        append(le);
        olderr = le;
        writeErrorFile(le.fullMsg);
    }
    private static void append(LogMessage lx)
    {
        Log.add(lx);
        flushWhenFull();
    }
    private static LogMessage createMsg(String msg, int type)
    {
        msg = Util.getNow(2) + " " + msg + "\r\n";
        return new LogMessage(msg, type);
    }
    private static void flushWhenFull()
    {
        if (Log.size() >= IXRPreferences.getFlushLines())
        {
           writeLog();
        }
    }
    public static int getCount() 
    {
        return Log.size();
    }
    public static LogMessage getLog()
    {
        LogMessage log = (LogMessage) Log.get(0);
        Log.remove(0);
        return log;
    }
    public static void writeLog()
    {
    	
    	        if (Log.size() > 0){  writeLogFile(createDoc(Log));
               }        
    }
    private static Document createDoc(Vector messages)
    {    
        int size = messages.size();  if (size <= 0) return null;
        DefaultStyledDocument doc = new DefaultStyledDocument();
        SimpleAttributeSet sas = new SimpleAttributeSet();
        for (int i=0; i < size; i++)
        {
            try
            {
                LogMessage log = (LogMessage) messages.elementAt(i);
                StyleConstants.setForeground(sas, log.color);
                doc.insertString(doc.getLength(), log.fullMsg , sas);
            }
            catch(Exception e){}
        }
  //      messages.removeAllElements();
        return doc;
    }

//Log Error issue. Function modified
    private static void writeErrorFile(String msg)
    {
        try
        {
        	int maxFileCount = IXRPreferences.getLogFileCount();
        	int maxLineCount = IXRPreferences.getLogFileLength();
        	
        	String line="";
        	int lineCount=0;
            File errFile = new File(assertLogFolder() + ERROR_LOG_FILE);
            BufferedReader br = new BufferedReader (new FileReader(errFile));
            while((line=br.readLine())!=null){
            	lineCount++;
            }
            line="";
            if(lineCount > maxLineCount){
            	try{
	    			FileInputStream fin = new FileInputStream (errFile);
	    			byte by[]= new byte[fin.available()];
	    			fin.read(by);
	    			Date date=new Date();
	    			String fileName = String.valueOf(System.currentTimeMillis());
	            	BufferedReader br1 = new BufferedReader (new FileReader(errFile));
	            	File file = new File(assertLogFolder() + "IXRErrors_" + fileName+".log");
	            	FileOutputStream fout = new FileOutputStream(file);
	            	fout.write(by);
	            	FileOutputStream fos = new FileOutputStream(errFile);
	                fos.write(msg.getBytes());
	                fos.close();
            	}catch(Exception e){
           	}
         }
         else{
           	FileOutputStream fos = new FileOutputStream(errFile, true);
            fos.write(msg.getBytes());
            fos.close();
         }
            
            deleteOldFiles(assertLogFolder(),maxFileCount,".log");
            
      }catch(Exception ex){

      }
    }
    
    //Function deletes old files, based on registry entry count.
    public static void deleteOldFiles(String path, int maxCount,String type){
    	try{  
			int curCount=0;
			String curFile="";
			String filePath = path;
			TreeSet sortedFiles = new TreeSet();
			File f=new File(filePath);
			String files[] = f.list();
			for(int fileCount=0;fileCount<files.length;fileCount++){
				if(files[fileCount].endsWith(type)){
					sortedFiles.add(files[fileCount]);
				}
			}
			String strFiles[] = new String[sortedFiles.size()];
			
			Iterator fileIterator = sortedFiles.iterator();
				while(fileIterator.hasNext()){
					curFile = String.valueOf(fileIterator.next());
					strFiles[curCount] = curFile;
					curCount++;
				}
				for(int fCount=0; fCount < (strFiles.length-maxCount); fCount++){
						File oldFile = new File(filePath+"\\"+strFiles[fCount]);
						oldFile.delete();
				}
    	}catch(Exception e){
    	
    	}
    }
    private static void writeLogFile(Document doc)
    {
        try
        {
        	if(doc.getLength()==0)return;
            File logFile = new File(assertLogFolder() + LOG_FILE_PREFIX + 
                                                       Util.getNow(1) + ".rtf");
            FileOutputStream fos = new FileOutputStream(logFile);
            new RTFEditorKit().write(fos, doc, 0, doc.getLength());
            fos.close();    
            Log.clear();
    //          Issue 562
     //        * 
      //       * 
       //      *               
           int maxFileCount =IXRPreferences.getLogFileCount();
           deleteOldFiles(assertLogFolder(),maxFileCount, ".rtf");
       //    Util.maintainLogFiles(assertLogFolder(),LOG_FILE_PREFIX,maxFileCount);           
           //End of fix 562  
        }
        catch(Exception e){}
    }
    private static String assertLogFolder()
    {
        File folder = new File(Util.checkPath(IXRUtil.getHome() + Util.pathSep + LOG_FOLDER));
        try
        {
            if (!folder.exists()) folder.mkdir();
            return folder.getPath() + Util.pathSep;
        }
        catch(Exception e){}
        return "c:" + Util.pathSep;
    }
    

}
