/*
 * VWSPreferences.java
 *
 * Created on 22 ����� ������, 2003, 03:21 �
 */
package com.computhink.ixr.server;
import com.computhink.common.*;
/**
 *
 * @author  Administrator
 */
import java.util.prefs.*;
import java.util.*;

public class IXRPreferences implements IXRConstants
{
    private static Preferences VWSPref;
    
    static
    {
        VWSPref= Preferences.systemRoot().node(IXR_PREF_ROOT);
    }
    private static Preferences getNode(String node)
    {
        return VWSPref.node(node);
    }
    public static void setComPort(int port)
    {
        getNode("host").putInt("comport", port);
        flush();
    }
    public static void setIdleTimeout(int timeout)
    {
        getNode("host").putInt("idletimeout", timeout);
        flush();
    }
    public static int getComPort()
    {
        return getNode("host").getInt("comport", DEFAULT_COM_PORT);
    }
    public static int getIdleTimeout()
    {
        return getNode("host").getInt("idletimeout", DEFAULT_IDLE_TIME);
    }
     public static void setDataPort(int port)
    {
        getNode("host").putInt("dataport", port);
        flush();
    }
    public static int getDataPort()
    {
        return getNode("host").getInt("dataport", DEFAULT_DATA_PORT);
    }
    public static void setProxy(boolean b)
    {
        getNode("proxy").putBoolean("enabled", b);
        flush();
    }
    public static void setProxyHost(String server)
    {
        getNode("proxy").put("host", server);
        flush();
    }
    public static void setProxyPort(int port)
    {
        getNode("proxy").putInt("port", port);
        flush();
    }
    public static boolean getProxy()
    {
        return getNode("proxy").getBoolean("enabled", false);
    }
    public static String getProxyHost()
    {
        return getNode("proxy").get("host", "");
    }
    public static int getProxyPort()
    {
        return getNode("proxy").getInt("port", DEFAULT_PROXY_PORT);
    }
    public static int getSnooze()
    {
        return getNode("general").getInt("snooze", DEFAULT_SNOOZE);
    }
// get the Monitor Thread Snoozing time
    public static int getMonitorSnooze()
    {
        return getNode("general").getInt("monitor", DEFAULT_MONITOR_SNOOZE);
    }    
    public static void setSnooze(int s)
    {
        getNode("general").putInt("snooze", s);
        flush();
    }
    public static int getFlushLines()
    {
        return getNode("general").getInt("loglines", DEFAULT_LOG_LINES);
    }
    public static String getLanguage()
    {
        return getNode("general").get("language", "English");
    }
    public static void setLanguage(String lang)
    {
        getNode("general").put("language", lang);
        flush();
    }
    public static String getEngine()
    {
        return getNode("general").get("engine", "AUTO");
    }
    public static void setEngine(String engine)
    {
        getNode("general").put("engine", engine);
        flush();
    }
    public static String getCodePage()
    {
        return getNode("general").get("codePage", "Windows ANSI");
    }
    public static void setCodePage(String codePage)
    {
        getNode("general").put("codePage", codePage);
        flush();
    }
    public static void setVWS(ServerSchema vws)
    {
        getNode("vws").put("host", vws.address);
        getNode("vws").putInt("port", vws.comport);
        flush();
    }
    public static ServerSchema getVWS()
    {
        ServerSchema ss = new ServerSchema();
        ss.address = getNode("vws").get("host", "");
        ss.comport = getNode("vws").getInt("port", 0);
        ss.type = SERVER_VWS;
        return ss;
    }
    public static boolean getLogInfo()
    {
        return getNode("general").getBoolean("loginfo", true);
    }
    public static void setLogInfo(boolean b)
    {
        getNode("general").putBoolean("loginfo", b);
        flush();
    }
    public static boolean getDebugInfo()
    {
        return getNode("general").getBoolean("debuginfo", false);
    }
    public static void setDebugInfo(boolean b)
    {
        getNode("general").putBoolean("debuginfo", b);
        flush();
    }
    public static String getManager()
    {
        String man = getNode("general").get("manager", "admin");
        String pas = getNode("general").get("manpass", "manager");
        //man = Util.decryptKey(man);
        if (!pas.equals("manager")) pas = Util.decryptKey(pas);
        return man + "||" + pas;
    }
    public static boolean setManager(String man, String pas)
    {
        try
        {
            pas = Util.encryptKey(pas);
            getNode("general").put("manager", man);
            getNode("general").put("manpass", pas);
            flush();
            return true;
        }
        catch(Exception e){}
        return false;
    }
    // Issue 562
    public static void setLogFileCount(int count)
    {
        getNode("general").putInt("maxlogfilecount", count);
        flush();
    }    
    public static int getLogFileCount()
    {
        return getNode("general").getInt("maxlogfilecount",100);
    }    
    //Issue 562
    
    protected static void setLogFileLength(int fLength)
    {
         Preferences genPref = getNode("log"); 
         genPref.putInt("flength",fLength);        
    }
    public static int getLogFileLength()
    {
        return getNode("log").getInt("flength",500000); 
    } 
// Enhancement for Indexer - selected room will process    
    public static String getSelectedRooms()
    {
        return getNode("general").get("selectedrooms", "");
    }
    public static void setSelectedRooms(String selectedRooms)
    {
        getNode("general").put("selectedrooms", selectedRooms);
        flush();
    }
// Enhancement for Indexer - selected room will process
    
    public static String getAddedRooms()
    {
        return getNode("general").get("addedrooms", "");
    }
    public static void setAddedRooms(String selectedRooms)
    {
        getNode("general").put("addedrooms", selectedRooms);
        flush();
    }
    static private void flush()
    {
        try
        {
            VWSPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }
    //Registry added for indexer restart.

    public static int getDocCount()
    {
    	return getNode("general").getInt("ixrprocessDocCount",0);
    }
    public static void setDocCount(int docCount) {
    	getNode("general").putInt("ixrprocessDocCount", docCount);
    	flush();
    }

    public static String getIXRRST()
    {
    	return getNode("general").get("ixrpollingtime", "0");
    }
    public static void setIXRRST(String engine)
    {
    	getNode("general").put("ixrpollingtime", engine);
    	flush();
    }
    public static int getAutoRstMaxDocCount()
    {
    	return getNode("general").getInt("ixrmaxdoccount",10);
    }
    public static boolean getAutoRstdebug()
    {
    	 return getNode("general").getBoolean("ixrautorestart",false);
    }
  
}
