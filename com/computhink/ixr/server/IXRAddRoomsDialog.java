package com.computhink.ixr.server;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import com.computhink.resource.ResourceManager;
import com.computhink.common.Constants;
import com.computhink.vwc.VWClient;

public class IXRAddRoomsDialog extends JDialog implements Constants {

	IXRSettingsPanel ixrSettingsPanel = null;

	JPanel mainPanel = null;
    private ResourceManager resourceManager=null;
	JLabel lblRoomName = null;

	JTextField txtRoomName = null;

	JButton btnAdd = null, btnCancel = null;

	public IXRAddRoomsDialog(IXRSettingsPanel ixrSettingsPanel) {
		this.ixrSettingsPanel = ixrSettingsPanel;
		initComponents();
	}

	private void initComponents() {
	    resourceManager=ResourceManager.getDefaultManager();
		int width = 400, height = 147;
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(null);
		setModal(true);
		setTitle(PRODUCT_NAME);
		setSize(width, height);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width - width) / 2,
				(screenSize.height - height) / 2);
		setResizable(false);

		mainPanel = new JPanel();
		mainPanel.setLayout(null);
		mainPanel.setBackground(getPanelBackGroundColor());
		mainPanel.setBounds(10, 10, 370, 60);
		mainPanel.setBorder(new TitledBorder(resourceManager.getString("IXRAddRoom.Title")+" "+ PRODUCT_NAME +" "+ resourceManager.getString("IXRAddRoom.Title1")));

		lblRoomName = new JLabel(resourceManager.getString("IXRAddRoom.RoomName"));
		lblRoomName.setBounds(10, 23, 80, 20);
		// lblRoomName.setBorder(new LineBorder(Color.red));
		mainPanel.add(lblRoomName);

		txtRoomName = new JTextField();
		txtRoomName.setBounds(90, 23, 265, 20);
		txtRoomName.setToolTipText(resourceManager.getString("IXRAddRoom.ToolTipRoomName"));
		mainPanel.add(txtRoomName);

		btnAdd = new JButton(resourceManager.getString("IXRAddRoomBtn.Add"));
		btnAdd.setBounds(208, 80, 80, 20);
		getContentPane().add(btnAdd);

		btnCancel = new JButton(resourceManager.getString("IXRAddRoomBtn.Cancel"));
		btnCancel.setBounds(298, 80, 80, 20);
		getContentPane().add(btnCancel);

		SymAction symAction = new SymAction();
		btnAdd.addActionListener(symAction);
		btnCancel.addActionListener(symAction);

		SymKey symKey = new SymKey();
		txtRoomName.addKeyListener(symKey);
		btnAdd.addKeyListener(symKey);
		btnCancel.addKeyListener(symKey);

		getContentPane().add(mainPanel);
		getContentPane().setBackground(getPanelBackGroundColor());
		setVisible(true);

	}

	class SymAction implements ActionListener {

		public void actionPerformed(ActionEvent event) {
			Object obj = event.getSource();
			if (obj == btnAdd) {
				BtnAdd_actionPerformed(event);

			} else if (obj == btnCancel) {
				BtnCancel_actionPerformed(event);
			}

		}

	}

	class SymKey extends java.awt.event.KeyAdapter {
		public void keyTyped(java.awt.event.KeyEvent event) {
			Object object = event.getSource();
			if (event.getKeyText(event.getKeyChar()).equals("Enter")) {
				btnAdd.requestFocus();
				BtnAdd_actionPerformed(null);
			} else if (event.getKeyText(event.getKeyChar()).equals("Escape")) {
				btnCancel.requestFocus();
				BtnCancel_actionPerformed(null);
			}
		}
	}

	void BtnAdd_actionPerformed(java.awt.event.ActionEvent event) {
		VWClient.printToConsole("btnAdd clicked...");
		System.out.println("btnAdd clicked...");

		String roomName = txtRoomName.getText();
		VWClient.printToConsole("room Name : " + roomName);

		if (ixrSettingsPanel.isValidRoomName(roomName)) {
			ixrSettingsPanel.addRoom(roomName);
			String addedRooms = IXRPreferences.getAddedRooms();
			//if (addedRooms != null && addedRooms.trim().length() > 0) {
				if (!addedRooms.contains(roomName)) {
					addedRooms += roomName + ";" ;
				}
			/*} else {
				addedRooms = roomName;
			}*/
			IXRPreferences.setAddedRooms(addedRooms);
			closeDialog();
		} else {
			JOptionPane.showMessageDialog(this, resourceManager.getString("IXRAddRoomMsg.ValidRoomName"),
					PRODUCT_NAME, JOptionPane.ERROR_MESSAGE);
			txtRoomName.requestFocus();
		}
	}

	void BtnCancel_actionPerformed(java.awt.event.ActionEvent event) {
		VWClient.printToConsole("btnCancel clicked...");
		System.out.println("btnCancel clicked...");

		closeDialog();
	}

	private void closeDialog() {
		setVisible(false);
		dispose();
	}

	public Color getPanelBackGroundColor() {
		return new Color(238, 242, 244);
	}

	public static void main(String[] args) {
		IXRSettingsPanel ixr = new IXRSettingsPanel();
		IXRAddRoomsDialog dlg = new IXRAddRoomsDialog(ixr);
	}
}
