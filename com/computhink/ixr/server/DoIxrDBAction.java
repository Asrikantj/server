/*
 * DoIxrDBAction.java
 *
 * Created on Jan 15, 2009
 * This class is used to be gateway for Indexer related stored procedure to execute 
 */
package com.computhink.ixr.server;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;

import oracle.jdbc.OracleTypes;
//import oracle.jdbc.driver.OracleTypes;

import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.database.Database;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWSPreferences;

public class DoIxrDBAction implements ViewWiseErrors{
   
    public static int get(Database db, String Act, String par, Vector<String> ret)
    {
    	return get(null, db, Act, par, ret);
    }
    public static int get(Client client, Database db, String Act, String par, Vector ret)
    {
    	String user = "";
    	String ipAddress = "";
    	String sid = "";
    	String clientType = ""; 
    	String isAdmin = " ";
    	if ( client != null){
    	    user = client.getUserName();
    	    ipAddress = client.getIpAddress();
    	    sid = String.valueOf(client.getId());
    	    clientType  = String.valueOf(client.getClientType());
    	    isAdmin=(client.isAdmin()?"1":"0");
    	    clientType=clientType+Util.SepChar+isAdmin+Util.SepChar;	 
    	}
        String engine = db.getEngineName();
         if (engine.equalsIgnoreCase("Oracle"))
         					return getOra(db, Act, par, ret, sid, user, ipAddress, clientType);
         else if (engine.equalsIgnoreCase("SQLServer"))
        	 				return getSql(db, Act, par, ret, sid, user, ipAddress, clientType);
         else if (engine.equalsIgnoreCase("Sybase"))
                            return getSyb(db, Act, par, ret);
         else return Error;
    }
    public static int getOra(Database db, String Act, String Params, Vector<String> ret, String sid, String user, String ipAddress, String clientType)
    {
        CallableStatement cs = null;
        ResultSet rs  = null;
        Connection con = null; 
        Params = appendTab(Params);
        //System.out.println(Act + "==>" + Params + "==>");
        if (VWSPreferences.getDebug())
        	IXRLog.war(Act + "==>" + Params + "==>"); 
        try
        {
            con = db.aquireConnection();
            cs = con.prepareCall("{ Call ViewWiseActions(?,?,?,?,?,?,?) }");
            cs.setString(1, Act); 
            cs.setString(2, Params); 
            cs.setString(3, sid); 
            cs.setString(4, user);
            cs.setString(5, ipAddress);
            cs.setString(6, clientType);
            cs.registerOutParameter(7,OracleTypes.CURSOR);
            cs.executeQuery();
            rs = (ResultSet) cs.getObject(7);
            if (rs != null)
            {
                while(rs.next())
                {
                    String str = rs.getString("Fld");
                    //System.out.println(str);
                    ret.add(str /*rs.getString("Fld")*/);
                   ///VWSLog.war("str ->" + str); 
                    if (VWSPreferences.getOutputDebug())
                	IXRLog.add("Output Fld:->"+db.getName()+":" + str); 
                }
                rs.close();
                rs = null;
            }
            cs.close(); cs = null;
        }
        catch (SQLException e) 
        {
        	IXRLog.err("Room: "+db.getName()+" "+Act + ":" + e.getMessage()+" Params: '"+Params+"'");
            return Error;
        }
        finally {
        	//Make sure that result set and connection is closed
        		try{
        			if (rs != null) {rs.close();rs = null;}
       			}catch (SQLException e){}
           		try{
           			if (cs != null) {cs.close();cs = null;}
           		}catch (SQLException e){}           		
        	assertConnection(db, con);}
        return NoError;       
    }
    public static int getSql(Database db, String Act, String Params, Vector<String> ret, String sid, String user, String ipAddress, String clientType)
    {
        CallableStatement cs = null;
        ResultSet rs  = null;
        Connection con = null;
        Params = appendTab(Params);
        InputStream paramsInputStream = null;
        //System.out.println(Act + "==>" + Params + "==>");
        if (VWSPreferences.getDebug())
        	IXRLog.war(Act + "==>" + Params + "==>"); 
        try
        {        	
            con = db.aquireConnection();            
            cs = con.prepareCall("{ Call ViewWiseActions(?,?,?,?,?,?) }");
            cs.setString(1, Act); 
            /*
            Issue No / Purpose:  <622>
            Created by: <Pandiya Raj.M>
            Date: <07 Jul 2006>           
            Since the charset is "UNICODE" (set by the JDBC driver), each character takes up 2 bytes and 
            only 4000 characters are processed. When the page count exceeds a certain value (ex: 500), 
            "Params" is not passed to the stored procedure and an error is displayed "Cannot convert Ntext to Varchar".  
            This issue can be resolved by replacing the "setString()" method with the "setAsciiStream()". 
            For reference,It is explained in the  Program Specification "Program Specification_622.doc"             
            */            
            if(Params.length()>3999)
            {
               paramsInputStream = new ByteArrayInputStream(Params.getBytes("UTF-8"));
               cs.setAsciiStream(2, paramsInputStream,paramsInputStream.available());
            }else
            	cs.setString(2, Params);
            cs.setString(3, sid); 
            cs.setString(4, user);
            cs.setString(5, ipAddress);
            cs.setString(6, clientType); 
            rs = cs.executeQuery();
            if (rs != null)
            {
                while(rs.next())
                {
                    String str = rs.getString("Fld");
                    //System.out.println(str);
                    ret.add(str /*rs.getString("Fld")*/);
                    ///VWSLog.add(str);
                    if (VWSPreferences.getOutputDebug())
                	IXRLog.add("Output Fld:->"+db.getName()+":" + str); 
                }
                rs.close();
                rs = null;
            }
            cs.close(); cs = null;
        }
        catch (UnsupportedEncodingException e) 
        {
            return Error;
        }
        catch (IOException e) 
        {
            return Error;
        }
        catch (SQLException e) 
        {
            IXRLog.err("Room: "+db.getName()+" "+Act + ":" + e.getMessage()+" Params: '"+Params+"'");
            if ((e.getMessage().toUpperCase()).indexOf("CONNECTION RESET BY PEER") > -1 ||
                	(e.getMessage().toUpperCase()).indexOf("CONNECTION REFUSED") > -1){
/*            	if (db.isDbConnected()){
	                	ConnectDatabase connectDB = new DoDBAction().new ConnectDatabase(db);
	                	db.setDbConnected(false);  
	                	connectDB.start();
            	}*/
                }
            return Error;
        }
        finally {
        	//Make sure that result set and connection is closed        	
    		try{
    			if (rs != null) {rs.close();rs = null;}
   			}catch (SQLException e){}
       		try{
       			if (cs != null) {cs.close();cs = null;}
       		}catch (SQLException e){}           		        	
       		try{
       			if (paramsInputStream != null) {paramsInputStream.close();cs = null;}
       		}catch (IOException e){}           		        	

       		assertConnection(db, con);}
        return NoError;
    }
    public static int getSyb(Database db, String Act, String Params, Vector<String> ret)
    {
        CallableStatement cs = null;
        ResultSet rs  = null;
        Connection con = null;  
        Params = appendTab(Params);
        try
        {
            con = db.aquireConnection();
            cs = con.prepareCall("{ Call ViewWiseActions(?,?) }");
            cs.setString(1, Act); 
            cs.setString(2, Params); 
            rs = cs.executeQuery();
            if (rs != null)
            {
                while(rs.next())
                {
                    String str = rs.getString("Fld");
                    ret.add(str /*rs.getString("Fld")*/);
                }
                rs.close();
                rs = null;
            }
            cs.close(); cs = null;
        }
        catch (SQLException e) {IXRLog.err(e.getMessage()); return Error;}
        finally {
    		try{
    			if (rs != null) {rs.close();rs = null;}
   			}catch (SQLException e){}
       		try{
       			if (cs != null) {cs.close();cs = null;}
       		}catch (SQLException e){}         	
        	assertConnection(db, con);}
        return NoError;
    }
    private static String appendTab(String Params)
    {
        if( Params != null && 
            !Params.equals("") && 
            Params.indexOf(Util.SepChar) > 0 && 
            !Params.endsWith(Util.SepChar)) 
            Params += Util.SepChar; 
        return Params;
    }
    private synchronized static void assertConnection(Database db, Connection con)
    {
        if (con == null) return;
        try
        {
            con.commit();                       
            db.releaseConnection(con);
            db.dbFailed = 0;
            db.setDbConnected(true);
        }
        catch (Exception e)
        {     
        	// Close the Room If Db is not available. To Solve the Issue No 501
        	String Msg = e.getMessage();
        	IXRLog.err(Msg);
        	try {
/*                if ((e.getMessage().toUpperCase()).indexOf("CONNECTION RESET BY PEER") > -1 ||
                	(e.getMessage().toUpperCase()).indexOf("CONNECTION REFUSED") > -1){
                	ConnectDatabase connectDB = new DoDBAction().new ConnectDatabase(db);
                	db.setDbConnected(false);  
                	connectDB.start();                	              	               
                }else if(Msg.toUpperCase().indexOf("SHUTDOWN") != -1
					|| Msg.toUpperCase().indexOf("CONNECTION ABORT") != -1
					|| Msg.toUpperCase().indexOf("ERROR ESTABLISHING SOCKET") != -1
					|| Msg.toUpperCase().indexOf("SOCKET WRITE ERROR") != -1
					|| Msg.toUpperCase().indexOf("CONNECTION RESET") != -1
					|| Msg.toUpperCase().indexOf("CLOSED CONNECTION") != -1) {    		
    		CIServer cServer = (CIServer) Util.getServer(Util.SERVER_VWS,db.serverIP,db.serverPort);
    		VWS vws = (VWS) cServer;
    		vws.closeRoom(db.getName());
            VWSLog.err("Connection orphaned ::: " + e.getMessage());                       
        }*/
      }catch(Exception ex){}  
    }
    }
  }
