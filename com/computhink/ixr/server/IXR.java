package com.computhink.ixr.server;

import com.computhink.common.*;
import java.rmi.*;
import java.util.*;

public interface IXR extends CIServer
{
    public ServerSchema getVWS() throws RemoteException; 
    public void setVWS(ServerSchema vws) throws RemoteException; 
    public void startWorkers() throws RemoteException;
    //Function for issue 805
    public void startWorker(String roomName) throws RemoteException;
    //public void shutDown() throws RemoteException;
    public void addtoQueue(String room, int docID, int reIndex) throws RemoteException;
    public void processDoc(String room, int docId, String docName) throws RemoteException;
    public Vector getRejectedDocs(String room, int docCount) throws RemoteException;
    // Add the method to get the Processing Document
    public Vector getProcessingDocs(String room, int docCount) throws RemoteException;
    
    /*
     *  Issue No -  QC Indexer
     *  Issue description - In the QC Indexer, when Pending documents is pressed, 
     *  All documents from Indexer table that has a value of qc > 0 be fetched and 
     *  appended to the list and the the status of  those docs set to "P" indicating 
     *  Page Errors, so we know the Indexer processed that document with page errors.
     *  
     *  Developer - Nebu Alex
     * **/
    public Vector getPageErrosDocs(String room, int docCount) throws RemoteException;
    // End of Fix
    
    public Vector getQueuedDocs(String room, int docCount) throws RemoteException;
    public String processPage(String room, int did, int page) 
                                                         throws RemoteException;
    public Vector getViewPageDetails(String room, int nodeId) throws RemoteException;
    public boolean manualInsertPage(String room, int did, int pno, String txt) 
                                                         throws RemoteException;
    public int getSnooze() throws RemoteException; 
    public void setSnooze(int s) throws RemoteException; 
    public void setIdleTimeout(int timeout) throws RemoteException;
    public String getLanguage() throws RemoteException; 
    public void setLanguage(String l) throws RemoteException; 
    public String getEngine() throws RemoteException; 
    public int getIdleTimeout() throws RemoteException;
    public void setEngine(String e) throws RemoteException;
    public String getCodePage() throws RemoteException;
    public void setCodePage(String c) throws RemoteException;
    // Enhancement for Indexer - selected room will process
    public void setSelectedRooms(String selectedRoom) throws RemoteException;
    public String getSelectedRooms() throws RemoteException;
    // Enhancement for Indexer - selected room will process
}

