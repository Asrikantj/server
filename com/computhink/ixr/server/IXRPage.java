/*
 * IXRPage.java
 *
 * Created on March 23, 2004, 12:14 PM
 */
package com.computhink.ixr.server;
/** 
 * 
 * @author Saad
 * @version 1.0
 */

public class IXRPage
{
    private int ordinal;
    private int id;
    private int version;
    private String file;
    private String pageName;
    public IXRPage(int ordinal, int id, int version){
        this.ordinal = ordinal;
        this.id = id;
        this.version = version;        
    }
    
    
    public IXRPage(int ordinal, int id, int version, String file){
        this.ordinal = ordinal;
        this.id = id;
        this.version = version;
        this.file = file;
    }
    
    public IXRPage(int ordinal, int id, int version, String file, String pageName)
    {
        this.ordinal = ordinal;
        this.id = id;
        this.version = version;
        this.file = file;
        this.pageName = pageName;
    }
    /** Getter for property ordinal.
     * @return Value of property ordinal.
     */
    public int getOrdinal() {
        return ordinal;
    }
    
    /** Setter for property ordinal.
     * @param ordinal New value of property ordinal.
     */
    public void setOrdinal(int ordinal) {
        this.ordinal = ordinal;
    }
    
    /** Getter for property id.
     * @return Value of property id.
     */
    public int getId() {
        return id;
    }
    
    /** Setter for property id.
     * @param id New value of property id.
     */
    public void setId(int id) {
        this.id = id;
    }
    
    /** Getter for property version.
     * @return Value of property version.
     */
    public int getVersion() {
        return version;
    }
    
    /** Setter for property version.
     * @param version New value of property version.
     */
    public void setVersion(int version) {
        this.version = version;
    }
    
    /** Getter for property file.
     * @return Value of property file.
     */
    public java.lang.String getFile() {
        return file;
    }
    
    /** Setter for property file.
     * @param file New value of property file.
     */
    public void setFile(java.lang.String file) {
        this.file = file;
    }

	/**
	 * @return the fileName
	 */
	public String getPageName() {
		return pageName;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setPageName(String pageName) {
		this.pageName = pageName;
	}
    
}