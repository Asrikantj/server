/*
 * IXRDoc.java
 *
 * Created on March 23, 2004, 12:14 PM
 */
package com.computhink.ixr.server;
/** 
 * 
 * @author Saad
 * @version 1.0
 */
import java.util.*;

public class IXRDoc
{
    private int id;
    private String docName = "";
    private int docNotificationExist = 0;
    private Vector<IXRPage> pages = new Vector<IXRPage>();
    
    public IXRDoc(int id)
    {
        this.id = id;
    }
    public IXRDoc(int id, String docName)
    {
        this.id = id;
        this.docName = docName;
    }
    public IXRDoc(int id, String docName, int docNotificationExist){
    	this.id = id;
        this.docName = docName;
        this.docNotificationExist = docNotificationExist;
    }
    public void setPages(String dbString)
    {
        pages.removeAllElements();
        if (dbString == null || dbString.equals("")) return;
        dbString = prepareForTokenizer(dbString);
        try
        {
            StringTokenizer st = new StringTokenizer(dbString, "\t");
            while (st.hasMoreTokens())
            {
                int ordinal = Integer.parseInt(st.nextToken());
                int id = Integer.parseInt(st.nextToken());
                int version = Integer.parseInt(st.nextToken());
                String pageName = st.nextToken();
                String file = st.nextToken();
                IXRPage page = new IXRPage(ordinal, id, version, file, pageName);
                pages.addElement(page);
            }
        }
        catch(Exception e){}
    }
    public void setPages(String dbString, boolean flag)
    {
        pages.removeAllElements();
        if (dbString == null || dbString.equals("")) return;
        dbString = prepareForTokenizer(dbString);
        try
        {
            StringTokenizer st = new StringTokenizer(dbString, "\t");
            while (st.hasMoreTokens())
            {
                int ordinal = Integer.parseInt(st.nextToken().trim());
                int id = Integer.parseInt(st.nextToken().trim());
                int version = Integer.parseInt(st.nextToken().trim());
                //String pageName = st.nextToken();
                //String file = st.nextToken();
                IXRPage page = new IXRPage(ordinal, id, version);
                pages.addElement(page);
            }
        }
        catch(Exception e){
            
        }
    }
    private String prepareForTokenizer(String srs)
    {
      final String SepChar= "\t";
      int i = 0;
      
      while( srs.indexOf(SepChar + SepChar , i) >0)
      {
          i = srs.indexOf(SepChar + SepChar,i);
          srs = srs.substring(0,i+1)+" "+srs.substring(i+1) ;
      }
      if (srs.endsWith("\t")) srs += " ";
      return srs;
    }

    public Vector getPages() 
    {
        return pages;
    }
    public int getId() 
    {
        return id;
    }
    public String getDocName(){
    	return docName;
    }
	public int getDocNotificationExist() {
		return docNotificationExist;
	}
	public void setDocNotificationExist(int docNotificationExist) {
		this.docNotificationExist = docNotificationExist;
	}
}