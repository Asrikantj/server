/*
 * ViewWiseIndexer.java
 *
 * Created on March 17, 2002, 12:14 AM
 */
package com.computhink.ixr.server;
import com.computhink.common.*;
import com.computhink.vws.server.*;
/**
 *
 * @author  Saad
 * @version 
 */
import java.util.*;
import javax.swing.text.html.*;
//import javax.swing.text.*;
import java.rmi.*;
import java.rmi.server.*;
import java.text.*;
import java.io.*;
import java.net.*;

public class IXRServer extends UnicastRemoteObject implements IXR, DocServer,
                                                                SCMEventListener
{
    private ServerSchema mySchema;
    private Hashtable workers = new Hashtable();
    private long stime;
    private boolean shutDownRequest = false;

    private static ServerSchema  managerSchema = null;
    private MonitorThread monitor = this.new MonitorThread();
    
    private Vector selectedRoomsList = new Vector();    
    public IXRServer(ServerSchema s) throws RemoteException 
    {
        super(s.dataport);
        this.mySchema = s;
        SCMEventManager scm = SCMEventManager.getInstance();
        scm.addSCMEventListener(this);
        //stime = System.currentTimeMillis();
    }
    public void handleSCMEvent(SCMEvent event)
    {
        shutDown();
    }
    public void exit() throws RemoteException
    {
        shutDown();
    }
    public String getServerOS() throws RemoteException
    {
        return Util.getEnvOSName();
    }
// Enhancement for Indexer - selected room will process
    private boolean isRoomSelected(String roomName){
    	//if (selectedRoomsList.size() == 0) return true;
    	if (selectedRoomsList.contains(roomName)) return true;
    	return false;
    }
    public void startWorkers() throws RemoteException 
    {
        ServerSchema vwss = IXRPreferences.getVWS();
        VWS vws = (VWS) Util.getServer(vwss);
        if (vws == null)
        {
            IXRLog.add(PRODUCT_NAME + " Server @" + vwss.address + ":" + 
                                                    vwss.comport + " inactive");
            IXRLog.war("Listening to "+ PRODUCT_NAME +" Server...");
            vws = waitForViewWiseServer(vwss);
        }
        if (shutDownRequest) return;
        IXRLog.add("Connected to "+ PRODUCT_NAME +" Server @" + vwss.address + ":" + 
                                                                  vwss.comport);
        IXRLog.add("OCR Language set to " + IXRPreferences.getLanguage());
        IXRLog.add("OCR Engine set to " + IXRPreferences.getEngine());
        IXRLog.add("OCR CodePage set to " + IXRPreferences.getCodePage());
        RoomProperty[] rooms = null;
        try
        {
            rooms = vws.getRooms();
        }
        catch(RemoteException re)
        {
            IXRLog.err(re.getMessage());
        }
        
// Enhancement for Indexer - selected room will process
        String selectedRooms = IXRPreferences.getSelectedRooms();    	
    	StringTokenizer tokens = new StringTokenizer(selectedRooms, ";");
    	while(tokens.hasMoreTokens()){
    		selectedRoomsList.add(tokens.nextToken());
    	}
    	
        if (rooms != null)
        {
            int snoozeTime = IXRPreferences.getSnooze() * 1000;
            for (int i=0; i < rooms.length; i++)
            {
            	if (!isRoomSelected(rooms[i].getName())) continue;
                Room room = new Room(rooms[i]);
                room.setVWS(vwss);
                Worker worker = new Worker(room, snoozeTime);
                if (worker == null || !worker.canWork()) 
                {
                    IXRLog.war("Cannot start Indexer Worker for room " + 
                                                                room.getName());
                    if (worker == null)
                    IXRLog.war("Indexer Worker Object is null for room " + 
                            room.getName());
                    if (!worker.canWork()){
                        IXRLog.war("Indexer Worker Object not started database not enabled the FTS for room " + 
                                room.getName());
                    	
                    }

                    continue;
                                                                 
                }
                //new Timer().schedule(worker, snoozeTime);
                worker.schema = mySchema;
                worker.start();
                
                IXRLog.add("Started Indexer Worker for Room '" 
                                                        + room.getName() + "'");
                workers.put(room.getName(), worker);
            }
            monitor.startMonitor();
            return;
        }
        else
        {
            IXRLog.err("No registered rooms for " + PRODUCT_NAME + " Server @" + 
                                             vwss.address + ":" + vwss.comport);
            return;
        }
    }
   
    public void startWorker(String roomName) throws RemoteException 
    {
	        ServerSchema vwss = IXRPreferences.getVWS();
	        VWS vws = (VWS) Util.getServer(vwss);
	        if (vws == null)
	        {
	            IXRLog.add(PRODUCT_NAME + " Server @" + vwss.address + ":" + vwss.comport + " inactive");
	            IXRLog.war("Listening to " + PRODUCT_NAME + " Server...");
	            vws = waitForViewWiseServer(vwss);
	        }
	        if (shutDownRequest) return;
	        RoomProperty[] rooms = null;
	        try
	        {
	            rooms = vws.getRooms();
	        }
	        catch(RemoteException re)
	        {
	            IXRLog.err(re.getMessage());
	        }
	        if (rooms != null){
	            int snoozeTime = IXRPreferences.getSnooze() * 1000;
	            Room room = null;
	                for (int i=0; i < rooms.length; i++)
	                {
	                    room = new Room(rooms[i]);	                        
	                    if (room.getName().equals(roomName))  break;
	                }
	                if (room != null){
                	try{
	                    room.setVWS(vwss);
	                    Worker worker = new Worker(room, snoozeTime);
	                    if (worker == null || !worker.canWork()) {
	                        IXRLog.war("Cannot start Indexer Worker for room " + room.getName());                   
	                   }else{
	                       worker.schema = mySchema;
	                       worker.start();
	   	                   workers.put(room.getName(), worker);	           	                
	                   }
                	}catch(Exception e){
                        IXRLog.err(e.toString());	                        		
                	}
                }
	        }else{
	            IXRLog.err("No registered rooms for " + PRODUCT_NAME + " Server @" + vwss.address + ":" + vwss.comport);
	            return;
	        }
    	}

    private VWS waitForViewWiseServer(ServerSchema vwss)
    {
        VWS vws = null;
        while (vws == null && !shutDownRequest) 
        { 
            Util.sleep(1000);
            vws = (VWS) Util.getServer(vwss);
        }
        return vws;
    }
    public ServerSchema getSchema() throws RemoteException 
    {
        return this.mySchema;
    }
    public void setComPort(int port) throws RemoteException 
    {
        IXRPreferences.setComPort(port);
    }
    public void setIdleTimeout(int timeout) throws RemoteException 
    {
        IXRPreferences.setIdleTimeout(timeout);
    }
    public int getComPort() throws RemoteException
    {
        return IXRPreferences.getComPort();
    }
    public int getIdleTimeout() throws RemoteException
    {
        return IXRPreferences.getIdleTimeout();
    }
     public void setDataPort(int port) throws RemoteException
    {
        IXRPreferences.setDataPort(port);
    }
    public int getDataPort() throws RemoteException
    {
        return IXRPreferences.getDataPort();
    }
    public void setProxy(boolean b) throws RemoteException 
    {
        IXRPreferences.setProxy(b);
    }
    public void setProxyHost(String server) throws RemoteException 
    {
        IXRPreferences.setProxyHost(server);
    }
    public void setProxyPort(int port) throws RemoteException 
    {
        IXRPreferences.setProxyPort(port);
    }
    public boolean getProxy() throws RemoteException 
    {
        return IXRPreferences.getProxy();
    }
    public String getProxyHost() throws RemoteException 
    {
        return IXRPreferences.getProxyHost();
    }
    public int getProxyPort() throws RemoteException 
    {
        return IXRPreferences.getProxyPort();
    }
    public ServerSchema getVWS() throws RemoteException 
    {
        return IXRPreferences.getVWS();
    }
    public void setVWS(ServerSchema vws) throws RemoteException 
    {
        IXRPreferences.setVWS(vws);
    }
    public boolean getLogInfo() throws RemoteException
    {
        return IXRPreferences.getLogInfo();
    }
    public void setLogInfo(boolean b) throws RemoteException
    {
        IXRPreferences.setLogInfo(b);
    }
    public boolean getDebugInfo() throws RemoteException
    {
        return IXRPreferences.getDebugInfo();
    }
    public void setDebugInfo(boolean b) throws RemoteException
    {
        IXRPreferences.setDebugInfo(b);
    }
    public long getTimeOn() throws RemoteException 
    {
        return (System.currentTimeMillis() - stime) / 1000;
    }
    public LogMessage getLogMsg() throws RemoteException 
    {
        return IXRLog.getLog();
    }
    public int getLogCount() throws RemoteException
    {
        return IXRLog.getCount();
    }
    public Vector getClients() throws RemoteException 
    {
        return new Vector();
    }
    public int getSnooze() throws RemoteException 
    {
        return IXRPreferences.getSnooze();
    }
    public void setSnooze(int s) throws RemoteException 
    {
        IXRPreferences.setSnooze(s);
    }
    public String getLanguage() throws RemoteException 
    {
        return IXRPreferences.getLanguage();
    }
    public void setLanguage(String l) throws RemoteException 
    {
        IXRPreferences.setLanguage(l);
    }
    public String getEngine() throws RemoteException 
    {
        return IXRPreferences.getEngine();
    }
    public void setEngine(String e) throws RemoteException 
    {
        IXRPreferences.setEngine(e);
    }
    public String getCodePage() throws RemoteException 
    {
        return IXRPreferences.getCodePage();
    }
    public void setCodePage(String c) throws RemoteException 
    {
        IXRPreferences.setCodePage(c);
    }
    public boolean setManager(String man, String pas) throws RemoteException
    {
        return IXRPreferences.setManager(man, pas);
    }
    public String getManager() throws RemoteException
    {
        return IXRPreferences.getManager();
    }
// Enhancement for Indexer - selected room will process
    public void setSelectedRooms(String selectedRoom) throws RemoteException
    {
        IXRPreferences.setSelectedRooms(selectedRoom);
    }
    public String getSelectedRooms() throws RemoteException
    {
        return IXRPreferences.getSelectedRooms();
    }    
    public VWDoc setDocument(Document doc, String room) throws RemoteException 
    {
        VWDoc vwdoc = doc.getVWDoc();
        int ret = vwdoc.writeContents();
        if (ret < 0){
        	vwdoc.setVaildZip(false);
        }
        return vwdoc;
    }
    public boolean getDocument(Document doc,String room, String file, 
                         ServerSchema svr, boolean dummy) throws RemoteException 
    {
        //for now no one will ask indexer for documents
        return false;
    }
    public boolean getDocFolder(Document doc, String room, String path, 
                                          ServerSchema s) throws RemoteException
    {
        return false;
    }
    public VWDoc setDocFolder(Document doc, String room) throws RemoteException
    {
        return null;
    }
    
    public void addtoQueue(String room, int did, int reIndex) throws RemoteException
    {
        getWorker(room).DBAddtoQueue(did, reIndex);
    }
    public void processDoc(String room, int did, String docName) throws RemoteException
    {
        getWorker(room).processDoc(did, docName);
    }
    public String processPage(String room, int did, int pno) throws RemoteException
    {
        return getWorker(room).processPage(did, pno);
    }
    public boolean manualInsertPage(String room, int did, int pno, String txt) 
                                                          throws RemoteException
    {
        return getWorker(room).DBManualInsertPage(did, pno, txt);
    }
    public Vector getRejectedDocs(String room, int docCount) throws RemoteException
    {
        return getWorker(room).DBGetRejectedDocs(docCount);
    }
// Add the Method to get the current Processing document
    public Vector getProcessingDocs(String room, int docCount) throws RemoteException
    {
        return getWorker(room).DBGetProcessingDocs(docCount);
    }    
    /*
     *  Issue No -  QC Indexer
     *  Issue description - In the QC Indexer, when Pending documents is pressed, 
     *  All documents from Indexer table that has a value of qc > 0 be fetched and 
     *  appended to the list and the the status of  those docs set to "P" indicating 
     *  Page Errors, so we know the Indexer processed that document with page errors.
     *  
     *  Developer - Nebu Alex
     * **/
    public Vector getPageErrosDocs(String room, int docCount) throws RemoteException
    {
        return getWorker(room).DBGetPageErrorDocs(docCount);
    } 
    // End of fix
    public Vector getQueuedDocs(String room, int docCount) throws RemoteException
    {
        return getWorker(room).DBGetQueuedDocs(docCount);
    }
    public Vector getViewPageDetails(String room,int nodeId) throws RemoteException
    {
        return getWorker(room).DBGetPageDetails(nodeId);
    }
    public String managerBegin(ServerSchema manager) throws RemoteException 
    {
        if (managerSchema == null) 
        {
            managerSchema = manager;
            return "";
        }
        else
        {
            if (managerSchema.address.equals(manager.address))
                return "";
            else
                return "Cannot monitor server. Manager @" 
                                + managerSchema.address + " already connected.";
        }
    }
    public void managerEnd() throws RemoteException 
    {
        managerSchema = null;
    }
    public Worker getWorker(String room)
    {
        return (Worker) workers.get(room);
    }
    private void shutDown()
    {
        shutDownRequest = true;
        Enumeration wenum = workers.elements();
        while (wenum.hasMoreElements())
        {
            Worker worker = (Worker) wenum.nextElement();
            worker.shutDown();
            IXRLog.add("Stopped Indexer Worker for Room " + worker.roomName);
        }
        IXRLog.add("Stopping Indexer Server...");
        IXRLog.writeLog();
        Util.sleep(500);
        Util.killServer(mySchema);
    }
    public class MonitorThread extends Thread{
    	public boolean isActive;
    	Worker workerObject = null;
    	MonitorThread(){    		
    		isActive = true;    	
    	}
    	public void startMonitor(){
    		start();
    	}
    	public void run(){
	        	int snoozeTime = IXRPreferences.getMonitorSnooze() * 1000;
	    		while(isActive){
	    	        try{
	    			Enumeration workerThread = workers.elements();
	    			while(workerThread.hasMoreElements()){
	    				workerObject = null;
	    				workerObject = (Worker) workerThread.nextElement();	    				
	    				if (workerObject == null || !workerObject.indexer.isActive()){
	    					// Kill the indexer service 
	    					IXRLog.add("Indexer service is stopped by Monitor Thread.");    				
	    					Util.killServer(mySchema);
	    					isActive = false;
	    				}
	    				//IXRLog.add(workerObject.roomName + " ::: " + workerObject);
	    				if (workerObject != null && (!workerObject.isDBAvailable || !workerObject.indexer.isDBAvailable)){
	    					workerObject.shutDownRequest = true;
	    		        	ServerSchema vwss = IXRPreferences.getVWS();
	    		        	VWS vws = (VWS) Util.getServer(vwss);
	    		        	if (vws != null){
		    	        		Vector roomList = vws.getRoomNames();
		    	        		//IXRLog.add("roomList \t:"+ roomList.size() + " :: " + roomList.toString());
		    	        		//IXRLog.add(workerObject.roomName + " ::: " + roomList.contains(workerObject.roomName));
		    	        		//IXRLog.add(workerObject.roomName + " ::: DB " + vws.pingDB(workerObject.roomName));
		    	        		if (roomList.contains(workerObject.roomName) && vws.pingDB(workerObject.roomName)){
		    	        			workerObject.isDBAvailable = true;
		    	        			IXRLog.add("Connected to database...");
		    	        			startWorker(workerObject.roomName);
		    	        			//IXRLog.add(workerObject.roomName + " ::: Thread Started " );
		    	        		}
	    		        	}
	    				}
	    			}
	    			sleep(snoozeTime);
			    
	        }catch(Exception e){//IXRLog.add("Error in Monitor Thread" + e.getMessage());}    			
	        }   
    		}
    	}
    }
}
