/*
 * Indexer.java
 *
 * Created on March 22, 2002, 12:14 AM
 */
package com.computhink.ixr.server;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.io.Reader;
import java.io.Writer;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.rmi.RemoteException;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Vector;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.concurrent.Semaphore;
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
/*import oracle.jdbc.driver.OracleCallableStatement;
import oracle.jdbc.driver.OracleTypes;
*/
import oracle.sql.CLOB;
//import EDU.oswego.cs.dl.util.concurrent.Mutex;

import com.computhink.common.DocServer;
import com.computhink.common.Document;
import com.computhink.common.Room;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.common.VWDoc;
import com.computhink.common.VWFileFilter;
import com.computhink.common.ViewWiseErrors;
import com.computhink.database.Database;
import com.computhink.dss.server.DSS;
import com.computhink.pc1.CryptographyUtils;
import com.computhink.vns.server.VNSConstants;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.ZipUtil;
import com.computhink.vws.server.DoDBAction;
import com.computhink.vws.server.VWS;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.file.CloudFile;
import com.microsoft.azure.storage.file.CloudFileClient;
import com.microsoft.azure.storage.file.CloudFileDirectory;
import com.microsoft.azure.storage.file.CloudFileShare;

public class Indexer implements IXRConstants, ViewWiseErrors
{
    private String       homePath;
    private Database     database; 
    private ServerSchema schema = new ServerSchema();
    private ServerSchema vwsSchema = new ServerSchema();
    private VWS          vws;
//    private VWClient client;
    private boolean  loaded = false;
    private boolean healthy = false;
    private static boolean isWorkerActive = true;
    public boolean isDBAvailable = true;
    
    private static native String passDoc(String path, String data);
    private static native void passShutDown();
    private static native void passStartup();
    private static native String passEnumerate();
    private static native void passLanguage(String lang);
    private static native void passEngine(int style);
    private static native void passCodePage(String codePage);
    private int qc = IXR_INDEX_ALL;
    public static  int docCount=1;
    
    private int ax,bx,cx,dx,res,i,inter,cfc,cfd,compte; 
    private int max_limit = 1024 * 1024;     
    private byte cle[] = new byte [17];               // holds key    
    private int x1a0[] = new int [8];
    private static final String DEFAULT_KEY = "D36083DE-284C-4d62-8B4A-0F1537A654B0";    
    private static final String DEFAULT_SIGNATURE = "18f39b10-b4c7-11d9-9669-0800200c9a65";
    
    /*Issue/Purpose:	Indexer is giving problem when multiple room/thread access same method at one time
    * Mutex object implemented for that. 	C.Shanmugavalli		20 Feb 2007	
    */
  //  public static Mutex lock = new Mutex();
    //Added for Jarvalidity after removing concurrent.jar(Mutext class) and changed with java.util.semaphore.
    //Semaphore value set in the constructor 1 in CV83B2
    public static Semaphore lock = new Semaphore(1);
    public Indexer(Room room)
    {
        if (!loadNativeLibrary()) 
        {
            IXRLog.err("Could not load Indexer native libraries.");
            return;
        }
        database = room.getDatabase("com.computhink.ixr.server.IXRLog");
        vwsSchema = room.getVWS();
        setSchema(room.getName());
        this.healthy = true;
    }
    private void setSchema(String name)
    {
        schema = Util.getMySchema();
        schema.comport = IXRPreferences.getComPort();
        schema.type = SERVER_IXR;
        schema.room = name;
        schema.path = CreateCacheforRoom(name);
    }
    public void process(IXRDoc doc,int qc)
    {
    	this.qc = qc;
    	// Set the status as "Processing" for a document before starting the index.
    	// Document Status maintained in the 'IndexerDocs' table. column 'qc' details
    	// 0 - Queue
    	// 1 - Rejected
    	// 2 - Processing

    	DBIndexerProcessingDoc(doc);
    	
    	// End
        doc.setPages(DBGetDocData(doc.getId()), true);
        indexDoc(doc);
    }
    public void processRemote(IXRDoc doc)
    {
    	
        indexDoc(doc);
    }
    public String processPage(IXRDoc doc, int page)
    {
        String txt = "";
        int id = doc.getId();
        int ret = getDocument(id);
        if (ret == 0)
        { 
            try
            {
            	String allDotZip = getDocLocalPath(id);
            	
            	String allDotZipUptoLocation = allDotZip.substring(0, allDotZip.lastIndexOf("all.zip"));
            	
            	unzip(allDotZip, allDotZipUptoLocation);
            	
            	if(new File(allDotZipUptoLocation+"Pagesinfo.xml").exists()){
            		File allDotZipFile = new File(allDotZip);
                	
                	File[] allDotZipFileList = allDotZipFile.listFiles();
                	for(int i=0; i<allDotZipFileList.length; i++){
                		String fileName = allDotZipFileList[i].getName();
                		IXRLog.dbg("fileName inside indexDoc :::: "+fileName);
                		if(fileName.endsWith(".vw")){
                			IXRLog.dbg("FileName ends with .vw ::::: "+fileName.endsWith(".vw"));
                			String onlyFileName = fileName.substring(0, fileName.lastIndexOf("."));
                			decryptFileIndexer(allDotZipUptoLocation+File.separator+fileName, allDotZipUptoLocation+File.separator+onlyFileName+".temp");
                			
                			if(new File(allDotZipUptoLocation+File.separator+fileName).exists()){
                				new File(allDotZipUptoLocation+File.separator+fileName).delete();
                			}
                			
                			if(!new File(allDotZipUptoLocation+File.separator+fileName).exists()){
                				new File(allDotZipUptoLocation+File.separator+onlyFileName+".temp").renameTo(new File(allDotZipUptoLocation+File.separator+fileName));
                			}
                		}
                	}
                	
                    passDoc(allDotZip, "");
            	} else {
            		if(new File(allDotZipUptoLocation+"FolderdatabasePageinfo.xml").exists()){
            			passDoc(allDotZip, "");
            		}
            	}
            	
            }
            catch (Exception e){}
            
            File file = new File(getPageLocalPath(id, page)); 
            if (file.exists())
            {
                try
                {
                    BufferedReader in = new BufferedReader
                             (new InputStreamReader(new FileInputStream(file)));

                    char[] buff = new char[1024];
                    int nch;
                    while ((nch = in.read(buff, 0, buff.length)) != -1)
                        txt = txt + new String(buff, 0, nch);
                    in.close();
                }
                catch(Exception e){}
            }
        }
        removeDocFolder(id);
        return txt.trim();
    }
/*
Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
Created by: <Pandiya Raj.M>
Date: <6 Jun 2006>
*/
/*Issue/Purpose:	Indexer is giving problem when multiple room/thread access same method at one time
* So block for code is locked for one thread once job is over it will be released. 	C.Shanmugavalli		20 Feb 2007	
*/
    private void indexDoc(IXRDoc doc)
    {
        int id = doc.getId();
        //Condition added for Indexer AutoRestart CV83B5
        if(IXRPreferences.getAutoRstdebug()==true){
        	if ((docCount > IXRPreferences.getAutoRstMaxDocCount())) { 
        		return;
        	}
        }
        
        String docName = doc.getDocName();
        try{
        	//IXRLog.add("before acquire");
	    	try{
	        	lock.acquire();
	        	IXRLog.add(schema.room + ": Indexing started for Document '"+docName+"'- [" + id + "]...");
		        int ret = getDocument(id);        
		        if ( ret == 0)
		        {
		            String docData = "";

		            // Comment the following checking, when delete all the pages qc value is coming as '0' not as '3'. 
		            // set the doc pages for both 0 and 3
		            
					// Check the qc value, if qc is 3, it will send the existing indexed page information to native method passDoc()
		           	//if (qc == IXR_INDEX_UPDATE){
		           	//	IXRLog.dbg("Page Size  " + doc.getPages().size());	
			            for (int i = 0; i < doc.getPages().size(); i++ )
			            {
			                IXRPage ixrPage = (IXRPage) doc.getPages().get(i);
			                docData = docData +
			                          String.valueOf(ixrPage.getOrdinal())  + "\t" +
			                          String.valueOf(ixrPage.getId())       + "\t" +
			                          String.valueOf(ixrPage.getVersion())  + "\t" ;
			            }
		        	// }
		            if (docData.length() > 0) //remove last tab
		                docData = docData.substring(0, docData.length()-1);
		            indexDoc(doc, docData);
		            removeDocFolder(id);
		        }
		        else
		            handleError(ret, id);
	    	}finally{
	    		lock.release();
	    	}
	    
        }catch (InterruptedException e) {
    		//IXRLog.err("Error "+e.getMessage());
    		lock.release();
    	
        }catch(Exception e){
        	IXRLog.err(schema.room + ": Indexing Document '"+docName+"' - [" + id + "]failed"+e.getMessage());
        	return;
        }
    }
    private void indexDoc(IXRDoc doc, String docData)
    {
    	int id = doc.getId();
    	String ret ="";
    	String docName = doc.getDocName();
    	//IXRLog.add(schema.room + ": Indexing Process Started for Document: " + id + "...");
    	IXRLog.dbg("Native index for: " + getDocLocalPath(id));
    	IXRLog.dbg("Native index doc Data: " + docData);

    	String allDotZip = getDocLocalPath(id);
    	
    	String allDotZipUptoLocation = allDotZip.substring(0, allDotZip.lastIndexOf("all.zip"));
    	IXRLog.dbg("allDotZipUptoLocation inside indexDoc :::: "+allDotZipUptoLocation);
    	
    	unzip(allDotZip, allDotZipUptoLocation);
    	
    	if(new File(allDotZipUptoLocation+"Pagesinfo.xml").exists()){
    		File allDotZipFile = new File(allDotZipUptoLocation);

    		File[] allDotZipFileList = allDotZipFile.listFiles();
    		IXRLog.dbg("allDotZipFileList.length inside indexDoc  ::::  "+allDotZipFileList.length);
    		for(int i=0; i<allDotZipFileList.length; i++){
    			String fileName = allDotZipFileList[i].getName();
    			IXRLog.dbg("fileName inside indexDoc :::: "+fileName);
    			if(fileName.endsWith(".vw")){
    				IXRLog.dbg("FileName ends with .vw ::::: "+fileName.endsWith(".vw"));
    				String onlyFileName = fileName.substring(0, fileName.lastIndexOf("."));
    				decryptFileIndexer(allDotZipUptoLocation+fileName, allDotZipUptoLocation+onlyFileName+".temp");

    				if(new File(allDotZipUptoLocation+fileName).exists()){
    					new File(allDotZipUptoLocation+fileName).delete();
    				}

    				if(!new File(allDotZipUptoLocation+fileName).exists()){
    					new File(allDotZipUptoLocation+onlyFileName+".temp").renameTo(new File(allDotZipUptoLocation+fileName));
    				}
    			}
    		}


    		ret = passDoc(allDotZip, docData);
    	} else {
    		if(new File(allDotZipUptoLocation+"FolderdatabasePageinfo.xml").exists()){
    			ret = passDoc(allDotZip, docData);
    		}
    	}    	
    	IXRLog.dbg("Native index returned: " + ret);
    	//Added in CV83b4 to clear the doc object fix for indexer crash
    	if(IXRPreferences.getAutoRstdebug()==true){
    		long time2 = System.currentTimeMillis();
    		IXRPreferences.setIXRRST(String.valueOf(time2));        	
    		docCount=docCount+1;
    		IXRPreferences.setDocCount(docCount);
    		IXRLog.dbg("docCount value is ::"+docCount);
    	}
    	if (ret == null)    //Native Method return null.
    	{
    		IXRLog.war("\t Cannot process Document '"+docName+"' - [" + id + "]");
    		DBRemoveIndexerDoc(id, "reject");
    		return;
    	}
    	//if (ret.length() == 0)  //all pages have been deleted
    	// Check that if native method passDoc() returns "Empty"  then delete all pages.
    	if (("EMPTY").equals(ret.toUpperCase())) //all pages have been deleted
    	{
    		for (int i=0; i < doc.getPages().size(); i ++)
    		{
    			if (DBRemovePageText(id,((IXRPage)doc.getPages().get(i)).getId()))//if (DBRemovePageText(id, i))
    				IXRLog.add("\t Removed page " + (i+1));
    		}
    		DBRemoveIndexerDoc(id);
    		IXRLog.add(schema.room + ": Indexed Document '"+docName+"' - ["+ id+"]");
    		return;
    	}
    	doc.setPages(ret);
    	IXRLog.add(schema.room + ": Indexing Information for Document '"+docName+"' - ["+id+"]...");
    	int qc = 0;
    	boolean flag = false;
    	String indexedPageNo = "";
    	String failedPageNo = "";
    	for (int i=0; i < doc.getPages().size(); i ++)
    	{
    		try{
    			IXRPage ixrPage = (IXRPage) doc.getPages().get(i);
    			int pageNo = ixrPage.getOrdinal();
    			int pageId = ixrPage.getId();
    			int pageVr = ixrPage.getVersion();
    			String pageFile = ixrPage.getFile();
    			String pageName =ixrPage.getPageName();
    			qc = 0;
    			if (pageNo == 0)
    			{
    				if (DBRemovePageText(id, pageId))
    					IXRLog.add("\t Removed page Id " + pageId);
    				continue;
    			}
    			if (pageFile.equalsIgnoreCase("none"))
    			{
    				if (DBUpdatePageText(id, pageNo, pageId, pageVr))
    					IXRLog.add("\t Updated page " + pageNo);
    				continue;
    			}

    			/*
    			 * Issue # 633 	: Indexer logs "page has no text" for pages they
    			 * do have text and can be ocr'ed using the desktop client's OCR
    			 * rubber band tool. The indexer needs to be more descriptive as
    			 * to why it is failing to extract text from these documents.
    			 * Additionally there needs to be some report or state in the
    			 * database (similar to rejected) to indicate which documents/pages
    			 * failed to extract text.   Pages failing that have valid text,
    			 * present a search integrity issue and the pages need to be
    			 * indexed/re-indexed.
    			 * Created By	: M.Premananth
    			 * Date			: 10-Jul-2006
    			 */
    			if(pageFile.trim().equalsIgnoreCase("1"))
    			{
    				IXRLog.add("\t Error while loading Image for page no "+pageNo);
    				qc = 3;
    			}
    			else if(pageFile.trim().equalsIgnoreCase("2"))
    			{
    				IXRLog.add("\t Error setting Image resolution enhancement mode for page no "+pageNo);
    				qc = 4;
    			}
    			else if(pageFile.trim().equalsIgnoreCase("3"))
    			{
    				IXRLog.add("\t Error while pre-processing of image for page no "+pageNo);
    				qc = 5;
    			}
    			else if(pageFile.trim().equalsIgnoreCase("4"))
    			{
    				IXRLog.add("\t Error setting Page parser algorithm for page no "+pageNo);
    				qc = 6;
    			}
    			else if(pageFile.trim().equalsIgnoreCase("5"))
    			{
    				IXRLog.add("\t Error applying Language settings for page no "+pageNo);
    				qc = 7;
    			}
    			else if(pageFile.trim().equalsIgnoreCase("6"))
    			{
    				IXRLog.add("\t Error while locating zones for page no "+pageNo);
    				qc = 8;
    			}
    			else if(pageFile.trim().equalsIgnoreCase("7"))
    			{
    				IXRLog.add("\t Page No "+ pageNo +" has no text");
    				qc = 9;
    			}
    			else if(pageFile.trim().equalsIgnoreCase("8"))
    			{
    				IXRLog.add("\t Error while writing recognition data to file for page no "+pageNo);
    				qc = 10;
    			}
    			else if(pageFile.trim().equalsIgnoreCase("9"))
    			{
    				IXRLog.add("\t Time out while processing page no "+pageNo);
    				qc = 11;
    			}
    			if(qc >= 3 && qc <= 11){
    				failedPageNo = failedPageNo + pageNo + ",";
    			}
    			/*
    			 * Issue QCIndexer02 	: For error log 9 also 'Indexer' table should be updated 
    			 * so that indexer will not index that "No text page" again
    			 * Created By	: C.Shanmugavalli		Date: 5-Sep-2006
    			 */
    			//if(qc != 0 && qc != 9) 
    			try{
    				File file = new File(getPageLocalPath(id, pageNo));
    				if (file != null){
    					IXRLog.dbg("file is exist " + file.exists() + " : " + file.getAbsolutePath());
    				}
    				if(qc != 0 && !file.exists()){
    					file.createNewFile();
    				}                
    				pageName += "\n";
    				mergeFile(pageName, file);
    			}catch(Exception exPageNameMerge){}
    			if(qc != 0)
    			{
    				DBInsertPageText(id, pageNo, pageId, pageVr,qc);
    				continue;
    			}
    			if (new File(pageFile).exists())
    			{
    				if (DBInsertPageText(id, pageNo, pageId, pageVr)){
    					IXRLog.add("\t Indexed page " + pageNo);
    					indexedPageNo = indexedPageNo + pageNo + ",";
    					flag = true;
    				}
    				else
    					handleError(ixrTextIndexFailed, id);
    				continue;
    			}
    		}catch(Exception ex){}
    	}

    	//Set notification history for the document having notification.
    	String message = "";
    	if(indexedPageNo.length()>0){
    		indexedPageNo = indexedPageNo.substring(0,indexedPageNo.length()-1);
    		message = "Pages '"+indexedPageNo+"' indexed";
    	}
    	if(failedPageNo.length()>0){
    		failedPageNo = failedPageNo.substring(0,failedPageNo.length()-1);
    		if(indexedPageNo.length()>0)
    			message = message + " and ";
    		message = message + "Pages '"+failedPageNo+"' failed to index";
    	}
    	if(indexedPageNo.length()>0 || failedPageNo.length()>0)
    		message = message + " for Document '"+doc.getDocName()+"'"; 
    	else
    		message = "Indexed for Document '"+doc.getDocName()+"'";
    	try{
    		int nodeId = doc.getId();
    		int nodeType;
    		//getDocNotificationExist will contain the nodeId (doc or folder) to check for notification exist.
    		if(doc.getDocNotificationExist()>0){
    			nodeId = doc.getDocNotificationExist();//Parent node id
    			if(doc.getId()==doc.getDocNotificationExist()){
    				nodeType = VNSConstants.nodeType_Document;
    			}else{
    				nodeType = VNSConstants.nodeType_Folder;
    			}
    			setNotificationHistory(nodeId, nodeType, VNSConstants.notify_Doc_Indexed, message);
    		}
    	}catch(Exception ex){

    	}//Finally block added  in CV83b4 to clear the doc object fix for indexer crash
    	finally{
    		doc=null;
    	}
    	DBRemoveIndexerDoc(id);
    	IXRLog.dbg(schema.room + ": Indexed Document '"+docName+"' - [" + id+"] ::::: docCount: " + docCount);
    	//Condition added for Indexer AutoRestart CV83B5
    	if(IXRPreferences.getAutoRstdebug()){
    		if(docCount>IXRPreferences.getAutoRstMaxDocCount()){
    			IXRLog.dbg("Shut Down Request passed after document reached to 10:::"+docCount);
    			shutDown();
    			Runtime rt = Runtime.getRuntime();
    			try {
    				IXRLog.dbg("Time before stopping the indexer services:::"+Util.getNow(2));
    				long time = System.currentTimeMillis();
    				IXRPreferences.setIXRRST(String.valueOf(time));
    				String str = IXRUtil.getHome();
    				String[] tokens = str.split(":");
    				String path="cmd"+" "+"/"+tokens[0]+" "+"Net stop \"Contentverse Indexer Service\"";
    				Process proc = rt.exec(path);
    				IXRLog.add("Indexer Service Stopped for 2 min...");
    			} catch (Exception e) {
    				// TODO Auto-generated catch block
    				IXRLog.dbg("Exception inside stop service of IXR by Restart Thread: "+e.getMessage());
    			}
    		}
    	}
    }
    
    public int unzip(String zipFilePath, String destDir) {
		File dir = new File(destDir);
		// create output directory if it doesn't exist
		if(!dir.exists()) dir.mkdirs();
		FileInputStream fis;
		//buffer for read and write data to file
		byte[] buffer = new byte[1024];
		try {
			fis = new FileInputStream(zipFilePath);
			ZipInputStream zis = new ZipInputStream(fis);
			ZipEntry ze = zis.getNextEntry();
			while(ze != null){
				String fileName = ze.getName();
				File newFile = new File(destDir + File.separator + fileName);
				IXRLog.dbg("Unzipping to :::: "+newFile.getAbsolutePath());
				//create directories for sub directories in zip
				new File(newFile.getParent()).mkdirs();
				FileOutputStream fos = new FileOutputStream(newFile);
				int len;
				while ((len = zis.read(buffer)) > 0) {
					fos.write(buffer, 0, len);
				}
				fos.close();
				//close this ZipEntry
				zis.closeEntry();
				ze = zis.getNextEntry();
			}
			//close last ZipEntry
			zis.closeEntry();
			zis.close();
			fis.close();
			return 1;
		} catch (Exception e) {
			IXRLog.add("EXCEPTION While unzipping :::: "+e.getMessage());
			return -1;
		}
	}
    
    public int decryptFileIndexer(String fileIn, String fileOut) {
    	IXRLog.dbg("Inside decryptFileIndexer :::: "+fileIn+"\t"+fileOut);
    	try {
//    		CryptographyUtils utils = new CryptographyUtils();
    		IXRLog.dbg("1 !!!!");
    		decryptFile(fileIn, fileOut);
    		IXRLog.dbg("2 !!!!");
    	} catch (Exception ex) {
    		IXRLog.add("EXCEPTION while decryptFileIndexer ::::  "+ex.getMessage());
    		IXRLog.add("EXCEPTION while decryptFileIndexer ::::  "+ex.getMessage());
    		return Error;
    	}
    	return NoError;
    }
    
    public void decryptFile(String fileIn, String fileOut) {								
		FileChannel fis = null, fos = null;
		FileInputStream inputStream = null;
		FileOutputStream outputStream = null;
		ByteBuffer buffer = null, signature = null;
		File encryptFile = null, encryptedFile = null;
		try {
			if (fileIn!=null && fileOut!=null) {
				if (!fileIn.trim().equals("") && !fileOut.trim().equals("")) {
					saveKey(DEFAULT_KEY.getBytes());
					encryptFile = new File(fileIn);
					if (encryptFile.isFile() && encryptFile.exists()) {
						inputStream = new FileInputStream(encryptFile);
						fis = inputStream.getChannel();			
						buffer = ByteBuffer.allocate((int) encryptFile.length());
						@SuppressWarnings("unused")
						long checkSum = 0L;
						int nRead;
						while ((nRead = fis.read(buffer)) != -1) {
							if (nRead == 0)
								continue;
							buffer.position(0);
							buffer.limit(nRead);
							while (buffer.hasRemaining())
								checkSum += buffer.get();
							buffer.clear();
							
						}						
						fis.close();	
						inputStream.close();
						// signature verification
						int bIndex = 0;
						String fileSignature = getFileSignature();
						boolean validateSign = true;
						if (buffer.limit()>71 && fileSignature!=null) {
							for (int cIndex=0; cIndex<buffer.limit(); cIndex++) {
								if (cIndex>71 && validateSign==true) {
									if (signature==null) {
										signature = ByteBuffer.allocate((int) (buffer.limit()-72));
									}
									signature.put(bIndex, buffer.get(cIndex));
									bIndex++;
								} else {															
									System.out.print((char)buffer.get(cIndex));
									if (((char)buffer.get(cIndex)==fileSignature.charAt(cIndex))==false) {
										validateSign = false;								
									}		
								}
							}
						}
						IXRLog.dbg(">>>>>>>>>>>>>>>>>>>>>2 : " + buffer.limit());
						IXRLog.dbg(">>>>>>>>>>>>>>>>>>>>>3 : " + signature.limit());
						cfc = 0; cfd = 0;
						for (int index=0; index<signature.limit(); index++) {
							if (index<=max_limit)
								signature.put(index, (byte)decrypt(signature.get(index)));
							else
								break;
						}			
						encryptedFile = new File(fileOut);
						outputStream = new FileOutputStream(encryptedFile);
						fos = outputStream.getChannel();						
						fos.write(signature);
						fos.close();		
						outputStream.close();
					}
				}
			}			
		} catch (Exception exception) {
			exception.printStackTrace();
		} finally {
			signature = null;
			buffer = null; fis = null; fos = null;
			inputStream  = null; outputStream = null;
			encryptFile = null; encryptedFile = null;						
		}
	}
    
    /**
     * This method is used to save the key
     * @param key
     */
    private void saveKey(byte[] key) throws Exception
    { 
        for (int idx=0; idx<Math.min(16, key.length); idx++){ 
          cle[idx] = key[idx]; 
        } 
    }      
    
    private String getFileSignature() throws Exception 
    {
    	String formattedSignature = "";
    	for (int idx=0; idx<DEFAULT_SIGNATURE.length(); idx++) {    		
    		formattedSignature += DEFAULT_SIGNATURE.charAt(idx);
    		formattedSignature += Character.toString('\0');
    	}    	
    	return formattedSignature;
    }
    
    private int decrypt(int c) throws Exception 
    {   
    	assemble();
    	cfc=(int) (inter>>8);
    	cfd=(int) (inter&255); /* cfc^cfd = random byte */
  	    
    	/* K ZONE !!!!!!!!!!!!! */
    	/* here the mix of c and cle[compte] is after the decryption of c */
  	    
    	c = c ^ (cfc^cfd);
  	   
    	for (compte=0;compte<=15;compte++)
    	{
    		/* we mix the plaintext byte with the key */
    		cle[compte]=(byte) (cle[compte]^ c);
    	}

    	return c;      /* write the decrypted byte */
    }
    
    private void assemble() throws Exception
    {	 
    	x1a0[0]= (int) ( ( cle[0]*256 )+ cle[1]);
    	code();
    	inter=res;
		 
    	x1a0[1]= (int) (x1a0[0] ^ ( (cle[2]*256) + cle[3]));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[2]= (int) (x1a0[1] ^ ( (cle[4]*256) + cle[5]));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[3]= (int) (x1a0[2] ^ ( (cle[6]*256) + cle[7] ));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[4]= (int) (x1a0[3] ^ ( (cle[8]*256) + cle[9] ));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[5]= (int) (x1a0[4] ^ ( (cle[10]*256) + cle[11] ));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[6]= (int) (x1a0[5] ^ ( (cle[12]*256) + cle[13] ));
    	code();
    	inter=(int) (inter^res);
		 
    	x1a0[7]= (int) (x1a0[6] ^ ( (cle[14]*256) + cle[15] ));
    	code();
    	inter=(int) (inter^res);
		 
    	i=0;
    }
    
    private void code() throws Exception
    {
    	int si = 0, tmp = 0, x1a2 = 0;
    	
    	dx=(int) (x1a2+i);
    	ax=x1a0[i];
    	cx=0x015a;
    	bx=0x4e35;
	 
    	tmp=ax;
    	ax=si;
    	si=tmp;
	 
    	tmp=ax;
    	ax=dx;
    	dx=tmp;
	 
    	if (ax!=0) {
    		ax=(int) (ax*bx);
    	}

    	tmp=ax;
    	ax=cx;
    	cx=tmp;
	 
    	if (ax!=0)  {
    		ax=(int) (ax*si);
    		cx=(int) (ax+cx);
    	}

    	tmp=ax;
    	ax=si;
    	si=tmp;
    	ax=(int) (ax*bx);
    	dx=(int) (cx+dx);
	 
    	ax=(int) (ax+1);
	 
    	x1a2=dx;
    	x1a0[i]=ax;
	 
    	res=(int) (ax^dx);
    	i=(int) (i+1);
    }
    
    public static void mergeFile(String pageName, File infFileTo) {
		RandomAccessFile randomFile = null;
    	try {    		
    		randomFile = new RandomAccessFile(infFileTo, "rw");
    		//adding the content to the end of the file - Census Issue 975
			randomFile.seek(randomFile.length());
			randomFile.write(pageName.getBytes());
			randomFile.close();			
		} catch (Exception e) {
						
		}
		finally{
			randomFile = null;
		}
	}
    
    private void handleError(int err, int id)
    {
        switch(err)
        {
            case ServerNotFound: 
                IXRLog.war(schema.room + ": "+ PRODUCT_NAME +" Server inactive");
                break;
            case dssInactive:
                IXRLog.war(schema.room + ": DSS Server inactive");
                break;    
            case documentNotFound:
                IXRLog.war(schema.room + ": Document ID '" + id + "' not found");
                break;
            case ixrTextIndexFailed:
                IXRLog.war(schema.room + ": Error Indexing Document ID '" + id);
                break;
            case ixrTextExtractFailed:
                IXRLog.war(schema.room + ": Extract text failed, Document ID '" + id);
                break;
                //If "getDocument() returns 'Error' handle the error message.
            case Error:
                IXRLog.war(schema.room + ": Error in get Document, Document ID '" + id);
                break;                
        }
        DBRemoveIndexerDoc(id, "reject");
    }
    private boolean writeDefaultTextFor(int id, int page)
    {
        try
        {
            FileOutputStream fos = 
                            new FileOutputStream(getPageLocalPath(id, page));
            fos.write("Non-text ViewWise Page".getBytes());
            fos.close();
            return true;
        }
        catch(Exception e)
        {
            return false;
        }
    }
    private String getDocLocalPath(int id)
    {
        return  getDocLocalFolder(id) + Util.pathSep + DOC_CONTAINER;
    }
    private String getDocLocalFolder(int id)
    {
        return  schema.path + Util.pathSep + id;
    }
    private String getPageLocalPath(int id, int page)
    {
        return  getDocLocalFolder(id) + Util.pathSep + page + ".VWT";
    }
    private int getDocument(int id)
    {
        Document doc = new Document(id);
        String storageType="";
        String azureUserName="";
        String azurePassword="";
    	CloudStorageAccount storageAccount=null;
    	CloudFileClient fileClient =null;
    	CloudFile cloudFile=null;
    	CloudFileShare share=null;
    	CloudFileDirectory rootDir=null;
    	CloudFileDirectory sampleDir =null;
    	String subSourcePath="";
        // Everytime all.zip has to download to the 'cache' folder
        //if (new File(getDocLocalPath(id)).exists()) return NoError;
        if (vws == null)
        {
            vws = (VWS) Util.getServer(vwsSchema);
            if (vws == null) return ServerNotFound;
        }        
        ServerSchema dssSchema = new ServerSchema();
        try
        {
            ServerSchema ss = vws.getDSSforDoc(schema.room, 0, doc, dssSchema);
            dssSchema.set(ss);
        }
        catch(RemoteException re)
        {
        	//Census Issue - 881 ViewWise Indexer does not re-establish a connection to the ViewWise server in a scenario where the VWS has restarted
        	try{
        		vws = (VWS) Util.getServer(vwsSchema);
        		if (vws == null) return ServerNotFound;
                ServerSchema ss = vws.getDSSforDoc(schema.room, 0, doc, dssSchema);
                dssSchema.set(ss);
        	}catch(Exception remote){
        		return Error;
        	}        		
        }
        dssSchema.type = SERVER_DSS;
        DSS ds = (DSS) Util.getServer(dssSchema);
        if (ds == null) return dssInactive;
        try
        {
/*            if (ds.getDocument(doc, schema.room, dssSchema.path, schema, true)){
            	// Extract the 'all.zip' into the 'cache' before it start the Indexing Process.
            	//extract(new File(getDocLocalPath(id)),new File(getDocLocalFolder(id)),id);	
                return NoError;
            }
            else
                return documentNotFound;*/
            boolean gotDocFile = false;
            VWDoc vwdoc = ds.getRMIDoc(doc, schema.room, dssSchema.path, schema, true);
         
            storageType=dssSchema.storageType;
            IXRLog.dbg("dssSchema Type::::"+dssSchema.storageType);
            azureUserName=dssSchema.userName;
            IXRLog.dbg("dssSchema userName::::"+dssSchema.userName);
            azurePassword=dssSchema.password;
            IXRLog.dbg("dssSchema password::::"+dssSchema.password);
        
            if (vwdoc == null) return documentHasNoPage;
            if(storageType.equals("On Premises")){
            doc.setVWDoc(vwdoc);    
            vwdoc.writeContents();  
            while(vwdoc.getBytesWritten()<vwdoc.getSize()){
        	vwdoc = ds.getNextRMIDoc(dssSchema, doc);
        	doc.setVWDoc(vwdoc);              	
        	vwdoc.writeContents();
            }
            ds.writeLogMsg(schema, doc,schema.room);
            gotDocFile = doc.getVWDoc().getIntegrity();

            if (gotDocFile){
        	return NoError;
            }else 
                return documentNotFound;
            }else if(storageType.equals("Azure Storage")){
            	
            	/*IXRLog.add("Source File:::::"+vwdoc.getSrcFile());
             	IXRLog.add("Dest File:::::"+vwdoc.getDstFile());*/
        		String accountName="AccountName="+azureUserName+";";
        		String password="AccountKey="+azurePassword;
        		String sourcePath=vwdoc.getSrcFile();
        		String destPath=vwdoc.getDstFile();
        		sourcePath=sourcePath.substring(0,sourcePath.lastIndexOf("\\"));
        		IXRLog.dbg("sourcePath::::::::"+sourcePath);
        		//subSourcePath=sourcePath.substring(sourcePath.lastIndexOf("\\")+1,sourcePath.length());
        		String   storageConnectionString1   ="DefaultEndpointsProtocol=https;"+accountName+password;
        		System.setProperty("javax.net.ssl.trustStoreType","JCEKS");
        		storageAccount = CloudStorageAccount.parse(storageConnectionString1);
        		fileClient = storageAccount.createCloudFileClient();
        		String pattern = Pattern.quote(System.getProperty("file.separator"));
        		String dbpath[]=sourcePath.split(pattern);

        		for(int i=0;i<dbpath.length;i++){
        			//IXRLog.add("value of i------"+i);
        			if(i==2)
        			{
        				//IXRLog.add("dbpath[i] inside 1 equal to 2-----"+dbpath[i]);
        				share = fileClient.getShareReference(dbpath[i]);
        			}
        			else if(i==3)
        			{
        				//IXRLog.add("dbpath[i] inside i equal to 3"+dbpath[i]);
        				sampleDir=	share.getRootDirectoryReference().getDirectoryReference(dbpath[i]);
        				rootDir = sampleDir;
        				sampleDir = null;
        			}
        			else if(i>=4)
        			{
        				//IXRLog.add("dbpath[i] inside i greater than 4"+dbpath[i]);
        				if ((i+1) != dbpath.length) {
        					//IXRLog.add("Inside if condition of  not equal to dbpath length :::::"+i);
        					sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        					if(!sampleDir.exists()){
        						sampleDir.create();
        					}
        					rootDir = sampleDir;
        					sampleDir = null;
        				}
        				else if ((i+1) == dbpath.length) {
        					//IXRLog.add("Inside else if condition of equal to dbpath length ::::::::::"+i);
        					sampleDir =	rootDir.getDirectoryReference(dbpath[i]);
        					if(!sampleDir.exists()){
        						sampleDir.create();
        					}
        					rootDir = sampleDir;
        					sampleDir = null;
        				}
        			}

        		}

        		sampleDir = rootDir;
        		IXRLog.dbg("SampleDir geturi::::"+sampleDir.getUri());
        			cloudFile = sampleDir.getFileReference("all.zip");
        		File dstFile=new File(destPath);
        		File parentFile=dstFile.getParentFile();
        		if(!parentFile.exists()){
        			parentFile.mkdir();
        		}
        		if(cloudFile.exists())
    			cloudFile.downloadToFile(destPath);
        		IXRLog.add(" Indexer Document with "+" "+ doc.getId()+" "+"downloaded from Azure Storage");
            }
        }
//        catch(RemoteException re)
//        {
//        	return Error;
//        }
        catch(Exception ioe)
        {
            return Error;
        }
		return NoError;
    }
    // This method will extract the *.vw and stored into the 'cache' folder.
    
    public void extract(File zipFile, File directory, int did) throws IOException
    {
    	int count = 1;
        ZipFile zF = new ZipFile(zipFile);
        String fileName = "";
        Enumeration zEntries = zF.entries();
        IXRLog.add("Extracting Process Started for Document " + did +" ...");
        while(zEntries.hasMoreElements()) {
        	fileName = ((ZipEntry)zEntries.nextElement()).getName();
        	if(fileName.indexOf(".vw")!= -1)
        	IXRLog.add("\t Extracting Page " + count++);	
            ZipUtil.extract(zipFile, 
                    directory, 
                    fileName);
            
        }
    }
        
    private boolean loadNativeLibrary()
    {
        if (loaded) return true;
        homePath = IXRUtil.getHome();
        try
        {
            System.load(homePath + Util.pathSep 
                                       + "system" + Util.pathSep + INDEXER_LIB);
            passStartup();
            String language = IXRPreferences.getLanguage();
            passLanguage(language);
            String engine = IXRPreferences.getEngine();
            passEngine(getEngine(engine));
            String codePage = IXRPreferences.getCodePage();
            passCodePage(codePage);
            IXRLog.dbg("VWIndex.dll loaded SucessFully by Indexer Server");
            loaded = true;
        }
        catch (UnsatisfiedLinkError e)
        {
            loaded = false;
        }
        return loaded;
    }
    private int getEngine(String engine)
    {
        if (engine.equals("AUTO")) return PGP_AUTO;
        else if (engine.equals("LEGACY")) return PGP_LEGACY;
        else if (engine.equals("STANDARD")) return PGP_STANDARD;
        else if (engine.equals("PGP_FAST")) return PGP_AUTO;
        else return PGP_AUTO;
    }
    private String CreateCacheforRoom(String room)
    {
       String cacheFolder =  homePath + Util.pathSep + CACHE_NAME + 
                                                     Util.pathSep + room;
        File fcacheFolder = new File(cacheFolder);
        if (!fcacheFolder.exists())
        {
            if (!fcacheFolder.mkdirs())
            {
                IXRLog.err("Could not create: " + cacheFolder);
            }
        }
        return cacheFolder;
    }
    private CLOB getClob(Reader reader, Connection cn)
    {
        CLOB clob = null;
        //To store the clob content, we need to use the same connection object
        //Connection cn = null;
        Writer os = null;
        char[] cbuf = new char[65536];
        try
        {
            //cn = database.aquireConnection();
            clob = CLOB.createTemporary(cn, false, CLOB.DURATION_SESSION);
            clob.open(CLOB.MODE_READWRITE);
            os = clob.getCharacterOutputStream();
            
            int offs = 0;
            int len = 0;
            while ( (len = reader.read(cbuf)) != -1)
            {
                os.write(cbuf, offs, len);
                offs += len;
            }

        }
        catch (Exception e)
        {
            IXRLog.err(e.getMessage());
        }  
        finally {
        	try{
            os.flush();
            os.close();
            clob.close();
            cbuf = null;
        	}catch(Exception e){}        	
        	//assertConnection(cn);
        	}
        return clob;
    }
    private void assertConnection(Connection cn)
    {
        if (cn == null) return;
        try
        {
            database.releaseConnection(cn);
        }
        catch (Exception e)
        {
        	//Issue 805
        	// Close the Room If Db is not available. To Solve the Issue No 501
        	String Msg = e.getMessage();
        	 IXRLog.err("Connection orphaned");
        }
    }
    private boolean DBInsertPageText(int did, int pno, int pid, int pvr)
    {
    	return DBInsertPageText( did, pno, pid, pvr,0);
    }

    /*
     * Issue # 633 	: Indexer logs "page has no text" for pages they
     * do have text and can be ocr'ed using the desktop client's OCR
     * rubber band tool. The indexer needs to be more descriptive as
     * to why it is failing to extract text from these documents.
     * Additionally there needs to be some report or state in the
     * database (similar to rejected) to indicate which documents/pages
     * failed to extract text.   Pages failing that have valid text,
     * present a search integrity issue and the pages need to be
     * indexed/re-indexed.
     * Created By	: M.Premananth
     * Date		: 10-Jul-2006
     */
    private boolean DBInsertPageText(int did, int pno, int pid, int pvr,int qc)
    {
        ResultSet rs  = null;
        OracleCallableStatement ocs = null;
        CallableStatement cs = null;
        Connection cn = null;
        BufferedReader reader = null;
        FileInputStream is = null;
        CLOB clob =null;
        int ret = -1;
        //IXRLog.dbg("DBInsertPageText " + did + " : " + pno + " : " + pid + " : " + pvr + " : " + qc);
        try
        {
            File file = new File(getPageLocalPath(did, pno));
            if(qc != 0)
            	file.createNewFile();
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
            cn = database.aquireConnection();
            
            if (database.getEngineName().equalsIgnoreCase("Oracle"))
            {
            	//To store the clob content, we need to use the same connection object
            	//Pass the Connection Object into the getClob method
                 clob = getClob(reader, cn);
                /*
                 *  Below code is commended to capture the error.
                 */
                //if (clob == null || clob.length() <= 0) return false;
                ocs = (OracleCallableStatement) cn.prepareCall
                                       ("{call IXInsertPageText(?,?,?,?,?,?,?)}");
                ocs.setInt(1, did);
                ocs.setInt(2, pno);
                ocs.setInt(3, pid);
                ocs.setInt(4, pvr);
                ocs.setCLOB(5, clob);
                ocs.setInt(6, qc);
                ocs.registerOutParameter(7, OracleTypes.CURSOR);
                ocs.execute();
                rs = (ResultSet) ocs.getObject(7);
                clob.freeTemporary();
            }
            else
            {
                cs = cn.prepareCall("{call IXInsertPageText(?,?,?,?,?,?)}");
                cs.setInt(1, did);
                cs.setInt(2, pno);
                cs.setInt(3, pid);
                cs.setInt(4, pvr);
                cs.setCharacterStream(5, reader);
                cs.setInt(6, qc);
                rs = cs.executeQuery();
            }
            if (rs != null && rs.next())
            {
                ret = rs.getInt("Fld");
                //rs.close();
                //rs = null;

            }
            // Close the resultset in outside of the if statement. In 'if' statement it will close the result set if it has atleast one record.
            if (rs != null)  { rs.close(); rs = null;}
            if (cs != null)  { cs.close(); cs = null;}
            if (ocs != null) { ocs.close(); ocs = null;}
            cn.commit();
        }
        catch(Exception e)
        {
            IXRLog.err(e.getMessage());
        }
        finally
        {
            if (reader != null) try{reader.close();} catch(IOException ioe){}
            assertConnection(cn);
           /* try {
				clob.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				//e.printStackTrace();
			}*/
            
        }
        return (ret == 0 ? true : false);
    }
    private boolean DBUpdatePageText(int did, int pno, int pid, int pvr)
    {      
	int ret = -1;
	Vector result = new Vector();
	String returnValue = "";
        String param = String.valueOf(did) + Util.SepChar + 
        			String.valueOf(pno) + Util.SepChar + 
        			String.valueOf(pid) + Util.SepChar +
        			String.valueOf(pvr);
        
    	try{    		    		  	
    	    DoIxrDBAction.get(null, database, "IXUpdatePageText", param, result);    
            if (result != null && result.size() > 0 ){
        	returnValue = result.get(0).toString();
                ret = Integer.parseInt(returnValue);
            }        	
        }catch(Exception ex){        		
        }	
        return (ret == 0 ? true : false);            
    }
    private boolean DBRemovePageText(int did, int pid)
    { 
	int ret = -1;
	Vector result = new Vector();
	String returnValue = "";
        String param = String.valueOf(did) + Util.SepChar + 
        			String.valueOf(pid);
        
    	try{    		    		  	
    	    DoIxrDBAction.get(null, database, "IXRemovePageText", param, result);    
            if (result != null && result.size() > 0 ){
        	returnValue = result.get(0).toString();
                ret = Integer.parseInt(returnValue);
            }        	
        }catch(Exception ex){        		
        }
        
        return (ret == 0 ? true : false);            
    }
    private String DBGetDocData(int id)
    {      
	
	String docData = "";
	Vector result = new Vector();
        String param = String.valueOf(id);
        
    	try{    		    		  	
        	DoIxrDBAction.get(null, database, "IXGetDocData", param, result);    
                if (result != null && result.size() > 0 ){
                    int count = 0;
                    while( count < result.size() ){
                	docData += result.get(count).toString() + " \t";                	
                	count++;                	
                    }
                    
                    if (docData.length() > 0) //remove last tab
                        docData = docData.substring(0, docData.length() -1 );
                   
                }
        	
        }catch(Exception ex){
            IXRLog.err("Error in the getDocData " + ex.getMessage());		
        }	
	return docData;
    }
    private void DBRemoveIndexerDoc(int id)
    {
        DBRemoveIndexerDoc(id, "delete");
    }
    private void DBRemoveIndexerDoc(int id, String how)
    {
	Vector result = new Vector();
        String sp = "IXRemoveIndexerDoc";
        if (how.equalsIgnoreCase("reject"))
                sp = "IXRejectIndexerDoc";
    	try{
    	String param = String.valueOf(id);
    	DoIxrDBAction.get(null, database, sp, param, result);       	
    	}catch(Exception ex){
    		
    	}    
    }
    // Add a New method to update the document status as "Processing". 
    
    private void DBIndexerProcessingDoc(IXRDoc doc)
    {     

    	Vector result = new Vector();
    	try{    	    
    	    String param = String.valueOf(doc.getId());
    	    DoIxrDBAction.get(null, database, "IXProcessingIndexerDoc", param, result);
        }catch(Exception ex){
        	
        }   
    }    
    
    private int setNotificationHistory(int nodeId, int nodeType, int settingsId, String message){
		Vector output = new Vector();
		try{
		String param =  0 + Util.SepChar + 	//FolderId
						nodeId + Util.SepChar +
						nodeType + Util.SepChar +
						settingsId + Util.SepChar +
						"-" + Util.SepChar +//OldValue
						"-" + Util.SepChar +//NewValue
						IXRConstants.ixrAdminUser + Util.SepChar +
						message;
		DoIxrDBAction.get(null, database, "VW_NFSetHistory", param, output);
		}catch(Exception ex){
			
		}
		return NoError;
	}
    
    // End
    private void removeDocFolder(int id)
    {
        String folder = getDocLocalFolder(id);
        if (folder.indexOf(schema.room) < 0 ||  folder.indexOf(CACHE_NAME) < 0)
        {
            IXRLog.err("Could not remove cache folder for document id " + id);
            return;
        }
        File CachePath = new File(folder);
        VWFileFilter ff = new VWFileFilter("*");
        String[] filestoDelete = CachePath.list(ff);
        for (int j=0; filestoDelete != null && j < filestoDelete.length; j++)
        {
            File thisfile = new File( folder + Util.pathSep + filestoDelete[j] );          
            thisfile.delete();
        }
        CachePath.delete();
    }
    public void removeCache()
    {
        if (schema.path.indexOf(schema.room) < 0 || 
                               schema.path.indexOf(CACHE_NAME) < 0) return;
        File CachePath = new File(schema.path);
        VWFileFilter ff = new VWFileFilter("*");
        String[] filestoDelete = CachePath.list(ff);
        for (int j=0; filestoDelete != null && j < filestoDelete.length; j++)
        {
            File currfile = new File(schema.path + CachePath.separator + 
                                                              filestoDelete[j]);          
            if (currfile.isDirectory())
            {
                String[] df = currfile.list(ff);
                for (int k=0; df != null && k < df.length; k++)
                {
                    new File(currfile + CachePath.separator + df[k]).delete();
                }
            }
            currfile.delete();
        }
        CachePath.delete();
    }
    public ServerSchema getSchema()
    {
        return this.schema;
    }
    public void shutDown()
    {
        removeCache();
        passShutDown();
    }
    public boolean isHealthy() 
    {
        return healthy;
    }
    public boolean isActive() 
    {
        return isWorkerActive;
    }
    
    public void checkException(String Msg){
        if (Msg.toUpperCase().indexOf("CONNECTION RESET BY PEER") != -1
			|| Msg.toUpperCase().indexOf("SHUTDOWN") != -1
			|| Msg.toUpperCase().indexOf("CONNECTION ABORT") != -1
			|| Msg.toUpperCase().indexOf("SOCKET WRITE ERROR") != -1
			|| Msg.toUpperCase().indexOf("CONNECTION RESET") != -1
			|| Msg.toUpperCase().indexOf("CLOSED CONNECTION") != -1) {
        	IXRLog.err(": No Database connection available...");
        	isDBAvailable = false;        	
        	//waitForDatabaseConnection();
        }
    }
    
    private void waitForDatabaseConnection()
    {
    
        boolean msg = true;
        Connection con = null;
        while (con == null)
        {
            try{con = database.aquireConnection();}
            catch (Exception e)
            {
                String db = database.getJdbcUrl();
                if (msg) IXRLog.war(database.getName() 
                    + ": Listening to Database Server on: " + db + "\r\n" 
                                                              + e.getMessage());
                msg = false;
                IXRPreferences ixrPrefs=new IXRPreferences ();
                int snoozeTime = ixrPrefs.getSnooze();
                IXRLog.war("Snooze Time\t:"+snoozeTime);
                Util.sleep(snoozeTime);
            }
        }
        try{database.releaseConnection(con);}
        catch (Exception e){}
    }
    public static void main(String[] args) {
	File file = new File("c:\\VWISSPath.txt");
	mergeFile("data\n", file);
    }
}