/*
 * IndexerConstants.java
 *
 * Created on July 17, 2002, 7:35 AM
 */
package com.computhink.ixr.server;
import com.computhink.common.*;
/**
 *
 * @author  Saad
 * @version 
 */
public interface IXRConstants extends Constants
{
    public static final String IXR_PREF_ROOT 
                                           = "/computhink/" + PRODUCT_NAME.toLowerCase() + "/ixr/server";
    public static final int DEFAULT_DATA_PORT = 4001;
    public static final int DEFAULT_COM_PORT = 4000;
    public static final int DEFAULT_IDLE_TIME = 45;
    public static final int DEFAULT_PROXY_PORT = 8080;
    public static final int DEFAULT_SNOOZE = 5;
    public static final int DEFAULT_MONITOR_SNOOZE = 120;
    public static final int DEFAULT_LOG_LINES = 100;
   
    public static final String CACHE_NAME = "Cache";
    public static final String INDEXER_LIB = "VWIndex.dll";
    public static final String ERROR_LOG_FILE = "IXRErrors.log";
    public static final String LOG_FILE_PREFIX = "IXRLog-";
    public static final String LOG_FOLDER = "log";

    public static final int INDEXER_START_SERVICE= 1;
    public static final int INDEXER_STOP_SERVICE= 0;
    public static final int ixrTextExtractFailed = -11001;
    public static final int ixrTextIndexFailed = -11002;
    
    public static final int PGP_AUTO = 0;
    public static final int PGP_LEGACY = 1;
    public static final int PGP_STANDARD = 2;
    public static final int PGP_FAST = 3;
/*
Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
Created by: <Pandiya Raj.M>
Date: <6 Jun 2006>
Add the following Constants variable
*/
        
    public static final int IXR_INDEX_ALL = 0;
    public static final int IXR_INDEX_UPDATE= 3;
    public static final int IXR_INDEX_PROCESSING = 2;
    
    
    public static final String ixrAdminUser = "IXRAdmin";
}
