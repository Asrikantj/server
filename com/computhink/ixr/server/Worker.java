/*
 * IndexerTask.java
 *
 * Created on June 5, 2003, 9:09 AM
 */
package com.computhink.ixr.server;
import com.computhink.common.*;
import com.computhink.database.*;

/**
 *
 * @author  administrator
 */
import java.util.StringTokenizer;
import java.sql.ResultSet;
import java.util.Vector;
import java.sql.CallableStatement;
import java.sql.Connection;

import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
//import oracle.jdbc.driver.*;

public class Worker extends Thread //TimerTask 
{
    private Database database; 
    public Indexer indexer;
    private boolean canWork = false;
    public String roomName;
    public boolean isDBAvailable = true;
    public boolean shutDownRequest = false;
    private static boolean snoozing = false;
    private int snooze;
    public  ServerSchema schema = new ServerSchema();
    public Room curRoom;
        
    public Worker(Room room, int snooze)
    {
    	curRoom = room;
        this.snooze = snooze;
        //setPriority(Thread.MIN_PRIORITY);
        indexer = new Indexer(room);
        if (indexer == null)
        IXRLog.war("Indexer Object is not created for the room : " + room + " : " + indexer.isHealthy());
        if (indexer == null || !indexer.isHealthy()) return;
        this.database = room.getDatabase("com.computhink.ixr.server.IXRLog");
        this.roomName = room.getName();
        if (isFTSEnabled()) canWork = true;
    }
   
    public void run() 
    {
        int docId = 0;
        int qc = 0;
        String docName = null;
        String result = null;
		StringTokenizer sToken = null;
		int docNotificationExist;
        while (!shutDownRequest)
        {
			/*
			Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
			Created by: <Pandiya Raj.M>
			Date: <6 Jun 2006>
			*/
            docName = "";
        	result = DBGetIndexerDoc();
        	 //Condition added for Indexer AutoRestart CV83B5
        	if(IXRPreferences.getAutoRstdebug()==true){
        		if (indexer.docCount <=10) {
        			IXRLog.war("DocCount in Worker thread :: " + indexer.docCount);
        			result = DBGetIndexerDoc();
        		}
        		else {
        			return;
        		}
        	}
            sToken =  new StringTokenizer(result,"\t");
            docNotificationExist = -1;
            // To resolve the Issue # 829 - If document is not available to process initiate the snooze time.
            if (result == null || result.equals(""))
            snoozing = true;            
            try
            {
            	if (snoozing)
                sleep(snooze);
            	snoozing = false;
            	if (sToken.countTokens() >= 3){
            	    docId = Integer.parseInt(sToken.nextToken());
            	    qc = Integer.parseInt(sToken.nextToken());
            	    docName = sToken.nextToken(); 
            	    docNotificationExist = Integer.parseInt(sToken.nextToken());
            	    //IXRLog.add("Indexer Value -> " + docId + "\t" + qc);
            	}
            }
            catch(Exception e){}
            if (docId > 0){
            	indexer.process(new IXRDoc(docId, docName, docNotificationExist),qc);
            	docId = 0;
            	qc = 0;
            }
        }
    }
    private boolean isFTSEnabled()
    {
	String returnValue = "";
	Vector<String> result = new Vector<String>();
	if ("Oracle".equalsIgnoreCase(database.getEngineName().trim())) return true;
    	try{    		    		  	
        	DoIxrDBAction.get(null, database, "IXisFTSEnabled", "", result);    	
        if (result != null && result.size() > 0 ){
            returnValue = result.get(0).toString();
            if (returnValue != null && returnValue.trim().equals("1")){
        	return true;
            }
        }
        }catch(Exception ex){
		
        }

        return false;
    }
    private String DBGetIndexerDoc()
    {        
    	String returnValue = "";
    	Vector<String> result = new Vector<String>();
    	try{    		    		  	
        	DoIxrDBAction.get(null, database, "IXGetIndexerDocs", "", result);    	
        }catch(Exception ex){
        		
        }
        if (result != null && result.size() > 0 ){
            returnValue = result.get(0).toString();
        }
        return returnValue;
    }
    public void processDoc(int did, String docName)
    {
        indexer.processRemote(new IXRDoc(did, docName));
    }
    public String processPage(int did, int page)
    {
        return indexer.processPage(new IXRDoc(did), page);
    }
    public boolean DBManualInsertPage(int did, int pno, String txt)
    {
        ResultSet rs  = null;
        OracleCallableStatement ocs = null;
        CallableStatement cs = null;
        Connection cn = null;
        int ret = -1;
        try
        {
            cn = database.aquireConnection();
            if (database.getEngineName().equalsIgnoreCase("Oracle"))
            {
                ocs = (OracleCallableStatement) cn.prepareCall
                                       ("{call IXMInsertPage(?,?,?,?)}");
                ocs.setInt(1, did);
                ocs.setInt(2, pno);
                ocs.setString(3, txt);
                ocs.registerOutParameter(4, OracleTypes.CURSOR);
                ocs.execute();
                rs = (ResultSet) ocs.getObject(4);
            }
            else
            {
                cs = cn.prepareCall("{call IXMInsertPage(?,?,?)}");
                cs.setInt(1, did);         
                cs.setInt(2, pno);
                cs.setString(3, txt);
                rs = cs.executeQuery();
            }
            if (rs != null && rs.next())
            {
                ret = rs.getInt("Fld");
                //rs.close();
                //rs = null;
            }
            // Close the resultset in outside of the if statement. In 'if' statement it will close the result set if it has atleast one record.
            if (rs != null)  { rs.close(); rs = null;}            
            if (cs != null)  { cs.close(); cs = null;}
            if (ocs != null) { ocs.close(); ocs = null;}
            cn.commit();
            IXRLog.add(roomName + ": Manual Index Document id = " + did + 
                                                              " page = " + pno);
        }
        catch(Exception e)
        {
            IXRLog.err(e.getMessage());
        } 
        finally 
        {
            assertConnection(cn);
        }
        return (ret == 0 ? true : false);            
    }
    public Vector DBGetRejectedDocs(int docCount)
    {
    	Vector<String> result = new Vector<String>();
    	try{    		    		
        	String param = String.valueOf(docCount);    	
        	DoIxrDBAction.get(null, database, "IXGetRejectedDocs", param, result);    	
        }catch(Exception ex){
        		
        }
        return result;            
    }
    public Vector DBGetProcessingDocs(int docCount)
    {
    	Vector<String> result = new Vector<String>();
    	String param = String.valueOf(docCount);
    	try{    		    		  	
        	DoIxrDBAction.get(null, database, "IXGetProcessingDocs", param, result);    	
        }catch(Exception ex){
        		
        }        
        return result;            
    }
   /*
     *  Issue No -  QC Indexer
     *  Issue description - In the QC Indexer, when Pending documents is pressed, 
     *  All documents from Indexer table that has a value of qc > 0 be fetched and 
     *  appended to the list and the the status of  those docs set to "R" indicating 
     *  Page Errors, so we know the Indexer processed that document with page errors.
     *  
     *  Developer - Nebu Alex
     * **/
    public Vector DBGetPageErrorDocs(int docCount)
    {
	
    	Vector<String> result = new Vector<String>();
    	String param = String.valueOf(docCount);
    	try{    		    		  	
        	DoIxrDBAction.get(null, database, "IXGetPageErrorDocs", param, result);    	
        }catch(Exception ex){
        		
        }	
        return result;
    }    
    // End of Fix
    public Vector DBGetQueuedDocs(int docCount)
    {
    	Vector<String> result = new Vector<String>();
    	String param = String.valueOf(docCount);
    	try{    		    		  	
        	DoIxrDBAction.get(null, database, "IXGetQueuedDocs", param, result);    	
        }catch(Exception ex){
        		
        }	
        return result;            
    }
    //Function for Issue 817
    public Vector DBGetPageDetails(int nodeId)
    {

    	Vector<String> result = new Vector<String>();
    	String param = String.valueOf(nodeId);
    	try{    		    		  	
        	DoIxrDBAction.get(null, database, "IXGetIndexerDocPages", param, result);    	
        }catch(Exception ex){
        		
        }	
        return result;            
    }
    
    public void DBAddtoQueue(int docId, int reIndex)
    {        

    	Vector<String> result = new Vector<String>();
    	String param = String.valueOf(docId) + Util.SepChar + String.valueOf(reIndex);
    	try{    		    		  	
        	DoIxrDBAction.get(null, database, "IXAddtoQueue", param, result);    	
        }catch(Exception ex){
        		
        }
    }
    
    public void checkException(String Msg){
        if (Msg.toUpperCase().indexOf("CONNECTION RESET BY PEER") != -1
			|| Msg.toUpperCase().indexOf("SHUTDOWN") != -1
			|| Msg.toUpperCase().indexOf("CONNECTION ABORT") != -1
			|| Msg.toUpperCase().indexOf("SOCKET WRITE ERROR") != -1
			|| Msg.toUpperCase().indexOf("CONNECTION RESET") != -1
			|| Msg.toUpperCase().indexOf("CLOSED CONNECTION") != -1) {
        	IXRLog.err(roomName + ": No Database connection available...");
        	isDBAvailable = false;        	
        	this.shutDownRequest = true;
        	//waitForDatabaseConnection();
        }
    }
    private void assertConnection(Connection cn)
    {
        if (cn == null) return;
        try
        {
            database.releaseConnection(cn);
        }
        catch (Exception e)
        {
        	
        	// Close the Room If Db is not available. To Solve the Issue No 501
        	String Msg = e.getMessage();
        	try {
        		checkException(Msg);
            IXRLog.err("Connection orphaned ::: " + e.getMessage());                       
        	}catch(Exception ex){}  
            IXRLog.err("Connection orphaned");
        }  
    }
    public void shutDown()
    {
        while (!snoozing) {}
        shutDownRequest = true;
        indexer.shutDown();
        try{join();} catch(Exception e){}
    }
    public boolean canWork() 
    {
        return canWork;
    }
}

