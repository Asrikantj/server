/*
 * VWUtil.java
 *
 * Created on March 17, 2002, 12:17 AM
 */
package com.computhink.ixr.server;
import com.computhink.common.*;
/**
 *
 * @author  Saad
 * @version 
 */
//import java.awt.*;
import javax.swing.*;
import java.io.*;
import java.rmi.*;
import java.net.*;
import java.text.*;
import java.util.*;
import java.rmi.registry.*;



public class IXRUtil implements IXRConstants
{
    private static Room room = null;
    private static String home = "";
    
    static
    {
        findHome();
    }
    public static String getHome()
    {
        return home;
    }
    private static void findHome()
    {
        String  urlPath = "";
        String  indexerHome = "";
        String res = "com/computhink/ixr/image/images/ixr.gif";
        String jarres = "/lib/ViewWise.jar!";
        
        int startIndex = 0;
        int endIndex = 0;
        try
        {
            URL url = ClassLoader.getSystemClassLoader().getSystemResource(res);
            urlPath = url.getPath();
        }
        catch(Exception e){urlPath="";}
        if (!urlPath.equalsIgnoreCase(""))
        {
            if (urlPath.startsWith("file"))
            {
                //file:/c:/program%20files/viewwise%20indexer/lib/viewwiseindexer.jar!/image/indexer.gif
                startIndex = 6;
                endIndex = urlPath.length() - res.length() - jarres.length();
            }
            else
            {
                ///c:/program%20files/viewwise%20indexer/Classes/indexer.gif
                startIndex = 1;
                endIndex = urlPath.length() - res.length();
            }
            indexerHome = urlPath.substring(startIndex, endIndex-1);  
            indexerHome = indexerHome.replace('/', '\\');
        }
        home = cleanUP(indexerHome);
    }
    private static String cleanUP(String str) // removes %20    
    {
       String cleanStr = ""; 
       StringTokenizer st = new StringTokenizer(str, "%20");
       while (st.hasMoreElements())
       {
           cleanStr = cleanStr + st.nextToken() + " ";
       }
       return (cleanStr.equalsIgnoreCase("")? str : cleanStr.trim());
    }
    public static Room getRoom()
    {
        return room;
    } 
    private static String getHostIpAddress()
    {
        String hostName = "";
        try
        {
            hostName = InetAddress.getLocalHost().getHostAddress();
        }
        catch (Exception e)
        {
        }
        return hostName;
    }
    public static String getNow(int style)
    {
        String now = "";
        switch (style)
        {
            case 1:
                now = new SimpleDateFormat("yyMMddHHmm").format(
                                              Calendar.getInstance().getTime());
                break;
            case 2:
                now = new SimpleDateFormat("yy-MM-dd HH:mm:ss").format(
                                       Calendar.getInstance().getTime()) + "\t";
                break;
        }
        return now;
    }
}
