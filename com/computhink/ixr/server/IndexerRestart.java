package com.computhink.ixr.server;

import java.util.TimerTask;
import com.computhink.common.Util;
import com.computhink.vws.server.VWSLog;
/**
 * Class added to start the indexer automatically
 * For DJVU processing hang issue fix
 * @author madhavan.b
 *
 */

public class IndexerRestart extends  TimerTask{
	long diff =0;
	int docCount=0;
	int maxCount=0;
	public IndexerRestart(){    		

	}
	public void run(){
		Runtime rt = Runtime.getRuntime();
		if(IXRPreferences.getAutoRstdebug()==true){
			//VWSLog.dbg("Inside isIXRstarted Restart Run Method  Start Time::"+Util.getNow(2));
			//IXRLog.dbg("Inside isIXRstarted Restart Run Method  Start Time::"+Util.getNow(2));
		}
		try {
			long d1=Long.valueOf(IXRPreferences.getIXRRST());
			if(d1>0){
				diff= System.currentTimeMillis() - d1;
				IXRLog.dbg("diff value is :::::"+diff);
			}
			long diffMinutes = diff / (60 * 1000) % 60;
			if(IXRPreferences.getAutoRstdebug()==true)
			VWSLog.dbg("Difference in minutes "+diffMinutes);
			docCount=IXRPreferences.getDocCount();
			maxCount=IXRPreferences.getAutoRstMaxDocCount();
			if((docCount<=maxCount && docCount > 1)&& (d1==0)) {
        		Runtime rt1 = Runtime.getRuntime();
        		try {
        			IXRLog.dbg("Time before stopping the indexer services:::"+Util.getNow(2));
        			long time1 = System.currentTimeMillis();
        			IXRPreferences.setIXRRST(String.valueOf(time1));
        			String str = IXRUtil.getHome();
        			String[] tokens = str.split(":");
        			String path="cmd"+" "+"/"+tokens[0]+" "+"Net stop \"Contentverse Indexer Service\"";
        			Process proc = rt1.exec(path);
        			IXRLog.add("Indexer Service Stopped for 2 min by Indexer Restart Thread...");
        		} catch (Exception e) {
        			// TODO Auto-generated catch block
        			IXRLog.dbg("Exception inside stop service of IXR: "+e.getMessage());
        		}
			}
			if(((diffMinutes>2)&&(docCount>maxCount))||((docCount<=maxCount)&&(diffMinutes>10))){
				try {
					if(IXRPreferences.getAutoRstdebug()==true){
					IXRLog.dbg("Inside indexer start by Restart Thread....");
					}
					String str = IXRUtil.getHome();
					String[] tokens = str.split(":");
					String path="cmd"+" "+"/"+tokens[0]+" "+"Net start \"Contentverse Indexer Service\"";
					Process proc = rt.exec(path);
					if(IXRPreferences.getAutoRstdebug()==true)
					IXRLog.dbg("Indexer restarted Service started by thread.");
					long time1 = System.currentTimeMillis();
					IXRPreferences.setIXRRST(String.valueOf(0));
					Indexer.docCount = 0;
					IXRPreferences.setDocCount(0);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					IXRLog.dbg("Exception inside start service of IXR: "+e.getMessage());
				}
			}else if(d1==0&&docCount==0&&diffMinutes>=2){
				try {
					VWSLog.dbg("Inside  else before start ::::::");
					IXRLog.dbg("Inside  else before start ::::::");
					if(IXRPreferences.getAutoRstdebug()==true){
					IXRLog.dbg("Inside indexer restart by Restart Thread....");
					}
					String str = IXRUtil.getHome();
					String[] tokens = str.split(":");
					String path="cmd"+" "+"/"+tokens[0]+" "+"Net start \"Contentverse Indexer Service\"";
					Process proc = rt.exec(path);
					if(IXRPreferences.getAutoRstdebug()==true)
					IXRLog.dbg("Indexer restarted Service started by thread in else condition.");
					long time1 = System.currentTimeMillis();
					IXRPreferences.setIXRRST(String.valueOf(0));
					Indexer.docCount = 0;
					IXRPreferences.setDocCount(0);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					IXRLog.dbg("Exception inside start service of IXR: "+e.getMessage());
				}
			}

		}catch(Exception ex){
			VWSLog.dbg("Exception while Restarting indexer::"+ex.getMessage());
		}


	}

}
