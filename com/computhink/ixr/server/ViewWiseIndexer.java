/*
 * ViewWiseIndexer.java
 *
 * Created on December 7, 2003, 6:12 PM
 */
package com.computhink.ixr.server;
import com.computhink.common.*;
/**
 *
 * @author  Administrator
 */
public class ViewWiseIndexer implements IXRConstants
{
    public static void main(String args[]) 
    {
        if (IXRPreferences.getProxy())
            Util.RegisterProxy(IXRPreferences.getProxyHost(), 
                               IXRPreferences.getProxyPort(),
                               SERVER_IXR);
        
        ServerSchema s = Util.getMySchema();
        s.comport = IXRPreferences.getComPort();
        s.dataport = IXRPreferences.getDataPort();
        s.type = SERVER_IXR;
        int port = Util.createServer(s);
        if ( port != 0)
        {
            IXRLog.add("Started " + PRODUCT_NAME + " Indexer Server @ " + 
                                                         s.address +":" + port);
            try
            {
                ((IXR) Util.getServer(s)).startWorkers();
            	long time1 = System.currentTimeMillis();
                if(IXRPreferences.getAutoRstdebug()==true)
                IXRPreferences.setIXRRST(String.valueOf(0));
            }
            catch(Exception e){}

        }
        else
            IXRLog.err("Could not start " + PRODUCT_NAME + " Indexer Server on port: " 
                                                                   + s.comport);
    }
}
