/*
 * IXRPreferences.Settings.java
 *
 * Created on Decembaer 21, 2003, 4:24 PM
 */
package com.computhink.ixr.server;

import com.computhink.ixr.image.*;
import com.computhink.manager.VWPanel;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWClient;
import com.computhink.vws.server.VWS;
import com.computhink.common.*;
import com.computhink.dss.server.DSSPreferences;
/**
 * 
 * @author Administrator
 */
import javax.swing.*;
import java.awt.*;
import java.awt.Toolkit.*;
import java.awt.event.*;

import javax.swing.event.*;
import javax.swing.border.*;
import java.rmi.*;
import java.io.*;
import java.util.*;
import java.util.List;

import com.jgoodies.looks.plastic.Plastic3DLookAndFeel;

public class IXRSettingsPanel extends VWPanel implements Constants {
	private JButton bOK, bCancel, bConnect, bSelectAll, bClearAll, bAddRooms;

	private VWPanel pPorts, pProxy, pVWS, pGeneral, pLang;
	private static ResourceManager resourceManager=null;
	private JLabel lDataPort, lProxyPort, lProxyHost, lComPort, lRoom,
			lVWSPort, lVWSHost, lNote, lLang, lSnooze, lEngine, lCodePage,
			lblIdleTimeout, lRooms;

	private JTextField tProxyPort, tComPort, tDataPort, tProxyHost, tVWSHost,
			tVWSPort, txtIdleTimeout;

	private JCheckBox cbProxy, cbLog;

	private JComboBox cmbLang, cmbEngine, cmbCodePage;

	private ServerSchema vwsSchema;

	private Vector<String> vLanguages = new Vector<String>();

	private Vector<String> vEngines = new Vector<String>();

	private Vector<String> vCodePages = new Vector<String>();

	private JSpinner spSnooze;

	private VWS vws = null;

	private VWCheckList roomsList = new VWCheckList();

	private static final int width = 500;

	private static final int height = 510;

	private Vector roomsVector = new Vector();

	private Vector<String> addedRoomsVector = new Vector<String>();

	public IXRSettingsPanel() {
		super();
		new Images();
		loadLanguages();
		loadEngines();
		loadCodePage();
		initComponents();
		getCurrentSettings();
	}

	private void loadEngines() {
		vEngines.addElement("AUTO");
		vEngines.addElement("LEGACY");
		vEngines.addElement("STANDARD");
		vEngines.addElement("FAST");
	}

	private void loadLanguages() {
		vLanguages.addElement("Afrikaans");
        vLanguages.addElement("Albanian");
        vLanguages.addElement("Aymara");
        vLanguages.addElement("Basque");
        vLanguages.addElement("Bemba");
        vLanguages.addElement("Blackfoot");
        vLanguages.addElement("Breton");
        vLanguages.addElement("Bugotu");
        vLanguages.addElement("Bulgarian");
        vLanguages.addElement("Byelorussian");
        vLanguages.addElement("Catalan");
        vLanguages.addElement("Chamorro");
        vLanguages.addElement("Chechen. Cyrillic alphabet");
        vLanguages.addElement("Chinese Simplified");
        vLanguages.addElement("Chinese Traditional");
        vLanguages.addElement("Corsican");
        vLanguages.addElement("Croatian");
        vLanguages.addElement("Crow");
        vLanguages.addElement("Czech");
        vLanguages.addElement("Danish");
        vLanguages.addElement("Dutch");
        vLanguages.addElement("English");
        vLanguages.addElement("Eskimo");
        vLanguages.addElement("Esperanto");
        vLanguages.addElement("Estonian");
        vLanguages.addElement("Faroese");
        vLanguages.addElement("Fijian");
        vLanguages.addElement("Finnish");
        vLanguages.addElement("French");
        vLanguages.addElement("Frisian");
        vLanguages.addElement("Friulian");
        vLanguages.addElement("Gaelic Irish");
        vLanguages.addElement("Gaelic Scottish");
        vLanguages.addElement("Ganda (Luganda)");
        vLanguages.addElement("German");
        vLanguages.addElement("Greek");
        vLanguages.addElement("Guarani");
        vLanguages.addElement("Hani");
        vLanguages.addElement("Hawaiian");
        vLanguages.addElement("Hungarian");
        vLanguages.addElement("Icelandic");
        vLanguages.addElement("Ido");
        vLanguages.addElement("Indonesian");
        vLanguages.addElement("Interlingua");
        vLanguages.addElement("Italian");
        vLanguages.addElement("Japanese");
        vLanguages.addElement("Kabardian. Cyrillic alphabet");
        vLanguages.addElement("Kasub");
        vLanguages.addElement("Kawa");
        vLanguages.addElement("Kikuyu");
        vLanguages.addElement("Kongo");
        vLanguages.addElement("Korean");
        vLanguages.addElement("Kpelle");
        vLanguages.addElement("Kurdish (Latin alphabet only)");
        vLanguages.addElement("Latin");
        vLanguages.addElement("Latvian");
        vLanguages.addElement("Lithuanian");
        vLanguages.addElement("Luba");
        vLanguages.addElement("Luxembourgian");
        vLanguages.addElement("Macedonian");
        vLanguages.addElement("Malagasy");
        vLanguages.addElement("Malay");
        vLanguages.addElement("Malinke");
        vLanguages.addElement("Maltese");
        vLanguages.addElement("Maori");
        vLanguages.addElement("Mayan");
        vLanguages.addElement("Miao (Latin alphabet only)");
        vLanguages.addElement("Minankabaw");
        vLanguages.addElement("Mohawk");
        vLanguages.addElement("Moldavian");
        vLanguages.addElement("Nahuatl");
        vLanguages.addElement("Norwegian");
        vLanguages.addElement("Nyanja");
        vLanguages.addElement("Occidental");
        vLanguages.addElement("Ojibway");
        vLanguages.addElement("Papiamento");
        vLanguages.addElement("Pigin Englis");
        vLanguages.addElement("Polish");
        vLanguages.addElement("Portuguese");
        vLanguages.addElement("Portuguese (Brazilian)");
        vLanguages.addElement("Provencal");
        vLanguages.addElement("Quechua");
        vLanguages.addElement("Rhaetic");
        vLanguages.addElement("Romanian");
        vLanguages.addElement("Romany");
        vLanguages.addElement("Ruanda");
        vLanguages.addElement("Rundi");
        vLanguages.addElement("Russian");
        vLanguages.addElement("Samoan");
        vLanguages.addElement("Sardinian");
        vLanguages.addElement("Serbian");
        vLanguages.addElement("Shona");
        vLanguages.addElement("Sioux");
        vLanguages.addElement("Slovak");
        vLanguages.addElement("Slovenian");
        vLanguages.addElement("Somali");
        vLanguages.addElement("Sorbian (Wend)");
        vLanguages.addElement("Sotho");
        vLanguages.addElement("Spanish");
        vLanguages.addElement("Sundanese");
        vLanguages.addElement("Swahili");
        vLanguages.addElement("Swazi");
        vLanguages.addElement("Swedish");
        vLanguages.addElement("Tagalog");
        vLanguages.addElement("Tahitian");
        vLanguages.addElement("Tinpo");
        vLanguages.addElement("Tongan");
        vLanguages.addElement("Tswana (Chuana)");
        vLanguages.addElement("Tun (Latin alphabet only)");
        vLanguages.addElement("Turkish");
        vLanguages.addElement("Ukrainian");
        vLanguages.addElement("Visayan");
        vLanguages.addElement("Welsh");
        vLanguages.addElement("Wolof");
        vLanguages.addElement("Xhosa");
        vLanguages.addElement("Zapotec");
        vLanguages.addElement("Zulu");
	}

	private void loadCodePage() {
		vCodePages.addElement("Code Page 437");
		vCodePages.addElement("Code Page 850");
		vCodePages.addElement("Code Page 852");
		vCodePages.addElement("Code Page 860");
		vCodePages.addElement("Code Page 863");
		vCodePages.addElement("Code Page 865");
		vCodePages.addElement("Code Page 866");
		vCodePages.addElement("CWI Magyar");
		vCodePages.addElement("Greek-ELOT");
		vCodePages.addElement("Greek-MEMOTEK");
		vCodePages.addElement("Icelandic");
		vCodePages.addElement("IVKAM C-S");
		vCodePages.addElement("Latin 1");
		vCodePages.addElement("Mac Central EU");
		vCodePages.addElement("Mac INSO Latin 2");
		vCodePages.addElement("Mac Primus CEu");
		vCodePages.addElement("Macintosh");
		vCodePages.addElement("Magyar Ventura");
		vCodePages.addElement("Maltese");
		vCodePages.addElement("Mazowia Polish");
		vCodePages.addElement("OCR");
		vCodePages.addElement("Roman 8");
		vCodePages.addElement("Sloven & Coat");
		vCodePages.addElement("Turkish");
		vCodePages.addElement("Unicode");
		vCodePages.addElement("UTF-8");
		vCodePages.addElement("Windows ANSI");
		vCodePages.addElement("Windows Baltic");
		vCodePages.addElement("Windows Cyrilic");
		vCodePages.addElement("Windows Eastern");
		vCodePages.addElement("Windows Esperant");
		vCodePages.addElement("Windows Greek");
		vCodePages.addElement("Windows Sami");
		vCodePages.addElement("Windows Turkish");
		vCodePages.addElement("WordPerfect");
		vCodePages.addElement("WordPerfect No");
		vCodePages.addElement("WordPerfect Old");
	}

	private void initComponents() {
		resourceManager=ResourceManager.getDefaultManager();
		pPorts = new VWPanel();
		pVWS = new VWPanel();
		pProxy = new VWPanel();
		pGeneral = new VWPanel();
		pLang = new VWPanel();
		cbProxy = new JCheckBox();
		cbLog = new JCheckBox(resourceManager.getString("IXRChK.LogInfo"));
		bOK = new JButton(resourceManager.getString("IXRBtn.Apply"));
		bCancel = new JButton(resourceManager.getString("IXRBtn.Cancel"));

		cbProxy.setBackground(this.getBackground());
		cbLog.setBackground(this.getBackground());
		bOK.setBackground(this.getBackground());
		bCancel.setBackground(this.getBackground());

		tComPort = new JTextField();
		tDataPort = new JTextField();
		tProxyHost = new JTextField();
		tProxyPort = new JTextField();
		tVWSHost = new JTextField();
		tVWSPort = new JTextField();
		txtIdleTimeout = new JTextField();

		lProxyHost = new JLabel(resourceManager.getString("IXRLbl.ProxyHost"));
		lProxyPort = new JLabel(resourceManager.getString("IXRLbl.Port"));
		lVWSHost = new JLabel(resourceManager.getString("IXRLbl.VWSHost"));
		lVWSPort = new JLabel(resourceManager.getString("IXRLbl.VWSComPort"));
		lComPort = new JLabel(resourceManager.getString("IXRLbl.ComPort"));
		lDataPort = new JLabel(resourceManager.getString("IXRLbl.Dataport"));
		lLang = new JLabel(resourceManager.getString("IXRLbl.OCRLanguage"));
		lEngine = new JLabel(resourceManager.getString("IXRLbl.OCREngine"));
		lCodePage = new JLabel(resourceManager.getString("IXRLbl.OCRCode"));
		lNote = new JLabel(resourceManager.getString("IXRLbl.Note"));

		lSnooze = new JLabel(resourceManager.getString("IXRLbl.Snooze"));
		spSnooze = new JSpinner(new SpinnerNumberModel(1, 1, 300, 1));

		lblIdleTimeout = new JLabel(resourceManager.getString("IXRLbl.IdleTimeout"));

		cmbEngine = new JComboBox(vEngines);
		cmbLang = new JComboBox(vLanguages);
		cmbCodePage = new JComboBox(vCodePages);

		bConnect = new JButton();
		bConnect.setToolTipText(resourceManager.getString("IXRBtn.Connect"));

		bAddRooms = new JButton();
		bAddRooms.setToolTipText(resourceManager.getString("IXRBtn.AddRooms"));
		bAddRooms.setEnabled(false);

		bSelectAll = new JButton();
		bSelectAll.setToolTipText(resourceManager.getString("IXRBtn.SelectAll"));
		bClearAll = new JButton();
		bClearAll.setToolTipText(resourceManager.getString("IXRBtn.ClearAll"));
		lRooms = new JLabel(resourceManager.getString("IXRLbl.Rooms"));
		setLayout(null);
		// setResizable(false);

		bConnect.setBackground(getBackground());
		bAddRooms.setBackground(getBackground());
		bSelectAll.setBackground(this.getBackground());
		bClearAll.setBackground(this.getBackground());

		pPorts.setLayout(null);
		pPorts.setBorder(new TitledBorder(resourceManager.getString("IXRTitleBorder.ServPorts")));
		pPorts.setFocusable(false);
		add(pPorts);
		pPorts.setBounds(20, 20, 470, 55);
		pPorts.add(lComPort);
		lComPort.setBounds(15, 20, 120, 16);
		pPorts.add(lDataPort);
		lDataPort.setBounds(300, 20, 60, 16);
		pPorts.add(tComPort);
		tComPort.setBounds(150, 20, 50, 20);
		pPorts.add(tDataPort);
		tDataPort.setBounds(370, 20, 50, 20);

		pProxy.setLayout(null);
		pProxy.setBorder(new TitledBorder("         "+resourceManager.getString("IXRTitleBorder.HttpProxy")));
		pProxy.setFocusable(false);
		add(pProxy);
		pProxy.setBounds(20, 85, 470, 55);
		pProxy.add(lProxyHost);
		lProxyHost.setBounds(15, 20, 70, 16);
		pProxy.add(lProxyPort);
		lProxyPort.setBounds(325, 20, 30, 16);
		pProxy.add(cbProxy);
		cbProxy.setBounds(10, 0, 20, 21);
		pProxy.add(tProxyHost);
		tProxyHost.setBounds(90, 20, 110, 20);
		pProxy.add(tProxyPort);
		tProxyPort.setBounds(370, 20, 50, 20);

		pVWS.setLayout(null);
		pVWS.setBorder(new TitledBorder(resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("IXRTitleBorder.Server")));
		pVWS.setFocusable(false);
		add(pVWS);
		pVWS.setBounds(20, 150, 470, 155);
		pVWS.add(lVWSHost);
		lVWSHost.setBounds(15, 20, 70, 16);
		pVWS.add(tVWSHost);
		tVWSHost.setBounds(90, 20, 110, 20);
		pVWS.add(lVWSPort);
		lVWSPort.setBounds(250, 20, 120, 16);
		pVWS.add(tVWSPort);
		tVWSPort.setBounds(370, 20, 50, 20);
		/*
		 * Vector roomDetails = new Vector(); roomDetails.add("Room1");
		 * roomDetails.add("Room2"); roomDetails.add("Room3");
		 * roomDetails.add("Room4"); roomDetails.add("Room5");
		 * roomDetails.add("Room6"); roomsList.loadData(roomDetails);
		 */

		// Enhancement for Indexer - selected room will process
		pVWS.add(lRooms);
		lRooms.setBounds(15, 50, 70, 16);
		pVWS.add(roomsList);
		roomsList.setBounds(90, 50, 302, 80);
		pVWS.add(bConnect);
		bConnect.setBounds(397, 50, 23, 23);
		pVWS.add(bAddRooms);
		bAddRooms.setBounds(425, 50, 23, 23);
		pVWS.add(bSelectAll);
		bSelectAll.setBounds(397, 78, 23, 23);
		pVWS.add(bClearAll);
		bClearAll.setBounds(397, 106, 23, 23);

		bConnect.setIcon(Images.connect);
		bAddRooms.setIcon(Images.addRooms);
		bClearAll.setIcon(Images.clearAll);
		bSelectAll.setIcon(Images.selectAll);

		pLang.setLayout(null);
		pLang.setBorder(new TitledBorder(resourceManager.getString("IXRTitleBorder.General")));
		pLang.setFocusable(false);
		add(pLang);
		pLang.setBounds(20, 315, 470, 130);
		pLang.add(lLang);
		lLang.setBounds(15, 20, 100, 16);
		pLang.add(cmbLang);
		cmbLang.setBounds(125, 20, 120, 20);
		pLang.add(lEngine);
		lEngine.setBounds(15, 55, 80, 16);
		pLang.add(cmbEngine);
		cmbEngine.setBounds(125, 55, 120, 20);
		pLang.add(lSnooze);
		lSnooze.setBounds(260, 20, 120, 16);
		pLang.add(spSnooze);
		spSnooze.setBounds(370, 16, 45, 26);
		pLang.add(lblIdleTimeout);
		lblIdleTimeout.setBounds(260, 55, 120, 16);
		pLang.add(txtIdleTimeout);
		txtIdleTimeout.setBounds(370, 55, 45, 20);
		pLang.add(lCodePage);
		lCodePage.setBounds(15, 90, 120, 16);
		pLang.add(cmbCodePage);
		cmbCodePage.setBounds(125, 90, 120, 20);

		add(bOK);
		bOK.setBounds(400, 455, 90, 23);
		// add(bCancel); bCancel.setBounds(388, 430, 80, 23);
		add(cbLog);
		cbLog.setBounds(23, 455, 150, 20);
		bOK.setToolTipText(lNote.getText());

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setSize(new Dimension(width, height));
		setLocation((screenSize.width - width) / 2,
				(screenSize.height - height) / 2);
		// setIconImage(Images.app.getImage());
		// -------------Listeners------------------------------------------------
		/*
		 * addWindowListener(new WindowAdapter() { public void
		 * windowClosing(WindowEvent evt) { closeDialog(); } });
		 */
		bOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				setSettings();
				// closeDialog();
			}
		});
		bCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				closeDialog();
			}
		});
		cbProxy.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent evt) {
				cbProxyStateChanged();
			}
		});
		bConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				ConnecttoVWS();

			}
		});
		bAddRooms.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				VWClient.printToConsole("bAddRooms clicked()...");
				btnAddRooms_ActionPerformed(evt);
			}
		});
		bSelectAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				roomsList.selectAllItems();

			}
		});
		bClearAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				roomsList.deSelectAllItems();

			}
		});
	}

	public void btnAddRooms_ActionPerformed(ActionEvent evt) {
		try {
			IXRAddRoomsDialog addRoomsDlg = new IXRAddRoomsDialog(this);
		} catch (Exception e) {
			VWClient
					.printToConsole("Exception in btnAddRooms_ActionPerformed :: "
							+ e.getMessage());
		}
	}

	// Enhancement for Indexer - selected room will process
	private boolean ConnecttoVWS() {
		applyVWSSettings();
		vws = (VWS) Util.getServer(vwsSchema);
		if (vws != null) {
			boolean isHosting = CVPreferences.getHosting();
			VWClient.printToConsole("isHosting :: " + isHosting);
			if (isHosting) {
				bAddRooms.setEnabled(true);
				getAddedRooms();
			} else {
				bAddRooms.setEnabled(false);
				populateRooms();
			}
			roomsList.loadData(roomsVector);
			setSelectedRooms();
			return true;
		} else {
			return false;
		}
	}

	private void getAddedRooms() {
		try{
			String addedRooms = IXRPreferences.getAddedRooms();
			roomsVector.clear();
			addedRoomsVector.clear();
			if(addedRooms != null && addedRooms.length() > 0){
				Vector ret = vws.getRoomNames();
				if(ret != null && ret.size() > 0){
					StringTokenizer st = new StringTokenizer(addedRooms, ";");
					String currentAddedRooms = "";
					while(st.hasMoreTokens()){
						String roomName = st.nextToken();
						if(ret.contains(roomName)){
							currentAddedRooms += roomName + ";";
							roomsVector.add(roomName);
							addedRoomsVector.add(roomName);
						}
					}
					IXRPreferences.setAddedRooms(currentAddedRooms);
				}else{
					IXRPreferences.setAddedRooms("");
				}
			}
		}catch (Exception e) {
			VWClient.printToConsole("Exception while getting added rooms : "+e.getMessage());
		}
	}

	private void applyVWSSettings() {
		vwsSchema.address = tVWSHost.getText();
		vwsSchema.comport = Util.to_Number(tVWSPort.getText());
		
		ServerSchema currentVWSSchema = IXRPreferences.getVWS();
		if(!currentVWSSchema.address.equalsIgnoreCase(vwsSchema.address)){
			IXRPreferences.setAddedRooms("");
			IXRPreferences.setSelectedRooms("");
		}
		IXRPreferences.setVWS(vwsSchema);
	}

	private void populateRooms() {
		try {
			roomsVector.clear();
			Vector ret = vws.getRoomNames();
			roomsVector.addAll(ret);
		} catch (Exception e) {
		}
	}

	private String getSelectedRooms() {
		StringBuffer selectedRooms = new StringBuffer("");
		List rooms = roomsList.getSelectedItems();
		for (int item = 0; item < rooms.size(); item++) {
			selectedRooms.append(rooms.get(item).toString() + ";");
		}
		return selectedRooms.toString();
	}

	private void setSelectedRooms() {
		String selectedRooms = IXRPreferences.getSelectedRooms();
		Vector roomsVector = new Vector();
		StringTokenizer tokens = new StringTokenizer(selectedRooms, ";");
		while (tokens.hasMoreTokens()) {
			roomsVector.add(tokens.nextToken());
		}
		if(roomsVector != null && roomsVector.size() > 0)
			roomsList.setCheckItems(roomsVector);
	}

	private void setSettings() {
		IXRPreferences.setComPort(Integer.parseInt(tComPort.getText()));
		IXRPreferences.setDataPort(Integer.parseInt(tDataPort.getText()));
		IXRPreferences.setProxy(cbProxy.getSelectedObjects() != null ? true
				: false);
		IXRPreferences.setProxyHost(tProxyHost.getText());
		String pPort = tProxyPort.getText();
		if (pPort.equals(""))
			IXRPreferences.setProxyPort(0);
		else
			IXRPreferences.setProxyPort(Integer.parseInt(pPort));
		vwsSchema.address = tVWSHost.getText();
		vwsSchema.comport = Util.to_Number(tVWSPort.getText());
		IXRPreferences.setVWS(vwsSchema);
		IXRPreferences.setLogInfo(cbLog.isSelected());
		IXRPreferences.setLanguage((String) cmbLang.getSelectedItem());
		IXRPreferences.setSnooze(((Integer) spSnooze.getValue()).intValue());
		IXRPreferences.setIdleTimeout(Integer.parseInt(this.txtIdleTimeout
				.getText()));
		IXRPreferences.setEngine((String) cmbEngine.getSelectedItem());
		IXRPreferences.setCodePage((String) cmbCodePage.getSelectedItem());
		try {
			String selectedRoomList = getSelectedRooms();
			if (selectedRoomList.length() > 0)
				IXRPreferences.setSelectedRooms(selectedRoomList);
			else
				IXRPreferences.setSelectedRooms("");
		} catch (Exception ex) {
		}
	}

	private void getCurrentSettings() {
		tComPort.setText(String.valueOf(IXRPreferences.getComPort()));
		tDataPort.setText(String.valueOf(IXRPreferences.getDataPort()));
		cbProxy.setSelected(IXRPreferences.getProxy());
		tProxyHost.setText(IXRPreferences.getProxyHost());
		int pPort = IXRPreferences.getProxyPort();
		if (pPort > 0)
			tProxyPort.setText(String.valueOf(pPort));
		else
			tProxyPort.setText("");
		vwsSchema = IXRPreferences.getVWS();
		tVWSHost.setText(vwsSchema.address);
		int vPort = vwsSchema.comport;
		if (vPort > 0)
			tVWSPort.setText(String.valueOf(vPort));
		else
			tVWSPort.setText("");
		cbLog.setSelected(IXRPreferences.getLogInfo());
		cbProxyStateChanged();
		cmbLang.setSelectedItem(IXRPreferences.getLanguage());
		cmbEngine.setSelectedItem(IXRPreferences.getEngine());
		cmbCodePage.setSelectedItem(IXRPreferences.getCodePage());
		spSnooze.setValue(new Integer(IXRPreferences.getSnooze()));
		this.txtIdleTimeout.setText(String.valueOf(IXRPreferences
				.getIdleTimeout()));
	}

	private void cbProxyStateChanged() {
		if (cbProxy.getSelectedObjects() != null) {
			switchProxyData(true);
		} else {
			switchProxyData(false);
		}
	}

	private void switchProxyData(boolean b) {
		tProxyHost.setEnabled(b);
		tProxyPort.setEnabled(b);
	}

	private void closeDialog() {
		setVisible(false);
		// dispose();
		/*
		 * Issue #607 : On exit of the Indexer Settings, the executable stays
		 * running until the ViewWiseIndexerSettings.exe process is killed (task
		 * manager). A minimized title bar stays on the screen that becomes a
		 * blue pixel when double clicked. Opening the task manager reveals that
		 * the ViewWiseIndexerSettings.exe process never ended on close. Created
		 * By : M.Premananth Date : 5-July-2006
		 */
		System.exit(0);
	}

	public void addRoom(String roomName) {
		try {
			addedRoomsVector.add(roomName);
			VWClient.printToConsole("added rooms size : "
					+ addedRoomsVector.size());
			roomsList.loadData(addedRoomsVector);
		} catch (Exception e) {
			VWClient.printToConsole("Exception in addRoom : " + roomName);
		}
	}

	public boolean isValidRoomName(String roomName) {
		try {
			Vector ret = vws.getRoomNames();
			if (ret.contains(roomName)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			VWClient.printToConsole("Exception in isValidRoomName : "
					+ e.getMessage());
			return false;
		}
	}

	public static void main(String args[]) {
		try {
			String plasticLookandFeel = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
			UIManager.setLookAndFeel(plasticLookandFeel);
			new Images();
		} catch (Exception ex) {
		}
		/*
    	 * new IXRSettings().show(); method is replaced with new IXRSettings().setVisible(true); 
    	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
    	 */
		new IXRSettings().setVisible(true);
	}

}
