 /*
 * Images.java
 *
 * Created on Sep 21, 2003, 12:14 AM
 */
package com.computhink.ixr.image;
/**
 *
 * @author  Saad
 * @version 
 */
import javax.swing.ImageIcon;

public class Images
{
    public static ImageIcon app;
    public static ImageIcon connect;
    public static ImageIcon selectAll;
    public static ImageIcon clearAll;
    public static ImageIcon addRooms;

    public Images()
    {
        try
        {
            app = new ImageIcon(getClass().getResource("images/ixr.gif"));
            connect = new ImageIcon(getClass().getResource("images/connect.gif"));
            selectAll = new ImageIcon(getClass().getResource("images/selectAll.gif"));
            clearAll = new ImageIcon(getClass().getResource("images/clearAll.gif"));
            addRooms = new ImageIcon(getClass().getResource("images/AddIcon.gif"));
        }
        catch (Exception e){}
    }
}