package com.computhink.drs.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;

import com.computhink.drs.server.Worker;
import com.computhink.ars.server.ARSConstants;
import com.computhink.common.LogMessage;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.SCMEvent;
import com.computhink.common.SCMEventListener;
import com.computhink.common.SCMEventManager;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWClient;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWS;

public class DRSServer extends UnicastRemoteObject implements DRS, SCMEventListener {
	private Vector clients = new Vector();
	private Hashtable workers = new Hashtable();
	private long stime;
    private ServerSchema mySchema;
    private boolean shutDownRequest = false;
    private static ServerSchema  managerSchema = null;
    Timer processTimer;
	private final String SIGN = "0B9B5177-1011-4BBF-B617-455288043D4A";
	VWClient vwClient;
	VWS vws;
	
	public DRSServer(ServerSchema s) throws RemoteException
    {
        super(s.dataport);
        this.mySchema = s;
        SCMEventManager scm = SCMEventManager.getInstance();
        scm.addSCMEventListener(this);
        stime = System.currentTimeMillis();
    }
    public void handleSCMEvent(SCMEvent event)
    {
        shutDown();
    }
    public void exit() throws RemoteException
    {
        shutDown();
    }
    public String getServerOS() throws RemoteException
    {
        return Util.getEnvOSName();
    }
    public void init() throws RemoteException
    {
    	
    	ServerSchema vwss = DRSPreferences.getVWS();
	    vws = (VWS) Util.getServer(vwss);
        if (vws == null)
        {
            DRSLog.add(PRODUCT_NAME + " Server @" + vwss.address + ":" +vwss.comport + " inactive");
            DRSLog.war("Listening to " + PRODUCT_NAME + " Server...");
            vws = waitForViewWiseServer(vwss);
        }
        if (shutDownRequest) return;
        DRSLog.add("Connected to "+ PRODUCT_NAME +" Server @" + vwss.address + ":" +vwss.comport);
        try
        {
            String serverName = null;
        	String cashDir = "c:\\temp";
	  		vwClient = new VWClient(SIGN, cashDir,Client.Drs_Client_Type);
            /*Vector servers=new Vector();
            int retValue = vwClient.getServers(servers);
            if(servers == null || servers.size() == 0){
            	DRSLog.err("Couldn't get server Name ");
            	return;
            }*/
        	serverName = ARSConstants.DUMMY_SERVERNAME+"DWS";
            Vector rooms =new Vector();
            rooms = vws.getRoomNames();
            //int ret = vwClient.getRooms(serverName, rooms);
            // loop for multiple rooms
            if(rooms!=null && rooms.size()>0)
            {
            for (int i = 0; i < rooms.size(); i++)
            {
                String roomName = (String) rooms.get(i);
                int schedulerStartInfo = DRSPreferences.getSchedulerStartInfo();
            	int sessionID = -1;
        		sessionID = vws.login(roomName, Client.Drs_Client_Type, vwss.getAddress(), vwClient.encryptStr(DRSConstants.drsAdminUser), "", vwss.getComport());
        		if(sessionID>0){
        			vwClient.setSession(sessionID, serverName, roomName, DRSConstants.drsAdminUser);	
            	}else{
            		if(sessionID == ViewWiseErrors.NoMoreLicenseSeats){
            			DRSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.NoMoreLicenseSeats));
            		}else if(sessionID==ViewWiseErrors.ServerNotFound)
                    	DRSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.ServerNotFound));
                    else if(sessionID==ViewWiseErrors.checkUserError)
                    	DRSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.checkUserError));
                    else if(sessionID==ViewWiseErrors.NotAllowedConnections) 
                    	DRSLog.war("This Room is Administratively Down."+
                            NewLineChar+"Please contact your "+ PRODUCT_NAME +" Administrator"+
                            NewLineChar+"before attempting to reconnect.");
                    else if(sessionID==ViewWiseErrors.accessDenied)
                    	DRSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.accessDenied));
                    else if(sessionID==ViewWiseErrors.LoginCountExceeded)
                    	DRSLog.war(vwClient.getErrorDescription(ViewWiseErrors.LoginCountExceeded));
                    else if(sessionID==ViewWiseErrors.JarFileMisMatchErr)
                    	DRSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.JarFileMisMatchErr));
    	            else
    	            	DRSLog.war(""+ PRODUCT_NAME +" room may be down.");
            		continue;
            	}
        		
	         	//int snoozeTime = 10 * 1000;
	         	int snoozeTime = Integer.parseInt(DRSPreferences.getSnoozeTime()) * 60 * 1000;
                if(schedulerStartInfo == 0){
                    Worker worker = new Worker(vwClient, roomName, snoozeTime, DRSPreferences.getSchedulerStartInfo(), sessionID);
                    if (worker == null)
                    {
                        DRSLog.war("Cannot start "+ WORKFLOW_MODULE_NAME +" Worker for room " + roomName);
                        continue;
                    }
                    DRSLog.add(WORKFLOW_MODULE_NAME +" services started for Room : '"+roomName+"'");
                	worker.start();
                    workers.put(roomName, worker);
                    
                }else if(schedulerStartInfo == 1){
                	//Worker class added here also to get room name and sessionId. This is to teminate session on logout
                	snoozeTime = 1000*60*60*24;
                	Worker worker = new Worker(vwClient, roomName, snoozeTime, DRSPreferences.getSchedulerStartInfo(), sessionID);
                	workers.put(roomName, worker);
                    String dailyFrequency = DRSPreferences.getDialyFrequency();
                    StringTokenizer stDailyFreq = null;
                    stDailyFreq = new StringTokenizer(dailyFrequency, ":");
                    Calendar calendar = Calendar.getInstance();
                    if(stDailyFreq != null && stDailyFreq.countTokens() == 3){
                    	calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(stDailyFreq.nextToken()));
                    	calendar.set(Calendar.MINUTE, Integer.parseInt(stDailyFreq.nextToken()));
                    	calendar.set(Calendar.SECOND, Integer.parseInt(stDailyFreq.nextToken()));
                    }else{
                    	calendar.set(Calendar.HOUR_OF_DAY, 0);
                    	calendar.set(Calendar.MINUTE, 0);
                    	calendar.set(Calendar.SECOND, 0);
                    }
                    Date startTime = calendar.getTime();
                    DRSLog.add("DWS will send documents of the room: '"+roomName+"' at " +startTime.toString() + " to "+ WORKFLOW_MODULE_NAME.toLowerCase());
                	processTimer = new Timer();
                	processTimer.schedule(new SheduledProcess(vwClient, sessionID, roomName), startTime, snoozeTime);
                }//end for schedulerStartInfo
            }// end for ForLoop
            }//if condition for room null
        }catch(Exception ex){
        	DRSLog.err(PRODUCT_NAME + " Server connection error: " + ex.getMessage());
        }
    }
    
    public String managerBegin(ServerSchema manager) throws RemoteException
    {
        if (managerSchema == null)
        {
            managerSchema = manager;
            return "";
        }
        else
        {
            if (managerSchema.address.equals(manager.address))
                return "";
            else
            	return ResourceManager.getDefaultManager().getString("DRSServMsg.MonitorServ1")
            			+ managerSchema.address +" "+ ResourceManager.getDefaultManager().getString("DRSServMsg.MonitorServ2");
        }
    }
    public void managerEnd() throws RemoteException
    {
        managerSchema = null;
    }
    private void shutDown()
    {
    	if(vwClient!=null){
    		vwClient.shutdown();
    		DRSLog.add("VW client stopped");
    	}
    	if(processTimer!=null){
    		processTimer.cancel();
    		processTimer = null;
    	}
    	if(workers!=null && workers.size()>0){
	        Enumeration wenum = workers.elements();
	        while (wenum.hasMoreElements())
	        {
	            Worker worker = (Worker) wenum.nextElement();
	            try{
	            	vws.logout(worker.roomName, worker.sessionID);
	            }catch(Exception ex){}
	            worker.shutDown();
	            DRSLog.add("Stopped "+ WORKFLOW_MODULE_NAME +" Worker for Room " + worker.roomName);
	        }
    	}
        DRSLog.writeLog();
        Util.sleep(500);
        Util.killServer(mySchema);
        DRSLog.add("Stopped");
    }

    private VWS waitForViewWiseServer(ServerSchema vwss)
    {
        VWS vws = null;
        while (vws == null && !shutDownRequest)
        {
            Util.sleep(2000);
            vws = (VWS) Util.getServer(vwss);
        }
        return vws;
    }
    public ServerSchema getSchema() throws RemoteException
    {
        return this.mySchema;
    }
    public void setComPort(int port) throws RemoteException
    {
        DRSPreferences.setComPort(port);
    }
    public int getComPort() throws RemoteException
    {
        return DRSPreferences.getComPort();
    }
     public void setDataPort(int port) throws RemoteException
    {
        DRSPreferences.setDataPort(port);
    }
    public int getDataPort() throws RemoteException
    {
        return DRSPreferences.getDataPort();
    }
    public void setProxy(boolean b) throws RemoteException
    {
        DRSPreferences.setProxy(b);
    }
    public void setProxyHost(String server) throws RemoteException
    {
        DRSPreferences.setProxyHost(server);
    }
    public void setProxyPort(int port) throws RemoteException
    {
        DRSPreferences.setProxyPort(port);
    }
    public boolean getProxy() throws RemoteException
    {
        return DRSPreferences.getProxy();
    }
    public String getProxyHost() throws RemoteException
    {
        return DRSPreferences.getProxyHost();
    }
    public int getProxyPort() throws RemoteException
    {
        return DRSPreferences.getProxyPort();
    }
    public ServerSchema getVWS() throws RemoteException
    {
        return DRSPreferences.getVWS();
    }
    public void setVWS(ServerSchema vws) throws RemoteException
    {
        DRSPreferences.setVWS(vws);
    }
    public boolean getLogInfo() throws RemoteException
    {
        return DRSPreferences.getLogInfo();
    }
    public void setLogInfo(boolean b) throws RemoteException
    {
        DRSPreferences.setLogInfo(b);
    }

    public boolean getDebugInfo() throws RemoteException
    {
        return DRSPreferences.getDebugInfo();
    }
    public void setDebugInfo(boolean b) throws RemoteException
    {
        DRSPreferences.setDebugInfo(b);
    }
    public boolean setManager(String man, String pas) throws RemoteException
    {
        return DRSPreferences.setManager(man, pas);
    }
    public String getManager() throws RemoteException
    {
    	return DRSPreferences.getManager();
    }


    public long getTimeOn() throws RemoteException
    {
    	return (System.currentTimeMillis() - stime) / 1000;
    }
    public LogMessage getLogMsg() throws RemoteException
    {
        return DRSLog.getLog();
    }
    public Vector getClients() throws RemoteException
    {
        return this.clients;
    }
    public int getLogCount() throws RemoteException
    {
        return DRSLog.getCount();
    }
    public int getSchedulerStartInfo() throws RemoteException
	 {
	        return DRSPreferences.getSchedulerStartInfo();
	 }
	 public void setSchedulerStartInfo(int schStart) throws RemoteException
	 {
	        DRSPreferences.setSchedulerStartInfo(schStart);
	 }

	 public String getDialyFrequency() throws RemoteException
	 {
	        return DRSPreferences.getDialyFrequency();
	 }
	 public void setDialyFrequency(String dialyFrequency) throws RemoteException
	 {
	        DRSPreferences.setDialyFrequency(dialyFrequency);
	 }
	 public String getSnoozeTime() throws RemoteException{
		 return DRSPreferences.getSnoozeTime();
	 }
	 public void setSnoozeTime(String snoozeTime) throws RemoteException{
		 
		 DRSPreferences.setSnoozeTime(snoozeTime);
		 
	 }
	 private Vector vectorToRouteMaster(Vector toDoRouteList, Vector v)
	    {
	        //Vector routeHistoryInfo = new Vector();
	        for (int i = 0; i < v.size(); i++)
	        {
	        	RouteMasterInfo routeMasterInfo = new RouteMasterInfo((String) v.get(i));
	        	toDoRouteList.add(routeMasterInfo);
	        }
	        return toDoRouteList;
	    }   
	 class SheduledProcess extends TimerTask {
		   	int ret = -1;
		   	private String roomName;
		   	private int sessionID;
		   	VWClient vwClient;
		   	public SheduledProcess(VWClient vwClient, int sessionID, String roomName){
		   		this.vwClient = vwClient;
		   		this.roomName = roomName;
		   		this.sessionID = sessionID;
		   	}
		   	private void doSheduledProcess(){
		   		RouteDocuments routeDocuments = new RouteDocuments(vwClient, sessionID, roomName);
	    		ret = routeDocuments.sendDocumentsInRoute();
	    		routeDocuments.getRouteCompletedDocs();
	    		//Task escalation enhancement
	    		ret = routeDocuments.updateRouteHistoryForEscalatedDocuments();
	        	//processTimer.cancel();
	        	//processTimer = null;
		   	}
		   	public void run(){
		   		doSheduledProcess();
		  	}
	   }
}
