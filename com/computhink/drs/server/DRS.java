package com.computhink.drs.server;

import java.rmi.RemoteException;

import com.computhink.common.CIServer;
import com.computhink.common.ServerSchema;

public interface DRS extends CIServer {
	public ServerSchema getVWS() throws RemoteException; 
    public void setVWS(ServerSchema vws) throws RemoteException;
    public void init() throws RemoteException;
    public int getSchedulerStartInfo() throws RemoteException; 
    public void setSchedulerStartInfo(int schedStart) throws RemoteException;
    public String getDialyFrequency() throws RemoteException;
    public void setDialyFrequency(String dialyFrequency) throws RemoteException;
}
