package com.computhink.drs.server;

import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.manager.ManagerPreferences;
import com.computhink.vws.server.VWSLog;

public class ViewWiseDRS implements DRSConstants{

	public static void main(String[] args) {

	      if (DRSPreferences.getProxy())
            Util.RegisterProxy(DRSPreferences.getProxyHost(),
                               DRSPreferences.getProxyPort(),
                               SERVER_DRS);
        String hostName = DRSPreferences.getHostName();
        if (hostName.length() > 0)
        {
            System.getProperties().put("java.rmi.server.useLocalHostname", "true");
            System.getProperties().put("java.rmi.server.hostname", hostName);
        }
        ServerSchema s = Util.getMySchema();
        s.comport = DRSPreferences.getComPort();
        s.dataport = DRSPreferences.getDataPort();
        s.type = SERVER_DRS;
        DRSLog.add("DWS : " + DRSPreferences.getComPort()+":"+DRSPreferences.getDataPort()+":"+SERVER_DRS);
        if(ManagerPreferences.getDRSEnable().equalsIgnoreCase("true")){
	        int port = Util.createServer(s);
	        if ( port != 0)
	        {
	            DRSLog.add("Started "+ PRODUCT_NAME +" "+ WORKFLOW_MODULE_NAME +" Server @" + hostName +"[" + s.address + "]:" + port);
	            try
	            {
	                ((DRS) Util.getServer(s)).init();
				}
	            catch(Exception e){}
	        }
	        else{
	            DRSLog.err("Could not start "+ PRODUCT_NAME +" "+ WORKFLOW_MODULE_NAME +" on port: " + port);
	        }
	        
	    }// Master switch off condition
	}
}
