package com.computhink.drs.server;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerDateModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.computhink.drs.image.Images;
import com.computhink.drs.server.DRSSettings;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.manager.ManagerConstants;
import com.computhink.resource.ResourceManager;


public class DRSSettings extends JDialog implements ManagerConstants, DRSConstants {

	/**
	 * @param args
	 */
	private JButton bOK, bCancel;
    private JPanel pPorts, pProxy, pVWS, pLang;
    private JLabel lDataPort, lProxyPort, lProxyHost, lComPort, 
                              lVWSPort, lVWSHost,lNote,
                              lSnooze, lMinute;
    private JTextField tProxyPort, tComPort, tDataPort, tProxyHost, 
                                                        tVWSHost, tVWSPort;
    private JCheckBox cbProxy, cbLog;
  
    JRadioButton ChkOpt1 = new JRadioButton();
    JRadioButton ChkOpt2 = new JRadioButton();
    ButtonGroup group1 = new ButtonGroup(); 
    JSpinner jSpinner;
    JSpinner minuteSpinner;

    SpinnerDateModel dateModel = new SpinnerDateModel();
    SpinnerNumberModel minuteModel = new SpinnerNumberModel(5, 1, 59, 1);
    
    private static final int width = 500;
    private static final int height = 510;
    private static final int schedStartByAuto = 0;
    private static final int schedStartByFrequency= 1;
    
    private ServerSchema vwsSchema;
    
    
    public DRSSettings() 
    {
        new Images();
        initComponents();
        getCurrentSettings();
    }
    private void initComponents() 
    {
    	setTitle("Document "+ WORKFLOW_MODULE_NAME +" Server Settings "); 
        pPorts = new JPanel();
        pVWS = new JPanel();
        pProxy = new JPanel();
        pLang = new JPanel();
        cbProxy = new JCheckBox();
        cbLog = new JCheckBox("Log Information");
        bOK = new JButton("OK");
        bCancel = new JButton("Cancel");

        tComPort = new JTextField();
        tDataPort = new JTextField();
        tProxyHost = new JTextField();
        tProxyPort = new JTextField();
        tVWSHost = new JTextField();
        tVWSPort = new JTextField();

        lProxyHost = new JLabel("Proxy Host:");
        lProxyPort = new JLabel("Port:");
        lVWSHost = new JLabel("Host:");
        lVWSPort = new JLabel("Communication Port:");
        lComPort = new JLabel("Communication Port:");
        lDataPort = new JLabel("Data Port:");
        lNote = new JLabel
        ("Server must be restarted for the new settings to take effect.");
        lSnooze = new JLabel ("Snooze:");
        lMinute = new JLabel ("minutes");
        getContentPane().setLayout(null);
        setResizable(false);
        
        pPorts.setLayout(null);
        pPorts.setBorder(new TitledBorder("Server Ports"));
        pPorts.setFocusable(false); getContentPane().add(pPorts);
        pPorts.setBounds(20, 20, 450, 80);
        pPorts.add(lComPort); lComPort.setBounds(30, 30, 120, 16);
        pPorts.add(lDataPort); lDataPort.setBounds(300, 30, 60, 16);
        pPorts.add(tComPort); tComPort.setBounds(160, 30, 50, 20);
        pPorts.add(tDataPort); tDataPort.setBounds(370, 30, 50, 20);
        
        pProxy.setLayout(null);
        pProxy.setBorder(new TitledBorder("         Use Http Proxy"));
        pProxy.setFocusable(false);  getContentPane().add(pProxy);
        pProxy.setBounds(20, 110, 450, 80);
        pProxy.add(lProxyHost); lProxyHost.setBounds(30, 30, 70, 16);
        pProxy.add(lProxyPort); lProxyPort.setBounds(325, 30, 30, 16);
        pProxy.add(cbProxy); cbProxy.setBounds(10, 0, 20, 21);
        pProxy.add(tProxyHost); tProxyHost.setBounds(110, 30, 110, 20);
        pProxy.add(tProxyPort); tProxyPort.setBounds(370, 30, 50, 20);
        
        pVWS.setLayout(null);
        pVWS.setBorder(new TitledBorder(ResourceManager.getDefaultManager().getString("CVProduct.Name") + " Server"));
        pVWS.setFocusable(false); getContentPane().add(pVWS);
        pVWS.setBounds(20, 200, 450, 80);
        pVWS.add(lVWSHost); lVWSHost.setBounds(30, 30, 70, 16);
        pVWS.add(lVWSPort); lVWSPort.setBounds(240, 30, 120, 16);
        pVWS.add(tVWSHost); tVWSHost.setBounds(70, 30, 110, 20);
        pVWS.add(tVWSPort); tVWSPort.setBounds(370, 30, 50, 20);
        
        pLang.setLayout(null);
        pLang.setBorder(new TitledBorder("Schedule Information"));
        pLang.setFocusable(false); 
        getContentPane().add(pLang);
        pLang.setBounds(20, 290, 450, 110);
        pLang.add(ChkOpt1); ChkOpt1.setBounds(30, 30, 160, 16);
        pLang.add(lSnooze); lSnooze.setBounds(190, 30, 45, 16);
        pLang.add(lMinute); lMinute.setBounds(300, 30, 50, 16);
        pLang.add(ChkOpt2); ChkOpt2.setBounds(30, 55, 215, 16);
        ChkOpt1.setText("Start when DWS starts");
        ChkOpt2.setText("Daily frequency : Occurs once at ");
        group1.add(ChkOpt1);
        group1.add(ChkOpt2);
        ChkOpt1.setSelected(true);
        ChkOpt1.addActionListener(new ActionListener() 
        {
            public void actionPerformed(ActionEvent evt) 
            {            	
            	toggleTime(evt.getSource());
            }
        });
        ChkOpt2.addActionListener(new ActionListener() 
                {
                    public void actionPerformed(ActionEvent evt) 
                    {            	
                    	toggleTime(evt.getSource());
                    }
        });

	    dateModel.setCalendarField(Calendar.HOUR_OF_DAY);
	    jSpinner = new JSpinner(dateModel);jSpinner.setBounds(258,55, 100, 20);
	    JSpinner.DateEditor editor2 = new JSpinner.DateEditor(jSpinner, "HH:mm:ss a");
	    jSpinner.setEditor(editor2);
	    minuteSpinner = new JSpinner(minuteModel);minuteSpinner.setBounds(258,30,36, 20);

//	  Get the date formatter
	    JFormattedTextField tf = ((JSpinner.DefaultEditor)jSpinner.getEditor()).getTextField();
	    //DefaultFormatterFactory factory = (DefaultFormatterFactory)tf.getFormatterFactory();
	    tf.setEditable(false);
	    //DateFormatter formatter = (DateFormatter)factory.getDefaultFormatter();
	    
	    // Change the date format to only show the hours
	    //formatter.setFormat(new SimpleDateFormat("HH:MM:ss a"));
	    pLang.add(minuteSpinner);
		pLang.add(jSpinner);
        enableTime(false);

        getContentPane().add(bOK); bOK.setBounds(270, 430, 90, 26);
        getContentPane().add(bCancel); bCancel.setBounds(378, 430, 90, 26);
        getContentPane().add(cbLog); cbLog.setBounds(20, 410, 450, 20);
        bOK.setToolTipText(lNote.getText());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(new Dimension(width, height));
        setLocation((screenSize.width - width)/2,(screenSize.height-height)/2);
        //-------------Listeners------------------------------------------------
        addWindowListener(new WindowAdapter() 
        {
            public void windowClosing(WindowEvent evt) {
                closeDialog();
            }
        });
        bOK.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                setSettings();
                closeDialog();
            }
        });
        bCancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent evt){
                closeDialog();
            }
        });
        cbProxy.addChangeListener(new ChangeListener() 
        {
            public void stateChanged(ChangeEvent evt) 
            {
                cbProxyStateChanged();
            }
        });
    }
     private void toggleTime(Object btn){
    	 if (btn == ChkOpt1){
    		 enableTime(false);
    	 } else if (btn == ChkOpt2){
    		 enableTime(true);
    	 } 
     }
     private void enableTime(boolean flag){
     	jSpinner.setEnabled(flag);
     	minuteSpinner.setEnabled(!flag);
     }
    private void setSettings()
    {
        try
        {
            DRSPreferences.setComPort(Integer.parseInt(tComPort.getText()));
            DRSPreferences.setDataPort(Integer.parseInt(tDataPort.getText()));
            DRSPreferences.setProxy(cbProxy.getSelectedObjects() != null? true : false);
            DRSPreferences.setProxyHost(tProxyHost.getText());
            String pPort = tProxyPort.getText();
            if (pPort.equals(""))
                DRSPreferences.setProxyPort(0);
            else
                DRSPreferences.setProxyPort(Integer.parseInt(pPort));
            vwsSchema.address = tVWSHost.getText();
            vwsSchema.comport = Util.to_Number(tVWSPort.getText());
            DRSPreferences.setVWS(vwsSchema);
            DRSPreferences.setLogInfo(cbLog.isSelected());

            if(ChkOpt1.isSelected()){
            	DRSPreferences.setSchedulerStartInfo(schedStartByAuto);
        		DRSPreferences.setSnoozeTime(minuteModel.getNumber().toString());
            }
            if(ChkOpt2.isSelected()){
            	DRSPreferences.setSchedulerStartInfo(schedStartByFrequency);
	        	Date dailyFreq = dateModel.getDate();
            	String dailyFrequency = new SimpleDateFormat("HH:mm:ss").format(dailyFreq);
            	DRSPreferences.setDialyFrequency(dailyFrequency);
            }
        }
        catch(Exception e) {}
    }
    private void getCurrentSettings()
    {
        try
        {
            tComPort.setText(String.valueOf(DRSPreferences.getComPort()));
            tDataPort.setText(String.valueOf(DRSPreferences.getDataPort()));
            cbProxy.setSelected(DRSPreferences.getProxy());
            tProxyHost.setText(DRSPreferences.getProxyHost());
            int pPort = DRSPreferences.getProxyPort();
            if (pPort > 0)
                tProxyPort.setText(String.valueOf(pPort));
            else
                tProxyPort.setText("");
            vwsSchema = DRSPreferences.getVWS();
            tVWSHost.setText(vwsSchema.address);
            int vPort = vwsSchema.comport;
            if (vPort > 0)
                tVWSPort.setText(String.valueOf(vPort));
            else
                tVWSPort.setText("");
            cbLog.setSelected(DRSPreferences.getLogInfo());
            cbProxyStateChanged();
            //use StringTok and put it in separate spinner
            
            int schedulerStartInfo = DRSPreferences.getSchedulerStartInfo();
            if(schedulerStartInfo == 0){
            	ChkOpt1.setSelected(true);
            	Integer intValue = new Integer(DRSPreferences.getSnoozeTime());
            	minuteModel.setValue(intValue);
            	minuteSpinner.setEnabled(true);
            }
            if(schedulerStartInfo == 1){
            	ChkOpt2.setSelected(true);
            	jSpinner.setEnabled(true);
            	String dailyFreq = null;
            	dailyFreq = DRSPreferences.getDialyFrequency();
            	SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
            	Date date = timeFormat.parse(dailyFreq);
            	//Date date = new Date(dailyFreq);
            	dateModel.setValue(date);
            }
        }
        catch(Exception re)
        {
        }
    }
    private void cbProxyStateChanged() 
    {
        if (cbProxy.getSelectedObjects() != null)
        {
            switchProxyData(true);
        }
        else
        {
            switchProxyData(false);
        }
    }
    private void switchProxyData(boolean b)
    {
        tProxyHost.setEnabled(b);
        tProxyPort.setEnabled(b);
    }
    private void closeDialog() 
    {
    	System.exit(0);
    }

	public static void main(String[] args) {
		try{
            String plasticLookandFeel  = "com.jgoodies.looks.plastic.Plastic3DLookAndFeel";
    		UIManager.setLookAndFeel(plasticLookandFeel);
        	}catch(Exception ex){}  
		/*
    	 * new DRSSettings(.show(); method is replaced with new DRSSettings().setVisible(true); 
    	 * as show() method is deprecated  //Gurumurthy.T.S 18/12/2013,CV8B5-001
    	 */
		new DRSSettings().setVisible(true);
		// TODO Auto-generated method stub
	}
}
