package com.computhink.drs.server;

import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.VWClient;

public class Worker extends Thread implements ViewWiseErrors//TimerTask
{

    private static boolean shutDownRequest = false;
    private static boolean snoozing = false;
    public String roomName;
    private int snooze;
    public int sessionID;
    VWClient vwClient;
    private int schedulerStartInfo;

	public Worker(VWClient vwClient, String roomName, int snooze, int schedulerStartInfo, int sessionID)
    {
		this.vwClient = vwClient;
		this.snooze = snooze;
		this.sessionID = sessionID;
        this.roomName = roomName;
        this.schedulerStartInfo = schedulerStartInfo;
    }

	public void run()
    {
	  	 while (!shutDownRequest)
         {
	  		if(schedulerStartInfo == 0)
	    	{
	  			RouteDocuments routeDocuments = new RouteDocuments(vwClient, sessionID,roomName);
	  			//procedure call added for displaying workflow update Warining message
	  			int retMainFlag=routeDocuments.getDWSMaintenanceFlag();
	  			if(retMainFlag==1){
	  				//Need to add in resource string
	  				DRSLog.war("Workflow update in progress, please wait...");
	  			}
	    		routeDocuments.sendDocumentsInRoute();
	    		routeDocuments.getRouteCompletedDocs();
	    		//Task escalation enhancement
	    		routeDocuments.updateRouteHistoryForEscalatedDocuments();
	            snoozing = true;
	            try
	            {
	                sleep(snooze);
	            }
	            catch(Exception e){}
	            snoozing = false;
	    		
	    	}
         }
    }

	 public void shutDown()
	    {
	        shutDownRequest = true;
	    }

}
