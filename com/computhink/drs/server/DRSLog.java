package com.computhink.drs.server;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Vector;

import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.rtf.RTFEditorKit;

import com.computhink.drs.server.DRSPreferences;
import com.computhink.drs.server.DRSUtil;
import com.computhink.drs.server.DRSConstants;
import com.computhink.common.LogMessage;
import com.computhink.common.Util;

public class DRSLog  implements DRSConstants{
	private static Vector Log = new Vector();
    private static LogMessage oldlog = new LogMessage("", LogMessage.MSG_INF);
    private static LogMessage oldwar = new LogMessage("", LogMessage.MSG_WAR);
    private static LogMessage olderr = new LogMessage("", LogMessage.MSG_ERR);
    private static LogMessage olddbg = new LogMessage("", LogMessage.MSG_DBG);

    public static void add(String log)
    {
        if (!DRSPreferences.getLogInfo()) return;
        LogMessage lm = createMsg(log, LogMessage.MSG_INF);
        if (lm.equals(oldlog)) return;
        append(lm);
        oldlog = lm;
        flushWhenFull();
    }
    public static void war(String war)
    {
        LogMessage lw = createMsg(war, LogMessage.MSG_WAR);
        if (lw.equals(oldwar)) return;
        append(lw);
        oldwar = lw;
    }
    public static void err(String err)
    {
        LogMessage le = createMsg(err, LogMessage.MSG_ERR);
        if (le.equals(olderr)) return;
        append(le);
        olderr = le;
        writeErrorFile(le.fullMsg);
    }
     public static void dbg(String dbg)
    {
        if (!DRSPreferences.getDebugInfo()) return;
        LogMessage ld = createMsg(dbg, LogMessage.MSG_DBG);
        if (ld.equals(oldwar)) return;
        append(ld);
        olddbg = ld;
    }
    private static void append(LogMessage lx)
    {
        Log.add(lx);
        flushWhenFull();
    }
    private static LogMessage createMsg(String msg, int type)
    {
        msg = Util.getNow(2) + " " + msg + "\r\n";
        return new LogMessage(msg, type);
    }
    private static void flushWhenFull()
    {

        if (Log.size() >= DRSPreferences.getFlushLines())
        {
           writeLog();
        }
    }
    public static int getCount()
    {
        return Log.size();
    }
    public static LogMessage getLog()
    {
        LogMessage log = (LogMessage) Log.get(0);
        Log.remove(0);
        return log;
    }
    public static void writeLog()
    {
         if (Log.size() > 0) {
         	writeLogFile(createDoc(Log));
         }
    }
    private static Document createDoc(Vector messages)
    {
        int size = messages.size();  if (size <= 0) return null;
        DefaultStyledDocument doc = new DefaultStyledDocument();
        SimpleAttributeSet sas = new SimpleAttributeSet();
        for (int i=0; i < size; i++)
        {
            try
            {
                LogMessage log = (LogMessage) messages.elementAt(i);
                StyleConstants.setForeground(sas, log.color);
                doc.insertString(doc.getLength(), log.fullMsg , sas);
            }
            catch(Exception e){}
        }
        return doc;
    }
    private static void writeErrorFile(String msg)
    {
        try
        {
            File errFile = new File(assertLogFolder() + DRSConstants.ERROR_LOG_FILE);
            FileOutputStream fos = new FileOutputStream(errFile, true);
            fos.write(msg.getBytes());
            fos.close();
        }
        catch(Exception e){}
    }
    private static void writeLogFile(Document doc)
    {
        try
        {
        	if(doc.getLength()==0)return;
            File logFile = new File(assertLogFolder() + LOG_FILE_PREFIX +
                                                       Util.getNow(1) + ".rtf");
            FileOutputStream fos = new FileOutputStream(logFile);
            new RTFEditorKit().write(fos, doc, 0, doc.getLength());
            fos.close();
            Log.clear();
           int maxFileCount =DRSPreferences.getLogFileCount();
           Util.maintainLogFiles(assertLogFolder(),LOG_FILE_PREFIX,maxFileCount);

        }
        catch(Exception e){}

    }
    private static String assertLogFolder()
    {
        File folder = new File(Util.checkPath(DRSUtil.getHome() + Util.pathSep + LOG_FOLDER));
        try
        {
            if (!folder.exists()) folder.mkdir();
            return folder.getPath() + Util.pathSep;
        }
        catch(Exception e){}
        return "c:" + Util.pathSep;
    }
}
