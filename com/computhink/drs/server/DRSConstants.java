package com.computhink.drs.server;

import com.computhink.common.Constants;
import com.computhink.resource.ResourceManager;
public interface DRSConstants extends Constants {

	public static final String SETTINGS = ResourceManager.getDefaultManager().getString("CVProduct.Name") + " "+ ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+ResourceManager.getDefaultManager().getString("DRS.ServerSettings");
	public static final String DRS_PREF_ROOT  = "/computhink/" + PRODUCT_NAME.toLowerCase() + "/dws/server";
	public static final String DRS_CLASSPATH = "com/computhink/drs/image/images/drs.gif";
	public static final String DRS_JAR = "/lib/ViewWise.jar!";
		
	public static final int DEFAULT_DATA_PORT = 6001;
	public static final int DEFAULT_COM_PORT = 6000;
	public static final int DEFAULT_PROXY_PORT = 8080;
	public static final int DEFAULT_SCHEDULARSTART = 0;
	public static final String DEFAULT_DAILYFREQUENCY = "6:01:01";
		
	public static final int DEFAULT_LOG_LINES = 100;
	public static final String ERROR_LOG_FILE = "DWSErrors.log";
	public static final String LOG_FILE_PREFIX = "DWSLog-";
	public static final String LOG_FOLDER = "log";
	public static final String drsAdminUser = "DWSAdmin";
	//public static final String[] ReportColumnNames = {"S.No", "User Name","Received On","Action Taken", "Processed On", "Comments"};
	public static final String[] ReportColumnNames = 
		{ResourceManager.getDefaultManager().getString("DRSReportCol.SNo"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.UserName"), 
		ResourceManager.getDefaultManager().getString("DRSReportCol.Task"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.Comments"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.Status"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.ReceivedOn"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.ProcessedOn"),
		ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+ResourceManager.getDefaultManager().getString("DRSReportCol.Name"), 
		ResourceManager.getDefaultManager().getString("DRSReportCol.Signature"), 
		ResourceManager.getDefaultManager().getString("DRSReportCol.UserStatus")};
	
	public static final String[] ReportColumnNamesNew = 
		{ResourceManager.getDefaultManager().getString("DRSNewReportCol.UserName"),
		ResourceManager.getDefaultManager().getString("DRSNewReportCol.ReceivedOn"), 
		ResourceManager.getDefaultManager().getString("DRSNewReportCol.Action"),
		ResourceManager.getDefaultManager().getString("DRSNewReportCol.dateofAction"),
		ResourceManager.getDefaultManager().getString("DRSNewReportCol.comments"),
		ResourceManager.getDefaultManager().getString("DRSNewReportCol.version"),
		ResourceManager.getDefaultManager().getString("DRSNewReportCol.forwardTo"),
		ResourceManager.getDefaultManager().getString("DRSNewReportCol.refernceCount")};
	
	public static final String[] ColumnWidthsNew = {"14","8","10","15","10","10","10","10"};
	public static final String[] ColumnWidths = {"3","10","10","20","13","10","10","12","5","7"};
	
	public static final String[] ExportReportColumnNames = 
		{ResourceManager.getDefaultManager().getString("DRSReportCol.SNo"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.UserName"), 
		ResourceManager.getDefaultManager().getString("DRSReportCol.Task"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.Comments"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.TaskDesc"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.Status"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.ReceivedOn"),
		ResourceManager.getDefaultManager().getString("DRSReportCol.ProcessedOn"),
		ResourceManager.getDefaultManager().getString("WORKFLOW_NAME") +" "+ResourceManager.getDefaultManager().getString("DRSReportCol.Name"), 
		ResourceManager.getDefaultManager().getString("DRSReportCol.Signature"), 
		ResourceManager.getDefaultManager().getString("DRSReportCol.UserStatus")};	
	public static final String[] ExportColumnWidths = {"3","10","9","15","10","13","8","8","10","5","7"};
	//public static final String[] ColumnWidths = {"5","10","15","30","15","20"};
	
	public static final String[] MetaDataColumnNames = {ResourceManager.getDefaultManager().getString("DRSMetaCol.SNo"),
			ResourceManager.getDefaultManager().getString("DRSMetaCol.IndexName"),
			ResourceManager.getDefaultManager().getString("DRSMetaCol.IndexVal")};
	public static final String[] MetaDataColumnWidths = {"5","30","30"};
	public static String LBL_DRS_Rep_Title=ResourceManager.getDefaultManager().getString("routesummary.officeNoteTitleLbl");
	public static String LBL_DRS_Rep_Subject=ResourceManager.getDefaultManager().getString("routesummary.officeNoteSubjectLbl");
	public static String LBL_DRS_Rep_OffVertical=ResourceManager.getDefaultManager().getString("routesummary.officeNoteVerticalLbl");
	public static String LBL_DRS_Rep_OffDocumentLbl=ResourceManager.getDefaultManager().getString("routesummary.officeNoteDocumentLbl");
	
	
	public static String LBL_DRS_NAME=ResourceManager.getDefaultManager().getString("WORKFLOW_NAME")+" "+ResourceManager.getDefaultManager().getString("DRSLbl.SummarStatus");
	public static String LBL_DOC_META_DATA=ResourceManager.getDefaultManager().getString("DRSLbl.DocMetaData");
	public static final String DEFAULT_SNOOZETIME = "5";
}
