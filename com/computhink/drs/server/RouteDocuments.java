
package com.computhink.drs.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.zip.ZipFile;

import com.computhink.common.Constants;
import com.computhink.common.Document;
import com.computhink.common.Node;
import com.computhink.common.RouteInfo;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.RouteUsers;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.common.VWATEvents;
import com.computhink.common.VWEmailOptionsInfo;
import com.computhink.common.ViewWiseErrors;
import com.computhink.resource.ResourceManager;
import com.computhink.vns.server.VNSConstants;
import com.computhink.vwc.Session;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.VWUtil;
import com.computhink.vwc.ZipUtil;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;

public class RouteDocuments  implements ViewWiseErrors{

	
	private VWClient vwClient;
	private int sessionId;
	private String roomName;
	private String message;
	private VWS vws;
	private static ResourceManager resourceManager=null;
	public String docPublishedPath = "";
	public String destPathStr = "";
	public String destTempPathStr = null;
	public String routeNameStr = "";
	public String docNameStr = "";
	public int compressDocFlag=0; 
	public int convertToPdfFlag=0;
	public RouteDocuments(VWClient vwClient, int sessionId, String roomName){
		this.vwClient = vwClient;
		this.sessionId = sessionId;
		this.roomName = roomName;
		ServerSchema vwss = DRSPreferences.getVWS();
		resourceManager=ResourceManager.getDefaultManager();
		this.vws = (VWS) Util.getServer(vwss);
		
	}
	 // Auto Route
	 public int sendDocumentsInRoute()
	 {

		 int docId;
		 int routeId;
		 String docName;
		 String docCreatorName;
		 String routeName;
		/****CV2019 merges from SIDBI line--------------------------------***/
     	 Vector sendDocumentsInAutoRoute = new Vector();
     	 Vector doctypes = null;
		 Document doc = null;
		 Document doc1 = null;
		 
         try
         {
        	int returnId = vwClient.getDocsToAutoRoute(sessionId, sendDocumentsInAutoRoute);
        	if(returnId<0){
        		if(returnId == ViewWiseErrors.invalidSessionId){
        			message = ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+resourceManager.getString("RouteDoc.ServerDown");
        			DRSLog.war(message);
        			vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);	        			
        		}
        		return Error;
        	}
         	//routeDocs.add("1753	4");
           	//String routeInitiatedBy = "Auto";
        	String param = "Allow 0 page document(s) to WF";
	     	boolean allowZeroPages = vws.getServerSettingsInfo(this.roomName, sessionId, param);
	     	DRSLog.dbg("zeroPageCheck :: "+allowZeroPages);
        	if(sendDocumentsInAutoRoute != null && sendDocumentsInAutoRoute.size() > 0){
         		HashSet docIds = new HashSet();
	   			for(int index=0; index < sendDocumentsInAutoRoute.size(); index++) {
	   				String sendDoc = sendDocumentsInAutoRoute.get(index).toString();
	   				StringTokenizer stDocInfos = new StringTokenizer(sendDoc, "\t");
	   				routeId = Util.to_Number(stDocInfos.nextToken());
	   				routeName = stDocInfos.nextToken();
	   				docId = Util.to_Number(stDocInfos.nextToken());
	   				boolean isDocIdAdded = docIds.add(""+docId);
	   				docName = stDocInfos.nextToken();
	   				docCreatorName = stDocInfos.nextToken();//DRSConstants.drsAdminUser;
	   				int taskSequence = 0;
	   				if(isDocIdAdded){
	   					Vector resultVector = new Vector();
	   					int ret = vwClient.validateRouteDocument(sessionId, routeId, docId, resultVector);
	   					if(resultVector != null && resultVector.size() > 0){
	   						String msg = (String) resultVector.get(0);
	   						//DRSLog.err("Document <"+docName+"> does not contains the indices \""+ msg+"\"");
	   						DRSLog.war(msg+" <"+docName+"> ");
	   						continue;
	   					}
		   				 /**This part is added for zero pages check*/
		   				 if (!allowZeroPages) {
		   					 doctypes = new Vector();
			   				 doc = new Document();
			   				 doc.setId(docId);
			   				 vwClient.getDocumentInfo(sessionId, doc, doctypes);
			   				 doc1=(Document)doctypes.get(0);		   				 
			   				 DRSLog.dbg("sendDocumentsInRoute document page count :"+doc1.getPageCount());
		   				 }
		   				 if ((allowZeroPages) || (allowZeroPages == false && doc1 != null && doc1.getPageCount() > 0)) {
		   					 sendDocInAutoRoute(docId, docName, routeId, routeName, taskSequence, docCreatorName);
		   					 DRSLog.add("Room name: "+this.roomName+" Initiate "+ Constants.WORKFLOW_MODULE_NAME +": '"+routeName +"' applied to Document '"+docName+"'");
		   				 } else {
		   					DRSLog.err("Document '"+docName+"' has no pages. Can not send to work-flow.");
		   				 }
	   				}
	   			}// for loop end
	   			docIds = new HashSet();
	   			doc = null; doc1 = null;
	   		}// checking sendDocumentsInAutoRoute null end 
         }catch(Exception ex){
	   				
         }
		 /*------------------End of SIDBI merges----------------------------*/
		 return NoError;
	 }
	 
	 public int sendDocInAutoRoute(int docId, String docName, int routeId, 
			 String routeName, int taskSequence, String docCreatorName){
		 DRSLog.dbg("Inside sendDocInAutoRoute ....");
		 Vector vTaskInfo = new Vector();
		 StringBuilder emailUsersList = null;
		 try
		 {
			 int ret = vwClient.getTaskInfo(sessionId, routeId, taskSequence, vTaskInfo);
			 Session session = vwClient.getSession(sessionId);
			 if(vTaskInfo!=null&& vTaskInfo.size()>0)
			 {
				 RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(0);
				 int routeTaskId = routeTaskInfo.getRouteTaskId();
				 int TaskSeq = routeTaskInfo.getTaskSequence();
				 Vector vRouteUsers = new Vector();
				 ret = vwClient.getRouteUsers(sessionId, routeTaskId, vRouteUsers, docId);
				 if(vRouteUsers!=null && vRouteUsers.size()>0)
				 {
					 boolean sidbiCustomization = vws.getSidbiCustomizationFlag(roomName, sessionId);
					 for(int k=0; k<vRouteUsers.size(); k++)
					 {
						 RouteUsers routeUser = (RouteUsers)vRouteUsers.get(k);
						 int routeUserId =routeUser.getRouteUserId();
						 String groupName = routeUser.getGroupName();
						 String userName = "";
						 userName = session.user;
						 vws.setRouteHistory(roomName, sessionId, docId, routeId, TaskSeq, routeUserId, docCreatorName, userName, userName, 0, 0, groupName);//RouteMasterId = 0 
						 try{
							/**CV2019 merges from SIDBI line***/
							 if(routeUser.getSendEmail() == 1){								
    							// sendMailWithAttachment should be called only if the emailToPreviousUser check should be disabled(0)
    							//if ((!sidbiCustomization) || (sidbiCustomization == true && routeTaskInfo.getEmailToPrvTaskUsers() != 1)) {
								if (!sidbiCustomization) {
	    								vws.sendMailWithAttachment(roomName, session.server, sessionId, routeUser.getUserName(), docName, 
    										 routeName, docId, new Vector(), null, "Pending", null, null, "");
    							} else {
    								if (emailUsersList == null) {
    									emailUsersList = new StringBuilder();
    									emailUsersList.append(routeUser.getUserName());
    								} else {
    									emailUsersList.append(";"+routeUser.getUserName());
    								}
    							}
							 }
							 /*------------------End of SIDBI merges----------------------------*/
						 }catch(Exception ex){
							 DRSLog.dbg("Exception in sendDocInAutoRoute......."+ex.getMessage());
						 }
					 }
				 }
				/**CV2019 merges from SIDBI line***/
				 DRSLog.dbg("emailUsersList......."+emailUsersList);
				 if (emailUsersList != null && emailUsersList.length() > 0) {
					 vws.sendMailWithAttachment(roomName, session.server, sessionId, emailUsersList.toString(), docName, 
							 routeName, docId, new Vector(), null, "Pending", null, null, "");
				 }
				 /*------------------End of SIDBI merges----------------------------*/
			 }
		 }catch(Exception e){
			 return Error;
		 }
		 return NoError;
	 }
	 private ArrayList vectorToRouteUsersArrayList(Vector v)
	    {
	    	ArrayList routeUsersArrayList = new ArrayList();
	        for (int i = 0; i < v.size(); i++)
	        {
	        	RouteUsers routeUsers = new RouteUsers((String) v.get(i));
	        	routeUsersArrayList.add(routeUsers);
	        }
	        return routeUsersArrayList;
	    }
	 public int sendDocInAnotherRoute(String sendToRouteId, int docId, String docName){
		 int returnValue = 0;
		 int userLevel = 1;
		 Vector routeInfo = new Vector();
		 String routeInitiatedBy = DRSConstants.drsAdminUser;
		 int routeId = Util.to_Number(sendToRouteId); 
		 try{
			 /**This part is added for zero pages check*/
			 Document doc1= null;
			 Vector doctypes = null;
			 Document doc = null;
			 /****CV2019 merges from SIDBI line--------------------------------***/
			 String param = "Allow 0 page document(s) to WF";
	     	 boolean allowZeroPages = vws.getServerSettingsInfo(this.roomName, sessionId, param);
	     	 DRSLog.dbg("allowZeroPages :: "+allowZeroPages);
	     	 if (!allowZeroPages) {
	     		 doctypes=new Vector();
	    		 doc = new Document();
	    		 doc.setId(docId);
	    		 vwClient.getDocumentInfo(sessionId, doc, doctypes);
	    		 doc1=(Document)doctypes.get(0);
	    		 DRSLog.dbg("sendDocInAnotherRoute document page count :"+doc1.getPageCount());
	     	 }
	     	 if ((allowZeroPages) || (allowZeroPages == false && doc1 != null && doc1.getPageCount() > 0)) {
				try {
					vwClient.getRouteInfo(sessionId, routeId, userLevel, routeInfo);
					if (routeInfo != null && routeInfo.size() > 0) {
						RouteInfo rInfo = (RouteInfo) routeInfo.get(0);
						String routeName = rInfo.getRouteName();
						int taskSequence = 0;
						sendDocInAutoRoute(docId, docName, routeId, routeName, taskSequence, routeInitiatedBy);
					} // checking routeInfo null end
				} catch (Exception ex) {
					returnValue = Error;
				}
				returnValue = NoError;
	     	 } else {
				 DRSLog.err("Document '"+docName+"' has no pages. Can not send to work-flow.");
				 returnValue = Error;//Zero pages
			 }
			 doc = null;
			 doc1 = null;
			 /*------------------End of SIDBI merges----------------------------*/
		 } catch (Exception e) {
			 DRSLog.dbg("Exception in sendDocInAnotherRoute method :"+e.getMessage());
		 }
		 return returnValue;
	 }
	 
	 /*public int getRouteCompletedDocs1() {
		 DRSLog.add("inside getRouteCompletedDocs....");
		 VWCUtil vwUtil = new VWCUtil();
			String libName="VWView.dll";
			//String libPath = "D:\\tmp\\System\\";
			DRSLog.add("before calling loadNativeLibraryForDRS");
			VWCUtil.home = "C:\\Program Files (x86)\\Contentverse Client";
			vwUtil.loadNativeLibraryForDRS(libName, "");
			vwUtil.convertToPDF("C:\\Users\\narendra\\WorkFlow\\40\\1workflow11_manual workflow.pdf", "C:\\Users\\narendra\\WorkFlow\\40\\all.zip", "","", 0, 0, 1, 24,1);
			DRSLog.add("after dlll method....");
			return 1;
	 }*/
	 
	 public int getRouteCompletedDocs()
	 {
		 	ServerSchema vwss = DRSPreferences.getVWS();
			VWS vws = (VWS) Util.getServer(vwss);
			int routeMasterId = 0;
			int docId = 0;
			int routeId = 0;
			int postActionId = 0;
			String docName = null;
	      	String moveLocation = "";
	      	String reportLocation = "";
	      	int emailOriginator = 0;
	      	int sendMail = 0;
	      	String mailIds = "";
	      	String indexValue = "";
	      
	         try
	         {
	         	/*Cast(x.Docid AS Varchar)   	 + CHAR(9) + 
	    		ISNULL(x.Name,' ')		 + CHAR(9) + 
	    		Cast(x.Routeid AS Varchar) 	 + CHAR(9) + 
	    		LTRIM(RTRIM(ri.Name))	   	 + CHAR(9) + 
	    		LTRIM(RTRIM(ri.StartAction)) 	 + CHAR(9) +  
	    		LTRIM(RTRIM(ri.StartLocation))	 + CHAR(9) +  
	    		LTRIM(RTRIM(ri.ProcessType))	 + CHAR(9) + 
	    		Cast(ri.RouteActionid AS Varchar)+ CHAR(9) + 
	    		LTRIM(RTRIM(ri.ActionLocation))  + CHAR(9) AS Fld*/
	        	Vector retDocs = new Vector();
	         	int returnId = vwClient.getDocsForFinalAction(sessionId, retDocs);
	         	DRSLog.dbg("getDocsForFinalAction returnId : "+returnId);
	         	if(returnId< 0){
	         		if(returnId == ViewWiseErrors.invalidSessionId){
	         			message = ResourceManager.getDefaultManager().getString("CVProduct.Name") +" "+ resourceManager.getString("RouteDoc.ServerDown");
	         			DRSLog.war(message);
	        			vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
	         		}
	         		return Error;
	         	}
	         	if(retDocs == null || (retDocs!=null && retDocs.size()<=0)){
					DRSLog.add(""+roomName+": No documents to process ");
					return NoError;
	         	}
	         	if(retDocs != null && retDocs.size() > 0){
	         		DRSLog.dbg("final action resultset size : "+retDocs.size());
		   			for(int index=0; index < retDocs.size(); index++) {
		   				//DRSLog.dbg("retDocs.get(index).toString(): "+retDocs.get(index).toString());
						StringTokenizer stDocInfos = new StringTokenizer(retDocs.get(index).toString(), "\t");
						routeMasterId = Util.to_Number(stDocInfos.nextToken());
						docId = Util.to_Number(stDocInfos.nextToken());
						docName = stDocInfos.nextToken();
						//DRSLog.add("Route Completed Document Name :"+docName);
						routeId = Util.to_Number(stDocInfos.nextToken());
						routeNameStr = stDocInfos.nextToken();
						String routeDesc = stDocInfos.nextToken();
						String startAction =  stDocInfos.nextToken();
						postActionId = Util.to_Number(stDocInfos.nextToken());
						//DRSLog.add("postActionId from db...."+postActionId);
						//if(postActionId == 1 || postActionId == 2 || postActionId == 3 || postActionId == 4)
						moveLocation = stDocInfos.nextToken();
						reportLocation = stDocInfos.nextToken();
						emailOriginator = Util.to_Number(stDocInfos.nextToken());
						sendMail = Util.to_Number(stDocInfos.nextToken().toString());
						mailIds = stDocInfos.nextToken();
						//DRSLog.add("Mail id's from FinalAction procedure :"+mailIds);
						indexValue = stDocInfos.nextToken();
						compressDocFlag=Integer.parseInt(stDocInfos.nextToken());
						//DRSLog.add("compressDocFlag::::"+compressDocFlag);
						convertToPdfFlag=Integer.parseInt(stDocInfos.nextToken());
						//DRSLog.add("convertToPdfFlag :::: "+convertToPdfFlag);
						//DRSLog.add("before calling dopostrouteaction");
						doPostRouteAction(retDocs, routeMasterId, docId, routeId, postActionId, docName, 
								moveLocation, reportLocation, sendMail, mailIds, indexValue,compressDocFlag,convertToPdfFlag);
						DRSLog.dbg("after calling dopostrouteaction");
						stDocInfos = null;
						docId = 0;
						postActionId = 0;
						moveLocation = null;
						indexValue = null;
						if(index == retDocs.size()-1)
							DRSLog.dbg(""+roomName+": No documents to process ");
		   			}
	         	}
	         }catch(Exception ex){
	        	 //DRSLog.add("Exception "+ex.getMessage());
	        	 return Error;
	        	
	         }
		 return NoError;
	 }
	 
	 public void doPostRouteAction(Vector retDocs,int routeMasterId, int docId, 
			 int routeId, int postActionId, String docName, String moveLocation, 
			 String reportLocation, int sendMail, String mailIds, String indexValue,int compress,int convertopdf) throws RemoteException
	 {
		    //DRSLog.add("inside doPostRouteAction......");
		    ServerSchema vwss = DRSPreferences.getVWS();
			VWS vws = (VWS) Util.getServer(vwss);
			int returnValue = -1;
			int docPageCount = -1;
			String docMetaDataPath = moveLocation;
			Node node = null;
			Document doc = new Document(docId); 
			node = new Node(docId);
			/****CV2019 merges from SIDBI line--------------------------------***/
			Vector vectDoctype=new Vector();
			String wfdoctype="";
			Vector htmlContents = new Vector();
			boolean reportGenerated =false;
			Vector versionSummary = null;
			String param = null;
			Vector result = null;
			List<String> wfdoctypeList=new ArrayList<String>();
			vwClient.getWorkflowDoctype(sessionId, vectDoctype);
			if(vectDoctype.size()>0&&vectDoctype!=null){
				wfdoctype=(String) vectDoctype.get(0);
				String[] wfdoctyparray = wfdoctype.split(";");
				Collections.addAll(wfdoctypeList, wfdoctyparray);
			}
			DRSLog.dbg("wfdoctypeList..........."+wfdoctypeList);
			DRSLog.dbg("postActionId..........."+postActionId);
			/****End of SIDBI merges---------------------------------------***/
			if(postActionId == 1){// || (postActionId == 2)){
				if(moveLocation!=null && !moveLocation.equals("") && moveLocation.indexOf(":")!=-1)
					moveLocation = moveLocation.substring(0, moveLocation.indexOf(":"));
			}
			Vector name  = vws.getLockOwner(roomName, sessionId, docId, new Vector(), false);
			DRSLog.dbg("getLockOwner..........."+name);
			if(name!=null && name.size()>0){
				StringTokenizer st = new StringTokenizer((String) name.get(0), Util.SepChar);
				String resultValue = st.nextToken();
				if(!resultValue.equalsIgnoreCase("0")&&  !resultValue.equalsIgnoreCase("1") && !resultValue.equalsIgnoreCase("2")){
					message = resourceManager.getString("RouteDoc.CouldNotLockDoc1")+roomName+" "+
							resourceManager.getString("RouteDoc.CouldNotLockDoc2")+" '"+docId+"' "
							+resourceManager.getString("RouteDoc.CouldNotLockDoc3")+" '"+docName+"'";
        			DRSLog.war(message);
        			vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
		        	return;
				}
			}
			/**CV2019 Merges  for SIDBI ---------------------------------------**/
			Vector doctypes=new Vector();
			DRSLog.dbg("Before calling getDocumentInfo..");
			vwClient.getDocumentInfo(sessionId,doc,doctypes);
			Document doc1=(Document)doctypes.get(0);
			Vector docRouteSummary =null;
			DRSLog.dbg("xxx wfdoctypeList......"+wfdoctypeList);
			if(wfdoctypeList!=null&&wfdoctypeList.size()>0){
				DRSLog.dbg("wfdoctypeList is not null :::"+wfdoctypeList);
			DRSLog.dbg("xxx docType......"+doc1.getDocType().getName());
			DRSLog.dbg("xxx wfdoctypeList.contains(doc1.getDocType().getName())......"+wfdoctypeList.contains(doc1.getDocType().getName()));
			if(wfdoctypeList.contains(doc1.getDocType().getName())){
				DRSLog.dbg("Inside new route summary");
				docRouteSummary	= vws.getRouteNewSummary(roomName, sessionId, docId, 0);
				// This code is added by vanitha
				// If system flag is YES then version revision html file has to be generated and it has to be send to DTC along with summary html  
				try {
					param = "Include Version history in WF report";
					DRSLog.dbg("roomName :"+roomName+"   sessionId :"+sessionId);
					result = new Vector();
					result = vws.getSettingsInfo(roomName, sessionId, param, result);
					DRSLog.dbg("result..........."+result);
					if(result!= null && !result.isEmpty()){
						if (String.valueOf(result.get(0)) != null && String.valueOf(result.get(0)).equalsIgnoreCase("YES")) {
							try {
								versionSummary = vws.getDocumentVersionInfo(roomName, sessionId, docId);
							} catch (Exception e) {
								DRSLog.dbg("Exception in get document version info "+e.getMessage());
							}
						}
						result.removeAllElements();
					}
				} catch (Exception rev) {
					DRSLog.dbg("Exception after calling VWGetSettingsInfo method : " + rev.getMessage());
				}
				DRSLog.dbg("After calling getDocumentVersionInfo...."+versionSummary);
			}else{
				DRSLog.dbg("Inside route summary");

				docRouteSummary=vws.getRouteSummary(roomName, sessionId, docId, 0);
			}
			}else{
				DRSLog.dbg("Inside route summary 2 ");

				docRouteSummary=vws.getRouteSummary(roomName, sessionId, docId, 0);
			}			
			/****End of SIDBI merges---------------------------------------***/
			DRSLog.dbg("postActionId...."+postActionId);
			switch(postActionId)
			{
				case 0:		//lock
					returnValue = vws.lockDocument(roomName, sessionId, docId);
					//DRSLog.add("doc locked by DRS ");
					if(returnValue == NoError){
						vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Locked By DWS -Locked");
						vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
						DRSLog.add("RoomName:"+roomName+" Action: 'Read Only' processed for document '"+docName+"'");
					}
					break;
				case 1: //move
					String actionId = "";
					String indexId = "";
					int indexOfSlash = 0;
					boolean finalAction = false;
					boolean createNewFolder = false;
					if(moveLocation.contains("\\")){
						indexOfSlash = moveLocation.indexOf("\\");
						actionId = moveLocation.substring(0,indexOfSlash);
						indexId = moveLocation.substring(indexOfSlash+1, moveLocation.length());
						if(!indexId.equals("0") && !indexValue.equals("~") && !indexValue.equals("-")){
							createNewFolder = true;
							finalAction = true;	
						}else if(!indexId.equals("0") && indexValue.equals("-")){
							createNewFolder = false;
							node.setParentId(Integer.parseInt(actionId));
							finalAction = true;
							String message = resourceManager.getString("RouteDoc.DocToParent1")+docName+resourceManager.getString("RouteDoc.DocToParent2");
							DRSLog.war(message);
							int ret = vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
						}
						else if(!indexId.equals("0") && indexValue.equals("~")){
							node.setParentId(Integer.parseInt(actionId));
							createNewFolder = false;
							finalAction = true;
							String message = resourceManager.getString("RouteDoc.DocToParentIndexName1")+docName+resourceManager.getString("RouteDoc.DocToParentIndexName2");
							DRSLog.war(message);
							int ret = vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
						}
						else if(indexId.equals("0")){//Old route functionality
							node.setParentId(Integer.parseInt(actionId));
							createNewFolder = false;
							finalAction = true;	
						}
						//DRSLog.add("MoveLocation contains index id too.... after seperation actionId is :: "+actionId);
					}

					if(createNewFolder){//Create new folder for the enhancement
						node = new Node(1); 
						Vector nodeList = new Vector();
						String documentId = String.valueOf(docId);
						Vector list = vws.getNodeDetails(roomName, sessionId, actionId, indexValue, nodeList);						
						boolean isDocMoved = false;
						for (int count = 0; count < list.size(); count++){
							StringTokenizer stToken = new StringTokenizer(list.get(count).toString(), "\t");
							String parentId = stToken.nextToken();
							stToken.nextToken();
							stToken.nextToken();
							stToken.nextToken();
							int nodeType = Integer.parseInt(stToken.nextToken().trim());
							if (nodeType == 0){
								moveLocation = parentId;
								node = new Node(docId);
								node.setParentId(Integer.parseInt(moveLocation));
								isDocMoved = true;
							}
						}
						if (!isDocMoved){
							node.setParentId(Integer.parseInt(actionId));
							node.setName(indexValue);
							int createdNodeId = vws.createNode(roomName, sessionId, node, 0);
							node.setId(docId);
							node.setParentId(createdNodeId);							
						}
					}else{
						//If create folder is false then only need to check for existance of destination node id
						Node destNode = new Node(node.getParentId());
						destNode = vws.getNodePropertiesByNodeID(roomName, sessionId, destNode);

						//Move Location not exist in the procedure call after deletion of destination location.
						//DRSLog.war(moveLocation+" not exist.");
						try{
							if(destNode.getPath().equals("") || destNode.getPath().length()==0){
								node.setId(docId);

								Node docNode = new Node(docId);
								docNode = vws.getNodePropertiesByNodeID(roomName, sessionId, docNode);
								String newFolName = routeNameStr+"_pending"; 
								docNode.setName(newFolName);
								String newPath = docNode.getPath();
								newPath = newPath.substring(0,(newPath.indexOf(docName)));
								newPath = newPath + newFolName;

								String srcParentId = String.valueOf(docNode.getParentId());
								Vector nodeList = new Vector();
								Vector list = vws.getNodeDetails(roomName, sessionId, srcParentId, newFolName, nodeList);
								
								boolean routeNameFolderNotExist = true;
								//This is for getting the first folder location, if it has many with the same name
								if(list != null && list.size()>0){
									StringTokenizer stToken = new StringTokenizer(list.get(0).toString(), "\t");
									int parentId = Integer.parseInt(stToken.nextToken());
									node.setParentId(parentId);
									routeNameFolderNotExist = false;
								}

								//Need to create folder in the name of route and set the created id as parentId to move the document.
								if(routeNameFolderNotExist){
									int createdNodeId = vws.createNode(roomName, sessionId, docNode, 0);
									node.setParentId(createdNodeId);
								}

								DRSLog.add("Document '"+docName+"' of "+ Constants.WORKFLOW_MODULE_NAME +" <"+routeNameStr+"> will move to : "+newPath);
								finalAction = true;
							}else{
								if((moveLocation.contains("\\") && indexValue.equals("~")) || (moveLocation.contains("\\") && indexValue.equals("-"))){
									if(indexId.equals("0")){//Old route functionality
										finalAction = true;	
									}
									node = new Node(docId);
									node.setParentId(Integer.parseInt(actionId));
								}
							}
						}catch(Exception ex){
							//DRSLog.add("Exception : "+ex);
						}
					}
					/*if(vws.isDocDeleted(roomName, sessionId, docId)){
							DRSLog.war("Action: 'Move' is not processed for document '"+docName+"'"+". Document folder missing");
							break;
						}*/
					if(finalAction){
						returnValue = vws.moveNode(roomName, sessionId, node);
						if(returnValue == NoError){
							//AT handling in Script
							//vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Moved By DRS -Moved");
							vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
							DRSLog.add("RoomName:"+roomName+" Action: 'Move' processed for document '"+docName+"'");
						}
					}
					break;
				case 2:					//Publish and lock
					if(moveLocation!=null && !moveLocation.equals("")){
				  		File filePath = new File(moveLocation);
				  		if(!filePath.exists() || !filePath.isDirectory() || !filePath.canWrite()){
				  			message = resourceManager.getString("RouteDoc.NoWritePermission1")+" "+roomName+" "+resourceManager.getString("RouteDoc.NoWritePermission2")+" "+moveLocation+" "+resourceManager.getString("RouteDoc.NoWritePermission3")+" "+VWCUtil.getHome()+"\\Server_Generated_Files";
							DRSLog.war(message);
							vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
				  			moveLocation = VWCUtil.getHome()+"\\Server_Generated_Files";
				  		}
					}
					docPageCount = vwClient.getPageCount(sessionId, doc);
					
					//XML creation for document metadata
					docNameStr = VWCUtil.fixFileName(docName);
					docMetaDataPath = moveLocation+"\\"+docNameStr+"_"+docId+"\\"+docId+"\\"+docNameStr+"_MetaData.xml";
					VWCUtil.initFile(docMetaDataPath, "");
					VWCUtil.getDocumentMetaData(sessionId, vwClient, doc);
					VWCUtil.closeFile();
					
					if(docPageCount>0){
						moveLocation = moveLocation+"\\"+docNameStr+"_"+docId;
						returnValue = vwClient.publish(sessionId, doc, moveLocation);
						

					}else {
						vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Processed By DWS - Not Published and Locked");
						vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
						DRSLog.add("RoomName:"+roomName+" Action: 'Publish and Lock' not processed for document '"+docName+"' having no pages");
					}
					if(returnValue == NoError){
						returnValue = vws.lockDocument(roomName, sessionId, docId);
						//DRSLog.add("Document Published and locked by DRS");
						if(returnValue == NoError){
							vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Processed By DWS -Published and Locked");
							vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
							DRSLog.add("RoomName:"+roomName+" Action: 'Publish and Lock' processed for document '"+docName+"'");
						}
					}
					break;
				case 3: //					Publish
					if(moveLocation!=null && !moveLocation.equals("")){
				  		File filePath = new File(moveLocation);
				  		if(!filePath.exists() || !filePath.isDirectory() || !filePath.canWrite()){
				  			message = resourceManager.getString("RouteDoc.NoWritePermission1")+" "+roomName+" "+resourceManager.getString("RouteDoc.NoWritePermission2")+" "+moveLocation+" "+resourceManager.getString("RouteDoc.NoWritePermission3")+" "+VWCUtil.getHome()+"\\Server_Generated_Files";
							DRSLog.war(message);
							vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
				  			moveLocation = VWCUtil.getHome()+"\\Server_Generated_Files";
				  		}
					}
					docPageCount = vwClient.getPageCount(sessionId, doc);
					
					//XML creation
					docNameStr = VWCUtil.fixFileName(docName);
					docMetaDataPath = moveLocation+"\\"+docNameStr+"_"+docId+"\\"+docId+"\\"+docNameStr+"_MetaData.xml";
					VWCUtil.initFile(docMetaDataPath, "");
					VWCUtil.getDocumentMetaData(sessionId, vwClient, doc);
					VWCUtil.closeFile();
					
					if(docPageCount > 0){
						moveLocation = moveLocation+"\\"+docNameStr+"_"+docId;
						returnValue = vwClient.publish(sessionId, doc, moveLocation);
					}else{
						vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Processed By DWS - Not Published");
						vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
						DRSLog.add("RoomName:"+roomName+" Action: 'Publish' not processed for document '"+docName+"' having no pages");
					}
					
					//DRSLog.add("Document Published by DRS "+returnValue);
					if(returnValue == NoError){
						vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Processed By DWS -Published");
						vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
						DRSLog.add("RoomName:"+roomName+" Action: 'Publish' processed for document '"+docName+"'");
					}
					break;
				case 4:	// Send the document to another route
					returnValue = sendDocInAnotherRoute(moveLocation, docId, docName);
					if(returnValue == ViewWiseErrors.NoError){
						vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Processed By DWS -Sent to Another "+ Constants.WORKFLOW_MODULE_NAME);
						vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
						DRSLog.add("RoomName:"+roomName+" Action: 'Send in another "+ Constants.WORKFLOW_MODULE_NAME.toLowerCase() +"' processed for document '"+docName+"'");
					}
					break;
				case 5: // No action needed - Previously had Generate report as action 5 that is removed
					vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
					vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Processed By DWS -No Action");
					DRSLog.add("RoomName:"+roomName+" Action: 'No Action' processed for document '"+docName+"'");
					break;
				case 6: // Email Documents 
					vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
					vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Processed By DWS - Email Documents");
					DRSLog.add("RoomName:"+roomName+" Action: 'Email Document' processed for document '"+docName+"'");
					break;
					
				case 7: 
					// Conver to pdf - CV2019 merges from SIDBI line***/
					if(wfdoctypeList.contains(doc1.getDocType().getName())){
						String dssPath="";
						Vector storageResult=new Vector();
			  			vwClient.getAzureStorageCredentials(sessionId,docId,storageResult);
			  			if(storageResult!=null&&storageResult.size()>0) {
			  				String storageArray[]=storageResult.get(0).toString().split(Util.SepChar);
			  				if(storageArray.length>0&&storageArray[5].length()>0){
			  					dssPath=storageArray[5];
			  				}
			  			}
			  			DRSLog.dbg("Before calling generateDRSReportNew report....."+docName);
						reportGenerated =generateDRSReportNew(docRouteSummary, docName, reportLocation,postActionId, htmlContents,dssPath,doc1.getDocType().getName(),versionSummary);
					}
					DRSLog.add("reportGenerated status for office Notes ::"+reportGenerated);
					if(reportGenerated==true){
						vws.updateRouteCompletedDocs(roomName, sessionId, routeMasterId, Constants.WORKFLOW_MODULE_NAME.toLowerCase());
						int lockRetValue = vws.lockDocument(roomName, sessionId, docId);
						vws.addATRecord(roomName, sessionId, docId, VWATEvents.AT_OBJECT_DRS, VWATEvents.AT_DRS_APPLIED, "Document Processed By DWS - Pdf generated with docpages and workflow summary report");
						DRSLog.add("RoomName:"+roomName+" Action: 'Pdf generated with docpages and workflow summary report' processed for document '"+docName+"'");
	  					DRSLog.dbg("Retrun value from lockDocument :::"+lockRetValue);
					}
					break;
					/****End of SIDBI merges---------------------------------------***/
				default : DRSLog.err("UNKNOWN Post "+ Constants.WORKFLOW_MODULE_NAME +" Action");
			}
			
			// Generate Report
			Vector<VWEmailOptionsInfo> vwEmailOptionsInfoList = new Vector<VWEmailOptionsInfo>();
			Vector<VWEmailOptionsInfo> vwrEmailOptionsList = null;
			DRSLog.dbg("mailIds :::: "+mailIds);
			vwEmailOptionsInfoList = Util.getEmailOptionsInfoVector(mailIds);
			DRSLog.dbg("vwEmailOptionsInfoList :::: "+vwEmailOptionsInfoList);
			int docMetaData = -1;
			int reportSummary = -1;
			int docPages = -1;
			int docVwr = -1;
			Vector emailOptions = null;
			boolean deleteFiles = false;
			int res = 0;
			//, sendMail, mailIds, emailOptions
			Vector storageResult=new Vector();
  			/****CV2019 merges from SIDBI line--------------------------------***/
			/*DRSLog.add("Before calling generateRouteSummaryHTML.....");
  			generateRouteSummaryHTML(2013, 0, "C:\\WorkFlowReport");
  			DRSLog.add("After calling generateRouteSummaryHTML......");*/			
  			String dssPath="";
  			vwClient.getAzureStorageCredentials(sessionId,docId,storageResult);
  			if(storageResult!=null&&storageResult.size()>0) {
				String storageArray[]=storageResult.get(0).toString().split(Util.SepChar);
				if(storageArray.length>0&&storageArray[5].length()>0){
					dssPath=storageArray[5];
				}
				/**CV2019 merges from 10.2 line - PDF conversion changes***/
				reportGenerated = generateDRSReport(docRouteSummary, docName, reportLocation, htmlContents,compress,convertopdf,dssPath,vwEmailOptionsInfoList);
	
				String messageStr = Constants.WORKFLOW_MODULE_NAME +" '"+routeNameStr+"' completed for Document '"+docName+"'";
				Session session = vwClient.getSession(sessionId);
				vws.addNotificationHistory(roomName, sessionId, routeId, VNSConstants.nodeType_Route, VNSConstants.notify_Route_Comp, session.user, "-", "-", messageStr);
				 
				/**CV2019 merges from SIDBI line - VWR email option changes***/
				if(vwEmailOptionsInfoList!=null && vwEmailOptionsInfoList.size()>0) {
					boolean sidbiCustomization = vws.getSidbiCustomizationFlag(roomName, sessionId);
					DRSLog.dbg("email options list BF :::: "+vwEmailOptionsInfoList.size());
					//if (sidbiCustomization)
						vwrEmailOptionsList = segrigateEmailOptionsInfoList(vwEmailOptionsInfoList);
					DRSLog.dbg("email options list AF :::: "+vwEmailOptionsInfoList.size());
					for(int i=0; i<vwEmailOptionsInfoList.size(); i++){
						//Need to delete the zip file and metadata.html file after sending mail to all email options
						if(i==(vwEmailOptionsInfoList.size()-1))
							deleteFiles = true;
						else
							deleteFiles = false;
						//
						emailOptions = new Vector();
						VWEmailOptionsInfo vwEmailOptionsInfo = (VWEmailOptionsInfo)vwEmailOptionsInfoList.get(i);
						DRSLog.dbg("vwEmailOptionsInfo.getUserEmailAddress ::: "+vwEmailOptionsInfo.getUserEmailAddress());
						mailIds = vwEmailOptionsInfo.getUserEmailAddress().trim();
						docMetaData = vwEmailOptionsInfo.getDocumentMetaDataFlag();
						DRSLog.dbg("docMetaData ::: "+docMetaData);
						reportSummary = vwEmailOptionsInfo.getRouteSummaryReportFlag();
						DRSLog.dbg("reportSummary ::: "+reportSummary);
						docPages = vwEmailOptionsInfo.getDocumentPagesFlag();
						DRSLog.dbg("docPages ::: "+docPages);
						docVwr = vwEmailOptionsInfo.getDocumentVwrFlag();					
						emailOptions.add(docMetaData);
						emailOptions.add(reportSummary);
						emailOptions.add(docPages);
						emailOptions.add(docVwr);
						DRSLog.dbg("docVwr :: "+docVwr);
						DRSLog.dbg("mailIds  :: "+mailIds);
						emailOptions.add(compress);
						emailOptions.add(convertopdf);
						
						
						//Empty email options table - need to generate route summary in the report location
						if(mailIds.trim().equals("-") && vwEmailOptionsInfoList.size()==1){
							emailOptions = null;
							try{
								if(sendMail!=0 && !mailIds.trim().equals("")){
									//File attachment = new File(path);
									Vector emailIds = new Vector();
									//if(!emailIds.contains(mailIds))
										emailIds.add(mailIds);
										if(!reportLocation.trim().equalsIgnoreCase("-")){
											res = vws.sendMailWithAttachment(roomName, session.server, sessionId, new String(), docName, routeNameStr, docId, emailIds, htmlContents, Constants.Route_Status_Notify_OnRptGen, null, emailOptions, destPathStr);
										}
									}
							}catch(Exception e){
					   		 	DRSLog.add(e.toString());
							}	
							
							
						}else{
							if(!mailIds.trim().equals("-") && !mailIds.equals("~") && !mailIds.equals("^")){
								if(docMetaData!=0 || reportSummary!=0 || docPages!=0 || docVwr!=0){
									DRSLog.dbg("inside else part 2................ "+sendMail);
									try{
										if(sendMail!=0 && !mailIds.trim().equals("")){
											DRSLog.dbg("before calling sendMailWithAttachment.....");
											//File attachment = new File(path);
											Vector emailIds = new Vector();
											//if(!emailIds.contains(mailIds))
											emailIds.add(mailIds);
											DRSLog.dbg("reportLocation......"+reportLocation);
											if(!reportLocation.trim().equalsIgnoreCase("-")){
												DRSLog.dbg("before calling sendMailWithAttachment.....");
												res = vws.sendMailWithAttachment(roomName, session.server, sessionId, new String(), docName, routeNameStr, docId, emailIds, htmlContents, Constants.Route_Status_Notify_OnRptGen, null, emailOptions, destPathStr);
												DRSLog.dbg("after calling sendMailWithAttachment..... "+res);
											}
										}
									}catch(Exception e){
							   		 	DRSLog.add(e.toString());
									}	
									DRSLog.dbg("ReportGenerated........"+reportGenerated);
									if(reportGenerated)
										DRSLog.add("DWSReport generated for Mail Id : "+mailIds);
									else if(!reportLocation.trim().equalsIgnoreCase("-")){
										DRSLog.add("DWSReport generated; Failed to send mail for : "+mailIds);
										}
									mailIds = "";
								}
							}else{
								if(mailIds.equals("-")){
									DRSLog.add("DWS Report cannot be generated : IndexName does not exist in ["+docName+"]");
								}
								if(mailIds.equals("~")){
									DRSLog.add("DWS Report cannot be generated : IndexValue is empty");
								}
								if(mailIds.equals("^")){
									DRSLog.add("DWS Report cannot be generated : Invalid Mail id for originator");
								}
							}
						}
					}
					/**This part has added for vwr flag enabled mail.Only one common mail will be generated for all the mail id which is having vwr flag true**/
					VWEmailOptionsInfo vwEmailOptionsInfo = null;
					emailOptions = new Vector();
					mailIds = "";
					DRSLog.dbg("email options list for VWR AF :::: "+vwrEmailOptionsList);					
					if (vwrEmailOptionsList != null && vwrEmailOptionsList.size() > 0) {
						DRSLog.dbg("email options list for VWR AF :::: "+vwrEmailOptionsList.size());
						Vector emailIds = new Vector();					
						for (int listIndex = 0; listIndex < vwrEmailOptionsList.size(); listIndex ++) {
							vwEmailOptionsInfo = (VWEmailOptionsInfo)vwrEmailOptionsList.get(listIndex);
							if(vwEmailOptionsInfo.getUserEmailAddress() != null && vwEmailOptionsInfo.getUserEmailAddress().length() > 0 
									&& !vwEmailOptionsInfo.getUserEmailAddress().trim().equals("-") 
									&& !vwEmailOptionsInfo.getUserEmailAddress().equals("~") 
									&& !vwEmailOptionsInfo.getUserEmailAddress().equals("^")){
								mailIds = mailIds+","+vwEmailOptionsInfo.getUserEmailAddress().trim();
								if (listIndex == 0) {								
									emailOptions.add(vwEmailOptionsInfo.getDocumentMetaDataFlag());
									emailOptions.add(vwEmailOptionsInfo.getRouteSummaryReportFlag());
									emailOptions.add(vwEmailOptionsInfo.getDocumentPagesFlag());
									emailOptions.add(vwEmailOptionsInfo.getDocumentVwrFlag());	
								}
							}						
						}
						if(!reportLocation.trim().equalsIgnoreCase("-") && mailIds.trim().length() > 0){
							if (mailIds.startsWith(",")) mailIds = mailIds.substring(1, mailIds.length());
							emailIds.add(mailIds);
							if (sidbiCustomization) {
								res = vws.sendMailWithAttachment(roomName, session.server, sessionId, new String(), docName, routeNameStr, docId, emailIds, htmlContents, Constants.Route_Status_Notify_OnVwrRptGen, null, emailOptions, destPathStr);
							} else {
								res = vws.sendMailWithAttachment(roomName, session.server, sessionId, new String(), docName, routeNameStr, docId, emailIds, htmlContents, Constants.Route_Status_Notify_OnRptGen, null, emailOptions, destPathStr);
							}
							if (res == 0 && reportGenerated) {
								DRSLog.add("DWSReport generated for Mail Id : "+mailIds);
							}
						}
					}
					vwEmailOptionsInfo = null; emailOptions = null; vwrEmailOptionsList = null;
				}				
				/**************-------------------------------------------------------------------------------***************/
				
				if(deleteFiles){
					//Need to delete the metadata and zip file and files that are published.
					DRSLog.dbg("is DebugCheck enabled :"+VWSPreferences.getDebugCheck());
					if (!VWSPreferences.getDebugCheck()) {
						deleteGeneratedFiles(docNameStr, routeNameStr, destPathStr, docId);
						deleteGeneratedFiles(docNameStr, routeNameStr, destTempPathStr, docId);
					}
				}
			}
  			/*------------------End of SIDBI merges----------------------------*/
			
	 }
	 
	 /**
	  * CV2019 merges from SIDBI line
	  * @param docName
	  * @param routeName
	  * @param destPath
	  * @param docId
	  */
	 private void deleteGeneratedFiles(String docName, String routeName, String destPath, int docId){
		 String docMetaDataPath = destPath+"\\"+docId+"_"+routeName+"_MetaData.html";
		 String docMetaDataAndSummaryPath = destPath+"\\"+docId+"_"+routeName+"_MetaDataAndSummary.html";
		 String zipFilePath = destPath+"\\"+docId+"_"+routeName+"_Zip.zip"; 
		 String pdfPath = destPath+"\\"+docId+"_"+routeName+".pdf";
		 String allzipLocation= destPath+"\\"+String.valueOf(docId);
		 String docPagesPath = destPath + "\\DocumentPages";	
		 try{
				File zipFile = new File(zipFilePath);
				File metaFile = new File(docMetaDataPath);
				File metaDataSummary = new File(docMetaDataAndSummaryPath);
				File pdfFile = new File(pdfPath);
				File allzipFolder = new File(allzipLocation);
				File docPagesFile = new File(docPagesPath);
				if(zipFile.exists()){
					zipFile.delete();
				}
				if(metaFile.exists()){
					metaFile.delete();
				}
				if(metaDataSummary.exists()){
					metaDataSummary.delete();
				}
				if(allzipFolder.exists()){					
					deleteDirectory(allzipFolder, true);
				}
				if(pdfFile.exists()){					
					pdfFile.delete();
				}
				if(docPagesFile.exists()){					
					deleteDirectory(docPagesFile, false);
				}
				if(!docPublishedPath.equals("")){
		  			File publishedPath = new File(docPublishedPath);
		  			if(publishedPath.exists()){
		  				File list[] = publishedPath.listFiles();
		  				if(list!=null && list.length>0){
		  					for(int index=0; index<list.length; index++){
		  						list[index].delete();
		  					}
		  				}
		  				publishedPath.delete();
		  			}
		  		}
			}catch(Exception ex){
				DRSLog.add("Exception in deleting files : "+ex.getMessage());
			}
	 }
	 /*------------------End of SIDBI merges----------------------------*/
	 
	 public void deleteDirectory(File file, boolean folderDelete) {
		    if( file.exists() ) {
		        if (file.isDirectory()) {
		            File[] files = file.listFiles();	  
		            if (files != null && files.length > 0) {
			            for(int i=0; i<files.length; i++) {
			                if(files[i].isDirectory()) {
			                    deleteDirectory(files[i], true);
			                }
			                else {
			                    files[i].delete();
			                }
			            }
		            }
		        }
		        if (folderDelete)
		        	file.delete();
		    }
	}
	 /****CV2019 merges from SIDBI line - Watermark pdf changes--------------------------------***/
	 private boolean generateDRSReportNew(Vector retDocs, String docName, String path,int postActionId, Vector htmlContents,String dssPath,String doctypeName, Vector versionSummary)
	 {
		 int routeId = 0;
		 String versionFileName = null;
  		 String versionFilePath = null;
  		 boolean saveInFile = false;
		 if(path!=null&&path.trim().equals("-")){
			 return false;
		 }
		 try{
		  	int row = retDocs.size();
		  	int column = DRSConstants.ReportColumnNamesNew.length;//6//9
		  	DRSLog.dbg("column"+column);
		  	DRSLog.dbg("retDocs ::"+retDocs);
		  	String[][] data = new String[row][column];//7//9
		  	///String path = null;
		  	StringTokenizer stDocInfos;
		  	
		  	int docId = 0;
		  	
		  	//String sNo = "";
		  	String userName = "";
		  	String comments = "";
		  	String action = "";
		  	String receivedOn = "";
		  	String processOn = "";
		  	String routeName ="";
		  	String docVersion="";
		  	String forwardTo="";
		  	String referenceCount="";
		  	String docPublishPath = "";
		  	String offNoteSubVal="";
		  	String offNoteVerticalVal="";
		 	String selDocName="";
		 	String summaryFilePath = null;
		  	for(int i = 0; i<row; i++){
		  		stDocInfos = new StringTokenizer(retDocs.get(i).toString(), "\t");
		  		DRSLog.dbg("counts ::::"+stDocInfos.countTokens());
		  		//DRSLog.add("stDocInfos ::"+stDocInfos);
		  		DRSLog.dbg("1 sno::::"+stDocInfos.nextToken());// Serial number
		  		docId = Util.to_Number(stDocInfos.nextToken());
		  		DRSLog.dbg("2 docid::::"+docId);
		  		DRSLog.dbg("3 docname::::"+stDocInfos.nextToken());//document name
		  		routeId = Util.to_Number(stDocInfos.nextToken());//routeid
		  		DRSLog.dbg("4 routeid::::"+routeId);
		  		routeName = stDocInfos.nextToken();//routename
		  		DRSLog.dbg("5 routename::::"+routeName);
		  		DRSLog.dbg("6 ruserid::::"+stDocInfos.nextToken());//route userid
		  		DRSLog.dbg("7 initiated by::::"+stDocInfos.nextToken());//orginator/intiated by
		  		receivedOn = stDocInfos.nextToken();
		  		DRSLog.dbg("8 received on::::"+receivedOn);
		  		action = stDocInfos.nextToken();
		  		DRSLog.dbg("9 action::::"+action);
		  		processOn = stDocInfos.nextToken();
		  		DRSLog.dbg("10 processOn::::"+processOn);
		  		comments = stDocInfos.nextToken();
		  		DRSLog.dbg("11 comments::::"+comments);
		  		DRSLog.dbg("12 tas seq::::"+stDocInfos.nextToken());//Task sequence
		  		DRSLog.dbg("13 taskname::::"+stDocInfos.nextToken());//task name
		  		DRSLog.dbg("14 taskdes ::::"+stDocInfos.nextToken());//Task Desc
		  		DRSLog.dbg("15 sendmail ::::"+stDocInfos.nextToken());//send mail status
		  		DRSLog.dbg("16 approved by ::::"+stDocInfos.nextToken());//ApprovedBy
		  		DRSLog.dbg("17 approved by name ::::"+stDocInfos.nextToken());//ApprovedByName
		  		DRSLog.dbg("18 authorized by ::::"+stDocInfos.nextToken());//AuthorizedBy
		  		DRSLog.dbg("19 authorized byname ::::"+stDocInfos.nextToken());//AuthorizedByName
		  		DRSLog.dbg("20 routesigntype ::::"+stDocInfos.nextToken());//routesignType
		  		DRSLog.dbg("21 wfstatus ::::"+stDocInfos.nextToken());//workflowstatus
				userName = stDocInfos.nextToken();
				DRSLog.dbg("22 userName ::::"+userName);
				docVersion = stDocInfos.nextToken();
				DRSLog.dbg("23 docVersion ::::"+docVersion);
				forwardTo = stDocInfos.nextToken();
				DRSLog.dbg("24 forwardTo ::::"+forwardTo);
				referenceCount = stDocInfos.nextToken();
				DRSLog.dbg("25 referenceCount ::::"+referenceCount);
				offNoteSubVal=stDocInfos.nextToken() ;//subject
				DRSLog.dbg("26 offNoteSubVal ::::"+offNoteSubVal);
				offNoteVerticalVal=stDocInfos.nextToken() ;//officenote
				DRSLog.dbg("27 offNoteVerticalVal ::::"+offNoteVerticalVal);

				int j = 0;
				DRSLog.dbg("userName ::::"+userName);
				DRSLog.dbg("receivedOn ::::"+receivedOn);
				DRSLog.dbg("action ::::"+action);
				DRSLog.dbg("processOn ::::"+processOn);
				DRSLog.dbg("comments ::::"+comments);
				DRSLog.dbg("docVersion ::::"+docVersion);
				DRSLog.dbg("forwardTo ::::"+forwardTo);
				DRSLog.dbg("referenceCount ::::"+referenceCount);
				data[i][j++] = userName;
				data[i][j++] = receivedOn;
				data[i][j++] = action;
				data[i][j++] = processOn;
				data[i][j++] = comments;
			
				data[i][j++] = docVersion;
				data[i][j++] = forwardTo;
				data[i][j++] = referenceCount;
				
			
		  	}	
		  	String actualFileName = docName;
		  	docName = VWCUtil.fixFileName(docName);
		  	routeName = VWCUtil.fixFileName(routeName);
		  	DRSLog.dbg("referenceCount ::::"+referenceCount);
		  	docNameStr = docName;
		  	routeNameStr = routeName;
		  	if(docName.length()>=75){
		  	selDocName=docName.substring(0, 75);
		  	}else{
		  		selDocName=docName;
		  	}
	  		//String fileName = docName+"_"+routeName+"_"+Util.getNow(3)+".html";
		  	//Modified the route summary name for the Route enhancement Email options
	  		String fileName =  docId+"_"+routeName+"_"+ Constants.WORKFLOW_MODULE_NAME +"Summary.html";
	  		// This is to get the original selected path without the fileName 
	  		String destPath = "";
		  	if(path!=null && !path.equals("") && !path.equals("-")){
		  		//		  	\\syntaxwkt-117\Data
		  		File filePath = new File(path);
		  		if(!filePath.exists() || !filePath.isDirectory() || !filePath.canWrite()){
		  			//DRSLog.war(" filePath.exists():"+filePath.exists()+" filePath.isDirectory():"+filePath.isDirectory()+" filePath.canWrite():"+filePath.canWrite());
		  			message = resourceManager.getString("RouteDoc.PathNotExist1")+" "+path+" "+resourceManager.getString("RouteDoc.PathNotExist2")+" "+resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RouteDoc.PathNotExist3")+" "+VWCUtil.getHome()+"\\Server_Generated_Files";
					DRSLog.war(message);
					vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
		  			path = VWCUtil.getHome()+"\\Server_Generated_Files";
		  			destPath = path;
		  		}
			  	// If the document name or route name is having '//' it creates sub folders with that name
			  	// issue fixed 1 Dec 2007. Valli
		  		destPath = path;
		  		path = path+ "\\"+fileName;
		  	}
		  	destPathStr = destPath;		  	
		  		Document doc = new Document(docId);
		  		String docPagesPath = destPath + "\\DocumentPages";//Later we have to decide how to give the path...
		  		
		  		docPublishPath = destPath+"\\DocumentPages\\"+docId;
		  		docPublishedPath = docPublishPath;
		  		
		  		int docPageCount = vwClient.getPageCount(sessionId, doc);
		  		DRSLog.dbg("docPageCount :::"+docPageCount);
		  		if(docPageCount>0){
		  			//DRSLog.add("postActionId ::"+postActionId);
		  			vwClient.publish(sessionId, doc, docPagesPath);
		  			try{
		  				
		  				String allzipLocation= dssPath+"\\"+String.valueOf(docId);
		  				//DRSLog.dbg("allzipLocation ::"+allzipLocation);
		  				File allzipFile=new File(allzipLocation+"\\all.zip");
		  				//DRSLog.dbg("allzipFile ::"+allzipFile);
		  				
		  				destTempPathStr = VWCUtil.getHome()+"\\"+roomName;
		  				String tempStrVal=destTempPathStr+"\\"+docId;
		  				//DRSLog.dbg("tempStrVal ::"+tempStrVal);
		  				saveInFile = saveArrayInHtmlFileNew(docName, routeName, data, DRSConstants.ReportColumnNamesNew,
				        		DRSConstants.ColumnWidthsNew, path, DRSConstants.LBL_DRS_Rep_Title,DRSConstants.LBL_DRS_Rep_Subject,offNoteSubVal,DRSConstants.LBL_DRS_Rep_OffVertical,offNoteVerticalVal,DRSConstants.LBL_DRS_Rep_OffDocumentLbl, docId, routeId, destPath, htmlContents);
		  				File destFile=new File(tempStrVal);
		  				if(!destFile.exists()) {
		  					destFile.mkdirs();
		  				}
		  				VWCUtil.CopyFile(allzipFile, new File(destFile.getPath()+"\\"+"all.zip"));
		  				int filesCount=getZipFileSize(destFile.getPath()+"\\"+"all.zip");
		  				DRSLog.dbg("filesCount ::"+filesCount);
		  				String htmlPath=destPath+"\\"+ fileName;
		  				File htmlFile=new File(htmlPath);
		  				String newHtmlPath = destFile+"\\"+fileName;
		  				File newhtmlFile=new File(newHtmlPath);
		  				if(htmlFile.exists()){
		  					VWCUtil.CopyFile(htmlFile,newhtmlFile);		  					
		  				}
		  				if(newhtmlFile.exists()){
		  					DRSLog.dbg("summary file generated....true");
		  				}
		  				summaryFilePath = newhtmlFile.getAbsolutePath();
		  				//Version revision summary
		  				//DRSLog.dbg("versionSummary ::"+versionSummary);
		  				if (versionSummary != null) {
		  					boolean ret = generateVersionHTMLContent(versionSummary, actualFileName, path, routeName);
		  					DRSLog.dbg("Is version revision added...."+ret);
		  					versionFileName = docId+"_"+routeName+"_"+"VersionRevisionSummary.html";
		  					versionFilePath = destPath+"\\"+versionFileName;
		  					String newVersionFilePath = destFile+"\\"+versionFileName;		  				
		  					File versionHTMLFile = new File(versionFilePath);
		  					File newVersionHTMLFile = new File(newVersionFilePath);
		  					
			  				if(versionHTMLFile.exists()){
			  					VWCUtil.CopyFile(versionHTMLFile, newVersionHTMLFile);			  					
			  					DRSLog.dbg("version revision summary report has created....");
			  					summaryFilePath = summaryFilePath +";"+newVersionHTMLFile.getAbsolutePath();
			  				}
			  			}	
		  				String newPdfPath=destFile+"\\"+"DocumentPages"+"\\"+docId+"_"+doctypeName+"-"+selDocName+".pdf";
		  				
		  				if(!(new File(destFile+"\\"+"DocumentPages").exists())){
		  					new File(destFile+"\\"+"DocumentPages").mkdirs();
		  				}		  				
		  				try {
		  					int hcPdf = 0, appendReport = 0, xresolution = 0, yresolution = 0, orientation = 0;
		  					String pdfProfile = DRSPreferences.getPdfProfile();
		  					ArrayList<String> possibleValues = VWCUtil.getPdfProfileValues();
		  					try {
			  					if (!possibleValues.contains(pdfProfile)) {
			  						pdfProfile = "photo600";
			  					}
		  					} catch (Exception e) {
		  						pdfProfile = "photo600";
		  					}
		  					try {
			  					hcPdf = Integer.parseInt(DRSPreferences.getHCPdf());
		  					}catch (Exception nfe1) {
		  						DRSLog.dbg("Number Format Exception while reading hcPdf registry "+nfe1.getMessage());
		  					}
		  					try {
			  					appendReport = Integer.parseInt(DRSPreferences.getAppendReport());
		  					} catch (Exception nfe2) {
		  						DRSLog.dbg("Number Format Exception while reading appendreport registry "+nfe2.getMessage());
		  					}
		  					try {
		  						orientation = Integer.parseInt(DRSPreferences.getOrientation());
		  					} catch (Exception e) {}
		  					/*try {
			  					xresolution = Integer.parseInt(DRSPreferences.getXResolution());
		  					} catch (Exception nfe3) {
		  						xresolution = 150;
		  						DRSLog.dbg("Number Format Exception while reading x resolution registry "+nfe3.getMessage());
		  					}
		  					try {
			  					yresolution = Integer.parseInt(DRSPreferences.getYResolution());
		  					} catch (Exception nfe4) {
		  						yresolution = 150;
		  						DRSLog.dbg("Number Format Exception while reading y resolution registry "+nfe4.getMessage());
		  					}*/
		  					DRSLog.dbg("hcPdf :"+hcPdf);
		  					DRSLog.dbg("pdfProfile :"+pdfProfile);
		  					DRSLog.dbg("appendReport :"+appendReport);
		  					DRSLog.dbg("xresolution :"+xresolution);
		  					DRSLog.dbg("yresolution :"+yresolution);
		  					DRSLog.dbg("Before calling cvwebConverttoPDFandAddWatermarkToPDF ");
						    int retval = vwClient.cvwebConverttoPDFandAddWatermarkToPDF(sessionId,
								String.valueOf(doc.getId()), docName, newPdfPath, summaryFilePath,
								destFile.getPath() + "\\" + "all.zip", hcPdf, pdfProfile, appendReport, xresolution,
								yresolution, orientation);
			  				DRSLog.dbg("retval after convert to pdf::::"+retval);
			  				File pdfFile=new File(newPdfPath);
			  				if(pdfFile.exists()){
			  					File directory = new File(pdfFile.getParent());
			  					// Get all files in directory
			  					File[] files = directory.listFiles();
			  					for (File file : files)
			  					{	// Delete each file
			  						if (!(file.getName().equals(pdfFile.getName()))){
			  							file.delete();
			  						}
			  						
			  					} 
			  					vwClient.addCVWFReportPdf(sessionId,pdfFile,doc);
			  					File allzip = new File("/attachments/" + sessionId +"/"+ docId +"/"+"all.zip");
			  					if(allzip.exists()){
			  						DRSLog.dbg("allzip exist in path ");
			  					int updatedfilesCount=getZipFileSize(allzip.getAbsolutePath());
			  					DRSLog.dbg("filesCount :"+filesCount);
			  					DRSLog.dbg("updatedfilesCount :::"+updatedfilesCount);
			  					if(filesCount+1==updatedfilesCount){
			  						DRSLog.add("pdf file created ...");
			  						saveInFile= true;
			  					}
			  					}
			  					
			  					if(destFile.exists()){
			  						VWUtil.deleteDirectory(destFile.getPath());
			  					}
			  					
			  				}
		  				} catch (Exception dtc) {
		  					DRSLog.dbg("Exception in ConvertToPdf method in DTC :::"+dtc.getMessage());
		  				}
		  				
		  			}catch(Exception e){
		  				DRSLog.dbg("Exception While Converting to pdf....."+e.getMessage());
		  			}
		  		}		  		
		  } catch(Exception e){
		  	DRSLog.add("Error in report  generation new"+e.getMessage());
		  	return saveInFile;
		  }
		  return saveInFile;
		 //return true;
	 } 	 
	 /*------------------End of SIDBI merges----------------------------*/
	 private boolean generateDRSReport(Vector retDocs, String docName, String path, Vector htmlContents, int compress,int convertopdf,String dssPath,Vector<VWEmailOptionsInfo> vwEmailOptionsInfoList)
	 {
		 int routeId = 0;
		 boolean saveInFile = false;
		 if(path!=null&&path.trim().equals("-")){
			 return false;
		 }
		 try{
		  	int row = retDocs.size();
		  	int column = DRSConstants.ReportColumnNames.length;//6//9
		  	String[][] data = new String[row][column+1];//7//9
		  	///String path = null;
		  	StringTokenizer stDocInfos;
		  	
		  	int docId = 0;
		  	
		  	String sNo = "";
		  	String userName = "";
		  	String taskName = "";
		  	String comments = "";
		  	String status = "";
		  	String receivedOn = "";
		  	String processOn = "";
		  	String routeName ="";
		  	String signature = "";
		  	String userStatus = "";
		  	
		  	
		  	String drsSendMail = "";
		  	String docPublishPath = "";
		  	for(int i = 0; i<row; i++){
		  		stDocInfos = new StringTokenizer(retDocs.get(i).toString(), "\t");
		  		// Adding serial number as a first column
		  		//data[i][0] = stDocInfos.nextToken();// Serial number
		  		sNo = stDocInfos.nextToken();// Serial number
		  		docId = Util.to_Number(stDocInfos.nextToken());
		  		stDocInfos.nextToken();//document name
		  		routeId = Util.to_Number(stDocInfos.nextToken());
		  		routeName = stDocInfos.nextToken();
		  		stDocInfos.nextToken();
		  		
		  		
		  		userName = stDocInfos.nextToken();
		  		receivedOn = stDocInfos.nextToken();
		  		status = stDocInfos.nextToken();
		  		processOn = stDocInfos.nextToken();
		  		comments = stDocInfos.nextToken();
		  		stDocInfos.nextToken();//Task sequence
		  		taskName = stDocInfos.nextToken();
				stDocInfos.nextToken();//Task Desc
				//Not going to send mail for the task user on final action completion
				drsSendMail = stDocInfos.nextToken();//Send mail enabled or disabled
				//data[i][column] = drsSendMail;
				stDocInfos.nextToken();//ApprovedBy
				stDocInfos.nextToken();//ApprovedByName
				stDocInfos.nextToken();//AuthorizedBy
				stDocInfos.nextToken();//AuthorizedByName
				signature = stDocInfos.nextToken();
				userStatus = stDocInfos.nextToken();
				
				
				int j = 0;
				data[i][j++] = sNo;
				data[i][j++] = userName;
				data[i][j++] = taskName;
				data[i][j++] = comments;
				data[i][j++] = status;
				data[i][j++] = receivedOn;
				data[i][j++] = processOn;
				data[i][j++] = routeName;
				String tempSign = "-";
				
				if(status.contains("Rejected"))
					signature = "-";
				
				tempSign = (signature.trim().equals("1")?"Authorized":(signature.trim().equals("2")?"Certified":"-"));
				
				data[i][j++] = tempSign;
				data[i][j++] = userStatus;
		  	}
		  	docName = VWCUtil.fixFileName(docName);
		  	routeName = VWCUtil.fixFileName(routeName);
		  	docNameStr = docName;
		  	routeNameStr = routeName;
		  	
	  		//String fileName = docName+"_"+routeName+"_"+Util.getNow(3)+".html";
		  	//Modified the route summary name for the Route enhancement Email options
	  		String fileName = docId+"_"+routeName+"_"+ Constants.WORKFLOW_MODULE_NAME +"Summary.html";
	  		// This is to get the original selected path without the fileName 
	  		String destPath = "";
		  	if(path!=null && !path.equals("") && !path.equals("-")){
		  		//		  	\\syntaxwkt-117\Data
		  		File filePath = new File(path);
		  		if(!filePath.exists() || !filePath.isDirectory() || !filePath.canWrite()){
		  			//DRSLog.war(" filePath.exists():"+filePath.exists()+" filePath.isDirectory():"+filePath.isDirectory()+" filePath.canWrite():"+filePath.canWrite());
		  			message = resourceManager.getString("RouteDoc.PathNotExist1")+" "+path+" "+resourceManager.getString("RouteDoc.PathNotExist2")+" "+resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RouteDoc.PathNotExist3")+" "+VWCUtil.getHome()+"\\Server_Generated_Files";
					DRSLog.war(message);
					vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
		  			path = VWCUtil.getHome()+"\\Server_Generated_Files";
		  			destPath = path;
		  		}
			  	// If the document name or route name is having '//' it creates sub folders with that name
			  	// issue fixed 1 Dec 2007. Valli
		  		destPath = path;
		  		path = path+ "\\"+fileName;
		  	}
		  //commented for,Bug 391 - Administration : DO NOT generate a status report at end of workflow if the Location is left blank.
		  	/*else{
		  		path = VWCUtil.getHome()+"\\Server_Generated_Files\\"+fileName;
		  		destPath = path;
		  		message = " "+resourceManager.getString("RouteDoc.ReportLocEmpty")+" "+path.toString();
				DRSLog.war(message);
				vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
		  	}*/
		  	//Passed docTypeName as parameter, so that document type name can be received and displayed in saveArrayInHtmlFile function...
		  	//DRSLog.add("path"+path);
		  	int docPages = 0 ;
		  	/*if(emailOptions!=null && emailOptions.size()>0){
		  		docPages = Integer.parseInt(emailOptions.get(2).toString());//Document Pages flag
		  	}*/
		  	/** CV10.2 - Added by vanitha****/
		  	/** CompressPages and convertToPdf part will execute only if the documentPages option is enabled in emailOptions list**/
		  	/** This is handled in UI also. This server side check is for existing records............**/
		  	boolean isCompressAndPdfCvrtRequired = checkIsDocumentPagesEnabled(vwEmailOptionsInfoList);
		  	/***************************************/
		  	DRSLog.dbg("isCompressAndPdfCvrtRequired......."+isCompressAndPdfCvrtRequired);
		  	if (isCompressAndPdfCvrtRequired) {	
		  		destPathStr = destPath;
		  		Document doc = new Document(docId);
		  		String docPagesPath = destPath + "\\DocumentPages";//Later we have to decide how to give the path...
		  		
		  		docPublishPath = destPath+"\\DocumentPages\\"+docId;
		  		docPublishedPath = docPublishPath;
		  		
		  		int docPageCount = vwClient.getPageCount(sessionId, doc);
		  		//DRSLog.add("docPageCount :::"+docPageCount);
		 		//DRSLog.add("convertopdf :::"+convertopdf);
		  		if(compress==0&&convertopdf==0){
		  			vwClient.publish(sessionId, doc, docPagesPath);
		  		}
		  		DRSLog.dbg("docPageCount:"+docPageCount+" compress:"+compress+" convertopdf:"+convertopdf);
		  		if((docPageCount>0)&&(compress==1&&convertopdf==0)){
		  			DRSLog.dbg("inside compress : 1 and convertopdf : 0");
		  			int ret = vwClient.publish(sessionId, doc, docPagesPath);
		  			//DRSLog.add("Value after publish the document is : "+ret);
		  			String zipFilePath = destPath+"\\"+docId+"_"+routeName+"_zip.zip";
		  			//DRSLog.add("zipFilePath....."+zipFilePath);
		  			try{
		  				if(ret==0){
		  					File zipArchive = new File(zipFilePath);//D:/zipArchive.zip
		  					File baseDir = new File(docPublishPath+"\\");//"D:/"//destPath
		  					//DRSLog.add("zipFilePath....."+zipFilePath);
		  					File publishFile = new File(docPublishPath);
		  					if(publishFile.exists()){
		  						File pages[] = publishFile.listFiles();
		  						int pageCount = pages.length;
		  						File[] fileExcludePages = new File[(pageCount-1)];
		  						int j=0;
		  						for(int i=0;i<pages.length; i++){
		  							if(!pages[i].getName().equalsIgnoreCase("Pages.xml")){
		  								fileExcludePages[j] = pages[i];
		  								j++;
		  							}
		  						}
		  						if(pageCount>0){
		  							if(compress==1) {
		  								ZipUtil.compress(zipArchive, baseDir, fileExcludePages);
		  							}
		  						}
		  					}
		  				}
		  			}catch(Exception ex){
		  				DRSLog.dbg("Exception while compress the files : "+ex.getMessage());
		  			}
		  			//DRSLog.add("end of the if part....");
		  		}else if(convertopdf==1) {
		  			DRSLog.dbg("inside convertopdf : 1");
		  			Vector result=new Vector();
		  			try{
		  				String allzipLocation= dssPath+"\\"+String.valueOf(docId);
		  				File allzipFile=new File(allzipLocation+"\\all.zip");
		  				String tempStrVal=destPath+"\\"+String.valueOf(docId);
		  				File destFile=new File(tempStrVal);
		  				if(!destFile.exists()) {
		  					destFile.mkdirs();
		  				}
		  				VWCUtil.CopyFile(allzipFile, new File(destFile.getPath()+"\\"+"all.zip"));
		  				String newPdfPath=destPath+"\\"+docId+"_"+routeName+".pdf";
		  				/***CV10.2 - HCpdf and QFactor registry parameters has added for pdf convertion***/
		  				int hcPdf = 0, qFactor = 2, orientation = 0;
	  					String pdfProfile = DRSPreferences.getPdfProfile();
	  					ArrayList<String> possibleValues = VWCUtil.getPdfProfileValues();
	  					try {
		  					if (!possibleValues.contains(pdfProfile)) {
		  						pdfProfile = "photo600";
		  					}
	  					} catch (Exception e) {
	  						pdfProfile = "photo600";
	  					}
	  					try {
		  					hcPdf = Integer.parseInt(DRSPreferences.getHCPdf());
	  					}catch (Exception nfe1) {
	  						DRSLog.dbg("Number Format Exception while reading hcPdf registry "+nfe1.getMessage());
	  					}
	  					String qFactorRegistry = DRSPreferences.getQFactor();
	  					DRSLog.dbg("qFactorRegistry :"+qFactorRegistry);
	  					if (qFactorRegistry.toLowerCase().equalsIgnoreCase("best")) {
	  						qFactor = 2;
	  					} else if (qFactorRegistry.toLowerCase().equalsIgnoreCase("minimum")) {
	  						qFactor = 45;
	  					} else if (qFactorRegistry.toLowerCase().equalsIgnoreCase("average")) {
	  						qFactor = 90;
	  					}
	  					try {
	  						orientation = Integer.parseInt(DRSPreferences.getOrientation());
	  					} catch (Exception e) {	  						
	  					}
	  					
		  				DRSLog.dbg("Before calling convert to pdf pdfFilePath....."+newPdfPath);
		  				DRSLog.dbg("Before calling convert to pdf all.zip path....."+destFile+"\\"+"all.zip");
		  				int retval=vwClient.ConvertDocToPDFDRS(sessionId, newPdfPath, destFile+"\\"+"all.zip", hcPdf, pdfProfile, qFactor, orientation);
		  				DRSLog.add("After calling convert to pdf jni call.....");
						/**End of CV10.2 merges********/
		  				File pdfFile=new File(newPdfPath);
		  				File pdfDirectory=new File(destPath);
		  				File[] pdfFileArray={pdfFile};
		  				//DRSLog.add("pdfFile exist........"+pdfFile.exists());
		  				if(pdfFile.exists()){
			  				File zipArchive=new File(destPath+"\\"+docId+"_"+routeName+"_zip.zip");
			  				if(compress==1){
			  					ZipUtil.compress(zipArchive, pdfDirectory, pdfFileArray);
			  				}
		  				}
		  			}catch(Exception e){
		  				DRSLog.dbg("Exception While Converting to pdf....."+e.getMessage());
		  			}
		  			DRSLog.dbg("end of the else part....");
	  			}
	 		}
		  	DRSLog.dbg("Before calling saveArrayInHtmlFile....");
			saveInFile = saveArrayInHtmlFile(docName, routeName, data, DRSConstants.ReportColumnNames,
	        		DRSConstants.ColumnWidths, path, DRSConstants.LBL_DRS_NAME, docId, routeId, destPath, htmlContents, true, true);
			DRSLog.dbg("After calling saveArrayInHtmlFile....");
		 }
		 catch(Exception e){
			 DRSLog.dbg("Exception in generating drs mail  :::"+e.getMessage());
			 return saveInFile;
		 }
		 return saveInFile;
	 } 	
	 
	 /**
	  * CV2019 Merges from SIDBI
	  * @param docName
	  * @param routeName
	  * @param data
	  * @param caption
	  * @param colWidth
	  * @param path
	  * @param title
	  * @param subject
	  * @param subVal
	  * @param officeNoteVertical
	  * @param verticalVal
	  * @param offDocId
	  * @param docId
	  * @param routeId
	  * @param destPath
	  * @param htmlContents
	  * @return
	  */
	private boolean saveArrayInHtmlFileNew(String docName, String routeName, String[][] data, String[] caption,
			String[] colWidth, String path, String title, String subject, String subVal, String officeNoteVertical,
			String verticalVal, String offDocId, int docId, int routeId, String destPath, Vector htmlContents) {
		// Vector htmlContents=new Vector();
		htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
		htmlContents.add("<html>");
		htmlContents.add("<head>");
		htmlContents.add("<title>" + title + "</title>");
		htmlContents.add("<style>");
		htmlContents.add("TH {");
		htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699'; font-weight:bold");
		htmlContents.add("}");
		htmlContents.add("TD {");
		htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
		htmlContents.add("}");
		htmlContents.add("</style>");
		htmlContents.add("</head>");
		htmlContents.add("<body BGCOLOR='#F7F7E7'>");
		htmlContents.add("<p align='center'><span lang='en-us'><font size='2'></font></span></p>");
		htmlContents.add("<p align='center'><b><span lang='en-us'><font size='4'>" + title + "</font></span></b></p>");

		htmlContents.add("<table border=0 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
		htmlContents.add("<tr><td ><B>" + subject + "</B>");
		htmlContents.add("		" + subVal + "</td></tr>");
		htmlContents.add("<tr><td ><B>" + officeNoteVertical + "</B>");
		htmlContents.add("		" + verticalVal + "</td></tr>");
		htmlContents.add("<tr><td><B>" + offDocId + "</B>");
		htmlContents.add("		" + docId + "</td></tr>");
		htmlContents.add("</table>");

		htmlContents.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
		htmlContents.add("<tr>");
		for (int i = 0; i < caption.length; i++)
			htmlContents.add("<td width='" + colWidth[i]+"%' align='center' BGCOLOR='#cccc99'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>" + caption[i]+"</B></FONT></td>");
		htmlContents.add("</tr>");
		for (int i = 0; i < data.length; i++) {
			htmlContents.add("<tr>");
			for (int j = 0; j < caption.length; j++)
				htmlContents.add("<td width='" + colWidth[j] + "%'>" + data[i][j] + "</td>");
			htmlContents.add("</tr>");
		}
		htmlContents.add("</table>");
		// htmlContents.add("<p align='left'><span lang='en-us'><font size='2'>Records
		// found : "+data.length+"</font></span></p>");
		htmlContents.add("<p align='left'><span lang='en-us'><font size='2'>Date : " + (new Date()).toString()+ "</font></span></p>");
		htmlContents.add("</body>");
		htmlContents.add("</html>");
		String errDesc = "";

		boolean routeSummaryFlag = true;
		boolean docMetaDataFlag = true;

		boolean ret = false;
		String routeSummaryPath = destPath + "\\" + docId + "_" + routeName + "_" + Constants.WORKFLOW_MODULE_NAME+ "Summary.html";
		DRSLog.dbg("routeSummaryPath :" + path);
		if (routeSummaryFlag)
			ret = writeListToFile(htmlContents, path, errDesc);
		try {
			String docMetaDataPath = destPath + "\\" + docId + "_" + routeName + "_MetaData.html";
			DRSLog.dbg("docMetaDataPath :" + docMetaDataPath);
			if (docMetaDataFlag) {
				Vector indices = new Vector();
				Document document = new Document(docId);
				vwClient.getDocumentIndices(sessionId, document);
				indices = document.getDocType().getIndices();

				Vector documentInfo = new Vector();
				String docStr = String.valueOf(docId);
				Vector documentInfoList = vws.getDocumentMetaDataInfo(roomName, sessionId, docStr, documentInfo);

				if (indices != null && indices.size() > 0 && documentInfoList != null && documentInfoList.size() > 0) {
					Vector htmlMetaDataContent = new Vector();
					// Need to build only once for one docId
					htmlMetaDataContent = Util.getHtmlMetaDataContents(indices, documentInfoList);
					VWCUtil.writeListToFile(htmlMetaDataContent, docMetaDataPath, "");
				}
			}
		} catch (Exception ex) {
			DRSLog.dbg("Exception while createing the document metadata attachment : " + ex.getMessage());
		}
		return ret;
	}
	 private boolean saveArrayInHtmlFile(String docName, String routeName, String[][] data,String[] caption,
		        String[] colWidth,String path,String title, int docId, int routeId, String destPath, Vector htmlContents, boolean routeSummaryFlag, boolean docMetaDataFlag)
	 {
		   // Vector htmlContents=new Vector();
		 	htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
	        htmlContents.add("<html>");
	        htmlContents.add("<head>");
	        htmlContents.add("<title>"+ title +"</title>");
	        htmlContents.add("<style>");
	        htmlContents.add("TH {");
	        htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699'; font-weight:bold");
	        htmlContents.add("}");
	        htmlContents.add("TD {");
	        htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
	        htmlContents.add("}");
	        htmlContents.add("</style>");
	        htmlContents.add("</head>");	        
	        htmlContents.add("<body BGCOLOR='#F7F7E7'>");
	        htmlContents.add("<p align='center'><span lang='en-us'><font size='2'></font></span></p>");
	        htmlContents.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");

	        htmlContents.add("<table border=0 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
	        htmlContents.add("<tr><td ><B>Document Name :</B>");
	        htmlContents.add("		"+docName+"</td></tr>");
	        htmlContents.add("<tr><td ><B>"+ Constants.WORKFLOW_MODULE_NAME +" Name :</B>");
	        htmlContents.add("		"+routeName+"</td></tr>");
	        htmlContents.add("<tr><td><B>"+ Constants.WORKFLOW_MODULE_NAME +" Server User :</B>");
	        htmlContents.add("		"+DRSConstants.drsAdminUser+"</td></tr>");
	        htmlContents.add("</table>");

	        htmlContents.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
	        htmlContents.add("<tr>");
	        for(int i=0;i<caption.length;i++)
	            htmlContents.add("<td width='" + colWidth[i] + "%' align='center' BGCOLOR='#cccc99'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>" + caption[i] + "</B></FONT></td>");
	        htmlContents.add("</tr>");
	        for(int i=0;i<data.length;i++)
	        {
	            htmlContents.add("<tr>");
	            for(int j=0;j<caption.length;j++)
	                htmlContents.add("<td width='" + colWidth[j] + "%'>" +
	                data[i][j] + "</td>");
	            htmlContents.add("</tr>");
	        }
	        htmlContents.add("</table>");
	        //htmlContents.add("<p align='left'><span lang='en-us'><font size='2'>Records found : "+data.length+"</font></span></p>");
	        htmlContents.add("<p align='left'><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></p>");
	        htmlContents.add("</body>");
	        htmlContents.add("</html>");
	        String errDesc = "";
	       
	        boolean ret = false;
        	if(routeSummaryFlag)
				ret = writeListToFile(htmlContents, path, errDesc);
        	if(docMetaDataFlag){
        		try{		  		
		  			String docMetaDataPath = destPath+"\\"+docId+"_"+routeName+"_MetaData.html";
		  			Vector indices = new Vector();
		  			Document document = new Document(docId);
		  			vwClient.getDocumentIndices(sessionId, document);
		  			indices = document.getDocType().getIndices();
		  			
		  			Vector documentInfo = new Vector(); 
		  			String docStr = String.valueOf(docId);
		  			Vector documentInfoList = vws.getDocumentMetaDataInfo(roomName, sessionId, docStr, documentInfo);
		  			
		  			if(indices!=null && indices.size()>0 && documentInfoList!=null && documentInfoList.size()>0){
			  			Vector htmlMetaDataContent = new Vector();
			  			//Need to build only once for one docId
			  			htmlMetaDataContent = Util.getHtmlMetaDataContents(indices, documentInfoList);
			  			VWCUtil.writeListToFile(htmlMetaDataContent, docMetaDataPath, "");
		  			}		  		
	        	}catch(Exception ex){
	        		DRSLog.add("Exception while createing the document metadata attachment : "+ex.getMessage());
	        	}
        	}
        
        	return ret;
	 }
	 
	 
	   public  boolean writeListToFile(Vector data, String path, String error)
	   {
	        return writeListToFile(data, path, error, "UTF-8");
	   }
	   public  boolean writeListToFile(Vector data, String path, String error, String format)
	   {
	        int count=data.size();
	        error = "";
	        try
	        {
	            File filePath = new File(path);
	            filePath.getParentFile().mkdirs();
	            if(filePath.exists())
	            {
	                filePath.renameTo(new File(path+Util.getNow(1)));	//Renaming the routesummary file name.
	            }
	            FileOutputStream ufos = new FileOutputStream(path);
	            OutputStreamWriter osw = new OutputStreamWriter(ufos, format);
	            for(int i=0;i<count;i++) osw.write((String)data.get(i)+Constants.NewLineChar);
	            osw.close();
	            ufos.close();
	        }
	        catch(IOException e)
	        {
	            error=e.getLocalizedMessage();
	            return false;
	        }
	        return true;
	   }
	    
	    
	 //Get the email ids for sending report.
	 // Not in use. Need to remove later. Valli
	 public String getEMailIdsForGenRptAction(int routeId){
		 String emailIds = "";
		 try{
			 Vector vTaskInfo = new Vector();
			 vwClient.getTaskInfo(sessionId, routeId, -1, vTaskInfo);
			 if(vTaskInfo!=null&& vTaskInfo.size()>0){
				 RouteTaskInfo routeTaskInfo = (RouteTaskInfo)vTaskInfo.get(0);
				 if(routeTaskInfo.getTaskName().trim().equalsIgnoreCase("Enable"))
					 emailIds = routeTaskInfo.getTaskDescritpion();
				 else
					 emailIds = "";
			 }
		 }catch(Exception ex){
		 }
		 return emailIds;
	 }
	public int updateRouteHistoryForEscalatedDocuments() {

		Vector escalatedDocsList = new Vector();
		int returnId = -1;
		int escalationUserId = -1;
		int ret = 0;
		int selectedTaskSeq;
		
		String status = "";
		String comments = "";
		String notifyMail = "";
		
		try
		{
			//getEscalatedDocsList - this method will give the documents that are escalated.
			returnId = vwClient.getEscalatedDocsList(sessionId, escalatedDocsList);
			if(returnId<0){
				if(returnId == ViewWiseErrors.invalidSessionId){
					message = ResourceManager.getDefaultManager().getString("CVProduct.Name")+" "+ResourceManager.getDefaultManager().getString("RouteDoc.ServerDown");
					DRSLog.war(message);
					vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);	        			
				}
				return Error;
			}
			
			if(escalatedDocsList!=null && escalatedDocsList.size()>0){
				DRSLog.add("Escalation process started.");
				for(int index=0; index<escalatedDocsList.size(); index++){
					RouteMasterInfo routeMasterInfo = (RouteMasterInfo)escalatedDocsList.get(index);

					notifyMail = routeMasterInfo.getNotifyMail();
					escalationUserId = routeMasterInfo.getEUserId();
					selectedTaskSeq = routeMasterInfo.getOnRejectAction();

					//From script we are blocking escalationUserId for -1 <none>
					if(escalationUserId == Constants.Route_Esc_Status_AutoAccept){
						status = Constants.Route_Status_Approve;
						comments = "Auto Accepted by DWS Admin";						
						message = resourceManager.getString("RouteDoc.AutoApprove1")+" '"+routeMasterInfo.getDocName()+"' "+resourceManager.getString("RouteDoc.AutoApprove2")+" "+ resourceManager.getString("WORKFLOW_NAME") +" '"+routeMasterInfo.getRouteName()+"' " +
	    				resourceManager.getString("RouteDoc.AutoApprove3")+" '"+routeMasterInfo.getRouteUserGrpName()+"' "+resourceManager.getString("RouteDoc.AutoApprove4");
						
						returnId = vws.setUserEscalation(roomName, sessionId, routeMasterInfo);
						
						vwClient.moveDocOnApproveRejectForEscalatedDocs(sessionId,  routeMasterInfo, status, comments, selectedTaskSeq);
						
					}else if (escalationUserId == Constants.Route_Esc_Status_AutoReject){
						status = Constants.Route_Status_Reject;
						comments = "Auto Rejected by DWS Admin";
						message = resourceManager.getString("RouteDoc.AutoReject1")+" "+routeMasterInfo.getDocName()+" "+resourceManager.getString("RouteDoc.AutoReject2")+resourceManager.getString("WORKFLOW_NAME") +" '"+routeMasterInfo.getRouteName()+"' " +
						resourceManager.getString("RouteDoc.AutoReject3")+routeMasterInfo.getRouteUserGrpName()+" "+ resourceManager.getString("RouteDoc.AutoReject4");
						
						returnId = vws.setUserEscalation(roomName, sessionId, routeMasterInfo);
						
						vwClient.moveDocOnApproveRejectForEscalatedDocs(sessionId,  routeMasterInfo, status, comments, selectedTaskSeq);
						
					}else if (escalationUserId > 0){
						returnId = vws.setUserEscalation(roomName, sessionId, routeMasterInfo);						
						message = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RouteDoc.ActionTimeFrame1")+" '"+routeMasterInfo.getDocName()+"' "+resourceManager.getString("RouteDoc.ActionTimeFrame2")+" "+ resourceManager.getString("WORKFLOW_NAME") +" '"+routeMasterInfo.getRouteName()+"' " +
					     resourceManager.getString("RouteDoc.ActionTimeFrame3")+" '"+routeMasterInfo.getEUserName()+"' "+resourceManager.getString("RouteDoc.ActionTimeFrame4")+" '"+routeMasterInfo.getRouteUserGrpName()+"' "+resourceManager.getString("RouteDoc.ActionTimeFrame5");
					}
					
					/**
					 * This is to get the Escalation user's mail id.
					 */
					String esclationUserEmailId = "";
					Vector<String> emailId = new Vector<String>();
					
					try{
						emailId = vws.getUserMail(roomName, sessionId, routeMasterInfo.getEUserName(), emailId);
						if(emailId!=null && emailId.size()>0 && emailId.get(0).trim().length()>0){
							esclationUserEmailId = emailId.get(0).trim();
						}
						
					}catch(Exception ex){
						esclationUserEmailId = "";
					}
					if((notifyMail!=null && !notifyMail.equals("-") && notifyMail.contains("@")) || (esclationUserEmailId.length()>0 && !esclationUserEmailId.equals("-") && esclationUserEmailId.contains("@"))){//if notify mail exist
						//DRSLog.add("Escalation mail sending to : "+routeMasterInfo.getNotifyMail()+(esclationUserEmailId.length()==0?"":esclationUserEmailId));
						ret = vws.sendEscalationMail(roomName, sessionId, routeMasterInfo, esclationUserEmailId, message);
						
						if(ret==0){
							String emailsSentTo = notifyMail.equals("-")?"":notifyMail;
							if(emailsSentTo.length() > 0){
								if(!emailsSentTo.contains(esclationUserEmailId))
									emailsSentTo = emailsSentTo + ";" + esclationUserEmailId;
							}else
								emailsSentTo = esclationUserEmailId.equals("-")?"":esclationUserEmailId;							
							
							DRSLog.add("Escalation Mail sent to : "+emailsSentTo);
						}
					}
					if(ret<0 && (!notifyMail.equals("-") || (esclationUserEmailId.length()>0 && !esclationUserEmailId.equals("-")))){
						message = resourceManager.getString("RouteDoc.EScalationMail")+" "+(notifyMail.equals("-")?"":notifyMail+";")+(esclationUserEmailId.length()>0?esclationUserEmailId:"");
						vws.sendNotification(roomName, sessionId, VWSConstants.DRS, message);
					}
				}
			}else{
				DRSLog.add("No document(s) to escalate");
			}
		}catch(Exception ex){
			DRSLog.add("Exception in updateRouteHistoryForEscalatedDocuments : "+ex.getMessage());
		}
		return returnId;
	}
	  /**
     * Added to get the maintanance flag from procedure call
     * and display warining message in DRS server.
     * Review Update button in adminwise 
     * Enhancement:-Update WorkFlow 
     * Version :- CV83B3
     */
	public int getDWSMaintenanceFlag(){
		int retVal=vwClient.getDWSMaintenanceFlag(sessionId);
		return retVal;
	}
	
	/**
	 * CV2019 merges from SIDBI
	 * @param allzipPath
	 * @return
	 */
	public int getZipFileSize(String allzipPath){
		ZipFile zipFile=null;
		int filesCount=0;
		try{
			zipFile=new ZipFile(allzipPath);
			filesCount= zipFile.size();
		}catch(Exception e){
			DRSLog.add("Exception in getting ZipFileSize "+e.getMessage());
		}finally{
			try {
				zipFile.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return filesCount;

	}
	
	/**
	 * This method is for generating version revision html file
	 * @param versionSummary
	 * @param docName
	 * @param path
	 * @param destPath
	 * @param routeName
	 * @return
	 */
	private boolean generateVersionHTMLContent(Vector versionSummary, String docName, String path, String routeName) {
		DRSLog.dbg("inside generateVersionHTMLContent....");
		Vector htmlContents = new Vector();
		if (versionSummary != null && versionSummary.size() > 0) {
			String dbString = null;
			StringTokenizer st = null;
			String versionNumber = null;
			String creator = null;
			String creationDate = null;
			String comment = null;
			String title = "Version revision summary report for "+docName+" document";
			htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
	        htmlContents.add("<html>");
	        htmlContents.add("<head>");
	        htmlContents.add("<title>"+ title +"</title>");
	        htmlContents.add("<style>");
	        htmlContents.add("TH {");
	        htmlContents.add("align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;COLOR='#336699'; font-weight:bold");
	        htmlContents.add("}");
	        htmlContents.add("TD {");
	        htmlContents.add("align='left';BGCOLOR: #f7f7e7; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px");
	        htmlContents.add("}");
	        htmlContents.add("</style>");
	        htmlContents.add("</head>");	        
	        htmlContents.add("<body BGCOLOR='#F7F7E7'>");
	        htmlContents.add("<p align='center'><span lang='en-us'><font size='2'></font></span></p>");
	        htmlContents.add("<p align='center'><b><span lang='en-us'><font size='4'>"+title+"</font></span></b></p>");	        
	        htmlContents.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
	        htmlContents.add("<tr><th width='20%' align='center' BGCOLOR='#cccc99'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>Version Number</B></FONT></th>"
	        		+ "<th width='20%' align='center' BGCOLOR='#cccc99'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>Creator</B></FONT></th>"
	        		+ "<th width='20%' align='center' BGCOLOR='#cccc99'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>Creation Date</B></FONT></th>"
	        		+ "<th width='20%' align='center' BGCOLOR='#cccc99'><B><FONT FACE='ARIAL' COLOR='#336699' SIZE=1>Comments</B></FONT></th></tr>");
			for (int index =0; index < versionSummary.size(); index ++) {
				dbString = versionSummary.get(index).toString();
				st = new StringTokenizer(dbString, "\t");
				st.nextToken();
				versionNumber = st.nextToken();
				creator = st.nextToken();
		        creationDate = st.nextToken();
		        comment = st.nextToken();
		        htmlContents.add("<tr><td width='20%'>"+versionNumber+"</td>");
		        htmlContents.add("<td width='20%'>"+creator+"</td>");
		        htmlContents.add("<td width='20%'>"+creationDate+"</td>");
		        htmlContents.add("<td width='20%'>"+comment+"</td></tr>");
			}
			htmlContents.add("</table>");

	        htmlContents.add("<p align='left'><span lang='en-us'><font size='2'>Date : "+(new Date()).toString()+"</font></span></p>");
	        htmlContents.add("</body>");
	        htmlContents.add("</html>");

		}
		path = path.replace("WorkflowSummary", "VersionRevisionSummary");
		DRSLog.dbg("Version html path : "+path);
		boolean ret = writeListToFile(htmlContents, path, "");
		DRSLog.dbg("Version revision summary file created  : "+ret);
		return ret;
	}
	
	public boolean writeListToExistingFile(Vector data, String path)
    {
		String format = "UTF-8";
		boolean isFileAppended = false;
        int count=data.size();
        try
        {
            File filePath = new File(path);
            if(filePath.exists())
            {
                FileOutputStream ufos = new FileOutputStream(path, true);
                OutputStreamWriter osw = new OutputStreamWriter(ufos, format);
                for(int i=0;i<count;i++) osw.write((String)data.get(i)+Constants.NewLineChar);
                osw.close();
                ufos.close();
                isFileAppended = true;
            }
           
        }
        catch(IOException e)
        {
        	isFileAppended = false;
        }
        return isFileAppended;
    }
	
	/**
	 * This method is used to check whether the document pages option is enabled in email options list
	 * @param vwEmailOptionsInfoList
	 * @return
	*/
	public boolean checkIsDocumentPagesEnabled(Vector<VWEmailOptionsInfo> vwEmailOptionsInfoList) {
		boolean isDocPagesEnabled = false;
		VWEmailOptionsInfo vwEmailOptionsInfo = null;
		if (vwEmailOptionsInfoList != null && vwEmailOptionsInfoList.size() > 0) {			
			for(int i=0; i<vwEmailOptionsInfoList.size(); i++){
				vwEmailOptionsInfo = (VWEmailOptionsInfo)vwEmailOptionsInfoList.get(i);
				DRSLog.dbg("vwEmailOptionsInfo.getDocumentPagesFlag()....."+vwEmailOptionsInfo.getDocumentPagesFlag());
				if (vwEmailOptionsInfo.getDocumentPagesFlag() == 1) {
					isDocPagesEnabled = true;
					break;
				}
			}
		}
		return isDocPagesEnabled;
	}
	
	/**
	 * This method is used to segrigate vwr selected mailIds from EmailOptionsList
	 * @param vwEmailOptionsInfoList
	 * @return
	 */
	private Vector<VWEmailOptionsInfo> segrigateEmailOptionsInfoList(Vector<VWEmailOptionsInfo> vwEmailOptionsInfoList) {
		Vector<VWEmailOptionsInfo> vwrEmailOptionsList = null;
		VWEmailOptionsInfo vwEmailOptionsInfo = null;
		if (vwEmailOptionsInfoList != null && vwEmailOptionsInfoList.size() > 0) {			
			for (int index = vwEmailOptionsInfoList.size() - 1; index >= 0; index--) {
				vwEmailOptionsInfo = vwEmailOptionsInfoList.get(index);
				if (vwEmailOptionsInfo.getDocumentVwrFlag() == 1) {
					if (vwrEmailOptionsList == null) vwrEmailOptionsList = new Vector<VWEmailOptionsInfo>();
					vwrEmailOptionsList.add(vwEmailOptionsInfo);
					vwEmailOptionsInfoList.remove(index);
				}
			}
		}
		return vwrEmailOptionsList;
	}
}
