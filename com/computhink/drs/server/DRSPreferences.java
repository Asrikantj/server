package com.computhink.drs.server;

import java.rmi.RemoteException;
import java.util.prefs.Preferences;

import com.computhink.common.RoomProperty;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.vws.server.VWSPreferences;

public class DRSPreferences implements DRSConstants{
	
	private static Preferences DRSPref;
    
    static
    {
        DRSPref= Preferences.systemRoot().node(DRS_PREF_ROOT);
        
    }
    public static String getHostName()
    {
        return getNode("host").get("name", "");
    }
    public static void setHostName(String name)
    {
        getNode("host").put("name", name);
        flush();
    }
    private static Preferences getNode(String node)
    {
        return DRSPref.node(node);
    }
    public static void setComPort(int port)
    {
        getNode("host").putInt("comport", port);
        flush();
    }
    public static int getComPort()
    {
        return getNode("host").getInt("comport", DEFAULT_COM_PORT);
    }
     public static void setDataPort(int port)
    {
        getNode("host").putInt("dataport", port);
        flush();
    }
    public static int getDataPort()
    {
        return getNode("host").getInt("dataport", DEFAULT_DATA_PORT);
    }
    public static void setProxy(boolean b)
    {
        getNode("proxy").putBoolean("enabled", b);
        flush();
    }
    public static void setProxyHost(String server)
    {
        getNode("proxy").put("host", server);
        flush();
    }
    public static void setProxyPort(int port)
    {
        getNode("proxy").putInt("port", port);
        flush();
    }
    public static boolean getProxy()
    {
        return getNode("proxy").getBoolean("enabled", false);
    }
    public static String getProxyHost()
    {
        return getNode("proxy").get("host", "");
    }
    public static boolean getLogInfo()
    {
        return getNode("general").getBoolean("loginfo", true);
    }
    public static void setLogInfo(boolean b)
    {
        getNode("general").putBoolean("loginfo", b);
        flush();
    }
    
    public static boolean getDebugInfo()
    {
        return getNode("general").getBoolean("debuginfo", false);
    }
    public static void setDebugInfo(boolean b)
    {
        getNode("general").putBoolean("debuginfo", b);
        flush();
    }
    
    
    public static int getProxyPort()
    {
        return getNode("proxy").getInt("port", DEFAULT_PROXY_PORT);
    }
    public static int getFlushLines()
    {
        return getNode("general").getInt("loglines", DEFAULT_LOG_LINES);
    }
    public static void setVWS(ServerSchema vws)
    {
        getNode("vws").put("host", vws.address);
        getNode("vws").putInt("port", vws.comport);
        flush();
    }
    public static ServerSchema getVWS()
    {
        ServerSchema ss = new ServerSchema();
        ss.address = getNode("vws").get("host", "");
        ss.comport = getNode("vws").getInt("port", 0);
        ss.type = SERVER_VWS;
        return ss;
    }
    
    public static String getManager()
    {
        String man = getNode("general").get("manager", "admin");
        String pas = getNode("general").get("manpass", "manager");
        if (!pas.equals("manager")) pas = Util.decryptKey(pas);
        return man + "||" + pas;
    }
    public static boolean setManager(String man, String pas)
    {
        try
        {
            pas = Util.encryptKey(pas);
            getNode("general").put("manager", man);
            getNode("general").put("manpass", pas);
            flush();
            return true;
        }
        catch(Exception e){}
        return false;
    }
    
    static private void flush()
    {
        try
        {
            DRSPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }   
    
    public RoomProperty[] getRooms() throws RemoteException
    {
        return VWSPreferences.getRooms();
    }
    
    public static int getSchedulerStartInfo()
    {
        return getNode("general").getInt("schedularStart", DEFAULT_SCHEDULARSTART);
    }
    public static void setSchedulerStartInfo(int s)
    {
        getNode("general").putInt("schedularStart", s);
        flush();
    }
    
    public static String getDialyFrequency() 
	{
    	return getNode("general").get("DailyFrequency", DEFAULT_DAILYFREQUENCY); 
	}
	public static void setDialyFrequency(String dialyFrequency) 
	{
	 	getNode("general").put("DailyFrequency", dialyFrequency);
        flush();
	}
    public static void setLogFileCount(int count)
    {
        getNode("general").putInt("maxlogfilecount", count);
        flush();
    }    
    public static int getLogFileCount()
    {
        return getNode("general").getInt("maxlogfilecount",100);
    }
	public static String getSnoozeTime() 
	{
    	return getNode("general").get("SnoozeTime", DEFAULT_SNOOZETIME); 
	}
	public static void setSnoozeTime(String snoozeTime) 
	{
	 	getNode("general").put("SnoozeTime", snoozeTime);
        flush();
	}
	/**CV2019 merges from CV10.2 Line - Added for hcpdf pdf convertion ***/
	public static String getHCPdf(){
		return getNode("general").get("hcpdf", "0");
    }
    public static String getPdfProfile(){
        return getNode("general").get("hcpdfprofile", "photo600");
    }
    public static String getAppendReport(){
        return getNode("general").get("appendreport", "1");
    }
    public static String getXResolution(){
        return getNode("general").get("xresolution", "150");
    }
    public static String getYResolution(){
        return getNode("general").get("yresolution", "150");
    }
    public static String getQFactor(){
        return getNode("general").get("qfactor", "Best");
    }
    public static String getOrientation(){
        return getNode("general").get("orientation", "0");
    }
    public static String getBurnAnnotation(){
        return getNode("general").get("burnannotation", "1");
    }
    public static String getWhiteoutValue(){
        return getNode("general").get("whiteout", "1");
    }
    public static String getDepthValue(){
        return getNode("general").get("depth", "24");
    }
    public static String getColorValue(){
        return getNode("general").get("color", "1");
    }
    
    public static void setBurnAnnotation(String burnAnnVal){
    	getNode("general").put("burnannotation", burnAnnVal);
    	flush();
    }
    public static void setWhiteoutValue(String whiteOutVal){
    	getNode("general").put("whiteout", whiteOutVal);
    	flush();
    }
    public static void setDepthValue(String depthVal){
    	getNode("general").put("depth", depthVal);
    	flush();
    }
    public static void setColorValue(String colorVal){
    	getNode("general").put("color", colorVal);
    	flush();
    }
    
	/***End of CV10.2 Merges*******/
}
