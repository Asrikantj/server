package com.computhink.vws.directory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.CompositeName;
import javax.naming.Context;
import javax.naming.Name;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.event.NamingExceptionEvent;
import javax.naming.ldap.UnsolicitedNotificationEvent;
import javax.naming.ldap.UnsolicitedNotificationListener;

import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;
import com.computhink.vws.server.mail.VWMail;

public class LDAP extends Directory{

    private String loginServer;
    private String port;
    private String adminUser;
    private String adminPass;
    private String ldapDirName;
    private String securityAuthentication;
    private String OUs[];
    private String OUList;
    private static final String ldapCTX = "com.sun.jndi.ldap.LdapCtxFactory";
    private DirContext initialContext;
    private String domainComponent;
    private String schemeType;
    private String userMailAttribute;
    private String message;
    private String userLastName;
	public ResourceManager resourceManager=ResourceManager.getDefaultManager();
	private String ldapuserDN;	  
	 private Map ldapDomainIPMap;
	  LDAP(String ldapDirectory, String loginServer, String adminUser, String adminPass, 
			  String ldapSecurity, String OUs[], String OUList, String port, String schemeType, String userMailAttribute)
	  {
	        this.ldapDirName = ldapDirectory;
	        this.loginServer = loginServer;
	        this.adminUser = adminUser;
	        this.adminPass = adminPass;
	        this.securityAuthentication = ldapSecurity;
	        this.OUs = OUs;
	        this.OUList = OUList;
	        this.schemeType = schemeType;
	        ldapDomainIPMap = getLdapDirectories();
	        VWSLog.dbg("loginServer initial::::"+loginServer);
	        VWSLog.dbg("ldapDomainIPMap:::::"+ldapDomainIPMap);
	        if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS) && ldapDirName.indexOf(".") != -1 ){
		        /*this.domainComponent = "DC="+(ldapDirName.substring(0, ldapDirName.indexOf(".")))
		        					+", DC="+ldapDirName.substring(ldapDirName.indexOf(".") + 1);*/
	        	try{
		        	StringTokenizer st = new StringTokenizer(ldapDirName, ".");
		        	this.domainComponent = "";
		        	if(st.countTokens()>0){
						while(st.hasMoreTokens()){
							this.domainComponent = this.domainComponent+"DC = "+st.nextToken().toString()+", ";
							VWSLog.add("this.domainComponent  in while ::::"+this.domainComponent );
						}
						this.domainComponent = this.domainComponent.substring(0,this.domainComponent.length()-2);
		        	}else{
		        		VWSLog.dbg("Inside else of ldapDirName::::"+ldapDirName);
		        		this.domainComponent = ldapDirName;
		        	}
		        		
		        }catch(Exception ex){	        	
		        }
	        }else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL)) 
	        	this.domainComponent = "O="+(ldapDirName);
	        else
	        	this.domainComponent = "O="+(ldapDirName);
	        //VWSLog.add(("this.domainComponent ")+this.domainComponent);
	        this.port = port;
	        this.userMailAttribute = userMailAttribute;
	    }

	    public String getCurrentUser(boolean ensureLoggedIn)
	    {
	        return getCurrentUser(null,ensureLoggedIn);
	    }
	    public String getCurrentUser(Object context, boolean ensureLoggedIn)
	    {
	        return getCurrentUser(context);
	    }
	    public String getCurrentUser()
	    {
	        return getCurrentUser(null);
	    }
	    public String getCurrentUser(Object context)
	    {
	    	return null;
	    }
	    public Vector getUserMailIds(List users){
		try{
			//VWSLog.dbg("users in getUserMailIds:::"+users);
			//VWSLog.dbg("ldapDirName in getUserMailIds :"+ldapDirName);
		    Vector userVector = new Vector();
		    DirContext dircontext = getInitialContext();
		    if(dircontext == null)	return userVector;
		    Iterator iterator = users.iterator();
		    while (iterator.hasNext())
		    {
			String userName = ((String) iterator.next()).trim();
			String context = "";		                
			String userMailIds = "";
			if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
			    userMailIds = searchForUserMails(dircontext,context,"(&(objectClass=user)(cn="+userName+"))");
			else{ // For LDAP ADS CV10.1 Issue fix to pull other ou mailid's modified by B.Madhavan 26/6/2017
				//VWSLog.dbg("user name before getting user dn ......"+userName);
				String userDN=getUserBasicAttributes(userName+"@"+ldapDirName);
				//VWSLog.dbg("usertDN inside getUserMailIds....."+userDN);
				if(userDN.length()>0){
				    userMailIds = searchForUserMails(dircontext,userDN,"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(name=" + userName + ")(userPrincipalName=" + userName+"@"+ldapDirName + ")))");
	
				}else{				
			    userMailIds = searchForUserMails(dircontext,context,"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(name=" + userName + ")(userPrincipalName=" + userName+"@"+ldapDirName + ")))");
				}
			   }//End of fix.
			if(userMailIds.trim().equals("")) userMailIds = "-";
			userVector.add(userMailIds);
		    }
		    dircontext.close();		        
		    return userVector;
		}catch(Exception ex){
			message = resourceManager.getString("LDAPMSG.ERRORMailIds")+ex.getMessage();
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		   // VWSLog.err("Error in getting UserMailIds "+ex.getMessage());
		    return new Vector();
		}
	    }
	 //Commented Existing 10.1 UserIngroup method for LDAP Multi domain 1-feb-2019
	/*    public List getUsersInGroup(String groupName)
	    {
		try{
			VWSLog.dbg("Inside getUsersInGroup method::::");
		    DirContext initialContext = getInitialContext();
		    if (initialContext == null) return new LinkedList();
		    groupName = handlingSpecialCharacters(groupName);
		    String filter = "(&(CN=" + groupName + ")(ObjectClass=Group))";		        
		    List userGroupList=new LinkedList();
		    //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
		    VWSLog.dbg("Examining the search results for groupName "+groupName);
		    if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
			List contextUserGroup = new LinkedList();	
			VWSLog.dbg("domainComponent before calling search formembers..."+domainComponent);
			contextUserGroup=searchForMembers(initialContext, domainComponent, filter);
			for(int j=0;j<contextUserGroup.size();j++)
			    userGroupList.add(contextUserGroup.get(j));

		    }else{
			for(int i=0;i<OUs.length;i++)
			{
			    List contextUserGroup = new LinkedList();
			    String sContext = "";
			    if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
    		        	sContext = frameContextNameForNovell(OUs[i].trim());    		        	
    		        	contextUserGroup=searchForMembers(initialContext, sContext, filter);
    		        	for(int j=0;j<contextUserGroup.size();j++)
    		        	    userGroupList.add(contextUserGroup.get(j));
			}
		    }
		    initialContext.close();
		    return userGroupList;
		}catch(Exception ex){
			message = resourceManager.getString("LDAPMSG.GetUsersInGroup")+ex.getMessage();
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    VWSLog.err("Error in getting getUsersInGroup "+ex.getMessage());
		    return new LinkedList();
		}
	    }*/
	    /**
	     * CV10.1 Enhancement Method added to get Users in Group
	     */
	    public List getUsersInGroup(String groupName)
	    {
	    	VWSLog.dbg("Start of getUsersIngroup for group name: "+groupName);
		try{
			int flag=1;
			VWSLog.dbg("Inside getUsersInGroup method::::");
		    DirContext initialContext = getInitialContext();
		    if (initialContext == null) return new LinkedList();
		    groupName = handlingSpecialCharacters(groupName);
		    String sfilter="";
		    String filter = "(&(CN=" + groupName + ")(ObjectClass=group))";		        
		    List userGroupList=new LinkedList();
		    //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
		    VWSLog.dbg("Examining the search results for groupName "+groupName);
		    if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
				/**CV2019 code merges from CV10.2 line. Merged by Vanitha.S**/
				List contextUserGroup = new LinkedList();
				String sContext = "";
				VWSLog.dbg("domainComponent before calling search for members..."+domainComponent);
				/*for(int i=0;i<OUs.length+1;i++)
				{
					VWSLog.dbg("OUs.length: "+OUs.length + " OUs[i].trim(): " + OUs[i].trim());
					if (i==OUs.length-1)
						contextUserGroup=searchForMembers_New(initialContext, domainComponent, filter, flag);
					else {
						sContext = frameContextNameForADS(OUs[i].trim());
						sfilter = "(&(CN=" + groupName + ")(ObjectClass=group))";	
						VWSLog.dbg("sContext: " + sContext + " groupName: " + groupName);
						contextUserGroup=searchForMembers_New(initialContext, sContext, sfilter, flag);
					}
					//VWSLog.dbg("sContext before calling search for members..."+sContext);
					
					//contextUserGroup=searchForNames(initialContext, domainComponent, filter);
					//contextUserGroup=searchForNamesExcludeGroupName(initialContext, domainComponent, filter, groupName);
					VWSLog.dbg("Members count after searching: "+contextUserGroup.size());
					for(int j=0;j<contextUserGroup.size();j++) {
						VWSLog.dbg("contextUserGroup: " + contextUserGroup);
						userGroupList.add(contextUserGroup.get(j));
					}
				}*/
				//for(int i=0;i<OUs.length+1;i++){
					sContext = frameContextNameForADS(OUs[0].trim());
					//sfilter = "(&(CN=" + groupName + ")(ObjectClass=group))";	
					contextUserGroup=searchForMembers_New(initialContext, sContext, filter, flag);
					VWSLog.dbg("Members count after searching: "+contextUserGroup.size());
					for(int j=0;j<contextUserGroup.size();j++) {
						if (!userGroupList.contains(contextUserGroup.get(j)))
							userGroupList.add(contextUserGroup.get(j));
					}
				//}
					
				VWSLog.dbg("Users in group after search  with sContext: "+userGroupList);
				contextUserGroup=searchForMembers_New(initialContext, domainComponent, filter, flag);
				for(int j=0;j<contextUserGroup.size();j++) {
					if (!userGroupList.contains(contextUserGroup.get(j)))
						userGroupList.add(contextUserGroup.get(j));
				}
				VWSLog.dbg("Users in group after search  with domaincomponent: "+userGroupList);

		    }else{
				for(int i=0;i<OUs.length;i++)
				{
				    List contextUserGroup = new LinkedList();
				    String sContext = "";
				    if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
	    		        	sContext = frameContextNameForNovell(OUs[i].trim());    		        	
	    		        	contextUserGroup=searchForMembers_New(initialContext, sContext, filter, flag);
	    		        	for(int j=0;j<contextUserGroup.size();j++)
	    		        	    userGroupList.add(contextUserGroup.get(j));
				}
				/**End of CV10.2 merges****/
		    }
		    initialContext.close();
		    VWSLog.dbg("End of getUsersIngroup for group name "+ groupName + " and group users " + userGroupList);
		    return userGroupList;
		}catch(Exception ex){
			message = resourceManager.getString("LDAPMSG.GetUsersInGroup")+ex.getMessage();
			//VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    VWSLog.err("Error in getting getUsersInGroup "+ex.getMessage());
		    return new LinkedList();
		}
	    }
	    
	/***
	 * CV2019 merges from CV10.2 line
	 */
	public List getUsersInGroup_New(String groupName) {
		VWSLog.dbg("Start of getUsersIngroup for group name: " + groupName);
		try {
			int flag = 1;
			VWSLog.dbg("Inside getUsersInGroup method::::");
			DirContext initialContext = getInitialContext();
			if (initialContext == null)
				return new LinkedList();
			groupName = handlingSpecialCharacters(groupName);
			String filter = "(&(CN=" + groupName + ")(ObjectClass=group))";
			List userGroupList = new LinkedList();
			// User tables are populated based on context value entered as in NDS scheme.
			// valli 16 July 2007
			VWSLog.dbg("Examining the search results for groupName " + groupName);
			if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)) {
				List contextUserGroup = new LinkedList();
				VWSLog.dbg("domainComponent before calling search formembers..." + domainComponent);
				contextUserGroup = searchForMembers_New(initialContext, domainComponent, filter, flag);
				// contextUserGroup=searchForNames(initialContext, domainComponent, filter);
				// contextUserGroup=searchForNamesExcludeGroupName(initialContext,
				// domainComponent, filter, groupName);

				for (int j = 0; j < contextUserGroup.size(); j++) {
					VWSLog.dbg("contextUserGroup: " + contextUserGroup);
					userGroupList.add(contextUserGroup.get(j));
				}
		    }else{
				for(int i=0;i<OUs.length;i++)
				{
				    List contextUserGroup = new LinkedList();
				    String sContext = "";
				    if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
	    		        	sContext = frameContextNameForNovell(OUs[i].trim());    		        	
	    		        	contextUserGroup=searchForMembers_New(initialContext, sContext, filter, flag);
	    		        	for(int j=0;j<contextUserGroup.size();j++)
	    		        	    userGroupList.add(contextUserGroup.get(j));
				}
			}
			initialContext.close();
			VWSLog.dbg("End of getUsersIngroup for group name " + groupName + " and group users " + userGroupList);
			return userGroupList;
		} catch (Exception ex) {
			message = resourceManager.getString("LDAPMSG.GetUsersInGroup") + ex.getMessage();
			// VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			VWSLog.err("Error in getting getUsersInGroup " + ex.getMessage());
			return new LinkedList();
		}
	}

	    private String getJNDICompositeName(String name){
	    	try{
		    	Name compositeName =  new CompositeName().add(name);
		    	String jndi_name = compositeName.toString();
		    	//VWSLog.dbg("gettingJNDICompositename jndiName::"+jndi_name);
		    	return jndi_name;
	    	}catch(Exception ex){
	    		//VWSLog.dbg("Exception in gettingJNDICompositename::"+ex.getMessage());
	    		return name;
	    	}
	    }
	    public static Object getKeyFromValue(Map hm, Object value) {
	        for (Object o : hm.keySet()) {
	          if (hm.get(o).equals(value)) {
	            return o;
	          }
	        }
	        return null;
	      }
	    private DirContext getInitialContext()
	    {
	    	//VWSLog.dbg(" ");
	    	//VWSLog.dbg("Start of getInitialContext from LDAP.java");
	        //if(initialContext == null)
	        {
	        	    Hashtable env = new Hashtable();
		            //Example principal for Novell LDAP "cn=admin, o=ssvwc");
		            env.put(Context.INITIAL_CONTEXT_FACTORY,ldapCTX);
		            VWSLog.dbg("loginservername before provider url 1 ::::"+loginServer);
		            String url = "ldap://" + loginServer + ":" + port;
		            VWSLog.dbg("PROVIDER_URL: "+url);
		            env.put(Context.PROVIDER_URL, url);
		            if(Integer.parseInt(port) == SSL_PORT)
		            	env.put(Context.SECURITY_PROTOCOL, SECURITY_PROTOCOL_SSL);
		            String principal = new String();
		            if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS) && (ldapDirName.indexOf(".")!=-1)){
		            	//VWSLog.dbg("ldapDomainIPMap 1 :::::"+ldapDomainIPMap);
		            	//VWSLog.dbg("loginServer 1 :::::"+loginServer);
		            	Object value=  getKeyFromValue(ldapDomainIPMap,loginServer);
		            	if(value!=null){
		            		String mapDir=value.toString();
		            		//VWSLog.dbg("mapDir::::::::"+mapDir);
		            		if(ldapDirName.equalsIgnoreCase(mapDir.toString())){
		            			principal = adminUser+"@"+ldapDirName;
		            		}else{
		            			principal = adminUser+"@"+mapDir;
		            		}
		            	}else{
		            		principal = adminUser+"@"+ldapDirName;
		            	}
		                //VWSLog.dbg("principal :::::::::::::"+principal);
		            }
		            else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
		        	// Comment the existing "cn=" + adminUser, User will framing the admin user with OU's  		        	
		        	principal = adminUser+", "+domainComponent;//principal = "cn="+adminUser+", "+domainComponent;
		        	
		            else 
		            	principal = adminUser+"@"+ldapDirName;

		            			            
		            env.put(Context.SECURITY_PRINCIPAL, principal);
		            VWSLog.dbg("LDAP Context parameters ------->:"+principal);
		            env.put(Context.SECURITY_CREDENTIALS, adminPass);
		            env.put(Context.SECURITY_AUTHENTICATION, securityAuthentication);
		            VWSLog.dbg("SECURITY_AUTHENTICATION :"+securityAuthentication);
		            env.put("com.sun.jndi.ldap.connect.timeout", "-1");
	            try
	            {
	                initialContext = new InitialDirContext(env);
	            }
	            catch(AuthenticationNotSupportedException authenticationnotsupportedexception)
	            {
	            	message = resourceManager.getString("LDAPMSG.Authentication")+" "+authenticationnotsupportedexception.getMessage();
	    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	                //VWSLog.err("Authentication specified is not Supported.  "+authenticationnotsupportedexception.getMessage());
	    			VWSLog.err("Authentication specified is not Supported.  "+authenticationnotsupportedexception);
	                
	            }
	            catch(NamingException namingexception)
	            {
	            	message =resourceManager.getString(resourceManager.getString("LDAPMSG.DirectoryContext")+" "+namingexception.getMessage());
	    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	                //VWSLog.err("Unable to get Initial LDAP Directory Context-> "+namingexception.getMessage());
	    			VWSLog.err("Unable to get Initial LDAP Directory Context-> "+namingexception);
	            }
	            catch(Exception exception)
	            {
	            	message =resourceManager.getString(resourceManager.getString("LDAPMSG.DirectoryContext")+" "+exception.getMessage());
	    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	                //VWSLog.err("Unable to get Initial LDAP Directory Context-> "+namingexception.getMessage());
	    			VWSLog.err("Unable to get Initial LDAP Directory Context-> "+exception);
	            }
	        }
	        //VWSLog.dbg("End of getInitialContext from LDAP.java");
	        //VWSLog.dbg(" ");
        
	        return initialContext;
	    }
	    private List searchForNames(DirContext dirContext,String context,String filter)
	    {
	    	VWSLog.dbg("Inside searchForNames method::::");
	    	LinkedList names = new LinkedList();
			if (dirContext == null)
			{
				System.out.println("dirContext was null-> ");
				return names;
			}
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String returningAttributes[] = new String[0];
			controls.setReturningAttributes(returningAttributes);
			String principalName = null;
			try
			{
				VWSLog.dbg("Inside SearchForName Using context -> "+context+" filter -> "+filter+" controls -> "+controls);
				VWSLog.dbg(" ");
				context = getJNDICompositeName(context);
				NamingEnumeration searchResults = dirContext.search(context, filter, controls);
				VWSLog.dbg("Start of examining the search result from directory context search");
				//VWSLog.dbg("Examining the search results");
				while (searchResults != null && searchResults.hasMoreElements())
				{
					SearchResult searchResult=(SearchResult) searchResults.nextElement();
					VWSLog.dbg("+++++++Examining the search results searchResult.getNameInNamespace()   ++++"+searchResult.getNameInNamespace());
					//showAttributes(searchResult);
					//getUserPrincipalName will not work for Novell LDAP
					 if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS))
					 {
						 /**CV2019 code merges from CV10.2 line. Merged by Vanitha.S**/
						 principalName = getUserPrincipalName(searchResult);
						 VWSLog.dbg("+++++++getUserPrincipalName(searchResult)++++"+principalName);
						 //if (groupName != searchResult)						 
						 names.add(principalName);
					 }
					 else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
						 names.add(extractUser(searchResult.getName()));
					 else
						 names.add(extractUser(searchResult.getName()));
				}
				searchResults.close();
				VWSLog.dbg("End of examining the search result from directory context search and End of SearchFornames from LDAP.java");
				VWSLog.dbg("searchForNames names:: " + names);
				return names;
			}
			catch (NamingException e)
			{
				System.out.println("JNDISearch failed ->"+e.getMessage());
				VWSLog.dbg("Exception in and End of SearchFornames from LDAP.java");
				return new LinkedList();
			}
	    }
	    
	    private List searchForNamesUPN(DirContext dirContext,String context,String filter)
	    {
	    	VWSLog.dbg("Inside searchForNames:::");
	    	VWSLog.dbg("+++++++dirContext+++++++"+dirContext);
	    	VWSLog.dbg("+++++++context+++++++"+context);
	    	VWSLog.dbg("+++++++filter+++++++"+filter);
	    	
	        LinkedList names = new LinkedList();
			if (dirContext == null)
			{
				System.out.println("dirContext was null-> ");
				return names;
			}
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String returningAttributes[] = new String[0];
			controls.setReturningAttributes(returningAttributes);
			try
			{
				context = getJNDICompositeName(context);
				VWSLog.dbg("Before directoryContext search in searchForNamesUPN:::"+context);
				NamingEnumeration userSearchResults = dirContext.search(context, filter, controls);		
				VWSLog.dbg("After directoryContext search in searchForNamesUPN");
				VWSLog.dbg("userSearchResults size"+ userSearchResults.toString());
				
				while(userSearchResults.hasMoreElements())
				{
				    SearchResult userSearchResult = (SearchResult)userSearchResults.nextElement();
				    VWSLog.dbg("userSearchResult:::"+userSearchResult.getName());
				    VWSLog.dbg("userSearchResult:::"+userSearchResult.getNameInNamespace());
				    Attributes attrList = userSearchResult.getAttributes();
				  //******************************************************************
					Attributes attrsNamespace = dirContext.getAttributes(userSearchResult.getNameInNamespace());
					
						String userPrincipalName = "";
						String name = "";
						String suffix="";
						String fullNameWithUPN = "";
				        try{
							userPrincipalName = attrsNamespace.get("userPrincipalName").toString();
							VWSLog.dbg("+++++++++++userPrincipalName:::::	"+userPrincipalName);
						}catch(Exception ex){
							userPrincipalName = attrsNamespace.get("sAMAccountName").toString();
							VWSLog.dbg("inside catch samaccountname::"+userPrincipalName);
						}
				        try{
				        	if(userPrincipalName != null && !userPrincipalName.equals("")){
				        		StringTokenizer st1=new StringTokenizer(userPrincipalName,":");
				        		st1.nextToken();
				        		name = st1.nextToken().trim();
				        		fullNameWithUPN = name;
				        		if(name.indexOf("@")>0) {
				        			suffix = name.substring((name.indexOf("@")+1), (name.length()));
				        			name = name.substring(0, (name.indexOf("@")));
				        			VWSLog.dbg("**************** Inside if name+++++++  "+name + " suffix::"+suffix);
				        		}
				        	}else{
				        		name= extractUser(userSearchResult.getName().toString());
				        		VWSLog.dbg("**************** Inside else name+++++++  "+name);
				        		suffix="";
				        	}
				        	VWSLog.dbg("fullNameWithUPN:::"+fullNameWithUPN);
				        	names.add(fullNameWithUPN);
				        	VWSLog.dbg("Names List value:::"+names);
				        }catch(Exception exp){
				        	VWSLog.dbg("Exception while spliting upn suffix::"+exp.getMessage());
				        }
				}
				return names;
			}
			catch (NamingException e)
			{
				VWSLog.dbg("Inside catch block of searchForNamesUPN NamingException ->"+e.getMessage());
				return new LinkedList();
			}
	    }
	    
	    //Searching for the logged in user to get his Distingushed Name. 
	    private Vector searchForUserDn(DirContext dirContext,String context,String filter)
	    {
	    	SearchControls controls = new SearchControls();
	    	controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    	String returningAttributes[] = new String[0];
	    	controls.setReturningAttributes(returningAttributes);
	    	Vector userNameInNamespace = new Vector();
	    	try
	    	{
	    		dirContext = getInitialContext();
	    		NamingEnumeration searchResults = dirContext.search(context, filter, controls);
	    		while(searchResults != null && searchResults.hasMoreElements())
	    		{
	    			SearchResult searchResult = (SearchResult)searchResults.nextElement();
	    			String userDisplayName = searchResult.getNameInNamespace();
	    			//VWSLog.dbg(" userDisplayName:"+userDisplayName+":");
	    			userNameInNamespace.add(userDisplayName);
	    		}
    			searchResults.close();
	    		dirContext.close();
	    	}catch (NamingException e)
	    	{
	    		userNameInNamespace = new Vector();
	    		//VWSLog.dbg("JNDISearch failed ->"+e.getMessage());
	    	}catch (Exception e){
	    	}
	    	return userNameInNamespace;
	    }
	    
	    private String searchForUserMails(DirContext dirContext,String context,String filter)
	    {
		String mailId = "";
		try
		{
		    SearchControls controls = new SearchControls();
		    controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		    String returningAttributes[] = null;
		    if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
			controls.setReturningObjFlag(true);
		    }
		    else{
			returningAttributes = new String[0];
			controls.setReturningAttributes(returningAttributes);
		    }
		    //CV10.1 Issue fix for pulling mailId from other ou. Modified by B.Madhavan 26/6/2016
		    boolean userMailNotFound = true;
		    if(context.length()>0){
		    	VWSLog.dbg("context  dn information....:::::"+context);
		    	if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
				String userDN=context;
				context="";
				String dnOUString="";
		    	String reverseOU="";
		    	if ( (!userDN.equalsIgnoreCase("")) && userDN.length()>0 ) 
		    	{
		    		VWSLog.dbg("ldapuserDN value inside authenticate user ::::"+userDN);
		    		String totalUserDN[]=userDN.split(",");
		    		for(int k=0; k < totalUserDN.length; k++){
		    			System.out.println(totalUserDN[k]);
		    			if(totalUserDN[k].contains("OU=")){
		    				dnOUString=dnOUString+";"+totalUserDN[k].substring(totalUserDN[k].indexOf("=")+1,totalUserDN[k].length());
		    			}
		    		}

		    		String newUserDN[]=dnOUString.split(";");
		    		for(int j=newUserDN.length-1;j>0;j--){
		    			reverseOU=reverseOU+";"+newUserDN[j];
		    		}
		    		reverseOU=reverseOU.substring(1,reverseOU.length());
		    		String reverseOU1[]=reverseOU.toString().split(";");
		    	
		    		for(int j=0;j<reverseOU1.length&&userMailNotFound;j++){
		    			VWSLog.dbg("reverseOU Inside user mail id is ::::"+reverseOU1[j]);
						context = frameContextNameForADS(reverseOU1[j]);
						NamingEnumeration searchResults = dirContext.search(context, filter, controls);
						if(searchResults != null && searchResults.hasMoreElements())
						{
							SearchResult searchResult = (SearchResult)searchResults.nextElement();
							Attributes attrs = searchResult.getAttributes();
							NamingEnumeration allAttributes = attrs.getAll();
							while (allAttributes.hasMoreElements()) {
								Attribute attr = (Attribute)allAttributes.nextElement();	
								if (attr.getID().equalsIgnoreCase("mail")){
									mailId = attr.get(0).toString() ;
									VWSLog.dbg("mailId from reverseOU is ....."+mailId);
									/**CV2019 code merges from CV10.2 line. Merged by Vanitha.S**/
									if(mailId.length()>0){
										userMailNotFound=false;
									}
									break;
								}
							}		    			    
							searchResults.close();
						}
					
		    		}
		    		
		    	}
		    	}
				
		    }else{	

		    	for(int i = 0; i<OUs.length && userMailNotFound; i++){
		    		if (schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
		    		{

		    			context = frameContextNameForNovell(OUs[i]);
		    			NamingEnumeration searchResults = dirContext.search(context, filter, controls);
		    			if(searchResults != null && searchResults.hasMoreElements())
		    			{
		    				SearchResult searchResult = (SearchResult)searchResults.nextElement();
		    				//VWSLog.dbg("Show attributes for -> "+searchResult.getNameInNamespace());
		    				String userDn = searchResult.getNameInNamespace();
		    				Attributes attrs = dirContext.getAttributes(userDn);
		    				NamingEnumeration ae = attrs.getAll();
		    				while(ae.hasMoreElements()) {
		    					Attribute attr = (Attribute)ae.nextElement();		    				
		    					if(attr.getID().trim().equalsIgnoreCase(this.userMailAttribute.trim())){
		    						NamingEnumeration e = attr.getAll();
		    						while (e.hasMore()){
		    							mailId = mailId +e.next()+";";
		    							//VWSLog.dbg(" mail id of "+searchResult.getName().toString()+" " + mailId);
		    						}
		    						mailId = mailId.substring(0,(mailId.length()-1));
		    						if (mailId.length() > 0)
		    							userMailNotFound = false;
		    						break;
		    					}
		    				}
		    				searchResults.close();
		    			}
		    		}
		    		else{

		    			context = frameContextNameForADS(OUs[i]);
		    			NamingEnumeration searchResults = dirContext.search(context, filter, controls);
		    			if(searchResults != null && searchResults.hasMoreElements())
		    			{
		    				SearchResult searchResult = (SearchResult)searchResults.nextElement();
		    				Attributes attrs = searchResult.getAttributes();
		    				NamingEnumeration allAttributes = attrs.getAll();
		    				while (allAttributes.hasMoreElements()) {
		    					Attribute attr = (Attribute)allAttributes.nextElement();	
		    					if (attr.getID().equalsIgnoreCase("mail")){
		    						mailId = attr.get(0).toString() ;
		    						break;
		    					}
		    				}		    			    
		    				searchResults.close();
		    			}
		    		}
		    	}//End of issue fix.
		    }
		}
		catch (NamingException e)
		{	    
		    mailId = new String();
		    VWSLog.dbg("JNDISearch failed ->"+e.getMessage());
		}
		catch (Exception ex)
		{	    
		    mailId = new String();
		    VWSLog.dbg("Exception: JNDISearch failed ->"+ex.getMessage());
		}	
		VWSLog.dbg("mailId before return...."+mailId);
		return mailId;
	    }
	    
	private String extractUser(String context) {
	try {
		VWSLog.dbg("Inside extractUser method parameter Context::::"+context);
	    String name = context;
	    if (name.toUpperCase().contains(",OU"))
		name = context.substring(0, context.toUpperCase().indexOf(",OU"));
	    else if(name.toUpperCase().contains(",O"))
		name = context.substring(0, context.toUpperCase().indexOf(",O"));
	    else if(!name.toUpperCase().contains(",O") && !name.toUpperCase().contains(",OU")){
		if(context.contains(","))
		    name = context.substring(3, context.indexOf(","));
		else
		    name = context.substring(3, context.length());
	    }
	    VWSLog.dbg("Inside extractUser method fetching cn::::"+name);

	    // StringTokenizer stringtokenizer = new
                // StringTokenizer(context, ",");
	    // if(stringtokenizer.hasMoreTokens())
	    {
		String s2 = name;
	            StringTokenizer stringtokenizer1 = new StringTokenizer(s2, "=");
	            stringtokenizer1.nextToken();
	            name = stringtokenizer1.nextToken();
	        }
	        return name;
	} catch (Exception ex) {
	    //VWSLog.dbg("Exception and context is : "+context);
	    if(context.contains(","))
		return context.substring(3, context.indexOf(","));
	    else
		return context.substring(3, context.length());
	}
	    }
	    
	    private List searchForMembers(DirContext dirContext, String sContext, String filter)
	    {
	    	VWSLog.dbg("Inside searchForMembers method::::");
		LinkedList names = new LinkedList();
		if (dirContext  == null)
		{
		    VWSLog.dbg("dirContext was null");
		    return names;
		}
		SearchControls controls = new SearchControls();
		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String returningAttributes[] = new String[1];
		returningAttributes[0] = "member";
		controls.setReturningAttributes(returningAttributes);
		//controls.setTimeLimit(3000);
		try
		{
		    //sContext = "OU="+sContext +","+domainComponent;
		    sContext = getJNDICompositeName(sContext);
		   // VWSLog.dbg(" context -> " + sContext +" filter -> " + filter);
		    if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
				return searchForMembersInADS(dirContext, sContext, filter);
		    }
		    NamingEnumeration searchResults = dirContext.search(sContext,filter,controls);
		    //VWSLog.dbg("Examining the search results");//+database.getName());
		    while (searchResults != null && searchResults.hasMoreElements())
		    {
			SearchResult searchResult=(SearchResult) searchResults.nextElement();
			//showAttributes(searchResult);
			Attributes attributes = searchResult.getAttributes();
			//Attribute attribute = attributes.get("member");
			//if (attribute == null) return names;
			NamingEnumeration ae = attributes.getAll();
			while (ae.hasMore())
			{

			    Attribute attr = (Attribute)ae.next();
			    //VWSLog.dbg(" attribute: " + attr.getID());
			    NamingEnumeration e = attr.getAll();
			    while (e.hasMore()){
				Object value = e.next();				
				//VWSLog.dbg("Members: " + value.toString());//+database.getName());
				names.add(extractUser(value.toString()));
			    }
			    e.close();
			}
		    }		
		    searchResults.close();
		    return names;
		}
		catch (NamingException e)
		{
			message = resourceManager.getString("LDAPMSG.JNDISearchFailed") + e.getMessage();
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    VWSLog.err("In searchForMembers JNDISearch failed ->" + e.getMessage());
		    return new LinkedList();
		}
	    }
	    /**
	     * CV10.1 Enhancement Method added for searching memebers.
	     * @param dirContext
	     * @param sContext
	     * @param filter
	     * @param flag
	     * @return
	     */
	    private List searchForMembers_New(DirContext dirContext, String sContext, String filter, int flag)
	    {
	    	VWSLog.dbg("Inside searchForMembers_New method::::");
		LinkedList names = new LinkedList();
		if (dirContext  == null)
		{
		    VWSLog.dbg("dirContext was null");
		    return names;
		}
		SearchControls controls = new SearchControls();
		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String returningAttributes[] = new String[1];
		returningAttributes[0] = "member";
		controls.setReturningAttributes(returningAttributes);
		//controls.setTimeLimit(3000);
		try
		{
		    //sContext = "OU="+sContext +","+domainComponent;
		    sContext = getJNDICompositeName(sContext);
		   // VWSLog.dbg(" context -> " + sContext +" filter -> " + filter);
		    if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
				return searchForMembersInADS_New(dirContext, sContext, filter, flag);
		    }
		    NamingEnumeration searchResults = dirContext.search(sContext,filter,controls);
		    //VWSLog.dbg("Examining the search results");//+database.getName());
		    while (searchResults != null && searchResults.hasMoreElements())
		    {
			SearchResult searchResult=(SearchResult) searchResults.nextElement();
			//showAttributes(searchResult);
			Attributes attributes = searchResult.getAttributes();
			//Attribute attribute = attributes.get("member");
			//if (attribute == null) return names;
			NamingEnumeration ae = attributes.getAll();
			while (ae.hasMore())
			{

			    Attribute attr = (Attribute)ae.next();
			    //VWSLog.dbg(" attribute: " + attr.getID());
			    NamingEnumeration e = attr.getAll();
			    while (e.hasMore()){
				Object value = e.next();				
				//VWSLog.dbg("Members: " + value.toString());//+database.getName());
				names.add(extractUser(value.toString()));
			    }
			    e.close();
			}
		    }		
		    searchResults.close();
		    return names;
		}
		catch (NamingException e)
		{
			message = resourceManager.getString("LDAPMSG.JNDISearchFailed") + e.getMessage();
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    VWSLog.err("In searchForMembers JNDISearch failed ->" + e.getMessage());
		    return new LinkedList();
		}
	    }
	    
	    private List searchForMembersInADS(DirContext dirContext, String sContext, String filter)
	    {	    
	    	LinkedList names = new LinkedList();
	    	if (dirContext  == null)
	    	{
	    		VWSLog.dbg("dirContext was null");
	    		return names;
	    	}
	    	try{
	    		SearchControls controls = new SearchControls();
	    		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    		controls.setReturningObjFlag(true);
	    		String returningAttributes[] = new String[1];
	    		returningAttributes[0] = "distinguishedName";
	    		controls.setReturningAttributes(returningAttributes);		    
	    		sContext = getJNDICompositeName(sContext);
	    		//System.out.println(" context -> " + sContext +" filter -> " + filter);		    
	    		NamingEnumeration searchResults = dirContext.search(sContext,filter,controls);
	    		//VWSLog.dbg("Examining the search results");//+database.getName());			   
	    		if(searchResults != null && searchResults.hasMoreElements())
	    		{
	    			SearchResult searchResult = (SearchResult)searchResults.nextElement();
	    			Attributes attrs = searchResult.getAttributes();
	    			NamingEnumeration allAttributes = attrs.getAll();
	    			if (allAttributes != null && allAttributes.hasMoreElements()) {
	    				Attribute attr = (Attribute)allAttributes.nextElement();
	    				//System.out.println("attr "  + attr.getID() + " :::: " + attr.get());
	    				if (attr.getID().equalsIgnoreCase("distinguishedName")){
	    					String groupSearchFilters = "(MemberOf=" +attr.get()+")";
	    					VWSLog.dbg("Search for Members in searchForMembersInADS : " + groupSearchFilters);
	    					//System.out.println("Search Filters " + groupSearchFilters);
	    					if (VWSPreferences.getSyncGroupUsers()==false) {
	    					returningAttributes[0] = "sAMAccountName";
	    					} else  {
	    						returningAttributes[0] = "userPrincipalName";
	    					}
	    					SearchControls userControls = new SearchControls();
	    					userControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    					userControls.setReturningObjFlag(true);				
	    					userControls.setReturningAttributes(returningAttributes);
	    					NamingEnumeration userSearchResults = dirContext.search(sContext,groupSearchFilters,userControls);				
	    					while(userSearchResults.hasMoreElements())
	    					{
	    						SearchResult userSearchResult = (SearchResult)userSearchResults.nextElement();
	    						Attributes attrList = userSearchResult.getAttributes();
	    						NamingEnumeration userAttributesList = attrList.getAll();				    
	    						while (userAttributesList != null && userAttributesList.hasMoreElements()) {
	    							Attribute userAttribute = (Attribute)userAttributesList.nextElement();
	    							//System.out.println("Result: "+ userAttribute.getID() + " ::: " + userAttribute.get());
	    							if (VWSPreferences.getSyncGroupUsers()==true) {
	    								String name = "";
	    								if(userAttribute != null && !userAttribute.equals("")){
	    									name = userAttribute.get().toString();
											/**CV2019 - Code commented in CV10.2 line. Merged by Vanitha.S**/
	    									/*if(name.indexOf("@")!=-1)
	    										name = name.substring(0, (name.indexOf("@")));*/
	    								}
	    								VWSLog.dbg("Members : " + name);
	    								names.add(name);	
	    							} else {
	    								VWSLog.dbg("Members : " + userAttribute.get());
	    								names.add(userAttribute.get());
	    							}
	    						}				    
	    					}
	    					userSearchResults.close();
	    					//break;
	    				}
	    			}		    			    			
	    		}	
	    		searchResults.close();
	    		return names;		    

	    	}catch(Exception ex){
	    		ex.printStackTrace();
	    	}
	    	return names;
	    }
	    
	    private List searchForMembersInADS_New(DirContext dirContext, String sContext, String filter, int flag)
	    {	    
	    //VWSLog.add("searchForMembersInADS_New sContext: "+sContext+ " filter: " + filter);
		List names = new ArrayList();
		List namesList = new ArrayList();
		String ousName = null;
		if (dirContext  == null)
		{
		    VWSLog.dbg("dirContext was null");
		    return names;
		}
		try{
		    SearchControls controls = new SearchControls();
		    controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		    controls.setReturningObjFlag(true);
		    String returningAttributes[] = new String[1];
	    	returningAttributes[0] = "distinguishedName";
	    	
		    String returningAttributes_New[] = new String[1];
	    	returningAttributes_New[0] = "distinguishedName";
	    	
	    	
		    controls.setReturningAttributes(returningAttributes);		    
		    sContext = getJNDICompositeName(sContext);
		    //System.out.println(" context -> " + sContext +" filter -> " + filter);		    
		    VWSLog.dbg(" context -> " + sContext +" filter -> " + filter);
		    NamingEnumeration searchResults = dirContext.search(sContext,filter,controls);
		    //VWSLog.dbg("Examining the search results");//+database.getName());			   
		    VWSLog.dbg(" searchResults -> " +searchResults);
		    if(searchResults != null && searchResults.hasMoreElements())
		    {
			SearchResult searchResult = (SearchResult)searchResults.nextElement();
			String namespace2=searchResult.getNameInNamespace();
			System.out.println("namespace2 :: "+namespace2);
			Attributes attrs = searchResult.getAttributes();
			NamingEnumeration allAttributes = attrs.getAll();
			if (allAttributes != null && allAttributes.hasMoreElements()) {
			    Attribute attr = (Attribute)allAttributes.nextElement();
			    System.out.println("attr "  + attr.getID() + " :::: " + attr.get());
			    if (attr.getID().equalsIgnoreCase("distinguishedName")){
				/**CV2019 code merges from CV10.2 line. Merged by Vanitha.S**/
			    //String groupSearchFilters = "(MemberOf=" + attr.get()+")";
			    String groupSearchFilters = null;
			    groupSearchFilters = "(&(objectCategory=Person)(sAMAccountName=*)(MemberOf=" + attr.get()+"))";
			    //groupSearchFilters = "(&(objectCategory=Person)(MemberOf=" + attr.get()+"))";
				//VWSLog.dbg("Search for Members from searchForMembersInADS_New : " + groupSearchFilters);
			    VWSLog.dbg("OUs >>>>>>>>>>>>>>>>>>> "+OUs);
				for(int i=0;i<OUs.length;i++)
				{
					ousName = frameContextName(OUs[i]);
					VWSLog.dbg("OUs: "+ ousName);
					VWSLog.dbg("attr.get(): "+ attr.get().toString());
					VWSLog.dbg("Condition : "+!attr.get().toString().contains(ousName));
		    		if (!attr.get().toString().contains(ousName)) {
		    			if (i+1==OUs.length)
		    				return names;
		    		} else {
		    			i = OUs.length;
		    		}
		    			
				}
				VWSLog.dbg("names>>>>>>>>>>>>>>>>>>> "+names);
				//System.out.println("Search Filters " + groupSearchFilters);
				if (flag==1) {
					returningAttributes[0] = "userPrincipalName";
				} else  {
					returningAttributes[0] = "userPrincipalName";
					returningAttributes_New[0] = "sAMAccountName";
				}
				SearchControls userControls = new SearchControls();
				userControls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				userControls.setReturningObjFlag(true);				
				userControls.setReturningAttributes(returningAttributes);
				NamingEnumeration userSearchResults = dirContext.search(sContext,groupSearchFilters,userControls);
				
				SearchControls userControls_New = new SearchControls();
				userControls_New.setSearchScope(SearchControls.SUBTREE_SCOPE);
				userControls_New.setReturningObjFlag(true);				
				userControls_New.setReturningAttributes(returningAttributes_New);
				NamingEnumeration userSearchResults_New = dirContext.search(sContext,groupSearchFilters,userControls_New);
				
				while(userSearchResults.hasMoreElements())
				{
					String userNameWithFullInfo="";
				    SearchResult userSearchResult = (SearchResult)userSearchResults.nextElement();
				    Attributes attrList = userSearchResult.getAttributes();
				    NamingEnumeration userAttributesList = attrList.getAll();				    
				    while (userAttributesList != null && userAttributesList.hasMoreElements()) {
						/**CV2019 code merges from CV10.2 line. Merged by Vanitha.S**/
				    	Attribute userAttribute = (Attribute)userAttributesList.nextElement();
				    	String userattrId=userAttribute.getID();

				    	String name_New = "";
				    	if (flag==0) {
				    		while(userSearchResults_New.hasMoreElements())
				    		{
				    			SearchResult userSearchResult_New = (SearchResult)userSearchResults_New.nextElement();
				    			Attributes attrList_New = userSearchResult_New.getAttributes();
				    			NamingEnumeration userAttributesList_New = attrList_New.getAll();				    
				    			while (userAttributesList_New != null && userAttributesList_New.hasMoreElements()) {
				    				Attribute userAttribute_New = (Attribute)userAttributesList_New.nextElement();
				    				VWSLog.dbg("------------------Members userAttribute_New.get().toString() : " + userAttribute_New.get().toString());
				    				name_New = userAttribute_New.get().toString();
				    			}
				    		}
				    		userSearchResults_New.close();
				    	}
				    	String name = "";
				    	String allNames = "";
				    	if(userAttribute != null && !userAttribute.equals("")){
				    		name = userAttribute.get().toString();
				    		VWSLog.dbg("Members userAttribute.get().toString() : " + userAttribute.get().toString());
				    		/*if(name.indexOf("@")!=-1)
							name = name.substring(0, (name.indexOf("@")));*/
				    		if (flag==0) {
				    			allNames =  name_New + Util.SepChar + name;
				    			//VWSLog.dbg("Members userAttribute.get().toString() name : " + name);
				    			name = allNames;
				    			name_New="";
				    		}
				    	}
				    	VWSLog.dbg("Members : " + name);
				    	if(name!=null&&name.length()>0){
				    		userNameWithFullInfo=getUserFullInfoOtherOU(name);
				    		//VWSLog.add("userNameWithFullInfo from searchForMembersInADS_New:"+userNameWithFullInfo);
				    		if (userNameWithFullInfo.length()==0) {
				    			userNameWithFullInfo=getUserFullInfo(name);
				    			//VWSLog.add("userNameWithFullInfo1 :"+userNameWithFullInfo);
				    		}
				    	}
				    	VWSLog.dbg("userNameWithFullInfo : " + userNameWithFullInfo);
				    	if(userNameWithFullInfo.length()>0){
				    		if(!names.contains(userNameWithFullInfo)){
				    			names.add(userNameWithFullInfo);
				    		}
				    	}
						/**End of CV10.2 code merges*****/
				    }	
					
				}
				userSearchResults.close();
				//break;
			    }
			}		    			    			
		    } else {
		    	VWSLog.add("Failed to fetch users for groupSearchFilters from searchForMembersInADS_New method.");
		    }
		    searchResults.close();
		    VWSLog.dbg("searchForMembersInADS_New names: "+ names);
		    return names;		    

		}catch(Exception ex){
		    ex.printStackTrace();
		}
		return names;
	    }
//------------------------------------------------------------------------------
//	  ------------------------------------------------------------------------------
	    //Return type linked list is giving problem when data is having &. So changed into vector
	    public Vector getUsers(boolean isLowerCaseRequired)
	    {
	    	VWSLog.dbg(" ");
	    	VWSLog.dbg("Start of getUsers from LDAP.java");
    		VWSLog.dbg("Getting Users ");
    		try{
    			Vector userVector = new Vector();
    			String userName = null;
		        DirContext dircontext = getInitialContext();
		        if(dircontext == null)	return userVector;
		        List list = new LinkedList();
		        if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
		        	for(int i=0; i < OUs.length; i++)
		            {
			        	list = searchForNames(dircontext, frameContextNameForADS(OUs[i]), "(&(objectCategory=Person))");
				        VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
				        for(int j = 0; j < list.size(); j++)
				        {
				        	userName = list.get(j).toString();
				        	if (isLowerCaseRequired) {
				        		userName = userName.toLowerCase();
				        		if (userName.contains("@")) {
				        			userName = userName.substring(0, userName.indexOf("@"));
				        		}
				        	}
				        	userVector.add(userName);
			        		
				        }
		            }
		        	
		        }else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL)){
		        	for(int i=0; i < OUs.length; i++)
		            {
		                list = searchForNames(initialContext,frameContextNameForNovell(OUs[i]),"(&(objectClass=user))");
		                VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
		    	        for(int j = 0; j < list.size(); j++)
		    	        {
		    	        	userName = list.get(j).toString();
		    	        	if (isLowerCaseRequired) {
				        		userName = userName.toLowerCase();
				        		if (userName.contains("@")) {
				        			userName = userName.substring(0, userName.indexOf("@"));
				        		}
				        	}
				        	userVector.add(userName);
		    	        }
		            }
		        }
		        dircontext.close();
		        VWSLog.dbg(" ");
		        VWSLog.dbg("End of getUsers from LDAP.java: " + userVector);
		        VWSLog.dbg("\n");
		        return userVector;
    		}catch(Exception ex){
    			message = resourceManager.getString("LDAPMSG.ERRORUsers")+ex.getMessage();
    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    		    VWSLog.err("Error in getting users "+ex.getMessage());
    		    VWSLog.dbg("End of getUsers from LDAP.java");
    			return new Vector();
    		}
	    }
	    public Vector getUsers1(String dirName1, String loginServer1)
	    {
    		VWSLog.dbg("Getting Users ");
    		try{
    			Vector userVector = new Vector();
		        DirContext dircontext = getInitialContext1(dirName1,loginServer1);
		        if(dircontext == null)	{
		        	VWSLog.dbg("Inside dircontext is null.....");
		        	return userVector;
		        }
		        List list = new LinkedList();
		        if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
		        	for(int i=0; i < OUs.length; i++)
		            {
			        	list = searchForNames(dircontext, frameContextNameForADS(OUs[i]), "(&(objectCategory=Person))");
				        VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
				        for(int j = 0; j < list.size(); j++)
				        {
				        	userVector.add(list.get(j));
			        		VWSLog.dbg("User -> "+list.get(j));
				        }
		            }
		        	
		        }else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL)){
		        	for(int i=0; i < OUs.length; i++)
		            {
		                list = searchForNames(initialContext,frameContextNameForNovell(OUs[i]),"(&(objectClass=user))");
		                VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
		    	        for(int j = 0; j < list.size(); j++)
		    	        {
		    	        	userVector.add(list.get(j));
		            		VWSLog.dbg("User -> "+list.get(j));
		    	        }
		            }
		        }
		        dircontext.close();
		    	VWSLog.dbg("userVector before returning  -> "+userVector);
		        return userVector;
    		}catch(Exception ex){
    			message = resourceManager.getString("LDAPMSG.ERRORUsers")+ex.getMessage();
    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    		    VWSLog.err("Error in getting users "+ex.getMessage());
    			return new Vector();
    		}
	    }
	    public Vector getUsers1New(String dirName1, String loginServer1, String searchForPName)
	    {
    		VWSLog.dbg("Getting Users New ");
    		try{
    			Vector userVector = new Vector();
		        DirContext dircontext = getInitialContext1(dirName1,loginServer1);
		        if(dircontext == null)	{
		        	VWSLog.dbg("Inside dircontext is null.....");
		        	return userVector;
		        }
		        List list = new LinkedList();
		        if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
		        	for(int i=0; i < OUs.length; i++)
		            {
		        		VWSLog.dbg("getUsers1New dircontext  ::: "+dircontext);
		        		VWSLog.dbg("getUsers1New frameContextNameForADS(OUs[i])  ::: "+frameContextNameForADS(OUs[i]));
			        	//list = searchForNames(dircontext, frameContextNameForADS(OUs[i]), "(&(objectCategory=Person)(|(cn="+searchForPName+")))");
		        		if((searchForPName.length()>0)&&(searchForPName!=null)){
		        			list = searchForNames(dircontext, frameContextNameForADS(OUs[i]), "(&(objectCategory=Person)(|(cn="+searchForPName+")))");
		        			VWSLog.dbg("getUsers1New Groups list after pulling from Ldap getSyncGroupsWithRegistry ::: "+list);
		        		}else{
		        			list = searchForNames(dircontext, frameContextNameForADS(OUs[i]), "(&(objectCategory=Person))");
		        			VWSLog.dbg("Groups list in else ::: "+list);
		        		}

				        VWSLog.dbg("getUsers1New Users size for context '"+OUs[i]+"' -> "+list.size());
				        for(int j = 0; j < list.size(); j++)
				        {
				        	userVector.add(list.get(j).toString().toLowerCase());
			        		VWSLog.dbg("getUsers1New User -> "+list.get(j));
				        }
		            }
		        	
		        }else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL)){
		        	for(int i=0; i < OUs.length; i++)
		            {
		                list = searchForNames(initialContext,frameContextNameForNovell(OUs[i]),"(&(objectClass=user))");
		                VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
		    	        for(int j = 0; j < list.size(); j++)
		    	        {
		    	        	userVector.add(list.get(j).toString().toLowerCase());
		            		VWSLog.dbg("User -> "+list.get(j));
		    	        }
		            }
		        }
		        dircontext.close();
		    	VWSLog.dbg("getUsers1New userVector before returning  -> "+userVector);
		        return userVector;
    		}catch(Exception ex){
    			message = resourceManager.getString("LDAPMSG.ERRORUsers")+ex.getMessage();
    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    		    VWSLog.err("Error in getting users "+ex.getMessage());
    			return new Vector();
    		}
	    }
	    public Vector getUserMailIds1(List users,String principalDirName,String curLoginServer){
			try{
			    Vector userVector = new Vector();
			    DirContext dircontext = getInitialContext1(principalDirName,curLoginServer);
			    if(dircontext == null)	{
			    	VWSLog.dbg("Inside principal directory name is null......");
			    	return userVector;
			    }
			    Iterator iterator = users.iterator();
			    while (iterator.hasNext())
			    {
				String userName = ((String) iterator.next()).trim();
				String context = "";		                
				String userMailIds = "";
				if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
				    userMailIds = searchForUserMails(dircontext,context,"(&(objectClass=user)(cn="+userName+"))");
				else{ // For LDAP ADS CV10.1 Issue fix to pull other ou mailid's modified by B.Madhavan 26/6/2017
					VWSLog.dbg("user name before getting user dn ......"+userName);
					String userDN=getUserBasicAttributes(userName+"@"+principalDirName);
					VWSLog.dbg("usertDN inside getUserMailIds....."+userDN);
					if(userDN.length()>0){
					    userMailIds = searchForUserMails(dircontext,userDN,"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(name=" + userName + ")(userPrincipalName=" + userName+"@"+principalDirName + ")))");
					 	VWSLog.dbg("userMailIds inside user dn ......"+userMailIds);
					}else{				
				    userMailIds = searchForUserMails(dircontext,context,"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(name=" + userName + ")(userPrincipalName=" + userName+"@"+principalDirName + ")))");
				 	VWSLog.dbg("userMailIds inside else......"+userMailIds);
					}
				   }//End of fix.
				if(userMailIds.trim().equals("")) userMailIds = "-";
				userVector.add(userMailIds);
			    }
			    dircontext.close();		        
			    return userVector;
			}catch(Exception ex){
				message = resourceManager.getString("LDAPMSG.ERRORMailIds")+ex.getMessage();
				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    VWSLog.err("Error in getting UserMailIds "+ex.getMessage());
			    return new Vector();
			}
		    }
	    private DirContext getInitialContext1(String dirName1,
				String loginServer2) {
    	    Hashtable env = new Hashtable();
            //Example principal for Novell LDAP "cn=admin, o=ssvwc");
            env.put(Context.INITIAL_CONTEXT_FACTORY,ldapCTX);
	        VWSLog.dbg("loginserver2 is :::::::"+loginServer2);
            String url = "ldap://" + loginServer2 + ":" + port;
            VWSLog.dbg("PROVIDER_URL: "+url);
            env.put(Context.PROVIDER_URL, url);
            if(Integer.parseInt(port) == SSL_PORT)
            	env.put(Context.SECURITY_PROTOCOL, SECURITY_PROTOCOL_SSL);
            String principal = new String();
            if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS) && (dirName1.indexOf(".")!=-1)){
            	VWSLog.dbg("loginServer before calling the key 2::::"+loginServer2);
            	VWSLog.dbg("loginServer2 :::::"+loginServer2);
            	VWSLog.dbg("ldapDomainIPMap 2::::"+ldapDomainIPMap);
            	Object value=  getKeyFromValue(ldapDomainIPMap,loginServer2);
            	if(value!=null){
            		String mapDir=value.toString();
            		VWSLog.dbg("mapDir::::::::"+mapDir);
            		if(ldapDirName.equalsIgnoreCase(mapDir.toString())){
            			principal = adminUser+"@"+dirName1;
            		}else{
            			principal = adminUser+"@"+mapDir;
            		}
            	}else{
            		principal = adminUser+"@"+dirName1;
            	}
                VWSLog.dbg("principal :::::::::::::"+principal);
            }
            else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
        	// Comment the existing "cn=" + adminUser, User will framing the admin user with OU's  		        	
        	principal = adminUser+", "+domainComponent;//principal = "cn="+adminUser+", "+domainComponent;
        	
            else 
            	principal = adminUser+"@"+dirName1;

            	
            //VWSLog.dbg("SECURITY_PRINCIPAL "+principal);
            env.put(Context.SECURITY_PRINCIPAL, principal);
            env.put(Context.SECURITY_CREDENTIALS, adminPass);
            env.put(Context.SECURITY_AUTHENTICATION, securityAuthentication);
            //VWSLog.dbg("LDAP Provider Tree: "+ldapDirName);
        try
        {
            initialContext = new InitialDirContext(env);
        }
        catch(AuthenticationNotSupportedException authenticationnotsupportedexception)
        {
        	message = resourceManager.getString("LDAPMSG.Authentication")+" "+authenticationnotsupportedexception.getMessage();
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
            VWSLog.err("Authentication specified is not Supported.  "+authenticationnotsupportedexception.getMessage());
        }
        catch(NamingException namingexception)
        {
        	message =resourceManager.getString(resourceManager.getString("LDAPMSG.DirectoryContext")+" "+namingexception.getMessage());
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
            VWSLog.err("Unable to get Initial LDAP Directory Context-> "+namingexception.getMessage());
        }
		return initialContext;
    }

		public Vector getUsersUPN()
	    {
    		VWSLog.dbg("Getting UserUPN ");
    		try{
    			Vector userVector = new Vector();
		        DirContext dircontext = getInitialContext();
		        if(dircontext == null)	return userVector;
		        List list = new LinkedList();
		        if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
		        	for(int i=0; i < OUs.length; i++)
		            {
		        		VWSLog.dbg("searchForNamesUPN inside getUsersUPN::"+"dircontext::"+dircontext+"OUs[i]"+OUs[i]);
		        		//Objectclass=user added in search filter to select only the type as user from ldap issue fix.
		        		list = searchForNamesUPN(dircontext, frameContextNameForADS(OUs[i]), "(&(objectCategory=Person)(objectClass=user))");
				        VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
				        VWSLog.dbg("List value in getUsersUPn::"+list);
				        for(int j = 0; j < list.size(); j++)
				        {
				        	userVector.add(list.get(j));
			        		//VWSLog.dbg("UserUPN -> "+list.get(j));
				        }
		            }
		        	
		        }else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL)){
		        	for(int i=0; i < OUs.length; i++)
		            {
		                list = searchForNamesUPN(initialContext,frameContextNameForNovell(OUs[i]),"(&(objectClass=user))");
		                VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
		    	        for(int j = 0; j < list.size(); j++)
		    	        {
		    	        	userVector.add(list.get(j));
		            		//VWSLog.dbg("UserUPN -> "+list.get(j));
		    	        }
		            }
		        }
		        dircontext.close();
		        VWSLog.dbg("getUsersUPN before returning the vector value::"+userVector);
		        return userVector;
    		}catch(Exception ex){
    			message = resourceManager.getString("LDAPMSG.ERRORUsers")+ex.getMessage();
    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    		    VWSLog.err("Error in getting users inside catch "+ex.getMessage());
    			return new Vector();
    		}
	    }
	    public String getUserPrincipalName(SearchResult sr){
	    	VWSLog.dbg("inside getUserPrincipalName...");
	    	VWSLog.dbg("Start of getUserPrincipalName from LDAP.java");
	    	//VWSLog.dbg("getName:	"+sr.getName().toString()+", "+domainComponent);
	    	String cnName = sr.getName().toString();//+", "+domainComponent;
	    	VWSLog.dbg("domainComponent in getUserPrincipalName: "+cnName);
		    String[] dirList = null;
	    	try{
		    	//Attributes attrs = initialContext.getAttributes(cnName);
	    		VWSLog.dbg("Before getInitialContext of getUserPrincipalName from LDAP.java");
	    		DirContext dircontext = getInitialContext();
	    		VWSLog.dbg("After getInitialContext of getUserPrincipalName from LDAP.java");
	    		Attributes attrs = dircontext.getAttributes(sr.getNameInNamespace());
		        String userPrincipalName = "";
		        try{
					userPrincipalName = attrs.get("userPrincipalName").toString();
				}catch(Exception ex){
					userPrincipalName = attrs.get("sAMAccountName").toString();
				}
		        VWSLog.dbg("user Principal Name"+userPrincipalName);
				if(userPrincipalName != null && !userPrincipalName.equals("")){
					StringTokenizer st1=new StringTokenizer(userPrincipalName,":");
					st1.nextToken();
					String name = st1.nextToken().trim();
					//Added condition by Srikanth on 14 Feb 2019 for single LDAP and multiple LDAP

					String ldapDirectory = VWSPreferences.getTree();
					//VWSLog.dbg("ldapDirectory ::::::::"+ldapDirectory);
					dirList = ldapDirectory.split(",");
					//VWSLog.dbg("No of domains :::" + dirList.length);

					if (dirList.length == 1) {
						if(name.indexOf("@")!=-1)
							name = name.substring(0, (name.indexOf("@")));
					}
					
					dircontext.close();
					VWSLog.dbg("End of getUserPrincipalName from LDAP.java" + name);
					return name;
				}else{
					dircontext.close();
					VWSLog.dbg("End of getUserPrincipalName from LDAP.java" +extractUser(sr.getName().toString()) );
					return extractUser(sr.getName().toString());
				}
				
	    	}catch(Exception ex){
	    		VWSLog.dbg("Exception in getUserPrincipalName from LDAP.java");
	    		return extractUser(sr.getName().toString());
	    	}
	    }
	    
	    /*public void showAttributes(SearchResult sr){
	    	//VWSLog.err("getName:	"+sr.getName().toString()+", DC=vwdev,DC=com");
	    	String cnName = sr.getName().toString()+", DC=vwdev,DC=com";
	    	try{
		    	Attributes attrs = initialContext.getAttributes(cnName);
		    	//Attributes attrs = sr.getAttributes();
		    	NamingEnumeration ne = attrs.getAll();
				while (ne.hasMoreElements()){
					String value=String.valueOf(ne.nextElement());
					if(value.indexOf("userPrincipalName") !=-1 || value.indexOf("sAMAccountName") !=-1 || 
							value.indexOf("givenName") !=-1 )
						VWSLog.err("Name value pair:	"+value);
					//StringTokenizer st1=new StringTokenizer(value,":");
				    //String name = st1.nextToken();
				    //if(name.equalsIgnoreCase("sAMAccountName"))
				    	//VWSLog.dbg("value "+ st1.nextToken());
				    //st1=null;
				}
	    	}catch(Exception ex){	    	}
	    }*/
	    /*
	    * Return type linked list is giving problem when data is having &. So changed into vector
	    * @see com.computhink.vws.directory.Directory#getGroups()
	    */
	    public Vector getGroups()
	    {
    		VWSLog.dbg("Getting Groups");
	    	try{
	    		Vector groupVector = new Vector();
		        DirContext dircontext = getInitialContext();
		        if(dircontext == null)	return groupVector;
		        List list = new LinkedList();
		        //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
	        	for(int i=0; i < OUs.length; i++)
	            {
	        		String sContext = "";
	        		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS))
	        			sContext = frameContextNameForADS(OUs[i]);
	            	else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
	            		sContext = frameContextNameForNovell(OUs[i]);
	        		//groupNameRegistry=;
					/**CV2019 code merges from CV10.2 line. Merged by Vanitha.S**/	  
	        		VWSLog.dbg("groupName from getSyncGroupsWithRegistry is  :::"+VWSPreferences.getSyncGroupsWith());
	        		if((VWSPreferences.getSyncGroupsWith().length()>0)&&(VWSPreferences.getSyncGroupsWith()!=null)){
	        			list = searchForNames(dircontext, sContext,"(&(objectClass=Group)(|(cn="+VWSPreferences.getSyncGroupsWith()+")))");
	        			VWSLog.dbg("Groups list after pulling from Ldap getSyncGroupsWithRegistry ::: "+list);
	        		}else{
	        			list = searchForNames(dircontext, sContext, "(&(objectClass=group))");
	        			VWSLog.dbg("Groups list in else ::: "+list);
	        		}
					/**End of CV10.2 code merge****/
	            	VWSLog.dbg("Groups size for context '"+OUs[i]+"' -> "+list.size());
	    	        for(int j = 0; j < list.size(); j++)
	    	        {
	    	        	groupVector.add(list.get(j));
	    	        		VWSLog.dbg(("Group -> "+list.get(j)));
	    	        }
	            }
	        	dircontext.close();
	        	return groupVector;
	    	}catch(Exception ex){
	    		VWSLog.dbg("Error in getting groups "+ex.getMessage());
	    		return new Vector();
	    	}
	    }
	    
    
	    
    	
	    public Vector getGroups1(String dirName, String loginServer)
	    {
    		VWSLog.dbg("Getting Groups");
	    	try{
	    		Vector groupVector = new Vector();
		        DirContext dircontext = getInitialContext1(dirName,loginServer);
		        if(dircontext == null)	return groupVector;
		        List list = new LinkedList();
		        //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
	        	for(int i=0; i < OUs.length; i++)
	            {
	        		String sContext = "";
	        		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS))
	        			sContext = frameContextNameForADS(OUs[i]);
	            	else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
	            		sContext = frameContextNameForNovell(OUs[i]);
	        		list = searchForNames(dircontext, sContext, "(&(objectClass=group))");
	            	VWSLog.dbg("Groups size for context '"+OUs[i]+"' -> "+list.size());
	    	        for(int j = 0; j < list.size(); j++)
	    	        {
	    	        	groupVector.add(list.get(j));
	    	        		VWSLog.dbg(("Group -> "+list.get(j)));
	    	        }
	            }
	        	dircontext.close();
	        	return groupVector;
	    	}catch(Exception ex){
	    		VWSLog.dbg("Error in getting groups "+ex.getMessage());
	    		return new Vector();
	    	}
	    }
	    public Vector getGroups1New(String dirName, String loginServer, String searchForPName)
	    {
    		VWSLog.dbg("Getting Groups New");
	    	try{
	    		Vector groupVector = new Vector();
		        DirContext dircontext = getInitialContext1(dirName,loginServer);
		        VWSLog.dbg("getGroups1New sContext ::: "+dircontext);
		        if(dircontext == null)	return groupVector;
		        List list = new LinkedList();
		        //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
	        	for(int i=0; i < OUs.length; i++)
	            {
	        		String sContext = "";
	        		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS))
	        			sContext = frameContextNameForADS(OUs[i]);
	            	else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
	            		sContext = frameContextNameForNovell(OUs[i]);
	        		VWSLog.dbg("getGroups1New sContext ::: "+dircontext);
	        		VWSLog.dbg("getGroups1New sContext ::: "+sContext);
	        		//list = searchForNames(dircontext, sContext, "(&(objectClass=group)(|(cn="+searchForPName+")))");
	        		VWSLog.dbg("search filter : (&(objectClass=group)(|(cn="+searchForPName+")))");
	        		if((searchForPName.length()>0)&&(searchForPName!=null)){
	        			list = searchForNames(dircontext, sContext, "(&(objectClass=group)(|(cn="+searchForPName+")))");
	        			VWSLog.dbg("getGroups1New Groups list after pulling from Ldap getSyncGroupsWithRegistry ::: "+list);
	        		}else{
	        			list = searchForNames(dircontext, sContext, "(&(objectClass=group))");
	        			VWSLog.dbg("Groups list in else ::: "+list);
	        		}
	        	
	            	VWSLog.dbg("getGroups1New Groups size for context '"+OUs[i]+"' -> "+list.size());
	    	        for(int j = 0; j < list.size(); j++)
	    	        {
	    	        	groupVector.add(list.get(j));
	    	        		VWSLog.dbg(("getGroups1New Group -> "+list.get(j)));
	    	        }
	            }
	        	dircontext.close();
	        	return groupVector;
	    	}catch(Exception ex){
	    		VWSLog.dbg("Error in getting groups "+ex.getMessage());
	    		return new Vector();
	    	}
	    }
	    public int AuthenticateUser(String user, String pass)
	    {
	    	//VWSLog.dbg("AuthenticateUser method UPNSuffix ::" + userLastName);
	    	//VWSLog.dbg("AuthenticateUser method user ::" + user);
	    	VWSLog.dbg("VWSPreferences.getAuthenticateUserDN in authenticateuser:::::"+VWSPreferences.getAuthenticateUserDN());
	    	VWSLog.dbg("LDAP UserDN :"+ldapuserDN);
	    	if ( (ldapuserDN != null && (ldapuserDN.equals("") || ldapuserDN.equals("-")))||(VWSPreferences.getAuthenticateUserDN()==false) ) {
	    	VWSLog.dbg("Inside authenticatedn false ........userLastName :"+userLastName);
	    	String userUPN = "";
	    	if ( userLastName != null && (!userLastName.equalsIgnoreCase("")) && userLastName.length()>0 ) {
		    	if (user.indexOf("@") > 0)
		    		userUPN = user ;
		    	else
		    		userUPN = user + "@" + userLastName;
	    	}
	    	else {
	    		userUPN = user;
	    	}
	    	VWSLog.dbg("AuthenticateUser method userUPN:: " + userUPN);
	    	if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
	    		int index=0;
	    		String commonName = "";
	    		
	    		if(user.contains(".")){
	    			index = user.indexOf(".");
	    			VWSLog.dbg("8 index is :: " + user);
	    		}
		    
		    	if (index > 0)
		        {
		            String userName = user.substring(0, index);
		    		String context = user.substring(index+1);
		    		commonName = getCommonName(userName, pass);
		    		if(AuthenticateUser(commonName, pass, context)){
	    				return 1;}
		    		//Consider the user name is with . character	
		    		try{
		    		    //commonName = getCommonName(user, pass);
		    			commonName = getCommonName(userUPN, pass);
		    		    VWSLog.dbg("Return value after getCommonName inside try block ::" + commonName);
		    		    for(int x= 0; x < OUs.length; x++){
		    		    	VWSLog.dbg("Inside loop before authenticateUser commonName::" + commonName + "  OUs[x]::" + OUs[x]);
		    		    	if(AuthenticateUser(commonName, pass, OUs[x])){
		    			    return 2;// The User name is having the '.' character
		    			}}
		    		}catch(Exception ex){
		    		    VWSLog.dbg("Exception AuthenticateUser  is : " + ex.getMessage());    
		    		}


		        }else{
		        	VWSLog.dbg("AuthenticateUser method before fetching commonName userUPN in else1::" + userUPN );
		        	//commonName = getCommonName(user, pass);
		        	commonName = getCommonName(userUPN, pass);
		        	VWSLog.dbg("Return value after getCommonName inside else block commonName::" + commonName);
		        	for(int i = 0; i < OUs.length; i++){
	        	    	if(AuthenticateUser(commonName, pass, OUs[i])){
	        	    		return 1;}
	        	    }
		    	}
		    	VWSLog.dbg("AuthenticateUser '" + user + "' for context '" + OUList + "' " + "Failed");
		        return 0;
	    	}else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL)){
	    		VWSLog.dbg("Inside schemeType novell ........"+user);
		    	int index = user.indexOf(".");
		    	VWSLog.dbg("index :"+index);
		    	if (index > 0)
		        {
		            String userName = user.substring(0, index);
		    		String context = user.substring(index+1);
		    		context = frameContextNameForNovell(context);
		    		context = "cn="+userName+","+context;
		    		VWSLog.dbg("Before calling AuthenticateUserID  context :"+context);
		    		if(AuthenticateUserID(userName, pass, context))
		    			return 1;
		        }else{
		        	//No need to call getCommonName for novell 
		        	//commonName = getCommonName(user, pass);
		        	for(int i = 0; i < OUs.length; i++){
		        		VWSLog.dbg("Before calling AuthenticateUserNovell  OUs["+i+"] :"+OUs[i]);
	        	    	if(AuthenticateUserNovell(user, pass, OUs[i])){
	        	    		return 1;}
	        	    }
		    	}
		    	VWSLog.dbg("AuthenticateUser '" + user + "' for context '" + OUList + "' " + "Failed");
		        return 0;
	    	}
	    	}else{
	    		VWSLog.dbg("AuthenticateUser method before fetching commonName userUPN2000000000000000559999999::" );
	    		if(VWSPreferences.getAuthenticateUserDN()==true){
	    			int returnUserDN=AuthenticateUserDN(user,pass);

	    			return returnUserDN;
	    		}
	    	}
	    	VWSLog.dbg("AuthenticateUser '" + user + "' for context '" + OUList + "' " + "Failed");
	        return 0;

	    }
	    
	    @Override
	    /**
	     * CV2019 code merge from CV10.2 line. Merged by Vanitha.S
	     */
		public int AuthenticateUserDN(String userName, String password, String userDN) {
			// TODO Auto-generated method stub
	    	if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
	    		//getCommonNameDN();
	    		if(AuthenticateUserID(userName, password, userDN)){
	    			VWSLog.dbg("Before returning 1 from authenticate userid");
					return 1;
				}
	    	}
			return 0;
		}
	    /**
	     * CV10.1 :- Ldap authenticate user with dn 
	     * @param user
	     * @param pass
	     * @return
	     * modified by: sudhansu
	     */
	    
	    public int AuthenticateUserDN(String user, String pass)
	    {
	    	VWSLog.dbg("AuthenticateUser method UPNSuffix ::" + userLastName);
	    	System.out.println(userLastName);
	    	
	    	VWSLog.dbg("AuthenticateUser method user ::" + user);
	    	String userUPN = "";
	    	if ( (!userLastName.equalsIgnoreCase("")) && userLastName.length()>0 ) {
		    	if (user.indexOf("@") > 0)
		    		userUPN = user ;
		    	else
		    		userUPN = user + "@" + userLastName;
	    	}
	    	else {
	    		userUPN = user;
	    	}
	    	String dnOUString="";
	    	String reverseOU="";
	    	if ( ldapuserDN != null && (!ldapuserDN.equalsIgnoreCase("")) && ldapuserDN.length()>0 ) 
	    	{
	    		VWSLog.dbg("ldapuserDN value inside authenticate user ::::"+ldapuserDN);
	    		String totalUserDN[]=ldapuserDN.split(",");
	    		for(int i=0; i < totalUserDN.length; i++){
	    			System.out.println(totalUserDN[i]);
	    			if(totalUserDN[i].contains("OU=")){
	    				dnOUString=dnOUString+";"+totalUserDN[i].substring(totalUserDN[i].indexOf("=")+1,totalUserDN[i].length());
	    			}
	    		}

	    		String newUserDN[]=dnOUString.split(";");

	    		for(int j=newUserDN.length-1;j>0;j--){
	    			reverseOU=reverseOU+";"+newUserDN[j];
	    		}
	    		reverseOU=reverseOU.substring(1,reverseOU.length());
	    		
	    		VWSLog.dbg("reverseOU "+reverseOU);
	    	}
	    	
	    	VWSLog.dbg("AuthenticateUser method userUPN:: " + userUPN);
	    	if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
	    		VWSLog.dbg("Inside if condition of ldap ads");
		    	int index = user.indexOf(".");
		    	String commonName = "";
		    	if (index > 0)
		        {
		            String userName = user.substring(0, index);
		        	VWSLog.dbg("userName in Authenticate method :::::::::"+userName);
		    		String context = user.substring(index+1);
		    		VWSLog.dbg("userName in Authenticate method :::::::::"+context);
		    		commonName = getCommonName(userName, pass);
		    		VWSLog.dbg("userName in Authenticate method :::::::::"+commonName);
		    		VWSLog.dbg("commonName:::::::::"+commonName);
		    		VWSLog.dbg("pass:::::::::"+pass);
		    		VWSLog.dbg("context:::::::::"+context);
		    		
		    /*		if(AuthenticateUser(commonName, pass, reverseOU)){
	    				return 1;}*/
		    		//Consider the user name is with . character	
		    		try{
		    		    //commonName = getCommonName(user, pass);
		    			commonName = getCommonName(userUPN, pass);
		    		    VWSLog.dbg("Return value after getCommonName inside try block ::" + commonName);
		    		    String reverseOU1[]=reverseOU.toString().split(";");
		    		    for(int x= 0; x < reverseOU1.length; x++){
		    		    	VWSLog.dbg("Inside loop before authenticateUser commonName::" + commonName + "  OUs[x]::" + OUs[x]);
		    		    	if(AuthenticateUser(commonName, pass, reverseOU1[x])){
		    			    return 2;// The User name is having the '.' character
		    			}}
		    		}catch(Exception ex){
		    		    VWSLog.dbg("Exception AuthenticateUser  is : " + ex.getMessage());    
		    		}


		        }else{
		        	VWSLog.dbg("AuthenticateUser method before fetching commonName userUPN in else2::" + userUPN );
		        	//commonName = getCommonName(user, pass);
		        	commonName = getCommonName(userUPN, pass);
		        	VWSLog.dbg("Return value after getCommonName inside else block commonName::" + commonName);
		        	VWSLog.dbg("commonName1 :::::::::"+commonName);
	        		VWSLog.dbg("reverseOU:::::::::"+reverseOU);
	        		String reverseOU1[]=reverseOU.toString().split(";");
		        	for(int i = 0; i < reverseOU1.length; i++){
		        		
	        	    	if(AuthenticateUser(commonName, pass, reverseOU1[i])){
	        	    		return 1;}
	        	    }
		    	}
		    	VWSLog.dbg("AuthenticateUser '" + user + "' for context '" + OUList + "' " + "Failed");
		        return 0;
	    	}
	    	VWSLog.dbg("AuthenticateUser '" + user + "' for context '" + OUList + "' " + "Failed");
	        return 0;

	    }
	    /*User is login with User login name, So getting common name to check login context validity.
	     * Valli 22 March 2207*/
	    public String getCommonName(String user, String pass)
	    {
	    	int indexPos =0;
	    	Hashtable env = new Hashtable();
	    	VWSLog.dbg("loginServer inside getcommonName::::"+loginServer);
			String providerURL = "ldap://" + loginServer + ":" + port;
			VWSLog.dbg("providerURL3:::::"+providerURL);
			env.put(Context.INITIAL_CONTEXT_FACTORY,ldapCTX);
			env.put(Context.PROVIDER_URL,providerURL);
			env.put(Context.SECURITY_AUTHENTICATION,securityAuthentication);
			if(user.contains("@")){
			user.indexOf("@");
			}
			VWSLog.dbg("11 ldapDirName :::"+ldapDirName);
			if (indexPos <= 0 ) 
				env.put(Context.SECURITY_PRINCIPAL, user+"@"+ldapDirName);
			else
				env.put(Context.SECURITY_PRINCIPAL, user);
			VWSLog.dbg("user name after put in env is :::::"+user);
			VWSLog.dbg("Inside getCommonName method hashtable value::" + env);
			env.put(Context.SECURITY_CREDENTIALS,pass);
			InitialDirContext initialdircontext;
			try
			{
				initialdircontext = new InitialDirContext(env);
				SearchControls controls = new SearchControls();
				controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
				String returningAttributes[] = new String[1];
				returningAttributes[0] = "userPrincipalName";
				controls.setReturningAttributes(returningAttributes);
				//String filter = "(&(objectClass=user))";
				
				String filter = "";
				if (indexPos <=0 )
					filter = "(&(objectCategory=Person)(userPrincipalName="+user+"@"+ldapDirName+"))";
				else
					filter = "(&(objectCategory=Person)(userPrincipalName="+user+"))";	
				
				//String filter = "(&(objectCategory=Person)(sAMAccountName="+user+"))";
				
				System.out.println("searchResults before filter" );
				//user="vijaypriya@vwdev.com";
				
				VWSLog.dbg("Inside commonName method filter value::" + filter);
				VWSLog.dbg("domainComponent before naming enum:::"+domainComponent);
				NamingEnumeration searchResults = initialdircontext.search(domainComponent, filter, controls);
				
/*				if (searchResults ==null) {
					VWSLog.dbg(":::::Inside commonName method searchResult value is null:::::" );
				}
*/				while (searchResults != null && searchResults.hasMoreElements())
				{
					SearchResult searchResult=(SearchResult) searchResults.nextElement();					
					return extractUser(searchResult.getName());
				}
				searchResults.close();
				initialdircontext.close();
			}catch(AuthenticationException authenticationexception)
			{
				//VWSLog.dbg("Error in getting CommonName for  (" + user +  ") " + "Failed");			    
				return user;
			}
			catch(NamingException namingexception)
			{
				//VWSLog.dbg("Error in getting CommonName "+namingexception.getMessage());
				return user;
			}
			return user;

		}

	    /*Authentication check with User login Name.   Valli 22 march 2007*/
	    //Not in Use need to remove later
	    /*public boolean AuthenticateUserID(String user, String pass){
	    	Hashtable env = new Hashtable();
   	        String providerURL = "ldap://" + loginServer + ":" + port;
   	        env.put(Context.INITIAL_CONTEXT_FACTORY,ldapCTX);
   			env.put(Context.PROVIDER_URL,providerURL);
   			env.put(Context.SECURITY_AUTHENTICATION,securityAuthentication);
   			env.put(Context.SECURITY_PRINCIPAL, user+"@"+ldapDirName);
   			env.put(Context.SECURITY_CREDENTIALS,pass);
   	        InitialDirContext initialdircontext;
   	        try
   	        {
   	            initialdircontext = new InitialDirContext(env);
   	        }catch(AuthenticationException authenticationexception)
	        {
   	        	//VWSLog.dbg("AuthenticateUser (" + user +  ") " + "Failed");
	        	return false;
	        }
	        catch(NamingException namingexception)
	        {
	        	//VWSLog.dbg("Unable to get Initial LDAP Directory Context-> "+namingexception.getMessage());
	            return false;
	        }
	    	return true;
	    }*/

	    /*Authentication check with common name and context path.   Valli 22 march 2007*/
	    public boolean AuthenticateUser(String user, String pass, String context)
	    {
			VWSLog.dbg(" context "+context);
			/**
			 * CV10.1 Issue fix for multiple domain login.
			 * 12/6/2017
			 * Modified by :- B.Madhavan
			 */
			String curOuList="";/* = frameContextNameForADS(context);*/
			//VWSLog.dbg("AuthenticateUser method curOuList::: "+curOuList);
			if(user.contains("@")){
				String userSuffix = user.substring(user.indexOf("@") + 1, user.indexOf("."));
				if(userSuffix.equals("-"))
				{
					curOuList = frameContextNameForADS(context);
				} else
				{
					curOuList = frameContextNameForADS(context,userSuffix);
				}
			} else{
				curOuList = frameContextNameForADS(context);
			}
			try{
				String parentContext = curOuList;
				 /**
                 * CV10.1 Issue fix for multiple domain login.
                 * 12/6/2017
                 * Modified by :- B.Madhavan
                 */
				if(user.contains("@")){
					String userWithoutSuffix = user.substring(0,user.indexOf("@"));
					curOuList = "cn="+userWithoutSuffix+", "+curOuList;
				}else
				curOuList = "cn="+user+", "+curOuList;
				VWSLog.dbg("curOuList :::::::::"+curOuList);
				if(AuthenticateUserID(user, pass, curOuList)){
					return true;
				}else{
					String filter = "(&(objectClass=organizationalUnit))";
					//VWSLog.dbg(" parentContext "+parentContext);
					initialContext = getInitialContext();
					List OUsList=searchForOUs(initialContext,parentContext,filter);
					initialContext.close();
					for(int i = 0; i<OUsList.size(); i++){
						String subContext = OUsList.get(i).toString();
						//VWSLog.dbg("Sub OU:"+subContext);
						if(subContext!=null&& !subContext.equals("")){
							//subContext = "cn="+user+", "+subContext+", "+parentContext+", "+domainComponent;
							subContext = "cn="+user+", "+subContext+", "+parentContext;
							if(AuthenticateUserID(user, pass, subContext))
								return true;
						}
					}
				}
			}catch(Exception ex){
				//VWSLog.dbg("Error "+ex.getMessage());
				return false;
			}
			//VWSLog.dbg("AuthenticateUser (" + user + "." + context + ") " + "Failed");
			return false;
	    }
	    
	    public boolean AuthenticateUserNovell(String user, String pass, String context){
	    	VWSLog.dbg("Inside AuthenticateUserNovell method :: " + user);
    		boolean validUser = false;
    		context = frameContextNameForNovell(context);
	    	Vector userDisplayNames = searchForUserDn(initialContext, context, "(&(objectClass=user)(cn="+user+"))");
	    	if(userDisplayNames!=null && userDisplayNames.size()>0)
	    	{
	    		for(int i = 0; i<userDisplayNames.size(); i++){
	    			String fullContext = userDisplayNames.get(i).toString();
	    			VWSLog.dbg("fullContext :" + fullContext);
	    			if(AuthenticateUserID(user, pass, fullContext)){
	    				validUser = true;
	    				return true;
	    			}else{
	    				validUser = false;
	    				continue;
	    			}
	    		}
	    	}
	    	return validUser;
	    	
	    	/*String curOuList = frameContextNameForNovell(context);
	    	String parentContext = curOuList;
	    	//VWSLog.dbg(" parentContext "+parentContext);
	    	if(curOuList!=null && !curOuList.equals(""))
	    		curOuList = "cn="+user+", "+curOuList;
	    	else
	    		curOuList = "cn="+user+", "+domainComponent;
	    	//VWSLog.dbg(" curOuList "+curOuList);
	    	try{
	    		if(AuthenticateUserID(user, pass, curOuList)){
	    			return true;
	    		}else{
	    			String filter = "(&(objectClass=organizationalUnit))";
	    			//VWSLog.dbg(" parentContext "+parentContext);
	    			initialContext = getInitialContext();
	    			List OUsList=searchForOUs(initialContext,parentContext,filter);
	    			initialContext.close();
	    			for(int i = 0; i<OUsList.size(); i++){
	    				String subContext = OUsList.get(i).toString();
	    				//VWSLog.dbg("Sub OU:"+subContext);
	    				if(subContext!=null&& !subContext.equals("")){
	    					//subContext = "cn="+user+", "+subContext+", "+parentContext+", "+domainComponent;
	    					subContext = "cn="+user+", "+subContext+", "+parentContext;
	    					if(AuthenticateUserID(user, pass, subContext))
	    						return true;
	    				}
	    			}
	    		}
	    	}catch(Exception ex){
	    		
	    	}*/
	    	//return false;
	    }

	    private List searchForOUs(DirContext dirContext,String sContext,String filter)
	    {
	        LinkedList OUs = new LinkedList();
	        if (dirContext == null)          return OUs;
	        SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String returningAttributes[] = new String[0];
			controls.setReturningAttributes(returningAttributes);
	        try{
	        	//if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS))
	        		//sContext = sContext+", "+domainComponent;
	        	VWSLog.dbg("searchForOUs-> "+sContext);
	            NamingEnumeration searchResults = dirContext.search(sContext, filter, controls);
	            while (searchResults != null && searchResults.hasMore())
	            {
					//SearchResult searchResult=(SearchResult) searchResults.nextElement();
					String temp = extractOU(searchResults.next().toString());
					OUs.add(temp);
	            }
	            searchResults.close();
	            return OUs;
	        }catch (NamingException e){
	        	//VWSLog.dbg("JNDISearch failed ->"+e.getMessage());
	            return new LinkedList();
	        }
	    }


	    /*Authentication check with common name and context path.   Valli 22 march 2007*/
	    public boolean AuthenticateUserID(String user, String pass, String OUContext){
	    	 boolean isValidUser =false;
	    	 Hashtable env = new Hashtable();
		     //String providerURL = "ldap://" + loginServer + ":" + port + "/" + domainComponent;
			 /**CV10.2 - Condition added for LDAP Novel issue fix.Added by Vanitha.S**/
	    	 String domainName="";
	    	 String loginSrv="";
	    		if(OUContext.length()>0){
	    			if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
		    			String domainCoponent=OUContext.substring(OUContext.indexOf("DC"),OUContext.length());
		    			VWSLog.dbg("domainCoponent :"+domainCoponent);
		    			String[] splittedDC=domainCoponent.split(",");
		    			VWSLog.dbg("splittedDC :"+splittedDC);
		    			for(String dcval:splittedDC){
		    				dcval=dcval.substring(dcval.indexOf("=")+1,dcval.length());
	
		    				domainName=domainName+dcval+".";
		    			}
		    			domainName=domainName.substring(0,domainName.lastIndexOf("."));
		    			VWSLog.dbg("domainName :"+domainName);
	    			} else {
	    				String domainCoponent=OUContext.substring(OUContext.indexOf("o="),OUContext.length());
	    				VWSLog.dbg("domainCoponent :"+domainCoponent);
		    			String[] splittedDC=domainCoponent.split(",");
		    			VWSLog.dbg("splittedDC :"+splittedDC);
		    			for(String dcval:splittedDC){
		    				dcval=dcval.substring(dcval.indexOf("=")+1,dcval.length());
		    				VWSLog.dbg("dcval :"+dcval);
		    				domainName=domainName+dcval+".";
		    				VWSLog.dbg("domainName :"+domainName);
		    			}
		    			domainName=domainName.substring(0,domainName.lastIndexOf("."));
		    			VWSLog.dbg("domainName :"+domainName);
	    			}
	    		}
	    		if(domainName.length()>0){
	    			domainName = domainName.toLowerCase();
	    			loginSrv=(String) ldapDomainIPMap.get(domainName);
	    			VWSLog.dbg("loginSrv :::"+loginSrv);
	    		}
		     String providerURL = "ldap://" + loginSrv + ":" + port;
		     VWSLog.dbg("providerURL ::"+providerURL);
		     env.put(Context.INITIAL_CONTEXT_FACTORY,ldapCTX);
		     if(Integer.parseInt(port) == SSL_PORT)
	            	 env.put(Context.SECURITY_PROTOCOL, SECURITY_PROTOCOL_SSL);
			 env.put(Context.PROVIDER_URL,providerURL);
			 env.put(Context.SECURITY_AUTHENTICATION,securityAuthentication);
			 env.put(Context.SECURITY_PRINCIPAL, OUContext);
			 VWSLog.dbg("AuthenticateUser details : "+ env);
			 env.put(Context.SECURITY_CREDENTIALS,pass);
			 //subOuContext = "cn="+user+", "+subOuContext+", "+parentContext+", "+domainComponent;
			 //VWSLog.dbg(" providerURL "+providerURL);
			 VWSLog.dbg(" securityAuthentication : "+securityAuthentication);
			//VWSLog.dbg(" OUContext "+OUContext);	 		 
	 		 
	 		 
	 		 /***CV10.2 Socket closed exception issue fix. Added by Vanitha.S on 10October2019******/
	 		 //Before establishing the connection ldap connection timeout is happening. Bz of that socket closed exception is occuring 
	 		 // Specify time-out to be infinite time . make it like never expired. Have to close the connection in finally block.
	 		 env.put("com.sun.jndi.ldap.connect.timeout", "-1");
	 		 /*************************************************/
	 		 
	 		 InitialDirContext initialdircontext = null;
		     try
		     {
		            initialdircontext = new InitialDirContext(env);
		            /*if(initialdircontext!=null)
		            	initialdircontext.close();*/
		        	VWSLog.dbg("AuthenticateUser (" + user + ") " + "passed");
		        	isValidUser = true;
		     }catch(AuthenticationException authenticationexception)
		     {
		        	VWSLog.dbg("AuthenticateUser (" +  OUContext + ") " + "Failed");
		        	VWSLog.dbg("AuthenticationException : "+authenticationexception.getMessage());
		        	isValidUser = false;
		     }
		     catch(NamingException namingexception)
		     {
		            VWSLog.dbg("Unable to get Initial LDAP Directory Context-> "+namingexception.getMessage());
		            VWSLog.dbg("Unable to get Initial LDAP Directory Context-> "+namingexception);
		            isValidUser = false;
		     } finally {
		    	 	/***CV10.2 Socket closed exception issue fix. Added by Vanitha.S on 10October2019******/
		    	 	try {
		            	 initialdircontext.close();
		            } catch (Exception e) {

		            }
		    	 	/*************************************************/
		     }
		     return isValidUser;
	    }
	    
	    private String extractOU(String contextName){
			int index = contextName.indexOf(":");
			String name = "";
			if (index >0){
				StringTokenizer stringtokenizer = new StringTokenizer(contextName, ":");
				if(stringtokenizer.hasMoreTokens())
			    {
					name = stringtokenizer.nextToken();
				}
			}
			return name;
		}
	    
	    private String frameContextNameForNovell(String contextName){
	    	if(contextName!=null && !contextName.equals("")){
		    	if(contextName.indexOf(".")!=-1){
			    	String name = "";
			    	StringTokenizer stringtokenizer = new StringTokenizer(contextName, ".");
			    	int count = stringtokenizer.countTokens();
			    	for(int i = 0; i<count ; i++)
				    {
						String temp = stringtokenizer.nextToken();
						if(i == count-1)
							name = name+"o="+temp;
						else
							name = name+"ou="+temp +",";
					}
			    	contextName = name;
		    	}else{
		    		contextName = "o="+contextName;
		    	}
	    	}else
	    		contextName = "";
	    	return contextName;
	    }
	    
	    private String handlingSpecialCharacters(String name){
	    	
	    	StringBuffer sb = new StringBuffer();
	    	char[] fChars = name.toCharArray();
	    	for(int i=0;i<fChars.length;i++)
	    	{         
	    		//short sName = (short)fChars[i];
	    		//VWSLog.dbg( " sName "+sName + " "+fChars[i]);
	    		if( fChars[i] == '&' || fChars[i] == '(' || fChars[i] == ')' || fChars[i] == '/' ) 
	    			sb.append("\\"+fChars[i]);
	    		else if( fChars[i] == '\\') 
	    			sb.append("\\\\");
	    		else
	    			sb.append(fChars[i]);
	    	}       
	    	//VWSLog.dbg( " sName sb.toString():"+sb.toString());
	    	return (sb.toString());
	    }
	    
	    private String frameContextNameForADS(String contextName){
	    	VWSLog.dbg("ContextName in frameContextNameForADS: "+ contextName);
	    	if(contextName!=null && !contextName.equals("")){
		    	if(contextName.indexOf(".")!=-1){
			    	String name = "";
			    	StringTokenizer stringtokenizer = new StringTokenizer(contextName, ".");
			    	int count = stringtokenizer.countTokens();
			    	for(int i = 0; i<count ; i++)
				    {
						String tempName = stringtokenizer.nextToken();
						if(i==count-1)
							name = name+"OU = "+tempName;
						else
							name = name+"OU = "+tempName +",";
					}
			    	contextName = name;
		    	}else{
		    		if(contextName.equals("Users")||contextName.equals("Builtin")||contextName.equals("Computers"))
		    			contextName = "CN="+contextName;
		    		else
		    			contextName = "OU="+contextName;
		    	}
		    	contextName = contextName +", "+domainComponent;
		    	VWSLog.dbg("contextName inside frameContextNameForADS if :::::::::"+contextName);
	    	}else{
	    		contextName = domainComponent;
	    		VWSLog.dbg("contextName inside frameContextNameForADS else :::::::::"+contextName);
	    	}
	    	//VWSLog.dbg("result contextName inside frameContextNameForADS: "+contextName);
	    	return contextName;
	    }
	    
	    /**
	     * CV2019 code merge from CV10.2 line
	     * @param contextName
	     * @return
	     */
	    private String frameContextNameForOtherOUADS(String contextName){
	    	if(contextName!=null && !contextName.equals("")){
		    	if(contextName.indexOf(".")!=-1){
			    	String name = "";
			    	StringTokenizer stringtokenizer = new StringTokenizer(contextName, ".");
			    	int count = stringtokenizer.countTokens();
			    	for(int i = 0; i<count ; i++)
				    {
						String tempName = stringtokenizer.nextToken();
						if(i==count-1)
							name = name+"OU = "+tempName;
						else
							name = name+"OU = "+tempName +",";
					}
			    	contextName = name;
		    	}else{
		    		VWSLog.dbg("frameContextNameForADS Other OU else condition");
		    		if(contextName.equals("Users")||contextName.equals("Builtin")||contextName.equals("Computers"))
		    			contextName = "CN="+contextName;
		    		else
		    			contextName = "OU="+contextName;
		    	}
		    	contextName = domainComponent ;
	    		
		    }else{
	    		contextName = domainComponent;
	    	}
	    	VWSLog.dbg("contextName in frameContextNameForADS method: "+contextName);
	    	return contextName;
	    }
	    
	    
	    
	    /**
         * CV10.1 Issue fix for multiple domain login.
         * 12/6/2017
         * Modified by :- B.Madhavan
         */
	    private String frameContextNameForADS(String contextName,String userName){
	    	VWSLog.dbg("contextName in frameContextNameForADS:::::"+contextName);
	    	if(contextName!=null && !contextName.equals("")){
	    		try{
		    	if(contextName.indexOf(".")!=-1){
			    	String name = "";
			    	StringTokenizer stringtokenizer = new StringTokenizer(contextName, ".");
			    	int count = stringtokenizer.countTokens();
			    	for(int i = 0; i<count ; i++)
				    {
						String tempName = stringtokenizer.nextToken();
						VWSLog.dbg("tempName in frameContextNameForADS:::::"+tempName);
						if(i==count-1)
							name = name+"OU = "+tempName;
						else
							name = name+"OU = "+tempName +",";
					}
			    	contextName = name;
			    	VWSLog.dbg("contextName inside if block of frameContextNameForADS:::::"+contextName);
		    	}else{
		    		if(contextName.equals("Users")||contextName.equals("Builtin")||contextName.equals("Computers"))
		    			contextName = "CN="+contextName;
		    		else
		    			contextName = "OU="+contextName;
		    	}
		    	
		    	String dcUserName = domainComponent;
		    	VWSLog.dbg("32 domainComponent is in framecontext 2 is :::"+domainComponent);
		    	//domainComponent.substring(domainComponent.indexOf("=") + 1, domainComponent.indexOf(","));
		    	VWSLog.dbg("33 userName upn is ::::::::::"+userName);
		    	VWSLog.dbg("34 dcUserName ::::::::::"+dcUserName);
		    	// VWSLog.dbg("domainComponent inside if:::::::::::"+domainComponent);
		    	 if(userName.equalsIgnoreCase(dcUserName))
		            {
		                domainComponent = domainComponent;
		                
		                VWSLog.dbg("domainComponent inside if11:::::::::::"+domainComponent);
		            } else
		            {
		            	 Set<String> keys = ldapDomainIPMap.keySet();
		            	 String keyfromMap="";
		            	 if(ldapDomainIPMap.size() == 1){
		            		 VWSLog.dbg("Iniside single domain 1....");
		            		 if(keys!=null&keys.size()>0){
		            			 VWSLog.dbg("after geting keys in single domain 1....");
		            			 Iterator < String > setItrator = keys.iterator();
		            			 while (setItrator.hasNext()) {
		            				 keyfromMap=setItrator.next();
		            			 }//Commented for adminuser login issue fix
		            			 /*System.out.println("keyfromMap inside single domain1::::"+keyfromMap);
		            			 StringTokenizer st = new StringTokenizer(keyfromMap, ".");
		            			 domainComponent="DC"+" "+"="+" "+st.nextToken().toString()+", "+"DC"+" "+"="+" "+st.nextToken().toString();
		            			 VWSLog.dbg("domainComponent inside single esle 1:::"+domainComponent);*/
		            		 }
		            	 }else if(ldapDomainIPMap.size() > 1){
		            		 VWSLog.dbg("Iniside multi domain 1....");
		            		 Set<String> keys1 = ldapDomainIPMap.keySet();
		            		 VWSLog.dbg("after geting keys in multidomain 1....");
		            		 if(keys1!=null&keys1.size()>0){
		            			 VWSLog.dbg("Inside if of key validation in multidomain 1....");
		            			 Iterator < String > setItrator1 = keys1.iterator();
		            			 VWSLog.dbg("after key iterator in multidomain 1....");
		            			 while (setItrator1.hasNext()) {
		            				 VWSLog.dbg("Inisde while of multidomain 1....");
		            				 keyfromMap=setItrator1.next();
		            				 VWSLog.dbg("Key map from multidomain 1...."+keyfromMap);
		            				 StringTokenizer st1 = new StringTokenizer(keyfromMap, ".");
		            				 String domainName=st1.nextToken();
		            				 VWSLog.dbg("domainName in multidomain 1....."+domainName);
		            				 String domainSuffix=st1.nextToken();
		            				 VWSLog.dbg("domainSuffix in multidomain 1....."+domainSuffix);
		            				 if( domainName.toString().equalsIgnoreCase(userName)){
		            					 String domainCompontent1="";
		            					 VWSLog.dbg("46 inside if2 of  multidomain 1.....");
		            					 String loginServer1 = (String) ldapDomainIPMap.get(keyfromMap);
		            					 VWSLog.dbg("loginServer1 in multidomain 1::::"+loginServer1);
		            					 loginServer = loginServer1;
										 /**CV2019 code merge from CV10.2 line. Merged by Vanitha.S**/
		            					 VWSLog.dbg("48 loginServer in multidomain 1::::"+loginServer);
		            					 // domainComponent="DC"+" "+"="+" "+domainName.toString()+", "+"DC"+" "+"="+" "+domainSuffix.toString();
		            					 VWSLog.dbg("49 st1.countTokens() :::"+st1.countTokens());
		            					 if(st1.countTokens()>0){
		            						 while(st1.hasMoreTokens()){
		            							 domainCompontent1 = domainCompontent1+"DC = "+st1.nextToken().toString()+", ";
		            						 }
		            						 VWSLog.dbg("50 domainCompontent1 is ::"+domainCompontent1);
		            						 //domainCompontent1 = domainCompontent1.substring(0,domainCompontent1.length()-2);
		            						 VWSLog.dbg("51 domainComponent in else1  ::"+this.domainComponent);
		            						 domainComponent=domainCompontent1;
			         		        	}else{
			         		        		domainComponent = ldapDirName;
			         		        		VWSLog.dbg("52 Inside else2 of ldapDirName::::"+ldapDirName);
			         		        	}
		            					 VWSLog.dbg("53 domainComponent in multidomain 1..."+domainComponent);
										 /**End of CV10.2 code merges***/
		            				 }
		            			 }

		            		 }
		            	 }
		               
		            }
		    	
		    	
		    	contextName = contextName +", "+domainComponent;
		    	VWSLog.dbg("contextName in frameContextNameForADS::::"+contextName);
		    	//end of line
	    	}catch(Exception e){
	    		VWSLog.dbg("Exception is ::"+e.getMessage());
	    	}
	    	}else{

	    		String s4 = domainComponent.substring(domainComponent.indexOf("=") + 1, domainComponent.indexOf(",") - 1);
	    		if(userName.equals(s4))
	    		{
	    			domainComponent = domainComponent;
	    		} else
	    		{
	    			Set<String> keys = ldapDomainIPMap.keySet();
	    			String keyfromMap="";
	    			if(ldapDomainIPMap.size() == 1){
	            		 VWSLog.dbg("Iniside single domain 2....");
	            		 if(keys!=null&keys.size()>0){
	            			 VWSLog.dbg("key validation in single 2....");
	            			 Iterator < String > setItrator = keys.iterator();
	            			 while (setItrator.hasNext()) {
	            				 keyfromMap=setItrator.next();
	            			 }
	            			 System.out.println("keyfromMap in single domain 2::::"+keyfromMap);
	            			 StringTokenizer st = new StringTokenizer(keyfromMap, ".");
	            			 domainComponent="DC"+" "+"="+" "+st.nextToken().toString()+", "+"DC"+" "+"="+" "+st.nextToken().toString();
	            			 VWSLog.dbg("domainComponent inside single domain 2:::"+domainComponent);
	            		 }
	            	 }else if(ldapDomainIPMap.size() > 1){
	            		 VWSLog.dbg("Iniside multi domain 2....");
	            		 Set<String> keys1 = ldapDomainIPMap.keySet();
	            		 if(keys1!=null&keys1.size()>0){
	            			 VWSLog.dbg("key validation in multidomain 2....");
	            			 Iterator < String > setItrator1 = keys1.iterator();
	            			 VWSLog.dbg("after key iterator in multidomain 2....");
	            			 while (setItrator1.hasNext()) {
	            				 keyfromMap=setItrator1.next();
	            				 VWSLog.dbg("keyfromMap in multidomain 2...."+keyfromMap);
	            				 StringTokenizer st1 = new StringTokenizer(keyfromMap, ".");
	            				 String domainName=st1.nextToken();
	            				 VWSLog.dbg("domainName in multidomain 2...."+domainName);
	            				 String domainSuffix=st1.nextToken();
	            				 VWSLog.dbg("domainSuffix in multidomain 2...."+domainSuffix);
	            				 if( domainName.equalsIgnoreCase(userName)){
	            					 //String loginServer1 = (String) ldapDomainIPMap.get(keyfromMap);
	            					 //loginServer = loginServer1;
	            					 domainComponent="DC"+" "+"="+" "+domainName+", "+"DC"+" "+"="+" "+domainSuffix;
	            					 VWSLog.dbg("domainComponent in  multidomain2..."+domainComponent);
	            				 }
	            			 }

	            		 }
	            	 }
	               
	            
	    		}
	    		contextName = domainComponent;
	    	}
	    	VWSLog.dbg("result contextName inside frameContextNameForADS"+contextName);
	    	return contextName;
	    }
	    /**
	     * A sample UnsolicitedNotificationListener.
	     */
	    static class UnsolListener implements UnsolicitedNotificationListener {
		public void notificationReceived(UnsolicitedNotificationEvent evt) {
		    System.out.println("received: " + evt);
		}

		public void namingExceptionThrown(NamingExceptionEvent evt) {
		    System.out.println(">>> UnsolListener got an exception");
		    evt.getException().printStackTrace();
		}
	    }
	    
	    /***
	     * CV10.2 - Added to get user full information from directory
	     */
	    public String getUserFullInfo(String users) {
	    	VWSLog.dbg("Inside getUserFullInfo method...");
	    	String mailIdWithSamName="";
	    	try {
	    		Vector userVector = new Vector();
	    		DirContext dircontext = getInitialContext();
	    		VWSLog.dbg("dircontext inside getUserFullInfo method: "+ dircontext);
	    		if (dircontext == null)
	    			return "Err";
	    		String userName = users.trim();
	    		String context = "";
	    		String userDn="";
	    		String userMail="";
	    		String samName="";
	    		String userPrincipalName="";
	    		VWSLog.dbg("getUserFullInfo userName: "+userName);
	    		SearchControls controls = new SearchControls();
	    		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    		controls.setReturningObjFlag(true);
	    		for (int i = 0; i < OUs.length; i++) {
	    			VWSLog.dbg("Checking for OU inside getUserFullInfo method: "+OUs[i]);
	    			context = frameContextNameForADS(OUs[i]);
	    			//NamingEnumeration searchResults = dircontext.search(context,
	    					//"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(userPrincipalName=" + userName +/* "@" + ldapDirName +*/ ")))",
	    			
	    			try {
	    			NamingEnumeration searchResults = dircontext.search(context,	    			
	    					//"(&(objectCategory=Person)(|(sAMAccountName=*" + userName + "*)(userPrincipalName=*" + userName + "*)))",
	    					//"(&(objectCategory=Person)(|(sAMAccountName=" + userName + ")(userPrincipalName=" + userName + ")))",
	    					"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(name=" + userName + ")(userPrincipalName=" + userName + ")))",
	    					controls);
	    			VWSLog.dbg("user search info from getUserFullInfo method : "+searchResults);
	    			if (searchResults != null && searchResults.hasMoreElements()) {
	    				SearchResult searchResult = (SearchResult) searchResults.nextElement();
	    				{
	    					Attributes attrs = searchResult.getAttributes();
	    					NamingEnumeration allAttributes = attrs.getAll();
	    					while (allAttributes.hasMoreElements()) {
	    						Attribute attr = (Attribute) allAttributes.nextElement();
	    						if(attr.getID().equalsIgnoreCase("distinguishedName")){
	    							String dn=attr.toString();
	    							String dnArray[]=dn.split(":");
	    							if(dnArray[1].length()>0){
	    								userDn=dnArray[1];
	    								VWSLog.dbg("4 userDn in getUserFullInfo: "+userDn);
	    							}
	    						}
	    						if (attr.getID().equalsIgnoreCase("userPrincipalName")){
	    							String userWithUpn=attr.toString();
	    							String userWithSuffix[]=userWithUpn.split(":");
	    							if((userWithSuffix!=null)&&(userWithSuffix.length>1)){
	    								userPrincipalName=userWithSuffix[1].trim();
	    							}
	    						}
	    						if (attr.getID().equalsIgnoreCase("mail")){
	    							String mailId=attr.toString();
	    							String userMailIds[]=mailId.split(":");
	    							VWSLog.dbg("3 userMailIds in getUserFullInfo: "+userMailIds[1]);
	    							if(userMailIds[1].length()>0){
	    								userMail=userMailIds[1];
	    							}
	    							//System.out.println("mail Id is :::"+mailId);
	    						}
	    						if (attr.getID().equalsIgnoreCase("sAMAccountName")){
	    							String samNameAttr=attr.toString();
	    							String SamName1[]=samNameAttr.split(":");
	    							VWSLog.dbg("4 SamName in getUserFullInfo: "+SamName1[1]);
	    							if((SamName1!=null)&&(SamName1.length>1)){
	    								samName=SamName1[1].trim();
	    							}
	    						}
	    						if(userMail.equalsIgnoreCase("")){
	    							userMail="-";
	    						}
	    						if(samName.equalsIgnoreCase("")){
	    							samName="-";
	    						}
	    						mailIdWithSamName=userPrincipalName+Util.SepChar+userDn+Util.SepChar+samName+Util.SepChar+userMail+Util.SepChar;
	    						
	    					}
	    					VWSLog.dbg("Inside getFullUserInfo mailIdWithSamName value for userName11: " + userName + " mailIdWithSamName: " + mailIdWithSamName);
	    					searchResults.close();
	    				}
	    				if (mailIdWithSamName.length()>0) 
	    					break;
	    			 } 
	    			} catch (Exception ex) {
	    	    		//System.out.println("Exception while getting user fullInfo "+ex.getMessage());
	    	    		VWSLog.err("Exception while getting searchResults  for user "+users+" "+ex.getMessage());
	    			}
	    		}
	    			
	    			
	    		dircontext.close();
	    		VWSLog.dbg("fullUserInfo of user: "+userName+ " mailIdWithSamName: " + mailIdWithSamName);
	    		return mailIdWithSamName;
	    	} catch (Exception ex) {
	    		//System.out.println("Exception while getting user fullInfo "+ex.getMessage());
	    		VWSLog.err("Exception while getting userfullInfo  for user "+users+" "+ex.getMessage());
	    		return "";
	    	}finally{
	    		return mailIdWithSamName;
	    	}
	    }
	    public String getUserFullInfoOtherOU(String users) {
	    	String mailIdWithSamName="";
	    	try {
	    		Vector userVector = new Vector();
	    		DirContext dircontext = getInitialContext();
	    		if (dircontext == null)
	    			return "Err";
	    		String userName = users.trim();
	    		String context = "";
	    		String userDn="";
	    		String userMail="";
	    		String samName="";
	    		String userPrincipalName="";
	    		VWSLog.dbg("getUserFullInfo OtherOU userName: "+userName);
	    		SearchControls controls = new SearchControls();
	    		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    		controls.setReturningObjFlag(true);
	    		for (int i = 0; i < OUs.length; i++) {
	    			context = frameContextNameForOtherOUADS(OUs[i]);
	    			//NamingEnumeration searchResults = dircontext.search(context,
	    					//"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(userPrincipalName=" + userName +/* "@" + ldapDirName +*/ ")))",
	    			NamingEnumeration searchResults = dircontext.search(context,	    			
	    					"(&(objectCategory=Person)(|(sAMAccountName=" + userName + ")(userPrincipalName=" + userName + ")))",
	    					controls);
	    			VWSLog.dbg("user search info from getUserFullInfoOtherOU method : "+searchResults);
	    			if (searchResults != null && searchResults.hasMoreElements()) {
	    				SearchResult searchResult = (SearchResult) searchResults.nextElement();
	    				{
	    					Attributes attrs = searchResult.getAttributes();
	    					NamingEnumeration allAttributes = attrs.getAll();
	    					while (allAttributes.hasMoreElements()) {
	    						Attribute attr = (Attribute) allAttributes.nextElement();
	    						if(attr.getID().equalsIgnoreCase("distinguishedName")){
	    							String dn=attr.toString();
	    							String dnArray[]=dn.split(":");
	    							if(dnArray[1].length()>0){
	    								userDn=dnArray[1];
	    								VWSLog.dbg("4 userDn in getUserFullInfo: "+userDn);
	    							}
	    						}

	    						if (attr.getID().equalsIgnoreCase("userPrincipalName")){
	    							String userWithUpn=attr.toString();
	    							String userWithSuffix[]=userWithUpn.split(":");
	    							if((userWithSuffix!=null)&&(userWithSuffix.length>1)){
	    								userPrincipalName=userWithSuffix[1].trim();
	    							}
	    						}
	    						if (attr.getID().equalsIgnoreCase("mail")){
	    							String mailId=attr.toString();
	    							String userMailIds[]=mailId.split(":");
	    							VWSLog.dbg("3 userMailIds in getUserFullInfo: "+userMailIds[1]);
	    							if(userMailIds[1].length()>0){
	    								userMail=userMailIds[1];
	    							}
	    							//System.out.println("mail Id is :::"+mailId);
	    						}
	    						if (attr.getID().equalsIgnoreCase("sAMAccountName")){
	    							String samNameAttr=attr.toString();
	    							String SamName1[]=samNameAttr.split(":");
	    							VWSLog.dbg("4 SamName in getUserFullInfo: "+SamName1[1]);
	    							if((SamName1!=null)&&(SamName1.length>1)){
	    								samName=SamName1[1].trim();
	    							}
	    						}
	    						if(userMail.equalsIgnoreCase("")){
	    							userMail="-";
	    						}
	    						if(samName.equalsIgnoreCase("")){
	    							samName="-";
	    						}
	    						mailIdWithSamName=userPrincipalName+Util.SepChar+userDn+Util.SepChar+samName+Util.SepChar+userMail+Util.SepChar;
	    						
	    					}
	    					searchResults.close();
	    				}
	    			}
	    		}
	    		
	    		dircontext.close();
	    		VWSLog.dbg("fullUserInfo of user: "+userName+ " mailIdWithSamName: " + mailIdWithSamName);
	    		return mailIdWithSamName;
	    	} catch (Exception ex) {
	    		//System.out.println("Exception while getting getUserFullInfoOtherOU "+ex.getMessage());
	    		VWSLog.err("Exception while getting userfullInfo  for user "+users+" "+ex.getMessage());
	    		return "";
	    	}finally{
	    		return mailIdWithSamName;
	    	}
	    }
	    /**
	     * CV10.1 Enahancement :- Getting the sam account name for login.
	     * Modified by :- Sudhansu    
	     */
	    public String getSamAttribute(String users) {
	    	try {
	    		String samName ="";
	    		SearchControls constraints = new SearchControls();
	    		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    		String[] attrIDs = { "sAMAccountName"};
	    		constraints.setReturningAttributes(attrIDs);
	    		DirContext dircontext = getInitialContext();
	    		if(dircontext == null)	
	    			return samName;
	    		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
	    			//NamingEnumeration answer = dircontext.search(domainComponent, "userPrincipalName="+ users,constraints);
	    			//NamingEnumeration answer = dircontext.search(domainComponent, "sAMAccountName="+ users,constraints);
	    			//VWSLog.dbg("domainComponent ss:::::"+users);
	    			String filter = "(&(objectClass=User)(|(sAMAccountName=" + users + ")(name=" + users + ")(userPrincipalName=" + users + ")))";
	    			//String filter = "(|(sAMAccountName=" + users + ")(name=" + users + ")(userPrincipalName=" + users + "))";
	    			//NamingEnumeration answer = dircontext.search(domainComponent, "userPrincipalName="+ users,constraints);
	    			NamingEnumeration answer = dircontext.search(domainComponent, filter,constraints);

	    			if (answer.hasMore()) {
	    				Attributes attrs = ((SearchResult) answer.next()).getAttributes();
	    				samName=attrs.get("sAMAccountName").toString();
	    				VWSLog.dbg("samName ..................................."+ samName);
	    				//VWSLog.dbg("distinguishedName sssssssssss"+ attrs.get("distinguishedName"));
	    			}
	    			dircontext.close();	
	    		}else{
	    			throw new Exception("Invalid User");
	    		}
	    		return samName;
	    	}catch (Exception ex) {
	    		VWSLog.add("Inside catch block of getSamAttribute:::"+ex.getMessage());
	            ex.printStackTrace();
	            return "";
	        }
			
	        
	    }
	    
	    /**
	     * CV10.1 Enahancement :- Getting the distinguishedName name .
	     * Modified by :- Sudhansu    
	     */
	    public String getUserBasicAttributes(String users) {
	    	try {
	    		String userDN ="";
	    		SearchControls constraints = new SearchControls();
	    		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    		String[] attrIDs = { "distinguishedName"};
	    		constraints.setReturningAttributes(attrIDs);
	    		DirContext dircontext = getInitialContext();
	    		if(dircontext == null)	
	    			return userDN;
	    		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
	    			String filter = "(&(objectClass=User)(|(sAMAccountName=" + users + ")(name=" + users + ")(userPrincipalName=" + users + ")))";
	    			//NamingEnumeration answer = dircontext.search(domainComponent, "userPrincipalName="+ users,constraints);
	    			NamingEnumeration answer = dircontext.search(domainComponent, filter,constraints);
	    			
	    			//VWSLog.dbg("getUserBasicAttributes users is "+users);
	    			if (answer.hasMore()) {
	    				Attributes attrs = ((SearchResult) answer.next()).getAttributes();
	    				VWSLog.dbg("distinguishedName "+ attrs.get("distinguishedName"));
	    				userDN=attrs.get("distinguishedName").toString();
	    				VWSLog.dbg("getUserBasicAttributes distinguishedName "+ attrs.get("distinguishedName"));
	    			}
	    			dircontext.close();	
	    		}else{
	    			throw new Exception("Invalid User");
	    		}
	    		return userDN;
	    	}catch (Exception ex) {
	    		VWSLog.add("Inside catch block of db:::"+ex.getMessage());
	            ex.printStackTrace();
	            return "";
	        }
			
	        
	    }
	    
	    /**
	     * CV10.2 - Added to get user mail id from directory
	     */
	    public String getUserDNMailAttributes(String userName) {
	    	String userMailIds = "";
	    	try{
	    		VWSLog.dbg("ldapDirName in getUserMailIds :"+ldapDirName);
	    		
	    		DirContext dircontext = getInitialContext();
	    		if(dircontext == null)	return userMailIds;
    			String context = "";		                
    			
    			if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
    				userMailIds = searchForUserMails(dircontext,context,"(&(objectClass=user)(cn="+userName+"))");
    			else{ // For LDAP ADS CV10.1 Issue fix to pull other ou mailid's modified by B.Madhavan 26/6/2017..
    				VWSLog.dbg("user name before getting user dn ......"+userName);
    				String userDN=getUserBasicAttributes(userName+"@"+ldapDirName);
    				VWSLog.dbg("usertDN inside getUserMailIds....."+userDN);
    				if(userDN.length()>0){
    					userMailIds = searchForUserMails(dircontext,userDN,"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(name=" + userName + ")(userPrincipalName=" + userName+"@"+ldapDirName + ")))");

    				}else{				
    					userMailIds = searchForUserMails(dircontext,context,"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(name=" + userName + ")(userPrincipalName=" + userName+"@"+ldapDirName + ")))");
    				}
    			}//End of fix.

	    		dircontext.close();		        
	    		return userMailIds;
	    	}catch(Exception ex){
	    		message = resourceManager.getString("LDAPMSG.ERRORMailIds")+ex.getMessage();
	    		VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    		VWSLog.err("Error in getting UserMailIds "+ex.getMessage());
	    		return userMailIds;
	    	}



	    }
	    
	    /**CV2019 code merges from CV10.2 line*****/
	    public String getUserAttributes(String users, String attr) {
	    	try {
	    		String userDN ="";
	    		SearchControls constraints = new SearchControls();
	    		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    		String[] attrIDs = { "mail"};
	    		constraints.setReturningAttributes(attrIDs);
	    		DirContext dircontext = getInitialContext();
	    		if(dircontext == null)	
	    			return userDN;
	    		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
	    			VWSLog.dbg("domainComponent from getUserAttributes: "+ domainComponent);
	    			//NamingEnumeration answer = dircontext.search(domainComponent, "userPrincipalName="+ users,constraints);
	    			String filter = "(&(objectClass=User)(|(sAMAccountName=" + users + ")(name=" + users + ")(userPrincipalName=" + users + ")))";
	    			NamingEnumeration answer = dircontext.search(domainComponent, filter,constraints);

	    			VWSLog.dbg("getUserBasicAttributes users is "+users);
	    			if (answer.hasMore()) {
	    				Attributes attrs = ((SearchResult) answer.next()).getAttributes();
    					userDN=attrs.get("mail").toString();
	    					
	    			}
	    			dircontext.close();	
	    		}else{
	    			throw new Exception("Invalid User");
	    		}
	    		return userDN;
	    	}catch (Exception ex) {
	    		VWSLog.add("Inside catch block of getUserAttributes of email :::"+ex.getMessage());
	            ex.printStackTrace();
	            return "";
	        }
			
	        
	    }


	    
	    public Vector getUsersUPN(String[] OUs1)
	    {
	    	VWSLog.dbg("Getting UserUPN11111..... "+OUs1.length);
	    	try{
	    		Vector userVector = new Vector();
	    		DirContext dircontext = getInitialContext();
	    		if(dircontext == null)	return userVector;
	    		List list = new LinkedList();
	    		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
	    			for(int i=0; i < OUs1.length; i++)
	    			{
	    				VWSLog.dbg("searchForNamesUPN inside getUsersUPN:::::"+"dircontext::"+dircontext+"OUs[i]"+OUs[i]);
	    				//Objectclass=user added in search filter to select only the type as user from ldap issue fix.
	    				list = searchForNamesUPN(dircontext, frameContextNameForADS(OUs1[i]), "(&(objectCategory=Person)(objectClass=user))");
	    				VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
	    				VWSLog.dbg("List value in getUsersUPn111::"+list);
	    				for(int j = 0; j < list.size(); j++)
	    				{
	    					userVector.add(list.get(j));
	    					//VWSLog.dbg("UserUPN -> "+list.get(j));
	    				}
	    			}

	    		}
	    		dircontext.close();
	    		VWSLog.dbg("getUsersUPN before returning the vector value1111::"+userVector);
	    		return userVector;
	    	}catch(Exception ex){
	    		message = resourceManager.getString("LDAPMSG.ERRORUsers")+ex.getMessage();
	    		VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    		VWSLog.err("Error in getting users inside catch "+ex.getMessage());
	    		return new Vector();
	    	}
	    }
	    
	    
	   
	    public static void main(String[] args) {
		String[] OU = {"Bangalore"};
	    	//String[] OU = {"userList"};
		//Directory theInstance = new LDAP("vwdev.com", "10.4.10.120", "administrator", "vw", "simple", OU, "UserList", "389","LDAP(ADS)", "" );
		//Directory theInstance = new LDAP("vwdev2.com", "10.4.10.75", "administrator", "Syntax@#12", "simple", OU, "Bangalore", "389","LDAP(ADS)", "" );
		Directory theInstance = new LDAP("vwdev1.com", "10.4.10.76", "administrator", "Syntax@#12", "simple", OU, "Bangalore", "389","LDAP(ADS)", "" );
		//Directory theInstance = new LDAP("vw.dev3.com", "10.4.10.127", "administrator", "Syntax@521", "simple", OU, "Bangalore", "389","LDAP(ADS)", "" );
		//List totalList=theInstance.getUsersInGroup_New("CVNamed",1);
		
		//List totalList=theInstance.getUsersInGroup_New("CVNamed",1);
		//System.out.println("get users in group is :"+theInstance.getUsersInGroup("vwadmins"));
		System.out.println("totalList ::"+theInstance.getUserFullInfo("vwadmin"));
		//Vector groups=theInstance.getGroups();
		//System.out.println("groups :::"+groups);
		//Vector userList = theInstance.getUsers();
		//Vector mailList = theInstance.getUserMailIds(userList);
		//System.out.println("userList " + userList);
		//System.out.println("mail List " + mailList);
		// System.out.println("mail List " +
		// theInstance.getCommonName("vv", ""));
		// String data = theInstance.getCommonName("administrator",
		// "admin123");
		// System.out.println("Data -> " + data);

	    }
	    public String getLdapDirName() {
			return ldapDirName;
		}

		public void setLdapDirName(String ldapDirName) {
			this.ldapDirName = ldapDirName;
		}
		@Override
		public String AuthenticateUserLastName(String lastName) {
			// TODO Auto-generated method stub
			//VWSLog.dbg("last name in AuthenticateUserLastname method:::" + lastName);
			this.userLastName = lastName;
			return lastName;
		}
		public String AuthenticateUserDN(String ldapUserDN) {
			// TODO Auto-generated method stub
			//VWSLog.dbg("last name in AuthenticateUserLastname method:::" + lastName);
			this.ldapuserDN = ldapUserDN;
			return ldapuserDN;
		}
		@Override
		public Vector getUserWithUPN() {
			// TODO Auto-generated method stub
			return getUsersUPN();			
			}
	public String  getScheme(){
		return   this.schemeType;		
		
	}

	@Override
	public Vector getUserWithUPN(String[] ouList1) {
		// TODO Auto-generated method stub
		return getUsersUPN(ouList1);			
		}

	@Override
	public String getuserPrincipalName(String userName) {
		try {
    		String uPrincipalName ="";
    		SearchControls constraints = new SearchControls();
    		constraints.setSearchScope(SearchControls.SUBTREE_SCOPE);
    		String[] attrIDs = { "userPrincipalName"};
    		constraints.setReturningAttributes(attrIDs);
    		DirContext dircontext = getInitialContext();
    		
    		if(dircontext == null)	
    			return uPrincipalName;
    		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
    			//NamingEnumeration answer = dircontext.search(domainComponent, "userPrincipalName="+ users,constraints);
    			String filter = "(&(objectClass=User)(|(sAMAccountName=" + userName + ")(name=" + userName + ")))";
    			NamingEnumeration answer = dircontext.search(domainComponent, filter,constraints);

    			if (answer.hasMore()) {
    				Attributes attrs = ((SearchResult) answer.next()).getAttributes();
    				uPrincipalName=attrs.get("userPrincipalName").toString();
    				VWSLog.dbg("uPrincipalName ..................................."+ uPrincipalName);
    			}
    			dircontext.close();	
    		}else{
    			throw new Exception("Invalid User");
    		}
    		return uPrincipalName;
    	}catch (Exception ex) {
    		VWSLog.add("Inside catch block of userPrincipalName:::"+ex.getMessage());
            ex.printStackTrace();
            return "";
        }
	}
	public String getDirName() {
		return this.ldapDirName ;
	}
	
	/*
    * Return type linked list is giving problem when data is having &. So changed into vector
    * @see com.computhink.vws.directory.Directory#getGroups()
    */
	public HashMap<String, String> getRegisteredGroups(HashMap<String, String> groupsList)
    {
		VWSLog.dbg("Getting Registered Groups");
    	try{
    		String groupName = null;
    		HashMap<String, String> groupVector = new HashMap<String, String>();
	        DirContext dircontext = getInitialContext();
	        if(dircontext == null)	return groupVector;
	        List list = new LinkedList();
	        VWSLog.dbg("OUS........."+OUs.toString());
	        //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
        	for(int i=0; i < OUs.length; i++)
            {
        		String sContext = "";
        		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS))
        			sContext = frameContextNameForADS(OUs[i]);
            	else if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
            		sContext = frameContextNameForNovell(OUs[i]);
        		//groupNameRegistry=;
        		VWSLog.dbg("sContext........."+sContext);
        		
				/**
				 * CV2019 issue fix - Commented on 12/12/2019 by Vanitha.S
				 * Following codes are commented bz if the Named desktop, Named online and
				 * Professional Named server settings are having the same group name then always
				 * we are taking the NamedGroupDesktop license count. Have to store the groups with key(Which group it belongs to).
				 ******/
        		/*for (int groupIndex = 0; groupIndex < groupsList.size(); groupIndex ++) {
        			list = searchForRegisteredGroups(dircontext, sContext, "(&(objectClass=group)(CN="+groupsList.get(groupIndex)+"))");
	        		VWSLog.dbg("Groups list in else ::: "+list);
	        		for(int j = 0; j < list.size(); j++)
	    	        {
	    	        	groupVector.put(list.get(j));
	    	        }
	            }*/
        		
        		for (String key : groupsList.keySet()) {
        		    groupName = groupsList.get(key);
        		    VWSLog.dbg("Key = " + key + ", Value = " + groupName);
        		    list = searchForRegisteredGroups(dircontext, sContext, "(&(objectClass=group)(CN="+groupName+"))");
	        		VWSLog.dbg("Groups list in else ::: "+list);
	        		if(list != null && list.size() > 0)
	    	        {
	    	        	groupVector.put(key, groupName);
	    	        }
	        		list.clear();
        		}
        	}
        		
        	dircontext.close();
        	return groupVector;
    	}catch(Exception ex){
    		VWSLog.add("Error in getting groups "+ex.getMessage());
    		return new HashMap<String, String>();
    	}
    }
	
	//This method is duplicate of searchForRegisteredGroups method.
    //The purpose of adding this seperate method is to get only registered groups from the directory
    //searchForRegisteredGroups method will return all the registered and nonreg groups from the directory
    private List searchForRegisteredGroups(DirContext dirContext,String context,String filter)
    {
    	VWSLog.dbg("Inside searchForRegisteredGroups method::::");
    	LinkedList names = new LinkedList();
    	SearchResult searchResult= null;
		if (dirContext == null)
		{
			return names;
		}
		SearchControls controls = new SearchControls();
		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String returningAttributes[] = new String[0];
		controls.setReturningAttributes(returningAttributes);
		try
		{
			VWSLog.dbg("Inside SearchForName Using context -> "+context+" filter -> "+filter+" controls -> "+controls);				
			String dirGroupName = null;
			context = getJNDICompositeName(context);
			NamingEnumeration searchResults = dirContext.search(context, filter, controls);
			VWSLog.dbg("Start of examining the search result from directory context search ");
			VWSLog.dbg("search results "+searchResults);
			while (searchResults != null && searchResults.hasMoreElements())
			{
				searchResult=(SearchResult) searchResults.nextElement();
				VWSLog.dbg("Group Name from LDAP search :"+searchResult.getName().toString());
				if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)) {
					dirGroupName = getUserPrincipalName(searchResult);
					names.add(dirGroupName);
				} else {
					dirGroupName = extractUser(searchResult.getName());
					names.add(dirGroupName);
				}
			}
			searchResults.close();
			VWSLog.dbg("End of examining the search result from directory context search and End of searchForRegisteredGroups from LDAP.java");
			VWSLog.dbg("searchForNames names:: " + names);
			return names;
		}
		catch (NamingException e)
		{
			System.out.println("JNDISearch failed ->"+e.getMessage());
			VWSLog.dbg("Exception in End of searchForRegisteredGroups from LDAP.java");
			return names;
		}
    }
    /**
     * This method is used to fetch group/users list based on filter 
     * @param dirName
     * @param loginServer
     * @param filter
     * @return
     */
	public Vector getAttributeList(String dirName, String loginServer, String filter) {
		VWSLog.dbg("getAttributeList filter search......."+filter);
		Vector attributeList = new Vector();
		try {
			DirContext dircontext = getInitialContext1(dirName, loginServer);
			VWSLog.dbg("getGroups1New sContext ::: " + dircontext);
			if (dircontext == null)
				return attributeList;
			List list = new LinkedList();
			VWSLog.dbg("search filter : " + filter);
			VWSLog.dbg("getGroups1New sContext ::: " + dircontext);
			String sContext = "";
			for (int i = 0; i < OUs.length; i++) {						
				if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS))
					sContext = frameContextNameForADS(OUs[i]);
				else if (schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
					sContext = frameContextNameForNovell(OUs[i]);
				list = searchForNames(dircontext, sContext, filter);
				VWSLog.dbg("group list in else ::: " + list);
				if (list.size() > 0)
					attributeList.addAll(list);
			}
		
			dircontext.close();
			return attributeList;
		} catch (Exception ex) {
			VWSLog.dbg("Error in getting groups " + ex.getMessage());
			return attributeList;
		}
	}
	
	private String frameContextName(String contextName){
    	if(contextName!=null && !contextName.equals("")){
    		if (contextName.indexOf(".") != -1) {
		    	String name = "";
		    	StringTokenizer stringtokenizer = new StringTokenizer(contextName, ".");
		    	int count = stringtokenizer.countTokens();
		    	for(int i = 0; i<count ; i++) {
					String tempName = stringtokenizer.nextToken();
					if (i == count - 1)
						name = name+"OU="+tempName;
					else
						name = name+"OU="+tempName +",";
				}
		    	contextName = name;
	    	}
	    }
    	return contextName;
    }
    /**
     * CV10.1 Enhancement Method added to get Users in Group
     */
    /*public List getUsersInGroup_New(String groupName, int flag)
    {
	try{
		VWSLog.dbg("Inside getUsersInGroup_New method::::");
	    DirContext initialContext = getInitialContext();
	    if (initialContext == null) return new LinkedList();
	    groupName = handlingSpecialCharacters(groupName);
	    String filter = "(&(CN=" + groupName + ")(ObjectClass=Group))";		        
	    List userGroupList=new LinkedList();
	    //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
	    VWSLog.dbg("Examining the search results for groupName "+groupName);
	    if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
		List contextUserGroup = new LinkedList();	        
		VWSLog.dbg("domainComponent before calling search formembers..."+domainComponent);
		contextUserGroup=searchForMembers_New(initialContext, domainComponent, filter, flag);
		//contextUserGroup=searchForNames(initialContext, domainComponent, filter);
		for(int j=0;j<contextUserGroup.size();j++)
		    userGroupList.add(contextUserGroup.get(j));

	    }else{
		for(int i=0;i<OUs.length;i++)
		{
		    List contextUserGroup = new LinkedList();
		    String sContext = "";
		    if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
		        	sContext = frameContextNameForNovell(OUs[i].trim());    		        	
		        	contextUserGroup=searchForMembers_New(initialContext, sContext, filter, flag);
		        	for(int j=0;j<contextUserGroup.size();j++)
		        	    userGroupList.add(contextUserGroup.get(j));
		}
	    }
	    initialContext.close();
	    return userGroupList;
	}catch(Exception ex){
		message = resourceManager.getString("LDAPMSG.GetUsersInGroup")+ex.getMessage();
		//VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    VWSLog.err("Error in getting getUsersInGroup "+ex.getMessage());
	    return new LinkedList();
	}
    }*/
    /*
     * public String getUserMail(String users) {
		try {
		    Vector userVector = new Vector();
		    DirContext dircontext = getInitialContext();
		    if (dircontext == null)
			return "Err";
		    String userName = users.trim();
		    String context = "";
		    // String userMailIds = searchForUserMails(dircontext, context, "(&(objectClass=User)(|((CN="+userName+")(givenname=" + userName + ")(name=" + userName + "))))");
		    String mailId = "";
		    SearchControls controls = new SearchControls();
		    controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		    controls.setReturningObjFlag(true);
		    for (int i = 0; i < OUs.length; i++) {
			context = frameContextNameForADS(OUs[i]);
			System.out.println("Context Name " + context);
			NamingEnumeration searchResults = dircontext.search(context,
				"(&(objectClass=User)(|(sAMAccountName=" + userName + ")(userPrincipalName=" + userName + "@" + ldapDirName + ")))",
				controls);
			if (searchResults != null && searchResults.hasMoreElements()) {
			    SearchResult searchResult = (SearchResult) searchResults
			    .nextElement();
			    // if (searchResult.getAttributes().get("mail")
			    // != null)
			    {
				// mailId =
				// searchResult.getAttributes().get("mail").get(0).toString();
				Attributes attrs = searchResult.getAttributes();
				NamingEnumeration allAttributes = attrs.getAll();
				while (allAttributes.hasMoreElements()) {
				    Attribute attr = (Attribute) allAttributes.nextElement();
				    System.out.println(" attribute: " + attr);
				}
				searchResults.close();
			    }
			}
		    }
		    if (mailId.trim().equals("")) {
			System.out.println("Mail id is empty for " + userName);
			mailId = "-";
		    }
		    dircontext.close();
		    return mailId;
		} catch (Exception ex) {
			message = resourceManager.getString("DAPMSG.ERRORMailIds") +" "+ ex.getMessage();
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    VWSLog.err("Error in getting UserMailIds " + ex.getMessage());
		    ex.printStackTrace();
		    return "None";
		}
	    }*/     
		/**End of CV10.2 code merges****/
}