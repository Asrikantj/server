/*
 * RampageServer.java
 *
 * 
 */
package com.computhink.vws.directory;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.computhink.common.Util;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSUtil;
import com.computhink.vws.server.mail.VWMail;

public class Nt extends Directory implements VWSConstants
{
    private static native int getNtUsers(String serverName, List list);
    private static native int getNtGroups(String serverName, List list);
    private static native int getUsersInNtGroup
                                    (String Server, List list,String groupName);
    private static native int checkUser
                                      (String user, String pass, String Server);
    
    //WNetAddConnection2 returns
    private static final int ERROR_LOGON_FAILURE = 1326;
    private static final int ERROR_ACCOUNT_RESTRICTION = 1327;
    private static final int ERROR_INVALID_LOGON_HOURS = 1328;
    private static final int ERROR_INVALID_WORKSTATION = 1329;
    private static final int ERROR_PASSWORD_EXPIRED = 1330;
    private static final int ERROR_ACCOUNT_DISABLED = 1331;
    private static final int ERROR_LOGON_NOT_GRANTED = 1380;
    private static final int ERROR_LOGON_TYPE_NOT_GRANTED = 1385;
    private static final int ERROR_WRONG_TARGET_NAME = 1396;
    
    private String loginServer = null;
    private String AuthorsServer = null;
    
    Nt(String loginServer, String AuthorsServer)
    {
         this.loginServer = "\\\\" + loginServer;
         this.AuthorsServer = "\\\\" + AuthorsServer;

         if (!loadLibrary()){
        	String message = ResourceManager.getDefaultManager().getString("Nt.LoadNativeLib");
 			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
            VWSLog.err("Could not load Windows native library");
         }
    }
    public String getCurrentUser(Object o, boolean b){return "";}
    public String getCurrentUser(){return "";}
    public String getCurrentUser(Object context){return "";}
    public String getCurrentUser(boolean ensureLoggedIn){return "";}
    public Vector getUserMailIds(List users){
    	return new Vector();
    }
    /*getCommonName added for LDAP. Valli 22 March 2007*/
    public String getCommonName(String userName,String password){
    	return "";
    }
    
    //Return type linked list is giving problem when data is having &. So changed into vector
    public synchronized Vector getUsers(boolean isLowerCaseRequired)
    {
        List list = new LinkedList();
        Vector userVector = new Vector();
        String userName = null;
        VWSLog.dbg("Before calling getNtUsers...");
        getNtUsers(AuthorsServer, list);
        VWSLog.dbg("After fetching users from getNtUsers..." + list);
        for(int j = 0; j < list.size(); j++)
        {
        	userName = list.get(j).toString();
        	if (isLowerCaseRequired) {
        		userName = userName.toLowerCase();
        	}
        	userVector.add(userName);
        }
        return userVector;
    }
    public synchronized Vector getGroups(){
    	List list = new LinkedList();
    	Vector groupVector = new Vector();
        getNtGroups(AuthorsServer, list);
        for(int j = 0; j < list.size(); j++)
        {
    		groupVector.add(list.get(j));
        }
        return groupVector;
    }
    public synchronized List getUsersInGroup(String group)
    {
        List list = new LinkedList();
        VWSLog.dbg("before calling native method getUsersInNtGroup ---> AuthorsServer :"+AuthorsServer+" group :"+group);
        int returnValue = getUsersInNtGroup(AuthorsServer, list, group);
        VWSLog.dbg("getUsersInNtGroup native call return value :"+returnValue);
        VWSLog.dbg("Users List :"+returnValue);
        List returnList = new LinkedList();
        Iterator iterator = list.iterator();
        while (iterator.hasNext())
        {
            String groupUser = (String) iterator.next();
            int index = groupUser.lastIndexOf('\\');
            if (index != -1)
            {
                groupUser = groupUser.substring(index+1);
            }
            returnList.add(groupUser);
        }
        //VWSLog.add("returnList.............NT :"+returnList);
        return returnList;
    }
    public synchronized List getUsersInGroup_New(String group, int flag)
    {
        List list = new LinkedList();
        int returnValue = getUsersInNtGroup(AuthorsServer, list, group);
        List returnList = new LinkedList();
        Iterator iterator = list.iterator();
        while (iterator.hasNext())
        {
            String groupUser = (String) iterator.next();
            int index = groupUser.lastIndexOf('\\');
            if (index != -1)
            {
                groupUser = groupUser.substring(index+1);
            }
            returnList.add(groupUser);
        }
        return returnList;
    }
    private static boolean loadLibrary()
    {
        try
        {
            System.load(VWSUtil.getHome() + Util.pathSep 
                                            + "system" + Util.pathSep + NT_LIB);
             return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }
    public int AuthenticateUser(String user, String pass)
    {
        int ret; 
        try
        {
            if (user == null || user.equals("")) return 0;
            VWSLog.dbg("UserName >>>:"+user);
            VWSLog.dbg("loginServer >>>:"+loginServer);
            ret = checkUser(user, pass, loginServer);
            VWSLog.dbg("Return value from checkUser method :"+ret);
        }
        catch(Exception e){return 0;}
        switch (ret)
        {
            case 0:                     return 1;    
            case ERROR_LOGON_FAILURE:   return 0;   
            case ERROR_LOGON_NOT_GRANTED: if (isLocal()) return 1;
            case ERROR_LOGON_TYPE_NOT_GRANTED: if (isLocal()) return 1;
            default: 
                VWSLog.war("Invalid login by " + user +  ": " 
                                                         + getDescription(ret));
                return 0;
        }
    }
    private String getDescription(int err)
    {
        String des;
        switch (err)
        {
            case ERROR_ACCOUNT_RESTRICTION:
                des = "policy restriction has been enforced.";
                break;
            case ERROR_INVALID_LOGON_HOURS:
                des = "account logon time restriction violation.";
                break;
            case ERROR_INVALID_WORKSTATION:
                des = "user not allowed to log on to this computer.";
                break;
            case ERROR_PASSWORD_EXPIRED:
                des = "the specified account password has expired.";
                break;
            case ERROR_ACCOUNT_DISABLED:
                des = "account currently disabled.";
                break;
            case ERROR_LOGON_NOT_GRANTED:
                des = "the user has not been granted the requested logon at this computer.";
                break;
            case ERROR_LOGON_TYPE_NOT_GRANTED:
                des = "the user has not been granted the requested logon type at this computer.";
                break;
            case ERROR_WRONG_TARGET_NAME:        
                des = "The target account name is incorrect.";
                break;
            default: 
                des = "unknown error:" + err;
        }
        return des;
    }
    private boolean isLocal()
    {
        try
        {
            String server = loginServer.substring(2);
            if (server.equals(InetAddress.getLocalHost().getHostAddress()) ||
                server.equals(InetAddress.getLocalHost().getHostName())       )
            return true;
        }
        catch(UnknownHostException e){}
        return false;
    }
	@Override
	public String AuthenticateUserLastName(String lastName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUserWithUPN() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getScheme() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getSamAttribute(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getUserBasicAttributes(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUserWithUPN(String[] ouList1) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String AuthenticateUserDN(String ldapUserDN) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getuserPrincipalName(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getDirName() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUsers1(String dirName, String loginServer) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getGroups1(String dirName, String loginServer) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUserMailIds1(List users, String principalDirName,
			String loginServer) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUsers1New(String dirName, String loginServer, String searchForPName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Vector getGroups1New(String dirName, String loginServer, String searchForPName) {
		// TODO Auto-generated method stub
		return null;
	}

	/**CV2019 code merges from CV10.2 line****/
	@Override
	public String getUserFullInfo(String users) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int AuthenticateUserDN(String userName, String password, String userDN) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public List getUsersInGroup_New(String groupName) {
		List list = new LinkedList();
        int returnValue = getUsersInNtGroup(AuthorsServer, list, groupName);
        List returnList = new LinkedList();
        Iterator iterator = list.iterator();
        while (iterator.hasNext())
        {
            String groupUser = (String) iterator.next();
            int index = groupUser.lastIndexOf('\\');
            if (index != -1)
            {
                groupUser = groupUser.substring(index+1);
            }
            returnList.add(groupUser);
        }
        return returnList;
	}
	@Override
	public String getUserAttributes(String userName, String attrib1) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getUserDNMailAttributes(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getUserFullInfoOtherOU(String users) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public HashMap<String, String> getRegisteredGroups(HashMap<String, String> groupsList) {
		List list = new LinkedList();
		HashMap<String, String> groupVector = new HashMap<String, String>();
		String groupName = null;
        //getNtGroups(AuthorsServer, list);    	
        if (groupsList != null && groupsList.size() > 0) {
        	/**
        	 * CV2019 issue fix - Commented on 12/12/2019 by Vanitha.S
			 * Following codes are commented bz if the Named desktop, Named online and
			 * Professional Named server settings are having the same group name then always
			 * we are taking the NamedGroupDesktop license count. Have to store the groups with key(Which group it belongs to).
			 ******/
        	/*for (int groupIndex = 0; groupIndex < groupsList.size(); groupIndex ++) {
    			
        		getUsersInNtGroup(AuthorsServer, list, groupsList.get(groupIndex).toString());
        		if (list != null && list.size() > 0)
    	        {
    	        	groupVector.add(groupsList.get(groupIndex));
    	        	VWSLog.add(("Group -> "+groupsList.get(groupIndex)));
    	        }
            }*/      	
        	for (String key : groupsList.keySet()) {
    		    groupName = groupsList.get(key);
    		    VWSLog.dbg("Key = " + key + ", Value = " + groupName);
    		    getUsersInNtGroup(AuthorsServer, list, groupName);
        		VWSLog.dbg("getUsersInNtGroup return value ::: "+list);
        		if(list != null && list.size() > 0)
    	        {
    	        	groupVector.put(key, groupName);
    	        	VWSLog.add("Group -> "+groupName);
    	        }
        		list.clear();
    		}
        }
        return groupVector;
    }
	@Override
	public Vector getAttributeList(String dirName, String loginServer, String filter) {
		return null;
	}
	/***End of CV10.2 code merges******************************************/
}