/**
 * 
 */
package com.computhink.vws.directory;

import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.vwc.VWCUtil;
/**
 * @author shanmugavalli.c
 *
 */
public class UserGroupInXML {

	//public String userGroupListFile = VWCUtil.getHome()+"\\"+lib+"\\"+Constants.userGroupXMLFile;
	public String userGroupListFile = VWCUtil.getHome()+Util.pathSep+"lib"+Util.pathSep+Constants.userGroupXMLFile;
	private Hashtable userGroupList;
	
	
	public Vector getUsersFromXML(){
	    
		VWSAXParser parser = new VWSAXParser();
		VWSAXParser.getParser(userGroupListFile);
        userGroupList=parser.getInfo();
        if(userGroupList==null || userGroupList.size()==0)
            return null;
        String prefix="UserGroupList\\Users\\";
        int recCount=getIntValue(prefix+"UserCount");
        if(recCount==0) return null;
        prefix+="User";
        Vector userList =  new Vector();
        for(int i=0;i<recCount;i++)
        {
        	prefix="UserGroupList\\Users\\"+"User"+i;
            String recName=getValue(prefix);
            if(!recName.equals(""))
            	userList.add(recName);
        }
    	return userList;
    }
    public Vector getGroupsFromXML(){
   	
    	VWSAXParser parser = new VWSAXParser();
    	VWSAXParser.getParser(userGroupListFile);
        userGroupList=parser.getInfo();
        if(userGroupList==null || userGroupList.size()==0)
            return null;
        String prefix="UserGroupList\\Groups\\";
        int recCount=getIntValue(prefix+"GroupCount");
        if(recCount==0) return null;
        Vector groupList =  new Vector();
        for(int i=0;i<recCount;i++)
        {
        	prefix="UserGroupList\\Groups\\"+"Group"+i;
            String recName=getValue(prefix);
            if(!recName.equals(""))
            	groupList.add(recName);
        }
    	return groupList;
    }
    
    public List getUsersInGroupsXML(String groupName){
    	VWSAXParser parser = new VWSAXParser();
    	VWSAXParser.getParser(userGroupListFile);
        userGroupList=parser.getInfo();
        if(userGroupList==null || userGroupList.size()==0)
            return null;
        String prefix="UserGroupList\\UsersInGroup\\GroupName\\MemberCount"+groupName;
        int recCount=getIntValue(prefix);
        if(recCount==0) return null;
        LinkedList usersInGroupList =  new LinkedList();
        for(int i=0;i<recCount;i++)
        {
        	prefix="UserGroupList\\UsersInGroup\\GroupName\\Member"+i+groupName;
            String recName=getValue(prefix);
            if(!recName.equals(""))
            	usersInGroupList.add(recName);
        }
    	return usersInGroupList;
    }
    private String getValue(String key)
    {
        String value ="";
        try{
           value=(String)userGroupList.get(key);
           if(value==null) value="";
           return value;
        }
        catch(Exception e){}
        return "";
    }
//   ------------------------------------------------------------------------------
    private int getIntValue(String key)
    {
        String value="";
        try{
           value=(String)userGroupList.get(key);
           if(value==null) return 0;
           return Util.to_Number(value);
        }
        catch(Exception e){}
        return 0;
    }
    
 /*   public static void main(String arg[]){
    	getUsersInGroupsXML("Backup Operators");
    }*/
}
