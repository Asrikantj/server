/*
 * Nds.java
 *
 * Created on December 9, 2003, 6:03 PM
 */
package com.computhink.vws.directory;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import com.computhink.vwc.resource.ResourceManager;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.mail.VWMail;
import com.novell.java.security.Authenticator;
import com.novell.java.security.GetIdentitiesException;
import com.novell.java.security.Identity;
import com.novell.java.security.IdentityScope;
import com.novell.java.security.KeyManagementException;
import com.novell.service.security.NdsIdentity;
import com.novell.service.security.NdsIdentityScope;
import com.novell.service.security.NdsPasswordIdentity;
import com.novell.service.security.Password;

public class Nds extends Directory
{
    private String tree = null;
    private String[] context = null;
    private DirContext initialContext = null;

    Nds(String tree, String[] context)
    {
        this.tree = tree;
        this.context = context;
    }
    public String getTree()
    {
        return tree;
    }
    public String getCurrentUser(boolean ensureLoggedIn)
    {
        return getCurrentUser(null,ensureLoggedIn);
    }
    public String getCurrentUser(Object context, boolean ensureLoggedIn)
    {
        return getCurrentUser(context);
    }
    public String getCurrentUser()
    {
        return getCurrentUser(null);
    }
    public Vector getUserMailIds(List users){
    	return new Vector();
    }
    /*getCommonName added for LDAP. Valli 22 March 2007*/
    public String getCommonName(String userName,String password){
    	return "";
    }
    public String getCurrentUser(Object context)
    {
        Identity[] identities = null;
        try
        {
            identities = Authenticator.getIdentities();
        }
        catch (GetIdentitiesException e)
        {
            VWSLog.err(e.getMessage());
            return null;
        }
        catch (Exception e)
        {
            VWSLog.err(e.getMessage());
            return null;
        }
        int length = identities.length;
        if(identities == null || length == 0)
        {
            return null;
        }
        // if context was specified use it
        if (context == null || context.toString().length() == 0)
        {
            VWSLog.dbg("First Ident -> " + identities[0].getName());
            return extractName(identities[0].getName());
        }
        String contextString = context.toString().toUpperCase();
        for (int i=0;i<length;i++)
        {
            String name = identities[i].getName();
            if (name.toUpperCase().endsWith(contextString))
            {
                // found it
                 VWSLog.dbg("ident #"+i+" matched");
                return extractName(name);
            }
        }
        VWSLog.dbg("Context " + contextString +
                   " not found using first name -> " + identities[0].getName());
        return extractName(identities[0].getName());
    }

    private DirContext getInitialContext()
    {
        if (initialContext == null)
        {
            if (tree == null) return null;
            Properties prop = new Properties();
            prop.put(Context.INITIAL_CONTEXT_FACTORY,
                      "com.novell.service.nds.naming.NdsInitialContextFactory");
            String providerUrl = "nds://" + tree + "/";
            prop.put(Context.PROVIDER_URL, providerUrl);
            VWSLog.dbg("NDS Provider Tree: " + providerUrl);
            try
            {
                initialContext = new InitialDirContext(prop);
            }
            catch (NamingException e)
            {
                VWSLog.dbg("Unable to get Initial Directory Context-> " 
                                                              + e.getMessage());
	    }
        }
        return initialContext;
    }
    private String extractName(String contextName)
    {
        int index = contextName.indexOf(".");
        if (index == -1) return contextName;

        return contextName.substring(0, index);
    }
    private String extractDistingushedName(String distingushedName)
    {
        String distingushedText = "Distinguished Name ";
        if (distingushedName.startsWith(distingushedText))
        {
            // skip the distingushed text
            distingushedName = distingushedName.substring
                                                    (distingushedText.length());
        }
        return extractName(distingushedName);
    }
    private List searchForNames(DirContext dirContext,String sContext,String filter)
    {
        LinkedList names = new LinkedList();
        if (dirContext == null)
        {
            VWSLog.dbg("dirContext was null-> ");
            return names;
        }
        SearchControls controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String returningAttributes[] = new String[0];
        controls.setReturningAttributes(returningAttributes);
        try
        {
            VWSLog.dbg("Using context -> "+context+" filter -> "+filter);
            NamingEnumeration searchResults = dirContext.search
                                                   (sContext, filter, controls);
            VWSLog.dbg("Examining the search results");
            while (searchResults != null && searchResults.hasMore())
            {
                SearchResult searchResult=(SearchResult) searchResults.next();
                //showAttributes(searchResult);
                names.add(extractName(searchResult.getName()));
            }
            return names;
        }
        catch (NamingException e)
        {
            VWSLog.dbg("JNDISearch failed ->"+e.getMessage());
            return new LinkedList();
        }
    }
    private List searchForMembers(DirContext dirContext, String sContext, 
                                                                  String filter)
    {
        LinkedList names = new LinkedList();
        if (dirContext  == null)
        {
            VWSLog.dbg("dirContext was null");
            return names;
        }
        SearchControls controls = new SearchControls();
        controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String returningAttributes[] = new String[1];
        returningAttributes[0] = "Member";
        controls.setReturningAttributes(returningAttributes);
        try
        {
            VWSLog.dbg("Using initialContext -> " + 
                                    dirContext  +
                                    " context -> " + 
                                    context + 
                                    " filter -> " + filter);
            NamingEnumeration searchResults = dirContext.search
                                                     (sContext,filter,controls);
            VWSLog.dbg("Examining the search results");
            while (searchResults != null && searchResults.hasMore())
            {
                SearchResult searchResult=(SearchResult) searchResults.next();
                showAttributes(searchResult);
                Attributes attributes = searchResult.getAttributes();
                Attribute attribute = attributes.get("Member");
                if (attribute == null) return names;
                NamingEnumeration values = attribute.getAll();
                while (values.hasMore())
                {
                    Object value = values.next();
                    names.add(extractDistingushedName(value.toString()));
                }
            }
            return names;
        }
        catch (NamingException e)
        {
        	String message = ResourceManager.getDefaultManager().getString("NDS.JNDISearchFail") +" "+ e.getMessage();
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
            VWSLog.err("JNDISearch failed ->" + e.getMessage());
            return new LinkedList();
        }
    }
    
		private List searchForMembers_New(DirContext dirContext, String sContext, 
		            String filter)
		{
		LinkedList names = new LinkedList();
		if (dirContext  == null)
		{
		VWSLog.dbg("dirContext was null");
		return names;
		}
		SearchControls controls = new SearchControls();
		controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
		String returningAttributes[] = new String[1];
		returningAttributes[0] = "Member";
		controls.setReturningAttributes(returningAttributes);
		try
		{
		VWSLog.dbg("Using initialContext -> " + 
		dirContext  +
		" context -> " + 
		context + 
		" filter -> " + filter);
		NamingEnumeration searchResults = dirContext.search
		(sContext,filter,controls);
		VWSLog.dbg("Examining the search results");
		while (searchResults != null && searchResults.hasMore())
		{
		SearchResult searchResult=(SearchResult) searchResults.next();
		showAttributes(searchResult);
		Attributes attributes = searchResult.getAttributes();
		Attribute attribute = attributes.get("Member");
		if (attribute == null) return names;
		NamingEnumeration values = attribute.getAll();
		while (values.hasMore())
		{
		Object value = values.next();
		names.add(extractDistingushedName(value.toString()));
		}
		}
		return names;
		}
		catch (NamingException e)
		{
		String message = ResourceManager.getDefaultManager().getString("NDS.JNDISearchFail") +" "+ e.getMessage();
		VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		VWSLog.err("JNDISearch failed ->" + e.getMessage());
		return new LinkedList();
		}
		}
//------------------------------------------------------------------------------
    private void showAttributes(SearchResult searchResult)
    {
        VWSLog.dbg("Show attributes for -> "+searchResult.getName());
        Attributes attributes = searchResult.getAttributes();
        //if (Log.LOG_DEBUG) Log.verboseDebug("Attributes -> "+attributes.toString());
        /*
        NamingEnumeration ids = attributes.getIDs();
        try
        {
            while (ids.hasMore())
            {
                String id = (String) ids.next();
                if (Log.LOG_DEBUG) Log.verboseDebug("Id -> " + id);

                Attribute attribute = attributes.get(id);
                NamingEnumeration values = attribute.getAll();
                while (values.hasMore())
                {
                    Object value = values.next();
                    if (Log.LOG_DEBUG)
                        Log.verboseDebug("    value -> " + value + ":" +
                                         value.getClass());
                }
            }
        }
        catch (NamingException e)
        {
            if (Log.LOG_ERROR) Log.errorLog(e);
        }
        */
    }
    
    //Return type linked list is giving problem when data is having &. So changed into vector
    public Vector getUsers(boolean isLowerCaseRequired) 
    {
        VWSLog.dbg("Getting Users");
        DirContext initialContext = getInitialContext();
        Vector userVector = new Vector();
        String userName = null;
        if (initialContext == null) return userVector;
        for(int i=0; i < context.length; i++)
        {
            List contextUser = searchForNames(initialContext,context[i],
                                                         "(Object Class=User)");
            VWSLog.dbg("Users -> " + contextUser.size());
            for(int j=0; j < contextUser.size(); j++)
            {
            	userName = contextUser.get(j).toString();
            	if (isLowerCaseRequired) {
            		userName = userName.toLowerCase();
            	}
            	userVector.add(userName);
            }
        }
        return userVector;
    }
    public Vector getGroups()
    {
        DirContext initialContext = getInitialContext();
        Vector groupVector = new Vector();
        if (initialContext == null) return groupVector;
        for(int i=0;i<context.length;i++)
        {
            List contextGroup=searchForNames(initialContext,context[i],"(Object Class=Group)");
            for(int j=0;j<contextGroup.size();j++)
            {
            	groupVector.add(contextGroup.get(j));
                VWSLog.dbg("Group -> " + contextGroup.get(j));
            }
        }
        return groupVector;
    }
    
    public List getUsersInGroup(String groupName)
    {
        DirContext initialContext = getInitialContext();
        if (initialContext == null) return new LinkedList();
        String filter = "(CN=" + groupName + ")(Object Class=Group)";
        //String filter = "(Object Class=Group)";
        List userGroupList=new LinkedList();
        for(int i=0;i<context.length;i++)
        {
            List contextUserGroup=searchForMembers(initialContext, context[i], filter);
            for(int j=0;j<contextUserGroup.size();j++)
                userGroupList.add(contextUserGroup.get(j));
        }
        return userGroupList;
    }
    
    public List getUsersInGroup_New(String groupName, int flag)
    {
        DirContext initialContext = getInitialContext();
        if (initialContext == null) return new LinkedList();
        String filter = "(CN=" + groupName + ")(Object Class=Group)";
        //String filter = "(Object Class=Group)";
        List userGroupList=new LinkedList();
        for(int i=0;i<context.length;i++)
        {
            List contextUserGroup=searchForMembers(initialContext, context[i], filter);
            for(int j=0;j<contextUserGroup.size();j++)
                userGroupList.add(contextUserGroup.get(j));
        }
        return userGroupList;
    }
    
    public int AuthenticateUser(String user, String pass)
    {
        String params[] = {"[NDS]", tree, "", user, pass};

        int index = user.indexOf(".");
        

        
        
        if (index > 0)
        {
            /* Issue/Purpose : Authenticate fails for direct call 
            * param[3] value was wrong. It was user name with context value. It should be only user name 
            *  Created By : C.Shanmugavalli Date: 16 Oct 2006
            */
             params[3] = user.substring(0, index);
            params[2] = user.substring(index+1);
            String userName = user.substring(0, index);
            /* Issue/Purpose : Authenticate fails for user name with not full context level values 
            *  Created By : C.Shanmugavalli Date: 3 Nov 2006
            */
            boolean authenticateReturn = authenticate(params, userName, params[2], false);
            if(!authenticateReturn){
            	
            	for (int i=0; i < context.length; i++)
                {
                    params[2] = context[i];
                    params[3] = user;
                    //VWSLog.add(" params[2](Context) = "+params[2]+ "  params[3](User) = "+params[3]);
                    if (authenticate(params, userName, params[2], true))
                        return 1;
                    else
                        continue;
                }
            		
            }
            
            if (authenticateReturn) return 1;
            
            for (int i=0; i < context.length; i++)
            {
                params[2] = context[i];
                if (authenticate(params, user, params[2], true))
                    return 2;
                else
                    continue;
            }
            
            
        }
        else
        {
            for (int i=0; i < context.length; i++)
            {
                params[2] = context[i];
                if (authenticate(params, user, params[2], true))
                    return 1;
                else
                    continue;
            }
        }
        return 0;
    }
    private boolean authenticate(String params[], String user, String context, boolean flag)
    {
       Identity identity = null;
       try
       {
           identity = buildNdsIdentity(null, 0, params);
           Authenticator.verifyTokens(identity);
       }
       catch(Exception e)
       {
       	   if(flag)
           VWSLog.dbg("AuthenticateUser (" + user + "." + context + 
                                                         ") " + e.getMessage());
           return false;
       }
       return true;
    }
   static Identity buildNdsIdentity(IdentityScope scope,int offset,String args[])
                                                   throws KeyManagementException
   {
      if( args.length == 0)
         return new NdsIdentity("");
      if(!(offset<args.length-2))
      {
         Identity pi = new NdsPasswordIdentity(args[offset],scope);
         ((NdsPasswordIdentity)pi).setPassword(new Password(args[offset+1]));
         return pi;
      }
      return buildNdsIdentity(new NdsIdentityScope(args[offset],scope),++offset,args);
   }
   //public static void main(String[] args)
   //{
   //    String[] ctx = {"N65CONTEXT"};
   //    Nds nds = new Nds("N65TREE", ctx);
   //    System.out.println(nds.AuthenticateUser("admin", "manager"));
   //}
@Override
public String AuthenticateUserLastName(String lastName) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Vector getUserWithUPN() {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getScheme() {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getSamAttribute(String userName) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getUserBasicAttributes(String userName) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Vector getUserWithUPN(String[] ouList1) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String AuthenticateUserDN(String ldapUserDN) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getuserPrincipalName(String userName) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getDirName() {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Vector getUsers1(String dirName, String loginServer) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Vector getGroups1(String dirName, String loginServer) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Vector getUserMailIds1(List users, String principalDirName,
		String loginServer) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Vector getUsers1New(String dirName, String loginServer, String searchForPName) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public Vector getGroups1New(String dirName, String loginServer, String searchForPName) {
	// TODO Auto-generated method stub
	return null;
}

/***CV10.2 - Added for User/Group sync enhancement **/
@Override
public String getUserFullInfo(String users) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public int AuthenticateUserDN(String userName, String password, String userDN) {
	// TODO Auto-generated method stub
	return 0;
}
@Override
public List getUsersInGroup_New(String groupName) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getUserAttributes(String userName, String attrib1) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getUserDNMailAttributes(String userName) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public String getUserFullInfoOtherOU(String users) {
	// TODO Auto-generated method stub
	return null;
}
@Override
public HashMap<String, String> getRegisteredGroups(HashMap<String, String> groupsList) {
	return null;
}
@Override
public Vector getAttributeList(String dirName, String loginServer, String filter) {
	return null;
}
/**End of CV10.2 merges***************************/ 
}
