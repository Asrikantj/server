
package com.computhink.vws.directory;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.Vector;

import com.computhink.common.Constants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;

public abstract class Directory implements Constants
{
    private static Directory theInstance = null;
    private Object context = null;
    
    //public static synchronized Directory getInstance()
  //Flag set to true for creating multiple object for mutlidomain
    public static synchronized Directory getInstance(String ldapDirectory, String  loginServer,boolean flag)
    {
		if (theInstance == null || flag)
        {
        	String port = "";
            //String loginServer = "";
            String authorsServer = "";
            //String ldapDirectory ="";
            String adminUser = "";
            String adminPass = "";
            String loginContext = "";
            String ldapSecurity = "";
            String userMailAttribute = "";
            String type = VWSPreferences.getSSType();
            //CV10.1 LDAP Multidomain enhancement If condition added for ad user
			if (type.equalsIgnoreCase("NT") || type.equalsIgnoreCase(SSCHEME_ADS)) {
				loginServer = "";
				ldapDirectory ="";
				ldapDirectory = "";
				loginServer = "";
			}
            if (type.equalsIgnoreCase(SSCHEME_NDS))
            {
                VWSLog.dbg("Designated NDS Tree: " + VWSPreferences.getTree());
                loginServer = VWSPreferences.getLoginContext();
                authorsServer = VWSPreferences.getAuthorsContext();
                VWSLog.dbg("Designated Login Host: " + loginServer); 
                VWSLog.dbg("Designated Authors Host: " + authorsServer);
              
            /*LDAP security server added . Valli  13 March*/    
            }else if(type.equalsIgnoreCase(SCHEME_LDAP_ADS) 
            		||type.equalsIgnoreCase(SCHEME_LDAP_LINUX)
            		||type.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
            {
            	
            	//ldapDirectory = VWSPreferences.getTree();
                //loginServer = VWSPreferences.getSSLoginHost();
                adminUser = VWSPreferences.getSSAuthorsHost();
                adminPass = VWSPreferences.getAdminPass();
                loginContext = VWSPreferences.getLoginContext();
                ldapSecurity = VWSPreferences.getLdapSecurity();
                port = VWSPreferences.getLdapPort();
                userMailAttribute =  VWSPreferences.getUserMailAttribute().trim();
                if(userMailAttribute == null || userMailAttribute.equals(""))
                	userMailAttribute = "eMailAddress";
                VWSLog.dbg(("Designated LDAP Server : ")+ldapDirectory);
                VWSLog.dbg(("Designated Login Context: ")+loginContext);
                
            }else{
            	
                loginServer = VWSPreferences.getSSLoginHost();
                authorsServer = VWSPreferences.getSSAuthorsHost();
                VWSLog.dbg("Designated Login Host: " + loginServer); 
                VWSLog.dbg("Designated Authors Host: " + authorsServer);
            }
                
            if (type.equalsIgnoreCase("NT") || 
                                           type.equalsIgnoreCase(SSCHEME_ADS))
            {
                theInstance = new Nt(loginServer, authorsServer);
            }
            else if (type.equalsIgnoreCase(SSCHEME_SSH))
            {
                theInstance = new SSH(loginServer);
            }
            /*LDAP security server added. Valli  13 March*/
            //Different LDAP scheme are added here. Valli  16 July 07*/
            else if(type.equalsIgnoreCase(SCHEME_LDAP_ADS) ||
        			type.equalsIgnoreCase(SCHEME_LDAP_NOVELL) )
            {
            	String context = VWSPreferences.getLoginContext();
                String[] contexts = null; 
                if ( context.indexOf(";") >= 0)
                {
                    StringTokenizer tokens = new StringTokenizer(context,";",false);
                    if(tokens.countTokens()>1)
                    {
                        contexts = new String[tokens.countTokens()];
                        int i=0;
                        while (tokens.hasMoreTokens())
                        {
                            String str = tokens.nextToken();
                            if(str!=null && !str.equals(""))
                            {
                                contexts[i]=str;
                                i++;
                            }
                        }
                     }
                }
                else    
                {
                    contexts = new String[1];
                    contexts[0] = context;
                }
                theInstance = new LDAP(ldapDirectory, loginServer, adminUser, adminPass, ldapSecurity, 
                		contexts, context, port, type, userMailAttribute);
                theInstance.context = contexts[0];
                
            }else if (type.equalsIgnoreCase(SSCHEME_NDS))
            {
                        	
                String context = VWSPreferences.getLoginContext();
                String tree = VWSPreferences.getTree();
                String[] contexts = null; 
                if ( context.indexOf(";") >= 0)
                {
                    StringTokenizer tokens = new StringTokenizer
                                                            (context,";",false);
                    if(tokens.countTokens()>1)
                    {
                        contexts = new String[tokens.countTokens()];
                        int i=0;
                        while (tokens.hasMoreTokens())
                        {
                            String str = tokens.nextToken();
                            if(str!=null && !str.equals(""))
                            {
                                contexts[i]=str;
                                i++;
                            }
                        }
                     }
                }
                else    
                {
                    contexts = new String[1];
                    contexts[0] = context;
                }
                theInstance = new Nds(tree, contexts);
                theInstance.context = contexts[0];
            }
            else if(type.equalsIgnoreCase(SCHEME_LDAP_LINUX))
            {
            	String context = VWSPreferences.getLoginContext();
                String[] contexts = null; 
                if ( context.indexOf(";") >= 0)
                {
                    StringTokenizer tokens = new StringTokenizer(context,";",false);
                    if(tokens.countTokens()>1)
                    {
                        contexts = new String[tokens.countTokens()];
                        int i=0;
                        while (tokens.hasMoreTokens())
                        {
                            String str = tokens.nextToken();
                            if(str!=null && !str.equals(""))
                            {
                                contexts[i]=str;
                                i++;
                            }
                        }
                     }
                }
                else    
                {
                    contexts = new String[1];
                    contexts[0] = context;
                }
                theInstance = new LDAPLinux(ldapDirectory, loginServer, adminUser, adminPass, ldapSecurity, 
                		contexts, context, port, type);
                theInstance.context = contexts[0];
            }
        }
        return theInstance;
    }

    public Object getContext()
    {
        return context;
    }
    /**
     * CV10.1 Enhancement MultiDomain.
     * @return
     * 12/6/2017
     * Added By:-Madhavan
     */
    public static Map getLdapDirectories()
    {
        HashMap hashmap = new HashMap();
        String ladpDir = VWSPreferences.getTree();
        String ladpHost = VWSPreferences.getSSLoginHost();
        String splittedDir[] = ladpDir.split(",");
        String splittedHost[] = ladpHost.split(",");
        /**CV2019 code merges from CV10.2 line***/
        String hmapKey = null;
        for(int i = 0; i < splittedHost.length; i++)
        {
        	hmapKey = splittedDir[i].toLowerCase();
        	VWSLog.dbg("hmapKey: "+ hmapKey);
            hashmap.put(hmapKey,splittedHost[i]);
        }
        VWSLog.dbg("hashmap ::::::::::::"+hashmap);
        /**End of CV10.2 line merges***********/
        return hashmap;
    }
    public abstract String getCurrentUser();
    public abstract String getCurrentUser(Object context);
    public abstract String getCurrentUser(boolean ensureLoggedIn);
    public abstract String getCurrentUser(Object context,
                                                        boolean ensureLoggedIn);
    
    //Database added here to get room name in LDAP debug messages.
    /* Getting user details with room name for debug message commented.
    *  Since Sych from Security Server done before room thread starts and wriiten in a xml file
    *  Valli 13 Feb 2008*/
    /*public abstract List getUsers(Database database);
    public abstract List getGroups(Database database);
    public abstract List getUsersInGroup(Database database, String groupName);*/
    /*public abstract List getUsers();
    public abstract List getGroups();*/
    public abstract Vector getUsers(boolean isLowerCaseRequired);
    public abstract Vector getGroups();
    public abstract List getUsersInGroup(String groupName);
    /**CV2019 code merges from CV10.2 line***/ 
    public abstract List getUsersInGroup_New(String groupName);
    public abstract int AuthenticateUser(String userName,String password);
    /**CV2019 code merges from CV10.2 line***/
    public abstract int AuthenticateUserDN(String userName,String password,String userDN);
    public abstract String AuthenticateUserDN(String ldapUserDN);
    public abstract String AuthenticateUserLastName(String userLastName);
    public abstract Vector getUserWithUPN();
    /*getCommonName added for LDAP. Valli 22 March 2007*/
    public abstract String getCommonName(String userName,String password);
    public abstract String getUserBasicAttributes(String userName) ;
    /**CV2019 code merges from CV10.2 line***/
    public abstract String getUserDNMailAttributes(String userName) ;
    public abstract String getUserAttributes(String userName, String attrib1) ;
    public abstract Vector getUserMailIds(List users);
    public abstract String getUserFullInfo(String users) ;
    public abstract String getUserFullInfoOtherOU(String users) ;
    /**End of CV10.2 line merges***********/
    public abstract Vector getUserMailIds1(List users,String principalDirName,String loginServer);
    public abstract String getSamAttribute(String userName);
    public abstract String getuserPrincipalName(String userName);
    public abstract String getScheme();
    public abstract Vector getUserWithUPN(String[] ouList1);
    public abstract String getDirName();

	public abstract Vector getUsers1(String dirName, String loginServer);

	public abstract Vector getGroups1(String dirName, String loginServer);
	public abstract Vector getUsers1New(String dirName, String loginServer, String searchForPName);

	public abstract Vector getGroups1New(String dirName, String loginServer, String searchForPName);
	/**CV2019 - Added for licence count issue for same groupname***/
	public abstract HashMap<String, String> getRegisteredGroups(HashMap<String, String> groupsList);
	public abstract Vector getAttributeList(String dirName, String loginServer, String filter);
	
}