package com.computhink.vws.directory;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.naming.AuthenticationException;
import javax.naming.AuthenticationNotSupportedException;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.event.NamingExceptionEvent;
import javax.naming.ldap.UnsolicitedNotificationEvent;
import javax.naming.ldap.UnsolicitedNotificationListener;

import com.computhink.vwc.resource.ResourceManager;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.mail.VWMail;

public class LDAPLinux extends Directory{

    private String loginServer;
    private String port;
    private String adminUser;
    private String adminPass;
    private String ldapDirName;
    private String securityAuthentication;
    private String OUs[];
    private String OUList;
    private static final String ldapCTX = "com.sun.jndi.ldap.LdapCtxFactory";
    private DirContext initialContext;
    private String domainComponent;
    private String schemeType;
    private String message = "";
    private ResourceManager resourceManager=ResourceManager.getDefaultManager();

    LDAPLinux(String ldapDirectory, String loginServer, String adminUser, String adminPass, 
			  String ldapSecurity, String OUs[], String OUList, String port, String schemeType)
	  {
	        this.ldapDirName = ldapDirectory;
	        this.loginServer = loginServer;
	        this.adminUser = adminUser;
	        this.adminPass = adminPass;
	        this.securityAuthentication = ldapSecurity;
	        this.OUs = OUs;
	        this.OUList = OUList;
	        this.schemeType = schemeType;
	        if(schemeType.equalsIgnoreCase(SCHEME_LDAP_LINUX) && ldapDirName.indexOf(".") != -1 ){
		        this.domainComponent = "dc="+(ldapDirName.substring(0, ldapDirName.indexOf(".")))
		        					+", dc="+ldapDirName.substring(ldapDirName.indexOf(".") + 1);
	        }
	        //VWSLog.add(("this.domainComponent ")+this.domainComponent);
	        this.port = port;
	    }

	    public String getCurrentUser(boolean ensureLoggedIn)
	    {
	        return getCurrentUser(null,ensureLoggedIn);
	    }
	    public String getCurrentUser(Object context, boolean ensureLoggedIn)
	    {
	        return getCurrentUser(context);
	    }
	    public String getCurrentUser()
	    {
	        return getCurrentUser(null);
	    }
	    public String getCurrentUser(Object context)
	    {
	    	return null;
	    }
	    public String getCommonName(String user, String pass){
	    	return null;
	    }
	    public Vector getUserMailIds(List users){
	    	return new Vector();
	    }
	    public List getUsersInGroup(String groupName)
	    {
	        try{
		        DirContext initialContext = getInitialContext();
		        if (initialContext == null) return new LinkedList();
		        String filter = "(&(cn=" + groupName + ")(objectClass=groupOfNames))";
		        List userGroupList=new LinkedList();
		        //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
		        for(int i=0;i<OUs.length;i++)
		        {
		        	List contextUserGroup = new LinkedList();
		        	String sContext = "";
	        		sContext = frameContextName(OUs[i].trim());
		        	VWSLog.dbg("Examining the search results for groupName '"+groupName);
		        	contextUserGroup=searchForMembers(initialContext, sContext, filter);
		        	for(int j=0;j<contextUserGroup.size();j++)
		                userGroupList.add(contextUserGroup.get(j));
		        }
		        initialContext.close();
		        return userGroupList;
	        }catch(Exception ex){
	        	message = resourceManager.getString("LDAPLnxMsg.GetUserInGroup") +" "+ ex.getMessage();
				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	        	VWSLog.err("Error in getting getUsersInGroup "+ex.getMessage());
	        	return new LinkedList();
	        }
	    }

	    public List getUsersInGroup_New(String groupName, int flag)
	    {
	        try{
		        DirContext initialContext = getInitialContext();
		        if (initialContext == null) return new LinkedList();
		        String filter = "(&(cn=" + groupName + ")(objectClass=groupOfNames))";
		        List userGroupList=new LinkedList();
		        //User tables are populated based on context value entered as in NDS scheme. valli 16 July 2007
		        for(int i=0;i<OUs.length;i++)
		        {
		        	List contextUserGroup = new LinkedList();
		        	String sContext = "";
	        		sContext = frameContextName(OUs[i].trim());
		        	VWSLog.dbg("Examining the search results for groupName '"+groupName);
		        	contextUserGroup=searchForMembers(initialContext, sContext, filter);
		        	for(int j=0;j<contextUserGroup.size();j++)
		                userGroupList.add(contextUserGroup.get(j));
		        }
		        initialContext.close();
		        return userGroupList;
	        }catch(Exception ex){
	        	message = resourceManager.getString("LDAPLnxMsg.GetUserInGroup") +" "+ ex.getMessage();
				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	        	VWSLog.err("Error in getting getUsersInGroup "+ex.getMessage());
	        	return new LinkedList();
	        }
	    }
	    private DirContext getInitialContext()
	    {
	        //if(initialContext == null)
	        {
	        	 	Hashtable env = new Hashtable();
		            //Example principal for Novell LDAP "cn=admin, o=ssvwc");
		            env.put(Context.INITIAL_CONTEXT_FACTORY,ldapCTX);
		            if(Integer.parseInt(port) == SSL_PORT)
		            	env.put(Context.SECURITY_PROTOCOL, SECURITY_PROTOCOL_SSL);
		            String url = "ldap://" + loginServer + ":" + port;
		            env.put(Context.PROVIDER_URL, url);
		            String principal = new String();
	            	principal = "cn="+adminUser+","+domainComponent;
		            //VWSLog.dbg("principal: "+principal);
		            env.put(Context.SECURITY_PRINCIPAL, principal);
		            env.put(Context.SECURITY_CREDENTIALS, adminPass);
		            env.put(Context.SECURITY_AUTHENTICATION, securityAuthentication);
		            //VWSLog.dbg("LDAP Provider Tree: "+ldapDirName);
	            try
	            {
	                initialContext = new InitialDirContext(env);
	            }
	            catch(AuthenticationNotSupportedException authenticationnotsupportedexception)
	            {
	            	message = resourceManager.getString("LDAPLnxMsg.SpecifiedSupport")+" "+authenticationnotsupportedexception.getMessage();
					VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	                VWSLog.err("Authentication specified is not Supported.  "+authenticationnotsupportedexception.getMessage());
	            }
	            catch(NamingException namingexception)
	            {
	            	message = resourceManager.getString("LDAPLnxMsg.SpecifiedSupport")+" "+namingexception.getMessage();
					VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	                VWSLog.err("Unable to get Initial LDAP Directory Context-> "+namingexception.getMessage());
	            }
	        }
	        return initialContext;
	    }
	    private List searchForNames(DirContext dirContext,String context,String filter)
	    {
	        LinkedList names = new LinkedList();
			if (dirContext == null)
			{
				System.out.println("dirContext was null-> ");
				return names;
			}
			SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String returningAttributes[] = new String[0];
			controls.setReturningAttributes(returningAttributes);
			try
			{
				//VWSLog.dbg("Using context -> "+context+" filter -> "+filter);
				NamingEnumeration searchResults = dirContext.search(context, filter, controls);
				VWSLog.dbg("Examining the search results");
				while (searchResults != null && searchResults.hasMore())
				{
					SearchResult searchResult=(SearchResult) searchResults.nextElement();
					String name  = searchResult.getName();
					String temp = extractUser(name.toString().trim());
					if(temp!=null && !temp.trim().equals(""))
						names.add(temp);
				}
				searchResults.close();
				return names;
			}
			catch (NamingException e)
			{
				System.out.println("JNDISearch failed ->"+e.getMessage());
				return new LinkedList();
			}
	    }
	    private String extractUser(String context)
	    {
	    	 String name = null;
	    	 if(context!=null && !context.equals("")){
		         StringTokenizer stringtokenizer = new StringTokenizer(context, ",");
		         if(stringtokenizer.hasMoreTokens())
		         {
		            String s2 = stringtokenizer.nextToken();
		            StringTokenizer stringtokenizer1 = new StringTokenizer(s2, "=");
		            stringtokenizer1.nextToken();
		            name = stringtokenizer1.nextToken();
		         }else{
	    			StringTokenizer stringtokenizer1 = new StringTokenizer(context, "=");
	    			stringtokenizer1.nextToken();
	    			name = stringtokenizer1.nextToken();
		         }
	    	 }
    		return name;
    	}
	    private String extractMember(String context)
	    {
	        String name = null;
	        StringTokenizer stringtokenizer = new StringTokenizer(context, ",");
	        if(stringtokenizer.hasMoreTokens())
	        {
	            String s2 = stringtokenizer.nextToken();
	            StringTokenizer stringtokenizer1 = new StringTokenizer(s2, "=");
	            stringtokenizer1.nextToken();
	            name = stringtokenizer1.nextToken();
	        }
	        return name;
	    }
	    
	    
	    private List searchForMembers(DirContext dirContext, String sContext, String filter)
		{
	    	LinkedList names = new LinkedList();
	    	if (dirContext  == null)
	    	{
	    		VWSLog.dbg("dirContext was null");
	    		return names;
	    	}
	    	SearchControls controls = new SearchControls();
	    	controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    	String returningAttributes[] = new String[1];
	    	returningAttributes[0] = "Member";
	    	controls.setReturningAttributes(returningAttributes);
	    	try
	    	{
	    		//VWSLog.dbg(" context -> " + sContext +" filter -> " + filter);
	    		NamingEnumeration searchResults = dirContext.search
	    		(sContext,filter,controls);
	    		while (searchResults != null && searchResults.hasMoreElements())
	    		{
	    			SearchResult searchResult=(SearchResult) searchResults.nextElement();
	    			Attributes attributes = searchResult.getAttributes();
	    			NamingEnumeration ae = attributes.getAll();
	    			while (ae.hasMore())
	    			{

	    				Attribute attr = (Attribute)ae.next();
						NamingEnumeration e = attr.getAll();
						while (e.hasMore()){
							 Object value = e.next();
							 VWSLog.dbg("Members: " + value.toString());
							 names.add(extractMember(value.toString()));
						 }
						 e.close();
	    			}
	    		}
	    		searchResults.close();
	    		return names;
	    	}
	    	catch (NamingException e)
	    	{
	    		message = resourceManager.getString("LDAPLnxMsg.JNDISearch") +" "+ e.getMessage();
				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    		VWSLog.err("In searchForMembers JNDISearch failed ->" + e.getMessage());
	    		return new LinkedList();
	    	}
		}
	    
	    private List searchForMembers_New(DirContext dirContext, String sContext, String filter, int flag)
		{
	    	LinkedList names = new LinkedList();
	    	if (dirContext  == null)
	    	{
	    		VWSLog.dbg("dirContext was null");
	    		return names;
	    	}
	    	SearchControls controls = new SearchControls();
	    	controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	    	String returningAttributes[] = new String[1];
	    	returningAttributes[0] = "Member";
	    	controls.setReturningAttributes(returningAttributes);
	    	try
	    	{
	    		//VWSLog.dbg(" context -> " + sContext +" filter -> " + filter);
	    		NamingEnumeration searchResults = dirContext.search
	    		(sContext,filter,controls);
	    		while (searchResults != null && searchResults.hasMoreElements())
	    		{
	    			SearchResult searchResult=(SearchResult) searchResults.nextElement();
	    			Attributes attributes = searchResult.getAttributes();
	    			NamingEnumeration ae = attributes.getAll();
	    			while (ae.hasMore())
	    			{

	    				Attribute attr = (Attribute)ae.next();
						NamingEnumeration e = attr.getAll();
						while (e.hasMore()){
							 Object value = e.next();
							 VWSLog.dbg("Members: " + value.toString());
							 names.add(extractMember(value.toString()));
						 }
						 e.close();
	    			}
	    		}
	    		searchResults.close();
	    		return names;
	    	}
	    	catch (NamingException e)
	    	{
	    		message = resourceManager.getString("LDAPLnxMsg.JNDISearch") +" "+ e.getMessage();
				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    		VWSLog.err("In searchForMembers JNDISearch failed ->" + e.getMessage());
	    		return new LinkedList();
	    	}
		}
//------------------------------------------------------------------------------
	    /*public List getUsers()
	    {
    		VWSLog.dbg("Getting Users ");
    		try{
		        LinkedList userList = null;
		        DirContext dircontext = getInitialContext();
		        
		        if(dircontext == null)
		            return new LinkedList();
		        userList = new LinkedList();
		        List list = new LinkedList();
	        	for(int i=0; i < OUs.length; i++)
	            {
		        	list = searchForNames(dircontext, frameContextName(OUs[i]), "(&(objectClass=posixAccount))");
			        VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
			        for(int j = 0; j < list.size(); j++)
			        {
			        	userList.add(list.get(j));
		        		VWSLog.dbg("User -> "+list.get(j));
			        }
	            }
		        dircontext.close();
		        return userList;
    		}catch(Exception ex){
    			VWSLog.err("Error in getting users "+ex.getMessage());
    			return new LinkedList();
    		}
	    }*/
	    
	    public Vector getUsers(boolean isLowerCaseRequired)
	    {
    		VWSLog.dbg("Getting Users ");
    		try{
		        Vector userVector = new Vector();
		        String userName = null;
		        DirContext dircontext = getInitialContext();
		        if(dircontext == null)
		            return new Vector();
		        List list = new LinkedList();
	        	for(int i=0; i < OUs.length; i++)
	            {
		        	list = searchForNames(dircontext, frameContextName(OUs[i]), "(&(objectClass=posixAccount))");
			        VWSLog.dbg("Users size for context '"+OUs[i]+"' -> "+list.size());
			        for(int j = 0; j < list.size(); j++)
			        {
			        	userName = list.get(j).toString();
		            	if (isLowerCaseRequired) {
		            		userName = userName.toLowerCase();
		            		if (userName.contains("@")) {
			        			userName = userName.substring(0, userName.indexOf("@"));
			        		}
		            	}
		            	userVector.add(userName);
			        }
	            }
		        dircontext.close();
		        return userVector;
    		}catch(Exception ex){
    			message = resourceManager.getString("LDAPLnxMsg.GettingUsers")+" "+ex.getMessage();
				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    			VWSLog.err("Error in getting users "+ex.getMessage());
    			return new Vector();
    		}
	    }
	    
	   /* public void showAttributes(SearchResult sr){
	    	String cnName = sr.getName().toString()+", DC=vwdev,DC=com";
	    	try{
		    	Attributes attrs = initialContext.getAttributes(cnName);
		    	//Attributes attrs = sr.getAttributes();
		    	NamingEnumeration ne = attrs.getAll();
				while (ne.hasMoreElements()){
					String value=String.valueOf(ne.nextElement());
					if(value.indexOf("userPrincipalName") !=-1 || value.indexOf("sAMAccountName") !=-1 || 
							value.indexOf("givenName") !=-1 )
						VWSLog.err("Name value pair:	"+value);
					//StringTokenizer st1=new StringTokenizer(value,":");
				    //String name = st1.nextToken();
				    //if(name.equalsIgnoreCase("sAMAccountName"))
				    	//VWSLog.dbg("value "+ st1.nextToken());
				    //st1=null;
				}
	    	}catch(Exception ex){	    	}
	    }*/
	    
	    /*public List getGroups()
	    {
    		VWSLog.dbg("Getting Groups");
	    	try{
		    	LinkedList groupList = null;
		        DirContext dircontext = getInitialContext();
		        if(dircontext == null)
		            return new LinkedList();
		        groupList = new LinkedList();
		        List list = new LinkedList();
	        	for(int i=0; i < OUs.length; i++)
	            {
	        		String sContext = "";
        			sContext = frameContextName(OUs[i]);
	        		list = searchForNames(dircontext, sContext, "(&(objectClass=posixGroup))");
	        		VWSLog.dbg("Group size for context '"+OUs[i]+"' -> "+list.size());
	    	        for(int j = 0; j < list.size(); j++)
	    	        {
	    	        	groupList.add(list.get(j));
	    	        	VWSLog.dbg(("Group -> "+list.get(j)));
	    	        }
	            }
	        	dircontext.close();
	        	return groupList;
	    	}catch(Exception ex){
	    		VWSLog.dbg("Error in getting groups "+ex.getMessage());
	    		return new LinkedList();
	    	}
	    }*/
	    
	    public Vector getGroups()
	    {
    		VWSLog.dbg("Getting Groups");
	    	try{
		    	Vector groupVector = new Vector();
		        DirContext dircontext = getInitialContext();
		        if(dircontext == null)	return groupVector;
		        List list = new LinkedList();
	        	for(int i=0; i < OUs.length; i++)
	            {
	        		String sContext = "";
        			sContext = frameContextName(OUs[i]);
	        		list = searchForNames(dircontext, sContext, "(&(objectClass=posixGroup))");
	        		VWSLog.dbg("Group size for context '"+OUs[i]+"' -> "+list.size());
	    	        for(int j = 0; j < list.size(); j++)
	    	        {
	    	        	groupVector.add(list.get(j));
	    	        	VWSLog.dbg(("Group -> "+list.get(j)));
	    	        }
	            }
	        	dircontext.close();
	        	return groupVector;
	    	}catch(Exception ex){
	    		VWSLog.dbg("Error in getting groups "+ex.getMessage());
	    		return new Vector();
	    	}
	    }
	    	

	    public int AuthenticateUser(String user, String pass)
	    {
	    	int index = user.indexOf(".");

	    	if (index > 0)
	        {
	            String userName = user.substring(0, index);
	    		String context = user.substring(index+1);
	    		if(AuthenticateFullPathUser(userName, pass, context)){
    				return 1;}
	    		
	    		for(int i = 0; i < OUs.length; i++){
	    		    if(AuthenticateUser(user, pass, OUs[i])){
	    			return 2;}
	    		}	    		
	        }else{
	        	for(int i = 0; i < OUs.length; i++){
        	    	if(AuthenticateUser(user, pass, OUs[i])){
        	    		return 1;}
        	    }
	    	}
	    	VWSLog.dbg("AuthenticateUser '" + user + "' for context '" + OUList + "' " + "Failed");
	        return 0;
	    }
	    public boolean AuthenticateFullPathUser(String user, String pass, String context)
	    {
			String curOuList = frameContextName(context);
			try{
				curOuList = "uid="+user+","+curOuList;
				//VWSLog.dbg(" curOuList "+curOuList);
				if(AuthenticateUserInAllSubOus(user, pass, curOuList))
					return true;
			}catch(Exception ex){
				return false;
			}
			return false;
	    }

	    /*Authentication check with common name and context path.   Valli 22 march 2007*/
	    public boolean AuthenticateUser(String user, String pass, String context)
	    {
			String curOuList = frameContextName(context);
			try{
				curOuList = "uid="+user+","+curOuList;
				VWSLog.dbg(" curOuList "+curOuList);
				if(AuthenticateUserInAllSubOus(user, pass, curOuList)){
					return true;
				}else{
					String filter = "(&(objectClass=organizationalUnit))";
					initialContext = getInitialContext();
					List OUsList=searchForOUs(initialContext,domainComponent,filter);
					initialContext.close();
					List newOusList = new LinkedList();
					for(int i = 0; i<OUsList.size(); i++){
						String subContext = OUsList.get(i).toString();
						if(isInLoginContextSetting(subContext)){
							newOusList.add(subContext);
						}
					}
					for(int i = 0; i<newOusList.size(); i++){
						String subContext = newOusList.get(i).toString();
						if(subContext!=null&& !subContext.equals("")){
							subContext = "uid="+user+","+subContext+","+domainComponent;
							if(AuthenticateUserInAllSubOus(user, pass, subContext))
								return true;
						}
					}
				}
			}catch(Exception ex){
				//VWSLog.add("Error "+ex.getMessage());
				return false;
			}
			//VWSLog.dbg("AuthenticateUser (" + user + "." + context + ") " + "Failed");
			return false;
	    }
	    

	    private List searchForOUs(DirContext dirContext,String sContext,String filter)
	    {
	        LinkedList OUs = new LinkedList();
	        if (dirContext == null)          return OUs;
	        SearchControls controls = new SearchControls();
			controls.setSearchScope(SearchControls.SUBTREE_SCOPE);
			String returningAttributes[] = new String[0];
			controls.setReturningAttributes(returningAttributes);
	        try{
	        	//VWSLog.dbg("searchForOUs-> "+sContext+" filter-> "+filter);
	            NamingEnumeration searchResults = dirContext.search(sContext, filter, controls);
	            while (searchResults != null && searchResults.hasMore())
	            {
					SearchResult searchResult=(SearchResult) searchResults.nextElement();
	            	String name = searchResult.getName();
	            	//VWSLog.dbg("name "+name);
					//String temp = extractOU(searchResults.next().toString());
					OUs.add(name);
	            }
	            searchResults.close();
	            return OUs;
	        }catch (NamingException e){
	        	//VWSLog.dbg("JNDISearch failed ->"+e.getMessage());
	            return new LinkedList();
	        }
	    }


	    /*Authentication check with common name and context path.   Valli 22 march 2007*/
	    public boolean AuthenticateUserInAllSubOus(String user, String pass, String OUContext){
	    	 Hashtable env = new Hashtable();
		     String providerURL = "ldap://" + loginServer + ":" + port;
		     env.put(Context.INITIAL_CONTEXT_FACTORY,ldapCTX);
		     if(Integer.parseInt(port) == SSL_PORT)
	            	env.put(Context.SECURITY_PROTOCOL, SECURITY_PROTOCOL_SSL);
			 env.put(Context.PROVIDER_URL,providerURL);
		     env.put(Context.SECURITY_AUTHENTICATION,securityAuthentication);
			 env.put(Context.SECURITY_CREDENTIALS,pass);
	 		 //VWSLog.dbg(" OUContext "+OUContext);
	 		 env.put(Context.SECURITY_PRINCIPAL, OUContext);

		     InitialDirContext initialdircontext;
		     try
		     {
		            initialdircontext = new InitialDirContext(env);
		            if(initialdircontext!=null)
		            	initialdircontext.close();
		        	//VWSLog.dbg("AuthenticateUser (" + user + ") " + "passed");
		     }catch(AuthenticationException authenticationexception)
		     {
		        	//VWSLog.dbg("AuthenticateUser (" +  OUContext + ") " + "Failed");
		        	return false;
		     }
		     catch(NamingException namingexception)
		     {
		            //VWSLog.dbg("Unable to get Initial LDAP Directory Context-> "+namingexception.getMessage());
		            return false;
		     }
		     return true;
	    }
	    private String extractOU(String contextName){
			int index = contextName.indexOf(":");
			String name = "";
			if (index >0){
				StringTokenizer stringtokenizer = new StringTokenizer(contextName, ":");
				if(stringtokenizer.hasMoreTokens())
			    {
					name = stringtokenizer.nextToken();
				}
			}
			return name;
		}
	    private boolean isInLoginContextSetting(String OUName){
	    	for(int i = 0; i<OUs.length; i++){
	    		if(OUName.indexOf(OUs[i])!=-1){
	    			//VWSLog.dbg( " OUName "+true);
	    			return true;
	    		}
	    		//String loginContextOU = frameContextName(OUs[i]);
	    		//VWSLog.dbg( " loginContextOU "+loginContextOU);
	    		//if(loginContextOU.indexOf(OUName)!=-1){
	    			//return true;
	    		//}
	    	}
	    	return false;
	    }
	    
	    private String frameContextName(String contextName){
	    	if(contextName!=null && !contextName.equals("")){
		    	if(contextName.indexOf(".")!=-1){
			    	String name = "";
			    	StringTokenizer stringtokenizer = new StringTokenizer(contextName, ".");
			    	int count = stringtokenizer.countTokens();
			    	for(int i = 0; i<count ; i++)
				    {
						String tempName = stringtokenizer.nextToken();
						if(i==count-1)
							name = name+"ou="+tempName;
						else
							name = name+"ou="+tempName +",";
					}
			    	contextName = name;
		    	}else{
	    			contextName = "ou="+contextName;
		    	}
		    	contextName = contextName +","+domainComponent;
	    	}else{
	    		contextName = domainComponent;
	    	}
	    	//VWSLog.add("result contextName "+contextName);
	    	return contextName;
	    }
	    /**
	     * A sample UnsolicitedNotificationListener.
	     */
	    static class UnsolListener implements UnsolicitedNotificationListener {
		public void notificationReceived(UnsolicitedNotificationEvent evt) {
		    System.out.println("received: " + evt);
		}

		public void namingExceptionThrown(NamingExceptionEvent evt) {
		    System.out.println(">>> UnsolListener got an exception");
		    evt.getException().printStackTrace();
		}
	    }
		@Override
		public String AuthenticateUserLastName(String lastName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Vector getUserWithUPN() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getScheme() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getSamAttribute(String userName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getUserBasicAttributes(String userName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Vector getUserWithUPN(String[] ouList1) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String AuthenticateUserDN(String ldapUserDN) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getuserPrincipalName(String userName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getDirName() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Vector getUsers1(String dirName, String loginServer) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Vector getGroups1(String dirName, String loginServer) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Vector getUserMailIds1(List users, String principalDirName,
				String loginServer) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Vector getUsers1New(String dirName, String loginServer, String searchForPName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public Vector getGroups1New(String dirName, String loginServer, String searchForPName) {
			// TODO Auto-generated method stub
			return null;
		}

		
		
		/***CV10.2 - Added for User/Group sync enhancement **/
		@Override
		public String getUserFullInfo(String users) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public int AuthenticateUserDN(String userName, String password, String userDN) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public List getUsersInGroup_New(String groupName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getUserAttributes(String userName, String attrib1) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getUserDNMailAttributes(String userName) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getUserFullInfoOtherOU(String users) {
			// TODO Auto-generated method stub
			return null;
		}
		
		@Override
		public HashMap<String, String> getRegisteredGroups(HashMap<String, String> groupsList) {
			return null;
	    }
		
		@Override
		public Vector getAttributeList(String dirName, String loginServer, String filter) {
			return null;
	    }
		/**End of CV10.2 merges***************************/ 
}

