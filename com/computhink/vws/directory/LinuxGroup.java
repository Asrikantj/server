package com.computhink.vws.directory;

/*LIBRARIES*/
import java.io.*;
import java.util.StringTokenizer;
import java.util.List;

/*CLASSES*/
class LinuxGroup
{
/*VARIABLES*/
    private final String groupName;
    private final int groupID;
    private final List users;

/*CONSTRUCTORS*/
    public LinuxGroup(String groupName, int groupID, List users)
    {
        this.groupName = groupName;
        this.groupID = groupID;
        this.users = users;
    }

/*PRIVATE METHODS*/

/*PUBLIC METHODS*/
public String groupName() //returns group name
{
    return groupName;
}

public int groupID() //returns group ID
{
    return groupID;
}

public String userNameAt(int i) //returns group user name at index i
{
    return (String)users.get(i);
}

public int numOfUsers() //returns number of users in group
{
    return users.size();
}
public List groupUsers()
{
    return users;
}
}