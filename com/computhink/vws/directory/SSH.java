/*
 * SSH.java
 *
 * 
 */
package com.computhink.vws.directory;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.mail.VWMail;
import com.sshtools.j2ssh.SshClient;
import com.sshtools.j2ssh.authentication.AuthenticationProtocolState;
import com.sshtools.j2ssh.authentication.KBIAuthenticationClient;
import com.sshtools.j2ssh.authentication.KBIPrompt;
import com.sshtools.j2ssh.authentication.KBIRequestHandler;
import com.sshtools.j2ssh.configuration.ConfigurationLoader;
import com.sshtools.j2ssh.configuration.SshConnectionProperties;

public class SSH extends Directory 
{
    private RandomAccessFile passwd;
    private RandomAccessFile group;
    private List users=new LinkedList();
    private Hashtable groups= new Hashtable();
    private String server;
    private static BufferedReader reader =
                           new BufferedReader(new InputStreamReader(System.in));
    SSH()
    {
        UserGroupExplorerOnLinux();
    }
    SSH(String server)
    {
        this.server = server;
        VWSLog.dbg("SSH Authority--> " + server);
        UserGroupExplorerOnLinux();
    }
    SSH(String host, String userName, String userPassword)
    {
        UserGroupExplorerOnLinux(host,userName,userPassword);
    }
    public synchronized String getCurrentUser()
    {
        return getCurrentUser(null);
    }
    public synchronized String getCurrentUser(Object context)
    {
        return null; //userName;
    }
    public synchronized String getCurrentUser(boolean ensureLoggedIn)
    {
        return getCurrentUser(null, ensureLoggedIn);
    }

    public synchronized String getCurrentUser(Object context,boolean ensureLoggedIn)
    {
        String userName = getCurrentUser(context);
        return userName;
    }
    /*getCommonName added for LDAP. Valli 22 March 2007*/
    public String getCommonName(String userName,String password){
    	return "";
    }
    public Vector getUserMailIds(List users){
    	return new Vector();
    }
    public void UserGroupExplorerOnLinux()
    {
	try
	{
            this.passwd = new RandomAccessFile("/etc/passwd", "r");
	    this.group = new RandomAccessFile("/etc/group", "r");
        }
        catch (Exception e)
        {
            VWSLog.err("Error accessing files: " + e.getMessage());
        }
	finally
	{
            initialize();
        }
    }
    public void UserGroupExplorerOnLinux(String host,String user,String pass)
    {
    }
    private synchronized void initialize()
    {
        users=new LinkedList();
        groups=new Hashtable();
     try
     {
        StringTokenizer st;
        boolean emptyTokensExist;
        int distance;
        int numOfColons;
        
        //passwd line layout is: account:password:UID:GID:GECOS:directory:shell
        String passwdLine;
        String userName;
        int userID;
        int groupID = 0;
        while ( (passwdLine = passwd.readLine()) != null)
        {
            if (passwdLine.startsWith("+")) continue;
            emptyTokensExist = false;
            distance = 0;
            numOfColons=0;
            for (int i = 0; i < passwdLine.length(); i++)
            {
                if (passwdLine.charAt(i) != ':')
                    distance++;
                else
                {
                    numOfColons++;
                    if (numOfColons > 2) break;
                    if (distance == 0)
                    {
                        emptyTokensExist = true;
                        break;
                    }
                    else
                        distance = 0;
                }
            }
            st = new StringTokenizer(passwdLine,":");
            userName = st.nextToken();
            if (userName.endsWith("$"))
            {
                passwdLine = passwd.readLine();
                continue;
            }
            if (!emptyTokensExist && st.hasMoreTokens()) st.nextToken();
            if (st.hasMoreTokens()) userID = Integer.parseInt(st.nextToken());
            if (st.hasMoreTokens()) groupID = Integer.parseInt(st.nextToken());
            users.add(userName);
            VWSLog.dbg("Sync.User-> " + userName);
        }

        //group line layout is: group_name:passwd:GID:user1,user2,...,userx\n
        String groupName;
        List groupUsers;
        String groupLine;
        while ( (groupLine = group.readLine()) != null)
        {
            if (groupLine.startsWith("+")) continue;
            StringTokenizer st2;
            emptyTokensExist = false;
            distance = 0;
            numOfColons=0;
            for (int i = 0; i < groupLine.length(); i++)
            {
                if (groupLine.charAt(i) != ':')
                distance++;
                else
                {
                    numOfColons++;
                    if (numOfColons > 2) break;
                    if (distance == 0)
                    {
                        emptyTokensExist = true;
                        break;
                    }
                    else
                        distance = 0;
                }
            }
            st = new StringTokenizer(groupLine, ":");
            groupName = st.nextToken();
            if (!emptyTokensExist && st.hasMoreTokens()) st.nextToken();
            if (st.hasMoreTokens()) groupID = Integer.parseInt(st.nextToken());
            st2 = null;
            if (st.hasMoreTokens())
                st2 = new StringTokenizer(st.nextToken(), ",");
            groupUsers = new LinkedList();
            if (st2 != null)
                while (st2.hasMoreTokens()) groupUsers.add(st2.nextToken());
            groups.put(groupName, new LinuxGroup(groupName, groupID,groupUsers));
            VWSLog.dbg("Sync.Group-> " + groupName);
        }
     }
     catch(Exception e)
     {
    	 String message = ResourceManager.getDefaultManager().getString("SSH.SSHIntialError") +" "+ e.getMessage();
		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
         VWSLog.err("SSH initialization error: " + e.getMessage());
     }
    }
    /*public List getUsers()
    {
        return users;       
    }
    public List getGroups()
    {
        List unixGroups = new LinkedList();
        Enumeration keys=groups.keys();
        for (int i=0;i<groups.size();i++)   
            unixGroups.add(keys.nextElement());
        return unixGroups;      
    }*/
    
    public Vector getUsers(boolean isLowerCaseRequired)
    {
        Vector userVector = new Vector();
        String userName = null;
        for(int j = 0; j < users.size(); j++)
        {
        	userName = users.get(j).toString();
        	if (isLowerCaseRequired) {
        		userName = userName.toLowerCase();
        	}
        	userVector.add(userName);
        }
        return userVector;
    }
    public Vector getGroups(){
    	List unixGroups = new LinkedList();
        Enumeration keys=groups.keys();
        for (int i=0;i<groups.size();i++)   
            unixGroups.add(keys.nextElement());
    	Vector groupVector = new Vector();
        for(int j = 0; j < unixGroups.size(); j++)
        {
    		groupVector.add(unixGroups.get(j));
        }
        return groupVector;
    }
    
    public List getUsersInGroup(String group)
    {
        LinuxGroup lg = (LinuxGroup) groups.get(group);
        if (lg == null) return null;
        return lg.groupUsers();
    }
    public List getUsersInGroup_New(String group, int flag)
    {
        LinuxGroup lg = (LinuxGroup) groups.get(group);
        if (lg == null) return null;
        return lg.groupUsers();
    }
    public int AuthenticateUser(String user, String pass)
    {
        int  iret = -99;
        final String p = pass;
        SshClient ssh = new SshClient();
        ssh.setSocketTimeout(3000);
        try
        {
            ConfigurationLoader.initialize(true);
            SshConnectionProperties properties = new SshConnectionProperties();
            properties.setHost(server);
            ssh.connect(properties);
            KBIAuthenticationClient kbi = new KBIAuthenticationClient();
            kbi.setUsername(user);
            kbi.setKBIRequestHandler(new KBIRequestHandler() 
            {
                public void showPrompts(String name, String ins, 
                                                            KBIPrompt[] prompts) 
                {
                    if (prompts != null)
                    {
                        for (int i = 0; i < prompts.length; i++) 
                        {
                            if ((prompts[i].getPrompt().startsWith("Password")))
                                prompts[i].setResponse(p);
                            else
                                prompts[i].setResponse("");
                        }
                    }
                  }
             });
            iret = ssh.authenticate(kbi);
        }
        catch(Exception e){}
        ssh.disconnect();
        return ( iret == AuthenticationProtocolState.COMPLETE? 1 : 0);
    }
	@Override
	public String AuthenticateUserLastName(String lastName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUserWithUPN() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getScheme() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getSamAttribute(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getUserBasicAttributes(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUserWithUPN(String[] ouList1) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String AuthenticateUserDN(String ldapUserDN) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getuserPrincipalName(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getDirName() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUsers1(String dirName, String loginServer) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getGroups1(String dirName, String loginServer) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUserMailIds1(List users, String principalDirName,
			String loginServer) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getUsers1New(String dirName, String loginServer, String searchForPName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public Vector getGroups1New(String dirName, String loginServer, String searchForPName) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/****CV2019 merges from 10.2 line--------------------------------***/
	@Override
	public String getUserFullInfo(String users) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int AuthenticateUserDN(String userName, String password, String userDN) {
		// TODO Auto-generated method stub
		return 0;
	}
	@Override
	public List getUsersInGroup_New(String groupName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getUserAttributes(String userName, String attrib1) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getUserDNMailAttributes(String userName) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getUserFullInfoOtherOU(String users) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public HashMap<String, String> getRegisteredGroups(HashMap<String, String> groupsList){
		return null;
    }
	@Override
	public Vector getAttributeList(String dirName, String loginServer, String filter) {
		return null;
	}
/*------------------End of 10.2 merges----------------------------*/

}