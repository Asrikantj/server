/*
 * $Id: LicenseManagerImpl.java,v 1.2 2014/03/15 06:38:24 madhavan Exp $
 *
 *  Copyright (c) 2003 SourceTap - www.sourcetap.com
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  
 */
package com.computhink.vws.license;
import com.computhink.resource.ResourceManager;
import com.computhink.vws.server.*;
import com.computhink.common.*;

import java.util.*;
import java.io.*;

public class LicenseManagerImpl implements LicenseManager, VWSConstants
{
    private static LicenseManager licenseManager;
    private static License license = null;
    private static SWLicense swLicense = null;

    public static LicenseManager getInstance()
    {
        if(licenseManager == null) licenseManager = new LicenseManagerImpl();
        return licenseManager;
        /*
        if(licenseManager == null)
            synchronized(com.computhink.vws.license.LicenseManagerImpl.class)
            {
                if(licenseManager == null)
                    licenseManager = new LicenseManagerImpl();
            }
        return licenseManager;
        */
    }
    public static void setLicense()
    {
        license = null;
    }
    public LicenseManagerImpl()
    {
    }
    public boolean hasValidLicense() throws LicenseException
    {
        License license = getLicense();
        return !license.isExpired();
    }
    public License getLicense() throws LicenseException
    {
        if(license != null) return license;
        try
        {
            String licFile = VWSPreferences.getLicenseFile();
            if (licFile.equals(""))
                           throw new LicenseException(com.computhink.resource.ResourceManager.getDefaultManager().getString
                        		   ("ManagerImplMsg.LicenseFileNotFound"));
            FileInputStream licfis = new FileInputStream(licFile);
            byte[] licBytes = new byte[licfis.available()];
            licfis.read(licBytes);
            licfis.close();
            String licStr = new String(licBytes);
            String pKeyFile = VWSUtil.getHome() + Util.pathSep + PUBLICKEY;
            license = LicenseVerifier.validateLicense(licStr, pKeyFile);
            return license;
	}
        catch (Exception e)
        {
            throw new LicenseException(com.computhink.resource.ResourceManager.getDefaultManager()
            		.getString("ManagerImplMsg.GetLicenseError" )+ e.getMessage());
        }
        
    }
    public SWLicense getSignWiseLicense() throws LicenseException
    {
        if(swLicense != null) return swLicense;
        try
        {
            String licFile = VWSPreferences.getLicenseFile();
            if (licFile.equals(""))
                           throw new LicenseException(com.computhink.resource.ResourceManager.getDefaultManager()
                        		  .getString("ManagerImplMSG.LicenseFileNotFound"));
            licFile = licFile.substring(0, licFile.length() - 3) + "swl";
            FileInputStream licfis = new FileInputStream(licFile);
            byte[] licBytes = new byte[licfis.available()];
            licfis.read(licBytes);
            licfis.close();
            String licStr = new String(licBytes);
            String pKeyFile = VWSUtil.getHome() + Util.pathSep + PUBLICKEY;
            swLicense = LicenseVerifier.validateSWLicense(licStr, pKeyFile);
            return swLicense;
	}
        catch (Exception e)
        {
            throw new LicenseException(com.computhink.resource.ResourceManager.getDefaultManager().getString("ManagerImplMsg.GetLicenseError") + e.getMessage());
        }
        
    }
}
