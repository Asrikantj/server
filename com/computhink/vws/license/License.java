/*
 * $Id: License.java,v 1.24 2020/06/22 06:07:44 vanitha Exp $
 * 
 *  Copyright (c) 2003 SourceTap - www.sourcetap.com
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 */
package com.computhink.vws.license;

import java.text.DateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Locale;
import java.util.Vector;

import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.mdss.MDSS;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.license.LicenseManager;

public class License implements java.io.Serializable
{
	private String companyName;
	private String email;
	private Date expirationDate;

	private int serverSeats = 0;
	private int adminWiseSeats = 0;
	private int desktopSeatsConcurrent = 0;
	private int desktopSeatsNamed = 0;
	private int applet1SeatsConcurrent = 0;
	private int applet2SeatsConcurrent = 0;
	private int applet3SeatsConcurrent = 0;
	private int applet1SeatsNamed = 0;
	private int applet2SeatsNamed = 0;
	private int applet3SeatsNamed = 0;
	private int indexerSeats = 0;
	private int aipSeats = 0;
	private int easSeats = 0;
	private int eworkSeats = 0;
	private int sdkSeats = 0;
	private int arsSeats = 0;
	private int webtopSeatsConcurrent = 0;
	private int webtopSeatsNamed = 0;
	private int webliteSeatsConcurrent = 0;
	private int webliteSeatsNamed = 0;
	private int drsSeats = 0;
	private int webAccessSeats = 0;
	private int notificationSeats= 0;
	private int customLabelWebAccessSeats = 0;
	private int professionalWebAccessSeats = 0;
	private int webaccessSeatsConcurrent=0;
	private int professionalConcWebAccessSeats=0;
    private int namedOnlineWebaccessSeats=0;
    private int csserverSeats=0;
    private int csswebaccessSeat=0;
    private int subAdminSeats = 0;
    
	private int rmSeatsNamed = 0;
	private int rmSeatsConcurrent = 0;
	private int wfSeatsNamed = 0;
	private int wfSeatsConcurrent = 0;
	private int lcSeatsNamed = 0;
	private int lcSeatsConcurrent = 0;

	private int reserved1Seats = 0;
	private int reserved2Seats = 0;
	private int reserved3Seats = 0;
	private int reserved4Seats = 0;
	private int reserved5Seats = 0;
	private int reserved6Seats = 0;
	private int reserved7Seats = 0;
	private int reserved8Seats = 0;
	private int reserved9Seats = 0;

	private Hashtable cserverSeats = new Hashtable();
	private Hashtable cadminWiseSeats = new Hashtable();
	private Hashtable csubAdminWiseSeats = new Hashtable();
	private Hashtable cdesktopSeatsConcurrent = new Hashtable();
	private Hashtable cdesktopSeatsNamed = new Hashtable();
	private Hashtable capplet1SeatsConcurrent = new Hashtable();
	private Hashtable capplet1SeatsNamed = new Hashtable();
	private Hashtable ceworkSeats = new Hashtable();
	private Hashtable caipSeats = new Hashtable();
	private Hashtable cindexerSeats = new Hashtable();
	private Hashtable csdkSeats = new Hashtable();

	private Hashtable cwebliteSeatsConcurrent = new Hashtable();    
	private Hashtable cwebliteSeatsNamed = new Hashtable();
	private Hashtable cwebtopSeatsConcurrent = new Hashtable();
	private Hashtable cwebtopSeatsNamed = new Hashtable();    
	private Hashtable ceasSeats = new Hashtable();
	private Hashtable carsSeats = new Hashtable();
	private Hashtable cdrsSeats = new Hashtable();
	private Hashtable cwebaccessSeats = new Hashtable();
	private Hashtable cprofessionalWebaccessSeats = new Hashtable();
	private Hashtable cconrWebaccessSeats=new Hashtable();
	private Hashtable cprofconcrWebaccessSeats=new Hashtable();
	private Hashtable cnamedOnlineWebaccessSeats=new Hashtable();
	private Hashtable cnotificationSeats = new Hashtable();
	private Hashtable cCustomLabelWebaccessSeats = new Hashtable();
    private Hashtable ccssserverSeats=new Hashtable();
    private Hashtable ccsswebaccessSeats=new Hashtable();

	private Hashtable fatInstances = new Hashtable();

	public License(String licenseKey) throws LicenseException
	{
		setLicenseKey(licenseKey);
	}
	public License(String cName, String cEmail) throws LicenseException
	{
		this(cName, cEmail, "01/01/2025");
	}
	public License(String cName, String cEmail, String expirationDate )
	throws LicenseException
	{
		setCompanyName(cName);
		setEmailAddress(cEmail);
		setExpirationDate(expirationDate);
	}
	public String getCompanyName()
	{
		return companyName;
	}
	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}
	private void setLicenseKey(String licenseKey) throws LicenseException
	{	
		String tokens[] = licenseKey.split("#");
	/*	for(int i=0;i<tokens.length;i++) {
			VWSLog.dbg(i+"::::value::::"+tokens[i]);
		}*/
		if(tokens.length < 18)
			throw new LicenseException("LicenseKey is invalid");
		setCompanyName(tokens[0]);
		setEmailAddress(tokens[1]);
		setExpirationDate(tokens[2]);
		setServerSeats(Integer.parseInt(tokens[3]));
		setAdminWiseSeats(Integer.parseInt(tokens[4]));
		setDesktopSeatsConcurrent(Integer.parseInt(tokens[5]));
		setDesktopSeatsNamed(Integer.parseInt(tokens[6]));
		setWebliteSeatsConcurrent(Integer.parseInt(tokens[7]));//setApplet1SeatsConcurrent(Integer.parseInt(tokens[7]));
		setWebliteSeatsNamed(Integer.parseInt(tokens[8]));//setApplet2SeatsConcurrent(Integer.parseInt(tokens[8]));
		setWebtopSeatsConcurrent(Integer.parseInt(tokens[9]));//setApplet3SeatsConcurrent(Integer.parseInt(tokens[9]));
		setWebtopSeatsNamed(Integer.parseInt(tokens[10]));//setApplet1SeatsNamed(Integer.parseInt(tokens[10]));
		setArsSeats(Integer.parseInt(tokens[11]));//setApplet2SeatsNamed(Integer.parseInt(tokens[11]));
		setDrsSeats(Integer.parseInt(tokens[12]));
		setIndexerSeats(Integer.parseInt(tokens[13]));
		setAipSeats(Integer.parseInt(tokens[14]));
		setEasSeats(Integer.parseInt(tokens[15]));
		setEworkSeats(Integer.parseInt(tokens[16]));
		setSdkSeats(Integer.parseInt(tokens[17]));
		try{
			setNamedOnlineWebaccessSeats(Integer.parseInt(tokens[18]));
		}catch (Exception e) {
			setNamedOnlineWebaccessSeats(0);
		}
		try{
			setNotificationSeats(Integer.parseInt(tokens[19]));
		}catch(Exception ex){
			setNotificationSeats(0);		
		}
		try{
			setCustomLabelWebAccessSeats(Integer.parseInt(tokens[20]));            
		}catch(Exception ex){			
			setCustomLabelWebAccessSeats(0);
		}
		try{
			setProfessionalWebAccessSeats(Integer.parseInt(tokens[21]));            
		}catch(Exception ex){			
			setProfessionalWebAccessSeats(0);
		}try{
			setWebaccessSeatsConcurrent(Integer.parseInt(tokens[22]));
		   
		}catch(Exception ex){			
			System.out.println("Exception5"+ex.getMessage());
			setWebaccessSeatsConcurrent(0);
			
		}
		try{
			setProfessionalConcWebAccessSeats(Integer.parseInt(tokens[23]));
			    
		}catch(Exception ex){			
			System.out.println("Exception6"+ex.getMessage());
			setProfessionalConcWebAccessSeats(0);
		
		}
		try{
			setProfessionalConcWebAccessSeats(Integer.parseInt(tokens[23]));
			    
		}catch(Exception ex){			
			System.out.println("Exception6"+ex.getMessage());
			setProfessionalConcWebAccessSeats(0);
		
		}
		try{
			setCsserverSeats(Integer.parseInt(tokens[24]));
			    
		}catch(Exception ex){			
			System.out.println("Exception6"+ex.getMessage());
			setCsserverSeats(0);
		
		}
		try{
			setCsswebaccessSeat(Integer.parseInt(tokens[25]));
			    
		}catch(Exception ex){			
			System.out.println("Exception6"+ex.getMessage());
			setCsswebaccessSeat(0);
		
		}
		try {
			setSubAdminSeats(Integer.parseInt(tokens[26]));
		} catch (Exception ex) {
			System.out.println("Exception7"+ex.getMessage());
		}
		
		
		
	}
	public String getLicenseKey()
	{
		return companyName + "#" + email + "#" + 
		getExpirationDateString() +  "#" +
		getServerSeats() +  "#" +
		getAdminWiseSeats() +  "#" +
		getDesktopSeatsConcurrent() +  "#" +
		getDesktopSeatsNamed() +  "#" +
		getWebliteSeatsConcurrent() +  "#" +
		getWebliteSeatsNamed() +  "#" +
		getWebtopSeatsConcurrent() +  "#" +
		getWebtopSeatsNamed() +  "#" +
		getArsSeats() +  "#" +
		getDrsSeats() +  "#" +
		getIndexerSeats() +  "#" +
		getAipSeats() +  "#" +
		getEasSeats() +  "#" +
		getEworkSeats() +  "#" +
		getSdkSeats()+  "#" +
		getNamedOnlineWebaccessSeats() +  "#" +
		getNotificationSeats() + "#" +
		getCustomLabelWebAccessSeats() + "#" +
		getProfessionalWebAccessSeats()+ "#" +
		getWebaccessSeatsConcurrent()+"#" +
		getProfessionalConcWebAccessSeats()+"#"+
		getCsserverSeats()+"#"+getCsswebaccessSeat()+"#"+
		getSubAdminSeats();
		
	}  
	public String getEmailAddress()    
	{
		return this.email;
	}
	public void setEmailAddress(String email)    
	{
		this.email=email;
	}
	public Date getExpirationDate() 
	{
		return this.expirationDate;
	}
	public String getExpirationDateString()
	{
		DateFormat df = DateFormat.getDateInstance(DateFormat.SHORT, Locale.US);
		return df.format(expirationDate);
	}
	public void setExpirationDate(String expirationDateStr) throws LicenseException
	{
		try 
		{
			DateFormat df = DateFormat.getDateInstance
			(DateFormat.SHORT, Locale.US);
			Date date = df.parse(expirationDateStr);
			this.expirationDate=date;
		}
		catch(Exception e)
		{
			throw new LicenseException(e.getMessage());
		}
	}
	public boolean isExpired()
	{
		return System.currentTimeMillis() > expirationDate.getTime();
	}
	public Vector removeOrphaned()
	{
		Vector all = new Vector();
		all.addAll(removeOrphaned(cdesktopSeatsConcurrent));
		all.addAll(removeOrphaned(cdesktopSeatsNamed));
		all.addAll(removeOrphaned(capplet1SeatsConcurrent));
		all.addAll(removeOrphaned(capplet1SeatsNamed));
		all.addAll(removeOrphaned(ceworkSeats));
		all.addAll(removeOrphaned(caipSeats));
		all.addAll(removeOrphaned(csdkSeats));
		all.addAll(removeOrphaned(cwebaccessSeats));
		all.addAll(removeOrphaned(cprofessionalWebaccessSeats));
		all.addAll(removeOrphaned(cconrWebaccessSeats));
		all.addAll(removeOrphaned(cprofconcrWebaccessSeats));		
		all.addAll(removeOrphaned(cnotificationSeats));
		all.addAll(removeOrphaned(ccssserverSeats));
		all.addAll(removeOrphaned(ccsswebaccessSeats));
		
		return all;
	}
	public Vector removeOrphaned(Hashtable clients)
	{
		Client client = null;
		Vector orphaned = new Vector();
		Enumeration eClients = clients.elements();
		while(eClients.hasMoreElements())
		{
			client = (Client) eClients.nextElement();
			ServerSchema ss = client.getSchema();
			/*
				Issue No / Purpose:  <650/Terminal Services Client>
				Created by: <Pandiya Raj.M>
				Date: <19 Jul 2006>
				Check that stubIPaddress is different from address, If it is different call is made from Terminal Service Client.
				Get the MDSS object using stubIPaddress.
			 */ 

			MDSS dss = null;
			//if(client.getClientType() != Client.Web_Client_Type){
			String address = ss.address;
			if (!ss.address.equalsIgnoreCase(ss.stubIPaddress) && ss.stubIPaddress.length()>0) {            	
				ss.address = ss.stubIPaddress;            	            	
			}
			dss = (MDSS) Util.getServer(ss);
			ss.address = address;
			/* Issue: Commenting the following code because in Novell Netware env it is not able to 
			 * get MDSS server and ping it. Because of this it is releasing and 
			 * reassigning the sessionId to prev Client 
			 **/
			/*	
            try
            {
                if (dss == null || (dss !=null && !dss.ping()))
                {
                    release(client);
                    orphaned.addElement(new Integer(client.getId()));
                }
            }
            catch(Exception e){}
			 */
			//}
		}
		return orphaned;
	}
	/*public boolean isAvailableSeat(Client client)
	{
		int type = client.getClientType();
		// client type included for ARS, 20 July 2006, Valli
		// DRS checking is enabled. so comment the following statement - PR  
		// if(type == Client.Drs_Client_Type )	return true;
		// Fix Start: Issue 483  - Check that if already DT connection made for named user. if yes  then no need to check that server seat.
		String key = getClientKey(client);
		if (type == Client.NFat_Client_Type) {
			if (cdesktopSeatsNamed.containsKey(key)) {
				if (cdesktopSeatsNamed.size() < desktopSeatsNamed 
						|| ipExists(cdesktopSeatsNamed, client)) return true;
			}
		}
		if (type == Client.Fat_Client_Type) {
			if (cdesktopSeatsConcurrent.containsKey(key)) {
				if (cdesktopSeatsConcurrent.size() < desktopSeatsConcurrent 
						|| ipExists(cdesktopSeatsConcurrent, client)) return true;
			}
		}        

		//Checking for WebTop Concurrent
		if (type == Client.Web_Client_Type) {
			if (cwebtopSeatsConcurrent.containsKey(key)) {
				if (cwebtopSeatsConcurrent.size() < webtopSeatsConcurrent 
						|| ipExists(cwebtopSeatsConcurrent, client)) return true;
			}
		}         

		//Checking for WebTop Named
		if (type == Client.NWeb_Client_Type) {
			if (cwebtopSeatsNamed.containsKey(key)) {
				if (cwebtopSeatsNamed.size() < webtopSeatsNamed
						|| ipExists(cwebtopSeatsNamed, client)) return true;
			}
		}         

		//Checking for WebLite Concurrent
		if (type == Client.WebL_Client_Type) {
			if (cwebliteSeatsConcurrent.containsKey(key)) {
				if (cwebliteSeatsConcurrent.size() < webliteSeatsConcurrent 
						|| ipExists(cwebliteSeatsConcurrent, client)) return true;
			}
		}         

		//Checking for WebLite Named
		if (type == Client.NWebL_Client_Type) {
			if (cwebliteSeatsNamed.containsKey(key)) {
				if (cwebliteSeatsNamed.size() < webliteSeatsNamed
						|| ipExists(cwebliteSeatsNamed, client)) return true;
			}
		}         

		//Checking for EAS
		if (type == Client.EAS_Client_Type) {
			if (ceasSeats.containsKey(key)) {
				if (ceasSeats.size() < easSeats
						|| ipExists(ceasSeats, client)) return true;
			}
		}            

		//Checking for AIP
		//if (type == Client.AIP_Client_Type) {  // commented by srikanth on 16 Sep 2015 for right fax consumption issue
		if ((type == Client.AIP_Client_Type) || (type == Client.Fax_Client_Type)) {
			if (caipSeats.containsKey(key)) {
				if (caipSeats.size() < aipSeats
						|| ipExists(caipSeats, client)) return true;
			}
		}            

		//Checking for Indexer
		if (type == Client.IXR_Client_Type) {
			if (cindexerSeats.containsKey(key)) {
				if (cindexerSeats.size() < indexerSeats
						|| ipExists(cindexerSeats, client)) return true;
			}
		}            

		// Fix End
		//Saad: Internal Exception...Change later    
		//Comment the indexer
		//if(type == Client.IXR_Client_Type) return true;
		if (cserverSeats.size() < serverSeats)
		{        	
			switch(type)
			{
			case Client.Fat_Client_Type:
				if (cdesktopSeatsConcurrent.size() < desktopSeatsConcurrent //) return true;
						|| ipExists(cdesktopSeatsConcurrent, client)) return true;
				break;
			case Client.NFat_Client_Type:
				if (cdesktopSeatsNamed.size() < desktopSeatsNamed //) return true; 
						|| ipExists(cdesktopSeatsNamed, client)) return true;
				break;
			case Client.Web_Client_Type:
				if (cwebtopSeatsConcurrent.size() < webtopSeatsConcurrent) return true;//if (capplet1SeatsConcurrent.size() < applet1SeatsConcurrent) return true;
				break;
			case Client.NWeb_Client_Type:
				if (cwebtopSeatsNamed.size() < webtopSeatsNamed) return true;//if (capplet1SeatsNamed.size() < applet1SeatsNamed) return true;
				break;
			case Client.WebL_Client_Type:
				if (cwebliteSeatsConcurrent.size() < webliteSeatsConcurrent) return true;//if (capplet1SeatsConcurrent.size() < applet1SeatsConcurrent) return true;
				break;
			case Client.NWebL_Client_Type:
				if (cwebliteSeatsNamed.size() < webliteSeatsNamed) return true;//if (capplet1SeatsNamed.size() < applet1SeatsNamed) return true;
				break;                    
			case Client.Adm_Client_Type:
				if (cadminWiseSeats.size() < adminWiseSeats) return true;
				break;
			case Client.IXR_Client_Type:
				if (cindexerSeats.size() < indexerSeats)  return true;
				break;
			case Client.AIP_Client_Type:
				if (caipSeats.size() < aipSeats)  return true;
				break;
			// case added by srikanth for right fax consumption issue on 16 Sep 2015
			case Client.Fax_Client_Type:
				if (caipSeats.size() < aipSeats)  return true;
				break;
			// case added by srikanth for right fax consumption issue on 16 Sep 2015				
			case Client.EAS_Client_Type:
				if (ceasSeats.size() < easSeats)  return true;
				break;
			case Client.Ewk_Client_Type:
				if (ceworkSeats.size() < eworkSeats)  return true;
				break;
			case Client.sdk_Client_Type:
				//if (csdkSeats.size() < sdkSeats)  return true;
				if (csdkSeats.size() < sdkSeats || ipExists(csdkSeats, client)) return true;
				break;       
			case Client.WebAccess_Client_Type:
				//if (csdkSeats.size() < sdkSeats)  return true;
				if (cwebaccessSeats.size() < webAccessSeats || ipExists(cwebaccessSeats, client)) return true;
				break;                     
			case Client.Notification_Client_Type:

				if (cnotificationSeats.size() < notificationSeats || ipExists(cnotificationSeats, client)) return true;	
				break;
			case Client.Concurrent_WebAccess_Client_Type:
				//if (csdkSeats.size() < sdkSeats)  return true;
				if (cconrWebaccessSeats.size() < webaccessSeatsConcurrent || ipExists(cconrWebaccessSeats, client)) return true;
				break;  
			case Client.Professional_Conc_WebAccess_Client_Type:
				//if (csdkSeats.size() < sdkSeats)  return true;
				if (cprofconcrWebaccessSeats.size() < professionalConcWebAccessSeats || ipExists(cprofconcrWebaccessSeats, client)) return true;
				break; 			
				
			case Client.Professional_WebAccess_Client_Type:
				if(cprofessionalWebaccessSeats.size()<professionalWebAccessSeats||ipExists(cprofessionalWebaccessSeats,client)) return true;
				break;
			case Client.Custom_Label_WebAccess_Client_Type:
				if(cCustomLabelWebaccessSeats.size()<professionalConcWebAccessSeats||ipExists(cCustomLabelWebaccessSeats,client)) return true;
				break;
				
			}
		}
		// these clients do not require server seats
		switch(type)
		{
		case Client.IXR_Client_Type:
			//if (caipSeats.size() < aipSeats) return true;
			if (cindexerSeats.size() < indexerSeats || ipExists(cindexerSeats, client)) return true;
			break;
		case Client.Ars_Client_Type:
			/* ARS should occupy only license seat for all the connected rooms and it does the same
			 * But when checking availableSeat for second room it is going wrong and gives 'No More License Seat'  
			 * user Name and ip address check was missing ARS client Type and added the same. Valli 29 Jan 2008	*/
			/*if (carsSeats.size() < arsSeats
					|| ipExists(carsSeats, client)) return true;
			//if (carsSeats.size() < arsSeats)  return true;
			break;
			// Added for DRS license seat    
		case Client.Drs_Client_Type:
			if (cdrsSeats.size() < drsSeats    
					|| ipExists(cdrsSeats, client)) return true;
			break; 
		case Client.Notification_Client_Type:

			if (cnotificationSeats.size() < notificationSeats || ipExists(cnotificationSeats, client)) return true;	
			break;                                  

		}
		return false;
	}*/

	public boolean isAvailableSeatNew(Client client){

		int type = client.getClientType();
		/**
		 * professionalSeats - is nothing but additional WebAccess named seats.
		 * So if a customer buys 50 users, then they automatically get 50 named web Access as well.
		 * Total WebAccess Seats = Number of Users + Professional Seats
		 * where, Number of Users - Number of named seats which is shared among Named DTC, Named WebTop and Named WebLite.
		 * 		  Professional Seats - Named web access seats which are having reserved server seats.
		 * For Named DTC, Named WebTop and Named WebLite check the seat availability with (Total server seats - professional seats).    	 * 
		 */
		int consumedWebAccessSeats = 0;
		int consumedProfessionalWebAccessSeats=0;
		int consumedProfConcrWebAccessSeats=0;
		
		
		if(getConsumed(Client.Professional_Conc_WebAccess_Client_Type) > professionalConcWebAccessSeats){
			consumedProfConcrWebAccessSeats = getConsumed(Client.Professional_Conc_WebAccess_Client_Type) ;
			
		}
		
		if(getConsumed(Client.Professional_WebAccess_Client_Type) > professionalWebAccessSeats){
			consumedProfessionalWebAccessSeats = getConsumed(Client.Professional_WebAccess_Client_Type) ;
			
		}
		if(getConsumed(Client.WebAccess_Client_Type) > webAccessSeats){
			consumedWebAccessSeats = getConsumed(Client.WebAccess_Client_Type) ;
		}
		
		
		int totalSharedConsumedSeats = getConsumed(Client.NFat_Client_Type) + getConsumed(Client.NWeb_Client_Type) + 
									getConsumed(Client.NWebL_Client_Type) + getConsumed(Client.sdk_Client_Type) + getConsumed(Client.WebAccess_Client_Type) ; // + consumedProfessionalWebAccessSeats;
		//Replaced serverSeats by getLicenseNamedGroupCount for license changes,Madhavan.B
		//if (totalSharedConsumedSeats < (getLicenseNamedGroupCount() - professionalWebAccessSeats) || isIPExists(client))
		//For totalsharedconsumedseats=maxnamedcount-professionl fix - professionalWebAccessSeats added extra.
		if ((totalSharedConsumedSeats < getLicenseNamedGroupCount())&&(cserverSeats.size() < serverSeats) || isIPExists(client))
		{
			switch(type)
			{
			case Client.NFat_Client_Type:
				if (cdesktopSeatsNamed.size() < desktopSeatsNamed //) return true; 
						|| ipExists(cdesktopSeatsNamed, client)) return true;
				break;
			case Client.NWeb_Client_Type:
				if (cwebtopSeatsNamed.size() < webtopSeatsNamed
						|| ipExists(cwebtopSeatsNamed, client)) return true;//if (capplet1SeatsNamed.size() < applet1SeatsNamed) return true;
				break;
			case Client.NWebL_Client_Type:
				if (cwebliteSeatsNamed.size() < webliteSeatsNamed
						|| ipExists(cwebliteSeatsNamed, client)) return true;//if (capplet1SeatsNamed.size() < applet1SeatsNamed) return true;
				break;                    
			case Client.sdk_Client_Type:
				if (csdkSeats.size() < desktopSeatsNamed || ipExists(csdkSeats, client)) return true;
				break;        
			}
		}
		
		int totalSharedConsumedConcrSeats = getConsumed(Client.Fat_Client_Type) + getConsumed(Client.Web_Client_Type) + 
				getConsumed(Client.WebL_Client_Type) /*+ getConsumed(Client.Concurrent_WebAccess_Client_Type)*/ ;
		//VWSLog.add("totalSharedConsumedConcrSeats :::::"+totalSharedConsumedConcrSeats);
		//VWSLog.add("getLicenseConcurrentGrouptCount() :::::"+getLicenseConcurrentGrouptCount());
		if ((totalSharedConsumedConcrSeats < getLicenseConcurrentGrouptCount())&&(cserverSeats.size() < serverSeats) || isIPExists(client))
		{
			switch(type)
			{
			case Client.Fat_Client_Type:
				if (cdesktopSeatsConcurrent.size() < desktopSeatsConcurrent //) return true; 
						|| ipExists(cdesktopSeatsConcurrent, client)) return true;
				break;
			case Client.NWeb_Client_Type:
				if (cwebtopSeatsConcurrent.size() < webtopSeatsConcurrent
						|| ipExists(cwebtopSeatsConcurrent, client)) return true;//if (capplet1SeatsNamed.size() < applet1SeatsNamed) return true;
				break;
			case Client.NWebL_Client_Type:
				if (cwebliteSeatsConcurrent.size() < webliteSeatsConcurrent
						|| ipExists(cwebliteSeatsConcurrent, client)) return true;//if (capplet1SeatsNamed.size() < applet1SeatsNamed) return true;
				break;        
		/*	case Client.Concurrent_WebAccess_Client_Type:
				if (cconrWebaccessSeats.size() < webaccessSeatsConcurrent || ipExists(cconrWebaccessSeats, client)) return true;
				break;  
			case Client.Professional_Conc_WebAccess_Client_Type:
				if (cprofconcrWebaccessSeats.size() < professionalConcWebAccessSeats || ipExists(cprofconcrWebaccessSeats, client)) return true;
				break;      */  
			}
		}
		
		/**
		 * For WebAccess client type check seat availability with total number of server seats.
		 */
		//Replaced serverSeats by getLicenseNamedGroupCount for license changes,Madhavan.B
		//if(type == Client.WebAccess_Client_Type && (cserverSeats.size() < (getLicenseNamedGroupCount()) || isIPExists(client))){
		//For totalSharedConsumedSeats < getLicenseNamedGroupCount()- professionalWebAccessSeats fix - professionalWebAccessSeats added extra.
		/*if(type == Client.WebAccess_Client_Type && ((totalSharedConsumedSeats < getLicenseNamedGroupCount())&& (cserverSeats.size() <getServerSeats())|| isIPExists(client))){
			if (cwebaccessSeats.size() < (webAccessSeats) || ipExists(cwebaccessSeats, client)) 
				return true;	
		}*/

		if(type == Client.Professional_WebAccess_Client_Type && (cprofessionalWebaccessSeats.size() < getLicenseProfessionalNamedGroupCount())&& (cserverSeats.size() <getServerSeats()) || isIPExists(client)){
			if (cprofessionalWebaccessSeats.size() < (professionalWebAccessSeats) || ipExists(cprofessionalWebaccessSeats, client)) 
				return true;	
		}
		
		if(type == Client.Concurrent_WebAccess_Client_Type && (cconrWebaccessSeats.size() < getLicenseConcurrentOnlineGrouptCount())&& (cserverSeats.size() <getServerSeats()) || isIPExists(client)){
			if (cconrWebaccessSeats.size() < (webaccessSeatsConcurrent) || ipExists(cconrWebaccessSeats, client)) 
				return true;	
		}

		if(type == Client.NamedOnline_WebAccess_Client_Type && (cnamedOnlineWebaccessSeats.size() < getLicenseNamedOnlineWebaccessSeats()&& (cserverSeats.size() <getServerSeats()) || isIPExists(client))){
			if (cnamedOnlineWebaccessSeats.size() < (namedOnlineWebaccessSeats) || ipExists(cnamedOnlineWebaccessSeats, client)) 
				return true;	
		}
		if(type == Client.Professional_Conc_WebAccess_Client_Type && (cprofconcrWebaccessSeats.size() < getLicenseProfessionalConcurrentGroupCount())&& (cserverSeats.size() <getServerSeats()) || isIPExists(client)){
			if (cprofconcrWebaccessSeats.size() < (professionalConcWebAccessSeats) || ipExists(cprofconcrWebaccessSeats, client)) 
				return true;	
		}
		//CV10.1 Condition newly added for customlabel ip check
		if(type == Client.Custom_Label_WebAccess_Client_Type && (cCustomLabelWebaccessSeats.size() < getLicenseCustomLabelCount())&& (cserverSeats.size() <getServerSeats()) || isIPExists(client)){
			if (cCustomLabelWebaccessSeats.size() < (customLabelWebAccessSeats) || ipExists(cCustomLabelWebaccessSeats, client)) 
				return true;	
		}
		
		// these clients do not require server seats
		switch(type)
		{
		/*case Client.Fat_Client_Type:
			//Added the consument server seats<searverseats for concurrent dtc licence Modified for CV8.3.1
			if (((cdesktopSeatsConcurrent.size() < desktopSeatsConcurrent)&&(cserverSeats.size() < serverSeats))
					|| ipExists(cdesktopSeatsConcurrent, client)){
				VWSLog.add("test consument dtc :::::::::::::4444444444");
				return true;
			}
			break;*/
		case Client.Web_Client_Type:
			if (cwebtopSeatsConcurrent.size() < webtopSeatsConcurrent
					|| ipExists(cwebtopSeatsConcurrent, client)) return true;
			break;
		case Client.WebL_Client_Type:
			if (cwebliteSeatsConcurrent.size() < webliteSeatsConcurrent
					|| ipExists(cwebliteSeatsConcurrent, client)) return true;
			break;
		case Client.Adm_Client_Type:
			if (cadminWiseSeats.size() < adminWiseSeats
					|| ipExists(cadminWiseSeats, client)) return true;
			break;
		case Client.IXR_Client_Type:
			if (cindexerSeats.size() < indexerSeats
					|| ipExists(cindexerSeats, client))  return true;
			break;
		case Client.AIP_Client_Type:
			if (caipSeats.size() < aipSeats
					|| ipExists(caipSeats, client))  return true;
			break;
		case Client.EAS_Client_Type:
			if (ceasSeats.size() < easSeats
					|| ipExists(ceasSeats, client))  return true;
			break;            
		case Client.Ars_Client_Type:
			if (carsSeats.size() < arsSeats
					|| ipExists(carsSeats, client))  return true;
			break; 
		case Client.Drs_Client_Type:
			if (cdrsSeats.size() < drsSeats
					|| ipExists(cdrsSeats, client))  return true;
			break; 
		case Client.Notification_Client_Type:     	
			if (cnotificationSeats.size() < notificationSeats || ipExists(cnotificationSeats, client)) return true;	
			break;
			
		case Client.ContentSentinel_client_Type:
			if (ccssserverSeats.size() < csserverSeats || ipExists(ccssserverSeats, client)) return true;	
			break;
			
		case Client.CSWebaccess_client_Type:
			if (ccsswebaccessSeats.size() < csswebaccessSeat /*|| ipExists(ccsswebaccessSeats, client)*/) return true;	
			break;
		
			
		case Client.Custom_Label_WebAccess_Client_Type:
			/* For Custom label web access, 1 seat = 25 concurrent users. i.e. group of 25 users will consume 1 web access seat.*/
			int consumedCustomLabelWebAccessSeats = 0;
			if(cCustomLabelWebaccessSeats != null && cCustomLabelWebaccessSeats.size() > 0){
				//consumedCustomLabelWebAccessSeats = cCustomLabelWebaccessSeats.size()/25;  // change to 25..
				//Change to one customLabelWebAcess is equal to one instead of 25
				consumedCustomLabelWebAccessSeats = cCustomLabelWebaccessSeats.size();
			}
			//Added the consument server seats<searverseats for concurrent dtc licence Modified for CV8.3.1
			if(((consumedCustomLabelWebAccessSeats < customLabelWebAccessSeats)&&(cserverSeats.size() < serverSeats)) || ipExists(cCustomLabelWebaccessSeats, client)) {
				return true;
			}
			break;
		case Client.Fax_Client_Type:
			if (caipSeats.size() < aipSeats	|| ipExists(caipSeats, client))  return true;
			break;
		case Client.SubAdm_Client_Type:
			if (csubAdminWiseSeats.size() < subAdminSeats
					|| ipExists(csubAdminWiseSeats, client)) return true;
			break;
		}
		return false;
	}

	public boolean isIPExists(Client client) {
		try{
			int type = client.getClientType();
			switch(type){
			case Client.NFat_Client_Type: 
				if(ipExists(cdesktopSeatsNamed, client)) return true; 
				break;
			case Client.NWeb_Client_Type: 
				if(ipExists(cwebtopSeatsNamed, client)) return true; 
				break;	
			case Client.NWebL_Client_Type: 
				if(ipExists(cwebliteSeatsNamed, client)) return true; 
				break;
			case Client.sdk_Client_Type: 
				if(ipExists(csdkSeats, client)) return true; 
				break;
			case Client.WebAccess_Client_Type: 
				if(ipExists(cwebaccessSeats, client)) 
					return true; 
				break;
			case Client.Fat_Client_Type:
				if(ipExists(cdesktopSeatsConcurrent,client))return true;
				break;
			case Client.Custom_Label_WebAccess_Client_Type:
				if(ipExists(cCustomLabelWebaccessSeats, client)) return true; 
				break;
			case Client.Professional_WebAccess_Client_Type:
				if(ipExists(cprofessionalWebaccessSeats, client)) return true; 
				break;
			case Client.Professional_Conc_WebAccess_Client_Type: 
				if(ipExists(cprofconcrWebaccessSeats, client)) return true; 
				break;
			case Client.Concurrent_WebAccess_Client_Type: 
				if(ipExists(cconrWebaccessSeats, client)) return true; 
				break;
			case Client.NamedOnline_WebAccess_Client_Type:
				 if(ipExists(cnamedOnlineWebaccessSeats,client))return true;
			/*case Client.CSWebaccess_client_Type:
				if(ipExists(ccsswebaccessSeats, client));*/
			}
		}catch (Exception e) {
			System.out.println("Exception while checking isIPExists : "+e.getMessage());
		}
		return false;
	}

	public int getConsumed(int type)
	{
		switch (type)
		{
		case Client.All_Client_Type:
			return cserverSeats.size();
		case Client.Adm_Client_Type:
			return cadminWiseSeats.size();

		case Client.Fat_Client_Type:
			return cdesktopSeatsConcurrent.size();
		case Client.NFat_Client_Type:
			return cdesktopSeatsNamed.size();
		case Client.Web_Client_Type:
			return cwebtopSeatsConcurrent.size();
		case Client.NWeb_Client_Type:
			return cwebtopSeatsNamed.size();
		case Client.WebL_Client_Type:
			return cwebliteSeatsConcurrent.size();
		case Client.NWebL_Client_Type:
			return cwebliteSeatsNamed.size();
		case Client.IXR_Client_Type:
			return cindexerSeats.size();
		case Client.Ars_Client_Type:
			return carsSeats.size();
		case Client.EAS_Client_Type:
			return ceasSeats.size();
		case Client.AIP_Client_Type:
			return caipSeats.size();
			// Added for DRS license seat                
		case Client.Drs_Client_Type:
			return cdrsSeats.size();
		case Client.sdk_Client_Type:
			return csdkSeats.size();    
		case Client.Ewk_Client_Type:
			return ceworkSeats.size();
		case Client.WebAccess_Client_Type:
			return cwebaccessSeats.size();      
		case Client.Professional_WebAccess_Client_Type:
			return cprofessionalWebaccessSeats.size();   
		case Client.Notification_Client_Type:
			return cnotificationSeats.size();
		case Client.Custom_Label_WebAccess_Client_Type:
			/* For Custom label web access, 1 seat = 25 concurrent users. i.e. group of 25 users will consume 1 web access seat.*/
			return cCustomLabelWebaccessSeats.size();
		case Client.Concurrent_WebAccess_Client_Type:
			return cconrWebaccessSeats.size();
		case Client.NamedOnline_WebAccess_Client_Type:
			return cnamedOnlineWebaccessSeats.size();
		case Client.Professional_Conc_WebAccess_Client_Type:
			return cprofconcrWebaccessSeats.size();
		case Client.ContentSentinel_client_Type:
			return ccssserverSeats.size();
		case Client.CSWebaccess_client_Type:
			return ccsswebaccessSeats.size();
		case Client.SubAdm_Client_Type:
			return csubAdminWiseSeats.size();
			
			
		}
		return 0;
	}  
	public void consume(Client client) 
	{
		switch (client.getClientType())
		{
		case Client.Fat_Client_Type:
			addSeat(cdesktopSeatsConcurrent, client);
			break;
		case Client.NFat_Client_Type:
			addSeat(cdesktopSeatsNamed, client);
			break;
		case Client.Web_Client_Type:
			addSeat(cwebtopSeatsConcurrent, client);
			break;
		case Client.NWeb_Client_Type:
			addSeat(cwebtopSeatsNamed, client);  
			break;
		case Client.WebL_Client_Type:
			addSeat(cwebliteSeatsConcurrent, client);
			break;
		case Client.NWebL_Client_Type:
			addSeat(cwebliteSeatsNamed, client);  
			break;
		case Client.Adm_Client_Type:
			addSeat(cadminWiseSeats, client);
			break;
		case Client.AIP_Client_Type:
			addSeat(caipSeats, client);
			break;
		case Client.EAS_Client_Type:
			addSeat(ceasSeats, client);
			break;
		case Client.IXR_Client_Type:
			addSeat(cindexerSeats, client);
			break;
		case Client.Ars_Client_Type:
			addSeat(carsSeats, client);
			break;
			// Added for DRS license seat
		case Client.Drs_Client_Type:
			addSeat(cdrsSeats, client);
			break;


		case Client.Ewk_Client_Type:
			addSeat(ceworkSeats, client);
			break;
		case Client.sdk_Client_Type:
			addSeat(csdkSeats, client);  
			break;    
		case Client.WebAccess_Client_Type:
			addSeat(cwebaccessSeats, client);  
			break;     
		case Client.Professional_WebAccess_Client_Type:
			addSeat(cprofessionalWebaccessSeats, client);  
			break; 
		case Client.Concurrent_WebAccess_Client_Type:
			addSeat(cconrWebaccessSeats, client);  
			break; 
		case Client.Notification_Client_Type:
			addSeat(cnotificationSeats, client);  
			break;   
		case Client.SubAdm_Client_Type:
			addSeat(csubAdminWiseSeats, client);
			break;
		}
	}

	public void consumeNew(Client client) 
	{
		switch (client.getClientType())
		{
		case Client.NFat_Client_Type:
			addSeatNew(cdesktopSeatsNamed, client);
			break;
		case Client.NWeb_Client_Type:
			addSeatNew(cwebtopSeatsNamed, client);  
			break;
		case Client.NWebL_Client_Type:
			addSeatNew(cwebliteSeatsNamed, client);  
			break;
		case Client.Fat_Client_Type:
			addSeatNew(cdesktopSeatsConcurrent, client);
			break;
		case Client.Web_Client_Type:
			addSeatNew(cwebtopSeatsConcurrent, client);
			break;
		case Client.WebL_Client_Type:
			addSeatNew(cwebliteSeatsConcurrent, client);
			break;
		case Client.Adm_Client_Type:
			addSeatNew(cadminWiseSeats, client);
			break;
		case Client.AIP_Client_Type:
			addSeatNew(caipSeats, client);
			break;
		case Client.EAS_Client_Type:
			addSeatNew(ceasSeats, client);
			break;
		case Client.IXR_Client_Type:
			addSeatNew(cindexerSeats, client);
			break;
		case Client.Ars_Client_Type:
			addSeatNew(carsSeats, client);
			break;
			// Added for DRS license seat 
		case Client.Drs_Client_Type:
			addSeatNew(cdrsSeats, client);
			break;
		case Client.sdk_Client_Type:
			addSeatNew(csdkSeats, client);  
			break;    
		case Client.WebAccess_Client_Type:
			addSeatNew(cwebaccessSeats, client);  
			break;
		case Client.Professional_WebAccess_Client_Type:
			addSeatNew(cprofessionalWebaccessSeats, client);  
			break; 
		case Client.Notification_Client_Type:
			addSeatNew(cnotificationSeats, client);  
			break;
		case Client.Custom_Label_WebAccess_Client_Type:
			addSeatNew(cCustomLabelWebaccessSeats, client);  
			break; 
		case Client.Fax_Client_Type:
			addSeatNew(caipSeats, client);
			break;
		case Client.Concurrent_WebAccess_Client_Type:
			addSeatNew(cconrWebaccessSeats, client);
			break;
		case Client.NamedOnline_WebAccess_Client_Type:
			addSeatNew(cnamedOnlineWebaccessSeats, client);
			break;
			
		case Client.Professional_Conc_WebAccess_Client_Type:
			addSeatNew(cprofconcrWebaccessSeats, client);
			break;
			
		case Client.ContentSentinel_client_Type:
			addSeatNew(ccssserverSeats, client);
			break;
		case Client.CSWebaccess_client_Type:
			addSeatNew(ccsswebaccessSeats, client);
			break;
		case Client.SubAdm_Client_Type:
			addSeatNew(csubAdminWiseSeats, client);
			break;
		}
	}

	private String getClientKey(Client client)
	{
		String ip = client.getIpAddress();
		int type = client.getClientType();
		// Add the user name in the key. For same IP and same client type and same user it will not consume more than one seat.
		String userName = client.getUserName();
		if(client.getClientType()==client.CSWebaccess_client_Type) {
			return ip + "|" + type + "|" + userName.toLowerCase()+"|"+client.getActualUserName();
		}else {
			return ip + "|" + type + "|" + userName.toLowerCase();
		}
	}
	private void addSeat(Hashtable clients, Client client)
	{
		String key = getClientKey(client);
		if (!clients.containsKey(key)) 
			clients.put(key, client);
		else{
			((Client) clients.get(key)).incrementInstance();

			if (client.getClientType() == Client.sdk_Client_Type){
				int instanceCount =((Client) clients.get(key)).getInstance();
				key += "|" + instanceCount;
				clients.put(key, client);
				//VWSLog.add("Add SDK Seat " + instanceCount + " : " + key + " : " + clients.toString());
			}            	
		}
		/*
		 *  6-28-05 requested by Joe
		 *  Only those clients will consume a server seat
		 */

		/*        if ( client.getClientType() == Client.Fat_Client_Type  ||
             client.getClientType() == Client.Web_Client_Type  ||
             client.getClientType() == Client.Adm_Client_Type  ||
             client.getClientType() == Client.NFat_Client_Type ||
             client.getClientType() == Client.NWeb_Client_Type )
		 */
		if (client.getClientType() != Client.Ars_Client_Type &&
				client.getClientType() != Client.Drs_Client_Type &&
				client.getClientType() != Client.IXR_Client_Type)
		{
			if (!cserverSeats.containsKey(key))
				cserverSeats.put(key, client);
			else
				((Client) cserverSeats.get(key)).incrementInstance();
		}
	}

	private void addSeatNew(Hashtable clients, Client client)
	{
		String key = getClientKey(client);
		if (!clients.containsKey(key)) 
			clients.put(key, client);
		else
			((Client) clients.get(key)).incrementInstance();
		/**Added the FatClient_type and customlabel to consume the server seats for 
		 * Destopconcurrent and CustomLabelWebAccess CV8.3.1
		**/
		if (client.getClientType() == Client.NFat_Client_Type  ||
				client.getClientType() == Client.NWeb_Client_Type  ||
				client.getClientType() == Client.NWebL_Client_Type ||			
				client.getClientType() == Client.WebAccess_Client_Type ||
				client.getClientType() == Client.Professional_WebAccess_Client_Type ||
				client.getClientType() == Client.sdk_Client_Type||
				client.getClientType() == Client.Fat_Client_Type||
				client.getClientType() == Client.Custom_Label_WebAccess_Client_Type||
				client.getClientType()==Client.Concurrent_WebAccess_Client_Type||
				client.getClientType()==Client.NamedOnline_WebAccess_Client_Type||
				client.getClientType()==Client.Professional_Conc_WebAccess_Client_Type||
				client.getClientType()==Client.Professional_WebAccess_Client_Type
				)
		{
			if (!cserverSeats.containsKey(key))
				cserverSeats.put(key, client);
			else
				((Client) cserverSeats.get(key)).incrementInstance();
		}
	}

	private void removeSeat(Hashtable clients, Client client)
	{
		String key = getClientKey(client);
		if (clients.containsKey(key))
		{
			if ( ((Client) clients.get(key)).getInstance() == 0 )
				clients.remove(key);
			else{
				int instanceCount = ((Client) clients.get(key)).getInstance();
				((Client) clients.get(key)).decrementInstance();
				if (client.getClientType() == Client.sdk_Client_Type){

					key += "|" + instanceCount;
					//VWSLog.add("Remove SDK Seat " + instanceCount + " : " + key + " : " + clients.toString());
					clients.remove(key);
				}            	

			}

		}
		if (cserverSeats.containsKey(key)) 
		{
			if ( ((Client) cserverSeats.get(key)).getInstance() == 0 )
				cserverSeats.remove(key);
			else
				((Client) cserverSeats.get(key)).decrementInstance();
		}
	}
	public void release(Client client)
	{ 
		switch (client.getClientType())
		{
		case Client.Fat_Client_Type:
			removeSeat(cdesktopSeatsConcurrent, client);
			break;
		case Client.Web_Client_Type:
			removeSeat(cwebtopSeatsConcurrent, client);
			break;
		case Client.Adm_Client_Type:
			removeSeat(cadminWiseSeats, client);
			break;
		case Client.AIP_Client_Type:
			removeSeat(caipSeats, client);
			break;
		case Client.Ewk_Client_Type:
			removeSeat(ceworkSeats, client);
			break;
		case Client.NFat_Client_Type:
			removeSeat(cdesktopSeatsNamed, client);
			break;
		case Client.NWeb_Client_Type:
			removeSeat(cwebtopSeatsNamed, client);  
			break;
		case Client.sdk_Client_Type:   
			removeSeat(csdkSeats, client);  
			break;
		case Client.WebL_Client_Type:   
			removeSeat(cwebliteSeatsConcurrent, client);  
			break;
		case Client.NWebL_Client_Type:   
			removeSeat(cwebliteSeatsNamed, client);  
			break;
		case Client.EAS_Client_Type:   
			removeSeat(ceasSeats, client);  
			break;
		case Client.IXR_Client_Type:   
			removeSeat(cindexerSeats, client);  
			break;
		case Client.Ars_Client_Type:   
			removeSeat(carsSeats, client);  
			break;
			// Added for DRS license seat    
		case Client.Drs_Client_Type:   
			removeSeat(cdrsSeats, client);  
			break;
		case Client.WebAccess_Client_Type:   
			removeSeat(cwebaccessSeats, client);  
			break;                
		case Client.Notification_Client_Type:   
			removeSeat(cnotificationSeats, client);  
			break;
		case Client.Custom_Label_WebAccess_Client_Type:
			removeSeat(cCustomLabelWebaccessSeats, client);
			break;    
		case Client.Professional_WebAccess_Client_Type:
			removeSeat(cprofessionalWebaccessSeats, client);
			break;
		case Client.Fax_Client_Type:
			removeSeat(caipSeats, client);
			break;
		case Client.Concurrent_WebAccess_Client_Type:
			removeSeat(cconrWebaccessSeats, client);
			break;
			
		case Client.Professional_Conc_WebAccess_Client_Type:
			removeSeat(cprofconcrWebaccessSeats, client);
			break;
		case Client.ContentSentinel_client_Type:
			removeSeat(ccssserverSeats,client);
			break;
		case Client.CSWebaccess_client_Type:
			removeSeat(ccsswebaccessSeats,client);
			break;
		case Client.NamedOnline_WebAccess_Client_Type:
			removeSeat(cnamedOnlineWebaccessSeats,client);
			break;
		case Client.SubAdm_Client_Type:
			removeSeat(csubAdminWiseSeats, client);
			break;
		}
	}
	private boolean ipExists(Hashtable clients, Client client)
	{
		String ip = client.getIpAddress();
		String user = client.getUserName();
		int type = client.getClientType();  //Code added by srikanth on sep 16 for right fax consumption issue
		Iterator it = clients.values().iterator();
		while (it.hasNext())
		{
			Client c = (Client) it.next();
			// Checking the username and IP address
			//below line commented and added if else code by srikanth on sep 16 for right fax consumption issue
			//if (c.getIpAddress().equalsIgnoreCase(ip) && c.getUserName().equals(user)) return true;
			
			if (c.getClientType() == 21 || c.getClientType() == 4) {
				if (c.getIpAddress().equalsIgnoreCase(ip) && c.getUserName().equalsIgnoreCase(user)&& c.getClientType()== type){
					return true;
				}
			}else {
				//CV10.1 u
				if (c.getIpAddress().equalsIgnoreCase(ip) && c.getUserName().equalsIgnoreCase(user)){
					return true;	
				}
			}
			
		}
		return false;
	}
	public int getServerSeats() 
	{
		return serverSeats;
	}
	public void setServerSeats(int serverSeats) 
	{
		this.serverSeats = serverSeats;
	}
	public int getAdminWiseSeats() 
	{
		return adminWiseSeats;
	}
	public void setAdminWiseSeats(int adminWiseSeats) 
	{
		this.adminWiseSeats = adminWiseSeats;
	}

	/** Getter for property desktopSeatsConcurrent.
	 * @return Value of property desktopSeatsConcurrent.
	 */
	public int getDesktopSeatsConcurrent() {
		return desktopSeatsConcurrent;
	}

	/** Setter for property desktopSeatsConcurrent.
	 * @param desktopSeatsConcurrent New value of property desktopSeatsConcurrent.
	 */
	public void setDesktopSeatsConcurrent(int desktopSeatsConcurrent) {
		this.desktopSeatsConcurrent = desktopSeatsConcurrent;
	}

	/** Getter for property desktopSeatsNamed.
	 * @return Value of property desktopSeatsNamed.
	 */
	public int getDesktopSeatsNamed() {
		return desktopSeatsNamed;
	}

	/** Setter for property desktopSeatsNamed.
	 * @param desktopSeatsNamed New value of property desktopSeatsNamed.
	 */
	public void setDesktopSeatsNamed(int desktopSeatsNamed) {
		this.desktopSeatsNamed = desktopSeatsNamed;
	}

	/** Getter for property applet1SeatsConcurrent.
	 * @return Value of property applet1SeatsConcurrent.
	 */
	public int getApplet1SeatsConcurrent() {
		return applet1SeatsConcurrent;
	}

	/** Setter for property applet1SeatsConcurrent.
	 * @param applet1SeatsConcurrent New value of property applet1SeatsConcurrent.
	 */
	public void setApplet1SeatsConcurrent(int applet1SeatsConcurrent) {
		this.applet1SeatsConcurrent = applet1SeatsConcurrent;
	}

	/** Getter for property applet2SeatsConcurrent.
	 * @return Value of property applet2SeatsConcurrent.
	 */
	public int getApplet2SeatsConcurrent() {
		return applet2SeatsConcurrent;
	}

	/** Setter for property applet2SeatsConcurrent.
	 * @param applet2SeatsConcurrent New value of property applet2SeatsConcurrent.
	 */
	public void setApplet2SeatsConcurrent(int applet2SeatsConcurrent) {
		this.applet2SeatsConcurrent = applet2SeatsConcurrent;
	}

	/** Getter for property applet3SeatsConcurrent.
	 * @return Value of property applet3SeatsConcurrent.
	 */
	public int getApplet3SeatsConcurrent() {
		return applet3SeatsConcurrent;
	}

	/** Setter for property applet3SeatsConcurrent.
	 * @param applet3SeatsConcurrent New value of property applet3SeatsConcurrent.
	 */
	public void setApplet3SeatsConcurrent(int applet3SeatsConcurrent) {
		this.applet3SeatsConcurrent = applet3SeatsConcurrent;
	}

	/** Getter for property applet1SeatsNamed.
	 * @return Value of property applet1SeatsNamed.
	 */
	public int getApplet1SeatsNamed() {
		return applet1SeatsNamed;
	}

	/** Setter for property applet1SeatsNamed.
	 * @param applet1SeatsNamed New value of property applet1SeatsNamed.
	 */
	public void setApplet1SeatsNamed(int applet1SeatsNamed) {
		this.applet1SeatsNamed = applet1SeatsNamed;
	}

	/** Getter for property applet2SeatsNamed.
	 * @return Value of property applet2SeatsNamed.
	 */
	public int getApplet2SeatsNamed() {
		return applet2SeatsNamed;
	}

	/** Setter for property applet2SeatsNamed.
	 * @param applet2SeatsNamed New value of property applet2SeatsNamed.
	 */
	public void setApplet2SeatsNamed(int applet2SeatsNamed) {
		this.applet2SeatsNamed = applet2SeatsNamed;
	}

	/** Getter for property applet3SeatsNamed.
	 * @return Value of property applet3SeatsNamed.
	 */
	public int getApplet3SeatsNamed() {
		return applet3SeatsNamed;
	}

	/** Setter for property applet3SeatsNamed.
	 * @param applet3SeatsNamed New value of property applet3SeatsNamed.
	 */
	public void setApplet3SeatsNamed(int applet3SeatsNamed) {
		this.applet3SeatsNamed = applet3SeatsNamed;
	}

	/** Getter for property indexerSeats.
	 * @return Value of property indexerSeats.
	 */
	public int getIndexerSeats() {
		return indexerSeats;
	}

	/** Setter for property indexerSeats.
	 * @param indexerSeats New value of property indexerSeats.
	 */
	public void setIndexerSeats(int indexerSeats) {
		this.indexerSeats = indexerSeats;
	}

	/** Getter for property aipSeats.
	 * @return Value of property aipSeats.
	 */
	public int getAipSeats() {
		return aipSeats;
	}

	/** Setter for property aipSeats.
	 * @param aipSeats New value of property aipSeats.
	 */
	public void setAipSeats(int aipSeats) {
		this.aipSeats = aipSeats;
	}

	/** Getter for property easSeats.
	 * @return Value of property easSeats.
	 */
	public int getEasSeats() {
		return easSeats;
	}

	/** Setter for property easSeats.
	 * @param easSeats New value of property easSeats.
	 */
	public void setEasSeats(int easSeats) {
		this.easSeats = easSeats;
	}

	/** Getter for property eworkSeats.
	 * @return Value of property eworkSeats.
	 */
	public int getEworkSeats() {
		return eworkSeats;
	}

	/** Setter for property eworkSeats.
	 * @param eworkSeats New value of property eworkSeats.
	 */
	public void setEworkSeats(int eworkSeats) {
		this.eworkSeats = eworkSeats;
	}

	/** Getter for property sdkSeats.
	 * @return Value of property sdkSeats.
	 */
	public int getSdkSeats() {
		return sdkSeats;
	}

	/** Setter for property sdkSeats.
	 * @param sdkSeats New value of property sdkSeats.
	 */
	public void setSdkSeats(int sdkSeats) {
		this.sdkSeats = sdkSeats;
	}
	/**
	 * @return Returns the arsSeats.
	 */
	public int getArsSeats() {
		return arsSeats;
	}
	/**
	 * @param arsSeats The arsSeats to set.
	 */
	public void setArsSeats(int arsSeats) {
		this.arsSeats = arsSeats;
	}
	/**
	 * @return Returns the webliteSeatsConcurrent.
	 */
	public int getWebliteSeatsConcurrent() {
		return webliteSeatsConcurrent;
	}
	/**
	 * @param webliteSeatsConcurrent The webliteSeatsConcurrent to set.
	 */
	public void setWebliteSeatsConcurrent(int webliteSeatsConcurrent) {
		this.webliteSeatsConcurrent = webliteSeatsConcurrent;
	}
	/**
	 * @return Returns the webliteSeatsNamed.
	 */
	public int getWebliteSeatsNamed() {
		return webliteSeatsNamed;
	}
	/**
	 * @param webliteSeatsNamed The webliteSeatsNamed to set.
	 */
	public void setWebliteSeatsNamed(int webliteSeatsNamed) {
		this.webliteSeatsNamed = webliteSeatsNamed;
	}
	/**
	 * @return Returns the webtopSeatsConcurrent.
	 */
	public int getWebtopSeatsConcurrent() {
		return webtopSeatsConcurrent;
	}
	/**
	 * @param webtopSeatsConcurrent The webtopSeatsConcurrent to set.
	 */
	public void setWebtopSeatsConcurrent(int webtopSeatsConcurrent) {
		this.webtopSeatsConcurrent = webtopSeatsConcurrent;
	}
	/**
	 * @return Returns the webtopSeatsNamed.
	 */
	public int getWebtopSeatsNamed() {
		return webtopSeatsNamed;
	}
	/**
	 * @param webtopSeatsNamed The webtopSeatsNamed to set.
	 */
	public void setWebtopSeatsNamed(int webtopSeatsNamed) {
		this.webtopSeatsNamed = webtopSeatsNamed;
	}
	/**
	 * @return Returns the drsSeats.
	 */
	public int getDrsSeats() {
		return drsSeats;
	}
	/**
	 * @param drsSeats The drsSeats to set.
	 */
	public void setDrsSeats(int drsSeats) {
		this.drsSeats = drsSeats;
	}
	/**
	 * @return the webAccessSeats
	 */
	public int getWebAccessSeats() {
		return webAccessSeats;
	}
	/**
	 * @param webAccessSeats the webAccessSeats to set
	 */
	public void setWebAccessSeats(int webAccessSeats) {
		this.webAccessSeats = webAccessSeats;		
	}
	public int getNotificationSeats() {
		return notificationSeats;
	}
	public void setNotificationSeats(int notificationSeats) {
		this.notificationSeats = notificationSeats;
	}
	public int getCustomLabelWebAccessSeats() {
		return customLabelWebAccessSeats;
	}
	public void setCustomLabelWebAccessSeats(int customLabelWebAccessSeats) {
		this.customLabelWebAccessSeats = customLabelWebAccessSeats;
	}
	public int getProfessionalWebAccessSeats() {
		return professionalWebAccessSeats;
	}
	public void setProfessionalWebAccessSeats(int professionalWebAccessSeats) {
		this.professionalWebAccessSeats = professionalWebAccessSeats;
	}			  
	public int getWebaccessSeatsConcurrent() {
		return webaccessSeatsConcurrent;
	}
	public void setWebaccessSeatsConcurrent(int webaccessSeatsConcurrent) {
		this.webaccessSeatsConcurrent = webaccessSeatsConcurrent;
	}
	public int getProfessionalConcWebAccessSeats() {
		return professionalConcWebAccessSeats;
	}
	public void setProfessionalConcWebAccessSeats(
			int professionalConcWebAccessSeats) {
		this.professionalConcWebAccessSeats = professionalConcWebAccessSeats;
	}
	
	public int getCsserverSeats() {
		return csserverSeats;
	}
	public void setCsserverSeats(int csserverSeats) {
		this.csserverSeats = csserverSeats;
	}
	public int getCsswebaccessSeat() {
		return csswebaccessSeat;
	}
	public void setCsswebaccessSeat(int csswebaccessSeat) {
		this.csswebaccessSeat = csswebaccessSeat;
	}
	
	public int getNamedOnlineWebaccessSeats() {
		return namedOnlineWebaccessSeats;
	}
	public void setNamedOnlineWebaccessSeats(int namedOnlineWebaccessSeats) {
		this.namedOnlineWebaccessSeats = namedOnlineWebaccessSeats;
	}

	
	public int getLicenseConcurrentGrouptCount(){
		int licenseConcurrentGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseConcurrentGrpCount = lic.getDesktopSeatsConcurrent();
    			//Commented for CV2019 license implementation
    			/*
    			if(lic.getWebliteSeatsConcurrent() > licenseConcurrentGrpCount)
    				licenseConcurrentGrpCount = lic.getWebliteSeatsConcurrent();
    			if(lic.getWebtopSeatsConcurrent() > licenseConcurrentGrpCount)
    				licenseConcurrentGrpCount = lic.getWebtopSeatsConcurrent();
    			if(lic.getWebaccessSeatsConcurrent() > licenseConcurrentGrpCount)
    				licenseConcurrentGrpCount = lic.getWebaccessSeatsConcurrent();*/
    		}    		
    	}catch (Exception e) {}
    	return licenseConcurrentGrpCount;
	}
	
	public int getLicenseNamedOnlineWebaccessSeats(){

		int licenseNamedOnlinGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseNamedOnlinGrpCount = lic.getNamedOnlineWebaccessSeats();
    			
    		}    		
    	}catch (Exception e) {}
    	return licenseNamedOnlinGrpCount;
	}
	
	
	
	public int getLicenseConcurrentOnlineGrouptCount(){
		int licenseConcurrentOnlineGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseConcurrentOnlineGrpCount = lic.getWebaccessSeatsConcurrent();
    		
    		}    		
    	}catch (Exception e) {}
    	return licenseConcurrentOnlineGrpCount;
	}
	
	
	
	//Added for License changes Madhavan.B
	public int getLicenseNamedGroupCount(){
    	int licenseNamedGrpCount = -1;
    	int licenseprofessionalNamedGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseNamedGrpCount = lic.getDesktopSeatsNamed();
    			
    			if(lic.getWebliteSeatsNamed() > licenseNamedGrpCount)
    				licenseNamedGrpCount = lic.getWebliteSeatsNamed();
    			if(lic.getWebtopSeatsNamed() > licenseNamedGrpCount)
    				licenseNamedGrpCount = lic.getWebtopSeatsNamed();
    			if(lic.getWebAccessSeats() > licenseNamedGrpCount)
    				licenseNamedGrpCount = lic.getWebAccessSeats();
    			//Added for License changes, Madhavan.B
    			if(lic.getSdkSeats()>licenseNamedGrpCount)
    				licenseNamedGrpCount=lic.getSdkSeats();
    			/*if(lic.getProfessionalWebAccessSeats() >0)
    				licenseNamedGrpCount =licenseNamedGrpCount+ lic.getProfessionalWebAccessSeats();*/
    			if(lic.getProfessionalWebAccessSeats() >0)
    				licenseprofessionalNamedGrpCount = lic.getProfessionalWebAccessSeats();
    		}    		
    	}catch (Exception e) {}
    	return licenseNamedGrpCount;
    }
	
	public int getLicenseProfessionalNamedGroupCount(){
		int licenseprofessionalNamedGrpCount = -1;

		try{    		
			LicenseManager lm =  LicenseManagerImpl.getInstance();    	

			if (lm != null){
				com.computhink.vws.license.License lic  = lm.getLicense();
				//licenseprofessionalNamedGrpCount = lic.getDesktopSeatsNamed();
				if(lic.getProfessionalWebAccessSeats() >0)
					licenseprofessionalNamedGrpCount = lic.getProfessionalWebAccessSeats();
			}
		}
		catch(Exception e){}		
		return licenseprofessionalNamedGrpCount;
	}
	/**
	 * CV10.1 Public webaccess enhancement.
	 * @return
	 */
	public int getLicenseProfessionalConcurrentGroupCount(){
		int licenseprofessionalConcurrentGrpCount = -1;
		try{    		
			LicenseManager lm =  LicenseManagerImpl.getInstance();    	
			if (lm != null){
				com.computhink.vws.license.License lic  = lm.getLicense();
				if(lic.getProfessionalConcWebAccessSeats() >0)
					licenseprofessionalConcurrentGrpCount = lic.getProfessionalConcWebAccessSeats();
			}
		}
		catch(Exception e){}		
		return licenseprofessionalConcurrentGrpCount;
	}

	
	public int getLicenseCustomLabelCount(){
		int licenseCustomLabelCount = -1;
		try{    		
			LicenseManager lm =  LicenseManagerImpl.getInstance();    	
			if (lm != null){
				com.computhink.vws.license.License lic  = lm.getLicense();
				if(lic.getProfessionalConcWebAccessSeats() >0)
					licenseCustomLabelCount = lic.getCustomLabelWebAccessSeats();
			}
		}
		catch(Exception e){}		
		return licenseCustomLabelCount;
	}
	public int getSubAdminSeats() {
		return subAdminSeats;
	}
	public void setSubAdminSeats(int subAdminSeats) {
		this.subAdminSeats = subAdminSeats;
	}	
}
