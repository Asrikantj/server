package com.computhink.vws.license;

import java.io.*;
import java.security.PublicKey;
import com.computhink.resource.ResourceManager;

public class LicenseVerifier 
{
    private Boolean valid = new Boolean(false);
    public LicenseVerifier(){}
    private static ResourceManager resourceManager=ResourceManager.getDefaultManager();
    public static License validateLicense(String licenseEncoded, 
                                   String publicKeyFile) throws LicenseException 
    {
        String licenseKey = "";
        String signature = "";
        try 
        {
            EncryptionUtil signer = new EncryptionUtil();
            String tokens[] = licenseEncoded.split("#");
            if (tokens.length != 2)
                            throw new LicenseException(resourceManager.getString("LicenseVerifierMsg.TokenErr"));
            licenseKey = Base64Coder.decode(tokens[0] );
            signature = tokens[1];

            if (!signer.verify(licenseKey, signature, publicKeyFile))					
                    throw new LicenseException(resourceManager.getString("LicenseVerifierMsg.CannotVerify"));
            License lic = new License(licenseKey);
            return lic;
        }
        catch ( Exception e)
        {
            throw new LicenseException
                            ("LicenseKey is invalid: " + e.getMessage());

        }
    }
     public static SWLicense validateSWLicense(String licenseEncoded, 
                                   String publicKeyFile) throws LicenseException 
    {
        String licenseKey = "";
        String signature = "";

        try 
        {
            EncryptionUtil signer = new EncryptionUtil();
            String tokens[] = licenseEncoded.split("#");
            if (tokens.length != 2)
                            throw new LicenseException(resourceManager.getString("LicenseVerifierMsg.TokenErr"));
            licenseKey = Base64Coder.decode(tokens[0] );
            signature = tokens[1];

            if (!signer.verify(licenseKey, signature, publicKeyFile))					
                    throw new LicenseException(resourceManager.getString("LicenseVerifierMsg.CannotVerify"));
            SWLicense lic = new SWLicense(licenseKey);
            return lic;
        }
        catch ( Exception e)
        {
            throw new LicenseException
                            (resourceManager.getString("LicenseVerifierMsg.KeyInvalid") + e.getMessage());

        }
    }
    private static String readSignature(String signatureFile) throws 
                                                                 IOException 
    {
        FileInputStream sigfis = new FileInputStream(signatureFile);
        byte[] sigToVerify = new byte[sigfis.available()];
        sigfis.read(sigToVerify);
        sigfis.close();
        return new String(sigToVerify);
    }
    public static PublicKey readPublicKey(String URI) throws LicenseException
    {
        try
        {
            EncryptionUtil keyUtil = new EncryptionUtil();
            PublicKey publicKey = keyUtil.readPublicKey( URI );
            return publicKey;
        }
        catch (Exception e)
        {
            throw new LicenseException(resourceManager.getString("LicenseVerifierMsg.ReadKeyFile") + e.getMessage());
        }
    }

}