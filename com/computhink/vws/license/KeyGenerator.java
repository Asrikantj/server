/*
 * $Id: KeyGenerator.java,v 1.2 2014/03/15 06:38:24 madhavan Exp $
 *
 *  Copyright (c) 2003 SourceTap - www.sourcetap.com
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  
 */
package com.computhink.vws.license;

import java.security.*;

import com.computhink.resource.ResourceManager;

public class KeyGenerator
{
    private PublicKey publicKey;
    private PrivateKey privateKey;
    EncryptionUtil keygen;
	
    public KeyGenerator()
    {
        keygen = new EncryptionUtil();
    }
    public void createKeys(String publicURI, String privateURI) throws LicenseException
    {
    	try 
    	{
            keygen.generateKeys(publicURI, privateURI);
            publicKey = keygen.getPublic();
            privateKey = keygen.getPrivate();
    	}
    	catch (Exception e)
    	{
            throw new LicenseException (e.getMessage());
    	}
    }
    public void readKeys( String publicURI, String privateURI ) throws LicenseException
    {
        try
        {
            keygen.readKeys( publicURI, privateURI );
            publicKey = keygen.getPublic();
            privateKey = keygen.getPrivate();
        }
        catch (Exception e)
        {
            throw new LicenseException("Unable to read Key Files: " + e.getMessage());
        }
    }
    public void readPublicKey( String URI ) throws LicenseException
    {
        try
        {
            publicKey = keygen.readPublicKey( URI );
        }
        catch (Exception e)
        {
            throw new LicenseException(ResourceManager.getDefaultManager().getString("KeyGen.ReadKeyFile") + e.getMessage());
        }
    }
    public void readPrivateKey( String URI ) throws LicenseException
    {
        try
        {
            privateKey = keygen.readPrivateKey( URI );
        }
        catch (Exception e)
        {
            throw new LicenseException(ResourceManager.getDefaultManager().getString("KeyGen.ReadKeyFile") + e.getMessage());
        }
    }
    public PublicKey getPublic()
    {
        return publicKey;
    }
    public PrivateKey getPrivate()
    {
        return privateKey;
    }
    public String getPublicString()
    {
        return publicKey.toString();
    }
    public String getPrivateString()
    {
        return privateKey.toString();
    }
}
