/*
 * $Id: EncryptionUtil.java,v 1.1 2013/06/10 13:14:58 shylesh Exp $
 *
 *  Copyright (c) 2003 SourceTap - www.sourcetap.com
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 *  
 */
package com.computhink.vws.license;

import java.io.*;
import java.security.*;
import java.security.spec.*;

public class EncryptionUtil
{
    private PublicKey publicKey = null;
    private PrivateKey privateKey = null;

    public EncryptionUtil()
    {
    }
    public void generateKeys() throws IOException, NoSuchAlgorithmException, 
                                                         NoSuchProviderException
    {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("DSA");
        keyGen.initialize(1024, new SecureRandom());
        KeyPair pair = keyGen.generateKeyPair();
        this.privateKey = pair.getPrivate();
        this.publicKey = pair.getPublic();
    }
    public void generateKeys(String publicURI, String privateURI )
           throws IOException, NoSuchAlgorithmException, NoSuchProviderException
    {
            generateKeys();
            writeKeys(publicURI, privateURI);
    }
    public PublicKey getPublic()
    {
            return publicKey;
    }
    public PrivateKey getPrivate()
    {
            return privateKey;
    }
    public void writeKeys(String publicURI, String privateURI) throws 
                                              IOException, FileNotFoundException
    {
            writePublicKey(publicURI);
            writePrivateKey(privateURI);
    }
    public void writePublicKey(String URI) throws IOException, 
                                                           FileNotFoundException
    {
            byte[] enckey = publicKey.getEncoded();
            FileOutputStream keyfos = new FileOutputStream(URI);
            keyfos.write(enckey);
            keyfos.close();
    }
    public void writePrivateKey(String URI) throws IOException, 
                                                           FileNotFoundException
    {
            byte[] enckey = privateKey.getEncoded();
            FileOutputStream keyfos = new FileOutputStream(URI);
            keyfos.write(enckey);
            keyfos.close();
    }
    public void readKeys(String publicURI, String privateURI) throws IOException, 
      NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException
    {
            readPublicKey(publicURI);
            readPrivateKey(privateURI);
    }
    public PublicKey readPublicKey(String URI) throws IOException, 
      NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException
    {
        FileInputStream keyfis = new FileInputStream(URI);
        byte[] encKey = new byte[keyfis.available()];
        keyfis.read(encKey);
        keyfis.close();
        X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(encKey);
        KeyFactory keyFactory = KeyFactory.getInstance("DSA");
        publicKey = keyFactory.generatePublic(pubKeySpec);
        return publicKey;
    }
    public PrivateKey readPrivateKey(String URI) throws IOException, 
      NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException
    {
        FileInputStream keyfis = new FileInputStream(URI);
        byte[] encKey = new byte[keyfis.available()];
        keyfis.read(encKey);
        keyfis.close();
        PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(encKey);
        KeyFactory keyFactory = KeyFactory.getInstance("DSA");
        privateKey = keyFactory.generatePrivate(privKeySpec);
        return privateKey;
    }
    public String sign(String message) throws IOException, 
                              NoSuchAlgorithmException, NoSuchProviderException, 
                InvalidKeySpecException, InvalidKeyException, SignatureException
    {
        return sign(message, privateKey);
    }
    public String sign(String message, String privateKeyURI) throws IOException,
     NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException, 
     InvalidKeyException, SignatureException, IOException
    {
        PrivateKey pk = readPrivateKey(privateKeyURI);
        return sign(message, pk);
    }
    public String sign(String message, PrivateKey privateKey)throws IOException,
     NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException,
     InvalidKeyException, SignatureException
    {
        Signature dsa = Signature.getInstance("SHA/DSA");
        dsa.initSign(privateKey);
        dsa.update(message.getBytes());
        byte m1[] = dsa.sign();
        String signature = new String(Base64Coder.encode(m1));
        return signature;
    }
    public boolean verify(String message, String signature, String publicKeyURI)
            throws IOException, NoSuchAlgorithmException, NoSuchProviderException,
            InvalidKeySpecException, InvalidKeyException, SignatureException
    {

        PublicKey pk = readPublicKey(publicKeyURI);
        return verify(message, signature, pk);
    }
    public boolean verify(String message, String signature)
            throws IOException, NoSuchAlgorithmException, NoSuchProviderException,
            InvalidKeySpecException, InvalidKeyException, SignatureException
    {
            if ( publicKey == null )
                    throw new InvalidKeyException("Public Key not provided.");
            return verify( message, signature, publicKey);
    }
    /**
    * verify that the message was signed by the private key by using the public key
    * @param message     the message to be verified
    * @param signature   the signature generated by the private key and encoded in Base64
    * @param publicKey   the public key
    * @return boolean   true if the message was signed by the private key
    */
    public boolean verify(String message, String signature, PublicKey publicKey)
            throws IOException, NoSuchAlgorithmException, NoSuchProviderException,
            InvalidKeySpecException, InvalidKeyException, SignatureException
    {
            Signature dsa = Signature.getInstance("SHA/DSA");
            dsa.initVerify(publicKey);
            dsa.update(message.getBytes());

            byte sigDec[] = Base64Coder.decode(signature.toCharArray());
            return dsa.verify(sigDec);
    }

}