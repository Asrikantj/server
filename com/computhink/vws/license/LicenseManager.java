/*
 * $Id: LicenseManager.java,v 1.1 2013/06/10 13:14:58 shylesh Exp $
 *
 *  Copyright (c) 2003 SourceTap - www.sourcetap.com
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *  
 */
package com.computhink.vws.license;
import com.computhink.vws.server.*;

public interface LicenseManager
{
    public abstract boolean hasValidLicense() throws LicenseException;
    public abstract License getLicense() throws LicenseException;
    public abstract SWLicense getSignWiseLicense() throws LicenseException;
}
