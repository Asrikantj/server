/*
 * SWLicense.java
 *
 * Created on June 27, 2005, 3:45 PM
 */

package com.computhink.vws.license;

import com.computhink.resource.ResourceManager;

/**
 *
 * @author  Administrator
 */
public class SWLicense implements java.io.Serializable
{
    
    private String companyName;
    private String email;
    
    public SWLicense(String licenseKey) throws LicenseException
    {
        setLicenseKey(licenseKey);
    }
    public SWLicense(String cName, String cEmail) throws LicenseException
    {
        setCompanyName(cName);
        setEmailAddress(cEmail);
    }
    public String getCompanyName()
    {
       return companyName;
    }
    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }
    public void setEmailAddress(String email)    
    {
        this.email = email;
    }
    public String getLicenseKey()
    {
        return companyName + "#" + email;
    }
    private void setLicenseKey(String licenseKey) throws LicenseException
    {
        String tokens[] = licenseKey.split("#");
        if(tokens.length < 2)
                            throw new LicenseException(ResourceManager.getDefaultManager().getString("LicenseVerifierMsg.KeyInvalid"));
        setCompanyName(tokens[0]);
        setEmailAddress(tokens[1]);
     }
    
}
