/*
 * $Id: LicenseGenerator.java,v 1.2 2014/03/15 06:38:24 madhavan Exp $
 *
 *  Copyright (c) 2003 SourceTap - www.sourcetap.com
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included
 *  in all copies or substantial portions of the Software.
 *
 * 
 */
package com.computhink.vws.license;

import java.io.*;
import java.security.*;
import java.security.spec.*;

public class LicenseGenerator {
private static com.computhink.resource.ResourceManager resourceManager=com.computhink.resource.ResourceManager.getDefaultManager();
    public LicenseGenerator(){}

    public static String generateLicense(String license, String privateKeyFile)
                                                         throws LicenseException 
    {
        try 
        {
            String signature = signMessage(license, privateKeyFile);
            String licenseEncoded = Base64Coder.encode(license) + "#" + signature;
            return licenseEncoded;
        }
        catch (Exception e)
        {
            throw new LicenseException( e.getMessage() );	
        }
    }
    private static String signMessage(String message, String privateKeyFile)
          throws IOException, NoSuchAlgorithmException, NoSuchProviderException,
          InvalidKeyException, SignatureException, InvalidKeySpecException 
    {
        if (message == null) throw new SignatureException("No Message to sign");

        EncryptionUtil signer = new EncryptionUtil();
        String signature = signer.sign(message, privateKeyFile);
        return signature;
    }
    private static void saveSignature(String signature, String signatureFile)
                                                              throws IOException 
    {
        FileOutputStream sigfos = new FileOutputStream(signatureFile);
        sigfos.write(signature.getBytes());
        sigfos.close();
    }
    public static PrivateKey readPrivateKey( String URI ) throws LicenseException
    {
        try
        {
            EncryptionUtil keyUtil = new EncryptionUtil();
            PrivateKey privateKey = keyUtil.readPrivateKey( URI );
            return privateKey;
        }
        catch (Exception e)
        {
            throw new LicenseException(resourceManager.getString("KeyGen.ReadKeyFile") + e.getMessage());
        }
    }
}