 /*
 * Images.java
 *
 * Created on Sep 21, 2003, 12:14 AM
 */
package com.computhink.vws.image;
/**
 *
 * @author  Saad
 * @version 
 */
import javax.swing.ImageIcon;

public class Images
{
    public static ImageIcon app;
    public static ImageIcon rof;
    public static ImageIcon ron;
    public static ImageIcon cdb;
    public static ImageIcon bro;

    public Images()
    {
        try
        {
            app = new ImageIcon(getClass().getResource("images/vws.gif"));
            rof = new ImageIcon(getClass().getResource("images/rof.gif"));
            ron = new ImageIcon(getClass().getResource("images/ron.gif"));
            cdb = new ImageIcon(getClass().getResource("images/cdb.gif"));
            bro = new ImageIcon(getClass().getResource("images/bro.gif"));
        }
        catch (Exception e){}
    }
}