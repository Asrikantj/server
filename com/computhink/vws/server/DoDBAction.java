package com.computhink.vws.server;
import com.computhink.database.*;
import com.computhink.common.*;

import java.util.*;

import oracle.jdbc.OracleTypes;
import oracle.jdbc.driver.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.sql.*;

public class DoDBAction implements ViewWiseErrors
{
    private static final int SQLServer_DB = 1;
    private static final int Oracle_DB = 2;
    private static final int Sybase_DB = 3;

    public static int get(Database db, String Act, String par, Vector ret)
    {
	return get(null, db, Act, par, ret);
    }
    public static int get(Client client, Database db, String Act, String par, Vector ret)
    {
	String user = "";
	String ipAddress = "";
	String sid = "";
	String clientType = ""; 
	//Added for Retention based on folder policy enhancement,Gurumurthy.T.S 22-05-2012
	String isAdmin = " ";
	
	if ( client != null){
	    user = client.getUserName();
	    ipAddress = client.getIpAddress();
	    sid = String.valueOf(client.getId());
	    clientType = String.valueOf(client.getClientType());
	    //Added for Retention based on folder policy enhancement,Gurumurthy.T.S 22-05-2012
	    isAdmin=(client.isAdmin()?"1":"0");
	    clientType=clientType+Util.SepChar+isAdmin+Util.SepChar;	    
	}
        String engine = db.getEngineName();
         if (engine.equalsIgnoreCase("Oracle"))
                            return getOra(db, Act, par, ret, sid, user, ipAddress,clientType);
         else if (engine.equalsIgnoreCase("SQLServer"))
                            return getSql(db, Act, par, ret, sid, user, ipAddress,clientType);
         else if (engine.equalsIgnoreCase("Sybase"))
                            return getSyb(db, Act, par, ret);
         else return Error;
    }
    //Any change done to this method need to added in DoIXRDACtion also
    public static int getOra(Database db, String Act, String Params, Vector ret, String sid, String user, String ipAddress, String clientType)
    {
    	int returnValue = 0;
        CallableStatement cs = null;
        ResultSet rs  = null;
        Connection con = null;      String tmp;
        Params = appendTab(Params);
        //System.out.println(Act + "==>" + Params + "==>");
        if (VWSPreferences.getDebug())
        	VWSLog.war(Act + "==>" + Params + "==>"); 
        try
        {
            con = db.aquireConnection();
            cs = con.prepareCall("{ Call ViewWiseActions(?,?,?,?,?,?,?)}");
            cs.setString(1, Act); 
            cs.setString(2, Params);             
            cs.setString(3, sid); 
            cs.setString(4, user);
            cs.setString(5, ipAddress);
            cs.setString(6, clientType);
           cs.registerOutParameter(7,OracleTypes.CURSOR);
            cs.executeQuery();
            rs = (ResultSet) cs.getObject(7);
            if (rs != null)
            {
                while(rs.next())
                {
                    String str = rs.getString("Fld");
                    //System.out.println(str);
                    ret.add(str /*rs.getString("Fld")*/);
                    if (VWSPreferences.getOutputDebug())
                    VWSLog.add("Output Fld:->"+db.getName()+":" + str); 
                }
                rs.close();
                rs = null;
            }
            cs.close(); cs = null;
        }
        catch (SQLException e) 
        {
            String Msg = e.getMessage();
        	VWSLog.err("Room: "+db.getName()+" "+Act + ":" + e.getMessage()+" Params: '"+Params+"'");
        	try{
        	if(Msg.toUpperCase().indexOf("SOCKET READ TIMED OUT") != -1 || Msg.toUpperCase().indexOf("MAXIMUM OPEN CURSORS EXCEEDED") != -1){
                    try{
                	con.commit();
                    }catch(Exception ex){
                	db.printToConsole("Exception while commit the connection in closing " + ex.getMessage());
                    }
                    db.printToConsole("Closing connection in getOra ");
                    db.closeConnection(con);
                }}catch (Exception ex) {db.printToConsole("Exception connection closing " + ex.getMessage());}
		    // TODO: handle exception		
        	returnValue = Error;
        }
        finally {
        	//Make sure that result set and connection is closed
        		try{
        			if (rs != null) {rs.close();rs = null;}
       			}catch (SQLException e){}
           		try{
           			if (cs != null) {cs.close();cs = null;}
           		}catch (SQLException e){}           		
        	assertConnection(db, con);
        }
        return returnValue;       
    }
    public static int getSql(Database db, String Act, String Params, Vector ret, String sid, String user, String ipAddress , String clientType)
    {
    	int returnValue = 0;
        CallableStatement cs = null;
        ResultSet rs  = null;
        Connection con = null;      String tmp;
        Params = appendTab(Params);
        InputStream paramsInputStream = null;
        //System.out.println(Act + "==>" + Params + "==>");
        if (VWSPreferences.getDebug())
        	VWSLog.war(Act + "==>" + Params + "==>"); 
        try
        {        	
            con = db.aquireConnection();            
            cs = con.prepareCall("{ Call ViewWiseActions(?,?,?,?,?,?) }");
            cs.setString(1, Act); 
            /*
            Issue No / Purpose:  <622>
            Created by: <Pandiya Raj.M>
            Date: <07 Jul 2006>           
            Since the charset is "UNICODE" (set by the JDBC driver), each character takes up 2 bytes and 
            only 4000 characters are processed. When the page count exceeds a certain value (ex: 500), 
            "Params" is not passed to the stored procedure and an error is displayed "Cannot convert Ntext to Varchar".  
            This issue can be resolved by replacing the "setString()" method with the "setAsciiStream()". 
            For reference,It is explained in the  Program Specification "Program Specification_622.doc"             
            */            
            if(Params.length()>3999)
            {
               paramsInputStream = new ByteArrayInputStream(Params.getBytes("UTF-8"));
               cs.setAsciiStream(2, paramsInputStream,paramsInputStream.available());
            }else
               cs.setString(2,Params);
            cs.setString(3, sid); 
            cs.setString(4, user);
            cs.setString(5, ipAddress);
            cs.setString(6, clientType); 
            rs = cs.executeQuery();
            if (rs != null)
            {
                while(rs.next())
                {
                    String str = rs.getString("Fld");
                    //System.out.println(str);
                    ret.add(str /*rs.getString("Fld")*/);
                    if (VWSPreferences.getOutputDebug())
                        VWSLog.add("Output Fld:->"+db.getName()+":" + str);
                    ///VWSLog.add(str);
                }
                rs.close();
                rs = null;
            }
            cs.close(); cs = null;
        }
        catch (UnsupportedEncodingException e) 
        {
        	/***CV2019 code merges from CV10.2 line. Added by Vanitha.S****/
        	returnValue = Error;
        }
        catch (IOException e) 
        {
        	/***CV2019 code merges from CV10.2 line. Added by Vanitha.S****/
        	returnValue = Error;
        }
        catch (SQLException e) 
        {
        	VWSLog.err("Room: "+db.getName()+" "+Act + ":" + e.getMessage()+" Params: '"+Params+"'");
            if ((e.getMessage().toUpperCase()).indexOf("CONNECTION RESET BY PEER") > -1 ||
                	(e.getMessage().toUpperCase()).indexOf("CONNECTION REFUSED") > -1){
            	if (db.isDbConnected()){
	                	ConnectDatabase connectDB = new DoDBAction().new ConnectDatabase(db);
	                	db.setDbConnected(false);  
	                	connectDB.start();
            	}
            }
            /***CV2019 code merges from CV10.2 line. Added by Vanitha.S****/
            returnValue = Error;
        }
        finally {
        	//Make sure that result set and connection is closed        	
    		try{
    			if (rs != null) {rs.close();rs = null;}
   			}catch (SQLException e){}
       		try{
       			if (cs != null) {cs.close();cs = null;}
       		}catch (SQLException e){}           		        	
       		try{
       			if (paramsInputStream != null) {paramsInputStream.close();cs = null;}
       		}catch (IOException e){}           		        	

       		assertConnection(db, con);
       	}
        return returnValue;
    }
    public static int getSyb(Database db, String Act, String Params, Vector ret)
    {
    	int returnValue = 0;
        CallableStatement cs = null;
        ResultSet rs  = null;
        Connection con = null;      String tmp;
        Params = appendTab(Params);
        try
        {
            con = db.aquireConnection();
            cs = con.prepareCall("{ Call ViewWiseActions(?,?) }");
            cs.setString(1, Act); 
            cs.setString(2, Params); 
            rs = cs.executeQuery();
            if (rs != null)
            {
                while(rs.next())
                {
                    String str = rs.getString("Fld");
                    ret.add(str /*rs.getString("Fld")*/);
                }
                rs.close();
                rs = null;
            }
            cs.close(); cs = null;
        } catch (SQLException e) {
			VWSLog.err(e.getMessage());
			returnValue = Error;
		} finally {
    		try{
    			if (rs != null) {rs.close();rs = null;}
   			}catch (SQLException e){}
       		try{
       			if (cs != null) {cs.close();cs = null;}
       		}catch (SQLException e){}         	
        	assertConnection(db, con);
        }
        return returnValue;
    }
    private static String appendTab(String Params)
    {
        if( Params != null && 
            !Params.equals("") && 
            Params.indexOf(Util.SepChar) > 0 && 
            !Params.endsWith(Util.SepChar)) 
            Params += Util.SepChar; 
        return Params;
    }
    private synchronized static void assertConnection(Database db, Connection con)
    {
        if (con == null) return;
        try
        {
            con.commit();                       
            db.releaseConnection(con);
            db.dbFailed = 0;
            db.setDbConnected(true);
        }
        catch (Exception e)
        {     
        	// Close the Room If Db is not available. To Solve the Issue No 501
        	String Msg = e.getMessage();

        	try {
                if ((e.getMessage().toUpperCase()).indexOf("CONNECTION RESET BY PEER") > -1 ||
                	(e.getMessage().toUpperCase()).indexOf("CONNECTION REFUSED") > -1){
                	ConnectDatabase connectDB = new DoDBAction().new ConnectDatabase(db);
                	db.setDbConnected(false);  
                	connectDB.start();                	              	               
                }else if(Msg.toUpperCase().indexOf("SOCKET READ TIMED OUT") != -1 || Msg.toUpperCase().indexOf("MAXIMUM OPEN CURSORS EXCEEDED") != -1){
                    try{
                	con.commit();
                    }catch(Exception ex){
                	//db.printToConsole("Exception while commit the connection in closing " + ex.getMessage());
                    }                    
                    db.closeConnection(con);
                }else if(Msg.toUpperCase().indexOf("SHUTDOWN") != -1
					|| Msg.toUpperCase().indexOf("CONNECTION ABORT") != -1
					|| Msg.toUpperCase().indexOf("ERROR ESTABLISHING SOCKET") != -1
					|| Msg.toUpperCase().indexOf("SOCKET WRITE ERROR") != -1
					|| Msg.toUpperCase().indexOf("CONNECTION RESET") != -1
					/*|| Msg.toUpperCase().indexOf("CLOSED CONNECTION") != -1*/) {
                	/**If condition has added for DBLookup customer issue. If dsn connection is failed then server room is going offline**/
                	VWSLog.dbg("engine name inside assertConnection......"+db.getEngineName());
                	if (!db.getEngineName().equalsIgnoreCase("DSN")) {
			    		CIServer cServer = (CIServer) Util.getServer(Util.SERVER_VWS,db.serverIP,db.serverPort);
			    		VWS vws = (VWS) cServer;
			    		VWSLog.dbg("before making the room offline in assertConnection method......");
			    		vws.closeRoom(db.getName());
			            VWSLog.err("Connection orphaned ::: " + e.getMessage()); 
                	}                 
		        }
		    }catch(Exception ex){}  
		}
    }
    class ConnectDatabase extends Thread{
    	private Database db = null;
    	public ConnectDatabase(Database db){
    		this.db = db;
    	}
    	public void run(){
    		waitForDatabaseConnection(db);
    	}
	    private void waitForDatabaseConnection(Database database)
	    {
	    	VWSLog.dbg("inside waitForDatabaseConnection method in DoDBAction......");
	        boolean msg = true;
	        Connection con = null;
	        try{
	    	con = database.aquireConnection();con.close();
	        }catch(Exception e){}
	        con = null;
	        while (con == null)
	        {
	            try{
	            	/**If condition has added for DBLookup customer issue. If dsn connection is failed then server room is going offline**/
	                if (!database.getEngineName().equalsIgnoreCase("DSN")) {
	                	database.dbFailed++;
	                	VWSLog.dbg("database.dbFailed : "+database.dbFailed);
	                	VWSLog.dbg("database.getCountDBFail : "+database.getCountDBFail());
	                	if (database.dbFailed > database.getCountDBFail()){
	                		database.dbConnected = false;
	                		try{
		                		CIServer cServer = (CIServer) Util.getServer(Util.SERVER_VWS,database.serverIP,database.serverPort);
		                		VWS vws = (VWS) cServer;
		                		VWSLog.dbg("before making the room offline in waitForDatabaseConnection method DoDBAction class......");
		                		vws.closeRoom(database.getName());	                		
	                		}catch(Exception ex){
	                			break;
	                		}
	                		break;
	                	}
	                }
	            	// Establish the connection again.
	            	database.initialize();
	            	con = database.aquireConnection();	            	
	            	database.dbFailed = 0;
	            }catch (Exception e)
	            {
	            	try{
	                String db = database.getJdbcUrl();
	                String Msg = e.getMessage();              
	                if (msg) VWSLog.war(database.getName() 
	                    + ": Listening to Database Server on: " + db + "\r\n" 
	                                                              + e.getMessage());	                
/*	               if(Msg.toUpperCase().indexOf("SHUTDOWN") != -1
	    					|| Msg.toUpperCase().indexOf("CONNECTION ABORT") != -1
	    					|| Msg.toUpperCase().indexOf("ERROR ESTABLISHING SOCKET") != -1
	    					|| Msg.toUpperCase().indexOf("SOCKET WRITE ERROR") != -1
	    					|| Msg.toUpperCase().indexOf("CONNECTION RESET") != -1
	    					|| Msg.toUpperCase().indexOf("CLOSED CONNECTION") != -1) {    		
	    			    		CIServer cServer = (CIServer) Util.getServer(Util.SERVER_VWS,database.serverIP,database.serverPort);
	    			    		VWS vws = (VWS) cServer;    		
	    			    		vws.closeRoom(database.getName());
	    			            VWSLog.err("Connection orphaned ::: " + e.getMessage());
	    			            break;
	                }	
*/	            	}catch(Exception ex){}
	            	int frequency = 10;
	            	try{
	            		frequency = VWSPreferences.getDBFrequency();
	            	}catch(Exception ex){frequency = 10;}
	            	Util.sleep(frequency * 1000);
	            }
	        }
            if (con != null) db.setDbConnected(true);
	        try{database.releaseConnection(con);}
	        catch (Exception e){}
	    }
    }
    public static int getPrincipalMembers(Database db, String pid, Vector principals)
    {
        PreparedStatement pstmt = null;
        Connection con = null; 
        ResultSet rs  = null;
        String stmt = 
        "Select Fld = cast(Principal.id as varchar) + CHAR(9) +" + 
        "rtrim(UserTable.Name) + CHAR(9) + cast(Principal.Type as varchar)" + 
                        " From UserTable, Principal where Principal.Type = 2" +
                        " and Principal.GroupUserid = UserTable.id" +
                        " and Principal.GroupUserid in" +
                        " (select Userid from GroupToUser where Groupid =" + 
                        " (Select GroupUserid from Principal where id = " +
                        pid + "))";
        try
        {
            con = db.aquireConnection();
            pstmt = con.prepareStatement(stmt);
            rs = pstmt.executeQuery();
            if (rs != null)
            {
                while(rs.next())
                {
                    String str = rs.getString("Fld");
                    principals.add(str);
                }
                rs.close();
                rs = null;
            }
            pstmt.close(); pstmt = null;
        }
        catch (SQLException e) 
        {
            VWSLog.err("getPrincipalMembers: " + e.getMessage()); 
            return Error;
        }
        finally {assertConnection(db, con);}
        return NoError;
    }
}