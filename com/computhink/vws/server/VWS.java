/*
 * VWS.java
 *
 * 
 */
package com.computhink.vws.server;
import java.io.File;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;

import com.computhink.common.Acl;
import com.computhink.common.CIServer;
import com.computhink.common.DBLookup;
import com.computhink.common.DocComment;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.common.EWorkSubmit;
import com.computhink.common.EWorkTemplate;
import com.computhink.common.Index;
import com.computhink.common.Node;
import com.computhink.common.NodeIndexRules;
import com.computhink.common.NotificationSettings;
import com.computhink.common.Principal;
import com.computhink.common.RoomProperty;
import com.computhink.common.RouteIndexInfo;
import com.computhink.common.RouteInfo;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.RouteUsers;
import com.computhink.common.Search;
import com.computhink.common.ServerSchema;
import com.computhink.common.Signature;
import com.computhink.common.VWDoc;
import com.computhink.common.VWRetention;
import com.computhink.vwc.DBConnectionBean;
import com.computhink.vws.license.License;

public interface VWS extends CIServer
{
    public boolean ping() throws RemoteException;
    public void startWorkers() throws RemoteException;
    public void shutDown() throws RemoteException;
    public License getLicense() throws RemoteException;
    public Vector getRoomNames() throws RemoteException;
    public int login(String room, int type, String address, String user, 
                                  String pass, int port) throws RemoteException;
/*
Issue No / Purpose:  <650/Terminal Services Client>
Created by: <Pandiya Raj.M>
Date: <19 Jul 2006>
Pass the stubIPAddress" to the login method.
*/    
                              
    public int login(String room, int type, String address, String user, 
            String pass, int port,String stubIPAddress) throws RemoteException;    
    public int isHacker(String room, String ip) throws RemoteException;
    public int logout(String room, int sessionId) throws RemoteException;
    public Vector getConnectedClients(String room, int sid, Vector users) 
                                                         throws RemoteException;
    public Vector getPrincipal(String room, int sid, Principal princ , Vector result) 
                                                         throws RemoteException;
    public int removePrincipal(String room, int sid, Principal princ) 
                                                         throws RemoteException;
    public int updatePrincipal(String room, int sid, Principal princ) throws RemoteException;
    public int addPrincipal(String room, int sid, Principal princ)
         throws RemoteException;
    public int syncPrincipals(String room, int sid)
         throws RemoteException;
	/**CV2019 merges from 10.2 line***/
    public int syncPrincipals(String room, int sid,Principal principal) throws RemoteException;
    public Vector getDirectoryPrincipals(String room, int sid) 
        throws RemoteException;
    public Vector getDirectoryPrincipalsNew(String room, int sid, String searchForPName) 
            throws RemoteException;
    public int isConnectionEnabled(String room, int sid) throws RemoteException; 
    public int setAllowConnection(String room,int sid,boolean allow) 
                                                         throws RemoteException; 
    public int logoutClient(String room, int sid,int clientsid) 
                                                         throws RemoteException;
    public Vector getNodePath(String room, int sid, int nodeId) throws RemoteException;    
    public Node getNodeProperties(String room, int sid, Node node) 
                                                         throws RemoteException;
    public Vector getNodeContents(String room, int sid, int nid, int coid,
                                           Vector nodes) throws RemoteException;
    // Fixing Issue 735, add the sort argument to retrieve the data based on sort value. Sort will be done based on 'sort' value
    public Vector getNodeContents(String room, int sid, int nid, int coid,
                                           Vector nodes, int sort) throws RemoteException;
	
	/* Issue Description - AIP Hangs when processing large rooms
	*  Developer - Nebu Alex
	*  Code Description - This function makes a database call and
	*					- returns node information, 
	* Arguments required - Nodes parentid and nodes name
	* 
	*/
	public Vector getNodeDetails(String room, int sid, String parentid, String nodeName,Vector nodes)
     throws RemoteException;
     // End of code change
    public Vector getNodeParents(String room, int sid,int nid, Vector nodes) 
                                                         throws RemoteException;
    public int createNode(String room, int sid, Node node, int withUniqueName) 
                                                         throws RemoteException;
    public int delNode(String room, int sid, int nid, boolean toRecycle) 
                                                         throws RemoteException;
    public int azureARSDeleteNode(String room, int sid, int nid, boolean toRecycle)throws RemoteException;
    public Vector getDocsToPurge(String room, int sid, int docsCountToPurge, Vector docsToPurge)throws RemoteException;
    public int renameNode(String room, int sid, Node node) 
                                                         throws RemoteException;
    public int copyNode(String room, int sid, Node node, int nodeProperties) throws RemoteException;
    public Vector azureCopyNode(String room, int sid, Node node, int nodeProperties) throws RemoteException;
    public int moveNode(String room, int sid, Node node) throws RemoteException;
    public int moveNode(String room, int sid, Node node, int nodeProperties) throws RemoteException;
    public Vector getNodeDocuments(String room, int sid, int nid, int coid, int refNid,
                                       Vector documents) throws RemoteException;
    public Vector getAIPNodeDocuments(String room, int sid, int nid, int coid, int docTypeId, String curIndexVal, Vector documents) throws RemoteException;
    public Vector getAllNodeDocuments(String room, int sid,int nid, String ipAddress, 
            Vector documents) throws RemoteException;
    
     public Vector getDocumentByInfo(String room, int sid, String nodePath, 
        String docTypeName,String indexName,String indexValue,int indexKey) 
                                                         throws RemoteException;
    public Document createDocument(String room, int sid, Document doc) 
                                                         throws RemoteException;
	/**CV2019 - Enhancement***/
    public Vector getDocumentProperties(String room, int sid, int did,
            Vector indices)  throws RemoteException;
    public Vector getDocumentIndices(String room, int sid, int did, String ver, 
                                        Vector indices)  throws RemoteException;
    public Vector getDocTypeIndices(String room, int sid, int dtid, 
                                         Vector indices) throws RemoteException;    
    public int setDocumentIndices(String room, int sid, Document doc)
                                                         throws RemoteException;
    public int updateDocumentDocType(String room, int sid, Document doc)
    throws RemoteException;
    public Vector updateAzureDocumentDocType(String room, int sid, Document doc)
    		throws RemoteException;
    
    public int setDocActiveVer(String room, int sid, int did, String ver)
                                                         throws RemoteException;
    public Vector setDocActiveVerAzure(String room, int sid, int did, String ver)throws RemoteException;
    public Vector populateSelectionIndex(String room, int sid, int iid,
                                          Vector values) throws RemoteException; 
    public Vector getDocTypes(String room, int id, Vector types)
                                                         throws RemoteException;
     public Vector getDocType(String room, int sid, int did, Vector type)
                                                         throws RemoteException;
    public int violatesUnique(String room, int sid, int nodeId, int dtid, int iid, 
                                         String value)   throws RemoteException;
    public Vector getCreators(String room, int sid, Vector creators)
                                                         throws RemoteException;
    public Vector getDocumentComment(String room, int sid, int did,String docVersion, 
                                       Vector comments)  throws RemoteException;
    public int deleteDocumentComment(String room, int sid,int did)
                                                         throws RemoteException;
    public Vector getDocumentVers(String room, int sid, int did, Vector vers)
                                                         throws RemoteException;
    public int setDocVerComment(String room,int sid,int did, String ver, 
                                         String comment) throws RemoteException;
    public int deleteDocumentVers(String room, int sid, int did)  
                                                         throws RemoteException; 
    public Vector getRedactions(String room, int sid, int did, String ver, 
                                      Vector redactions) throws RemoteException;
     public int setRedactions(String room, int sid, int did , String redactions) 
                                                         throws RemoteException;
    public Vector setDocumentComment(String room, int sid, DocComment dc, 
                                boolean withCheck,int docRestoreFlag)throws RemoteException;
    public int lockDocument(String room, int sid, int did) 
                                                         throws RemoteException;
    public int unlockDocument(String room, int sid, int did, int force) 
                                                         throws RemoteException;
    public Vector getLockOwner(String room, int sid, int did, Vector name , 
        boolean withMe)     throws RemoteException;   
    // Issue No - 567    
	public Vector getSessionOwner(String room, int sid, int did, Vector name , 
            boolean withMe)     throws RemoteException;
	 // End of Issue No - 567            
    public Index getNextSerial(String room, int sid, Index index)
                                                         throws RemoteException;
    public Vector getNextSequence(String room, int sid, int indexId, int docTypeId,int nodeId) throws RemoteException;    
    public int addRef(String room, int sid, int nid, int rid, boolean bi) 
                                                         throws RemoteException; 
    public int delRef(String room, int sid, int nid, int rid) 
                                                         throws RemoteException; 
    public Vector getRefs(String room, int sid, int nid, Vector refs) 
                                                         throws RemoteException;
    public int addShortcut(String room, int sid, int nid, String name, 
                                  String user, int type) throws RemoteException; 
    public int delShortcut(String room, int sid, int scid) 
                                                         throws RemoteException; 
    public Vector getShortcuts(String room, int sid, Vector scs) 
                                                         throws RemoteException; 
    public int updateShortcut(String room, int sid, int scid, 
                                            String name,int type) throws RemoteException;
    public int createSearch(String room, int sid, Search search) 
                                                         throws RemoteException;
    public Search getSearch(String room, int sid,Search search)
                                                         throws RemoteException;  
    public Vector getSearchs(String room, int sid, Vector searchs) 
                                                         throws RemoteException;
    public Vector getSearchPages(String room, int sid, int did, String contents,
                                           Vector pages) throws RemoteException;
    public int delSearch(String room, int sid, int srid) throws RemoteException; 
    public Vector getSearchAuthors(String room, int sid, int srid, 
                                         Vector authors) throws RemoteException;
    public Vector getSearchDocTypes(String room, int sid, int srid, 
                                             Vector dts) throws RemoteException;
    public Vector getSearchConds(String room, int sid, int srid, 
                                           Vector conds) throws RemoteException;
    public int find(String room, int sid, int srid, int hlid, int rowNo, boolean docsOnly) 
                                                         throws RemoteException;
    //find signature changed for enh 1157
    public int find(String room, int sid, Search search, boolean docsOnly) throws RemoteException;
    public Vector getFindResult(String room, int sid, Search srch, Vector docs) 
                                                         throws RemoteException; 
    public Vector getIndexValues(String room, int sid, int dtid, int iid,  
                                          Vector values) throws RemoteException;
    public int addIndexerDoc(String room, int sid, int nodeId, int option) 
                                                        throws RemoteException;
    public int addATRecord(String room, int sid, int nid, int otype, int eid, 
                                            String desc) throws RemoteException;
    public int registerOutput(String room, int sid, String output)
                                                         throws RemoteException;
    public int isVWSignInstalled() throws RemoteException; 
    //--------------------------------------------------------------------------
    public boolean isAdmin(String room,int sid) throws RemoteException;
    public boolean isSecurityAdmin(String room,int sid)throws RemoteException;
    public boolean enableSecurityAdmin(String room,int sid)throws RemoteException;
    public boolean isManager(String room,int sid) throws RemoteException;
    public Vector getGroups(String room, int sid, Vector groups)
                                                         throws RemoteException;
    public Vector getPrincipals(String room, int sid, Vector princ)
                                                         throws RemoteException;
    
	/**CV2019 merges from SIDBI line***/
    public Vector getPrincipalsSecurity(String room, int sid, Vector princ,String startsWith,int mode)
            throws RemoteException;
    
    /**CV2019 merges from 10.2 line***/
	public Vector getPrincipalsWithNoSign(String room, int sid, Vector princ)
			throws RemoteException;
    public Vector getPrincipalMembers(String room, int sid, String pid, 
                                           Vector princ) throws RemoteException;
    public int setNodeAcl(String room, int sid, Acl acl) 
                                                         throws RemoteException;
    public Acl getNodeAcl(String room, int sid, Acl acl, Principal principal, 
                                      boolean effective) throws RemoteException;
    public Acl getAcl(String room, int sid, Acl acl) throws RemoteException;
    public Acl getAclFor(String room, int sid, Acl acl, String user) 
                                                         throws RemoteException;
    public ServerSchema getDSSforDoc(String room, int sid, Document doc, 
                                        ServerSchema ss) throws RemoteException;
    public Vector getDSSStores(ServerSchema ss, Vector stores) 
                                                         throws RemoteException;
    public int addDSSStore(ServerSchema svr) throws RemoteException;
    public int updateAzureStorageInfo(String room,String storageValues,String storageUserName,String StoragePassword) throws RemoteException;
    
    public int removeDSSStore(ServerSchema svr) throws RemoteException;
    public String getEncryptionKey(String room, String GUID) 
                                                         throws RemoteException;
    public Vector getDSSList(String room, int sid, Vector dssList) 
                                                         throws RemoteException;
    //public String getLicenseKey() throws RemoteException;
    //public void setLicenseKey(String license) throws RemoteException;
    public String getLicenseFile() throws RemoteException;
    public String gethotFolderPath() throws RemoteException;
    public void setLicenseFile(String license) throws RemoteException;
    public void setHotFolderPath(String location) throws RemoteException;
    public void setNamedUsers(String users) throws RemoteException;
    public void setNamedOnlineUsers(String users) throws RemoteException;
    
    public void setSecurityAdministrator(String subAdministrator)throws RemoteException;
    public void setProfessionalNamedUsers(String users) throws RemoteException;
    // Email server related methods added 30 Jan 2007
    public void setEmailServerName(String emailServerName) throws RemoteException;
    public void setFromEmailId(String emailId) throws RemoteException;
    public void setEmailServerPort(String emailServerPort) throws RemoteException;
    public void setEmailServerUsername(String emailServerName) throws RemoteException;
    public void setEmailServerPassword(String emailServerPort) throws RemoteException;    
    public String getEmailServerName() throws RemoteException;
    public String getFromEmailId() throws RemoteException;
    public String getEmailServerPort() throws RemoteException;
    public String getEmailServerUsername() throws RemoteException;
    public String getEmailServerPassword() throws RemoteException;
    
    public String getNamedUsers() throws RemoteException;
    public String getNamedOnlineUsers() throws RemoteException;
    public String getConcurrentOnlineUsers() throws RemoteException;
    public String getProfessionalNamedUsers() throws RemoteException;
    public String getEnterPriseConcurrent() throws RemoteException;
    public String getProfessionalConcurrent() throws RemoteException;
    public String getSecurityAdministrator() throws RemoteException;
    public int getLoginLockTime() throws RemoteException;
    public void setLoginLockTime(int minutes) throws RemoteException;
    public int getMaxLoginAttempts() throws RemoteException;
    public void setMaxLoginAttempts(int count) throws RemoteException;
    public Vector getLockedUsers() throws RemoteException;
    public void unlockUser(String ip) throws RemoteException;
    public void applyNewSettings(int setting) throws RemoteException;
    public Vector getRoomStatistics(String room, int sid, Vector info) 
                                                         throws RemoteException;
    public int isTextSearchEnabled(String room, int sid) throws RemoteException;
    public Vector adminWise(String room, int sid, String action, String param,
                                          Vector result) throws RemoteException;
    public String getSSType() throws RemoteException;
    public String getSSAuthorsHost() throws RemoteException;
    public String getSSLoginHost() throws RemoteException;
    public String getTree() throws RemoteException;
    public String getLoginContext() throws RemoteException; 
    public String getAuthorsContext() throws RemoteException;
    public String getLdapPort() throws RemoteException;
	public void setLdapPort(String port) throws RemoteException;    
    public void setSSType(String type) throws RemoteException;
    public void setSSLoginHost(String host) throws RemoteException;
    public void setSSAuthorsHost(String host) throws RemoteException;
    public void setTree(String tree) throws RemoteException;
    public void setLoginContext(String context) throws RemoteException;    
    public void setAuthorsContext(String context) throws RemoteException;
    public void removeRoom(String room) throws RemoteException;
    public void addRoom(RoomProperty room) throws RemoteException;
    public RoomProperty[] getRooms() throws RemoteException;
    public RoomProperty[] getRegRooms() throws RemoteException;
    public void setRoomState(String room, boolean state) throws RemoteException;
    public void setTabStatus(String room,int tabstatus)throws RemoteException;
    public void setToggleStatus(String room,int togglestatus)throws RemoteException;
    public boolean getRoomState(String room) throws RemoteException;
    public Vector getNodeAllDocuments(String room, int sid, int nid, 
                        int sessionId, Vector documents) throws RemoteException;
    public Vector getDocumentInfo(String room, int sid, Document doc, 
                                       Vector documents) throws RemoteException;
    public int createSession(String room, int sid, Node node, 
                                     String sessionName) throws RemoteException;
    public int releaseSession(String room, int sid, Node node) 
                                                         throws RemoteException;
    public Vector getRecycleDocList(String room, int sid, int rowCount, int lastRowId, int actionType, Vector documents) 
                                                         throws RemoteException;
    public Vector getRecycleDocList(String room, int sid, int rowCount, int lastRowId, int actionType, String indexValue, Vector documents) 
    throws RemoteException;

    public int getRecycleDocCount(String room, int sid, String indexValue) throws RemoteException;
    public Vector getLockDocList(String room, int sid, Vector documents) 
                                                         throws RemoteException;
    public int setNodeStorage(String room, int sid, Node node, int withInherit)
                                                         throws RemoteException;
    public int isDocTypeFound(String room, int sid, DocType docType)
                                                         throws RemoteException;
    public int isIndexFound(String room, int sid, Index index, int checkType, DocType dType)
                                                         throws RemoteException;
    public int deleteDocType(String room, int sid, DocType docType)
                                                         throws RemoteException;
    public int deleteIndex(String room, int sid, Index index)
                                                         throws RemoteException;
    public int deleteDTIndex(String room, int sid, Index index)
                                                         throws RemoteException;
    public int setDocTypeAutoMail(String room, int sid, DocType docType)
                                                         throws RemoteException;
    public int restoreNodeFromRecycle(String room, int sid, Node node) 
                                                         throws RemoteException;   
    public Vector getUserRedactions(String room, int sid, String userName, 
                                       Vector documents) throws RemoteException;
    public int setUserRedactions(String room, int sid, String userName, 
        String oldPassword, String newPassword, Vector docs) 
                                                         throws RemoteException;
    public Vector getAllRedactions(String room, int sid, Document doc, 
                                      Vector redactions) throws RemoteException;
    public int setDTVersionSettings(String room, int sid, DocType docType) 
                                                         throws RemoteException;
    public Vector getDTVersionSettings(String room, int sid, DocType docType) 
                                                         throws RemoteException;
    public Vector getATSettings(String room, int sid) 
                                                         throws RemoteException;
    public int setATSetting(String room, int sid, int eventId, int status) 
                                                         throws RemoteException;
    public Vector getATData(String room, int sid, Vector filter) 
                                                         throws RemoteException;
    public int detATData(String room, int sid, Vector filter,String archivePath) 
                                                         throws RemoteException;
    public int updateDocType(String room, int sid, DocType docType)  
                                                         throws RemoteException;
    public int updateIndex(String room, int sid, Index index)  
                                                         throws RemoteException;
    public int updateDTIndex(String room, int sid, int docTypeId, Index index)  
                                                         throws RemoteException;
    public int updateIndexSelection(String room, int sid, Index index, 
                                       String selValues, String clientMode) throws RemoteException;
    public Vector getIndices(String room, int sid, int indexId, Vector indices) 
                                                         throws RemoteException;
    public Vector getDocTypeInfo(String room, int sid, int docTypeId, 
                                     Vector docTypeInfo) throws RemoteException;
    public int resyncDirectory(String room, int sid) throws RemoteException;
    public Vector checkNextDocVersion(String room, int sid, Document doc, 
                               String pi,boolean newDoc) throws RemoteException; 

    public int setDocAuditInfo(String room, int sid, Document doc, 
                        boolean newDoc, String pageInfo) throws RemoteException;
    //-----------------------------------eWork----------------------------------
    public Vector getEWorkTemplates(String room, int sid, Vector templates)
                                                         throws RemoteException;
    public EWorkTemplate newEWorkTemplate(String room, int sid, 
                                 EWorkTemplate template) throws RemoteException;
    public int updateEWorkTemplate(String room, int sid,  
                                 EWorkTemplate template) throws RemoteException;
    public int deleteEWorkTemplate(String room, int sid,  
                                 EWorkTemplate template) throws RemoteException;
    public Vector getEWorkTemplateMapping(String room, int sid, int templateId, 
                                         Vector mapping) throws RemoteException;
    public EWorkSubmit newEWorkSubmit(String room, int sid, 
                                   EWorkSubmit ewSubmit) throws RemoteException;
    public Vector getEWorkSubmits(String room, int sid, Vector ewSubmits)
                                                    throws RemoteException;
    public Vector getEWorkSubmitMapping(String room, int sid, int submitId, 
                                         Vector mapping) throws RemoteException;
    public int endEWorkSubmit(String room, int sid, int submitId) 
                                                         throws RemoteException;
    //---------------------------------------AIP--------------------------------
    public int addMimeMessage(String room, int sid,String mesgParms) 
                                                         throws RemoteException;
    public Vector getMimeMessages(String room,int sid) 
                                                         throws RemoteException;
    
    public int deleteOldMimeMessages(String room, int sid,String hours) 
                                                         throws RemoteException;
    public int deleteMimeMessage(String room, int sid,int msgId) 
                                                         throws RemoteException;
    public Hashtable getAIPInit(String room,int sid,String ipaddress) throws RemoteException;
    public int setAIPInit(String room,int sid,Hashtable initMap,String ipaddress) throws RemoteException;
    public Hashtable getAIPInit(String room,int sid) throws RemoteException;
    public int setAIPInit(String room,int sid,Hashtable initMap) 
                                                         throws RemoteException;
    public int setAIPForm(String room,int sid,String formParams) 
                                                         throws RemoteException; 
    public int setAIPFormField(String room,int sid,int formId,String fieldParam) 
                                                         throws RemoteException;
    public Vector getAIPForms(String room,int sid) throws RemoteException;        
    public Vector getAIPFormFields(String room,int sid,int formId) 
                                                         throws RemoteException;
    public int deleteAIPForm(String room,int sid,int formId) 
                                                         throws RemoteException;
    public int deleteAIPFormFields(String room,int sid,int formId)
                                                         throws RemoteException;
    public int deleteAIPFormField(String room,int sid,int fieldId) 
                                                         throws RemoteException;
    public Vector getAIPFormMap(String room,int sid,int formId,int mapDTId) 
                                                         throws RemoteException;
    public int  setAIPFormFieldMap(String room,int sid,String fieldMapParms) 
                                                         throws RemoteException;
    public int deleteAIPFormMap(String room,int sid,int formId,int mapDTId) 
                                                         throws RemoteException;
    public Vector getAIPHistoryFiles(String room,int sid) 
                                                         throws RemoteException;
    // Code added for EAS Enahance History Tab - Start
    public Vector getAIPHistoryFiles(String room,int sid,String params)
														 throws RemoteException;
    public Vector getAIPHistoryCount(String room,int sid,String params) 
    													throws RemoteException;
    // Code added for EAS Enahance History Tab - End
    // Archive History Files - Start
    public Vector getAIPHistoryFilesForArchive(String room,int sid,String params)
                                                        throws RemoteException;
    // Archive History Files - End
    public int deleteAIPHistoryArchivedFiles(String room,int sid)
    throws RemoteException;

    //Issue 04080603
    /*
   There is a significant delay in the launch of the application,
   then a significant delay as soon as a connection has been established
    to the room, If populating of the Messages ID tab is the reason then this 
    should be eliminated and fetched on demand,
   */
    public Vector getMimeMsg(String room,int sid,String params)throws RemoteException;
    public Vector getMimeMessages(String room,int sid,String params)
	 													throws RemoteException;
    public Vector getMimeMessagesCount(String room,int sid) 
														throws RemoteException;
    //Issue 04080603
    public int setAIPHistoryFile(String room,int sid,String fileParams)
                                                         throws RemoteException;
    public int deleteAIPHistoryFile(String room,int sid,int fileId)
                                                         throws RemoteException;
    public int setAIPDocument(String room,int sid,String docParams) 
                                                         throws RemoteException;
    public int setAIPDocFields(String room,int sid,Vector fieldsParams) 
                                                         throws RemoteException;
    public Vector getAIPDocuments(String room,int sid,int aipFileId, int pendingStatus) 
                                                         throws RemoteException;
    public Vector getAIPDocFields(String room,int sid,int aipDocId) 
                                                         throws RemoteException;
    public int deleteAIPDocument(String room ,int sid,int aipDocId) 
                                                         throws RemoteException;
    public Vector getAIPEmailSuffixes(String room,int sid) 
                                                         throws RemoteException;
    public int setAIPEmailSuffixes(String room,int sid,Vector suffixes) 
                                                         throws RemoteException;
    public int isSignAnnotationExist(int sid, String room) throws RemoteException;
    public int setUserSign(String room, int sid, String userName,
                            int signType, VWDoc signDoc) throws RemoteException;
    public int removeUserSign(String room, int sid, String userName, int signType)
    													throws RemoteException;
    public VWDoc getUserSign(String room, int sid, String userName, 
                            int signType, VWDoc signDoc) throws RemoteException;
    public int setDocumentSign(String room,int sid, Signature sign) 
                                                         throws RemoteException;
    public Vector getDocumentSign(String room,int sid, Signature sign) 
                                                         throws RemoteException;
    public Vector getDocumentSigns(String room,int sid, Document doc) 
                                                         throws RemoteException;
    public int updateStorageInfo(String room,int sid, ServerSchema storage) 
                                                         throws RemoteException;
    public Vector DBGetDocPageText(String room, int sid, Document doc) 
                                                         throws RemoteException;
    public int getActiveError() throws RemoteException;
    // 	Issue 511
    public Vector getDSSIPPort(ServerSchema ss,String location)
    													throws RemoteException;
    // Issue 511
    
    //  Add the Encoding method to read the encoding type from server registry.
    public String getEncoding()	throws RemoteException;
    // End
// Issue 501
    
    //  Add the closeRoom method to close the room which database is unavailable.    
    public void closeRoom(String room) throws RemoteException;
/*
Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
Created by: <Pandiya Raj.M>
Date: <6 Jun 2006>
*/
    
    public void DBRemovePageText(String room, int docId, int pageId) throws RemoteException;
    
     /*
     * Added for Novell Netware issue 736 - MDSS server ping issue
     */
    public int getRoomIdleTime(String room) throws RemoteException;
    public int isRoomIdle(int sid, String roomName) throws RemoteException;
    
    /*
    Purpose:  Following methods added for ARS
    Created by: <Valli>
    Date: <20 July 2006>
    */
    
    public int arsMoveNode(String room, int sid, Node node) throws RemoteException;
    
    public int arsLockDocument(String room, int sid, int did)throws RemoteException;
       
    public boolean isDocumentLocked(String room, int sid, int did, Vector name)  throws RemoteException;

    public Vector getDTRetentionSettings(String room, int sid, DocType docType)	throws RemoteException;
    public int setDTRetentionSettings(String room, int sid, DocType docType)	throws RemoteException;
    public Vector getDisposalActions(String room, int sid, Vector disposalActs)  throws RemoteException;

    /* Purpose:  Following ARS methods changed for Oracle script changes  
    * Created by: <Valli>	Date: <08 Sep 2006>
    */
    public Vector getRetentionDocuments(int sessionID, String room, Vector retentionDocs)  throws RemoteException;
    public Vector getARSReportDocTypes(int sessionID, String room, Vector ARSDocTypes)  throws RemoteException;
    public Vector getReportRetentionDocs(int sessionID, String room, String docTypeID)  throws RemoteException;
	
    public boolean isDocDeleted(String room, int sid, int docID) throws RemoteException;
    public int isDocFreezed(String room, int sid, int docID) throws RemoteException;
    public Vector getFreezeDocs(String room, int sid) throws RemoteException;
    public int unFreezeDocument(String room,int sid, int docID)throws RemoteException;
    /*public int freezeDocument(String room,int sid, int docID, String retentionId)throws RemoteException;*/
    public int freezeDocument(String room,int sid, int docID, String docName, String retentionId)throws RemoteException;
    public int getARSEnable(int sid)throws RemoteException;
    /*	Enhancement No:66-To read build time of server ViewWise.jar from manifext file
	 *	Created by: <Shanmugavalli.C>	Date: <04 Aug 2006>
	 */
    public String getSvrJarBuildTime()	throws RemoteException;

    public String getSvrJarBuildVersion()	throws RemoteException;
    /*End of ARS methods*/
    //Start of DSS related method calls

    public VWDoc setDocument(String room, ServerSchema dssSchema, Document doc, File file,ServerSchema svr) throws RemoteException;
    public VWDoc setDocFolder(Document doc) throws RemoteException;
    public VWDoc getRMIDoc(Document doc, String room, String dssSchema_path, ServerSchema dssSchema, ServerSchema mySchema, boolean insideIdFolder)throws RemoteException;
    // Add the one more parameter to check the version/revision have the document or not
    public VWDoc getRMIDoc(Document doc, String room, String dssSchema_path, ServerSchema dssSchema, ServerSchema mySchema, boolean insideIdFolder, Vector versionList)throws RemoteException;
	/**CV2019 merges from SIDBI line***/
    public VWDoc getRMIDocWeb(Document doc, String room, String dssSchema_path, ServerSchema dssSchema, ServerSchema mySchema, boolean insideIdFolder, Vector versionList)throws RemoteException;
    public VWDoc getNextRMIDoc(Document doc, String room, String dssSchema_path, ServerSchema dssSchema, ServerSchema mySchema, boolean insideIdFolder)throws RemoteException;
    public boolean getDocument(Document doc, String room, String path, ServerSchema dssSchema,ServerSchema mySchema, boolean insideIdFolder) throws RemoteException;
    public boolean moveDocFolder(Document doc, String room, ServerSchema Sdss,ServerSchema Ddss) throws RemoteException;
    public boolean getDocFolder(Document doc, String room, String path, ServerSchema dssSchema, ServerSchema s) throws RemoteException;
    public boolean deleteDocument(ServerSchema dssSchema, Document doc) throws RemoteException ;
    public long getLastModifyDate(ServerSchema dssSchema, Document doc) throws RemoteException;
    /**CV2019 merges from SIDBI line***/
	public boolean isAllWebExist(ServerSchema dssSchema, Document doc) throws RemoteException;
    
    public long getDocSize(ServerSchema dssSchema,Document doc, boolean withVR) throws RemoteException;
    public long getDocSize(ServerSchema dssSchema,Document doc, boolean withVR, String version) throws RemoteException;    
    public int getPageCount(ServerSchema dssSchema, Document doc, String room) throws RemoteException;
    // Add the one more parameter to check the version/revision have the document or not
    public int getPageCount(ServerSchema dssSchema, Document doc, String room, Vector versionList) throws RemoteException;
    public boolean deleteDocumentVersions(ServerSchema dssSchema, Document doc) throws RemoteException;
    // Add the following method for download the document through VWS
    public ArrayList getRMIDocList(ServerSchema dssSchema, String srcPath) throws RemoteException;
    public VWDoc getRMIDoc(Document doc, String room, String path, 
            ServerSchema svr, ServerSchema dssSchema) throws RemoteException;	
    public void writeLogMsg(ServerSchema dssSvr, Document doc,String room) throws RemoteException;
   	//Function Signature for issue 793 
    public int resetSecurityDetails(int sid,String roomName,int nodeID, int aclID,Vector result) throws RemoteException;
    public boolean pingDB(String roomName) throws RemoteException;
    
    /*  LDAP related attribute methods added here Valli 13 March 2007*/
	public void setAdminPass(String adminPass)    throws RemoteException;
    public String getAdminPass()    throws RemoteException;
    public void setLdapSecurity(String ldapSecurity)    throws RemoteException;
    public String getLdapSecurity()    throws RemoteException;
    
	/*Start of DRS methods 19 Dec 2006*/
    public String getAcceptLabel(String room, int sid, int routeId, int taskId) throws RemoteException;
    public String getRejectLabel(String room, int sid, int routeId, int taskId) throws RemoteException;    
    public Vector getRouteInfo(String room, int sid, int routeId, Vector routeInfo) throws RemoteException;
    public VWDoc getRouteImage(String room, int sid, int routeId, VWDoc routeImageDoc) throws RemoteException;
    public Vector getRouteIndexInfo(String room, int sid, int routeId, Vector routeIndexInfo) throws RemoteException;
    public Vector getRouteTaskInfo(String room, int sid, int routeId, int taskSequence, Vector routeTaskInfo) throws RemoteException;
    
    public int setRouteInfo(String room, int sid, RouteInfo route)throws RemoteException;
    public int setRouteTaskInfo(String room, int sid, RouteTaskInfo routeTaskInfo)throws RemoteException;
    public int setRouteIndexInfo(String room, int sid, RouteIndexInfo routeIndexInfo)throws RemoteException;
    
    public Vector getRouteUsers(String room, int sid, int routeTaskId, Vector vRouteUsers) throws RemoteException;
    public Vector getRouteUsers(String room, int sid, int routeTaskId, Vector vRouteUsers, int docId) throws RemoteException;
    /*public Vector getRouteUser(String room, int sid, int routeUserId, Vector docRouteUsers) throws RemoteException;*/
    public int setRouteUsers(String room, int sid, ArrayList routeUserList, int routeId, int routeTaskId) throws RemoteException;
    public int setDocTypeRoutes(String room,int sid, int RouteId, int DocTypeId)throws RemoteException;
    public int setDocTypeRoutes(String room,int sid, ArrayList RouteId, int DocTypeId)throws RemoteException;
    public Vector getDocTypeRoutes(String room, int sid, int DocTypeId, Vector docTypeRoutes)throws RemoteException;
    public int setUserMail(String room,int sid, int userId, String userMailId) throws RemoteException;
    public Vector getUserMail(String room, int sid, String userName, Vector userMailId) throws RemoteException;
    public Vector getAssignedRoutesOfDocType(String room, int sid, int docTypeId) throws RemoteException;
    public int setRouteHistory(String room, int sid, int DocId, int routeId, int taskSeq, int routeUserId, String routeInitiatedBy, String approvedUserName, String authorizedUserName, int signId, int routeMasterId) throws RemoteException;
    public int setRouteHistory(String room, int sid, int DocId, int routeId, int taskSeq, int routeUserId, String routeInitiatedBy, String approvedUserName, String authorizedUserName, int signId, int routeMasterId, String groupName) throws RemoteException;
    public Vector getToDoRouteList(String room, int adminUser, int routeId, int sid) throws RemoteException;
    
    /*public int routeLockDocument(String room, int sid, int did, String docOwner)    throws RemoteException;
    public int routeUnlockDocument(String room, int sid, int did, String routeUser)   throws RemoteException;*/
    
    public int updateRouteHistory(String room, int sid, int docId, int routeId, int routeUserId, String status, String comments, String prevStatus, int routeMasterId,int routeHistroyId, String approvedUserName, String authorizedUserName, int signId) throws RemoteException ;
    public int deleteDocumentUsers(String room, int sid, int routeId)  throws RemoteException;
    public int isRouteInUse(String room, int sid, int routeId, int mode)  throws RemoteException;
    public Vector isUserInRoute(String room, int sid, int principalId)  throws RemoteException;
    public int deleteRouteInfo(String room, int sid, int routeId, int isRouteNotUsed)  throws RemoteException;    
    public int deleteRouteTaskUserIndexInfo(String room, int sid, int routeId, int markAsDelete, int indexBaseRoute)  throws RemoteException;
    public Vector getDocumentsInRoute(String room, int sid, int docId, int sourceType) throws RemoteException;
    public Vector getDocsToAutoRoute(String room, int sid, Vector documentsToRouteList) throws RemoteException ;
    public Vector getDocsForFinalAction(String room, int sid, Vector routeCompletedDocs) throws RemoteException ;
    public int updateRouteCompletedDocs(String room, int sid, int routeMasterId, String taskFlag) throws RemoteException;
    /*public int sendMail(String room, int sid, int userID, String docName, String routeName, Vector userInfo) throws RemoteException;*/
    public int sendMailWithAttachment(String room, int sid, String userName, String docName, 
    		String routeName, int docId, Vector userInfo, Vector attachment, String status, Vector docNames, Vector emailOptions, String destPath) throws RemoteException;
    public int sendMailWithAttachment(String room, String serverName, int sid, String userName, String docName, 
    		String routeName, int docId, Vector userInfo, Vector attachment, String status, Vector docNames, Vector emailOptions, String destPath) throws RemoteException;
	/****CV2019 merges from SIDBI line--------------------------------***/
    /*public int sendMailWithAttachment(String room, String serverName, int sid, String userName, String docName, 
    		String routeName, int docId, Vector userInfo, Vector attachment, String status, Vector docNames, Vector emailOptions, String destPath,String clientType) throws RemoteException;*/
    public int sendMailWithAttachment(String room, String serverName, int sid, String userName, String docName, 
    		String routeName, int docId, Vector userInfo, Vector attachment, String status, Vector docNames, Vector emailOptions, String destPath, String clientType, String secureURL) throws RemoteException;
    /*public int sendMailWithAttachmentWS(String room, String serverName, int sid, String userName, String docName, 
	String routeName, int docId, Vector userInfo, Vector attachment, String status, Vector docNames, Vector emailOptions, String destPath, String secureurl) throws RemoteException;*/
	/*--------------------------------------------------------------*/
    public Vector getRouteSummary(String room, int sid, int docId, int mode) throws RemoteException;
	/****CV2019 merges from SIDBI line--------------------------------***/
    public Vector getRouteNewSummary(String room, int sid, int docId, int mode) throws RemoteException;
    public Vector getDocumentVersionInfo(String room, int sid, int docID) throws RemoteException;
	/*--------------------------------------------------------------*/
    public int isRouteNameFound(String room, int sid, String routeName) throws RemoteException;
    public int isRouteLocationFound(String room, int sid, String nodeId) throws RemoteException;
    public int getDRSEnable(int sid)throws RemoteException;
    public int setDRSEnable(int sid, String status)throws RemoteException;
    public boolean DBInsertPageText(int sid, String room, int did, int pno, int pid, int pvr,int qc,String localPath)throws RemoteException;
    public int getRouteTaskCount(String room, int sid,  int routeId)throws RemoteException;
    public int updateStatusOfOutOfTskUsrs(String room, int sid,  int routeMasterId, int taskSeq, String status)throws RemoteException;
    public Vector getDRSPrincipals(String room, int sid, int type, String groupName, Vector princ)throws RemoteException;
    public Vector getRouteCompletedDocuments(String room, int sid, int routeId, int adminUser) throws RemoteException;
    public Vector getTaskCompletedDocuments(String room, int sid, int routeId) throws RemoteException;    
    public Vector checkGroupsInRoute(String room, int sid) throws RemoteException;
    public int syncRouteGroups(String room, int sid, String processFlag) throws RemoteException;
    //For Invalid route
    public Vector getRouteInvalidTasks(String room, int sid, int routeId, Vector tasks)throws RemoteException;
    public int isRouteValid(String room, int sid, int routeId) throws RemoteException;
	public int syncInvalidRouteUsers(String room, int sid, int routeId)throws RemoteException;
	public Vector getEscalatedDocList(String room, int sid, Vector escalatedDocsList)throws RemoteException;
	public int setUserEscalation(String room, int sid, RouteMasterInfo routeMasterInfo)throws RemoteException;
	public int sendEscalationMail(String room, int sid, RouteMasterInfo routeMasterInfo, String esclationUserEmailId, String messageStr)throws RemoteException;
	//---
    /*End of DRS methods*/
    public boolean isFTSEnabled(int sid, String room, String doctypeID)throws RemoteException;
    public int getApprovedCountForTask(String room, int sid,  int routeId, int routeTaskId, int docId, int routeUserId)throws RemoteException;
    // DL enhancement methods. Valli 8 May 2007
    public int setCustomisedIndexList(int hitList, int sid, String room, int nodeId, int rowCount, String customIndexList)throws RemoteException;
    public Vector getCustomisedIndexValues(int sid, String room, int nodeId, int rowCount, int refNid, String customIndexList, Vector result)throws RemoteException;
    public Vector getCustomisedSearchResult(String room, int sid, String customIndexList, Search srch, Vector docs)throws RemoteException;
    // Read/Write the customised settings information to the Database
    public int addCustomList(int sid, String room, String userName, int objectType, String settingsId, String settingsValue, int nodeId)throws RemoteException;
    public Vector getCustomList(int sid, String room, String userName, int objectType, Vector settingsValue, int nodeId)throws RemoteException;
    public int deleteCustomList(int sid, String room, String userName, int mode, int nodeId)throws RemoteException;
    public int isCustomListInherited(int sid, String room, int objectType, int nodeId)throws RemoteException;
    public int checkDocsInRoute_CheckedOut(int sid, String room, int nodeId, boolean flagRoute)throws RemoteException;
	/**CV2019 merges from SIDBI line***/
    public int checkDocIsInRoute(int sid, String room, int nodeId, boolean flagRoute)throws RemoteException;
    //Overrided Method to Check locked docuements from the DTC
    public int checkDocsInRoute_CheckedOut(int sid, String room, int nodeId, int flagRoute)throws RemoteException;
    	
    public Vector getDocumentAuditDetails(String room, int sid, int docId, int mode) throws RemoteException;
    public int getAclForNode(String room, int sid, int nodeId, String userName) throws RemoteException;
    public int nodeShortCutAdd(int sid, String room, int parentNodeId, String nodeName, String description) throws RemoteException;
    public int nodeShortCutDelete(int sid,  String room,  int nodeId) throws RemoteException;
    public Vector getNodeShortCutDesc(int sid,  String room, int nodeId) throws RemoteException;
    //Add method for default properties for node.
    public int updateNodeRules(String room, int sid, NodeIndexRules nodeIndexRules) throws RemoteException;
    public Vector getNodeRules(String room, int sid, int nodeId) throws RemoteException;
    public Vector checkNodeIndexRules(String room, int sid, int nodeId) throws RemoteException;
    public Vector checkNodeTreeIndexRules(String room, int sid, int nodeId) throws RemoteException;
    public int deleteNodeRules(String room, int sid, int nodeId) throws RemoteException;
    public Vector getAllDocTypeIndices(String room, int sid, int nodeId, 
            Vector indices) throws RemoteException;
    public Vector getDocTypeIndicesWithRules(String room, int sid, int nodeId, int docTypeId,  
            Vector indices) throws RemoteException;
    //Method for enhancement for creating the template for security
    public Vector getNodeContentsWithAcl(String room, int sid, int nid, int coid,
            Vector nodes, int sort) throws RemoteException;
    public int copyNodeSecurity(String room, int sid, int nodeId, String ip, String userGroup, String permission) throws RemoteException;
    public int updateLockedDocument(String room, int sid, int did) 
    throws RemoteException;    
    public boolean isDSSLocationExists(ServerSchema dssSchema, String location) throws RemoteException;
    public Vector getFTSEnabledPages(String room, int sid, int nodeId, Vector vecPages) throws RemoteException;
    public Vector getKeyIndexName(String room, int sid, int docTypeId, Vector vecKeyIndexName) throws RemoteException;
    public int getMessageIDExpiry(String room) throws RemoteException;
    public void setMessageIDExpiry(String room, int hours) throws RemoteException;
    public int isUserExistsInRoute(String room, int sid,  int userId)throws RemoteException;
    public void updateVersionRevision(String room, ServerSchema dssSchema, Document doc, String newVersion) throws RemoteException;
    public void setPageCount(String room, int sid, int docId, int pCount) throws RemoteException;

    //public int setDSNInfo(String room, int sid, int docTypeId, String dsnName, String userName, String password, String externalTable)throws RemoteException;
    public int setDBLookup(String room, int sid, DBLookup lookupInfo)throws RemoteException;
    public Vector getDSNInfo(String room, int sid, int docTypeId, int indexId, int dsnType)throws RemoteException;
    //public int setIndexMapping(String room, int sid, DocType dt, int triggerDataType)throws RemoteException;
    public Vector getIndexMapping(String room, int sid, DocType dt)throws RemoteException;
    public Vector generateXmlfile(String room, int sid, String finalParams)throws RemoteException;
    public Vector getExternalData(String room, int sid, DocType dt) throws RemoteException;
    public Vector getExternalLookupColumns(String room, int sid, String dsnName, String userName, String passWord, String externalTable, String query) throws RemoteException;
	public Vector getFolders(String room, int sid, String folderName) throws RemoteException;
	/**CV2019 merges from SIDBI line***/
	public Vector searchNodePath(String room, int sid, String folderName,String folderPath) throws RemoteException;   
   //Enhancement - Custom Color Code 
    public Vector getNodeRGBColor(String room, int sid)throws RemoteException;
    public int setNodeCustomColor(String room, int sid, int nodeId, int colorId, boolean type, boolean inherit, int scope)throws RemoteException;
    public Vector getNodeCustomColorCode(String room, int sid, int nodeId)throws RemoteException;
    public void deleteNodeCustomColorCode(String room, int sid, int nodeId)throws RemoteException;
	public Vector getAvailableRetentions(String room, int sid, Vector availRetention) throws RemoteException;
	public Vector getRetentionSettings(String room, int sid, int retentionId, Vector retentionSettings) throws RemoteException;
	public Vector getRetentionIndexInfo(String room, int sid, int retentionId, Vector retentionIndexInfo) throws RemoteException;
	public int setRetentionSettings(String room, int sid, VWRetention retention, Vector retentionId) throws RemoteException;
	public int setIndexConditions(String room, int sid, VWRetention retention) throws RemoteException;
	public int delRetentionSettings(String room, int sid, VWRetention retention, int deleteOption) throws RemoteException;
	public int delIndexInfo(String room, int sid, VWRetention retention) throws RemoteException;
	public int checkRetentionInUse(String room, int sid, int retentionId, Vector retention) throws RemoteException;
	public int isRetentionNameFound(String room, int sid, String retentionName) throws RemoteException;
	public Vector getRetentionDocumentList(String room, int sid, int retentionId, Vector retentionDocs) throws RemoteException;
	public Vector getRetentionCompletedDocs(String room, int sid, int retentionId, Vector completedDocs) throws RemoteException;
	public Vector getNotifyDocumentList(String room, int sid, Vector notifyDocs) throws RemoteException;
	public int addNotifiedDocs(String room, int sid, int retentionId, int docId, int docTypeId)throws RemoteException;
	public int sendNotification(String room, int sid, int VWSCount, String message) throws RemoteException;
	public int sendNotififyDocMail(String room, int sid, int VWSCount, String message,Vector mailIds) throws RemoteException;
	public Vector getGroupMembers(String room, int sid, String groupId) throws RemoteException;
	public String getLoggedInUser(String room, int sid) throws RemoteException;
	//Added for deleting existing empty users from database.
	public int deleteEmptyUsers(String room) throws RemoteException;
	public int isDSSActive(String room, int sid) throws RemoteException;
	public boolean getPurgeDocs(String room, int sid, Vector purgeDocs) throws RemoteException;
	public boolean getKeepDocType(String room, int sid, Vector keepdoctypeFlag) throws RemoteException;
	public int setPurgeDocs(String room, int sid, String purgeDocs) throws RemoteException;
	public int setKeepDocType(String room, int sid, String keepDocTypeFlag) throws RemoteException;
	public Vector searchByIndex(String room, int sid, String docTypeName, String indexName, String indexValue, int conditionType, int includeCustom) throws RemoteException;
	public int authenticateUser(String room, int sid, String user, String pwd)throws RemoteException;
	public Vector getDocumentMetaDataInfo(String room, int sid, String docId, Vector documentInfo) throws RemoteException;
	public int revokeDocSignature(String room, int sid, int documentId) throws RemoteException;
	public int getARSServer(String room, int sid) throws RemoteException;
	public Vector getTreeDetails(String room, int sid) throws RemoteException;
	public long checkAvailableSpace(ServerSchema dssSchema,String dssLocation)throws RemoteException;
	public int saveNotificationSettings(String room, int sid, NotificationSettings settings)throws RemoteException;
	public Vector loadNotificationSettings(String room, int sid, int nodeId, int isAdmin, String userName, String module)throws RemoteException;
	public int sendNotificationAlert(String room, int sid, Vector mailIds, Vector mailMessage, Vector attachmentData, int status, int nodeType, int docId, int attachmentExist, Vector nodeName, int attempt) throws RemoteException;
	public Vector readNotificationDetails(String room, int sid, String FailedFlag, String NotificationRetries) throws RemoteException;
	public int updateNotificationHistory(String room, int sid, int sentFlag,int failedflag, int historyCount, String historyIds) throws RemoteException;
	public Vector getLoggedInUserMailId(String room, int sid) throws RemoteException;
	public int deleteNotificationSettings(String room, int sid, NotificationSettings settings) throws RemoteException;
	public int addNotificationHistory(String room, int sid, int nodeId, int nodeType, int notifyId, String userName, String oldValue, String newValue, String message)throws RemoteException;
	public int setNotificationHistory(String room, int sid, int folderId, int nodeId, int nodeType, int settingsId, String oldValue, String newValue, String modifiedUser, String message)throws RemoteException;
	public Node getNodePropertiesByNodeID(String room, int sid, Node node)throws RemoteException;
	public int CheckNotificationExist(String room, int sid, int nodeId, int nodeType, int notifyId, String userName)throws RemoteException;
	public Vector getNotificationMasters(String room, int sid, int option, String module)throws RemoteException;
	public Vector getNotificationList(String room, int sid)throws RemoteException;
	public int setActiveClient(String room, int sid) throws RemoteException;
	public Vector getSignSettings(String room, int sid) throws RemoteException;
	public Vector getSignTitle(String room, int sid) throws RemoteException;
	public ServerSchema getDSSForLocation (String room, int sid, int moveLocationId, ServerSchema ss) throws RemoteException;
	public int updateIndexSelection(String room, int sid, Index index, Vector selValues, String clientMode) throws RemoteException;
	public int getIndexId(String room, int sid, Index index)  throws RemoteException;
	public Vector getNodeContentsWS(String room, int sid, int nid, int coid, Vector nodes, int sort) throws RemoteException;
	public Vector getOptionSettings(String room, int sid, Vector output) throws RemoteException;
	public int setOptionSettings(String room, int sid, String output) throws RemoteException;
	public void stopWorker(String room) throws RemoteException;
	public int getTaskApproversCount(String room, int sid, int routeId, int currentTaskSequence, int docId) throws RemoteException;
	public Vector getWebAccessUrl(String room, int sid) throws RemoteException;
	public Vector validateRouteDocument(String room, int sid, int routeId, int docId) throws RemoteException;
	public Vector getDocIndicesInRoute(String room, int sid, int did) throws RemoteException;
	public int changeUserPWD(String room,int sid,Principal principal,String oldPassword,String newPassword,String confirmPassword)throws RemoteException;
	public boolean getHostedAdmin(int sid) throws RemoteException;
	public Vector checkKeyIndex(String room, int sid, DocType docType, Vector indiceslist, int count)throws RemoteException;
	public int updateDocTypeKeyField(String room, int sid, int oldKeyIndexId, int newkeyIndexId, DocType docType)throws RemoteException;
	public Vector restoreIndicesCheck(String room, int sid,DocType docType, int dtindexCount,Vector restoreIndiceCheck)throws RemoteException;
	public Vector createOwnership(String room, int sid, String sessionName, int type, Vector nodes) throws RemoteException;
	public int releaseOwnership(String room, int sid, Node node, int type) throws RemoteException;
	public Vector checkOwnership(String room,int sid, int type, Vector nodes) throws RemoteException;
	public int ForceFTS(String room,int sid, int DocId) throws RemoteException;
	public Vector getLatestDocId(String room,int sid) throws RemoteException;
	public Vector getChildNodes(String room,int sid,int nodeid)throws RemoteException;
	public Vector getDocumentVersions(String room, int sid, int did,String version)throws RemoteException;
	public int CheckLockDocTypePerm(String room,int sid,int nodeid)throws RemoteException;
	public Vector getDocumentTypes(String room, int sid, int indexId, Vector indices)throws RemoteException;
	public Vector getDocumentTypeList(int sid, String room, Vector settingsValue, int nodeId)throws RemoteException;
	public Vector addDocumentTypeList(int sid, String room, String userName,String settingsValue, int nodeId)throws RemoteException;
	public int resetDocumentTypeList(int sid, String room, int nodeId)throws RemoteException;
	public Vector VWNodeGetDocTypes(int sid, String room, int nodeId,Vector documentTypes)throws RemoteException;
	public Vector ValidateNodeDocType(int sid, String room, int flag, int sourceNodeId,int destinationNodeId)throws RemoteException;
	public Vector getRouteUsers(String room, int sid, Vector creators)throws RemoteException;
	public Vector drsValidateTaskDocument(String room, int sid,int docId,int routeId,int action,String routeTaskSequence)throws RemoteException;
	public Vector getTaskIndicesInRoute(String room, int sid, int did) throws RemoteException;
	public Vector getArsValidationFinalAction(String room,int sid,int docId,String retentionId) throws RemoteException;
	public Vector getUserNameInOrder(String room,int sid, String previousValue,String currentValue) throws RemoteException;
	public int checkUser(String room,int sid, String userName) throws RemoteException;
	public int checkGroup(String room,int sid, String groupName) throws RemoteException;
	public Vector checkValidateSeqIndex(String room,int sid,int indexid) throws RemoteException;
	public Vector checkValidateUniqueSeq(String room,int sid,int doctypeId) throws RemoteException;
	public Vector checkValidateCopySeq(String room, int sid, Node node, int nodeProperties) throws RemoteException;
	public Vector resubmitDoctoWorkFlow(String room,int sid, String doc)throws RemoteException;
	public Vector showhideDoctoWorkFlow(String room,int sid, String doc)throws RemoteException;
	public int getEnableSelect(String room,int sid,String registryValue)throws RemoteException;
	public Vector getAllCabinets(String room, int sid)throws RemoteException;
	public Vector getFaxDocType(String room, int sid)throws RemoteException;
	public Vector getTodoListResubmit(String room, int sid,String doc,int routeHistoryId)throws RemoteException;
	public Vector getDRSValidateUpdateRoute(String room, int sid, int routeId,String routeName)throws RemoteException;
	public Vector getDWSMaintenanceFlag(String room, int sid)throws RemoteException;	
	public Vector getDRSReviewUpdWorkflow(String room, int sid)throws RemoteException;		
	public Vector getDRSGetReviewUpdFlag(String room, int sid)throws RemoteException;		
	public Vector getDRSGetWorkflowStatus(String room, int sid)throws RemoteException;
	public int resetUserPWD(String room, int sid, Principal principal,
			String oldPassword, String newPassword, String confirmPassword)throws RemoteException;
	public Vector workFlowUpdateSendEmail(String room, int sid) throws RemoteException;
	public Vector delUpdateWorkFlow(String room, int sid, int refId)throws RemoteException;
	public  Vector getMailId(String room,int sid, String userName)throws RemoteException;
	public Vector generateNodeSecurityReport(String room, int sid,
			String selectedPrincipal, int reportNodeId, int checkBoxValue)throws RemoteException;	
	//public Vector getDocPageCount(String room, int sid,int nodeId)throws RemoteException;	
	public Vector setDocstoNotify(String room, int sid,int nodeId,String userName)throws RemoteException;
	public Vector getUsertoNotifyDocs(String room, int sid,int nodeId)throws RemoteException;	
	public Vector delNotifiedDocs(String room, int sid,int nodeId) throws RemoteException;
	public Vector	setSubAdminRoles(String room,int sid,String user,String roles,int updFlag)throws RemoteException;
	public Vector	getSubAdminRoles(String room,int sid,String user)throws RemoteException;
	public Vector	removeSubAdminRoles(String room,int sid,String groupName)throws RemoteException;
	public Vector	checkSubAdmin(String room,int sid,String userName)throws RemoteException;
	public Vector getSubAdminTabs(String room,int sid,String userName)throws RemoteException;
	public Vector getDRSValidateToDoList(String room, int sid, String userName,int nodeId,String receivedDate)throws RemoteException;
	public Vector getGetDocsInRouteColor(String room, int sid,int colorType)throws RemoteException;
	public Vector getWorkFlowReportOptions(String room, int sid, int type,String routeId)throws RemoteException;
	public Vector generateWFReport(String room, int sid, int mode,	String availableIds, String tasks, String workFlowIds,String taskIds, String userGroupNames, String date)throws RemoteException;
	public Vector saveWFReport(String room, int sid, int mode,String reportName,String availableIds, String tasks, String workFlowIds,String taskIds, String userGroupNames, String date)throws RemoteException;
	public Vector checkWFReport(String room,int sid ,String reportName )throws RemoteException;
	public Vector getWFReport(String room, int sid,int flag,String reportName) throws RemoteException;
	public Vector getAgregateReport(String room, int sid, int mode,	String availableIds, String tasks, String workFlowIds,String taskIds, String userGroupNames, String date)throws RemoteException;
	public Vector deletWFReport(String room,int sid ,String reportName )throws RemoteException;
	public Vector getAzureStorageCredentials(String room,int sid,int docId)throws RemoteException;
	public Vector getAzureStore(ServerSchema ss, Vector stores)throws RemoteException;
	public Vector validateDynamicIndex(String room,int Sid,String indexValues)throws RemoteException;
	public Vector getStorageId(ServerSchema createStoreSchema, String location) throws RemoteException;
	public Vector getStorageFromId(ServerSchema serverSchema, String storageId)throws RemoteException;
	public Vector setAIPConnectionStatus(String room, int currSid)throws RemoteException;
	public File[] getTemplateFiles()throws RemoteException;
	public Vector setCVDTForm(String room,int sid,int docTypeId,String formName)throws RemoteException;
	public Vector getCVDTForm(String room,int sid,int docTypeId)throws RemoteException;
	public String getServerHome(String room,int sid)throws RemoteException;
	public Vector checkTodoDocsToNotify(String room, int sid)throws RemoteException;
	Vector getIndexInfoTemplate(String room, int sid, int retentionId)throws RemoteException;
	Vector getRetentionDoctypes(String room, int sid, int retentionId)throws RemoteException;
	public void checkUpdNotificationDocs(String room, int sid,int routeMasterId, int taskSequence)throws RemoteException;
	public int  isRetentionDocTypeFound(String room, int sid, String docTypeName)throws RemoteException;
	public int  isRetentionDTIndexFound(String room, int sid, String doctypeId,String indexName)throws RemoteException;
	public int  checkCVSearchNode(String room, int sid, String nodePath)throws RemoteException;
	public Vector cvCreateNodePath(String room, int sid, String panelName, Vector result)throws RemoteException;
	public Document createCVReportsDocument(String room, int sid, int docTypeId, int parentId, int idxCount, String indices, Document doc, DocType dt, String panelName, int indexId, String path) throws RemoteException;
	public Vector CVSetDTFormTemplate (String room, int sid,int tempalteId, String templateName, String formName, String sourceLocation, String destinationLocation, Vector result)throws RemoteException;
	public Vector CVGetDTFormTemplate (String room, int sid, Vector result)throws RemoteException;
	public Vector CVGetDTFormTemplateDetails(String room, int sid, String templateName, Vector result)throws RemoteException;
	public Vector CVDelDTFormTemplate(String room, int sid, String templateName, Vector result)throws RemoteException;
	public Vector CVCheckForTemplateExist(String room, int sid, String templateName, Vector result)throws RemoteException;
	public int createSearch1(String room, int sid, Search search) throws RemoteException;
	public int setCVWebDownloadPage(String room,int sid,int docId, String docName,String pageId) throws RemoteException;
	
	
    public int syncLockDocument(String room, int sid, int did,int type)throws RemoteException;
    public Vector setStickyNote(String room, int sid, DocComment dc,boolean withCheck,int docRestoreFlag) throws RemoteException;
    public int docDelStickyNote(String room, int sid, int did) throws RemoteException;
    public int setSecureLink(String room, int sid, int docId,String recepientEmail,String password,int modifyPermission,String deliveryOn,String epiresOn,String url ) throws RemoteException;
    public int setUpdateSecureLink(String room, int sid, int docId,String recepientEmail,String password,int modifyPermission,String deliveryOn,String epiresOn  ) throws RemoteException;
    public int getSecureLink(String room, int sid, int docId,int flag,String recepientEmail ) throws RemoteException;
    public int expireSelectedLink(String room, int sid, int secureLinkId)throws RemoteException;
    public Vector getSecureLinkDataAdminwise(String room, int sid, Vector info) throws RemoteException;
    public Vector readSecureLinkDetails(String room, int sid) throws RemoteException;
    public Vector readSecureLinkUrl(String room, int sid) throws RemoteException ;
    public int  sendCSSecureLinkEmail(String room, int sid,String docName,int type,String message,String mailIdstr,String subject,String messsagetype,String expiryDate)throws RemoteException;
    public int updateSecureLink(String room,int sid,String secureId,String status)throws RemoteException;
    public int updateExpiredLink(String room,int sid)throws RemoteException;
    public int getLogedInClientType(String room,int sid)throws RemoteException;
    public Vector getSecureLinkPermissionModify(String room,String documentId,int sid, String userName, String password) throws RemoteException;
    public boolean assertSessionWS(String room, int sid) throws RemoteException;
    public int docDeleteAllPrevComments(String room, int sid, int documentId, String userName) throws RemoteException;
    public int setPagesInfo(String room, int sid, int documentId, String pageInfoVecElem) throws RemoteException;
    public Vector getPagesInfo(String room, int sid, int documentId, Vector sortedPageInfoVec) throws RemoteException;
    public int deletetPagesInfo(String room, int sid, int documentId) throws RemoteException;

    /****CV2019 merges from 10.2 line--------------------------------***/
    public Vector getStatisticsInfo(String room,int sid, String optionKey,Vector result) throws RemoteException;
    public File getRoomAndRouteBasedMessge(String filePath, String roomName, String routeName) throws RemoteException;
    /*--------------------------------------------------------------*/
    
    /****CV2019 merges from SIDBI line--------------------------------***/
    public Vector getSettingsInfo(String room, int sid, String param, Vector result)throws RemoteException;
	public boolean getSidbiCustomizationFlag(String room, int sid)throws RemoteException;
	public boolean getServerSettingsInfo(String room, int sid, String param)throws RemoteException;
	public int getDocumentStatusInWorkflow(String room, int sid, int docID)throws RemoteException;
	/*--------------------------------------------------------------*/

    public int setPagesInformation(String room, int sid, String docId, String oldFileName, String fileName, String pagesInformation) throws RemoteException;
    public Vector getDocumentPagesInformation(String room, int sid, int documentId, Vector<String> sortedPageInfoVec) throws RemoteException;
    public int checkDocsInRoute(String room, int sid, int docId)throws RemoteException;
    
    /****CV2019 merges from SIDBI line--------------------------------***/
    public Vector getCustomUsers(String room, int sid,String param, Vector creators)throws RemoteException;
	public int setRouteOTPInfo(String room,int sid,int recId,int docId,String userName,String OTP,Vector result)throws RemoteException;
	public int sendRouteOTPMail(String room,int sid,int docId,String userName,String docName,String OTP,String loggedUserMailId,String action)throws RemoteException;
	public Vector checkNextTaskExist(String room,int sid,int docId,int routeid,String taskName)throws RemoteException;
	public Vector getBranchInfo(String room, int sid,String flag,String branchCode, Vector result)throws RemoteException ;
	public Vector getWorkflowDoctype(String room, int sid, Vector result)throws RemoteException ;
	public boolean getWARoomCache(int sid) throws RemoteException ;
	public boolean getNativeWrap() throws RemoteException ;
	public int setEncryptionType(String room,int sid,int docId,int encryptionType,Vector result) throws RemoteException ;
	public Vector getEncryptionType(String room,int sid,int docId,Vector result) throws RemoteException ;
	public Vector getRouteTaskByName(String room,int sid,int routeId,int docId,String taskName,Vector result) throws RemoteException ;
	public Vector getCVCurrentFinancialYearPath(String room,int sid,Vector result)throws RemoteException;
    public Vector getHRMSUserInfo(String room,int sid,String userName,Vector result) throws RemoteException ;
    public Vector getCVGetLoginInfo(String room,int sid,String userName,Vector result) throws RemoteException;
    public Vector checkDocViewPerm(String room,int sid,String userName,int docId,Vector result)throws RemoteException;
    
    public String getMsiVersionInfo()throws RemoteException;
	public String getMsiUrlInfo()throws RemoteException;
	public Vector getMsiServerSettingsInfo() throws RemoteException;
	public void setMsiServerSettingsInfo(String msiVersion, String msiLocation, String userName, String password, String domainName)throws RemoteException;
	/*-----End of SIDBI Merges---------------------------------------------------------*/
	/****CV2019 merges from CV10.2 line------------------------------------------------***/
	public DBConnectionBean getDBConnection(int sid, String roomName) throws RemoteException;
	public boolean generateStaticJasperReport(int sid, String roomName, String selectedReport, String outputFormat, String dateFrom, String dateTo, String reportLocation, int staticReportID, int nodeID) throws RemoteException;
	public ArrayList<String> getStaticReportsList(int sid, String roomName) throws RemoteException;
	public Vector<String> checkStaticReport(int sid, String roomName, String selectedValue) throws RemoteException;
	public Vector getStaticReportResult(int sid, String roomName, int reportID, String fromDate, String toDate) throws RemoteException;
	/*-----End of CV0.2 Merges---------------------------------------------------------*/
	public boolean generateDynamicJasperReport(int sid, String roomName, VWDoc vwdoc, String reportLocation,Vector<String> resultSet, String columnDescription,String selectedReport,String JrxmlPath,boolean isPreviewSelected,String outputformat) throws RemoteException;
	public ServerSchema checkStorageLocationForEWA(String roomName, int sid, int nodeID, ServerSchema dssSchema) throws RemoteException;
	public VWDoc getServerJasperContents(int sid, String roomName, String reportLocation) throws RemoteException;
	public void setBatchInputAdministrator(String batchInputAdministrator)throws RemoteException;
	public String getBatchInputAdministrator()throws RemoteException;
	public boolean checkSubAdminUser(String roomName, int sid, String user) throws RemoteException;
	public int getDocumentStorageDetails(int sid, String location) throws RemoteException;
	public RoomProperty getRoomAuthenticationProperties(String room) throws RemoteException;
}