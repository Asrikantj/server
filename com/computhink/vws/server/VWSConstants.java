/*
 * VWSConstants.java
 *
 * Created on 22 ����� ������, 2003, 03:23 �
 */
package com.computhink.vws.server;
import com.computhink.common.*;
import com.computhink.resource.ResourceManager;
/**
 *
 * @author  Administrator
 */
public interface VWSConstants extends Constants
{
    public static final String VWS_PREF_ROOT = "/computhink/" + PRODUCT_NAME.toLowerCase() + "/vws/server";
    public static final String SETTINGS = PRODUCT_NAME + " Server Settings - offline";
    public static final int DEFAULT_DATA_PORT = 2001;
    public static final int DEFAULT_COM_PORT = 2000;
    public static final int DEFAULT_PROXY_PORT = 8080;
	// Issue 562
    public static final int DEFAULT_LOGFILE_COUNT = 100;
    // Issue 562
    
    public static final String NT_LIB = "VWJNT.dll";
    //public static final String UTIL_JAR = "ViewWiseUtil.jar";
    
    public static final String DUMMY_CLASS_1 = "ADSDirectory.class";
    public static final String DUMMY_CLASS_2 = "NDSDirectory.class";
    public static final String DUMMY_CLASS_3 = "SSHDirectory.class";
    
    public static final String PUBLICKEY = "license" + Util.pathSep + 
                                                                "vwsPublic.lic";
    public static final String PRIVATEKEY = "license" + Util.pathSep + 
                                                               "vwsPrivate.lic";
    
    public static final int DEFAULT_LOG_LINES = 100;
    public static final String DEFAULT_LICENSE_KEY = "";
    public static final String ERROR_LOG_FILE = "VWSErrors.log";
    public static final String LOG_FILE_PREFIX = "VWSLog-";
    public static final String LOG_FOLDER = "log";
    public static final int DEFAULT_IDLE_TIME = 15;
    public static final int DTCWC_IDLE_TIME = 45;
    public static final String EMAIL_SUBJECT = " "+ResourceManager.getDefaultManager().getString("Const.EmaiSub");
    public static final String DSS_INACTIVE_MSG=ResourceManager.getDefaultManager().getString("DSSInactive.Mail");
    public static final String ADMIN_EMAIL_ID = "administrator@" + PRODUCT_NAME + ".com";
    public static String NEW_LINE="\n";
	public static String NEW_TAB="\t";
	public static String HTML_NEW_LINE="<BR>";
	public static String HTML_NEW_TAB="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	public static final String MAIL_ROUTE_PENDING = "dwsMail"+ WORKFLOW_MODULE_NAME +"Pending.rtf";
	public static final String MAIL_ROUTE_REJECTED = "dwsMail"+ WORKFLOW_MODULE_NAME +"Rejected.rtf";
	public static final String MAIL_ROUTE_ENDED = "dwsMai"+ WORKFLOW_MODULE_NAME +"Ended.rtf";
	public static final String MAIL_ROUTE_REPORT = "dwsMail"+ WORKFLOW_MODULE_NAME +"Report.rtf";
	public static final String MAIL_EMAIL_REPORT = "dwsMailEmailReport.rtf";
	public static final String MAIL_ROUTE_APPROVED = "dwsMail"+ WORKFLOW_MODULE_NAME +"Approved.rtf";
	public static final String MAIL_RETENTION_MAIL = "arsMail.rtf";
	public static final String MAIL_RETENTION_NOTIFICATION = "arsNotifyEmail.rtf";
	public static final String MAIL_CSS_URL_MAIL= "cssUrlMail.rtf";
	public static final String MAIL_CSS_PASS_MAIL= "cssPasswordMail.rtf";
    public static final String DEBUG_MSG_FILE = "VWSDebugMsg.log";
	public static final String MAIL_NOTIFY_DOCUMENT = "vnsMailDocument.rtf";
	public static final String MAIL_NOTIFY_MODULE = "vnsMailModule.rtf";
	/****CV2019 merges from 10.2 line--------------------------------***/
	public static final String MAIL_WORKFLOW_PENDING= WORKFLOW_MODULE_NAME +"Pending.rtf";
	public static final String MAIL_WORKFLOW_APPROVE= WORKFLOW_MODULE_NAME +"Approved.rtf";
	public static final String MAIL_WORKFLOW_REJECTED= WORKFLOW_MODULE_NAME +"Rejected.rtf";
	public static final String MAIL_WORKFLOW_END = WORKFLOW_MODULE_NAME +"Ended.rtf";
	public static final String MAIL_WORKFLOW_SUMMARY = "EmailReport.rtf";
	/*--------------------------------------------------------------*/
	public static final String CSS_MAIL_Url="url";
	public static final String CSS_MAIL_PASSWORD="password";
	
	public static final int VWS = 1;
	public static final int DRS = 2;
	public static final int ARS = 3;
	public static final int VNS = 4;
	public static final int DSS = 5;
	public static final int CSS = 6;
	public static final int MAIL_SINGLE = 1;
	public static final int MAIL_BUILK = 2;
	
	public static final String DOCUMENT_ALERT = "DOCUMENT_ALERT";
	public static final String FOLDER_ALERT = "FOLDER_ALERT";
	public static final String BACKUP_ALERT = "BACKUP_ALERT";
	public static final String RECYCLE_ALERT = "RECYCLE_ALERT";
	public static final String RETENTION_ALERT = "RETENTION_ALERT";
	public static final String ROUTE_ALERT = WORKFLOW_MODULE_NAME.toUpperCase() +"_ALERT";
	
	public static final String ROUTE_ACCEPT_LABEL = "ACCEPT";
	public static final String ROUTE_REJECT_LABEL = "REJECT";
}
