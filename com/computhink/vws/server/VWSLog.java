/*
 * VWSLog.java
 *
 * Created on September 25, 2003, 12:44 PM
 */

package com.computhink.vws.server;
import com.computhink.common.*;
import com.computhink.ixr.server.IXRPreferences;
/**
 *
 * @author  Saad
 */
import java.util.*;
import java.io.*;
import java.awt.Color;
import javax.swing.text.html.*;
import javax.swing.text.Document;
import javax.swing.text.*;
import javax.swing.text.rtf.RTFEditorKit;

public class VWSLog implements VWSConstants
{
    private static Vector Log = new Vector();
    private static LogMessage oldlog = new LogMessage("", LogMessage.MSG_INF);
    private static LogMessage oldwar = new LogMessage("", LogMessage.MSG_WAR);
    private static LogMessage olderr = new LogMessage("", LogMessage.MSG_ERR);
    private static LogMessage olddbg = new LogMessage("", LogMessage.MSG_DBG);
    private static RandomAccessFile logFile;
 	public  static String sLogPath;
     private static int nFileSize;
    public static void add(String log)
    {
        if (!VWSPreferences.getLogInfo()) return;
        LogMessage lm = createMsg(log, LogMessage.MSG_INF);
        if (lm.equals(oldlog)) return;
        append(lm);
        oldlog = lm;
    }
    public static void war(String war)
    {
        LogMessage lw = createMsg(war, LogMessage.MSG_WAR);
        if (lw.equals(oldwar)) return;
        append(lw);
        oldwar = lw;
    }
    public static void dbg(String dbg)
    {
        if (!VWSPreferences.getDebugInfo()) return;
        LogMessage ld = createMsg(dbg, LogMessage.MSG_DBG);
        if (ld.equals(olddbg)) return;
        append(ld);
        olddbg = ld;
    }
    public static void err(String err)
    {
        LogMessage le = createMsg(err, LogMessage.MSG_ERR);
        if (le.equals(olderr)) return;
        append(le);
        olderr = le;
        writeErrorFile(le.fullMsg);
    }
    /**
     * Enhancment:-Issue fix for directory sync alert message not displayed
     *Method added to creat the log message in servermanger while 
     *sync operationfor license change 
     * @param err
     */
    public static void errMsg(String err){
    	 LogMessage le = createMsg(err, LogMessage.MSG_ERR);
    	 append(le);
    	 writeErrorFile(le.fullMsg);
    }
    private static void append(LogMessage lx)
    {
        Log.add(lx);
        flushWhenFull();
    }
    private static LogMessage createMsg(String msg, int type)
    {
        msg = Util.getNow(2) + " " + msg + "\r\n";
        return new LogMessage(msg, type);
    }
    private static void flushWhenFull()
    {
    	if (VWSPreferences.getLogFlush() && Log.size() >= VWSPreferences.getFlushLines()) {
    		writeLog();
    	}
    }
    public static int getCount() 
    {
        return Log.size();
    }
    public static LogMessage getLog()
    {
        LogMessage log = (LogMessage) Log.get(0);
        Log.remove(0);
        return log;
    }
    public static void writeLog()
    {
         if (Log.size() > 0) {
         	writeLogFile(createDoc(Log));
         }
    }
    private static Document createDoc(Vector messages)
    {
        int size = messages.size();  if (size <= 0) return null;
        DefaultStyledDocument doc = new DefaultStyledDocument();
        SimpleAttributeSet sas = new SimpleAttributeSet();
        for (int i=0; i < size; i++)
        {
            try
            {
                LogMessage log = (LogMessage) messages.elementAt(i);
                StyleConstants.setForeground(sas, log.color);
                doc.insertString(doc.getLength(), log.fullMsg , sas);
            }
            catch(Exception e){}
        }
    //    messages.removeAllElements();
        return doc;
    }
    //Added for VWSDebugMsg.log issue fix in CV83B2
    private static void copyFileUsingFileStreams(File source, File dest)
    		throws IOException {
    	InputStream input = null;
    	OutputStream output = null;
    	try {

    		input = new FileInputStream(source);
    		output = new FileOutputStream(dest);
    		byte[] buf = new byte[1024];
    		int bytesRead;
    		while ((bytesRead = input.read(buf)) > 0) {
    			output.write(buf, 0, bytesRead);
    		}

    	} finally {
    		input.close();
    		source.delete();
    		output.close();
    	}
    }
    //Modified to fix the log writing continuously issue in CV83B2 
    private static void writeDebugFile(String msg)
    {
    	try
    	{
    		if(msg.length()<=0)return;
    		if (logFile == null )
    			sLogPath = assertLogFolder()+DEBUG_MSG_FILE;
    		logFile = new RandomAccessFile(sLogPath, "rwd");
    		nFileSize=49*1024;
    		if (logFile.length() > nFileSize)
    		{
    			logFile.close();
    			File oldFile = new File(sLogPath);
    			int lastindex=  sLogPath.lastIndexOf(".");
    			File newFile = new File(sLogPath.substring(0, lastindex) + "_" + (System.currentTimeMillis()/1000)+"."+sLogPath.substring(lastindex+1));
    			if(oldFile.renameTo(newFile)){
    				logFile = new RandomAccessFile(sLogPath, "rw");
    				nFileSize=0;
    				logFile.setLength(0);
    			}else{
    				copyFileUsingFileStreams(oldFile,newFile);
    				logFile = new RandomAccessFile(sLogPath, "rw");
    				oldFile.delete();
    				nFileSize=0;
    				logFile.setLength(0);
    			}
    		}

    		logFile.seek(logFile.length());
    		String line =msg;
    		logFile.write(line.getBytes());
    		logFile.close();
    	}
    	catch(Exception e){
    		//VWSLog.dbg("Exception while writing the VWSDebugMsg.log file :"+e.getMessage());
    	}
    }
    
    private static void writeErrorFile(String msg)
    {
        try
        {
            File errFile = new File(assertLogFolder() + ERROR_LOG_FILE);
            FileOutputStream fos = new FileOutputStream(errFile, true);
            fos.write(msg.getBytes());
            fos.close();
        }
        catch(Exception e){}
    }
    private static void writeLogFile(Document doc)
    {
        try
        {
        	if(doc.getLength()==0)return;
            File logFile = new File(assertLogFolder() + LOG_FILE_PREFIX + 
                                                       Util.getNow(1) + ".rtf");
            FileOutputStream fos = new FileOutputStream(logFile);
            new RTFEditorKit().write(fos, doc, 0, doc.getLength());
            fos.close();
            Log.clear();
            /*  Issue 562
             * 
             * 
             * */ 
           int maxFileCount =VWSPreferences.getLogFileCount();
           Util.maintainLogFiles(assertLogFolder(),LOG_FILE_PREFIX,maxFileCount);           
           //End of fix 562 
        }
        catch(Exception e){}
		
    }
    private static String assertLogFolder()
    {
	String path = Util.checkPath(VWSUtil.getHome() + Util.pathSep + LOG_FOLDER);
	if (String.valueOf(File.separatorChar).equals("/") && !path.startsWith("/")){
	    path = "/" + path;
	}
	File folder = new File(path);
        try
        {
            if (!folder.exists()) folder.mkdir();
            return folder.getPath() + Util.pathSep;
        }
        catch(Exception e){}
        return "c:" + Util.pathSep;
    }
    public static void dbgCheck(String dbgcheck)
    {
    	if (!VWSPreferences.getDebugCheck()) return;
    	String log= createdbgMsg(dbgcheck);
    	writeDebugFile(log);
    }
    private static String createdbgMsg(String msg)
    {
    	msg = Util.getNow(2) + " " + msg + "\r\n";
    	return msg;
    }
    
}
