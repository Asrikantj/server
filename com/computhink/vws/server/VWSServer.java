/*
 * VWSServer.java
 *
 * 
 */
package com.computhink.vws.server;
import java.io.File;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Random;
import java.util.Vector;
import java.util.Map.Entry;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.computhink.common.Acl;
import com.computhink.common.DBLookup;
import com.computhink.common.DocComment;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.common.EWorkSubmit;
import com.computhink.common.EWorkTemplate;
import com.computhink.common.Index;
import com.computhink.common.LogMessage;
import com.computhink.common.LoginAttempt;
import com.computhink.common.Node;
import com.computhink.common.NodeIndexRules;
import com.computhink.common.NotificationSettings;
import com.computhink.common.Principal;
import com.computhink.common.Room;
import com.computhink.common.RoomProperty;
import com.computhink.common.RouteIndexInfo;
import com.computhink.common.RouteInfo;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.SCMEvent;
import com.computhink.common.SCMEventListener;
import com.computhink.common.SCMEventManager;
import com.computhink.common.Search;
import com.computhink.common.ServerSchema;
import com.computhink.common.Signature;
import com.computhink.common.Util;
import com.computhink.common.VWDoc;
import com.computhink.common.VWRetention;
import com.computhink.common.ViewWiseErrors;
import com.computhink.manager.ManagerPreferences;
import com.computhink.dss.server.DSS;
import com.computhink.vws.directory.Directory;
import com.computhink.vws.license.Base64Coder;
import com.computhink.vws.license.License;
import com.computhink.vws.license.LicenseException;
import com.computhink.vws.license.LicenseManager;
import com.computhink.vws.license.LicenseManagerImpl;
import com.computhink.vws.license.SWLicense;
import com.computhink.vws.server.mail.VWMail;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.DBConnectionBean;
public class VWSServer extends UnicastRemoteObject implements VWS, 
                  ViewWiseErrors, VWSConstants, SCMEventListener//, Unreferenced
{
    private long stime;
    private ServerSchema mySchema;
    private static HashMap servers = new HashMap();
    public static HashMap<String, Vector> selectionIndex = new HashMap<String, Vector>();
    
    private static HashMap rooms = new HashMap();
    private static HashMap hackers = new HashMap();
    
    private static boolean running = true;
    private static ServerSchema  managerSchema = null;
    private static LicenseManager lm = null;
    private static int activeError = 0;
    
    private static int maxLoginAttempt;
    private static long loginLockTime;
    private Random rand = new Random();
    private String licensee;
    private Directory directory;
    private String message = "";
    /**
     * Enhancement :- CV10.1 Ldap multile domain sync.
     * dirList added to store processed domain 
     * Modified BY :- RaviKant
     */
    public static ArrayList<String> dirList = new ArrayList<>();
    //Global array list for manage the Session id across the rooms
    public static ArrayList sidList = new ArrayList();
    public static boolean VNSServiceVerified = false;
    public static boolean CSServiceVerified = false;
    public static String LastRoom = null;
    private static ResourceManager resourceManager=null;
    public VWSServer(ServerSchema s) throws RemoteException 
    {
        super(s.dataport);
        resourceManager=ResourceManager.getDefaultManager();
        mySchema = s;
        SCMEventManager scm = SCMEventManager.getInstance();
        scm.addSCMEventListener(this);
        stime = System.currentTimeMillis();
        setLoginManagement();
        running = true;
       // System.setProperty("java.rmi.server.ignoreStubClasses", "true");
    }
    public void handleSCMEvent(SCMEvent event)
    {
        shutDown();
    }
    public void startWorkers() throws RemoteException 
    {
        if (!checkLicense()) return;
        /* user and group sync is done before starting room thread
        * and details are written in UserGroupList.xml and from there populated 
        * into all room tabels. This will reduce connecting security server if many rooms are there. Valli 14 Feb 2008*/
        if(VWSPreferences.getUseUsrGrpXMLFile()!=null && VWSPreferences.getUseUsrGrpXMLFile().equals(""))
        	VWSPreferences.setUseUsrGrpXMLFile("");
        
        
        /**
         * Enhancement :- CV10.1 Ldap multile domain sync.
         * 
		 * Reading loginServer & ldapDirectory,
		 * Spliting and passing in getInstance Method..
         * @param ldapDirectory
         * @param loginServer
         * @param flag
         * @return
         * Modified BY :- RaviKant
         */
        
        String loginServer = "";
		String ldapDirectory = "";
		ldapDirectory = VWSPreferences.getTree();
		String type = VWSPreferences.getSSType();
		if (type.equalsIgnoreCase(SSCHEME_NDS)) {
			loginServer = VWSPreferences.getLoginContext();
		} else if (type.equalsIgnoreCase(SCHEME_LDAP_ADS) || type.equalsIgnoreCase(SCHEME_LDAP_LINUX)
				|| type.equalsIgnoreCase(SCHEME_LDAP_NOVELL)) {
			loginServer = VWSPreferences.getSSLoginHost();
		} else {
			loginServer = VWSPreferences.getSSLoginHost();
		}
		
		
		String[] dirList = ldapDirectory.split(",");
		String[] serList = loginServer.split(",");
		for(int k=0; k< serList.length; k++){
	        synchronized(this)
	        {
	        	if (directory != null) {
					VWSLog.add("Inside VWSServer start action before new DirectorySync... and directory object not null condition satisfied...");
					directory = null;
				}
	        	directory = Directory.getInstance(dirList[0],serList[0],true);
	        	//directory = Directory.getInstance();//Commented for CV10.1 multidomain implementation
	            new DirectorySync(directory);
	        }
		}
        //End of cv10.1 Multipdomain changes
        RoomProperty[] rps = VWSPreferences.getRooms();
        if (rps.length == 0)
        {
        	message = resourceManager.getString("VWSMSG.NoRegRooms");
        	VWSLog.war(message);
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
            return;
        }
        for (int i=0; i < rps.length; i++)
        {
			/**CV2019 merges from 10.2 line***/
        	if (i == (rps.length)-1) {
        		LastRoom = rps[i].getName();
        	}
            startWorker(rps[i]);
        }
    }
    private boolean checkLicense()
    {
        lm =  LicenseManagerImpl.getInstance();
        if (lm == null)
        {
        	message = resourceManager.getString("VWSMSG.InvalidLicense");
        	VWSLog.war(message);
			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
            return false;
        }
        try
        {
            if (!lm.hasValidLicense())
            {
            	message = resourceManager.getString("VWSMSG.LicenseExpired");
            	VWSLog.war(message);
    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
                return false;
            }
            License lc = lm.getLicense();
            if (lc != null)
            {
                licensee = lc.getCompanyName();
                VWSLog.add("Licensed for: " + licensee);
                return true;
            }
            else
            {
            	message = resourceManager.getString("VWSMSG.InvalidLicense");
            	VWSLog.war(message);
    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
                return true;
            }
         }
        catch(LicenseException le)
        {
            VWSLog.war(le.getMessage());
        }
        return false;
    }
    private void startWorker(RoomProperty rp)
    {
        if (!running) return;
        Room room = new Room(rp);
        room.setVWS(mySchema);
        if (rooms.containsKey(room.getName())) return;
        new RoomWorker(room, rand.nextInt(10000)).start();
        /*Code to disable ARS/DRS when ars/drs license is not there.
         * License lic = null;
        try{
        	lic = getLicense();
        	if(lic.getDrsSeats()<=0){
        		VWSLog.add("There is no DRS license. DRS will be disabled");
        		ManagerPreferences.setDRSEnable("false");
        	}
        	if(lic.getArsSeats()<=0){
        		VWSLog.add("There is no ARS license. ARS will be disabled");
        		ManagerPreferences.setARSEnable("false");
        	}
        }catch(Exception ex){
        	VWSLog.err("Error in getting license details "+ex.getMessage());
        }*/
    }
    public void closeRoom(String room)throws RemoteException{
    	message = resourceManager.getString("VWSMSG.Room")+" "+ room +" "+ resourceManager.getString("VWSMSG.DBNotAvailable");
    	VWSLog.err("Room " + room + " is Stopped. Database Service is not available");
    	removeHashMapRoom(room);
		VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    	getServer(room).logoutClients();
        RoomWorker rm = (RoomWorker) servers.get(room + "T");
        try{rm.join();} catch(Exception e){}
        unRegisterRoom(room);
        unRegisterServer(room);
        unRegisterServerThread(room);
        VWSLog.add("Stopped "+ PRODUCT_NAME +" Worker for Room " + room);
    }
    public void stopWorker(String room)
    {
    	removeHashMapRoom(room);
    	getServer(room).logoutAllClient();
        RoomWorker rm = (RoomWorker) servers.get(room + "T");
        try{rm.join();} catch(Exception e){}
        unRegisterRoom(room);
        unRegisterServer(room);
        unRegisterServerThread(room);
        VWSLog.add("Stopped " + PRODUCT_NAME + " Worker for Room " + room);
    }
    public void shutDown()
    {
        VWSLog.add("Shut Down requested...");
        String roomName = null;
        Vector vr = new Vector(rooms.keySet());
        for (int i = 0; i < vr.size(); i++)
        {
            roomName = (String) vr.elementAt(i);
            stopWorker(roomName);
        }
        VWSLog.add("Writing Server Log");
        VWSLog.writeLog();
        VWSLog.add("Stopping " + PRODUCT_NAME + " Server...");
        Util.sleep(500);
        Util.killServer(mySchema);
    }
    public void removeHashMapRoom(String roomName){
    	String key = roomName+"_";
    	if(this.selectionIndex!=null && this.selectionIndex.size()>0){
	    	Iterator<Entry<String, Vector>> iter = this.selectionIndex.entrySet().iterator();
	    	int i = 1; 
	    	while (iter.hasNext()) {
	    	    Entry<String,Vector> entry = iter.next();
	    	    if(entry.getKey().startsWith(key)){
	    	    	
	    	    	VWSLog.add(i+" - Removed key : "+entry.getKey());
	    	    	i++;
	    	        iter.remove();
	    	    }
	    	}
	 	}
    }
    private void registerRoom(Room room)
    {
        rooms.put(room.getName(), room);
    }
    private void unRegisterRoom(String room)
    {
        rooms.remove(room);
    }
    private void registerServer(RampageServer server)
    {
        String roomName = server.getRoomName();
        servers.put(roomName.toUpperCase(), server);
    }
    private void unRegisterServer(String room)
    {
        servers.remove(room.toUpperCase());
    }
    private void registerServerThread(RoomWorker rm)
    {
        String roomName = rm.getServer().getRoomName();
        servers.put(roomName.toUpperCase() + "T", rm);
    }
    private void unRegisterServerThread(String room)
    {
        servers.remove(room.toUpperCase() + "T");
    }
    private RampageServer getServer(String room)
    {
        return (RampageServer) servers.get(room.toUpperCase());
    }
    private void addHacker(LoginAttempt la)
    {
        long counter = 1L;
        String ip = la.getIp();
        if (hackers.containsKey(ip))
        {
            LoginAttempt lax = (LoginAttempt) hackers.get(ip);
            counter = lax.getDate(); 
            if (counter < (long) maxLoginAttempt - 1)
                counter++;
            else
                counter = System.currentTimeMillis();
            hackers.remove(ip);
        }
        la.setDate(counter);
        hackers.put(ip, la);
    }
    private int isHacker(String ip)
    {
        if (!hackers.containsKey(ip)) return 0;
        LoginAttempt la = (LoginAttempt) hackers.get(ip);
        long counter = la.getDate();
        if (counter <= (long) maxLoginAttempt - 1) return 0; 
        long now = System.currentTimeMillis();
        if ((now - counter) <= (loginLockTime * 60L * 1000L) ) 
            return 1;
        else
        {
            hackers.remove(ip);
            return 0;
        }
    }
    private void setLoginManagement()
    {
        maxLoginAttempt = VWSPreferences.getMaxLoginAttempts();
        loginLockTime = VWSPreferences.getLoginLockTime();
        readjustHackers();
    }
    private void readjustHackers()
    {
        Vector vh = new Vector(hackers.values());
        long now = System.currentTimeMillis();
        for (int i = 0; i < vh.size(); i++)
        {
            LoginAttempt la = (LoginAttempt) vh.get(i);
            if ((now - la.getDate()) >= (loginLockTime * 60L * 1000L))
                hackers.remove(la.getIp());
        }
    }
    //---------------------------public methods---------------------------------
    public boolean ping() throws RemoteException
    {
        return true;
    }
    public void exit() throws RemoteException
    {
        shutDown();
    }
    public String getServerOS() throws RemoteException
    {
        return Util.getEnvOSName();
    }
    public void applyNewSettings(int setting) throws RemoteException
    {
        switch (setting)
        {
            case 1:
                setLoginManagement();
                break;
        }
    }
    public Vector getLockedUsers() throws RemoteException
    {
        return new Vector(hackers.values());
    }
    public void unlockUser(String ip) throws RemoteException
    {
        if (hackers.containsKey(ip)) hackers.remove(ip);
        //CV10 Enhancement issue fix:-Condition newly added to fix the refresh issue in login management.
        if(hackers.containsKey(ip)){
            LoginAttempt lax = (LoginAttempt) hackers.get(ip);
            lax.now="";
            lax.releaseDtTime="";
        }
    }
    public License getLicense() throws RemoteException
    {
        try
        {
            return lm.getLicense();
        }
        catch(LicenseException le)
        {
            return null;
        }
    }
    public Vector getRoomNames() throws RemoteException
    {
    	return new Vector(rooms.keySet());
    }
    public boolean pingDB(String room) throws RemoteException{
    	   return getServer(room).pingDB();
    }
/*
Issue No / Purpose:  <650/Terminal Services Client>
Created by: <Pandiya Raj.M>
Date: <19 Jul 2006>
Pass the stubIPAddress" to the login method.
*/    
    public int login(String room, int type, String ip, String user, String pass, 
            int port) throws RemoteException{
    	return login(room,type,ip,user,pass,port,"");
    }
	public int login(String room, int type, String ip, String user, String pass, int port, String stubIPAddress)
			throws RemoteException {
		/*CV2019 merges from SIDBI line------------------------------------------------***/
		String actualUser = user;
		String actualPass = pass;
		if (isHacker(ip) == 1)
			return LoginCountExceeded;
		try {
			if (type == Client.Web_Client_Type || type == Client.WebAccess_Client_Type
					|| type == Client.Custom_Label_WebAccess_Client_Type
					|| type == Client.Professional_WebAccess_Client_Type
					|| type == Client.Concurrent_WebAccess_Client_Type
					|| type == Client.Professional_Conc_WebAccess_Client_Type 
					|| type == Client.Fat_Client_Type || type == Client.AIP_Client_Type || type == Client.Adm_Client_Type 
					|| type == Client.sdk_Client_Type || type == Client.IXR_Client_Type) {
				try {
					user = Base64Coder.decode(user);
					if (pass != null && pass.trim().length() > 0) {
						pass = Base64Coder.decode(pass);
					}
				} catch (Exception e) {
					VWSLog.dbg("EXCEPTION inside decode  :" + e.getMessage());
					/*user = Util.decryptKey(user);
		    		if (pass != null && pass.trim().length() > 0)
		    			pass = Util.decryptKey(pass);*/
					
				}
			} else {
				user = Util.decryptKey(user);
	    		if (pass != null && pass.trim().length() > 0)
	    			pass = Util.decryptKey(pass);
			}
		} catch (Exception e) {
			VWSLog.dbg("EXCEPTION inside decode :::: " + e.getMessage());
		}
		/*------------------------End of SIDBI merges--------------------------------------*/

		/* } */
		// End
		// Pass the stubIPAddress to the login method. to resolve the issue #
		// 650.
		int sid = getServer(room).login(type, ip, user, pass, port, stubIPAddress);
		VWSLog.dbg("Sid :" + sid);
		/*CV2019 merges from SIDBI line------------------------------------------------***/
		if (sid == -45) {
			try {				 
				 actualUser = Util.decryptKey(actualUser);
				 actualPass = Util.decryptKey(actualPass);				 
				 sid = getServer(room).login(type, ip, actualUser, actualPass, port, stubIPAddress);
				 if (sid > 0)
					 user = actualUser;
			} catch (Exception e) {
				VWSLog.dbg("EXCEPTION inside second time login :::: " + e.getMessage());
			}
		}
		VWSLog.dbg("login sid :" + sid);
		/*------------------------End of SIDBI merges--------------------------------------*/
		if (sid < 0) {
			VWSLog.dbg("Before adding to hackers list : :" + user);
			if (user.contains("~cs~")) {
				String[] urserlist = user.split("~cs~");
				String[] userWithDocIdPass = urserlist[1].split(Util.SepChar);
				user = userWithDocIdPass[1];

			}
			LoginAttempt la = new LoginAttempt(room, type, ip, user);
			addHacker(la);
		} else if (sid > 0) {
			getServer(room).setNLSSORT_NLSCOMP();
		}
		return sid;
	}
    public int authenticateUser(String room, int sid, String user, String pass)throws RemoteException{
    	return getServer(room).authenticateUser(sid, user, pass);
    }
    public int isHacker(String room, String ip) throws RemoteException
    {
        return isHacker(ip);
    }
    public int logout(String room, int sid) throws RemoteException
    {
        return getServer(room).logout(sid);
    }
    public Vector getConnectedClients(String room, int sid, Vector users) 
                                                          throws RemoteException
    {
        activeError = getServer(room).getConnectedClients(sid, users);
        VWSLog.dbgCheck("activeError inside getConnectedClients::::: "+ activeError);
        return users;
    }
    public Vector getPrincipal(String room, int sid, Principal princ, 
                                           Vector result) throws RemoteException
    {
        activeError = getServer(room).getPrincipal(sid, princ, result);
        VWSLog.dbgCheck("activeError inside getPrincipal::::: "+ activeError);
        return result;
    }
    public int isConnectionEnabled(String room, int sid) throws RemoteException 
    {
        return getServer(room).isConnectionEnabled(sid); 
    }
    public int setAllowConnection(String room,int sid,boolean allow) 
                                                          throws RemoteException 
    {
        return getServer(room).setAllowConnection(sid, allow); 
    }
    public int logoutClient(String room, int sid, int clientsid) 
                                                          throws RemoteException
    {
        return getServer(room).logoutClient(sid, clientsid);
    }
    public Vector getDocType(String room, int sid, int did, Vector type)
                                                          throws RemoteException
    {
        activeError = getServer(room).getDocType(sid, did, type); 
        VWSLog.dbgCheck("activeError inside getDocType::::: "+ activeError);
        return type;
    }
    public Vector getDocTypes(String room, int sid, Vector types)
                                                          throws RemoteException
    { 
        activeError = getServer(room).getDocTypes(sid, types); 
        VWSLog.dbgCheck("activeError inside getDocTypes::::: "+ activeError);
        return types;
    }
    public Vector getCreators(String room, int sid, Vector creators)
                                                          throws RemoteException
    { 
        activeError = getServer(room).getUsers(sid, creators); 
        VWSLog.dbgCheck("activeError inside getCreators::::: "+ activeError);
        return creators;
    }
    public Node getNodeProperties(String room, int sid, Node node) 
                                                          throws RemoteException
    {
        activeError = getServer(room).getNodeProperties(sid, node);
        VWSLog.dbgCheck("activeError inside getNodeProperties::::: "+ activeError);
        return node;
    }
    public Vector getNodePath(String room, int sid, int nodeId) throws RemoteException
    {
    	Vector result = new Vector();
    	activeError = getServer(room).getNodePath(sid, nodeId, result);   
        VWSLog.dbgCheck("activeError inside getNodePath::::: "+ activeError);
    	return result;    	
    }
    // Fixing Issue 735, add the sort argument to retrieve the data based on sort value. Sort will be done based on 'sort' value
    public Vector getNodeContents(String room, int sid, int nid, int coid,
                                            Vector nodes) throws RemoteException
    {
    	activeError = getServer(room).getNodeContents(sid, nid, coid, nodes, 0);
    	   VWSLog.dbgCheck("activeError inside getNodeContents::::: "+ activeError);
    	return nodes;
   }

    public Vector getNodeContents(String room, int sid, int nid, int coid,
                                            Vector nodes, int sort) throws RemoteException
    {
        activeError = getServer(room).getNodeContents(sid, nid, coid, nodes, sort);
        VWSLog.dbgCheck("activeError inside getNodeContents::::: "+ activeError);
        return nodes;
    }
/* Issue Description - AIP Hangs when processing large rooms
*  Developer - Nebu Alex
*  Code Description - This function makes a database call and
*					- returns node information, 
* Arguments required - Nodes parentid and nodes name
* 
*/
 	public Vector getNodeDetails(String room, int sid, String parentid, String nodeName,Vector nodes)throws RemoteException {
    	activeError = getServer(room).getNodeDetails(sid, parentid, nodeName, nodes);
    	  VWSLog.dbgCheck("activeError inside getNodeDetails::::: "+ activeError);
    	return nodes;
	}
// End of code change
    public Vector getNodeParents(String room, int sid,int nid, Vector nodes) 
                                                          throws RemoteException
    {
        activeError = getServer(room).getNodeParents(sid, nid, nodes);
        VWSLog.dbgCheck("activeError inside getNodeParents::::: "+ activeError);
        return nodes;
    }
    public int createNode(String room, int sid, Node node, int withUniqueName) 
                                                          throws RemoteException
    {
        return getServer(room).createNode(sid, node, withUniqueName);
    }
    public int delNode(String room, int sid, int nid, boolean toRecycle) 
                                                          throws RemoteException
    {
        return getServer(room).delNode(sid, nid, toRecycle);
    }
    public int azureARSDeleteNode(String room, int sid, int nid, boolean toRecycle) throws RemoteException{
    	return getServer(room).azureARSDeleteNode(sid, nid, toRecycle);
    }

    public Vector getDocsToPurge(String room, int sid,  int docsCountToPurge, Vector docsToPurge)throws RemoteException{
    	activeError = getServer(room).getDocsToPurge(sid, docsCountToPurge, docsToPurge);
        VWSLog.dbgCheck("activeError inside getDocsToPurge::::: "+ activeError);
    	return docsToPurge;
    }
    public int renameNode(String room, int sid, Node node) 
                                                          throws RemoteException
    {
        return getServer(room).renameNode(sid, node);
    }
    public int copyNode(String room, int sid, Node node, int nodeProperties) throws RemoteException
    {
        return getServer(room).copyNode(sid, node, nodeProperties);
    }
    
    public Vector azureCopyNode(String room, int sid, Node node, int nodeProperties) throws RemoteException
    {
    	Vector totalPath=new Vector();
    	activeError = getServer(room).azureCopyNode(sid, node, nodeProperties,totalPath);
        VWSLog.dbgCheck("activeError inside getDocsToPurge::::: "+ activeError);
        return totalPath;
    }
    public int moveNode(String room, int sid, Node node) throws RemoteException
    {
        return getServer(room).moveNode(sid,node, 0);
    }
    
    public int moveNode(String room, int sid, Node node, int nodeProperties) throws RemoteException
    {
        return getServer(room).moveNode(sid,node, nodeProperties);
    }
    public Vector getNodeDocuments(String room, int sid, int nid, int coid, int refNid, 
                                             Vector docs) throws RemoteException
    {
        activeError = getServer(room).getNodeDocuments(sid, nid, coid, refNid, docs);
        VWSLog.dbgCheck("activeError inside getNodeDocuments::::: "+ activeError);
        return docs;
    }
    public Vector getAIPNodeDocuments(String room, int sid, int nid, int coid, int docTypeId, String curIndexVal, Vector docs) throws RemoteException{
    	activeError = getServer(room).getAIPNodeDocuments(sid, nid, coid, docTypeId, curIndexVal, docs);
    	 VWSLog.dbgCheck("activeError inside getAIPNodeDocuments::::: "+ activeError);
    	return docs;
    }
    public Vector getAllNodeDocuments (String room, int sid, int nid, String ipAddress, Vector docs)
    throws RemoteException
    {
    	activeError = getServer(room).getAllNodeDocuments (sid, nid, ipAddress, docs);
    	VWSLog.dbgCheck("activeError inside getAllNodeDocuments::::: "+ activeError);
    	return docs;
    }
    
    public Vector getDocumentByInfo(String room, int sid, String nodePath, 
             String docTypeName,String indexName,String indexValue,int indexKey) 
                                                          throws RemoteException
    {
        Vector docs = new Vector();
        activeError = getServer(room).getDocumentByInfo(sid, nodePath, 
                              docTypeName, indexName,indexValue,indexKey, docs);
    	VWSLog.dbgCheck("activeError inside getDocumentByInfo::::: "+ activeError);
        return docs;
    }
    
    public Document createDocument(String room, int sid, Document doc) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).createDocument(sid , doc);
    	VWSLog.dbgCheck("activeError inside createDocument::::: "+ activeError);
        return doc;
    }
	/**CV2019 - SummaryInfo enhancement***/
    public Vector getDocumentProperties(String room, int sid, int did, Vector indices)  throws RemoteException {
    	activeError = getServer(room).getDocumentProperties(sid, did, indices);
    	VWSLog.dbgCheck("activeError inside getDocumentIndicesForSummaryInfo::::: "+ activeError);
        return indices;
    }
    public Vector getDocumentIndices(String room, int sid, int did, String ver, 
                                         Vector indices)  throws RemoteException 
    {
        activeError = getServer(room).getDocumentIndices(sid, did, ver, indices);
    	VWSLog.dbgCheck("activeError inside getDocumentIndices::::: "+ activeError);
        return indices;
    }
    public int setDocumentIndices(String room, int sid, Document doc)
                                                          throws RemoteException
    {
        return getServer(room).setDocumentIndices(sid, doc);
    }
    public int updateDocumentDocType(String room, int sid, Document doc)
    throws RemoteException
    {
    	return getServer(room).updateDocumentDocType(sid, doc);
    }
    public Vector updateAzureDocumentDocType(String room, int sid, Document doc)
    throws RemoteException
    {
    	Vector result=new Vector();
    	 activeError = getServer(room).updateAzureDocumentDocType(sid, doc,result);
    	return result;
    }

    public int setDocActiveVer(String room, int sid, int did, String ver)
                                                          throws RemoteException
    {
        return getServer(room).setDocActiveVer(sid, did, ver);
    }
    
    public Vector setDocActiveVerAzure(String room, int sid, int did, String ver)throws RemoteException
    {
    	Vector result=new Vector();
    	   activeError =getServer(room).setDocActiveVerAzure(sid, did, ver,result);
    	return result;
    }
    public Vector getDocTypeIndices(String room, int sid, int dtid, 
                                          Vector indices) throws RemoteException 
    {
        activeError = getServer(room).getDocTypeIndices(sid, dtid, indices);
    	VWSLog.dbgCheck("activeError inside getDocTypeIndices::::: "+ activeError);
        return indices;
    }
    public Vector populateSelectionIndex(String room, int sid, int iid,
                                           Vector values) throws RemoteException 
    {
        activeError = getServer(room).populateSelectionIndex(sid, iid, values);
    	VWSLog.dbgCheck("activeError inside populateSelectionIndex::::: "+ activeError);
        return values;
    }
    public int violatesUnique(String room, int sid, int nodeId, int dtid, int iid, 
                                            String value) throws RemoteException
    {
        return getServer(room).violatesUnique(sid, nodeId, dtid, iid, value);
    }
    public Vector getDocumentComment(String room, int sid, int did,String docVersion, 
                                         Vector comments) throws RemoteException
    {
        activeError = getServer(room).getDocumentComment(sid, did,docVersion,comments);
      	VWSLog.dbgCheck("activeError inside getDocumentComment::::: "+ activeError);
        return comments;
    }
    public int deleteDocumentComment(String room, int sid,int did)
                                                          throws RemoteException
    {
        return getServer(room).deleteDocumentComment(sid, did);
    }    
    public int setDocVerComment(String room,int sid,int did, String ver, 
                                          String comment) throws RemoteException 
    {
        return getServer(room).setDocVerComment(sid, did, ver, comment);
    }
    public int deleteDocumentVers(String room, int sid, int did)  
                                                          throws RemoteException 
    {
        return getServer(room).deleteDocumentVers(sid, did);
    }    
    public Vector getDocumentVers(String room, int sid, int did, Vector vers)
                                                          throws RemoteException
    {
        activeError = getServer(room).getDocumentVers(sid, did, vers);
    	VWSLog.dbgCheck("activeError inside  getDocumentVers::::: "+ activeError);
        return vers;
    }
    public Vector getRedactions(String room, int sid, int did, String ver, 
                                       Vector redactions) throws RemoteException
    {
        activeError = getServer(room).getRedactions(sid, did, ver, redactions);
     	VWSLog.dbgCheck("activeError inside  getRedactions::::: "+ activeError);
        return redactions;
    }
    public int setRedactions(String room, int sid, int did , String redactions) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).setRedactions(sid, did, redactions);
        VWSLog.dbgCheck("activeError inside  setRedactions::::: "+ activeError);
        return NoError;
    }
    public int lockDocument(String room, int sid, int did) 
                                                          throws RemoteException
    {
         return getServer(room).lockDocument(sid, did);
    }
    public int updateLockedDocument(String room, int sid, int did) throws RemoteException
    {
    	return getServer(room).updateLockedDocument(sid, did);
	}

    public int unlockDocument(String room, int sid, int did, int force) 
                                                          throws RemoteException
    {
         return getServer(room).unlockDocument(sid, did, force);
    }
    public Vector getLockOwner(String room, int sid, int did, Vector name, 
            boolean withMe)  throws RemoteException
    {
         activeError = getServer(room).getLockOwner(sid, did, name, withMe);
         VWSLog.dbgCheck("activeError inside  getLockOwner::::: "+ activeError);
         return name;
    }
	// Issue No :567
	public Vector getSessionOwner(String room, int sid, int did, Vector name,
			boolean withMe) throws RemoteException {
		activeError = getServer(room).getSessionOwner(sid, did, name, withMe);
	      VWSLog.dbgCheck("activeError inside  getSessionOwner::::: "+ activeError);
		return name;
	}
	// End of issue no: 567
    public Vector setDocumentComment(String room, int sid, DocComment dc,
                boolean withCheck,int docRestoreFlag) throws RemoteException
    {
        Vector ret =new Vector();
        activeError = getServer(room).setDocumentComment(sid, dc, withCheck,docRestoreFlag, ret);
        VWSLog.dbgCheck("activeError inside  setDocumentComment::::: "+ activeError);
        return ret;
    }
    public void setPageCount(String room, int sid, int docId, int pCount) throws RemoteException{
	getServer(room).setPageCount(sid, docId, pCount);
    }
    public Index getNextSerial(String room, int sid, Index index)
                                                          throws RemoteException
    {
        activeError = getServer(room).getNextSerial(sid, index);
        VWSLog.dbgCheck("activeError inside  getNextSerial::::: "+ activeError);
        return index;
    }
    public Vector getNextSequence(String room, int sid, int indexId, int docTypeId,int nodeId) throws RemoteException
    {
	Vector ret = new Vector();
	activeError = getServer(room).getNextSequence(sid, indexId, docTypeId,nodeId, ret);	
	   VWSLog.dbgCheck("activeError inside  getNextSequence::::: "+ activeError);
	return ret;
    }    
    public int addRef(String room, int sid, int nid, int rid, boolean bi) 
                                                          throws RemoteException 
    {
        return getServer(room).addRef(sid, nid, rid, bi);
    }
    public int delRef(String room, int sid, int nid, int rid) 
                                                          throws RemoteException 
    {
        return getServer(room).delRef(sid, nid, rid); 
    }
    public Vector getRefs(String room, int sid, int nid, Vector refs) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).getRefs(sid, nid, refs); 
 	   VWSLog.dbgCheck("activeError inside  getRefs::::: "+ activeError);
        return refs;
    }
    public int addShortcut(String room, int sid, int nid, String name, 
                                   String user, int type) throws RemoteException 
    {
        return getServer(room).addShortcut(sid, nid, name, user, type); 
    }
    public int delShortcut(String room, int sid, int scid) 
                                                          throws RemoteException 
    {
        return getServer(room).delShortcut(sid ,scid); 
    }
    public Vector getShortcuts(String room, int sid, Vector scs) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).getShortcuts(sid, scs); 
 	   VWSLog.dbgCheck("activeError inside  getShortcuts::::: "+ activeError);
        return scs;
    }
    public int updateShortcut(String room, int sid, int scid, 
                                    String name,int type) throws RemoteException 
    {
        return getServer(room).updateShortcut(sid, scid, name,type);
    }
    public int createSearch(String room, int sid, Search search) 
                                                          throws RemoteException 
    {
        return getServer(room).createSearch(sid, search);
    }
    public Search getSearch(String room, int sid,Search search) 
                                                          throws RemoteException  
    {
       activeError = getServer(room).getSearch(sid,search);
       VWSLog.dbgCheck("activeError inside  getSearch::::: "+ activeError);
       return search;
    }
    public Vector getSearchs(String room, int sid, Vector searchs) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).getSearchs(sid, searchs);
        VWSLog.dbgCheck("activeError inside  getSearchs::::: "+ activeError);
        return searchs;
    }
    public Vector getSearchPages(String room, int sid, int did, String contents,
                                            Vector pages) throws RemoteException
    {
        activeError = getServer(room).getSearchPages(sid, did, contents, pages);
        VWSLog.dbgCheck("activeError inside  getSearchPages::::: "+ activeError);
        return pages;
    }
    public int delSearch(String room, int sid, int srid) throws RemoteException 
    {
        return getServer(room).delSearch(sid, srid);
    }
    public Vector getSearchAuthors(String room, int sid, int srid, 
                                          Vector authors) throws RemoteException 
    {
        activeError = getServer(room).getSearchAuthors(sid, srid, authors);
        VWSLog.dbgCheck("activeError inside  getSearchAuthors::::: "+ activeError);
        return authors;
    }
    public Vector getSearchDocTypes(String room, int sid, int srid, 
                                              Vector dts) throws RemoteException 
    {
        activeError = getServer(room).getSearchDocTypes(sid, srid, dts);
        VWSLog.dbgCheck("activeError inside  getSearchDocTypes::::: "+ activeError);
        return dts;
    }
    public Vector getSearchConds(String room, int sid, int srid, 
                                            Vector conds) throws RemoteException 
    {
        activeError = getServer(room).getSearchConds(sid, srid, conds);
        VWSLog.dbgCheck("activeError inside  getSearchConds::::: "+ activeError);
        return conds;
    }
    public int find(String room, int sid, int srid, int hlid, int rowNo, boolean docsOnly) 
                                                          throws RemoteException 
    {
       return getServer(room).find(sid, srid, hlid, rowNo, docsOnly);
    }
    //find signature changed for enh 1157 - audit catch for search criteria
    public int find(String room, int sid, Search search, boolean docsOnly)    throws RemoteException 
	{
    	return getServer(room).find(sid, search, docsOnly);
	}
    public Vector getFindResult(String room, int sid, Search srch, Vector docs) 
                                                          throws RemoteException 
    {
       activeError = getServer(room).getFindResult(sid, srch, docs);
       VWSLog.dbgCheck("activeError inside  getFindResult::::: "+ activeError);
       return docs;
    }
    public Vector getIndexValues(String room, int sid, int dtid, int iid,  
                                           Vector values) throws RemoteException 
    {
       activeError = getServer(room).getIndexValues(sid, dtid, iid, values);
       VWSLog.dbgCheck("activeError inside  getIndexValues::::: "+ activeError);
       return values;
    }
    public Vector adminWise(String room, int sid, String action, String param,
                                           Vector result) throws RemoteException
    {
        activeError = getServer(room).adminWise(sid, action, param, result);
        VWSLog.dbgCheck("activeError inside  adminWise::::: "+ activeError);
        return result;
    }
    public int addATRecord(String room, int sid, int nid, int otype, int eid, 
                                             String desc) throws RemoteException 
    {
        return getServer(room).addATRecord(sid, nid, otype, eid, desc);
    }
    public int registerOutput(String room, int sid,String output)
                                                          throws RemoteException 
    {
        return getServer(room).registerOutput(sid, output);
    }
    public int isVWSignInstalled() throws RemoteException 
    {
        lm =  LicenseManagerImpl.getInstance();
        if (lm == null)
        {
            return 0;
        }
        try
        {
            SWLicense lc = lm.getSignWiseLicense();
            if (lc != null && licensee.equals(lc.getCompanyName()))
            {
                return 1;
            }
            else
            {
                return 0;
            }
         }
        catch(LicenseException le)
        {
            //VWSLog.war(le.getMessage());
        }
        return 0;
    }
    //------------------------------Security Methods----------------------------
    public boolean isAdmin(String room,int sid) throws RemoteException 
    {
            return getServer(room).isAdmin(sid);
    }
    public boolean isSecurityAdmin(String room,int sid) throws RemoteException 
    {
            return getServer(room).isSecurityAdmin(sid);
    }
    
    public boolean enableSecurityAdmin(String room,int sid) throws RemoteException 
    {
            return getServer(room).enableSecurityAdmin(sid);
    }
    
    public boolean isManager(String room, int sid) throws RemoteException 
    {
            return getServer(room).isManager(sid);
    }
    public Vector getGroups(String room, int sid, Vector groups)
                                                          throws RemoteException
    { 
        getServer(room).getGroups(sid, groups); 
        return groups;
    }
    public Vector getPrincipals(String room, int sid, Vector princ)
                                                          throws RemoteException
    { 
        activeError = getServer(room).getPrincipals(sid, princ); 
        VWSLog.dbgCheck("activeError inside  getPrincipals::::: "+ activeError);
        return princ;
    }
	/**CV2019 merges from SIDBI line***/
    public Vector getPrincipalsSecurity(String room, int sid, Vector princ,String startWith,int mode)
            throws RemoteException
		{ 
		activeError = getServer(room).getPrincipalsSecurity(sid, princ,startWith,mode); 
		VWSLog.dbgCheck("activeError inside  getPrincipals::::: "+ activeError);
		return princ;
	}
	/**CV2019 merges from 10.2 line***/
    public Vector getPrincipalsWithNoSign(String room, int sid, Vector princ)
    		throws RemoteException
    { 
    	activeError = getServer(room).getPrincipals(sid, princ); 
    	VWSLog.dbgCheck("activeError inside  getPrincipals::::: "+ activeError);
    	return princ;
    }
    public Vector getPrincipalMembers(String room, int sid, String pid, 
                                            Vector princ) throws RemoteException
    {
        activeError = getServer(room).getPrincipalMembers(sid, pid, princ);
        VWSLog.dbgCheck("activeError inside  getPrincipalMembers::::: "+ activeError);
        return princ;
    }
    public int removePrincipal(String room, int sid, Principal princ) 
                                                          throws RemoteException
    {
        activeError = getServer(room).removePrincipal(sid, princ);
        VWSLog.dbgCheck("activeError inside  removePrincipal::::: "+ activeError);
        return NoError;
    }
	// Added for DRS 30 Jan 2007
    public int updatePrincipal(String room, int sid, Principal princ) throws RemoteException
    {
    	activeError = getServer(room).updatePrincipal(sid, princ);
    	 VWSLog.dbgCheck("activeError inside  updatePrincipal::::: "+ activeError);
    	return NoError;
    }
    public int addPrincipal(String room, int sid, Principal princ) 
                                                          throws RemoteException
    {
        activeError = getServer(room).addPrincipal(sid, princ);
        VWSLog.dbgCheck("activeError inside  addPrincipal::::: "+ activeError);
        return NoError;
    }
	/**CV2019 merges from 10.2 line***/
    public int syncPrincipals(String room, int sid,Principal principal)throws RemoteException
		{
    	VWSLog.add("SyncPrincipals called when click on Administration sync for selected users....");
		activeError = getServer(room).syncLDAPPrincipals(sid,principal);
		return NoError;
		}
    public int syncPrincipals(String room, int sid) 
                                                          throws RemoteException
    {
        activeError = getServer(room).syncPrincipals(sid);
        VWSLog.dbgCheck("activeError inside  syncPrincipals::::: "+ activeError);
        return NoError;
    }
    public int setNodeAcl(String room, int sid, Acl acl) throws RemoteException 
    {
        return getServer(room).setNodeAcl(sid, acl);
    }
    public Acl getNodeAcl(String room, int sid, Acl acl, Principal principal, 
                                       boolean effective) throws RemoteException 
    {
        activeError = getServer(room).getNodeAcl(sid, acl, principal, effective);
        VWSLog.dbgCheck("activeError inside  getNodeAcl::::: "+ activeError);
        return acl;
    }
    public int getAclForNode(String room, int sid, int nodeId, String userName) throws RemoteException 
    {
    	int ret = getServer(room).getAclForNode(sid, nodeId, userName);
    	return ret;
    }
    public Acl getAcl(String room, int sid, Acl acl) throws RemoteException 
    {
        activeError = getServer(room).getAcl(sid, acl);
        VWSLog.dbgCheck("activeError inside  getAcl::::: "+ activeError);
        return acl;
    }
    public Acl getAclFor(String room, int sid, Acl acl, String user) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).getAclFor(sid, acl, user);
        VWSLog.dbgCheck("activeError inside  getAcl1::::: "+ activeError);
        return acl;
    }
    //---------------------------DSS Methods------------------------------------
    public ServerSchema getDSSforDoc(String room, int sid, Document doc, 
                                         ServerSchema ss) throws RemoteException
    {
        activeError = getServer(room).getDSSforDoc(sid, doc, ss);
        VWSLog.dbgCheck("activeError inside  getDSSforDoc::::: "+ activeError);
        return ss;
    }
    public ServerSchema getDSSForLocation(String room, int sid, int moveLocationId, ServerSchema ss) throws RemoteException{
    	activeError = getServer(room).getDSSForLocation(sid, moveLocationId, ss);
        VWSLog.dbgCheck("activeError inside  getDSSForLocation::::: "+ activeError);
    	return ss;
    }
    
    public Vector getDSSList(String room, int sid, Vector dssList) 
                                                          throws RemoteException
    {
        activeError = getServer(room).getDSSList(sid, dssList);
        VWSLog.dbgCheck("activeError inside  getDSSList::::: "+ activeError);
        return dssList;
    }
    //---------------------DSS Methods called by DSS Server---------------------
    public Vector getDSSStores(ServerSchema ss, Vector stores) 
                                                          throws RemoteException
    {
        activeError = getServer(ss.room).getDSSStores(ss, stores);
        VWSLog.dbgCheck("activeError inside  getDSSStores::::: "+ activeError);
        return stores;
    }
    public int addDSSStore(ServerSchema ss) throws RemoteException
    {
        return getServer(ss.room).addDSSStore(ss);
    }
    public int updateAzureStorageInfo(String room,String storageValue,String storageUserName,String StoragePassword) throws RemoteException
    {
        return getServer(room).updateAzureStorageInfo(storageValue,storageUserName,StoragePassword);
    }
    
    
    public int removeDSSStore(ServerSchema ss) throws RemoteException
    {
        return getServer(ss.room).removeDSSStore(ss);
    }
    public String getEncryptionKey(String room, String GUID) 
                                                          throws RemoteException
    {
         return getServer(room).getEncryptionKey(GUID);
    }
    public Vector getRoomStatistics(String room, int sid, Vector info) 
                                                          throws RemoteException
    {
        activeError = getServer(room).getRoomStatistics(sid, info);
        VWSLog.dbgCheck("activeError inside  getRoomStatistics::::: "+ activeError);
        return info;
    }
    public int isTextSearchEnabled(String room, int sid) throws RemoteException
    {
        return getServer(room).isTextSearchEnabled(sid); 
    }
/*
Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
Created by: <Pandiya Raj.M>
Date: <6 Jun 2006>
Add the argument option which is either '0' or '3'.
*/

    public int addIndexerDoc(String room, int sid, int nodeId, int option) 
                                                          throws RemoteException
    {
        return getServer(room).addIndexerDoc(sid,nodeId,option); 
    }
    // Issue 511
    public Vector getDSSIPPort(ServerSchema ss,String location)
														throws RemoteException {
    		Vector address = new Vector();
    		activeError =getServer(ss.room).getDSSIPPort(location,address);
            VWSLog.dbgCheck("activeError inside  getDSSIPPort::::: "+ activeError);
    		return address;		
    }
    
    public Vector getStorageId(ServerSchema ss, String location)
    		throws RemoteException {
    	Vector retVect = new Vector();
    	activeError =getServer(ss.room).getStorageId(location,retVect);
    	return retVect;		
    }

   
    // Issue 511
    //---------------------------Preferences Methods----------------------------
    public ServerSchema getSchema() throws RemoteException 
    {
        return mySchema;
    }                                                          
    public void setComPort(int port) throws RemoteException 
    {
        VWSPreferences.setComPort(port);
    }
    public int getComPort() throws RemoteException
    {
        return VWSPreferences.getComPort();
    }
     public void setDataPort(int port) throws RemoteException
    {
        VWSPreferences.setDataPort(port);
    }
    public int getDataPort() throws RemoteException
    {
        return VWSPreferences.getDataPort();
    }
    public void setProxy(boolean b) throws RemoteException 
    {
        VWSPreferences.setProxy(b);
    }
    public void setProxyHost(String server) throws RemoteException 
    {
        VWSPreferences.setProxyHost(server);
    }
    public void setProxyPort(int port) throws RemoteException 
    {
        VWSPreferences.setProxyPort(port);
    }
    public boolean getProxy() throws RemoteException 
    {
        return VWSPreferences.getProxy();
    }
    public String getProxyHost() throws RemoteException 
    {
        return VWSPreferences.getProxyHost();
    }
    public int getProxyPort() throws RemoteException 
    {
        return VWSPreferences.getProxyPort();
    }
    public String getSSType() throws RemoteException 
    {
        return VWSPreferences.getSSType();
    }
    public String getSSLoginHost() throws RemoteException 
    {
        return VWSPreferences.getSSLoginHost();
    }
    public String getSSAuthorsHost() throws RemoteException 
    {
        return VWSPreferences.getSSAuthorsHost();
    }
    public String getTree() throws RemoteException 
    {
        return VWSPreferences.getTree();
    }
    public void setSSType(String type) throws RemoteException
    {
        VWSPreferences.setSSType(type);
    }
    public void setSSLoginHost(String host) throws RemoteException
    {
        VWSPreferences.setSSLoginHost(host);
    }
    public void setSSAuthorsHost(String host) throws RemoteException
    {
        VWSPreferences.setSSAuthorsHost(host);
    }
    public void setTree(String tree) throws RemoteException
    {
        VWSPreferences.setTree(tree);
    }
    public String getLoginContext() throws RemoteException 
    {
        return VWSPreferences.getLoginContext();
    }
    public String getLdapPort() throws RemoteException 
    {
        return VWSPreferences.getLdapPort();
    }
    public void setLdapPort(String port) throws RemoteException
    {
        VWSPreferences.setLdapPort(port);
    }
    public void setLoginContext(String context) throws RemoteException
    {
        VWSPreferences.setLoginContext(context);
    }
    public String getAuthorsContext() throws RemoteException 
    {
        return VWSPreferences.getAuthorsContext();
    }
    public void setAuthorsContext(String context) throws RemoteException
    {
        VWSPreferences.setAuthorsContext(context);
    }
    public boolean setManager(String man, String pas) throws RemoteException
    {
        return VWSPreferences.setManager(man, pas);
    }
    public String getManager() throws RemoteException
    {
        return VWSPreferences.getManager();
    }
    //------------------------------E n d    P r e f s--------------------------
    public long getTimeOn() throws RemoteException 
    {
        return (System.currentTimeMillis() - 0) / 1000;
    }
    public String managerBegin(ServerSchema manager) throws RemoteException 
    {
        if (managerSchema == null) 
        {
            managerSchema = manager;
            return "";
        }
        else
        {
            if (managerSchema.address.equals(manager.address))
                return "";
            else
                return "Cannot monitor server. Manager @" 
                                + managerSchema.address + " already connected.";
        }
    }
    public void managerEnd() throws RemoteException 
    {
        managerSchema = null;
    }
    public LogMessage getLogMsg() throws RemoteException 
    {
        return VWSLog.getLog();
    }
    public int getLogCount() throws RemoteException
    {
        return VWSLog.getCount();
    }
    public Vector getClients() throws RemoteException 
    {
        return new Vector();
    }
    public void removeRoom(String room) throws RemoteException
    {
        VWSPreferences.removeRoom(room);
    }
    public void addRoom(RoomProperty rp) throws RemoteException
    {
        VWSPreferences.addRoom(rp);
        int timeout = rp.getIdle();
        if (timeout > 0) getServer(rp.getName()).setIdleTimeout(timeout);
    }
    public boolean getRoomState(String room) throws RemoteException
    {
        return VWSPreferences.getRoomState(room);
    }
    
    public void setRoomState(String room, boolean state) throws RemoteException
    {
        VWSPreferences.setRoomState(room, state);
        if (state) startWorker(VWSPreferences.getRoom(room));
        //enabling this feature requires extra work
        else stopWorker(room);
    }
    public RoomProperty[] getRooms() throws RemoteException
    {
        return VWSPreferences.getRooms();
    }
    public RoomProperty[] getRegRooms() throws RemoteException
    {
        return VWSPreferences.getRegRooms();
    }
    public boolean getLogInfo() throws RemoteException
    {
        return VWSPreferences.getLogInfo();
    } 
    public void setLogInfo(boolean b) throws RemoteException
    {
        VWSPreferences.setLogInfo(b);
    }
    public boolean getDebugInfo() throws RemoteException
    {
        return VWSPreferences.getDebugInfo();
    }
    public void setDebugInfo(boolean b) throws RemoteException
    {
        VWSPreferences.setDebugInfo(b);
    }
    /*
    public String getLicenseKey() throws RemoteException
    {
        return VWSPreferences.getLicenseKey();
    }
    public void setLicenseKey(String license) throws RemoteException
    {
        VWSPreferences.setLicenseKey(license);
    }
     */
    public String getLicenseFile() throws RemoteException
    {
        return VWSPreferences.getLicenseFile();
    }
    public String gethotFolderPath() throws RemoteException
    {
        return VWSPreferences.getHotFolderPath();
    }
    public void setLicenseFile(String license) throws RemoteException
    {
        VWSPreferences.setLicenseFile(license);
    }
    
    public void setHotFolderPath(String location) throws RemoteException
    {
        VWSPreferences.setHotFolderPath(location);
    }
    public String getNamedOnlineUsers() throws RemoteException
    {
        return VWSPreferences.getNamedOnline();
    }
    public String getConcurrentOnlineUsers() throws RemoteException
    {
        return VWSPreferences.getConcurrentOnline();
    }
    public String getNamedUsers() throws RemoteException
    {
        return VWSPreferences.getNamedUsers();
    }
    public void setNamedUsers(String users) throws RemoteException
    {
        VWSPreferences.setNamedUsers(users);
    }
    
    public void setNamedOnlineUsers(String users) throws RemoteException
    {
        VWSPreferences.setNamedOnline(users);
    }
    
    
    public void setSecurityAdministrator(String users) throws RemoteException
    {
        VWSPreferences.setSecurityAdministrator(users);
    }
    public String getProfessionalNamedUsers() throws RemoteException
    {
        return VWSPreferences.getProfessionalNamedUsers();
    }
    public String getEnterPriseConcurrent() throws RemoteException
    {
        return VWSPreferences.getEnterpriseConcurrent();
    }
    public String getProfessionalConcurrent()throws RemoteException{
    	return VWSPreferences.getProfessionalConcurrent();
    }
    public String getSecurityAdministrator() throws RemoteException
    {
        return VWSPreferences.getSecurityAdministrator();
    }
    public void setProfessionalNamedUsers(String users) throws RemoteException
    {
    	VWSPreferences.setProfessionalNamedUsers(users);
    }
    // Added for Email Server attributes 30 Jan 2007
    public void setEmailServerName(String emailServerName) throws RemoteException{
    	VWSPreferences.setEmailServerName(emailServerName);
    }
    public void setFromEmailId(String emailId) throws RemoteException{
    	VWSPreferences.setFromEmailId(emailId);
    }
    public void setEmailServerPort(String emailServerPort) throws RemoteException{
    	VWSPreferences.setEmailServerPort(emailServerPort);
    }
    public void setEmailServerUsername(String emailServerUsername) throws RemoteException{
    	VWSPreferences.setEmailServerUsername(emailServerUsername);
    }
    public void setEmailServerPassword(String emailServerPassword) throws RemoteException{
    	VWSPreferences.setEmailServerPassword(emailServerPassword);
    }
    
    public String getEmailServerName() throws RemoteException{
    	return VWSPreferences.getEmailServerName();
    }
    public String getFromEmailId() throws RemoteException{
    	return VWSPreferences.getFromEmailId();
    }
    public String getEmailServerPort() throws RemoteException{
    	return VWSPreferences.getEmailServerPort();
    }
    public String getEmailServerUsername() throws RemoteException{
    	return VWSPreferences.getEmailServerUsername();
    }
    public String getEmailServerPassword() throws RemoteException{
    	return VWSPreferences.getEmailServerPassword();
    }
    
    public int getLoginLockTime() throws RemoteException
    {
        return VWSPreferences.getLoginLockTime();
    }
    public void setLoginLockTime(int minutes) throws RemoteException
    {
        VWSPreferences.setLoginLockTime(minutes);
    }
    public int getMaxLoginAttempts() throws RemoteException
    {
         return VWSPreferences.getMaxLoginAttempts();
    }
    public void setMaxLoginAttempts(int count) throws RemoteException
    {
        VWSPreferences.setMaxLoginAttempts(count);
    }
    /* For Issue no 736: Instead of using idle timeout with MDSS, it is changed based on lastActiveTime 
    *  Method added for client to get idle timeout, C.Shanmugavalli, 27 Oct 2006
    */
    public int getRoomIdleTime(String room) throws RemoteException{
    	int idleTime =  VWSPreferences.getRoomIdleTimeOut(room);
    	if(idleTime == 0)
    		idleTime = VWSConstants.DEFAULT_IDLE_TIME;
  	  return idleTime;
    }
    //--------------------------------------------------------------------------
    protected class RoomWorker extends Thread
    {
        private Room room;
        private RampageServer server; 
        private int bsi;
        
        public RoomWorker(Room room, int bsi)
        {
            this.room = room;
            this.bsi = bsi;
        }
        public RampageServer getServer()
        {
            return this.server;
        }
        public void run()
        {
            server = new RampageServer(room, mySchema, bsi);
            registerRoom(room);
            registerServer(server);
            registerServerThread(this);
            server.start();            
            
        }
    }
    public Vector getDocumentInfo(String room, int sid, Document doc, 
                                             Vector docs) throws RemoteException
    {
        activeError = getServer(room).getDocumentInfo(sid, doc, docs);
        VWSLog.dbgCheck("activeError inside  getDocumentInfo::::: "+ activeError);
        return docs;
    }
    public Vector getNodeAllDocuments(String room, int sid, int nid, 
                         int sessionId, Vector documents) throws RemoteException 
    {
        activeError = getServer(room).getNodeAllDocuments(sid, nid, sessionId, 
                                                                     documents);
  	  VWSLog.dbgCheck("activeError inside  getNodeAllDocuments::::: "+ activeError);
        return documents;
    }

    public int releaseSession(String room, int sid, Node node) 
                                                          throws RemoteException 
    {
        return getServer(room).releaseSession(sid, node);
    }
    
    public int createSession(String room, int sid, Node node, String sessionName) 
                                                          throws RemoteException 
    {
        return getServer(room).createSession(sid, node, sessionName);
    }

    public Vector getRecycleDocList(String room, int sid, int rowCount, int lastRowId, int actionType, Vector documents) 
    throws RemoteException 
    {
	activeError = getServer(room).getRecycleDocList(sid, rowCount, lastRowId, actionType, documents);
	  VWSLog.dbgCheck("activeError inside  getRecycleDocList::::: "+ activeError);
	return documents;
    }

    
    public Vector getRecycleDocList(String room, int sid, int rowCount, int lastRowId, int actionType, String indexValue, Vector documents) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).getRecycleDocList(sid, rowCount, lastRowId, actionType, indexValue, documents);
        VWSLog.dbgCheck("activeError inside  getRecycleDocList1::::: "+ activeError);
        return documents;
    }
    
    public int getRecycleDocCount(String room, int sid, String indexValue) throws RemoteException {
	int recCount = getServer(room).getRecycleDocCount(sid, indexValue);
	return recCount;
    }
    
    public Vector getLockDocList(String room, int sid, Vector documents) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).getLockDocList(sid, documents);
        VWSLog.dbgCheck("activeError inside  getLockDocList::::: "+ activeError);
        return documents;
    }
    
    public int setNodeStorage(String room, int sid, Node node, int withInherit)
                                                          throws RemoteException 
    {
        return getServer(room).setNodeStorage(sid, node, withInherit);
    }
    
    public int isDocTypeFound(String room, int sid, DocType docType)
                                                          throws RemoteException 
    {
        return getServer(room).isDocTypeFound(sid, docType);
    }
    
    public int isIndexFound(String room, int sid, Index index, int checkType, DocType dType)
                                                          throws RemoteException 
    {
        return getServer(room).isIndexFound(sid, index, checkType, dType);
    }
    
    public int deleteDocType(String room, int sid, DocType docType)
                                                          throws RemoteException 
    {
        return getServer(room).deleteDocType(sid, docType);
    }
    
    public int deleteIndex(String room, int sid, Index index)
                                                          throws RemoteException 
    {
        return getServer(room).deleteIndex(sid, index);
    }
    
    public int deleteDTIndex(String room, int sid, Index index)
                                                          throws RemoteException 
    {
        return getServer(room).deleteDTIndex(sid, index);
    }
    
    public int setDocTypeAutoMail(String room, int sid, DocType docType)
                                                          throws RemoteException 
    {
        return getServer(room).setDocTypeAutoMail(sid, docType);
    }
    
    public int restoreNodeFromRecycle(String room, int sid, Node node) 
                                                          throws RemoteException
    {
        return getServer(room).restoreNodeFromRecycle(sid, node);
    }
    
    public Vector getUserRedactions(String room, int sid, String userName, 
                                        Vector documents) throws RemoteException 
    {
        activeError = getServer(room).getUserRedactions(sid, userName, documents);
        VWSLog.dbgCheck("activeError inside  getUserRedactions::::: "+ activeError);
        return documents;
    }
    public Vector getAllRedactions(String room, int sid, Document doc, 
        Vector redactions)                                throws RemoteException 
    {
        activeError = getServer(room).getAllRedactions(sid, doc, redactions);
        VWSLog.dbgCheck("activeError inside  getAllRedactions::::: "+ activeError);
        return redactions;
    }
    public int setUserRedactions(String room, int sid, String userName, 
     String oldPassword, String newPassword, Vector docs) throws RemoteException 
    {
        return getServer(room).setUserRedactions(sid, userName, oldPassword, 
            newPassword, docs);
    }
    public int setDTVersionSettings(String room, int sid, DocType docType) 
                                                          throws RemoteException 
    {
        return getServer(room).setDTVersionSettings(sid, docType);
    }
    public Vector getDTVersionSettings(String room, int sid, DocType docType) 
                                                          throws RemoteException 
    {
        Vector settings = new Vector();
        activeError = getServer(room).getDTVersionSettings(sid, docType,settings);
        VWSLog.dbgCheck("activeError inside  getDTVersionSettings::::: "+ activeError);
        return settings;
    }
    
    public Vector getATSettings(String room, int sid) throws RemoteException 
    {
        Vector settings=new Vector();
        activeError = getServer(room).getATSettings(sid, settings);
        VWSLog.dbgCheck("activeError inside  getATSettings::::: "+ activeError);
        return settings;
    }
    
    public int setATSetting(String room, int sid, int eventId, int status)
                                                          throws RemoteException 
    {
        return getServer(room).setATSetting(sid, eventId, status);
    }
    
    public Vector getATData(String room, int sid, Vector filter)  
                                                          throws RemoteException
    {
        Vector data=new Vector();
        activeError = getServer(room).getATData(sid, filter,data);
        VWSLog.dbgCheck("activeError inside  getATData::::: "+ activeError);
        return data;
    }
    public int detATData(String room, int sid, Vector filter,String archivePath) 
                                                          throws RemoteException
    {
            return getServer(room).detATData(sid, filter, archivePath);
    }
    public int updateDocType(String room, int sid, DocType docType)  
                                                          throws RemoteException
    {
        return getServer(room).updateDocType(sid, docType);
    }
    public int updateIndex(String room, int sid, Index index)  
                                                          throws RemoteException
    {
        return getServer(room).updateIndex(sid, index);
    }
    public int updateDTIndex(String room, int sid, int docTypeId, Index index)  
                                                          throws RemoteException
    {
        return getServer(room).updateDTIndex(sid, docTypeId, index);
    }
    public int updateIndexSelection(String room, int sid, Index index, 
                                        String selValues, String clientMode) throws RemoteException
    {
        return getServer(room).updateIndexSelection(sid, index, selValues, clientMode);
    }
    public Vector getIndices(String room, int sid, int indexId, Vector indices) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).getIndices(sid, indexId, indices);
        VWSLog.dbgCheck("activeError inside  getIndices::::: "+ activeError);
        return indices;
    }
    public Vector getDocTypeInfo(String room, int sid, int docTypeId, 
                                      Vector docTypeInfo) throws RemoteException 
    {
        activeError = getServer(room).getDocTypeInfo(sid, docTypeId, docTypeInfo);
        VWSLog.dbgCheck("activeError inside  getDocTypeInfo::::: "+ activeError);
        return docTypeInfo;
    }
    public int resyncDirectory(String room, int sid) throws RemoteException 
    {
        return getServer(room).resyncDirectory(sid);
    }
    public Vector checkNextDocVersion(String room, int sid, Document doc, 
                               String pi, boolean newDoc) throws RemoteException 
    {
        Vector lastVer = new Vector();
        activeError = getServer(room).checkNextDocVersion(sid, doc, pi, newDoc, 
                                                                       lastVer);
        VWSLog.dbgCheck("activeError inside  checkNextDocVersion::::: "+ activeError);
        return lastVer;
        
    }

    public int setDocAuditInfo(String room, int sid, Document doc, 
        boolean newDoc,String pageInfo) throws RemoteException 
    {
        return getServer(room).setDocAuditInfo(sid, doc, newDoc,pageInfo);
    }
    /********************************eWork*************************************/
    public Vector getEWorkTemplates(String room, int sid, Vector templates)
                                                          throws RemoteException
    { 
        activeError = getServer(room).getEWorkTemplates(sid, templates); 
        VWSLog.dbgCheck("activeError inside  getEWorkTemplates::::: "+ activeError);
        return templates;
    }
    
    public EWorkTemplate newEWorkTemplate(String room, int sid, EWorkTemplate template) 
                                                          throws RemoteException 
    {
        activeError = getServer(room).newEWorkTemplate(sid , template);
        VWSLog.dbgCheck("activeError inside  newEWorkTemplate::::: "+ activeError);
        return template;
    }
    
    public int updateEWorkTemplate(String room, int sid,  EWorkTemplate template)
                                                          throws RemoteException
    {
        return getServer(room).updateEWorkTemplate(sid, template); 
    }
    public int deleteEWorkTemplate(String room, int sid,  EWorkTemplate template)
                                                          throws RemoteException
    {
        return getServer(room).deleteEWorkTemplate(sid, template); 
    }
    public Vector getEWorkTemplateMapping(String room, int sid, int templateId, 
                                         Vector mapping) throws RemoteException
    {
        activeError = getServer(room).getEWorkTemplateMapping(sid, templateId, 
                                                                       mapping);         
        VWSLog.dbgCheck("activeError inside  getEWorkTemplateMapping::::: "+ activeError);
        return mapping;
    }
    public EWorkSubmit newEWorkSubmit(String room, int sid, EWorkSubmit ewSubmit) 
                                                          throws RemoteException
    {
        getServer(room).newEWorkSubmit(sid , ewSubmit);
        return ewSubmit;
    }
    public Vector getEWorkSubmits(String room, int sid, Vector ewSubmits)
                                                          throws RemoteException
    {
        activeError = getServer(room).getEWorkSubmits(sid, ewSubmits); 
        VWSLog.dbgCheck("activeError inside  getEWorkSubmits::::: "+ activeError);
        return ewSubmits;
    }
    
    public Vector getEWorkSubmitMapping(String room, int sid, int submitId, 
                                         Vector mapping) throws RemoteException
    {
        activeError = getServer(room).getEWorkSubmitMapping(sid, submitId, 
                                                                       mapping); 
        VWSLog.dbgCheck("activeError inside  getEWorkSubmitMapping::::: "+ activeError);
        return mapping;
    }
    
    public int endEWorkSubmit(String room, int sid, int submitId) 
                                                         throws RemoteException
    {
        return getServer(room).endEWorkSubmit(sid, submitId); 
    }
    //------------------------------------AIP-----------------------------------
    public int addMimeMessage(String room, int sid,String mesgParms) 
                                                          throws RemoteException
    {
         return  getServer(room).addMimeMessage(sid,mesgParms);
    }
    public Vector getMimeMessages(String room,int sid) throws RemoteException
    {
        Vector ret =new Vector();  
        activeError = getServer(room).getMimeMessages(sid, ret);
        VWSLog.dbgCheck("activeError inside  getMimeMessages::::: "+ activeError);
        return ret;        
    }
    //Issue 04080603
	 /*
   There is a significant delay in the launch of the application,
   then a significant delay as soon as a connection has been established
    to the room, If populating of the Messages ID tab is the reason then this 
    should be eliminated and fetched on demand,
   */
    public Vector getMimeMessages(String room,int sid,String params)  throws RemoteException
    {
        Vector ret =new Vector();  
        activeError = getServer(room).getMimeMessages(sid, ret,params);
        VWSLog.dbgCheck("activeError inside  getMimeMessages::::: "+ activeError);
        return ret;        
    }
    public Vector getMimeMsg(String room,int sid,String params)  throws RemoteException
    {
        Vector ret =new Vector();  
        activeError = getServer(room).getMimeMsg(sid, ret,params);
        VWSLog.dbgCheck("activeError inside  getMimeMsg::::: "+ activeError);
        return ret;        
    }
     // End of Issue 04080603
    public int deleteOldMimeMessages(String room, int sid,String hours) 
                                                          throws RemoteException
    {
         return  getServer(room).deleteOldMimeMessages(sid,hours);
    }
    public int deleteMimeMessage(String room, int sid,int msgId) 
                                                          throws RemoteException
    {
         return  getServer(room).deleteMimeMessage(sid,msgId);
    }
    public Hashtable getAIPInit(String room,int sid,String ipaddress) throws RemoteException
    {
        Hashtable initMap =new Hashtable(); 
        int status =getServer(room).getAIPInit(sid,initMap,ipaddress);
        if(status !=NoError)
            return null;
        else return initMap;
     }  
    
    public Hashtable getAIPInit(String room,int sid) throws RemoteException
    {
        Hashtable initMap =new Hashtable(); 
        int status =getServer(room).getAIPInit(sid,initMap);
        if(status !=NoError)
            return null;
        else return initMap;
     }  
     public int setAIPInit(String room,int sid,Hashtable initMap) 
                                                          throws RemoteException
     {
            return getServer(room).setAIPInit(sid,initMap);
     }  
     
     public int setAIPInit(String room,int sid,Hashtable initMap,String ipaddress) 
    		 throws RemoteException
     {
    	 return getServer(room).setAIPInit(sid,initMap,ipaddress);
     }  
     
     public int setAIPForm(String room,int sid,String formParam) 
                                                          throws RemoteException
     {
         return getServer(room).setAIPForm(sid,formParam);
     } 
    
     public int setAIPFormField(String room,int sid,int formId,String fieldParam) 
                                                          throws RemoteException
     {
          return  getServer(room).setAIPFormField(sid,formId,fieldParam);
          
     } 
    public Vector getAIPFormFields(String room,int sid,int formId) 
                                                          throws RemoteException
    {   
        Vector fields =new Vector();
        activeError = getServer(room).getAIPFormFields(sid,formId,fields);
        return fields;
    }    
    public Vector getAIPForms(String room,int sid) throws RemoteException
    {
        Vector forms =new Vector();
        activeError = getServer(room).getAIPForms(sid,forms);         
        VWSLog.dbgCheck("activeError inside  getAIPForms::::: "+ activeError);
        return forms;         
    }    
    public int deleteAIPForm(String room,int sid,int formId) 
                                                          throws RemoteException
    {
      return getServer(room).deleteAIPForm(sid,formId);
    }
    public int deleteAIPFormFields(String room,int sid,int formId)
                                                          throws RemoteException
    {
      return  getServer(room).deleteAIPFormFields(sid,formId);   
    } 
    public int deleteAIPFormField(String room,int sid,int fieldId) 
                                                          throws RemoteException
    {
        return  getServer(room).deleteAIPFormField(sid,fieldId); 
    }    
    public Vector getAIPFormMap(String room,int sid,int formId,int mapDTId) 
                                                          throws RemoteException
    {
         Vector maps =new Vector();
         activeError = getServer(room).getAIPFormMap(sid,formId,mapDTId,maps);
         return maps;
    } 
    public int setAIPFormFieldMap(String room,int sid,String fieldMapParms) 
                                                          throws RemoteException
    {
        return getServer(room).setAIPFormFieldMap(sid,fieldMapParms);   
    }    
    public int deleteAIPFormMap(String room,int sid,int formId,int mapDTId) 
                                                          throws RemoteException
    {
        return getServer(room).deleteAIPFormMap(sid,formId,mapDTId);   
    } 
    // Code added for EAS Enahance History Tab - Start
    public Vector getAIPHistoryFiles(String room,int sid) 
                                                          throws RemoteException   
      {
    	return getAIPHistoryFiles(room,sid,"");
    }
   
    public Vector getAIPHistoryFiles(String room,int sid,String params) 
                                                          throws RemoteException   
    {
           Vector files =new Vector();
           activeError = getServer(room).getAIPHistoryFiles(sid,files,params);
       		VWSLog.dbgCheck("activeError inside  getAIPHistoryFiles::::: "+ activeError);
           return files;
    }
    public Vector getAIPHistoryCount(String room,int sid,String params) 
    														throws RemoteException   
	{
           Vector files =new Vector();
		activeError = getServer(room).getAIPHistoryCount(sid,files,params);
		VWSLog.dbgCheck("activeError inside  getAIPHistoryCount::::: "+ activeError);
           return files;
      }
    // Code added for EAS Enahance History Tab - End
    // Archive History Files - Start
    public Vector getAIPHistoryFilesForArchive(String room,int sid,String params)throws RemoteException
    {
        Vector files =new Vector();
        activeError = getServer(room).getAIPHistoryFilesForArchive(sid,files,params);
        return files;
    }
    public int deleteAIPHistoryArchivedFiles(String room,int sid)throws RemoteException
    {
        activeError = getServer(room).deleteAIPHistoryArchivedFiles(sid);
   	  	VWSLog.dbgCheck("activeError inside  deleteAIPHistoryArchivedFiles::::: "+ activeError);
        return activeError;
    }
    // Archive History Files - End
	//Issue 04080603
    public Vector getMimeMessagesCount(String room,int sid) 
	throws RemoteException   
	{
    	Vector files =new Vector();
    	activeError = getServer(room).getMimeMessagesCount(sid,files);
    	  VWSLog.dbgCheck("activeError inside  getMimeMessagesCount::::: "+ activeError);
    	return files;
	}
    //Issue 04080603
      public int setAIPHistoryFile(String room, int sid, String fileParams)
                                                          throws RemoteException
      {
            return getServer(room).setAIPHistoryFile(sid,fileParams);
      }
      public int deleteAIPHistoryFile(String room,int sid,int fileId)
                                                          throws RemoteException
      {   
             return getServer(room).deleteAIPHistoryFile(sid,fileId);
      }
      public int setAIPDocument(String room,int sid,String docParams) 
                                                          throws RemoteException
      {
            return getServer(room).setAIPDocument(sid,docParams);
      }
      public int setAIPDocFields(String room,int sid,Vector fieldsParams) 
                                                          throws RemoteException
      {
          return getServer(room).setAIPDocFields(sid,fieldsParams);
      }
      public Vector getAIPDocuments(String room,int sid,int aipFileId, int pendingStatus) 
                                                          throws RemoteException
      {             
            Vector aipDocs =new Vector();
            getServer(room).getAIPDocuments(sid,aipFileId, aipDocs, pendingStatus);
            return aipDocs;
      }   
      public Vector getAIPDocFields(String room,int sid,int aipDocId) 
                                                          throws RemoteException
      {
           Vector fields =new Vector();
           activeError = getServer(room).getAIPDocFields(sid,aipDocId,fields);
           VWSLog.dbgCheck("activeError inside  getAIPDocFields::::: "+ activeError);
           return fields; 
      } 
      public int deleteAIPDocument(String room ,int sid,int aipDocId) 
                                                          throws RemoteException
      {
          return getServer(room).deleteAIPDocument(sid,aipDocId);
      }
      public Vector getAIPEmailSuffixes(String room,int sid) 
                                                          throws RemoteException
      {
        Vector ret =new Vector();  
        VWSLog.dbgCheck("activeError inside  getAIPEmailSuffixes::::: "+ activeError);
        activeError = getServer(room).getAIPEmailSuffixes(sid, ret);
        return ret;        
      } 
      public int setAIPEmailSuffixes(String room, int sid,Vector suffixes) 
                                                          throws RemoteException
      {
         return  getServer(room).setAIPEmailSuffixes(sid,suffixes);
      }
      public Vector getDirectoryPrincipals(String room, int sid) 
                                                        throws RemoteException 
      {
        Vector principals = getServer(room).getDirectoryPrincipals(sid);
        return principals; 
      }
      public Vector getDirectoryPrincipalsNew(String room, int sid, String searchForPName) 
              throws RemoteException 
      {
    	  Vector principals = getServer(room).getDirectoryPrincipalsNew(sid, searchForPName);
    	  return principals; 
      }

      public int setUserSign(String room, int sid, String userName, 
                              int signType,VWDoc signDoc) throws RemoteException
      {
        return getServer(room).setUserSign(sid, userName, signType, signDoc);
      }
      public int removeUserSign(String room, int sid, String userName, int signType){
    	  return getServer(room).removeUserSign(sid, userName, signType);
      }
		
      public VWDoc getUserSign(String room, int sid, String userName, 
                              int signType,VWDoc signDoc) throws RemoteException
      {
          activeError = getServer(room).getUserSign(sid, userName, signType, 
                                                                       signDoc);
          return signDoc;
      }
      public int setDocumentSign(String room, int sid, Signature sign)
                                                          throws RemoteException 
      {
          return getServer(room).setDocumentSign(sid, sign);
      }
      public Vector getDocumentSign(String room, int sid, Signature sign)
                                                          throws RemoteException 
      {
          Vector signInfo=new Vector();
          activeError = getServer(room).getDocumentSign(sid, sign,signInfo);
          VWSLog.dbgCheck("activeError inside  getUserSign::::: "+ activeError);
          return signInfo;
      }
      
      public int isSignAnnotationExist(int sid, String room){
          Vector signInfo=new Vector();
          activeError = getServer(room).isSignAnnotationExist(sid, signInfo);
          int isSignAnnotation = 0;
          if (signInfo != null && signInfo.size() > 0){
        	  isSignAnnotation = Integer.parseInt(signInfo.get(0).toString());  
          }          
          return isSignAnnotation;   	  
      }
      
      public Vector getDocumentSigns(String room, int sid, Document doc)
                                                          throws RemoteException 
      {
          Vector signInfo=new Vector();
          activeError = getServer(room).getDocumentSigns(sid, doc,signInfo);
          return signInfo;
      }
      public int updateStorageInfo(String room,int sid, ServerSchema storage) 
                                                          throws RemoteException
      {
          return getServer(room).updateStorageInfo(sid, storage);
      }
      public Vector DBGetDocPageText(String room, int sid, Document doc) 
                                                          throws RemoteException
      {
          Vector docPages=new Vector();
          activeError = getServer(room).DBGetDocPageText(sid, doc,docPages);
          return docPages;
      }
      public int getActiveError() throws RemoteException
      {
          return activeError;
      }
      // Add the Encoding method to read the encoding type from server registry.
      public String getEncoding() throws RemoteException 
	  {
      	return VWSPreferences.getEncoding();
	  }
      // End
/*
Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
Created by: <Pandiya Raj.M>
Date: <6 Jun 2006>
Add this method to remove the page information from the 'Indexer' table. It helps to re-index the page.
*/
      
      public void DBRemovePageText(String room, int docId, int pageId) throws RemoteException{
    	      	  getServer(room).DBRemovePageText(docId,pageId);
      }
   
      /*
		 * Purpose: following are methods which will in turn call DSS methods
		 * Created by: C.Shanmugavalli Date: 09 Oct 2006
		 */
	// Start of DSS related method calls
	// Pass the schema to displayed the client address in Server Manager
	public VWDoc setDocument(String room, ServerSchema dssSchema, Document doc, File file, ServerSchema svr) throws RemoteException {
		VWDoc vwDoc = doc.getVWDoc();
		// vwDoc.setSrcFile(file.getPath());
		// vwDoc.setDstFile(dssSchema.path);
		DSS ds = (DSS) Util.getServer(dssSchema);
		if (ds == null)
			return null;
		try {
			vwDoc = ds.setDocument(doc, room, svr);
		} catch (Exception ex) {
			// VWSLog.err("error in getting "+ex.getMessage());
			return null;
		}
		return vwDoc;
	}	
      public VWDoc setDocFolder(Document doc) throws RemoteException{
      		//VWSLog.err("coming here in VWS setDocFolder");
      		VWDoc vwdoc = doc.getVWDoc();
      		int ret = vwdoc.writeContents();
            if (ret < 0){
            	vwdoc.setVaildZip(false);
            }
       return vwdoc;
      }
      
  	public void updateVersionRevision(String room, ServerSchema dssSchema, Document doc, String newVersion) throws RemoteException {
		DSS ds = (DSS) Util.getServer(dssSchema);
		if (ds == null) return;
		try {
			ds.updateVersionRevision(doc, newVersion);
		} catch (Exception ex) {
		
		}
		
	}
  	public long checkAvailableSpace(ServerSchema dssSchema, String dssLocation)throws RemoteException{
  		DSS ds = (DSS) Util.getServer(dssSchema);
		if (ds == null) return 0;
		long availableSpace = 0;
		try {
			 availableSpace = ds.checkAvailableSpace(dssLocation);
			 VWSLog.add("Available free space :"+availableSpace);
		} catch (Exception ex) {
		
		}
		return availableSpace;
  	}
      
    // Add the one more parameter to check the version/revision have the document or not
      public VWDoc getRMIDoc(Document doc, String room, String dssSchema_path, ServerSchema dssSchema, ServerSchema mySchema, 
        		boolean insideIdFolder)throws RemoteException{
    	  return getRMIDoc(doc, room, dssSchema_path, dssSchema, mySchema, insideIdFolder, new Vector());
      }
      public VWDoc getRMIDoc(Document doc, String room, String dssSchema_path, ServerSchema dssSchema, ServerSchema mySchema, 
      		boolean insideIdFolder, Vector versionList)throws RemoteException{
      		VWDoc vwDoc = doc.getVWDoc();
      		DSS ds = (DSS) Util.getServer(dssSchema);
      		if (ds == null){
      		    activeError = dssInactive;
      		    return null;
      		}
      		try{
      			vwDoc = ds.getRMIDoc(doc, room, dssSchema_path, 
      					mySchema, insideIdFolder, versionList);
      			
      		}catch(Exception ex){
      			//VWSLog.err("error in getting  "+ex.getMessage());
      			return null;
      		}
      			return vwDoc;
      } 
      /*Purpose: Issue # 791 Methods added to fix the issue. "Document is not downloaded if the size is greater than 4mb
      * C.Shanmugavalli 19 Dec 2006 
      */
      public VWDoc getNextRMIDoc(Document doc, String room, String dssSchema_path, ServerSchema dssSchema, ServerSchema mySchema, 
        		boolean insideIdFolder)throws RemoteException{
    	VWDoc vwDoc = doc.getVWDoc();
  		DSS ds = (DSS) Util.getServer(dssSchema);
  		if (ds == null) return null;
  		try{
  			vwDoc = ds.getNextRMIDoc(dssSchema, doc);
  			//doc.setVWDoc(vwDoc);
  		}catch(Exception ex){
  			//VWSLog.err("error in getting  "+ex.getMessage());
  			return null;
  		}
  			return vwDoc;
     }
      public void writeLogMsg(ServerSchema dssSvr, Document doc,String room) throws RemoteException{
    	  VWDoc vwDoc = doc.getVWDoc();
    		DSS ds = (DSS) Util.getServer(dssSvr);
    		try{
	    		if (ds != null)
	    			ds.writeLogMsg(dssSvr, doc,room);
    		}catch(Exception ex){      		}
      }
    //This method used return the list of documents to download through the VWS
      public ArrayList getRMIDocList(ServerSchema dssSchema, String srcPath) throws RemoteException {
      	ArrayList fileList = new ArrayList();
          try
          {
              DSS ds = (DSS) Util.getServer(dssSchema);
              if (ds == null) return null;
              fileList = ds.getRMIDocList(srcPath);
          }        
          catch(Exception e){return null;}
          return fileList;
      }
      public VWDoc getRMIDoc(Document doc, String room, String path, 
              ServerSchema svr, ServerSchema dssSchema) throws RemoteException{
    	  return getRMIDoc(doc, room, path, svr, dssSchema, new Vector());
      }

      public VWDoc getRMIDoc(Document doc, String room, String path, 
              ServerSchema svr, ServerSchema dssSchema, Vector versionList) throws RemoteException 
	  {
	          boolean ret = false;
	          VWDoc vwdoc = doc.getVWDoc();
	          try{
	              DSS ds = (DSS) Util.getServer(dssSchema);
	              if (ds == null) return null;
	              vwdoc = ds.getRMIDoc(doc, room, path, svr);
	          }catch(Exception ex){}
	          return vwdoc;
	  } 
	  /**CV2019 merges from SIDBI line***/  
	  public VWDoc getRMIDocWeb(Document doc, String room, String dssSchema_path, ServerSchema dssSchema, ServerSchema mySchema, 
	  		boolean insideIdFolder, Vector versionList)throws RemoteException{
	  		VWDoc vwDoc = doc.getVWDoc();
	  		DSS ds = (DSS) Util.getServer(dssSchema);
	  		if (ds == null){
	  		    activeError = dssInactive;
	  		    return null;
	  		}
	  		try{
	  			vwDoc = ds.getRMIDocWeb(doc, room, dssSchema_path, 
	  					mySchema, insideIdFolder, versionList);
	  			
	  		}catch(Exception ex){
	  			//VWSLog.err("error in getting  "+ex.getMessage());
	  			return null;
	  		}
	  			return vwDoc;
	  } 
      
	  public boolean getDocument(Document doc, String room, String path, ServerSchema dssSchema, ServerSchema mySchema, boolean insideIdFolder) throws RemoteException 
      {
      			boolean gotDocFile = false;
      			DSS ds = (DSS) Util.getServer(dssSchema);
      			if (ds == null) return false;
      			try{
      				gotDocFile = ds.getDocument(doc, room, dssSchema.path, mySchema, insideIdFolder);
      			}catch(Exception ex){
      		 	   //VWSLog.err("error in getDocument  "+ex.getMessage());
      		 	   return false;
      		    }
      		return gotDocFile;
      }
      public boolean moveDocFolder(Document doc, String room, ServerSchema dssSchema,ServerSchema Ddss) throws RemoteException
      {
      		DSS ds = (DSS) Util.getServer(dssSchema);
      		if (ds == null) return false;
      		return ds.moveDocFolder(doc, room, dssSchema, Ddss); 
      	
      }
      
      public boolean getDocFolder(Document doc, String room, String path, ServerSchema dssSchema,  ServerSchema mySchema) throws RemoteException
      {
      		DSS ds = (DSS) Util.getServer(dssSchema);
      		if (ds == null) return false;
      		return ds.getDocFolder(doc, room, dssSchema.path, mySchema);
      }
    
      public boolean deleteDocument(ServerSchema dssSchema, Document doc) throws RemoteException {
      	
      		DSS ds = (DSS) Util.getServer(dssSchema);
      		if (ds == null) return false;
      		return ds.deleteDocument(doc);
      }
      
      public long getLastModifyDate(ServerSchema dssSchema, Document doc) throws RemoteException{
      		long date = -1;
      		DSS ds = (DSS) Util.getServer(dssSchema);
   		if (ds == null) return date;
   		date = ds.getLastModifyDate(doc);
   		return date;
      }
      /**CV2019 merges from SIDBI line***/
      public boolean isAllWebExist(ServerSchema dssSchema, Document doc) throws RemoteException{
    	  boolean retval = false;
    	  DSS ds = (DSS) Util.getServer(dssSchema);
    	  if (ds == null) return false;
    	  retval = ds.isAllWebExist(doc);
    	  return retval;
      }
      public long getDocSize(ServerSchema dssSchema, Document doc, boolean withVR) throws RemoteException{
      	return getDocSize(dssSchema, doc, withVR, "");
      }
      public long getDocSize(ServerSchema dssSchema, Document doc, boolean withVR, String Version) throws RemoteException{
      	
      		DSS ds = (DSS) Util.getServer(dssSchema);
      		if (ds == null) return 0;
      		long size = ds.getDocSize(doc, withVR, Version);
      		return size;
      }
      public int getPageCount(ServerSchema dssSchema, Document doc, String room) throws RemoteException{
    	return   getPageCount(dssSchema, doc, room, new Vector());
      }
      public int getPageCount(ServerSchema dssSchema, Document doc, String room, Vector versionList) throws RemoteException{
      		DSS ds = (DSS) Util.getServer(dssSchema);
      		if (ds == null) return 0;
      		return ds.getPageCount(doc, room);
      }
      public boolean deleteDocumentVersions(ServerSchema dssSchema, Document doc) throws RemoteException{
      	
      		DSS ds = (DSS) Util.getServer(dssSchema);
   		if (ds == null) return false;
   		return ds.deleteDocumentVersions(doc);
      	
      }
      
      /*End of DSS related method calls*/   

    
      /*
      Purpose:  Following methods are used by ARS
      Created by: Shanmugavalli.C
      Date: <20 July 2006>
      */    
      //---------------------ARS Methods called by ARS Server---------------------//
      /*
       * (non-Javadoc)
       * @see com.computhink.vws.server.VWS#setDTRetentionSettings(java.lang.String, int, com.computhink.common.DocType)
       */
      public int setDTRetentionSettings(String room, int sid, DocType docType)
      throws RemoteException
  	  {
      	return getServer(room).setDTRetentionSettings(sid, docType);
  	  }
      /*
       *  (non-Javadoc)
       * @see com.computhink.vws.server.VWS#getDTRetentionSettings(java.lang.String, int, com.computhink.common.DocType)
       */
  	public Vector getDTRetentionSettings(String room, int sid, DocType docType)
  	    throws RemoteException
  	{
  		Vector settings = new Vector();
  		activeError = getServer(room).getDTRetentionSettings(sid, docType,settings);
  		return settings;
  	}
  
  	 /* Purpose:  Following ARS methods changed for Oracle script changes (param sessionID included 
     * Created by: <Valli>	Date: <08 Sep 2006>
     */
  	public Vector getRetentionDocuments(int sessionID, String room, Vector retentionDocs)
      throws RemoteException
  	{
  		activeError = getServer(room).getRetentionDocuments(sessionID, retentionDocs);
  		return retentionDocs;
  	}
  	
  	/*
  	 * To get ARS report doc type ids  
  	 */
  	public Vector getARSReportDocTypes(int sessionID, String room, Vector ARSreportDocTypes) throws RemoteException
  	{
  		activeError = getServer(room).getARSReportDocTypes(sessionID, ARSreportDocTypes);
  		return ARSreportDocTypes;
  	}
  	
  	/*
  	 * To get ARS report documents for a document type  
  	 */
  	public Vector getReportRetentionDocs(int sessionID, String room, String docTypeID) throws RemoteException
  	{
  		Vector reportDocs = new Vector();
  		activeError = getServer(room).getReportRetentionDocs(sessionID, reportDocs, docTypeID);
  		return reportDocs;
  	}
  	/*ARS method change for Oracle script change end*/
  	/*
  	 * To get the disposal actions
  	 */
  	public Vector getDisposalActions(String room, int sid, Vector disposalActs)	throws RemoteException
  	{
  		activeError = getServer(room).getDisposalActions(sid, disposalActs);
  		return disposalActs;
	}
  	// Methods to freeze and unfreeze
  	public int unFreezeDocument(String room,int sid, int docID)throws RemoteException{
      	return getServer(room).unFreezeDocument(sid, docID);
    }
      
   /* public int freezeDocument(String room,int sid, int docID, String retentionId)throws RemoteException{
      	return getServer(room).setFreezedDocs(sid, docID, retentionId);
    }*/
    public int freezeDocument(String room, int sid, int docID, String docName, String retentionId) throws RemoteException {
		return getServer(room).setFreezedDocs(sid, docID, docName, retentionId);
	}
   public int isDocFreezed(String room, int sid, int nid) throws RemoteException{
			return getServer(room).isDocFreezed(sid, nid);
   }   	
   
   public Vector getFreezeDocs(String room, int sid){
       Vector docs = new Vector();
       activeError = getServer(room).getFreezeDocs(sid, docs);
       return docs;
   }
   public boolean isDocDeleted(String room, int sid, int nid) throws RemoteException
   {
    	if(getServer(room).isDocDeleted(sid, nid) == 1)
    		return true;
    	else
    		return false;
   }
	
   public int arsMoveNode(String room, int sid, Node node) throws RemoteException
   {
        return getServer(room).arsMoveNode(sid,node);
   }

   public int arsLockDocument(String room, int sid, int did) throws RemoteException
   {
    	return getServer(room).arsLockDocument(sid, did);
   }
    
   public boolean isDocumentLocked(String room, int sid, int did,
            Vector name)  throws RemoteException
   {
    	boolean locked = false;
         activeError = getServer(room).isDocumentLocked(sid, did, name);
         if (activeError == -1)
         	locked = false;
         else if (activeError == 0)
         	locked = true;;
         return locked;      
   }
   /* Purpose:	Master Switch off added ARS in registry for client call
   * Created By: C.Shanmugavalli		Date:	22 Sep 2006
   */
   public int getARSEnable(int sid)throws RemoteException{
	   	if(ManagerPreferences.getARSEnable().equalsIgnoreCase("true")){
	 		return 1;
	 	}
	 	else{
	 		return 0;
	 	}
   }
    //---------------------End of ARS Methods called by ARS Server---------------------
    /*	Enhancement No:66-To read build time of server ViewWise.jar from manifext file
  	 *	Created by: <Shanmugavalli.C>	Date: <04 Aug 2006>
  	 */
     public String getSvrJarBuildTime()	throws RemoteException{
    	 	String checkJar = VWSPreferences.getCheckViewWiseJar();
    	 	VWSLog.dbg("checkJar registry value :"+checkJar);
      		String svrJarLocation = VWSUtil.getHome()+"\\lib\\ViewWise.jar";
    	 	String svrJarBuildTime = "-1";
    	 	if (checkJar.equalsIgnoreCase("true")){
    	 		svrJarBuildTime = Util.getJarManifestInfo(svrJarLocation);	
    	 	}      		
    	 	VWSLog.dbg("svrJarBuildTime :::"+svrJarBuildTime);
      		return svrJarBuildTime; 
      }
     /**
      * Enhancement:- CV10.1 
      */
     public String getSvrJarBuildVersion()	throws RemoteException{
 	 	String checkJar = VWSPreferences.getCheckViewWiseJar();
   		String svrJarLocation = VWSUtil.getHome()+"\\lib\\ViewWise.jar";
 	 	String svrJarBuildVersion = "-1";
 	 	if (checkJar.equalsIgnoreCase("true")){
 	 		svrJarBuildVersion = Util.getBuildVersion(svrJarLocation);	
 	 	}      		
   		return svrJarBuildVersion; 
   }
   /**
    * CV10.1 Enhancement to get the template file name.
    */
     public File[] getTemplateFiles()throws RemoteException{
    	 String templateLocation = VWSUtil.getHome()+"\\forms";
    	 File[] listOfFiles=null;
    	 if(templateLocation.length()>0){
    		 listOfFiles = Util.getPdfTemplates(templateLocation);
    	 }
    	 return listOfFiles;
     }
     
   /*
    * Added for Novell Netware issue 736 - MDSS server ping issue
    */
   public int isRoomIdle(int sid, String roomName) throws RemoteException{    	  
	  return  getServer(roomName).isRoomIdle(sid);
}
//Function implementation for issue 793   
   public int resetSecurityDetails(int sid,String roomName,int nodeID,int aclID,Vector result) throws RemoteException{
	   return  getServer(roomName).resetSecurityDetails(sid,roomName,nodeID,aclID,result);
   }
   /*  LDAP related attribute methods added here Valli 13 March 2007*/
   public String getLdapSecurity() throws RemoteException
	{
	   return VWSPreferences.getLdapSecurity();
	}
	
	public void setLdapSecurity(String s)  throws RemoteException
	{
	   VWSPreferences.setLdapSecurity(s);
	}
	
	public void setAdminPass(String s)throws RemoteException
	{
		VWSPreferences.setAdminPass(s);
	}
	
	public String getAdminPass()throws RemoteException
	{
		return VWSPreferences.getAdminPass();
	}
   
   /*Start of DRS methods 19 Dec 2006*/
	
   public String getAcceptLabel(String room, int sid, int routeId, int taskId) throws RemoteException{
	   	 Vector labelAccept = new Vector();
	  	 activeError = getServer(room).getAcceptLabel(sid, routeId, taskId, labelAccept);
	  	 if (labelAccept !=null && labelAccept.size() > 0){
	  		 return labelAccept.get(0).toString();
	  	 }
	  	 
	  	 return ROUTE_ACCEPT_LABEL;
	   }

   public String getRejectLabel(String room, int sid, int routeId, int taskId) throws RemoteException{
	   	 Vector labelReject = new Vector();
	  	 activeError = getServer(room).getRejectLabel(sid, routeId, taskId, labelReject);
	  	 if (labelReject !=null && labelReject.size() > 0){
	  		 return labelReject.get(0).toString();
	  	 }	  	 
	  	 return ROUTE_REJECT_LABEL;
	   }
   
   public Vector getRouteInfo(String room, int sid, int routeId, Vector routeInfo) throws RemoteException{
	   
  	 activeError = getServer(room).getRouteInfo(sid, routeId, routeInfo);
  	 return routeInfo;
   }
   public VWDoc getRouteImage(String room, int sid, int routeId, VWDoc routeImageDoc) throws RemoteException
   {
	   activeError = getServer(room).getRouteImageInfo(sid, routeId, routeImageDoc);
	   return routeImageDoc;
   }
   public Vector getRouteIndexInfo(String room, int sid, int routeId, Vector routeIndexInfo) throws RemoteException{
	   
	   activeError = getServer(room).getRouteIndexInfo(sid, routeId, routeIndexInfo);
	   return routeIndexInfo;
   }
   public Vector getRouteTaskInfo(String room, int sid, int routeId, int taskSequence, Vector routeTaskInfo) throws RemoteException{
	   
	   activeError = getServer(room).getRouteTaskInfo(sid, routeId, taskSequence, routeTaskInfo);
	   return routeTaskInfo;
   }
   
   public Vector getRouteUsers(String room, int sid, int routeTaskId, Vector vRouteUsers) throws RemoteException
   {
	   activeError = getServer(room).getRouteUsers(sid, routeTaskId, vRouteUsers,0);
	   return vRouteUsers;
   }
   public Vector getRouteUsers(String room, int sid, int routeTaskId, Vector vRouteUsers, int docId) throws RemoteException
   {
	   activeError = getServer(room).getRouteUsers(sid, routeTaskId, vRouteUsers, docId);
	   return vRouteUsers;
   }
   /*public Vector getRouteUser(String room, int sid, int routeUserId, Vector docRouteUsers) throws RemoteException
   {
	   activeError = getServer(room).getRouteUser(sid, routeUserId, docRouteUsers);
	   return docRouteUsers;
   }*/
		
   public int setRouteInfo(String room, int sid, RouteInfo route)throws RemoteException{
	   
	   return getServer(room).setRouteInfo(sid, route);
   }
   public int setRouteTaskInfo(String room, int sid, RouteTaskInfo routeTaskInfo)throws RemoteException{
	   
	   return getServer(room).setRouteTaskInfo(sid, routeTaskInfo);
   }
   public int setRouteIndexInfo(String room, int sid, RouteIndexInfo routeIndexInfo)throws RemoteException{
	   
	   return getServer(room).setRouteIndexInfo(sid, routeIndexInfo);
   }
   public int setRouteUsers(String room, int sid, ArrayList routeUserList,  int routeId,  int routeTaskId) throws RemoteException
   {
	   return getServer(room).setRouteUsers(sid, routeUserList, routeId, routeTaskId);
   }
   
   public int setDocTypeRoutes(String room,int sid, int RouteId, int DocTypeId) throws RemoteException
   {
	   return getServer(room).setDocTypeRoutes(sid, RouteId, DocTypeId);
   }
   
   public int setDocTypeRoutes(String room,int sid, ArrayList RouteId, int DocTypeId) throws RemoteException
   {	   
	   return getServer(room).setDocTypeRoutes(sid, RouteId, DocTypeId);
   }
   
   public Vector getDocTypeRoutes(String room, int sid, int DocTypeId, Vector docTypeRoutes) throws RemoteException
   {
	   activeError = getServer(room).getDocTypeRoutes(sid, DocTypeId, docTypeRoutes);
	   return docTypeRoutes;
   }
   public int setUserMail(String room,int sid, int userId, String userMailId) throws RemoteException
   {
	   return getServer(room).setUserMail(sid, userId, userMailId);
   }
   public Vector getUserMail(String room, int sid, String userName, Vector userMailId) throws RemoteException
   {
	   activeError = getServer(room).getUserMail(sid, userName, userMailId);
	   return userMailId;
   }
   public Vector getAssignedRoutesOfDocType(String room, int sid, int docTypeId) throws RemoteException
   {
	   Vector assignedRoutesOfDocType = new Vector();
	   activeError = getServer(room).getAssignedRoutesOfDocType(sid, docTypeId, assignedRoutesOfDocType);
	   return assignedRoutesOfDocType;
   }
   public int setRouteHistory(String room, int sid, int DocId, int routeId, int taskSeq, int routeUserId, String routeInitiatedBy, String approvedUserName, String authorizedUserName, int signId, int routeMasterId) throws RemoteException{
	   return getServer(room).setRouteHistory(sid, DocId, routeId, taskSeq, routeUserId, routeInitiatedBy, approvedUserName, authorizedUserName, signId, routeMasterId, "");
   }
   public int setRouteHistory(String room, int sid, int DocId, int routeId, int taskSeq, int routeUserId, String routeInitiatedBy, String approvedUserName, String authorizedUserName, int signId, int routeMasterId, String groupName) throws RemoteException{
	   return getServer(room).setRouteHistory(sid, DocId, routeId, taskSeq, routeUserId, routeInitiatedBy, approvedUserName, authorizedUserName, signId, routeMasterId, groupName);
   }
   
   public Vector getToDoRouteList(String room, int adminUser, int routeId, int sid) throws RemoteException{
	   Vector toDoRouteList = new Vector();
	   activeError = getServer(room).getToDoRouteList(sid, adminUser, routeId, toDoRouteList);
	   return toDoRouteList;
   }

   
   public int updateRouteHistory(String room, int sid, int docId, int routeId, int routeUserId, String status, String comments, String prevStatus, int routeMasterId,int routeHistoryId, String approvedUserName, String authorizedUserName, int signId) throws RemoteException 
   {
	   return getServer(room).updateRouteHistory(sid, docId, routeId, routeUserId, status, comments, prevStatus, routeMasterId,routeHistoryId,approvedUserName, authorizedUserName, signId);
   }
   
   public Vector getDocsToAutoRoute(String room, int sid, Vector documentsToRouteList) throws RemoteException 
   {
	   activeError = getServer(room).getDocsToAutoRoute(sid, documentsToRouteList);
	   return documentsToRouteList;
   }
   public Vector getDocsForFinalAction(String room, int sid, Vector routeCompletedDocs) throws RemoteException 
   {
	   Vector result = new Vector();
	   activeError = getServer(room).getDocsForFinalAction(sid, result);
	   return result;
   }
   public Vector getDocumentsInRoute(String room, int sid, int docId, int sourceType) throws RemoteException 
   {
	   Vector documentInRoute = new Vector();
	   activeError = getServer(room).getDocumentsInRoute(sid, docId, documentInRoute, sourceType);
	   return documentInRoute;	  
   }   
   public int updateRouteCompletedDocs(String room, int sid,  int routeMasterId, String taskFlag) throws RemoteException 
   {
	   return getServer(room).updateRouteCompletedDocs(sid, routeMasterId, taskFlag);
   }
   public int deleteDocumentUsers(String room, int sid, int routeId)  throws RemoteException{
	   return getServer(room).deleteDocumentUsers(sid, routeId);
   }
   public int isRouteInUse(String room, int sid, int routeId, int mode)  throws RemoteException{
	   return getServer(room).isRouteInUse(sid, routeId, mode);
   }
   public Vector isUserInRoute(String room, int sid, int principalId)  throws RemoteException{
       Vector resultRouteInfo = new Vector();
       activeError = getServer(room).isUserInRoute(sid, principalId, resultRouteInfo);
       return resultRouteInfo;
   }

   public int deleteRouteInfo(String room, int sid, int routeId, int isRouteNotUsed)  throws RemoteException{
	   return getServer(room).deleteRouteInfo(sid, routeId, isRouteNotUsed);
   }      
   public int deleteRouteTaskUserIndexInfo(String room, int sid, int routeId, int markAsDelete, int indexBaseRoute)  throws RemoteException{
	   return getServer(room).deleteRouteTaskUserIndexInfo(sid, routeId, markAsDelete, indexBaseRoute);
   }      
  /* public int sendMail(String room, int sid, int userID, String docName, String routeName, Vector userInfo) throws RemoteException{
	   return getServer(room).sendMail(room, sid, userID, docName, routeName, userInfo); 
   }*/
   public int sendMailWithAttachment(String room, int sid, String userName, String docName, String routeName, int docId, Vector userInfo, 
		   Vector attachment, String status, Vector docNames, Vector emailOptions, String destPath) throws RemoteException{
	   return getServer(room).sendMailWithAttachment(room, sid, userName, docName, routeName, docId, userInfo, attachment, status, docNames, emailOptions, destPath); 
   }
   
   /**CV2019 merges from SIDBI line***/
   public int sendMailWithAttachment(String room, String serverName, int sid, String userName, String docName, String routeName, int docId, Vector userInfo, 
		   Vector attachment, String status, Vector docNames, Vector emailOptions, String destPath) throws RemoteException{
	   return getServer(room).sendMailWithAttachment(room, serverName, sid, userName, docName, routeName, docId, userInfo, attachment, status, docNames, emailOptions, destPath, null); 
   }
   
   public Vector getRouteSummary(String room, int sid, int docId, int mode) throws RemoteException{
	   Vector routeSummary = new Vector();
	   activeError = getServer(room).getRouteSummary(sid, docId, mode, routeSummary);
	   return routeSummary;
   }
   
   /**CV2019 merges from SIDBI line--------------------------------***/
   public Vector getRouteNewSummary(String room, int sid, int docId, int mode) throws RemoteException{
	   Vector routeNewSummary = new Vector();
	   activeError = getServer(room).getRouteNewSummary(sid, docId, mode, routeNewSummary);
	   return routeNewSummary;
   }   
   public Vector getDocumentVersionInfo(String room, int sid, int docId) throws RemoteException{
	   Vector versionSummary = new Vector();
	   activeError = getServer(room).getDocumentVers(sid, docId, versionSummary);
	   VWSLog.dbg("activeError in getDocumentVersionInfo : "+activeError);
	   return versionSummary;
   }
   /*--------------------------------------------------------------*/

   public Vector getDocumentAuditDetails(String room, int sid, int docId, int mode) throws RemoteException{
	   Vector auditDetails = new Vector();
	   activeError = getServer(room).getDocumentAuditDetails(sid, docId, mode, auditDetails);
	   return auditDetails;
   }

   public int isRouteNameFound(String room, int sid, String routeName) throws RemoteException {	   
	   activeError = getServer(room).isRouteNameFound(sid, routeName);
	   return activeError;
   }
   public int isRouteLocationFound(String room, int sid, String nodeId) throws RemoteException {	   
	   activeError = getServer(room).isRouteLocationFound(sid, nodeId);
	   return activeError;
   }
   /* Purpose:	Master Switch off added DRS in registry for client call
    * Created By: C.Shanmugavalli		Date:	13 Feb 2007
    */
    public int getDRSEnable(int sid)throws RemoteException{
 	   	if(ManagerPreferences.getDRSEnable().equalsIgnoreCase("true")){
 	 		return 1;
 	 	}
 	 	else{
 	 		return 0;
 	 	}
    }
    public int setDRSEnable(int sid, String status)throws RemoteException{
	   	ManagerPreferences.setDRSEnable(status);
	 	return NoError;
    }
    
    public int checkDocsInRoute_CheckedOut(int sid, String room, int nodeId, boolean flagRoute)
    {
    	return getServer(room).checkDocsInRoute_CheckedOut(sid, nodeId, flagRoute);
    }
    //Overrided Method for Check locked docuements from the DTC
    public int checkDocsInRoute_CheckedOut(int sid, String room, int nodeId, int flagRoute)
    {
    	return getServer(room).checkDocsInRoute_CheckedOut(sid, nodeId, flagRoute);
    }
    public int getApprovedCountForTask(String room, int sid, int routeId, int routeTaskId, int docId, int routeUserId)throws RemoteException{
    	return getServer(room).getApprovedCountForTask(sid, routeId, routeTaskId, docId, routeUserId);
    }
    public int getRouteTaskCount(String room, int sid,  int routeId)throws RemoteException{
    	return getServer(room).getRouteTaskCount(sid, routeId);
    }
    public int updateStatusOfOutOfTskUsrs(String room, int sid,  int routeMasterId, int taskSeq, String status)throws RemoteException{
    	return getServer(room).updateStatusOfOutOfTskUsrs(sid, routeMasterId, taskSeq, status);
    }
    public Vector getDRSPrincipals(String room, int sid, int type, String groupName, Vector princ)
    throws RemoteException
	{ 
		activeError = getServer(room).getDRSPrincipals(sid, type, groupName, princ); 
		return princ;
	}
    
    public Vector getRouteCompletedDocuments(String room, int sid, int routeId, int adminUser) throws RemoteException{
 	   Vector routeCompletedDocsList = new Vector();
 	   activeError = getServer(room).getRouteCompletedDocuments(sid, routeId, adminUser, routeCompletedDocsList);
 	   return routeCompletedDocsList;
    }
    
    //This method return documents which is completed by all task and waiting for process final Action
    public Vector getTaskCompletedDocuments(String room, int sid, int routeId) throws RemoteException{
  	   Vector taskCompletedDocsList = new Vector();
  	   activeError = getServer(room).getTaskCompletedDocuments(sid, routeId, taskCompletedDocsList);
  	   return taskCompletedDocsList;
     }
    
    public Vector checkGroupsInRoute(String room, int sid) throws RemoteException {
	Vector result = new Vector();
	activeError = getServer(room).checkGroupsInRoute(sid, result);
	return result;	
    }

    public int syncRouteGroups(String room, int sid, String processFlag) throws RemoteException {
	activeError = getServer(room).syncRouteGroups(sid, processFlag);
	return activeError;
    }
   
   //For Invalid Route 
   public int isRouteValid(String room, int sid, int routeId)  throws RemoteException{
	   return getServer(room).isRouteValid(sid, routeId);
   }
    public int syncInvalidRouteUsers(String room, int sid, int routeId) throws RemoteException {
    	activeError = getServer(room).syncInvalidRouteUsers(sid, routeId);
    	return activeError;
	}
    
    public Vector getRouteInvalidTasks(String room, int sid, int routeId, Vector tasks) throws RemoteException {
    	activeError =  getServer(room).getRouteInvalidTasks(sid, routeId, tasks);
    	return tasks;
	}
    public Vector getEscalatedDocList(String room, int sid, Vector escalatedDocsList) throws RemoteException {
    	activeError =  getServer(room).getEscalatedDocList(sid, escalatedDocsList);
    	return escalatedDocsList;
	}
    public int setUserEscalation(String room, int sid, RouteMasterInfo routeMasterInfo) throws RemoteException {
		return activeError = getServer(room).setUserEscalation(sid, routeMasterInfo);
	}
	public int sendEscalationMail(String room, int sid, RouteMasterInfo routeMasterInfo, String esclationUserEmailId, String messageStr) throws RemoteException{
    	return activeError = getServer(room).sendEscalationMail(room, sid, routeMasterInfo, esclationUserEmailId, messageStr);
    }
    //-----
 /*End of DRS methods*/
    public boolean DBInsertPageText(int sid, String room, int did, int pno, int pid, int pvr,int qc,String localPath) throws RemoteException {	   
  	   getServer(room).DBInsertPageText(did, pno, pid, pvr, qc, localPath);
  	   return true;
    }
     
    public boolean isFTSEnabled(int sid, String room, String doctypeID) throws RemoteException {	   
   	   return getServer(room).isFTSEnabled(sid, doctypeID);
    }
    //  DL enhancement methods. Valli 8 May 2007
    public int setCustomisedIndexList(int hitList, int sid, String room, int nodeId, int rowCount, String customIndexList)throws RemoteException{
    	activeError = getServer(room).setCustomisedIndexList(hitList, sid, nodeId, rowCount, customIndexList);
    	return activeError;
    }
    public Vector getCustomisedIndexValues(int sid, String room, int nodeId, int rowCount, int refNid, String customIndexList, Vector result)throws RemoteException{
    	activeError =  getServer(room).getCustomisedIndexValues(sid, nodeId, rowCount, refNid, customIndexList, result);
    	return result;
    }
    public Vector getCustomisedSearchResult(String room, int sid,  String customIndexList, Search srch, Vector docs)throws RemoteException{
    	activeError = getServer(room).getCustomisedSearchResult(sid, customIndexList, srch, docs);
    	return docs;
    }
//  Read/Write the customised settings information to the Database
    public int addCustomList(int sid, String room, String userName, int objectType, String settingsId, String settingsValue, int nodeId)throws RemoteException{
    	getServer(room).addCustomList(sid, room, userName, objectType, settingsId, settingsValue, nodeId);
    	return activeError;
    }
    public Vector getCustomList(int sid, String room, String userName, int objectType, Vector settingsValues, int nodeId)throws RemoteException{
    	activeError = getServer(room).getCustomList(sid, room, userName, objectType, settingsValues, nodeId);
    	return settingsValues;
    }
    public int isCustomListInherited(int sid, String room, int objectType, int nodeId)throws RemoteException{
	int result = 0;
    	result = getServer(room).isCustomListInherited(sid, room, objectType, nodeId);
    	return result;
	
    }
    public int deleteCustomList(int sid, String room, String userName, int mode, int nodeId)throws RemoteException{
    	getServer(room).deleteCustomList(sid, room, userName, mode, nodeId);
    	return activeError;
    }
    //Following 3 methods are for another room node short cut enhancement.
    public int nodeShortCutAdd(int sid, String room, int parentNodeId, String nodeName, String description){
    	getServer(room).nodeShortCutAdd(sid, parentNodeId, nodeName, description);
       	return activeError;
    }
    
    public int nodeShortCutDelete(int sid,  String room,  int nodeId){
    	getServer(room).nodeShortCutDelete(sid, nodeId);
    	return activeError;
    }
    public Vector getNodeShortCutDesc(int sid,  String room, int nodeId){
    	Vector nodeShortCutDesc = new Vector();
    	getServer(room).getNodeShortCutDesc(sid, nodeId, nodeShortCutDesc);
    	return nodeShortCutDesc;
    }

// Add Methods - default properties for node 
    public int updateNodeRules(String room, int sid, NodeIndexRules nodeIndexRules) throws RemoteException{
    	getServer(room).updateNodeRules(sid, nodeIndexRules);
    	return activeError;
    }
    public Vector getNodeRules(String room, int sid, int nodeId) throws RemoteException{
    	Vector indexRules = new Vector();
    	getServer(room).getNodeRules(sid, nodeId, indexRules);
    	return indexRules;
    }
    public Vector checkNodeIndexRules(String room, int sid, int nodeId) throws RemoteException{
    	Vector indexRules = new Vector();
    	getServer(room).checkNodeIndexRules(sid, nodeId, indexRules);
    	return indexRules;
    }
    public Vector checkNodeTreeIndexRules(String room, int sid, int nodeId) throws RemoteException{
    	Vector indexRules = new Vector();
    	getServer(room).checkNodeTreeIndexRules(sid, nodeId, indexRules);
    	return indexRules;
    }
    public int deleteNodeRules(String room, int sid, int nodeId) throws RemoteException{
    	getServer(room).deleteNodeRules(sid, nodeId);
      	VWSLog.dbgCheck("activeError inside  deleteNodeRules::::: "+ activeError);
    	return activeError;
    }
    public Vector getAllDocTypeIndices(String room, int sid, int nodeId,
			Vector indices) throws RemoteException {
		activeError = getServer(room).getAllDocTypeIndices(sid, nodeId, indices);
	  	VWSLog.dbgCheck("activeError inside  getAllDocTypeIndices::::: "+ activeError);
		return indices;
	}

    public Vector getDocTypeIndicesWithRules(String room, int sid, int nodeId, int docTypeId,
			Vector indices) throws RemoteException {
		activeError = getServer(room).getDocTypeIndicesWithRules(sid, nodeId, docTypeId, indices);
    	VWSLog.dbgCheck("activeError inside  getDocTypeIndicesWithRules::::: "+ activeError);
		return indices;
	}
//Method for enhancement for creating the template for security
    public Vector getNodeContentsWithAcl(String room, int sid, int nid, int coid,
            Vector nodes, int sort) throws RemoteException
    {
    	activeError = getServer(room).getNodeContentsWithAcl(sid, nid, coid, nodes, sort);
    	VWSLog.dbgCheck("activeError inside  getNodeContentsWithAcl::::: "+ activeError);
    	return nodes;
    }
    public int copyNodeSecurity(String room, int sid, int nid, String ip, String userGroup, String permission) throws RemoteException
    {
    	return getServer(room).copyNodeSecurity(sid, nid, ip, userGroup, permission);
    	
    }    
    
	//Desc   :Added method 'getFTSEnabledPages()' which will give the FTS Enabled Pages list.
    public Vector getFTSEnabledPages(String room, int sid, int nodeId, Vector vecPages) throws RemoteException{
    	int activeError = getServer(room).getFTSEnabledPages(room, sid, nodeId, vecPages);
    	VWSLog.dbgCheck("activeError inside  getFTSEnabledPages::::: "+ activeError);
    	return vecPages;
    }
    public Vector getKeyIndexName(String room, int sid, int docTypeId, Vector vecKeyIndexName) throws RemoteException{
    	activeError = getServer(room).getKeyIndexName(room, sid, docTypeId, vecKeyIndexName);
    	VWSLog.dbgCheck("activeError inside  getExternalData::::: "+ activeError);
    	return vecKeyIndexName;
    }
    public int getMessageIDExpiry(String room) throws RemoteException{
    	return VWSPreferences.getMessageIDExpiry(room);
    }
    public void setMessageIDExpiry(String room, int hours) throws RemoteException{
    	VWSPreferences.setMessageIDExpiry(room, hours);
    }
    public int isUserExistsInRoute(String room, int sid, int userId)throws RemoteException{
    	return getServer(room).isUserExistsInRoute(sid, userId);
    }
    public boolean isDSSLocationExists(ServerSchema dssSchema, String location) throws RemoteException
    {
	DSS ds = (DSS) Util.getServer(dssSchema);    		
	if (ds == null) return false;
	return ds.isDSSLocationExists(location); 
    }
    public int setDBLookup(String room, int sid, DBLookup lookupInfo)throws RemoteException{
    	return getServer(room).setDBLookup(sid, lookupInfo);
    }
    
    public Vector getDSNInfo(String room, int sid, int docTypeId, int indexId, int dsnType)throws RemoteException{	
    	return getServer(room).getDSNInfo(sid, docTypeId, indexId, dsnType);
    }
/*    public int setIndexMapping(String room, int sid, DocType dt, int triggerDataType)throws RemoteException{
    	return getServer(room).setIndexMapping(sid, dt, triggerDataType);
    }*/
    public Vector getIndexMapping(String room, int sid, DocType dt)throws RemoteException{
	Vector result = new Vector();
    	getServer(room).getIndexMapping(sid, dt, result);    	
    	return result;
    }
    public Vector getExternalData(String room, int sid, DocType dt) throws RemoteException {
	Vector output = new Vector();
	activeError = getServer(room).getExternalData(sid, dt, output);
	VWSLog.dbgCheck("activeError inside  getExternalData::::: "+ activeError);
	output.add(dt);
	output.add(activeError);
	return output;
    }
    public Vector getExternalLookupColumns(String room, int sid, String dsnName, String userName, String passWord, String externalTable, String query) throws RemoteException {	
	Vector columnList = getServer(room).getExternalLookupColumns(sid, dsnName, userName, passWord, externalTable, query);	
	return columnList;
    }
    public Vector getFolders(String room, int sid, String folderName) throws RemoteException {
	Vector folderList = new Vector();
	activeError = getServer(room).getFolders(sid, folderName, folderList);	
	VWSLog.dbgCheck("activeError inside  getFolders::::: "+ activeError);
	return folderList;
	}
    
    /**CV2019 merges from SIDBI line***/
    public Vector searchNodePath(String room, int sid, String folderName,String folderPath) throws RemoteException {
    	Vector folderList = new Vector();
    	activeError = getServer(room).searchNodePath(sid, folderName,folderPath, folderList);	
    	VWSLog.dbgCheck("activeError inside  getFolders::::: "+ activeError);
    	return folderList;
    }
    
    /*Enhancement for custom color*/
    public Vector getNodeRGBColor(String room, int sid) throws RemoteException {
    Vector result = new Vector();
	int columnColor = getServer(room).getNodeRGBColor(sid, result);	
	return result;
    }
    public int setNodeCustomColor(String room, int sid, int nodeId, int colorId, boolean type, boolean inherit, int scope)throws RemoteException{
	int columnColor = getServer(room).setNodeCustomColor(sid, nodeId, colorId, type, inherit, scope);	
	return columnColor;
    }
   public Vector getNodeCustomColorCode(String room, int sid, int nodeId)throws RemoteException{
    	Vector result = new Vector();
    	int columnColor = getServer(room).getNodeCustomColorCode(sid, nodeId, result);
    	return result; 
    	//else return "";
    }
   public void deleteNodeCustomColorCode(String room, int sid, int nodeId)throws RemoteException{
	   getServer(room).deleteNodeCustomColorCode(sid, nodeId);
   }
	/**
     * 
     */
    public Vector getAvailableRetentions(String room, int sid, Vector availRetention) throws RemoteException 
	{
		activeError = getServer(room).getAvailableRetentions(sid, availRetention);
		VWSLog.dbgCheck("activeError inside  getAvailableRetentions::::: "+ activeError);
		return availRetention;
	}
    public Vector getRetentionSettings(String room, int sid, int retentionId,  Vector availRetention) throws RemoteException 
	{
		activeError = getServer(room).getRetentionSettings(sid, retentionId, availRetention);
		VWSLog.dbgCheck("activeError inside  getRetentionSettings::::: "+ activeError);
		return availRetention;
	}
    public Vector getRetentionIndexInfo(String room, int sid, int retentionId,  Vector availRetention) throws RemoteException 
	{
		activeError = getServer(room).getRetentionIndexInfo(sid, retentionId, availRetention);
 		VWSLog.dbgCheck("activeError inside  getRetentionIndexInfo::::: "+ activeError);
		return availRetention;
	}
    public int setRetentionSettings(String room, int sid, VWRetention retention, Vector retentionId) throws RemoteException 
	{
    	return getServer(room).setRetentionSettings(sid, retention, retentionId);
	}
    public int setIndexConditions(String room, int sid, VWRetention retention) throws RemoteException 
	{
    	return getServer(room).setIndexConditions(sid, retention);
	}
    
    public int delRetentionSettings(String room, int sid, VWRetention retention, int deleteOption) throws RemoteException 
	{
    	return getServer(room).delRetentionSettings(sid, retention, deleteOption);
	}
    
    public int delIndexInfo(String room, int sid, VWRetention retention) throws RemoteException 
	{
    	return getServer(room).delIndexInfo(sid, retention);
	}
    public int checkRetentionInUse(String room, int sid, int retentionId, Vector retention) throws RemoteException 
	{
    	return getServer(room).checkRetentionInUse(sid, retentionId, retention);
	}
    public int isRetentionNameFound(String room, int sid, String RetentionName) throws RemoteException {	   
 	   activeError = getServer(room).isRetentionNameFound(sid, RetentionName);
 		VWSLog.dbgCheck("activeError inside  isRetentionNameFound::::: "+ activeError);
 	   return activeError;
    }
    public Vector getRetentionDocumentList(String room, int sid, int retentionId, Vector retentionDocs) throws RemoteException 
	{
		activeError = getServer(room).getRetentionDocumentList(sid, retentionId, retentionDocs);
	 	VWSLog.dbgCheck("activeError inside  getRetentionDocumentList::::: "+ activeError);
		return retentionDocs;
	}
    public Vector getRetentionCompletedDocs(String room, int sid, int retentionId, Vector retentionDocs) throws RemoteException 
	{
		activeError = getServer(room).getRetentionCompletedDocs(sid, retentionId, retentionDocs);
	 	VWSLog.dbgCheck("activeError inside  getRetentionCompletedDocs::::: "+ activeError);
		return retentionDocs;
	}
    public Vector getNotifyDocumentList(String room, int sid, Vector notifyDocs) throws RemoteException{
    	activeError = getServer(room).getNotifyDocumentList(sid, notifyDocs);
    	VWSLog.dbgCheck("activeError inside  getNotifyDocumentList::::: "+ activeError);
		return notifyDocs;
    }
    public int addNotifiedDocs(String room,int sid, int retentionId, int docID, int docTypeId )throws RemoteException{
      	return getServer(room).addNotifiedDocs(sid, retentionId, docID, docTypeId);
    }
    public int sendNotification(String room, int sid, int VWSCount, String message) throws RemoteException{
    	return getServer(room).sendNotification(room, sid, VWSCount, message);
    }
    
    public int sendNotififyDocMail(String room, int sid, int VWSCount, String message,Vector mailIds) throws RemoteException{
    	return getServer(room).sendNotififyDocMail( sid, VWSCount,message,mailIds);
    }
    
    
	public Vector getGroupMembers(String room, int sid, String groupId) throws RemoteException {	
    	Vector columnList = getServer(room).getGroupMembers(sid, groupId);	
    	return columnList;
    }
    public String getLoggedInUser(String room, int sid) throws RemoteException {
	return getServer(room).getLoggedInUser(sid);
    }
    
    public int deleteEmptyUsers(String room) throws RemoteException{
    	int delEmptyUsers = getServer(room).deleteEmptyUsers();
    	return delEmptyUsers;
    }
    public int isDSSActive(String room, int sid) throws RemoteException{
    	int ret = getServer(room).isDSSActive(room, sid);
    	return ret;
    }
    public boolean getPurgeDocs(String room, int sid, Vector purgeDocsFlag) throws RemoteException{
    	boolean ret = getServer(room).getPurgeDocs(room, sid, purgeDocsFlag);
    	return ret;
    }
    
    public boolean getKeepDocType(String room, int sid, Vector keepdoctypeFlag) throws RemoteException{
    	boolean ret = getServer(room).getKeepDocType(room, sid, keepdoctypeFlag);
    	return ret;
    }
    
    public int setPurgeDocs(String room, int sid, String purgeDocsFlag) throws RemoteException{
    	int ret = getServer(room).setPurgeDocs(room, sid, purgeDocsFlag);
    	return ret;
    }
    public int setKeepDocType(String room, int sid, String keepDocTypeFlag) throws RemoteException{
    	int ret = getServer(room).setKeepDocType(room, sid, keepDocTypeFlag);
    	return ret;
    }
    public Vector searchByIndex(String room, int sid, String docTypeName, String indexName, String indexValue, int conditionType, int includeCustom) throws RemoteException{
    	Vector documents = new Vector();
    	activeError = getServer(room).searchByIndex(sid, docTypeName, indexName, indexValue, conditionType, includeCustom, documents);
    	VWSLog.dbgCheck("activeError inside  searchByIndex::::: "+ activeError);
    	return documents;
    }
    public Vector getDocumentMetaDataInfo(String room, int sid, String docId, Vector documentInfo) throws RemoteException{
    	activeError = getServer(room).getDocumentMetaDataInfo(sid, docId, documentInfo);
    	VWSLog.dbgCheck("activeError inside  getDocumentMetaDataInfo::::: "+ activeError);
    	return documentInfo;
    }
    
    public int revokeDocSignature(String room, int sid, int documentId) throws RemoteException{
    	int ret = getServer(room).revokeDocSignature(sid, documentId);
    	return ret;
    }
    
    public int getARSServer(String room, int sid) throws RemoteException{
    	int ret = getServer(room).getARSServer(room, sid);
    	return ret;
    }
    public Vector getTreeDetails(String room, int sid) {
    	Vector treeDetails = new Vector();
    	activeError = getServer(room).getTreeDetails(sid, treeDetails );
    	VWSLog.dbgCheck("activeError inside  getTreeDetails::::: "+ activeError);
    	return treeDetails;
    }

    public int saveNotificationSettings(String room, int sid, NotificationSettings settings)throws RemoteException{
    	activeError = getServer(room).saveNotificationSettings(sid, settings );
    	VWSLog.dbgCheck("activeError inside  saveNotificationSettings::::: "+ activeError);
    	return activeError;
    }
    public Vector loadNotificationSettings(String room, int sid, int nodeId, int isAdmin, String userName, String module)throws RemoteException{
    	Vector settings = new Vector();
    	activeError = getServer(room).loadNotificationSettings(sid, nodeId, isAdmin, userName, module, settings);
    	VWSLog.dbgCheck("activeError inside  loadNotificationSettings::::: "+ activeError);
    	return settings;
    }
    public int sendNotificationAlert(String room, int sid, Vector mailIds, Vector mailMessage, Vector attachmentData, int status, int nodeType, int docId, int attachmentExist, Vector nodeName, int attempt) throws RemoteException{
    	return getServer(room).sendNotificationAlert(room, sid, mailIds, mailMessage, attachmentData, status, nodeType, docId, attachmentExist, nodeName, attempt);
    }
    public Vector readNotificationDetails(String room, int sid, String failedFlag, String notificationRetries) throws RemoteException{
    	Vector notificationHistory = new Vector();
    	activeError = getServer(room).readNotificationDetails(sid, failedFlag, notificationRetries, notificationHistory);
    	VWSLog.dbgCheck("activeError inside  readNotificationDetails::::: "+ activeError);
    	return notificationHistory;
    }
    public int updateNotificationHistory(String room, int sid, int sentFlag,int failedFlag, int historyCount, String historyIds) throws RemoteException{
    	return getServer(room).updateNotificationHistory(sid, sentFlag, failedFlag, historyCount, historyIds);
	}
    public Vector getLoggedInUserMailId(String room, int sid) throws RemoteException{
    	Vector loggedUserMailId = new Vector();
    	activeError = getServer(room).getLoggedInUserMailId(sid, loggedUserMailId);
     	VWSLog.dbgCheck("activeError inside  getLoggedInUserMailId::::: "+ activeError);
    	return loggedUserMailId;
    }
    public int deleteNotificationSettings(String room, int sid, NotificationSettings settings) throws RemoteException{
    	return getServer(room).deleteNotificationSettings(sid, settings);
    }
    public int addNotificationHistory(String room, int sid, int nodeId, int nodeType, int notifyId, String userName, String oldValue, String newValue, String message)throws RemoteException{
    	return activeError = getServer(room).addNotificationHistory(sid, nodeId, nodeType, notifyId, userName, oldValue, newValue, message);
    }	
    public int setNotificationHistory(String room, int sid, int folderId, int nodeId, int nodeType, int settingsId, String oldValue, String newValue, String modifiedUser, String message)throws RemoteException{
    	return activeError = getServer(room).setNotificationHistory(sid, folderId, nodeId, nodeType, settingsId, oldValue, newValue, modifiedUser, message);
    }
    public Node getNodePropertiesByNodeID(String room, int sid, Node node)throws RemoteException{
    	activeError = getServer(room).getNodePropertiesByNodeID(sid, node);
     	VWSLog.dbgCheck("activeError inside  getNodePropertiesByNodeID::::: "+ activeError);
    	return node;
    }
    public int CheckNotificationExist(String room, int sid, int nodeId, int nodeType, int notifyId, String userName)throws RemoteException{
    	return activeError = getServer(room).CheckNotificationExist(sid, nodeId, nodeType, notifyId, userName);
    }
    public Vector getNotificationMasters(String room, int sid, int option, String module)throws RemoteException{
    	Vector masterTable = new Vector();
    	activeError = getServer(room).getNotificationMasters(sid, option, module, masterTable);
    	VWSLog.dbgCheck("activeError inside  getNotificationMasters::::: "+ activeError);
    	return masterTable;
    }
    public Vector getNotificationList(String room, int sid)throws RemoteException{
    	Vector masterTable = new Vector();
    	activeError = getServer(room).getNotificationList(sid, masterTable);
    	VWSLog.dbgCheck("activeError inside  getNotificationList::::: "+ activeError);
    	return masterTable;
    }
    public int setActiveClient(String room, int sid) throws RemoteException{
    	boolean flag = getServer(room).isClientConnected(sid, false);
    	int ret = (flag==true? 1 : 0);
    	return ret; 
    }
    public Vector getSignSettings(String room, int sid) throws RemoteException 
    {
    	Vector signSettings = new Vector();
    	activeError = getServer(room).getSignSettings(sid, signSettings);
    	VWSLog.dbgCheck("activeError inside  getSignSettings::::: "+ activeError);
    	return signSettings;
    }
	public Vector getSignTitle(String room, int sid) throws RemoteException {
		Vector result = new Vector();
    	activeError = getServer(room).getSignTitle(sid, result);
    	VWSLog.dbgCheck("activeError inside  getSignTitle::::: "+ activeError);
    	return result;
	}
	public int updateIndexSelection(String room, int sid, Index index, Vector selValues, String clientMode) throws RemoteException {
		return getServer(room).updateIndexSelection(sid, index, selValues, clientMode);
	}
	public int getIndexId(String room, int sid, Index index) throws RemoteException {
		return getServer(room).getIndexId(sid, index);
	}
	public Vector getNodeContentsWS(String room, int sid, int nid, int coid, Vector nodes, int sort) throws RemoteException {
		activeError = getServer(room).getNodeContentsWS(sid, nid, coid, nodes, 0);
		VWSLog.dbgCheck("activeError inside  getNodeContentsWS::::: "+ activeError);
    	return nodes;
	}

	public Vector getOptionSettings(String room, int sid, Vector output) throws RemoteException {
		activeError = getServer(room).getOptionSettings(sid, output);
		VWSLog.dbgCheck("activeError inside  getOptionSettings::::: "+ activeError);
    	return output;
	}
	
	public int setOptionSettings(String room, int sid, String output) throws RemoteException {
		activeError = getServer(room).setOptionSettings(sid, output);
		VWSLog.dbgCheck("activeError inside  setOptionSettings::::: "+ activeError);
    	return activeError;
	}
	public int getTaskApproversCount(String room , int sid, int routeId, int currentTaskSequence, int docId) throws RemoteException {
		return getServer(room).getTaskApproversCount(sid, routeId, currentTaskSequence, docId);    	
	}
	public Vector getWebAccessUrl(String room, int sid) throws RemoteException {
		Vector result = new Vector();
		activeError = getServer(room).getWebAccessUrl(sid, result);
		VWSLog.dbgCheck("activeError inside  getWebAccessUrl::::: "+ activeError);
		return result;
	}	
	
	public Vector validateRouteDocument(String room, int sid, int routeId, int docId) throws RemoteException{
		Vector resultVector = new Vector();
		activeError = getServer(room).validateRouteDocument(sid, routeId, docId, resultVector);
		VWSLog.dbgCheck("activeError inside  validateRouteDocument::::: "+ activeError);
		return resultVector;
	}
	public Vector getDocIndicesInRoute(String room, int sid, int docid) throws RemoteException {
		Vector routeIndices = new Vector();
		activeError = getServer(room).getDocIndicesInRoute(sid,docid,routeIndices);
		VWSLog.dbgCheck("activeError inside  getDocIndicesInRoute::::: "+ activeError);
		return routeIndices;
	}

	public int changeUserPWD(String room, int sid, Principal principal, String oldPassword, String newPassword, String confirmPassword){
		return getServer(room).changeUserPWD(sid, principal, oldPassword, newPassword, confirmPassword);
	}
	
	public int resetUserPWD(String room, int sid, Principal principal, String oldPassword, String newPassword, String confirmPassword){
		return getServer(room).resetUserPWD(sid, principal, oldPassword, newPassword, confirmPassword);
	}
	
	public boolean getHostedAdmin(int sid) throws RemoteException {
		return VWSPreferences.getHostedAdmin();
	}
	/* Method checkKeyIndex is added to do the procedure call 
	 * If the index value is empty it returns the index name 
	 * otherwise returns zero.
	 * Implemented for the document key field change. 
	 * Release :Build5-001
	 * Added By Madhavan
	 */
	public Vector checkKeyIndex(String room, int sid, DocType docType, Vector indiceslist, int count) throws RemoteException {
		Vector result = new Vector();
		activeError = getServer(room).checkKeyIndex(sid, docType, indiceslist, count,result);
		VWSLog.dbgCheck("activeError inside  checkKeyIndex::::: "+ activeError);
		return result;
	}
	/*
	 * updateDocTypeKeyField to set the current key field.
	 * Implemented for the document key field change.
	 * Release :Build5-001
	 * Added By Madhavan
	 */
	public int updateDocTypeKeyField(String room, int sid, int oldKeyIndexId,  int newKeyIndexId, DocType docType) {		
		return getServer(room).updateDocTypeKeyField(sid, oldKeyIndexId, newKeyIndexId, docType);
	}
	/*
	 * Implemented to check whether the index field mathes in the Backup set and Restoring room,
	 * Before Restoration. Gurumurthy.T.S
	 * Bug 206 CV8B5-002
	 */
	public Vector restoreIndicesCheck(String room, int sid,DocType docType, int dtindexCount,
			Vector restoreIndiceCheck) throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).restoreIndicesCheck(sid,docType,dtindexCount,restoreIndiceCheck,result);
		VWSLog.dbgCheck("activeError inside  restoreIndicesCheck::::: "+ activeError);
		return result;
	}
	/**
	 * Method added for enhancement of Create ownership, Sachin Patil , 26-02-2014
	 */
	public Vector createOwnership(String room, int sid, String sessionName, int type, Vector nodes) throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).createOwnership(sid, sessionName, type, nodes,result);
		VWSLog.dbgCheck("activeError inside  createOwnership::::: "+ activeError);
		return result;
	}

	/**
	 * Method added for enhancement of Release ownership, Sachin Patil , 26-02-2014
	 */
	public int releaseOwnership(String room, int sid, Node node, int type)throws RemoteException {
		
		return getServer(room).releaseOwnership(sid, node, type);
	}
	/**
	 * Method added for enhancement of Check ownership, Sachin Patil , 26-02-2014
	 */
	public Vector checkOwnership(String room,int sid, int type, Vector nodes) throws RemoteException {
		
		return getServer(room).checkOwnership(sid, type, nodes);
	}
	public int ForceFTS(String room, int sid, int DocId)throws RemoteException {
		getServer(room).ForceFTS(sid, DocId);
		return NoError;
	}
	public Vector getLatestDocId(String room,int sid)throws RemoteException {
		return getServer(room).getLatestDocId(sid);
	}
	public Vector getChildNodes(String room, int sid, int nodeid)throws RemoteException {
		return getServer(room).getChildNodes(sid,nodeid);
	}
	public Vector getDocumentVersions(String room, int sid, int did,String version)throws RemoteException{
		return getServer(room).getDocumentVersions(sid, did,version);
	}
	public int CheckLockDocTypePerm(String room, int sid, int nodeid)throws RemoteException {
		return getServer(room).CheckLockDocTypePerm(sid,nodeid);
	}
	public Vector getDocumentTypes(String room, int sid, int indexId,Vector indices) throws RemoteException {
		activeError = getServer(room).getDocumentTypes(sid, indexId, indices);
		VWSLog.dbgCheck("activeError inside  getDocumentTypes::::: "+ activeError);
		return indices;
	}
	public Vector getDocumentTypeList(int sid, String room, Vector settingsDocTypeValues, int nodeId)throws RemoteException {
		activeError = getServer(room).getDocumentTypeList(sid,settingsDocTypeValues, nodeId);
		VWSLog.dbgCheck("activeError inside  getDocumentTypeList::::: "+ activeError);
		return settingsDocTypeValues;
	}
	public Vector addDocumentTypeList(int sid, String room, String userName,String settingsValue, int nodeId)throws RemoteException {
		return getServer(room).addDocumentTypeList(sid, room, userName, settingsValue, nodeId);
	}
	public int resetDocumentTypeList(int sid, String room, int nodeId)throws RemoteException {
		activeError=getServer(room).resetDocumentTypeList(sid, room, nodeId);
		VWSLog.dbgCheck("activeError inside  resetDocumentTypeList::::: "+ activeError);
		return activeError;
	}
	public Vector VWNodeGetDocTypes(int sid, String room, int nodeId,Vector documentTypes) throws RemoteException {
		activeError = getServer(room).VWNodeGetDocTypes(sid, nodeId, documentTypes);
		VWSLog.dbgCheck("activeError inside  VWNodeGetDocTypes::::: "+ activeError);
		return documentTypes;
	}
	public Vector ValidateNodeDocType(int sid, String room, int flag,int sourceNodeId, int destinationNodeId) throws RemoteException {
		Vector result = new Vector();
		activeError=getServer(room).ValidateNodeDocType(sid, room, flag,sourceNodeId,destinationNodeId,result);
		VWSLog.dbgCheck("activeError inside  ValidateNodeDocType::::: "+ activeError);
		return result;
	}
	public Vector getRouteUsers(String room, int sid, Vector creators)throws RemoteException
	{ 
		activeError = getServer(room).getRouteUsers(sid, creators); 
		VWSLog.dbgCheck("activeError inside  drsValidateTaskDocument::::: "+ activeError);
		return creators;
	}
	public Vector drsValidateTaskDocument(String room, int sid,int docId,int routeId,int action,String routeTaskSequence)throws RemoteException
	{  Vector result = new Vector();
		activeError = getServer(room).drsValidateTaskDocument(sid,docId,routeId,action,routeTaskSequence, result); 
		VWSLog.dbgCheck("activeError inside  drsValidateTaskDocument::::: "+ activeError);
		return result;
	}
	public Vector getTaskIndicesInRoute(String room, int sid, int docid) throws RemoteException {
		Vector taskIndices = new Vector();
		activeError = getServer(room).getTaskIndicesInRoute(sid,docid,taskIndices);
		VWSLog.dbgCheck("activeError inside  getTaskIndicesInRoute::::: "+ activeError);
		return taskIndices;
	}

	public Vector  getArsValidationFinalAction(String room,int sid,int docId, String retentionId)throws RemoteException {
		Vector result=new Vector();
		 activeError = getServer(room).getArsValidationFinalAction(sid,docId,retentionId,result);
			VWSLog.dbgCheck("activeError inside  getArsValidationFinalAction::::: "+ activeError);
         return result;
	}
	public Vector getUserNameInOrder(String room,int sid, String previousValue,
			String currentValue) throws RemoteException {
		Vector result=new Vector();
		 activeError = getServer(room).getUserNameInOrder(sid,previousValue,currentValue,result);
			VWSLog.dbgCheck("activeError inside  getUserNameInOrder::::: "+ activeError);
        return result;
	}
	public int checkUser(String room,int sid,  String userName)throws RemoteException {
		return getServer(room).checkUser(sid, userName);
	}
	public int checkGroup(String room,int sid,  String groupName)throws RemoteException {
		return getServer(room).checkGroup(sid, groupName);
	}
	@Override
	public Vector checkValidateSeqIndex(String room, int sid, int indexid) throws RemoteException {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError=getServer(room).checkValidateSeqIndex(sid,indexid,result);
		VWSLog.dbgCheck("activeError inside  checkValidateSeqIndex::::: "+ activeError);
		return result;
	}
	public Vector checkValidateUniqueSeq(String room, int sid, int doctypeId) throws RemoteException {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError=getServer(room).checkValidateUniqueSeq(sid,doctypeId,result);
		VWSLog.dbgCheck("activeError inside  checkValidateUniqueSeq::::: "+ activeError);
		return result;
	}
	
	public Vector checkValidateCopySeq(String room, int sid, Node node,
			int nodeProperties) throws RemoteException {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError=getServer(room).checkValidateCopySeq(sid,node,nodeProperties,result);
		VWSLog.dbgCheck("activeError inside  checkValidateCopySeq::::: "+ activeError);
		return result;
	}
	

	@Override
	public Vector resubmitDoctoWorkFlow(String room,int sid, String doc)
			throws RemoteException {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError=getServer(room).resubmitDoctoWorkFlow(sid,doc,result);
		VWSLog.dbgCheck("activeError inside  resubmitDoctoWorkFlow::::: "+ activeError);
		return result;
	}
	@Override
	public Vector showhideDoctoWorkFlow(String room,int sid, String doc)throws RemoteException {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError=getServer(room).showhideDoctoWorkFlow(sid,doc,result);
	     VWSLog.dbgCheck("activeError inside  showhideDoctoWorkFlow::::: "+ activeError);
		return result;
	}
	public int getEnableSelect(String room,int sid,String registryValue)throws RemoteException {
		return getServer(room).getEnableSelect(sid,registryValue);
	}

	public Vector getAllCabinets(String room, int sid)throws RemoteException {
		return getServer(room).getAllCabinets(sid);
	}
	public Vector getFaxDocType(String room, int sid)throws RemoteException {
		return getServer(room).getFaxDocType(sid);
	}
	@Override
	public Vector getTodoListResubmit(String room, int sid, String doc,
			int routeHistoryId) throws RemoteException {
		
		Vector result = new Vector();
		//VWSLog.add("activeError inside  setRouteStatusResubmit::::: "+ activeError);
		//VWSLog.dbg("activeError inside  setRouteStatusResubmit::::: "+ activeError);
		activeError=getServer(room).getTodoListResubmit(sid,doc,routeHistoryId,result);

	return result;
		
	}
	@Override
	public Vector getDRSValidateUpdateRoute(String room, int sid, int routeId,String routeName)
			throws RemoteException {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).getDRSValidateUpdateRoute(sid, routeId,routeName,result);
		 return result;
	}
	public Vector getDWSMaintenanceFlag(String room, int sid)throws RemoteException{
		Vector result = new Vector();
		activeError= getServer(room).getDWSMaintenanceFlag(sid,result);
		return result;
	}
	@Override
	public Vector getDRSReviewUpdWorkflow(String room, int sid)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).getDRSReviewUpdWorkflow(sid,result);
		 return result;
	}
	@Override
	public Vector getDRSGetReviewUpdFlag(String room, int sid)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).getDRSGetReviewUpdFlag(sid,result);
		 return result;
	}
	@Override
	public Vector getDRSGetWorkflowStatus(String room, int sid)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).getDRSGetWorkflowStatus(sid,result);
		 return result;
	}
	@Override
	public Vector workFlowUpdateSendEmail(String room, int sid)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).workFlowUpdateSendEmail(sid,result);
		 return result;
	}
	@Override
	public Vector delUpdateWorkFlow(String room, int sid,int refId)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).delUpdateWorkFlow(sid,refId,result);
		 return result;
	}
	@Override
	public Vector getMailId(String room,int sid, String userName)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).getMailId(sid,userName,result);
		 return result;
	}
	@Override
	public Vector generateNodeSecurityReport(String room, int sid,String selectedPrincipal, int reportNodeId, int checkBoxValue)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).generateNodeSecurityReport(sid,selectedPrincipal,reportNodeId,checkBoxValue,result);
		 return result;
	}
	//Method added for document packaging currenlty not in use
	/*public Vector getDocPageCount(String room, int sid,int nodeId)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).getDocPageCount(sid,nodeId,result);
		 return result;
	}*/
	
	public Vector setDocstoNotify(String room, int sid,int nodeId,String userName)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).setDocstoNotify(sid,nodeId,userName,result);
		 return result;
	}
	
	public Vector getUsertoNotifyDocs(String room, int sid,int nodeId)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).getUsertoNotifyDocs(sid,nodeId,result);
		 return result;
	}
	

	public Vector delNotifiedDocs(String room, int sid,int nodeId)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).delNotifiedDocs(sid,nodeId,result);
		 return result;
	}
	public Vector setSubAdminRoles(String room,int sid,String user,String roles,int updFlag)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).setSubAdminRoles(sid,user,roles,updFlag);
		 return result;
	}
	
	public Vector getSubAdminRoles(String room,int sid,String user)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).getSubAdminRoles(sid,user,result);
		 return result;
	}
	public Vector removeSubAdminRoles(String room,int sid,String groupName)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).removeSubAdminRoles(sid, groupName);
		 return result;
	}
	public Vector checkSubAdmin(String room,int sid,String userName)throws RemoteException  {
		// TODO Auto-generated method stub
		Vector result = new Vector();
		activeError= getServer(room).checkSubAdmin(sid, userName,result);
		 return result;
	}
	@Override
	public Vector getSubAdminTabs(String room, int sid, String userName)
			throws RemoteException {
		// TODO Auto-generated method stub
				Vector result = new Vector();
				activeError= getServer(room).getSubAdminTabs(sid, userName,result);
				 return result;
	}
	
	public Vector getDRSValidateToDoList(String room, int sid, String userName,int nodeId,String receivedDate)
			throws RemoteException {
		// TODO Auto-generated method stub
				Vector result = new Vector();
				activeError= getServer(room).getDRSValidateToDoList(sid, userName,nodeId,receivedDate,result);
				 return result;
	}
	//CV10 Enhancment to Enable/Disable Backup/Restore tabs in adminwise
	public void  setTabStatus(String room,int tabStatus)throws RemoteException
	{
		VWSPreferences.setTabStatus(room, tabStatus);
	}

	public void setToggleStatus(String room, int togglestatus)
			throws RemoteException {
		// TODO Auto-generated method stub
		VWSPreferences.setToggleStatus(room, togglestatus);

	}
	public Vector getGetDocsInRouteColor(String room, int sid, int colortype)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).getGetDocsInRouteColor(sid, colortype,result);
		return result;
	}
	@Override
	public Vector getWorkFlowReportOptions(String room, int sid, int type,String routeId)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).getWorkFlowReportOptions(sid,type,routeId,result);
		return result;
	}
	@Override
	public Vector generateWFReport(String room, int sid, int mode,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).generateWFReport(sid,mode,availableIds,tasks,workFlowIds,taskIds,userGroupNames,date, result);
		return result;
	}
	public Vector saveWFReport(String room, int sid, int mode,String reportName,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date)
			throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).saveWFReport(sid,mode,reportName,availableIds,tasks,workFlowIds,taskIds,userGroupNames,date, result);
		return result;
	}
	@Override
	public Vector checkWFReport(String room,int sid,String reportName)throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).checkWFReport(sid,reportName,result);
		return result;
	}
	@Override
	public Vector getWFReport(String room,int sid,int flag,String reportName)throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).getWFReport(sid,flag,reportName,result);
		return result;
	}
	@Override
	public Vector getAgregateReport(String room, int sid, int mode,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date)throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).getAgregateReport(sid,mode,availableIds,tasks,workFlowIds,taskIds,userGroupNames,date, result);
		return result;
	}
	
	@Override
	public Vector deletWFReport(String room, int sid, String reportName)throws RemoteException {
		Vector result = new Vector();
		activeError= getServer(room).deleteWFReport(sid,reportName,result);
		return result;
	}
	
	public Vector getAzureStorageCredentials(String room,int sid,int docId)throws RemoteException  {
			Vector result = new Vector();
			activeError= getServer(room).getAzureStorageCredentials(sid,docId,result);
			return result;
	 }
	public Vector getAzureStore(ServerSchema ss, Vector stores)throws RemoteException  {
		activeError= getServer(ss.room).getAzureStore( ss, stores);
		return stores;
 }
	
	public Vector validateDynamicIndex(String room,int sid,String indexValue)throws RemoteException  {
		Vector result =new Vector();
		activeError= getServer(room).validateDynamicIndex( sid, indexValue,result);
		return result;
 }
	@Override
	public Vector getStorageFromId(ServerSchema ss,String storageId) throws RemoteException {
		Vector result =new Vector();
		activeError= getServer(ss.room).getStorageFromId(storageId,result);
		VWSLog.dbg("result in getStorageFromId :"+result);
		return result;
	}
	public Vector setAIPConnectionStatus(String room, int currSid)throws RemoteException {
		// TODO Auto-generated method stub
		VWSLog.add("currSid inside setAIPConnectionStatus :::::::"+currSid);
		int retVal=0;
		Vector result =new Vector();
		retVal= getServer(room).setAIPConnectionStatus(currSid,room);
		result.add(retVal);
		
		//VWSLog.dbg("result in getStorageFromId :"+result);
		return result;
	
		
	}
	public Vector setCVDTForm(String room,int sid,int docTypeId,String formName)throws RemoteException  {
		Vector result =new Vector();
		activeError= getServer(room).setCVDTForm(sid,docTypeId,formName,result);
		return result;
	}
	
	public Vector getCVDTForm(String room,int sid,int docTypeId)throws RemoteException  {
		Vector result =new Vector();
		activeError= getServer(room).getCVDTForm(sid,docTypeId,result);
		return result;
	}
	
	@Override
	public String getServerHome(String room, int sid) throws RemoteException {
		// TODO Auto-generated method stub
		return VWSUtil.getHome();
	}
	@Override
	public Vector checkTodoDocsToNotify(String room, int sid)
			throws RemoteException {
		// TODO Auto-generated method stub
		Vector result =new Vector();
		activeError= getServer(room).checkTodoDocsToNotify(sid,result);
		return result;
	}
	@Override
	public Vector getIndexInfoTemplate(String room,int sid,int retentionId)
			throws RemoteException {
		// TODO Auto-generated method stub
		Vector result =new Vector();
		activeError= getServer(room).getIndexInfoTemplate(sid,retentionId,result);
		return result;
	}
	@Override
	public Vector getRetentionDoctypes(String room,int sid,int retentionId)
			throws RemoteException {
		// TODO Auto-generated method stub
		
		Vector result =new Vector();
		activeError= getServer(room).getRetentionDoctypes(sid,retentionId,result);
		return result;
	}
	@Override
	public void checkUpdNotificationDocs(String room, int sid, int routeMasterId, int taskSequence)
			throws RemoteException {
		getServer(room).checkUpdNotificationDocs(sid, routeMasterId, taskSequence);
		
	}
    public int isRetentionDocTypeFound(String room,int sid, String docTypeName)throws RemoteException
    {
		// TODO Auto-generated method stub
		Vector result =new Vector();
		return getServer(room).isRetentionDocTypeFound(sid,docTypeName);
	
    }
    public int isRetentionDTIndexFound(String room,int sid, String docTypeId,String indexName)throws RemoteException
    {
		// TODO Auto-generated method stub
		Vector result =new Vector();
		return getServer(room).isRetentionDTIndexFound(sid,docTypeId,indexName);
	
    }
    public int checkCVSearchNode(String room,int sid, String nodePath)throws RemoteException
    {
		// TODO Auto-generated method stub
		Vector result =new Vector();
		return getServer(room).checkCVSearchNode(sid,nodePath);
	
    }

	public Vector cvCreateNodePath(String room, int sid, String panelName, Vector result)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).cvCreateNodePath(sid, panelName,result);
		return result;
	}
	

	public Document createCVReportsDocument(String room, int sid, int docTypeId, int parentId, int idxCount, String indices, Document doc, DocType dt, String panelName, int indexId, String path)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).createCVReportsDocument(sid, docTypeId, parentId, idxCount, indices, doc, dt, panelName,indexId, path);
		return doc;
	}
	
	public Vector CVSetDTFormTemplate(String room, int sid,int templatId, String templateName, String formName, String sourceLocation, String destinationLocation, Vector result)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).CVSetDTFormTemplate(sid,templatId,templateName, formName, sourceLocation, destinationLocation, result);
		return result;
	}
	
	public Vector CVGetDTFormTemplate(String room, int sid, Vector result)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).CVGetDTFormTemplate(sid, result);
		return result;
	}
	
	public Vector CVGetDTFormTemplateDetails(String room, int sid, String templateName, Vector result)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).CVGetDTFormTemplateDetails(sid, templateName, result);
		return result;
	}
	
	public Vector CVDelDTFormTemplate(String room, int sid, String templateName, Vector result)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).CVDelDTFormTemplate(sid, templateName, result);
		return result;
	}
	
	public Vector CVCheckForTemplateExist(String room, int sid, String templateName, Vector result)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).CVCheckForTemplateExist(sid, templateName, result);
		return result;
	}

	public int createSearch1(String room, int sid, Search search) throws RemoteException {
		return getServer(room).createSearch1(sid, search);
	}

	
	public int setCVWebDownloadPage(String room,int sid,int docId, String docName,String pageId) throws RemoteException
	{
		return getServer(room).setCVWebDownloadPage(sid,docId,docName,pageId);
	}

	
	@Override
	public Vector generateXmlfile(String room, int sid, String finalParams) throws RemoteException {
		Vector result = new Vector();
    	getServer(room).generateXmlfile(sid, finalParams, result);    	
    	return result;
	}
	
    public int syncLockDocument(String room, int sid, int did,int type) throws RemoteException
	 {
	    	return getServer(room).syncLockDocument(sid, did,type);
	 }
   
    public Vector setStickyNote(String room, int sid, DocComment dc,
    		boolean withCheck,int docRestoreFlag) throws RemoteException
    {
    	Vector ret =new Vector();
    	activeError = getServer(room).setStickyNote(sid, dc, withCheck,docRestoreFlag, ret);
    	VWSLog.dbgCheck("activeError inside  setDocumentComment::::: "+ activeError);
    	return ret;
    }

    public int docDelStickyNote(String room, int sid, int did) throws RemoteException
    {
    	return getServer(room).docDelStickyNote(sid, did);
    }
      
    
    public int setSecureLink(String room, int sid, int docId,String recepientEmail,String password,int modifyPermission,String deliveryOn,String epiresOn,String url ) throws RemoteException
    {
    	return getServer(room).setSecureLink(sid, docId, recepientEmail, password, modifyPermission, deliveryOn, epiresOn, room, url);
    }
    
    
    public int setUpdateSecureLink(String room, int sid, int docId,String recepientEmail,String password,int modifyPermission,String deliveryOn,String epiresOn  ) throws RemoteException
    {
    	return getServer(room).setUpdateSecureLink( sid,docId,recepientEmail,password,modifyPermission,deliveryOn, epiresOn );
    }
    
    
    public int getSecureLink(String room, int sid, int docId,int flag,String recepientEmail ) throws RemoteException
    {
    	return getServer(room).getSecureLink( sid,docId,flag,recepientEmail );
    }
    
	@Override
	public int expireSelectedLink(String room, int sid, int secureLinkId) throws RemoteException {
		activeError = getServer(room).expireSelectedLink(sid, secureLinkId);
    	VWSLog.dbgCheck("activeError inside  getSecureLinkData ::::: "+ activeError);
    	return activeError;
	}
	
	@Override
	public Vector getSecureLinkDataAdminwise(String room, int sid, Vector info) throws RemoteException {
		// TODO Auto-generated method stub
		Vector ret = new Vector();
		activeError = getServer(room).getSecureLinkDataAdminWise(sid, info);
		return info;
	}
	
	
	
	public Vector readSecureLinkDetails(String room, int sid) throws RemoteException {
		// TODO Auto-generated method stub
		Vector ret = new Vector();
		activeError = getServer(room).readSecureLinkDetails(sid, ret);
		return ret;
	}
	
	
	public Vector readSecureLinkUrl(String room, int sid) throws RemoteException {
		// TODO Auto-generated method stub
		Vector ret = new Vector();
		activeError = getServer(room).readSecureLinkUrl(sid, ret);
		return ret;
	}
	 public int sendCSSecureLinkEmail(String room, int sid,String docName, int type, String message,String mailIdstr,String subject,String messsagetype,String expiryDt) throws RemoteException {
			Vector ret = new Vector();
			// TODO Auto-generated method stub
			return  getServer(room).sendCSSecureLinkEmail(sid, type,docName, message, mailIdstr, subject, messsagetype,expiryDt);
			
		}
    
	 public int updateSecureLink(String room,int sid,String secureId,  String status)throws RemoteException {
			Vector ret = new Vector();
			return getServer(room).updateSecureLink(sid, secureId, status);
		}
	 public int updateExpiredLink(String room,int sid)throws RemoteException{
			Vector ret = new Vector();
			return getServer(room).updateExpiredLink(sid);
	 }
	 
	 public int getLogedInClientType(String room,int sid)throws RemoteException{
			
			return getServer(room).getClientType(sid);
	 }
	 
	 public Vector getSecureLinkPermissionModify(String room,String documentId,int sid, String userName, String password) throws RemoteException{
		 Vector ret = new Vector();
		 activeError= getServer(room).getSecureLinkPermissionModify(documentId,sid,userName,password,ret);
		 return ret;
	 }
	 
	 /**CV2019 merges from SIDBI line***/
	@Override
	public int sendMailWithAttachment(String room, String serverName, int sid, String userName, String docName,
			String routeName, int docId, Vector userInfo, Vector attachment, String status, Vector docNames,
			Vector emailOptions, String destPath, String clientType, String secureurl) throws RemoteException {
		
		 return getServer(room).sendMailWithAttachment(room, serverName, sid, userName, docName, routeName, docId, userInfo, attachment, status, docNames, emailOptions, destPath, clientType, secureurl);
	}
	@Override
	public boolean assertSessionWS(String room, int sid) throws RemoteException {
		// TODO Auto-generated method stub
		return getServer(room).assertSessionWS(sid);
	}
	
	@Override
	public int docDeleteAllPrevComments(String room, int sid, int documentId, String userName) throws RemoteException {
		// TODO Auto-generated method stub
		return getServer(room).docDeleteAllPrevComments(sid,documentId,userName);
	}
	
	@Override
	public int setPagesInfo(String room, int sid, int documentId, String pageInfoVecElem) throws RemoteException {
		// TODO Auto-generated method stub
		return getServer(room).setPagesInfo(sid,documentId,pageInfoVecElem);
	}
	
	@Override
	public Vector getPagesInfo(String room, int sid, int documentId, Vector sortedPageInfoVec) throws RemoteException {
		// TODO Auto-generated method stub
		activeError = getServer(room).getPagesInfo(sid,documentId,sortedPageInfoVec);
		return sortedPageInfoVec;
	}
	
	@Override
	public int deletetPagesInfo(String room, int sid, int documentId) throws RemoteException {
		// TODO Auto-generated method stub
		return getServer(room).deletetPagesInfo(sid,documentId);
	}
	
	/****CV2019 merges from 10.2 line--------------------------------***/
	public Vector getStatisticsInfo(String room,int sid, String optionKey,Vector result) throws RemoteException{
		 activeError= getServer(room).getStatisticsInfo(sid, optionKey, result);
			return result;
	}	
	public File getRoomAndRouteBasedMessge(String filePath, String room, String routeName) throws RemoteException{
		return getServer(room).getRoomAndRouteBasedMessge(filePath, room, routeName); 
	}
	public Vector getSettingsInfo(String room, int sid, String param, Vector result) throws RemoteException {
	    activeError = getServer(room).getSettingsInfo(sid, param, result);
	    return result;
    } 
    public boolean getSidbiCustomizationFlag(String room, int sid) throws RemoteException {
    	return getServer(room).getSidbiCustomizationFlag(sid);
    }   
    public boolean getServerSettingsInfo(String room, int sid, String param) throws RemoteException {
    	return getServer(room).getServerSettingsInfo(sid, param);
    }
    public int getDocumentStatusInWorkflow(String room, int sid, int docID) throws RemoteException {
		   return getServer(room).getDocumentStatusInWorkflow(sid, docID);
    }
    /*--------------------------------------------------------------*/

	@Override
	public int setPagesInformation(String room, int sid, String docId, String oldFileName, String fileName,
			String pagesInformation) {
		return getServer(room).setPagesInformation(sid, docId, oldFileName, fileName, pagesInformation);
	}
	
	@Override
	public Vector getDocumentPagesInformation(String room, int sid, int documentId, Vector<String> sortedPageInfoVec) throws RemoteException {
		// TODO Auto-generated method stub
		activeError = getServer(room).getDocumentPagesInformation(sid,documentId,sortedPageInfoVec);
		return sortedPageInfoVec;
	}
	
	/**CV2019 Enhancement***/
	public int checkDocsInRoute(String roomName, int sid, int docID) throws RemoteException {
		return getServer(roomName).checkDocsInRoute(sid, docID);
	}
	
	/****CV2019 merges from SIDBI line--------------------------------***/
	public Vector getCustomUsers(String room, int sid,String param, Vector creators)
			throws RemoteException
	{ 
		activeError = getServer(room).getCustomUsers(sid,param, creators); 
		VWSLog.dbgCheck("activeError inside getCreators::::: "+ activeError);
		return creators;
	}
	public int setRouteOTPInfo(String room,int sid,int recId,int docId,String userName,String OTP,Vector result)throws RemoteException{
		
		
		activeError = getServer(room).setRouteOTPInfo(sid,recId, docId,userName,OTP,result); 

		
		return activeError;
		
	}
	
	public int sendRouteOTPMail(String room,int sid,int docId,String userName,String docName,String OTP,String loggedUserMailId,String action)throws RemoteException {
		activeError = getServer(room).sendRouteOTPMail(sid, docId, userName, docName, OTP, loggedUserMailId,action);
		return activeError;
	}
	
	public Vector checkNextTaskExist(String room,int sid,int docId,int routeid ,String taskName)throws RemoteException {
		Vector result =new Vector();
		activeError = getServer(room).checkNextTaskExist(sid, docId, routeid, taskName,result);
		VWSLog.add("checkNextTaskExist result::::::::"+result);
		return result;
	}
	public Vector getBranchInfo(String room, int sid,String flag,String branchCode, Vector result)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).getBranchInfo(sid,flag,branchCode, result);
		return result;
	}

	
	
	public Vector getWorkflowDoctype(String room, int sid, Vector regVal)
			throws RemoteException {
		// TODO Auto-generated method stub
		activeError= getServer(room).getWorkflowDoctype(sid, regVal);
		VWSLog.add("regVal in vwsserver::::::"+regVal);
		return regVal;
	}
	
	
	public boolean getWARoomCache(int sid) throws RemoteException {
		return VWSPreferences.getWARoomCache();
	}
	public boolean getNativeWrap() throws RemoteException {
		return VWSPreferences.getNativeWrap();
	}
	
	public int setEncryptionType(String room, int sid, int docId, int encryptionType, Vector result)
			throws RemoteException {
		activeError = getServer(room).setEncryptionType(sid, docId, encryptionType, result);
		return activeError;
	}

	public Vector getEncryptionType(String room, int sid, int docId, Vector result) throws RemoteException {
		activeError = getServer(room).getEncryptionType(sid, docId, result);
		return result;
	}

	public Vector getRouteTaskByName(String room, int sid, int routeId, int docId, String taskName, Vector result)
			throws RemoteException {
		activeError = getServer(room).getRouteTaskByName(sid, routeId, docId, taskName, result);
		return result;
	}

	public Vector getCVCurrentFinancialYearPath(String room, int sid, Vector result) throws RemoteException {
		activeError = getServer(room).getCVCurrentFinancialYearPath(sid, result);
		return result;
	}

	public Vector getHRMSUserInfo(String room, int sid, String userName, Vector result) throws RemoteException {
		activeError = getServer(room).getHRMSUserInfo(sid, userName, result);
		return result;
	}

	public Vector getCVGetLoginInfo(String room, int sid, String userName, Vector result) throws RemoteException {
		activeError = getServer(room).getCVGetLoginInfo(sid, userName, result);
		return result;
	}

	public Vector checkDocViewPerm(String room, int sid, String userName, int docId, Vector result)
			throws RemoteException {
		activeError = getServer(room).checkDocViewPerm(sid, userName, docId, result);
		return result;
	}

	public String getMsiVersionInfo() throws RemoteException {
		return VWSPreferences.getMsiVersion1();
	}

	public String getMsiUrlInfo() throws RemoteException {
		return VWSPreferences.getMsiURL1();
	}
	
	public Vector getMsiServerSettingsInfo() throws RemoteException {
		Vector msiSettingsInfo = new Vector();
		String userName = VWSPreferences.getUserName();
		String password = VWSPreferences.getUserPassword();
		String domainName = VWSPreferences.getDomainName();
		if (userName != null && userName.trim().length() > 0)
			msiSettingsInfo.add(userName);
		else
			msiSettingsInfo.add("");
		if (password != null && password.trim().length() > 0) {
			password = Util.decryptKey_MSI(password);
			msiSettingsInfo.add(password);
		} else
			msiSettingsInfo.add("");
		if (domainName != null && domainName.trim().length() > 0) {
			msiSettingsInfo.add(domainName);
		} else
			msiSettingsInfo.add("");
		return msiSettingsInfo;
	}

	public void setMsiServerSettingsInfo(String msiVersion, String msiLocation, String userName, String password,
			String domainName) throws RemoteException {
		VWSPreferences.setMsiVersion(msiVersion);
		VWSPreferences.setMsiURL(msiLocation);
		VWSPreferences.setUserName(userName);
		VWSPreferences.setUserPassword(password);
		VWSPreferences.setDomainName(domainName);
	}

	public int checkDocIsInRoute(int sid, String room, int nodeId, boolean flagRoute) throws RemoteException {
		return getServer(room).checkDocIsInRoute(sid, nodeId, flagRoute);
	}

	/****
	 * CV2019 merges from CV10.2 line------------------------------------------------
	 ***/
	public DBConnectionBean getDBConnection(int sid, String room) throws RemoteException {
		DBConnectionBean dbBean = null;
		try {
			dbBean = getServer(room).getDBConnection(sid);
		} catch (Exception e) {
			VWSLog.dbg("Exception in VWSServer :" + e.getMessage());
		}
		VWSLog.dbg("getDBConnection in VWSServer :" + dbBean);
		return dbBean;
	}

	public boolean generateStaticJasperReport(int sid, String room, String selectedReport, String outputFormat,
			String dateFrom, String dateTo, String reportLocation, int staticReportID, int nodeID) throws RemoteException {
		return getServer(room).generateStaticJasperReport(sid, selectedReport, outputFormat, dateFrom, dateTo,
				reportLocation, staticReportID, nodeID);
	}

	public ArrayList<String> getStaticReportsList(int sid, String roomName) throws RemoteException {
		return getServer(roomName).getStaticReportList(sid);
	}

	public Vector<String> checkStaticReport(int sid, String roomName, String selectedValue) throws RemoteException {
		return getServer(roomName).checkStaticReport(sid, selectedValue);
	}

	public Vector getStaticReportResult(int sid, String roomName, int reportID, String fromDate, String toDate) throws RemoteException {
		return getServer(roomName).getStaticReportResult(sid, reportID, fromDate, toDate);
	}
	/*-----End of CV0.2 Merges---------------------------------------------------------*/
	public boolean generateDynamicJasperReport(int sid, String roomName, VWDoc vwdoc, String reportLocation,
			Vector<String> resultSet, String columnDescription, String selectedReport, String JrxmlPath,
			boolean isPreviewSelected, String outputformat) throws RemoteException {
		return getServer(roomName).generateDynamicJasperReport(sid, vwdoc, reportLocation, resultSet,
				columnDescription, selectedReport, JrxmlPath, isPreviewSelected, outputformat);
	}
	public ServerSchema checkStorageLocationForEWA(String roomName, int sid, int nodeID, ServerSchema dssSchema) throws RemoteException {
		activeError = getServer(roomName).checkStorageLocationForEWA(sid, nodeID, dssSchema);
		return dssSchema;
	}
	public VWDoc getServerJasperContents(int sid, String roomName, String reportLocation) throws RemoteException {
		return getServer(roomName).getServerJasperContents(sid, reportLocation);
	} 
	public void setBatchInputAdministrator(String batchInputAdministrator) throws RemoteException {
        VWSPreferences.setBatchInputAdministrator(batchInputAdministrator);
    }
	public String getBatchInputAdministrator() throws RemoteException {
        return VWSPreferences.getBatchInputAdministrator();
    }
	public String getSubAdministrator(String roomName) throws RemoteException {
		return VWSPreferences.getSubAdministrator(roomName);
	}
	public boolean checkUsersInGroups(String roomName, int sid, String group, String user) throws RemoteException {
		return getServer(roomName).checkUsersInGroups(sid, group, user);
	}
	public boolean checkSubAdminUser(String roomName, int sid, String user) throws RemoteException {
		String group = getSubAdministrator(roomName);
		if (group != null && group.trim().length() > 0) {
			return checkUsersInGroups(roomName, sid, group, user);
		} else {
			return false;
		}		
	}
	public int getDocumentStorageDetails(int sid, String location) throws RemoteException {
		int pageCount = 0;
		try {			
			if (new File(location).exists()) {
				ZipFile zfile = new ZipFile(location);
				java.util.Enumeration e = zfile.entries();
				while (e.hasMoreElements())
				{
					if (((ZipEntry) e.nextElement()).getName().endsWith(".vw"))
						pageCount++;
				}
				zfile.close();
			}
		} catch (Exception e) {
			VWSLog.dbg("Exception while getting document storage information :"+e);
		}
		return pageCount;
	}
	public RoomProperty getRoomAuthenticationProperties(String roomName) throws RemoteException {
		return VWSPreferences.getRoomAuthenticationProperties(roomName);
	}
}