/*
 * RampageServer.java
 *
 * 
 */
package com.computhink.vws.server;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.rmi.RemoteException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TimerTask;
import java.util.Vector;
import java.text.ParseException;

/*import oracle.jdbc.driver.OracleCallableStatement;
import oracle.jdbc.driver.OracleTypes;
*/
import oracle.jdbc.OracleCallableStatement;
import oracle.jdbc.OracleTypes;
import oracle.sql.BLOB;
import oracle.sql.CLOB;

import com.computhink.ars.server.ARS;
import com.computhink.ars.server.ARSPreferences;
import com.computhink.common.Acl;
import com.computhink.common.AclEntry;
import com.computhink.common.CIServer;
import com.computhink.common.CVPreferences;
import com.computhink.common.Constants;
import com.computhink.common.Creator;
import com.computhink.common.DBLookup;
import com.computhink.common.DocComment;
import com.computhink.common.DocType;
import com.computhink.common.Document;
import com.computhink.common.EWorkMap;
import com.computhink.common.EWorkSubmit;
import com.computhink.common.EWorkTemplate;
import com.computhink.common.Index;
import com.computhink.common.IndicesCondition;
import com.computhink.common.Node;
import com.computhink.common.NodeIndexRules;
import com.computhink.common.NotificationSettings;
import com.computhink.common.Principal;
import com.computhink.common.Room;
import com.computhink.common.RouteIndexInfo;
import com.computhink.common.RouteInfo;
import com.computhink.common.RouteMasterInfo;
import com.computhink.common.RouteTaskInfo;
import com.computhink.common.RouteUsers;
import com.computhink.common.Search;
import com.computhink.common.SearchCond;
import com.computhink.common.ServerSchema;
import com.computhink.common.Signature;
import com.computhink.common.Util;
import com.computhink.common.VWATEvents;
import com.computhink.common.VWDoc;
import com.computhink.common.VWEmailOptionsInfo;
import com.computhink.common.VWEvent;
import com.computhink.common.VWRetention;
import com.computhink.common.ViewWiseErrors;
import com.computhink.database.DSNDatabase;
import com.computhink.database.Database;
import com.computhink.database.SQLQueryParser;
import com.computhink.dss.server.DSS;
import com.computhink.ixr.server.DoIxrDBAction;
import com.computhink.ixr.server.IXRLog;
import com.computhink.ixr.server.IXRPreferences;
import com.computhink.ixr.server.Indexer;
import com.computhink.ixr.server.IndexerRestart;
import com.computhink.manager.ManagerPreferences;
import com.computhink.mdss.MDSS;
import com.computhink.resource.ResourceManager;
import com.computhink.vns.server.VNS;
import com.computhink.vns.server.VNSConstants;
import com.computhink.vns.server.VNSPreferences;
import com.computhink.vwc.DBConnectionBean;
import com.computhink.vwc.Utils;
import com.computhink.vwc.VWCConstants;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.JasperReport.VWRouteJasperReport;
import com.computhink.vws.csserver.CSSPreferences;
import com.computhink.vws.directory.Directory;
import com.computhink.vws.license.License;
import com.computhink.vws.license.LicenseException;
import com.computhink.vws.license.LicenseManager;
import com.computhink.vws.license.LicenseManagerImpl;
import com.computhink.vws.server.mail.VWMail;

public class RampageServer implements VWSConstants, ViewWiseErrors, Constants 
{
    private Room room;
    private ServerSchema myServer;
    private Database database;
    private Directory directory;
    private Hashtable DSSs = new Hashtable();
    private boolean allowConnections = true;
    private javax.swing.Timer idleTimer;
    private String roomEncryptionKey;
    private int baseSessionId;
    private Hashtable clients = new Hashtable();
    private int countDBFail = 0;
    private boolean dbConnected = true;
    private java.util.Timer scheduleDBResetTimer;
    private java.util.Timer syncTimer;
    private java.util.Timer notificationTimer;
    //private java.util.Timer idrRestartTimer;
    private boolean retFromDateValidation = false;
    private boolean isServiceStarted = false;
    private boolean isOracle = false;
    Hashtable externalDBList = new Hashtable();
    Hashtable externalDSNList = new Hashtable();
    private boolean dsnFlag = false; 
    private String message = "";
    private String ipAddress = ""; 
    private VWS vws;
    private int dbPassFlag;
    private static ResourceManager resourceManager=ResourceManager.getDefaultManager();
    private java.util.Timer idrRestartTimer;
    private java.util.Timer aipRestartTimer;
    private HashMap<Integer,String> aipActiveClient =null;
    License lic = null;
    static String currentDomainName ="";
    static boolean deleteNamedGroupUsers = false;
    private String dbLookupErrorMsg = null;
    private boolean sidbiCustomization = false;
    protected RampageServer(Room room, ServerSchema ss, int bsi) 
    {
        this.room = room;
        this.myServer = ss;
        this.baseSessionId = bsi;
        this.ipAddress = ss.address;
        database = room.getDatabase("com.computhink.vws.server.VWSLog");
        if (database.getEngineName().equalsIgnoreCase("Oracle"))
        	isOracle = true;
        else
        	isOracle = false;
        VWSLog.add(room.getName() + ": Started " + PRODUCT_NAME + " Room Server");
    }
    protected void start()
    {
    	DirectorySync dirSync = null;
    	aipActiveClient= new HashMap<Integer,String>();
    	/**
         * Enhancement :- CV10.1 Ldap multiple domain sync.         * 
		 * Reading loginServer & ldapDirectory from registry.
		 * Passing the parameters to getInstance.		 * 
         * @param ldapDirectory
         * @param loginServer
         * @param flag
         * @return
         * Modified BY :- RaviKant
         */
    	String loginServer = "";
		String ldapDirectory = "";
		ldapDirectory = VWSPreferences.getTree();
		String type = VWSPreferences.getSSType();
		if (type.equalsIgnoreCase(SSCHEME_NDS)) {
			loginServer = VWSPreferences.getLoginContext();
		} else if (type.equalsIgnoreCase(SCHEME_LDAP_ADS) || type.equalsIgnoreCase(SCHEME_LDAP_LINUX)
				|| type.equalsIgnoreCase(SCHEME_LDAP_NOVELL)) {
			loginServer = VWSPreferences.getSSLoginHost();
		} else {
			loginServer = VWSPreferences.getSSLoginHost();
		}
		
		/**
		 * CV10.1 Ldap multi domain implementaion.
		 * For loop added.
		 */
		String[] dirList = ldapDirectory.split(",");
		String[] serList = loginServer.split(",");
		for(int k=0; k< serList.length; k++){
		synchronized(this)
		{
			//directory = Directory.getInstance();
			//Flag set to true for creating multiple object for mutlidomain
			if (directory != null) {
				VWSLog.dbg("Inside rampageserver start action before new DirectorySync... and directory object not null condition satisfied...");
				directory = null;
			}
			/**CV10.2 - Added for AD to LDAP user sync. Added by Vanitha.S**/
			setSidbiCustomizationFlag();
			directory = Directory.getInstance(dirList[k],serList[k],true);
			if (k ==0) { //update AD users to LDAP for first domain only				
				boolean isADUpgradeToLDAP = VWSPreferences.getADToLDAPFlag();
				VWSLog.dbg("First domain.....adtoldap registry value :"+isADUpgradeToLDAP);
		        if ((directory.getScheme() != null) && (directory.getScheme().length() > 0)
						&& directory.getScheme().equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)
						&& isADUpgradeToLDAP == true ) {
					VWSLog.dbg("inside update AD to LDAP.....");
					DirectorySync dirSyncIns = new DirectorySync(database);
					dirSyncIns.updateADUsersToLDAP(directory);	
					dirSyncIns = null;
					VWSLog.dbg("AD to LDAP updated.....");
		        }
			}
			/**End of AD to LDAP user updation******/
			try{
			//Get the Named license seat count and send to the directory object for displaying the message if named group user count exceed the named group license seat count
			int adminGrpCount=getLicenseAdminGroupCount();
			int namedGrpCount = getLicenseNamedGroupCount();
			int namedOnlineGrpCount=getLicenseNamedOnlineGroupCount();
			int professionalGrpCount=getLicenseProfessionalNamedGroupCount();
			int concurrentGrpCount=getLicenseConcurrentGroupCount();
			int concurrentOnlineGrpCount=getLicenseConcurrentOnlineGroupCount();
			int professionalConcurrentGrpCount=getLicenseProfessionalConcurrentGroupCount();
			int publicWAGrpCount=getLicensePublicWACount();
			int subAdminGrpCount=getLicenseSubAdminGroupCount();
			int batchInputGrpCount = getLicenseAIPGroupCount();
			if (dirSync != null) {
				VWSLog.dbg("Inside rampageserver start action before new DirectorySync... and dirSync object not null condition satisfied...");
				dirSync = null;
			}
			VWSLog.dbg("namedOnlineGrpCount :::"+namedOnlineGrpCount);
			VWSLog.dbg("initil lic concurrentonline:::"+concurrentOnlineGrpCount);
			VWSLog.dbg("publicWAGrpCount initial :::"+publicWAGrpCount);
			dirSync = new DirectorySync(database, directory, true, getAdminGroup(),getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),getSubAdminGroup(), getBatchInputAdminGroup(), adminGrpCount,namedGrpCount,namedOnlineGrpCount,concurrentGrpCount,concurrentOnlineGrpCount,professionalGrpCount,professionalConcurrentGrpCount,publicWAGrpCount,subAdminGrpCount,batchInputGrpCount,false);
			
			/*if ( dirSync.getDirNamedGrpCount() > namedGrpCount){
				VWSLog.err(E191);
				isServiceStarted = false;  
        		CIServer cServer = (CIServer) Util.getServer(Util.SERVER_VWS,database.serverIP,database.serverPort);
        		VWS vws = (VWS) cServer;
        		vws.stopWorker(database.getName());
        		VWMail.sendMailNotificationOnVSM(E191, VWSConstants.VWS);
        		return;
			}*/
		
    			/*
    			 * Condition added to Security waring in server manager for bypass security added for CV83B3 
    			 * Modified Date:-30/9/2015
    			 * if condition modified in CV10 enhancement
    			 */
    			if((room.getBypass()==true)&&(VWSPreferences.getDebug()==true)&&(VWSPreferences.getSecurityCheck()==true)){
    				String SecurityWarningMsg="Security warning: bypass security enabled for room : "+room.getName();
    				VWSLog.err(SecurityWarningMsg);
    				VWMail.sendMailNotificationOnVSM(SecurityWarningMsg, VWSConstants.VWS);
    			}
    		
			}catch(Exception ex){
			    
			}
		}
		
		
    	waitForDatabaseConnection();
    	if (dbConnected){
    		// Database connection reset - initiate the scheduler
    		if (room.getRoomProperty().getConnectionResetScheduler() == 1){
    			scheduleDBResetTimer = new java.util.Timer();
    			String dailyFrequency  = room.getRoomProperty().getSchedulerTime();
    			StringTokenizer stDailyFreq = null;
    			stDailyFreq = new StringTokenizer(dailyFrequency, ":");
    			Calendar calendar = Calendar.getInstance();
    			try{	
    				if(stDailyFreq != null && stDailyFreq.countTokens() == 3){
    					calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(stDailyFreq.nextToken()));
    					calendar.set(Calendar.MINUTE, Integer.parseInt(stDailyFreq.nextToken()));
    					calendar.set(Calendar.SECOND, Integer.parseInt(stDailyFreq.nextToken()));
    				}else{
    					calendar.set(Calendar.HOUR_OF_DAY, 0);
    					calendar.set(Calendar.MINUTE, 0);
    					calendar.set(Calendar.SECOND, 0);
    				}
    			}catch(Exception ex){}
    			Date startTime = calendar.getTime();
    			scheduleDBResetTimer.schedule(new DBSchedulerConnection(), startTime, 1000 * 24 * 60 * 60);      			
    			
    		}        
    		warnForDefaultStorage();
    		isServiceStarted = true;    
    		
    		/// Directory calling synchroning users and group
    		//setEncryption added before syncprocess in cv10 modified by madhavan
    		setEncryptionKey();
    		dirSync.syncProcess(false);
    		/**
    		 * Build : Contentverse 8 Post build 04
    		 * Date : 13/12/2013
    		 * Added below code for updating user mail id based on the registry 'synchusermailid'. 
    		 */
    		String schemeType = VWSPreferences.getSSType();
    		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL) || schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS))
    			dirSync.syncUserMailId();
            
    		VWSLog.dbg("Before setIdleTimeout to..."+ room.getRoomProperty().getIdle());
    		setIdleTimeout(room.getRoomProperty().getIdle());
    		//VWSLog.dbg("After setIdleTimeout...");
    		//setEncryptionKey();
    		//setDbResource();
    		int tempTableDelRes = 0;
    		int delEmptyUsers = 0;
    		tempTableDelRes = deleteTempTables();
    		if(tempTableDelRes == NoError){
    			VWSLog.add(database.getName() + ": Deleted temporary tables");
    		}
    		//Code added to delete empty users from the database if any.
    		delEmptyUsers = deleteEmptyUsers();
    		VWSLog.add(database.getName() + ": Room Server Ready...");
    		
    		/*
    		 * Establish the connection for the External database
    		 */
    		try{
    			int dsnType = 1; //DSNType for getting DSNINFO of Selection List
    		    Vector dsnList = getDSNInfo(dsnType);    		    
    		    String dsnName = "";
    		    String userName = "";
    		    String passWord = "";
    		    if (dsnList != null && dsnList.size() > 0){
    		    VWSLog.dbg("dsnList size check in service restart :"+dsnList);
    			for (int count = 0; count < dsnList.size(); count++){
    			    DBLookup lookupInfo = (DBLookup) dsnList.get(count);

    			    if (lookupInfo.getDsnName() != null && lookupInfo.getDsnName().length() > 0){    				
    			    	DSNDatabase dbInfo = new DSNDatabase(database.getName(), lookupInfo.getDsnName(), lookupInfo.getUserName(), lookupInfo.getPassWord());
    			    	
    			    	if(lookupInfo.getDsnType()==0)
    			    		externalDBList.put(lookupInfo.getDsnName(), dbInfo);
    			    	else if(lookupInfo.getDsnType()==1 && lookupInfo.getDsnStatus()==1)//Only Enabled Selection index need to be synchronized
    			    		externalDSNList.put(lookupInfo, dbInfo);
    			    }    			    
    			}
    			VWSLog.dbg("externalDBList :"+externalDBList);
    			VWSLog.dbg("externalDSNList :"+externalDSNList);
    			dsnFlag = true;    		
    		    }
    		}catch(Exception ex){
    			message =resourceManager.getString("RampServMsg.StartDBErr")+" "+ ex.getMessage();
    			VWSLog.err("Error in start DB " + ex.getMessage());
    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    		}
    		//Displaying the invalid task on starting of ViewWise Server
    		try{
    			Vector ret = getInvalidRouteNames();
    			if(ret!=null && ret.size()>0){
    				VWSLog.war("Invalid "+ WORKFLOW_MODULE_NAME.toLowerCase() +" name(s): ");
    				String dbstring = "";
    				for(int i=0; i<ret.size(); i++){
    					dbstring = ret.get(i).toString();
    					StringTokenizer str = new StringTokenizer(dbstring, "\t");
    					str.nextToken();//Route Id
    					VWSLog.war(""+str.nextToken());
    				}
    			}
    		}catch(Exception ex){
    			VWSLog.err("Error in getting invalid "+ WORKFLOW_MODULE_NAME.toLowerCase() +" names : "+ex.getMessage());
    		}

    		//Syncrozing the selection index schedular
    		Calendar calendar = Calendar.getInstance();
    		String syncTime = VWSPreferences.getSyncTime();
    		if(syncTime!=null && syncTime.length()!=0){
    			StringTokenizer stDailySync = null;
    			stDailySync = new StringTokenizer(syncTime, ":");
    			if(stDailySync != null && stDailySync.countTokens() == 3){
    				calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(stDailySync.nextToken()));
    				calendar.set(Calendar.MINUTE, Integer.parseInt(stDailySync.nextToken()));
    				calendar.set(Calendar.SECOND, Integer.parseInt(stDailySync.nextToken()));
    			}else{
                	calendar.set(Calendar.HOUR_OF_DAY, 0);
                	calendar.set(Calendar.MINUTE, 0);
                	calendar.set(Calendar.SECOND, 0);
                }
    			try{
    				Date startTime = calendar.getTime();
    				long snoozeTime = 1000 * 24 * 60 * 60;
    				Format formatter = new SimpleDateFormat("HH:mm:ss");
    		    	String s = formatter.format(startTime);
    				VWSLog.add("Synchronization process will start for the room '"+this.room.getName()+"' at "+s);
    				syncTimer = new java.util.Timer();
    				syncTimer.schedule(new syncSelListScheduler(this.room.getName()), startTime, snoozeTime);
    			}catch(Exception ex){}
    		}
    		
	    		try{
    			long delay = 1000 * 60 * 15;
	    			notificationTimer = new java.util.Timer();
	    			notificationTimer.schedule(new checkNotificationServerScheduler(), delay);
	    					idrRestartTimer= new java.util.Timer();
	    					aipRestartTimer=new java.util.Timer();
	    					if(IXRPreferences.getAutoRstdebug()==true){
	    						VWSLog.dbg("Scheduler call for Indexer Auto Restart");
	    						idrRestartTimer.schedule(new IndexerRestart(),3000,3000);
	    						Indexer.docCount = 0;
	    					}
	        
	    		}catch(Exception ex){}
    	}
    	
    	
    }
    		    		
	}
    //Check for ContentSentinel server exist or not.
    public void checkContentSentinelServer(){
    	com.computhink.vws.csserver.CSS css = null;
    	VWSServer.CSServiceVerified = true; 
    	boolean cssExist = false;
    	ServerSchema csSS = new ServerSchema();
    	csSS.setType(Constants.SERVER_CSS);
    	csSS.setHostName(CSSPreferences.getHostName());
    	csSS.setComport(CSSPreferences.getComPort());

    	try {
    		Client client = getServerClientObj();// 999999999
    		Vector contentSentinelSettings = new Vector();

    		String param = ""+ Util.SepChar; 
    		DoDBAction.get(client, database, "VW_CSGetSecureLink", param,
    				contentSentinelSettings);
    		if (contentSentinelSettings != null && contentSentinelSettings.size() > 0) {
    			cssExist = true;
    		}else{
    			VWSServer.CSServiceVerified = false;
    		}

    		if (cssExist) {
    			// Check for notification server runnung
    			try {
    				css = (com.computhink.vws.csserver.CSS) Util.getServer(csSS);
    				if (css != null && css.ping()) {
    					// VWSLog.war("Notification server is running.");
    				} else{
    					VWSLog.war("Content Sentinel Server is not running.");
    					VWSServer.CSServiceVerified = true;  					
    				}
    			} catch (Exception e) {
    			}
    		}
    	} catch (Exception ex) {
    	}
    }  

    //Check for notification server exist or not.
    public void checkNotificationServer(){
    	VNS vns = null;
    	VWSServer.VNSServiceVerified = true; 
    	boolean notificationExist = false;
    	ServerSchema vnsSS = new ServerSchema();
    	vnsSS.setType(Constants.SERVER_VNS);
    	vnsSS.setHostName(VNSPreferences.getHostName());
    	vnsSS.setComport(VNSPreferences.getComPort());

    	try {
    		Client client = getServerClientObj();// 999999999
    		Vector notificationSettings = new Vector();

    		String param = 0 + Util.SepChar + 1 + Util.SepChar + ""
    		+ Util.SepChar + "-" + Util.SepChar;
    		DoDBAction.get(client, database, "VW_NFGetSettings", param,
    				notificationSettings);
    		if (notificationSettings != null && notificationSettings.size() > 0) {
    			notificationExist = true;
    		}else{
    			VWSServer.VNSServiceVerified = false;
    		}

    		if (notificationExist) {
    			// Check for notification server runnung
    			try {
    				vns = (VNS) Util.getServer(vnsSS);
    				if (vns != null && vns.ping()) {
    					// VWSLog.war("Notification server is running.");
    				} else{
    					VWSLog.war("Notification server is not running.");
    					VWSServer.VNSServiceVerified = true;  					
    				}
    			} catch (Exception e) {
    			}
    		}
    	} catch (Exception ex) {
    	}
    }    /*
     * syncSelListScheduler timer - To syncronize the selection list values of indices once in a day
     */
    public class checkNotificationServerScheduler extends TimerTask{
    	
    	public checkNotificationServerScheduler(){}
    	public void run(){
    		try{
    			if (database != null && !VWSServer.VNSServiceVerified) {
    				checkNotificationServer();
    			}
    		}catch(Exception ex){}
    	}
    }
 public class checkContentSentinelServerScheduler extends TimerTask{
    	
    	public checkContentSentinelServerScheduler(){}
    	public void run(){
    		try{
    			if (database != null && !VWSServer.CSServiceVerified) {
    				checkContentSentinelServer();
    			}
    		}catch(Exception ex){}
    	}
    }

    /**
	 * Loop through the externalDBList and get the connection and 
	 * call updateIndexSelection procedure for each selection list
	 */
    public int getDSNSelList(String roomName) {
		if (externalDSNList == null || (externalDSNList != null && externalDSNList.size() == 0)) {
			//VWSLog.add("DSN information is not avilable for syncronization process at "+roomName+".");
			return Error;
		}
		String clientMode = "A";
		String indexName = "";
		try{
			for (Enumeration e =externalDSNList.keys(); e.hasMoreElements();) {
				DBLookup dsnDetails = (DBLookup)(e.nextElement());
				
				indexName = dsnDetails.getIndexName();
				
				VWSLog.add("Syncronization started for "+roomName+" - index : "+indexName);
				
				PreparedStatement stmt = null;
				ResultSet result = null;
				Connection con = null;
				try {
					Database dsnDB = (Database) externalDSNList.get(dsnDetails);
					String query = dsnDetails.getConfigQuery();
					String strValues = "";
					if (query != null && query.trim().length() > 0) {

						con = dsnDB.aquireConnection();
						stmt = con.prepareStatement(query);
						result = stmt.executeQuery();
						
						while (result.next()) {
							strValues += result.getString(1) + "|";
						}
						if(strValues.endsWith("|"))
							strValues = strValues.substring(0,strValues.lastIndexOf("|"));
						
						/*
						 * The following logic implement after face the problem in the Client GCNA - They have 14000+ items stored in the Selection List 
						 * Whilch will be expensive operation when it execute 14000 DB call, so we are passing 4000 character (Oracle String parameter maximum capacity is 4000 characters) to DB on each call. When it comes from adminwise AW will remove the duplicate 
						 * in the selection list 
						 */
						int MAXLENGTH = 3985;
						int lastIndex = 1, beginIndex = 0, delValues = 1;
						int length = strValues.length();
						if (length < MAXLENGTH) {
							lastIndex = length;
						} else
							lastIndex = MAXLENGTH;

						while (lastIndex > 0 && lastIndex <= length) {
							String tmpValue = "";
							if (beginIndex + MAXLENGTH < length)
								tmpValue = strValues.substring(beginIndex,
										beginIndex + MAXLENGTH);
							else
								tmpValue = strValues.substring(beginIndex, length);

							if (tmpValue.lastIndexOf("|") > 0
									&& ((lastIndex + MAXLENGTH) < length))
								lastIndex = beginIndex + tmpValue.lastIndexOf("|");
							else
								lastIndex = length;
							
							Client client = getServerClientObj();
							
							DoDBAction.get(client, database, "UpdateIndexSelection", 
									clientMode+ "|" 
									+ String.valueOf(dsnDetails.getIndexId())+ "|"
									+ String.valueOf(delValues)+ "|"
									+ strValues.substring(beginIndex,
											lastIndex), new Vector());

							beginIndex = lastIndex + 1;
							if (delValues == 1) {
								delValues = 0;
							}
							if (beginIndex >= length)
								break;
						}
					}
					stmt = null;
					result = null;
					con = null;
					VWSLog.add("Syncronization completed for "+roomName+" - index : "+indexName);
				} catch (Exception ex) {
					VWSLog.add("Exception while executing dsn query : "+ex.getMessage());
				}
				finally{
    				try{
    					if (result != null) result.close();
    					if (stmt != null) stmt.close();
    				}catch(Exception ex){
    					result = null;
    					stmt = null;
    				}
    			}
			}
		} catch(Exception ex){
			VWSLog.add("Exception in getting the dsn info : "+ex.getMessage());
		}
		return NoError;
	}
    /**
	 * getServerClientObj - to create client object
	 * This is for calling the existing procedure "UpdateIndexSelection" 
	 */
    public Client getServerClientObj(){
    	Client clientObj = new Client("VWSUser", 99999999);
    	clientObj.setClientType(Client.Adm_Client_Type);//Already in procedure same client type has been used
    	clientObj.setIpAddress(this.ipAddress);
	    return clientObj;
    }
    
    /*
	 * syncSelListScheduler timer - To syncronize the selection list values of indices once in a day
	 */
    public class syncSelListScheduler extends TimerTask{
    	public String roomName = "";
    	public syncSelListScheduler(String room){
    		roomName = room;
    	}
    	public void run(){
    		int ret = -1;
    		try{
    			if (database != null) {
    				ret = getDSNSelList(roomName);
    				if(ret==NoError)
    					VWSLog.add("Finished synchronizing for "+roomName);
    			}
    		}catch(Exception ex){}
    	}
    }
    
    // Database connection reset scheduler
    protected class DBSchedulerConnection extends TimerTask{
    	public DBSchedulerConnection(){
    		
    	}
    	public void run(){
    		try{
    			if (isServiceStarted) {
    			Connection con = null;
    	    	con = database.aquireConnection();con.close();    	    	
    	    	VWSLog.dbg(database.getName() +": Database Connection reset by Scheduler.");
    	    	database.initialize();
    	        con = null;
    			}
    		}catch(Exception ex){}
    	}
    }
    
    /**CV10.2 - Escalation mail Enhancement **/
    class sendMailThread extends Thread{  
    	VWMail vwMail1; 
    	String fromMail="";
    	String toMail1="";
    	String subject1="";
    	String message1="";
		String type = null;
		File attachment[] = null;
		String docName = null, status = null;
		Vector docNames = null;
		String routeName = null, webAccessReference = null, roomName = null, mailCcTo = null;
		HashMap<String, String> indexDetails = null;

		sendMailThread(VWMail vwMail, String fromMailId, String toMailId, String subject, String message,
				String mailType, File attachment[], String docName, String status, Vector docNames, String routeName,
				String webAccessReference, String roomName, String mailCcTo, HashMap<String, String> indexDetails) {
    		this.vwMail1=vwMail;  
    		this.fromMail=fromMailId;
    		this.toMail1=toMailId;
    		this.subject1=subject;
    		this.message1=message;
    		this.type = mailType;
    		this.attachment = attachment;
    		this.docName = docName;
    		this.status = status;
    		this.docNames = docNames;
    		this.routeName = routeName;
    		this.webAccessReference = webAccessReference;
    		this.roomName = roomName;
    		this.mailCcTo = mailCcTo;
    		this.indexDetails = indexDetails;
    	}  
    	public void run(){  
    		if (type != null && type.equalsIgnoreCase("OTP")) {
	    		VWSLog.dbg("Before calling escalation mail in sendemail therad ");
	    		//VWSLog.dbg("fromMail ::"+fromMail+"::toMail is "+toMail1+"::subject1::"+subject1+"::message1::"+message1);
	    		int ret=vwMail1.sendEscalationMail(fromMail, toMail1, subject1, message1);
	    		if(ret==1){
	    			VWSLog.dbg("OTP Mail sent sucessfully");
	    		}
    		} else {
				int res = vwMail1.sendDRSMail(toMail1, subject1, attachment, docName, status, docNames, routeName,
						webAccessReference, roomName, mailCcTo, indexDetails);
        		VWSLog.dbg("after calling the sendDRSMail....."+res);
        		String vwsHome = VWSUtil.getHome();
        		//After mail sent need to delete document.vwr file for DRS functionality
        		File tempFile = new File(vwsHome +Util.pathSep+"lib"+Util.pathSep+"document.vwr");
        		try{
        			if(tempFile!=null && tempFile.exists()){
        				boolean deleted = tempFile.delete();
        			}
        		}catch(SecurityException se){
        			VWSLog.dbg("SecurityException while deleting document.vwr file is : "+se.getMessage());
        		}catch(Exception ex){
        			VWSLog.dbg("Exception while deleting document.vwr file");
        		}
        		
        		switch(res){
	        		case -1:
	        			VWSLog.war("Error in sending mail. Check mail server settings.");
	        			break;
	        		case -2:
	        			VWSLog.war("Error in sending mail. Please check the user(s) mail id.");
	        			break;
        		}
    		}
    	}  

    }  
    protected void setIdleTimeout(int timeout)
    {
    	if (timeout == 0) timeout = DEFAULT_IDLE_TIME;
    	//if (timeout > 0)
        {
            if (idleTimer != null) idleTimer.stop();
            //idleTimer = new javax.swing.Timer(timeout*1000*60, new IdleTimerAction());
           // VWSLog.dbg("Inside setIdleTimeout.....");
            idleTimer = new javax.swing.Timer(60*1000*60, new IdleTimerAction());
            idleTimer.setRepeats(true);
            idleTimer.start();
            VWSLog.add(room.getName() + ": idle timeout set to " + timeout + 
                                                                  " minute(s)");
        }
    }
    private void waitForDatabaseConnection()
    {
    	VWSLog.dbg("inside waitForDatabaseConnection.....");
        boolean msg = true;
        Connection con = null;
        countDBFail = 0;
        while (con == null)
        {
            try{
            	con = database.aquireConnection();
        //Later we need to uncomment ; we have to get confirmation from Rajesh - by PR    	
        // This is to check the SQL Server database logging in with the mapped user name
		if (database.getEngineName().equalsIgnoreCase("SQLServer")){
		    boolean flag = checkDBUserMapping(con);		    
		    if (!flag){
			database.releaseConnection(con);
			dbConnected = false;
			break;
		    }
		}
            }catch (Exception e)
            {
                String db = database.getJdbcUrl();
                if (msg) {
                	message = database.getName() 
                    + resourceManager.getString("RampServMsg.ListeningDBServ")+" "+ db + "\r\n" 
                    + e.getMessage();
                	VWSLog.war(message);
					VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
                	
                	
                }
                /**If condition has added for DBLookup customer issue. If dsn connection is failed then server room is going offline**/
                if (!database.getEngineName().equalsIgnoreCase("DSN")) {
	                if ((e.getMessage().toUpperCase()).indexOf("CONNECTION RESET BY PEER") > -1 ||
	                        (e.getMessage().toUpperCase()).indexOf("CONNECTION REFUSED") > -1 ||
	                        (e.getMessage().toUpperCase()).indexOf("ERROR ESTABLISHING SOCKET") > -1){
	                	countDBFail++;
	                	VWSLog.dbg("DB connection failure count in waitForDatabaseConnection method :"+countDBFail);
	                	VWSLog.dbg("database.getCountDBFail() : "+database.getCountDBFail());
	                	if (countDBFail > database.getCountDBFail()){
	                		dbConnected = false;
	                		try{
		                		CIServer cServer = (CIServer) Util.getServer(Util.SERVER_VWS,database.serverIP,database.serverPort);
		                		VWS vws = (VWS) cServer;
		                		VWSLog.dbg("before making the room offline in waitForDatabaseConnection method ......");
		                		vws.closeRoom(database.getName());	                		
	                		}catch(Exception ex){
	                			//VWSLog.add("Exception in CloseRoom Name " + database.getName()+ " :::: " + ex.getMessage());
	                			break;
	                		}
	                		break;
	                	}
	                }
                }
                msg = false;
            	int frequency = 10;
            	try{
            		frequency = VWSPreferences.getDBFrequency();
            	}catch(Exception ex){frequency = 10;}
            	Util.sleep(frequency * 1000);                
            }
        }
        try{
        	database.releaseConnection(con);
        if (countDBFail > 0 && con != null){
        	VWSLog.add(database.getName() + " : Room Connected to the database.");
        	dbConnected = true;
        }

        }
        catch (Exception e){}
    }

    private boolean checkDBUserMapping(Connection con){
	ResultSet rs = null, rs1 = null;
	Statement st = null;
	String user = null;
	try{	   
	    String sqlVersionQuery = "Select @@Version as Version";
	    // To check the existance of database - 2000
	    String dbExistsQuery = "SELECT name FROM sysdatabases Where name='"+room.getDatabaseName()+"'";
	    // To check the existance of database - 2005
	    String dbExistsQuery2005 = "SELECT name FROM sys.databases Where name='"+room.getDatabaseName()+"'";
	    String dbUserMappingQuery = "SELECT '"+room.getDatabaseName()+"', name FROM "+room.getDatabaseName()+".dbo.sysusers " +
	    "WHERE sid in (Select sid from syslogins where ltrim(Rtrim(name)) = '"+database.getJdbcUser()+"')";
	    // To check the mapping of user and database - 2000 
	    user = database.getJdbcUser();
	    String authenticationMode = VWSPreferences.getAuthenticationMode(room.getName());
		VWSLog.dbg("authenticationMode ---->:"+authenticationMode);
		if (authenticationMode != null && authenticationMode.trim().length() > 0
				&& authenticationMode.equalsIgnoreCase("windows")) {
			String domain = VWSPreferences.getDomain(room.getName());
			domain = domain.substring(0, domain.indexOf("."));
			user = domain+"\\"+user;
		}
	    String dbUserMappingQuery2005 = "SELECT '"+room.getDatabaseName()+"', name FROM ["+room.getDatabaseName()+"].sys.database_principals " +
	    "WHERE sid in (Select sid from sys.server_principals where ltrim(Rtrim(name)) = '"+user+"')";
	    String umQuery = "use master";
	    st = con.createStatement();
	    st.execute(umQuery);
	    rs = st.executeQuery(sqlVersionQuery);
	    
	    String result = "";
	    if (rs.next()){
		result = rs.getString("Version");
	    }
	    result = result.substring(0,26);
	    if(result.equalsIgnoreCase("Microsoft SQL Server  2000")){
		rs = st.executeQuery(dbExistsQuery);
		if(rs.next()){
		    rs1 = st.executeQuery(dbUserMappingQuery);
		    if(!rs1.next()){
		    	message = resourceManager.getString("RampServMsg.User_Password")+" "+room.getDatabaseName();
	 			VWSLog.err("Enter correct username and password for "+room.getDatabaseName());		    	
	 			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			return false;
		    }
		}
	    }else{
		rs = st.executeQuery(dbExistsQuery2005);
		if(rs.next()){
		    rs1 = st.executeQuery(dbUserMappingQuery2005);
		    if(!rs1.next()){
		    	message = resourceManager.getString("RampServMsg.User_Password")+" "+room.getDatabaseName();
				VWSLog.err("Enter correct username and password for "+room.getDatabaseName());
	 			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
				return false;
		    }
		}
	    }
	    

	}catch(Exception ex){
		message = database.getName() +resourceManager.getString("RampServMsg.DBUserMapping")+" "+ ex.getMessage();
	    VWSLog.err(database.getName() + ": Error in checkDBUserMapping " + ex.getMessage());
		VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	}
	finally{
	    
	    try{
		st.execute("use " +room.getDatabaseName());
		if(st != null) st.close();
		if(rs != null) rs.close();
		if(rs1 != null) rs1.close();
	    }catch(Exception ex){
		
	    }
	}
	return true;
    }
    
    private void warnForDefaultStorage()
    {
        if (!isDefaultStorage()){
        	message = resourceManager.getString("RampServMsg.NOStorage")+" " 
                + room.getName();
        	VWSLog.war(message);
        	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
        }
    }
    private DSS getDSS(ServerSchema ss)
    {
        DSS dss =null;
        if (!DSSs.isEmpty() && DSSs.containsKey(ss.address))
        {
            dss = (DSS) DSSs.get(ss.address);
        }
        else
        {
            dss = (DSS) Util.getServer(ss);
            if (dss != null) DSSs.put(ss.address, dss);
        }
        if(dss != null)
            setDSSEncryptionKey(dss);
        return dss;
    }
    private void setDSSEncryptionKey(DSS dss)
    {
        try
            {        		
                if(dss.isEncryptionKeySet(getRoomName()))
                {
                    dss.setRoomEncryptionKey(getRoomName(),
                        getEncryptionKey("4223DAAB-D393-11D0-9A76-00C05FB68BF7"));
                }
            }
            catch(Exception e){}
    }
    private int deleteDocumentFiles(ServerSchema ss)
    {
        ss.type = SERVER_DSS;
        DSS dss = getDSS(ss);
        if (dss == null) return dssInactive;
        Document doc = new Document(ss.getId());
        doc.getVWDoc().setDstFile(ss.path + Util.pathSep + ss.getId());
        try
        {
            if (dss.deleteDocument(doc)) 
                return NoError;
            else
                return documentNotFound;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    private int setActiveFileVersion(int sid, int nid, DSS dss, String path, String oVer, 
                                                                    String nVer)
    {
        try
        {
            String roomName = getRoomName();
            if (dss.setActiveFileVersion(sid, nid, roomName, path, oVer, nVer)) 
                return NoError;
            else
                return documentNotFound;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    private boolean checkNodeRights(int nid, int Permission ,int sid)
    {
        if (isAdmin(sid)) return true;
        Client client = getClient(sid);
        String param = "" + nid + Util.SepChar + client.getUserName() + 
                                  Util.SepChar + Permission;
        Vector ret = new Vector();
        DoDBAction.get(getClient(sid),  database, "CheckNodeRightsAction", param, ret);
        if (ret.size() == 0) return false;
        return (((String) ret.elementAt(0)).equals("1"));
    }
    //---------------------------public methods---------------------------------
    // *
    // *
    //---------------------------Sessions Public--------------------------------
/*
Issue No / Purpose:  <650/Terminal Services Client>
Created by: <Pandiya Raj.M>
Date: <19 Jul 2006>
Pass the stubIPAddress" to the login method.
*/    
    public synchronized int login(int type, String ip, String user, String pass, 
            int port){
    	return login(type,ip,user,pass,port,"");
    }

    public synchronized int login(int type, String ip, String user, String pass, 
                                                                       int port,String stubIPAddress)
    {
    	
    	if (isConnectionEnabled() == 0) return NotAllowedConnections;
    	int sid = checkUserError;
    	String ldapUserDN="";
    	/*
        Purpose:  Code changed for ARS client type
        Created by: Shanmugavalli.C
        Date: <20 July 2006>
    	 */
    	VWSLog.dbg("byPassSecurity :::"+byPassSecurity());
    	if(byPassSecurity()) 
    	{
    		//Modified the audit trail description message for cv83b3 date:-30/9/2015
    		addATRecord(sid, user,ip, 1, VWATEvents.AT_OBJECT_LOGIN, 
    				VWATEvents.AT_LOGIN, "Logged in with room bypass security set");
    		sid = startSession(user, ip, type, port,stubIPAddress);
    		/*Included clientname,ipandusername for nomorelicense seats
    		 * notification mail
    		 * Modified by Madhavan
    		 */
    		if (sid == NoMoreLicenseSeats){
    			if(type==Client.Fat_Client_Type){
    				message = resourceManager.getString("RampServMsg.NOMoreLiceseDTC")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.Web_Client_Type){
    				message = resourceManager.getString("RampServMsg.NOMoreLiceseWebTop")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.Adm_Client_Type){
    				//VWSLog.add("adminmessage");
    				message = resourceManager.getString("RampServMsg.NOMoreLiceseAdmin")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.WebL_Client_Type){
    				message = resourceManager.getString("RampServMsg.NOMoreLiceseWebLite")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}

    			else if(type==Client.IXR_Client_Type){
    				message = resourceManager.getString("RampServMsg.NoMoreLicenseIXR")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.NFat_Client_Type){
    				message = resourceManager.getString("RampServMsg.NOMoreLiceseDTC")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.sdk_Client_Type){
    				message = resourceManager.getString("RampServMsg.NoMoreLicenseSDK")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.Ars_Client_Type){
    				message = resourceManager.getString("RampServMsg.NoMoreLicenseARS")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.NWebL_Client_Type){
    				message = resourceManager.getString("RampServMsg.NOMoreLiceseWebLite")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.WebAccess_Client_Type){
    					message = resourceManager.getString("RampServMsg.WebAccess")+" "+user+" "+
    					resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.Professional_WebAccess_Client_Type){
    				if(isProfessionalExceeds())
    					message = resourceManager.getString("RampServMsg.NoMoreLicenseproffessional")+" "+user+" "+
    							resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.Custom_Label_WebAccess_Client_Type){
    				message =  resourceManager.getString("RampServMsg.CustLblWebAccess")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else if(type==Client.Notification_Client_Type){
    				message = resourceManager.getString("RampServMsg.NotificationService")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}else if (type==Client.ContentSentinel_client_Type) {
    				message = resourceManager.getString("RampServMsg.ContentSentinelService")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}else if(type==Client.CSWebaccess_client_Type) {
    				message = resourceManager.getString("RampServMsg.CSWebAccess")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    			}
    			else{
    				message = resourceManager.getString("RampServMsg.NOMoreLicese");
    			}
    			VWSLog.war(message);
    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    		}
    		VWSLog.dbg("sid..........."+sid);
    		return sid;
    	}
    	if (type == Client.Ars_Client_Type || type == Client.Drs_Client_Type || type == Client.Notification_Client_Type||type==Client.ContentSentinel_client_Type) {
    		sid = startSession(user, ip, type, port,stubIPAddress);
    	} else {
    		/*Getting common name is added for LDAP. Valli 22 March 2007*/
    		/*
    		 * Added by PR. if user name having '.' in the name, we login with the given user name if it is not exists we will tokenized by the . operator and 
    		 * first value as user name and remaining will be considered as context name
    		 */
    		int retAuthenticate = 0;
    		try
    		{
			int userId = 0;
			Vector result = new Vector();
			String resultStr = "", resultPass = null;
			String param = null;
			String[] resultArray = null;
			String lastName = "";
			String samName="";
			
			/**CV2019 merges from SIDBI.  Added by Vanitha**/			
			boolean isSecurityChkRequired = true;
			boolean sidbiCustomization = getServerSettingsInfo(sid, "SIDBI Customization");
			VWSLog.dbg("sidbiCustomization..........."+sidbiCustomization);
			if (sidbiCustomization) {
				try {
					//SIDBI related change to allow password less entry to DTC
					if (type == Client.Fat_Client_Type ) {
						param = "Bypass Security for DTC";
						getSettingsInfo(sid, param, result);
						VWSLog.dbg("result..........."+result);
						if(result!= null && !result.isEmpty()){
							VWSLog.dbg("result.get(0)..........."+result.get(0));
							if (String.valueOf(result.get(0)) != null && String.valueOf(result.get(0)).equalsIgnoreCase("YES")) {
								isSecurityChkRequired = false;
							}
							result.removeAllElements();
						}
					}
					//SIDBI related change to allow password less entry to Old WebAccess
					if(type == Client.Web_Client_Type || type == Client.WebAccess_Client_Type || 
		        			type == Client.Custom_Label_WebAccess_Client_Type || type == Client.Professional_WebAccess_Client_Type ||
		        			type == Client.Concurrent_WebAccess_Client_Type ||
		        			type == Client.Professional_Conc_WebAccess_Client_Type){
						param = "Bypass Security for WebAccess";
						getSettingsInfo(sid, param, result);
						VWSLog.dbg("result for WebAccess ..........."+result);
						if(result!= null && !result.isEmpty()){
							VWSLog.dbg("result.get(0)..........."+result.get(0));
							if (String.valueOf(result.get(0)) != null && String.valueOf(result.get(0)).equalsIgnoreCase("YES")) {
								isSecurityChkRequired = false;
							}
							result.removeAllElements();
						}
					}
				} catch (Exception e) {
					VWSLog.dbg("Exception after calling VWGetSettingsInfo method : " + e.getMessage());
				}
			}
			/******************************************************************************************************/
			
			VWSLog.dbg("isSecurityChkRequired..........."+isSecurityChkRequired);	
			String schemeType = VWSPreferences.getSSType();
			String domainName = null;
			try
			{
				//VWSLog.add("type 19::::::::::::::"+type);
				//if(type==Client.CSWebaccess_client_Type) {				
				if(user.contains("~cs~")) {
					userId = -1;
					user=user.substring(4,user.length());
					String param1 = userId + Util.SepChar + user;
					DoDBAction.get(getClient(sid), database, "VWGetUserPWD", param1,result);
					type=25;
					
					if(result!= null && !result.isEmpty()) {
						resultStr = String.valueOf(result.get(0));
						VWSLog.dbg("resultStr ::"+resultStr);
						resultArray = resultStr.split("\t");
						VWSLog.dbg("resultArray[0] ::::"+resultArray[0]);
						if(!(resultArray[0].equals("0"))) {
							VWSLog.dbg("Inside else if of cswebaccess_Client_type");
							if((resultArray[0].equals("-77"))) {
								sid=-77;
							}else {
								StringTokenizer st = new StringTokenizer(user, Util.SepChar);
								int csDocId=Integer.parseInt(st.nextToken());
								user=st.nextToken();
								String actualPass=st.nextToken();
								//passing actual user name as actualpassword to fix the license consuming issue for securelink.
								sid = startSession(user, ip, type, port,stubIPAddress, actualPass);
								addATRecord(sid, user,ip,csDocId,VWATEvents.AT_OBJECT_DOC, 
					    				VWATEvents.AT_LOGIN_SECURE_LINK, "User :"+user+" has logged in using SecureLink from clientIP :"+ip);
							}

						} 
					}
					
				}else {
					param = userId + Util.SepChar + user;
					/**CV2019 merges from CV10.2.  Modified by Vanitha**/
					try {
						String dirName = "-";
						if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)) {
							String ldapDirectory = VWSPreferences.getTree();
							VWSLog.dbg("Directory name from server settings >>>> " + ldapDirectory);
							String[] dirList = ldapDirectory.split(",");
							if (dirList.length > 1) {
								dirName = "0";
							} else {
								dirName = dirList[0];
							}
						} else {
							/**CV2020 changes added by Vanitha on 08/01/2019**/
							domainName = VWSPreferences.getSSLoginHost();
						}
						param = param + Util.SepChar + dirName;
						DoDBAction.get(getClient(sid), database, "VWGetUserPWD", param, result);
					} catch (NullPointerException e) {
						VWSLog.dbg("Log Exception In" + e);
					}
					//DoDBAction.get(getClient(sid), database, "VWGetUserPWD", param,result);
				}
			} catch (NullPointerException e) {
				VWSLog.dbg("Log Exception In"+e);
			}
			VWSLog.dbg("result...."+result);
			if(result!= null && !result.isEmpty()){
				resultStr = String.valueOf(result.get(0));				
				resultArray = resultStr.split("\t");
			}
		/*	VWSLog.dbg("resultArray length :::"+resultArray.length);
			VWSLog.dbg("resultArray[0] :"+resultArray[0]);
			VWSLog.dbg("resultArray[1] :"+resultArray[1]);
			VWSLog.dbg("resultArray[2] :"+resultArray[2]);
			VWSLog.dbg("resultArray[3] :"+resultArray[3]);*/
			/**
			 * Enhancement CV10.1:- To replace the samaccount name with principal name
			 * Modified by :- Sudhansu
			 */
			if(resultArray.length>=5){
				if(!samName.equals("-")){
					user=resultArray[1];
				}
			}
			if (resultArray != null && resultArray.length >= 4){
				resultPass = resultArray[3];
				lastName = resultArray[2];
				ldapUserDN=resultArray[4];
				VWSLog.dbg("ldapUserDN ::"+ldapUserDN);
			}
			if(resultPass != null && !resultPass.equals("-")){
				lastName = resultArray[2];
				resultPass = Util.decryptKey(resultArray[3]);
			}
			if( result== null || (resultPass != null && resultPass.equals("-")) || result.isEmpty()){
				/**CV2019 merges from SIDBI.  Added by Vanitha**/
				if (isSecurityChkRequired) {
					/*VWSLog.add("lastname before authenticate last name is "+lastName);
					directory.AuthenticateUserLastName(lastName);
					VWSLog.dbg("ldapUserDN before ldapUserDN is "+ldapUserDN);
					directory.AuthenticateUserDN(ldapUserDN);*/
					/**CV2019 merges from CV10.2.  Added by Vanitha**/
					VWSLog.dbg("user name before authentication  :"+user);
					if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
						VWSLog.dbg("Inside ldap ads authenticateuserdn new :::"+user+"  ldapUserDN "+ldapUserDN);
						retAuthenticate = directory.AuthenticateUserDN(user, pass,ldapUserDN);
					} else {
						/**CV2020 changes added by Vanitha on 08/01/2019**/
						String userName = null;	
						boolean isDomainUser = false;
						/*if (user.contains("\\") || user.contains("/"))
							isDomainUser = true;*/
						VWSLog.dbg("isDomainUser :"+isDomainUser);
						if (domainName != null && domainName.trim().length() > 0 && !isDomainUser)
							userName = domainName+"\\"+user;
						else
							userName = user;
						retAuthenticate = directory.AuthenticateUser(userName, pass);
					}
				} else {
					retAuthenticate = 1;
				}
			} else if(resultPass.equals(pass)){
				retAuthenticate = 1;
				//Flag added to fix the username with dot fro change password CV83.1
				dbPassFlag = 1;
			}				
    		} catch(Exception ex) {
    			VWSLog.add("Log Exception In"+ex);
    		}
    		VWSLog.dbg("After checking AuthenticateUser "+retAuthenticate);
    		String actualUser = "";
    		if( retAuthenticate >= 1){

    			String schemeType = VWSPreferences.getSSType();
    			VWSLog.dbg("schemeType :"+schemeType);
    			//Commented below lines by Srikanth on 15 Feb 2019 for multiple domain login should not remove any text after "."
    			/*if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS) ||
    					schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL) )
    			{
    				int index = user.indexOf(".");

    				if (index > 0 && retAuthenticate == 1){
    					actualUser = user;
    					//Condition added to fix the username with dot fro change password CV83.1
    					if (dbPassFlag != 1) {
    						user = user.substring(0, index);
    					}
    				}

    				//User table is updated with userPricipalName. No need to get commonName
    				//user = directory.getCommonName(user, pass);
    			}*/
    			/**CV2019 merges from CV10.2 **/
    			actualUser = user;
			
				if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
					String domainName="";
					if(ldapUserDN.length()>0){
		    			String domainCoponent=ldapUserDN.substring(ldapUserDN.indexOf("DC"),ldapUserDN.length());
		    			String[] splittedDC=domainCoponent.split(",");
		    			for(String dcval:splittedDC){
		    				dcval=dcval.substring(dcval.indexOf("=")+1,dcval.length());

		    				domainName=domainName+dcval+".";
		    			}
		    			domainName=domainName.substring(0,domainName.lastIndexOf("."));
		    			VWSLog.dbg("domainName :"+domainName);
		    		}
		    		//Removed +"@"+domainName from below line 
					//sid = startSession(user+"@"+domainName, ip, type, port,stubIPAddress, actualUser);
					sid = startSession(user, ip, type, port,stubIPAddress, actualUser);
				}else{
					sid = startSession(user, ip, type, port,stubIPAddress, actualUser);
				}
    			VWSLog.dbg("login method after startsession :   "+sid);
    		}
    	}
    	
    	if(!(user.equals("ARSAdmin")) ){
    		if(!(user.equals("DWSAdmin"))){
    			if(!(user.equals("VNSAdmin")))
    			if(!(user.equals("CSSAdmin")))
    			{
    				int retval = checkLoginUser(user,pass,type);
    				if(retval==-45)
    					sid= UserDoesNotExistINDB;
    			}
    		}
    	}
    	if (sid == NoMoreLicenseSeats){
    		/*Included clientname,ipandusername for nomorelicense seats
    		 * notification mail
    		 * Modified by Madhavan
    		 */
    		if(type==Client.Fat_Client_Type){
				message = resourceManager.getString("RampServMsg.NOMoreLiceseDTC")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
			else if(type==Client.Web_Client_Type){
				message = resourceManager.getString("RampServMsg.NOMoreLiceseWebTop")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
			else if(type==Client.Adm_Client_Type){
				//VWSLog.add("adminmessage");
				message = resourceManager.getString("RampServMsg.NOMoreLiceseAdmin")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
    		else if(type==Client.AIP_Client_Type){
    			message = resourceManager.getString("RampServMsg.NoMoreLicenseAIP")+" "+user+" "+
    					resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    		}
    		else if(type==Client.Fax_Client_Type){
    			message = resourceManager.getString("RampServMsg.NoMoreLicenseAIP")+" "+user+" "+
    					resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    		}
    		else if(type==Client.WebL_Client_Type){
				message = resourceManager.getString("RampServMsg.NOMoreLiceseWebLite")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}

			else if(type==Client.IXR_Client_Type){
				message = resourceManager.getString("RampServMsg.NoMoreLicenseIXR")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
			else if(type==Client.NFat_Client_Type){
				message = resourceManager.getString("RampServMsg.NOMoreLiceseDTC")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
			else if(type==Client.sdk_Client_Type){
				message = resourceManager.getString("RampServMsg.NoMoreLicenseSDK")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
			else if(type==Client.Ars_Client_Type){
				message = resourceManager.getString("RampServMsg.NoMoreLicenseARS")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
			else if(type==Client.NWebL_Client_Type){
				message = resourceManager.getString("RampServMsg.NOMoreLiceseWebLite")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
    		else if(type==Client.EAS_Client_Type){
    			message = resourceManager.getString("RampServMsg.NoMoreLicenseEAS")+" "+user+" "+
    					resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    		} else if(type==Client.SubAdm_Client_Type){
    			message = resourceManager.getString("RampServMsg.NOMoreLiceseSubAdmin")+" "+user+" "+
    					resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    		}
    	
    		else if(type==Client.Professional_WebAccess_Client_Type){
    			if(isProfessionalExceeds())
    				message = resourceManager.getString("RampServMsg.NoMoreLicenseproffessional")+" "+user+" "+
    						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    		}
    		else if(type==Client.WebAccess_Client_Type){
    			message = resourceManager.getString("RampServMsg.WebAccess")+" "+user+" "+
    					resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    		}
    		else if(type==Client.Custom_Label_WebAccess_Client_Type){
    			message =  resourceManager.getString("RampServMsg.CustLblWebAccess")+" "+user+" "+
    					resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
    		}
			else if(type==Client.Notification_Client_Type){
				message = resourceManager.getString("RampServMsg.NotificationService")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}else if(type==Client.ContentSentinel_client_Type) {
				message = resourceManager.getString("No more license for CSServer")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}else if(type==Client.CSWebaccess_client_Type) {
				message = resourceManager.getString("RampServMsg.CSWebAccess")+" "+user+" "+
						resourceManager.getString("RampServMsg.LoginFromIP")+" "+ip;
			}
			else{
				message = resourceManager.getString("RampServMsg.NOMoreLicese");
			}
    		VWSLog.war(message);
    		VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    	}
    	if (sid > 0) 
    	{
    		VWSLog.add(room.getName() + ": Created session id = " + sid +
    				" for " +"<"+user+">"+ getSidType(type) + "@" +"<"+ ip +">" );

    		addATRecord(sid, user, ip, sid, VWATEvents.AT_OBJECT_LOGIN, 
    				VWATEvents.AT_LOGIN, "Success");
    	}
    	else
    	{
    		VWSLog.add(room.getName() + 
    				": Failed to create session for '"+"< "+user+" > @ " + ip + "'");
    		addATRecord(sid, user, ip, 1, VWATEvents.AT_OBJECT_LOGIN, 
    				VWATEvents.AT_LOGIN, (sid != NoMoreLicenseSeats? "Failed" : 
    				"Failed - No more license seats"));  
    	}
    	return sid;
    }
    
    public int authenticateUser(int sid, String user, String pass){
    	if (!assertSession(sid)) return invalidSessionId;
        if(byPassSecurity()) {
            return 1;
        }
        int retAuthenticate = directory.AuthenticateUser(user, pass);        
        return retAuthenticate;
    }
    public int getConnectedClients(int sid, Vector users)
    {
    	//VWSLog.err("before assertAdminSession in getConnectedClients");
    	if (!assertAdminSession(sid)){
    		VWSLog.dbg("Invalid session inside getConnectedClients from assertAdminSession"+sid);
    		if (!assertSubAdminSession(sid)){
    			VWSLog.dbg("Invalid session inside getConnectedClients from assertSubAdminSession"+sid);
    			return invalidSessionId;
    		}
    	}
        users.addAll(clients.values());
        VWSLog.dbg("users Vector inside getConnectedClients"+users);
        VWSLog.dbg("after assertAdminSession in getConnectedClients sid "+ sid+" users.size() "+users.size()+ " clients.size "+ clients.size());
        //VWSLog.err("after assertAdminSession in getConnectedClients sid "+ sid+" users.size() "+users.size()+ " clients.size "+ clients.size());
        for(int i=0; i < users.size(); i++)
        {
            Client client = (Client) users.get(i);
            VWSLog.dbg("assertAdminSession in getConnectedClients client.getId() "+ client.getId()+" - " +client.getUserName());
            //VWSLog.err("assertAdminSession in getConnectedClients client.getId() "+ client.getId()+" - " +client.getUserName());
            if ( client.getId() == sid)
            {
                client.setCaller(true);
                break;
            }
        }
        return NoError;
    }
    public int logoutClient(int sid, int clientsid)
    {
        if (!assertAdminSession(sid)){
        	VWSLog.dbgCheck("Invalid session inside logoutClient from assertAdminSession"+sid);
        	return invalidSessionId;
        }
        dispatchClientEvent(clientsid, VWEvent.ADMIN_DISCONNECT);
        if(aipActiveClient.containsKey(sid)){
        	VWSLog.dbg("Inside logoutClient before removing aipActiveClient::::: "+aipActiveClient);
        	aipActiveClient.remove(sid);
        	VWSLog.dbg("Inside logoutClient after removing aipActiveClient::::: "+aipActiveClient);
        }
        endSession(clientsid);
        addATRecord(sid,sid,VWATEvents.AT_OBJECT_LOGIN, VWATEvents.AT_LOGIN, 
                                                        "Success By Administration"); 
        return NoError;
    }
    public void logoutAllClient()
    {
        Enumeration keys = clients.keys();
        while (keys.hasMoreElements())
        {
            int sid = ((Integer) keys.nextElement()).intValue();
            dispatchClientEvent(sid, VWEvent.ADMIN_DISCONNECT);
            addATRecord(sid,sid,VWATEvents.AT_OBJECT_LOGIN, VWATEvents.AT_LOGIN, 
                                                        "Success By Manager");
            VWSLog.dbgCheck("Before end session of logout all clients1");
            if(aipActiveClient.containsKey(sid)){
            VWSLog.dbg("Inside logoutAllClient before removing aipActiveClient::::: "+aipActiveClient);         
            aipActiveClient.remove(sid);
            VWSLog.dbg("Inside logoutAllClient after removing aipActiveClient::::: "+aipActiveClient);         
            }
            endSession(sid);
        }
    }
// To Solve the Issue 501, logout all client without calling addATRecord. This method will execute when Database is not available.
    public void logoutClients()
    {
        Enumeration keys = clients.keys();
        while (keys.hasMoreElements())
        {
            int sid = ((Integer) keys.nextElement()).intValue();
            dispatchClientEvent(sid, VWEvent.ADMIN_DISCONNECT);
            VWSLog.dbgCheck("Before end session of logout all clients2");
            if(aipActiveClient.containsKey(sid)){
                VWSLog.dbg("Inside logoutClients before removing aipActiveClient::::: "+aipActiveClient);         
                aipActiveClient.remove(sid);
                VWSLog.dbg("Inside logoutClients after removing aipActiveClient::::: "+aipActiveClient);         
                }
            endSession(sid);
        }
    }
// End Of Fix
    public int logout(int sid)
    {
    	//int flag = isRoomIdle(sid, false); 
    	VWSLog.dbgCheck("Logout method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;        
        addATRecord(sid, sid, VWATEvents.AT_OBJECT_LOGIN, VWATEvents.AT_LOGOUT, 
                                                                     "Success");
        try{
	        Client client = (Client) clients.get(sid);
	        if(client.getConnectedStatus() == IdleTimeout){
	        	VWSLog.dbgCheck("Before calling end session");
	        	endSession(sid, IdleTimeout);
	        }
	        else{
	        	  if(aipActiveClient.containsKey(sid)){
	                  VWSLog.dbg("Inside logout before removing aipActiveClient::::: "+aipActiveClient);         
	                  aipActiveClient.remove(sid);
	                  VWSLog.dbg("Inside logout after removing aipActiveClient::::: "+aipActiveClient);         
	                  }
	        	endSession(sid);
	        }
        }catch(Exception ex){endSession(sid);}
        return NoError;
    }
    //---------------------------Sessions Private-------------------------------
    private String getSidType(int type)
    {
        switch (type)
        {
            case 1:
                return " (DT) ";
            case 2:
                return " (VWO) ";
            case 3:
                return " (AW) ";
            case 4:
                return " (AIP) ";
            case 5:
                return " (EW) ";
            case 8:
                return " (IXR) ";  
            case 9:
                return " (DT) ";
            case 12:
                return " (SDK) "; 
            case 13:
                return " (ARS) ";         
            case 14:
                return " (DWS) ";     
            case 15:
                return " (VWO) ";     
            case 16:
                return " (EAS) ";   
            case 17:
                return " (VWA) ";  
            case 21:
            	return " (FAX) ";
            default: 
                return "";
        }
    }
    private void dispatchClientEvent(int sid, int eid)
    {
        Client client = getClient(sid);
        ServerSchema ss = client.getSchema();
        // Check that stubIPaddress is different from address, If it is different call is made from Terminal Service Client.
        // Get the MDSS object using stubIPaddress.
        MDSS dss = null;
        String address = ss.address;
        VWSLog.dbgCheck("ss.address:::::::"+ss.address+"ss.stubIPaddress::::::::::!!!!!!"+ss.stubIPaddress+"ss.stubIPaddress.length()::::::::::::::::"+ss.stubIPaddress.length());
        if (!ss.address.equalsIgnoreCase(ss.stubIPaddress) && ss.stubIPaddress.length()>0) {            	
        	ss.address = ss.stubIPaddress;            	            	
        }
        dss = (MDSS) Util.getServer(ss);
        ss.address = address;
        if (dss != null)
        {
            VWEvent event = new VWEvent(myServer.address, room.getName(), 
                                                                      eid, sid);
            try
            {
                dss.dispatchEvent(event,client.getClientType());
            }
            catch(Exception e){}
        }
    }
    private int startSession(String user, String ip, int type, int port,String stubIPAddress)
    {
	return startSession(user, ip, type, port, stubIPAddress, "");
    }
    private int startSession(String user, String ip, int type, int port,String stubIPAddress, String actualUserName)
    {
    	VWSLog.dbg("inside startsession....");
    	// Pass the stubIPaddress to Client Object.
    	Client client = null;
    	//VWSLog.err("user in startSession "+user);
    	/*    	String schemeType = VWSPreferences.getSSType();
    	//For NDS loging user name can be entered with context values. LDAP also added here. Valli
        if (schemeType.equalsIgnoreCase(SSCHEME_NDS)|| schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS) 
        		|| schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL))
    	//if (schemeType.equalsIgnoreCase(SSCHEME_NDS))
        {
        	if(user.indexOf(".")>0){
        		String usrAfterTrimContext =user.substring(0,user.indexOf("."));
        		client = new Client(usrAfterTrimContext, ip, type,stubIPAddress);
        	}
        	else{
        		client = new Client(user, ip, type,stubIPAddress);
        	}
        }else*/
    	client = new Client(user, ip, type,stubIPAddress);
    	client.setActualUserName(actualUserName);
    	/*  By pass security is not allowed for EAS and AIP client type if user name itself wrong.
    	 * Created by: Shanmugavalli.C
    	 *	Date: 02 Dec 2008*/
    	if (byPassSecurity() && !(type == Client.AIP_Client_Type) &&!(type == Client.Fax_Client_Type) && !(type == Client.EAS_Client_Type) && !(type == Client.SubAdm_Client_Type)/* && !(type==Client.Notification_Client_Type)*/ )
    	{
    		client.setAdmin(true);
    		client.setManager(true);
    	}
    	else
    	{
    		setRank(client);
    	}
    	VWSLog.dbg("type XXXXXXX :"+type);
    	VWSLog.dbg("client.isAdmin() :"+client.isAdmin());
    	VWSLog.dbg("client.isSubAdmin() :"+client.isSubAdmin());
    	VWSLog.dbg("client.isNamed() :"+client.isNamed());
    	VWSLog.dbg("client.isProfessionalNamed() :"+client.isProfessionalNamed());
    	VWSLog.dbg("client.isProfessionalConcurrent() :"+client.isProfessionalConcurrent());
    	VWSLog.dbg("client.isNamed() :"+client.isNamed());
    	VWSLog.dbg("client.isAIP() :"+client.isAIP());
    	/**Below changes are added for CV2020-Sub-Administrator and AIP license changes. Added by Vanitha.S**/
    	if ((type == Client.EAS_Client_Type || type == Client.IXR_Client_Type||type==Client.Fax_Client_Type )&& !client.isAdmin()) return accessDenied;  //AdminWise/EAS/AIP
    	if (type == Client.Adm_Client_Type && !client.isAdmin() && !client.isSubAdmin()) return accessDenied;
    	if (type == Client.AIP_Client_Type && !client.isAIP()) return accessDenied;
    	/***End of License changes CV2020*****/
    	//Access denied message modified to No more license seats in CV83B2
    	if((type == Client.Fat_Client_Type || type == Client.Web_Client_Type || type == Client.WebL_Client_Type || type == Client.Custom_Label_WebAccess_Client_Type) && client.isProfessionalNamed() && !client.isNamed()){
    		if (type == Client.Fat_Client_Type) {
    			return ViewWiseErrors.invalidDesktopClientAccess;
    		} else {
    			return ViewWiseErrors.invalidWebClientAccess;
    		}
    	}
    	/**
    	 * CV10.1 Public webaccess enhancement
    	 */
    	if((type == Client.Fat_Client_Type || type == Client.Web_Client_Type || type == Client.WebL_Client_Type || type == Client.Custom_Label_WebAccess_Client_Type) && client.isProfessionalConcurrent() && !client.isNamed()){
    		if (type == Client.Fat_Client_Type) {
    			return ViewWiseErrors.invalidDesktopClientAccess;
    		} else {
    			return ViewWiseErrors.invalidWebClientAccess;
    		}
    	}    	
    	VWSLog.dbg("Before checking isEnterpriceConcurrent , isNamed & isAdmin for DTC client....."+(client.getClientType()== Client.Fat_Client_Type &&!client.isEnterpriceConcurrent()&&!client.isNamed()&&!client.isAdmin()));
    	if (client.getClientType()== Client.Fat_Client_Type &&!client.isEnterpriceConcurrent()&&!client.isNamed()&&!client.isAdmin())
    	{
    		if (type == Client.Fat_Client_Type) {
    			return ViewWiseErrors.invalidDesktopClientAccess;
    		} else {
    			return ViewWiseErrors.invalidWebClientAccess;
    		}
    	}//End 
    	
    	/* Admin and Concurrent users can access Custom Label Web Access. */
    	//Check for QC Indexer user type
    	if (type == Client.IXR_Client_Type && !client.isAdmin()) return accessDenied;  //AdminWise

    	
    	
    	/**
    	 * CV10.1 publicwebaccess Enhancement.
    	 */
    	int chkFlag = 0;
    	
    		if (client.isAdmin() )
    		{
    			if(type==Client.WebAccess_Client_Type||type==Client.Concurrent_WebAccess_Client_Type
    			||type==Client.Custom_Label_WebAccess_Client_Type||type==Client.Professional_WebAccess_Client_Type||type==Client.Professional_Conc_WebAccess_Client_Type||type==Client.NamedOnline_WebAccess_Client_Type) {
    			if (chkFlag == 0) 
    			{
    				if(!isEnterpriseNamedSeatsExceeds(client)){
    					VWSLog.dbg("Inside concurrent webaccess seats exceeds ....");
    					type = Client.NamedOnline_WebAccess_Client_Type;
    					client.setClientType(Client.NamedOnline_WebAccess_Client_Type);
    					chkFlag = 1;
    				}else if(isEnterpriseNamedSeatsExceeds(client) && chkFlag == 0){
    					type = Client.NamedOnline_WebAccess_Client_Type;
    					client.setClientType(Client.NamedOnline_WebAccess_Client_Type);
    					if(isIPExist(client)){
    						type = Client.NamedOnline_WebAccess_Client_Type;
    						client.setClientType(Client.NamedOnline_WebAccess_Client_Type);
    						chkFlag = 1;
    					}else{
    						VWSLog.dbg("Inside else of concurrent  webaccess seats exceeds ....");

    						type = Client.Concurrent_WebAccess_Client_Type;
    						client.setClientType(Client.Concurrent_WebAccess_Client_Type);
    						chkFlag = 0;
    					}
    				}
    			}
    			if (chkFlag == 0) 
    			{
    				if(!isConcrWebaccessSeatsExceeds(client)){
    					VWSLog.dbg("Inside concurrent webaccess seats exceeds ....");
    					type = Client.Concurrent_WebAccess_Client_Type;
    					client.setClientType(Client.Concurrent_WebAccess_Client_Type);
    					chkFlag = 1;
    				}else if(isConcrWebaccessSeatsExceeds(client) && chkFlag == 0){
    					//VWSLog.dbg("Inside concurrent seats exceeds .....");
    					type = Client.Concurrent_WebAccess_Client_Type;
    					client.setClientType(Client.Concurrent_WebAccess_Client_Type);
    					if(isIPExist(client)){
    						type = Client.Concurrent_WebAccess_Client_Type;
    						client.setClientType(Client.Concurrent_WebAccess_Client_Type);
    						chkFlag = 1;
    					}else{
    						VWSLog.dbg("Inside else of concurrent  webaccess seats exceeds ....");

    						type = Client.Professional_WebAccess_Client_Type;
    						client.setClientType(Client.Professional_WebAccess_Client_Type);
    						chkFlag = 0;
    					}
    				}
    			}
    			if (chkFlag == 0) 
    			{
    				if (!isProfessionalExceeds() && chkFlag == 0) {
    					type = Client.Professional_WebAccess_Client_Type;
    					client.setClientType(Client.Professional_WebAccess_Client_Type);
    					chkFlag = 1;
    				}else if (isProfessionalExceeds() && chkFlag == 0){
    					type = Client.Professional_WebAccess_Client_Type;
    					client.setClientType(Client.Professional_WebAccess_Client_Type);
    					if(isIPExist(client)){
    						type = Client.Professional_WebAccess_Client_Type;
    						client.setClientType(Client.Professional_WebAccess_Client_Type);
    						chkFlag = 1;
    					}else{
    						type = Client.WebAccess_Client_Type;
    						client.setClientType(Client.WebAccess_Client_Type);
    						chkFlag = 0;
    					}
    				}
    			}
    			if (chkFlag == 0) 
    			{
    				if(!isProfessionalConcurrentExceeds(client)){
    					type = Client.Professional_Conc_WebAccess_Client_Type;
    					client.setClientType(Client.Professional_Conc_WebAccess_Client_Type);
    					chkFlag = 1;
    				}else if(isProfessionalConcurrentExceeds(client) && chkFlag == 0){  //if professional seats are not available and further check for same user and same ipaddress in hashtable which contains all logged in users information
    					type = Client.Professional_Conc_WebAccess_Client_Type;
    					client.setClientType(Client.Professional_Conc_WebAccess_Client_Type);
    					if(isIPExist(client)){
    						type = Client.Professional_Conc_WebAccess_Client_Type;
    						client.setClientType(Client.Professional_Conc_WebAccess_Client_Type);
    						chkFlag = 1;
    					}else{
    						type = Client.Concurrent_WebAccess_Client_Type;
    						client.setClientType(Client.Concurrent_WebAccess_Client_Type);
    						chkFlag = 0;
    					}

    				}  
    			}
    			if (chkFlag == 0) 
    			{
    				if(!isPublicSeatsExceeds(client))  //If public seats are available 
    				{
    					type = Client.Custom_Label_WebAccess_Client_Type;
    					client.setClientType(Client.Custom_Label_WebAccess_Client_Type);
    					chkFlag = 1;
    				}else if(isPublicSeatsExceeds(client) && chkFlag == 0){  //if public seats are not available and further check for same user and same ipaddress in hashtable which contains all logged in users information
    					type = Client.Custom_Label_WebAccess_Client_Type;
    					client.setClientType(Client.Custom_Label_WebAccess_Client_Type);
    					if(isIPExist(client)){
    						type = Client.Custom_Label_WebAccess_Client_Type;
    						client.setClientType(Client.Custom_Label_WebAccess_Client_Type);
    						chkFlag = 1;
    					}else{
    						type = Client.WebAccess_Client_Type;
    						client.setClientType(Client.WebAccess_Client_Type);
    						chkFlag = 0;
    					}

    				}
    			}
    			}

    			/*
    			if((type == Client.WebAccess_Client_Type)) 
    	    	{
	    		VWSLog.dbg("isPublicSeatsExceeds(client)::::::::::::"+isPublicSeatsExceeds(client));
	    		VWSLog.dbg("isIPExist(client)::::::::::::::::"+isIPExist(client));
	
	    		if(!isPublicSeatsExceeds(client))  //If public seats are available 
	    		{
	    			type = Client.Custom_Label_WebAccess_Client_Type;
	    			client.setClientType(Client.Custom_Label_WebAccess_Client_Type);
	    			chkFlag = 1;
	    		}else if(isPublicSeatsExceeds(client) && chkFlag == 0){  //if public seats are not available and further check for same user and same ipaddress in hashtable which contains all logged in users information
	    			type = Client.Custom_Label_WebAccess_Client_Type;
	    			client.setClientType(Client.Custom_Label_WebAccess_Client_Type);
	    			if(isIPExist(client)){
	    				type = Client.Custom_Label_WebAccess_Client_Type;
	    				client.setClientType(Client.Custom_Label_WebAccess_Client_Type);
	    				chkFlag = 1;
	    			}else{
	    				type = Client.Professional_Conc_WebAccess_Client_Type;
	    				client.setClientType(Client.Professional_Conc_WebAccess_Client_Type);
	    				chkFlag = 0;
	    			}
	
	    		}
	    		VWSLog.dbg("chkFlag before profession conc"+chkFlag);
	    		if (chkFlag == 0) 
	    		{
		    		if(!isProfessionalConcurrentExceeds(client)){
		    			type = Client.Professional_Conc_WebAccess_Client_Type;
		    			client.setClientType(Client.Professional_Conc_WebAccess_Client_Type);
		    			chkFlag = 1;
		    		}else if(isProfessionalConcurrentExceeds(client) && chkFlag == 0){  //if professional seats are not available and further check for same user and same ipaddress in hashtable which contains all logged in users information
		    			type = Client.Professional_Conc_WebAccess_Client_Type;
		    			client.setClientType(Client.Professional_Conc_WebAccess_Client_Type);
		    			if(isIPExist(client)){
		    				type = Client.Professional_Conc_WebAccess_Client_Type;
		    				client.setClientType(Client.Professional_Conc_WebAccess_Client_Type);
		    				chkFlag = 1;
		    			}else{
		    				type = Client.Concurrent_WebAccess_Client_Type;
		    				client.setClientType(Client.Concurrent_WebAccess_Client_Type);
		    				chkFlag = 0;
		    			}
		
		    		}  
	    		}
	    		if (chkFlag == 0) 
	    		{
		    		if(!isConcrWebaccessSeatsExceeds(client)){
		    			type = Client.Concurrent_WebAccess_Client_Type;
		    			client.setClientType(Client.Concurrent_WebAccess_Client_Type);
		    			chkFlag = 1;
		    		}else if(isConcrWebaccessSeatsExceeds(client) && chkFlag == 0){
		    			type = Client.Concurrent_WebAccess_Client_Type;
		    			client.setClientType(Client.Concurrent_WebAccess_Client_Type);
		    			if(isIPExist(client)){
		    				type = Client.Concurrent_WebAccess_Client_Type;
		    				client.setClientType(Client.Concurrent_WebAccess_Client_Type);
		    				chkFlag = 1;
		    			}else{
		    				type = Client.Professional_WebAccess_Client_Type;
		    				client.setClientType(Client.Professional_WebAccess_Client_Type);
		    				chkFlag = 0;
		    			}
		    		}
	    		}
	    		if (chkFlag == 0) 
	    		{
		    		if (!isProfessionalExceeds() && chkFlag == 0) {
		    			type = Client.Professional_WebAccess_Client_Type;
		    			client.setClientType(Client.Professional_WebAccess_Client_Type);
		    			chkFlag = 1;
		    		}else if (isProfessionalExceeds() && chkFlag == 0){
		    			type = Client.Professional_WebAccess_Client_Type;
		    			client.setClientType(Client.Professional_WebAccess_Client_Type);
		    			if(isIPExist(client)){
		    				type = Client.Professional_WebAccess_Client_Type;
		    				client.setClientType(Client.Professional_WebAccess_Client_Type);
		    				chkFlag = 1;
		    			}else{
		    				type = Client.WebAccess_Client_Type;
		    				client.setClientType(Client.WebAccess_Client_Type);
		    				chkFlag = 0;
		    			}
		    		}
	    		}
	    		else{
	    			if (chkFlag == 0) {
	        		type = Client.WebAccess_Client_Type;
	        		client.setClientType(Client.WebAccess_Client_Type);
	    			}
	
	    		}
	    	}*/
	    	}else {
	    		if (type == Client.Adm_Client_Type && !client.isAdmin() && client.isSubAdmin()) {
	    			client.setClientType(Client.SubAdm_Client_Type);
	    		}
	    		VWSLog.dbg("Before checking webaccess......."+((type == Client.Web_Client_Type || type == Client.WebAccess_Client_Type || type == Client.WebL_Client_Type || type == Client.Custom_Label_WebAccess_Client_Type) && ((client.isAIP() || client.isSubAdmin()) && !client.isNamedOnline())));
	    		if((type == Client.Web_Client_Type || type == Client.WebAccess_Client_Type || type == Client.WebL_Client_Type || type == Client.Custom_Label_WebAccess_Client_Type) && ((client.isAIP() || client.isSubAdmin()) && !client.isNamedOnline())){
	        		return ViewWiseErrors.invalidWebClientAccess;
	        	}
	    		if( (type == Client.WebAccess_Client_Type) &&!client.isEnterpriceConcurrent()&&!client.isProfessionalConcurrent()&& !client.isNamed() && !client.isProfessionalNamed()&&!client.isNamedOnline()&&!client.isConcurrentOnline()&&!client.isSubAdmin()&&!client.isAIP()){
	    			type = Client.Custom_Label_WebAccess_Client_Type;
	    			client.setClientType(Client.Custom_Label_WebAccess_Client_Type);
	    		}   		
	    		if(client.getClientType()==Client.WebAccess_Client_Type&&client.isConcurrentOnline()){
	    			client.setClientType(Client.Concurrent_WebAccess_Client_Type);
	    		}
	    		if(client.getClientType()==Client.WebAccess_Client_Type&&client.isNamedOnline()){
	    			client.setClientType(Client.NamedOnline_WebAccess_Client_Type);
	    		}
		    	if (client.getClientType()== Client.WebAccess_Client_Type &&client.isProfessionalConcurrent())
		    	{
		    		client.setClientType(Client.Professional_Conc_WebAccess_Client_Type);
		    	}
		    	
		    	
		    	if(type==Client.WebAccess_Client_Type && client.isProfessionalNamed()){
		    		//VWSLog.dbg("Inside professional webaccess settings");
		    		type = Client.Professional_WebAccess_Client_Type;
		    		client.setClientType(Client.Professional_WebAccess_Client_Type);
		    	}
		    	/*if(type==Client.WebAccess_Client_Type&&client.isEnterpriceConcurrent()){
		    		type=Client.Concurrent_WebAccess_Client_Type;
		    		client.setClientType(Client.Concurrent_WebAccess_Client_Type);
		    	}*/
		    	
		    	/*if(type==Client.WebAccess_Client_Type&&client.isNamed()){
		    		//VWSLog.dbg("Inside  webaccess settings");
		    		type = Client.NamedOnline_WebAccess_Client_Type;
		    		client.setClientType(Client.NamedOnline_WebAccess_Client_Type);
		    	}*/
		   
		    	//------if fat or web client client see if he is named------------------
		    	// new feature 28-2-04
		    	if (client.getClientType() == Client.Fat_Client_Type && client.isNamed())
		    	{
		    		client.setClientType(Client.NFat_Client_Type);
		    	}
		   /* 	if (client.getClientType() == Client.Web_Client_Type && client.isNamed())
		    	{
		    		client.setClientType(Client.NWeb_Client_Type);    
		    	}
		    	//License Enhancement - split the web client license seat into 4. webtop concurrent/ webtop named/ weblite concurrent/ weblite named
		    	if (client.getClientType() == Client.WebL_Client_Type && client.isNamed())
		    	{
		    		client.setClientType(Client.NWebL_Client_Type);    
		    	}*/
		
		    	//---------------------------------------------------------------------- 
		    	if (client.getClientType()== Client.Fat_Client_Type &&client.isEnterpriceConcurrent())
		    	{
		    		client.setClientType(Client.Fat_Client_Type);
		    	}
		    	
		    	if(client.getClientType()==Client.CSWebaccess_client_Type) {
		    		client.setClientType(Client.CSWebaccess_client_Type);
		    	}
		    	
		    }
    		
	    	/*Issue No : 672
	    	 * Issue Description - Add two columns Session Id,Connected Date and Time to Conneted user table in adminwise
	    	 * Developer - Nebu Alex
	    	 * ***/	
	    	java.util.Date date = new java.util.Date();
	    	Format formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aaa");
	    	String s = formatter.format(date);
	    	client.setConnectedDate(s);
	    	//End of fix
	    	/*
	    	 *Admin User can consume the named user seat when concurrent seats are not available. 
	    	 *This is applicable for DTC - Concurrent, Web Top - Concurrent and Web Lite Concurrent license seats  
	    	 */
	    	boolean seatsAvailable = isAvailableSeat(client);
	    	if (client.isAdmin()&& !seatsAvailable && (type == Client.Fat_Client_Type || type == Client.Web_Client_Type || type == Client.WebL_Client_Type || type == Client.Custom_Label_WebAccess_Client_Type || type == Client.WebAccess_Client_Type )){ //|| type == Client.Professional_WebAccess_Client_Type)){        	
	    		if (client.getClientType() == Client.Fat_Client_Type)
	    			client.setClientType(Client.NFat_Client_Type);
	    		if (client.getClientType() == Client.Web_Client_Type)
	    			client.setClientType(Client.NWeb_Client_Type);
	    		if (client.getClientType() == Client.WebL_Client_Type)
	    			client.setClientType(Client.NWebL_Client_Type);
	    		if (client.getClientType() == Client.Custom_Label_WebAccess_Client_Type  && !seatsAvailable ) {
					if (!isProfessionalExceeds()) {
						client.setClientType(Client.Professional_WebAccess_Client_Type);
					} else if (lic.getConsumed(Client.WebAccess_Client_Type) < lic.getWebAccessSeats()) {
						client.setClientType(Client.WebAccess_Client_Type);
					}
				}
		
	    			
	    		
	    			/* commented on 22 jan 2015 for check admin login
	    			 * client.setClientType(Client.WebAccess_Client_Type);
	    			 */
	    		seatsAvailable = isAvailableSeat(client);
	    	}
	    	if (!seatsAvailable) return NoMoreLicenseSeats;
	    	int sid = getAvailableSessionId(); 
	    	client.setId(sid);
	    	client.setPort(port);
	
	    	
	    	if (sid > 0 && type == Client.Adm_Client_Type && client.isSubAdmin()) {
	    		Vector result = new Vector();
	    		getSubAdminTabs(0, client.getUserName(), result);
	    		VWSLog.dbg("getSubAdminTabs result :"+result);
	    		if (result.size() == 0) {
	    			return NoSubAdminRolesFound;
	    		}
	    	}
	    	VWSLog.dbg("Before calling consume......");
	    	consume(client);
	    	clients.put(new Integer(sid), client);
	    	VWSLog.dbgCheck("Hashtable contents added"+clients);
	    	VWSServer.sidList.add(new Integer(sid));
	    	if (type == Client.Fat_Client_Type || type == Client.NWeb_Client_Type || type == Client.Web_Client_Type) unlockDocuments(sid); //only for DT clients	    	
	    	return sid;
    		}
		
    public int getClientType(int sid)  
    {
    	VWSLog.dbgCheck("addATRecord method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        if (client == null) {
        	return Error;
        }else {
        	return client.getClientType();
        }
       
    }
    	//}
    
    public String getLoggedInUser(int sid){
	VWSLog.dbg("getLoggedInUser " + sid + " : " + getClient(sid).getUserName());
	return getClient(sid).getUserName();
    }
    private void consume(Client client)
    {
        LicenseManager lm =  LicenseManagerImpl.getInstance();
        if (lm == null) return;
        try
        {
            if (!lm.hasValidLicense())
            {
            	message = resourceManager.getString(resourceManager.getString("RampServMsg.LicenseExpired"));
            	VWSLog.war(message);
            	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
                return;
            }
            lm.getLicense().consumeNew(client);
        }
        catch(LicenseException le)
        {
            VWSLog.war(le.getMessage());
            return;
        }
    }
    private boolean isConcrWebaccessSeatsExceeds(Client client){

    	boolean concurrentSeats=false;
    	if(vws == null){
    		LicenseManager lm = LicenseManagerImpl.getInstance();
    		if(lm != null){
    			try {
    				lic = lm.getLicense();
    			} catch (LicenseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
    	}else{
    		try {
    			lic = vws.getLicense();
    		} catch (RemoteException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}				
    	}
    	if(lic != null){
    		//VWSLog.dbg("lic.getWebaccessSeatsConcurrent() :::"+lic.getWebaccessSeatsConcurrent());
      		//VWSLog.dbg("lic.getConsumed(Client.Concurrent_WebAccess_Client_Type) :::"+lic.getConsumed(Client.Concurrent_WebAccess_Client_Type));
    		if( lic.getWebaccessSeatsConcurrent()==lic.getConsumed(Client.Concurrent_WebAccess_Client_Type)/*+lic.getConsumed(Client.Fat_Client_Type))*/){
    			concurrentSeats=true;
    		}
    	}
    	//VWSLog.add("concurrentSeats :::"+concurrentSeats);
		return concurrentSeats;
    
    }
    //Method added for changing the license consumption for CVWeb in the following order.
    //Enterprisenamed,Enterpriseconcurrent,professionalNamed,professionalConcurrent,publicWebaccess.
    private boolean isEnterpriseNamedSeatsExceeds(Client client){
    	VWSLog.dbg("inside is enterprise named seats .....");
    	boolean enterPriseNamedSeats=false;
    
    	if(vws == null){
    		LicenseManager lm = LicenseManagerImpl.getInstance();
    		if(lm != null){
    			try {
    				lic = lm.getLicense();
    			} catch (LicenseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
    	}else{
    		try {
    			lic = vws.getLicense();
    		} catch (RemoteException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}				
    	}

    	if(lic != null){
    	//	int totalConsumedEnterpriseNamed=lic.getConsumed(Client.NFat_Client_Type) +lic.getConsumed(Client.NWeb_Client_Type)+lic.getConsumed(Client.NWebL_Client_Type)+lic.getConsumed(Client.WebAccess_Client_Type);
    		//VWSLog.dbg("lic.getWebAccessSeats() ....."+lic.getWebAccessSeats());
    		//VWSLog.dbg("totalConsumedEnterpriseNamed ....."+totalConsumedEnterpriseNamed);
    		if(lic.getNamedOnlineWebaccessSeats()==lic.getConsumed(Client.NamedOnline_WebAccess_Client_Type) ){
    			enterPriseNamedSeats=true;
    		}
    	}
		return enterPriseNamedSeats;
    
    }
    
    
    private boolean isIPExist(Client client){
    	boolean ipExist=false;
    	if(vws == null){
    		LicenseManager lm = LicenseManagerImpl.getInstance();
    		if(lm != null){
    			try {
    				lic = lm.getLicense();
    			} catch (LicenseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
    	}else{
    		try {
    			lic = vws.getLicense();
    		} catch (RemoteException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}				
    	}
    	if(lic != null){
    		ipExist=lic.isIPExists(client);
    	}
		return ipExist;
    }
    
    private boolean isPublicSeatsExceeds(Client client){
    	boolean publicSeats=false;
    	if(vws == null){
    		LicenseManager lm = LicenseManagerImpl.getInstance();
    		if(lm != null){
    			try {
    				lic = lm.getLicense();
    				} catch (LicenseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    				}
    		}
    	}else{
    		try {
    			lic = vws.getLicense();
    			} catch (RemoteException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}				
    	}
    	if(lic != null){
    		
    		if(lic.getCustomLabelWebAccessSeats()==lic.getConsumed(Client.Custom_Label_WebAccess_Client_Type)){
    				publicSeats=true;
    		}
    	}
		return publicSeats;
    }
    
    
    private boolean isProfessionalConcurrentExceeds(Client client){
    	VWSLog.dbg("Inside professional concurrent seats exceeds:::::");
    	boolean professionalConcurrent=false;
    	if(vws == null){
    		LicenseManager lm = LicenseManagerImpl.getInstance();
    		if(lm != null){
    			try {
    				lic = lm.getLicense();
    			} catch (LicenseException e) {
    				// TODO Auto-generated catch block
    				e.printStackTrace();
    			}
    		}
    	}else{
    		try {
    			lic = vws.getLicense();
    		} catch (RemoteException e) {
    			// TODO Auto-generated catch block
    			e.printStackTrace();
    		}				
    	}
    	if(lic != null){
			
			int professionalConurrentSeats = lic.getProfessionalConcWebAccessSeats();
			int consumedProfConcrSeats = 0;
			
			if(lic.getConsumed(Client.WebAccess_Client_Type) <= professionalConurrentSeats){
				consumedProfConcrSeats = lic.getConsumed(Client.Professional_Conc_WebAccess_Client_Type);
			}
			else if(lic.getConsumed(Client.Professional_Conc_WebAccess_Client_Type) > professionalConurrentSeats){
				consumedProfConcrSeats = professionalConurrentSeats;
			}
		
			if(consumedProfConcrSeats==professionalConurrentSeats){
				professionalConcurrent=true;
			}
		}
		return professionalConcurrent;
    }
    
    
    private boolean isProfessionalExceeds()
    {
    	 /*
         * To get the get the professional Webaccess count for no more license 
         * notification mail
         * 
         */
    	boolean professional=false;
        	if(vws == null){
        		LicenseManager lm = LicenseManagerImpl.getInstance();
        		if(lm != null){
        			try {
        				lic = lm.getLicense();
        			} catch (LicenseException e) {
        				// TODO Auto-generated catch block
        				e.printStackTrace();
        			}
        		}
        	}else{
        		try {
        			lic = vws.getLicense();
        		} catch (RemoteException e) {
        			// TODO Auto-generated catch block
        			e.printStackTrace();
        		}				
        	}
        	if(lic != null){
				int consumedWebAccessSeats =0;
				int professionalSeats = lic.getProfessionalWebAccessSeats();
				int consumedProfessionalSeats = 0;
				
				/*if(lic.getConsumed(Client.WebAccess_Client_Type) <= professionalSeats){
					consumedProfessionalSeats = lic.getConsumed(Client.WebAccess_Client_Type);
				}
				else if(lic.getConsumed(Client.WebAccess_Client_Type) > professionalSeats){
					consumedProfessionalSeats = professionalSeats;
					consumedWebAccessSeats = lic.getConsumed(Client.WebAccess_Client_Type) - consumedProfessionalSeats;
				}*/
				
				if(lic.getConsumed(Client.WebAccess_Client_Type) <= professionalSeats){
					consumedProfessionalSeats = lic.getConsumed(Client.Professional_WebAccess_Client_Type);
				}
				else if(lic.getConsumed(Client.Professional_WebAccess_Client_Type) > professionalSeats){
					consumedProfessionalSeats = professionalSeats;
				}
			
				/*code changes done by Madhavan
				 * if(lic.getConsumed(Client.Professional_WebAccess_Client_Type) <= professionalSeats){
					consumedProfessionalSeats = lic.getConsumed(Client.Professional_WebAccess_Client_Type);
				}*/
				
				if(consumedProfessionalSeats==professionalSeats){
					professional=true;
				}
			}
			return professional;
    }
    private boolean isAvailableSeat(Client client)
    {
        LicenseManager lm =  LicenseManagerImpl.getInstance();
        if (lm == null) return false;

        try
        {
            if (!lm.hasValidLicense())
            {
            	message = resourceManager.getString("RampServMsg.LicenseExpired");
            	VWSLog.war(message);
            	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
                return false;
            }
            boolean available = lm.getLicense().isAvailableSeatNew(client);
            //if(!available){
            	
	            /*ServerSchema ss = client.getSchema();
	            MDSS mdss = null;
	            String address = ss.address;
	            if (!ss.address.equalsIgnoreCase(ss.stubIPaddress) && ss.stubIPaddress.length()>0) {            	
	            	ss.address = ss.stubIPaddress;            	            	
	            }
	            mdss = (MDSS) Util.getServer(ss);
	            ss.address = address;
	            if (mdss == null || (mdss !=null && !mdss.ping())){*/
            	
	            	//VWSLog.err("  getting MDSS Server  null Rampage Server");
	            	//client.getClientType() == Client.Adm_Client_Type &&
                //VWSLog.add("available Licence  isAvailableSeat = "+available);
            
            /* Issue No 736: Changed for MDSS sessionId issue in Nevell Environment
            * Instead of using idle timeout with MDSS, It is done based on lastActiveTime 
            * , C.Shanmugavalli, Pandiyaraj.M  Date 3 Nov 2006
             */
		            if (!available){
		            	Enumeration clientTable = clients.elements();
		            	Hashtable connectedClient = new Hashtable();
		            	long now = System.currentTimeMillis();
		            	while(clientTable.hasMoreElements()){
		            		Client checkClient = (Client)clientTable.nextElement();
		            		 VWSLog.dbgCheck(" checkClient.getClientType() ::" + checkClient.getClientType() + "   ::: client.getClientType()" +  client.getClientType());
							 VWSLog.dbgCheck("checkClient.getId() " + checkClient.getId());
							 VWSLog.dbgCheck("now " + now);
							 VWSLog.dbgCheck("checkClient.getLastRequestFromClient() " + ( checkClient.getLastRequestFromClient()));
							 VWSLog.dbgCheck("now - checkClient.getLastRequestFromClient() " + (now - checkClient.getLastRequestFromClient()));
		            				            	
		            		/* Issue #754: Idle time out is removed for the clients EAS and AIP, C.Shanmugavalli, 10 Nov 2006*/
							if(checkClient.getClientType()!= Client.EAS_Client_Type && 
		            			checkClient.getClientType()!= Client.AIP_Client_Type &&
		            			checkClient.getClientType()!= Client.Ars_Client_Type &&
		            			checkClient.getClientType()!= Client.Drs_Client_Type &&
		            			checkClient.getClientType()!= Client.Notification_Client_Type &&
		            			checkClient.getClientType()!= Client.IXR_Client_Type &&
		            			checkClient.getClientType()!= Client.Fax_Client_Type &&
		            			checkClient.getClientType()!=Client.ContentSentinel_client_Type){
								if (checkClient.getLastRequestFromClient() > 0 && checkClient.getClientType() == client.getClientType())
								connectedClient.put((now - checkClient.getLastRequestFromClient()), checkClient.getId());
							}
		            	}
		            	removeOrphaned(connectedClient);
		            	available = lm.getLicense().isAvailableSeatNew(client);
		            }
		            //}
		            /*Vector orphaned = lm.getLicense().removeOrphaned();
		        for (int i = 0; i < orphaned.size(); i++)
		        {
		            int sid = ((Integer) orphaned.elementAt(i)).intValue(); 
		            VWSLog.war(room.getName() + 
		                   ": released license seat for orphaned session = " + sid); 
		            endSession(sid); 
		            }*/
		       // }
            return available;
        }
        catch(Exception le)
        {
            le.printStackTrace();
            VWSLog.war(le.getMessage());
            return false;
        }
    }
    private void removeOrphaned(Hashtable connectedClient){
		Client idleClient = null;
		try{
			LicenseManager lm =  LicenseManagerImpl.getInstance();
			long now = System.currentTimeMillis(); 
			if (connectedClient  != null && connectedClient.size() > 0){				
				int cnt=0;
				Long clientIdleTime[] = new Long[connectedClient.size()];
		        for(Enumeration e = connectedClient.keys(); e.hasMoreElements();){
		        	clientIdleTime[cnt] = Long.valueOf(e.nextElement().toString());
		        	cnt++;
		        }
				Arrays.sort(clientIdleTime);
				idleClient =(Client) clients.get(connectedClient.get(clientIdleTime[clientIdleTime.length - 1]));

				if (idleClient != null && (now - idleClient.getLastRequestFromClient()) >= (1000L * 60L * 3)){
			 		int sid = idleClient.getId();
					//int connectionStatus = isRoomIdle(sid, false); 
					//if (connectionStatus == IdleTimeout){
						lm.getLicense().release(idleClient);
						message = room.getName() + 
                        resourceManager.getString("RampServMsg.OrphanedSession")+" "+ sid;
		            	VWSLog.war(message);
		            	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		                 endSession(sid, IdleTimeout); 
					//}
				}
			}
		}catch(Exception ex){}    	
    }
     private void release(Client client)
    {
        LicenseManager lm =  LicenseManagerImpl.getInstance();
        if (lm == null) return;
        try
        {
            if (!lm.hasValidLicense())
            {
            	message = resourceManager.getString("RampServMsg.LicenseExpired");
            	VWSLog.war(message);
            	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
                return;
            }
            lm.getLicense().release(client);
        }
        catch(LicenseException le)
        {
            VWSLog.war(le.getMessage());
            return;
        }
    }
    private int getAvailableSessionId()
    {
   /*     int size = baseSessionId + clients.size();
        
         * Every time getting new session ID and not reusing sessionids
         * Pandiyaraj.M , C.Shanmugavalli Date 3 Nov 2006
         
        
        VWSLog.add(" getting session id "+baseSessionId +" "+clients.size());
        try{
	        if(clients != null && clients.size()>0){
		        int lastSessionId = ((Client)clients.get(new Integer(size))).getId() + 1;
		        for(int i = baseSessionId; i <= size ; i++){
		        	if(!clients.containsKey(new Integer(lastSessionId))) return lastSessionId;
		        	else lastSessionId++;
		        }        
		        return lastSessionId++;
	        }else
        return size + 1;
        }catch(Exception e){
        	//VWSLog.err(" Error in getting getAvailableSessionId "+e.getMessage());
        	return size + 1;
        }
        
        VWSLog.add(" getting session id "+baseSessionId +" "+clients.size());
        int tempSessionId = 0;
        if(clients != null && clients.size()>0){
        	int i = baseSessionId;
	        for(i = baseSessionId; i <= size ; i++){
	        	if(!clients.containsKey(new Integer(i))){
	        		if(tempSessionId<i)
	        			tempSessionId = i;
	        	}
	        }
	        tempSessionId = i+1;
        }else{
        	tempSessionId = size+1;
        }
        VWSLog.add(" getting tempSessionId id "+tempSessionId);
        return tempSessionId;	
        //return size + 1;
*/  // Checkin the session id is used, if it is used generate the next session id and give to the requested user 
	baseSessionId++;
	boolean isSidUsed = true;
	while (isSidUsed){
		if (VWSServer.sidList.contains(new Integer(baseSessionId)))
			baseSessionId++;
		else
			isSidUsed = false;
	}
    return 	baseSessionId;
    }  
    
    /* For endSession one more signature added to add idleTimeOut log msg - Valli*/
    private void endSession(int sid) 
    {
    	endSession(sid, 0); 
    }
    
    private void endSession(int sid, int isByIdleTimeOut) 
    {
        Integer key = new Integer(sid);
        VWSLog.dbg("is client contains key :"+clients.containsKey(key));
        if (!clients.containsKey(key)) return;
        Client client = (Client) clients.get(key);
        String byIdleTimeOut = null;
        if(isByIdleTimeOut == IdleTimeout && 
        	client.getClientType() != Client.EAS_Client_Type && 
        	client.getClientType() != Client.AIP_Client_Type && 
        	client.getClientType() != Client.Ars_Client_Type && 
        	client.getClientType() != Client.Drs_Client_Type && 
        	client.getClientType() != Client.Notification_Client_Type && 
        	client.getClientType() != Client.IndexerServer&&
        	client.getClientType() != Client.Fax_Client_Type&&
        	client.getClientType()!=Client.ContentSentinelServer)
        	byIdleTimeOut = " by idle time out";
        else
        	byIdleTimeOut = "";
        release(client); 
        String ip = client.getIpAddress();
        String name = client.getUserName();
        String sidTypeName = getSidType(client.getClientType());
        VWSLog.dbg("Before removing the server session hashtable-->key-->"+key+"ip::::"+ip+"name:::"+name+"TypeName:::::"+sidTypeName);
        clients.remove(key);
        if(aipActiveClient.containsKey(key)){
            VWSLog.dbg("Inside end session before removing aipActiveClient::::: "+aipActiveClient);         
            aipActiveClient.remove(key);
            VWSLog.dbg("Inside end session  after removing aipActiveClient::::: "+aipActiveClient);         
            }
        VWSLog.add(room.getName() + ": Terminated session id = " 
                               + sid + " for client "+"'" + name +sidTypeName+ "@" + ip + "'" +byIdleTimeOut);
        VWSServer.sidList.remove(new Integer(sid));
    }
    private boolean assertSession(int id)
    {
    	VWSLog.dbgCheck("assertSession method id value::::"+id);
        if(!isClientConnected(id, false)) return false;
        return true;
    }
    private boolean assertAdminSession(int id)
    {
    	//Code added for -499 issue Fix
    	VWSLog.dbg("Call from assertAdminSession");
    	 Client client = (Client) clients.get(new Integer(id));
    	VWSLog.dbg("Return value from asserted Session");
    	//End of -499 issue fix
        if(!isClientConnected(id, true)) return false;
        VWSLog.dbg("Client isAdmin"+!client.isAdmin());
        return true;
    }
    
    private boolean assertSubAdminSession(int id)
    {
    	//Code added for -499 issue Fix
    	VWSLog.dbg("Call from assertSubAdminSession");
    	 Client client = (Client) clients.get(new Integer(id));
    	VWSLog.dbg("Return value from sub admin asserted Session");
    	//End of -499 issue fix
        if(!isClientConnected(id, false, true)) return false;
        VWSLog.dbg("Client isSubAdmin"+!client.isSubAdmin());
        return true;
    }
    /**
     * Method Added for -499 issue fix
     * @param id
     * @return
     */
    private boolean assertNotifySession(int id)
    {
    	VWSLog.dbgCheck("assertNotifySession method id value::::"+id);
        if(!isNotifyClientConnected(id, false)) return false;
        return true;
    }
    
    public boolean isClientConnected(int sid, boolean admin) {
    	return isClientConnected(sid, admin, false);
    }

    public boolean isClientConnected(int sid, boolean admin, boolean isSubAdmin) 
    {
        if (sid == 0) return true;
        if( clients.containsKey(new Integer(sid)) )
        {
            Client client = (Client) clients.get(new Integer(sid));
            if (admin && !client.isAdmin()) {
            	return false; 
            } else if (isSubAdmin && !client.isSubAdmin()) {
            	return false; 
            }
            client.setLastActivateDate();
            // Add the last activated time for database level.
            database.setLastActivateDate();
            return true;
        }
        return false;
    }
    /**
     * Method added for -499 issue Fix
     * @param sid
     * @param admin
     * @return
     */
    public boolean isNotifyClientConnected(int sid, boolean admin) 
    {
    	VWSLog.dbgCheck("isClientConnected beginning sid value::::"+sid);
    	if (sid == 0) return true;
    	if( clients.containsKey(new Integer(sid)) )
    	{
    		Client client = (Client) clients.get(new Integer(sid));
    		VWSLog.dbgCheck("isClientConnected inside if sid value::::"+client.getId());
    		client.setLastActivateDate();
    		// Add the last activated time for database level.
    		database.setLastActivateDate();
    		return true;
    	}
    	return false;
    }
    //-----------------------------Node Methods---------------------------------
    public int getNodePath(int sid, int nodeId, Vector result){
    	VWSLog.dbgCheck("getNodePath method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(nodeId);       
        DoDBAction.get(getClient(sid),  database, "VWGetNodePath", param, result);
        return NoError;    	
    }
    public int getNodeProperties(int sid, Node node)
    {
    	VWSLog.dbgCheck("getNodeProperties method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        return getNodePropertiesByNodeID(sid, node);
    }
    // Fixing Issue 735, add the sort argument to retrieve the data based on sort value. Sort will be done based on 'sort' value
    public int getNodeContents(int sid, int nid, int coid, Vector nodes, int sort)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getNodeContents method sid val--->" + sid);
        Client client = getClient(sid);
        //CV10 SA user is set and login as admin user cabinet should not list for admin user if navigate permission is not set
        String admin ="0";
        if((isSecurityAdmin(sid)==false) && (client.isAdmin() == true)&&(enableSecurityAdmin(sid)==true)) {
        	admin = "0";
        }
        else {
        	admin = (client.isAdmin()? "1" : "0");
        }
        String levels = "0";
        String param = admin + Util.SepChar + 
                       String.valueOf(nid) + Util.SepChar + 
                       String.valueOf(coid) + Util.SepChar + 
                       client.getUserName() + Util.SepChar +
                       levels + Util.SepChar +
                       sort;
        DoDBAction.get(getClient(sid),  database, "Node_GetChildren", param, nodes);
        return NoError;
    }
    /**
     * getNodeContentsWS() : Added for getting node children along with node type for web service call.
     * @param sid
     * @param nid
     * @param coid
     * @param nodes
     * @param sort
     * @return
     */
    public int getNodeContentsWS(int sid, int nid, int coid, Vector nodes, int sort)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getNodeContentsWS method sid val--->" + sid);
        Client client = getClient(sid);
        String admin = (client.isAdmin()? "1" : "0");
        String levels = "0";
        String param = admin + Util.SepChar + 
                       String.valueOf(nid) + Util.SepChar + 
                       String.valueOf(coid) + Util.SepChar + 
                       client.getUserName() + Util.SepChar +
                       levels + Util.SepChar +
                       sort;
        DoDBAction.get(getClient(sid),  database, "Node_GetChildrenWS", param, nodes);
        return NoError;
    }
    public int getNodeContentsWithAcl(int sid, int nid, int coid, Vector nodes, int sort)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getNodeContentsWithAcl method sid val--->" + sid);
        Client client = getClient(sid);
        String admin = (client.isAdmin()? "1" : "0");
        String levels = "0";
        String param = String.valueOf(nid) + Util.SepChar + 
                       String.valueOf(coid) + Util.SepChar + 
                       client.getUserName() + Util.SepChar +
                       sort;
        DoDBAction.get(getClient(sid),  database, "VWNode_GetChildrenWithAcl", param, nodes);
        return NoError;
    }
    public int copyNodeSecurity(int sid, int nid, String ip, String userGroup, String permission)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("copyNodeSecurity method sid val--->" + sid);
        Client client = getClient(sid);

        String param = String.valueOf(nid) + Util.SepChar + 
        			   permission + Util.SepChar + 
                       ip + Util.SepChar +
                       userGroup;
        Vector result = new Vector();
        DoDBAction.get(getClient(sid),  database, "VWNodeSecurityCopy", param, result);
        int resultStatus = 0;
        if (result != null && result.size() > 0){
        	resultStatus = Integer.parseInt(result.get(0).toString().trim());        	
        }
        return resultStatus;
    }

/* Issue Description - AIP Hangs when processing large rooms
*  Developer - Nebu Alex
*  Code Description - This function makes a database call and
*					- returns node information, 
* Arguments required - Nodes parentid and nodes name
* 
*/
 public int getNodeDetails(int sid, String parentid, String nodeName,Vector nodes)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getNodeDetails method sid val--->" + sid);
        Client client = getClient(sid);
        String param = parentid + Util.SepChar + nodeName;       
        DoDBAction.get(getClient(sid),  database, "GetNodeDetails", param, nodes);
        return NoError;
    }
// End of  Code Change
    public boolean roomHasDocuments()
    {
        Vector count = new Vector();
        DoDBAction.get(null, database, "CountDocuments", "", count);
        if (count.size() > 0 && Util.to_Number((String) count.get(0)) > 0)
            return true;
        else
            return false;
    }
    public int getNodeParents(int sid, int nid, Vector nodes)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getNodeParents method sid val--->" + sid);
        String param = String.valueOf(nid) + Util.SepChar + "0";
        DoDBAction.get(getClient(sid),  database, "GetNodeParents", param, nodes);
        return  NoError;
    }
    public int createNode(int sid, Node node, int withUniqueName) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        Vector nodes = new Vector();
        String param = "";
        
        int retValue = 0;
        Vector path = new Vector();
        String nodePath = "";
        
        if(withUniqueName==0)
        {
            param = client.getUserName()  + Util.SepChar + 
            		client.getIpAddress() + Util.SepChar + 
            		node.getName()       + Util.SepChar + 
                    "0" + Util.SepChar + 
                    String.valueOf(node.getParentId());
            DoDBAction.get(getClient(sid),  database, "Node_Add", param, nodes);
            if( nodes.size() > 0 )
            {
                
                
                StringTokenizer st = new StringTokenizer(nodes.get(0).toString(), Util.SepChar);
                int nodeId = Util.to_Number(st.nextToken());
                if (nodeId == -1) return storageNotDefined;
                if (nodeId == -2) return parentNotFound;
                
                String nodeLevel = "";
                if(nodeId>0){
                	//It is send the level of the node - Cabinet, Drawer, Folder, Sub Folder, Document, Shortcut 
                	nodeLevel = st.nextToken();
                	//node path
                	nodePath = st.nextToken();
                }
                try{
                	int settingsId = -1;
                	if (node.getType() == 1) {
                		message = "Document '"+node.getName()+"' added to '"+nodePath+"'";
                		settingsId = CheckNotificationExist(sid, node.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName());
                	}
                	else if (node.getType() == 0 && (nodeLevel.equalsIgnoreCase("Folder") || nodeLevel.equalsIgnoreCase("Sub Folder"))) 
                	{
                		message = "Folder '"+node.getName()+"' added to '"+nodePath+"'";
                		settingsId = CheckNotificationExist(sid, node.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName());
                	}
                	
                	if ((settingsId >= 0) && (node.getType() == 1 || node.getType() == 0)) {
                		setNotificationHistory(sid, settingsId, nodeId, VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, "-", "-", client.getUserName(), message);
                	}                	

                }catch(Exception ex){
                }
                
                return nodeId;
            }
            else
                return Error;
        }
        else
        {   
            param = String.valueOf(node.getParentId()) + Util.SepChar +
            		node.getName() + Util.SepChar +
                    (withUniqueName==2?"1":"0");
            DoDBAction.get(getClient(sid),  database, "Node_Add_UniqueName", param, nodes);
            if( nodes.size() > 0 )
                return(Integer.parseInt((String) nodes.elementAt(0)));
            else
                return Error;
        }
        
    }
    public int delNode(int sid, int nid, boolean toRecycle)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("delNode method sid val--->" + sid);
        Client client = getClient(sid);
        // Added for ARS process
        int arsType = -1;
        if(client.getClientType() == Client.Ars_Client_Type)
        	arsType = 1;
        else
        	arsType = 0;
        String param = "";
        	param = nid +Util.SepChar + (toRecycle? "1" : "0") + 
                    Util.SepChar + Util.getOneSpace(client.getIpAddress()) +
                    Util.SepChar + Util.getOneSpace(client.getUserName()) + 
                    Util.SepChar +(client.isAdmin()?"0":"0")+// For admin user also security permissions needs to set.
                    Util.SepChar +String.valueOf(arsType);
        
    	int nodeType = 0;
    	int settingsId = -1;
        Node node = new Node(nid);
        if(toRecycle) 
        {
        	try{
        		int retValue = getNodePropertiesByNodeID(sid, node);
        	}catch(Exception ex){
        		VWSLog.err("Exception while get destination node path : "+ex.getMessage());
        	}

        	settingsId = CheckNotificationExist(sid, nid, node.getType(), VNSConstants.notify_Fol_DocRemoved, client.getUserName());
        }
        if(settingsId >=0 ){
        	if(node.getType()== VNSConstants.nodeType_Folder){
        		message = "Folder '"+node.getName() +"' removed from '"+node.getPath()+"'";
        		nodeType = VNSConstants.nodeType_Folder;
        	}else if(node.getType() == VNSConstants.nodeType_Document){
        		message = "Document '"+node.getName()+"' removed from '"+node.getPath()+"'";
        		nodeType = VNSConstants.nodeType_Document;
        	}
        }
       Vector nodes = new Vector();
        //String param = Util.GetFixedString(nid, 10) + (toRecycle? "1" : "0") + 
        //               Util.GetFixedString(client.getIpAddress(), 20) +
        //               Util.GetFixedString(client.getUserName(), 50) + 
        //              (client.isAdmin()?"1":"0");
     
        DoDBAction.get(getClient(sid),  database, "Node_Delete", param, nodes);
        if (nodes.size() == 0) return DelNodeError;
        if(toRecycle) 
        {
            String str = ((String) nodes.get(0)).trim();
            if(str.equals("2") || str.equals("3") || str.equals("4")) 
                  return DelNodeError;
            if(str.equals("5"))
            	  return NodeInUse;
            if(str.equals("6"))
          	  return NodeInRetention;
            if (settingsId >= 0){
            	if (nodeType==0 || nodeType==1){
            		/**
            		 * For Document if it is inheriting Folder, then need to send node type as 0 as a folder
            		 * or For a folder notification having document event need to send the node type as 0  
            		 */
            		if(settingsId!=nid)
            			nodeType = 0;
            		
            		setNotificationHistory(sid, settingsId, nid, nodeType, VNSConstants.notify_Fol_DocRemoved, "-", "-", client.getUserName(), message);
            	}
            }
            
            //message = "Document is removed";
            //need to get the parent id.
            //addNotificationHistory(sid, nid, 0, 23, client.getUserName(), "-", "", message);
            return NoError;
        }
        for (int i = 0; i < nodes.size(); i++)
        {
            String str = (String) nodes.get(i);
            if(str.equals("0")) return DelNodeError;    //error
            if(str.equals("1")) return NoError;         // do docs to del
            if(str.equals("-1")) return NoError;        //locked
            if(str.equals("-2")) return NoError;        //sec error
            ServerSchema dss = new ServerSchema(str);
            deleteDocumentFiles(dss);
        }
        return NoError;
    }
    public int azureARSDeleteNode(int sid, int nid, boolean toRecycle){
    	if (!assertSession(sid)) return invalidSessionId;
    	VWSLog.dbgCheck("delNode method sid val--->" + sid);
    	Client client = getClient(sid);
    	// Added for ARS process
    	int arsType = -1;
    	if(client.getClientType() == Client.Ars_Client_Type)
    		arsType = 1;
    	else
    		arsType = 0;
    	String param = "";
    	param = nid +Util.SepChar + (toRecycle? "1" : "0") + 
    			Util.SepChar + Util.getOneSpace(client.getIpAddress()) +
    			Util.SepChar + Util.getOneSpace(client.getUserName()) + 
    			Util.SepChar +(client.isAdmin()?"0":"0")+// For admin user also security permissions needs to set.
    			Util.SepChar +String.valueOf(arsType);

    	int nodeType = 0;
    	int settingsId = -1;
    	Node node = new Node(nid);
    	if(settingsId >=0 ){
    		if(node.getType()== VNSConstants.nodeType_Folder){
    			message = "Folder '"+node.getName() +"' removed from '"+node.getPath()+"'";
    			nodeType = VNSConstants.nodeType_Folder;
    		}else if(node.getType() == VNSConstants.nodeType_Document){
    			message = "Document '"+node.getName()+"' removed from '"+node.getPath()+"'";
    			nodeType = VNSConstants.nodeType_Document;
    		}
    	}
    	Vector nodes = new Vector();

    	DoDBAction.get(getClient(sid),  database, "Node_Delete", param, nodes);
    	if (nodes.size() == 0) return DelNodeError;

    	return NoError;
    }
    public int getDocsToPurge(int sid, int docsCountToPurge, Vector docsToPurge)
    {
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = ""+docsCountToPurge;
    	DoDBAction.get(getClient(sid),  database, "EmptyRecycleDocuments", param, docsToPurge);
    	return NoError;
    }
    
    public int renameNode(int sid, Node node) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("renameNode method sid val--->" + sid);
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = String.valueOf(node.getId()) + Util.SepChar +
        			   node.getName() + Util.SepChar + 
        			   node.getName() + Util.SepChar + //??????
        			   client.getUserName() + Util.SepChar + 
        			   client.getIpAddress();
        DoDBAction.get(getClient(sid),  database, "RenameNode", param, ret);
        
        /*
         * Update the Notification 
         * Module 	: 	Folder
         * Event 	:	Folder Renamed.
         * NotifyId	:	8
         * NodeType : 	1 for document
         * Message	:	Properties Values changed for <Document>
         */
        
        /*String message = "Folder "+  node.getName() +" renamed by " + client.getUserName();
        addNotificationHistory(sid, node.getId(), 1, 8, client.getUserName(), "",  node.getName(), message);
        */
        return NoError;
    }
    public int copyNode(int sid, Node node, int nodeProperties)
    {
        if (!assertSession(sid)) return invalidSessionId;
        if (!pingDSSs(sid, node)) return dssInactive;
        VWSLog.dbgCheck("copyNode method sid val--->" + sid);
        Vector ret = new Vector();
        Client client = getClient(sid);
        String sourcePath = "";
        String destPath = "";
        Vector path = new Vector();
        int retValue = 0;
        int settingId = 0;
        try{
        	Node tempNode = new Node(node.getId());
        	retValue = getNodePropertiesByNodeID(sid, tempNode);

        	int currentParentId = tempNode.getParentId();
        	retValue = getNodePath(sid, currentParentId, path);
        	if(path!=null && path.size()>0)
            	sourcePath = path.get(0).toString();
        	
        	path = new Vector();
        	int copyToParentId = node.getParentId();
        	retValue = getNodePath(sid, copyToParentId, path);
        	if(path!=null && path.size()>0)
            	destPath = path.get(0).toString();
        	
        
        	//we have to send the parentId of the destination folder and always node type as folder.
        	if(tempNode.getType()==VNSConstants.nodeType_Folder){
        		message = "Folder '"+node.getName()+"' copied from '"+sourcePath+"' to '"+destPath+"'";
        		settingId = CheckNotificationExist(sid, node.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName());
        	}else if(tempNode.getType()==VNSConstants.nodeType_Document){
        		message = "Document '"+node.getName()+"' copied from '"+sourcePath+"' to '"+destPath+"'";
        		settingId = CheckNotificationExist(sid, node.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName());
        	}
        }catch(Exception ex){
        	//VWSLog.err("Exception while get source node path in copyNode: "+ex.getMessage());
        }

        
        String param = client.getIpAddress() + Util.SepChar +
        			   client.getUserName()  + Util.SepChar +
                       node.getId() + Util.SepChar +
                       node.getParentId()+ Util.SepChar +
                       // For admin user also security permissions needs to set. 
                       (client.isAdmin()?"0":"0") + Util.SepChar + nodeProperties;
        
        DoDBAction.get(getClient(sid),  database, "CopyNode", param, ret);
        

        for (int i = 0; i < ret.size(); i++)
        {
            String result = (String) ret.get(i);
            if (result.equals("0")) {
            	if (i == 0){
                    if(settingId>=0){
                		setNotificationHistory(sid, settingId, node.getId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, "-", "-", client.getUserName(), message);
                	}                
                }
            	return NoError;
            }
            
            StringTokenizer st = new StringTokenizer(result, Util.SepChar);
            int srcDocId = Util.to_Number(st.nextToken());
            int destDocId = Util.to_Number(st.nextToken());
            int nodeType = Util.to_Number(st.nextToken());
            if(nodeType==1){//Document
            	int copy =  copyDocumentFile(sid, srcDocId, destDocId);
            	if (copy == dssInactive) return copy;
            	// The following code is adding the document to indexer queue, when user is paste the documents. 
            	//Procedure will take care of checking the document is FTS enbled or not - PR            
            	addIndexerDoc(sid, destDocId, 3);
            }
            
            if (i == 0){
                if(settingId>=0){
            		setNotificationHistory(sid, settingId, destDocId, VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, "-", "-", client.getUserName(), message);
            	}                
            }            	
        }
        return NoError;
    }
    /**
     * Enhancement CV10 Azure Storage Copy from Azure to onpremises,onpremises to azure and azure to azure copydocuments.
     * @param sid
     * @param node
     * @param nodeProperties
     * @param totalPath
     * @return
     */
    public int azureCopyNode(int sid ,Node node, int nodeProperties,Vector totalPath){
    	
    	 int settingId = 0;
    	Vector ret = new Vector();
    	  if (!assertSession(sid)) return invalidSessionId;
          if (!pingDSSs(sid, node)) return dssInactive;
    	Client client = getClient(sid);
    	try{
    		String param = client.getIpAddress() + Util.SepChar +
    				client.getUserName()  + Util.SepChar +
    				node.getId() + Util.SepChar +
    				node.getParentId()+ Util.SepChar +
    				// For admin user also security permissions needs to set. 
    				(client.isAdmin()?"0":"0") + Util.SepChar + nodeProperties;

    	DoDBAction.get(getClient(sid),  database, "CopyNode", param, ret);


    		for (int i = 0; i < ret.size(); i++)
    		{
    			String result = (String) ret.get(i);
    			if (result.equals("0")) {
    				if (i == 0){
    					if(settingId>=0){
    						setNotificationHistory(sid, settingId, node.getId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, "-", "-", client.getUserName(), message);
    					}                
    				}
    				return NoError;
    			}

    			StringTokenizer st = new StringTokenizer(result, Util.SepChar);
    			int srcDocId = Util.to_Number(st.nextToken());
    			int destDocId = Util.to_Number(st.nextToken());
    			int nodeType = Util.to_Number(st.nextToken());
    			if(nodeType==1){//Document
    				// The following code is adding the document to indexer queue, when user is paste the documents. 
    				//Procedure will take care of checking the document is FTS enbled or not - PR       
    				addIndexerDoc(sid, destDocId, 3);
    			}
    			ServerSchema srcss = new ServerSchema();
    			getDSSforDoc(sid, new Document(srcDocId), srcss);
    			ServerSchema desss = new ServerSchema();
    			getDSSforDoc(sid, new Document(destDocId), desss);
    			String sourcePath=srcss.getPath();
    			String sourceStorageType=srcss.getStorageType();
    			String dstPath=desss.getPath();
    			String destStroageType=desss.getStorageType();
    			VWSLog.dbg("sourceStorageType in copyNode: "+sourceStorageType);
    			VWSLog.dbg("destStroageType in copyNode: "+destStroageType);
    			if(sourceStorageType.equals("Azure Storage")&&destStroageType.equals("Azure Storage")){
    				String compltePath=sourcePath+Util.SepChar+dstPath;
    				totalPath.add(compltePath);
    			}else if(sourceStorageType.equals("Azure Storage")&&destStroageType.equals("On Premises")){
    				if(nodeType==1){//Document
    					int copy =  copyDocumentFile(sid, srcDocId, destDocId);
    					if (copy == dssInactive) return copy;
    					// The following code is adding the document to indexer queue, when user is paste the documents. 
    					//Procedure will take care of checking the document is FTS enbled or not - PR            
    					addIndexerDoc(sid, destDocId, 3);
    				}else if(sourceStorageType.equals("On Premises")&&destStroageType.equals("Azure Storage")){
    					if(nodeType==1){//Document
    						int copy =  copyDocumentFile(sid, srcDocId, destDocId);
    						if (copy == dssInactive) return copy;
    						// The following code is adding the document to indexer queue, when user is paste the documents. 
    						//Procedure will take care of checking the document is FTS enbled or not - PR            
    						addIndexerDoc(sid, destDocId, 3);
    					}
    				}

    			}
    		}
    	}catch(Exception e){
    		VWSLog.dbg("Error while fetching copyNode path for azure Storage::::"+e.getMessage());
    		return Error;
    		
    	}
		return NoError;
    }
    public int moveNode(int sid, Node node, int nodeProperties)
    {
    	VWSLog.dbgCheck("moveNode method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        Client client = getClient(sid);
        String sourcePath = "";
        String destPath = "";
        Vector path = new Vector();
        int retValue = 0;
        int returnValue=0;

        String messageRemove = "";
        String messageCreate = "";
        
        Node tempNode = new Node(node.getId());
        
        try{
        	retValue = getNodePropertiesByNodeID(sid, tempNode);
        	
        	int currentParentId = tempNode.getParentId();
        	retValue = getNodePath(sid, currentParentId, path);
        	if(path!=null && path.size()>0)
            	sourcePath = path.get(0).toString();
        	
        	path = new Vector();
        	int moveToParentId = node.getParentId();
        	retValue = getNodePath(sid, moveToParentId, path);
        	if(path!=null && path.size()>0)
            	destPath = path.get(0).toString();
        	
        }catch(Exception ex){
        	VWSLog.err("Exception while get source node path : "+ex.getMessage());
        }
        
        int settingId_remove = 0;
        int settingId_create = 0;
        /**
         * Remove - Checking for existance of the notification in source node; We need to pass the node id.
         * Create - Checking for existance of the notiticaiton in destination node. 
         * 			We need to pass the destination parent id; which is node.getParentId();
         */
        if(tempNode.getType()==VNSConstants.nodeType_Folder){
        	messageRemove = "Folder '"+tempNode.getName()+"' removed (cut-paste operation)  from '"+sourcePath+"' to '"+destPath+"'";
        	messageCreate = "Folder '"+tempNode.getName()+"' created (cut-paste operation) from '"+sourcePath+"' to '"+destPath+"'";
        	settingId_remove = CheckNotificationExist(sid, tempNode.getId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_DocRemoved, client.getUserName());
        	settingId_create = CheckNotificationExist(sid, node.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName());
        } else if (tempNode.getType()==VNSConstants.nodeType_Document){
        	messageRemove = "Document '"+tempNode.getName()+"' removed (cut-paste operation) from '"+sourcePath+"' to '"+destPath+"'";
        	messageCreate = "Document '"+tempNode.getName()+"' created (cut-paste operation) from '"+sourcePath+"' to '"+destPath+"'";
        	settingId_remove = CheckNotificationExist(sid, tempNode.getId(), VNSConstants.nodeType_Document, VNSConstants.notify_Fol_DocRemoved, client.getUserName());
        	settingId_create = CheckNotificationExist(sid, node.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName());
        }
        
        // Added for ARS process
        int arsType = -1; 
        if(client.getClientType() == Client.Ars_Client_Type)
        	arsType = 1;
        else if(client.getClientType() == Client.Drs_Client_Type)
        	arsType = 2;
        else
        	arsType = 0;
        String param = Integer.toString(node.getId()) + Util.SepChar +
                       Integer.toString(node.getParentId()) + Util.SepChar +
                       client.getUserName() + Util.SepChar + 
                       client.getIpAddress() + Util.SepChar +
                       Integer.toString(arsType) + Util.SepChar + 
                       nodeProperties;        
        //String param = Integer.toString(node.getId()) + Util.SepChar +
        //               Integer.toString(node.getParentId()) + Util.SepChar +
        //               client.getUserName() + Util.SepChar + 
        //               client.getIpAddress();
        DoDBAction.get(getClient(sid),  database, "Node_Move", param, ret);
        
        if(settingId_remove>=0){
    		setNotificationHistory(sid, settingId_remove, tempNode.getId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_DocRemoved, "-", "-", client.getUserName(), messageRemove);
    	}
        if(settingId_create>=0){
    		setNotificationHistory(sid, settingId_create, node.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, "-", "-", client.getUserName(), messageCreate);
    	}
        
        if (ret.size() > 0)
        	returnValue= Util.to_Number((String) ret.get(0));
        return returnValue;
    }
    private boolean pingDSSs(int sid, Node node)
    {
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = node.getId() + Util.SepChar + 
        				client.getIpAddress()+ Util.SepChar + 
        				client.getUserName();
        DoDBAction.get(getClient(sid),  database, "GetNodeDocs", param, ret);
        if (ret.size() > 0)
        {
            for (int i = 0; i < ret.size(); i++)
            {
                int did = Util.to_Number((String) ret.get(i));
                ServerSchema ss = new ServerSchema();
                getDSSforDoc(sid, new Document(did), ss);
                DSS dss = getDSS(ss);
                if (dss == null) return false;
            }
        }
        return true;
    }
    private int copyDocumentFile(int sid, int srcid, int desid)
    {
        ServerSchema srcss = new ServerSchema();
        getDSSforDoc(sid, new Document(srcid), srcss);
        ServerSchema desss = new ServerSchema();
        getDSSforDoc(sid, new Document(desid), desss);
        
        DSS dss = getDSS(srcss);
        if (dss == null) return dssInactive;
        boolean withID = false; //doc has a new ID now no need for old ID folder
        try
        {
            if (dss.copyDocFolder(new Document(srcid), room.getName(), 
                                                          srcss, desss, withID)) 
                return NoError;
            else
                return documentNotFound;
        }
        catch(Exception e)
        {
            return Error;
        }
    }
    public int getNodeDocuments(int sid, int nid, int coid, int refNid, Vector documents)
    {
    	int returnValue = NoError;
    	VWSLog.dbgCheck("getNodeDocuments method sid val--->" + sid);
    	try {
    		if (!assertSession(sid)) return invalidSessionId;        
            Client client = getClient(sid);
            // For admin user also security permissions needs to set.
            String admin = (client.isAdmin()? "0" : "0");
            String param = admin + Util.SepChar + nid + Util.SepChar +        
                    client.getUserName() + Util.SepChar + String.valueOf(coid)+ Util.SepChar + String.valueOf(refNid);

            returnValue = DoDBAction.get(getClient(sid),  database, "Node_GetDocuments", param, documents);
			
		} catch (Exception ex) {
			VWSLog.err("EXCEPTION in getNodeDocuments generalErrorCode(-1) :::: "+ex.getMessage());
			returnValue = Error;
		}
    	VWSLog.dbg("return value of Node_GetDocuments procedure " + returnValue);
    	return returnValue;
    	
    }
    
    public int getAIPNodeDocuments(int sid, int nid, int coid, int docTypeId, String curIndexVal, Vector documents)
    {
    	if (!assertSession(sid)) return invalidSessionId;
    	VWSLog.dbgCheck("getAIPNodeDocuments method sid val--->" + sid);
    	Client client = getClient(sid);
        // For admin user also security permissions needs to set.
    	/**Below line is commented for CV2020 - AIP Batch input administrator changes**/
        //String admin = (client.isAdmin()? "0" : "0");
    	String admin = "1";
        String param = admin + Util.SepChar + nid + Util.SepChar + client.getUserName() + Util.SepChar + String.valueOf(coid) + Util.SepChar + docTypeId + Util.SepChar + curIndexVal;
        DoDBAction.get(getClient(sid),  database, "Node_GetAIPDocuments", param, documents);
        return NoError;
    }        
    
    public int getAllNodeDocuments(int sid, int nid, String ipAddress, Vector docs)
    {    	
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getAllNodeDocuments method sid val--->" + sid);
        Client client = getClient(sid);
		// For admin user also security permissions needs to set.
        String admin = (client.isAdmin()? "0" : "0");  
        String param = nid + Util.SepChar + Util.replaceSplCharFromTempTable(ipAddress);
        DoDBAction.get(getClient(sid),  database, "VW_GetAllNodes", param, docs);
        return NoError;
    }
    
    public int getDocumentByInfo(int sid, String nodePath, 
        String docTypeName,String indexName,String indexValue,int indexKey,
        Vector documents)
    {
    	VWSLog.dbgCheck("getDocumentByInfo method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        // For admin user also security permissions needs to set.        
        String admin = (client.isAdmin()? "0" : "0");
        String param = admin + Util.SepChar + 
        				client.getUserName() + Util.SepChar + 
        				nodePath + Util.SepChar + 
				        docTypeName + Util.SepChar +
				        indexName + Util.SepChar +
				        indexValue + Util.SepChar +
				        String.valueOf(indexKey);
        DoDBAction.get(getClient(sid),  database, "Doc_GetByInfo", param, documents);
        return NoError;
    }
    //-------------------------Document Methods---------------------------------
    public int getDocType(int sid, int did, Vector types)
    {
    	try {
    		if (!assertSession(sid)) return invalidSessionId;
    		String param = String.valueOf(did);
    		VWSLog.dbg("GetDocTypes parm :" + param);
    		return DoDBAction.get(getClient(sid),  database, "GetDocTypes", param, types);
    	} catch (Exception ex) {
    		VWSLog.err("EXCEPTION in GetDocTypes generalErrorCode(-1)  :::: "+ex.getMessage());
    		return Error;
    	}
    }
    public int getDocTypes(int sid, Vector types)
    {
        return getDocType(sid, 0, types);
    }
    public int getDocTypeIndices(int sid, int dtid, Vector indices)
    {
    	VWSLog.dbgCheck("getDocTypeIndices method sid val--->" + sid);
    	try{
    		if (!assertSession(sid)) return invalidSessionId;
            return DoDBAction.get(getClient(sid),  database, "GetDocTypeIndices", String.valueOf(dtid), indices);    		
    	} catch(Exception ex){
    		VWSLog.err("EXCEPTION in GetDocTypeIndices generalErrorCode(-1)  :::: "+ex.getMessage());
    		return Error;
    	}
    }
/*    public int populateSelectionIndex(int sid, int iid, Vector values)
    {
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(iid)+Util.SepChar;
        return DoDBAction.get(getClient(sid),  database, "GetIndexSelectionsList", param , values);
    }*/
    

    public int populateSelectionIndex(int sid, int IndexId, Vector values)
    {
    	VWSLog.dbg("populateSelectionIndex method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String room = getRoomName();
    	String key = room+"_"+IndexId;
    	int ret = NoError;

    	//VWSLog.add("Selection List Optimization enabled : "+VWSPreferences.getEnableSelection());
    	VWSLog.dbg("VWSPreferences.getEnableSelection() : "+VWSPreferences.getEnableSelection());
    	if(VWSPreferences.getEnableSelection().equalsIgnoreCase("false")){
    		//Old functionality.
    		String param = String.valueOf(IndexId)+Util.SepChar;
    		VWSLog.dbg("VWSServer.selectionIndex : "+VWSServer.selectionIndex);
    		if(VWSServer.selectionIndex != null && VWSServer.selectionIndex.size() > 0){
    			VWSServer.selectionIndex.clear();	
    			VWSServer.selectionIndex = null;
    			VWSLog.dbg("Emptied selectionIndex hashmap");
    		}
    		ret = DoDBAction.get(getClient(sid),  database, "GetIndexSelectionsList", param , values);

    	}else if(VWSPreferences.getEnableSelection().equalsIgnoreCase("true")){
    		int selValuesCount = getCacheSelectionListCount();
    		//VWSLog.add("selValusesCount : "+selValuesCount);
    		VWSLog.dbg("VWSPreferences.getSelectionLimit() :"+VWSPreferences.getSelectionLimit());
    		int regSelectionLimit = Integer.parseInt(VWSPreferences.getSelectionLimit());
    		boolean selStoreFlag = true;
    		boolean selValuesCountFlag = false;
    		boolean addSelectionIndex = true;
    		try{
    			if(VWSServer.selectionIndex == null){
    				VWSServer.selectionIndex = new HashMap<String, Vector>();
    			}

    			if(selValuesCount > regSelectionLimit){
    				/*
    				 * The selectionIndex hashmap exeeds the limit
    				 */
    				selStoreFlag = false;
    				VWSLog.add("HashMap count : "+selValuesCount+" > "+regSelectionLimit+" : registry limit");
    			}
    			VWSLog.dbg("key :"+key);
    			if (VWSServer.selectionIndex.get(key) != null){
    				VWSLog.dbg("GetIndexSelectionsList For Index Id  : "+IndexId);
    				values.addAll((Vector)VWSServer.selectionIndex.get(key));
    				addSelectionIndex = false;
    				if (values != null && values.size() > 0){
    					ret = NoError;
    				}
    			}else{
    				//VWSLog.add("Key not exist and the key is "+key);
    				String param = String.valueOf(IndexId)+Util.SepChar;
    				ret = DoDBAction.get(getClient(sid),  database, "GetIndexSelectionsList", param , values);
    			}

    			/*
    			 * Default maximum selection values count is 1000, and user can modify in registry.
    			 */
    			VWSLog.dbg("values  : "+values);
    			VWSLog.dbg("VWSPreferences.getSelectionValuesCount() :"+VWSPreferences.getSelectionValuesCount());
    			if( values.size() > Integer.parseInt(VWSPreferences.getSelectionValuesCount()) ){
    				selValuesCountFlag = true;
    			}

    			if(selStoreFlag && selValuesCountFlag && addSelectionIndex){
    				try{
    					//Clearing the hashtable values before adding new values to hashtable for batch indexing CV8.3.1
    					if(VWSServer.selectionIndex.size()>0)
    						VWSServer.selectionIndex.clear();
    					//Condition added to avoidSorting for Index id -1 and -2
    					if(IndexId!=-1&&IndexId!=-2)
    						Collections.sort(values);
    					VWSServer.selectionIndex.put(key, values);
    				}catch(Exception ex){
    				}
    			}
    		}catch(Exception ex){
    		}
    	}
    	VWSLog.dbg("populateSelectionIndex return value "+ret);
    	return ret;
    }
    private int getCacheSelectionListCount(){
    	int count = 0;
    	try{
    		if(VWSServer.selectionIndex!=null && VWSServer.selectionIndex.size()>0){
    			Iterator selectionListArray =  VWSServer.selectionIndex.values().iterator();
    			while(selectionListArray.hasNext()){
    				Vector  selectionListVector = (Vector) selectionListArray.next();
    				count += selectionListVector.size();
    			}
    		}
    	}catch(Exception ex){
    		//VWSLog.add("getCacheSelListCount : "+ex.getMessage());
    	}
    	return count;
    }
    

    public int removeSelectionIndexMap(int IndexId, Vector values){
    	try{
    		//Master reset for optimizing the selection index values.
    		if(VWSPreferences.getEnableSelection().equalsIgnoreCase("true")){
    			String roomName = getRoomName();
    			String key = roomName+"_"+IndexId;

    			if(VWSServer.selectionIndex == null)
    				VWSServer.selectionIndex = new HashMap<String, Vector>();
    			
    			boolean selValuesCountFlag = false;

    			int selValuesCount = getCacheSelectionListCount();
    			int regSelectionLimit = Integer.parseInt(VWSPreferences.getSelectionLimit());
    			/*
    			 * Need to add to Map, if the selection values count is greater than 1000 - having registry entry also.
    			 */
    			if(values!=null){
    				if( values.size() > Integer.parseInt(VWSPreferences.getSelectionValuesCount()) ){
    					selValuesCountFlag = true;
    				}

    				if(VWSServer.selectionIndex!=null && VWSServer.selectionIndex.size()>0 && VWSServer.selectionIndex.containsKey(key)){
    					/*
    					 * If the index id exist, need to update the values irrespective of the selectionLimit.
    					 */
    					VWSServer.selectionIndex.remove(key);
    				}
    			}
    		}
    	}catch(Exception ex){
        		return Error;
    	}
        return NoError;
    }
    public int updateSelectionIndexMap(int IndexId, Vector values){
    	try{
    		//Master reset for optimizing the selection index values.
    		if(VWSPreferences.getEnableSelection().equalsIgnoreCase("true")){
    			String roomName = getRoomName();
    			String key = roomName+"_"+IndexId;

    			if(VWSServer.selectionIndex == null) {
    				VWSServer.selectionIndex = new HashMap<String, Vector>();
    			}
    			boolean selValuesCountFlag = false;

    			int selValuesCount = getCacheSelectionListCount();
    			int regSelectionLimit = Integer.parseInt(VWSPreferences.getSelectionLimit());
    			/*
    			 * Need to add to Map, if the selection values count is greater than 1000 - having registry entry also.
    			 */
    			if(values!=null){
    				if( values.size() > Integer.parseInt(VWSPreferences.getSelectionValuesCount()) ){
    					selValuesCountFlag = true;
    				}

    				if(VWSServer.selectionIndex!=null && VWSServer.selectionIndex.size()>0 && VWSServer.selectionIndex.containsKey(key)){
    					/*
    					 * If the index id exist, need to update the values irrespective of the selectionLimit.
    					 */
    					//VWSLog.add("update selectionIndex remove and put");
    					VWSServer.selectionIndex.remove(key);
    					//Sort method added to sort the SelectionListValues
    					Collections.sort(values);
    					VWSServer.selectionIndex.put(key, values);
    					//VWSLog.add(" sel index : "+VWSServer.selectionIndex.get(key));
    				}else if(VWSServer.selectionIndex !=null && VWSServer.selectionIndex.size()>0 && (selValuesCount < regSelectionLimit)){
    					/*
    					 * If the index id not exist in the map, need to check for the selectionMapLimit and selectionValuesCount.
    					 */
    					//VWSLog.add("update selectionIndex put selValuesCountFlag : "+selValuesCountFlag);
    					if(selValuesCountFlag){
    						//Sort method added to sort the SelectionListValues
    						Collections.sort(values);
    						VWSServer.selectionIndex.put(key, values);
    					}
    				}
    			}
    		}
    	}catch(Exception ex){
    		//VWSLog.add("Exception ex : "+ex.getMessage());
    		return Error;
	    }
    	return NoError;
    }
    
    public int createDocument(int sid, Document doc) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("createDocument method sid val--->" + sid);
        int retValue = -1;
        Vector path = new Vector();
        String nodePath = "";
        Vector indices = doc.getDocType().getIndices();
        int idxCount = indices.size();
        String idx = "";
        
        Vector indicesWithRules = new Vector();
        VWSLog.dbg("document type from doc object :::::::::"+doc.getDocType().getId());
        VWSLog.dbg("doc.getParentId() from createDocument :::: "+doc.getParentId());
        
        getDocTypeIndicesWithRules(sid, doc.getParentId(), doc.getDocType().getId(), indicesWithRules);
        VWSLog.dbg("indicesWithRules RampageServer.createDocument :::: "+indicesWithRules);
    	Client client = getClient(sid);
        int docTypeIndicesId=0;
        int indexId=0;
        String indexName="";
        int indexType=0;
        String indexMask="";    
        for(int count=0; count<indicesWithRules.size(); count++){
        	StringTokenizer st= new StringTokenizer(indicesWithRules.get(count).toString(), "\t");
        	docTypeIndicesId= Integer.parseInt(st.nextToken());
        	indexId= Integer.parseInt(st.nextToken());
        	indexName=st.nextToken();
        	indexType= Integer.parseInt(st.nextToken());
        	/**
        	 * Cv10.1 Issue Fix:- Date mask Validation for aip and change document type.
        	 * Modified by :- Madhavan.b
        	 * Date:-8-6-2017
        	 */
        
        	if((indexType == 2)&&(client.getClientType()==Client.AIP_Client_Type)){
        		String dateValue=(((Index) indices.elementAt(count)).getValue().trim());
        		VWSLog.dbg("dateValue :::::::"+dateValue);
        		VWSLog.dbg("dateVal length is :::::::"+dateValue.length());
        		//conditon added date:-8-12-2017
        		if(dateValue.length()>0){
        			VWSLog.dbg("inside date mask value of createdocument");
        			indexMask= st.nextToken();
        			VWSLog.dbg("indexMask::::::::::"+indexMask);
        			VWSLog.dbg("IndexValue::::::::::"+((Index) indices.elementAt(count)).getValue());
        			boolean retFromDateValidation=Util.validateDateMonthYear(indexMask,((Index) indices.elementAt(count)).getValue());
        			VWSLog.dbg("ret value from validation........"+retFromDateValidation);
        			if(retFromDateValidation == false){
        				doc.setId(InvalidDateMask);
        				return InvalidDateMask;
        			}
        		}

        	}
        }
       //--------------------CV10.1 Date Mask Validation ------------------------- 
        
        for(int i=0; i < idxCount; i++) {
            idx += Util.SepChar + ((Index) indices.elementAt(i)).getId() +
                   Util.SepChar + ((Index) indices.elementAt(i)).getValue() +
                   Util.SepChar + ((Index) indices.elementAt(i)).getActValue();
        VWSLog.dbg("((Index) indices.elementAt("+i+")).getValue() :::: "+((Index) indices.elementAt(i)).getValue());
        }
        
        Vector ret = new Vector();
       // Client client = getClient(sid);
        String param = client.getUserName() + Util.SepChar +
        			   client.getIpAddress()+ Util.SepChar +
                       String.valueOf(doc.getDocType().getId()) + Util.SepChar +
                       String.valueOf(doc.getParentId()) + Util.SepChar +
                       String.valueOf(idxCount) + idx;
        
        DoDBAction.get(getClient(sid),  database, "NewDocument", param, ret);
        if(ret.size() > 0)
        {
        	if (ret.get(0).toString().equals("-710")) {
        		doc.setId(-710);
        		return -710;
        	}else{
            StringTokenizer st = new StringTokenizer
                                            ((String) ret.get(0), Util.SepChar);
            doc.setId(Util.to_Number(st.nextToken()));
            doc.setName(st.nextToken());
            st.nextToken(); //docType id already set
            st.nextToken(); //doc type name already set
            doc.setCreator(st.nextToken());
            doc.setCreationDate(st.nextToken());
            doc.setModifyDate(st.nextToken());
            
            try{
            	retValue = getNodePath(sid, doc.getId(), path);
            	if(path!=null && path.size()>0)
            		nodePath = path.get(0).toString();
            }catch(Exception ex){
            	VWSLog.err("Exception while get the folder path : "+ex.getMessage());
            }
            try{
            	message = "Document '"+doc.getName()+"' added to '"+nodePath+"'";
            	int settingsId = CheckNotificationExist(sid, doc.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName());
            	if (settingsId >= 0){
            		setNotificationHistory(sid, settingsId, doc.getId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, "-", "-", client.getUserName(), message);
            	}
            	//addNotificationHistory(sid, doc.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName(), "-", doc.getName(), message);
            }catch(Exception ex){

            }
            return NoError;
          }
        }
        else
            return Error;
    }
    
    /**
     * CV10.1 Issue Fix: Added for validating the format of date value with mask value
     * @author apurba.m
     * @param maskValue
     * @param indexValue
     * @return
     */
    public boolean validateDateField(String maskValue, String indexValue) {
    	String mask = maskValue;
		String replacedMask = "";
		if(indexValue != null && !indexValue.equalsIgnoreCase("") && indexValue != ""){
			if(mask.startsWith("dddd,")){
				replacedMask= mask.replace("dddd", "EEEE"); 

			VWSLog.dbg("replacedMask inside validateDateField :::: "+replacedMask);
			VWSLog.dbg("indexValue inside validateDateField :::: "+indexValue);
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(replacedMask);
				sdf.setLenient(false);

				//if not valid, it will throw ParseException
				Date date = sdf.parse(indexValue);

			} catch (ParseException e) {
				//e.printStackTrace();
				VWSLog.dbg("Inside exception dateValidation1:::::"+e.getMessage());
				return false;
			}
		} else {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(maskValue);
				sdf.setLenient(false);

				//if not valid, it will throw ParseException
				Date date = sdf.parse(indexValue);

				} catch (ParseException e) {
					//e.printStackTrace();
					VWSLog.dbg("Inside exception dateValidation2:::::"+e.getMessage());
					return false;
				}			
			}
			VWSLog.dbg("Before returning true 1:::::");
			return true;
		} else {
			VWSLog.dbg("Before returning true 2 in else:::::");
			return true;
		}
    }
    
    /**
     * CV10.2 - Method to get document properties for summary info dialog
     * @param sid
     * @param did
     * @param ver
     * @param indices
     * @return
     */
	public int getDocumentProperties(int sid, int did, Vector indices) {
		Vector docTypeInfo = null;
		DocType docType = null;
		Vector settings = null;
		Vector docTypeList = null;
		Document doc = null;
		if (!assertSession(sid))
			return invalidSessionId;
		try {
			docTypeInfo = new Vector();
			getDocumentInfo(sid, did, docTypeInfo);
			VWSLog.dbg("getDocumentInfo return value....." + docTypeInfo);
			if (docTypeInfo.size() > 0 && docTypeInfo.get(0) != null
					&& docTypeInfo.get(0).toString().trim().length() > 0) {
				docTypeList = new Vector();
				settings = new Vector();
				docTypeList.addAll(vectorToDocuments(docTypeInfo, false));
				doc = (Document) docTypeList.get(0);
				// VWClient.printToConsole("doc 1 object id is ::::"+doc1.getId());
				VWSLog.dbg("doc1.getDocType().getName() :" + doc.getDocType().getName());
				VWSLog.dbg("doc1.getDocType().getName() :" + doc.getDocType().getId());
				
				docType = new DocType(doc.getDocType().getId());
				getDTVersionSettings(sid, docType, settings);
				VWSLog.dbg("getDTVersionSettings return value....." + settings);
				if (settings != null) {
					docType.setSettings(settings);
					VWSLog.dbg("Version enable exist....." + docType.getVREnable());
					if (docType.getVREnable().equals("0")) {
						getDocumentIndices(sid, did, "", indices);
					} else {
						getDocumentIndices(sid, did, "all.zip", indices);
					}
				}
			}
		} catch (Exception e) {
			VWSLog.dbg("Exception in e...." + e.getMessage());
		}
		VWSLog.dbg("indices :" + indices);
		docTypeInfo = null;
		docType = null;
		settings = null;
		return NoError;
	}

	private Vector vectorToDocuments(Vector v, boolean ver)
    {
        Vector docs = new Vector();
        for (int i = 0; i < v.size(); i++)
        {
            Document doc = new Document((String) v.elementAt(i), ver);
            docs.addElement(doc);
        }
        return docs;
    }
    
    public int getDocumentIndices(int sid, int did, String ver, Vector indices)
    {
    	VWSLog.dbgCheck("getDocumentIndices method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        if(ver.equals(""))
        {
            String param = String.valueOf(did);
            DoDBAction.get(getClient(sid),  database, "GetDocIndices", param, indices);
        }
        else
        {
            String param = String.valueOf(did) + Util.SepChar + ver;
            VWSLog.dbg("VR_GetDocProperties param....."+param);
            DoDBAction.get(getClient(sid),  database, "VR_GetDocProperties", param, indices);
        }
        return NoError;
    }
    public int updateDocumentDocType(int sid, Document doc)
    {
    	try {
	    	VWSLog.dbgCheck("updateDocumentDocType method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	    	//VWUpdateDocumentDocType
	        Vector indices = doc.getDocType().getIndices();
	        int idxCount = indices.size();
	        String idx = "";
	        String propValue = "";
	        
	      //--------------------------------------------------------------------
	        /**
	         * CV10.1 Issue Fix:- Date mask Validation for aip and change document type.
	         * Modified by :- Apurba.m
	         * Date:-19-4-2017
	         */
	        Vector indicesWithRules= new Vector();
	        VWSLog.dbg("indices Inside RampageServer.updateDocumentDocType :::: "+indices);
	        VWSLog.dbg("doc.getParentId() from updateDocumentDocType :::: "+doc.getParentId());
	        
	        getDocTypeIndicesWithRules(sid, doc.getParentId(), doc.getDocType().getId(), indicesWithRules);
	        
	        VWSLog.dbg("indicesWithRules inside RampageServer.updateDocumentDocType before calling UpdateDocumentDoctype Procedure :::: "+indicesWithRules);
	        
	        int docTypeIndicesId=0;
	        int indexId=0;
	        String indexName="";
	        int indexType=0;
	        String indexMask="";    
	        for(int count=0; count<indicesWithRules.size(); count++){
	        	VWSLog.dbg("Inside For Loop RampageServer.updateDocumentDocType before calling UpdateDocumentDoctype Procedure !!!!");
	        	StringTokenizer st= new StringTokenizer(indicesWithRules.get(count).toString(), "\t");
	        	docTypeIndicesId= Integer.parseInt(st.nextToken());
	        	indexId= Integer.parseInt(st.nextToken());
	        	indexName=st.nextToken();
	        	indexType= Integer.parseInt(st.nextToken());
	        	VWSLog.dbg("indexType Inside For Loop RampageServer.updateDocumentDocType before calling UpdateDocumentDoctype Procedure :::: "+indexType);
	        	if(indexType == 2){
	        		indexMask= st.nextToken();
	            	VWSLog.dbg("indexMask Inside For Loop RampageServer.updateDocumentDocType before calling UpdateDocumentDoctype Procedure :::: "+indexMask);
	            	retFromDateValidation= validateDateField(indexMask,((Index) indices.elementAt(count)).getValue());
	            	VWSLog.dbg("retFromDateValidation after calling validatDateField method RampageServer.updateDocumentDocType before calling UpdateDocument Procedure ::::  "+retFromDateValidation);
	            	if(retFromDateValidation == false){
	            		return InvalidDateMask;
	            	}
	            }
	        }
	      //--------------------CV10.1 Date Mask Validation -------------------------
	        for(int i=0; i < idxCount; i++){
	            idx += Util.SepChar + ((Index) indices.elementAt(i)).getId() +
	                   Util.SepChar + ((Index) indices.elementAt(i)).getValue() +
	                   Util.SepChar + ((Index) indices.elementAt(i)).getActValue();
	            
	            String tempStr = ((Index) indices.elementAt(i)).getValue();
	            if(tempStr.trim().length()>0)
	            	propValue +=  tempStr + ",";
	        }
	        if(propValue.length()>0)
	        	propValue = propValue.substring(0,propValue.length()-1);
	        
	        Vector ret = new Vector();
	        Client client = getClient(sid);
	        String param = client.getUserName() + Util.SepChar +
	                       client.getIpAddress()+ Util.SepChar +                     
	                       String.valueOf(doc.getId()) + Util.SepChar + 
	                       String.valueOf(doc.getDocType().getId()) + Util.SepChar +                           
	                       String.valueOf(idxCount) + idx;        
	        DoDBAction.get(getClient(sid),  database, "VWUpdateDocumentDocType", param, ret);
	        createVersionChangeDocType(sid,doc,ret);
    	} catch (Exception e) {
    		VWSLog.dbg("Exception in updateDocumentDocType :"+e.getMessage());
    		return Error;
    	}
    	return NoError;
    }
    //CV10 Enhancment added for azure storage document type change
    public int updateAzureDocumentDocType(int sid, Document doc,Vector result)
    {
    	VWSLog.dbgCheck("updateAzureDocumentDocType method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	//VWUpdateDocumentDocType
    	Vector indices = doc.getDocType().getIndices();
    	int idxCount = indices.size();
    	String idx = "";
    	String propValue = "";
    	for(int i=0; i < idxCount; i++){
    		idx += Util.SepChar + ((Index) indices.elementAt(i)).getId() +
    				Util.SepChar + ((Index) indices.elementAt(i)).getValue() +
    				Util.SepChar + ((Index) indices.elementAt(i)).getActValue();

    		String tempStr = ((Index) indices.elementAt(i)).getValue();
    		if(tempStr.trim().length()>0)
    			propValue +=  tempStr + ",";
    	}
    	if(propValue.length()>0)
    		propValue = propValue.substring(0,propValue.length()-1);

    	Client client = getClient(sid);
    	String param = client.getUserName() + Util.SepChar +
    			client.getIpAddress()+ Util.SepChar +                     
    			String.valueOf(doc.getId()) + Util.SepChar + 
    			String.valueOf(doc.getDocType().getId()) + Util.SepChar +                           
    			String.valueOf(idxCount) + idx;        
    	DoDBAction.get(getClient(sid),  database, "VWUpdateDocumentDocType", param, result);
    	// createVersionChangeDocType(sid,doc,ret);
    	return NoError;
    }
    public int setDocumentIndices(int sid, Document doc)
    {
        if (!assertSession(sid)) return invalidSessionId;
    	VWSLog.dbgCheck("setDocumentIndices method sid val--->" + sid);
        Vector indices = doc.getDocType().getIndices();
        int idxCount = indices.size();
        String idx = "";
        String propValue = "";
        
        /**
         * CV10.1 Issue Fix:- Date mask Validation for aip and change document type.
         * Modified by :- Apurba.m
         * Date:-28-3-2017
         */
        Vector indicesWithRules= new Vector();
        VWSLog.dbg("indices Inside RampageServer.setDocumentIndices :::: "+indices);
        VWSLog.dbg("doc.getParentId() from setDocumentIndices :::: "+doc.getParentId());
        
        getDocTypeIndicesWithRules(sid, doc.getParentId(), doc.getDocType().getId(), indicesWithRules);
        
        VWSLog.dbg("indicesWithRules inside RampageServer.setDocumentIndices before calling UpdateDocument Procedure :::: "+indicesWithRules);
        
        int docTypeIndicesId=0;
        int indexId=0;
        String indexName="";
        int indexType=0;
        String indexMask="";    
        for(int count=0; count<indicesWithRules.size(); count++){
        	VWSLog.dbg("Inside For Loop RampageServer.setDocumentIndices before calling UpdateDocument Procedure !!!!");
        	StringTokenizer st= new StringTokenizer(indicesWithRules.get(count).toString(), "\t");
        	docTypeIndicesId= Integer.parseInt(st.nextToken());
        	indexId= Integer.parseInt(st.nextToken());
        	indexName=st.nextToken();
        	indexType= Integer.parseInt(st.nextToken());
        	VWSLog.dbg("indexType Inside For Loop RampageServer.setDocumentIndices before calling UpdateDocument Procedure :::: "+indexType);
        	if(indexType == 2){
        		indexMask= st.nextToken();
            	VWSLog.dbg("indexMask Inside For Loop RampageServer.setDocumentIndices before calling UpdateDocument Procedure :::: "+indexMask);
            	retFromDateValidation= validateDateField(indexMask,((Index) indices.elementAt(count)).getValue());
            	VWSLog.dbg("retFromDateValidation after calling validatDateField method RampageServer.setDocumentIndices before calling UpdateDocument Procedure ::::  "+retFromDateValidation);
            	if(retFromDateValidation == false){
            		return InvalidDateMask;
            	}
            }
        }
      //--------------------CV10.1 Date Mask Validation -------------------------
        for(int i=0; i < idxCount; i++){
            idx += Util.SepChar + ((Index) indices.elementAt(i)).getId() +
                   Util.SepChar + ((Index) indices.elementAt(i)).getValue() +
                   Util.SepChar + ((Index) indices.elementAt(i)).getActValue();
            
            String tempStr = ((Index) indices.elementAt(i)).getValue();
            if(tempStr.trim().length()>0)
            	propValue +=  tempStr + ",";
        }
        if(propValue.length()>0)
        	propValue = propValue.substring(0,propValue.length()-1);
        
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = client.getUserName() + Util.SepChar +
                       client.getIpAddress()+ Util.SepChar +                     
                       String.valueOf(doc.getId()) + Util.SepChar + 
                       String.valueOf(doc.getDocType().getId()) + Util.SepChar +                           
                       String.valueOf(idxCount) + idx;
        
        DoDBAction.get(getClient(sid),  database, "UpdateDocument", param, ret);
        
        /*
         * Update the Notification 
         * Module 	: 	Document
         * Event 	:	Properties are (Index value) Modified.
         * NotifyId	:	2
         * Node Type: 	1 for Document
         * Message	:	Properties Values changed for <Document>
         */
        //VWSLog.dbg("UpdateDocument completed and isNotificationExist for this document : "+doc.isNotificationExist());
        //if (doc.isNotificationExist())
       /* {
        	String message = "Property Index Modified for document " + doc.getName();
        	addNotificationHistory(sid, doc.getId(), 1, 2, client.getUserName(), "", propValue, message);
        }
    */
        return NoError;
    }
    public int getDocumentVers(int sid, int did, Vector vers)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getDocumentVers method sid val--->" + sid);
        return getDocumentVers(sid, did, "0", vers);
    }
    private int getDocumentVers(int sid, int did, String ver, Vector vers)
    {
        String param = String.valueOf(did) + Util.SepChar + ver;
        VWSLog.dbg("getDocumentVers method param val--->" + param);
        DoDBAction.get(getClient(sid),  database, "VR_GetDocVersions", param, vers);
        return NoError;
    }
    public int deleteDocumentVers(int sid,int did)
    {
    	VWSLog.dbgCheck("deleteDocumentVers method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        String param = String.valueOf(did)+ Util.SepChar +
                        client.getUserName() + Util.SepChar +
                        client.getIpAddress(); 
        Vector rets =new Vector();
        DoDBAction.get(getClient(sid),  database, "VR_DelDocVersions", param, rets);
        return NoError;
        
    } 
    public int setDocVerComment(int sid,int did, String ver, String comment)
    {
       if (!assertSession(sid)) return invalidSessionId;
       	VWSLog.dbgCheck("setDocVerComment method sid val--->" + sid);
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = String.valueOf(did) + Util.SepChar +ver+Util.SepChar+
            			comment+Util.SepChar+client.getUserName()+Util.SepChar+client.getIpAddress();
        DoDBAction.get(getClient(sid),  database, "VR_SetDocVersionComment", param, ret);
        return NoError; 
    }
    public int setDocActiveVer(int sid, int nid, String ver)  
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("setDocActiveVer method sid val--->" + sid);
        ServerSchema ss = new ServerSchema();
        getDSSforDoc(sid, new Document(nid), ss);
        DSS dss = getDSS(ss);
        if (dss == null) return dssInactive;
         
        Client client = getClient(sid);
        String param= String.valueOf(nid) +  Util.SepChar + 
                      ver + Util.SepChar +
                      client.getUserName()+ Util.SepChar + 
                      client.getIpAddress();
        Vector ret = new Vector();
        DoDBAction.get(getClient(sid),  database, "VR_SetActiveDocVersion", param, ret);
        
        if(ret.size() > 0) 
        {
            String newVer = ((String) ret.get(0)).trim();
            if(!newVer.equals("")) 
                return setActiveFileVersion(sid, nid, dss, ss.path, ver, newVer);
            else
                return SetDocVerError;
        }
        else
            return SetDocVerError;
    }
    //CV10 Enhancement : azure storage added for set active version 
    public int setDocActiveVerAzure(int sid, int nid, String ver,Vector result)  
    {
    	if (!assertSession(sid)) return invalidSessionId;
    	VWSLog.dbgCheck("setDocActiveVerAzure method sid val--->" + sid);
    	ServerSchema ss = new ServerSchema();
    	getDSSforDoc(sid, new Document(nid), ss);
    	DSS dss = getDSS(ss);
    	if (dss == null) return dssInactive;

    	Client client = getClient(sid);
    	String param= String.valueOf(nid) +  Util.SepChar + 
    			ver + Util.SepChar +
    			client.getUserName()+ Util.SepChar + 
    			client.getIpAddress();
    	Vector ret = new Vector();
    	DoDBAction.get(getClient(sid),  database, "VR_SetActiveDocVersion", param, ret);

    	if(ret.size() > 0) 
    	{
    		String newVer = ((String) ret.get(0)).trim();
    		if(!newVer.equals("")) {
    			String retVal=ss.path+Util.SepChar+ver+Util.SepChar+newVer;
    			result.add(retVal);
    			//  return setActiveFileVersion(sid, nid, dss, ss.path, ver, newVer);
    			// result.add(e)
    			return NoError;
    		}
    		else
    			return SetDocVerError;
    	}
    	else
    		return SetDocVerError;

    }
    
    public int setPageCount(int sid,int docId, int pCount)
    {
       VWSLog.dbgCheck("setPageCount method sid val--->" + sid);
       if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        String param = String.valueOf(docId) + Util.SepChar + String.valueOf(pCount) ;
        DoDBAction.get(getClient(sid),  database, "Doc_SetPageCount", param, ret);
        return NoError; 
    }
    /*
     * CV10.1 Enhament added document version in comments.
     * Modified by madhavan.
     * docVersion parmater introduced
     */
    public int getDocumentComment(int sid, int nid,String docVersion,Vector comments)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getDocumentComment method sid val--->" + sid);
        Client client = getClient(sid);
        String param = String.valueOf(nid)+  Util.SepChar + client.getUserName()+Util.SepChar+docVersion;
        DoDBAction.get(getClient(sid),  database, "DOC_GetComment", param, comments);
        return NoError;
    }
    public int deleteDocumentComment (int sid,int dcId)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("deleteDocumentComment method sid val--->" + sid);
        Client client = getClient(sid);
        String param = String.valueOf(dcId)+Util.SepChar+client.getUserName();
        Vector ret =new Vector();
        DoDBAction.get(getClient(sid),  database, "DOC_DeleteComment", param,ret);
       
        if( ret.size() == 0 )   return Error;
        int iret = Util.to_Number( (String) ret.get(0) );        
        if(iret < 0)             return accessDenied;                
        return NoError;
    }
    public int setDocumentComment(int sid, DocComment dc,boolean withCheck,int docRestoreFlag,Vector ret)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("setDocumentComment method sid val--->" + sid);
        /*Issue #776 To maintain comments created date 'getTimeStamp' is added to param
         * C.Shanmugavalli Date:22 Nov 2006*/
        Client client = getClient(sid);
        dc.setUser(client.getUserName());
        String param = "";
	        param = dc.getId() +
                    Util.SepChar + dc.getDocId() +
                    Util.SepChar + Util.getOneSpace(client.getUserName()) +                       
                    Util.SepChar + (dc.getPublic()? "1" : "0") +
                    Util.SepChar + (withCheck? "1" : "0") +
                    Util.SepChar + Util.getOneSpace(dc.getComment())+Util.SepChar+docRestoreFlag;
        DoDBAction.get(getClient(sid),  database, "DOC_SetComment", param, ret);
        return NoError;
    }
    public int addRef(int sid, int nid, int rid, boolean bi)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("addRef method sid val--->" + sid);
        Vector ret = new Vector();
        String param = String.valueOf(nid) + Util.SepChar +
                       String.valueOf(rid) + Util.SepChar +
                       String.valueOf(bi? "1" : "0");
        DoDBAction.get(getClient(sid),  database, "Node_Ref_Add", param, ret);
        return NoError;
    }
    public int delRef(int sid, int nid, int rid) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        String param = String.valueOf(nid) + Util.SepChar +
                       String.valueOf(rid); 
        DoDBAction.get(getClient(sid),  database, "Node_Ref_Del", param, ret);
        return NoError;
    }
    public int getRefs(int sid, int nid, Vector refs) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("delRef method sid val--->" + sid);
        String param = String.valueOf(nid); 
        DoDBAction.get(getClient(sid),  database, "Node_Ref_Get", param, refs);
        return NoError;
    }
    public int getNextSerial(int sid, Index index) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getNextSerial method sid val--->" + sid);
        Vector ret = new Vector();
        String param = String.valueOf(index.getId()); 
        DoDBAction.get(getClient(sid),  database, "GetNextSerial", param, ret);
        
        if(ret.size() > 0)
        {
            index.setDef((String) ret.get(0));
        }
        return NoError;
    }
    
    //
    public int getNextSequence(int sid, int indexId, int docTypeId,int nodeId, Vector ret) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getNextSequence method sid val--->" + sid);
        String param = indexId + Util.SepChar + docTypeId + Util.SepChar+nodeId;
        DoDBAction.get(getClient(sid),  database, "VWGetNextSeq", param, ret);       
        return NoError;
    }
    
    
    public  int violatesUnique(int sid, int nodeId, int dtid, int iid, String value) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("violatesUnique method sid val--->" + sid);
        Vector ret = new Vector();
        String param = String.valueOf(iid) + Util.SepChar +
                       String.valueOf(dtid) + Util.SepChar + 
                       String.valueOf(nodeId) + Util.SepChar +value;
        DoDBAction.get(getClient(sid),  database, "CheckIndexValueIsFound", param, ret);
        if (ret.size() > 0) 
        {
            if ( ((String) ret.get(0)).equals("TRUE"))
                return True;
            else
                return False;
        }
        else
            return Error;
    }
    public int addShortcut(int sid, int nid, String name, String user, int type) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("addShortcut method sid val--->" + sid);
        String key = Integer.toString(sid);
        Client client = getClient(sid);
        
        Vector ret = new Vector();
        String param = String.valueOf(nid) + Util.SepChar +
                       name + Util.SepChar +
                       user + Util.SepChar + 
                       String.valueOf(type);
        DoDBAction.get(getClient(sid),  database, "Node_Shortcut_Add", param, ret);
        if (ret.size() > 0)
            return Util.to_Number((String) ret.get(0));
        else
            return Error;
    }
    public int delShortcut(int sid, int scid) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("delShortcut method sid val--->" + sid);
        Vector ret = new Vector();
        String param = String.valueOf(scid);
        DoDBAction.get(getClient(sid),  database, "Node_Shortcut_Del", param, ret);
        return NoError;
    }
    public int getShortcuts(int sid, Vector scs) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getShortcuts method sid val--->" + sid);
        String key = Integer.toString(sid);
        Client client = getClient(sid);
        DoDBAction.get(getClient(sid),  database, "Node_Shortcut_Get", client.getUserName(), scs);
        return NoError;
    }
    public int updateShortcut(int sid, int scid, String name, int type) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("updateShortcut method sid val--->" + sid);
        String key = Integer.toString(sid);
        Client client = getClient(sid);
        Vector ret = new Vector();
        String param = String.valueOf(scid) + Util.SepChar +
                       name + Util.SepChar + 
                       client.getUserName() + Util.SepChar + 
                       String.valueOf(type);
        DoDBAction.get(getClient(sid),  database, "Node_Shortcut_Update", param, ret);
        return NoError;
    }
    //---------------------------Search Methods---------------------------------
    public int createSearch(int sid, Search search) 
    {
    	try {
	        if (!assertSession(sid)) return invalidSessionId;
	        VWSLog.dbgCheck("createSearch method sid val--->" + sid);
	        if(!search.getContents().equals("")) 
	        {
	            if(database.getEngineName().equalsIgnoreCase(DBENGINE_ORA))
	                SQLQueryParser.parseOracleQuery(search.getContents(), false);
	            else if(database.getEngineName().equalsIgnoreCase(DBENGINE_SQL))
	                SQLQueryParser.parseSQLQuery(search.getContents(), false);
	        }
	        Client client = getClient(sid);
	        Vector ret = new Vector();
	        /* Issue #765: (se) Ability to control / set saved searches to be user specific and not just room specific.
	         * User name added in param for that
	         * C.Shanmugavalli Date 1 Dec 2006*/
	        /*getFindResultWith is added to do search(index) like in advanced google search. 28 Aug 2007 */
	        String param = String.valueOf(search.getId()) + Util.SepChar +
	                       search.getName() + Util.SepChar +
	                       (search.isTemp()? "1" : "0") + Util.SepChar +
	                       String.valueOf(search.getNodeId()) + Util.SepChar +
	                       search.getIndex() + Util.SepChar +
	                       search.getContents() + Util.SepChar +
	                       (search.isPreviousVersions()? "1" : "0") + Util.SepChar +
	                       (search.isSearchInComment()? "1" : "0") + Util.SepChar +
	                       search.getCreationDate1() + Util.SepChar +
	                       search.getCreationDate2() + Util.SepChar +
	                       search.getModifiedDate1() + Util.SepChar +
	                       search.getModifiedDate2() + Util.SepChar +
	                       client.getUserName() + Util.SepChar +
	                       client.getIpAddress() + Util.SepChar +
	        			   search.getFindResultWith() + Util.SepChar +
	        			   search.getTextResultWith();
	
	        DoDBAction.get(getClient(sid),  database, "UpdateSearch", param, ret);
	        if(ret == null || ret.size() == 0)   return CreateSearchError;
	        search.setId(Util.to_Number((String) ret.get(0)));        
	        param = String.valueOf(search.getId());
	        DoDBAction.get(getClient(sid),  database, "RemoveSearchAuthors", param, ret);
	        if (search.getCreators() != null) {
	        	try {
			        VWSLog.dbg("search.getCreators().size() :::: "+search.getCreators().size());
			        for (int i=0; i < search.getCreators().size(); i++)
			        {
			            param = String.valueOf(search.getId()) + Util.SepChar +
			                    String.valueOf(((Creator) search.getCreators().get(i)).getId());
				            DoDBAction.get(getClient(sid),  database, "AddSearchAuthor", param, ret);
			        }
			        param = String.valueOf(search.getId());
			        DoDBAction.get(getClient(sid),  database, "RemoveSearchDocTypes", param, ret);
	        	} catch (Exception e) {
	        		VWSLog.dbg("Exception while calling RemoveSearchDocTypes :"+e);
	        	}
	        }
	        if (search.getDocTypes() != null) {
	        	try {
			        VWSLog.dbg("search.getDocTypes().size() :::: "+search.getDocTypes().size());	        
			        for (int i=0;i< search.getDocTypes().size(); i++)
			        {
			        	VWSLog.dbg("((DocType)search.getDocTypes().get("+i+")).getId() :::: "+((DocType)search.getDocTypes().get(i)).getId());
			            param = String.valueOf(search.getId()) + Util.SepChar +
			                    String.valueOf(((DocType)search.getDocTypes().get(i)).getId());
			            DoDBAction.get(getClient(sid),  database, "AddSearchDocType", param, ret);
			        }
	        	} catch (Exception e) {
	        		VWSLog.dbg("Exception while calling AddSearchDocType :"+e);
	        	}
	        }
	        param = String.valueOf(search.getId());
	        if (search.getConds() != null) {
		        DoDBAction.get(getClient(sid),  database, "RemoveSearchConds", param, ret);
		        for (int i=0; i < search.getConds().size(); i++)
		        {
		            SearchCond cond = (SearchCond) search.getConds().get(i);
		            param = String.valueOf(search.getId()) + Util.SepChar +
		                    String.valueOf(cond.getSerial()) + Util.SepChar +
		                    String.valueOf(cond.getIndexId()) + Util.SepChar +
		                    String.valueOf(cond.getCond()) + Util.SepChar +
		                    String.valueOf(cond.getDocTypeId()) + Util.SepChar +
		                    cond.getValue1() + Util.SepChar + cond.getValue2();
		            
		            DoDBAction.get(getClient(sid),  database, "AddSearchCond", param, ret);
		        }
	        }        
    	} catch (Exception e) {
    		VWSLog.dbg("Exception while calling createSearch procedures " + e);
    	}
    	VWSLog.add("createSearch method search.getId() val--->" + search.getId());
        return search.getId();
    }
    public int getSearch(int sid, Search search) 
    {
    	if (!assertSession(sid)) return invalidSessionId;
    	VWSLog.dbgCheck("getSearch method sid val--->" + sid);
        Vector ret = new Vector();
        String param = String.valueOf(search.getId());
		//Desc   :Saved Search - Values are not saving with ORACLE rooms, since username is not passed as parameter for SP.
		//Author :Nishad Nambiar
		//Date   :20-Mar-2007        
        Client client = getClient(sid);
        param =param+" "+Util.SepChar +client.getUserName();   
        DoDBAction.get(getClient(sid),  database, "GetSearchsList", param, ret);
        if(ret.size() >0){
        	String searchString = String.valueOf(ret.get(0));
        	Search objSearch = null;
        	objSearch = new Search(searchString);
            search.set(objSearch);
        }
        return NoError;
    }
    public int getSearchs(int sid, Vector searchs) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getSearchs method sid val--->" + sid);
        Client client = getClient(sid);
        /* Issue #765: (se) Ability to control / set saved searches to be user specific and not just room specific.
        * User name added in param for that
        * C.Shanmugavalli Date 1 Dec 2206*/
        String param = " "+Util.SepChar +client.getUserName();
        DoDBAction.get(getClient(sid),  database, "GetSearchsList", param, searchs);
        return NoError;
    }
    public int getSearchAuthors(int sid, int srid, Vector authors) 
    {   
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getSearchAuthors method sid val--->" + sid);
        String param = String.valueOf(srid);
        DoDBAction.get(getClient(sid),  database, "GetSearchAuthorsList", param, authors);
        return NoError;
    }
    public int getSearchDocTypes(int sid, int srid, Vector dts) 
    {   
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getSearchDocTypes method sid val--->" + sid);
        String param = String.valueOf(srid);        
        DoDBAction.get(getClient(sid),  database, "GetSearchDocTypesList", param, dts);
        return NoError;
    }
    public int getSearchConds(int sid, int srid, Vector conds) 
    {   
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(srid);
        VWSLog.dbgCheck("getSearchConds method sid val--->" + sid);
        DoDBAction.get(getClient(sid),  database, "GetSearchCondsList", param, conds);
        return NoError;
    }
    public int getSearchPages(int sid, int did, String contents, Vector pages) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getSearchPages method sid val--->" + sid);
        if(database.getEngineName().equalsIgnoreCase(DBENGINE_ORA))
            SQLQueryParser.parseOracleQuery(contents, false);
        else
            SQLQueryParser.parseSQLQuery(contents, false);
        
        String param = String.valueOf(did) + Util.SepChar + contents;
        DoDBAction.get(getClient(sid),  database, "GetSearchPages", param, pages);
        return NoError;
    }
    public int delSearch(int sid, int srid) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("delSearch method sid val--->" + sid);
        Vector ret = new Vector();
        DoDBAction.get(getClient(sid),  database, "DeleteSearch", String.valueOf(srid) , ret);
        return NoError;
    }
    //onlyDocs flag added to get only document for AdminWise, QC indexer and GroupWise Find 
    public int find(int sid, int srid, int hlid, int rowNo, boolean onlyDocs)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("find method sid val--->" + sid);
        Vector ret = new Vector();
        Client client = getClient(sid);
        // For admin user also security permissions needs to set.
        String admin = (client.isAdmin()? "0" : "0");
        int onlyDocument = (onlyDocs)? 0:1;
        String param = String.valueOf(srid) + Util.SepChar +
                       String.valueOf(hlid) + Util.SepChar +
                       String.valueOf(rowNo) + Util.SepChar +
                       client.getUserName() + Util.SepChar +
                       Util.replaceSplCharFromTempTable(client.getIpAddress()) + Util.SepChar +
                       admin + Util.SepChar +onlyDocument;
        DoDBAction.get(getClient(sid),  database, "Search", param, ret);
        int found = 0;
        if (ret.size() > 0) found = Util.to_Number((String)  ret.get(0));
        return found;
    }
    //find signature changed for enh 1157  - audit catch for search criteria
    public int find(int sid, Search search, boolean onlyDocs)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("find method sid val 1-->" + sid);
        Vector ret = new Vector();
        Client client = getClient(sid);
        // For admin user also security permissions needs to set.
        String admin = (client.isAdmin()? "0" : "0");
        int onlyDocument = (onlyDocs)? 0:1;
        String AuditDescription = "Search Criteria = "+getAuditDescription(search)+" ";
        //VWSLog.add("AuditDescription "+AuditDescription);
        String param = String.valueOf(search.getId()) + Util.SepChar +
                       String.valueOf(search.getHitListId()) + Util.SepChar +
                       String.valueOf(search.getRowNo()) + Util.SepChar +
                       client.getUserName() + Util.SepChar +
                       client.getIpAddress() + Util.SepChar +
                       admin + Util.SepChar +
                       onlyDocument+ Util.SepChar + AuditDescription;
        DoDBAction.get(getClient(sid),  database, "Search", param, ret);
        int found = 0;
        if (ret.size() > 0) found = Util.to_Number((String)  ret.get(0));
        return found;
    }
    //method added for enh 1157
    private String getAuditDescription(Search search){
    	VWSLog.dbgCheck("Inside getAuditDescription() Method");
        String AuditDescription = "";
        if(search.getIndex()!=null && !search.getIndex().trim().equals(""))
        	AuditDescription = AuditDescription+ " [Index contains = "+search.getIndex()+"]";
        if(search.getContents()!=null && !search.getContents().trim().equals(""))
        	AuditDescription = AuditDescription+ " [Text contains = "+search.getContents()+"]";
        if(search.getTreePath()!=null && !search.getTreePath().trim().equals(""))
        	AuditDescription = AuditDescription+ " [Location = "+search.getTreePath()+"]";
        if(search.getNodeId()>0)
        	AuditDescription = AuditDescription+ " [ Node Id = "+search.getNodeId()+"]";

        Vector docTypes = search.getDocTypes();
        if(docTypes!=null && docTypes.size()>0){
        	for(int i =0; i<docTypes.size(); i++){
        		AuditDescription = AuditDescription+ " [document type = "+docTypes.get(i).toString()+"]";
        	}
    	}
        Vector conditions = search.getConds();
        if(conditions!=null && conditions.size()>0){
        	AuditDescription = AuditDescription+ " [conditions: ";
        	String conditionString = "";
        	for(int j =0; j<conditions.size(); j++){
        		SearchCond searchCond = (SearchCond)conditions.get(j);
        		conditionString  = conditionString+searchCond.getIndex();
        		conditionString  = " "+conditionString+searchCond.getCondString(searchCond.getCond())+" ";
        		conditionString  = conditionString+searchCond.getValue1();
        		if( searchCond.getCond() == 7 )
        			conditionString  = conditionString+" and "+searchCond.getValue2();
        		if(j <= conditions.size()-1)
        			conditionString = conditionString+",";
        	}
        	AuditDescription = AuditDescription+ conditionString+"]";
    	}
        Vector creators = search.getCreators();
        if(creators!=null && creators.size()>0){
        	for(int i =0; i<creators.size(); i++){
        		AuditDescription = AuditDescription+ " [creator = "+creators.get(i).toString()+"]";
        	}
    	}
        if(search.getCreationDate1()!=null && !search.getCreationDate1().trim().equals(""))
        {
        	AuditDescription = AuditDescription+ " [created date from  = "+search.getCreationDate1();
        	AuditDescription = AuditDescription+ " to = "+search.getCreationDate2()+"]";
        }
        
        if(search.getModifiedDate1()!=null && !search.getModifiedDate1().trim().equals(""))
        {
        	AuditDescription = AuditDescription+ " [modified date from  = "+search.getModifiedDate1();
        	AuditDescription = AuditDescription+ " to = "+search.getModifiedDate2()+"]";
        }
       	AuditDescription = AuditDescription+ " [search in comment = "+search.isSearchInComment()+"]";
       	AuditDescription = AuditDescription+ " [previous version = "+search.isPreviousVersions()+"]";
        
	        //search.isTemp();noneed
	        //search.getHitListId();no need
	        //search.getName();no neded
	        //search.getId(); no need
	        //search.getLastDocId(); no need
	        //search.getRowNo(); no need
        return AuditDescription;
    }
    /*Issue: Method added for Oracle Case sensitive issue
     * C.Shanmugavalli 1 Dec 2006
     */
    public void setNLSSORT_NLSCOMP(){
    	ResultSet rs  = null;
        Connection con = null;
        boolean execute = false;
    	 try
	        {
    	 	
    	 	if (database.getEngineName().equalsIgnoreCase("oracle"))
            {
    	 		execute = true;
    	 		con = database.aquireConnection();
    	 		Statement stmt = con.createStatement();                
                rs = stmt.executeQuery ("Select Version from product_component_version where rownum<2");
                if(rs.next()){
                	int oracleVersion = 0;
                	try{
                		oracleVersion = Integer.parseInt(rs.getString(1).substring(0,2));
                	}catch (Exception e) {
						// TODO: handle exception
                		
					}
                	//VWSLog.add("*************************** oracleVersion " + oracleVersion);
/*        			if(oracleVersion =null && !oracleVersion.equals("") 
                			&& (oracleVersion.substring(0,2).equalsIgnoreCase("10") || oracleVersion.substring(0,2).equalsIgnoreCase("11")) )
*/
                	if(oracleVersion >= 10 )
                	{
                    	//VWSLog.err("before firing alter session ");
                		stmt.executeQuery("ALTER SESSION SET NLS_SORT='BINARY_CI'");
                		stmt.executeQuery("ALTER SESSION SET NLS_COMP='LINGUISTIC'");
                		//VWSLog.add("after firing alter session ");
                	}
               	}
                rs.close();
                rs = null;
                //con.close();
            }
        }
    	catch(SQLException se)
	    {
    		//rs.close();
    		//con.close();
    		//VWSLog.err("Error in setting NLSSORT "  + se.getMessage()); 
             //return Error;
	    }
    	finally{
    		try{
    			if (execute){
    				assertConnection(con);
    			}
    		}catch(Exception ex){}
    	}
    }
    public int getFindResult(int sid, Search srch, Vector docs)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getFindResult method sid val--->" + sid);
        Client client = getClient(sid);
        String param = Util.replaceSplCharFromTempTable(client.getIpAddress()) + Util.SepChar +
                       String.valueOf(srch.getLastDocId()) + Util.SepChar +
                       String.valueOf(srch.getRowNo())+ Util.SepChar +
                       client.getUserName();
        DoDBAction.get(getClient(sid),  database, "GetSearchResult", param, docs);
        //VWSLog.add(" find result "+docs.toString());
        return NoError;
    }
    public int getIndexValues(int sid, int dtid, int iid, Vector values) 
    {
    	int returnValue = NoError;
    	VWSLog.dbgCheck("getIndexValues method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String room = getRoomName();
    	String key = room+"_"+iid;
    	String param = "";
    	param = String.valueOf(iid) +
                Util.SepChar + String.valueOf(dtid);
        if(VWSPreferences.getEnableSelection().equalsIgnoreCase("false")){
        	try {
        		if(VWSServer.selectionIndex != null && VWSServer.selectionIndex.size() > 0){
        			VWSServer.selectionIndex.clear();	
        			VWSServer.selectionIndex = null;
        			VWSLog.dbg("Emptied SelectionIndex hashmap value");
        		}
        		VWSLog.dbg("Before calling GetDTIndexValuesList ....");
        		returnValue = DoDBAction.get(getClient(sid),  database, "GetDTIndexValuesList", param , values);
			} catch (Exception ex) {
				VWSLog.errMsg("EXCEPTION in GetDTIndexValuesList generalErrorCode(-1)  :::: "+ex.getMessage());
				returnValue = Error;
			}
        }else if(VWSPreferences.getEnableSelection().equalsIgnoreCase("true")){
        
        	int selValuesCount = getCacheSelectionListCount();
        	//int ret;
    		int regSelectionLimit = Integer.parseInt(VWSPreferences.getSelectionLimit());
    		boolean selStoreFlag = true;
    		boolean selValuesCountFlag = false;
    		boolean addSelectionIndex = true;
    		try{
    			if(VWSServer.selectionIndex == null){
    				VWSServer.selectionIndex = new HashMap<String, Vector>();
    			}
    			if(selValuesCount > regSelectionLimit){
    				/*
    				 * The selectionIndex hashmap exeeds the limit
    				 */
    				selStoreFlag = false;
    			}
    			if (VWSServer.selectionIndex.get(key) != null){
    				VWSLog.dbg("VWSServer.selectionIndex : "+VWSServer.selectionIndex);
					VWSLog.dbg("key : "+key);
    				values.addAll((Vector)VWSServer.selectionIndex.get(key));
    				addSelectionIndex = false;
    				if (values != null && values.size() > 0){
    					returnValue = NoError;
    				}
    			}else{
    				returnValue = DoDBAction.get(getClient(sid),  database, "GetDTIndexValuesList", param , values);
    				VWSLog.dbg("After calling GetDTIndexValuesList : "+values);
    			}
    			/*
    			 * Default maximum selection values count is 1000, and user can modify in registry.
    			 */
    			if( values.size() > Integer.parseInt(VWSPreferences.getSelectionValuesCount()) ){
    				selValuesCountFlag = true;
    			}

    			if(selStoreFlag && selValuesCountFlag && addSelectionIndex){
    				try{
    					VWSLog.dbg("VWSServer.selectionIndex : "+VWSServer.selectionIndex);
    					VWSLog.dbg("key : "+key);
    					//Added in CV83B2 to optimize the selection values
    					if (VWSServer.selectionIndex.get(key) != null){
    						VWSServer.selectionIndex.clear();
    					}
    					Collections.sort(values);
    					VWSServer.selectionIndex.put(key, values);
    				}catch(Exception ex){
    					VWSLog.errMsg("EXCEPTION in getIndexValues selection generalErrorCode(-1)  :::: "+ex.getMessage());
    					returnValue = Error;
    				}
    			}
    		}catch(Exception ex){
    			VWSLog.errMsg("EXCEPTION in getIndexValues generalErrorCode(-1)  :::: "+ex.getMessage());
    			returnValue = Error;
    		}
        }
        return returnValue;
    }
    //--------------------------Security----------------------------------------
    public int getUsers(int sid, Vector users)
    {
    	VWSLog.dbgCheck("getUsers method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        if(isOracle)
        	param = "2"+Util.SepChar;
        else
        	param = "2";
        return DoDBAction.get(getClient(sid),  database, "User_Get", param, users);
    }
 
    /**CV2019 merges from CV10.2 line**/
    public int getCustomUsers(int sid,String paramval, Vector users)
    {
    	//14 need to be passed from dtc
    	VWSLog.dbgCheck("getCustomUsers method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        if(isOracle)
        	param =paramval+Util.SepChar;
        else
        	param = paramval;
        return DoDBAction.get(getClient(sid),  database, "VWUser_Get1", param, users);
    }
    
    public int setRouteOTPInfo(int sid,int recId,int docId,String userName,String OTP,Vector result)
    {
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	try{
    	VWSLog.dbgCheck("SetRouteOTPInfo method sid val--->" + sid);
    	VWSLog.dbg("Inside setRouteOTPInfo");
        if (!assertSession(sid)) return invalidSessionId;
        String param =String.valueOf(recId)+Util.SepChar+String.valueOf(docId)+Util.SepChar+userName+Util.SepChar+OTP+Util.SepChar;
        VWSLog.dbg("setRouteOTPInfo::::::::"+param);
    //    addATRecord(sid, user, client.getIpAddress(), 1, VWATEvents.AT_OBJECT_FIND, VWATEvents.AT_FIND, searchDesc);   
        
		int retVal=DoDBAction.get(getClient(sid),database, "VW_DRS_SetRouteOTPInfo", param,result);
		VWSLog.dbg("result :::::::::"+result);
        VWSLog.dbg("retVal::::::::"+retVal);
		if(retVal==0){}
		return NoError;
    	}catch(Exception e) {
    		VWSLog.dbg("Exception while CVCheckForTemplateExist ::::"+e.getMessage());
    		return Error;
    	}
    }
    public int sendRouteOTPMail(int sid,int docId,String userName,String docName,String OTP,String loggedUserMailId,String action)
    {
    	VWSLog.dbg("Inside otp mail ::::"+sid +"userName is "+userName+"docName is "+docName+"otp is XXXX loggedUserMailId :"+loggedUserMailId+"action is "+action);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	try{
    		VWSLog.dbg("before sending otp mail....");
    		 if (!assertSession(sid)) return invalidSessionId;
    		String fromMailId=VWSPreferences.getFromEmailId().toString();
    		String toMailId=loggedUserMailId;
    		String subject="DMS : OTP - Workflow";
    		int mailSentStatus=0;
    		if(action.equals("End")){
    			action=action+" "+"Workflow";
    		}else if(action.equals("Accept End Workflow")){
    			
    			action=	action.replace("Accept End Workflow",  "Accept and End Workflow");
    			//action=replace("Accept EndWorkflow", "Accept and EndWorkflow");
    		}
    		String message=OTP+" "+"is your verification code (OTP) for "+action+" action of Document - "+docName+"."+HTML_NEW_LINE+"This OTP is valid for 15 minutes. Please do not share the OTP for security reasons.";
    		VWMail vwMail=new VWMail();
			sendMailThread thread = new sendMailThread(vwMail, fromMailId, toMailId, subject, message, "OTP", null,
					null, null, null, null, null, null, null, null);
    		addATRecord(sid, userName, client.getIpAddress(), docId, VWATEvents.AT_OBJECT_OTP, VWATEvents.AT_OTP, "OTP send to user "+userName); 
    		thread.start();
    		VWSLog.dbg("after sending otp mail....");
    	}catch(Exception e){ 
    		VWSLog.dbg("Exception in sending otp mail ::::"+e.getMessage());
    		return Error;
    	}
    	return NoError;

    }
    
    
   
    public int checkNextTaskExist(int sid,int docId,int routeid ,String taskName,Vector result)
    {
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	try{
    		String param= String.valueOf(docId)+Util.SepChar+String.valueOf(routeid)+Util.SepChar+taskName+Util.SepChar;
    		VWSLog.dbg("checkNextTaskExist parms::::::"+param);
    		DoDBAction.get(getClient(sid),database, "VW_DRS_CheckNextTaskExist",param,result);
    		VWSLog.add("checkNextTaskExist result is ::::"+result);
    		return NoError;
    	}
    	catch(Exception ex){
    		VWSLog.dbg("Exception while checkNextTaskExist ::::"+ex.getMessage());
    		return Error;
    	}
    }
    public int getGroups(int sid, Vector groups)
    {
    	VWSLog.dbg("Calling getGroups from RampageServer....");
    	VWSLog.dbgCheck("getGroups method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        if(isOracle)
        	param = "2" + Util.SepChar + "~" + Util.SepChar;
        	//param = "2"+Util.SepChar;
        else
        	param = "2" + Util.SepChar + "~";
        	//param = "2";
        return DoDBAction.get(getClient(sid),  database, "Group_Get", param, groups);
    }
    public int getPrincipals(int sid, Vector princ)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getPrincipals method sid val--->" + sid);
        return DoDBAction.get(getClient(sid),  database, "Principal_GetAll", "0" + 
            Util.SepChar + getAdminGroup() + Util.SepChar + "" + Util.SepChar + getSubAdminGroup(), princ);
    }
	/**CV2019 merges from CV10.2**/
    public int getPrincipalsWithNoSign(int sid, Vector princ)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getPrincipals method sid val--->" + sid);
        return DoDBAction.get(getClient(sid),  database, "Principal_GetAll", "6" + 
            Util.SepChar + getAdminGroup() + Util.SepChar + "" + Util.SepChar + getSubAdminGroup(), princ);
    }
    public int removePrincipal(int sid, Principal principal)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("removePrincipal method sid val--->" + sid);
        String param = "";
        	param = principal.getId()+Util.SepChar;
        return DoDBAction.get(getClient(sid),  database, "Principal_Del", param, null);
    }
    public int updatePrincipal(int sid, Principal principal)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("updatePrincipal method sid val--->" + sid);
        String param=String.valueOf(principal.getId()) + Util.SepChar + principal.getEmail();
        return DoDBAction.get(getClient(sid),  database, "VW_SetUserMail",param,null);
    }
    public int addPrincipal(int sid, Principal principal)
    {
    	VWSLog.dbg("Inside add principals...."+principal);
    	boolean LDAPsingleToMultiple = VWSPreferences.getLDAPUpgradeToMultiDomain();
    	
    	if (!assertSession(sid)) return invalidSessionId;
    	/**CV2019 code merges from CV10.2***/
	    String[] dirList = null;
	    String ldapDirectory = VWSPreferences.getTree();
		dirList = ldapDirectory.split(",");

		
    	VWSLog.dbgCheck("addPrincipal method sid val--->" + sid);
    	Vector ret=new Vector();
    	boolean isHosting = false;
    	Directory directory1=null;
    	 try{
	    	isHosting = CVPreferences.getHosting();
	    }catch (Exception e) {
	    	isHosting = false;
	    }
    	String schemeType = VWSPreferences.getSSType();
    	if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL) || schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
    		List newUser = new  LinkedList();
    		newUser.add(principal.getName());
    		Map dirMap=directory.getLdapDirectories();
    		VWSLog.dbg("dirMap in addPrincipal........."+dirMap);
    		String curLoginServer=(String) dirMap.get(principal.getDirName());
    		VWSLog.dbg("curLoginServer in addprincipal........."+curLoginServer);
    		 directory1 = Directory.getInstance(principal.getDirName(),curLoginServer,true);
    		 if(principal.getType()==Principal.USER_TYPE){
    			 Vector userMailId = directory1.getUserMailIds1(newUser,principal.getDirName(),curLoginServer);	
    			 if(userMailId!=null && userMailId.size()>0)
    				 principal.setEmail(userMailId.get(0).toString());

    			 else
    				 principal.setEmail("-");
    		 }
    	}else
    		principal.setEmail("-");
    	
    	/**Code optimized by Vanitha on 17\12\2019***********/
    	int adminGrpLicenseCount=getLicenseAdminGroupCount();
		int namedGrpLicenseCount = getLicenseNamedGroupCount();
		int licenseNamedOnlineGrpCount=getLicenseNamedOnlineGroupCount();
		int professionalNamedGrpCount=getLicenseProfessionalNamedGroupCount();
		int subAdminGrpLicenseCount = getLicenseSubAdminGroupCount();
		int batchInputGrpCount = getLicenseAIPGroupCount();
		DirectorySync directorySync=null;
		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
			directorySync = new DirectorySync(database, directory1, true, getAdminGroup(),getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),getSubAdminGroup(),getBatchInputAdminGroup(),adminGrpLicenseCount,namedGrpLicenseCount,licenseNamedOnlineGrpCount,getLicenseConcurrentGroupCount(),getLicenseConcurrentOnlineGroupCount(),professionalNamedGrpCount,getLicenseProfessionalConcurrentGroupCount(),getLicensePublicWACount(),getLicenseSubAdminGroupCount(),getLicenseAIPGroupCount(), true);
		} else {
			directorySync = new DirectorySync(database, directory, true, getAdminGroup(),getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),getSubAdminGroup(),getBatchInputAdminGroup(),adminGrpLicenseCount,namedGrpLicenseCount,licenseNamedOnlineGrpCount,getLicenseConcurrentGroupCount(),getLicenseConcurrentOnlineGroupCount(),professionalNamedGrpCount,getLicenseProfessionalConcurrentGroupCount(),getLicensePublicWACount(),getLicenseSubAdminGroupCount(),getLicenseAIPGroupCount(), true);
		}
		/***********************************************/
    	if(principal.getType()==Principal.USER_TYPE)
    	{	
    		try {
    			
    			/*int licenseConcrGrpCount=getLicenseConcurrentGroupCount();
    			int licenseConcrOnlineGrpCount=getLicenseConcurrentOnlineGroupCount();
    			int licenseprofConcrGrpCount=getLicenseProfessionalConcurrentGroupCount();
    			int licensePublicWAGrpCount=getLicensePublicWACount();*/
    			List usersInGroup=null;
    			/**Commented by Vanitha on 17\12\2019*****/
    			/*if(schemeType.equalsIgnoreCase(SSCHEME_ADS)){
    				directorySync = new DirectorySync(database, directory, true, getAdminGroup(),getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),licenseAdminGrpCount,licenseNamedGrpCount,getLicenseNamedOnlineGroupCount(),getLicenseConcurrentGroupCount(),getLicenseConcurrentOnlineGroupCount(),getLicenseProfessionalNamedGroupCount(),getLicenseProfessionalConcurrentGroupCount(),getLicensePublicWACount(), true);
    			}else*/if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
    				//directorySync = new DirectorySync(database, directory1, true, getAdminGroup(),getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),licenseAdminGrpCount,licenseNamedGrpCount,getLicenseNamedOnlineGroupCount(),getLicenseConcurrentGroupCount(),getLicenseConcurrentOnlineGroupCount(),getLicenseProfessionalNamedGroupCount(),getLicenseProfessionalConcurrentGroupCount(),getLicensePublicWACount(), true);
    				usersInGroup = directorySync.getGroupsToUser(principal.getName());
    			}
    			if(usersInGroup != null && usersInGroup.contains(getAdminGroup())){
    				if((directorySync.getDirAdminGrpCount() > adminGrpLicenseCount)&&(getNamedUserCountInDB(sid,getAdminGroup())>=adminGrpLicenseCount )){
    					return ViewWiseErrors.AdminUsersCountExceedsLicenseSeats;
    				}
    			} else if(usersInGroup != null && usersInGroup.contains(getSubAdminGroup())){
    				if((directorySync.getDirSubAdminGrpCount() > subAdminGrpLicenseCount)&&(getNamedUserCountInDB(sid,getSubAdminGroup())>=subAdminGrpLicenseCount )){
    					return ViewWiseErrors.SubAdminUsersCountExceedsLicenseSeats;
    				}
    			} else if(!isHosting&&usersInGroup != null && usersInGroup.contains(getNamedGroup())){
    				VWSLog.dbg("getDirNamedGrpCount::::"+directorySync.getDirNamedGrpCount());
    				if((directorySync.getDirNamedGrpCount() > namedGrpLicenseCount)&&(getNamedUserCountInDB(sid,getNamedGroup())>=namedGrpLicenseCount )){
    					return ViewWiseErrors.NamedUsersCountExceedsLicenseSeats;
    				}
    			} else if(!isHosting&&usersInGroup != null && usersInGroup.contains(getNamedOnlineGroup()))   	{
    				//need to modify
    				if((directorySync.getDirNamedOnlineGrpCount() > licenseNamedOnlineGrpCount)&&(getNamedUserCountInDB(sid,getNamedOnlineGroup())>=licenseNamedOnlineGrpCount )){
    					return ViewWiseErrors.NamedOnlineCountExceedsLicenseSeats;
    				}
    			
    			} else if(!isHosting&&usersInGroup != null && usersInGroup.contains(getProfessionalNamedGroup())){
    				if((directorySync.getDirProfessionalNamedGrpCount() > professionalNamedGrpCount)&&(getNamedUserCountInDB(sid,getProfessionalNamedGroup())>=professionalNamedGrpCount)){
    					return ViewWiseErrors.ProfessionalNamedUsersCountExceedsLicenseSeats;
    				}
    			}
    			VWSLog.dbg("After checking license count...........");
    			//Commented on 5\8\2017 comment by madhavan.
    			/*else if(!isHosting&&usersInGroup != null && usersInGroup.contains(getConcurrentGroup())){
    				if((directorySync.getDirConcurrentGrpCount() > licenseConcrGrpCount)&&(getNamedUserCountInDB(sid,getConcurrentGroup())>=licenseConcrGrpCount)){
    					return ViewWiseErrors.ConcurrentUsersCountExceedsLicenseSeats;
    				}
    			}
    			else if(!isHosting&&usersInGroup != null && usersInGroup.contains(getConcurrentOnlineGroup())){
    				if((directorySync.getDirConcurrentOnlineGrpCount() > licenseConcrOnlineGrpCount)&&(getNamedUserCountInDB(sid,getConcurrentOnlineGroup())>=licenseConcrOnlineGrpCount)){
    					return ViewWiseErrors.ConcurrentUsersCountExceedsLicenseSeats;
    				}
    			}
    			else if(!isHosting&&usersInGroup != null && usersInGroup.contains(getProfessionalConcurrentGroup())){
    				if((directorySync.getDirProfConcrGrpCount() > licenseprofConcrGrpCount)&&(getNamedUserCountInDB(sid,getProfessionalConcurrentGroup())>=licenseprofConcrGrpCount)){
    					return ViewWiseErrors.ProfConcrUsersCountExceedsLicenseSeats;
    				}
    			}*/
    	/*	else if(!isHosting&&usersInGroup != null && usersInGroup.contains(getPublicWAGroup())){
        				if((directorySync.getDirPublicWAGrpCount() > licensePublicWAGrpCount)&&(getNamedUserCountInDB(sid,getPublicWAGroup())>=licensePublicWAGrpCount)){
        					return ViewWiseErrors.PublicWACountExceedsLicenseSeats;
        		}
    			}*/
    		} catch (RemoteException e){
    			VWSLog.dbg("Exception while checking license count in addPrincipal :"+e);
       		}
    	}
    	VWSLog.dbg("Sync Type (User/Group)......."+principal.getType());
    	if(principal.getType()==1){
			/**CV2019 merges from CV10.2**/
    		if((schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)) && (dirList.length>1)){
    			VWSLog.dbg("User / Group adding from Administration principal.getDirName() "+principal.getDirName());
    			VWSLog.dbg("directory.getDirName() ----->"+directory1.getDirName());
    			VWSLog.dbg("dirList[0] ----->"+dirList[0]);
    			
    			if (directory1.getDirName().equalsIgnoreCase(dirList[0])) {
    				DoDBAction.get(getClient(sid),  database, "Principal_Add",
        					principal.getName() + Util.SepChar +
        					principal.getType()+Util.SepChar+"",ret);
    			} else {
	    			DoDBAction.get(getClient(sid),  database, "Principal_Add",
	    					principal.getName() + Util.SepChar +
	    					principal.getType()+Util.SepChar+principal.getDirName(),ret);
    			}
    		}else{
    			DoDBAction.get(getClient(sid),  database, "Principal_Add",
    					principal.getName() + Util.SepChar +
    					principal.getType()+Util.SepChar+"",ret);
    		}
    	} else{
			/**CV2019 merges from CV10.2**/
        	try{
        		VWSLog.dbg("Inside add user....");
	    		String param="";
	    		String domainName="";
	    		VWSLog.dbg("addPrincipal for users...");
	    		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
		    		String userNameInfo=directory1.getUserFullInfo(principal.getName());
		    		VWSLog.dbg("userNameInfo before principal add "+userNameInfo);
		    		if(userNameInfo.length()>0){
		    			StringTokenizer st = new StringTokenizer(userNameInfo, Util.SepChar);
		    			VWSLog.dbg("st.hashtokens "+st.countTokens());
		    			if(st.hasMoreTokens()){
							String name = st.nextToken();
							VWSLog.dbg("name :::"+name);
							String principalName = "";
							String upnSuffix= "";
							if (((name.indexOf("@")) >= 0) && (dirList.length==1)) {
								principalName=name.substring(0,(name.indexOf("@")));								
							} else {
								principalName=name;
							}
							if (name.contains("@"))
								upnSuffix=name.substring(name.indexOf("@")+1,name.length());							
							VWSLog.dbg("principalName :::"+principalName);					
							VWSLog.dbg("upnSuffix :::"+upnSuffix);
							String userDn=st.nextToken();
							VWSLog.dbg("userDn :::"+userDn);
							String samName=st.nextToken();
							VWSLog.dbg("samName :::"+samName);
							String emailId=st.nextToken();
							VWSLog.dbg("emailId :::"+emailId);
							if(userDn.length()>0){
				    			String domainCoponent=userDn.substring(userDn.indexOf("DC"),userDn.length());
				    			String[] splittedDC=domainCoponent.split(",");
				    			for(String dcval:splittedDC){
				    				dcval=dcval.substring(dcval.indexOf("=")+1,dcval.length());
			
				    				domainName=domainName+dcval+".";
				    			}
				    			domainName=domainName.substring(0,domainName.lastIndexOf("."));
				    			VWSLog.dbg("domainName :"+domainName);
				    		}
							//VWSLog.add("id :::"+id+"name :::"+name+"userUPnSuffix:::"+userUPnSuffix+"userDn::"+userDn);
							if (dirList.length>1) {
								if (LDAPsingleToMultiple) {
									if (directory1.getDirName().equalsIgnoreCase(dirList[0])) {
										principalName = principal.getName();
									}
								}
							}
							if (emailId != null && emailId.length() > 0)
								emailId = emailId.trim();
							param =principalName + Util.SepChar +principal.getType()+Util.SepChar+upnSuffix+Util.SepChar+userDn+Util.SepChar+samName+Util.SepChar+emailId+Util.SepChar+domainName+Util.SepChar;
							VWSLog.dbg("param before principal add "+param);
						}
		    			DoDBAction.get(getClient(sid),  database, "Principal_Add",param,ret);
		    		}
	    		}else {
					param =principal.getName() + Util.SepChar + principal.getType()+Util.SepChar +""+Util.SepChar+""+Util.SepChar+""+Util.SepChar+""+Util.SepChar+""+Util.SepChar;
					VWSLog.dbg("param before principal add "+param);
					DoDBAction.get(getClient(sid),  database, "Principal_Add",param,ret);
	    		}
    		}catch(Exception e){
    			VWSLog.dbg("Exception ::"+e);
    		}
    		// Commented on 12/08/2019 by Vanitha 
    		// 10.2 Line code merged in cv2019
    		/*String param = principal.getName()+Util.SepChar+principal.getEmail();
    		DoDBAction.get(getClient(sid),  database, "VW_SetUserMails", param, new Vector());*/
    	}
    	int principalId=0;
    	VWSLog.dbg("Principal_add ret ::"+ret);
    	if(ret!=null && ret.size()>0)
    	{
    		principalId=Util.to_Number((String)ret.get(0));
    		VWSLog.dbg("principalId ........."+principalId);
    		
    		/**Commented by Vanitha on 17\12\2019.*****/
    		/*int licenseAdminGrpCount=getLicenseAdminGroupCount();
    		int licenseNamedGrpCount = getLicenseNamedGroupCount();*/
    		
    		String loginServer = "";
    		ldapDirectory = VWSPreferences.getTree();
    		String type = VWSPreferences.getSSType();
    		String[] serList = loginServer.split(",");
    		
    		/**Commented by Vanitha on 17\12\2019.*****/
    		/*if(schemeType.equalsIgnoreCase(SSCHEME_ADS)){
    			directorySync= new DirectorySync(database,directory, true, getAdminGroup(), getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),licenseAdminGrpCount,licenseNamedGrpCount,getLicenseNamedOnlineGroupCount(),getLicenseConcurrentGroupCount(),getLicenseConcurrentOnlineGroupCount(),getLicenseProfessionalNamedGroupCount(),getLicenseProfessionalConcurrentGroupCount(),getLicensePublicWACount(), true);

    		} else if ((schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)) && principal.getType()!=1){
				//directorySync = new DirectorySync(database,directory1, true, getAdminGroup(), getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),licenseAdminGrpCount,licenseNamedGrpCount,getLicenseNamedOnlineGroupCount(),getLicenseConcurrentGroupCount(),getLicenseConcurrentOnlineGroupCount(),getLicenseProfessionalNamedGroupCount(),getLicenseProfessionalConcurrentGroupCount(),getLicensePublicWACount(), true);
    			/**CV2020 merges from CV10.2 line. Commented on 06\01\2020 commented by Vanitha**/
				/*VWSLog.dbg("Inside ldap ads of add principals ..........");
				VWSLog.dbg("directory.getDirName()::::::::::"+directory1.getDirName());
				VWSLog.dbg("principal.getName():::::"+principal.getName());
				directorySync.getLDAPSamdn(principal.getName()+"@"+principal.getDirName());*/
			/*}else if (schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)&&principal.getType()==1){
				 directorySync = new DirectorySync(database,directory1, true, getAdminGroup(), getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),licenseAdminGrpCount,licenseNamedGrpCount,getLicenseNamedOnlineGroupCount(),getLicenseConcurrentGroupCount(),getLicenseConcurrentOnlineGroupCount(),getLicenseProfessionalNamedGroupCount(),getLicenseProfessionalConcurrentGroupCount(),getLicensePublicWACount(), true);
			}*/
    		/*********************************************/
    		
			if(principal.getType()==Principal.USER_TYPE)
    		{	//Commented on 5\8\2017 commented by madhavan
    			//List usersInGroup = directorySync.getGroupsToUser(principal.getName());
    			//getUsersInGroup(principal.getName());
    					
    			/*		for(int i=0;i<usersInGroup.size();i++)
						{
						    DoDBAction.get(getClient(sid),  database, "Principal_Add",
							    principal.getName() + Util.SepChar +
							    //principal.getFullName() + Util.SepChar +
							    principal.getType(),ret);
						}*/
				//Commented on 8/5/2017 commented by madhavan
    			//if(usersInGroup != null && usersInGroup.contains(getNamedGroup())){
        			/*if(directorySync.getDirNamedGrpCount() > licenseNamedGrpCount){
        				return NamedUsersCountExceedsLicenseSeats;
        			}*/
        		//}
    			//Commented on 8/5/2017 commented by madhavan
    			/*for(int i=0;i<usersInGroup.size();i++)
    			{
    				DoDBAction.get(getClient(sid),  database, "UserToGroup_Add",
    						principal.getName() + Util.SepChar +
    						(String)usersInGroup.get(i) ,null);
    			}	*/	
    		}
    		else if(principal.getType()==Principal.GROUP_TYPE)
    		{
    			List groupsToUser = directorySync.getUsersInGroup(principal.getName());
    			//getGroupsToUser(principal.getName());
    			if(principal != null && principal.getName().equalsIgnoreCase(getNamedGroup())){	
    				/*if(directorySync.getDirNamedGrpCount() > licenseNamedGrpCount){
    					return NamedUsersCountExceedsLicenseSeats;
    				}*/
    			}
    			if((groupsToUser.size()>0)&&(groupsToUser!=null)){
    				for(int i=0;i<groupsToUser.size();i++)
    				{
    					DoDBAction.get(getClient(sid),  database, "UserToGroup_Add",
    							(String)groupsToUser.get(i) + Util.SepChar +
    							principal.getName() ,null);
    				}
    			}
    		}
    	}
    	return principalId;
    }
    
    /**
     * CV10.2 - Method to sync User/Group from administration
     * @param sid
     * @param principal
     * @return
     */
    public int syncLDAPPrincipals(int sid,Principal principal){
    	VWSLog.dbg("syncLDAPPrincipals...."+SCHEME_LDAP_ADS);
    	if (!assertSession(sid)) return invalidSessionId;
    	try{
    	String schemeType = VWSPreferences.getSSType();
    	Vector resultValue=new Vector();
    	String param = "";
    	String domainName="";
    	String ldapDirectory = "";
		ldapDirectory = VWSPreferences.getTree();
		String[] dirList = ldapDirectory.split(",");
		VWSLog.dbg("dirList.size...."+dirList.length);
		VWSLog.dbg("principal.getName()....."+principal.getName());
		
    	if(schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
    		for (int i=0;i<dirList.length;i++) {
	    		Map dirMap=directory.getLdapDirectories();
	    		VWSLog.dbg("dirMap in addPrincipal........."+dirMap);
	    		String ldapCurDomainSuffix=principal.getName();
	    		VWSLog.dbg("ldapCurDomainSuffix: " + ldapCurDomainSuffix);
	    		
	    		//String ldapDirectoryName=ldapCurDomainSuffix.substring(ldapCurDomainSuffix.indexOf("@")+1,ldapCurDomainSuffix.length());
	    		String ldapDirectoryName = dirList[i];
	    		
	    		VWSLog.dbg("ldapDirectoryName: " + ldapDirectoryName);
	    		String curLoginServer=(String) dirMap.get(ldapDirectoryName);
	    		VWSLog.dbg("curLoginServer in addprincipal........."+curLoginServer);
	    		Directory directory1 = Directory.getInstance(ldapDirectoryName,curLoginServer,true);
	    		VWSLog.dbg("ldapDirectoryName ::"+ldapDirectoryName);
	    		VWSLog.dbg("Principal.getName(): " + principal.getName());
	    		String userFullInfo=directory1.getUserFullInfoOtherOU(principal.getName());
	    		VWSLog.dbg("userFullInfo fetch from all OU's :::"+userFullInfo);
	    		if(userFullInfo.length()==0)
	    			userFullInfo=directory1.getUserFullInfo(principal.getName());
	    		VWSLog.dbg("userFullInfo  :::"+userFullInfo);	    			
	    			if(userFullInfo.length()>0){
		    			StringTokenizer st = new StringTokenizer(userFullInfo, Util.SepChar);
		    			VWSLog.dbg("st.hashtokens "+st.countTokens());
		    			if(st.hasMoreTokens()){
		    				String name = st.nextToken();
		    				VWSLog.dbg("name :::"+name);
		    				String principalName = name;
		    				if (name.contains("@"))
		    					principalName = name.substring(0,name.indexOf("@"));
		    				VWSLog.dbg("principalName :::"+principalName);
//		    				VWSLog.add("name.substring(0,name.indexOf(@)-1:::"+name.substring(0,name.indexOf("@")));
		    				String upnSuffix=name.substring(name.indexOf("@")+1,name.length());
		    				String userDn=st.nextToken();
		    				VWSLog.dbg("userDn :::"+userDn);
		    				String samName=st.nextToken();
		    				VWSLog.dbg("samName :::"+samName);
		    				String emailId=st.nextToken();
		    				VWSLog.dbg("emailId :::"+emailId);
		    				if(userDn.length()>0){
		    					String domainCoponent=userDn.substring(userDn.indexOf("DC"),userDn.length());
		    					String[] splittedDC=domainCoponent.split(",");
		    					for(String dcval:splittedDC){
		    						dcval=dcval.substring(dcval.indexOf("=")+1,dcval.length());
		    						domainName=domainName+dcval+".";
		    					}
		    					domainName=domainName.substring(0,domainName.lastIndexOf("."));
		    					VWSLog.dbg("domainName :"+domainName);
		    				}
		    				if (dirList.length>1) {
		    					if (directory1.getDirName().equalsIgnoreCase(dirList[0])) {
	    							param = "16"+Util.SepChar+principalName+Util.SepChar+userDn+Util.SepChar+samName+Util.SepChar+emailId+Util.SepChar+domainName+Util.SepChar+upnSuffix;
	    						} else {
	    							param = "16"+Util.SepChar+name+Util.SepChar+userDn+Util.SepChar+samName+Util.SepChar+emailId+Util.SepChar+domainName+Util.SepChar+upnSuffix;
	    						}
		    					
		    				}
		    				else
		    					param = "8"+Util.SepChar+name+Util.SepChar+userDn+Util.SepChar+samName+Util.SepChar+emailId+Util.SepChar+domainName;
		    				VWSLog.dbg("param value before calling update email: " + param);
	    					i=dirList.length;
		    			}
	    			
	    		} 
	    		VWSLog.dbg("Param before calling user_get..." + param);
	    		DoDBAction.get(null,  database, "User_Get", param,resultValue);
	    	}
    	}
    	}catch(Exception e){
    		return sid;
    	}
		return sid;    	
    }
    
    public int syncPrincipals(int sid)
    {
    	VWSLog.dbg("syncPrincipals method sid val--->" + sid);
    	//From AdminWise Synch button
    	if (!assertSession(sid)) return invalidSessionId;
    	try{
    		//Get the Named license seat count and send to the directory object for displaying the message if named group user count exceed the named group license seat count
    		int licenseAdminGrpCount=getLicenseAdminGroupCount();
    		int licenseNamedGrpCount = getLicenseNamedGroupCount();    		
    		String schemeType = VWSPreferences.getSSType();
    		
    		if(schemeType.equalsIgnoreCase(SSCHEME_ADS) ){
				DirectorySync directorySync = (new DirectorySync(database, directory, true, getAdminGroup(),
						getNamedGroup(), getNamedOnlineGroup(), getConcurrentGroup(), getConcurrentOnlineGroup(),
						getProfessionalNamedGroup(), getProfessionalConcurrentGroup(), getPublicWAGroup(),
						getSubAdminGroup(), getBatchInputAdminGroup(), licenseAdminGrpCount, licenseNamedGrpCount,
						getLicenseNamedOnlineGroupCount(), getLicenseConcurrentGroupCount(),
						getLicenseConcurrentOnlineGroupCount(), getLicenseProfessionalNamedGroupCount(),
						getLicenseProfessionalConcurrentGroupCount(), getLicensePublicWACount(),
						getLicenseSubAdminGroupCount(), getLicenseAIPGroupCount(), false));
	    		VWSLog.dbg("directorySync.isSyncFailed()--->" + directorySync.isSyncFailed());
	    		if (directorySync.isSyncFailed()) {
	    			return syncFailed;
	    		}
	    		directorySync.syncProcess(false);
    		}/*if ( directorySync.getDirNamedGrpCount() > licenseNamedGrpCount){
    			return NamedUsersCountExceedsLicenseSeats;
    		}else{*/
    		
    		//}    	

    		/**
    		 * Build : Contentverse 8 Post build 04
    		 * Date : 12/12/2013
    		 * UnCommented the below code for updating user mail id based on the registry 'synchusermailid'. 
    		 */
    	
    		if(schemeType.equalsIgnoreCase(SCHEME_LDAP_NOVELL) || schemeType.equalsIgnoreCase(SCHEME_LDAP_ADS)){
    			String loginServer = "";
    			String ldapDirectory = "";
    			ldapDirectory = VWSPreferences.getTree();
    			loginServer = VWSPreferences.getSSLoginHost();
    			String type = VWSPreferences.getSSType();
    			String[] dirList = ldapDirectory.split(",");
    			String[] serList = loginServer.split(",");
    			for(int dirCount=0; dirCount< dirList.length; dirCount++){
    			Directory directory1 = Directory.getInstance(dirList[dirCount],serList[dirCount],true);
    			DirectorySync directorySync1 = (new DirectorySync(database, directory1, true, getAdminGroup(), getNamedGroup(),getNamedOnlineGroup(),getConcurrentGroup(),getConcurrentOnlineGroup(),getProfessionalNamedGroup(),getProfessionalConcurrentGroup(),getPublicWAGroup(),getSubAdminGroup(),getBatchInputAdminGroup(),licenseAdminGrpCount,licenseNamedGrpCount,getLicenseNamedOnlineGroupCount(),getLicenseConcurrentGroupCount(),getLicenseConcurrentOnlineGroupCount(),getLicenseProfessionalNamedGroupCount(),getLicenseProfessionalConcurrentGroupCount(),getLicensePublicWACount(),getLicenseSubAdminGroupCount(),getLicenseAIPGroupCount(),false));
    			if (directorySync1.isSyncFailed())
        			return syncFailed;
    			directorySync1.syncProcess(false);
    			directorySync1.syncUserMailId();
    			}
    			VWSLog.dbg("After  calling sync uer mail id .......");
    		}
    	}catch (Exception e) {
    		// TODO: handle exception
    		VWSLog.dbg("Exception while sync process....."+e.getMessage());
    	}    	
    	return NoError;
    }
    public int getLicenseProfessionalNamedGroupCount(){
    	int licenseProfessionalNamedGrpCount=-1;
     	try{    
    	LicenseManager lm =  LicenseManagerImpl.getInstance();    		
		if (lm != null){
			com.computhink.vws.license.License lic  = lm.getLicense();
    	if(lic.getProfessionalWebAccessSeats() >0){
    	licenseProfessionalNamedGrpCount= lic.getProfessionalWebAccessSeats();
    	}
		}
     	}
     	catch(Exception e){
     		
     	}
		return licenseProfessionalNamedGrpCount;
    }
    /**
     *  CV10.1 Public webaccess enhancement.
     * @return
     */
    public int getLicenseProfessionalConcurrentGroupCount(){
    	int licenseProfessionalConcurrentGrpCount=-1;
    	try{    
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			if(lic.getProfessionalConcWebAccessSeats() >0){
    				licenseProfessionalConcurrentGrpCount= lic.getProfessionalConcWebAccessSeats();
    			}
    		}
    	}
    	catch(Exception e){

    	}
    	return licenseProfessionalConcurrentGrpCount;
    }
    /**
     * CV10.1 Public webaccess enhancement.
     * @return
     */
    public int getLicenseConcurrentGroupCount(){
    	int licenseConcurrentGrpCount=-1;
    	try{    
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseConcurrentGrpCount = lic.getDesktopSeatsConcurrent();
    		/*	if(lic.getWebliteSeatsConcurrent() >licenseConcurrentGrpCount){
    				licenseConcurrentGrpCount= lic.getWebaccessSeatsConcurrent();
    			}
    			if(lic.getWebtopSeatsConcurrent() > licenseConcurrentGrpCount){
    				licenseConcurrentGrpCount= lic.getWebtopSeatsConcurrent();
    			}if(lic.getWebaccessSeatsConcurrent() > licenseConcurrentGrpCount){
    				licenseConcurrentGrpCount= lic.getWebaccessSeatsConcurrent();
    			}*/
    		}
    	}
    	catch(Exception e){

    	}
    	return licenseConcurrentGrpCount;
    }
    public int getLicenseAdminGroupCount(){
    	int licenseAdminGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseAdminGrpCount = lic.getAdminWiseSeats();
    		}    		
    	}catch (Exception e) {}
    	return licenseAdminGrpCount;
    }
    
	public int getLicenseSubAdminGroupCount() {
		int licenseSubAdminGrpCount = -1;
		try {
			LicenseManager lm = LicenseManagerImpl.getInstance();
			if (lm != null) {
				com.computhink.vws.license.License lic = lm.getLicense();
				licenseSubAdminGrpCount = lic.getSubAdminSeats();
			}
		} catch (Exception e) {
		}
		return licenseSubAdminGrpCount;
	}
	
	public int getLicenseAIPGroupCount() {
		int licenseAIPGrpCount = -1;
		try {
			LicenseManager lm = LicenseManagerImpl.getInstance();
			if (lm != null) {
				com.computhink.vws.license.License lic = lm.getLicense();
				licenseAIPGrpCount = lic.getAipSeats();
			}
		} catch (Exception e) {
		}
		return licenseAIPGrpCount;
	}
    
    public int getLicenseNamedGroupCount(){
    	int licenseNamedGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseNamedGrpCount = lic.getDesktopSeatsNamed();
    			
    			/*if(lic.getWebliteSeatsNamed() > licenseNamedGrpCount)
    				licenseNamedGrpCount = lic.getWebliteSeatsNamed();
    			if(lic.getWebtopSeatsNamed() > licenseNamedGrpCount)
    				licenseNamedGrpCount = lic.getWebtopSeatsNamed();
    			if(lic.getWebAccessSeats() > licenseNamedGrpCount)
    				licenseNamedGrpCount = lic.getWebAccessSeats();
    			//Added for License changes, Madhavan.B
    			if(lic.getSdkSeats()>licenseNamedGrpCount)
    				licenseNamedGrpCount=lic.getSdkSeats();*/
    			/**Commented for professional group license changes */
    			/*if(lic.getProfessionalWebAccessSeats() >0)
    				licenseNamedGrpCount =licenseNamedGrpCount+ lic.getProfessionalWebAccessSeats();*/
    			/*End of comment*/
    			//
    			//Commented for License changes, Madhavan.B
    			/*if(lic.getProfessionalWebAccessSeats() > licenseNamedGrpCount)
    				licenseNamedGrpCount = lic.getProfessionalWebAccessSeats();*/
    			//
    		}    		
    	}catch (Exception e) {}
    	return licenseNamedGrpCount;
    }
    
    public int getLicenseNamedOnlineGroupCount(){
    	int licenseNamedOnlineGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseNamedOnlineGrpCount = lic.getNamedOnlineWebaccessSeats();
    			
    			
    		}    		
    	}catch (Exception e) {}
    	return licenseNamedOnlineGrpCount;
    }
    
    
    public int getLicenseConcurrentOnlineGroupCount(){
    	int licenseConcurrentOnlineGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licenseConcurrentOnlineGrpCount = lic.getWebaccessSeatsConcurrent();
    			VWSLog.dbg("licenseConcurrentOnlineGrpCount ::::"+licenseConcurrentOnlineGrpCount);
    			
    		}    		
    	}catch (Exception e) {}
    	return licenseConcurrentOnlineGrpCount;
    }
    
    public int getLicensePublicWACount(){
    	int licensepublicWAGrpCount = -1;
    	try{    		
    		LicenseManager lm =  LicenseManagerImpl.getInstance();    		
    		if (lm != null){
    			com.computhink.vws.license.License lic  = lm.getLicense();
    			licensepublicWAGrpCount = lic.getCustomLabelWebAccessSeats();
    			VWSLog.add("Inside publicWA lic count ::::"+licensepublicWAGrpCount);
    		}    		
    	}catch (Exception e) {}
    	return licensepublicWAGrpCount;
    }
    
    public int getPrincipal(int sid, Principal princ, Vector ret)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getPrincipal method sid val--->" + sid);
        String param = princ.getName() + Util.SepChar + "2";
        DoDBAction.get(getClient(sid),  database, "Principal_GetByName", param, ret);
        return NoError;
    }
    public int getPrincipalMembers(int sid, String pid, Vector Principals)
    {
    	VWSLog.dbgCheck("find getPrincipalMembers method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        return 
           DoDBAction.get(getClient(sid),  database, "Principal_GetGroupUsers", pid, Principals);
    }
    public int getAclFor(int sid, Acl acl, String user) 
    {
    	VWSLog.dbgCheck("getAclFor method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        String param = String.valueOf(acl.getNodeId()) + Util.SepChar + user; 
        DoDBAction.get(getClient(sid),  database, "GetAcl", param, ret);
        if (ret.size() > 0)
        {
            String perms = (String) ret.get(0);
            Iterator iterator = acl.getEntries().iterator();
            AclEntry entry = (AclEntry) iterator.next();
            entry.setNavigate(Util.to_Number(perms.substring(0,1)));
            entry.setView(Util.to_Number(perms.substring(1,2)));
            entry.setDelete(Util.to_Number(perms.substring(2,3)));
            entry.setShare(Util.to_Number(perms.substring(3,4)));
            entry.setModify(Util.to_Number(perms.substring(4,5)));
            entry.setCreate(Util.to_Number(perms.substring(5,6)));
            entry.setAssign(Util.to_Number(perms.substring(6,7)));
            /// Enhancement for Security - No 32
            entry.setLaunch(Util.to_Number(perms.substring(7,8)));
            entry.setExport(Util.to_Number(perms.substring(8,9)));
            entry.setPrint(Util.to_Number(perms.substring(9,10)));
            entry.setMail(Util.to_Number(perms.substring(10,11)));
            entry.setSendTo(Util.to_Number(perms.substring(11,12)));
            entry.setCheckInOut(Util.to_Number(perms.substring(12,13)));
 
            /*
             * Enhancement for Security - No 32
             * When upgrade from 5.6.4 to 6.0;  The new permission added in the 'Share' privilege.
             * Launch
             * Export
             * Print
             * Mail
             * SendTo
             * CheckIn/ CheckOut
             * 
             *  In 5.6.4, if 'Share' Privilege selected; all new permission should be selected. 
             */
            
            if (perms.substring(3,4).equals("1") &&
            	perms.substring(7,8).equals("0") &&
            	perms.substring(8,9).equals("0") &&
            	perms.substring(9,10).equals("0") &&
            	perms.substring(10,11).equals("0") &&
            	perms.substring(11,12).equals("0") &&
            	perms.substring(12,13).equals("0")){
            		//VWSLog.add("Update 5.6.4 to 6.0 ");
	                entry.setLaunch(1);
	                entry.setExport(1);
	                entry.setPrint(1);
	                entry.setMail(1);
	                entry.setSendTo(1);
	                entry.setCheckInOut(1);
            }
            
            ///
        }
        return NoError;
    }
    public int getAcl(int sid, Acl acl) 
    {
    	VWSLog.dbgCheck("getAcl method sid val--->" + sid);
        Client client = getClient(sid);
        String user = client.getUserName();
        //VWSLog.err("user in getAcl "+user);
        return getAclFor(sid, acl, user); 
    }
    public int getNodeAcl(int sid, Acl acl, Principal princ, boolean effective) 
    {
    	VWSLog.dbgCheck("getNodeAcl method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        return getNodeAcl(sid, acl, effective, princ);
    }
    public int setNodeAcl(int sid, Acl acl) 
    {
    	VWSLog.dbgCheck("setNodeAcl method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        return setNodeAcl(sid, acl, client.getUserName(), client.getIpAddress());
    }
    //--------------------------Security Privates-------------------------------
    private int getNodeAcl(int sid, Acl acl, boolean inheritance, Principal principal)
    {
        Node node = acl.getNode();
        if ( getNodeProperties(sid, node) == Error) return Error;
        acl.setId(node.getAclId());
        Vector entries = getNodeAclEntries(sid, node, principal);
        
        if ( entries.size() > 0)
        {
            for (int i=0; i < entries.size(); i++)
            {
                AclEntry entry = (AclEntry) entries.get(i);
                /*
                 * Enhancement for Security - No 32
                 * When upgrade from 5.6.4 to 6.0;  The new permission added in the 'Share' privilege.
                 * Launch
                 * Export
                 * Print
                 * Mail
                 * SendTo
                 * CheckIn/ CheckOut
                 * 
                 *  In 5.6.4, if 'Share' Privilege selected; all new permission should be selected. 
                 */
                               
                if (entry.canShare() == 1 &&
                	entry.canLaunch() == 0 &&
                	entry.canExport() == 0 &&
                	entry.canPrint() == 0 &&
                	entry.canMail() == 0 &&
                	entry.canSendTo() == 0 &&
                	entry.canCheckInOut() == 0 ){
                    		//VWSLog.add("get Node Acl Update 5.6.4 to 6.0 ");
        	                entry.setLaunch(1);
        	                entry.setExport(1);
        	                entry.setPrint(1);
        	                entry.setMail(1);
        	                entry.setSendTo(1);
        	                entry.setCheckInOut(1);
        	                // calling the update method for the modified entry.
        	                try{        	                
        	                	updateAclEntry(sid, entry);
        	                }catch(Exception ex){}
        	                
                    }
                
                acl.addEntry(entry);
            }
            //return NoError;
        }
        
        if (inheritance)    
        {
            int nodeParentId = node.getParentId();
            while (nodeParentId > 0)        
            {
                Node pNode = new Node(nodeParentId);
                if (getNodePropertiesByNodeID(sid, pNode) == Error) return Error;
                Vector e = getNodeAclEntries(sid, pNode, principal);
                if ( e.size() > 0)
                {
                    for (int i=0; i < e.size(); i++)
                    {
                        AclEntry entry = (AclEntry) e.get(i);
                        entry.setInherited(true);
                        //  When the source node is a Document we need to
                        //  translate (Cast) its parent (Node) permissions to 
                        //  Document Permissions.
                        //if (node.isDocument()) entry.castPermissions();
                        acl.addEntry(entry);    
                    }
                    //break; //We only get entries of first parent found!!
                }
                nodeParentId = pNode.getParentId();
            }
        }
        return NoError;
    }
    
    // Add the method to list the assigned permissions    
    private StringBuffer getSelectedPermission(AclEntry entry){
    	
        StringBuffer permissions = new StringBuffer();
        permissions.append(entry.canAssign() == 1 ?"Assign, ":"");
        permissions.append(entry.canCreate() == 1 ?"Create, ":"");                
        permissions.append(entry.canModify() == 1 ?"Modify, ":"");
        permissions.append(entry.canDelete() == 1 ?"Delete, ":"");
        permissions.append(entry.canLaunch() == 1 ?"Launch, ":"");
        permissions.append(entry.canExport() == 1 ?"Export, ":"");
        permissions.append(entry.canPrint() == 1 ?"Print/Fax, ":"");
        permissions.append(entry.canMail() == 1 ?"Mail, ":"");
        permissions.append(entry.canSendTo() == 1 ?"SendTo, ":"");
        permissions.append(entry.canCheckInOut() == 1 ?"CheckIn/CheckOut, ":"");                                
        permissions.append(entry.canView() == 1 ?"View, ":"");
        permissions.append(entry.canNavigate() == 1 ?"Navigate ":"");
        
    	return permissions;
    }
    private int setNodeAcl(int sid, Acl acl, String user, String ip)
    {
    	VWSLog.dbgCheck("setNodeAcl method sid val--->" + sid);
        String secDesc = "";
        int aclid = 0;
        Node node = acl.getNode();
        Acl dbAcl  = new Acl(node);
        
        int nodeType = 0;
        Node nodeObj = new Node(node.getId());
        int ret = getNodePropertiesByNodeID(sid, nodeObj);
        nodeType = nodeObj.getType();
        int notifyId = 0;
        if(nodeType == VNSConstants.nodeType_Folder){
        	notifyId = VNSConstants.notify_Fol_PermissionMod;
        }else if(nodeType == VNSConstants.nodeType_Document){
        	notifyId = VNSConstants.notify_Doc_PermissionMod;
        }
        
        Iterator newEntries = acl.getEntries().iterator();
        if (getNodeAcl(sid, dbAcl, false, null) == Error) return Error; 
        //--------//No entries for this node...add all new entries -------------
        if (dbAcl.getEntries().size() == 0)
        {
        	while (newEntries.hasNext())
        	{
        		AclEntry entry = (AclEntry) newEntries.next();
        		if (aclid > 0) entry.setAclId(aclid);
        		aclid = addAclEntry(sid, node, entry);
        		StringBuffer permissions = getSelectedPermission(entry);
        		secDesc = "New Security entry on '" + node.getName() + "' for '" 
        		+ entry.getPrincipal().getName() + "' - assigned permissions : " + permissions.toString();
        		addATRecord(sid, user, ip, node.getId(), 
        				VWATEvents.AT_OBJECT_SECURITY, VWATEvents.AT_SEC_NEW, secDesc);
        		message = "New Security entry on  '"+node.getName()+"' for '"+entry.getPrincipal().getName()+"' - assigned permissions: '"+permissions.toString()+"'";
        		addNotificationHistory(sid, node.getId(), nodeType, notifyId, user, "-", "-", message);

        		//Comment the following code, this will cause the issue of if multiple user add with security it is adding only
        		// first user.
        		//acl.removeEntry(entry);
        	}
        	newEntries = acl.getEntries().iterator();
        }else{
        	while (newEntries.hasNext())
        	{
        		AclEntry entry = (AclEntry) newEntries.next();
        		AclEntry dbEntry = dbAcl.getEntry(entry.getPrincipal());
        		if (dbEntry == null)
        		{
        			entry.setAclId(dbAcl.getId());
        			StringBuffer permissions = getSelectedPermission(entry);
        			secDesc = "New Security entry on '" + node.getName() + "' for '" 
        			+ entry.getPrincipal().getName() + "' - assigned permissions : " + permissions.toString();
        			addATRecord(sid, user, ip, node.getId(), 
        					VWATEvents.AT_OBJECT_SECURITY, VWATEvents.AT_SEC_NEW, secDesc);
        			message = "New Security entry on '"+node.getName()+"' for '"+entry.getPrincipal().getName()+"' - assigned permissions : '"+permissions.toString()+"'";
        			addNotificationHistory(sid, node.getId(), nodeType, notifyId, user, "-", "-", message);
        			addAclEntry(sid, node, entry);
        		}
        		else
        		{
        			entry.setId(dbEntry.getId());
        			StringBuffer permissions = getSelectedPermission(entry);
        			secDesc = "Security entry update on '" + node.getName() + 
        			"' for '" + entry.getPrincipal().getName() + "' - assigned permissions : " + permissions.toString();
        			addATRecord(sid, user, ip, node.getId(), 
        					VWATEvents.AT_OBJECT_SECURITY, VWATEvents.AT_SEC_UPDATE, secDesc);
        			message = "Security entry update on '"+node.getName()+"' for '"+entry.getPrincipal().getName()+"' - assigned permissions : '"+permissions.toString()+"'";
        			addNotificationHistory(sid, node.getId(), nodeType, notifyId, user, "-", "-", message);
        			updateAclEntry(sid, entry);
        		}
        		dbAcl.removeEntry(dbEntry);
        	}
        }
        Iterator dbEntries = dbAcl.getEntries().iterator();
        while (dbEntries.hasNext())
        {
            AclEntry entry = (AclEntry) dbEntries.next();
            StringBuffer permissions = getSelectedPermission(entry);
            secDesc = "Security entry removed - '" + node.getName() + "' for '"
                                         + entry.getPrincipal().getName() + "' - assigned permissions " + permissions.toString();
            removeAclEntry(sid, entry);
            addATRecord(sid, user, ip, node.getId(), VWATEvents.AT_OBJECT_SECURITY, 
                                             VWATEvents.AT_SEC_REMOVE, secDesc);
            message = "Security entry removed - '"+node.getName()+"' for '"+entry.getPrincipal().getName()+"' - assigned permissions '"+permissions.toString()+"'";
            addNotificationHistory(sid, node.getId(), nodeType, notifyId, user, "-", "-", message);
        }
        return NoError;
    }
    private int addAclEntry(int sid, Node node, AclEntry entry)  
    {
        int principalId = entry.getPrincipal().getId();
        VWSLog.dbgCheck("addAclEntry method sid val--->" + sid);
        Vector ret = new Vector();
        String param =  String.valueOf(entry.getAclId()) + Util.SepChar + 
                        String.valueOf(node.getId()) + Util.SepChar + 
                        String.valueOf(entry.getType()) + Util.SepChar + 
                        String.valueOf(principalId) + Util.SepChar + 
                        entry.getMask();
        DoDBAction.get(getClient(sid),  database, "AclEntry_Add", param, ret);
        if(ret.size()== 0) return 0;
        return Util.to_Number( (String) ret.get(0));
    }
    private int getPrincipalId(Principal principal) 
    {
        int id = principal.getId(); if(id > 0) return id;
        
        Vector ret = new Vector();
        String param = principal.getName() + Util.SepChar + principal.getType();
        DoDBAction.get(null, database, "Principal_GetByName", param, ret);
        
        if(ret.size()== 0) return 0;
        return Util.to_Number( (String) ret.get(0));
    }
    private void updateAclEntry(int sid, AclEntry entry)  
    {
    	VWSLog.dbgCheck("updateAclEntry method sid val--->" + sid);
        String param = String.valueOf(entry.getId()) + Util.SepChar + entry.getMask();
        Vector ret = new Vector();
        //DoDBAction.set(database, "AclEntry_Update", param, 
        //                                       entry.getPermissionsMask(), ret);
        DoDBAction.get(getClient(sid),  database, "AclEntry_Update", param, ret);
    }
    private void removeAclEntry(int sid, AclEntry entry) 
    {
    	VWSLog.dbgCheck("removeAclEntry method sid val--->" + sid);
        Vector ret = new Vector();
        String param = String.valueOf(entry.getId());
        DoDBAction.get(getClient(sid),  database, "AclEntry_Del", param, ret);
    }
    
    public int getNodePropertiesByNodeID(int sid, Node node) 
    {
    	VWSLog.dbgCheck("getNodePropertiesByNodeID method sid val--->" + sid);
        if (node.getId() <= 0) return Error;
        Vector ret = new Vector();
        String param = "";
        	param = node.getId() + Util.SepChar;
        DoDBAction.get(getClient(sid),  database, "Node_GetNode", param , ret);
        
        if(ret.size() > 0) 
        {
            StringTokenizer st = new StringTokenizer
                                           ( (String) ret.get(0), Util.SepChar);
            st.nextToken();     //skip id
            node.setName(st.nextToken());
            node.setType(Util.to_Number(st.nextToken()));
            node.setParentId(Util.to_Number(st.nextToken()));
            node.setStorageId(Util.to_Number(st.nextToken()));
            node.setAclId(Util.to_Number(st.nextToken()));
            //added node path for notification funcationality
            node.setPath(st.nextToken());
            return NoError;
        }
        else
            return Error;
    }
// get the node is having the acl entry or not. 
    public int getAclForNode(int sid, int nodeId, String userName) 
    {
    	VWSLog.dbgCheck("getAclForNode method sid val--->" + sid);
    	// checkPermission - 0 is denied create permission ; 1 is having permission; 2 is not acl entry 
    	if (!assertSession(sid)) return invalidSessionId;
    	Vector ret = new Vector();    	
        String param = String.valueOf(nodeId) + Util.SepChar +
        String.valueOf(userName);
        DoDBAction.get(getClient(sid),  database, "VWGetCREATEPERMISSIONS", param, ret);
        int checkPermission = 1;
        if (ret.size() > 0){
        	try{
        	checkPermission = Integer.parseInt(ret.get(0).toString());
        	}
        	catch(Exception ex){checkPermission = 1;}
        }
    	return checkPermission;
    }
    private Vector getNodeAclEntries(int sid, Node node, Principal principal) 
    {
    	VWSLog.dbgCheck("find getNodeAclEntries method sid val--->" + sid);
        Vector entries = new Vector();
        Vector ret = new Vector();
        int principalId = (principal == null? 0 : principal.getId());
        String param = String.valueOf(node.getId()) + Util.SepChar +
                       String.valueOf(principalId);
        DoDBAction.get(getClient(sid),  database, "AclEntry_GetByNode", param, ret);
        if(ret.size() > 0)
        {
            for (int i = 0; i < ret.size(); i++)
                entries.add(new AclEntry((String) ret.get(i)));
        }
        else    // no entries found
        {
            //are we searching for a Principal User?
            if (principal != null && principal.getType() == Principal.USER_TYPE)
            {
                //get the Groups this principal belongs to
                Vector principals = getPrincipals(sid, principal);
                for (int x = 0; x < principals.size(); x++)
                    entries.addAll(getNodeAclEntries
                                         (sid, node, (Principal) principals.get(x))); 
            }
        }
        return entries;
    }
    private Vector getPrincipals(int sid, Principal principal)
    {
    	VWSLog.dbgCheck("getPrincipals method sid val--->" + sid);
        Vector ret = new Vector();
        Vector principals = new Vector();
        String param = String.valueOf(principal.getId());
        DoDBAction.get(getClient(sid),  database, "Principal_GetGroups", param, ret);
        if (ret.size() > 0)
             for (int i = 0; i < ret.size(); i++)
                 principals.add(new Principal((String) ret.get(i)));
        return principals;
    }
    private Principal getPrincipal(int sid, int pid) 
    {
    	VWSLog.dbgCheck("getPrincipal5151515 method sid val--->" + sid);
        Vector ret = new Vector();
        DoDBAction.get(getClient(sid),  database, "Principal_Get", String.valueOf(pid) , ret);
        if(ret.size() == 0) return null;
        return new Principal((String) ret.get(0) );
    }
    private boolean isDefaultStorage() 
    {
        Vector ret = new Vector();
        String param = "";
        if(isOracle)
        	param = "0"+Util.SepChar;
        else
        	param = "0";
        DoDBAction.get(null, database, "Storage_Get", param , ret);
        if (ret.size() > 0)
            return true;
        else 
            return false;
    }
    
    public int getAzureStore(ServerSchema svr, Vector stores) 
    {
    	Vector ret = new Vector();
    	try{
    		String param = "";
    		param = "4" + Util.SepChar+"0"+Util.SepChar;
    		DoDBAction.get(null, database, "Storage_Get", param , ret);
    		if (ret.size() > 0){
    			//VWSLog.add("return value is ::::"+ret);
    			stores.addAll(ret);
    		}
    	}catch(Exception e){

    		return Error;
    	}
    	return NoError;
    }
    
    
    public int getStorageFromId(String storageId,Vector stores) 
    {
    	Vector ret = new Vector();
    	try{
    		String param = "";
    		param = "1" + Util.SepChar+storageId+Util.SepChar;
    		DoDBAction.get(null, database, "Storage_Get", param , ret);
    		if (ret!=null&&ret.size() > 0){
    			VWSLog.dbg("return value getStorageFromId is ::::"+ret);
    			stores.addAll(ret);
    		}
    	}catch(Exception e){

    		return Error;
    	}
    	return NoError;

    }
    
    public int getAzureStorageCredentials(int sid,int docId,Vector storageCredentials) 
    {
    	 if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
    	try{
        String param = "";
        	param = "4" + Util.SepChar+docId;
        
        DoDBAction.get(null, database, "Storage_Get", param , ret);
        if (ret.size() > 0){
        	storageCredentials.addAll(ret);
        }
    	}catch(Exception e){
    		
    		return Error;
    	}
		return NoError;
        
    }
    public int getDSSforDoc(int sid, Document doc, ServerSchema ss) 
    {
    	VWSLog.dbgCheck("getDSSforDoc method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        String param = "";
        	param = "4" + Util.SepChar+doc.getId();
        DoDBAction.get(getClient(sid),  database, "Storage_Get", param , ret);
        if (ret.size() > 0)
        {
        	try {
	            ServerSchema dss = new ServerSchema((String) ret.get(0));
	            /* If there is some issue in getting storage location. Eg. for some of the cabinet storage 
	            * location is specified and for some it is not specified. Then, ServerSchema constructor is
	            * throwing exception. So catching it and returing -1 for that.*/
	            if(dss.getId() == -1 && dss.getHostName().equalsIgnoreCase("")) {
	            	VWSLog.add("Returning -1 Error Bz ServerSchema ID :"+dss.getId()+" & HostName :"+dss.getHostName());
	            	return Error;
	            }
	            dss.type = SERVER_DSS;
	            dss.path = dss.path + Util.pathSep + String.valueOf(doc.getId()) 
	                                                                 + Util.pathSep;
	            if (!doc.getVersion().equals(""))
	            {
	            	VWSLog.dbg("Inside version check of getDSSforDoc--->");
	                Vector versions = new Vector();
	                getDocumentVers(sid, doc.getId(), doc.getVersion(),  versions);
	                if (versions.size() > 0)
	                {
	                    Document ver = new Document((String) versions.get(0), true);
	                    dss.path += ver.getVersionDocFile();
	                }
	                else
	                {
	                    //requesting a version that does not exist
	                    dss.path = ""; 
	                }
	            }
	            VWSLog.dbg("After version check dss value in getDSSforDoc--->"+ dss);
	            ss.set(dss);
        	} catch (Exception e) {
        		VWSLog.add("Exception in getDSSforDoc : "+ e.getMessage());
        		return Error;
        	}
            return NoError;
        }
        else 
            return Error;
    }
    public int getDSSForLocation (int sid, int moveLocationId, ServerSchema ss){
    	VWSLog.dbgCheck("getDSSForLocation method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        String param = "";
        	param = "4" + Util.SepChar+moveLocationId;
        
        DoDBAction.get(getClient(sid),  database, "Storage_Get", param , ret);
        if (ret.size() > 0) {
            ServerSchema dss = new ServerSchema((String) ret.get(0));
            /* If there is some issue in getting storage location. Eg. for some of the cabinet storage 
            * location is specified and for some it is not specified. Then, ServerSchema constructor is
            * throwing exception. So catching it and returing -1 for that.*/
            if(dss.getId() == -1 && dss.getHostName().equalsIgnoreCase(""))
            	return Error;
            dss.type = SERVER_DSS;
            dss.path = dss.path + Util.pathSep;
            ss.set(dss);
            return NoError;
        }
        else 
            return Error;
    }
    public int getDSSList(int sid, Vector dssList)
    {
    	VWSLog.dbgCheck("getDSSList method sid val--->" + sid);
        Vector ret = new Vector();
        String param = "";
        if(isOracle)
        	param = "9"+Util.SepChar;
        else
        	param = "9";
        DoDBAction.get(getClient(sid),  database, "Storage_Get", param , dssList);
        if (ret.size() > 0)
        {
            return NoError;
        }
        else 
            return Error;
    }
    //-------------------------Public methods for DSS Server--------------------
    public int getDSSStores(ServerSchema svr, Vector stores)
    {
        String param = svr.address + Util.SepChar +
                                                    String.valueOf(svr.comport);
        return DoDBAction.get(null, database, "Storage_DSSGet", param , stores);
    }
    
    public int updateAzureStorageInfo(String storageValue,String StorageUserName,String StoragePassword)    {
        Vector result=new Vector();
        String param = "";
        try{
        	String storageArray[]=storageValue.split(Util.SepChar);
        	VWSLog.dbg("Storage Id :"+storageArray[0]);
        	VWSLog.dbg("System Name :"+storageArray[1]);
        	VWSLog.dbg("Host Name :"+storageArray[2]);
        	VWSLog.dbg("Storage path :"+storageArray[5]);
        	VWSLog.dbg("Storage type :"+storageArray[7]);
        	VWSLog.dbg("StorageUserName :::"+StorageUserName);
        	VWSLog.dbg("StoragePassword :::"+StoragePassword);
        	if (storageArray[7].equals("Azure Storage")) {
        		param=storageArray[0]+Util.SepChar +storageArray[1]+Util.SepChar+
        				storageArray[2]+Util.SepChar+
        				storageArray[3]+Util.SepChar+
        				storageArray[5]+Util.SepChar + storageArray[7] + Util.SepChar+
        				StorageUserName+ Util.SepChar+
        				StoragePassword+Util.SepChar;
        		VWSLog.dbg("Inside azure updateAzureStorageInfo param::::: "+param);
        		DoDBAction.get(null,  database, "Storage_UpdateInfo", param, result);
        	}

        }catch(Exception e){
        	return Error;
        }
        return NoError;
    }
    
    public int addDSSStore(ServerSchema svr)
    {
        Vector ret = new Vector();
        String param = "";
        if(svr.getStorageType().equals("Azure Storage")){
        	param= svr.name + Util.SepChar +
        			svr.address + Util.SepChar +
        			String.valueOf(svr.comport) + Util.SepChar +
        			String.valueOf(svr.dataport) + Util.SepChar +
        			svr.path + Util.SepChar +
        			svr.hostName+ Util.SepChar +
        			svr.storageType+ Util.SepChar +
        			svr.userName+ Util.SepChar +
        			svr.password+ Util.SepChar ;
        }else{
        	param= svr.name + Util.SepChar +
        			svr.address + Util.SepChar +
        			String.valueOf(svr.comport) + Util.SepChar +
        			String.valueOf(svr.dataport) + Util.SepChar +
        			svr.path + Util.SepChar +
        			svr.hostName+ Util.SepChar +
        			" "+ Util.SepChar +
        			" "+ Util.SepChar +
        			" "+ Util.SepChar ;
        }
        DoDBAction.get(null, database, "Storage_Add", param , ret);
        if (ret.size() > 0) 
        {
            VWSLog.add(room.getName() + ": Added Storage Server: " + 
                                                  svr.path + "@" + svr.address);
            return Util.to_Number( (String) ret.get(0));
        }
        else
            return Error;
    }
    public int removeDSSStore(ServerSchema svr)
    {
        Vector ret = new Vector();
        String param = svr.name + Util.SepChar +
                       svr.address + Util.SepChar +
                       svr.path;
        DoDBAction.get(null, database, "Storage_Del", param , ret);
        if (ret.size() > 0) 
        {
            VWSLog.add(room.getName() + ": Removed Storage Server: " + 
                                                  svr.path + "@" + svr.address);
            return Util.to_Number((String) ret.get(0) );
        }
        else
            return Error;
    }
    //  Issue 511
    public int getDSSIPPort(String location, Vector address)
    {    	 
    	 return DoDBAction.get(null, database, "Storage_DSSIPPortGet", location , address);
    }
    public int getStorageId(String location, Vector retVect) {
		// TODO Auto-generated method stub
    	 return DoDBAction.get(null, database, "Storage_Getid", location , retVect);
	}	 
 
	

    // Issue 511
    //-------------------------Public methods for DSS---------------------------
    public Client getClient(int sid) 
    {
        return (Client) clients.get(new Integer(sid));
    }
    public Room getRoom()
    {
        return room;
    }
    public Object getDirectoryContext() 
    {
        return directory.getContext();
    }
    public String getRoomName() 
    {
        return room.getName();
    }
    public boolean moveDocument(int documentId, int newParentNodeId)
    {
        return false;
    }
    public int lockDocument(int sid, int did)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("lockDocument method sid val--->"+sid );
        Client client = getClient(sid);
        Vector ret = new Vector();
        String param = new String();
        	param = did + Util.SepChar +
        			Util.getOneSpace(client.getUserName()) + Util.SepChar +
        			Util.getOneSpace(client.getIpAddress());
        DoDBAction.get(getClient(sid),  database, "DocumentLock", param , ret);
        
        if( ret.size() == 0 )   return LockDenied;
        int iret = Util.to_Number( (String) ret.get(0) );
        if(iret == 0)            return documentAlreadyLocked;
        if(iret < 0)             return DocumentDeleted;
        return NoError;
    }
    public int updateLockedDocument(int sid, int did)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("updateLockedDocument method sid val--->"+sid );
        Client client = getClient(sid);
        Vector ret = new Vector();
        String param = did + Util.SepChar + 
                       client.getIpAddress()+ Util.SepChar +
                       client.getUserName();
       
        DoDBAction.get(getClient(sid),  database, "VWUpdateDocumentLock", param , ret);
        return NoError;
    }    
    public int unlockDocument(int sid, int did, int force)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("unlockDocument method sid val--->"+sid );
        Client client = getClient(sid);
        Vector ret = new Vector();
        String param ="";
        	if(force==0)
	            param = "1" + Util.SepChar + 
	            		did + Util.SepChar + 
	            		Util.getOneSpace(client.getIpAddress());
	        else
	            param = "5" + Util.SepChar+ did;
        	
        DoDBAction.get(getClient(sid),  database, "DocumentUnLock", param , ret);
        return NoError;
    }
// Add the argument for Index Option either  '0' or '3'
    public int addIndexerDoc(int sid, int nid,int option)
    {
    	VWSLog.dbgCheck(" addIndexerDoc method sid val--->"+sid );
        Vector ret = new Vector();
        String param = String.valueOf(nid) + Util.SepChar + 3;
        DoDBAction.get(getClient(sid),  database, "AddIndexerDoc", param, ret);        
        return NoError;
    }
    private int unlockDocuments(int sid)
    {
    	VWSLog.dbgCheck(" unlockDocuments method sid val--->"+sid );
        Client client = getClient(sid);
        Vector ret = new Vector();
        String param = "";
        	param = "2" + Util.SepChar + Util.getOneSpace(client.getIpAddress());
       
        DoDBAction.get(getClient(sid),  database, "DocumentUnLock", param , ret);
        return NoError;
    }
    private int unlockDocuments(String name)
     {
        Vector ret = new Vector();
        String param = "";
        	param = "3" + Util.SepChar + Util.getOneSpace(name);
        DoDBAction.get(null, database, "DocumentUnLock", param , ret);
        return NoError;
    }
    public int getLockOwner(int sid, int did, Vector name, boolean withMe)
    {
    	VWSLog.dbgCheck("find getLockOwner method"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        //Added For create ownership enhancement,Gurumurthy.T.S 01-03-2014
        Client client = getClient(sid);
        //(client.isAdmin()?"1":"0") is Added For create ownership enhancement,Gurumurthy.T.S 01-03-2014
        String param = "";
        	param = did + Util.SepChar + Util.getOneSpace(client.getUserName())+Util.SepChar+(client.isAdmin()?"1":"0")+ Util.SepChar;
        //VWSLog.add("param "+param);
        DoDBAction.get(getClient(sid),  database, "CheckDocumentIsLock", param , name);
        if (name.size() > 0)
        {
            
            //VWSLog.add("result "+name.toString());
            // returning record from document lock means locked by common user
            //"0" means doc is in route but logged in user is not an approver 
            //"1" means doc is in route but logged in user is an approver
            // "2" means document is not in route and not locked also
            StringTokenizer st = new StringTokenizer((String) name.get(0), 
                                                                  Util.SepChar);
            
            String resultValue = st.nextToken();
            if(!resultValue.equalsIgnoreCase("0")&&  !resultValue.equalsIgnoreCase("1") && !resultValue.equalsIgnoreCase("2")){
            	
            	/*if (!withMe && client.getUserName().equalsIgnoreCase(resultValue) &&
                client.getIpAddress().equals(st.nextToken()))*/
            	name.add(resultValue);
            }else if((resultValue!=null)&&resultValue.equalsIgnoreCase("2")){
            	name.remove(0);
            }else{
            	name.add(resultValue);
            }
        }
        return NoError;
    }
    // Enhansment for Issue No 567
    // Nebu
    public int getSessionOwner(int sid, int did, Vector name, boolean withMe)
    {
    	VWSLog.dbgCheck("find getSessionOwner method"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(did)+Util.SepChar; 
        DoDBAction.get(getClient(sid),  database, "GetCheckOutSessionOwner",  param , name);
        return NoError;
    }
    // Nebu
    // End of 567
    private boolean byPassSecurity() 
    {
    	//return room.getBypass();//Existing code before cv10 enhancment
    	/**
    	 * Enhacment :- Bypass Security CV10 to enable bypass security based on the following registy set in if condition
    	 */
		if ((room.getBypass() == true) && (VWSPreferences.getDebug() == true)
				&& (VWSPreferences.getSecurityCheck() == true)) {
			return true;
		} else {
			return false;
		}
    }
    private String getAdminGroup() 
    {
        return room.getAdmins();
    }
    private String getSubAdminGroup() 
    {
        return room.getSubAdmins();
    }
    private String getManagerGroup() 
    {
        return room.getManagers();
    }
    private String getNamedGroup() 
    {
    	String namedGroup = "";
    	if(VWSPreferences.getRoomLevelNamed()){
    		namedGroup = room.getNamedGroup();
    		if(namedGroup.trim().length() == 0){
    			namedGroup = VWSPreferences.getNamedUsers();
    		}else if(namedGroup.equalsIgnoreCase("-")){
    			namedGroup = "";
    		}
    	}else{
    		namedGroup = VWSPreferences.getNamedUsers();
    	}
    	return namedGroup;
    }
    
    
    private String getNamedOnlineGroup() 
    {
    	String namedOnlineGroup = "";
    	if(VWSPreferences.getRoomLevelNamed()){
    		namedOnlineGroup = room.getNamedOnlineGroup();
    		if(namedOnlineGroup.trim().length() == 0){
    			namedOnlineGroup = VWSPreferences.getNamedOnline();
    		}else if(namedOnlineGroup.equalsIgnoreCase("-")){
    			namedOnlineGroup = "";
    		}
    	}else{
    		namedOnlineGroup = VWSPreferences.getNamedOnline();
    	}
    	return namedOnlineGroup;
    }
    
    
    private String getProfessionalNamedGroup() 
    {
    	String professionalNamedGroup = "";
    	if(VWSPreferences.getRoomLevelNamed()){
    		professionalNamedGroup = room.getProfessionalNamedGroup();
    		if(professionalNamedGroup.trim().length() == 0){
    			professionalNamedGroup = VWSPreferences.getProfessionalNamedUsers();
    		}else if(professionalNamedGroup.equalsIgnoreCase("-")){
    			professionalNamedGroup = "";
    		}
    	}else{
    		professionalNamedGroup = VWSPreferences.getProfessionalNamedUsers();
    	}
    	return professionalNamedGroup;
    }
    /**
     * CV10.1 Enhancement public webaccess
     * @return
     * Modified by :- Madhavan
     */
    private String getConcurrentGroup() 
    {
    	String concurrentGroup = "";
    	if(VWSPreferences.getRoomLevelNamed()){
    		//need to implement for roomlevelnamed
    		concurrentGroup = room.getEnterpriseConcurrentGroup();
    		if(concurrentGroup.trim().length() == 0){
    			concurrentGroup = VWSPreferences.getEnterpriseConcurrent();
    		}else if(concurrentGroup.equalsIgnoreCase("-")){
    			concurrentGroup = "";
    		}
    	}else{
    		concurrentGroup = VWSPreferences.getEnterpriseConcurrent();
    	}
    	return concurrentGroup;
    }
    
    private String getConcurrentOnlineGroup() 
    {
    	String concurrentOnlineGroup = "";
    	if(VWSPreferences.getRoomLevelNamed()){
    		//need to implement for roomlevelnamed
    		concurrentOnlineGroup = room.getConcurrentOnlineGroup();
    		if(concurrentOnlineGroup.trim().length() == 0){
    			concurrentOnlineGroup = VWSPreferences.getConcurrentOnline();
    		}else if(concurrentOnlineGroup.equalsIgnoreCase("-")){
    			concurrentOnlineGroup = "";
    		}
    	}else{
    		concurrentOnlineGroup = VWSPreferences.getConcurrentOnline();
    	}
    	return concurrentOnlineGroup;
    }
    
    
    
    
    private String getProfessionalConcurrentGroup() 
    {
    	String professionalConcurrentGroup = "";
    	if(VWSPreferences.getRoomLevelNamed()){
    		//need to implement for roomlevelnamed
    		professionalConcurrentGroup = room.getProfessionalConcurrentGroup();
    		if(professionalConcurrentGroup.trim().length() == 0){
    			professionalConcurrentGroup = VWSPreferences.getProfessionalConcurrent();
    		}else if(professionalConcurrentGroup.equalsIgnoreCase("-")){
    			professionalConcurrentGroup = "";
    		}
    	}else{
    		professionalConcurrentGroup = VWSPreferences.getProfessionalConcurrent();
    	}
    	return professionalConcurrentGroup;
    }
    
    private String getPublicWAGroup() 
    {
    	String publicWAGroup = "";
    	if(VWSPreferences.getRoomLevelNamed()){
    		//need to implement for roomlevelnamed
    		publicWAGroup = room.getPublicWAGroup();
    		if(publicWAGroup.trim().length() == 0){
    			publicWAGroup = VWSPreferences.getPublicWebAccess();
    		}else if(publicWAGroup.equalsIgnoreCase("-")){
    			publicWAGroup = "";
    		}
    	}else{
    		publicWAGroup = VWSPreferences.getPublicWebAccess();
    	}
    	return publicWAGroup;
    }
    
    
    
    
    
    private void setRank(Client client)
    {
    	String param = "";

    	//checking for Admin group
    	Vector retAdmin = new Vector();
    	param = Util.getOneSpace(getAdminGroup()) + Util.SepChar
    			+Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retAdmin);
    	String registrySecurityAdmin=VWSPreferences.getSecurityAdministrator();
    			
    	if (retAdmin.size() > 0)
    		if ( ((String) retAdmin.get(0)).startsWith("TRUE"))
    			client.setAdmin(true);
    	retAdmin.removeAllElements();
    	VWSLog.dbg("client.isAdmin() after CheckUserGroup procedure call :::"+client.isAdmin());
    	    	
    	//checking for Named group
    	Vector retNamed = new Vector();
    	param = Util.getOneSpace(getNamedGroup()) +
    			Util.SepChar + Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retNamed);
    	//Enhancement :-CV10 Security Administrator If condition added and existing funcitionality in else condition for Named User
    	//&&((client.getClientType()==Client.Fat_Client_Type)||(client.getClientType()==Client.NFat_Client_Type)
    	if ((retNamed.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    		client.setAdmin(true);
    		retNamed.removeAllElements();
    	}
    	else {
    		if (retNamed.size() > 0){
    			if ( ((String) retNamed.get(0)).startsWith("TRUE")){
    				client.setNamed(true);
    			}
    			retNamed.removeAllElements();
    		}
    	}
    	
    	VWSLog.dbg("client.isNamed() after CheckUserGroup procedure call :::"+client.isNamed());
    	Vector retNamedOnline = new Vector();
    	
    	param = Util.getOneSpace(getNamedOnlineGroup()) +
    			Util.SepChar + Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retNamedOnline);
    	VWSLog.dbg("retNamedOnline ::::::::"+retNamedOnline);
    	if ((retNamedOnline.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    	  	VWSLog.dbg("Inside set admin of named online ::::::::");
    		client.setAdmin(true);
    		retNamedOnline.removeAllElements();
    	}
    	else {
    		if (retNamedOnline.size() > 0){
    			if ( ((String) retNamedOnline.get(0)).startsWith("TRUE")){
    				client.setNamedOnline(true);
    			}
    			retNamedOnline.removeAllElements();
    		}
    	}
    	
    	VWSLog.dbg("client.isNamedOnline() after CheckUserGroup procedure call :::"+client.isNamedOnline());
 	/*Vector retPublicWebAccess = new Vector();
    	
    	param = Util.getOneSpace(getPublicWAGroup()) +
    			Util.SepChar + Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retPublicWebAccess);
    	VWSLog.add("retPublicWebAccess ::::::::"+retPublicWebAccess);
    	if ((retPublicWebAccess.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    	  	VWSLog.add("Inside set admin of named online ::::::::");
    		client.setAdmin(true);
    		retPublicWebAccess.removeAllElements();
    	}
    	else {
    		if (retPublicWebAccess.size() > 0){
    			if ( ((String) retPublicWebAccess.get(0)).startsWith("TRUE")){
    				client.setPublicWebAccess(true);
    			}
    			retPublicWebAccess.removeAllElements();
    		}
    	}*/
    	

    	//checking for Profressional Named group
    	Vector retProfess = new Vector();
    	param = Util.getOneSpace(getProfessionalNamedGroup()) +
    			Util.SepChar + Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retProfess);
    	//Enhancement :-CV10 Security Administrator If condition added and existing funcitionality in else condition for professional User
    	//&&((client.getClientType()==Client.Fat_Client_Type)||(client.getClientType()==Client.NFat_Client_Type) removed to allow admin login
    	if ((retProfess.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    		client.setAdmin(true);
    		retProfess.removeAllElements();
    	}else{
    		if (retProfess.size() > 0)
    		{
    			if ( ((String) retProfess.get(0)).startsWith("TRUE"))
    				client.setProfessionalNamed(true);
    		}
    		retProfess.removeAllElements();
    	}
    	VWSLog.dbg("client.isProfessionlNamed() after CheckUserGroup procedure call :::"+client.isProfessionalNamed());
      	//checking where user belong to that group or not
    	Vector retConcurrent = new Vector();
    	param = Util.getOneSpace(getConcurrentGroup()) +
    			Util.SepChar + Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retConcurrent);
    	
    	//Enhancement :-CV10.1 Security Administrator If condition added and existing funcitionality in else condition for professional User
    	//&&((client.getClientType()==Client.Fat_Client_Type)||(client.getClientType()==Client.NFat_Client_Type) removed to allow admin login
    	if ((retConcurrent.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    		client.setAdmin(true);
    		retConcurrent.removeAllElements();
    	}else{
    		if (retConcurrent.size() > 0)
    		{
    			if ( ((String) retConcurrent.get(0)).startsWith("TRUE"))
    				client.setEnterpriceConcurrent(true);
    		}
    		retConcurrent.removeAllElements();
    	}
    	VWSLog.dbg("client.isEnterpriceConcurrent() after CheckUserGroup procedure call :::"+client.isEnterpriceConcurrent());
    	
    	Vector retConcurrentOnline = new Vector();
    	param = Util.getOneSpace(getConcurrentOnlineGroup()) +
    			Util.SepChar + Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retConcurrentOnline);
    	VWSLog.dbg("retConcurrentOnline ::::"+retConcurrentOnline);
    	//Enhancement :-CV10.1 Security Administrator If condition added and existing funcitionality in else condition for professional User
    	//&&((client.getClientType()==Client.Fat_Client_Type)||(client.getClientType()==Client.NFat_Client_Type) removed to allow admin login
    	if ((retConcurrentOnline.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    		client.setAdmin(true);
    		retConcurrentOnline.removeAllElements();
    	}else{
    		if (retConcurrentOnline.size() > 0)
    		{
    			if ( ((String) retConcurrentOnline.get(0)).startsWith("TRUE"))
    				client.setConcurrentOnline(true);
    		}
    		retConcurrentOnline.removeAllElements();
    	}
    	VWSLog.dbg("client.isConcurrentOnline() after CheckUserGroup procedure call :::"+client.isConcurrentOnline());
    	//checking where user belong to that group or not
    	Vector retProfessConcurrent = new Vector();
    	param = Util.getOneSpace(getProfessionalConcurrentGroup()) +
    			Util.SepChar + Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retProfessConcurrent);
    	
    	//Enhancement :-CV10.1 Security Administrator If condition added and existing funcitionality in else condition for professional User
    	//&&((client.getClientType()==Client.Fat_Client_Type)||(client.getClientType()==Client.NFat_Client_Type) removed to allow admin login
    	if ((retProfessConcurrent.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    		client.setAdmin(true);
    		retProfessConcurrent.removeAllElements();
    	}else{
    		if (retProfessConcurrent.size() > 0)
    		{
    			if ( ((String) retProfessConcurrent.get(0)).startsWith("TRUE"))
    				client.setProfessionalConcurrent(true);
    		}
    		retProfessConcurrent.removeAllElements();
    	}
    	VWSLog.dbg("client.isProfessionalConcurrent() after CheckUserGroup procedure call :::"+client.isProfessionalConcurrent());
    	
    	//checking for Batch Input Administrator group
    	Vector retAip = new Vector();
    	param = Util.getOneSpace(getBatchInputAdminGroup()) + Util.SepChar
    			+Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retAip);
    	if ((retAip.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    		client.setAdmin(true);
    	} else { 
    		if(retAip.size() > 0) {
	    		if (((String) retAip.get(0)).startsWith("TRUE"))
	    			client.setAIP(true);
    		}
    	}
    	retAip.removeAllElements();
    	VWSLog.dbg("client.isAIP() after CheckUserGroup procedure call :::"+client.isAIP());
    	
    	/** CV10 Code added to check the user exisit in subadministrator group or not
    	 if exist then setting the subadministrator user as admin user to login adminwise**/
    	//checking for SubAdmin group
    	Vector retSubAdmin = new Vector();
    	param = Util.getOneSpace(getSubAdminGroup()) + Util.SepChar
    			+Util.getOneSpace(client.getUserName());
    	DoDBAction.get(null, database, "CheckUserGroup", param , retSubAdmin);
    	if ((retSubAdmin.size() > 0)&&(client.getUserName().equalsIgnoreCase(registrySecurityAdmin))&&(registrySecurityAdmin.length()>0)){
    		client.setAdmin(true);
    	} else { 
    		if(retSubAdmin.size() > 0) {
	    		if (((String) retSubAdmin.get(0)).startsWith("TRUE"))
	    			client.setSubAdmin(true);
    		}
    	}
    	retSubAdmin.removeAllElements();
    	VWSLog.dbg("client.isSubAdmin() after CheckUserGroup procedure call :::"+client.isSubAdmin());
    	
    	/*Vector subAdminGroup=new Vector();
    	String subAdminGroupName="";
    	param = "";
    	DoDBAction.get(null, database, "CVGetSubAdminRoles", param , subAdminGroup);
    	if((subAdminGroup.size()>0)&&(!(subAdminGroup.isEmpty()))){
    		for(int i=0;i<subAdminGroup.size();i++){
    			StringTokenizer st = new StringTokenizer ( (String) subAdminGroup.elementAt(i), "\t");
    			subAdminGroupName=st.nextToken();
    			retSubAdmin = new Vector();
    			param = Util.getOneSpace(subAdminGroupName) +
    					Util.SepChar + Util.getOneSpace(client.getUserName());
    			DoDBAction.get(null, database, "CheckUserGroup", param , retSubAdmin);
    			if ((retSubAdmin.size() > 0)&&(client.getClientType()==Client.Adm_Client_Type)){
    				if ( ((String) retSubAdmin.get(0)).startsWith("TRUE")){
    					client.setAdmin(true);
    				}
    				retSubAdmin.removeAllElements();
    			}
    		}
    	}*/
    }
    public boolean isAdmin(int sid) 
    {
    	/**
    	 * IF condition newly added
    	* Enhancement:-Register Security officer (USER name) for server at Connection & security.
        * If specified, Administaors will not have the default behaiviour of Assign permisison,
        * and  only this user can assign security to Administrator.
    	 */
    	
    	if((getClient(sid).getClientType()==Client.Fat_Client_Type)||(getClient(sid).getClientType()==Client.NFat_Client_Type)&&(enableSecurityAdmin(sid)==true)){
    		if(isSecurityAdmin(sid)==true){
    			return true;
    		}
    		else{
    			if (!assertSession(sid)) return false;
    			return getClient(sid).isAdmin();
    		}
    	}// else condition is to retain the existing functionality
    	else{
    		if (!assertSession(sid)) return false;
    		return getClient(sid).isAdmin();
    	}
    }
    /**
     *  Enhancement:-Register Security officer (USER name) for server at Connection & security.
     * If specified, Administaors will not have the default behaiviour of Assign permisison,
     * and  only this user can assign security to Administrator.
     * @param sid
     * @return
     */
    public boolean isSecurityAdmin(int sid)
    {
    	String securityAdmin=getClient(sid).getUserName();
    	String registrySecurityAdmin=VWSPreferences.getSecurityAdministrator();
    	if(securityAdmin.equalsIgnoreCase(registrySecurityAdmin)){
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    /**
     * Enhancement:-Register Security officer (USER name) for server at Connection & security.
     * If specified, Administaors will not have the default behaiviour of Assign permisison,
     * and  only this user can assign security to Administrator.
     * @param sid
     * @return
     */
    public boolean enableSecurityAdmin(int sid)
    {
    	String registrySecurityAdmin=VWSPreferences.getSecurityAdministrator();
    	if(registrySecurityAdmin.length()>0)
    		return true;
    	else 
    		return false;
    }
    public boolean isManager(int sid) 
    {
        if (!assertSession(sid)) return false;
        return getClient(sid).isManager();
    }
    public int getRoomStatisticsOld(int sid, Vector info)
    {
        if (!assertSession(sid)) return invalidSessionId;
        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getServerSeats()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}
       
        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getDesktopSeatsConcurrent()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}
       
        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getDesktopSeatsNamed()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}
        
        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getWebtopSeatsConcurrent()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}

        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getWebtopSeatsNamed()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}

        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getWebliteSeatsConcurrent()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}

        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getWebliteSeatsNamed()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}

        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getAdminWiseSeats()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}

        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getAipSeats()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}
        
        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getEasSeats()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}

        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getIndexerSeats()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}

        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getArsSeats()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}
        
/*        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getEworkSeats()));
        }
        catch(LicenseException le){info.add(Integer.toString(0));}*/
        
         try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getSdkSeats()));
        }
         catch(LicenseException le){info.add(Integer.toString(0));}
         try
         {
             info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                 getLicense().getDrsSeats()));
         }
        catch(LicenseException le){info.add(Integer.toString(0));}
         try
         {
             info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                 getLicense().getWebAccessSeats()));
         }
        catch(LicenseException le){info.add(Integer.toString(0));}
        try
        {
            info.add(Integer.toString(LicenseManagerImpl.getInstance().
                                                getLicense().getNotificationSeats()));
        }
       catch(LicenseException le){info.add(Integer.toString(0));} 
        getCounts(sid, info);
        return NoError;
    }
	//Modified for cv83b3 WorkFlow License UI  count changes 
    private int getMaxEnterpriceNamed(int desktopNamed,int webTopNamed,int webLiteNamed,int webaccessSeats){
		if(desktopNamed>webTopNamed&&desktopNamed>webLiteNamed&&desktopNamed>webaccessSeats)
			return desktopNamed;
		else if(webTopNamed>webLiteNamed&&webTopNamed>webaccessSeats)
			return webTopNamed;
		else if(webLiteNamed>webaccessSeats)
			return webLiteNamed;
		else
			return webaccessSeats;
	}
    public int getRoomStatistics(int sid, Vector info){
    	if (!assertSession(sid)) 
    		return invalidSessionId;
    	try{
    		info.add(Integer.toString(LicenseManagerImpl.getInstance().
    				getLicense().getServerSeats()));
    	}
    	catch(LicenseException le){
    		info.add(Integer.toString(0));
    	}
    	try{
    		boolean isDRSSeatsAvailable = false;
    		int sharedServerSeats = 0;
    		int totalDRSSeats=0;
    		com.computhink.vws.license.License lic = LicenseManagerImpl.getInstance().getLicense();
    		if(lic.getDrsSeats() > 0){
    			isDRSSeatsAvailable = true;
    			//int professionalSeats = lic.getServerSeats() - lic.getDesktopSeatsNamed();
    			/**int professionalSeats = lic.getProfessionalWebAccessSeats();
    			sharedServerSeats = lic.getServerSeats() - professionalSeats;**/
    			//Modified for cv83b3 WorkFlow License UI  count changes 
    			int maxEnterpriseNamed=getMaxEnterpriceNamed(lic.getDesktopSeatsNamed(),lic.getWebtopSeatsNamed(),lic.getWebliteSeatsNamed(),lic.getWebAccessSeats());
    			totalDRSSeats=maxEnterpriseNamed+lic.getProfessionalWebAccessSeats()+lic.getDesktopSeatsConcurrent()+lic.getCustomLabelWebAccessSeats();
    		}
    		info.add(Integer.toString(isDRSSeatsAvailable ? totalDRSSeats : 0));
    	}
    	catch(LicenseException le){
    		info.add(Integer.toString(0));
    	}
    	try{
    		info.add(Integer.toString(LicenseManagerImpl.getInstance().
    				getLicense().getAipSeats()));
    	}
    	catch(LicenseException le){
    		info.add(Integer.toString(0));
    	}
    	try{
    		info.add(Integer.toString(LicenseManagerImpl.getInstance().
    				getLicense().getCustomLabelWebAccessSeats()));
    	}
    	catch(LicenseException le){
    		info.add(Integer.toString(0));
    	}
    	try{
    		/*int serverSeats = LicenseManagerImpl.getInstance().getLicense().getServerSeats();
    		int desktopNamedSeats = LicenseManagerImpl.getInstance().getLicense().getDesktopSeatsNamed();    		
    		int professionalSeats = serverSeats - desktopNamedSeats;*/
    		
    		int professionalSeats = LicenseManagerImpl.getInstance().getLicense().getProfessionalWebAccessSeats();
    		info.add(Integer.toString(professionalSeats));
    	}
    	catch(LicenseException le){
    		info.add(Integer.toString(0));
    	}
    	getCounts(sid, info);
    	return NoError;
    }
    private void getCounts(int sid, Vector counts)
    {
        int size = counts.size();
        DoDBAction.get(getClient(sid),  database,"ViewWise_GetCounts", "", counts);
        if(counts.size() > size) 
        {
            StringTokenizer st = new StringTokenizer((String) counts.get(size), 
                                                                  Util.SepChar);
            counts.remove(size);
            while (st.hasMoreTokens()) counts.add(st.nextToken());
        }
    }
    public int isTextSearchEnabled(int sid)
    {
    	VWSLog.dbgCheck("isTextSearchEnabled method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        DoDBAction.get(getClient(sid),  database,"System_Get", "ENABLESIC", ret);
        if(ret!=null && ret.size() > 0) 
        {
            return (((String) ret.get(0)).equals("1")? True : False);
        }          
        return False;
    }
    public int setRedactions(int sid, int did , String redactions)
    {
    	VWSLog.dbgCheck("setRedactions method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        String param = String.valueOf(did) + Util.SepChar + redactions;
        DoDBAction.get(getClient(sid),  database, "DOC_SetRedactions", param, ret);
        return NoError;
    }
    public int getRedactions(int sid, int did, String ver, Vector redactions)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("getRedactions method sid val--->" + sid);
        String param = String.valueOf(did) + Util.SepChar + ver;
        DoDBAction.get(getClient(sid),  database, "DOC_GetRedactions", param , redactions);
        return NoError;
    }
    //-------------------------Audit Trail Methods------------------------------
    public int AT_setData(int sid, String param, Vector ret)
    {
    	VWSLog.dbgCheck("AT_setData method sid val--->" + sid);
         if (sid!=1 && !assertSession(sid)) return invalidSessionId;
         DoDBAction.get(getClient(sid),  database, "AT_SetData", param , ret);
         return NoError;
    }
    public int AT_getSettings(int sid, String param, Vector ret)
    {
    	VWSLog.dbgCheck("AT_getSettings method sid val--->" + sid);
         if (!assertSession(sid)) return invalidSessionId;
         DoDBAction.get(getClient(sid),  database, "AT_GetSettings", param , ret);
         return NoError;
    }
    /*
    public int AT_docOpened(int sid, String param, Vector ret)
    {
         if (!assertSession(sid)) return invalidSessionId;
         
         Client client = getClient(sid);
         param = param + Util.SepChar + client.getUserName() + Util.SepChar +
                                        client.getIpAddress()+ Util.SepChar;
         DoDBAction.get(getClient(sid),  database, "AT_OpenDoc", param , ret);
         return NoError;
    }
    public int AT_docPagesUpdated(int sid, String param, Vector ret)
    {
         if (!assertSession(sid)) return invalidSessionId;
         
         Client client = getClient(sid);
         param = param + Util.SepChar + client.getUserName() + Util.SepChar +
                                        client.getIpAddress()+ Util.SepChar;
         DoDBAction.get(getClient(sid),  database, "AT_UpdateDocPage", param , ret);
         return NoError;
    }
    public int AT_delDoc(int sid, String param, Vector ret)
    {
         if (!assertSession(sid)) return invalidSessionId;
         DoDBAction.get(getClient(sid),  database, "AT_DeleteDoc", param , ret);
         return NoError;
    }*/
    //---------------------------------AdminWise--------------------------------
    //  18-1-2004
    //
    public int addATRecord(int sid, int nid, int otype, int eid, String desc)  
    {
    	VWSLog.dbgCheck("addATRecord method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        if (client == null) return Error;
        return addATRecord(sid, client.getUserName(),client.getIpAddress(), 
            nid, otype, eid, desc);
    }
// Check the database is available or not.
    public boolean pingDB(){
    	try{  
    	Connection con = database.aquireConnection();
    	if (con == null) return false;
    	con.commit();
    	database.releaseConnection(con);
    	}catch(SQLException ex){ 
    	return false;}    	
    	return true;
    }
    public int addATRecord(int sid, String user, String ip, int nid, int otype, int eid, 
                                                                    String desc)  
    {
    	VWSLog.dbgCheck("addATRecord method sid val--->" + sid);
    	if (!pingDB()) {return NoError;}
        String param = String.valueOf(nid) + Util.SepChar + 
                       String.valueOf(eid) + Util.SepChar + 
                       String.valueOf(otype/100) + Util.SepChar + 
                       user + Util.SepChar + 
                       ip + Util.SepChar + desc;
        DoDBAction.get(getClient(sid),  database, "AT_SetData", param, new Vector());
        return NoError;
    }
    public int registerOutput(int sid, String output)
    {
        if (!assertSession(sid)) return invalidSessionId;
        StringTokenizer st = new StringTokenizer(output, ":");
        while (st.hasMoreTokens())
        {
            int atEvent = getOutputATEvent(st.nextToken().trim());
            int did = 0;
            if(st.hasMoreTokens()) did = Util.to_Number(st.nextToken());
            if(atEvent == 0 || did == 0) continue;
            String comment="";
            try
            {
                    comment=st.nextToken();
            }
            catch(Exception e){}
            addATRecord(sid, did, VWATEvents.AT_OBJECT_DOC, atEvent, comment);      
        }
        return NoError;
    } 
    private int getOutputATEvent(String outEvent)
    {
        int event = 0;
        if(outEvent.equalsIgnoreCase("EX"))
            event = VWATEvents.AT_DOC_EXPORT;
        else if(outEvent.equalsIgnoreCase("EM"))
            event = VWATEvents.AT_DOC_EMAIL;
        else if(outEvent.equalsIgnoreCase("PR"))
            event = VWATEvents.AT_DOC_PRINT;
        else if(outEvent.equalsIgnoreCase("FX"))
            event = VWATEvents.AT_DOC_FAX;
        return event;
    }    
    public int adminWise(int sid, String action, String param, Vector ret)
    {
    	// Issue No 565 : To fix the named user can checkin the document. Checking the User is not required.
    		//if (!assertAdminSession(sid)) return invalidSessionId;
    	// End Issue No 565
    		VWSLog.dbgCheck("adminWise method sid val--->" + sid);
         DoDBAction.get(getClient(sid),  database, action, param , ret);
         return NoError;
    }
    public int setAllowConnection(int sid, boolean allow)
    {
    	VWSLog.dbgCheck("setAllowConnection method sid val--->" + sid);
    	if (!assertAdminSession(sid)){
    		VWSLog.dbgCheck("Invalid session inside setAllowConnections from assertAdminSession"+sid);
    		if (!assertSubAdminSession(sid)) {
        		return invalidSessionId;
        	}
    	}
        allowConnections = allow;
        return NoError;
    }
    public int isConnectionEnabled(int sid)
    {
    	VWSLog.dbgCheck("isConnectionEnabled method sid val--->" + sid);
        if (!assertAdminSession(sid)) {
        	if (!assertSubAdminSession(sid)) {
        		return invalidSessionId;
        	}
        }
        return isConnectionEnabled();
    }
    private int isConnectionEnabled()
    {
        return (allowConnections? True : False);
    }
    private void setEncryptionKey() 
    {
        getDBEncryptionKey();
        //VWSLog.dbg("After calling the db getDBEncryptionKey");
        if (roomEncryptionKey.length() > 0){
        	VWSLog.dbg("roomEncryptionKey.length() is greate than zero");
        	return;
        }
        
        String registryKey = room.getKey();
        if (registryKey.equals("") ||
                   Util.decryptKey(registryKey).equals("KeyAlreadySet")) return;
        
        // key in registry has not been set
        // if we have any docs in room, cannot accept new key
        if (roomHasDocuments())
        {
        	message = room.getName() + 
            resourceManager.getString("RampServMsg.DocEncryptKeyReject");
        	VWSLog.war(message);
        	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
            VWSPreferences.setEncryptionKey(room.getName(), 
                                              Util.encryptKey("KeyAlreadySet"));
            return;
        }
         // No nodes..clean room...then we accept registry key
        roomEncryptionKey = registryKey;
        if(!roomEncryptionKey.equals(""))
        {
            String param = "";
            	param = "VWKey" + Util.SepChar +
            	Util.getOneSpace(roomEncryptionKey);
        	//VWSLog.dbg("Room Encryption key before setting to db::::"+param);
            DoDBAction.get(null, database, "System_Set", param, new Vector());
            roomEncryptionKey=Util.decryptKey(registryKey);//Added in CV10 for room encryption enhancment
        }
        VWSPreferences.setEncryptionKey(room.getName(), 
                                              Util.encryptKey("KeyAlreadySet"));
    }
    private void getDBEncryptionKey() 
    {
        Vector ret = new Vector();
        DoDBAction.get(null, database, "System_Get", "VWKey", ret);
        // CV10 Enhancement Util.decryptKey() added for roomlevel encryption to decrypt the key from database
        if(ret.size() > 0)  {
        	if(room.getISEncryptedKey()==true){
                	 roomEncryptionKey = Util.decryptKey((String) ret.get(0)); //modified by madhavan 4/4/2016
                	 //VWSLog.dbg("Decrypted key set from database"+roomEncryptionKey);
        	}
        	else{
        		 roomEncryptionKey = (String) ret.get(0);
        		// VWSLog.dbg("inside else getDBEncryptionKey:::"+roomEncryptionKey);
        	}
        		
        	}
        else 
            roomEncryptionKey = "";
    }
    public String getEncryptionKey(String GUID)
    {
        if (GUID.equals("4223DAAB-D393-11D0-9A76-00C05FB68BF7")){
        	//VWSLog.dbg("Key inside getEncryptionKey "+roomEncryptionKey);
            return roomEncryptionKey;
        }
        else
            return "";
    }
	/**CV2019 code merges from SIDBI**/
    public int getDocumentInfo(int sid, int docID, Vector documents)
    {
    	VWSLog.dbgCheck("getDocumentInfo method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(docID);
        DoDBAction.get(getClient(sid),  database, "Doc_GetInfo", param, documents);
        return NoError;
    }
    public int getDocumentInfo(int sid, Document doc, Vector documents)
    {
    	VWSLog.dbgCheck("getDocumentInfo method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(doc.getId());
        DoDBAction.get(getClient(sid),  database, "Doc_GetInfo", param, documents);
        return NoError;
    }
    public int getNodeAllDocuments(int sid, int nid, int sessionId, Vector documents) 
    {
    	VWSLog.dbgCheck("getNodeAllDocuments method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        String param =  String.valueOf(nid) + Util.SepChar + 
                        String.valueOf(sessionId) + Util.SepChar +
                        client.getIpAddress() + Util.SepChar + 
        			// For admin user also security permissions needs to set.                        
                        //(client.isAdmin()?"":client.getUserName());
                        client.getUserName();
        DoDBAction.get(getClient(sid),  database, "Node_GetAllDocs", param, documents);
        return NoError;
    }
    public int releaseSession(int sid, Node node)
    {
    	VWSLog.dbgCheck("find releaseSession method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        String param = String.valueOf(node.getId()) + Util.SepChar +
                       String.valueOf(node.getSessionId())+ Util.SepChar + 
                       client.getIpAddress() + Util.SepChar +
                       client.getUserName();
        DoDBAction.get(getClient(sid),  database, "Node_ReleaseSession", param, new Vector());
        return NoError;
    }
    public int createSession(int sid, Node node, String sessionName)
    {
    	VWSLog.dbgCheck("find createSession method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        String param =  String.valueOf(node.getId()) + Util.SepChar +
                        sessionName + Util.SepChar +
                        client.getUserName()+ Util.SepChar +
                        client.getIpAddress() + Util.SepChar +
        			// For admin user also security permissions needs to set.                        
                        (client.isAdmin()?"0":"0");
        Vector ret=new Vector();
        DoDBAction.get(getClient(sid),  database, "Node_CreateSession", param, ret);
        
        if (ret.size() > 0)
            return Util.to_Number((String) ret.get(0));
        return 0;
    }
   //------------------------------End AdminWise--------------------------------
    protected class IdleTimerAction implements java.awt.event.ActionListener 
    {
        public void actionPerformed(java.awt.event.ActionEvent e) 
        {
        	VWSLog.dbg("Inside idletimer action performed");
            /*int idleTime = room.getRoomProperty().getIdle();
           
            if (idleTime == 0){
            	idleTime = DEFAULT_IDLE_TIME;
            }
            int lockedDocSize = 0;
             * */           
            Enumeration enumc = clients.elements();
            long now = System.currentTimeMillis();
            /*VWSLog.dbg("Current time :"+now);
            VWSLog.dbg("Inside IdleTimerAction Performed");
            VWSLog.dbg("enumc.hasMoreElements()::::::"+enumc.hasMoreElements());*/
            while (enumc.hasMoreElements())
            {
            	//VWSLog.add("Idle Time Out for  " + room + " is "+ idleTime);
                Client client = (Client) enumc.nextElement();
                int type = client.getClientType();
                //.dbg("Client type :"+type);
                /*if (type ==1 || type == 2 || type == 4 ) {
                	lockedDocSize = getLockDocList(client.getId());
                	if(lockedDocSize > 0) idleTime = DTCWC_IDLE_TIME;
                	 VWSLog.add("Idle Time Out for  " + room + " is "+ idleTime);
                }*/
                /* Issue # 754: For EAS and AIP idle time out is removed
                 *  C.Shanmugavalli 10 Nov 2006 */ 
                //if (type <= 4 || type == 9 || type == 10 || type == 12)  
                
                //if (type < 4 || type == 9 || type == 10 || type == 12)       // DT, Web, AW and Added the Named User Check
            	int sid = client.getId();
            	if(type==Client.AIP_Client_Type){
            		try{
            			/*VWSLog.dbg("Clienttype in idle timeout"+sid);
            			VWSLog.dbg("After checking Clienttype is AIP or not");*/
            			if(aipActiveClient!=null &&aipActiveClient.size()>0){       
            				long timeactive=Long.valueOf((String) aipActiveClient.get(sid));
            				/*VWSLog.dbg("timeactive in long value is "+timeactive);
            				VWSLog.dbg("current time :"+now);*/
            				VWSLog.dbg("3 * 1000L * 60L"+3 * 1000L * 60L);
            				if((now - timeactive) >= (3 * 1000L * 60L)){
            					dispatchClientEvent(sid, VWEvent.IDLE_DISCONNECT);
            					addATRecord(sid, sid, VWATEvents.AT_OBJECT_LOGIN, 
            							VWATEvents.AT_LOGIN, "Idle Timeout logout");
            					//VWSLog.dbg("Before calling endsession from IdleTimerAction for aip");
            					endSession(sid, IdleTimeout);
            					//VWSLog.dbg("Before removing aipActiveClient:::: "+aipActiveClient);
            					aipActiveClient.remove(sid);
            					//VWSLog.dbg("After removing aipActiveClient:::: "+aipActiveClient);
            				}
            			}
            		}catch(Exception e1){
            			VWSLog.dbg("Exception while getting time from aipclient :::::"+e1.getMessage());
            		}
            	}
                
                if (type == Client.Fat_Client_Type || 
                		type == Client.NFat_Client_Type ||
                		type == Client.Web_Client_Type ||
                		type == Client.NWeb_Client_Type ||
                		type == Client.WebL_Client_Type ||
                		type == Client.NWebL_Client_Type ||
                		type == Client.Adm_Client_Type ||
                		type == Client.sdk_Client_Type ||
                		(type==Client.WebAccess_Client_Type) || 
                		(type==Client.Custom_Label_WebAccess_Client_Type) || 
                		(type == Client.Professional_WebAccess_Client_Type)||
                		(type == Client.Professional_Conc_WebAccess_Client_Type)||
                		(type == Client.Concurrent_WebAccess_Client_Type)||
                		(type==Client.NamedOnline_WebAccess_Client_Type)||
                		(type==Client.CSWebaccess_client_Type)){
                	//if ((now - client.getLastActivateDate()) >= 
                	//                                   (idleTime * 1000L * 60L))
                	int connectionStatus = isRoomIdle(sid, false);
                	//VWSLog.dbg("connectionStatus :"+connectionStatus);
                	if (connectionStatus == IdleTimeout)
                	{

                		dispatchClientEvent(sid, VWEvent.IDLE_DISCONNECT);
                		addATRecord(sid, sid, VWATEvents.AT_OBJECT_LOGIN, 
                				VWATEvents.AT_LOGIN, "Idle Timeout logout");
                		VWSLog.dbgCheck("Before calling endsession from IdleTimerAction");
                		endSession(sid, connectionStatus);
                	}                
                }
            }
        }
    }
/* Issue No 736: added methods for MDSS sessionId issue in Nevell Environment
* Instead of using idle timeout with MDSS, It is done based on lastActiveTime 
* C.Shanmugavalli, Pandiyaraj 3 Nov 2006
*/
    public int isRoomIdle(int sid){
    	return isRoomIdle(sid, true);
    }
    public int isRoomIdle(int sid, boolean flag)
    {
    	boolean isIdle = false;
		//Database connection reset for room is idle for 'scheduleTime' minutes
        try {
			long now = System.currentTimeMillis();
			//VWSLog.dbg("ConnectionResetScheduler :"+room.getRoomProperty().getConnectionResetScheduler());
			if (room.getRoomProperty().getConnectionResetScheduler() == 0) {
				scheduleDBResetTimer = new java.util.Timer();
				String dailyFrequency = room.getRoomProperty()
						.getSchedulerTime();
				int scheduleTime = Integer.parseInt(dailyFrequency);
				/*VWSLog.dbg("Current time iniside isRoomIdle:::::"+((now/1000)%3600));
				VWSLog.dbg("scheduleTime::::::::"+scheduleTime);
				VWSLog.dbg("scheduleTime * 1000L * 60::::::"+scheduleTime * 1000L * 60);
				VWSLog.dbg("database.getLastActivateDate()::::::"+((database.getLastActivateDate()/1000)%3600));*/
				if ((now - database.getLastActivateDate()) >= ((scheduleTime * 1000L * 60L))) {
					//VWSLog.dbg(database.getName() + ": Database connection reset due to room idle for " + scheduleTime  + " minute(s).");
					Connection con = null;
					try {
						con = database.aquireConnection();
						con.close();
					} catch (Exception ex) {
					}
					database.initialize();
					database.setLastActivateDate();
				}
			}
		} catch (Exception ex) {
		}

		int idleTime = room.getRoomProperty().getIdle();
		//VWSLog.dbg("room idleTime::::::::"+idleTime);
		/* Issue: System shoud take DTCWC_IDLE_TIME/DEFAULT_IDLE_TIME only when idle time out is '0'
		 * C.Shanmugavalli 7 Jan 2007*/
        /*if (idleTime == 0){
        	idleTime = DEFAULT_IDLE_TIME;
        }*/ 		
		Client client = (Client) clients.get(new Integer(sid));
		if (flag)
			client.setLastRequestFromClient();		
		long now = System.currentTimeMillis();
		int type = client.getClientType();
	    int lockedDocStatus = 0;
	    //License Enhancement - split the web client license seat into 4. webtop concurrent/ webtop named/ weblite concurrent/ weblite named
	    //if ((type ==1 || type == 2 || type == 9 || type == 10)  && idleTime == 0) {
	    if ((type == Client.Fat_Client_Type ||
	    		type == Client.NFat_Client_Type ||
	    		type == Client.Web_Client_Type ||
	    		type == Client.NWeb_Client_Type ||
	    		type == Client.WebL_Client_Type ||
	    		type == Client.NWebL_Client_Type)||
	    		(type==Client.WebAccess_Client_Type) || 
        		(type==Client.Custom_Label_WebAccess_Client_Type) || 
        		(type == Client.Professional_WebAccess_Client_Type)|| 
        		(type == Client.Concurrent_WebAccess_Client_Type)||
        		(type==Client.Professional_Conc_WebAccess_Client_Type)
	    		) 
	    {

	    	/*
	    	 Room Idle timeout for Launched and View Documents

				Idle Timeout = 0	if user View the document 	45 mins
				Idle Timeout = 0	if user Launched the document 	infinite - idle time out will not happen
				Idle Timeout > 0	if user View the document 		x mins
				Idle Timeout > 0	if user Launched the document 	infinite - idle time out will not happen

			getOpenedDocList  
				return 0 - no documents exist in the documentlock table			
				return 1 - View documents exist in the documentlock table		
				return 2 - Launched documents exist in the documentlock table		
	    	 */
	    	//VWSLog.dbg("getOpenedDocList parameter::::::::"+lockedDocStatus);
	    	lockedDocStatus = getOpenedDocList(client.getId());
	    	VWSLog.dbg("lockedDocStatus::::::::"+lockedDocStatus);
	    	if (lockedDocStatus == 2){
	    		return connectionaAvailable;
	    	}else if(lockedDocStatus == 1){
	    		if (idleTime == 0) idleTime = DTCWC_IDLE_TIME;	    		
	    	}
        	/*if(lockedDocStatus > 0){
        		idleTime = DTCWC_IDLE_TIME;
        	}*/

        }
	    if (idleTime == 0){
        	idleTime = DEFAULT_IDLE_TIME;
        } 
	    /*VWSLog.dbg("IdleTime >>>>>>> "+idleTime);
	    VWSLog.dbg("now >>>>>>> "+now);
	    VWSLog.dbg("client.getLastActivateDate() >>>>>>> "+client.getLastActivateDate());
	    VWSLog.dbg("idleTime * 1000L * 60L >>>>>>> "+idleTime * 1000L * 60L);
	    VWSLog.dbg("now - client.getLastActivateDate()"+(now - client.getLastActivateDate()));
	    VWSLog.dbg("isIdle : "+((now - client.getLastActivateDate()) >= 
		    	(idleTime * 1000L * 60L)));*/
        if ((now - client.getLastActivateDate()) >= 
	    	(idleTime * 1000L * 60L)){
	    	client.setConnectedStatus(IdleTimeout);
		    isIdle = true;
		}
	    int status = connectionaAvailable;	    
	    if (!database.isDbConnected()) status = DatabaseError;
	    if (isIdle) status = IdleTimeout;
	    //VWSLog.dbg("status : "+status);
	    return status;
    }    
    public int getOpenedDocList(int sid) 
    {
    	Vector documents = new Vector();
        if( !clients.containsKey(new Integer(sid)) ) return NoError;
        Client client = getClient(sid);
        String param = client.getIpAddress()+ Util.SepChar +
        client.getUserName();

        DoDBAction.get(getClient(sid),  database, "VWOpenedDocs_GetAll", param, documents);
        int lockedDocStatus = 0;
        if (documents.size() > 0)
        	lockedDocStatus = Integer.parseInt(documents.get(0).toString().trim());
        return lockedDocStatus;
    }

    public int getLockDocList(int sid, Vector documents) 
    {
    	VWSLog.dbgCheck("getLockDocList method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        DoDBAction.get(getClient(sid),  database, "DocumentLock_GetAll", "", documents);
        return NoError;
    }
    
    
    public int getRecycleDocList(int sid, int rowCount, int lastRowId, int actionType, Vector documents)
    {
	return getRecycleDocList(sid, rowCount, lastRowId, actionType, "", documents);
    }
    public int getRecycleDocList(int sid, int rowCount, int lastRowId, int actionType, String indexValue, Vector documents)
    {
	// The ActionType is 0, 1, 2 or 3; 
	// 0 - First
	// 1 - Next 
	// 2 - Previous
	// 3 - Last
	
        if (!assertSession(sid)) return invalidSessionId; 
        String param = rowCount + Util.SepChar +lastRowId + Util.SepChar +actionType + Util.SepChar + indexValue;
        DoDBAction.get(getClient(sid),  database, "Node_GetRecycleDocuments", param, documents);
        return NoError;
    }
    public int getRecycleDocCount(int sid, String indexValue)
    {
    	VWSLog.dbgCheck("getRecycleDocCount method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId; 
        String param = indexValue + Util.SepChar;
        Vector docCount = new Vector();
        DoDBAction.get(getClient(sid),  database, "VWNode_GetRecycleDocCount", param, docCount);
        if (docCount != null && docCount.size() > 0){
            int count = Util.to_Number(docCount.get(0).toString().trim());
            return count;
        }
        return NoError;
    }
    
    public int setNodeStorage(int sid, Node node, int withInherit)
    {
    	VWSLog.dbgCheck("setNodeStorage method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param =  String.valueOf(node.getId()) + Util.SepChar +
                        String.valueOf(node.getStorageId())  + Util.SepChar +
                        String.valueOf(withInherit);
        DoDBAction.get(getClient(sid),  database, "UpdateNodesStorage", param, new Vector());
        return NoError;
    }
    
    public int isDocTypeFound(int sid, DocType docType)
    {
    	VWSLog.dbgCheck("isDocTypeFound method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result=new Vector();
        DoDBAction.get(getClient(sid),  database, "CheckDocType_Isfound", 
                                                     docType.getName(), result);
        if(result==null || result.size()==0) return Error;
        return Util.to_Number((String)result.get(0));
    }
    public int isIndexFound(int sid, Index index, int checkType, DocType dType)
    {
    	VWSLog.dbgCheck("isIndexFound method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result=new Vector();
        String param = ((dType == null) ?"" : dType.getName()) + Util.SepChar +
        	index.getName() + Util.SepChar +
            String.valueOf(index.getType()) + Util.SepChar +
            index.getMask()+ Util.SepChar +
            String.valueOf(index.getSystem()) + Util.SepChar +
            index.getInfo()  + Util.SepChar +
            String.valueOf(checkType)+ Util.SepChar;
        DoDBAction.get(getClient(sid),  database, "CheckIndex_Isfound", param, result);
        if(result==null || result.size()==0) return Error;
        return Util.to_Number((String)result.get(0));
    }
    public int deleteDocType(int sid, DocType docType)
    {
    	VWSLog.dbgCheck("deleteDocType method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result=new Vector();
        DoDBAction.get(getClient(sid),  database, "DeleteDocType", 
                                       String.valueOf(docType.getId()), result);
        if(result==null || result.size()==0) return Error;
        return Util.to_Number((String)result.get(0));
    }
    public int deleteIndex(int sid, Index index)
    {
    	VWSLog.dbgCheck("deleteIndex method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result=new Vector();
        DoDBAction.get(getClient(sid),  database, "DeleteIndex", 
                                         String.valueOf(index.getId()), result);
        if(result==null || result.size()==0) return Error;
        return Util.to_Number((String)result.get(0));
    }
    public int deleteDTIndex(int sid, Index index)
    {
    	VWSLog.dbgCheck("deleteDTIndex method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result=new Vector();
        DoDBAction.get(getClient(sid),  database, "DeleteDocTypeIndex", 
                                         String.valueOf(index.getId()), result);
        if(result==null || result.size()==0) return Error;
        return Util.to_Number((String)result.get(0));
    }
    public int setDocTypeAutoMail(int sid, DocType docType)
    {
    	VWSLog.dbgCheck("setDocTypeAutoMail method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result=new Vector();
        String param = Integer.toString(docType.getId()) + Util.SepChar + 
        docType.getTo() + Util.SepChar +
        docType.getCc() + Util.SepChar +
        docType.getSubject();
        
        DoDBAction.get(getClient(sid),  database, "DocType_Set_AutoMail", param, result);
        return NoError;
    }
    public int restoreNodeFromRecycle(int sid, Node node) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("restoreNodeFromRecycle method sid val--->" + sid);
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = String.valueOf(node.getId()) + Util.SepChar +
                                      node.getParentId();
        DoDBAction.get(getClient(sid),  database, "Node_Restore_FromRecycle", param, ret);
        return NoError;
    }
    public int getUserRedactions(int sid, String userName, Vector documents) 
    {
    	VWSLog.dbgCheck("getUserRedactions method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        DoDBAction.get(getClient(sid),  database, "DOC_GetRedactionDocs", userName, documents);
        return NoError;
    }
    public int getAllRedactions(int sid, Document doc, Vector redactions) 
    {
    	VWSLog.dbgCheck("getAllRedactions method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        DoDBAction.get(getClient(sid),  database, "Doc_GetAllRedactions", 
            String.valueOf(doc.getId()), redactions);
        return NoError;
    }
    public int setUserRedactions(int sid, String user, String oldPas, 
                                                     String newPas, Vector docs)
    {
    	VWSLog.dbgCheck("setUserRedactions method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        int count = docs.size();
        Client client = getClient(sid);
        if (user.equalsIgnoreCase("<All Users>")) user = ""; 
        for(int i=0;i<count;i++)
        {  
        	String param = "";
        		param = Util.getOneSpace(client.getUserName())+ Util.SepChar +  
    					Util.getOneSpace(client.getIpAddress())  + Util.SepChar +
    					Util.getOneSpace(user) + Util.SepChar +
    					Util.getOneSpace(oldPas) + Util.SepChar +
    					Util.getOneSpace(newPas) + Util.SepChar +
    					Util.getOneSpace((String) docs.get(i));
            DoDBAction.get(getClient(sid),  database, "DOC_SetRedactionsPassword", param, 
                                                                  new Vector());
            
            
            String docIds = docs.get(i).toString();
            Vector result = null;
            if(docIds!=null && docIds.length()>0 && docIds.contains(",")){
            	String docArray[] = docIds.split(",");
            	for(int index=0; index<docArray.length; index++){
            		result = new Vector();
            		int docId = Integer.parseInt(docArray[index]);
                	int ret = getNodeName(sid, docId, result);
                	String docName = result.get(0).toString();
                	try{
                		message = "Replaced Redaction Passwords for document '"+docName+"'";
                		//Node id for redaction is 0
                		addNotificationHistory(sid, 0, VNSConstants.nodeType_Redaction, VNSConstants.notify_Redaction_RepPassword, user, "-", "-", message);
                	}catch(Exception ex){}
            	}
            }else{
            	//docIds having only one docId
            	try{
            		result = new Vector();
            		int docId = Integer.parseInt(docIds);
            		int ret = getNodeName(sid, docId, result);
            		String docName = result.get(0).toString();
            		
            		message = "Replaced Redaction Passwords for document '"+docName+"'";
            		//Node id for redaction is 0
            		addNotificationHistory(sid, 0, VNSConstants.nodeType_Redaction, VNSConstants.notify_Redaction_RepPassword, user, "-", "-", message);
            	}catch(Exception ex){}
            }
        }
        return NoError;
    }
    public int setDTVersionSettings(int sid, DocType docType)
    {
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        VWSLog.dbgCheck("setDTVersionSettings method sid val--->" + sid);
        String param=client.getUserName() + Util.SepChar + 
            client.getIpAddress() + Util.SepChar +
            Integer.toString(docType.getId()) + Util.SepChar + 
            docType.getTag() + Util.SepChar + 
            docType.getVREnable() + Util.SepChar + 
            docType.getVRInitVersion() + Util.SepChar + 
            docType.getVRInitRevision() + Util.SepChar + 
            docType.getVRPagesChange() + Util.SepChar +
            docType.getVRIncPagesChange() + Util.SepChar +
            docType.getVRPageChange() + Util.SepChar +
            docType.getVRIncPageChange() + Util.SepChar +
            docType.getVRPropertiesChange() + Util.SepChar +
            docType.getVRIncPropertiesChange();
            DoDBAction.get(getClient(sid),  database, "VR_SetSettings", param, new Vector());
            
            try{
            	Vector name = new Vector();
            	int retV = getNFNodeName(sid, docType.getId(), VNSConstants.module_DT, name);
            	String dtName = "";
            	if(name!=null && name.size()>0)
            		dtName = name.get(0).toString();
            	
            	if(Integer.parseInt(docType.getVREnable())==1)
            		message = "Version-Revision enabled for document type '"+dtName+"'";
            	else
            		message = "Version-Revision disabled for document type '"+dtName+"'";
            	
            	addNotificationHistory(sid, docType.getId(), VNSConstants.nodeType_DT, VNSConstants.notify_DT_VREnabled, client.getUserName(), "-", "-", message);
                GetStorageId(sid, docType);
            }catch(Exception ex){
            }
            
        return NoError;
    }
    public int getDTVersionSettings(int sid, DocType docType, Vector settings)
    {
    	VWSLog.dbgCheck("getDTVersionSettings method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        DoDBAction.get(getClient(sid),  database, "VR_GetSettings", 
                String.valueOf(docType.getId()), settings);
        return NoError;
    }

    public int getATSettings(int sid, Vector ATSettings)
    {
    	VWSLog.dbgCheck("getATSettings method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        DoDBAction.get(getClient(sid),  database, "AT_GetSetting", "", ATSettings);
        return NoError;
    }
    
    public int setATSetting(int sid, int eventId, int status)
    {
    	VWSLog.dbgCheck("setATSetting method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        String param=Integer.toString(eventId) + Util.SepChar + 
            Integer.toString(status)+Util.SepChar+client.getUserName()+
                Util.SepChar+client.getIpAddress();
            DoDBAction.get(getClient(sid),  database, "AT_SetSettings", param, new Vector());
        return NoError;
    }
    
    public int getATData(int sid, Vector filter, Vector data)
    {
    	VWSLog.dbgCheck("getATData method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param=(String)filter.get(0);
        int count=filter.size();
        for(int i=1;i<count;i++)
            param+=Util.SepChar+(filter.get(i)!=null?(String)filter.get(i):".");

        DoDBAction.get(getClient(sid),  database, "AT_GetData", param, data);
        return NoError;
    }
    
    public int detATData(int sid, Vector filter, String archivePath)
    {
    	VWSLog.dbgCheck("detATData11111 method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        int count = filter.size();
        String param="";
        if(count<=500) {
            detATData(sid, filter);
        }
        else
        {
           int i=0; 
           while(count-i>500){
               DoDBAction.get(getClient(sid),  database, "AT_DelData", param, new Vector());
                detATData(sid, filter.subList(i,i+500));
                i+=500;
            }
           i-=500;
           if(i<count)
               detATData(sid, filter.subList(i,count-1));
        }
        if(archivePath==null || archivePath.equals(""))
        {
            addATRecord(sid,0,VWATEvents.AT_OBJECT_AT, VWATEvents.AT_DATA_DEL,
                "Delete " + String.valueOf(count)+" Audit trail entries .");
        }
        else
        {
            addATRecord(sid,0,VWATEvents.AT_OBJECT_AT, VWATEvents.AT_DATA_ARCHIVE, 
                "Archive " + String.valueOf(count)+" Audit trail entries to "+ 
                    archivePath); 
        }
        return NoError;
    }
    
    private void detATData(int sid, List filter)
    {
    	VWSLog.dbgCheck("detATData method sid val--->" + sid);
        int count=filter.size();
        String param="";
        for(int i=0;i<count-1;i++) param+=(String)filter.get(i)+",";
            param+=filter.get(count-1);
            DoDBAction.get(getClient(sid),  database, "AT_DelData", param, new Vector());
    }
        
    public int updateDocType(int sid, DocType docType)
    {
    	VWSLog.dbgCheck("updateDocType method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Vector result=new Vector();
    	String param = "";
    		param = docType.getId() + Util.SepChar +
    				Util.getOneSpace(docType.getName()) +Util.SepChar +
    				(docType.getEnableTextSearch()?"1":"0");
    	DoDBAction.get(getClient(sid),  database, "UpdateDocType", param, result);
    	if(result==null || result.size()==0)
    		return NoError;
    	return Util.to_Number((String)result.get(0));
    }
    
    public int updateIndex(int sid, Index index)
    {
    	VWSLog.dbgCheck("updateIndex method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = "";
    		param = index.getId()+Util.SepChar+
    		Util.getOneSpace(index.getName()) +Util.SepChar+
    		index.getType() +Util.SepChar+
    		Util.getOneSpace(index.getMask()) +Util.SepChar+
    		Util.getOneSpace(index.getDefaultValue())+ Util.SepChar+
    		(index.isRequired()?"1":"0") +Util.SepChar+
    		index.getCounter() +Util.SepChar+
    		index.getSystem() +Util.SepChar+
    		Util.getOneSpace(index.getInfo()) +Util.SepChar+
    		Util.getOneSpace(index.getDescription());
    	Vector result=new Vector();
    	DoDBAction.get(getClient(sid),  database, "UpdateIndex", param, result);
    	if(result==null || result.size()==0)
    		return NoError;
    	return Util.to_Number((String)result.get(0));
    }
    public int updateDTIndex(int sid, int docTypeId, Index index)
    {
    	VWSLog.dbgCheck("updateDTIndex method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        	param = index.getDTIndexId() + Util.SepChar+
				docTypeId +Util.SepChar+
				index.getId() +Util.SepChar+
				Util.getOneSpace(index.getMask()) +Util.SepChar+
				Util.getOneSpace(index.getDefaultValue())+Util.SepChar+
				(index.isKey()?"1":"0" )+Util.SepChar+
				(index.isRequired()?"1":"0") +Util.SepChar+
				index.getOrder() +Util.SepChar+
				Util.getOneSpace(index.getInfo()) +Util.SepChar+
				Util.getOneSpace(index.getDescription());
        Vector result = new Vector();
        
        DoDBAction.get(getClient(sid),  database, "UpdateDocTypeIndex", param, result);
        if(result==null || result.size()==0)
            return NoError;
        return Util.to_Number((String)result.get(0));
    }

    private String getStrSelectionValues(Vector selectionValues){
    	String strValues="";
    	StringBuffer buffer = new StringBuffer();
    	try{
    		if(selectionValues==null || selectionValues.size()==0) return "";
    		int count = selectionValues.size();
    		String sTemp = "";
    		/**
    		 * filter is used to remove the duplicate values.
    		 */
    		Vector<String> filter = new Vector<String>();
    		if (selectionValues.size() > 0){
    			try{
    				for (int i = 0 ; i < count; i++){
    					if (i >= (selectionValues.size())) break;
    					String temp = selectionValues.get(i).toString().trim().replaceAll("\n", "").toLowerCase();
    					if (filter.contains(temp)) {
    						Object tmpObj = selectionValues.get(i);
    						selectionValues.remove(tmpObj);
    						continue;
    					}
    					filter.add(temp);
    				}
    			}catch(Exception ex){
    				ex.printStackTrace();
    			}
    			filter = null;
    			count=selectionValues.size();
    			buffer.append(selectionValues.get(0).toString().trim().replaceAll("\n", ""));        	
    			for(int i=1;i<count;i++)
    			{
    				// Removes space in between values in selection index
    				/*
    				 * Issue #599 	: Add values to an existing selection IF or Create a new selection IF, 
    				 * and add values to it. (Example method: AW -> DT tab -> Update -> Selection IF -> 
    				 * Values -> Enter values -> OK -> Save). Choose one of these fields as the Default Value.	 
    				 * Created By	: M.Premananth
    				 * Date			: 6-July-2006
    				 */        	    
    				sTemp = ((String)selectionValues.get(i)).trim().replaceAll("\n", "");
    				if (sTemp.length() > 0){
    					buffer.append("|"+ sTemp);
    				}
    			}
    		}
    		buffer.trimToSize();
    		strValues = buffer.toString();
    		while(strValues.endsWith("|")){
    			strValues = strValues.substring(0,strValues.lastIndexOf("|"));
    		}
    	}catch(Exception ex){
    		//VWSLog.add("Exception in getStrSelectionValues : "+ex.getMessage());
    	}
    	return strValues;
    }

    public int updateIndexSelection(int sid, Index index, Vector selValues, String clientMode)
    {
    	if (!assertSession(sid)) return invalidSessionId;
    	VWSLog.dbgCheck("updateIndexSelection method sid val--->" + sid);
    	String strValues = "";
    	if(selValues==null || selValues.size()==0) return NoError;

    	if(selValues!=null && selValues.size()>0){
    		strValues = getStrSelectionValues(selValues);
    	}


    	/*Fix for Issue No 498
    	 * Desc -AW Selection field values not saved
    	 * Nebu Alex
    	 * Commented the block of code below
    	 */
    	/*   if(strValues.length()<7980)//8000 - IndexID + DelValues
        {
            DoDBAction.get(getClient(sid),  database, "UpdateIndexSelection", 
            Integer.toString(index.getId())+"|1|"+
                strValues, new Vector());
        }
        else 
        { */        	
    	/*
    	 * The following logic implement after face the problem in the Client GCNA - They have 14000+ items stored in the Selection List 
    	 * Whilch will be expensive operation when it execute 14000 DB call, so we are passing 4000 character (Oracle String parameter maximum capacity is 4000 characters) to DB on each call. When it comes from adminwise AW will remove the duplicate 
    	 * in the selection list 
    	 */
    	int MAXLENGTH = 3985;
    	int lastIndex = 1, beginIndex = 0, delValues = 1;
    	int length = strValues.length();
    	if (length < MAXLENGTH){
    		lastIndex = length;
    	}else
    		lastIndex = MAXLENGTH;

    	while (lastIndex > 0 && lastIndex <= length) {
    		String tmpValue = "";
    		if (beginIndex + MAXLENGTH < length)	
    			tmpValue = strValues.substring(beginIndex, beginIndex + MAXLENGTH);
    		else 
    			tmpValue = strValues.substring(beginIndex, length);

    		if (tmpValue.lastIndexOf("|") >  0 && ((lastIndex + MAXLENGTH) < length))
    			lastIndex = beginIndex + tmpValue.lastIndexOf("|");
    		else
    			lastIndex = length;
    		if(!clientMode.equalsIgnoreCase("DY")) {
    		
    		DoDBAction.get(getClient(sid),  database, "UpdateIndexSelection", clientMode + "|" + String.valueOf
    				(index.getId()) + "|" + String.valueOf(delValues) + "|" +
    				strValues.substring(beginIndex, lastIndex), new Vector());    
    		}

    		beginIndex = lastIndex + 1;
    		if (delValues == 1){
    			delValues = 0;
    		}

    		if (beginIndex >= length) break;
    	}    
    	
    	if(!clientMode.equalsIgnoreCase("DY")) {
    		//updateSelectionIndexMap(index.getId(),selValues);
    		Vector selIndexValues = new Vector();
    		removeSelectionIndexMap(index.getId(), selValues);
    		populateSelectionIndex(sid,index.getId(),selIndexValues);       		
    	}

    	return NoError;
    }
    public int updateIndexSelection(int sid, Index index, String strValues, String clientMode)
    {
    	VWSLog.dbgCheck("updateIndexSelection 1 method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String selValuesStr = strValues;
    	/*Fix for Issue No 498
    	 * Desc -AW Selection field values not saved
    	 * Nebu Alex
    	 * Commented the block of code below
    	 */
    	/*   if(strValues.length()<7980)//8000 - IndexID + DelValues
        {
            DoDBAction.get(getClient(sid),  database, "UpdateIndexSelection", 
            Integer.toString(index.getId())+"|1|"+
                strValues, new Vector());
        }
        else 
        { */        	
    	/*
    	 * The following logic implement after face the problem in the Client GCNA - They have 14000+ items stored in the Selection List 
    	 * Whilch will be expensive operation when it execute 14000 DB call, so we are passing 4000 character (Oracle String parameter maximum capacity is 4000 characters) to DB on each call. When it comes from adminwise AW will remove the duplicate 
    	 * in the selection list 
    	 */
    	int MAXLENGTH = 3985;
    	int lastIndex = 1, beginIndex = 0, delValues = 1;
    	int length = strValues.length();
    	if (length < MAXLENGTH){
    		lastIndex = length;
    	}else
    		lastIndex = MAXLENGTH;

    	while (lastIndex > 0 && lastIndex <= length) {
    		String tmpValue = "";
    		if (beginIndex + MAXLENGTH < length)	
    			tmpValue = strValues.substring(beginIndex, beginIndex + MAXLENGTH);
    		else 
    			tmpValue = strValues.substring(beginIndex, length);

    		if (tmpValue.lastIndexOf("|") >  0 && ((lastIndex + MAXLENGTH) < length))
    			lastIndex = beginIndex + tmpValue.lastIndexOf("|");
    		else
    			lastIndex = length;
    		if(!clientMode.equalsIgnoreCase("DY")) {
        	
    		DoDBAction.get(getClient(sid),  database, "UpdateIndexSelection", clientMode + "|" + String.valueOf
    				(index.getId()) + "|" + String.valueOf(delValues) + "|" +
    				strValues.substring(beginIndex, lastIndex), new Vector()); 
    		}

    		beginIndex = lastIndex + 1;
    		if (delValues == 1){
    			delValues = 0;
    		}

    		if (beginIndex >= length) break;
    	}          
    	Vector<String> selValues = new Vector<String>();
    	String[] selArr = selValuesStr.split("\\|");
    	
    	if(VWSPreferences.getEnableSelection().equalsIgnoreCase("true")){
        	for(int i=0; i<selArr.length; i++){
        		selValues.add(selArr[i]);
        	}
        	if(!clientMode.equalsIgnoreCase("DY")) {
        		//updateSelectionIndexMap(index.getId(),selValues);
        		Vector selIndexValues = new Vector();
        		removeSelectionIndexMap(index.getId(), selValues);
        		populateSelectionIndex(sid,index.getId(),selIndexValues);       		
        	}

    	}

    	return NoError;
    }
    public int getIndices(int sid, int indexId, Vector indices)
    {
    	VWSLog.dbgCheck("find getIndices method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        return DoDBAction.get(getClient(sid),  database, "GetIndices", String.valueOf(indexId), indices);
    }







    public int getDocTypeInfo(int sid, int docTypeId, Vector docTypeInfo)
    {
    	VWSLog.dbgCheck("getDocTypeInfo method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        return DoDBAction.get(getClient(sid),  database, "GetDocTypeInfo", 
                                        String.valueOf(docTypeId), docTypeInfo);
    }
    public int resyncDirectory(int sid)
    {
    	VWSLog.dbgCheck("resyncDirectory method sid val--->" + sid);
        new DirectorySync(database, directory, false);
        return NoError;
    }
    public Vector getDirectoryPrincipals(int sid)
    {
    	VWSLog.dbg("Inside rampage server getdirctory principals.....");
    	VWSLog.dbgCheck("getDirectoryPrincipals method sid val--->" + sid);
        Vector principals=new Vector();
        Vector multiprincipals=new Vector();
        String loginServer = "";
		String ldapDirectory = "";
		ldapDirectory = VWSPreferences.getTree();
		loginServer = VWSPreferences.getSSLoginHost();
		String type = VWSPreferences.getSSType();
		String[] dirList = ldapDirectory.split(",");
		String[] serList = loginServer.split(",");
		Map hashMap=new HashMap();
		try{
			if(type.equalsIgnoreCase(SSCHEME_ADS)){
			 principals=(new DirectorySync(database, directory, true)).getDirectoryPrincipals();
			 multiprincipals.addAll(principals);
			}else if(type.equalsIgnoreCase(SCHEME_LDAP_ADS)){
				for(int dirCount=0; dirCount< dirList.length; dirCount++){
					VWSLog.dbg("dirList[dirCount]....."+dirList[dirCount]);
					VWSLog.dbg("serList[dirCount]....."+serList[dirCount]);
					Directory directory1 = Directory.getInstance(dirList[dirCount],serList[dirCount],true);
					VWSLog.dbg("directory inside get principals......"+directory.getDirName());
					principals=(new DirectorySync(database, directory1, true)).getDirectoryPrincipals(directory1,serList[dirCount]);
					multiprincipals.addAll(principals);
				}
			}
			
		}catch(Exception e){
			VWSLog.dbg("Exception while getting dir principals....."+e.getMessage());
		}
        return multiprincipals;
    }
    public Vector getDirectoryPrincipalsNew(int sid, String searchForPName)
    {
    	VWSLog.dbg("Inside rampage server getdirctory principals.....");
    	VWSLog.dbg("getDirectoryPrincipals method sid val--->" + sid);
        Vector principals=new Vector();
        Vector multiprincipals=new Vector();
        String loginServer = "";
		String ldapDirectory = "";
		ldapDirectory = VWSPreferences.getTree();
		loginServer = VWSPreferences.getSSLoginHost();
		String type = VWSPreferences.getSSType();
		String[] dirList = ldapDirectory.split(",");
		String[] serList = loginServer.split(",");
		Map hashMap=new HashMap();
		try{
			/**Code optimized to fix LDAP Novell issue for Alberta Justice E-Directory Syncing.Changed by vanitha on 06-02-2020***/
			if(type.equalsIgnoreCase(SCHEME_LDAP_ADS)){
				for(int dirCount=0; dirCount< dirList.length; dirCount++){
					VWSLog.add("dirList[dirCount]....."+dirList[dirCount]);
					VWSLog.dbg("serList[dirCount]....."+serList[dirCount]);
					Directory directory1 = Directory.getInstance(dirList[dirCount],serList[dirCount],true);
					VWSLog.add("directory inside get principals......"+directory.getDirName());
					principals=(new DirectorySync(database, directory1, true)).getDirectoryPrincipalsNew(directory1,serList[dirCount],searchForPName);
					multiprincipals.addAll(principals);
				}
			} else {
				principals=(new DirectorySync(database, directory, true)).getDirectoryPrincipals();
				multiprincipals.addAll(principals);
			}
			VWSLog.dbg("multiprincipals: "+ multiprincipals);
		}catch(Exception e){
			VWSLog.dbg("Exception while getting dir principals....."+e.getMessage());
		}
        return multiprincipals;
    }
    public int checkNextDocVersion(int sid, Document doc, String pi, 
        boolean newDoc ,Vector ret)
    {
    	VWSLog.dbgCheck("checkNextDocVersion method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        /* Issue 737:	When pageinfo is null or empty it is throwing oracle script execption
        * resolved by added 0 for pageinfo when it is null. Valli 16 May 2008
        */
        if( pi==null || pi.trim().equals("") ) pi = "0";
        String param = (newDoc?"1":"0")+ Util.SepChar +String.valueOf(doc.getId())+ Util.SepChar + pi;
        DoDBAction.get(getClient(sid),  database, "VR_CheckNextDocVersion", param, ret);
        return NoError;
    }
    public int setDocAuditInfo(int sid, Document doc, boolean newDoc, String pageInfo)
    {
    	VWSLog.dbgCheck("setDocAuditInfo method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result = new Vector();
        Client client = getClient(sid);
        if(pageInfo!= null && !pageInfo.equals(""))
        {
        	String param =  (newDoc? "1" : "0") +
            Util.SepChar + String.valueOf(doc.getId()) + Util.SepChar +
            client.getUserName() + Util.SepChar + 
            client.getIpAddress() + Util.SepChar + pageInfo;
            DoDBAction.get(getClient(sid),  database, "Doc_SetAuditInfo", param, result);
        }
        return NoError;
    }
    /*************************************eWork********************************/
    public int getEWorkTemplates(int sid, Vector templates)
    {
    	VWSLog.dbgCheck("getEWorkTemplates method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        return DoDBAction.get(getClient(sid),  database, "EWGetTemplates", "", templates);  
    }
    
    public int newEWorkTemplate(int sid, EWorkTemplate template) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("newEWorkTemplate method sid val--->" + sid);
        Vector mapping = template.getMapping();
        int MappingCnt = mapping.size();
        String mappingStr = "";
        for(int i=0; i < MappingCnt; i++)
            mappingStr += Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getIndexId() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWFieldId() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWField() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFromForm() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFromId() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFrom();
        
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = client.getUserName()             + Util.SepChar +
                       client.getIpAddress()            + Util.SepChar +
                       template.getName()               + Util.SepChar +
                       String.valueOf(template.getDocType().getId())
                                                        + Util.SepChar +
                       template.getEWorkMap()           + Util.SepChar +
                       template.getEWorkMapForm()       + Util.SepChar +
                       String.valueOf(template.isDocCopy()?1:0) 
                                                        + Util.SepChar +
                        String.valueOf(template.isAttachPages()?1:0) 
                                                        + Util.SepChar +
                        String.valueOf(template.isSaveAssoc()?1:0) 
                                                        + Util.SepChar +
                        String.valueOf(MappingCnt) + mappingStr;
        
        DoDBAction.get(getClient(sid),  database, "EWNewTemplate", param, ret);
        if(ret.size() > 0)
        {
            StringTokenizer st = new StringTokenizer
                                            ((String) ret.get(0), Util.SepChar);
            template.setId(Util.to_Number(st.nextToken()));
            return NoError;
        }
        else
            return Error;
    }
    public int updateEWorkTemplate(int sid, EWorkTemplate template) 
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("updateEWorkTemplate method sid val--->" + sid);
        Vector mapping = template.getMapping();
        int MappingCnt = mapping.size();
        String mappingStr = "";
        for(int i=0; i < MappingCnt; i++)
        mappingStr += Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getIndexId() +
        Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWFieldId() +
        Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWField() +
        Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFromForm() +
        Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFromId() +
        Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFrom();
        
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = client.getUserName()             + Util.SepChar +
                       client.getIpAddress()            + Util.SepChar +
                       String.valueOf(template.getId()) + Util.SepChar +
                       template.getName()               + Util.SepChar +
                       String.valueOf(template.getDocType().getId())
                                                        + Util.SepChar +
                       template.getEWorkMap()           + Util.SepChar +
                       template.getEWorkMapForm()       + Util.SepChar +
                       String.valueOf(template.isDocCopy()?1:0) 
                                                        + Util.SepChar +
                        String.valueOf(template.isAttachPages()?1:0) 
                                                        + Util.SepChar +
                        String.valueOf(template.isSaveAssoc()?1:0) 
                                                        + Util.SepChar +
                        String.valueOf(MappingCnt) + mappingStr;
        
        DoDBAction.get(getClient(sid),  database, "EWUpdateTemplate", param, ret);
        if(ret.size() > 0)
        {
            StringTokenizer st = new StringTokenizer
                                            ((String) ret.get(0), Util.SepChar);
            if(Util.to_Number(st.nextToken()) != 0)
                return NoError;
            else
                return Error;
        }
        else
            return Error;
    }
    public int getEWorkTemplateMapping(int sid, int templateId, Vector mapping)
    {
    	VWSLog.dbgCheck("getEWorkTemplateMapping method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(templateId);
        return DoDBAction.get(getClient(sid),  database, "EWGetTemplateMapping", param, mapping);  
    }
    public int deleteEWorkTemplate(int sid, EWorkTemplate template)
    {
    	VWSLog.dbgCheck("deleteEWorkTemplate method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(template.getId());
        Vector ret = new Vector();
        return DoDBAction.get(getClient(sid),  database, "EWDeleteTemplate", param, ret);  
    }
    public int newEWorkSubmit(int sid, EWorkSubmit ewSubmit) 
    {
         if (!assertSession(sid)) return invalidSessionId;
      	VWSLog.dbgCheck("newEWorkSubmit method sid val--->" + sid);
        Vector mapping = ewSubmit.getEWorkTemplate().getMapping();
        int MappingCnt = mapping.size();
        String mappingStr = "";
        for(int i=0; i < MappingCnt; i++)
            mappingStr += Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getIndexId() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWFieldId() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWField() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFromForm() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFromId() +
                   Util.SepChar + ((EWorkMap) mapping.elementAt(i)).getEWorkFieldFrom();
        
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = client.getUserName()             + Util.SepChar +
                       client.getIpAddress()            + Util.SepChar +
                       String.valueOf(ewSubmit.getEWorkTemplate().getId())
                                                        + Util.SepChar +
                       String.valueOf(ewSubmit.getDoc().getId())
                                                        + Util.SepChar +
                       ewSubmit.getEWorkTemplate().getEWorkMap()
                                                        + Util.SepChar +
                       ewSubmit.getEWorkTemplate().getEWorkMapForm()
                                                        + Util.SepChar +
                       String.valueOf(ewSubmit.getEWorkTemplate().isDocCopy()?1:0) 
                                                        + Util.SepChar +
                        String.valueOf(ewSubmit.getEWorkTemplate().isAttachPages()?1:0) 
                                                        + Util.SepChar +
                        String.valueOf(ewSubmit.getEWorkTemplate().isSaveAssoc()?1:0) 
                                                        + Util.SepChar +
                        ewSubmit.getDateOfCheckout()    + Util.SepChar +
                        ewSubmit.getDoc().getName()     + Util.SepChar +
                        ewSubmit.getEWorkFolder()       + Util.SepChar +
                        String.valueOf(MappingCnt) + mappingStr;
        
        DoDBAction.get(getClient(sid),  database, "EWNewSubmit", param, ret);
        if(ret.size() > 0)
        {
            StringTokenizer st = new StringTokenizer
                                            ((String) ret.get(0), Util.SepChar);
            ewSubmit.setId(Util.to_Number(st.nextToken()));
            return NoError;
        }
        else
            return Error;
    }
    
    public int getEWorkSubmits(int sid, Vector ewSubmits)
    {
    	VWSLog.dbgCheck("getEWorkSubmits method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        return DoDBAction.get(getClient(sid),  database, "EWGetSubmits", "", ewSubmits);  
    }
    public int getEWorkSubmitMapping(int sid, int submitId, Vector mapping)
    {
    	VWSLog.dbgCheck("getEWorkSubmitMapping method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(submitId);
        return DoDBAction.get(getClient(sid),  database, "EWGetSubmitMapping", param, mapping);  
    }
    
    public int endEWorkSubmit(int sid, int submitId)
    {
    	VWSLog.dbgCheck("endEWorkSubmit method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param =String.valueOf(submitId);
        Vector ret = new Vector();
        DoDBAction.get(getClient(sid),  database, "EWEndSubmit", param , ret);
        return NoError;
    }
    //-----------------------------AIP Methods----------------------------------
    //Add the following method to identified the logged in user-client type
    public String getAppType(int sid){
    	VWSLog.dbgCheck("getAppType method sid val--->" + sid);
    	Client client = getClient(sid);
    	String appType = "";
    	if (client.getClientType() == Client.EAS_Client_Type) appType = "EAS";
    	if (client.getClientType() == Client.AIP_Client_Type) appType = "AIP";
    	if (client.getClientType() == Client.Fax_Client_Type) appType = "FAX";
    	return appType;
    }
    public int addMimeMessage(int sid,String sMimeParms)
    {
    	VWSLog.dbgCheck("addMimeMessage method sid val--->" + sid);
          if (!assertSession(sid)) return invalidSessionId;
          String appType = getAppType(sid);
          sMimeParms = sMimeParms + Util.SepChar + appType; 
           Vector rs =new Vector();
           DoDBAction.get(getClient(sid),  database,"EH_ADDMimeMessage",sMimeParms,rs);
           int recId =0;
           if(rs != null && rs.size() != 0)
           {
                 recId = Integer.parseInt((String)rs.get(0));                 
           }
           return recId;  
    } 
    public int getMimeMessages(int sid,Vector messages)
    {
    	  VWSLog.dbgCheck("getMimeMessages method sid val--->" + sid);
          if (!assertSession(sid)) return invalidSessionId;        
          String appType = getAppType(sid);
           DoDBAction.get(getClient(sid),  database,"EH_GetMimeMessages",appType,messages);
           return NoError; 
    } 
    //Issue 04080603
    /*
   There is a significant delay in the launch of the application,
   then a significant delay as soon as a connection has been established
    to the room, If populating of the Messages ID tab is the reason then this 
    should be eliminated and fetched on demand,
   */
    public int getMimeMsg(int sid,Vector messages,String Params)
    {
    	  VWSLog.dbgCheck("getMimeMsg method sid val--->" + sid);
          if (!assertSession(sid)) return invalidSessionId;        
          //String appType = getAppType(sid);
          //Params = appType + Util.SepChar + Params;
           DoDBAction.get(getClient(sid),  database,"EH_GetMimeMsg",Params,messages);
           return NoError; 
    } 
    
    
    public int getMimeMessages(int sid,Vector messages,String Params)
    {
    	  VWSLog.dbgCheck("getMimeMessages method sid val--->" + sid);
          if (!assertSession(sid)) return invalidSessionId;        
          String appType = getAppType(sid);
          Params = appType + Util.SepChar + Params;
           DoDBAction.get(getClient(sid),  database,"EH_GetMimeMessages",Params,messages);
           return NoError; 
    } 
    public int getMimeMessagesCount(int sid,Vector files)    
    {
    	VWSLog.dbgCheck("getMimeMessagesCount method sid val--->" + sid);
       if (!assertSession(sid)) return invalidSessionId;  
      DoDBAction.get(getClient(sid),  database,"EH_GetMimeMessagesRowCount","",files);       
      return NoError;
    }
    //Issue 04080603
    public int deleteOldMimeMessages(int sid,String hours)
    {
    	  VWSLog.dbgCheck("deleteOldMimeMessages method sid val--->" + sid);
          if (!assertSession(sid)) return invalidSessionId;          
          DoDBAction.get(getClient(sid),  database,"EH_DeleteOldMimeMessages",String.valueOf(hours),
                                                                     new Vector());
          return NoError; 
    } 
    public int deleteMimeMessage(int sid,int msgId)
    {
          if (!assertSession(sid)) return invalidSessionId;
          VWSLog.dbgCheck("deleteMimeMessage method sid val--->" + sid);
          DoDBAction.get(getClient(sid),  database,"EH_DeleteMimeMessage",Integer.toString(msgId),new Vector());
          return NoError; 
    }
    public int setAIPInit(int sid,Hashtable initMap)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("setAIPInit method sid val--->" + sid);
        String appType = getAppType(sid);
        if(initMap !=null)
        {    
            for(Enumeration e =initMap.keys();e.hasMoreElements();)
            {
               String key =(String)e.nextElement();
               if(key.equalsIgnoreCase("InputFolders"))
               {                   
                    String[] values =(String[])initMap.get(key);
                    int i=0;
                    for (; i < values.length; i++)
                        saveAIPInputFolder(sid, Integer.toString(i), values[i], appType);
                    if (i != 10)
                    for (; i < 10; i++)            
                        saveAIPInputFolder(sid, Integer.toString(i), "", appType);
               } 
               else
                 saveAIPInitParam(sid, key,(String)initMap.get(key), appType);
            }             
        }    
        return NoError;
    }
    /**
     * CV10.1 issue fix for AIP processing location  issue fix for multiple system.
     * Added pass-1 as parameter in saveAIPInputFolder
     * @param sid
     * @param initMap
     * @param ipaddress
     * @return
     */
    public int setAIPInit(int sid,Hashtable initMap,String ipaddress)
    {
        if (!assertSession(sid)) return invalidSessionId;
        VWSLog.dbgCheck("setAIPInit method sid val--->" + sid);
        String appType = getAppType(sid);
        if(initMap !=null)
        {    
            for(Enumeration e =initMap.keys();e.hasMoreElements();)
            {
               String key =(String)e.nextElement();
               if(key.equalsIgnoreCase("InputFolders"))
               {                   
                    String[] values =(String[])initMap.get(key);
                    int i=0;
                    saveAIPInputFolder(sid, "-1", "", appType,ipaddress);
                    for (; i < values.length; i++)
                        saveAIPInputFolder(sid, Integer.toString(i), values[i], appType,ipaddress);
                    if (i != 10)
                    for (; i < 10; i++)            
                        saveAIPInputFolder(sid, Integer.toString(i), "", appType,ipaddress);
               } 
               else
                 saveAIPInitParam(sid, key,(String)initMap.get(key), appType);
            }             
        }    
        return NoError;
    }
    
    public int getAIPInit(int sid,Hashtable initMap)
    {
    	VWSLog.dbgCheck("getAIPInit method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String appType = getAppType(sid);
        if(!getInitParameter(sid, initMap, appType))return Error;        
        initMap.put("InputFolders",getInputFolders(sid, appType));
        return NoError;
    }
    /**
     * CV10.1 issue fix for AIP processing location  issue fix for multiple system.
     * @param sid
     * @param initMap
     * @param ipaddress
     * @return
     */
    public int getAIPInit(int sid,Hashtable initMap,String ipaddress)
    {
    	VWSLog.dbgCheck("getAIPInit method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String appType = getAppType(sid);
        if(!getInitParameter(sid, initMap, appType))return Error;        
        initMap.put("InputFolders",getInputFolders(sid, appType,ipaddress));
        return NoError;
    }
    private void saveAIPInputFolder(int sid, String ListIndex,String Value, String appType)
    {
    	VWSLog.dbgCheck("saveAIPInputFolders method sid val--->" + sid);
        String param = "";
        	param = Util.getOneSpace(ListIndex) +	Util.SepChar + 
        			Util.getOneSpace(appType) + Util.SepChar + 
        					Util.getOneSpace(Value);
        DoDBAction.get(getClient(sid),  database,"EH_setEHInputFolder", param, new Vector());
    }
    /**
     * CV10.1 issue fix for AIP processing location  issue fix for multiple system.
     * @param sid
     * @param ListIndex
     * @param Value
     * @param appType
     * @param ipaddress
     */
    private void saveAIPInputFolder(int sid, String ListIndex,String Value, String appType,String ipaddress)
    {
    	VWSLog.dbgCheck("saveAIPInputFolders method sid val--->" + sid);
        String param = "";
        	param = Util.getOneSpace(ListIndex) +	Util.SepChar + 
        			Util.getOneSpace(appType) + Util.SepChar + 
        					Util.getOneSpace(Value)+Util.SepChar+ipaddress;
        DoDBAction.get(getClient(sid),  database,"EH_setEHInputFolder", param, new Vector());
    }
    private void saveAIPInitParam(int sid, String Name,String value, String appType)
    {
    	VWSLog.dbgCheck("saveAIPInitParam method sid val--->" + sid);
    	String param = "";
	        param = Util.getOneSpace(Name)+	Util.SepChar + Util.getOneSpace(appType) +
	        		Util.SepChar +Util.getOneSpace(value);

        DoDBAction.get(getClient(sid),  database,"EH_setInitParameter", param, new Vector());
    }
    private boolean getInitParameter(int sid, Hashtable initMap, String appType)
    {
    	VWSLog.dbgCheck("getInitParameter method sid val--->" + sid);
        final String SepChar= new Character((char)9).toString();
        Vector rs = new Vector();
        DoDBAction.get(getClient(sid),  database,"EH_getInitParameters", appType, rs);
        if(rs != null)
        {
             if (rs.size() != 0)
             {
                 Iterator iterator = rs.iterator();
                 while (iterator.hasNext())
                 {
                     String srs = (String) iterator.next();                       
                     StringTokenizer Stoken =new StringTokenizer(srs,SepChar);
                     String Name ="";
                     String value="";
                     if(Stoken.hasMoreTokens()) Name=Stoken.nextToken().trim();
                     if(Stoken.hasMoreTokens()) value=Stoken.nextToken().trim();
                     initMap.put(Name,value);              
                 }
                 return true;
             }
        }
        return false;
    }
    private String[] getInputFolders(int sid, String appType)
    {
    	VWSLog.dbgCheck("getInputFolders method sid val--->" + sid);
        String[] inputFolders = new String[10];
        final String SepChar= new Character((char)9).toString();
        Vector rs = new Vector();        
        DoDBAction.get(getClient(sid),  database,"EH_getEHInputFolders",appType,rs);
        if(rs != null)
        {
            if (rs.size() != 0)
            {
                Iterator iterator = rs.iterator();
                while (iterator.hasNext())
                {
                    String srs = (String) iterator.next();                      
                    StringTokenizer Stoken =new StringTokenizer(srs,SepChar);
                    String ListIndex ="";
                    String Value="";
                    if(Stoken.hasMoreTokens()) ListIndex=Stoken.nextToken().trim();
                    if(Stoken.hasMoreTokens()) Value=Stoken.nextToken().trim();
                            inputFolders[Integer.parseInt(ListIndex)] = Value;
                }
            }
            return inputFolders;
         }
        return new String[10];
   }
    /**
     * GetInputFolder issue fix for AIP processing location  issue fix for multiple system.
     * @param sid
     * @param appType
     * @param ipaddress
     * @return
     */
    private String[] getInputFolders(int sid, String appType,String ipaddress)
    {
    	VWSLog.dbgCheck("getInputFolders method sid val--->" + sid);
        String[] inputFolders = new String[10];
        final String SepChar= new Character((char)9).toString();
        Vector rs = new Vector();        
        String params=appType+Util.SepChar+ipaddress+Util.SepChar;
        DoDBAction.get(getClient(sid),  database,"EH_getEHInputFolders",params,rs);
        if(rs != null)
        {
            if (rs.size() != 0)
            {
                Iterator iterator = rs.iterator();
                while (iterator.hasNext())
                {
                    String srs = (String) iterator.next();                      
                    StringTokenizer Stoken =new StringTokenizer(srs,SepChar);
                    String ListIndex ="";
                    String Value="";
                    if(Stoken.hasMoreTokens()) ListIndex=Stoken.nextToken().trim();
                    if(Stoken.hasMoreTokens()) Value=Stoken.nextToken().trim();
                            inputFolders[Integer.parseInt(ListIndex)] = Value;
                }
            }
            return inputFolders;
         }
        return new String[10];
   } 
    
    
    
   public int setAIPForm(int sid,String params)
   {
       if (!assertSession(sid)) return invalidSessionId; 
  	   VWSLog.dbgCheck(" setAIPForm method sid val--->" + sid);       
       Vector rs =new Vector();
       DoDBAction.get(getClient(sid),  database,"EH_UpdateEHForm",params,rs);
       int formId =0;
       if(rs != null && rs.size() != 0)
       {
             formId = Integer.parseInt((String)rs.get(0));                 
       }
        return formId;  
   } 
   public int setAIPFormField(int sid,int formId,String fieldParam)
   {
        if (!assertSession(sid)) return invalidSessionId;        
        String Params =formId+Util.SepChar+fieldParam;        
        VWSLog.dbgCheck("setAIPFormField method sid val--->" + sid);
         Vector rs =new Vector();
         int id = 0;
         DoDBAction.get(getClient(sid),  database,"EH_UpdateEHFormField",Params,rs);
         if(rs != null && rs.size() != 0)
         {
             id =Integer.parseInt((String)rs.get(0));                 
         }
         return id;      
   } 
   public int deleteAIPFormField(int sid,int fieldId)
   {
		VWSLog.dbgCheck("deleteAIPFormField method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector rs =new Vector();
        DoDBAction.get(getClient(sid),  database, "EH_DeleteEHFormField", 
                                                  Integer.toString(fieldId),rs);
        return NoError;        
   }    
   public int getAIPForms(int sid,Vector forms)
   { 
	   VWSLog.dbg("getAIPForms method sid val--->" + sid);
       if (!assertSession(sid)) return invalidSessionId;         
      DoDBAction.get(getClient(sid),  database,"EH_GetEHForms","",forms);
      return NoError;       
   }
   public int getAIPFormFields(int sid,int formId,Vector fields)
   {
	  VWSLog.dbgCheck("getAIPFormFields method sid val--->" + sid);
      if (!assertSession(sid)) return invalidSessionId;  
      String parms = "";  
    	  parms = formId + Util.SepChar;
      DoDBAction.get(getClient(sid),  database,"EH_GetEHFormFields",parms,fields);
      return NoError;       
   }    
   public int deleteAIPForm(int sid,int formId)
   { 	
	   VWSLog.dbgCheck("deleteAIPForm method sid val--->" + sid);
       if (!assertSession(sid)) return invalidSessionId;
        DoDBAction.get(getClient(sid),  database,"EH_DeleteEHForm", Integer.toString(formId),
                                                                  new Vector());
        return NoError;
   }
   public int deleteAIPFormFields(int sid,int formId)
   {
	   VWSLog.dbgCheck("deleteAIPFormFields method sid val--->" + sid);
       if (!assertSession(sid)) return invalidSessionId;
       DoDBAction.get(getClient(sid),  database,"EH_DeleteEHFormFields", 
                                         Integer.toString(formId),new Vector());
       return NoError;       
   }  
   public int getAIPFormMap(int sid,int formId,int mapDTId,Vector maps)
   {
	  VWSLog.dbgCheck("getAIPFormMap method sid val--->" + sid);
      if (!assertSession(sid)) return invalidSessionId; 
      String params =formId+Util.SepChar+mapDTId;
      DoDBAction.get(getClient(sid),  database,"EH_GetEHMapDTFormFields",params, maps);      
      return NoError;
   }
   public int deleteAIPFormMap(int sid,int formId,int mapDTId)
   {  
	  VWSLog.dbgCheck("deleteAIPFormMap method sid val--->" + sid);
      if (!assertSession(sid)) return invalidSessionId;  
      String Params =formId+Util.SepChar+mapDTId;
      DoDBAction.get(getClient(sid),  database,"EH_DeleteEHMapDTFormFields",Params,new Vector());
      return NoError;
    }
    public int setAIPFormFieldMap(int sid,String fieldMapParms) 
    {  
      if (!assertSession(sid)) return invalidSessionId;  
  	  VWSLog.dbgCheck("setAIPFormFieldMap method sid val--->" + sid);
      Vector rs =new Vector();
      DoDBAction.get(getClient(sid),  database,"EH_UpdateEHMapDTFormField",fieldMapParms,rs);
      if(rs != null && rs.size() != 0){
              return Util.to_Number((String)rs.get(0));                 
      }
      return Error;         
    }    
    // Code added for EAS Enahance History Tab - Start
    public int getAIPHistoryFiles(int sid,Vector files,String Params)    
    {
      if (!assertSession(sid)) return invalidSessionId;
      VWSLog.dbgCheck("find getAIPHistoryFiles method sid val--->" + sid);
      String appType = getAppType(sid);
      Params = appType + Util.SepChar + Params;
      DoDBAction.get(getClient(sid),  database,"EH_GetEHHistoryFiles",Params,files);       
      return NoError;
    }
    public int getAIPHistoryCount(int sid,Vector files,String params)    
    {
       if (!assertSession(sid)) return invalidSessionId; 
   		VWSLog.dbgCheck("getAIPHistoryCount method sid val--->" + sid);
      DoDBAction.get(getClient(sid),  database,"EH_GetEHHistoryRowCount",params,files);       
      return NoError;
    }
    // Code added for EAS Enahance History Tab - End
    // Archive History Files - Start
    public int getAIPHistoryFilesForArchive(int sid,Vector files,String Params)    
    {
      VWSLog.dbgCheck("getAIPHistoryFilesForArchive method sid val--->" + sid);
      if (!assertSession(sid)) return invalidSessionId;
      String appType = getAppType(sid);
      Params = Params + Util.SepChar + appType;
      DoDBAction.get(getClient(sid),  database,"EH_ArchiveHistory",Params,files);       
      return NoError;
    }
    public int deleteAIPHistoryArchivedFiles(int sid)    
    {
    	VWSLog.dbgCheck("deleteAIPHistoryArchivedFiles method sid val--->" + sid);
      if (!assertSession(sid)) return invalidSessionId;
      String appType = getAppType(sid);
      DoDBAction.get(getClient(sid),  database,"EH_DeleteArchived",appType,new Vector());       
      return NoError;
    }
    // Archive History Files - End
    public int setAIPHistoryFile(int sid,String fileParams)
    {
    	VWSLog.dbgCheck("setAIPHistoryFile method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;  
        String appType = getAppType(sid);
        fileParams = fileParams + Util.SepChar + appType;
        int fileId =0;
        Vector rs =new Vector();
        DoDBAction.get(getClient(sid),  database,"EH_UpdateEHHistoryFile",fileParams,rs);
        if(rs != null && rs.size() != 0)
        {
            fileId =Integer.parseInt((String)rs.get(0));                 
        }          
        return fileId;
    }
    public int deleteAIPHistoryFile(int sid,int fileId)
    {
    	VWSLog.dbgCheck("deleteAIPHistoryFile method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        DoDBAction.get(getClient(sid),  database,"EH_DeleteEHHistoryFile", 
                                         Integer.toString(fileId),new Vector());
        return NoError;
    }
    public int setAIPDocument(int sid,String docParams)
    {
    	VWSLog.dbgCheck("setAIPDocument method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        String appType = getAppType(sid);
        docParams = docParams + Util.SepChar + appType;
        Vector rs =new Vector();
        int docId =0;
        DoDBAction.get(getClient(sid),  database,"EH_UpdateEHDocument",docParams,rs);
        if(rs != null && rs.size() != 0)
        {
            docId =Util.to_Number((String)rs.get(0));            
        }          
      return docId;
    }
    public int setAIPDocFields(int sid,Vector fieldsParams)
    {
    	VWSLog.dbgCheck("setAIPDocFields method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;  
        if(fieldsParams ==null || fieldsParams.size() == 0) 
                                                        return invalidParameter;
        for(int i=0;i< fieldsParams.size();i++)
        {
            String fieldParam =(String)fieldsParams.get(i);                           
            DoDBAction.get(getClient(sid),  database,"EH_UpdateEHDocumentField", fieldParam, 
                                                                  new Vector());
        }
      return NoError;
    }    
    public int getAIPDocuments(int sid,int AIPFileId,Vector aipDocs, int pendingStatus)
    {
    	VWSLog.dbgCheck("getAIPDocuments method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId; 
        String appType = getAppType(sid);
        String param="";
        	param = "P" + Util.SepChar + (Integer.toString(AIPFileId))+ Util.SepChar + Util.getOneSpace(appType) + Util.SepChar + pendingStatus;
        DoDBAction.get(getClient(sid),  database,"EH_GetEHDocuments", param, aipDocs);
        return NoError;
    }   
    public int getAIPDocFields(int sid,int aipDocId,Vector fields)
    {
    	VWSLog.dbgCheck("getAIPDocFields method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;  
        String param = "";
        	param =""+Integer.toString(aipDocId)+Util.SepChar;
        	
        DoDBAction.get(getClient(sid),  database,"EH_GetEHDocumentFields",param,fields);
        return NoError;       
    } 
    public int deleteAIPDocument(int sid,int aipDocId)
    {
     	VWSLog.dbgCheck("deleteAIPDocument method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;          
        DoDBAction.get(getClient(sid),  database,"EH_DeleteEHDocument", 
                                       Integer.toString(aipDocId),new Vector());
        return NoError;       
    }
    public int getAIPEmailSuffixes(int sid,Vector suffixes)
    {
     	VWSLog.dbgCheck("getAIPEmailSuffixes method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;           
        DoDBAction.get(getClient(sid),  database,"EH_GetEHEmailSuffixes"," ",suffixes);
        return NoError;
    } 
    public int setAIPEmailSuffixes(int sid,Vector suffixes)
    {
     	VWSLog.dbgCheck("setAIPEmailSuffixes method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        DoDBAction.get(getClient(sid),  database,"EH_DeleteEHEmailSuffixes","",new Vector());
        for(int i=0;i< suffixes.size();i++)
        {            
            DoDBAction.get(getClient(sid),  database,"EH_AddEHEmailSuffix", 
                                          (String)suffixes.get(i),new Vector());
        }  
        return NoError;      
    }    
    public int getUserSign(int sid, String userName, int signType,VWDoc signDoc)
    {
    	VWSLog.dbgCheck("getUserSign method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        DBGetSignImage(userName, signType, signDoc);
        return NoError;
    }
    public int setUserSign(int sid, String userName, int signType, VWDoc signDoc)
    {
    	VWSLog.dbgCheck("setUserSign method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        File signFile=Signature.makeSignFilePath(null,userName,signType);
        signDoc.setDstFile(signFile.getPath());
        signDoc.writeContents();
        boolean removeSign = false;
        DBInsertSignImage(sid, userName, signType, signFile, removeSign);
        signFile.deleteOnExit();
        return NoError;
    }
    public int removeUserSign(int sid, String userName, int signType)
    {
    	VWSLog.dbgCheck("removeUserSign method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        boolean removeSign = true;
        DBInsertSignImage(sid, userName, signType, null, removeSign);
        return NoError;
    }
    private boolean DBInsertSignImage(int sid, String userName, int type, File signFile, boolean removeSign)
    {
    	VWSLog.dbgCheck("DBInsertSignImage method sid val--->" + sid);
        ResultSet rs  = null;
        PreparedStatement ps = null;
        Connection cn = null;
        BufferedInputStream instream = null; 
        int ret = -1;
        try
        {
        	if(!removeSign)
        		instream = new BufferedInputStream(new FileInputStream(signFile));
            cn = database.aquireConnection();
            OracleCallableStatement ocs = null;
            CallableStatement cs = null;
            if (database.getEngineName().equalsIgnoreCase("Oracle"))
            {
            	BLOB blob = null;
            	if(!removeSign){
            		blob = getBlob(instream, cn);
            		if (blob == null || blob.length() <= 0) return false;
            	}
                ocs = (OracleCallableStatement) cn.prepareCall
                                       ("{call InsertSignImage(?,?,?,?)}");
                ocs.setString(1, userName);
                ocs.setInt(2, type);
                if(!removeSign){
                	ocs.setBLOB(3, blob);
                }else
                	ocs.setBLOB(3, null);
            	ocs.registerOutParameter(4, OracleTypes.CURSOR);
                ocs.execute();
                rs = (ResultSet) ocs.getObject(4);
                blob.freeTemporary();
                //if (ocs != null) { ocs.close(); ocs = null;}
                if (rs != null && rs.next() && (!removeSign))
                {
                    ret = rs.getInt("Fld");
                    rs.close();
                    rs = null;
                }
            }
            else if (database.getEngineName().equalsIgnoreCase("sqlserver"))
            {
            	/**
            	 * Code modified - Logic of updating the image changed 
            	 * from stored procedure to separate query execution for SQL SERVER 2005/2008
            	 * Vijaypriya.B.K
            	 */
            	userName = userName.toUpperCase();
            	ps = cn.prepareStatement("Select id From UserTable Where upper(Name) = ?");
                ps.setString(1, userName);
                rs = ps.executeQuery();
                int fileLength = 0;
                if(signFile!=null)
                	fileLength = (int)signFile.length();
                if (rs != null && rs.next())
                {
                	ret = Integer.parseInt(rs.getString("id"));
                	if(type==1){
                		ps = cn.prepareStatement("Update UserTable Set ASignature = ? Where id =?");
                		ps.setBinaryStream(1, instream, fileLength);
                		ps.setInt(2, ret);
                		ps.execute();
                	}else if(type==2){
                		ps = cn.prepareStatement("Update UserTable Set CSignature = ? Where id = ?");
                		ps.setBinaryStream(1, instream, fileLength);
                		ps.setInt(2, ret);
                		ps.execute();
                	}else if(type==3){
                		ps = cn.prepareStatement("Update UserTable Set TSignature = ? Where id = ?");
                		ps.setBinaryStream(1, instream, fileLength);
                		ps.setInt(2, ret);
                		ps.execute();
                	}
                }
            }
            if (ocs != null) { ocs.close(); ocs = null;}
            if (cs != null)  { cs.close(); cs = null;}
            if (rs != null)  { rs.close(); rs = null;}
            if (ps != null)  { ps.close(); ps = null;}
        }
        catch(Exception e)
        {
            VWSLog.war(e.getMessage());
        }
        finally
        {
            assertConnection(cn);
            if (instream != null) try{instream.close();} catch(IOException ioe){}
        }
        return (ret == 0 ? true : false);
    }
    private BLOB getBlob(BufferedInputStream instream, Connection cn)
    {
        BLOB blob = null;
        //Connection cn = null;
        try
        {
            //cn = database.aquireConnection();
            blob = BLOB.createTemporary(cn, false, CLOB.DURATION_SESSION);
            blob.open(BLOB.MODE_READWRITE);
            OutputStream outstream = blob.getBinaryOutputStream();
            byte[] buf = new byte[65536];
            int offs = 0;
            int len = 0;
            while ( (len = instream.read(buf)) != -1)
            {
                outstream.write(buf, offs, len);
                offs += len;
            }
            outstream.flush();
            blob.close();
            buf = null;
        }
        catch (Exception e)
        {
            VWSLog.war(e.getMessage());
        }
        //finally {assertConnection(cn);}
        return blob;
    }
    private void assertConnection(Connection cn)
    {
        if (cn == null) return;
        try
        {
            database.releaseConnection(cn);
        }
        catch (Exception e)
        {
        	message = resourceManager.getString("RampServMsg.ConnOrphaned");
        	VWSLog.war(message);
        	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
        }  
    }
    private int DBGetSignImage(String userName, int type, VWDoc signDoc)
    {
        ResultSet rs  = null;
        Connection cn = null;
        BufferedWriter writer = null; 
        FileInputStream is = null;
        int ret = -1;
        String FieldName="";
		if (type == 1 || type == 4)
        {      
            FieldName="ASignature";
        }
        else if(type==2)
        {
            FieldName="CSignature";
        }
        else if(type==3)
        {
            FieldName="TSignature";
        } 
		VWSLog.add("DBGetSignImage FieldName :"+FieldName);
        try
        {
            cn = database.aquireConnection();
            //if user name with ' symbol replaced with '' symbol in this query
            String user = userName.replaceAll("'","\'\'");
            if (database.getEngineName().equalsIgnoreCase("oracle"))
            {
                Statement stmt = cn.createStatement();                
                rs = stmt.executeQuery ("SELECT " + FieldName + " FROM UserTable Where RTRIM(UPPER(Name))= '"+
                		user.toUpperCase().trim() +"'");
                BLOB blob=null;
                if(rs.next())
                    blob = (BLOB) rs.getObject (FieldName); 
                ret=writeSignFile(blob.getBinaryStream(),signDoc,userName,type);
            }
            else if (database.getEngineName().equalsIgnoreCase("sqlserver"))
            {
                PreparedStatement  cs = null;
                VWSLog.dbg("QUERY :::"+"SELECT " + FieldName + " FROM UserTable Where RTRIM(UPPER(Name))= '"+
                        		user.toUpperCase().trim()+"'");
                cs = cn.prepareStatement
                        ("SELECT " + FieldName + " FROM UserTable Where RTRIM(UPPER(Name))= '"+
                        		user.toUpperCase().trim()+"'");
                rs=cs.executeQuery();
                java.io.InputStream in=null;
                if(rs.next())
                   in  = rs.getBinaryStream(1);
                VWSLog.dbg("InputStream :"+in.available());
                ret=writeSignFile(in,signDoc,userName,type);
            }
        }
        catch(Exception e)
        {
           VWSLog.war(e.getMessage());
        } 
        finally 
        {
            assertConnection(cn);
            if (writer != null) try{writer.close();} catch(IOException ioe){}
        }
        return ret;            
    }
    private int writeSignFile(InputStream in,VWDoc signDoc,String user,int type)
    {
        try
        {
                File file=Signature.makeSignFilePath(null, user, type);
                byte[] buf= new byte[10240];
                BufferedOutputStream fileOut = new BufferedOutputStream
                                                   (new FileOutputStream(file));
                while (in.read(buf) != -1)
                {
                    fileOut.write(buf);
                }
                fileOut.close();
                signDoc.setSrcFile(file.getPath());   
        }
          catch(Exception e)
          {
              return -1;
          }
          return 0;
    }
    public int setDocumentSign(int sid, Signature sign)
    {
    	VWSLog.dbgCheck("setDocumentSign method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result=new Vector();
        Client client = (Client) clients.get(new Integer(sid));
        String param=String.valueOf(sign.getDocId()) +Util.SepChar+
            client.getUserName()+Util.SepChar+
            client.getIpAddress()+Util.SepChar+
            String.valueOf(sign.getType());
            DoDBAction.get(getClient(sid),  database, "DOC_SetSignature", param, result);
            
            String docName = "";
            try{
            	Node node = new Node(sign.getDocId());
            	int ret = getNodePropertiesByNodeID(sid, node);
            	docName = node.getName();
            	message = "Signature applied for the document '"+docName+"'";

            	//For a folder notification having document event need to send the node type as 0
            	int settingsId = CheckNotificationExist(sid, sign.getDocId(), VNSConstants.nodeType_Document, VNSConstants.notify_Doc_Signature, client.getUserName());
        		if (settingsId >= 0){
        			int nodeType = VNSConstants.nodeType_Document;
        			if(sign.getDocId()!=settingsId)
        				nodeType = 0; 
        			setNotificationHistory(sid, settingsId, sign.getDocId(), nodeType, VNSConstants.notify_Doc_Signature, "-", "-", client.getUserName(), message);
        		}
            }catch(Exception ex){

            }
        if(result!=null && result.size()>0)
            return Util.to_Number((String)result.get(0));
        return NoError;
    }
    public int getDocumentSign(int sid, Signature sign, Vector signInfo)
    {
    	VWSLog.dbgCheck("find getDocumentSign method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = (Client) clients.get(new Integer(sid));
            DoDBAction.get(getClient(sid),  database, "DOC_GetSignature", 
                String.valueOf(String.valueOf(sign.getId()) +Util.SepChar+
                    sign.getDocId()), signInfo);
        return NoError;
    }
    public int getDocumentSigns(int sid, Document doc, Vector signInfo)
    {
    	VWSLog.dbgCheck("find getDocumentSigns method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = (Client) clients.get(new Integer(sid));
            DoDBAction.get(getClient(sid),  database, "DOC_GetSignatures", 
                String.valueOf(doc.getId()), signInfo);
        return NoError;
    }
    public int updateStorageInfo(int sid, ServerSchema storage)
    {
    	VWSLog.dbgCheck("find updateStorageInfo method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector result=new Vector();
        String param = "";
        if (storage.getStorageType().equals("Azure Storage")) {
        	param=storage.getId()+Util.SepChar + storage.getName()+Util.SepChar+
        			storage.getAddress()+Util.SepChar+
        			storage.getComport()+Util.SepChar+
        			storage.getPath()+Util.SepChar + storage.getStorageType() + Util.SepChar+
        			storage.getUserName() + Util.SepChar+
        			storage.getPassword();
        } else {
        	param=storage.getId()+Util.SepChar + storage.getName()+Util.SepChar+
        			storage.getAddress()+Util.SepChar+
        			storage.getComport()+Util.SepChar+
        			storage.getPath()+Util.SepChar +" "+ Util.SepChar+
        			" "+ Util.SepChar+
        			" "+Util.SepChar;

        }
        
        DoDBAction.get(getClient(sid),  database, "Storage_UpdateInfo", param, result);
        
        try{
        	message = "Storage location changed to '"+storage.getPath()+"'";
        	addNotificationHistory(sid, 0, VNSConstants.nodeType_Storage, VNSConstants.notify_Storage_LocationMod, getClient(sid).getUserName(), "-", "-", message);
        }catch(Exception ex){
        	
        }
        return NoError;
    }
    public int DBGetDocPageText(int sid, Document doc, Vector pages)
    {
    	VWSLog.dbgCheck("DBGetDocPageText method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        ResultSet rs  = null;
        Connection cn = null;
        BufferedWriter writer = null; 
        FileInputStream is = null;
        int ret = -1;
        VWDoc pageText=null;
        try
        {
            cn = database.aquireConnection();
            if (database.getEngineName().equalsIgnoreCase("oracle"))
            {
                Statement stmt = cn.createStatement();
                rs = stmt.executeQuery ("SELECT PageContents FROM Indexer Where Nodeid="+
                    doc.getId()  + " Order By PageId");
                while(rs.next())
                {
                    CLOB clob = (CLOB) rs.getObject ("PageContents"); 
                    Reader instream = clob.getCharacterStream(); 
                    pageText=new VWDoc();
                    ret=getPageText(clob.getCharacterStream(),pageText);
                    pages.add(pageText);
                }
            }
            else if (database.getEngineName().equalsIgnoreCase("sqlserver"))
            {
                PreparedStatement  cs = null;
                cs = cn.prepareStatement
                        ("SELECT PageContents FROM Indexer Where Nodeid="+
                    String.valueOf(doc.getId())+ " Order By PageId");
                VWSLog.dbg("Page count query : "+"SELECT PageContents FROM Indexer Where Nodeid="+String.valueOf(doc.getId())+ " Order By PageId");
                rs=cs.executeQuery();
                while(rs.next())
                {
                    pageText=new VWDoc();
                    ret=getPageText(rs.getString(1),pageText);
                    pages.add(pageText);
                }
            }
        }
        catch(Exception e)
        {
            VWSLog.war(e.getMessage());
        } 
        finally 
        {
            assertConnection(cn);
            if (writer != null) try{writer.close();} catch(IOException ioe){}
        }
        return ret;            
    }
    private int getPageText(java.io.Reader in,VWDoc pageText)
    {
        try
        {
           File file=File.createTempFile("VWPage",".tmp");
           char[] buf= new char[10240];
           OutputStreamWriter fileOut = new OutputStreamWriter
                                                   (new FileOutputStream(file));
           while (in.read(buf) != -1) fileOut.write(buf);
           fileOut.close();
           pageText.setSrcFile(file.getPath());
        }
        catch(Exception e)
        {
          return -1;
        }
        return 0;
    }
    private int getPageText(String content,VWDoc pageText)
    {
        try
        {
            File file=File.createTempFile("VWPage",".tmp");
            OutputStreamWriter fileOut = new OutputStreamWriter
                                                   (new FileOutputStream(file));
            fileOut.write(content);
            fileOut.close();
            pageText.setSrcFile(file.getPath());
        }
        catch(Exception e)
        {
          return -1;
        }
        return 0;
    }
/*
Issue No / Purpose:  <01/02/04/17/2006 Indexer Enhancement>
Created by: <Pandiya Raj.M>
Date: <6 Jun 2006>
This method will remove the given page reference from the 'Indexer' table.
*/
    
    public void DBRemovePageText(int docId, int pageId) throws RemoteException{
	int ret = -1;
	Vector result = new Vector();
	String returnValue = "";
        String param = String.valueOf(docId) + Util.SepChar + 
        			String.valueOf(pageId);
        
    	try{    		    		  	
    	    DoIxrDBAction.get(null, database, "IXRemovePageText", param, result);    
            if (result != null && result.size() > 0 ){
        	returnValue = result.get(0).toString();
                ret = Integer.parseInt(returnValue);
            }        	
        }catch(Exception ex){        		
        }
        
        //return (ret == 0 ? true : false);              	  
    }    
    
    /*
    Purpose:  Following methods are used by ARS
    Created by: Shanmugavalli.C
    Date: <20 July 2006>
    */    
    //---------------------ARS Methods called by ARS Server---------------------//    
    public int isDocDeleted(int sid, int nid)
    {
        if (!assertSession(sid)) return invalidSessionId;
       	VWSLog.dbgCheck("isDocDeleted method sid val--->" + sid);
        Client client = getClient(sid);
        Vector result = new Vector();
        String param = "";
        	param = nid + Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "CheckDocumentIsDel", param, result);
        if(result==null || result.size()==0) return Error;
        return Util.to_Number((String)result.get(0));
    }
    
    // Not in use remove later
    public int arsMoveNode(int sid, Node node)
    {
    	VWSLog.dbgCheck("arsMoveNode method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        Client client = getClient(sid);
        String param = Integer.toString(node.getId()) + Util.SepChar +
                       Integer.toString(node.getParentId()) + Util.SepChar +
                       client.getUserName() + Util.SepChar +
                       client.getIpAddress();
        //VWSLog.add("move username : "+client.getUserName());
        //DoDBAction.get(getClient(sid),  database, "ARSNode_Move", param, ret);
        DoDBAction.get(getClient(sid),  database, "Node_Move", param, ret);
        return NoError;
    }
    
       public int arsLockDocument(int sid, int did)
        {
            if (!assertSession(sid)) return invalidSessionId;
        	VWSLog.dbgCheck("arsLockDocument method sid val--->" + sid);
            Client client = getClient(sid);
            Vector ret = new Vector();
            int type = 1;
            String param = "";
            	param = did + Util.SepChar +
            	Util.getOneSpace(client.getUserName())+ Util.SepChar+
            			Util.getOneSpace(client.getIpAddress())+ Util.SepChar+
            			type;
            DoDBAction.get(getClient(sid),  database, "ARSDocumentLock", param , ret);
    
            if( ret.size() == 0 )   return LockDenied;
            int iret = Util.to_Number( (String) ret.get(0) );
            if(iret == 0)            return documentAlreadyLocked;
            if(iret < 0)             return DocumentDeleted;
            return NoError;
    }
       /* Used by ARS Need to change - Valli*/
       public int isDocumentLocked(int sid, int did, Vector name)
       {
            if (!assertSession(sid)) return invalidSessionId;
            VWSLog.dbgCheck("isDocumentLocked method sid val--->" + sid);
            Client client = getClient(sid);
            String param = "";
            	param = did+Util.SepChar;
            DoDBAction.get(getClient(sid),  database, "CheckDocumentIsLock", param , name);
            if (name.size() > 0)
            	return NoError;
    		else	
    			return Error;
       }
    
    	/*
    	* set Retention values of Document Type
    	*/
        public int setDTRetentionSettings(int sid, DocType docType)
        {
        	VWSLog.dbgCheck("setDTRetentionSettings method sid val--->" + sid);
            if (!assertSession(sid)) return invalidSessionId;
            Client client = getClient(sid);
            String param=client.getUserName() + Util.SepChar +
                client.getIpAddress() + Util.SepChar +
                Integer.toString(docType.getId()) + Util.SepChar +
                docType.getARSEnable() + Util.SepChar +
                docType.getARSRetentionPeriod() + Util.SepChar +
                docType.getARSRetentionPeriodStartsID() + Util.SepChar +
                docType.getARSDisposalActionID() + Util.SepChar +
                docType.getARSMoveDocLocation() + Util.SepChar +
                docType.getARSPolicyCreatedDate();
            	//VWSLog.err("Param : "+param);
                DoDBAction.get(getClient(sid),  database, "ARS_SetSettings", param, new Vector());
            return NoError;
        }
    
    	/*
    	* get Retention values of Document Type
    	*/
        public int getDTRetentionSettings(int sid, DocType docType, Vector settings)
        {
        	VWSLog.dbgCheck("getDTRetentionSettings method sid val--->" + sid);
            if (!assertSession(sid)) return invalidSessionId;
            DoDBAction.get(getClient(sid),  database, "ARS_GetSettings", String.valueOf(docType.getId()), settings);
            return NoError;
        }
    
    	/*
    	* get Retention values of Document Type
    	*/
        public int getDisposalActions(int sid, Vector disposalActions)
        {
        	VWSLog.dbgCheck("getDisposalActions method sid val--->" + sid);
            if (!assertSession(sid)) return invalidSessionId;
            DoDBAction.get(getClient(sid),  database, "ARS_GetSettings", "", disposalActions);
            return NoError;
        }
    
        /*
    	* get Retention Documents
    	*/
        /* Purpose:  Following ARS methods changed for Oracle script changes  (Param: int sessionID included)
         * Created by: <Valli>	Date: <08 Sep 2006>
         */
        public int getRetentionDocuments(int sid,Vector retentionDocuments)
        {
        	VWSLog.dbgCheck("getRetentionDocuments method sid val--->" + sid);
        	if (!assertSession(sid)) return invalidSessionId;
            Client client = getClient(sid);
            DoDBAction.get(getClient(sid),  database, "GetRetentionDocuments", Util.replaceSplCharFromTempTable(client.getIpAddress()), retentionDocuments);
            return NoError;
        }
        
        /*
    	* get document type which has ARS policy as generate report only
    	*/
        public int getARSReportDocTypes(int sid, Vector retentionDocuments)
        {
        	VWSLog.dbgCheck("getARSReportDocTypes method sid val--->" + sid);
            if (!assertSession(sid)) return invalidSessionId;
            Client client = getClient(sid);
            DoDBAction.get(getClient(sid),  database, "GetReportDocTypes", Util.replaceSplCharFromTempTable(client.getIpAddress()), retentionDocuments);
            //VWSLog.add("retentionDocuments : "+retentionDocuments.toString());
            return NoError;
        }
        
        /*
    	* get documents which has ARS policy as generate report only
    	*/
        public int getReportRetentionDocs(int sid, Vector retentionReportDocs, String docTypeID)
        {
        	VWSLog.dbgCheck("getReportRetentionDocs method sid val--->" + sid);
        	if (!assertSession(sid)) return invalidSessionId;
            Client client = getClient(sid);
            String param = docTypeID +Util.SepChar + Util.replaceSplCharFromTempTable(client.getIpAddress());
            DoDBAction.get(getClient(sid),  database, "GetRetentionReportDocs", param, retentionReportDocs);
            return NoError;
        }
        /*ARS method change for oracle script end*/
        

    public int arsDelDocument(String ipAddress, int nid, boolean toRecycle)
    {
        Vector nodes = new Vector();
        //String ipAddress = null;
        String arsUser = null;
        String param = "";
	        param = nid + Util.SepChar +
	        		(toRecycle? "1" : "0") + Util.SepChar +
	        		Util.getOneSpace(ipAddress) + Util.SepChar +
	        				Util.getOneSpace(arsUser)+ Util.SepChar +
	                "1";

        DoDBAction.get(null, database, "Node_Delete", param, nodes);
        if (nodes.size() == 0) return DelNodeError;
        if(toRecycle)
        {
            String str = ((String) nodes.get(0)).trim();
            if(str.equals("2") || str.equals("3") || str.equals("4"))
                  return DelNodeError;
            return NoError;
        }
        for (int i = 0; i < nodes.size(); i++)
        {
            String str = (String) nodes.get(i);
            if(str.equals("0")) return DelNodeError;    //error
            if(str.equals("1")) return NoError;         // do docs to del
            if(str.equals("-1")) return NoError;        //locked
            if(str.equals("-2")) return NoError;        //sec error
            ServerSchema dss = new ServerSchema(str);
            deleteDocumentFiles(dss);
        }
        return NoError;
    }
    public void setFreezedDocs(int sid, int docID){
    	setFreezedDocs(sid, docID, "", "0");
    }
 
    public int setFreezedDocs(int sid, int docID, String docName, String retentionId)
    {
    	VWSLog.dbgCheck("setFreezedDocs method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        int type = -1;
        if(client.getClientType() == Client.Ars_Client_Type)
        	type = 1;
		else 	
			type = 0;
        String param = Integer.toString(docID) + Util.SepChar +
        					docName + Util.SepChar +
							client.getUserName() + Util.SepChar +
							client.getIpAddress() + Util.SepChar +
							Integer.toString(type) + Util.SepChar +
							retentionId;
        int ret = DoDBAction.get(getClient(sid),  database, "VW_ARS_AddProcessedDocs", param, new Vector());
        return NoError;
    }
    
    public int unFreezeDocument(int sid, int docID)
    {
    	VWSLog.dbgCheck("unFreezeDocument method sid val--->" + sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        int type = -1;
        if(client.getClientType() == Client.Ars_Client_Type)
        	type = 1;
		else 	
			type = 0;
        /*Purpose:	Two param included for DB call (user and ipAddress) for Audit catch
         * Name:	C.Shanmugavalli		Date:	15 Sep 2006
         */
        String param = Integer.toString(docID) + Util.SepChar +
					   Integer.toString(type)+ Util.SepChar + 
					   client.getUserName()+ Util.SepChar + 
					   client.getIpAddress();
        DoDBAction.get(getClient(sid),  database, "VW_ARS_DelProcessedDocs", param, new Vector());
        return NoError;
    	
    }

    public int getFreezeDocs(int sid, Vector docs){
    VWSLog.dbgCheck("getFreezeDocs method sid val--->" + sid);
	if (!assertSession(sid)) return invalidSessionId;
	String param = "";
	DoDBAction.get(getClient(sid),  database, "VW_ARS_GetFreezeDocs", param, docs);
	return NoError;
    }
    public int isDocFreezed(int sid, int docID)
        {
    	 	VWSLog.dbgCheck("isDocFreezed method sid val--->" + sid);
            if (!assertSession(sid)) return invalidSessionId;
    
            Client client = getClient(sid);
            Vector result = new Vector();
            String param = Integer.toString(docID);
            /* Purpose: Wrong SP name was called. Name: C.Shanmugavall Date: 08 Sep 2006 
             */
            //DoDBAction.get(getClient(sid),  database, "CheckDocIsFreezed", param, result);
            /* Retention menu is enabled in client based on document type retention setting
            *  Return value 2 means for that document's doc type - retention is not set
            *  Return value 1 means for that document exists in ARSProcessedDocs table(i.e Freezed)
            *  Return value 0 means for that document not exists in ARSProcessedDocs table
            *  Return value 3 means for that document exists in ARSProcessedDocs table (i.e Processed)
        	*	C.Shanmugavalli 		21	Feb 2007 
        	*/
        	DoDBAction.get(getClient(sid),  database, "VW_ARS_CheckFreezeDocIsFound", param, result);
            if(result==null || result.size()==0) return Error;
            return Util.to_Number((String)result.get(0));
        	
    }
//  ---------------------End of ARS Methods called by ARS Server---------------------   
//  ---------------------Start of DRS Methods called by DRS Server---------------------
    
    
    public int getAcceptLabel(int sid, int routeId, int taskId, Vector output){
    	VWSLog.dbgCheck("getAcceptLabel method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(routeId)+Util.SepChar;
        DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteTaskAcceptLabel", param, output);            	
    	return NoError; 
    }
    
    public int getRejectLabel(int sid, int routeId, int taskId, Vector output){
    	VWSLog.dbgCheck("getRejectLabel method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(routeId)+Util.SepChar;
        DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteTaskRejectLabel", param, output);            	
    	return NoError; 
    }
    
    // To get all the route names pass -1 for routeId
    // To get manual route names pass 0 for routeId
    // To get particular route name pass routeid itself
    // To get all the invalid route names pass -2 for routeId
    public int getRouteInfo(int sid, int routeId, Vector docRoute){
    	VWSLog.dbg("getRouteInfo method sid val--->" + sid);
    	try {
    		if (!assertSession(sid)) return invalidSessionId;
	        String param = String.valueOf(routeId)+Util.SepChar;
	        //VWSLog.add(" GetRouteInfo:"+param);
	        DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteInfo", param, docRoute);
	        //VWSLog.add(" GetRouteInfo: "+docRoute.toString());
    	} catch (Exception e) {
    		VWSLog.err("Exception in getRouteInfo procedure call......."+e.getMessage());
    		return Error;
    	}
        return NoError;
    }
    public int getRouteImageInfo(int sid, int routeId, VWDoc routeImageDoc){
            ResultSet rs  = null;
            Connection cn = null;
            BufferedWriter writer = null; 
            int ret = -1;
            try
            {
                cn = database.aquireConnection();
                if (database.getEngineName().equalsIgnoreCase("oracle"))
                {
                    Statement stmt = cn.createStatement();
                    rs = stmt.executeQuery ("SELECT RouteImage FROM RouteInfo Where id="+
                        routeId);
                    while(rs.next())
                    {
                        CLOB clob = (CLOB) rs.getObject ("RouteImage");
                        Reader inStream = clob.getCharacterStream();
                        ret = writeRouteImageFile(inStream, routeImageDoc, routeId);
    	
                   }
                }
                else if (database.getEngineName().equalsIgnoreCase("sqlserver"))
                {
                    PreparedStatement  cs = null;
                    cs = cn.prepareStatement
                            ("SELECT RouteImage FROM RouteInfo Where id="+String.valueOf(routeId));
                    rs=cs.executeQuery();
                    while(rs.next())
                    {
                    	Reader inStream = rs.getCharacterStream(1);
                    	ret = writeRouteImageFile(inStream, routeImageDoc, routeId);
                    }
                }
            }
            catch(Exception e)
            {
            	ret = -1;
                VWSLog.war(e.getMessage());
            } 
            finally 
            {
                assertConnection(cn);
                if (writer != null) try{writer.close();} catch(IOException ioe){}
            }
            return ret;            
    }
    private int writeRouteImageFile(java.io.Reader in, VWDoc routeIamgeDoc, int routeId)
    {
        try
        {
                File file=RouteInfo.makeRouteImgFilePath(null, routeId);
                char[] charValue= new char[10240];
                OutputStreamWriter fileOut = new OutputStreamWriter(new FileOutputStream(file));
                while (in.read(charValue) != -1)
                {
                    fileOut.write(charValue);
                }
                fileOut.close();
                routeIamgeDoc.setSrcFile(file.getPath());
        }
          catch(Exception e)
          {
              return -1;
          }
          return 0;
    }
    public int getRouteIndexInfo(int sid, int routeId, Vector routeIndexList){
    	VWSLog.dbg("getRouteIndexInfo method sid val--->" + sid);
    	try {
	    	if (!assertSession(sid)) return invalidSessionId;
	        String param = String.valueOf(routeId)+Util.SepChar;
	        VWSLog.dbg(" GetRouteIndexInfo:"+param);
	        DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteIndexInfo", param, routeIndexList);
	        //VWSLog.add(" GetRouteIndexInfo: "+routeIndexList.toString());
    	} catch (Exception e) {
    		VWSLog.err("Exception in getRouteIndexInfo procedure call...."+e.getMessage());
    	}
    	return NoError;
    }
    /* if taskSequence is 0 SP will give all Task records of that route id
    * If taskSequence > 0 that particular task record is retrieved
    */
    public int getRouteTaskInfo(int sid, int routeId, int taskSequence, Vector routeTaskList){
    	int returnValue = 0;
    	try {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	String param = String.valueOf(routeId)+Util.SepChar+taskSequence;
	        //VWSLog.add(" GetRouteTaskInfo: "+param);
	    	returnValue = DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteTask", param, routeTaskList);
	        VWSLog.dbg(" GetRouteTaskInfo: "+routeTaskList.toString());
    	} catch (Exception e) {
    		VWSLog.err("Exception in getRouteTaskInfo procedure call...."+e.getMessage());
    	}
    	return returnValue;
    }
    public int getRouteUsers(int sid, int routeTaskId, Vector vRouteUsers){
    	return getRouteUsers(sid, routeTaskId, vRouteUsers, 0);
    }
    public int getRouteUsers(int sid, int routeTaskId, Vector vRouteUsers, int docId){
    	int returnValue = 0;
    	try {
	    	VWSLog.dbgCheck("getRouteUsers method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	    	int fetchGroup = 1;
	        String param = String.valueOf(routeTaskId)
	        				+Util.SepChar+fetchGroup 
	        				+Util.SepChar+docId;
	        VWSLog.dbg(" VW_DRSGetRouteUsers :"+param);
	        returnValue = DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteUsers", param, vRouteUsers);
	        //VWSLog.add(" routeUsers  "+vRouteUsers.toString());
    	} catch (Exception e) {
    		VWSLog.err("Exception in getRouteUsers procedure call...."+e.getMessage());
    	}
    	return returnValue;
    }
    
    /*public int getRouteUser(int sid, int routeUserId, Vector docRouteUsers){
    	
    	if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(routeUserId)+Util.SepChar;
        				
        //VWSLog.add(" VW_GetRouteUser:"+param);
        DoDBAction.get(getClient(sid),  database, "VW_GetRouteUser", param, docRouteUsers);
        //VWSLog.err(" VW_GetRouteUser:"+docRouteUsers.toString());
    	return NoError;
    }*/
    
    public int setRouteUsers(int sid, ArrayList routeUserList, int routeId, int routeTaskId){
    	 VWSLog.dbgCheck("setRouteUsers method sid val--->" + sid);
    	 if (!assertSession(sid)) return invalidSessionId;
    	 RouteUsers routeUsers = null;
    	 //RouteTaskid, routeid, GroupName, Userid, UserName, LevelSeq, SendEmail, ActionPermission, ESignature
    	 if (routeUserList != null && routeUserList.size() > 0)
    	 {
    		 for (int index = 0; index <routeUserList.size(); index++)
    		 {
    			 routeUsers = (RouteUsers)routeUserList.get(index);//new RouteUsers();
		         String param = routeTaskId+ Util.SepChar 
		        	 			+routeId+ Util.SepChar 
								+routeUsers.getGroupName()+ Util.SepChar
		         				+routeUsers.getUserId()+ Util.SepChar
		         				+routeUsers.getUserName()+ Util.SepChar
		         				+routeUsers.getRouteUserLevel()+ Util.SepChar
		         				+routeUsers.getSendEmail()+ Util.SepChar
		         				+routeUsers.getActionPermission()+ Util.SepChar
		         				+routeUsers.getESignature() + Util.SepChar
		         				+routeUsers.getExpiryPeriod() + Util.SepChar
		         				+routeUsers.getEUserid()+ Util.SepChar
		         				+routeUsers.getEUserName() + Util.SepChar
		         				+routeUsers.getNotifyMail() + Util.SepChar;
		         //VWSLog.add(" VW_DRSSetRouteUsers:"+param);
		         DoDBAction.get(getClient(sid),  database, "VW_DRSSetRouteUsers", param, new Vector());
    		 }
    	 }
    	 return NoError;
    }
	/*	* set route infovalues
	*/
    /*public int setRouteInfo(int sid, RouteInfo route)
    {
        if (!assertSession(sid)) return invalidSessionId;
        //Routeid, RouteName, StartAction, StartLocation, RouteActionid, 
        //ActionLocation, RouteDescription, RouteImage
        String param = route.getRouteId() + Util.SepChar + 
        	route.getRouteName() + Util.SepChar +
            route.getStartAction() + Util.SepChar +
            route.getStartLocation() + Util.SepChar +
            //route.getProcessType() + Util.SepChar +
            route.getRouteActionId() + Util.SepChar +
            route.getActionLocation() + Util.SepChar+
        	route.getRouteDescription() + Util.SepChar+
        	route.getRouteImage() + Util.SepChar;
        	Vector resultRouteInfo = new Vector();
            //VWSLog.err(" param:" + param);
            DoDBAction.get(getClient(sid),  database, "VW_DRSSetRouteInfo", param, resultRouteInfo);
            int routeId = 0;
            if (resultRouteInfo != null){
            	routeId = resultRouteInfo.size()>0?Util.to_Number(resultRouteInfo.get(0).toString()):0;
            	//VWSLog.add(" routeId=" + routeId);
            }           
        return routeId;
    }*/
    public int setRouteInfo(int sid, RouteInfo route)
    { 
    	VWSLog.dbg("setRouteInfo method sid val--->" + sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	ResultSet rs  = null;
        OracleCallableStatement ocs = null;
        CallableStatement cs = null;
        Connection cn = null;
        PreparedStatement  ps = null;
        BufferedReader reader = null;
        int ret = -1;
        try
        {
        	String actionId = route.getRouteActionId();
        	String actionLocation = "";
        	try{
        		VWSLog.dbg("setRouteInfo inside try block --->" );
        		if(actionId!=null && !("").equals(actionId) && ("1").equals(actionId) && route.getIndexId() != -1){
        			// This is to avoid adding \42 for locaiton having indexName like 168\42 (ie should not be like 168\42\42)
        			if(!route.getActionLocation().contains("\\"))
        				actionLocation = route.getActionLocation()+"\\"+route.getIndexId();
        			else
        				actionLocation = route.getActionLocation();
        		}else if(actionId!=null && !("").equals(actionId) && ("1").equals(actionId) && route.getIndexId() == -1){
        			//appending with slash zero for move location old functionality
        			if(!route.getActionLocation().contains("\\"))//This is to avoid adding \0 for locaiton having indexName like 168\42
        				actionLocation = route.getActionLocation() +"\\0";
        			else
        				actionLocation = route.getActionLocation();
        		}else{//Code added for Routing - ActionLocation for action id 2,3 and 4 not saved to database on create/update of routing
        			actionLocation = route.getActionLocation();
        		}
        	}catch(Exception ex){
        		//VWSLog.err("Exception while getting the action location :: "+ex.getMessage());
        		VWSLog.dbg("setRouteInfo inside catch block --->" );
        	}
            cn = database.aquireConnection();
            String param = route.getRouteId() + Util.SepChar + 
			route.getRouteName() + Util.SepChar +
			route.getStartAction() + Util.SepChar +
			route.getStartLocation() + Util.SepChar +
			route.getRouteActionId() + Util.SepChar +
			actionLocation + Util.SepChar+ // Modified for the enhancement to create the folder with index value
			route.getRouteDescription() + Util.SepChar+route.getRejectLocation()+Util.SepChar+route.getEmailOriginator()+Util.SepChar+route.getCompressdoc()+Util.SepChar+route.getPdfConversion();
            //Old functionality - Commenting last three parameters for Route enhancement
            /*route.getReportLocation() + Util.SepChar+
            route.getSendEmailOnRptGen() + Util.SepChar+
            route.getEmailIDs() + Util.SepChar;*/
            
            String image = route.getRouteImage();
        	Reader stringReader = new StringReader(image.toString());
        	VWSLog.dbg("VW_DRSSetRouteInfo parameter :"+param);
            if (database.getEngineName().equalsIgnoreCase("Oracle"))
            {
                CLOB clob = getClob(image, cn);//getClob(reader, cn);
                param = param + Util.SepChar;
                ocs = (OracleCallableStatement) cn.prepareCall
                                       ("{call VW_DRSSetRouteInfo(?,?,?)}");
                ocs.setString(1, param);
                ocs.setCLOB(2, clob);
                ocs.registerOutParameter(3, OracleTypes.CURSOR);
                boolean flag = ocs.execute();
                VWSLog.dbg("is VW_DRSSetRouteInfo procedure excuted :"+flag);
                rs = (ResultSet) ocs.getObject(3);
                clob.freeTemporary();
                if (rs != null && rs.next())
                {
                    ret = rs.getInt("Fld");
                    VWSLog.dbg("return value :"+ret);
                }
            }
            else
            if (database.getEngineName().equalsIgnoreCase("SQLServer"))
            {
            	/**
            	 * Code modified for the script changes for 2008 sqljdbc - query splited and excuted through DoDBAction.
            	 * Vijaypriya.B.K - 28 May 2009
            	 */
            	Vector resultRouteInfoInsert = new Vector();
            	DoDBAction.get(getClient(sid),  database, "VW_DRSSetRouteInfo", param, resultRouteInfoInsert);
            	if(resultRouteInfoInsert!=null && resultRouteInfoInsert.size()>0){
            		String[] resultArray = null;
            		String ret1 = resultRouteInfoInsert.get(0).toString();
            	    ret = Integer.parseInt(ret1);
            	
            	    // For updating image.
            	    ps = cn.prepareStatement("Update RouteInfo Set RouteImage = ? Where id = ?");
                    ps.setCharacterStream(1, stringReader, image.length());
                    ps.setInt(2, ret);
                    ps.execute();
            	}
            	
            }
            
            // Route functionality - adding email options
            Vector resultRouteInfoInsert = new Vector();
        	if(ret>0){
                route.setRouteId(ret);
                Vector vwEmailOptionsInfoList = (Vector)route.getRouteEmailOptionsInfoList();
                Vector resultDelRouteEmail = new Vector();
                VWEmailOptionsInfo vwEmailOptionsInfo = null;
                
                String delParam = route.getRouteId()+Util.SepChar;
            	//Delete if any row exist.
            	DoDBAction.get(getClient(sid), database, "VW_DRSDelEmailOption", delParam, resultDelRouteEmail);
            	
                if(vwEmailOptionsInfoList!=null && vwEmailOptionsInfoList.size()>0){
                	
                	for(int i=0; i<vwEmailOptionsInfoList.size(); i++){
                		vwEmailOptionsInfo = new VWEmailOptionsInfo();
                		vwEmailOptionsInfo = (VWEmailOptionsInfo)vwEmailOptionsInfoList.get(i);
                		String paramStr = "";
                		paramStr = route.getRouteId()+Util.SepChar
                				   +route.getReportLocation()+Util.SepChar
                				   +route.getSendEmailOnRptGen()+Util.SepChar
                				   +vwEmailOptionsInfo.getUserEmailAddress()+Util.SepChar
                				   +vwEmailOptionsInfo.getDocumentMetaDataFlag()+Util.SepChar
                				   +vwEmailOptionsInfo.getRouteSummaryReportFlag()+Util.SepChar
                				   +vwEmailOptionsInfo.getDocumentPagesFlag()+Util.SepChar
								   /**CV2019 code merges from SIDBI line**/
                				   +vwEmailOptionsInfo.getDocumentVwrFlag()+Util.SepChar;
                		DoDBAction.get(getClient(sid), database, "VW_DRSSetEmailOption", paramStr, resultRouteInfoInsert);
                	}
                }else{
                	String paramStr = route.getRouteId()+Util.SepChar
 				   					  +route.getReportLocation()+Util.SepChar
 				   					  +route.getSendEmailOnRptGen()+Util.SepChar
 				   					  +"-"+Util.SepChar
 				   					  +0+Util.SepChar
 				   					  +0+Util.SepChar
 				   					  +0+Util.SepChar
									  /**CV2019 code merges from SIDBI**/
 				   					  +0+Util.SepChar;
                	DoDBAction.get(getClient(sid), database, "VW_DRSSetEmailOption", paramStr, resultRouteInfoInsert);
                }
        	}
            
            // Close the resultset in outside of the if statement. In 'if' statement it will close the result set if it has atleast one record.
            if (rs != null)  { rs.close(); rs = null;}
            if (cs != null)  { cs.close(); cs = null;}
            if (ps != null)  { ps.close(); ps = null;}
            if (ocs != null) { ocs.close(); ocs = null;}
            cn.commit();
        }
        catch(Exception e)
        {
            VWSLog.err(e.getMessage());
        }
        finally
        {
        	if (reader != null) try{reader.close();} catch(IOException ioe){}
            assertConnection(cn);
        }
    	return ret;
    }
    public int setRouteTaskInfo(int sid, RouteTaskInfo routeTaskInfo)
    {
    	VWSLog.dbgCheck("setRouteTaskInfo method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	//Routeid, TaskSequence, TaskName, TaskDesc, TaskLength, Approvers, OnRejectAction
    	String acceptLabel = (routeTaskInfo.getAcceptLabel().trim().length() == 0 ? VWCConstants.DEFAULT_ACCEPT_LABEL : routeTaskInfo.getAcceptLabel());
    	String rejectLabel = (routeTaskInfo.getRejectLabel().trim().length() == 0 ? VWCConstants.DEFAULT_REJECT_LABEL : routeTaskInfo.getRejectLabel());
    	/**SIDBI - To get email to previous task users details**/
		int emailToPrvTaskUsers = routeTaskInfo.getEmailToPrvTaskUsers();
        
    	int approverCount = routeTaskInfo.getApproversCount();
    	
    	String param = routeTaskInfo.getRouteId()+Util.SepChar+
        				routeTaskInfo.getTaskSequence()+Util.SepChar+
        				routeTaskInfo.getTaskName()+Util.SepChar+
        				routeTaskInfo.getTaskDescritpion()+Util.SepChar+
        				routeTaskInfo.getTasklength()+Util.SepChar+
        				approverCount+Util.SepChar+
        				routeTaskInfo.getOnRejectTaskId()+Util.SepChar+
        				routeTaskInfo.getTaskFigureId()+Util.SepChar+
        				routeTaskInfo.getAcceptSignatureId()+Util.SepChar+
        				routeTaskInfo.getTaskReminder()+Util.SepChar+
        				routeTaskInfo.getTaskReminderEnabled()+Util.SepChar+
        				acceptLabel+Util.SepChar+
        				rejectLabel+Util.SepChar+emailToPrvTaskUsers+Util.SepChar;

          	Vector resultRouteTaskInfo = new Vector();
          	//VWSLog.add(" setRouteTaskInfo:" + param);
            DoDBAction.get(getClient(sid),  database, "VW_DRSSetRouteTask", param, resultRouteTaskInfo);
            int routeTaskId = 0;
            if (resultRouteTaskInfo != null){
            	routeTaskId = resultRouteTaskInfo.size()>0?Util.to_Number(resultRouteTaskInfo.get(0).toString()):0;
            	//VWSLog.add(" routeTaskId=" + routeTaskId);
            }           
    	return routeTaskId;
    	
    }
    public int setRouteIndexInfo(int sid, RouteIndexInfo routeIndexInfo )
    {
      	VWSLog.dbgCheck("setRouteIndexInfo method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	//Routeid, Indexid, ComparisonOper, IndexValue1, IndexValue2, LogicalOper
        String param = routeIndexInfo.getRouteId()+Util.SepChar+
        	routeIndexInfo.getIndexId()+Util.SepChar+
        	routeIndexInfo.getIndexName()+Util.SepChar+        	
        	routeIndexInfo.getComparisonOperator()+Util.SepChar+
        	routeIndexInfo.getIndexValue1()+Util.SepChar+
        	routeIndexInfo.getIndexValue2()+Util.SepChar+
        	routeIndexInfo.getLogicalOperator()+Util.SepChar+
        	routeIndexInfo.getDocTypeId()+Util.SepChar;
          	Vector resultRouteIndexInfo = new Vector();
            //VWSLog.add(" setRouteIndexInfo:" + param);
            DoDBAction.get(getClient(sid),  database, "VW_DRSSetRouteIndexInfo", param, resultRouteIndexInfo);
    	return NoError;
    }
    
    public int deleteDocumentUsers(int sid, int routeId)
    {
    	VWSLog.dbgCheck("deleteDocumentUsers method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(routeId);
        	Vector resultRouteInfo = new Vector();
            DoDBAction.get(getClient(sid),  database, "VW_DRSDelRouteUsers", param, resultRouteInfo);            
        return NoError;
    }
 
    public int deleteRouteInfo(int sid, int routeId, int isRouteNotUsed)
    {
    	VWSLog.dbgCheck("deleteRouteInfo method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        try{
        	Vector name = new Vector();
        	String routeName = "";
        	int ret = getNFNodeName(sid, routeId, VNSConstants.module_Route, name);
        	if(name!=null && name.size()>0){
        		routeName = name.get(0).toString();
        	}
        	message = resourceManager.getString("WORKFLOW_NAME") +" '"+routeName+"' "+resourceManager.getString("RamPageMessage.Deleted");
        }catch(Exception ex){
        }
        //We need to verify this 'addNotificationHistory' will call before the delete route or not
        try{
        	addNotificationHistory(sid, routeId, VNSConstants.nodeType_Route, VNSConstants.notify_Route_Delete, getClient(sid).getUserName(), "-", "-", message);
        }catch(Exception ex){}

        String param = String.valueOf(routeId)+Util.SepChar+isRouteNotUsed+Util.SepChar;
    	Vector resultRouteInfo = new Vector();
        DoDBAction.get(getClient(sid),  database, "VW_DRSDelRouteInfo", param, resultRouteInfo);
        
        return NoError;
    }
 
    public int deleteRouteTaskUserIndexInfo(int sid, int routeId, int markAsDelete, int indexBaseRoute)
    {
    	VWSLog.dbgCheck("deleteRouteTaskUserIndexInfo method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(routeId)+Util.SepChar+
        				markAsDelete+Util.SepChar+indexBaseRoute+Util.SepChar;
        	Vector resultRouteInfo = new Vector();
            DoDBAction.get(getClient(sid),  database, "VW_DRSUpdateRouteInfo", param, resultRouteInfo);            
        return NoError;
    }
    public int isUserInRoute(int sid, int principalId, Vector resultRouteInfo){
    VWSLog.dbgCheck("isUserInRoute method sid val--->"+sid);
	int routeExistsCount = 0;
	if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(principalId)+ Util.SepChar;        
        DoDBAction.get(getClient(sid),  database, "VW_DRSIsUserInRoute", param, resultRouteInfo);
        if (resultRouteInfo != null){
            routeExistsCount = resultRouteInfo.size()>0?Util.to_Number(resultRouteInfo.get(0).toString()):0;
        }         
	return routeExistsCount;
    }
    public int isRouteInUse(int sid, int routeId, int mode)
    {
    	 VWSLog.dbgCheck("isRouteInUse method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(routeId)
        				+ Util.SepChar +
        				(mode==0?"Update":"Delete");
        	Vector resultRouteInfo = new Vector();
        	/* For Delete button return values are 0,1,2: 0 = new route, 1 = route once used, pending, 2 = route used and no pending 
        	 * 3 = route is linked with final action.
        	 * For update button return values are 0, 1, 2:	 0 = new route, 1 = route once used, pending, 2 = route used and no pending  
        	 * */
            DoDBAction.get(getClient(sid),  database, "VW_DRSIsRouteInUse", param, resultRouteInfo);
            if (resultRouteInfo != null){
            	routeId = resultRouteInfo.size()>0?Util.to_Number(resultRouteInfo.get(0).toString()):0;
            } 
            //VWSLog.add("routeId " + routeId);
        return routeId;
    }
    
    public int setDocTypeRoutes(int sid, int RouteId, int DocTypeId){
    	 VWSLog.dbgCheck("setDocTypeRoutes method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;   	
    	String param = RouteId + Util.SepChar +DocTypeId;
    	//VWSLog.add("VW_SetRouteDocTypes param " + param);
    	DoDBAction.get(getClient(sid),  database, "VW_DRSSetRouteDocTypes", param, new Vector());
    	
    	int retValue = 0;
    	String routeName = "";
    	String docTypeName = "";
    	try{
    		Vector ret = new Vector();
    		retValue = getNFNodeName(sid, RouteId, VNSConstants.module_Route, ret);
    		
    		if(ret!=null && ret.size()>0)
    			routeName = ret.get(0).toString();
    		
    		ret = new Vector();
    		retValue = getNFNodeName(sid, DocTypeId, VNSConstants.module_DT, ret);
    		if(ret!=null && ret.size()>0)
    			docTypeName = ret.get(0).toString();
    		
    		message = resourceManager.getString("RamPageMessage.Assigned")+" "+ resourceManager.getString("WORKFLOW_NAME") +" '"+routeName+"' "+resourceManager.getString("RamPageMessage.ForDocType")+" '"+docTypeName+"'";
    		addNotificationHistory(sid, DocTypeId, VNSConstants.nodeType_DT, VNSConstants.notify_DT_AssignRoute, getClient(sid).getUserName(), "-", "-", message);
    	}catch(Exception ex){
    		
    	}
    	//VWSLog.err("routeId " + routeId);
    	return NoError;
    }
    
    public int setDocTypeRoutes(int sid, ArrayList RouteId, int DocTypeId){
    	try{
    		 VWSLog.dbgCheck("setDocTypeRoutes1 method sid val--->"+sid);
	    	if (!assertSession(sid)) return invalidSessionId;    	
	    	if (deleteAssignedDocTypeRoutes(sid, DocTypeId) != NoError) return Error;
	    	int selectedRouteId = 0;
	
	    	for(int index=0; index < RouteId.size(); index++){    		
	    		selectedRouteId = Util.to_Number(RouteId.get(index).toString());
	    		if (setDocTypeRoutes(sid, selectedRouteId, DocTypeId) != NoError) return Error;
	    	}
    	}catch(Exception ex){
    		return Error;
    	}
    	return NoError;
    }

    public int deleteAssignedDocTypeRoutes(int sid, int DocTypeId){
    	 VWSLog.dbgCheck("deleteAssignedDocTypeRoutes method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Vector docTypeRoutes = new Vector();
    	String param = ""+DocTypeId+Util.SepChar;
    	//VWSLog.add("VW_DelAssignedRoutes " + param);
    	DoDBAction.get(getClient(sid),  database, "VW_DRSDelAssignedRoutes", param, docTypeRoutes);
    	return NoError;
    }
    
    public int getDocTypeRoutes(int sid, int DocTypeId, Vector docTypeRoutes){
   	 	VWSLog.dbgCheck("getDocTypeRoutes method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = ""+DocTypeId+Util.SepChar;
    	//VWSLog.add("VW_GetAssignedRoutes " + param);
    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetAssignedRoutes", param, docTypeRoutes);
    	//VWSLog.err("VW_GetAssignedRoutes " + docTypeRoutes.toString());
    	return NoError;
    }
    //Duplicate call not in use.  
    public int setUserMail(int sid, int userId, String userMailId){
    	VWSLog.dbgCheck("setUserMail method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = userId+Util.SepChar+userMailId;
    	DoDBAction.get(getClient(sid),  database, "VW_SetUserMail", param, new Vector());
    	return NoError;
    }
    public int getLoggedInUserMailId(int sid, Vector loggedUserMailId){
    	VWSLog.dbgCheck("getLoggedInUserMailId method sid val--->"+sid);
    	Client client = getClient(sid);
    	String userName = client.getUserName();
    	int ret = getUserMail(sid, userName, loggedUserMailId);
    	if (ret<0)
    		return Error;
    	return NoError;
    }
    public int getUserMail(int sid, String userName, Vector userMailId){
    	VWSLog.dbgCheck("getUserMail method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = ""+userName+Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VW_GetUserMail", param, userMailId);
    	return NoError;
    }
    
    public int getAssignedRoutesOfDocType(int sid, int docTypeId, Vector assignedRoutesOfDocType){
    	VWSLog.dbgCheck("getAssignedRoutesOfDocType method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = ""+docTypeId+Util.SepChar;
    	//VWSLog.add(" VW_GetAssignedRoutes:"+param);
    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetAssignedRoutes", param, assignedRoutesOfDocType);
    	//VWSLog.add(" VW_GetAssignedRoutes:"+assignedRoutesOfDocType.toString());
    	return NoError;
    }
    
    public int setRouteHistory(int sid, int DocId, int routeId, int taskSeq, int routeUserId, String routeInitiatedBy, String approvedUserName, String authorizedUserName, int signId, int routeMasterId){
    	return setRouteHistory(sid, DocId, routeId, taskSeq, routeUserId, routeInitiatedBy, approvedUserName, authorizedUserName, signId, routeMasterId, "");
    }
    public int setRouteHistory(int sid, int DocId, int routeId, int taskSeq, int routeUserId, String routeInitiatedBy, String approvedUserName, String authorizedUserName, int signId, int routeMasterId, String groupName){
    	VWSLog.dbgCheck("setRouteHistory method sid val--->"+sid);
    	int returnValue = 0;
    	try {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	String status = Route_Status_Pending;
	    	if(taskSeq == 0)
	    		status = Route_Status_OnReject_ForOriginator;
	    	Client client = getClient(sid);
	    	if(routeInitiatedBy.equals(""))
	    		routeInitiatedBy	= 	client.getUserName();
	    	String param = 	client.getIpAddress()+Util.SepChar
							+DocId+Util.SepChar
	    					+routeId+Util.SepChar
	    					+taskSeq+Util.SepChar
	    					+routeUserId+Util.SepChar
	    					+status+Util.SepChar
	    					+routeInitiatedBy+Util.SepChar
	    					+approvedUserName+Util.SepChar
	    					+authorizedUserName+Util.SepChar
	    					+ signId + Util.SepChar
	    					+ routeMasterId + Util.SepChar
	    					+ groupName + Util.SepChar
	    					+ "0" + Util.SepChar;
	    	VWSLog.dbg("VW_SetRouteHistory param:"+param);
	    	returnValue = DoDBAction.get(getClient(sid),  database, "VW_DRSSetRouteHistory", param, new Vector());
    	} catch (Exception e) {
    		VWSLog.add("Exception in setRouteHistory :: "+e.getMessage());
    	}
    	return returnValue;
    }

    public int getToDoRouteList(int sid, int adminUser, int routeId, Vector toDoRouteList){
    	VWSLog.dbgCheck("getToDoRouteList method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	/*
    	 * Code optimization - found during the time of task user escalation enhancement
    	 * routeId - will be 0 for getting all the documents in all routes,
    	 * 			 will be greater than 0 for getting documents for selected route.
    	 */
    	String param = 	client.getUserName() + Util.SepChar + adminUser + Util.SepChar + routeId;
    	if((client.getClientType()==Client.WebAccess_Client_Type)||
    			(client.getClientType()==Client.Custom_Label_WebAccess_Client_Type)||
    			(client.getClientType()==Client.Professional_WebAccess_Client_Type) || 
    			(client.getClientType()==Client.Concurrent_WebAccess_Client_Type) || 
    			(client.getClientType()==Client.Professional_Conc_WebAccess_Client_Type) ||
    			(client.getClientType()==Client.NamedOnline_WebAccess_Client_Type)){
    		VWSLog.dbg("Before calling procedures.........");
        	DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteHistoryWS", param, toDoRouteList);
        	VWSLog.dbg("VW_DRSGetRouteHistoryWS resultset :"+toDoRouteList);
        	Vector temp = new Vector();
        	DoDBAction.get(getClient(sid),  database, "VW_DRSGetOriginatorWS", param, temp);
        	VWSLog.dbg("VW_DRSGetOriginatorWS resultset :"+temp);
        	toDoRouteList.addAll(temp);
    	}else{
    		
    		DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteHistory", param, toDoRouteList);
    		Vector temp = new Vector();
    		
    		DoDBAction.get(getClient(sid),  database, "VW_DRSGetOriginator", param, temp);
    		toDoRouteList.addAll(temp);
    	}
    	
    	//VWSLog.dbg("Rampage server ret vect value of Getorginator+RouteHistory:::"+toDoRouteList);
    	//VWSLog.err(" toDoRouteList "+temp.toString()+":"+temp.size());
    	return NoError;
    }
    //Desc   :Get Route Completed Documents
    //Author :Nishad Nambiar
    //Date   :01-Aug-2008
    /**
     * To get completed documents for routeId. As it is used in AdminWise module only, we are sending 1 for adminUser param
     * Code optimized during task escalation enhancement
     */
    public int getRouteCompletedDocuments(int sid, int routeId, int adminUser, Vector toDoRouteList){
    	VWSLog.dbgCheck("getRouteCompletedDocuments method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	routeId + Util.SepChar + adminUser + Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteCompletedDocs", param, toDoRouteList);
    	return NoError;
    }
    //This method return documents which is completed by all task and waiting for process final Action
    public int getTaskCompletedDocuments(int sid, int routeId, Vector toDoRouteList){
    	VWSLog.dbgCheck("getTaskCompletedDocuments method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	routeId + Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetTaskCompletedDocs", param, toDoRouteList);
    	return NoError;
    }    
    
    //Desc   :Check user exists in any routes.
    //Author :Nishad Nambiar
    //Date   :01-Aug-2008
    public int isUserExistsInRoute(int sid, int userId){
    	VWSLog.dbgCheck("isUserExistsInRoute method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param =   ""+userId;          
        Vector result = new Vector();
        DoDBAction.get(getClient(sid),  database, "VW_DRSChkUserExist", param, result);
        int ret = 0;
        if (result != null && result.size() > 0)
        	ret = Util.to_Number(result.get(0).toString());
        return ret;
    }
    /*public int routeLockDocument(int sid, int did, String currentRouteUser)
    {
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        Vector ret = new Vector();
        int routeLockType = 2;
        String param = Util.GetFixedString(did, 10) +
                       Util.GetFixedString(currentRouteUser, 50) +
                       Util.GetFixedString(client.getIpAddress(), 20)+
                       Util.GetFixedString(routeLockType, 10);
        
        DoDBAction.get(getClient(sid),  database, "DocumentLock", param , ret);
        
        if( ret.size() == 0 )   return LockDenied;
        int iret = Util.to_Number( (String) ret.get(0) );
        if(iret == 0)            return documentAlreadyLocked;
        if(iret < 0)             return DocumentDeleted;
        return NoError;
    }
    /*public int routeUnlockDocument(int sid, int did, String routeUser)
    {
        if (!assertSession(sid)) return invalidSessionId;
        Vector ret = new Vector();
        String param ="";
        param = did+Util.SepChar+routeUser;
        //VWSLog.add("param unLock "+param);
        DoDBAction.get(getClient(sid),  database, "VW_RouteDocumentUnLock", param , ret);
        return NoError;
    }*/
    public int updateRouteHistory(int sid, int docId, int routeId, int routeUserId, String status, String comments, String prevStatus, int routeMasterId,int routeHistoryId,String approvedUserName, String authorizedUserName, int signId){
    	VWSLog.dbgCheck("updateRouteHistory method sid val--->"+sid);
    	int returnValue = 0;
    	try {
	    	//VWSLog.add("routHistoryId inside updateRouteHistory::::"+routeHistoryId);
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	comments = comments.replaceAll("\t", " ");
	    	String param = 	client.getIpAddress()+Util.SepChar
	    					+docId+Util.SepChar
	    					+routeId+Util.SepChar	
	    					+((status.equalsIgnoreCase("Reviewed") && prevStatus.equalsIgnoreCase("Rejected, "+ WORKFLOW_MODULE_NAME +" completed") )?0:routeUserId)+Util.SepChar
	    					+status+Util.SepChar
							+comments.trim()+Util.SepChar
							+prevStatus+Util.SepChar
							+routeMasterId+Util.SepChar
							+approvedUserName+Util.SepChar
							+authorizedUserName+Util.SepChar
	    					+ signId +Util.SepChar+routeHistoryId+Util.SepChar;
	    	
	    	VWSLog.dbg(" VW_UpdateRouteHistory "+param);
	    	returnValue = DoDBAction.get(getClient(sid),  database, "VW_DRSUpdateRouteHistory", param, new Vector());
    	} catch (Exception e) {
    		VWSLog.err(" Exception in updateRouteHistory "+e.getMessage());
    	}
    	return returnValue;
    }    
    public int getDocsToAutoRoute(int sid, Vector documentsToRouteList){
    	VWSLog.dbgCheck("getDocsToAutoRoute method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	String param = 	"" + Util.replaceSplCharFromTempTable(client.getIpAddress());
    	DoDBAction.get(getClient(sid),  database, "VW_DRSAutoRoute", param, documentsToRouteList);
    	//VWSLog.err(" VW_AutoRoute "+documentsToRouteList.toString());
    	return NoError;
    }
    //To Validate Task Dynamic approvers are part of Document Indices or not
    public int validateRouteDocument(int sid, int routeId, int docId, Vector resultVector){
    	VWSLog.dbgCheck("validateRouteDocument method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	String param = 	routeId + Util.SepChar +
    					docId + Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VW_DRSValidateRouteDocument", param, resultVector);
    	//VWSLog.err(" VW_AutoRoute "+documentsToRouteList.toString());
    	return NoError;
    }
    //To apply final action
    public int getDocsForFinalAction(int sid, Vector routeCompletedDocs){
    	VWSLog.dbgCheck("getDocsForFinalAction method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	"";
    	DoDBAction.get(getClient(sid),  database, "VW_DRSFinalAction", param, routeCompletedDocs);
    	return NoError;
    }
    public synchronized int getDocumentsInRoute(int sid, int docId, Vector docsInRoute, int sourceType){
    	VWSLog.dbgCheck("getDocumentsInRoute method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	""+docId+Util.SepChar+sourceType+Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetDocsInRoute", param, docsInRoute);
    	VWSLog.dbg("VW_DRSGetDocsInRoute return value :"+docsInRoute);
    	return NoError;
    }
    public int updateRouteCompletedDocs(int sid, int routeMasterId, String taskFlag){
    	VWSLog.dbgCheck("updateRouteCompletedDocs method sid val--->"+sid);
    	try {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	String param = 	routeMasterId+Util.SepChar+taskFlag+Util.SepChar;
	    	DoDBAction.get(getClient(sid),  database, "VW_DRSUpdateRouteMaster", param, new Vector());
    	} catch (Exception e) {
    		VWSLog.err("Exception in updateRouteCompletedDocs procedure call...."+e.getMessage());
    		return Error;
    	}
    	return NoError;
    }
    //Functiona for mail functionality
    /*public int sendMail(String room, int sid, int userID, String docName, String routeName, Vector UserInfo) throws RemoteException{
    	 if (!assertSession(sid)) return invalidSessionId;
    	 int res=0;
    	 try{
    		 Vector userInfo=new Vector();
    		 String param=String.valueOf(userID);
    		 DoDBAction.get(getClient(sid),  database, "VW_GetUserMail", param, userInfo);
    		 StringTokenizer stUserInfo=new StringTokenizer(String.valueOf(userInfo.get(0)));
    		 String userName=String.valueOf(stUserInfo.nextToken());
    		 String userEmailId=String.valueOf(stUserInfo.nextToken());
    		 VWMail obj=new VWMail();
    	 	 res = obj.sendMail(room, sid, userName, userEmailId, docName, routeName);
    	 }catch(Exception e){
    	//	 	VWSLog.add(e.toString());
    	 }
    	return res;
    }*/
    public int getMailId(int sid, String userName, Vector userInfo){
    	VWSLog.dbgCheck("getMailId method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	try{
     		 DoDBAction.get(getClient(sid),  database, "VW_GetUserMail", userName, userInfo);
      		 //String userEmailId=String.valueOf(userInfo.get(0));
    	}catch(Exception e){
    		return Error;
    	}
    	return NoError;	 
    }
    
    public int getDocumentMetaDataInfo(int sid, String docId, Vector documentInfo){
    	VWSLog.dbgCheck("getDocumentMetaDataInfo method sid val--->"+sid);
    	if(!assertSession(sid)) return invalidSessionId;
    	try{
    		String param = docId + Util.SepChar;
    		DoDBAction.get(getClient(sid), database, "Node_GetDocInfo", param, documentInfo);
    	}catch(Exception ex){
    		return Error;
    	}
    	return NoError;
    }
    /*public int sendMailWithAttachment(String room, int sid, String userName, String docName, String routeName, 
    		int docId, Vector mailIds, Vector attachmentData, String status, Vector docNames, Vector emailOptions, String destPath,String clientType) throws RemoteException{
    	return sendMailWithAttachment(room, "", sid, userName, docName, routeName, docId, mailIds, attachmentData, status, docNames, emailOptions, destPath,clientType);
    }*/
    /**CV2019 - Added method overloading for sendMailWithAttachment after SIDBI & CV10.2 code merges**/
    public int sendMailWithAttachment(String room, int sid, String userName, String docName, String routeName, 
    		int docId, Vector mailIds, Vector attachmentData, String status, Vector docNames, Vector emailOptions, String destPath) throws RemoteException{
    	VWSLog.dbg("inside sendMailWithAttachment1...");
    	return sendMailWithAttachment(room, "", sid, userName, docName, routeName, docId, mailIds, attachmentData, status, docNames, emailOptions, destPath, "");
    }
    public int sendMailWithAttachment(String room, String serverName, int sid, String userName, String docName, String routeName, 
    		int docId, Vector mailIds, Vector attachmentData, String status, Vector docNames, Vector emailOptions, String destPath, String clientType) throws RemoteException{
    	VWSLog.dbg("inside sendMailWithAttachment2...");
    	return sendMailWithAttachment(room, serverName, sid, userName, docName, routeName, docId, mailIds, attachmentData, status, docNames, emailOptions, destPath, clientType, null);
    }
    public int sendMailWithAttachment(String room, String serverName, int sid, String userName, String docName, String routeName, 
    		int docId, Vector mailIds, Vector attachmentData, String status, Vector docNames, Vector emailOptions, String destPath,String clientType, String openDocUrl) throws RemoteException{if (!assertSession(sid)) return invalidSessionId;
    		VWSLog.dbg("inside sendMailWithAttachment3...");
    		int res=0;
    		String originator = null;
        	try{
        		/**CV2019 code merges from CV10.2 and SIDBI **/
        		Vector userInfo = null;
        		VWMail obj = new VWMail();
        		String mailSubject = null;
        		String subStatus = null;
        		File[] attachmentFile = null;
        		String webAccessReference = null;
    			String vwsHome = null;
    			String mailTo = null, mailCcTo = null;
    			String[] usersList = null, emailOptionsMailList = null;
    			VWSLog.dbg("docName before calling fixFileName..........."+docName);
    			VWSLog.dbg("routeName before calling fixFileName..........."+routeName);
        		docName = VWCUtil.fixFileName(docName);
    		  	routeName = VWCUtil.fixFileName(routeName);
    		  	VWSLog.dbg("docName after calling fixFileName..........."+docName);
    			VWSLog.dbg("routeName after calling fixFileName..........."+routeName);
    		  	VWSLog.dbg("userName :"+userName);
    			if (userName != null && userName.length() > 0)
    		  		usersList = userName.split(";");
    			VWSLog.dbg("sendMailWithAttachment status..........."+status);
        		VWSLog.dbg("sendMailWithAttachment mailIds..........."+mailIds);
        		VWSLog.dbg("sendMailWithAttachment userName..........."+userName);
        		VWSLog.dbg("sendMailWithAttachment usersList..........."+usersList);
        		//This condition will be true only in final action.
        		//mailIds from ARS contains value. mailIds from workflow contains null for all accept/reject/ended task
        		if (mailIds != null && mailIds.size() > 0 
        				&& !status.equalsIgnoreCase(Route_Status_Notify_OnVwrRptGen)) { 
        			VWSLog.dbg("inside if condition ......");
        			mailTo = mailIds.get(0).toString();
        		} else {// This will execute for all the workflow actions
        			//There is no notification for DRS Admin
        			VWSLog.dbg("inside else condition ......");
        			userInfo = new Vector();
        			if(userName!=null && userName.trim().equals("DWSAdmin")){
        				return NoError;
        			}
        			if(!status.startsWith("ARS")){
        				mailTo = ""; mailCcTo = "";
        				boolean sidbiCustomization = getSidbiCustomizationFlag(sid);
        				VWSLog.dbg("sidbiCustomization..........."+sidbiCustomization);
        				if (!sidbiCustomization) {
        					if (usersList != null && usersList.length > 0) {        						
        						for (int ursLstIndex = 0; ursLstIndex < usersList.length; ursLstIndex++) {
        							getMailId(sid, usersList[ursLstIndex], userInfo);
		        					VWSLog.dbg("getMailId userInfo "+userInfo);
									if (userInfo != null && userInfo.size() > 0) {
										mailTo = mailTo + "," +String.valueOf(userInfo.get(0));
									}
									userInfo.removeAllElements();
        						}
    						}
        				} else {
        					// For sidbi customization
        					// If condition is for final action VWR documents(SIDBI)
        					if (status.equalsIgnoreCase(Route_Status_Notify_OnVwrRptGen) || status.endsWith("Ended")) {
        						originator = "<originator>";
        					}
        					if (usersList != null && usersList.length > 0) {
        						if (status.equalsIgnoreCase(Route_Status_Pending)) {
	        						for (int ursLstIndex = 0; ursLstIndex < usersList.length; ursLstIndex++) {
	        							getMailId(sid, usersList[ursLstIndex], userInfo);
			        					VWSLog.dbg("getMailId userInfo "+userInfo);
										if (userInfo != null && userInfo.size() > 0) {
											mailTo = mailTo + "," +String.valueOf(userInfo.get(0));
										}
										userInfo.removeAllElements();
	        						}
        						}
        					}
    						if (usersList != null && usersList.length > 0)
    							userName = usersList[0];
    						else
    							userName = "";
    						getMailIdForDRS(sid, userName, docId, userInfo, originator);
        					if (userInfo != null && userInfo.size() > 0) {
        						VWSLog.dbg("getMailIdForDRS return value......"+userInfo);
        						for (int mailLstIndex = 0; mailLstIndex < userInfo.size(); mailLstIndex++) {
									VWSLog.dbg("Mail available in the mailTo/mailCcTo List : "+(!mailCcTo.contains(userInfo.get(mailLstIndex).toString()) && !mailTo.contains(userInfo.get(mailLstIndex).toString())));											
									if((!status.equalsIgnoreCase(Route_Status_Pending)) && mailLstIndex == 0){
										mailTo = String.valueOf(userInfo.get(mailLstIndex));
									} else {
										if (!mailCcTo.contains(userInfo.get(mailLstIndex).toString()) 
												&& !mailTo.contains(userInfo.get(mailLstIndex).toString())) {
											mailCcTo = mailCcTo + "," +String.valueOf(userInfo.get(mailLstIndex));
										}
									}
								}
        					}
        					VWSLog.dbg("MailTo 2..........."+mailTo);
        					VWSLog.dbg("mailCcTo 1..........."+mailCcTo);
    						// This condition is for work-flow final action. mailIds will contain end property's VWR flag selected email options list
    						if (mailIds != null && mailIds.size() > 0 && status.equalsIgnoreCase(Route_Status_Notify_OnVwrRptGen)) {
    							emailOptionsMailList = mailIds.get(0).toString().split(",");
    							for (int emailLstIndex =0; emailLstIndex < emailOptionsMailList.length; emailLstIndex++) {
    								VWSLog.dbg(" Mail available in the mailTo/mailCcTo List : "+(!mailCcTo.contains(emailOptionsMailList[emailLstIndex]) && !mailTo.contains(emailOptionsMailList[emailLstIndex])));
                					if (!mailCcTo.contains(emailOptionsMailList[emailLstIndex]) && !mailTo.contains(emailOptionsMailList[emailLstIndex])) {
                						mailCcTo = mailCcTo + "," + emailOptionsMailList[emailLstIndex];
                					}	
    							}            					
            				}
    						VWSLog.dbg("mailCcTo 2..........."+mailCcTo);
    						if (mailTo.trim().length() == 0 && mailCcTo.trim().length() > 0)
								mailTo = mailCcTo;
    						
    						if (status.equalsIgnoreCase(Route_Status_Notify_OnVwrRptGen)) {
    							status = Route_Status_Notify_OnRptGen;
    						}        					
        				}        				
        				if (mailTo.startsWith(",")) mailTo = mailTo.substring(1, mailTo.length());
        				if (mailCcTo.startsWith(",")) mailCcTo = mailCcTo.substring(1, mailCcTo.length());
        			}
        			VWSLog.dbg("mailTo..........."+mailTo);
        			VWSLog.dbg("mailCcTo..........."+mailCcTo);
        		}
        		if (mailTo == null || (mailTo != null && mailTo.trim().length() == 0)) mailTo = userName;       		
        		
        		subStatus = "";
        	
        		if(status.equals(Route_Status_Pending)){
        			mailSubject ="Route_Status_Pending";
        			//PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.DocIsPending1")+" "+"'"+docName+"'"+VWSConstants.EMAIL_SUBJECT;
        		}else if(status.equals(Route_Status_Reject)){
        			mailSubject ="Route_Status_Reject" ;
        			//PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.RouteRej1")+" "+"'"+docName+"'"+" "+resourceManager.getString("VWMailMsg.RouteRej2")+" "+resourceManager.getString("VWMailMessage.Reject");
        		}else if(status.equals(Route_Status_End)||status.equals(Route_Status_Approve_End)||status.equals(Route_Status_PSR_End)){
        			mailSubject = "Route_Status_End";
        			//resourceManager.getString("VWMailMessage.EndWorkFlow")+" "+resourceManager.getString("VWMailMsg.RouteEnd1")+" "+ PRODUCT_NAME+" "+resourceManager.getString("VWMailMsg.RouteEnd2")+" "+"'"+docName+"'";
        		}else if(status.equals(Route_Status_Approve)){
        			mailSubject = "Route_Status_Approve";
        			//PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.Approve1")+" "+"'"+docName+"'" +" "+resourceManager.getString("VWMailMsg.Approve2")+" "+resourceManager.getString("VWMailMessage.Approved");
        		}else if(status.equalsIgnoreCase(Route_Status_Notify_OnRptGen)){
        			mailSubject="Route_Status_Notify_OnRptGen";
        			//mailSubject = PRODUCT_NAME + " - \""+docName+"\"";
        		}else if(status.startsWith("ARS")){
        			subStatus = status.substring(3,status.length());
        			if(subStatus.equalsIgnoreCase("6"))
        				mailSubject = PRODUCT_NAME + " "+resourceManager.getString("RamPageMessage.ARSummary")+" "+"\""+routeName+"\"";// passing retentionName in the parameter of routeName
        			else
        				mailSubject = PRODUCT_NAME +" "+ resourceManager.getString("RamPageMessage.ARNotify")+" "+"\""+routeName+"\"";
        		}
        		else 
        			mailSubject = PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.Document")+" "+"'"+docName+"'"+VWSConstants.EMAIL_SUBJECT;
        		
        		webAccessReference = "";
    			vwsHome = VWSUtil.getHome();
    			attachmentFile = new File[3];
    			if( attachmentData == null || (attachmentData != null && attachmentData.size()==0) ){
        			//subStatus will be 1 or 6 for ARS
        			if(subStatus == "" ){// This block of code is for DRS and not for ARS
    	    			attachmentFile[0] = new File(vwsHome +Util.pathSep+"lib"+Util.pathSep+"document.vwr");
    	    			try{
    	    				FileOutputStream fout=new FileOutputStream(attachmentFile[0]);
    	    				String line="vwrp://"+room+"/"+docId;
    	    				fout.write(line.getBytes());
    	    				fout.close();
    	    			}catch(Exception exception){
    	    				//VWSLog.add("Error in generating document.vwr file.");
    	    				return Error;
    	    			}
    	    			String server = null;
    	    			try {
    	    				/**This part is for new web-access**/
    	    				VWSLog.dbg("openDocUrl......."+openDocUrl);
    	    				if (openDocUrl != null && openDocUrl.trim().length() > 0) {
	    	    				try{
									 server = openDocUrl+"&roomName="+room+"&documentId="+docId;//VWSPreferences.getWebAccessServerName();
									 
									 webAccessReference = server.replace("secureLinkLogin", "openDocumentLogin");
	//								 if(server == null || server.trim().length() == 0){
	//									 server = serverName;
	//								 }	    					
	//								 webAccessReference = getWebAccessReference(sid, server, room, docId);
									 VWSLog.dbg("webAccessReference sendMailWithAttachmentWS :::: "+webAccessReference);
									 webAccessReference = convertToSpecialChar(webAccessReference);
									 if(webAccessReference.contains(" ")){
										 webAccessReference = webAccessReference.trim().replaceAll(" ", "%20");
									 }
									 VWSLog.dbg("webAccessReference after conversion :::: "+webAccessReference);
								 }catch (Exception e) { 
									 
								 }
    	    				} else {
    	    					/**This part is for old web-access**/
    	    					server = VWSPreferences.getWebAccessServerName();
        	    				if(server == null || server.trim().length() == 0){
        	    					server = serverName;
        	    				}
        	    				//if(clientType.equals("cvweb")) {
        	    					
    	    					Vector secureEmailUrlVector=new Vector();
        	    				readSecureLinkUrl(sid, secureEmailUrlVector);
        	    				try {
    								 String cvWeburl = secureEmailUrlVector.get(0).toString()+"&roomName="+room+"&documentId="+docId;//VWSPreferences.getWebAccessServerName();
    								 webAccessReference = cvWeburl.replace("secureLinkLogin", "openDocumentLogin");
    								// VWSLog.add("webAccessReference sendMailWithAttachmentWS :::: "+webAccessReference);
    								 webAccessReference = convertToSpecialChar(webAccessReference);
    								 VWSLog.add("webAccessReference after conversion :::: "+webAccessReference);
    								 if(webAccessReference.contains(" ")){
										 webAccessReference = webAccessReference.trim().replaceAll(" ", "%20");
									 }
        	    				 } catch(Exception e1) {
        	    					 
        	    				 }

        	    				/*}else {    	    				
        	    				webAccessReference = getWebAccessReference(sid, server, room, docId);
        	    				//VWSLog.add("before sending mail webAccessReference :: "+webAccessReference);
        	    				}*/
    	    				}
    	    				
    	    			} catch (Exception e) {
    	    				
    	    			}
        			}
        		}else{
        			int compress = 0;
        			int convertopdf = 0;
        			String fileName = docId+"_"+routeName+"_"+ WORKFLOW_MODULE_NAME +"Summary.html";
        			String path = vwsHome +Util.pathSep+"Server_Generated_Files"+Util.pathSep+fileName;
        			//Here we have to decide for getting the file for html meta data and pages after publish
        			if(!status.startsWith("ARS")){//DRS part
        				VWSLog.dbg("emailOptions : "+emailOptions);
        				VWSLog.dbg("emailOptions size : "+emailOptions.size());
        				if(emailOptions!=null && emailOptions.size()>0){
        					try{
        						attachmentFile = new File[10];
        						int docVwr = Integer.parseInt(emailOptions.get(3).toString());// Document VWR
        						VWSLog.dbg("emailOptions.get(3) docVwr...."+docVwr + "status : "+status);
        						if (docVwr == 1) {
        							attachmentFile[0] = new File(vwsHome +Util.pathSep+"lib"+Util.pathSep+"document.vwr");
        	    	    			try{
        	    	    				mailSubject="Route_Status_OnEndVWR";
        	    	    				FileOutputStream fout=new FileOutputStream(attachmentFile[0]);
        	    	    				String line="vwrp://"+room+"/"+docId;
        	    	    				fout.write(line.getBytes());
        	    	    				fout.close();
        	    	    			}catch(Exception exception){
        	    	    				//VWSLog.add("Error in generating document.vwr file.");
        	    	    				return Error;
        	    	    			}
        						}else {
	        						//This emailOptions has the 3 values that we are going to get from the param of this functiona.
	        						int tempStr = Integer.parseInt(emailOptions.get(0).toString());//Document meta data
	        						VWSLog.dbg("metadata before send mail...."+tempStr);
	        						int summaryStr = Integer.parseInt(emailOptions.get(1).toString());
	        						VWSLog.dbg("summary report before send mail...."+summaryStr);
	        						String docMetaDataPath = destPath+"\\"+docId+"_"+routeName+"_MetaData.html";
	        						String docSummaryPath = destPath+"\\"+fileName;
	        						String mergedFilePath = null; 
	        						if (tempStr==1 && summaryStr==1) {
	        							mergedFilePath = destPath+"\\"+docId+"_"+routeName+"_MetaDataAndSummary.html";
	        							attachmentFile[0] = generateMergedReport(docMetaDataPath, docSummaryPath, mergedFilePath);
	        							attachmentFile[1] = null;
	        						} else {
										// Meta data
										if (tempStr == 1) {
											attachmentFile[0] = new File(docMetaDataPath);
										} else {
											attachmentFile[0] = null;
										}
										// Document Report Summary
										if (summaryStr == 1) {
											attachmentFile[1] = new File(docSummaryPath);
										} else {
											attachmentFile[1] = null;
										}
	        						}
	        						
	        						//Document Pages
	        						tempStr = Integer.parseInt(emailOptions.get(2).toString());
	        						VWSLog.dbg("document pages before send mail...."+tempStr);
	        						
	        						try {
	        							compress=Integer.parseInt(emailOptions.get(4).toString());
		        						convertopdf=Integer.parseInt(emailOptions.get(5).toString());
	        						} catch (Exception pdf) {
	        							VWSLog.dbg("Exception in compress and pdf convertion :"+pdf.getMessage());	
	        						}	        						
	        						VWSLog.dbg("compress before send mail...."+compress);
	        						VWSLog.dbg("converttopdf before send mail...."+convertopdf);
	        						if (tempStr == 1 && compress == 1) {
	        							//already published; only we need to create a file for that path.
	        							try{
	        								String zipFileName = docId+"_"+routeName+"_Zip.zip"; 
	        								VWSLog.dbg("zipFileName:::::::::"+zipFileName);
	        								String docPagesPath = destPath+"\\"+zipFileName;
	        								VWSLog.dbg("docPagesPath:::::::::"+docPagesPath);
	        								attachmentFile[2] = new File(docPagesPath);
	        							}catch(Exception ex){
	        							}
	        						} else if(tempStr==1&&convertopdf==1){
	        							//already published; only we need to create a file for that path.
	        							try{
	        								String pdfFileName = docId+"_"+routeName+".pdf"; 
	        								String docPagesPath = destPath+"\\"+pdfFileName;
	        								if(compress==0){
	        									attachmentFile[2] = new File(docPagesPath);
	        								}else if(compress==1){
	        									String zipFileName = docId+"_"+routeName+"_Zip.zip"; 
	        									String pdfZipPath = destPath+"\\"+zipFileName;
	        									attachmentFile[2] = new File(pdfZipPath);
	        								}
	        							}catch(Exception ex){
	        							}
	
									} else if (tempStr == 1 && compress == 0 && convertopdf == 0) {
	        							VWSLog.dbg("Inside else if ::::"+tempStr);
	        							String pagesPath=destPath+"\\"+"DocumentPages"+"\\"+docId;
	        							VWSLog.dbg("pagesPath :::: "+pagesPath);
	        							File[] extractedFile=Util.listAttachmentFiles(pagesPath);
	        							VWSLog.dbg("extractedFile size is"+extractedFile.length);
	        							List<String> extractedFileNameList=new ArrayList<String>();
	        							if(extractedFile!=null&extractedFile.length>0) {
	        								for(int i=0;i<extractedFile.length;i++) {
	        									if(extractedFile[i].isFile()) {
	        										if(!(extractedFile[i].getName().equals("Pages.xml"))) {
	        											extractedFileNameList.add(extractedFile[i].getName());
	        										}
	        									}
	        								}
	        								VWSLog.dbg("extractedFileNameList :::"+extractedFileNameList);
	        								File[] reportAttachement=new File[2];
	        								reportAttachement[0]=attachmentFile[0];
	        								reportAttachement[1]=attachmentFile[1];
	        								attachmentFile = new File[extractedFileNameList.size()+2];
	        								attachmentFile[0]=reportAttachement[0];
	        								attachmentFile[1]=reportAttachement[1];
	        								if(extractedFileNameList.size()>0&&extractedFileNameList!=null) {
	        									for(int j=0;j<extractedFileNameList.size();j++) {
	        										File generateFile=new File(pagesPath+"\\"+extractedFileNameList.get(j));
	        										attachmentFile[2+j]=generateFile;
	        									}
	        								}
	        							}
	
	        						}        						
	        						else{
	        							attachmentFile[2] = null;
	        						}
        						}
        					}catch(Exception ex){
        						//VWSLog.add("Exception in emailOptions check is : "+ex.getMessage());
        					}
        				}
        			}else{
        				attachmentFile[0] = new File(path);
        			}
        		}
        		//Added for ARS
        		String path = "";
        		if(status.startsWith("ARS")){
        			// Attachment is only for Generation of report and not for notify mails
        			if(subStatus.equalsIgnoreCase("6")){
        				// docName - is docTypeName
        				if(attachmentData!=null && attachmentData.size()>0)
        					path = attachmentData.get(0).toString();

        				/**
        				 * For Notifybefore mail there will be no attachment, only we are sending the mail body
        				 * for all final actions 
        				 */
        				if(!path.equals(""))
        					attachmentFile[0] = new File(path);
        			}else if (subStatus.equalsIgnoreCase("1")){
        				docName = routeName;
        			}
        		}
        		VWSLog.dbg("webAccessReference ::"+webAccessReference);
        		VWSLog.dbg("docid before getDocumentIndicesDetails :"+docId);
        		VWSLog.dbg("Before calling the getDocumentIndicesDetails....."+docId);
        		HashMap<String, String> indexDetails = getDocumentIndicesDetails(sid, docId);
        		VWSLog.dbg("after calling the getDocumentIndicesDetails....."+indexDetails);
				VWMail vwMail = new VWMail();
				sendMailThread thread = new sendMailThread(vwMail, null, mailTo, mailSubject, null, "WorkflowMail",
						attachmentFile, docName, status, docNames, routeName, webAccessReference, room, mailCcTo,
						indexDetails);  
				thread.start();
        	}catch(Exception e){
        		return Error;
        	}
        	VWSLog.dbg("return value from sendMailWithAttachment :"+res);
        	/**End of SIDBI & CV10.2 merges***/
        	return res;
        }
    
    private String getWebAccessReference(int sid, String server, String room, int docId) {
    	String webAccessReference = "";
		try{
			String webAccessURL = "";
			Vector result = new Vector();
			if(server != null && server.trim().length() > 0 && room != null && room.trim().length() > 0 && docId > 0){
				int ret = getWebAccessUrl(sid, result);
			}   		
    		
    		if(result.size() > 0){
    			webAccessURL = (String) result.get(0);
    			if(webAccessURL != null && webAccessURL.trim().length() > 0){
    				webAccessReference = webAccessURL.trim() + "?srv=" + server + "&room=" + room + "&id=" + docId;
    				webAccessReference = webAccessReference.replace(" ", "%20");
    			}    			
    		}
		}catch (Exception e) {
			//VWSLog.war("Exception while getting web access reference : "+e.getMessage());
		}
		return webAccessReference;
	}
    
	public int sendNotificationAlert(String room, int sid, Vector mailIds, Vector mailMessage, Vector attachmentPath, int status, int nodeType, int docId, int attachmentExist, Vector nodeName, int attempt) throws RemoteException{
    	String userEmailId = null;
    	String vwsHome = VWSUtil.getHome();
    	String mailSubject = "";
    	String nodeNameStr = "";
    	
    	VWMail obj=new VWMail();
    	File[] attachmentFile = new File[2];

    	if( mailIds!=null && mailIds.size()>0 ){
			userEmailId = mailIds.get(0).toString();
		}
    	
    	if(nodeName!=null && nodeName.size()>0){
    		nodeNameStr = nodeName.get(0).toString();
    	}
    	String mailTo = userEmailId;
    	switch(nodeType){
    		case VNSConstants.nodeType_Document:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.Documents");
    			break;
    		case VNSConstants.nodeType_Folder:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.Folders");
    			break;
    		case VNSConstants.nodeType_DT:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.DocTypes");
    			break;
    		case VNSConstants.nodeType_Storage:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.Storage");
    			break;
    		case VNSConstants.nodeType_AT:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+" "+resourceManager.getString("RamPageMessage.AduitTrail");
    			break;
    		case VNSConstants.nodeType_RecycledDocs:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.RecycleDocument");
    			break;
    		case VNSConstants.nodeType_RestoreDocs:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.ResotredDocument");
    			break;
    		case VNSConstants.nodeType_Redaction:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.Redactions");
    			break;
    		case VNSConstants.nodeType_Retention:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.Retention");
    			break;
    		case VNSConstants.nodeType_Route:
    			mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("RamPageMessage.ReportFor")+" "+ resourceManager.getString("WORKFLOW_NAME") +resourceManager.getString("RamPageMessage.S");
    			break;
    	}
    	
    	
    	String filePath = vwsHome +Util.pathSep+"lib"+Util.pathSep+"document.vwr";
    	String historyPath = "";
    	if(attachmentPath!=null && attachmentPath.size()>0)
    		historyPath = attachmentPath.get(0).toString();
    	
    	if(attempt==0){
    		getAttachmentVWRFile(filePath,room,docId);
    	}
    	File vwrFile;
    	switch(attachmentExist){
	    	case 10:
	    		vwrFile = new File(filePath);
	    		attachmentFile[0] = vwrFile;//getAttachmentVWRFile(filePath,room,docId);
	    		break;
	    	case 01:
	    		attachmentFile[1] = getAttachmentHistoryFile(historyPath);
	    		break;
	    	case 11:
	    		vwrFile = new File(filePath);
	    		attachmentFile[0] = vwrFile;//getAttachmentVWRFile(filePath,room,docId);
	    		attachmentFile[1] = getAttachmentHistoryFile(historyPath);
	    		break;
    		default:
    			break;
    	}
    	Client client = getClient(sid);
		Vector userInfo=new Vector();
		String fromMailID = null;
		getMailId(sid, client.getUserName(), userInfo);
		if(userInfo!=null&&userInfo.size()>0){
			fromMailID = String.valueOf(userInfo.get(0));
		}
		VWSLog.dbg("sendNotificationAlert fromMailID :: "+fromMailID);
    	int res = obj.sendNotificationMail(userEmailId, mailSubject, mailMessage, attachmentFile, status, nodeType, nodeNameStr, fromMailID, mailTo, null);
    	
    	
    	if(res>=0){
    		//After mail sent need to delete document.vwr file for Notification functionality same as DRS
    		File tempFile = new File(filePath);
    		try{
    			if(tempFile!=null && tempFile.exists()){
    				boolean deleted = tempFile.delete();
    			}
    		}catch(SecurityException se){
    			VWSLog.dbg("Notification - SecurityException while deleting document.vwr file is : "+se.getMessage());
    		}catch(Exception ex){
    			VWSLog.dbg("Exception while deleting document.vwr file");
    		}

    		//Attachment history file has to be deleted
    		tempFile = new File(historyPath);
    		try{
    			if(tempFile!=null && tempFile.exists()){
    				boolean deleted = tempFile.delete();
    			}
    		}catch(SecurityException se){
    			VWSLog.add("Notification - SecurityException while deleting document.vwr file is : "+se.getMessage());
    		}catch(Exception ex){
    			VWSLog.add("Exception while deleting document.vwr file");
    		}
    	}
    	switch(res){
    	case -1:
    		VWSLog.war("Error in Notification sending mail. Check mail server settings.");
    		break;
    	case -2:
    		VWSLog.war("Error in Notification sending mail. Please check the user(s) mail id.");
    		break;
    	}
    	return res;
    }
    
    public int sendEscalationMail(String room, int sid, RouteMasterInfo routeMasterInfo, String esclationUserEmailId, String messageStr){
		VWSLog.dbgCheck("sendEscalationMail method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	try{
    		VWMail obj = new VWMail();
    		String toEmailId = "";
    		
    		String subject = resourceManager.getString("WORKFLOW_NAME") +" "+resourceManager.getString("RamPageMessage.Document")+" '"+routeMasterInfo.getDocName()+"' "+resourceManager.getString("RamPageMessage.Escalated");
    		String fromEmailId = VWSPreferences.getFromEmailId();
    		
    		if(routeMasterInfo.getNotifyMail() != null && routeMasterInfo.getNotifyMail().length() > 0 && 
    				!routeMasterInfo.getNotifyMail().equals("-")){
    			toEmailId = routeMasterInfo.getNotifyMail();
    			if(esclationUserEmailId.length()>0 && !esclationUserEmailId.equals("-") && !toEmailId.contains(esclationUserEmailId)){
    				toEmailId = toEmailId+";"+esclationUserEmailId;
    			}
    		}else{
    			if(esclationUserEmailId.length()>0 && !esclationUserEmailId.equals("-")){
    				toEmailId = esclationUserEmailId;
    			}
    		}
    		
    		obj.sendEscalationMail(fromEmailId, toEmailId, subject, messageStr);
    		
    	}catch(Exception ex){
    		return Error;
    	}
    	return NoError;
    }

    private File getAttachmentHistoryFile(String savedHistoryPath) {
		File attachment = new File(savedHistoryPath);
		return attachment;
	}
	private File getAttachmentVWRFile(String filePath, String roomName, int docId) {
    	File attachment = new File(filePath);
    	try{
    		FileOutputStream fout=new FileOutputStream(attachment);
    		String line="vwrp://"+roomName+"/"+docId;
    		fout.write(line.getBytes());
    		fout.close();
    	}catch(Exception ex){
    		VWSLog.dbg("Exception in getAttachmentVWRFile : "+ex.getMessage());
    		return null;
    	}
    	return attachment;
	}
	public int getRouteSummary(int sid, int docId, int mode, Vector routeSummary){
    	// mode - 0 for todo list summary - only listed the last pending route summary
    	// mode - 1 for document list summary - listed the all route summary including completed route.
    	// mode - 2 for todolist - move originator record.
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	docId + Util.SepChar + 
    					mode;	
    	VWSLog.dbg("VW_DRSGetRouteSummary param:"+param);
    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteSummary", param, routeSummary);
    	//VWSLog.add(" routeSummary "+routeSummary.toString());
    	return NoError;
    }
	
	/**SIDBI - Method to get summary details****/
	public int getRouteNewSummary(int sid, int docId, int mode, Vector routeSummary){
    	// mode - 0 for todo list summary - only listed the last pending route summary
    	// mode - 1 for document list summary - listed the all route summary including completed route.
    	// mode - 2 for todolist - move originator record.
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	docId + Util.SepChar + mode+Util.SepChar;	
    	VWSLog.dbg(" NewRouteSummary param:"+param);
    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetNewRouteSummary", param, routeSummary);
    	//VWSLog.add(" routeSummary "+routeSummary.toString());
    	return NoError;
    }
    
    
    public int getDocumentAuditDetails(int sid, int docId, int mode, Vector auditDetails){
    	VWSLog.dbgCheck("getDocumentAuditDetails method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = String.valueOf(docId);
    	DoDBAction.get(getClient(sid),  database, "VW_AT_GetDocData", param, auditDetails);
    	return NoError;
    }

    public int isRouteNameFound(int sid, String routeName){
    	VWSLog.dbgCheck("isRouteNameFound method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	""+routeName;	
    	Vector result = new Vector();
    	DoDBAction.get(getClient(sid),  database, "VW_DRSIsRouteNameFound", param, result);
    	int ret = 0;
    	if (result != null && result.size() > 0){
    		ret = Util.to_Number(result.get(0).toString());
    	}
    	return ret;
    }

    public int isRouteLocationFound(int sid, String nodeId){
      	VWSLog.dbgCheck("isRouteLocationFound method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	nodeId;	
    	Vector result = new Vector();
    	DoDBAction.get(getClient(sid),  database, "VW_DRSIsRouteLocationFound", param, result);
    	int ret = 0;
    	if (result != null && result.size() > 0){
    		ret = Util.to_Number(result.get(0).toString());
    	}
    	//VWSLog.add("isRouteLocationFound " + ret);
    	return ret;
    }
    public int getApprovedCountForTask(int sid, int routeId, int routeTaskId, int docId, int routeUserId){
     	VWSLog.dbg("getApprovedCountForTask method sid val--->"+sid);
     	int ret = 0;
     	try {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	String param = 	routeId+Util.SepChar+
	    					routeTaskId+Util.SepChar+
	    					docId+Util.SepChar+
	    					routeUserId+Util.SepChar;	
	    	Vector result = new Vector();
	    	VWSLog.dbg("ChkApprovedCount param "+param);
	    	int returnValue = DoDBAction.get(getClient(sid),  database, "VW_DRSChkApprovedCount", param, result);
	    	VWSLog.dbg("ChkApprovedCount procedure call return value "+result);    	
	    	if (result != null && result.size() > 0){
	    		ret = Util.to_Number(result.get(0).toString());
	    	}
	    	//VWSLog.add("getAprrovedCountForTask " + ret);
     	} catch (Exception e) {
     		VWSLog.err("Exception in getApprovedCountForTask procedure call....."+e.getMessage());
     	}
    	return ret;
    }
    public int getRouteTaskCount(int sid, int routeId){
    	VWSLog.dbg("getRouteTaskCount method sid val--->"+sid);
    	int ret = 0;
    	try {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	String param = 	routeId+Util.SepChar;	
	    	Vector result = new Vector();
	    	VWSLog.dbg("VW_DRSGetRouteTaskCount param :"+param);
	    	int returnValue = DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteTaskCount", param, result);
	    	VWSLog.dbgCheck("GetRouteTaskCount procedure call return value "+result);    	
	    	if (result != null && result.size() > 0){
	    		ret = Util.to_Number(result.get(0).toString());
	    	}
	    	//VWSLog.add("getRouteTaskCount " + ret);
    	} catch (Exception e) {
    		VWSLog.err("Exception in getRouteTaskCount procedure call...."+e.getMessage());
    	}
    	return ret;
    }
    public synchronized int updateStatusOfOutOfTskUsrs(int sid, int routeMasterId, int taskSeq, String status){
     	VWSLog.dbg("updateStatusOfOutOfTskUsrs method sid val--->"+sid);
     	int returnValue = 0;
     	try {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	String param = 	routeMasterId+Util.SepChar+
	    					taskSeq+Util.SepChar+status+Util.SepChar;
	    	Vector result = new Vector();
	    	//VWSLog.add("updateStatusOfOutOfTskUsrs " + param);
	    	returnValue = DoDBAction.get(getClient(sid),  database, "VW_DRSUpdateOutofTaskUsers", param, result);
     	} catch (Exception e) {
     		VWSLog.err("Exception in updateStatusOfOutOfTskUsrs "+e.getMessage());
     	}
    	return returnValue;
    }
    public int getDRSPrincipals(int sid, int type, String groupName, Vector princ)
    {
    	VWSLog.dbgCheck("getDRSPrincipals method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        /* If Type = 0)	--gets all Groups and Users 
        * if Type = 1)	-- members Info for a particular Group
        */
        String param = ""+type+Util.SepChar + groupName;
        //VWSLog.add("param "+param);
        DoDBAction.get(getClient(sid),  database, "VW_DRSGetGroupUsers", param, princ);
        //VWSLog.add("result "+princ.toString());
        return NoError;
    }
    
    public int checkGroupsInRoute(int sid, Vector result){
    	VWSLog.dbgCheck("checkGroupsInRoute method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	DoDBAction.get(getClient(sid),  database, "VW_DRSCheckGroupsInRoute", "", result);
    	return NoError;
    }

    public int syncRouteGroups(int sid, String processFlag){
     	VWSLog.dbgCheck("syncRouteGroups method sid val--->"+sid);
    	//Currently outputDocs not used;
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = processFlag + Util.SepChar ;
    	DoDBAction.get(getClient(sid),  database, "VW_DRSSyncRouteGroups", param, new Vector());
    	return NoError;
    }
    // For Invalid routes
     /**
     * getInvalidRouteNames - this is used to get invalid route names by passing -2 as a parameter
     * @return
     */
    protected Vector getInvalidRouteNames(){
    	Vector ret = new Vector();
		String param = "-2"+Util.SepChar;
		DoDBAction.get(database, "VW_DRSGetRouteInfo", param, ret);
    	return ret;
    }
    public int syncInvalidRouteUsers(int sid, int routeId){
    	VWSLog.dbgCheck("syncInvalidRouteUsers method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = routeId + Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VW_DRSSyncInvalidRouteUsers", param, new Vector());
    	return NoError;
    }
    
    public int getRouteInvalidTasks(int sid, int routeId, Vector tasks) {
    	VWSLog.dbgCheck("getRouteInvalidTasks method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = routeId + Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetInvalidTasks", param, tasks);
    	return NoError;
    }

    public int isRouteValid(int sid, int routeId) {
    	VWSLog.dbgCheck("isRouteValid method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = String.valueOf(routeId)+Util.SepChar;

    	Vector resultRouteStatus = new Vector();
    	DoDBAction.get(getClient(sid),  database, "VW_DRSChkRouteStatus", param, resultRouteStatus);
    	if (resultRouteStatus != null){
    		routeId = resultRouteStatus.size()>0?Util.to_Number(resultRouteStatus.get(0).toString()):0;
    	} 
    	return routeId;
    }
    public int getEscalatedDocList(int sid, Vector escalatedDocsList){
    	if(!assertSession(sid))
    		return invalidSessionId;
    	
    	DoDBAction.get(getClient(sid), database, "VW_DRSGetEscalationDocs", "", escalatedDocsList);
    	
    	return NoError;
    }
    
    public int setUserEscalation(int sid, RouteMasterInfo routeMasterInfo){
     	VWSLog.dbgCheck("setUserEscalation method sid val--->"+sid);
    	if(!assertSession(sid))
    		return invalidSessionId;
    	String param = "";

    	param = routeMasterInfo.getRouteId() + Util.SepChar
	    	+routeMasterInfo.getDocId() + Util.SepChar
	    	+routeMasterInfo.getRouteMasterId() + Util.SepChar
	    	+routeMasterInfo.getLevelSeq() + Util.SepChar
	    	+routeMasterInfo.getRouteUserId() + Util.SepChar
	    	+routeMasterInfo.getRouteUserGrpName() + Util.SepChar
	    	+routeMasterInfo.getEUserId() +Util.SepChar
	    	+routeMasterInfo.getEUserName() + Util.SepChar
	    	+routeMasterInfo.getStatus() + Util.SepChar
	    	+routeMasterInfo.getUserStatus() + Util.SepChar;

    	DoDBAction.get(getClient(sid), database, "VW_DRSSetUserEscalation", param, new Vector());

    	return NoError;
    }
	//-----
//  ---------------------End of DRS Methods called by DRS Server---------------------    
    public int resetSecurityDetails(int sid, String roomName, int nodeID,int aclID,Vector result){
    	VWSLog.dbgCheck("resetSecurityDetails method sid val--->"+sid);
   	 if (!assertSession(sid)) return invalidSessionId;
   	 int res=0;
   	 //Calling AclEntry_Reset SP for the issue 793
   	 try{
	    	 String param=aclID + Util.SepChar + String.valueOf(nodeID);
   		 res=DoDBAction.get(getClient(sid),  database, "AclEntry_Reset", param, result);
   	 }catch(Exception e){

   	 }
   	return res;
   }  
//  ---------------------Document List enh... SP calls---------------------    
    public int setCustomisedIndexList(int hitList, int sid, int nodeId, int rowCount, String customIndexList){
    	VWSLog.dbgCheck("setCustomisedIndexList method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	//customIndexList = "-1	-3	-8	-9	-13	-14	-15	-16	-17	"+customIndexList;
    	String paramCustomIndexList = "";
    	if(customIndexList != null && customIndexList.trim().equals("")){
    		paramCustomIndexList = ".";
    	}
    	else{
    		paramCustomIndexList = customIndexList;
    	}
    	// In param userName added for script optimization.
    	String param = 	hitList + Util.SepChar
    					+client.getUserName() + Util.SepChar
    					+Util.replaceSplCharFromTempTable(client.getIpAddress())+ Util.SepChar
    					+nodeId +Util.SepChar
    					+rowCount +Util.SepChar
    					+paramCustomIndexList.trim()+Util.SepChar;	
    	Vector result = new Vector();
    	DoDBAction.get(getClient(sid),  database, "ListIndices", param, result);
    	//VWSLog.add("result "+result.toString());
    	return NoError;
    }
    // Param = @Admin, @Nodeid, @User, @ClientIP, @RowCount,  @Colids
    public int getCustomisedIndexValues(int sid, int nodeId, int rowCount, int refNid, String customIndexList, Vector result){
    	VWSLog.dbgCheck("getCustomisedIndexValues method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
        // For admin user also security permissions needs to set.
    	String admin = (client.isAdmin()? "0" : "0");
    	// This ids are tab separated. Don't change. If need to be changed, make sure tab is delimitor.
    	//Pass '999999999' value if indicevalues list is empty.
    	String paramCustomIndexValuesList = "";
    	if(customIndexList != null && customIndexList.trim().equals("")){
    		paramCustomIndexValuesList = "999999999";
    	}
    	else{
    		paramCustomIndexValuesList = customIndexList;
    	}    	
    	customIndexList = "-1	-2	-3	-4	-5	-6	-7	-9	-10	-11	-12	-14	-15	-17"+Util.SepChar+paramCustomIndexValuesList;
    	String param = 	admin+ Util.SepChar
    					+ nodeId +Util.SepChar
    					+client.getUserName()+Util.SepChar
    					+Util.replaceSplCharFromTempTable(client.getIpAddress()) + Util.SepChar
    					+rowCount +Util.SepChar
    					+refNid +Util.SepChar
    					+customIndexList+Util.SepChar;
    	//VWSLog.err(" get param:"+param+":second Query");
    	DoDBAction.get(getClient(sid),  database, "Node_GetDocIndices", param.trim(), result);
    	//VWSLog.add(" result "+result.toString());
    	return NoError;
    }
    
    //param - @ClientIP, @BeginID, @RowCount, @Colids
    // look in again weather customIndexList is going to have RowCount - Valli
    public int getCustomisedSearchResult(int sid, String customIndexList, Search srch, Vector docs){
    	VWSLog.dbgCheck("getCustomisedSearchResult method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	//-18 added to get node type i.e folder/cabinet/drawer/document
		/**CV10.2 - IF condition added for administration workflow general report*****/
    	String ipAddress = client.getIpAddress();
    	if (srch.isWorkflowReport()) {
    		ipAddress = "A"+ipAddress;
    	} else {
    		customIndexList = "-1	-2	-3	-4	-5	-6	-7	-8	-9	-10	-11	-12	-13	-14	-15	-16	-17	-18"+Util.SepChar+customIndexList;
    	}
    	String param = 	Util.replaceSplCharFromTempTable(ipAddress) + Util.SepChar
    					+String.valueOf(srch.getLastDocId()) + Util.SepChar 
    					//+"252" + Util.SepChar
    					+srch.getRowNo() +Util.SepChar
    					+client.getUserName() +Util.SepChar
    					+customIndexList+Util.SepChar;	
    	//VWSLog.err(" get param:"+param+":third Query");
    	DoDBAction.get(getClient(sid),  database, "NewSearchResult", param, docs);
    	//VWSLog.add(" result "+docs.toString());
    	return NoError;
    }
//  Read/Write the customised settings information to the Database
    public int addCustomList(int sid, String room, String userName, int objectType, String settingsId, String settingsValue, int nodeId){
    	VWSLog.dbgCheck("addCustomList method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Vector docs = new Vector();
    	String param = userName + Util.SepChar +						
    				   objectType+ Util.SepChar + 
    				   settingsId 	 + Util.SepChar +
					   nodeId + Util.SepChar +
					   settingsValue + Util.SepChar ;	
    	//VWSLog.add(" addCustomList:"+param);
    	DoDBAction.get(getClient(sid),  database, "Add_CustomList", param, docs);
    	return NoError;
    }

    public int getCustomList(int sid, String room, String userName, int objectType, Vector settingsValue, int nodeId){
    	VWSLog.dbgCheck("getCustomList method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = userName + Util.SepChar +						
    	nodeId + Util.SepChar + objectType + Util.SepChar;	
    	//VWSLog.err(" getCustomList get param:"+param);
    	DoDBAction.get(getClient(sid),  database, "Get_CustomList", param, settingsValue);
    	//VWSLog.add(" result "+settingsValue.toString());
    	return NoError;
    }
    public int deleteCustomList(int sid, String room, String userName, int mode, int nodeId){
    	VWSLog.dbgCheck("deleteCustomList method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = userName + Util.SepChar + nodeId + Util.SepChar + mode;	
    	DoDBAction.get(getClient(sid),  database, "Delete_CustomList", param, new Vector());
    	return NoError;
    }
    public int isCustomListInherited(int sid, String room, int objectType,  int nodeId){
    	VWSLog.dbgCheck("isCustomListInherited method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Vector result = new Vector();
    	Client client = getClient(sid); 
    	String param = client.getUserName() + Util.SepChar +						
    	nodeId + Util.SepChar + objectType + Util.SepChar;	
    	//VWSLog.err(" getCustomList get param:"+param);
    	DoDBAction.get(getClient(sid),  database, "VWIsCustomListInherited", param, result);
    	int output = 0;
    	try{
    	    if (result != null && result.size() > 0){
    		output = Integer.parseInt(result.get(0).toString());
    	    }
    	}catch(Exception ex){
    	    output = 0;
    	}
    	return output;
    }
    /*Checks if any of the document in that node is in route or checked out
    * Returns 1 if document is in route or checked out. 
    * Returns 0 if no document is in route or checked out.*/
    public int checkDocsInRoute_CheckedOut(int sid,  int nodeId, boolean flagRoute){
    	VWSLog.dbgCheck("checkDocsInRoute_CheckedOut method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	int routeFlag = 0;
    	if(flagRoute)
    		routeFlag = 0;
    	else
    		routeFlag = 1;
    	String param = Util.replaceSplCharFromTempTable(client.getIpAddress())+Util.SepChar+nodeId+Util.SepChar+routeFlag+Util.SepChar;		
    	Vector returnValue = new Vector();
    	//VWSLog.err(" VWCheckDocsInRoute get param:"+param+":");
    	DoDBAction.get(getClient(sid),  database, "VW_DRSCheckDocsInRoute", param, returnValue);
    	//VWSLog.add(" VWCheckDocsInRoute returnValue:"+returnValue.toString());
    	if(returnValue==null || returnValue.size()==0) return Error;
        return Util.to_Number((String)returnValue.get(0));
    }
    //Overrided Method for Check locked docuements from the DTC
    public int checkDocsInRoute_CheckedOut(int sid,  int nodeId, int flagRoute){
    	VWSLog.dbgCheck("checkDocsInRoute_CheckedOut1 method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	String param = Util.replaceSplCharFromTempTable(client.getIpAddress())+Util.SepChar+nodeId+Util.SepChar+flagRoute+Util.SepChar;		
    	Vector returnValue = new Vector();
    	//VWSLog.err(" VWCheckDocsInRoute get param:"+param+":");
    	DoDBAction.get(getClient(sid),  database, "VW_DRSCheckDocsInRoute", param, returnValue);
    	//VWSLog.add(" VWCheckDocsInRoute returnValue:"+returnValue.toString());
    	if(returnValue==null || returnValue.size()==0) return Error;
        return Util.to_Number((String)returnValue.get(0));
    }
    //Following 3 methods are for another room node short cut enhancement.
    public int nodeShortCutAdd(int sid,  int parentNodeId, String nodeName, String description){
    	VWSLog.dbgCheck("nodeShortCutAdd method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = parentNodeId+Util.SepChar+nodeName+Util.SepChar+description;		
    	DoDBAction.get(getClient(sid),  database, "VWOtherRoomNodeShortCut_Add", param, new Vector());
    	return NoError;
    	
    }
    public int nodeShortCutDelete(int sid,   int nodeId){
    	VWSLog.dbgCheck("nodeShortCutDelete method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	String param = nodeId+Util.SepChar+client.getUserName();	
    	DoDBAction.get(getClient(sid),  database, "VWOtherRoomNodeShortCut_Del", param, new Vector());
    	return NoError;
    	
    }
    public int getNodeShortCutDesc(int sid,  int nodeId, Vector desc){
    	VWSLog.dbgCheck("getNodeShortCutDesc method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = ""+nodeId;
    	DoDBAction.get(getClient(sid),  database, "VWGetNodeShortCut_Descrip", param, desc);
    	return NoError;
    	
    }
    // Assign Index Default values in the Node
    public int updateNodeRules(int sid, NodeIndexRules nodeIndexRules){    	
      	VWSLog.dbgCheck("updateNodeRules method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	try{
    		String param = nodeIndexRules.getNodeId() + Util.SepChar
    		+ nodeIndexRules.getDocTypeId() + Util.SepChar
    		+ nodeIndexRules.getCreatorName() + Util.SepChar
    		+ nodeIndexRules.getIndicesCount() + Util.SepChar;    	
    		ArrayList indicesRulesList = nodeIndexRules.getIndicesRulesList();
    		/*
    		* Code modified for notification 1197 - Node property for the folder is not copied when they try coping folder structure using the templates tab
    		* Node properties need to create for default document type selection for the indicesCount 0.
    		*/
    		if(nodeIndexRules.getIndicesCount()>0){
	    		for(int count =0; count < nodeIndexRules.getIndicesCount(); count++){
	    			if(indicesRulesList!=null && indicesRulesList.size()>0){
	    				param += indicesRulesList.get(count) + Util.SepChar; 
	    			}
	    		}
    		}
    		DoDBAction.get(getClient(sid),  database, "CreateNodeRules", param, new Vector());
    		int nodeId = nodeIndexRules.getNodeId();
    		Node node = new Node(nodeId);
    		try{
    			int ret = getNodePropertiesByNodeID(sid, node);
    			if(node.getType()==VNSConstants.nodeType_Folder){
    				message = "Node Properties Changed for folder '"+node.getName()+"'";
    				addNotificationHistory(sid, nodeId, VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_NodeProp, getClient(sid).getUserName(), "-", "-", message);
    			}
    		}catch(Exception ex){
    		}
    	}
    	catch(Exception ex){
    		return Error;
    	}
    	//VWSLog.add(" result "+settingsValue.toString());
    	return NoError;
    }
    public int getNodeRules(int sid, int nodeId, Vector indexRules){    	
       	VWSLog.dbgCheck("getNodeRules method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	try{    		
    		
    	String param = String.valueOf(nodeId);
    	DoDBAction.get(getClient(sid),  database, "GetNodeIndexRule", param, indexRules);    	
    	}
    	catch(Exception ex){    		
    		return Error;
    	}
    	return NoError;
    } 
    public int checkNodeIndexRules(int sid, int nodeId, Vector indexRules){    	
     	VWSLog.dbgCheck("checkNodeIndexRules method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	try{    		    		
    	String param = String.valueOf(nodeId);    	
    	DoDBAction.get(getClient(sid),  database, "CheckNodeIndexRules", param, indexRules);    	
    	}
    	catch(Exception ex){
    		return Error;
    	}    	
    	return NoError;
    }     
    public int checkNodeTreeIndexRules(int sid, int nodeId, Vector indexExists){    	
    	VWSLog.dbgCheck("checkNodeTreeIndexRules method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	try{    		    	
    		Client client = getClient(sid);
    		String param = String.valueOf(nodeId) + Util.SepChar + client.getIpAddress();
    		DoDBAction.get(getClient(sid),  database, "CheckNodeTreeIndexRules", param, indexExists);    	
    	}
    	catch(Exception ex){
    		return Error;
    	}    	
    	return NoError;
    } 
	public int deleteNodeRules(int sid, int nodeId){
		VWSLog.dbgCheck("deleteNodeRules method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	try{
    		String param = String.valueOf(nodeId);    	
        	DoDBAction.get(getClient(sid),  database, "DeleteNodeRules", param, new Vector());    	
    	}catch(Exception ex){
        		return Error;
    	}    	
        return NoError;
    }
    
    public int getAllDocTypeIndices(int sid, int nodeId, Vector indices)
    {
    	VWSLog.dbgCheck("getAllDocTypeIndices method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(nodeId);
        return DoDBAction.get(getClient(sid),  database, "GetAllDocTypeIndices", param,  indices);
    }
    public int getDocTypeIndicesWithRules(int sid, int nodeId, int docTypeId, Vector indices)
    {
    	VWSLog.dbgCheck("getDocTypeIndicesWithRules method sid val--->"+sid);
    	try{
    		if (!assertSession(sid)) return invalidSessionId;
            String param = String.valueOf(nodeId) + Util.SepChar + String.valueOf(String.valueOf(docTypeId));
            return DoDBAction.get(getClient(sid),  database, "GetDocTypeIndicesWithRules", param,  indices);
    	} catch(Exception ex){
    		VWSLog.err("EXCEPTION in GetDocTypeIndicesWithRules generalErrorCode(-1)  :::: "+ex.getMessage());
    		return Error;
    	}
    }
//  ---------------------Document List enh... SP calls  end---------------------    
    public boolean DBInsertPageText(int did, int pno, int pid, int pvr,int qc,String localPath)
    {
        ResultSet rs  = null;
        OracleCallableStatement ocs = null;
        CallableStatement cs = null;
        Connection cn = null;
        BufferedReader reader = null;
        FileInputStream is = null;
        int ret = -1;
        try
        {
            File file = new File(localPath);
            if(qc != 0)
            	file.createNewFile();
            reader = new BufferedReader(new FileReader(file));
            cn = database.aquireConnection();

            if (database.getEngineName().equalsIgnoreCase("Oracle"))
            {
                CLOB clob = getClob(reader, cn);
                /*
                 *  Below code is commended to capture the error.
                 */
                //if (clob == null || clob.length() <= 0) return false;
                ocs = (OracleCallableStatement) cn.prepareCall
                                       ("{call IXInsertPageText(?,?,?,?,?,?,?)}");
                ocs.setInt(1, did);
                ocs.setInt(2, pno);
                ocs.setInt(3, pid);
                ocs.setInt(4, pvr);
                ocs.setCLOB(5, clob);
                ocs.setInt(6, qc);
                ocs.registerOutParameter(7, OracleTypes.CURSOR);
                ocs.execute();
                rs = (ResultSet) ocs.getObject(7);
                clob.freeTemporary();
            }
            else
            {
                cs = cn.prepareCall("{call IXInsertPageText(?,?,?,?,?,?)}");
                cs.setInt(1, did);
                cs.setInt(2, pno);
                cs.setInt(3, pid);
                cs.setInt(4, pvr);
                cs.setCharacterStream(5, reader, (int) file.length());
                cs.setInt(6, qc);
                rs = cs.executeQuery();
            }
            if (rs != null && rs.next())
            {
                ret = rs.getInt("Fld");
                //rs.close();
                //rs = null;

            }
            // Close the resultset in outside of the if statement. In 'if' statement it will close the result set if it has atleast one record.
            if (rs != null)  { rs.close(); rs = null;}
            if (cs != null)  { cs.close(); cs = null;}
            if (ocs != null) { ocs.close(); ocs = null;}
            cn.commit();
        }
        catch(Exception e)
        {
            IXRLog.err(e.getMessage());
        }
        finally
        {
        	if (reader != null) try{reader.close();} catch(IOException ioe){}
            assertConnection(cn);
            
        }
        return (ret == 0 ? true : false);
    }
    
    private CLOB getClob(Reader reader, Connection cn)
    {
        CLOB clob = null;
        //Connection cn = null;
        Writer os = null;
        char[] cbuf = new char[65536];
        try
        {
            //cn = database.aquireConnection();
            clob = CLOB.createTemporary(cn, false, CLOB.DURATION_SESSION);
            clob.open(CLOB.MODE_READWRITE);
            os = clob.getCharacterOutputStream();
            
            int offs = 0;
            int len = 0;
            while ( (len = reader.read(cbuf)) != -1)
            {
                os.write(cbuf, offs, len);
                offs += len;
            }

        }
        catch (Exception e)
        {
            IXRLog.err(e.getMessage());
        }  
        finally {
        	try{
            os.flush();
            os.close();
            clob.close();
            cbuf = null;
        	}catch(Exception e){}        	
        	//assertConnection(cn);
        	}
        return clob;
    }
    private CLOB getClob(String image, Connection cn)
    {
        CLOB clob = null;
        Writer os = null;
        try
        {
            clob = CLOB.createTemporary(cn, false, CLOB.DURATION_SESSION);
            clob.open(CLOB.MODE_READWRITE);
            os = clob.getCharacterOutputStream();
           	os.write(image);
    
        }
        catch (Exception e)
        {
            VWSLog.err(e.getMessage());
        }  
        finally {
        	try{
            os.flush();
            os.close();
            clob.close();
        	}catch(Exception e){}        	
        	}
        return clob;
    }
    public boolean isFTSEnabled(int sid, String doctypeID)
    {
	Vector output = new Vector();
        String param = String.valueOf(doctypeID);
        DoDBAction.get(getClient(sid),  database, "VWCheckFTSEnable", param,  output);	
        if (output != null && output.size() > 0){
            String ret = output.get(0).toString();
            if (ret.trim().equals("1")) return true;
        }
        return false;
/*    	OracleCallableStatement ocs = null;
    	boolean result = false;
        ResultSet rs  = null;
        Connection cn = null;
        CallableStatement cs = null;
        int ret = 0;
        try
        {
            if (database.getEngineName().equalsIgnoreCase("Oracle")){
            	cn = database.aquireConnection();
            	ocs = (OracleCallableStatement) cn.prepareCall("{call VWCheckFTSEnable(?,?)}");
				ocs.setString(1, String.valueOf(doctypeID));
				ocs.registerOutParameter(2, OracleTypes.CURSOR);
                ocs.execute();
                rs = (ResultSet) ocs.getObject(2);
				if (rs != null && rs.next())
	            {
	                String rret = rs.getString("Fld");
	                ret = Integer.parseInt(rret);
	            }
	            // Close the resultset in outside of the if statement. In 'if' statement it will close the result set if it has atleast one record.
	            if (rs != null)  { rs.close(); rs = null;}
	            if (cs != null)  { cs.close(); cs = null;}
	            if(ret==1)
	            	result=true;
	            else
	            	result=false;
        	}
            else
            {
                cn = database.aquireConnection();
                cs = cn.prepareCall("{call VWCheckFTSEnable(?)}");
                cs.setString(1,String.valueOf(doctypeID));
                rs = cs.executeQuery();
            }
            if (rs != null && rs.next())
            {
                String rret = rs.getString("Fld");
                ret = Integer.parseInt(rret);
            }
            // Close the resultset in outside of the if statement. In 'if' statement it will close the result set if it has atleast one record.
            if (rs != null)  { rs.close(); rs = null;}
            if (cs != null)  { cs.close(); cs = null;}
            if(ret==1)
            	result=true;
            else
            	result=false;
        }
        catch (SQLException e)
        {
        	String Msg = e.getMessage();
        }
        finally {assertConnection(cn);}*/       
    }
    
	//Desc   :Added method 'getFTSEnabledPages()' which will give the FTS Enabled Pages list, in the vecPages Vector.
	//Author :Nishad Nambiar
	//Date   :13-Nov-2007
    public int getFTSEnabledPages(String room, int sid, int nodeId, Vector vecPages)
    {
    	VWSLog.dbgCheck("getFTSEnabledPages method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(nodeId);
        int result = DoDBAction.get(getClient(sid),  database, "VWGetFTSEnabledPages", param, vecPages);
        return result;
    }
    
    public int getKeyIndexName(String room, int sid, int docTypeId, Vector vecKeyIndexName)
    {
    	VWSLog.dbgCheck("getKeyIndexName method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        String param = docTypeId + Util.SepChar;        
        DoDBAction.get(getClient(sid),  database, "VWGetKeyIndexName", param, vecKeyIndexName);
        return NoError;
    }
    
    /**
     * Desc   :Below method 'deleteTempTables' deletes temporary tables (requires to call when VW server starts).
     * Author :Nishad Nambiar
     * Date   :12-Jan-2009
     * @return
     */
    public int deleteTempTables()
    {
    	try{
	    	Vector vecRes = new Vector();
        	DoDBAction.get(null, database, "VWDelIPTable", "", vecRes);
    	}catch(Exception e){
    		return Error;
    	}
        return NoError;
    }
    
    /**
     * deleteEmptyUsers - used to deleted the users which is stored as empty.
     * @return
     */
    public int deleteEmptyUsers()
    {
    	try{
	    	Vector vecRes = new Vector();
        	DoDBAction.get(null, database, "VWDelEmptyUsers", "", vecRes);
    	}catch(Exception e){
    		return Error;
    	}
        return NoError;
    }
    /*
     * This block contains the method of the Link to the External database 
     */

    public Vector getDSNInfo(int dsnType){
	return getDSNInfo(0, 0, 0, dsnType);
    }
    public Vector getDSNInfo(int sid, int docTypeId){
    	return getDSNInfo(sid, docTypeId, 0, 0);
    }
    /**
     * 
     * @param sid
     * @param docTypeId
     * @param dsnType - This is to differentiate dsn settings for DBLookup and SelectionList
     * @return
     */
    public Vector getDSNInfo(int sid, int docTypeId, int indexId, int dsnType)
    {
	Vector dsnList = new Vector();
	Vector dbLookupList = new Vector();
	if (!assertSession(sid) && docTypeId > 0) return dsnList;

	try{	    	
		String param = String.valueOf(docTypeId)+ Util.SepChar + indexId + Util.SepChar + dsnType + Util.SepChar;
		DoDBAction.get(getClient(sid),  database, "VW_DSNGetDSNList", param, dsnList);
		if (dsnList != null && dsnList.size() > 0){
			for (int cnt = 0; cnt < dsnList.size(); cnt++){
				dbLookupList.add(new DBLookup(dsnList.get(cnt).toString(), dsnType));
			}
		}
	}catch(Exception e){
	    return dbLookupList;
	}
	return dbLookupList;
    }

	public int setDBLookup(int sid, DBLookup lookupInfo) {
		VWSLog.dbgCheck("setDBLookup method sid val--->" + sid);
		if (!assertSession(sid))
			return invalidSessionId;
		String param = "";
		param = lookupInfo.getDocTypeId() + Util.SepChar + lookupInfo.getDsnName() + Util.SepChar
				+ lookupInfo.getUserName() + Util.SepChar + lookupInfo.getPassWord() + Util.SepChar
				+ lookupInfo.getExternalTable() + Util.SepChar + lookupInfo.getDsnStatus() + Util.SepChar
				+ lookupInfo.getConfigQuery() + Util.SepChar + lookupInfo.getIndexId() + Util.SepChar
				+ lookupInfo.getDsnType();
		DoDBAction.get(getClient(sid), database, "VW_DSNSetDSNInfo", param, new Vector());
		DSNDatabase dbInfo = new DSNDatabase(database.getName(), lookupInfo.getDsnName(), lookupInfo.getUserName(),
				lookupInfo.getPassWord());
		if (lookupInfo.getDsnType() == 0 && lookupInfo.getDsnStatus() == 1)
			externalDBList.put(lookupInfo.getDsnName(), dbInfo);
		int output = setIndexMapping(sid, lookupInfo);

		String documentTypeName = "";
		if (lookupInfo.getDsnType() == 0) {// DBLookup
			try {
				Vector name = new Vector();
				int ret = getNFNodeName(sid, lookupInfo.getDocTypeId(), VNSConstants.module_DT, name);
				if (name != null && name.size() > 0) {
					documentTypeName = name.get(0).toString();
				}
				message = "DB Lookup modified for document type '" + documentTypeName + "'";
				addNotificationHistory(sid, lookupInfo.getDocTypeId(), VNSConstants.nodeType_DT,
						VNSConstants.notify_DT_ExternalDB, getClient(sid).getUserName(), "-", "-", message);
			} catch (Exception ex) {
			}
		}
		return NoError;
	}

    public int setIndexMapping(int sid, DBLookup lookupInfo){
    	VWSLog.dbgCheck("setIndexMapping method sid val--->"+sid);
	if (!assertSession(sid)) return invalidSessionId;
	DocType dt = lookupInfo.getDocType();
	Vector indices = dt.getIndices();
	for(int count = 0; count < indices.size(); count++){
	    Index selectedIndex = (Index) indices.get(count);
	    String param = "";
	    int isTriggerColumn = 0;
	    if ( selectedIndex.isTriggerEnabled()) {
		isTriggerColumn = 1;
	    }
	    param = dt.getId() + Util.SepChar + 
	    selectedIndex.getId() + Util.SepChar + 
	    selectedIndex.getExternalColumn() + Util.SepChar + 
	    selectedIndex.getTriggerDataType() + Util.SepChar +
	    isTriggerColumn + Util.SepChar + selectedIndex.getMask(); 
	    DoDBAction.get(getClient(sid),  database, "VW_DSNSetIndexMapping", param, new Vector());    
	}		
	return NoError;
    }
    public int getIndexMapping(int sid, DocType dt, Vector result){
	return getIndexMapping(sid, dt, result, 0);
    }
    public int getIndexMapping(int sid, DocType dt, Vector result, int mode){
    VWSLog.dbgCheck("getIndexMapping method sid val--->"+sid);
	if (!assertSession(sid)) return invalidSessionId;
	String param = String.valueOf(dt.getId()) + Util.SepChar + mode;	
	DoDBAction.get(getClient(sid),  database, "VW_DSNGetIndexMapping", param, result);	
	return NoError;	
    }
    /**
     * Added for DBLookup connection reeshablishment issue
     */
    public void reestablishDSNConnection() {
    	try{
    		int dsnType = 1; //DSNType for getting DSNINFO of Selection List
    		Vector dsnList = getDSNInfo(dsnType);    		    
    		String dsnName = "";
    		String userName = "";
    		String passWord = "";
    		VWSLog.dbg("dsnList size check in DSN Verify :"+dsnList);
    		if (dsnList != null && dsnList.size() > 0){
    			for (int count = 0; count < dsnList.size(); count++){
    				DBLookup lookupInfo = (DBLookup) dsnList.get(count);

    				if (lookupInfo.getDsnName() != null && lookupInfo.getDsnName().length() > 0){    				
						/**CV10.2 - Added for DBLookup connection issue fix**/
    					DSNDatabase dbInfo = new DSNDatabase(database.getName(), lookupInfo.getDsnName(), lookupInfo.getUserName(), lookupInfo.getPassWord());
    					//Database dbInfo = new Database(database.getName(), lookupInfo.getDsnName(), lookupInfo.getUserName(), lookupInfo.getPassWord());
    					if(lookupInfo.getDsnType()==0)
    						externalDBList.put(lookupInfo.getDsnName(), dbInfo);
    					else if(lookupInfo.getDsnType()==1 && lookupInfo.getDsnStatus()==1)//Only Enabled Selection index need to be synchronized
    						externalDSNList.put(lookupInfo, dbInfo);
    				}
    			}
    			VWSLog.dbg("externalDBList >>> "+externalDBList);
    			VWSLog.dbg("externalDSNList >>> "+externalDSNList);
    			dsnFlag = true;
    		}
    	}catch(Exception ex){
    		//message =resourceManager.getString("RampServMsg.StartDBErr")+" "+ ex.getMessage();
    		VWSLog.err("Error in start DB " + ex.getMessage());
    		//VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    		dsnFlag = false;
    	}

    }
    
    public int getExternalData(int sid, DocType dt, Vector output){
    	int retErrorCode = 0;
    	try{
    		VWSLog.dbgCheck("getExternalData method sid val--->"+sid);
    		if (!assertSession(sid)) return invalidSessionId;	
    		VWSLog.dbg("Inside getExternalData:::::");
    		VWSLog.dbg("dsnFlag value::::"+dsnFlag);
    		if (dsnFlag == false) {
    			VWSLog.dbg("Inside dnsflag false criteria");
    			reestablishDSNConnection();
    			/***Commented for CV10.2-DB Lookup connection issue**********/ 
    			//dsnFlag = true;
    		}
    		VWSLog.dbg("dsnFlag value::::"+dsnFlag);
    		if (dsnFlag) {
	    		PreparedStatement stmt = null;
	    		ResultSet result = null;
	    		ResultSetMetaData resultMeta = null;
	    		Vector dsnInfo = getDSNInfo(sid, dt.getId());
	
	    		if (dsnInfo == null || (dsnInfo != null && dsnInfo.size() == 0)) {
	    			output.add(Util.getNow(2) +" : DSN information is not avilable");
	    			return Error;
	    		}
	
	    		DBLookup dsnDetails = (DBLookup)dsnInfo.get(0);
	    		String sql = dsnDetails.getConfigQuery();//getExternalQuery(sid, dt.getId());
	    		if(sql == null || sql.trim().length() <= 0){
	    			VWSLog.dbg("Sql is null");
	    			return Error;
	    		}
	
	    		/// We need to pass the date value with the index date mask
	    		Vector indexMap = new Vector();
	    		Hashtable maskList = new Hashtable();
	    		getIndexMapping(sid, dt, indexMap, 0);
	    		String triggerIndexId = "";
	    		if (indexMap != null && indexMap.size() >0){
	    			for (int count = 0; count < indexMap.size(); count++){
	    				String indexId = "";
	    				String colName ="";
	    				String colDataType ="";
	    				String triggerKey ="";
	    				String externalDBColMask = ""; 
	    				String data = indexMap.get(count).toString();
	    				StringTokenizer tokens = new StringTokenizer(data, Util.SepChar);
	    				if (tokens.hasMoreTokens()) indexId = tokens.nextToken();
	    				if (tokens.hasMoreTokens()) colName = tokens.nextToken();
	    				if (tokens.hasMoreTokens()) colDataType = tokens.nextToken();
	    				if (tokens.hasMoreTokens()) triggerKey = tokens.nextToken();
	    				if (tokens.hasMoreTokens()) externalDBColMask = tokens.nextToken();
	    				String temp = colName + Util.SepChar + externalDBColMask;
	    				maskList.put(indexId, temp);
	    			}
	    		}
	    		Vector indices = dt.getIndices();
	    		if (indices != null && indices.size() > 0){
	    			String indexMask = "", externalDBMask = "", triggerValue = "", dummyValue = "";
	    			for (int pos = 0; pos < indices.size(); pos++){
	    				Index idx = (Index) indices.get(pos);	
	    				String tempStr = (String) maskList.get(String.valueOf(idx.getId()));
	    				if (idx.isTriggerEnabled()){
	    					triggerValue = idx.getTriggerValue();
	    					StringTokenizer st = new StringTokenizer(tempStr, Util.SepChar);
	    					if(st.hasMoreTokens()) 
	    						dummyValue = "@" + st.nextToken();
	    					if(st.hasMoreTokens()) 
	    						externalDBMask = st.nextToken();
	    					if(idx.getTriggerDataType() == DATE_DATA_TYPE && externalDBMask != null &&externalDBMask.trim().length() > 0){
	    						indexMask = idx.getMask();
	    						triggerValue = Util.convertDateFormat(triggerValue, indexMask, externalDBMask);
	    					}    					
	    					sql = sql.replace(dummyValue, triggerValue);
	    				}
	    			}
	    		}
	    		
	    		if(dsnDetails.getDsnStatus() == 0)
	    			return Error;
	    		output.add(Util.getNow(2) +" : External DB Query is " + sql);
	    		VWSLog.dbg(Util.getNow(2) +" : External DB Query is " + sql);
	    		if (sql != null && sql.length() > 0){
	    			String dsnName = dsnDetails.getDsnName();
	    			Connection con = null;
	    			DSNDatabase dsnDB = (DSNDatabase) externalDBList.get(dsnName);
	    			//Database dsnDB = (Database) externalDBList.get(dsnName);
	    			try{
	    				VWSLog.dbg(Util.getNow(2) +" : Getting DB Connection for DSN - " + dsnName);
	    				output.add(Util.getNow(2) +" : Getting DB Connection for DSN - " + dsnName);
	    				//con = dsnDB.aquireConnection();
	    				/***Added for CV10.2-DB Lookup connection issue**********/ 
	    				con = getDBConnection(dsnDB);
	    				stmt = con.prepareStatement(sql);
	    				output.add(Util.getNow(2) +" : DB Connection established for DSN - " + dsnName);
	    				VWSLog.dbg(Util.getNow(2) +" : DB Connection established for DSN - " + dsnName);
	    				result = stmt.executeQuery();
	    				output.add(Util.getNow(2) +" : Query executed - " + sql);
	    				VWSLog.dbg(Util.getNow(2) +" : Query executed - " + sql);
	    				resultMeta = result.getMetaData();
	    				int colCount = resultMeta.getColumnCount();
	    				Hashtable indexTable = new Hashtable();
	    				if(result.next()){
	    					for (int count =1; count < colCount; count++){
	    						String IndexId = result.getString(count);
	    						String value = result.getString(++count);
	    						if (value == null) value = "";
	    						indexTable.put(IndexId, value);
	    					}
	    				}
	    				VWSLog.dbg("Index table ::::"+indexTable);
	    				Vector modifiedIndices = new Vector(); 
	    				for (int count = 0; count < indices.size(); count++){
	    					Index selectedIndex = (Index) indices.get(count);
	    					String indexID = String.valueOf(selectedIndex.getId());
	    					if (indexTable.containsKey(indexID.trim())){
	    						String selectedVal  = (String)indexTable.get(indexID);
	    						if (selectedVal != null && selectedVal.length() > 0){
	    							/*
	    							 * This place we have to check the date index and convert the date mask
	    							 */
	    							selectedIndex.setDef(selectedVal);
	    							selectedIndex.setValue(selectedVal);	
	    							VWSLog.dbg("selectedIndex getDef"+selectedIndex.getDef());
	    							VWSLog.dbg("selectedIndex getvalue"+"selectedIndexid::"+	selectedIndex.getId()+"Value"+selectedIndex.getValue());
	    							if (selectedIndex.getType() == Index.IDX_DATE){
	    								if (maskList.containsKey(indexID) && !maskList.get(indexID).equals(selectedIndex.getMask()) && selectedIndex.isTriggerEnabled()){
	    									output.add(Util.getNow(2) + " ------------------------------------------------------------------------------------------");
	    									output.add(Util.getNow(2) + " : Index Name <" + selectedIndex.getName() + ">     Mask - " + selectedIndex.getMask() );
	    									output.add(Util.getNow(2) + " : External table column Mask " + maskList.get(indexID).toString());
	    									output.add(Util.getNow(2) + " : External table column Value "  + selectedVal);
	    									output.add(Util.getNow(2) + " ------------------------------------------------------------------------------------------");
	    									selectedVal = Util.convertDateFormat(selectedVal, maskList.get(indexID).toString(), selectedIndex.getMask());
	    									selectedIndex.setDef(selectedVal);
	    									selectedIndex.setValue(selectedVal);
	    									//output.add(Util.getNow(2) + " : actual Value " + actualValue);
	    									//output.add(Util.getNow(2) + " : actual Value " + selectedIndex.getActDef());
	    								}
	    								String actualValue = Util.convertDateFormat(selectedVal, selectedIndex.getMask(), "yyyyMMdd");
	    								selectedIndex.setActDef(actualValue);
	
	    							}				
	    						}
	    					}
	    					modifiedIndices.add(selectedIndex);
	    				}
	    				VWSLog.dbg("modifiedIndices::::"+modifiedIndices);
	    				dt.setIndices(modifiedIndices);
	    			} catch(SQLException ex){
	    				dsnFlag=false;
	    				output.add(Util.getNow(2) + " : Error while getting ExternalData " + ex.getMessage());
	    				message = resourceManager.getString("RampServMsg.ExternalData")+" " + ex.getMessage();
	    				//VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.err("Error while getting ExternalData " + ex.getMessage());
	    				retErrorCode = ViewWiseErrors.dsnConnError;
	    			} catch(Exception e){
	    				dsnFlag=false;
	    				message = resourceManager.getString("ViewWiseERR.dsnLkupConnError");
	    				output.add(Util.getNow(2) + " : " + message);
	    				// Error while getting ExternalData. Please contact administrator and check to verify external database lookup connection.
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.err("Error while getting ExternalData " + e.getMessage());
	    				retErrorCode = ViewWiseErrors.dsnLkupConnError;
	    			}
	    			finally{
	    				try{
	    					dsnDB.releaseConnection(con);
	    					if (result != null) result.close();
	    					if (stmt != null) stmt.close();
	    					if (con != null) con.close();
	    				}catch(Exception ex){
	    					result = null;
	    					stmt = null;
	    					con = null;
	    				}
	    			}
	
	    		}else{
	    			output.add(Util.getNow(2) +"Failed to execute Query - " + sql);
	    			VWSLog.dbg(Util.getNow(2) +"Failed to execute Query - " + sql);
	    			VWSLog.dbg("Before error inside else");
	    			retErrorCode = Error;
	    		}
	    		output.add(Util.getNow(2) + " : Getting external data is completed");
	    		VWSLog.dbg(Util.getNow(2) + " : Getting external data is completed");
    		} else {
    			dsnFlag=false;
    			message = resourceManager.getString("ViewWiseERR.dsnLkupConnError");
    			output.add(Util.getNow(2) + " : "+message);
				// Error while getting ExternalData. Please contact administrator and check to verify external database lookup connection.
				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
				VWSLog.err(message);
				retErrorCode = ViewWiseErrors.dsnLkupConnError;
    		}
    	}catch(Exception e){
    		dsnFlag=false;
    		VWSLog.dbg("exception e:::"+e.getMessage());
    		retErrorCode = ViewWiseErrors.dsnConnError;
    		
    	}
    	return retErrorCode;
}
    
    /**
     * CV10.2 - This method is used to get DB Connection for DB Lookup External Data
     * @param dsnDB
     * @return
     */
    //private Connection getDBConnection(Database dsnDB) {
    private Connection getDBConnection(DSNDatabase dsnDB) {
    	Connection con = null;
    	try {
    		con = dsnDB.aquireConnection();
    	} catch (Exception e) {
    		VWSLog.dbg("Exception in DBLookup Verify connection 1:"+e.getMessage());
    		try {
    			Util.sleep(5 * 1000);
    			VWSLog.dbg("DB connection try after 5 seconds");
    			con = dsnDB.aquireConnection();
    		} catch (Exception e2) {
    			VWSLog.dbg("Exception in DBLookup Verify connection 2:"+e2.getMessage());
    			try {
    				Util.sleep(5 * 1000);
    				VWSLog.dbg("DB 2 connection try after 5 seconds");
    				con = dsnDB.aquireConnection();
        		} catch (Exception e3) {
        			VWSLog.dbg("Exception in DBLookup Verify connection 3:"+e3.getMessage());
        			try {
        				Util.sleep(5 * 1000);
        				VWSLog.dbg("DB3 connection try after 5 seconds");
        				con = dsnDB.aquireConnection();
            		} catch (Exception e4) {
            			VWSLog.dbg("Exception in DBLookup Verify connection 4:"+e4.getMessage());
            			try {
            				Util.sleep(5 * 1000);
            				VWSLog.dbg("DB4 connection try after 5 seconds");
            				con = dsnDB.aquireConnection();
                		} catch (Exception e5) {
                			VWSLog.dbg("Exception in DBLookup Verify connection 5:"+e5.getMessage());
                			try {
                				dsnDB.releaseConnection(con);
                				con = null;
                			} catch (Exception e6) {		
                			}
                		}
            		}
        		}
    		}
    	}
    	return con;
    }
    
    public String getExternalQuery(int sid, int docTypeId){
	Vector result = new Vector();
	String param = String.valueOf(docTypeId)  + Util.SepChar ;
	DoDBAction.get(getClient(sid),  database, "VW_DSNGetDSNExternalQuery", param, result);
	String query ="";
	if (result != null && result.size() > 0){
	    query = result.get(0).toString();
	}
	return query;
    }
    public Vector getExternalLookupColumns(int sid, String dsnName, String userName, String passWord, String externalTable){
	return getExternalLookupColumns(sid, dsnName, userName, passWord, externalTable, "");
    }
    public Vector getExternalLookupColumns(int sid, String dsnName, String userName, String passWord, String externalTable, String query){
    	Vector columnList = new Vector();
    	if (!assertSession(sid)) {
    		columnList.add("Error");
    		columnList.add("Invalid Session Id");
    		return columnList;
    	}
    	Statement stmt = null;
    	ResultSet rset = null;
    	Connection conn = null;
    	try{
    		Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
    		String url = "jdbc:odbc:" + dsnName;
    		VWSLog.dbg("getExternalLookupColumns url :"+url);
    		/***Added for CV10.2-DB Lookup connection issue**********/ 
			dbLookupErrorMsg = null;
    		conn = getDBLookupConnection(url, userName, passWord);
    		VWSLog.dbg("getExternalLookupColumns DBConnection :"+conn);
    		if (conn != null) {
	    		//conn = DriverManager.getConnection(url, userName, passWord);
	    		stmt = conn.createStatement();	
	    		String queryForExtCols = "select * from " + externalTable + " where 1 = 0";
	    		if (query == null || (query != null && query.trim().length() == 0))
	    			rset = stmt.executeQuery(queryForExtCols);
	    		else{
	    			queryForExtCols = query;
	    			rset = stmt.executeQuery(query);
	    		}
	    		if (VWSPreferences.getOutputDebug()){
	    			VWSLog.add("Query to get External Column: " + queryForExtCols);
	    		}
	    		ResultSetMetaData metaData = rset.getMetaData();
	    		int colCount = metaData.getColumnCount();
	
	    		for (int column = 1; column <= colCount; column++){
	    			/*if (metaData.getColumnTypeName(column).toLowerCase().indexOf("date") == -1)*/
	    			columnList.add(metaData.getColumnName(column));
	    		}
    		} else {
    			columnList.add("Error");
    			VWSLog.dbg("dbLookupErrorMsg :" + dbLookupErrorMsg);
    			if (dbLookupErrorMsg != null)
    				columnList.add("Exception : " + dbLookupErrorMsg);
    			else
    				columnList.add("Exception : "+resourceManager.getString("RampServMsg.DBLookupConnectionError"));
    		}
    	} catch(ClassNotFoundException cnfe){
    		VWSLog.dbg("Exception in getExternalLookupColumns 1:"+cnfe.getMessage());
    		columnList.add("Error");
    		columnList.add("Exception : " + cnfe.getMessage());
    		//return columnList;
    	} catch(SQLException sqlEx){
    		VWSLog.dbg("Exception in getExternalLookupColumns 1:"+sqlEx.getErrorCode());
    		VWSLog.dbg("Exception in getExternalLookupColumns 2:"+sqlEx.getMessage());
    		columnList.add("Error");
    		columnList.add("Exception : " + sqlEx.getMessage());
    		//return columnList;

    	} catch(Exception ex){
    		columnList.add("Error");
    		columnList.add("Exception : " + ex.getMessage());
    		//return columnList;
    	} finally{
    		try {
    			rset.close();
    			stmt.close();
    			conn.close();
    		} catch(Exception ex){

    		}
    	}
    	VWSLog.dbg("columnList :" + columnList);
    	return columnList;
    }
    
    /**
     * CV10.2 - This method is used to get the DB connection for DBLookup
     * @param url
     * @param userName
     * @param passWord
     * @return
     */
    public Connection getDBLookupConnection(String url, String userName, String passWord) {
    	Connection con = null;
    	try {
    		//VWSLog.dbg("DSN connection details before connection eshtablishment :\n URL :"+url+"\n userName :"+userName+"\n passWord :"+passWord);
    		con = DriverManager.getConnection(url, userName, passWord);
    	} catch (Exception e) {
    		VWSLog.dbg("Exception in DBLookup connection 1:"+e.getMessage());
    		dbLookupErrorMsg = e.getMessage();
    		if (!e.getMessage().contains("Login failed") && !e.getMessage().contains("Data source name not found")) {
    			try {
	    			Util.sleep(5 * 1000);
	    			VWSLog.dbg("2nd time DB connection try after 5 seconds");
	    			con = DriverManager.getConnection(url, userName, passWord);
	    		} catch (Exception e2) {
	    			VWSLog.dbg("Exception in DBLookup connection 2:"+e2.getMessage());
	    			dbLookupErrorMsg = e2.getMessage();
	    			try {
	    				Util.sleep(5 * 1000);
	    				VWSLog.dbg("3rd time DB connection try after 5 seconds");
	        			con = DriverManager.getConnection(url, userName, passWord);
	        		} catch (Exception e3) {
	        			VWSLog.dbg("Exception in DBLookup connection 3:"+e3.getMessage());
	        			dbLookupErrorMsg = e3.getMessage();
	        			try {
	        				Util.sleep(5 * 1000);
	        				VWSLog.dbg("4th time DB connection try after 5 seconds");
	            			con = DriverManager.getConnection(url, userName, passWord);
	            		} catch (Exception e4) {
	            			VWSLog.dbg("Exception in DBLookup connection 4:"+e4.getMessage());
	            			dbLookupErrorMsg = e4.getMessage();
	            			try {
	            				Util.sleep(5 * 1000);
	            				VWSLog.dbg("5th time DB connection try after 5 seconds");
	                			con = DriverManager.getConnection(url, userName, passWord);
	                		} catch (Exception e5) {
	                			VWSLog.dbg("Exception in DBLookup connection 5:"+e5.getMessage());
	                			dbLookupErrorMsg = e5.getMessage();
	                		}
	            		}
	        		}
	    		}
	    	}
    	}
    	return con;
    }
    public int getFolders(int sid, String folderName, Vector results){	
    	VWSLog.dbgCheck("getFolders method sid val--->"+sid);
	if (!assertSession(sid))
	    return invalidSessionId;
        Client client = getClient(sid);
        String user = client.getUserName();
        String param = folderName + Util.SepChar + user;
        String searchDesc = "Search for folder - [" + folderName.substring(0, folderName.indexOf(Util.SepChar)) + "] by " + user;
        addATRecord(sid, user, client.getIpAddress(), 1, VWATEvents.AT_OBJECT_FIND, VWATEvents.AT_FIND, searchDesc);        
	DoDBAction.get(getClient(sid),  database, "VWSearchNode", param, results);
	return NoError;		
    }	    
    
    
    public int searchNodePath(int sid, String folderName,String folderPath, Vector results){	
    	VWSLog.dbgCheck("getFolders method sid val--->"+sid);
    	if (!assertSession(sid))
    		return invalidSessionId;
    	Client client = getClient(sid);
    	String user = client.getUserName();
    	String param = folderName + Util.SepChar+user+Util.SepChar+folderPath;
    	//VWSLog.add("param searcNodepath:::"+param);
    	String searchDesc = "Search for folder path - [" + folderName.substring(0, folderName.indexOf(Util.SepChar)) + "] by " + user;
    	addATRecord(sid, user, client.getIpAddress(), 1, VWATEvents.AT_OBJECT_FIND, VWATEvents.AT_FIND, searchDesc);        
    	DoDBAction.get(getClient(sid),  database, "VWSearchNodePath", param, results);
    	return NoError;		
    }
    /*Enhancement for custom color*/
    public int getNodeRGBColor(int sid, Vector result){
    	VWSLog.dbgCheck("getNodeRGBColor method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;    	
    	String param = "";					   	
    	//VWSLog.add(" getNodeRGBColor:"+param);
    	DoDBAction.get(getClient(sid),  database, "VWNode_GetRGBColors", param, result);    	
    	return NoError;
    }
    
    public int setNodeCustomColor(int sid, int nodeId, int colorId, boolean type, boolean inherit, int scope)
    {
      	VWSLog.dbgCheck("setNodeCustomColor method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);
    	Vector result = new Vector();
    	int typeInt = (type)?1:0;
    	int inheritInt = (inherit)?1:0;
    	String param =  client.getUserName() + Util.SepChar + 
    					nodeId + Util.SepChar +						
    					colorId + Util.SepChar +     					
    					typeInt + Util.SepChar +
    					inheritInt + Util.SepChar+
    					scope + Util.SepChar;    					
    	//VWSLog.add(" setNodeCustomColor:"+param);
    	DoDBAction.get(getClient(sid),  database, "VWNode_SetCustomColors", param, result);    	
    	return NoError;
    }
    public int getNodeCustomColorCode(int sid, int nodeId, Vector result){
    	VWSLog.dbgCheck("getNodeCustomColorCode method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Client client = getClient(sid);   
    	String param =  client.getUserName() + Util.SepChar + 
						nodeId + Util.SepChar;				
    	DoDBAction.get(getClient(sid),  database, "VWNode_GetCustomColor", param, result);
    	//VWSLog.add(" getNodeCustomColorCode:"+result);
    	return NoError;	
    }
    public int deleteNodeCustomColorCode(int sid, int nodeId){
    	VWSLog.dbgCheck("deleteNodeCustomColorCode method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	Vector result = new Vector();
    	Client client = getClient(sid);   
    	String param =  client.getUserName() + Util.SepChar + 
						nodeId + Util.SepChar;				
    	DoDBAction.get(getClient(sid),  database, "VWNode_DelCustomColor", param, result);
    	//VWSLog.add(" deleteNodeCustomColorCode:"+result);
    	return NoError;	
    }
// This procedure name has to be changed - depends on the sp given by Srikanth
	public int getAvailableRetentions(int sid, Vector availRetention) {
	 	VWSLog.dbgCheck("getAvailableRetentions method sid val--->"+sid);
		String param = "";// Need to know what is the parameter need to pass
		if (!assertSession(sid)) return invalidSessionId;
	        return DoDBAction.get(getClient(sid),  database, "GetAvailableRetention",param, 
	                                        availRetention);
	}
	/**
	 * getRetentionSettings - This method will call the sp :: VW_ARS_GetRetentionSettings
	 * @param sid
	 * @param retentionId
	 * @param availRetention
	 * @return
	 */
	public int getRetentionSettings(int sid, int retentionId, Vector availRetention) {
	 	VWSLog.dbgCheck("getRetentionSettings method sid val--->"+sid);
		String param = String.valueOf(retentionId);
		if (!assertSession(sid)) return invalidSessionId;
	        return DoDBAction.get(getClient(sid),  database, "VW_ARS_GetRetentionSettings",param, 
	                                        availRetention);
	}
	public int getRetentionIndexInfo(int sid, int retentionId, Vector availRetention) {
		VWSLog.dbgCheck("getRetentionIndexInfo method sid val--->"+sid);
		String param = String.valueOf(retentionId);
		if (!assertSession(sid)) return invalidSessionId;
	        return DoDBAction.get(getClient(sid),  database, "VW_ARS_GetIndexInfo",param, 
	                                        availRetention);
	}
	public int setRetentionSettings(int sid, VWRetention retention, Vector retentionId) {
		VWSLog.dbgCheck("setRetentionSettings method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        /*Check Insertion or updation
         For this check the retention id is present or not of present pass that id else 0.*/
        int retId = retention.getId();
        if(retId==-1)
        	retId = 0;
        
        String param = client.getUserName() + Util.SepChar +
                       client.getIpAddress()+ Util.SepChar +                     
                       retId + Util.SepChar + 
                       retention.getName() + Util.SepChar +                 
                       retention.getDocTypeId() + Util.SepChar +
                       retention.getStatus() + Util.SepChar +// Enable retention check for status how to send
                       retention.getActionId() + Util.SepChar +
                       retention.getLocation() + Util.SepChar +
                       retention.getNotifyBefore() + Util.SepChar +
        			   retention.getReportLocation() + Util.SepChar +
        			   retention.getSendMail() + Util.SepChar +
        			   retention.getMailIds()+ Util.SepChar +
        			   //Added for Retention based on folder policy enhancement,Gurumurthy.T.S 22-05-2012
        			   retention.getRetentionLocationId();
        
        //VWSLog.add("setRetentionSettings, param is :: "+param);
        DoDBAction.get(getClient(sid),  database, "VW_ARS_SetRetentionSettings", param, retentionId);
         if (retentionId != null && retentionId.size() > 0){
        	try{
        		int updatedRetId = Integer.parseInt(retentionId.get(0).toString());
        		if(updatedRetId>0){
        			message = "Retention '"+retention.getName()+"' updated.";
        			addNotificationHistory(sid, retention.getId(), VNSConstants.nodeType_Retention, VNSConstants.notify_Retention_Update, client.getUserName(), "-", "-", message);
        		}
        		return Integer.parseInt(retentionId.get(0).toString());
        	}catch (Exception ex){
        		return Error;
        	}
        }
        return NoError;
	}
	public int setIndexConditions(int sid, VWRetention retention) {
		if (!assertSession(sid)) return invalidSessionId;
		VWSLog.dbgCheck("setIndexConditions method sid val--->"+sid);
        Vector<IndicesCondition> indicesCondition = retention.getIndicesCondition();
        int idxConditionCount = indicesCondition.size();
        String idx = "";
        for(int i=0; i < idxConditionCount; i++){
	        Vector ret = new Vector();
	        Client client = getClient(sid);
	        String param = retention.getId() + Util.SepChar+
	                       retention.getDocTypeId() + Util.SepChar + 
	                       ((IndicesCondition) indicesCondition.elementAt(i)).getIndexId() + Util.SepChar +
	                       ((IndicesCondition) indicesCondition.elementAt(i)).getIndexName() + Util.SepChar +
	                       ((IndicesCondition) indicesCondition.elementAt(i)).getComparisonOpert() + Util.SepChar + 
	                       ((IndicesCondition) indicesCondition.elementAt(i)).getIndexValue1() +Util.SepChar + 
	                       ((IndicesCondition) indicesCondition.elementAt(i)).getIndexValue2() + Util.SepChar + 
	                       ((IndicesCondition) indicesCondition.elementAt(i)).getLogicalOpert();                     
	        DoDBAction.get(getClient(sid),  database, "VW_ARS_SetIndexInfo", param, ret);
        }
        return NoError;
        
        /*
         * for(int count = 0; count < indices.size(); count++){
	    Index selectedIndex = (Index) indices.get(count);
	    String param = "";
	    int isTriggerColumn = 0;
	    if (selectedIndex.getId() == dt.getTriggerIndexId()) {
		isTriggerColumn = 1;
	    }
	    param = dt.getId() + Util.SepChar + 
	    selectedIndex.getId() + Util.SepChar + 
	    selectedIndex.getExternalColumn() + Util.SepChar + 
	    lookupInfo.getTriggerIndexDataType() + Util.SepChar +
	    isTriggerColumn + Util.SepChar + selectedIndex.getMask(); 
	    DoDBAction.get(getClient(sid),  database, "VW_DSNSetIndexMapping", param, new Vector());    
	}	
         */
	}
	/**
	 * delRetentionSettings - This method is used to delete the retention settings
	 * @param sid
	 * @param retention
	 * @return
	 */
	public int delRetentionSettings(int sid, VWRetention retention, int deleteOption) {
		VWSLog.dbgCheck("delRetentionSettings method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		
		
		int settingsId = CheckNotificationExist(sid, retention.getId(), VNSConstants.nodeType_Retention, VNSConstants.notify_Retention_Delete, client.getUserName());
		String param = String.valueOf(retention.getId()) + Util.SepChar+
						deleteOption + Util.SepChar +
						client.getUserName() + Util.SepChar +
						client.getIpAddress()+ Util.SepChar ;
		Vector resultRetention = new Vector();
		DoDBAction.get(getClient(sid),  database, "VW_ARS_DelRetentionSettings", param, resultRetention);
		
		message = "Retention '"+retention.getName()+"' Deleted.";
		//Retenetion deleted from AdminWise will capture and for update retention (delete and create ) notification should not capture 
		if (deleteOption == 1 && settingsId>=0){
			setNotificationHistory(sid, 0, retention.getId(), VNSConstants.nodeType_Retention, VNSConstants.notify_Retention_Delete, "-", "-", client.getUserName(), message);
			//addNotificationHistory(sid, retention.getId(), VNSConstants.nodeType_Retention, VNSConstants.notify_Retention_Delete, client.getUserName(), "-", "-", message);
		}

	    return NoError;
	}
	
	public int delIndexInfo(int sid, VWRetention retention) {
		VWSLog.dbgCheck("delIndexInfo method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		String param = String.valueOf(retention.getId());
		Vector resultRetention = new Vector();
	    DoDBAction.get(getClient(sid),  database, "VW_ARS_DelIndexInfo", param, resultRetention);
	    return NoError;
	}
	public int checkRetentionInUse(int sid, int retentionId, Vector retention) {
		VWSLog.dbgCheck("checkRetentionInUse method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
        Client client = getClient(sid);
        
        String param = String.valueOf(retentionId) + Util.SepChar;
        
        DoDBAction.get(getClient(sid),  database, "VW_ARS_ChkRetentionUsed", param, retention);
        if (retention != null && retention.size() > 0){
        	try{
        		return Integer.parseInt(retention.get(0).toString());
        	}catch (Exception ex){
        		return Error;
        	}
        }
        return NoError;
	}
	
	//Stored procedure not yet created
	public int isRetentionNameFound(int sid, String retentionName){
		VWSLog.dbgCheck("isRetentionNameFound method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
    	String param = 	""+retentionName;	
    	Vector result = new Vector();
    	DoDBAction.get(getClient(sid),  database, "VW_ARS_IsRetentionNameFound", param, result);
    	int ret = 0;
    	if (result != null && result.size() > 0){
    		ret = Util.to_Number(result.get(0).toString());
    	}
    	return ret;
    }
	
	public int getRetentionDocumentList(int sid, int retentionId, Vector retentionDocs) 
    {
		VWSLog.dbgCheck("getRetentionDocumentList method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(retentionId);
        DoDBAction.get(getClient(sid),  database, "VW_ARS_AutoRetention", param, retentionDocs);
        return NoError;
    }
	public int getRetentionCompletedDocs(int sid, int retentionId, Vector retentionDocs) {
		VWSLog.dbgCheck("getRetentionCompletedDocs method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		String param = "0" + Util.SepChar + String.valueOf(retentionId) + Util.SepChar;

	    return DoDBAction.get(getClient(sid),  database, "VW_ARS_GetRetentionCompDocs", param, retentionDocs);
	}
	public int getNotifyDocumentList(int sid, Vector notifyDocs) {
		VWSLog.dbgCheck("getNotifyDocumentList method sid val--->"+sid);
		String param = "";
		//commented assertedSession for -499 ISSUE Check
		if (!assertNotifySession(sid)) return invalidSessionId;
	        return DoDBAction.get(getClient(sid),  database, "VW_ARS_Notify",param, 
	        		notifyDocs);
	}
	public int addNotifiedDocs(int sid, int retentionId, int docID, int docTypeId)
	{
		VWSLog.dbgCheck("addNotifiedDocs method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);

		String param = retentionId + Util.SepChar +
		docID + Util.SepChar +
		docTypeId+ Util.SepChar;
		
		int ret = DoDBAction.get(getClient(sid),  database, "VW_ARS_AddNotifiedDocs", param, new Vector());
		return NoError;
	}
	public int sendNotification(String room, int sid, int VWSCount, String message ) throws RemoteException{
		VWSLog.dbgCheck("sendNotification method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		
		int ret = VWMail.sendMailNotificationOnVSM(message, VWSCount);
		//VWSLog.add("In SendNotification, ret :: "+ret);
		return 108;
	}
	/*
     * This method used to get the list of users for the selected group 
     */
    public Vector getGroupMembers(int sid, String groupId)
    {
    	VWSLog.dbgCheck("getGroupMembers method sid val--->"+sid);
		Vector memberList = new Vector();
		if (!assertSession(sid)) return memberList;
		try{	    	
		    String param = groupId;
		    DoDBAction.get(getClient(sid),  database, "VW_GetMemberList", param, memberList);
		}catch(Exception e){
		    return memberList;
		}
		return memberList;
    }
    public int isDSSActive(String room, int sid){
    	try{
    		Client client = getClient(sid);
    		ServerSchema ss = client.getSchema();
    		DSS dss = null;
    		dss = (DSS) Util.getServer(ss);


    		if(dss!=null)
    			return 0;
    		else
    			return dssInactive;
    	}catch(Exception ex){
    		return -1;
    	}
    }
    public boolean getPurgeDocs(String room, int sid, Vector purgeDocsFlag){
		return ARSPreferences.getPurgeDocs();
    }
    
    public boolean getKeepDocType(String room, int sid, Vector keepdoctypeFlag){
    	boolean keepDocType = getServerSettingsInfo(sid, "Enable DocType In Retention");
		return keepDocType;
    }
	public int setPurgeDocs(String room2, int sid, String purgeDocsFlag) {
		try{
    		ARSPreferences.setPurgeDocs(purgeDocsFlag);
    		return 0;
    	}catch(Exception ex){
    		return Error;
    	}
	}
	public int setKeepDocType(String room2, int sid, String keepDocTypeFlag) {
		try{
    		ARSPreferences.setKeepDocType(keepDocTypeFlag);
    		return 0;
    	}catch(Exception ex){
    		return Error;
    	}
	}
	public int searchByIndex(int sid, String docTypeName, String indexName, String indexValue, int conditionType, int includeCustom, Vector documents) {		
		try{
			Client client = getClient(sid);
			String param = includeCustom + Util.SepChar + conditionType + Util.SepChar + 
							((docTypeName != null && docTypeName.length() ==0) ?"-":docTypeName)  + Util.SepChar +	
							((indexName != null && indexName.length() ==0) ?"-":indexName)  + Util.SepChar +	
							indexValue;
			
						   
			DoDBAction.get(client,  database, "VWSearchByIndexName", param, documents);
    	}catch(Exception ex){
    		return Error;
    	}
    	return NoError;
	}
	public int revokeDocSignature(int sid, int documentId) {
    	VWSLog.dbgCheck("revokeDocSignature method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);

		String param = ""+documentId;
		
		int ret = DoDBAction.get(getClient(sid),  database, "VWRevokeDocSignature", param, new Vector());
		return NoError;
	}
	
	
	public int getARSServer(String room, int sid){
		try{
			Client client = getClient(sid);

			String type = Util.SERVER_ARS;
			String host = "";
			int port = 0;

			Vector servers = ManagerPreferences.getServers(true);
			if(servers!=null && servers.size()>0){
				for(int i=0; i<servers.size(); i++){
					String serverType= ((ServerSchema) servers.elementAt(i)).type;
					if(serverType.equalsIgnoreCase(Util.SERVER_ARS)){
						host = ((ServerSchema) servers.elementAt(i)).address;
						port = ((ServerSchema) servers.elementAt(i)).comport;
					}
				}

				if(host != null && !host.equals("")){
					CIServer cServer = (CIServer) Util.getServer(type, host, port);
					ARS ars = (ARS) cServer;
					int ret = NoError;//0

					if(ars==null)
						ret = Error;//-1

					return ret;
				}else
					return Error;
			}		
			return Error;
		}catch(Exception ex){
			return Error;
		}
	}
	public int getTreeDetails(int sid, Vector treeDetails) {
		VWSLog.dbgCheck("getTreeDetails method sid val--->"+sid);
		if(!assertSession(sid)) return invalidSessionId;
		// 0 for getting the node details; 1 for getting the node details along with document info
		String param = 0+Util.SepChar;
		DoDBAction.get(getClient(sid), database, "VWGenTreeView", param, treeDetails);
		return NoError;
	}
	public int saveNotificationSettings(int sid, NotificationSettings settings) {
		VWSLog.dbgCheck("saveNotificationSettings method sid val--->"+sid);
		if(!assertSession(sid)) return invalidSessionId;
		Vector output = new Vector();
		String notifyIds = "";
		try{
			if (settings.getNotifications() != null && settings.getNotifications().length > 0 ){
				for (int i = 0; i < settings.getNotifications().length; i++){
					notifyIds += settings.getNotifications()[i].getId() + Util.SepChar; 
				}
			}
			String param = settings.getReferenceId() + Util.SepChar +
			settings.getNodeType() + Util.SepChar +
			settings.getNotifyCaptureType() + Util.SepChar +
			settings.getProcessType() + Util.SepChar +
			settings.getFolderInherit() + Util.SepChar +
			settings.getDocumentInherit() + Util.SepChar +
			settings.getUserName() + Util.SepChar +
			settings.getIpAddress() + Util.SepChar +
			settings.getAttachment() + Util.SepChar +
			settings.getEmails() + Util.SepChar +
			settings.getNotifications().length + Util.SepChar +
			notifyIds;
			DoDBAction.get(getClient(sid), database, "VW_NFSetSettings", param, output);
		}catch(Exception ex){
			return Error;
			//VWSLog.add("Exception while saveNotificationSettings : "+ex.getMessage());
		}
			return NoError;
		}
	
	public int loadNotificationSettings(int sid, int nodeId, int isAdmin, String userName, String module, Vector settings){
		VWSLog.dbgCheck("loadNotificationSettings method sid val--->"+sid);
		if(!assertSession(sid)) return invalidSessionId;
		String param = 	nodeId + Util.SepChar + 
						isAdmin + Util.SepChar + 
						userName + Util.SepChar +
						module + Util.SepChar;
		DoDBAction.get(getClient(sid), database, "VW_NFGetSettings", param, settings);
		return NoError;
	}
	
	public int getNFNodeName (int sid, int nodeId, String module, Vector settings){
		VWSLog.dbgCheck("getNFNodeName method sid val--->"+sid);
		if(!assertSession(sid)) return invalidSessionId;
		String param = 	nodeId + Util.SepChar + 
						module + Util.SepChar;
		DoDBAction.get(getClient(sid), database, "VW_NFGetNodeName", param, settings);
		return NoError;
	}

	public int addNotificationHistory(int sid, int nodeId, int nodeType, int notifyId, String userName, String oldValue, String newValue, String message){
		int settingsId = CheckNotificationExist(sid, nodeId, nodeType, notifyId, userName);
		if(/*nodeType == VNSConstants.Folder_TYPE && nodeId>0 && */settingsId>0)
		{
			if(settingsId!=nodeId){
				nodeType = 0;//Update folder node type
			}
		}
		if (settingsId >= 0){
			/**
			 * Other than Document and Folder module, we should pass 0 for the settingsId
			 */
			if(nodeType!=0 && nodeType!=1){
				settingsId = 0;
			}
			setNotificationHistory(sid, settingsId, nodeId, nodeType, notifyId, oldValue, newValue, userName, message);
		}
		return NoError;
	}
	public int CheckNotificationExist(int sid, int nodeId, int nodeType, int notifyId, String userName){
		VWSLog.dbgCheck("CheckNotificationExist method sid val--->"+sid);
		int settingsId = 0;
		if(!assertSession(sid)) return invalidSessionId;
		Vector output = new Vector();
		String param = 	nodeId + Util.SepChar +
						nodeType + Util.SepChar +
						notifyId + Util.SepChar +
						userName ;	
		DoDBAction.get(getClient(sid), database, "VW_NFChkEventExists", param, output);
		if (output != null && output.size() > 0){
			settingsId = Integer.parseInt(output.get(0).toString());
		}
		return settingsId;
	}
	
	public int setNotificationHistory(int sid, int folderId, int nodeId, int nodeType, int settingsId, String oldValue, String newValue, String modifiedUser, String message){
		VWSLog.dbgCheck("setNotificationHistory method sid val--->"+sid);
		if(!assertSession(sid)) return invalidSessionId;
		Vector output = new Vector();
		String param =  folderId + Util.SepChar + 	
						nodeId + Util.SepChar +
						nodeType + Util.SepChar +
						settingsId + Util.SepChar +
						"-" + Util.SepChar +//OldValue - For future
						newValue + Util.SepChar +
						modifiedUser + Util.SepChar +
						message;
		DoDBAction.get(getClient(sid), database, "VW_NFSetHistory", param, output);
		return NoError;
	}
	/**
	 * readNotificationDetails is to get the Notification history from database.
	 * 			FailedFlag 0/1 : 
	 * 			0 means user will get all the notifications whose sentflag is 0, 
	 * 			1 means user will get all the notification whose sentflag is 1;
	 * @param sid
	 * @param nodeId
	 * @param userName
	 * @param FailedFlag
	 * @param notificationHistory
	 * @return 
	 */
	public int readNotificationDetails(int sid, String FailedFlag, String NotificationRetries, Vector notificationHistory){
		String param = FailedFlag + Util.SepChar; 
					   DoDBAction.get(getClient(sid), database, "VW_NFGetHistory", param, notificationHistory);
		return NoError;
	}
	/**
	 * updateNotificationHistory - will set the notificastion for the historyId whethere notification sent or not.
	 * @param sid
	 * @param historyId
	 * @param sentFlag
	 * @param failedFlag
	 * @return
	 */
	public int updateNotificationHistory(int sid, int sentFlag,int failedFlag, int historyCount, String historyIds){
		VWSLog.dbgCheck("updateNotificationHistory method sid val--->"+sid);
		if(!assertSession(sid)) return invalidSessionId;
		Vector output = new Vector();
		String param = 	sentFlag + Util.SepChar +
						failedFlag + Util.SepChar +
						historyCount + Util.SepChar +
						historyIds;//Already historyIds is coming with tab char
		DoDBAction.get(getClient(sid), database, "VW_NFUpdateHistory", param, output);
		return NoError;
	}
	
	public int deleteNotificationSettings(int sid, NotificationSettings settings){
		VWSLog.dbgCheck("deleteNotificationSettings method sid val--->"+sid);
		if(!assertSession(sid)) return invalidSessionId;
		String param = 	settings.getReferenceId() + Util.SepChar +
						settings.getNodeType() + Util.SepChar + 
						settings.getUserName() + Util.SepChar;
		Vector ret = new Vector();
		DoDBAction.get(getClient(sid), database, "VW_NFDelSettings", param, ret);		
		return NoError;
	}

	public int getNodeName(int sid, int nodeId, Vector result){
		VWSLog.dbgCheck("getNodeName method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
        String param = String.valueOf(nodeId);       
        DoDBAction.get(getClient(sid),  database, "Node_GetName", param, result);
        return NoError;
	}
	/**
	 * getNotificationMasters - will give all the id, desc and module for the param : "1	-	"
	 * For getting the module related events need to send '0	Document	'
	 * @param sid
	 * @param result
	 * @return
	 */
	
	public Hashtable<Integer, String> masterTable = new Hashtable<Integer, String>();
	public int getNotificationMasters(int sid, int option, String module, Vector result){
		VWSLog.dbgCheck("getNotificationMasters method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		String param = option + Util.SepChar + 
					   module + Util.SepChar;
		Hashtable<Integer, String> master = new Hashtable<Integer, String>();
		
		if(masterTable!=null && masterTable.size()>0){
			result.add(masterTable);
		}
		else{
			Vector local = new Vector();
			DoDBAction.get(getClient(sid),  database, "VW_NFGetWatch", param, local);
			master = getVectorToHashtable(local);
			masterTable = master;
			result.add(master);
		}
        return NoError;
	}
	
	public int getNotificationList(int sid, Vector result){
		VWSLog.dbgCheck("getNotificationList method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		String param = "1" + Util.SepChar + 
					   "-" + Util.SepChar;
			Vector local = new Vector();
			DoDBAction.get(getClient(sid),  database, "VW_NFGetWatch", param, result);
        return NoError;
	}

	
	private Hashtable<Integer, String> getVectorToHashtable(Vector result) {
		Hashtable<Integer, String> notifyIdMasterTable = new Hashtable<Integer, String>();
		StringTokenizer st;
		for(int i=0; i<result.size(); i++){
			st = new StringTokenizer(result.get(i).toString(), "\t");
			//Id, event desc
			notifyIdMasterTable.put(Integer.parseInt(st.nextToken()), st.nextToken());
			st.nextToken();//ModuleName
		}
		return notifyIdMasterTable;
	}
	//This is method used to get the sign annotation settings read from client (registry) or server(database)
	public int isSignAnnotationExist(int sid, Vector settings){
		VWSLog.dbgCheck("isSignAnnotationExist method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        DoDBAction.get(getClient(sid),  database, "VWGetSignatureSettings", param, settings);	
		return NoError;
	}
	
	public int getSignSettings(int sid, Vector settings)
    {
		VWSLog.dbgCheck("getSignSettings method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        return DoDBAction.get(getClient(sid),  database, "VWGetSignSettings", param, settings);
    }
	public int getSignTitle(int sid, Vector result) {
		VWSLog.dbgCheck("getSignTitle method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        return DoDBAction.get(getClient(sid),  database, "VWGetSignTitle", param, result);
	}
	public int getIndexId(int sid, Index index) {
		VWSLog.dbgCheck("getIndexId method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		Vector result=new Vector();
		String param = index.getName() + Util.SepChar ;
		DoDBAction.get(getClient(sid),  database, "VWGetIndexid", param, result);
		if(result==null || result.size()==0) return Error;
		return Util.to_Number((String)result.get(0));
	}
	public int getOptionSettings(int sid, Vector result){
		VWSLog.dbgCheck("getOptionSettings method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;		
		String param = "";
		DoDBAction.get(getClient(sid),  database, "VWGetSystemSettings", param, result);
		
		return NoError;
	}
	public int setOptionSettings(int sid, String param){
		VWSLog.dbgCheck("setOptionSettings method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		DoDBAction.get(getClient(sid),  database, "VWSetSystemSettings", param, new Vector());
	
		return NoError;
	}
	public int getTaskApproversCount(int sid, int routeId, int currentTaskSequence, int docId) {
		int ret = 0;
		try {
			if (!assertSession(sid)) return invalidSessionId;
			VWSLog.dbgCheck("getTaskApproversCount method sid val--->"+sid);
			String param = routeId + Util.SepChar +
							currentTaskSequence + Util.SepChar +
							docId + Util.SepChar;
			
			Vector result = new Vector();
	    	DoDBAction.get(getClient(sid),  database, "VW_DRSGetTaskApproversCount", param, result);
	    	VWSLog.dbgCheck("VW_DRSGetTaskApproversCount procedure call return value "+result);
	    	
	    	if (result != null && result.size() > 0){
	    		ret = Util.to_Number(result.get(0).toString());
	    	}
	    	//VWSLog.add("getRouteTaskCount " + ret);
		} catch (Exception e) {
			VWSLog.err("Exception in getTaskApproversCount procedure call....."+e.getMessage());
		}
    	return ret;
	}
	public int getWebAccessUrl(int sid, Vector result) {
		VWSLog.dbgCheck("getWebAccessUrl method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        return DoDBAction.get(getClient(sid),  database, "VWGetWebAccessurl", param, result);
	}
	public int getDocIndicesInRoute(int sid, int did, Vector routeIndices){
		if (!assertSession(sid)) return invalidSessionId;
		VWSLog.dbgCheck("getDocIndicesInRoute method sid val--->"+sid);
		String param = did + Util.SepChar;
		DoDBAction.get(getClient(sid),  database, "VW_DRSGetDocIndicesInRoute", param, routeIndices);
		return NoError;
	}
	
	/**
	 * ChangeUserPWD
	 * Input sid princiapl oldPassword newPassword confirmPassword
	 * Output Integer
	 */
	public int changeUserPWD(int sid, Principal principal, String oldPassword, String newPassword, String confirmPassword) {
		VWSLog.dbgCheck("changeUserPWD method sid val--->"+sid);
		if (!assertSession(sid))return invalidSessionId;
		try {
			
			//As per the requirement below check is commented and directly saving the new password into DB
			//Reset of password from Administration cannot require old password.
			
			/*String param = principal.getId() + Util.SepChar + principal.getName();
			// VWSLog.add("paramPass " + param);
			Vector result = new Vector();
			DoDBAction.get(getClient(sid), database, "VWGetUserPWD", param, result);

			String resultStr = "";
			if (result != null && result.size() > 0)
				resultStr = String.valueOf(result.get(0));

			if (resultStr == null || resultStr.trim().length() == 0)
				return Error;

			String resultPass = null;
			String[] resultArray = resultStr.split("\t");

			if (resultArray != null && resultArray.length >= 3) {
				resultPass = resultArray[3];
				if (!resultPass.equals("-"))
					resultPass = Util.decryptKey(resultPass);
			}
			// VWSLog.add("resultPass " + resultPass);

			if (newPassword != null && !newPassword.equals(""))
				newPassword = Util.encryptKey(newPassword);

			param = principal.getId() + Util.SepChar + newPassword;

			if (resultPass.equals("-")) {
				int retAuthenticate = directory.AuthenticateUser(principal.getName(), oldPassword);
				// VWSLog.add("retAuthenticate:::" + retAuthenticate);

				if (retAuthenticate == 0) {
					return Error;
				} else {
					DoDBAction.get(getClient(sid), database, "VWSetUserPWD", param, result);
					return NoError;
				}
			} else {
				// VWSLog.add("oldPassword:::" + oldPassword);
				if (oldPassword.equals(resultPass)) {
					DoDBAction.get(getClient(sid), database, "VWSetUserPWD", param, result);
					return NoError;
				} else {
					return Error;
				}
			}*/
			
			String param = principal.getId() + Util.SepChar + principal.getName();
			Vector result = new Vector();
			if (newPassword != null && !newPassword.equals("")) {
				newPassword = Util.encryptKey(newPassword);
				param = principal.getId() + Util.SepChar + newPassword;
				DoDBAction.get(getClient(sid), database, "VWSetUserPWD", param, result);
			} 
			return NoError;
		} catch (Exception e) {
			return Error;
		}
	}
	
	/**
	 * Method added to Remove the db password and reset to AD or LDAP password
	 * Enhancement :- CV83B5 Date:-22/12/2015
	 * @param sid
	 * @param principal
	 * @param oldPassword
	 * @param newPassword
	 * @param confirmPassword
	 * @return
	 */
	public int resetUserPWD(int sid, Principal principal, String oldPassword, String newPassword, String confirmPassword) {
		VWSLog.dbgCheck("resetUserPWD method sid val--->"+sid);
		if (!assertSession(sid))return invalidSessionId;
		try {
			String param = principal.getId() + Util.SepChar + principal.getName();
			Vector result = new Vector();
			param = principal.getId() + Util.SepChar + newPassword;
			DoDBAction.get(getClient(sid), database, "VWSetUserPWD", param, result);
			return NoError;
		} catch (Exception e) {
			return Error;
		}

	}
	
	/* Method checkKeyIndex is added to do the procedure call 
	 * If the index value is empty it returns the index name 
	 * otherwise returns zero.
	 * Implemented for the document key field change. 
	 * Release :Build5-001
	 * Added By Madhavan
	 */
	public int checkKeyIndex(int sid, DocType docType, Vector indiceslist, int count,Vector result) {
		try{
			VWSLog.dbgCheck("checkKeyIndex method sid val--->"+sid);
			if (!assertSession(sid)) return invalidSessionId;
			StringBuffer indexId = new StringBuffer();
			for (int i = 0; i < indiceslist.size(); i++) {
				indexId.append(indiceslist.elementAt(i).toString());
				indexId.append("\t");
			}
			String param = docType.getId() + Util.SepChar + count + Util.SepChar + indexId;
			DoDBAction.get(getClient(sid),  database, "VWCheckKeyIndex", param, result);
			return NoError;
		}
		catch(Exception exp)
		{
			return Error;
		}

	}
	/*
	 * updateDocTypeKeyField to set the current key field.
	 * Implemented for the document key field change.
	 * Release :Build5-001
	 * Added By Madhavan
	 */
	public int updateDocTypeKeyField(int sid, int oldKeyIndexId, int newKeyIndexId, DocType docType) {	
		try{
			VWSLog.dbgCheck("updateDocTypeKeyField method sid val--->"+sid);
			if (!assertSession(sid)) return invalidSessionId;
			Vector result = new Vector();
			Client client = getClient(sid);
			String param = oldKeyIndexId + Util.SepChar + newKeyIndexId+Util.SepChar+docType.getId()+Util.SepChar+client.getIpAddress()+Util.SepChar+client.getUserName();
			DoDBAction.get(getClient(sid), database, "VWUpdateDocTypeKeyField", param, result);
			return Integer.valueOf(result.toString());
		}
		catch(Exception exp)
		{
			return Error;
		}
	}
	/*
	 * Implemented to check whether the index field mathes in the Backup set and Restoring room,
	 * Before Restoration. Gurumurthy.T.S
	 * Bug 206 CV8B5-002
	 */
	public int restoreIndicesCheck(int sid,DocType docType,int dtindexCount, Vector restoreIndiceCheck,
			Vector result) {
		try
		{
			VWSLog.dbgCheck("restoreIndicesCheck method sid val--->"+sid);
			if (!assertSession(sid)) return invalidSessionId;
			StringBuffer indexNames = new StringBuffer();
			for(int i=0;i<restoreIndiceCheck.size()-1;i++)
			{
				indexNames.append(restoreIndiceCheck.elementAt(i).toString());
				indexNames.append(",");
			}
			indexNames.replace(indexNames.length()-1, indexNames.length(), "");
			String param =docType.getName()+ Util.SepChar + dtindexCount+ Util.SepChar + indexNames;
			DoDBAction.get(getClient(sid), database, "DocRestore_IndexCheck", param, result);
			return NoError;
		}
		catch(Exception ex)
		{
			return Error;

		}
	}
	/*
	 * Added for creating 1.0 version for the Existing Documents of the Document type
	 *  when user clicks on Enable version/revision for the Document Type.
	 *  Added by Gurumurthy.T.S on JAN-08-2014
	 */
	public int GetStorageId(int sid,DocType docType) {
		try
		{
			Vector docTypePath = new Vector();
			String srcPath="",dstPath="";
			VWSLog.dbgCheck("GetStorageId method sid val--->"+sid);
			if (!assertSession(sid)) return invalidSessionId;
			String param = String.valueOf(docType.getId());
			DoDBAction.get(getClient(sid), database, "VWNode_GetStorageid", param, docTypePath);
			if(!docTypePath.elementAt(0).equals("0")){
				for(int i=0;i<docTypePath.size();i++)
				{
					String docIdWithImagePathStr = docTypePath.elementAt(i).toString();
					String docIdWithImagePathArray[] = null;
					if(docIdWithImagePathStr!=null && docIdWithImagePathStr.length()>0)
						docIdWithImagePathArray = docIdWithImagePathStr.split("\t");

					if(docIdWithImagePathArray!=null){
						srcPath = docIdWithImagePathArray[1]+File.separator+docIdWithImagePathArray[0]+File.separator+DOC_CONTAINER;
						dstPath = docIdWithImagePathArray[1]+File.separator+docIdWithImagePathArray[0]+File.separator+"1.0";
					}
					if(!new File(dstPath).exists() && new File(srcPath).exists()&& Integer.parseInt(docType.getVREnable())==1){
						boolean retValue = Util.CopyFile(new File(srcPath), new File(dstPath));
						//VWSLog.add("return value from copyFile : "+retValue);

					}
				}
			}
			return NoError;
		}
		catch(Exception ex)
		{
			return Error;

		}
	}
	/**
	 * Method added for enhancement of Create ownership, Sachin Patil , 26-02-2014
	 * @param sid
	 * @param sessionName
	 * @param type
	 * @param nodes
	 * @param result
	 * @return
	 */
	 
	public int createOwnership(int sid, String sessionName, int type, Vector nodes,Vector result)
	{
		if (!assertSession(sid)) return invalidSessionId;
		VWSLog.dbgCheck("createOwnership method sid val--->"+sid);
		try{
			int count = 0;
			Client client = getClient(sid);
			String param =  sessionName + Util.SepChar +
					client.getUserName()+ Util.SepChar +
					client.getIpAddress() + Util.SepChar +
					(client.isAdmin()?"1":"0")+ Util.SepChar +
					type + Util.SepChar ;

			if(nodes != null && nodes.size() > 0){
				String idStr="";
				count = nodes.size();
				for(int i=0; i<nodes.size(); i++){
					Node node = (Node) nodes.get(i);
					idStr += node.getId() + Util.SepChar;
				}
				param += count + Util.SepChar + idStr;
			}else{
				param += count + Util.SepChar;
			}
			DoDBAction.get(getClient(sid),  database, "Node_CreateOwnership", param, result);
			return NoError;
		}
		catch(Exception ex){
			return Error;
		}
	}
	/**
	 * Method added for enhancement of Release ownership, Sachin Patil , 26-02-2014
	 * @param sid
	 * @param node
	 * @param type
	 * @return
	 */
	public int releaseOwnership(int sid, Node node, int type)
	{
		try{
			VWSLog.dbgCheck("releaseOwnership method sid val--->"+sid);
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			String param = String.valueOf(node.getId()) + Util.SepChar +
					String.valueOf(node.getSessionId())+ Util.SepChar + 
					client.getIpAddress() + Util.SepChar +
					client.getUserName() + Util.SepChar + 
					type;
			DoDBAction.get(getClient(sid),  database, "Node_ReleaseOwnership", param, new Vector());
			return NoError;
		}
		catch(Exception ex){
			return Error;
		}
	}
	/**
	 * Method added for enhancement of Check ownership, Sachin Patil , 26-02-2014
	 * @param sid
	 * @param type
	 * @param nodes
	 * @return
	 */
	public Vector checkOwnership(int sid, int type, Vector nodes ){
		Vector ret=new Vector();
		try{
			int count = 0;
			String nodeIds = "";
			if(nodes != null && nodes.size()>0){
				count = nodes.size();
				for(int i= 0;i< nodes.size(); i++){
					Node node = (Node) nodes.get(i);
					nodeIds += node.getId()+ Util.SepChar;
				}
			}
			Client client = getClient(sid);
			String param =  client.getUserName()+ Util.SepChar +
					(client.isAdmin()?"1":"0")+ Util.SepChar +
					type+ Util.SepChar + 
					count+ Util.SepChar +
					nodeIds+ Util.SepChar ;
			DoDBAction.get(getClient(sid),  database, "Node_CheckOwnership", param, ret);
		}
		catch(Exception ex){
			VWSLog.add("Exception in checkOwnership : "+ ex.getMessage());
		}
		return ret;
	}
	
	public void ForceFTS(int sid, int docId) {
		try{
			Vector ret=new Vector();
			Client client = getClient(sid);
			String param =  String.valueOf(docId)+Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "VWForceFTS", param,ret);
		}
		catch(Exception ex){
			VWSLog.add("Exception in ForceFTS : "+ ex.getMessage());
		}
	}
	/*
	 * Method to get the latest docid used to 
	 * get DSS Low Disk Free space (Add for DSS low disk space enhancement)
	 */
	public Vector getLatestDocId(int sid){
		Vector ret=new Vector();
		try{
		Client client = getClient(sid);
		String param =  client.getUserName();
		DoDBAction.get(getClient(sid),  database, "VW_GetMaxDocIDInfo", param, ret);
		}
		catch(Exception exp){
		VWSLog.add("Exception in Getting Latest DocId"+exp.getMessage());
		}
		return ret;
	}
	
	/*
	  * Method to get the childnode used to 
	  * get DSS Low Disk Free space for copy and paste
	  */
	public Vector getChildNodes(int sid,int nodeid){
		Vector ret=new Vector();
		try{
			Client client = getClient(sid);
			String param =  client.getIpAddress()+ Util.SepChar+nodeid;
			DoDBAction.get(getClient(sid),  database, "VWNode_GetAllChildren", param, ret);

		}
		catch(Exception exp){
			VWSLog.add("Exception in Getting child Nodes"+exp.getMessage());
		}
		return ret;
	}
	//-----------------------------------------------------------------------------------------//
	public Vector getDocumentVersions(int sid, int did, String version)
	{
		Vector vers = new Vector();
		String param = String.valueOf(did) + Util.SepChar + version+ Util.SepChar;
		DoDBAction.get(getClient(sid),  database, "VR_GetDocVersions_DocType", param, vers);
		return vers;
	}
	/**
	 * Returns the permission value to enable the lock document menu for the node
	 * @param sid
	 * @param nodeid
	 * @return 1 if the folder is having permission to copy or else 0
	 */
	public int CheckLockDocTypePerm(int sid,int nodeid){
		Vector result=new Vector();
		int outputValue = 0;
		Client client = getClient(sid);
		String param =  client.getUserName()+Util.SepChar+(client.isAdmin()?"1":"0")+Util.SepChar+nodeid+Util.SepChar;
		DoDBAction.get(getClient(sid),  database, "VWNode_CheckLockDocTypePerm", param, result);
		try{
			if (result != null && result.size() > 0){
				outputValue = Integer.parseInt(result.get(0).toString());
			}
		}catch(Exception ex){
			outputValue = 0;
		}

		return outputValue;
	}

	public int getDocumentTypes(int sid, int indexId, Vector indices)
	{
		VWSLog.dbgCheck("getDocumentTypes method sid val--->"+sid);
		try {
			if (!assertSession(sid)) return invalidSessionId;
			return DoDBAction.get(getClient(sid),  database, "GetDocTypes", String.valueOf(indexId), indices);
		} catch (Exception ex) {
			VWSLog.err("EXCEPTION in getDocumentTypes :::: "+ex.getMessage());
			return Error;
		}
	}

	public int getDocumentTypeList(int sid, Vector settingsValue, int nodeId){
		VWSLog.dbgCheck("getDocumentTypeList method sid val--->"+sid);
		try {
			if (!assertSession(sid)) return invalidSessionId;
			DoDBAction.get(getClient(sid),  database, "VWNode_GetDocTypesList", String.valueOf(nodeId)+Util.SepChar, settingsValue);
			return NoError;			
		} catch (Exception ex) {
			VWSLog.err("EXCEPTION in getDocumentTypeList  generalErrorCode(-1) :::: "+ex.getMessage());
			return Error;
		}
	}

	// Read/Write the DocumetType settings information to the Database
	public Vector addDocumentTypeList(int sid, String room, String userName,String settingsValue, int nodeId){
		Vector retValue = new Vector();
		String param = nodeId + Util.SepChar +
				settingsValue + Util.SepChar ;	
		DoDBAction.get(getClient(sid),  database, "VWNode_SetDocTypesList", param, retValue);
		return retValue;
	}

	public int resetDocumentTypeList(int sid, String room, int nodeId){
		VWSLog.dbgCheck("resetDocumentTypeList method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		Vector ret = new Vector();
		DoDBAction.get(getClient(sid),  database, "VWNode_DeleteDocTypesList",String.valueOf(nodeId)+Util.SepChar,ret);
		return NoError;
	}

	public int VWNodeGetDocTypes(int sid, int Nodeid, Vector types)
	{
		VWSLog.dbgCheck("VWNodeGetDocTypes method sid val--->"+sid);
		try {
			if (!assertSession(sid)) return invalidSessionId;
			String param = String.valueOf(Nodeid)+Util.SepChar;
			return DoDBAction.get(getClient(sid),  database, "VWNodeGetDocTypes", param, types);
		} catch (Exception ex) {
			VWSLog.err("EXCEPTION in VWNodeGetDocTypes generalErrorCode(-1)  :::: "+ex.getMessage());
			return Error;
		}
	}

	 /**
	  * Method added to check whether the document types is allowed on the particular cabinet or not
	  * @param sid,session id generated by login method
	  * @param room,name of the room
	  * @param flag,value will bhe 1(cabinet type) or 2(document type) depending on the type
	  * @param sourceNodeId
	  * @param destinationNodeId
	  * @param result
	  * @return 0 for sucess else 0 for failure
	  */
	public int ValidateNodeDocType(int sid, String room,int flag,int sourceNodeId, int destinationNodeId,Vector result){
		VWSLog.dbgCheck("ValidateNodeDocType method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		try{
			Client client = getClient(sid);
			String param = String.valueOf(flag)+Util.SepChar+String.valueOf(sourceNodeId)+Util.SepChar+String.valueOf(destinationNodeId)+Util.SepChar+client.getIpAddress()+Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "VWValidateNodeDocType",param,result);
		}
		catch(Exception ex)
		{
			return Error;
		}
		return NoError;
	}
	/**
	 * Method creates a version when user changes the document type of a document. 
	 * @param sid,session id generated by login method
	 * @param doc is the document object
	 * @param version is the version need to create
	 *  @return 0 for sucess else 0 for failure
	 */
	public int createVersionChangeDocType(int sid,Document doc,Vector version){
		VWSLog.dbgCheck("createVersionChangeDocType method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		String srcPath="",dstPath="";
		try{
			ServerSchema desss = new ServerSchema();
			Vector ret = new Vector();
			String param = "";
			param = "4" + Util.SepChar+doc.getId();
			DoDBAction.get(getClient(sid),  database, "Storage_Get", param , ret);
			if (ret.size() > 0){
				ServerSchema dss = new ServerSchema((String) ret.get(0));
				/* If there is some issue in getting storage location. Eg. for some of the cabinet storage 
				 * location is specified and for some it is not specified. Then, ServerSchema constructor is
				 * throwing exception. So catching it and returing -1 for that.*/
				if(dss.getId() == -1 && dss.getHostName().equalsIgnoreCase(""))
					return Error;
				dss.type = SERVER_DSS;
				dss.path = dss.path + Util.pathSep + String.valueOf(doc.getId()) 
						+ Util.pathSep;
				if(dss.path!=null ){
					srcPath = dss.path+DOC_CONTAINER;
					dstPath =  dss.path+File.separator+version.get(0).toString();
				}
			}
			if(!new File(dstPath).exists() && new File(srcPath).exists()){
				boolean retValue = Util.CopyFile(new File(srcPath), new File(dstPath));
			}
		}
		catch(Exception e){
			return Error;
		}
		return NoError;
	}
	
	//--------------------------Dynamic WorkFlow----------------------------------------
    public int getRouteUsers(int sid, Vector users)
    {
    	VWSLog.dbgCheck("getRouteUsers method sid val--->"+sid);
        if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        if(isOracle)
        	param = "2"+Util.SepChar;
        else
        	param = "2";
        return DoDBAction.get(getClient(sid),  database, "User_Get", param, users);
    }
    
    public int drsValidateTaskDocument(int sid,int docId,int routeId,int action,String routeTaskSequence,Vector result)
    {
       	VWSLog.dbgCheck("drsValidateTaskDocument method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
        String param = "";
        	param = docId+Util.SepChar+routeId+Util.SepChar+String.valueOf(action)+Util.SepChar+routeTaskSequence+Util.SepChar;
        return DoDBAction.get(getClient(sid),  database, "VW_DRSValidateTaskDocument", param, result);
    }
    
    public int getTaskIndicesInRoute(int sid, int did, Vector taskIndices){
    	VWSLog.dbgCheck("getTaskIndicesInRoute method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		String param = did + Util.SepChar;
		DoDBAction.get(getClient(sid),  database, "VW_DRSGetCurrentTask_DynamicIndices", param, taskIndices);
		return NoError;
	}
    /**
     * Method added to check whether the document type is allowed to place in a node where custom document type is set.
     * @param sid,session id generated by login method
     * @param docid,document id
     * @param retentionId,retention id
     * @param result,will contain documents for retention
     * @return 0
     */
    public int getArsValidationFinalAction(int sid,int docid,String retentionId,Vector result){
		String param=docid+Util.SepChar+retentionId+Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VW_ARSValidateFinalAction", param, result);
    	return  NoError;
    }
    public int getUserNameInOrder(int sid, String previousValue,String currentValue,Vector result) throws RemoteException {
    	String param=previousValue+Util.SepChar+currentValue+Util.SepChar;
    	DoDBAction.get(getClient(sid),  database, "VWGetDocDynamicUsers", param, result);
    	return  NoError;
	}
    /**
     * Method added to get the named user counts form the database,Added for Named user count license enhancement
     * @param sid,session id generated by the login method
     * @param groupName,name of the group for which we need to get the count
     * @return returns the number of users in the group(namedUserCount)
     * @throws RemoteException
     */
    public int getNamedUserCountInDB(int sid,String groupName) throws RemoteException {
    	String param=groupName+Util.SepChar;
    	Vector resultVect=new Vector();
    	int namedUserCount=0;
    	try{
    		DoDBAction.get(getClient(sid),  database, "VWGetNamedUsersCount", param,resultVect);
    		if(resultVect!=null&&!resultVect.isEmpty()&&resultVect.size()>0)
    			namedUserCount=Integer.parseInt((String) resultVect.get(0));
    	}
    	catch(Exception exp){
    	}
    	return  namedUserCount;
    }
    /**
     * Enhancement:- Secruirty Administrator CV10
     * Added to check if user exist in db or not before login
     * If exist it will allow to login otherwise return error -45 
     * User does not exist in room(Viewise error)
     * @param userName
     * @return
     */
    public int checkLoginUser(String userName,String password,int type)
    {
    	try{
    		String result = "0";
    		Vector resultValue = new Vector();
    		String param = "7"+"\t"+userName;
    		if(type==25) {
    			String param1 = "13"+"\t"+userName+"\t"+password;
    			DoDBAction.get(null,  database, "User_Get", param1,resultValue);
    		}else {
    			DoDBAction.get(null,  database, "User_Get", param,resultValue);
    		}
    		if(resultValue!=null&&resultValue.size()>0){
    			result=(String) resultValue.get(0);
    			if (result.equals("0")) {
    				result ="-45";
    			}
    		} 
    		return Integer.valueOf(result);
    	}
    	catch(Exception ex)
    	{
    		return Error;
    	}
    	
    }
    
    /**
     * Enhacment:- WorkFlow Validation 
     * This methos id used to checkusers and group mistach in workflow 
     * @param sid
     * @param userName
     * @return
     * Date:-24/11/2014
     */
     public int checkUser(int sid,String userName)
     {
     	try{
     		String result = "0";
     		Vector resultValue = new Vector();
     		if(userName.contains("@")){
     			userName=userName.substring(0,userName.lastIndexOf("@"));
     		}
     		String param = "0"+"\t"+userName;
     		DoDBAction.get(getClient(sid),  database, "User_Get", param,resultValue);
     		if(resultValue!=null&&resultValue.size()>0){
     			result=(String) resultValue.get(0);
     		}
     		return Integer.valueOf(result);
     	}
     	catch(Exception ex)
     	{
     		return Error;
     	}
     	
     }
     /**
      * Enhacment:- WorkFlow Validation 
      * This methos id used to checkusers and group mistach in workflow 
      * @param sid
      * @param userName
      * @return
      * Date:-24/11/2014
      */
     public int checkGroup(int sid,String groupName)
     {
     	try{
     		String result = "0";
     		Vector resultValue = new Vector();
     		String param = "0"+"\t"+groupName;
     		DoDBAction.get(getClient(sid),  database, "Group_Get", param,resultValue);
     		if(resultValue!=null&&resultValue.size()>0){
     			result=(String) resultValue.get(0);
     		}
     		return Integer.valueOf(result);
     	}
     	catch(Exception ex)
     	{
     		return Error;
     	}
     	
     }
 
    public int checkValidateSeqIndex(int sid,int indexId,Vector result)throws RemoteException{
    	VWSLog.dbgCheck("checkValidateSeqIndex method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		String param=Integer.toString(indexId)+Util.SepChar;
		try{
		DoDBAction.get(getClient(sid),  database, "VWValidateSeqIndex", param, result);
		
		}
		catch(Exception e){
			return Error;
		}
		return NoError;
    }
    
    public int checkValidateUniqueSeq(int sid,int doctypeId,Vector result)throws RemoteException{
    	VWSLog.dbgCheck("checkValidateUniqueSeq method sid val--->"+sid);
    	if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		String param=Integer.toString(doctypeId)+Util.SepChar;
		try{
		DoDBAction.get(getClient(sid),  database, "VWValidateUniqueSeq", param, result);
		
		}
		catch(Exception e){
			return Error;
		}
		return NoError;
    }
    
    
	public int checkValidateCopySeq(int sid, Node node, int nodeProperties,Vector result) throws RemoteException{
		VWSLog.dbgCheck("checkValidateCopySeq method sid val--->"+sid);
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
	    String param = client.getIpAddress() + Util.SepChar +
 			   client.getUserName()  + Util.SepChar +
                node.getId() + Util.SepChar +
                node.getParentId()+ Util.SepChar +
                // For admin user also security permissions needs to set. 
                (client.isAdmin()?"0":"0") + Util.SepChar + nodeProperties;
		try{
			//updated the procedure name to proper case and commited for CV83B2 
		DoDBAction.get(getClient(sid),  database, "VWCopyNode_ValidateSeq", param, result);
		
		}
		catch(Exception e){
			return Error;
		}
		return NoError;
	}
	
	public int resubmitDoctoWorkFlow(int sid, String doc, Vector result) {
		VWSLog.dbgCheck("resubmitDoctoWorkFlow method sid val--->"+sid);
		// TODO Auto-generated method stub
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		String param=doc+Util.SepChar;
		try{
			DoDBAction.get(getClient(sid),  database, "VWReSubmitDocument", param,result);
		}
		catch(Exception e){
			return Error;
		}
		return NoError;
	}
	public int showhideDoctoWorkFlow(int sid, String doc, Vector result) {
		VWSLog.dbgCheck("showhideDoctoWorkFlow method sid val--->"+sid);
		// TODO Auto-generated method stub
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		String param=doc+Util.SepChar;
		try{
			DoDBAction.get(getClient(sid),  database, "VWShow_HideReSubmit", param,result);
		}
		catch(Exception e){
			return Error;
		}
		return NoError;
	}
	/**
	 * Ehancement:Sow2096_891-Add Default Index with users and groups
	 * Code added get the selection Enableslection registry value to C++.
	 */
	public int getEnableSelect(int sid, String registryValue) {
		VWSLog.dbgCheck("getEnableSelect method sid val--->"+sid);
		// TODO Auto-generated method stub
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			registryValue=VWSPreferences.getEnableSelection();
		}
		catch(Exception e){
			return Error;
		}
		return NoError;

	}
	/**
	 * Added for RightFax Initial Implemention
	 * To get all the cabinets listing for RightFax
	 * @param sid
	 * @return
	 */
	public Vector getAllCabinets(int sid) {
		Vector resultValue = new Vector();
		try{
		String param="";
		DoDBAction.get(getClient(sid),database, "VWGetAllCabinets", param,resultValue);
		}
		catch(Exception ex){
			VWSLog.dbg("Exception while getAllCabinets:::"+ex.getMessage());
		}
		return resultValue;
	}
    /**
     * Method add for new procedure call to get all the DocumentType for right
     * fax Enhancement
     * @param sid
     * @return
     */
	public Vector getFaxDocType(int sid){
		Vector resultValue = new Vector();
		try{
		String param="";
		DoDBAction.get(getClient(sid),database, "VWGet_FaxDocType", param,resultValue);
		}
		catch(Exception ex){
			VWSLog.dbg("Exception while getFaxDocType:::"+ex.getMessage());
		}
		return resultValue;
	}
	
	public int getTodoListResubmit(int sid, String doc,int routeHistoryId,Vector result) {

		// TODO Auto-generated method stub
		if (!assertSession(sid)) return invalidSessionId;
		
		Client client = getClient(sid);
		String param=doc+Util.SepChar+routeHistoryId;
		//VWSLog.dbg("param: " + param);
		try{
			DoDBAction.get(getClient(sid),  database, "VWToDoListReSubmitDoc", param,result);
		}
		catch(Exception e){
			return Error;
		}
		return NoError;
	
	}
	//Added for WorkFlow update Enhancement after cv83b3
	public int getDRSValidateUpdateRoute(int sid,int routeId,String routeName,Vector result) {
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = routeId+ Util.SepChar+routeName+Util.SepChar ;
			DoDBAction.get(getClient(sid),  database, "VW_DRSValidateUpdWorkFlow", param, result);

		}catch(Exception e){
			return Error;
		}

		return NoError;
	}
	  /**
     * Added to get the maintanance flag from procedure call
     * and display warining message in DRS server.
     * Review Update button in adminwise 
     * Enhancement:-Update WorkFlow 
     * Version :- CV83B3
     * @param sid
     * @return
     */
    
	public int getDWSMaintenanceFlag(int sid,Vector result) {
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = "";
			DoDBAction.get(getClient(sid),  database, "VW_DRSGetDWSMaintenanceFlag", param, result);

		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	
	/**
	 * Added to perform the RevieUpdate task by the procedure
	 * once the user clicks the review update button from adminwise
	 * Enhancement:-Update WorkFlow 
	 * Version :- CV83B3
	 * @param sid
	 * @return
	 */
    
	public int getDRSReviewUpdWorkflow(int sid,Vector result) {
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = "";
			DoDBAction.get(getClient(sid),  database, "VW_DRSReviewUpdWorkflow", param, result);

		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	   /**
     * Added to get the flag from procedure call to enable or disable
     * Review Update button in adminwise. 
     * Enhancement:-Update WorkFlow 
     * Version :- CV83B3
     * @param sid
     * @return
     */
    
	public int getDRSGetReviewUpdFlag(int sid,Vector result) {
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = "";
			DoDBAction.get(getClient(sid),  database, "VW_DRSGetReviewUpdFlag", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	
	public int getDRSGetWorkflowStatus(int sid,Vector result) {
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = "";
			DoDBAction.get(getClient(sid),  database, "VW_DRSGetWorkflowStatus", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}	 
	/**
	 * Added for procedure call to get the workflow email information like 
	 * refId,docId,docName,routeId,workflowName,TaskSequence from the result.
	 * Enhancement :- WorkFlow Update CV83B5 Date:-22/12/2015
	 * @param sid
	 * @param result
	 * @return
	 */
	public int workFlowUpdateSendEmail(int sid,Vector result){
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = "";
			DoDBAction.get(getClient(sid),  database, "VW_DRSGetUpdWFSendEmail", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	/**
	 * Method added to call the proceure and remove the entry from table for the passed reference id 
	 * after sending the workflow pending mail for that doucment.
	 * Enhancement :- WorkFlow Update CV83B5 Date:-22/12/2015
	 * @param sid
	 * @param refId
	 * @param result
	 * @return
	 */
	public int delUpdateWorkFlow(int sid,int refId,Vector result) {
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = refId+ Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "VW_DRSDelUpdWFSendEmail", param, result);
		}catch(Exception e){
			return Error;
		}

		return NoError;
	}
	/**
	 * Enhancement :-security report for user/group to confirm access CV10 Date:- 9-2-2016
	 * @param sid
	 * @param selectedPrincipal
	 * @param reportNodeId
	 * @param checkBoxValue
	 * @param result
	 * @return
	 */
	public int generateNodeSecurityReport(int sid, String selectedPrincipal,int reportNodeId, int checkBoxValue,Vector result){
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = selectedPrincipal+ Util.SepChar+String.valueOf(reportNodeId)+Util.SepChar+String.valueOf(checkBoxValue)+Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "CVNodeSecurityReport", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	//Method added for document packaging to get the document page count currently not implemented
	/*public int getDocPageCount(int sid,int nodeid,Vector result){

		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = nodeid+ Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "CVGetDocPageCount", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	
	}*/
	/**
	 * 
	 * Enhancement:-Notify User when document becomes editable 
	 * @param sid
	 * @param nodeid
	 * @param userName
	 * @param result
	 * @return
	 */
	public int setDocstoNotify(int sid,int nodeid,String userName,Vector result){
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = nodeid+ Util.SepChar+userName+Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "CVSetDocstoNotify", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	/**
	 *  Enhancement:-Notify User when document becomes editable 
	 * @param sid
	 * @param nodeid
	 * @param result
	 * @return
	 */
	public int getUsertoNotifyDocs(int sid,int nodeid,Vector result){
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = nodeid+ Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "CVGetUsertoNotifyDocs", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	/**
	 *   Enhancement:-Notify User when document becomes editable 
	 * @param sid
	 * @param nodeid
	 * @param result
	 * @return
	 */
	public int delNotifiedDocs(int sid,int nodeid,Vector result){
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = nodeid+ Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "CVDelNotifiedDocs", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	
	public int setSubAdminRoles(int sid,String user,String roles,int updFlag){
		VWSLog.dbg("inside setSubAdminRoles in RampageServer......");
		if (!assertSession(sid)) return invalidSessionId;
		try{
			//updFlag=1;
			String param = user+ Util.SepChar+roles+Util.SepChar+updFlag+Util.SepChar;
			VWSLog.dbg("param :"+param);
			Vector result=new Vector();
			DoDBAction.get(getClient(sid),  database, "CVSetSubAdminRoles", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	
	public int getSubAdminRoles(int sid,String user,Vector result){
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param = user+Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "CVGetSubAdminRoles", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	
	
	public int removeSubAdminRoles(int sid,String groupName){
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param =groupName+Util.SepChar;
			Vector result=new Vector();
			DoDBAction.get(getClient(sid),  database, "CVDelSubAdminRoles", param, result);
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}
	
	
	public int checkSubAdmin(int sid,String userName,Vector result){
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param =userName+Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "CVChkSubAdmin", param, result);
			
		}catch(Exception e){
			return Error;
		}
		return NoError;
	}

	public int getSubAdminTabs(int sid, String userName, Vector result) {
		
		try {
			String param = userName + Util.SepChar;
			VWSLog.dbg("parameter :" + userName);
			if (sid > 0) {
				if (!assertSession(sid))
					return invalidSessionId;						
				DoDBAction.get(getClient(sid), database, "CVGetSubAdminTabs", param, result);
			} else {
				DoDBAction.get(null, database, "CVGetSubAdminTabs", param, result);
			}
			return NoError;
		} catch (Exception e) {
			return Error;
		}

	}
	
	//Enhancment CV10:- Spliting workflow Todolist 
	public int getDRSValidateToDoList(int sid,String UserName,int nodeId,String receivedDate ,Vector result) {
		
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param =nodeId+Util.SepChar+UserName+Util.SepChar+receivedDate;
			DoDBAction.get(getClient(sid),  database, "VW_DRSValidateToDoList", param, result);
			return NoError;
		}catch(Exception e){
			return Error;
		}
		
	}
	//Enhancement CV10:- Method added to get the color code from db for document list and hitlist in dtc
	public int getGetDocsInRouteColor(int sid,int colortype ,Vector result) {
		if (!assertSession(sid)) return invalidSessionId;
		Client client = getClient(sid);
		try{
			String param =colortype+Util.SepChar;
			DoDBAction.get(getClient(sid),  database, "VW_DRSGetDocsInRouteColor", param, result);
			return NoError;
		}catch(Exception e){
			return Error;
		}
		
	}
	//CV10 Enhancemetns added for WorkFlow Report
		public int getWorkFlowReportOptions(int sid,int  type,String routeId,Vector result) {
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			try{
				String param="";
				if(type==4)
					 param =type+Util.SepChar+routeId+Util.SepChar;
				else
					param =type+Util.SepChar;
				DoDBAction.get(getClient(sid),  database, "VW_DRSWFReportOptions", param, result);
				return NoError;
			}catch(Exception e){
				return Error;
			}
			
		}
		public int sendNotififyDocMail( int sid, int vWSCount,String mailMessage, Vector mailIds) {
			// TODO Auto-generated method stub
			String userEmailId = "";
			int mailSent=0;
			try{
				String fromEmailId = VWSPreferences.getFromEmailId();
				if( mailIds!=null && mailIds.size()>0 ){
					userEmailId = mailIds.get(0).toString();
					VWMail obj=new VWMail();
					String subject="Notification on  Contentverse Server";
					mailSent=	obj.sendEscalationMail(fromEmailId, userEmailId, subject, mailMessage);
				}
				return mailSent;

			}catch(Exception e){
				return Error;
			}
		}
		/*
		 * Enhancement CV10 Method added For WorkFlow Generate Report
		 */
		public int generateWFReport(int sid,int mode,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date,Vector result) {
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			try{
				String param="";
				param =mode+Util.SepChar+availableIds+Util.SepChar+tasks+Util.SepChar+workFlowIds+Util.SepChar+taskIds+Util.SepChar+userGroupNames+Util.SepChar+date+Util.SepChar;
				DoDBAction.get(getClient(sid),  database, "CVGenerateWFReport", param, result);
				return NoError;
			}catch(Exception e){
				return Error;
			}
		}
		/*
		 * Enhancement CV10 Method added to save WorkFlow Report
		 */
		public int saveWFReport(int sid,int mode,String reportName,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date,Vector result) {
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			try{
				String param="";
				param =mode+Util.SepChar+reportName +Util.SepChar+availableIds+Util.SepChar+tasks+Util.SepChar+workFlowIds+Util.SepChar+taskIds+Util.SepChar+userGroupNames+Util.SepChar+date+Util.SepChar;
				DoDBAction.get(getClient(sid),  database, "CVNewWFReport", param, result);
				return NoError;
			}catch(Exception e){
				return Error;
			}

		}
		/*
		 * Enhancement CV10 Method added to Get WorkFlow Report
		 */
		public int getWFReport(int sid,int flag,String reportName,Vector result) {
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			try{
				String param="";
				if(flag==1)
					param="1"+Util.SepChar;
				else if(flag==2)
					param="2"+Util.SepChar+reportName+Util.SepChar;
				DoDBAction.get(getClient(sid),  database, "CVGetWFReport", param, result);
				return NoError;
			}catch(Exception e){
				return Error;
			}
		}
		/*
		 *  Enhancement CV10 Method added to delete WorkFlow Report
		 */
		public int deleteWFReport(int sid,String reportName,Vector result) {
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			try{
				String	param=reportName+Util.SepChar;
				DoDBAction.get(getClient(sid),  database, "CVDelWFReport", param, result);
				return NoError;
			}catch(Exception e){
				return Error;
			}
		}
		
		
		
		/*
		 * Enhancement CV10 Method added to check WorkFlow Report is duplicate or not before saving
		 */
		public int checkWFReport(int sid,String reportName,Vector result) {
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			try{
				String param=reportName+Util.SepChar;
				DoDBAction.get(getClient(sid),  database, "CVChkWFReport", param, result);
				return NoError;
			}catch(Exception e){
				return Error;
			}

		}
		/*
		 * Enhancement CV10 Method added to get agregare  WorkFlow Report to display in table.
		 */
		public int getAgregateReport(int sid,int mode,String availableIds,String tasks,String workFlowIds,String taskIds,String userGroupNames,String date,Vector result) {
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			try{
				String param="";
				param =mode+Util.SepChar+availableIds+Util.SepChar+tasks+Util.SepChar+workFlowIds+Util.SepChar+taskIds+Util.SepChar+userGroupNames+Util.SepChar+date+Util.SepChar;
				DoDBAction.get(getClient(sid),  database, "CVGenerateWFAggreReport", param, result);
				return NoError;
			}catch(Exception e){
				return Error;
			}
		}
		
		 /**
	     * CV10 :- setDbResource - used to udpate the DBresource with specified language strings.
	     * @return
	     */
	  /*  public int setDbResource()
	    {
	    	try{
	    		String param="";
	    		param=resourceManager.getLocale().toString();
	    		if (param == "zh_CN" ) {
	    			param="CHINESE";
	    		} else if (param == "nl_NL") {
	    			param="DUTCH";
	    		} else {
	    			param="ENGLISH";
	    		}
	    		Vector vecRes = new Vector();
	    		DoDBAction.get(null, database, "CVUpdateResourceStrings", param, vecRes);
	    	}catch(Exception e){
	    		return Error;
	    	}
	    	return NoError;
	    }		*/
	    //CV10 Issue Fix:- Node properties validate message for invalid users
	    public int validateDynamicIndex(int sid,String indexValue,Vector result) {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param = indexValue+Util.SepChar;
	    		DoDBAction.get(getClient(sid),  database, "CVValidateIndexValue", param, result);
	    	}catch(Exception e){
	    		return Error;
	    	}
	    	return NoError;
	    }
	    /**
	     * Issue Fix :- CV10.1 Method added to get the current session id 
	     * from aip to remove the orphan session.
	     * @param currSid
	     * @return
	     */
	    public int setAIPConnectionStatus(int sid,String room) {
	    	// TODO Auto-generated method stub
	    	long now1 = System.currentTimeMillis();
	       	VWSLog.dbg("Session ids available in   aipActiveClient before put::::::::"+aipActiveClient);
	    	aipActiveClient.put(sid, String.valueOf(now1));
	     	VWSLog.dbg("Session ids available in   aipActiveClient after put::::::::"+aipActiveClient);
	    	return sid;
	    }
		
	    public int setCVDTForm(int sid,int docTypeId,String formName,Vector result){	    
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param = docTypeId+ Util.SepChar+formName+Util.SepChar;	    		
	    		DoDBAction.get(getClient(sid),  database, "CVSetDTFormInfo", param, result);
	    		return NoError;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	 
	    public int getCVDTForm(int sid,int docTypeId,Vector result){	    
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param = docTypeId+ Util.SepChar;	    		
	    		DoDBAction.get(getClient(sid),  database, "CVGetDTFormInfo", param, result);
	    		return NoError;
	    	}catch(Exception e){
	    		return Error;
	    	}
	    }
	    
	    /**
	     * CV10.1 Enhancement: Added for checking pending documents in route and sending notifications
	     * @param sid
	     * @return
	     */
	    public int checkTodoDocsToNotify(int sid,Vector result){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param="";
	    		DoDBAction.get(getClient(sid),database, "VW_DRSCheckDocsToNotify", param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		return Error;
	    	}
	    }
	    /**
	     * CV10.1 Enhancement :SoW 2140_Template-save retention 
	     * Method added to get the retention index info
	     * @param sid
	     * @param retentionId
	     * @param result
	     * @return
	     */
	    public int getIndexInfoTemplate(int sid,int retentionId,Vector result){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param=String.valueOf(retentionId);
	    		DoDBAction.get(getClient(sid),database,"VW_ARS_GetIndexInfo_Template",param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		return Error;
	    	}
	    }
	    /**
	     * CV10.1 Enhancement :SoW 2140_Template-save retention 
	     * Method added to get the retention document types.
	     * @param sid
	     * @param retentionId
	     * @param result
	     * @return
	     */
	    public int getRetentionDoctypes(int sid,int retentionId,Vector result){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param=String.valueOf(retentionId);
	    		DoDBAction.get(getClient(sid),database,"VW_ARS_GetRetentionDocTypes",param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		return Error;
	    	}
	    }
	  
	    
	   /**
	    * CV10.1 Enhancements: Added for checking and stopping sending duplicate mails in case for 
	    * pending documents in ToDo List 
	    * @param sid
	    * @param routeMasterId
	    * @param taskSequence
	    * @return
	    */
	    public int checkUpdNotificationDocs(int sid,int routeMasterId,int taskSequence){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	Vector result= new Vector();
	    	try{
	    		String param=routeMasterId+Util.SepChar+taskSequence+Util.SepChar+"1";
	    		DoDBAction.get(getClient(sid),database, "VW_DRSUpdNotificationDocs", param,result);
	    		VWSLog.dbg("resultValue in checkUpdNotificationDocs ::::"+result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while checkUpdNotificationDocs ::::"+ex.getMessage());
	    		return Error;
	    	}
	    } 
	    /**
	     * CV10.1 Enhancement added for retention/notification template enhancement
	     * @param sid
	     * @param doctypeName
	     * @return
	     * Added By: Madhavan.b
	     */
	    public int isRetentionDocTypeFound(int sid, String doctypeName)
	    {
	    	VWSLog.dbgCheck("isDocTypeFound method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	    	try{
	    		Vector result=new Vector();
	    		DoDBAction.get(getClient(sid),database, "CheckDocType_Isfound",doctypeName, result);
	    		if(result==null || result.size()==0) return Error;
	    		return Util.to_Number((String)result.get(0));
	    	}catch(Exception e){
	    		VWSLog.dbg("Exception while isRetentionDocTypeFound ::::"+e.getMessage());
	    		return Error;
	    	}
	    }
	    /**
	     * CV10.1 Enhancement added for retention/notification template enhancement
	     * @param sid
	     * @param doctypeName
	     * @return
	     * Added By: Madhavan.b
	     */	    
	    public int isRetentionDTIndexFound(int sid, String doctypeId,String indexName)
	    {
	    	VWSLog.dbgCheck("isRetentionDTIndexFound method sid val--->" + sid);
	        if (!assertSession(sid)) return invalidSessionId;
	        try{
	        	Vector result=new Vector();
	        	String param=doctypeId+Util.SepChar+indexName+Util.SepChar;
	        	DoDBAction.get(getClient(sid),database, "VWCheckDTIndexName_Isfound",param, result);
	        	if(result==null || result.size()==0) return Error;
	        	return Util.to_Number((String)result.get(0));
	        }catch(Exception e){
	        	VWSLog.dbg("Exception while isRetentionDTIndexFound ::::"+e.getMessage());
	        	return Error;
	        }
	    }
	    /**
	     *  CV10.1 Enhancement added for retention/notification template enhancement
	     * @param sid
	     * @param nodepath
	     * @return
	     * Added By: Madhavan.b
	     */
	    
	    public int checkCVSearchNode(int sid, String nodepath)
	    {
	    	VWSLog.dbgCheck("isRetentionDTIndexFound method sid val--->" + sid);
	        if (!assertSession(sid)) return invalidSessionId;
	        try{
	        	Vector result=new Vector();
	        	String param=nodepath;
	        	DoDBAction.get(getClient(sid),database, "CVSearchNode",param, result);
	        	if(result==null || result.size()==0) return Error;
	        	return Util.to_Number((String)result.get(0));
	        }catch(Exception e){
	        	VWSLog.dbg("Exception while checkCVSearchNode ::::"+e.getMessage());
	        	return Error;	
	        }
	    }
	    
	    /**
	     * CV10.1 Enhancement : This method is created for calling CVCreateNodePath procedure which 
	     * will create folder for saving custom HTML reports of Recycled, AT and Retention panels
	     * @author apurba.m
	     * @param sid
	     * @return
	     */
	    public int cvCreateNodePath(int sid, String panelName, Vector result){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String[] monthName = {"January", "February",
	    				"March", "April", "May", "June", "July",
	    				"August", "September", "October", "November",
	    		"December"};

	    		Calendar cal = Calendar.getInstance();
	    		String month = monthName[cal.get(Calendar.MONTH)];
	    		VWSLog.dbg("Current Month Name cvCreateNodePath ::::"+month);
	    		int year = Calendar.getInstance().get(Calendar.YEAR);
	    		VWSLog.dbg("Current Year cvCreateNodePath ::::"+year);
	    		int day = Calendar.getInstance().get(Calendar.DATE);
	    		VWSLog.dbg("Current day cvCreateNodePath ::::"+day);

	    		String param="CVReports\\"+panelName+"\\"+year+"\\"+month+"\\"+day;
	    		DoDBAction.get(getClient(sid),database, "CVCreateNodePath", param,result);
	    		VWSLog.dbg("resultValue in CVCreateNodePath ::::"+result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while CVCreateNodePath ::::"+ex.getMessage());
	    		return Error;
	    	}
	    } 
	   
	    /**
	     * CV10.1 Enhancement: Created for creating new document of CV Reports on DTC from Adminwise
	     * @author apurba.m
	     * @param sid
	     * @param docTypeId
	     * @param parentId
	     * @param idxCount
	     * @param indices
	     * @return
	     */
	    
	    public int createCVReportsDocument(int sid, int docTypeId, int parentId, int idxCount, String indexName, Document doc, DocType dt, String panelName, int indexId, String reportPath) 
	    {
	        if (!assertSession(sid)) return invalidSessionId;
	        VWSLog.dbg("createCVReportsDocument method sid val--->" + sid);
	        int retValue = 0;
	        Vector path = new Vector();
	        String nodePath = "";
	        idxCount=1;
	        String idx="";
	        Vector indices = new Vector();
	        Vector indicesName = new Vector();
	        String indexValue= "";
	        indicesName.add(indexName);
	        doc.setDocType(dt);
	        doc.getDocType().setIndices(indicesName);
			/**CV10.2 - Added for administration workflow general report**/
	        if(panelName.equalsIgnoreCase("Recyled Documents Panel")|| panelName.equalsIgnoreCase("Retention Panel")||panelName.equalsIgnoreCase("Report Panel") || panelName.equalsIgnoreCase("Audit Trail Panel") || panelName.equalsIgnoreCase("Users and Groups Panel")){
	        	indexValue= reportPath;
	        }
	        
	        for(int i=0; i < idxCount; i++){
	        	idx += Util.SepChar + indexId +
	        			Util.SepChar + indexValue +
	        			Util.SepChar + "";
	        }
	        
	        Vector ret = new Vector();
	        Client client = getClient(sid);
	        String param = client.getUserName() + Util.SepChar +
	        			   client.getIpAddress()+ Util.SepChar +
	                       String.valueOf(docTypeId) + Util.SepChar +
	                       String.valueOf(parentId) + Util.SepChar +
	                       String.valueOf(idxCount) + idx;
	        
	        DoDBAction.get(getClient(sid),  database, "NewDocument", param, ret);
	        VWSLog.dbg("ret After Calling NewDocument for Creating Reports ::::"+ret);
	        if(ret.size() > 0)
	        {
	            StringTokenizer st = new StringTokenizer
	                                            ((String) ret.get(0), Util.SepChar);
	            doc.setId(Util.to_Number(st.nextToken()));
	            doc.setName(st.nextToken());
	            st.nextToken(); //docType id already set
	            st.nextToken(); //doc type name already set
	            doc.setCreator(st.nextToken());
	            doc.setCreationDate(st.nextToken());
	            doc.setModifyDate(st.nextToken());
	            try{
	            	retValue = getNodePath(sid, doc.getId(), path);
	            	if(path!=null && path.size()>0)
	            		nodePath = path.get(0).toString();
	            }catch(Exception ex){
	            	VWSLog.err("Exception while get the folder path : "+ex.getMessage());
	            }
	            try{
	            	message = "Document '"+doc.getName()+"' added to '"+nodePath+"'";
	            	int settingsId = CheckNotificationExist(sid, doc.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName());
	            	if (settingsId >= 0){
	            		setNotificationHistory(sid, settingsId, doc.getId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, "-", "-", client.getUserName(), message);
	            	}
	            	//addNotificationHistory(sid, doc.getParentId(), VNSConstants.nodeType_Folder, VNSConstants.notify_Fol_AddedDoc, client.getUserName(), "-", doc.getName(), message);
	            }catch(Exception ex){

	            }
	            return NoError;
	        }
	        else
	            return Error;
	    }
	    
	    /**
	     * CV10.1 Enhancement: This method is created for saving form template details 
	     * @author apurba.m
	     * @param sid
	     * @param templateName
	     * @param formName
	     * @param sourceLocation
	     * @param destinationLocation
	     * @param result
	     * @return
	     */
	    public int CVSetDTFormTemplate(int sid,int templateId,String templateName, String formName, String sourceLocation, String destinationLocation, Vector result){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param= String.valueOf(templateId)+Util.SepChar+templateName+Util.SepChar+formName+Util.SepChar+sourceLocation+Util.SepChar+
	    					  destinationLocation+Util.SepChar;
	    		DoDBAction.get(getClient(sid),database, "CVSetDTFormTemplate", param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while CVSetDTFormTemplate ::::"+ex.getMessage());
	    		return Error;
	    	}
	    }
	    
	    /**
	     * CV10.1 Enhancement : Created for fetching all form templates
	     * @author apurba.m
	     * @param sid
	     * @param result
	     * @return
	     */
	    public int CVGetDTFormTemplate(int sid, Vector result){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		DoDBAction.get(getClient(sid),database, "CVGetDTFormTemplate", "",result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while CVGetDTFormTemplate ::::"+ex.getMessage());
	    		return Error;
	    	}
	    }
	    
	    /**
	     * CV10.1 Enhancement: Fetches the details of selected template from drop down list
	     * @author apurba.m
	     * @param sid
	     * @param templateName
	     * @param result
	     * @return
	     */
	    public int CVGetDTFormTemplateDetails(int sid, String templateName, Vector result){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param=templateName+Util.SepChar;
	    		DoDBAction.get(getClient(sid),database, "CVGetDTFormTemplate", param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while CVGetDTFormTemplateDetails ::::"+ex.getMessage());
	    		return Error;
	    	}
	    }
	    
	    /**
	     * CV10.1 Enhancement: this method deletes the selected template 
	     * @author apurba.m
	     * @param sid
	     * @param templateName
	     * @param result
	     * @return
	     */
	    public int CVDelDTFormTemplate(int sid, String templateName, Vector result){
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param= templateName+Util.SepChar;
	    		DoDBAction.get(getClient(sid),database, "CVDelDTFormTemplate", param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while CVDelDTFormTemplate ::::"+ex.getMessage());
	    		return Error;
	    	}
	    }
	    
	    /**
	     * CV10.1 Enhancement: This method checks for duplicate template name
	     * @author apurba.m
	     * @param sid
	     * @param templateName
	     * @param result
	     * @return
	     */
	    public int CVCheckForTemplateExist(int sid, String templateName, Vector result) {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param= templateName+Util.SepChar;
	    		DoDBAction.get(getClient(sid),database, "CVChkTemplate_IsExist", param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while CVCheckForTemplateExist ::::"+ex.getMessage());
	    		return Error;
	    	}
	    }
	    
	    
	    /**
	     * This method created for New Web Access for calling updateSearch procedure separately
	     * @author apurba.m
	     * @param sid
	     * @param search
	     * @return
	     */
	    public int createSearch1(int sid, Search search) {
	    	if (!assertSession(sid)) return invalidSessionId;
	        VWSLog.dbgCheck("createSearch method sid val--->" + sid);
	        if(!search.getContents().equals("")) 
	        {
	            if(database.getEngineName().equalsIgnoreCase(DBENGINE_ORA))
	                SQLQueryParser.parseOracleQuery(search.getContents(), false);
	            else if(database.getEngineName().equalsIgnoreCase(DBENGINE_SQL))
	                SQLQueryParser.parseSQLQuery(search.getContents(), false);
	        }
	        Client client = getClient(sid);
	        Vector ret = new Vector();
	        /* Issue #765: (se) Ability to control / set saved searches to be user specific and not just room specific.
	         * User name added in param for that
	         * C.Shanmugavalli Date 1 Dec 2006*/
	        /*getFindResultWith is added to do search(index) like in advanced google search. 28 Aug 2007 */
	        String param = String.valueOf(search.getId()) + Util.SepChar +
	                       search.getName() + Util.SepChar +
	                       (search.isTemp()? "1" : "0") + Util.SepChar +
	                       String.valueOf(search.getNodeId()) + Util.SepChar +
	                       search.getIndex() + Util.SepChar +
	                       search.getContents() + Util.SepChar +
	                       (search.isPreviousVersions()? "1" : "0") + Util.SepChar +
	                       (search.isSearchInComment()? "1" : "0") + Util.SepChar +
	                       search.getCreationDate1() + Util.SepChar +
	                       search.getCreationDate2() + Util.SepChar +
	                       search.getModifiedDate1() + Util.SepChar +
	                       search.getModifiedDate2() + Util.SepChar +
	                       client.getUserName() + Util.SepChar +
	                       client.getIpAddress() + Util.SepChar +
	        			   search.getFindResultWith() + Util.SepChar +
	        			   search.getTextResultWith();

	        DoDBAction.get(getClient(sid),  database, "UpdateSearch", param, ret);
	        VWSLog.dbg("ret after calling UpdateSearch :::: "+ret);
	        if(ret == null || ret.size() == 0)   return CreateSearchError;
	        search.setId(Util.to_Number((String) ret.get(0)));
	        VWSLog.dbg("search.getId() after calling UpdateSearch :::: "+search.getId());
	        return search.getId();
	    }
	    
	    /**
	     * SIDBI code changes
	     * 20/09/2019 
	    */
	    //To call the branch details.
	    
	    public int getBranchInfo(int sid,String flag,String branchCode,Vector result) {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param= flag+Util.SepChar+branchCode+Util.SepChar;
	    		DoDBAction.get(getClient(sid),database, "CVGetBranchInfo", param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while CVGetBranchInfo ::::"+ex.getMessage());
	    		return Error;
	    	}
	    }
	    public int getWorkflowDoctype(int sid, Vector wfDoctypeValue) {
	    	VWSLog.dbg("getWorkflowDoctype in rampserver ::::");
			// TODO Auto-generated method stub
			String regVal="";
			if (!assertSession(sid)) return invalidSessionId;
			Client client = getClient(sid);
			try{
				regVal=VWSPreferences.getWFSummaryDoctype();
				wfDoctypeValue.add(regVal);
				VWSLog.dbg("wfDoctypeValue in rampserver ::::"+wfDoctypeValue);
			}
			catch(Exception e){
				return Error;
			}
			return NoError;

		}
	    public int setEncryptionType(int sid,int docId,int encryptionType,Vector result) {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param= "2"+Util.SepChar+String.valueOf(docId)+Util.SepChar+encryptionType;
	    		DoDBAction.get(getClient(sid),database, "VW_Get_SetJCryptFlag", param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while setEncryptionType ::::"+ex.getMessage());
	    		return Error;
	    	}
	    }
	    
	    public int getEncryptionType(int sid,int docId,Vector result) {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	Client client = getClient(sid);
	    	try{
	    		String param= "1"+Util.SepChar+String.valueOf(docId);
	    		DoDBAction.get(getClient(sid),database, "VW_Get_SetJCryptFlag", param,result);
	    		return NoError;
	    	}
	    	catch(Exception ex){
	    		VWSLog.dbg("Exception while getEncryptionType ::::"+ex.getMessage());
	    		return Error;
	    	}
	    }
	    public int getRouteTaskByName(int sid,int routeId, int docId, String taskName,Vector routeRestus){
	    	VWSLog.dbgCheck("getRouteTaskByName method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	        String param = String.valueOf(routeId)+Util.SepChar+String.valueOf(docId)+Util.SepChar+taskName+Util.SepChar;
	        DoDBAction.get(getClient(sid),  database, "VW_DRSGetRouteTaskByName", param, routeRestus);
	        //VWSLog.add(" GetRouteInfo: "+docRoute.toString());
	        return NoError;
	    }
	    
	    public int getCVCurrentFinancialYearPath(int sid,Vector vCurrentFinanceYear){
	    	VWSLog.dbgCheck("getCVCurrentFinancialYearPath method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	        String param = "";
	        DoDBAction.get(getClient(sid),  database, "CVGetCurrentFinancialYearPath", param, vCurrentFinanceYear);
	        //VWSLog.add(" GetRouteInfo: "+docRoute.toString());
	        return NoError;
	    }
	    
	    public int getHRMSUserInfo(int sid,String userName,Vector retInfo){
	    	VWSLog.dbgCheck("getHRMSUserInfo method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	        String param = userName+Util.SepChar;
	        DoDBAction.get(getClient(sid),  database, "VW_DRSGetHRMSUserInfo", param, retInfo);
	        return NoError;
	    }
	    
	    public int getCVGetLoginInfo(int sid,String userName,Vector retInfo){
	    	VWSLog.dbgCheck("getCVGetLoginInfo method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	        Client client = getClient(sid);
	        String param = client.getIpAddress()+Util.SepChar;
	        VWSLog.dbg("getCVGetLoginInfo method client.getIpAddress() val--->" + client.getIpAddress());
	        VWSLog.dbg("getCVGetLoginInfo method param val--->" + param);
	        DoDBAction.get(getClient(sid),  database, "CVGetLoginInfo", param, retInfo);
	        return NoError;
	    }
	    
	    public int checkDocViewPerm(int sid,String userName,int docId,Vector retPermInfo){
	    	VWSLog.dbgCheck("checkDocViewPerm method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	        Client client = getClient(sid);
	        String param = String.valueOf(docId)+Util.SepChar+userName+Util.SepChar;
	        DoDBAction.get(getClient(sid),  database, "CVChkDocViewPerm", param, retPermInfo);
	        return NoError;
	    }

	   /**
	     *  CVWeb Enhancement added for Sync and offiline 
	     * @param sid
	     * @param nodepath
	     * @return
	     * Added By: Madhavan.b
	     */
	    
	   public int setCVWebDownloadPage(int sid,int docId, String docName,String pageId)
	    {
	    	VWSLog.dbgCheck("isRetentionDTIndexFound method sid val--->" + sid);
	        if (!assertSession(sid)) return invalidSessionId;
	        try{
	            Client client = getClient(sid);
	        	String param=docId+Util.SepChar+client.getIpAddress()+Util.SepChar+client.getUserName()+Util.SepChar+docName+Util.SepChar+pageId+Util.SepChar;
	        	Vector ret=new Vector();
	        	DoDBAction.get(getClient(sid),database, "CVDoc_SetDownloadPage",param, ret);
	        }catch(Exception e){
	        	VWSLog.dbg("Exception while setCVWebDownloadPage ::::"+e.getMessage());
	        	return Error;
	        }
	    	return NoError;
	    }

	    
	    /**
	     * This method created to generate XML file for new web access
	     * @author apurba.m
	     * @param sid
	     * @param finalParams
	     * @param result
	     * @return
	     */
	    public int generateXmlfile(int sid, String finalParams, Vector result) {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbgCheck("generateXmlfile method sid val--->" + sid);
	    	if (!assertSession(sid)) return invalidSessionId;
	    	String param = finalParams;	
	    	DoDBAction.get(getClient(sid),  database, "CVWebGenXml", param, result);	
	    	System.out.println("result vector after calling CVWebGenXml :::: "+result);
	    	VWSLog.dbg("result vector after calling CVWebGenXml :::: "+result);
	    	return NoError;	
	    }
	    
	    public int syncLockDocument(int sid, int did,int type)
        {
            if (!assertSession(sid)) return invalidSessionId;
        	VWSLog.dbgCheck("arsLockDocument method sid val--->" + sid);
            Client client = getClient(sid);
            Vector ret = new Vector();
           // int type = 1;
            String param = "";
            	param = did + Util.SepChar +
            	Util.getOneSpace(client.getUserName())+ Util.SepChar+
            			Util.getOneSpace(client.getIpAddress())+ Util.SepChar+
            			type;
            DoDBAction.get(getClient(sid),  database, "ARSDocumentLock", param , ret);
    
            if( ret.size() == 0 )   return LockDenied;
            int iret = Util.to_Number( (String) ret.get(0) );
            if(iret == 0)            return documentAlreadyLocked;
            if(iret < 0)             return DocumentDeleted;
            return NoError;
    }
	    
	    
	    
	    
	    public int setStickyNote(int sid, DocComment dc,boolean withCheck,int docRestoreFlag,Vector ret)
	    {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbgCheck("docDelStickyNote method sid val--->" + sid);
	    	Client client = getClient(sid);
	    	/*(dc.getPublic()? "1" :)*/
	    	String  param = dc.getId() +
                    Util.SepChar + dc.getDocId() +
                    Util.SepChar + Util.getOneSpace(client.getUserName()) +                       
                    Util.SepChar +  "0"  +
                    Util.SepChar + (withCheck? "1" : "0") +
                    Util.SepChar + Util.getOneSpace(dc.getComment())+Util.SepChar+docRestoreFlag;
	    	DoDBAction.get(getClient(sid),  database, "VW_DOC_SetStickyNote", param , ret);
	    	return NoError;
	    }
	    
	    
	    public int docDelStickyNote(int sid, int did)
	    {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbgCheck("docDelStickyNote method sid val--->" + sid);
	    	Client client = getClient(sid);
	    	Vector ret = new Vector();
	    	String param = did + Util.SepChar + client.getUserName() ;
	    	DoDBAction.get(getClient(sid),  database, "VW_DOC_DelStickyNote", param , ret);
	    	return NoError;
	    }
	    
	    /**
	     * Created for Secure link enhancement
	     * @param sid
	     * @param docId
	     * @param recepientEmail
	     * @param password
	     * @param modifyPermission
	     * @param deliveryOn
	     * @param epiresOn
	     * @return
	     */
	    
	    
	    public int setSecureLink(int sid, int docId,String recepientEmail,String password,int modifyPermission,String deliveryOn,String epiresOn,String room,String url )
	    {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbgCheck("setSecureLink method sid val--->" + sid);
	    	Client client = getClient(sid);
	    	Vector ret = new Vector();
	    	int retVal=0;
	    	String param = docId + Util.SepChar + recepientEmail+Util.SepChar+password+Util.SepChar+modifyPermission+Util.SepChar+deliveryOn+Util.SepChar+epiresOn+Util.SepChar+client.getUserName()+Util.SepChar+url+Util.SepChar ;
	    	DoDBAction.get(getClient(sid),  database, "VW_DOC_SetSecureLink", param , ret);
	    	if(ret!=null& ret.size()>0) {
	    		retVal=Integer.parseInt(ret.get(0).toString());
	    	}
	    	return retVal;
	    }
	    
	    public int setUpdateSecureLink(int sid, int docId,String recepientEmail,String password,int modifyPermission,String deliveryOn,String epiresOn )
	    {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbgCheck("setUpdateSecureLink method sid val--->" + sid);
	    	Client client = getClient(sid);
	    	Vector ret = new Vector();
	    	String param = docId + Util.SepChar + recepientEmail+Util.SepChar+password+Util.SepChar+modifyPermission+Util.SepChar+deliveryOn+Util.SepChar+epiresOn+Util.SepChar+client.getUserName() ;
	    	DoDBAction.get(getClient(sid),  database, "VW_DOC_UpdateSecureLink", param , ret);
	    	return NoError;
	    }
	    
	    
	    
	    
	    public int getSecureLink(int sid, int docId,int flag,String recepientEmail)
	    {
	    	if (!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbgCheck("setUpdateSecureLink method sid val--->" + sid);
	    	Client client = getClient(sid);
	    	Vector ret = new Vector();
	    	String param = docId + Util.SepChar +flag+Util.SepChar+recepientEmail+Util.SepChar+client.getUserName() ;
	    	DoDBAction.get(getClient(sid),  database, "VW_DOC_GetSecureLink", param , ret);
	    	return NoError;
	    }
	    
	    
	    
	    
	    /**
	     * Fetches secure link information of connected room
	     * @author apurba.m
	     * @param sid
	     * @param info
	     * @return
	     */
	    public int expireSelectedLink(int sid, int secureLinkId){
	    	if(!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbgCheck("expireSelectedLink method sid val--->" + sid);
	    	Client client = getClient(sid);
	    	Vector ret = new Vector();
	    	String param = secureLinkId + Util.SepChar + client.getUserName()+ Util.SepChar + "2"+ Util.SepChar  ;
	    	DoDBAction.get(getClient(sid),  database, "VW_DOC_UpdateSecureLink", param , ret);
	    	VWSLog.dbg("ret Vector after calling VW_DOC_UpdateSecureLink procedure :::: "+ret);
	    	return NoError;
	    }
	    
	    
	    /**
	     * Fetches secure link information of connected room
	     * @author apurba.m
	     * @param sid
	     * @param info
	     * @return
	     */
	    public int getSecureLinkDataAdminWise(int sid, Vector info){
	    	if(!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbgCheck("getSecureLinkData method sid val--->" + sid);
	    	Client client = getClient(sid);
	    	Vector ret = new Vector();
	    	String param = "";
	    	DoDBAction.get(getClient(sid),  database, "VW_DOC_GetSecureLink", param , info);
	    	VWSLog.dbg("info Vector after calling VW_DOC_GetSecureLink procedure :::: "+info);
	    	return NoError;
	    }
	    
	    public int readSecureLinkDetails(int sid,  Vector secureLinkInfo){
	    	if(!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbg("Inside readSecureLinkDetails :::::::: "+sid);
	    	String param = ""+ Util.SepChar; 
	    	DoDBAction.get(getClient(sid), database, "VW_CSGetSecureLink", param, secureLinkInfo);
	    	return NoError;
	    }
	    
	    
	    public int readSecureLinkUrl(int sid,  Vector secureLinkUrl){
	    	if(!assertSession(sid)) return invalidSessionId;
	    	VWSLog.dbg("Inside readSecureLinkDetails :::::::: "+sid);
	    	String param = ""+ Util.SepChar; 
	    	DoDBAction.get(getClient(sid), database, "VWGetEnhancedWebAccessurl", param, secureLinkUrl);
	    	return NoError;
	    }
	    
	    
	    public int sendCSSecureLinkEmail(int sid,int type,String docName,String message,String mailIdstr,String mailSubject,String messageType,String expiryDt) {
	    	VWMail obj=new VWMail();
	    	int status=obj.sendSecureLinkMail(mailIdstr, mailSubject,docName,message,messageType,expiryDt);
			return status;
		}
		
		 public int updateSecureLink(int sid,String secureId,  String status){
		    	if(!assertSession(sid)) return invalidSessionId;
		    	Client client = getClient(sid);
		    	VWSLog.dbg("Inside readSecureLinkDetails :::::::: "+sid);
		    	Vector secureLinkUrl=new Vector();
		    	String param = secureId+ Util.SepChar+client.getUserName()+Util.SepChar+status; 
		    	DoDBAction.get(getClient(sid), database, "VW_DOC_UpdateSecureLink", param, secureLinkUrl);
		    	return NoError;
		    }
		    
		 
		 
		 public int updateExpiredLink(int sid){
		    	if(!assertSession(sid)) return invalidSessionId;
		    	Client client = getClient(sid);
		    	VWSLog.dbg("Inside updateExpiredLink :::::::: "+sid);
		    	Vector secureLinkUrl=new Vector();
		    	String param = ""; 
		    	DoDBAction.get(getClient(sid), database, "VW_CSUpdateExpiredLink", param, secureLinkUrl);
		    	return NoError;
		    }
		 public int getSecureLinkPermissionModify(String documentId,int sid, String userName, String password,Vector retPerm) {
				if(!assertSession(sid)) return invalidSessionId;
		    	Client client = getClient(sid);
		    	VWSLog.dbg("Inside updateExpiredLink :::::::: "+sid);
				String param = documentId+ Util.SepChar+userName+Util.SepChar+password+Util.SepChar;
		    	DoDBAction.get(getClient(sid), database, "VW_DOC_GetSecureLinkMPerm", param, retPerm);
		    	return NoError;
		 }
		
		 /**
		  * This method is created to send mail from web access
		  * @author apurba.m
		  * @param room
		  * @param serverName
		  * @param sid
		  * @param userName
		  * @param docName
		  * @param routeName
		  * @param docId
		  * @param mailIds
		  * @param attachmentData
		  * @param status
		  * @param docNames
		  * @param emailOptions
		  * @param destPath
		  * @return
		  * @throws RemoteException
		  */
		 /*public int sendMailWithAttachmentWS(String room, String serverName, int sid, String userName, String docName, String routeName, 
				 int docId, Vector mailIds, Vector attachmentData, String status, Vector docNames, Vector emailOptions, String destPath, String openDocUrl) throws RemoteException{if (!assertSession(sid)) return invalidSessionId;
				 int res=0;
				 try{
					 docName = VWCUtil.fixFileName(docName);
					 routeName = VWCUtil.fixFileName(routeName);
					 File[] attachmentFile = new File[3];
					 String userEmailId = "";					 
					 if( mailIds!=null && mailIds.size()>0 ){
						 userEmailId = mailIds.get(0).toString();
					 }
					 else{
						 //There is no notification for DRS Admin
						 if(userName!=null && userName.trim().equals("DWSAdmin")){
							 return NoError;
						 }
						 if(!status.startsWith("ARS")){
							 Vector userInfo=new Vector();
							 getMailId(sid, userName, userInfo);
							 /**
							  * CV10.1 Enhancement: If Else condition added for sending notification mail of 
							  * the pending documents to the task users 
							  */
							 /*if(userInfo!=null&&userInfo.size()>0){
								 userEmailId = String.valueOf(userInfo.get(0));
							 } else {
								 userEmailId= userName;
							 }
						 }
					 }
					 VWMail obj=new VWMail();
					 String mailSubject = "";
					 String subStatus = "";

					 if(status.equals(Route_Status_Pending)){
						 mailSubject ="Route_Status_Pending";
						 //PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.DocIsPending1")+" "+"'"+docName+"'"+VWSConstants.EMAIL_SUBJECT;
					 }else if(status.equals(Route_Status_Reject)){
						 mailSubject ="Route_Status_Reject" ;
						 //PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.RouteRej1")+" "+"'"+docName+"'"+" "+resourceManager.getString("VWMailMsg.RouteRej2")+" "+resourceManager.getString("VWMailMessage.Reject");
					 }else if(status.equals(Route_Status_End)){
						 mailSubject = "Route_Status_End";
						 //resourceManager.getString("VWMailMessage.EndWorkFlow")+" "+resourceManager.getString("VWMailMsg.RouteEnd1")+" "+ PRODUCT_NAME+" "+resourceManager.getString("VWMailMsg.RouteEnd2")+" "+"'"+docName+"'";
					 }else if(status.equals(Route_Status_Approve)){
						 mailSubject = "Route_Status_Approve";
						 //PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.Approve1")+" "+"'"+docName+"'" +" "+resourceManager.getString("VWMailMsg.Approve2")+" "+resourceManager.getString("VWMailMessage.Approved");
					 }else if(status.equalsIgnoreCase(Route_Status_Notify_OnRptGen)){
						 mailSubject="Route_Status_Notify_OnRptGen";
						 //mailSubject = PRODUCT_NAME + " - \""+docName+"\"";
					 }else if(status.startsWith("ARS")){
						 subStatus = status.substring(3,status.length());
						 if(subStatus.equalsIgnoreCase("6"))
							 mailSubject = PRODUCT_NAME + " "+resourceManager.getString("RamPageMessage.ARSummary")+" "+"\""+routeName+"\"";// passing retentionName in the parameter of routeName
						 else
							 mailSubject = PRODUCT_NAME +" "+ resourceManager.getString("RamPageMessage.ARNotify")+" "+"\""+routeName+"\"";
					 }
					 else 
						 mailSubject = PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.Document")+" "+"'"+docName+"'"+VWSConstants.EMAIL_SUBJECT;

					 String webAccessReference = "";
					 String vwsHome = VWSUtil.getHome();
					 if( attachmentData == null || (attachmentData != null && attachmentData.size()==0) ){
						 //subStatus will be 1 or 6 for ARS
						 if(subStatus == "" ){// This block of code is for DRS and not for ARS
							 attachmentFile[0] = new File(vwsHome +Util.pathSep+"lib"+Util.pathSep+"document.vwr");
							 try{
								 FileOutputStream fout=new FileOutputStream(attachmentFile[0]);
								 String line="vwrp://"+room+"/"+docId;
								 fout.write(line.getBytes());
								 fout.close();
							 }catch(Exception exception){
								 //VWSLog.add("Error in generating document.vwr file.");
								 return Error;
							 }

							 try{
								 String server = openDocUrl+"&roomName="+room+"&documentId="+docId;//VWSPreferences.getWebAccessServerName();
								 
								 webAccessReference = server.replace("secureLinkLogin", "openDocumentLogin");
//								 if(server == null || server.trim().length() == 0){
//									 server = serverName;
//								 }	    					
//								 webAccessReference = getWebAccessReference(sid, server, room, docId);
								 VWSLog.add("webAccessReference sendMailWithAttachmentWS :::: "+webAccessReference);
								 webAccessReference = convertToSpecialChar(webAccessReference);
								 VWSLog.add("webAccessReference after conversion :::: "+webAccessReference);
							 }catch (Exception e) { }
						 }
					 }else{
						 String fileName = docName+"_"+routeName+"_"+ WORKFLOW_MODULE_NAME +"Summary.html";
						 String path = vwsHome +Util.pathSep+"Server_Generated_Files"+Util.pathSep+fileName;

						 //Here we have to decide for getting the file for html meta data and pages after publish
						 if(!status.startsWith("ARS")){//DRS part
							 if(emailOptions!=null && emailOptions.size()>0){
								 try{
									 attachmentFile = new File[10];
									 //This emailOptions has the 3 values that we are going to get from the param of this functiona.
									 int tempStr = Integer.parseInt(emailOptions.get(0).toString());//Document meta data
									 if(tempStr==1){//For getting metadata htmt content and create as attachment.
										 try{
											 String docMetaDataPath = destPath+"\\"+docName+"_"+routeName+"_MetaData.html";
											 attachmentFile[0] = new File(docMetaDataPath);
										 }catch(Exception ex){
											 //VWSLog.add("Exception in getHtmlMetaDataContents is : "+ex.getMessage());
										 }
									 }else{
										 attachmentFile[0] = null;
									 }

									 //Document Report Summary
									 tempStr = Integer.parseInt(emailOptions.get(1).toString());// Document Report summary
									 if(tempStr==1){
										 String tempPath = destPath+"\\"+fileName;
										 //VWCUtil.writeListToFile(attachmentData,tempPath,"");
										 attachmentFile[1] = new File(tempPath);
									 }else{
										 attachmentFile[1] = null;
									 }

									 //Document Pages
									 tempStr = Integer.parseInt(emailOptions.get(2).toString());// Document Pages
									 int compress=Integer.parseInt(emailOptions.get(3).toString());
									 VWSLog.dbg("tempStr before send mail...."+tempStr);
									 VWSLog.dbg("compress before send mail...."+compress);
									 if(tempStr==1&&compress==1){
										 //already published; only we need to create a file for that path.
										 try{
											 String zipFileName = docName+"_"+routeName+"_Zip.zip"; 
											 VWSLog.dbg("zipFileName:::::::::"+zipFileName);
											 String docPagesPath = destPath+"\\"+zipFileName;
											 VWSLog.dbg("docPagesPath:::::::::"+docPagesPath);
											 attachmentFile[2] = new File(docPagesPath);
										 }catch(Exception ex){
											 //VWSLog.add("Exception in checking for the published pages");
										 }
									 }else if(tempStr==1&&compress==0) {
										 VWSLog.dbg("Inside else if ::::"+tempStr);
										 String pagesPath=destPath+"\\"+"DocumentPages"+"\\"+docId;
										 VWSLog.dbg("pagesPath :::: "+pagesPath);
										 File[] extractedFile=Util.listAttachmentFiles(pagesPath);
										 VWSLog.dbg("extractedFile size is"+extractedFile.length);
										 List<String> extractedFileNameList=new ArrayList<String>();
										 if(extractedFile!=null&extractedFile.length>0) {
											 for(int i=0;i<extractedFile.length;i++) {
												 if(extractedFile[i].isFile()) {
													 if(!(extractedFile[i].getName().equals("Pages.xml"))) {
														 extractedFileNameList.add(extractedFile[i].getName());
													 }
												 }
											 }
											 VWSLog.dbg("extractedFileNameList :::"+extractedFileNameList);
											 File[] reportAttachement=new File[2];
											 reportAttachement[0]=attachmentFile[0];
											 reportAttachement[1]=attachmentFile[1];
											 attachmentFile = new File[extractedFileNameList.size()+2];
											 attachmentFile[0]=reportAttachement[0];
											 attachmentFile[1]=reportAttachement[1];
											 if(extractedFileNameList.size()>0&&extractedFileNameList!=null) {
												 for(int j=0;j<extractedFileNameList.size();j++) {
													 File generateFile=new File(pagesPath+"\\"+extractedFileNameList.get(j));
													 attachmentFile[2+j]=generateFile;
												 }
											 }
										 }

									 }        						
									 else{
										 attachmentFile[2] = null;
									 }
								 }catch(Exception ex){
									 //VWSLog.add("Exception in emailOptions check is : "+ex.getMessage());
								 }
							 }
						 }else{
							 attachmentFile[0] = new File(path);
						 }
					 }
					 //Added for ARS
					 String path = "";
					 if(status.startsWith("ARS")){
						 // Attachment is only for Generation of report and not for notify mails
						 if(subStatus.equalsIgnoreCase("6")){
							 // docName - is docTypeName
							 if(attachmentData!=null && attachmentData.size()>0)
								 path = attachmentData.get(0).toString();

							 /**
							  * For Notifybefore mail there will be no attachment, only we are sending the mail body
							  * for all final actions 
							  */
							 /*if(!path.equals(""))
								 attachmentFile[0] = new File(path);
						 }else if (subStatus.equalsIgnoreCase("1")){
							 docName = routeName;
						 }
					 }
					 HashMap<String, String> indexDetails = getDocumentIndicesDetails(sid, docId);
					 VWSLog.dbg("attachmentFile length before sending mail:::"+attachmentFile.length);
					 res = obj.sendDRSMail(userEmailId, mailSubject, attachmentFile, docName, status, docNames, routeName, webAccessReference, room, null, indexDetails);

					 //After mail sent need to delete document.vwr file for DRS functionality
					 File tempFile = new File(vwsHome +Util.pathSep+"lib"+Util.pathSep+"document.vwr");
					 try{
						 if(tempFile!=null && tempFile.exists()){
							 boolean deleted = tempFile.delete();
						 }
					 }catch(SecurityException se){
						 VWSLog.add("SecurityException while deleting document.vwr file is : "+se.getMessage());
					 }catch(Exception ex){
						 VWSLog.add("Exception while deleting document.vwr file");
					 }

					 switch(res){
					 case -1:
						 VWSLog.war("Error in sending mail. Check mail server settings.");
						 break;
					 case -2:
						 VWSLog.war("Error in sending mail. Please check the user(s) mail id.");
						 break;
					 }
				 }catch(Exception e){
					 return Error;
				 }
				 return res;
		 }*/

		 private String  convertToSpecialChar(String input){
			 if(input.contains("&#40;"))
				 input=input.replaceAll("&#40;","(");
			 if(input.contains("&#41;"))
				 input=input.replaceAll("&#41;",")");
			 if(input.contains("&lt;"))
				 input=input.replaceAll("&lt;","<");
			 if(input.contains("&gt;"))
				 input=input.replaceAll("&gt;",">");
			 if(input.contains("&#39;"))
				 input=input.replaceAll("&#39;","'");
			 if(input.contains("%20"))
				 input = input.replaceAll(" ",	"%20");
			 return input;
		 }
		 
		 public int docDeleteAllPrevComments(int sid,int documentId,String userName){
			 VWSLog.dbg("sid method id value docDeleteAllPrevComments  ::::"+sid);
			 if (!assertSession(sid)) return invalidSessionId;
			 Vector result = new Vector();
			 String param = documentId + Utils.SepChar + userName + Utils.SepChar;
			 DoDBAction.get(getClient(sid),  database, "VW_DOC_DeleteAllPrevComments", param, result);
			 return NoError;
		 }
		 
		 public int setPagesInfo(int sid, int documentId, String pageInfoVecElem){
			 VWSLog.dbg("sid method id value setPagesInfo  ::::"+sid);
			 if (!assertSession(sid)) return invalidSessionId;
			 Vector result = new Vector();
			 String params = documentId + Utils.SepChar + sid + Utils.SepChar + pageInfoVecElem + Utils.SepChar;
			 DoDBAction.get(getClient(sid),  database, "CVSetPagesInfo", params, result);
			 return NoError;
		 }
		 
		 public int getPagesInfo(int sid, int documentId, Vector sortedPageInfoVec){
			 VWSLog.dbg("sid method id value getPagesInfo  ::::"+sid);
			 if (!assertSession(sid)) return invalidSessionId;
			 String params = documentId + Utils.SepChar + sid + Utils.SepChar ;
			 DoDBAction.get(getClient(sid),  database, "CVGetPagesInfo", params, sortedPageInfoVec);
			 return NoError;
		 }
		 
		 public int deletetPagesInfo(int sid, int documentId){
			 VWSLog.dbg("sid method id value deletetPagesInfo  ::::"+sid);
			 Vector result = new Vector();
			 if (!assertSession(sid)) return invalidSessionId;
			 String params = documentId + Utils.SepChar + sid + Utils.SepChar ;
			 DoDBAction.get(getClient(sid),  database, "CVDeletePagesInfo", params, result);
			 return NoError;
		 }
		 
		 public int getStatisticsInfo(int sid, String optionKey,Vector result) {
		    	if (!assertSession(sid)) return invalidSessionId;
		    	Client client = getClient(sid);
		    	try{
		    		String param= optionKey;
		    		DoDBAction.get(getClient(sid),database, "VWGetSettingsInfo", param,result);
		    		return NoError;
		    	}
		    	catch(Exception ex){
		    		VWSLog.dbg("Exception while getStatisticsInfo ::::"+ex.getMessage());
		    		return Error;
		    	}
		 }
		 
		 /**
		     * This method is used to reordering the registry tree and lhost details while restarting the contentverse server
		     */
	     public void reorderRegistoryDirectory() {
	    	String type = VWSPreferences.getSSType();
	    	if (type.equalsIgnoreCase(SCHEME_LDAP_ADS)) {
	    		VWSLog.dbg("Before calling reorderRegDirectoryAndHost method in RampageServer....");
	    		VWSPreferences.reorderRegDirectoryAndHost(database);
	    		VWSLog.dbg("After calling reorderRegDirectoryAndHost method in RampageServer....");
	    	}
	    }	
	    
	    /**
	     * 
	     * @param filePath
	     * @param roomName
	     * @param routeName
	     * @param status
	     * @return
	     * @throws RemoteException
	     */
	    public File getRoomAndRouteBasedMessge(String filePath, String roomName, String routeName) throws RemoteException{
	    	VWMail vwmail = new VWMail();
	    	File file = vwmail.getRoomAndRouteBasedMessge(filePath, roomName, routeName, "ARS6", 6);
	    	vwmail = null;
	    	return file;
	    }
	     
	     /**
	      * This Method is to get the document index name and values for rtf message 
	      * @param sid
	      * @param docID
	      * @return HashMap<String, String>
	      * @author : vanitha
	      */
	     public HashMap<String, String> getDocumentIndicesDetails(int sid, int docID) {
	    	 HashMap<String, String> index = null;
	    	 try {
		    	 Vector<String> indices = new Vector<String>();
		    	 String indexName = null, indexValue = null;
		    	 int ret = getDocumentProperties(sid, docID, indices);
		    	 //int ret = getDocumentIndices(sid, docID, "all.zip", indices);
		    	 VWSLog.dbg("indices....."+indices);
		    	 if (ret != -1 && indices != null && indices.size() > 0) {
		    		 index = new HashMap<String, String>();
			    	 for (int i = 0; i < indices.size(); i++)
			         {
			             StringTokenizer st = new StringTokenizer( (String) indices.elementAt(i), "\t");
			             indexName = st.nextToken();
			             indexValue = st.nextToken();
			             index.put(indexName, indexValue);
			         }
		    	 }
		    	 VWSLog.dbg("getDocumentIndexDetails :"+index);
	    	 } catch (Exception e) {
	    		 VWSLog.dbg("Exception in getDocumentIndicesDetails :"+e.getMessage());
	    	 }
	    	 return index;
	     }
	     
	     /**
		     * This method is used to get the previous task users list
		     * @param sid
		     * @param userName
		     * @param docId
		     * @param userInfo
		     * @param originator
		     * @return
		     */
		    public int getMailIdForDRS(int sid, String userName, int docId, Vector userInfo, String originator){
		    	VWSLog.dbgCheck("getMailIdForDRS method originator--->"+userName);
		    	if (!assertSession(sid)) return invalidSessionId;
		    	try{	    		 
		    		if (originator != null)
		    			userName = originator;
		    		 String param = userName+Util.SepChar+docId+Util.SepChar;	    		 
		     		 DoDBAction.get(getClient(sid),  database, "VW_GetUserMailForWorkflow", param, userInfo);
		      		 //String userEmailId=String.valueOf(userInfo.get(0));
		    	}catch(Exception e){
		    		return Error;
		    	}
		    	return NoError;	 
		    } 
		    
		   
			 public boolean assertSessionWS(int sid){
				 System.out.println("Inside RampageServer assertSessionWS Method:::");
				 System.out.println("sid in RampageServer assertSession method id value assertSessionWS  ::::"+sid);
				 VWSLog.dbg("sid assertSession method id value assertSessionWS  ::::"+sid);
				 if(!isClientConnected(sid, false)) return false;
				 return true;
			 }
			 
			 public int getSettingsInfo(int sid, String param, Vector result) {
				 try {
					 if (sid > 0) {
						 DoDBAction.get(getClient(sid), database, "VWGetSettingsInfo", param, result);
					 } else {
						 DoDBAction.get(null, database, "VWGetSettingsInfo", param, result);
					 }
				 } catch (Exception e) {
					 VWSLog.err("Exception in VWGetSettingsInfo procedure call :::: " + e.getMessage());
					 return Error;
				 }
				 return NoError;
			 }
			 
			 public boolean getServerSettingsInfo(int sid, String param) {
				 boolean settingInfo = false;
				 Vector result = new Vector();
				 getSettingsInfo(sid, param, result);
				 VWSLog.dbg("getServerSettingsInfo result ..........."+result);
				 if(result!= null && !result.isEmpty()){
					VWSLog.dbg("result.get(0)..........."+result.get(0));
					if (String.valueOf(result.get(0)) != null && String.valueOf(result.get(0)).equalsIgnoreCase("YES")) {
						settingInfo = true;
					}
					result.removeAllElements();
				 }
				 return settingInfo;
			  }
			 
			 /**
			  * Method to set sidbi customization flag on drs server restart
			  * @param sid
			  */
			 public void setSidbiCustomizationFlag() {
				 String param = "SIDBI Customization";
				 this.sidbiCustomization = getServerSettingsInfo(0, param);
			 }
			 
			 /**
			  * Method to get sidbi customization flag
			  * @param sid
			  * @return
			 */
			 public boolean getSidbiCustomizationFlag(int sid) {
				 if (sid != 0 && !assertSession(sid)){
					 return false;
				 } else {
					 VWSLog.dbg("Server settings session info :" + this.sidbiCustomization);
					 return this.sidbiCustomization;
				 }
			 }
			 
			 public int getDocumentStatusInWorkflow(int sid, int docId) {
				 VWSLog.dbg("getDocumentStatusInWorkflow method sid val--->" + sid);
				 int ret = 0;
				 try {
					 if (!assertSession(sid))
						 return invalidSessionId;
					 String param = docId + Util.SepChar;
					 Vector result = new Vector();
					 VWSLog.dbg("ChkApprovedCount param " + param);
					 DoDBAction.get(getClient(sid), database, "VWCheckWFHangDocs", param, result);
					 VWSLog.dbg("VWCheckWFHangDocs procedure call return value " + result);
					 if (result != null && result.size() > 0) {
						 ret = Util.to_Number(result.get(0).toString());
					 }
					 // VWSLog.dbg("getAprrovedCountForTask " + ret);
				 } catch (Exception e) {
					 VWSLog.err("Exception in getDocumentStatusInWorkflow : " + e.getMessage());
				 }
				 return ret;
			 }
			 
			 /**
			  * This method is created for App for checking whether the document is in workflow or not
			  * @author madhavan.b
			  * @param sid
			  * @param nodeId
			  * @param flagRoute
			  * @return
			  */
			 public int checkDocIsInRoute(int sid,  int nodeId, boolean flagRoute){
				 VWSLog.dbgCheck("checkDocIsInRoute method sid val--->"+sid);
				 if (!assertSession(sid)) return invalidSessionId;
				 Client client = getClient(sid);
				 int routeFlag = 0;
				 if(flagRoute)
					 routeFlag = 0;
				 else
					 routeFlag = 1;
				 String param = Util.replaceSplCharFromTempTable(client.getIpAddress())+Util.SepChar+nodeId+Util.SepChar+routeFlag+Util.SepChar;		
				 Vector returnValue = new Vector();
				 //VWSLog.err(" VWCheckDocsInRoute get param:"+param+":");
				 DoDBAction.get(getClient(sid),  database, "VWCheckDocsInRoute", param, returnValue);
				 //VWSLog.add(" VWCheckDocsInRoute returnValue:"+returnValue.toString());
				 if(returnValue==null || returnValue.size()==0) return Error;
				 return Util.to_Number((String)returnValue.get(0));
			 }
		 
		public int setPagesInformation(int sid, String docId, String oldFileName, String fileName,
					String pagesInformation){
			VWSLog.dbg("sid method id value setPagesInformation  ::::"+docId);
			Vector result = new Vector();
			if (!assertSession(sid)) return invalidSessionId;
			String param = docId + Utils.SepChar + oldFileName + Utils.SepChar + fileName + Utils.SepChar + pagesInformation + Utils.SepChar;
			DoDBAction.get(getClient(sid),  database, "CVSetDocsInfo", param, result);
			return NoError;
  	    }
		 
		public int getDocumentPagesInformation(int sid, int documentId, Vector<String> sortedPageInfoVec){
			VWSLog.dbg("sid method id value getDocumentPagesInformation  ::::"+sid);
			if (!assertSession(sid)) return invalidSessionId;
			String params = String.valueOf(documentId) + Utils.SepChar ;
			DoDBAction.get(getClient(sid),  database, "CVGetDocsInfo", params, sortedPageInfoVec);
			return NoError;
		}
		 
		public int checkDocsInRoute(int sid, int docID) {
			VWSLog.dbg("getDocumentStatusInWorkflow method sid val--->" + sid);
			int ret = 0;
			Vector result = new Vector();
			String param = null;
			Client client = null;
			try {
				if (!assertSession(sid))
					return invalidSessionId;
				client = getClient(sid);
				param = Util.replaceSplCharFromTempTable(client.getIpAddress())+Util.SepChar+docID+Util.SepChar+0+Util.SepChar;
				VWSLog.dbg("VWCheckDocsInRoute param " + param);
				DoDBAction.get(getClient(sid), database, "VWCheckDocsInRoute", param, result);
				VWSLog.dbg("VWCheckDocsInRoute procedure call return value " + result);
				if (result != null && result.size() > 0) {
					if (result.get(0) != null && result.get(0).toString().trim().length() > 0) {
						ret = Util.to_Number(result.get(0).toString());
					}
				}
			} catch (Exception e) {
				VWSLog.err("Exception in getDocumentStatusInWorkflow : " + e.getMessage());
			}
			result = null; client = null;
			return ret;
		}
		
		public int getPrincipalsSecurity(int sid, Vector princ,String startWith,int mode)
	    {
	        if (!assertSession(sid)) return invalidSessionId;
	        VWSLog.dbgCheck("getPrincipals method sid val--->" + sid);
	        return DoDBAction.get(getClient(sid),  database, "Principal_GetAll", String.valueOf(mode) + 
	            Util.SepChar + getAdminGroup()+Util.SepChar+startWith + Util.SepChar + getSubAdminGroup(), princ);
	    }
		
		/**
		 *  CV10.2 Merges - Workflow report enhancement
		  * This method is used to get the DB connection object for Workflow - Jasper reports
		  * @return
		  */
		 public DBConnectionBean getDBConnection(int sid) {
			 VWSLog.dbg("getDBConnection inside rampageserver...."+sid);
			 DBConnectionBean dbConnectionBean = null;
			 try {
				 if (assertSession(sid)) {
					 VWSLog.dbg("before calling aquireConnection....");
					 Connection con = database.aquireConnection();
					 if (con != null) {
						 dbConnectionBean = new DBConnectionBean();
						 dbConnectionBean.setConnection(con);
					 }
					 VWSLog.dbg("after calling aquireConnection...."+con);
				 }
			 } catch (Exception e) {
				 VWSLog.dbg("Exception in getDBConnection :"+e.getMessage());
			 }
			 return dbConnectionBean;
		 }
	    
		/**
		 * CV10.2 - Method to generate static jasper report
		 * @param sid
		 * @param selectedReport
		 * @param outputFormat
		 * @param dateFrom
		 * @param dateTo
		 * @param reportLocation
		 * @param reportName
		 * @return
		 */
	    public boolean generateStaticJasperReport(int sid, String selectedReport, String outputFormat, String dateFrom, String dateTo, String reportLocation, int staticReportID, int nodeID) {
			 VWSLog.dbg("generateStaticJasperReport.....");
			 boolean reportFlag = false;
			 VWRouteJasperReport jsReport = null;
			 if (assertSession(sid)) {
				 try {					 
					 dateTo = dateTo + Util.SepChar + nodeID;
			    	 Vector<String> resultSet = getStaticReportResult(sid, staticReportID, dateFrom, dateTo);
					 if (resultSet != null) {
						 ArrayList<String> reportList = new ArrayList<String>(resultSet);
						 if (reportList.size() > 1) {
							 jsReport = new VWRouteJasperReport();
							 VWSLog.dbg("Before calling generateStaticJasperReport ");
							 //reportFlag = jsReport.generateStaticJasperReport(sid, con, selectedReport, outputFormat, dateFrom, dateTo, reportLocation, database);
							 reportFlag = jsReport.generateStaticJasperReportFromResultSet(selectedReport, outputFormat, reportLocation, reportList);
							 VWSLog.dbg("After calling generateStaticJasperReport "+reportFlag);
							 jsReport = null;
						 }
					 }
				 } catch (Exception e) {
					 VWSLog.dbg("Exception in generateStaticJasperReport :"+e);
					 reportFlag = false;
				 }
			 }
			 return reportFlag;
		 }
		/**
		 * CV10.2 - This method used to get static report names list for administration workflow reports 
		 * @param sid
		 * @return
		 */
	    public ArrayList<String> getStaticReportList(int sid) {
	    	ArrayList<String> staticList = null;
	    	try {
	    		Vector result = new Vector();
	    		DoDBAction.get(getClient(sid),database, "CVGetStaticReports", "", result);
	    		if (result != null && result.size() > 0) {
	    			VWSLog.dbg("getStaticReport result....."+result.size());
	    			staticList = new ArrayList<String>(result);
	    		}
	    	} catch (Exception e) {
	    		VWSLog.dbg("Exception in getStaticReportsList :"+e.getMessage());
	    	}
	    	return staticList;
	    }
	    
	    /**
	     * CV10.2 - This method used to check whether selected report is static or workflow report
	     * @param sid
	     * @param selectedValue
	     * @return
	     */
	    public Vector<String> checkStaticReport(int sid, String selectedValue) {
	    	Vector result = new Vector();
	    	try {
	    		String param = selectedValue + Util.SepChar;
	    		DoDBAction.get(getClient(sid),database, "CVCheckStaticReports", param, result);
	    	} catch (Exception e) {
	    		VWSLog.dbg("Exception in getStaticReportsList :"+e.getMessage());
	    	}
	    	return result;
	    }
	    
	    /**
	     * CV10.2 - This method used to get static report resultset to generate html/csv report
	     * @param sid
	     * @param reportID
	     * @param fromDate
	     * @param toDate
	     * @return
	     */
	    public Vector getStaticReportResult(int sid, int reportID, String fromDate, String toDate) {
	    	Vector result = new Vector();
	    	try {
				String param = fromDate + Util.SepChar + toDate + Util.SepChar + reportID + Util.SepChar;
	    		DoDBAction.get(getClient(sid),database, "CVGenReports", param, result);
	    		VWSLog.dbg("CVGenReports result "+result);
	    	} catch (Exception e) {
	    		VWSLog.dbg("Exception in getStaticReportsList :"+e.getMessage());
	    	}
	    	return result;
	    }
	    /*-----End of CV0.2 Merges---------------------------------------------------------*/
	    /**
	     * This method used to combine meta data and summary report as a single file
	     * @param metaDataFilePath
	     * @param summaryFilePath
	     * @param filePath
	     * @return
	     */
	    public File generateMergedReport(String metaDataFilePath, String summaryFilePath, String filePath) {
	    	File mergedFile = null;
	    	PrintWriter pw = null;
	    	BufferedReader br1 = null;
	    	BufferedReader br2 = null;
	    	String line1 = null, line2 = null;
	    	try {
	    		VWSLog.dbg("summaryFilePath >>>> "+summaryFilePath);
	    		pw = new PrintWriter(filePath); 
		        br1 = new BufferedReader(new FileReader(metaDataFilePath)); 
		        br2 = new BufferedReader(new FileReader(summaryFilePath)); 		          
		        line1 = br1.readLine(); 
		        line2 = br2.readLine(); 		        
		        while (line1 != null) 
		        { 
		        	pw.println(line1); 
		            line1 = br1.readLine();
		        }
		        pw.println("");
		        pw.println("");
		        pw.println("");
		        while (line2 !=null) 
		        { 
		            pw.println(line2); 
		            line2 = br2.readLine(); 
		        } 		
		        mergedFile = new File(filePath);
		        VWSLog.dbg("MetaData.html and SummaryReport.html has merged into single html file"); 
	    	} catch (Exception e) {
	    		VWSLog.dbg("Exception while merging metadata and summary files :"+e);
			} finally {
				try {
					pw.flush();
					br1.close();
					br2.close();
					pw.close();
				} catch (Exception e) {
				}
			}
	    	
	    	return mergedFile;
	    }
	    
	    /**
	     * This method is used to generate jasper report for the administration general report
	     * @param sid
	     * @param reportLocation
	     * @param resultSet
	     * @param columnDescription
	     * @param selectedReport
	     * @param reportName
	     * @param JrxmlPath
	     * @param isPreviewSelected
	     * @param outputformat
	     * @return
	     */
	    public boolean generateDynamicJasperReport(int sid, VWDoc vwdoc, String reportLocation, Vector<String> resultSet,
			String columnDescription, String selectedReport, String JrxmlPath, boolean isPreviewSelected,
			String outputformat) {
	    	VWSLog.dbg("generateDynamicJasperReport.....");
			 boolean reportFlag = false;
			 VWRouteJasperReport jsReport = null;
			 if (assertSession(sid)) {
				 try {					 
					 jsReport = new VWRouteJasperReport();
					 VWSLog.dbg("Before calling generateDynamicJasperReport ");
					 reportFlag = jsReport.generateDynamicJasperReport(sid, vwdoc, reportLocation, resultSet, columnDescription, selectedReport, JrxmlPath, isPreviewSelected, outputformat);
					 VWSLog.dbg("After calling generateDynamicJasperReport "+reportFlag);
					 jsReport = null;
				 } catch (Exception e) {
					 VWSLog.dbg("Exception in generateDynamicJasperReport :"+e);
					 reportFlag = false;
				 }
			 }
			 return reportFlag;
	    }
	    
	    /**
	     * This method is used to check the DSS location for EWA
	     * @param sid
	     * @param nodeID
	     * @return
	     */
	    public int checkStorageLocationForEWA(int sid, int nodeID, ServerSchema dssSchema) {
	    	int returnValue = NoError;
	    	Vector storageCredentials = new Vector();	    	
	    	getAzureStorageCredentials(sid, nodeID, storageCredentials);
	    	if (storageCredentials.size() > 0) {
	    		ServerSchema ss = new ServerSchema((String) storageCredentials.get(0));
				dssSchema.set(ss);
				dssSchema.type = SERVER_DSS;
				VWSLog.dbg("Storage location :"+dssSchema.path);
				File storage = new File(dssSchema.path);
				VWSLog.dbg("is Storage location exist :"+storage.exists());
				if (!storage.exists()) {
					returnValue = storageNotFound;
					dssSchema = null;
				}
	    	} else {
	    		returnValue = Error;
	    	}
	    	return returnValue;
	    }
	    
	    public VWDoc getServerJasperContents(int sid, String reportLocation) {
	    	VWDoc vdoc = new VWDoc();
	    	vdoc.setSrcFile(reportLocation);
	    	File srvJasperFile = new File(reportLocation);
	    	if (srvJasperFile.exists()) {
		    	boolean isSrvFileDeleted = srvJasperFile.delete();
		    	VWSLog.dbg("is Server FileDeleted >>>>>> "+isSrvFileDeleted);
	    	}
	    	return vdoc;
	    }
	    
	    private String getBatchInputAdminGroup() {
	    	return VWSPreferences.getBatchInputAdministrator();
	    }
	    
	    public boolean checkUsersInGroups(int sid, String group, String user) {
	    	boolean isGroupUser = false;
	    	if (assertSession(sid)) {
		    	Vector result = new Vector();
		    	String param = Util.getOneSpace(group) + Util.SepChar + Util.getOneSpace(user);
		    	DoDBAction.get(null, database, "CheckUserGroup", param , result);
		    	VWSLog.dbg("CheckUserGroup procedure result :"+result);
		    	if (result != null && result.size() > 0) {
		    		if (((String) result.get(0)).startsWith("TRUE"))
		    			isGroupUser = true;
		    	}
	    	}
	    	return isGroupUser;
	    }
}