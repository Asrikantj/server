/*
 * VWSPreferences.java
 *
 * Created on 22 ����� ������, 2003, 03:21 �
 */
package com.computhink.vws.server;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import com.computhink.common.RoomProperty;
import com.computhink.common.Util;
import com.computhink.database.Database;
import com.computhink.vws.server.mail.VWMail;
/**
 *
 * @author  Administrator
 */
public class VWSPreferences implements VWSConstants
{
    private static Preferences VWSPref;
    
    static
    {
        VWSPref= Preferences.systemRoot().node(VWS_PREF_ROOT);
    }
    private static Preferences getNode(String node)
    {
        return VWSPref.node(node);
    }
    public static String getHostName()
    {
        return getNode("host").get("name", "");
    }
    public static void setHostName(String name)
    {
        getNode("host").put("name", name);
        flush();
    }
    public static void setComPort(int port)
    {
        getNode("host").putInt("comport", port);
        flush();
    }
    public static int getComPort()
    {
        return getNode("host").getInt("comport", DEFAULT_COM_PORT);
    }
     public static void setDataPort(int port)
    {
        getNode("host").putInt("dataport", port);
    }
    public static int getDataPort()
    {
        return getNode("host").getInt("dataport", DEFAULT_DATA_PORT);
    }
    public static void setProxy(boolean b)
    {
        getNode("proxy").putBoolean("enabled", b);
        flush();
    }
    public static void setProxyHost(String server)
    {
        getNode("proxy").put("host", server);
        flush();
    }
    public static void setProxyPort(int port)
    {
        getNode("proxy").putInt("port", port);
        flush();
    }
    public static boolean getProxy()
    {
        return getNode("proxy").getBoolean("enabled", false);
    }
    public static String getProxyHost()
    {
        return getNode("proxy").get("host", "");
    }
    public static int getProxyPort()
    {
        return getNode("proxy").getInt("port", DEFAULT_PROXY_PORT);
    }
    public static String getSSLoginHost()
    {
        return getNode("ss").get("lhost", "");
    }
    public static String getSSAuthorsHost()
    {
        return getNode("ss").get("ahost", "");
    }
    public static String getSSType()
    {
        return getNode("ss").get("type", SSCHEME_ADS);
    }
    public static String getTree()
    {
        return getNode("ss").get("tree", "");
    }
    public static String getLoginContext()
    {
        return getNode("ss").get("lcontext", "");
    }
    public static String getLdapPort()
    {
        return getNode("ss").get("ldapport", "389");
    }

    public static void setLdapPort(String port)
    {
        getNode("ss").put("ldapport", port);
        flush();
    }

    public static String getAuthorsContext()
    {
        return getNode("ss").get("acontext", "");
    }
    public static void setLoginContext(String context)
    {
        getNode("ss").put("lcontext", context);
        flush();
    }
    public static void setAuthorsContext(String context)
    {
        getNode("ss").put("acontext", context);
        flush();
    }
    public static void setSSLoginHost(String server)
    {
        getNode("ss").put("lhost", server);
        flush();
    }
    public static void setSSAuthorsHost(String server)
    {
        getNode("ss").put("ahost", server);
        flush();
    }
    public static void setSSType(String type)
    {
        getNode("ss").put("type", type);
        flush();
    }
    public static void setTree(String tree)
    {
        getNode("ss").put("tree", tree);
        flush();
    }
    public static boolean isCheckSQL2005()
    {
        return getNode("general").getBoolean("sql2005jdbc", true);
    }
    
    public static int getFlushLines()
    {
        return getNode("general").getInt("loglines", DEFAULT_LOG_LINES);
    }
    public static String getEncryptionKey(String room)
    {
        Preferences roomPref  = getNode("rooms/" + room);
        return roomPref.get("key", "");
    }





    public static void setEncryptionKey(String room, String key)
    {
        if(getEncryptionKey(room).equals(""))
        {
            Preferences roomPref  = getNode("rooms/" + room);
            roomPref.put("key", key);
            try
            {
                roomPref.flush();
            }
            catch(java.util.prefs.BackingStoreException e){}
        }
    }
    /*
    public static String getLicenseKey()
    {
        return getNode("general").get("key", DEFAULT_LICENSE_KEY);
    }
    public static void setLicenseKey(String license)
    {
        getNode("general").put("key", license.trim());
        flush();
    }
     */
    public static String getLicenseFile()
    {
        return getNode("general").get("licensefile", "");
    }
    public static String getCheckViewWiseJar()
    {
        return getNode("general").get("checkjar", "true");
    }
    public static void setCheckViewWiseJar(String option)
    {
        getNode("general").put("checkjar", option);
        flush();
    }
    /***
     * Build : Contentverse 8 Post build 04
     * Date : 12/12/2013
     * Changed the registry 'synchusermailid' reading location from 'general' to 'ss' and values from 'true' to '1', '2', '3'.
     */
    /*public static String getSynchUserMailId()
    {
        return getNode("general").get("synchusermailid", "true");
    }
    public static void setSynchUserMailId(String option)
    {
        getNode("general").put("synchusermailid", option);
        flush();
    }*/
    
    /**
     * Build : Contentverse 8 Post build 04
     * Date : 12/12/2013
     * Changes : 
     * synchUserMailId = 0  Don't update user mail ids from security server
     * synchUserMailId = 1  update user mail ids from security server if mail id is blank in db table
     * synchUserMailId = 2  overwrite all user mail ids from security server.
     */
    public static String getSynchUserMailId()
    {
        return getNode("ss").get("synchusermailid", "1");
    }
    public static void setSynchUserMailId(String option)
    {
        getNode("ss").put("synchusermailid", option);
        flush();
    }   
    public static void setLicenseFile(String license)
    {
        getNode("general").put("licensefile", license.trim());
        flush();
    }
    public static String getNamedUsers()
    {
        return getNode("general").get("named", "");
    }
    public static void setNamedUsers(String nusers)
    {
        getNode("general").put("named", nusers.trim());
        flush();
    }
    public static String getNamedOnline()
    {
        return getNode("general").get("namedonline", "");
    }
    
    
    public static void setNamedOnline(String nOnlineUsers) {
		// TODO Auto-generated method stub
    	 getNode("general").put("namedonline", nOnlineUsers.trim());
         flush();
	}
    public static String getConcurrentOnline()
    {
        return getNode("general").get("concurrentonline", "");
    }
    
    
    public static void setConcurrentOnline(String concurOnlineUsers) {
		// TODO Auto-generated method stub
    	 getNode("general").put("concurrentonline", concurOnlineUsers.trim());
         flush();
	}
    
    
    public static String getEnterpriseConcurrent()
    {
        return getNode("general").get("entrconcurrent", "");
    }
    public static void setEnterpriseConcurrent(String entrconcurrent)
    {
        getNode("general").put("entrconcurrent", entrconcurrent.trim());
        flush();
    }
    public static String getProfessionalNamedUsers()
    {
        return getNode("general").get("professionalnamed", "");
    }
    public static void setProfessionalNamedUsers(String professionalnamed)
    {
    	System.out.println(" professionalnamed.trim()"+ professionalnamed.trim());
        getNode("general").put("professionalnamed", professionalnamed.trim());
        flush();
    }
    public static String getProfessionalConcurrent()
    {
        return getNode("general").get("profconcurrent", "");
    }
    public static void setProfessionalConcurrent(String profconcurrent)
    {
        getNode("general").put("profconcurrent", profconcurrent.trim());
        flush();
    }
    public static String getPublicWebAccess()
    {
        return getNode("general").get("publicwebaccess", "");
    }
    public static void setPublicWebAccess(String publicwebaccess)
    {
        getNode("general").put("publicwebaccess", publicwebaccess.trim());
        flush();
    }
    
    
    /**
     * Enhancement:-Register Security officer (USER name) for server at Connection & security.
     * registry added to store the security administrator
     */
    public static String getSecurityAdministrator()
    {
        return getNode("general").get("securityAdministrator", "");
    }
    public static void setSecurityAdministrator(String securityAdministrator)
    {
        getNode("general").put("securityAdministrator", securityAdministrator.trim());
        flush();
    }
    
    public static boolean getRoomLevelNamed()
    {
        return getNode("general").getBoolean("roomlevelnamed", false);
    }
    public static void setRoomLevelNamed(boolean roomLevelNamed)
    {
        getNode("general").putBoolean("named", roomLevelNamed);
        flush();
    }    
    
    public static boolean getRoomLevelProfessionalNamed()
    {
        return getNode("general").getBoolean("roomlevelprofessionalnamed", false);
    }
    public static void setRoomLevelProfessionalNamed(boolean roomLevelNamed)
    {
        getNode("general").putBoolean("professionalnamed", roomLevelNamed);
        flush();
    }
    
    public static int getDBFrequency()
    {
        return getNode("general").getInt("dbfrequency", 10);        
    }
    // Changes For Email Configuration Settings
    public static void setEmailServerName(String emailServerName)
    {
        getNode("general").put("emailServerName", emailServerName.trim());
        flush();
    }
    public static void setFromEmailId(String emailId)
    {
        getNode("general").put("fromEmailId", emailId.trim());
        flush();
    }
    public static void setEmailServerPort(String emailServerPort)
    {
        getNode("general").put("emailServerPort", emailServerPort.trim());
        flush();
    }
    
    public static void setEmailServerUsername(String emailServerUsername)
    {
        getNode("general").put("emailServerUsername", emailServerUsername.trim());
        flush();
    }
    public static void setEmailServerPassword(String emailServerPassword)
    {
       	//Issue :Issue 893(The email server password is stored unencrypted in the registry)
    	//Author:Nishad Nambiar
    	//Date  :7-Sep-2007
    	try{
    		getNode("general").putByteArray("emailServerPassword", (Util.encryptKey(emailServerPassword)).getBytes("UTF8"));
    	}catch(Exception e){
    		
    	}
        flush();
    }    
    
    public static String getEmailServerName()
    {
        return getNode("general").get("emailServerName", "");
    }
    public static String getFromEmailId()
    {
        return getNode("general").get("fromEmailId", "");
    }
    public static String getEmailServerPort()
    {
        return getNode("general").get("emailServerPort", "");
    }
    public static String getEmailServerUsername()
    {
        return getNode("general").get("emailServerUsername", "");
    }
    public static String getEmailServerPassword()
    {
    	//Issue :Issue 893((The email server password is stored unencrypted in the registry)
    	//Author:Nishad Nambiar
    	//Date  :7-Sep-2007
    	String ePassword="";
    	byte eServerPassword[] = getNode("general").getByteArray("emailServerPassword", null);
    	try{
	    	if(eServerPassword==null)
	    		ePassword = Util.decryptKey(new String(""));
	    	else
	    		ePassword = Util.decryptKey(new String(eServerPassword));
    	}catch(Exception e){

    	}
    	return ePassword;
    }
    public static String getUseUsrGrpXMLFile()
    {
        return getNode("general").get("useusrgrpxmlfile", "");
    }
    public static void setUseUsrGrpXMLFile(String UseUsrGrpXMLFile)
    {
        getNode("general").put("useusrgrpxmlfile", UseUsrGrpXMLFile.trim());
    }
    
    public static void addRoom(RoomProperty rp) 
    {
        Preferences roomPref  = getNode("rooms/" + rp.getName());
        roomPref.put("engine", rp.getDatabaseEngineName());
        roomPref.put("dbname", rp.getDatabaseName());
        roomPref.put("host", rp.getDatabaseHost());
        roomPref.putInt("port", rp.getDatabasePort());
        roomPref.put("user", rp.getDatabaseUser());
        if(getSSType()==SSCHEME_NDS){
        	if(rp.getDatabasePassword().equalsIgnoreCase("")){        
        		roomPref.putByteArray("password",null);   
        	}else{
        		try{
        			roomPref.putByteArray("password",(Util.encryptKey(rp.getDatabasePassword())).getBytes("UTF8"));
        		}catch(Exception e){
        			String message = "Error in encrypting and setting password "+e.getMessage();
        			VWSLog.err("error in encrypting and setting password "+e.getMessage());        			
        			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
        		}
        	}	
        }else{
        roomPref.put("password", Util.encryptKey(rp.getDatabasePassword()));
		}
        roomPref.putBoolean("bypass", rp.getBypass());
        roomPref.put("admins", rp.getAdmins());
        roomPref.put("subadmins", rp.getSubAdmins());
        roomPref.put("named", rp.getNamedGroup());
        roomPref.put("namedonline", rp.getNamedOnlineGroup());
      //  roomPref.put("namedonline", rp.getNamedOnlineGroup());
        roomPref.put("concurrentonline", rp.getConcurrentOnlineGroup());
        roomPref.put("entrconcurrent", rp.getEnterpriseConcurrentGroup());
        roomPref.put("professionalnamed", rp.getProfessionaNamedGroup());
        roomPref.put("profconcurrent", rp.getProfessionalConcurrentGroup());
        roomPref.put("batchInputAdministrator", rp.getBatchInputAdministrator());
        //roomPref.put("managers", rp.getManagers());
        roomPref.putInt("idle",rp.getIdle());
        roomPref.put("key", rp.getKey());
        roomPref.putBoolean("EncryptedKey", rp.getIsEncryptedKey());
        roomPref.putBoolean("enabled", rp.isEnabled());
        roomPref.put("countdbfail","5");
        roomPref.putInt("tabstatus", 0);
        roomPref.putInt(" togglestatus", 0);       
        roomPref.putInt("dbscheduler",rp.getConnectionResetScheduler());
        roomPref.put("schedulertime",rp.getSchedulerTime());
        roomPref.put("sqlconnectionstring", rp.getSqlConnectionString());
        roomPref.put("authenticationmode", rp.getAuthenticationMode());        
        roomPref.putBoolean("integratedsecurity", rp.isIntegratedSecurity());
        roomPref.put("authenticationscheme", rp.getAuthenticationScheme());
        roomPref.put("domain", rp.getDomain());
        roomPref.put("serverspn", rp.getServerSpn());
        try
        {
            roomPref.flush();
        } catch(java.util.prefs.BackingStoreException e){}
    }
    public static void removeRoom(String room) 
    {
        Preferences roomPref  = getNode("rooms/" + room);
        try
        {
            roomPref.removeNode() ;
        }
        catch(BackingStoreException e){}
    }
    public static void setDBUserPassword(String room, String user, String pwd)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	roomPref.put("user", user);
    	if(getSSType()==SSCHEME_NDS){
    		if(pwd.equalsIgnoreCase("")){        
    			roomPref.putByteArray("password",null);   
    		}else{
    			try{
    				roomPref.putByteArray("password",(Util.encryptKey(pwd).getBytes("UTF8")));
    			}catch(Exception e){
    				String message = "Error in encrypting and setting password "+e.getMessage();
    				VWSLog.err("error in encrypting and setting password "+e.getMessage());    				
        			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
    			}
    		}	
    	}else{
    		roomPref.put("password", Util.encryptKey(pwd));
    	}
    	try
    	{
    		roomPref.flush();
    	}
    	catch(java.util.prefs.BackingStoreException e){}
    }
    public static void setDBName(String room, String dbName)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	roomPref.put("dbname", dbName);
    	
    	try
    	{
    		roomPref.flush();
    	}
    	catch(java.util.prefs.BackingStoreException e){}
    }
    public static void setRoomState(String room, boolean state)
    {
        Preferences roomPref  = getNode("rooms/" + room);
        roomPref.putBoolean("enabled", state);
        try
        {
            roomPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }
    public static boolean getRoomState(String room)
    {
        Preferences roomPref  = getNode("rooms/" + room);
        return roomPref.getBoolean("enabled", false);
    }
    public static RoomProperty[] getRegRooms()
    {
        Preferences roomsPref = getNode("rooms");
        String[] roomNames = null;
        Vector enabledRooms = new Vector();
        try
        {
            roomNames = roomsPref.childrenNames();
        }
        catch(BackingStoreException e)
        {
            return null;
        }
        int roomCount = roomNames.length;
        RoomProperty[] rooms = new RoomProperty[roomCount];
        for (int i=0; i < roomCount; i++)
        {
            rooms[i] = getRoom(roomNames[i]);
        }
        return rooms;
    }
    public static RoomProperty[] getRooms()
    {
        Preferences roomsPref = getNode("rooms");
        String[] roomNames = null;
        Vector enabledRooms = new Vector();
        try
        {
            roomNames = roomsPref.childrenNames();
        }
        catch(BackingStoreException e)
        {
            return null;
        }
        int roomCount = roomNames.length;
        for (int x=0; x < roomCount; x++)
        {
            Preferences roomPref  = getNode("rooms/" + roomNames[x]);
            if (roomPref.getBoolean("enabled", false)) 
                enabledRooms.add(roomNames[x]);
        }
        roomCount = enabledRooms.size(); 
        RoomProperty[] rooms = new RoomProperty[roomCount];
        for (int i=0; i < roomCount; i++)
        {
            rooms[i] = getRoom((String) enabledRooms.get(i));
        }
        return rooms;
    }
    /*
     * Enhancment:-Professional License changes
     * Getter and Setter Method Added for synchronizing the Registry Entry form license tab and rooms tab for 
     * RoomLevelNmaed Hosted Environment
     */
    public static String getRoomsNamed(String name)
    {
    	 Preferences roomPref = getNode("rooms/" + name);
    	 return roomPref.get("named","");
    }
    public static void setRoomsNamed(String name)
    {
    	String[] tokens = name.split("#");
    	Preferences roomPref = getNode("rooms/" +tokens[0]);
    	roomPref.put("named",tokens[1]);
    	try {
    		roomPref.flush();
    	} catch (BackingStoreException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }
    
    
    //Method added for CV10 Enhancement to enable/disable Backup/Restore tab based on the roomlevel registry set value
    public static void setTabStatus(String room, int status)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	roomPref.putInt("tabstatus", status);
    	try
    	{
    		roomPref.flush();
    	}
    	catch(BackingStoreException e){}
    }

    public static void setToggleStatus(String room, int status)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	roomPref.putInt("togglestatus", status);
    	try
    	{
    		roomPref.flush();
    	}
    	catch(BackingStoreException e){}
    }
    /*
     * Enhancment:-Professional License changes
     * Getter and Setter Method Added for synchronizing the Registry Entry form license tab and rooms tab for 
     * RoomLevelProfessionalNmaed Hosted Environment
     */
    public static String getRoomsProfessionalNamed(String name)
    {
    	 Preferences roomPref = getNode("rooms/" + name);
    	 return roomPref.get("professionalnamed","");
    }
    public static void setRoomsProfessionalNamed(String name)
    {
    	String[] tokens = name.split("#");
    	 Preferences roomPref = getNode("rooms/" + tokens[0]);
    	 roomPref.put("professionalnamed",tokens[1]);
    	 try {
 			roomPref.flush();
 		} catch (BackingStoreException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    
    
    public static String getRoomsNamedOnline(String name)
    {
    	 Preferences roomPref = getNode("rooms/" + name);
    	 return roomPref.get("namedonline","");
    }
    public static void setRoomsNamedOnline(String name)
    {
    	String[] tokens = name.split("#");
    	 Preferences roomPref = getNode("rooms/" + tokens[0]);
    	 roomPref.put("namedonline",tokens[1]);
    	 try {
 			roomPref.flush();
 		} catch (BackingStoreException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
    }
    
    public static RoomProperty getRoom(String name)
    {
        RoomProperty rp =  new RoomProperty(name);
        Preferences roomPref = getNode("rooms/" + name);
        rp.setDatabaseEngineName(roomPref.get("engine", ""));
        rp.setDatabaseName(roomPref.get("dbname", ""));
        rp.setDatabaseHost(roomPref.get("host", ""));
        rp.setDatabasePort(roomPref.getInt("port", 0 ));
        rp.setDatabaseUser(roomPref.get("user", "")); 
		if(getSSType()==SSCHEME_NDS){
        	 byte password[] = roomPref.getByteArray("password", null);
        	 rp.setDatabasePassword(Util.decryptKey(new String(password)));
        }else{
        rp.setDatabasePassword(Util.decryptKey(roomPref.get("password", "")));
		}
        rp.setBypass(roomPref.getBoolean("bypass", false));
        rp.setAdmins(roomPref.get("admins", ""));
        rp.setSubAdmins(roomPref.get("subadmins", "")); 
        rp.setNamedGroup(roomPref.get("named", ""));
        rp.setNamedOnlineGroup(roomPref.get("namedonline", ""));
        rp.setConcurrentOnlineGroup(roomPref.get("concurrentonline", ""));
        rp.setProfessionalNamedGroup(roomPref.get("professionalnamed", ""));
        rp.setPublicWebAccess(roomPref.get("publicwebaccess", ""));
        
        rp.setEnterpriseConcurrentGroup(roomPref.get("entrconcurrent",""));
        rp.setProfessionalConcurrentGroup(roomPref.get("profconcurrent",""));
        rp.setBatchInputAdministrator(roomPref.get("batchInputAdministrator",""));
        rp.setTabStatus(roomPref.getInt("tabstatus", 0)); 
        rp.setToggleStatus(roomPref.getInt("togglestatus",0));
        rp.setIdle(roomPref.getInt("idle", 0));
        rp.setKey(roomPref.get("key", ""));
        rp.setIsEncryptedKey(roomPref.getBoolean("EncryptedKey", false));
        rp.setMinPoolSize(roomPref.getInt("minpool", -1));
        rp.setMaxPoolSize(roomPref.getInt("maxpool", -1));
        rp.setEnabled(roomPref.getBoolean("enabled", false));
        rp.setCountDBFail(roomPref.getInt("countdbfail", 5));
        // Added for connection reset scheduler
        rp.setConnectionResetScheduler(roomPref.getInt("dbscheduler", 0));
        if (rp.getConnectionResetScheduler() == 0)
        	rp.setSchedulerTime(roomPref.get("schedulertime", "120"));
        else if(rp.getConnectionResetScheduler() == 1){
        	rp.setSchedulerTime(roomPref.get("schedulertime", "05:05:05"));
        }
        rp.setSqlConnectionString(roomPref.get("sqlconnectionstring", ""));
        rp.setAuthenticationMode(roomPref.get("authenticationmode", "sql")); 
        rp.setIntegratedSecurity(roomPref.getBoolean("integratedsecurity", true));
        rp.setAuthenticationScheme(roomPref.get("authenticationscheme", "NTLM"));
        rp.setDomain(roomPref.get("domain", ""));
        rp.setServerSpn(roomPref.get("serverspn", ""));
        return rp;
    }
    public static Vector getDSSs(boolean detailed) 
    {
        Preferences serverPref = getNode("dservers");
        String[] servers = null;
        Vector vServers = new Vector();
        try
        {
            servers = serverPref.childrenNames();
        }
        catch(BackingStoreException e){}
 
        for (int i=0; i < servers.length; i++)
        {
            Preferences server = serverPref.node(servers[i]); 
            if (detailed)
                vServers.addElement(servers[i] + "|" + getDSS(servers[i]));
            else
                vServers.addElement(servers[i]);
        }
        return vServers;
    }
    public static String getDSS(String sname)
    {
        Preferences serverPref = getNode("dservers/" + sname);
        return  serverPref.get("address", "") + "|" + serverPref.get("port", "");
    }
    public static void addDSS(String server) 
    {
        StringTokenizer st =  new StringTokenizer(server, "|");
        Preferences serverPref  = getNode("dservers/" + st.nextToken());
        serverPref.put("address", st.nextToken());
        serverPref.put("port", st.nextToken());
        try
        {
            serverPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }
    public static void removeDSS(String server) 
    {
        StringTokenizer st =  new StringTokenizer(server, "|") ;
        Preferences serverPref  = getNode("dservers/" + st.nextToken());
        try
        {
            serverPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }
    public static String getManager()
    {
        String man = getNode("general").get("manager", "admin");
        String pas = getNode("general").get("manpass", "manager");
        if (!pas.equals("manager")) pas = Util.decryptKey(pas);
        return man + "||" + pas;
    }
    public static boolean setManager(String man, String pas)
    {
        try
        {
            pas = Util.encryptKey(pas);
            getNode("general").put("manager", man);
            getNode("general").put("manpass", pas);
            flush();
            return true;
        }
        catch(Exception e){}
        return false;
    }
    public static boolean getLogInfo()
    {
        return getNode("general").getBoolean("loginfo", true);
    }
    public static boolean getDebug()
    {
        return getNode("general").getBoolean("debug", false);
    }
    public static boolean getOutputDebug()
    {
        return getNode("general").getBoolean("outputdebug", false);
    }
    public static void setLogInfo(boolean b)
    {
        getNode("general").putBoolean("loginfo", b);
        flush();
    }
    public static boolean getDebugInfo()
    {
        return getNode("general").getBoolean("debuginfo", false);
    }
    public static void setDebugInfo(boolean b)
    {
        getNode("general").putBoolean("debuginfo", b);
        flush();
    }
    public static int getMaxLoginAttempts()
    {
        return getNode("general").getInt("loginno", 3);
    }
    public static void setMaxLoginAttempts(int attempts)
    {
        getNode("general").putInt("loginno", attempts);
        flush();
    }
    public static int getLoginLockTime()
    {
        return getNode("general").getInt("loginlt", 30);
    }
    public static void setLoginLockTime(int minutes)
    {
        getNode("general").putInt("loginlt", minutes);
        flush();
    }
    // Issue 562
    public static void setLogFileCount(int count)
    {
        getNode("general").putInt("maxlogfilecount", count);
        flush();
    }    
    public static int getLogFileCount()
    {
        return getNode("general").getInt("maxlogfilecount",DEFAULT_LOGFILE_COUNT);
    }    
    //Read the encoding type from server registry . UTF-8 is default encoding type.
    public static String getEncoding()
    {
        return getNode("general").get("encoding","UTF-8");
    }    
    //
    /* For Issue no 736: Instead of using idle timeout with MDSS, it is changed based on lastActiveTime 
    *  Method added for client to get idle timeout, C.Shanmugavalli, 27 Oct 2006
    */
    public static int getRoomIdleTimeOut(String room)
    {
        Preferences roomPref  = getNode("rooms/" + room);
        return roomPref.getInt("idle", 0);
    }
    /*LDAP related method added Valli 13 Mar 2007*/
    public static String getAdminPass()
    {
        return Util.decryptKey(getNode("ss").get("adminpass", ""));
    }
    public static void setAdminPass(String s)
    {
        getNode("ss").put("adminpass", Util.encryptKey(s));
        flush();
    }

    public static String getLdapSecurity()
    {
        return getNode("ss").get("ldapsecurity", "");
    }
    public static void setLdapSecurity(String s)
    {
        getNode("ss").put("ldapsecurity", s);
        flush();
    }
    public static String getUserMailAttribute()
    {
        return getNode("ss").get("usermailattribute", "");
    }
    
    public static void setUserMailAttribute(String userMailAttribute)
    {
        getNode("ss").put("usermailattribute", userMailAttribute);
        flush();
    }
    public static int getMessageIDExpiry(String room)
    {
        Preferences roomPref  = getNode("rooms/" + room);
        return roomPref.getInt("msglifetime", 48);
    }
    public static void setMessageIDExpiry(String room, int hours)
    {
        Preferences roomPref  = getNode("rooms/" + room);
        roomPref.putInt("msglifetime", hours);
        try
        {
            roomPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}

    }

    /** 
     * Email server setting - offline 
     * Implementing send mail functionality for the exceptions to be automatically sent to the configured ids.
     * VWS, DSS and ARS
     */
    //
    public static void setVWSEmailId(String emailId){
        getNode("general").put("vwsEmailId", emailId.trim());
        flush();
    }
    public static String getVWSEmailId(){
        return getNode("general").get("vwsEmailId", "");
    }
    public static void setDSSEmailId(String emailId){
        getNode("general").put("dssEmailId", emailId.trim());
        flush();
    }
    public static String getDSSEmailId(){
        return getNode("general").get("dssEmailId", "");
    }
    
    public static void setDRSEmailId(String emailId){
        getNode("general").put("dwsEmailId", emailId.trim());
        flush();
    }
    public static String getDRSEmailId(){
        return getNode("general").get("dwsEmailId", "");
    }
    public static void setARSEmailId(String emailId){
        getNode("general").put("arsEmailId", emailId.trim());
        flush();
    }
    public static String getARSEmailId(){
        return getNode("general").get("arsEmailId", "");
    }
    public static void setVNSEmailId(String emailId){
        getNode("general").put("vnsEmailId", emailId.trim());
        flush();
    }
    public static String getVNSEmailId(){
        return getNode("general").get("vnsEmailId", "");
    }
    
    public static void setVWSEmailIdSet(String selected){
        getNode("general").put("vwsEmailSet", selected.trim());
        flush();
    }
    public static String getVWSEmailIdSet(){
        return getNode("general").get("vwsEmailSet", "");
    }
    public static void setDSSEmailIdSet(String selected){
        getNode("general").put("dssEmailSet", selected.trim());
        flush();
    }
    public static String getDSSEmailIdSet(){
        return getNode("general").get("dssEmailSet", "");
    }
    public static void setDRSEmailIdSet(String selected){
        getNode("general").put("dwsEmailSet", selected.trim());
        flush();
    }
    public static String getDRSEmailIdSet(){
        return getNode("general").get("dwsEmailSet", "");
    }
    public static void setARSEmailIdSet(String selected){
        getNode("general").put("arsEmailSet", selected.trim());
        flush();
    }
    public static String getARSEmailIdSet(){
        return getNode("general").get("arsEmailSet", "");
    }
    public static void setVNSEmailIdSet(String selected){
        getNode("general").put("vnsEmailSet", selected.trim());
        flush();
    }
    public static String getVNSEmailIdSet(){
        return getNode("general").get("vnsEmailSet", "");
    }
    
    
    public static void setSSLEnable(String selected){
        getNode("general").put("sslEnable", selected.trim());
        flush();
    }
    public static String getSSLEnable(){
        return getNode("general").get("sslEnable", "");
    }
    
    
    
    /**
     * Syncronize indices for selection list every 24 hours
     * @param synctime
     */
    public static void setSyncTime(String roomName, String synctime){
    	getNode("general").put("synctime", synctime);
    }
    public static String getSyncTime(){
    	return getNode("general").get("synctime", "");
    }

    /**
     * Optimize the selection list values.
     * @return
     */
    public static String getEnableSelection(){
    	return getNode("general").get("enableselection", "false");
    }
    
    /**
     * selection Map limit.
     * @return
     */
    public static String getSelectionLimit(){
    	return getNode("general").get("selectionmaplimit", "200000");
    }
    
    /**
     * selection values count.
     * @return
     */
    public static String getSelectionValuesCount(){
    	return getNode("general").get("selectionvaluescount", "1000");
    }
    
    public static int getDSNMinPool(){
    	return getNode("general").getInt("dsnminpool", 3);
    }
    
    public static int getDSNMaxPool(){
    	return getNode("general").getInt("dsnmaxpool", 5);
    }
    
    public static boolean getHostedAdmin(){
        return getNode("general").getBoolean("hostedadmin", false);
    }
    
    public static void setHostedAdmin(boolean hostedAdmin){
    	getNode("general").putBoolean("hostedadmin", hostedAdmin);
    }
    
    public static String getWebAccessServerName()
    {
        return getNode("general").get("waserver", "");
    }
    
    public static void setWebAccessServerName(String webAccessServer)
    {
        getNode("general").put("waserver", webAccessServer);
        flush();
    }
    
    static private void flush()
    {
        try
        {
            VWSPref.flush();
            
        }
        catch(java.util.prefs.BackingStoreException e){}
    }
    
    //Getting registry named groupname from vws/general/rooms for hosting environment
    public static String getRoomLevelNamed(String room)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	return roomPref.get("named", "");
    }
    
    
    public static String getRoomLevelNamedOnline(String room)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	return roomPref.get("namedonline", "");
    }
    
    
    
    //Getting registry Professional groupname from vws/general/rooms for hosting environment
    public static String getRoomLevelProfessionalNamed(String room)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	return roomPref.get("professionalnamed", "");

    }
    public static String getRoomLevelConcurrent(String room)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	return roomPref.get("entrconcurrent", "");
    }
    
 
    
    public static String getRoomLevelConcurrentOnline(String room)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	return roomPref.get("concurrentonline", "");
    }
    
    
    
    public static String getRoomLevelProfConcurrent(String room)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	return roomPref.get("profconcurrent", "");
    }
    
    public static String getRoomLevelPublicWA(String room)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	return roomPref.get("publicwebaccess", "");
    }
    
    public static String getRoomLevelBatchInputAdministrator(String room)
    {
    	Preferences roomPref  = getNode("rooms/" + room);
    	return roomPref.get("concurrentonline", "");
    }
    
	public static boolean getDebugCheck() {
		// TODO Auto-generated method stub
		 return getNode("general").getBoolean("debugcheck", false);
	}
	public static boolean getSecurityCheck()
	{
		return getNode("general").getBoolean("securitycheck", false);
	}
	public static void setAIPActiveTime( String activeTime) {
		// TODO Auto-generated method stub
		getNode("general").put("activeTime", activeTime);
		
	}
	/**
	 * CV10.1 Ehancement Hot foler monitor location from server
	 * Modified by :- Apurba.M
	 * @return
	 */
	public static String getHotFolderPath()
	{
		return getNode("general").get("hotfolderlocation", "");
	}

	public static void setHotFolderPath(String location)
	{
		getNode("general").put("hotfolderlocation", location.trim());
		flush();
	}
	 public static boolean getDNSyncStatus()
	    {
	        return getNode("ss").getBoolean("ldapsyncdn", false);
	    }
	 public static boolean getSyncGroupUsers()
	    {
	        return getNode("general").getBoolean("syncgroupusers", true);
	    }
    public static void setSyncGroupUsers(boolean syncPrincipalName)
	    {
	        getNode("general").putBoolean("syncgroupusers", syncPrincipalName);
	        flush();
	    } 
    public static boolean getAuthenticateUserDN()
    {
        return getNode("general").getBoolean("authenticatedn", false);
    }
    public static void setAuthenticateUserDN(boolean syncPrincipalName)
    {
        getNode("general").putBoolean("authenticatedn", syncPrincipalName);
        flush();
    } 
    public static boolean getSyncOtherOUUsers()
    {
        return getNode("general").getBoolean("syncotherouusers", false);
        
    }
	/****CV2019 merges from SIDBI line--------------------------------***/
    public static String getMsiVersion() {
    	return getNode("general").get("msiversion", "");
    }
    public static String getMsiURL() {
    	return getNode("general").get("msiurl", "");
    }
	/*--------------------------------------------------------------*/
	/****CV2019 merges from 10.2 line--------------------------------***/
    public static String getSyncGroupsWith()
    {
    	return getNode("general").get("syncgroupswith", "");
    }
    public static boolean getLDAPUpgradeToMultiDomain()
    {
        return getNode("general").getBoolean("ldapupgradetomulti", true);
    }
    public static boolean getSyncFlagForNonRegisteredGroup()
    {
        return getNode("general").getBoolean("syncnonregisteredgroup", true);
    }
    public static boolean getADToLDAPFlag()
    {
        return getNode("general").getBoolean("adtoldap", false);
    }
    public static void setADToLDAPFlag(boolean flag) {
    	getNode("general").putBoolean("adtoldap", flag);
    }
    public static void reorderRegDirectoryAndHost(Database database) {   
    	VWSLog.add("VWSPreferences reorderRegDirectoryAndHost.....");
		String loginHost = null;
		String ldapDirectory = null;
		String loginServer = null;
		String[] dirList = null;
		String[] serList = null;
    	DirectorySync dirSync = new DirectorySync(database);
    	String domainName = dirSync.getFirstDomainDetailsFromDB();
    	VWSLog.add("VWSPreferences fistDomainName....."+domainName);
    	if (domainName != null && domainName.trim().length() > 0) {
    		ldapDirectory = getTree();
        	loginServer = getSSLoginHost();
        	dirList = ldapDirectory.split(",");
        	serList = loginServer.split(",");
        	boolean isRequiredToSetRegistry = false;
        	VWSLog.add("dirList.length....."+dirList.length);
        	VWSLog.add("serList.length....."+serList.length);
        	if (dirList.length > 1 && dirList.length == serList.length) {        		
        		for (int dirIndex = 0; dirIndex < dirList.length; dirIndex ++) {
        			if (!domainName.equalsIgnoreCase(dirList[dirIndex])) {
        				domainName = domainName + "," + dirList[dirIndex];
        				loginHost = loginHost + "," + serList[dirIndex];
        			} else {
        				isRequiredToSetRegistry = true;
        			}        				
        		}
        		VWSLog.add("isRequiredToSetRegistry....."+isRequiredToSetRegistry);
        		if (isRequiredToSetRegistry) {
        			loginHost = loginHost.substring(1, loginHost.length());
        			VWSLog.add("Final domainName....."+domainName);
        			VWSLog.add("Final loginHost....."+loginHost);
        			setTree(domainName);
        			setSSLoginHost(loginHost);
        		}
        	}
        	
    	}
    }
    /*--------------------------------------------------------------*/
	/****CV2019 merges from SIDBI line--------------------------------***/
    public static String getWFSummaryDoctype()
    {
        return getNode("general").get("workflowdoctype", "");
    }
    public static boolean getWARoomCache()
    {
        return getNode("general").getBoolean("waroomlevel", false);
    }
    public static boolean getNativeWrap(){
    	  return getNode("general").getBoolean("nativeWrap", false);
    }
    public static boolean isEnableVWR(){
    	return getNode("general").getBoolean("enablevwr", false);
    }
    public static void setMsiVersion(String msiVersion)
	{
		getNode("general").put("msiversion", msiVersion.trim());
		flush();
	}
    public static void setMsiURL(String msiUrl)
	{
		getNode("general").put("msiurl", msiUrl.trim());
		flush();
	}
    public static void setUserName(String userName)
	{
		getNode("general").put("username", userName.trim());
		flush();
	}
    public static void setUserPassword(String password)
	{
		getNode("general").put("password", password.trim());
		flush();
	}
    public static void setDomainName(String domainName)
	{
		getNode("general").put("domainname", domainName.trim());
		flush();
	}
    public static String getMsiVersion1() {
    	return getNode("general").get("msiversion", "");
    }
    public static String getMsiURL1() {
    	return getNode("general").get("msiurl", "");
    }
    public static String getUserName() {
    	return getNode("general").get("username", "");
    }
    public static String getUserPassword() {
    	return getNode("general").get("password", "");
    }
    public static String getDomainName() {
    	return getNode("general").get("domainname", "");
    }
	/*--------------------------------------------------------------*/
    public static boolean getLogFlush()
    {
        return getNode("general").getBoolean("logflush", true);
    }
    public static void setBatchInputAdministrator(String batchInputAdministrator)
    {
        getNode("general").put("batchInputAdministrator", batchInputAdministrator.trim());
        flush();
    }
    public static String getBatchInputAdministrator()
    {
        return getNode("general").get("batchInputAdministrator", "");
    }
    public static String getRoomsBatchInputAdministrator(String name)
    {
    	 Preferences roomPref = getNode("rooms/" + name);
    	 return roomPref.get("batchInputAdministrator","");
    }
	public static void setRoomsBatchInputAdministrator(String name) {
		String[] tokens = name.split("#");
		Preferences roomPref = getNode("rooms/" + tokens[0]);
		roomPref.put("batchInputAdministrator", tokens[1]);
		try {
			roomPref.flush();
		} catch (BackingStoreException e) {
			e.printStackTrace();
		}
	}
	public static String getSubAdministrator(String name)
    {
    	Preferences roomPref = getNode("rooms/" + name);
    	return roomPref.get("subadmins","");
    }
	public static String getSQLConnectionString(String name) {
		Preferences roomPref = getNode("rooms/" + name);
		return roomPref.get("sqlconnectionstring", "");
	}
	public static String getAuthenticationMode(String name) {
		Preferences roomPref = getNode("rooms/" + name);
		return roomPref.get("authenticationmode", "sql");
	}
	public static boolean getIntegratedSecurity(String name) {
		Preferences roomPref = getNode("rooms/" + name);
		return roomPref.getBoolean("integratedsecurity", true);
	}
	public static String getAuthenticationScheme(String name) {
		Preferences roomPref = getNode("rooms/" + name);
		return roomPref.get("authenticationscheme", "NTLM");
	}
	public static String getDomain(String name) {
		Preferences roomPref = getNode("rooms/" + name);
		return roomPref.get("domain", "");
	}
	public static String getServerSpn(String name) {
		Preferences roomPref = getNode("rooms/" + name);
		return roomPref.get("serverspn", "");
	}
	public static RoomProperty getRoomAuthenticationProperties(String name) {
		RoomProperty roomAuthentications = new RoomProperty(name);
		roomAuthentications.setSqlConnectionString(getSQLConnectionString(name));
		roomAuthentications.setAuthenticationMode(getAuthenticationMode(name));
		roomAuthentications.setIntegratedSecurity(getIntegratedSecurity(name));
		roomAuthentications.setAuthenticationScheme(getAuthenticationScheme(name));
		roomAuthentications.setDomain(getDomain(name));
		roomAuthentications.setServerSpn(getServerSpn(name));
		return roomAuthentications;
	}
}