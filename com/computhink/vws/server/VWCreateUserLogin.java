/*
 * VWCreateUserLogin.java
 * Created on 25 May 2009
 * @Author : Vijaypriya.B.K
 */
package com.computhink.vws.server;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.swing.JDialog;
import javax.swing.UIManager;
import com.computhink.common.Util;

import com.computhink.resource.ResourceManager;
public class VWCreateUserLogin extends javax.swing.JDialog {
	private ResourceManager resourceManager=ResourceManager.getDefaultManager();
    public VWCreateUserLogin(JDialog frame, String title)
    {
	super(frame,title,true);	
	getContentPane().setLayout(null);
	setSize(350,200);	            
	PnlGeneral.setBorder(new javax.swing.border.TitledBorder(lbl_Border_Title));
	PnlGeneral.setLayout(null);
	getContentPane().add(PnlGeneral, BorderLayout.CENTER);
	PnlGeneral.setBounds(6,8,330,120);
	JLabel1.setText(lbl_UserName);
	PnlGeneral.add(JLabel1);
	JLabel1.setBounds(12,24,78, 22);
	JLabel2.setText(lbl_Password);
	PnlGeneral.add(JLabel2);
	JLabel2.setBounds(12,52,68,28);
	JLabel3.setText(lbl_ConfPassword);
	PnlGeneral.add(JLabel3);
	JLabel3.setBounds(12,82,152,28);
	PnlGeneral.add(TxtUserName);
	TxtUserName.setBounds(108,24,200,20);
	PnlGeneral.add(TxtPassword);
	TxtPassword.setBounds(108,56,200,20);
	PnlGeneral.add(TxtConfirmPassword);
	TxtConfirmPassword.setBounds(108,87,200,20);

	BtnCreateLogin.setText(resourceManager.getString("CreateUserLoginBtn.CreateUser"));
	//BtnCreateLogin.setBackground(java.awt.Color.white);
	getContentPane().add(BtnCreateLogin);
	BtnCreateLogin.setBounds(70,135,78,23);

	BtnCancel.setText(resourceManager.getString("CreateUserLoginBtn.ExistingUser"));
	//BtnCancel.setBackground(java.awt.Color.white);
	getContentPane().add(BtnCancel);
	BtnCancel.setBounds(163,135,78,23);

	BtnClose.setText(resourceManager.getString("CreateUserLoginBtn.Close"));
	getContentPane().add(BtnClose);
	BtnClose.setBounds(256, 135, 78, 23);

	//{{REGISTER_LISTENERS

	SymAction lSymAction = new SymAction();
	BtnCreateLogin.addActionListener(lSymAction);
	BtnCancel.addActionListener(lSymAction);
	BtnClose.addActionListener(lSymAction);
	SymKey aSymKey = new SymKey();
	addKeyListener(aSymKey);
	TxtUserName.addKeyListener(aSymKey);
	TxtPassword.addKeyListener(aSymKey);
	TxtConfirmPassword.addKeyListener(aSymKey);
	BtnCreateLogin.addKeyListener(aSymKey);
	BtnCancel.addKeyListener(aSymKey);
	BtnClose.addKeyListener(aSymKey);
	addWindowListener(new WindowAdapter(){
	    public void windowClosing(WindowEvent e) {
		setCancel("Cancel");
		//System.out.println("Closing...");
	    }
	});
	setResizable(false);

	setLocation(frame.getLocationOnScreen().x+55, frame.getLocationOnScreen().y+100);
	setVisible(true);

    }
//  ------------------------------------------------------------------------------
    class SymKey extends java.awt.event.KeyAdapter
    {
	public void keyTyped(java.awt.event.KeyEvent event)
	{
	    Object object = event.getSource();
	    if(event.getKeyText(event.getKeyChar()).equals("Enter"))
	    {
		BtnCreateLogin.requestFocus();
		BtnCreateLogin_actionPerformed(null);
	    }
	    else if(event.getKeyText(event.getKeyChar()).equals("Escape"))
	    {
		BtnCancel.requestFocus();
		BtnCancel_actionPerformed(null);
	    }
	}
    }
//  ------------------------------------------------------------------------------
    class SymAction implements java.awt.event.ActionListener
    {
	public void actionPerformed(java.awt.event.ActionEvent event)
	{
	    Object object = event.getSource();
	    if (object == BtnCreateLogin)
		BtnCreateLogin_actionPerformed(event);
	    else if (object == BtnCancel)
		BtnCancel_actionPerformed(event);
	    else if (object == BtnClose)
		BtnClose_actionPerformed(event);
	}
    }
//  ------------------------------------------------------------------------------
    //{{DECLARE_CONTROLS
    javax.swing.JPanel PnlGeneral = new javax.swing.JPanel();
    javax.swing.JLabel JLabel1 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel2 = new javax.swing.JLabel();
    javax.swing.JLabel JLabel3 = new javax.swing.JLabel();
    javax.swing.JTextField TxtUserName = new javax.swing.JTextField(50);
    javax.swing.JPasswordField TxtPassword = new javax.swing.JPasswordField(50);
    javax.swing.JPasswordField TxtConfirmPassword = new javax.swing.JPasswordField(50);
    javax.swing.JButton BtnCreateLogin = new javax.swing.JButton();
    javax.swing.JButton BtnCancel = new javax.swing.JButton();
    javax.swing.JButton BtnClose = new javax.swing.JButton();
//  ------------------------------------------------------------------------------
    void BtnCreateLogin_actionPerformed(java.awt.event.ActionEvent event)
    {
	//Verify password.
	if(verifyPassword()){
	    setVisible(false);
	}
	else{
	    Util.Msg(this,resourceManager.getString("CreateUserLoginMsg.ConfirmPassword1"),resourceManager.getString("CreateUserLoginMsg.ConfirmPassword2"));
	    TxtPassword.setText("");
	    TxtConfirmPassword.setText("");
	    setVisible(true);
	}
    }
//  ------------------------------------------------------------------------------
    void BtnCancel_actionPerformed(java.awt.event.ActionEvent event)
    {
	setCancel("Cancel");
	setVisible(false);
    }
//  ------------------------------------------------------------------------------
    void BtnClose_actionPerformed(java.awt.event.ActionEvent event)
    {
	closeFlag = true;
	setVisible(false);
    }
//  ------------------------------------------------------------------------------
    public String[] getValues()
    {
	String[] values=new String[3];
	values[0]= TxtUserName.getText();
	values[1]= new String(TxtPassword.getPassword());
	values[2]= new String(TxtConfirmPassword.getPassword());
	return values;
    }
//  ------------------------------------------------------------------------------
    public boolean verifyPassword()
    {
	boolean flag = true;
	String password = new String(TxtPassword.getPassword());
	String confirmPassword = new String(TxtConfirmPassword.getPassword());
	if(!password.equals(confirmPassword)){
	    flag = false;
	}
	return flag;
    }
//  ------------------------------------------------------------------------------

    String lbl_Border_Title=resourceManager.getString("CreateUserLoginLbl.UserCredential");
    String lbl_UseFor=resourceManager.getString("CreateUserLoginLbl.UseCredentialsFor");
    String lbl_UserName=resourceManager.getString("CreateUserLoginLbl.UserName");
    String lbl_Password=resourceManager.getString("CreateUserLoginLbl.Passoword");
    String lbl_ConfPassword =resourceManager.getString("CreateUserLoginLbl.ConfPassword");
    String cancel = "";
    boolean closeFlag = false;

    public boolean getClose(){
	return this.closeFlag;
    }
    public String getCancel() {
	return cancel;
    }
    public void setCancel(String cancel) {
	this.cancel = cancel;
    }
    public static void main(String[] args) {
	try{
		String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
		UIManager.setLookAndFeel(plasticLookandFeel);
	}catch(Exception ex){}
	JDialog dialog = new JDialog();
	dialog.setLocation(100, 100);
	dialog.setVisible(true);
	VWCreateUserLogin login = new VWCreateUserLogin(dialog, "Create New User");	
    }
}
