
package com.computhink.vws.server;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import com.computhink.common.CVPreferences;
import com.computhink.common.Constants;
import com.computhink.common.Creator;
import com.computhink.common.Group;
import com.computhink.common.GroupUser;
import com.computhink.common.Principal;
import com.computhink.common.Util;
import com.computhink.database.Database;
import com.computhink.resource.ResourceManager;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWUtil;
import com.computhink.vws.directory.Directory;
import com.computhink.vws.directory.UserGroupInXML;
import com.computhink.vws.server.mail.VWMail;

public class DirectorySync
{
 
	Directory directory;
    Database database;
    String room;
    private boolean gIgnoreSync = false;
    private boolean isAdminWiseCall = false;
    private static boolean isUserGroupXMLGenerated = false;
    private boolean isSyncRequired = false;
    
	private String adminGroup = "";
	private String namedGroup = "";
    private String namedOnlineGroup="";
    private String concurrentGroup="";
    private String concurrentOnlineGroup="";
    private String professionalConcrGroup="";
    private String professionalNamedGroup = "";
    private String publicWAGroup="";
    private String subAdminGroup = "";
    private String batchInputGroup = "";
  
    private int adminGrpCount=-1;
	private int namedGrpCount = -1;
	private int namedOnlineGrpCount=-1;
    private int concurrentGrpCount=-1;
    private int concurrentOnlineGrpCount=-1;
    private int professionalNamedGrpCount=-1;
    private int professConcrGrpCount=-1;
    private int publicWAGrpCount=-1;
    private int subAdminGrpCount=-1;
    private int batchInputGrpCount = -1;

    private int dirAdminGrpCount=-1;
    private int dirNamedGrpCount = -1;
    private int dirNamedOnlineGrpCount=-1;
    private int dirConcurrentGrpCount=-1;
    private int dirConcurrentOnlineGrpCount=-1;
    private int dirProfessionalNamedGrpCount = -1;
    private int dirProfConcrGrpCount=-1;
	private int dirpublicWAGrpCount=-1;
	private int dirSubAdminGrpCount=-1;
	private int dirBatchInputGrpCount=-1;
	
	/**CV2019 AD/LDAP changes**/
	private List dirAdminGrp = null;
	private List dirNamedGrp = null;
	private List dirNamedOnlineGrp = null;
	private List dirConcurrentGrp = null;
	private List dirConcurrentOnlineGrp = null;
	private List dirProfessionalNamedGrp = null;
	private List dirProfConcrGrp = null;
	private List dirpublicWAGrp = null;
	private List dirSubAdminGrp = null;
	private List dirBatchInputGrp=null;

    private int dirRoomLevelNamedGrpCount = -1;
    private int dirRoomLevelNamedOnlineGrpCount = -1;
    private int dirRoomLevelProfessionalNamedGrpCount = -1;
    private int dirRoomLevelConcurrentGrpCount=-1;
    private int dirRoomLevelConcurrentOnlineGrpCount=-1;
    private int dirRoomLevelProfConcrGrpCount=-1;
	private int dirRoomLevelpublicWAGrpCount=-1;
	private int dirRoomLevelBatchInputAdminGrpCount = -1;
	
	/**CV2019 AD/LDAP changes**/
	private List dirRoomLevelNamedGrp = null;
	private List dirRoomLevelNamedOnlineGrp = null;
	private List dirRoomLevelProfessionalNamedGrp = null;
	private List dirRoomLevelConcurrentGrp = null;
	private List dirRoomLevelConcurrentOnlineGrp = null;
	private List dirRoomLevelProfConcrGrp = null;
	private List dirRoomLevelpublicWAGrp = null;
	private List dirRoomLevelBatchInputAdminGrp = null; 
	
	private List usersWithoutUserInfo = new Vector();
	private boolean syncFailed = false;
		
	
	public DirectorySync(Directory directory)
    {
        this.directory = directory;
       
        getUsrsGrpsFromSecurityServer();
    }
	/**CV2019 code merges from CV10.2 line**/
	public DirectorySync(Database database)
    {
    	this.database = database;
    }

    public DirectorySync(Database database, Directory directory, boolean isAdminWiseCall)
    {
		this(database, directory, isAdminWiseCall, "", "", "", "", "", "", "", "", "", "", -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, true);
    }

	public DirectorySync(Database database, Directory directory, boolean isAdminWiseCall, String adminGroup,
			String namedGroup, String namedOnlineGroup, String concurrentGroup, String concurentOnlineGroup,
			String professionalNamedGroup, String professionalConcurrentGroup, String publicWAGroup,
			String subAdminGroup, String batchInputGroup, int adminGrpCount, int namedGrpCount, int namedOnlineGrpCount,
			int concurrentGroupCount, int concurrentOnlineGroupCount, int professionalGroupCount,
			int professionalConcurrentGroupCount, int publicWAGrpCount, int subAdminGrpCount, int batchInputGrpCount, boolean isFromAdminSync) {
    	VWSLog.dbg("publicWAGroupCount inside :::"+publicWAGrpCount);
        this.directory = directory;
        this.database = database;
        this.room = database.getName();
        this.isAdminWiseCall = isAdminWiseCall;
		/**CV2019 code merges from CV10.2 line**/
        if (adminGroup != null && adminGroup.trim().length() > 0) {
	        setAdminGroup(adminGroup);
	        setSubAdminGroup(subAdminGroup);
	        setNamedGroup(namedGroup);
	        setNamedOnlineGroup(namedOnlineGroup);
	    	setProfessionalNamedGroup(professionalNamedGroup);
			setConcurrentGroup(concurrentGroup);
			VWSLog.dbg("before settingconcurentOnlineGroup :::"+concurentOnlineGroup);
	    	setConcurrentOnlineGroup(concurentOnlineGroup);
	    	setProfessionalConcrGroup(professionalConcurrentGroup);
			setPublicWAGroup(publicWAGroup);
			setBatchInputGroup(batchInputGroup);
	
			setNamedGrpCount(namedGrpCount);
	        setNamedOnlineGrpCount(namedOnlineGrpCount);
	        setAdminGrpCount(adminGrpCount);
	        setSubAdminGrpCount(subAdminGrpCount);
	        SetProfessionalNamedGrpCount(professionalGroupCount);
	        setConcurrentGrpCount(concurrentGroupCount);
			VWSLog.dbg("license count:::"+concurrentOnlineGroupCount);
	        setConcurrentOnlineGrpCount(concurrentOnlineGroupCount);
	        setProfessConcrGrpCount(professionalConcurrentGroupCount);
	        setPublicWAGrpCount(publicWAGrpCount);
	        setBatchInputGrpCount(batchInputGrpCount);
	        
	        List adminList = getDirectoryUsersInGroup(this.dirAdminGrp, adminGroup);
	        VWSLog.dbg("User count for Admin Group >>>> "+adminList.size());
	        if (adminList.size() == 0) {
	        	setSyncFailed(true);
	        	VWSLog.dbg("Failed to fetch the users from directory........");
	        	return;
	        }
	        
	        VWSLog.dbg("this.directory.getDirName() from DirectorySync: " + this.directory.getDirName());		
			if((this.directory.getScheme()!=null)&&(this.directory.getScheme().length()>0)){
		       	if(this.directory.getScheme().equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)){	   
		       		String[] dirList = null;
		       	  	String ldapDirectory = VWSPreferences.getTree();
		  			dirList = ldapDirectory.split(",");
		  			VWSLog.dbg("dirList[0] from DirectorySync: " + dirList[0]);
		       		boolean updateUpnSuffix = checkUpnsuffixSettingsInfo();
		       		VWSLog.dbg("updateUpnSuffix..........." + updateUpnSuffix);
		       		// When this updateUpnSuffix is false then update the first domain group table upnsuffix with empty
		        	if (updateUpnSuffix == false 
		        			&& this.directory.getDirName().equalsIgnoreCase(dirList[0])) {
		        		VWSLog.dbg("Before calling update upnsuffix for GroupTable " );
		        		updateFirstDomain(dirList[0]);	
		        	}
				}
			}
					
	        VWSLog.dbg("Before calling Named, Professional Name, Concurrent, Professional Concurrent group users from DirectorySync method");     
	        boolean isHosting = false;
		    try{
		    	isHosting = CVPreferences.getHosting();
		    }catch (Exception e) {
		    	isHosting = false;
		    }
		    setDirAdminGrpCount(0);
		    setDirSubAdminGrpCount(0);
		    if (!isHosting) {	        
		        setDirNamedGrpCount(0);
		        setDirNamedOnlineGrpCount(0);
		        setDirProfessionalNamedGrpCount(0);
		        setDirConcurrentGrpCount(0);
		        setDirConcurrentOnlineGrpCount(0);
		        setDirProfConcrGrpCount(0);
		        setDirPublicWAGrpCount(0);	
		        setDirBatchInputGrpCount(0);
	        } else {
	        	setDirRoomLevelNamedGrpCount(0);
	        	setDirRoomLevelNamedOnlineGrpCount(0);
		        setDirRoomLevelProfessionalNamedGrpCount(0);
		        setDirRoomLevelConcurrentGrpCount(0);
		        setDirRoomLevelConcurrentOnlineGrpCount(0);
		        setDirRoomLevelProfConcrGrpCount(0);
		        setDirRoomLevelBatchInputAdminGrpCount(0);
	        }
		    VWSLog.dbg("After calling Named, Professional Name, Concurrent, Professional Concurrent group users from DirectorySync method ");
	        VWSLog.dbg("isAdminWiseCall value inside DirectorySync: " + isAdminWiseCall);
	        /**CV10.2 registered group/user pulling changes**/
	        /**Code optimized to fix LDAP Novell issue for Alberta Justice E-Directory Syncing.Changed by vanitha on 06-02-2020***/
			if (!isAdminWiseCall) {
				VWSLog.dbg("Inside sync process");
	            getUsrsGrpsFromSecurityServer();
	            VWSLog.dbg("is from admin sync : "+ isFromAdminSync);
	            if (!isFromAdminSync) {
	            	syncRegisteredUsers();
		        	syncRegisteredGroups();
		        } else {
		        	VWSLog.dbg("Before calling  syncUsers in Ldap sync process");
		            syncUsers();
		            VWSLog.dbg("Before calling syncGroups in Ldap sync process");
		            syncGroups();
		            VWSLog.dbg("Before calling  syncLinks in Ldap sync process");
		            syncLinks();
		        }
	        }  
	    }
    }
    
   


	/**CV2019 code merges from CV10.2 line**/
	public void syncProcess(boolean isFromAdminSync){
		VWSLog.dbg("Inside syncProcess::::::");
		VWSLog.dbg("isAdminWiseCall: "+ isAdminWiseCall);
		VWSLog.dbg("is from admin sync : "+ isFromAdminSync);
        getUsrsGrpsFromSecurityServer();
        if (!isFromAdminSync) {
        	VWSLog.dbg("Inside syncProcess before syncRegisteredUser :::");
        	syncRegisteredUsers();
        	VWSLog.dbg("Inside syncProcess before syncRegisteredGroups :::");
        	syncRegisteredGroups();
        } else {
	        VWSLog.dbg("Inside syncProcess before syncUsers :::");
	        syncUsers();
	        VWSLog.dbg("Inside syncProcess before syncGroups :::");
	        syncGroups();        
	        VWSLog.dbg("Inside syncProcess before syncLinks :::");
	        syncLinks(); 
        }
        syncFailedUserUPN();        
    }
    public Vector getDirectoryPrincipals(Directory directory1,String loginServer)
    {
    	VWSLog.dbg("Inside getDirectoryPrincipals from directorySync...");
    	Vector principals=new Vector();
    	Vector groups = new Vector();
    	if(isUserGroupXMLGenerated && isAdminWiseCall)
    		groups = getGroupsFromXML();
    	else
    		groups = directory1.getGroups1(directory1.getDirName(), loginServer);
    	Vector ret = new Vector();
    	int groupId=0;
    	int userId = 0;
    	int count=0;
    	if(groups!=null)    count=groups.size();



    	ret = new Vector();
    	String param = "";
    	if(database.getEngineName().equalsIgnoreCase("Oracle"))
    		param = "2" + Util.SepChar + "~" + Util.SepChar;
    	else
    		param = "2" + Util.SepChar + "~";
    	DoDBAction.get(database,"Group_Get",param,ret);
    	Hashtable groupTable = new Hashtable(); 
    	for (int cnt = 0; cnt < ret.size(); cnt++){
    		StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
    		int id = Util.to_Number(st.nextToken());
    		String name = st.nextToken();
    		String mailId = "";
    		if(st.hasMoreTokens())
    			mailId = st.nextToken();
    		else
    			mailId = "-";
    		Creator group = new Creator(id, name, mailId,"");            
    		groupTable.put(name.toLowerCase(), group);
    	}
    	VWSLog.dbg("groupTable values :"+groupTable);
    	VWSLog.dbg("groups count from directory :"+count);
    	for(int i=0;i<count;i++)
    	{
    		String groupName = (String) groups.get(i);
    		VWSLog.dbg("groups name from directory :"+groupName);
    		if (!groupTable.containsKey(groupName.toLowerCase())){
    			Principal principal = new Principal(0,groupName,Principal.GROUP_TYPE,directory1.getDirName().toString());
    			principals.add(principal);
    		}
    	}
    	Vector users = new Vector();
    	if(isUserGroupXMLGenerated && isAdminWiseCall) {
    		users = getUsersFromXML();
    	}
    	else
    	{
    		VWSLog.dbg("Inside getDirectoryPrincipals and before getting users from directory (directory.getUsers()).."+directory1.getDirName());
    		users = directory1.getUsers1(directory1.getDirName(), loginServer);

    		String schemeType = VWSPreferences.getSSType();
    		/**
    		 * CV10.1 LDAP Issue fix.
    		 * 21/6/2017
    		 */
    		if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
    			VWSLog.dbg("users list after pulling from directory:: " + users);
    			String groupName = "";
    			String otherOUUsers = "";
    			Vector ouUsers = new Vector();
    			
    			boolean syncotherouFlag = VWSPreferences.getSyncOtherOUUsers();
    			VWSLog.dbg("syncotherouFlag ::::"+syncotherouFlag);
    			if(syncotherouFlag==true) {
    				for(int k=0;k<count;k++)
    				{
    					try{
    						groupName = (String) groups.get(k);
    						ouUsers.addAll(getUsersInGroup_New(groupName, 1));
    						VWSLog.dbg("Users pulled for group: " + groupName + " and users : " + ouUsers );
    						if(ouUsers.size()>0&&ouUsers!=null){
    							for(int i=0;i<ouUsers.size();i++){
    								otherOUUsers = ouUsers.get(i).toString();
    								otherOUUsers = otherOUUsers.substring(0,otherOUUsers.indexOf("@"));
    								VWSLog.dbg("otherOUUsers : " + otherOUUsers);
    								if(!(users.contains(otherOUUsers))){
    									VWSLog.dbg("user that does not exist in: " + otherOUUsers);
    									users.add(otherOUUsers);
    								}  
    							}
    						}
    					}catch(Exception ex){
    						VWSLog.dbg("Error while sync groups inside getdirectoryprincipals" + ex.getMessage() + " ::: " + groupName);
    					}
    				}
    			}
    	}
    	}
    	count=0;
    	if(users!=null) 
    		count=users.size();
    	VWSLog.dbg("Count for users pulled from directory..." + count);
    	ret = new Vector();
    	param = "";
    	if(database.getEngineName().equalsIgnoreCase("Oracle"))
    		param = "2"; //+ Util.SepChar + Util.getOneSpace(userName);
    	else
    		param = "2"; //+ userName;
    	DoDBAction.get(database,"User_Get",param,ret);
    	Hashtable userTable = new Hashtable(); 
    	VWSLog.dbg("users pulled from db..." + ret);
    	VWSLog.dbg("size of ret after getting users from db..." + ret.size());
    	for (int cnt = 0; cnt < ret.size(); cnt++){
    		StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
    		int id = Util.to_Number(st.nextToken());
    		String name = st.nextToken();
    		String mailId = "";
    		if(st.hasMoreTokens())
    			mailId = st.nextToken();
    		else
    			mailId = "-";
    		Creator user = new Creator(id, name, mailId,"");            
    		userTable.put(name.toLowerCase(), user);
    	}
    	VWSLog.dbg("Count for users pulled from directory..." + count);
    	for(int i=0;i<count;i++)
    	{
    		String userName = (String) users.get(i);
    		if (!userTable.containsKey(userName.toLowerCase())){
    			VWSLog.dbg("user pull from directory not exists in db..." + userName);
    			Principal principal = new Principal(0,userName,Principal.USER_TYPE,directory1.getDirName().toString());        	
    			principals.add(principal);
    		}
    	}
    	/**
    	 * CV110.1 Enhancement.
    	 * Commented on :- 9/6/2017
    	 */
    	/********CV10.1 pulling users to display in add dialog when sync users is true*****/
    	/*String schemeType = VWSPreferences.getSSType();
        if ((VWSPreferences.getSyncGroupUsers()==true) && (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)))
        {
        	List usersInGroup=new ArrayList();
        	for(int i=0;i<groups.size();i++)
        	{
        		String groupName = (String) groups.get(i);
        		usersInGroup=  getUsersInGroup( groupName);
        		for(int j=0;j<usersInGroup.size();j++){
        			String userInGroup=usersInGroup.get(j).toString();
        			if(!(users.contains(userInGroup.toLowerCase()))){
        				if (!userTable.containsKey(userInGroup.toLowerCase())){
        					Principal principal = new Principal(0,userInGroup,Principal.USER_TYPE);  
        					principals.add(principal);
        				}
        			}
        		}
        	}
        }*/
    	/************************End of CV10.1  displaying users in add dialog******************/       
    	return principals;
    }
    
    
    /*public Vector getDirectoryPrincipalsNew(Directory directory1,String loginServer,String searcForPName) {
    	VWSLog.dbg("getDirectoryPrincipalsNew Inside getDirectoryPrincipals from directorySync...");
    	Vector principals=new Vector();
    	Vector groups = new Vector();
    	if(isUserGroupXMLGenerated && isAdminWiseCall)
    		groups = getGroupsFromXML();
    	else
    		groups = directory1.getGroups1New(directory1.getDirName(), loginServer, searcForPName);
    	
    	VWSLog.dbg("getDirectoryPrincipalsNew groups.size()..." + groups.size());
    	Vector ret = new Vector();
    	int groupId=0;
    	int userId = 0;
    	int count=0;
    	if(groups!=null)    count=groups.size();



    	ret = new Vector();
    	String param = "";
    	if(database.getEngineName().equalsIgnoreCase("Oracle"))
    		param = "2" + Util.SepChar + "~" + Util.SepChar;
    	else
    		param = "2" + Util.SepChar + "~";
    	DoDBAction.get(database,"Group_Get",param,ret);
    	Hashtable groupTable = new Hashtable(); 
    	for (int cnt = 0; cnt < ret.size(); cnt++){
    		StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
    		int id = Util.to_Number(st.nextToken());
    		String name = st.nextToken();
    		String mailId = "";
    		if(st.hasMoreTokens())
    			mailId = st.nextToken();
    		else
    			mailId = "-";
    		Creator group = new Creator(id, name, mailId);            
    		groupTable.put(name.toLowerCase(), group);
    	}
    	for(int i=0;i<count;i++)
    	{
    		String groupName = (String) groups.get(i);
    		if (!groupTable.containsKey(groupName.toLowerCase())){
    			Principal principal = new Principal(0,groupName,Principal.GROUP_TYPE,directory1.getDirName().toString());
    			principals.add(principal);
    		}
    	}
    	Vector users = new Vector();
    	if(isUserGroupXMLGenerated && isAdminWiseCall) {
    		users = getUsersFromXML();
    	}
    	else
    	{
    		VWSLog.dbg("Inside getDirectoryPrincipals and before getting users from directory (directory.getUsers()).."+directory1.getDirName());
    		users = directory1.getUsers1New(directory1.getDirName(), loginServer, searcForPName);

    		String schemeType = VWSPreferences.getSSType();
    		/**
    		 * CV10.1 LDAP Issue fix.
    		 * 21/6/2017
    		 */
    		/*if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
    			VWSLog.dbg("users list after pulling from directory:: " + users);
    			String groupName = "";
    			String otherOUUsers = "";
    			Vector ouUsers = new Vector();
    			
    			boolean syncotherouFlag = VWSPreferences.getSyncOtherOUUsers();
    			VWSLog.dbg("syncotherouFlag ::::"+syncotherouFlag);
    			if(syncotherouFlag==true) {
    				for(int k=0;k<count;k++)
    				{
    					try{
    						groupName = (String) groups.get(k);
    						ouUsers.addAll(getUsersInGroup_New(groupName, 1));
    						VWSLog.dbg("Users pulled for group: " + groupName + " and users : " + ouUsers );
    						if(ouUsers.size()>0&&ouUsers!=null){
    							for(int i=0;i<ouUsers.size();i++){
    								otherOUUsers = ouUsers.get(i).toString();
    								otherOUUsers = otherOUUsers.substring(0,otherOUUsers.indexOf("@"));
    								VWSLog.dbg("otherOUUsers : " + otherOUUsers);
    								if(!(users.contains(otherOUUsers))){
    									VWSLog.dbg("user that does not exist in: " + otherOUUsers);
    									users.add(otherOUUsers);
    								}  
    							}
    						}
    					}catch(Exception ex){
    						VWSLog.dbg("Error while sync groups inside getdirectoryprincipals" + ex.getMessage() + " ::: " + groupName);
    					}
    				}
    			}
    	}
    	}
    	count=0;
    	if(users!=null) 
    		count=users.size();
    	VWSLog.dbg("Count for users pulled from directory..." + count);
    	ret = new Vector();
    	param = "";
    	if(database.getEngineName().equalsIgnoreCase("Oracle"))
    		param = "2"; //+ Util.SepChar + Util.getOneSpace(userName);
    	else
    		param = "2"; //+ userName;
    	DoDBAction.get(database,"User_Get",param,ret);
    	Hashtable userTable = new Hashtable(); 
    	VWSLog.dbg("users pulled from db..." + ret);
    	VWSLog.dbg("size of ret after getting users from db..." + ret.size());
    	for (int cnt = 0; cnt < ret.size(); cnt++){
    		StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
    		int id = Util.to_Number(st.nextToken());
    		String name = st.nextToken();
    		String mailId = "";
    		if(st.hasMoreTokens())
    			mailId = st.nextToken();
    		else
    			mailId = "-";
    		Creator user = new Creator(id, name, mailId);            
    		userTable.put(name.toLowerCase(), user);
    	}
    	VWSLog.dbg("Count for users pulled from directory..." + count);
    	for(int i=0;i<count;i++)
    	{
    		String userName = (String) users.get(i);
    		if (!userTable.containsKey(userName.toLowerCase())){
    			VWSLog.dbg("user pull from directory not exists in db..." + userName);
    			Principal principal = new Principal(0,userName,Principal.USER_TYPE,directory1.getDirName().toString());        	
    			principals.add(principal);
    		}
    	}
    	/**
    	 * CV110.1 Enhancement.
    	 * Commented on :- 9/6/2017
    	 */
    	/********CV10.1 pulling users to display in add dialog when sync users is true*****/
    	/*String schemeType = VWSPreferences.getSSType();
        if ((VWSPreferences.getSyncGroupUsers()==true) && (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)))
        {
        	List usersInGroup=new ArrayList();
        	for(int i=0;i<groups.size();i++)
        	{
        		String groupName = (String) groups.get(i);
        		usersInGroup=  getUsersInGroup( groupName);
        		for(int j=0;j<usersInGroup.size();j++){
        			String userInGroup=usersInGroup.get(j).toString();
        			if(!(users.contains(userInGroup.toLowerCase()))){
        				if (!userTable.containsKey(userInGroup.toLowerCase())){
        					Principal principal = new Principal(0,userInGroup,Principal.USER_TYPE);  
        					principals.add(principal);
        				}
        			}
        		}
        	}
        }*/
    	/************************End of CV10.1  displaying users in add dialog******************/       
    	/*return principals;
    }*/
    
    /***
     * CV10.1 issue fixes and CV10.2 - search filter enhancement
     * @param directory1
     * @param loginServer
     * @param searcForPName
     * @return
     */
    public Vector getDirectoryPrincipalsNew(Directory directory1,String loginServer,String searcForPName)
    {
    	String schemeType = VWSPreferences.getSSType();
    	String[] dirList = null;
    	String ldapDirectory = VWSPreferences.getTree();
		dirList = ldapDirectory.split(",");
		VWSLog.add("getDirectoryPrincipalsNew Inside getDirectoryPrincipals from directorySync..." + directory1.getDirName());
    	Vector principals=new Vector();
    	Vector groups = new Vector();
    	Vector users = new Vector();
    	Vector ret = new Vector();
    	String groupName = null, otherOUUsers = null;
		Vector ouUsers = new Vector();
    	int groupCount=0, usersCount = 0;
    	String param = "";    	
    	String userName = null, userNameAlone = null;
    	String[] userNameArr = null;
    	String filter = null, filter1 = null;
		if (searcForPName != null && searcForPName.trim().length() > 0) {
			if (searcForPName.contains("*.*")) {
	        	VWSLog.dbg("*.* found in add dialog search  ");
	        	searcForPName = searcForPName.replace("*.*", "*");
	        }
			filter = getSearchFilter(searcForPName);
		}
		
		VWSLog.add("filter search in add dialog search : " + filter);
		if (filter != null && filter.trim().length() > 0) {
			if (filter.trim().contains("objectClass=group")) {
    			filter = "(&"+filter+")";
    			groups = directory1.getAttributeList(directory1.getDirName(), loginServer, filter);
				
    		} else if (filter.trim().contains("objectClass=Person")) {
    			filter = "(&"+filter+")";
    			users = directory1.getAttributeList(directory1.getDirName(), loginServer, filter);
    		} else {
    			filter1 = "(&(objectClass=group)"+filter+")";
    			groups = directory1.getAttributeList(directory1.getDirName(), loginServer, filter1);
    			filter1 = "(&(objectClass=Person)"+filter+")";
    			users = directory1.getAttributeList(directory1.getDirName(), loginServer, filter1);    			
    		}
    		/*if (filter.trim().toUpperCase().contains("CN=GROUP")) {
    			filter = "(&(objectClass=group)("+filter+"))";
    			groups = directory1.getAttributeList(directory1.getDirName(), loginServer, filter);
				
    		} else if (filter.trim().toUpperCase().contains("CN=USER")) {
    			filter = "(&(objectClass=Person)("+filter+"))";
    			users = directory1.getAttributeList(directory1.getDirName(), loginServer, filter);
    		} else {
    			filter1 = "(&(objectClass=group)("+filter+"))";
    			groups = directory1.getAttributeList(directory1.getDirName(), loginServer, filter1);
    			filter1 = "(&(objectClass=Person)("+filter+"))";
    			users = directory1.getAttributeList(directory1.getDirName(), loginServer, filter1);    			
    		}*/
    			
			if (groups != null)
				groupCount = groups.size();
			if (users != null)
				usersCount = users.size();
    	} else {
    		/*Group Search -----------------*/
	    	if(isUserGroupXMLGenerated && isAdminWiseCall) {
	    		groups = getGroupsFromXML();
	    	} else {
	    		groups = directory1.getGroups1New(directory1.getDirName(), loginServer, searcForPName);
	    	}
	    	VWSLog.add("Getting groups from directory..." + groups.size() + " groups : " + groups);
			if (groups != null)
				groupCount = groups.size();
	    	
	    	/*Users Search -----------------*/
			if (isUserGroupXMLGenerated && isAdminWiseCall) {
				users = getUsersFromXML();
			} else {
				VWSLog.add("Inside getDirectoryPrincipals and before getting users from directory (directory.getUsers()).."+ directory1.getDirName());
				users = directory1.getUsers1New(directory1.getDirName(), loginServer, searcForPName);
				VWSLog.add("users list after pulling from directory:: " + users);
				
				/**
				 * CV10.1 LDAP Issue fix. 21/6/2017
				 */
				if (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
					boolean syncotherouFlag = VWSPreferences.getSyncOtherOUUsers();
					VWSLog.dbg("syncotherouFlag ::::" + syncotherouFlag);
					if (syncotherouFlag == true) {
						for (int k = 0; k < groupCount; k++) {
							try {
								groupName = (String) groups.get(k);
								ouUsers.addAll(getUsersInGroup_New(groupName, 1));
								VWSLog.add("Users pulled for group: " + groupName + " and users : " + ouUsers+ " count: " + ouUsers.size());
								if (ouUsers.size() > 0 && ouUsers != null) {
									for (int i = 0; i < ouUsers.size(); i++) {
										otherOUUsers = ouUsers.get(i).toString();
										VWSLog.dbg("otherOUUsers: " + otherOUUsers);
										String[] OtherOUUsersArr = otherOUUsers.split("\t");
										if (dirList.length == 1) {
											otherOUUsers = otherOUUsers.substring(0, otherOUUsers.indexOf("@"));
											if (!(users.contains(otherOUUsers.toLowerCase()))) {
												VWSLog.dbg("user that does not exist in: "+ OtherOUUsersArr[0].toLowerCase());
												users.add(otherOUUsers.toLowerCase());
											}
										} else {
											VWSLog.dbg("Before checking user exists in users pulled from Directory OtherOUUsersArr[0].toLowerCase() : "+ OtherOUUsersArr[0].toLowerCase());
											if (!(users.contains(OtherOUUsersArr[0].toLowerCase()))) {
												VWSLog.dbg("user that does not exist in: "+ OtherOUUsersArr[0].toLowerCase());
												users.add(OtherOUUsersArr[0].toLowerCase());
											}
										}
									}
								}
							} catch (Exception ex) {
								VWSLog.dbg("Error while sync groups inside getdirectoryprincipals" + ex.getMessage()+ " ::: " + groupName);
							}
						}
					}
				}
			}
			if (users != null)
				usersCount = users.size();
	    }
		VWSLog.dbg("Count for groups pulled from directory..." + groupCount);
		if (groupCount > 0) {
	    	ret = new Vector();
	    	if (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS) && dirList.length > 1) {
				if (directory1.getDirName().trim().equalsIgnoreCase(dirList[0].trim()))
					param = "2" + Util.SepChar + "~";
				else
					param = "2" + Util.SepChar + directory1.getDirName().trim();
			} else {
				param = "2" + Util.SepChar + "~";
			}
	    	if(database.getEngineName().equalsIgnoreCase("Oracle"))
	    		param = param + Util.SepChar;
	    	DoDBAction.get(database,"Group_Get",param,ret);
	    	Hashtable groupTable = new Hashtable(); 
	    	for (int cnt = 0; cnt < ret.size(); cnt++){
	    		StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
	    		int id = Util.to_Number(st.nextToken());
	    		String name = st.nextToken();
	    		String mailId = "";
	    		if(st.hasMoreTokens())
	    			mailId = st.nextToken();
	    		else
	    			mailId = "-";
	    		Creator group = new Creator(id, name, mailId,"");            
	    		groupTable.put(name.toLowerCase(), group);
	    	}
	    	VWSLog.dbg("groupTable value :" + groupTable);
	    	for(int i=0;i<groupCount;i++)
	    	{
	    		groupName = (String) groups.get(i);
	    		if (!groupTable.containsKey(groupName.toLowerCase())){
	    			Principal principal = new Principal(0,groupName,Principal.GROUP_TYPE,directory1.getDirName().toString(), schemeType, ldapDirectory);
	    			VWSLog.add("principal getDirectoryPrincipalsNew  :::: "+principal);
	    			principals.add(principal);
	    		}
	    	}
    	}
    	VWSLog.dbg("Count for users pulled from directory..." + usersCount);
    	if (usersCount > 0) {
    		List usersFromDB = new ArrayList();
        	Hashtable userTable = new Hashtable();
    		ret = new Vector();
	    	if(database.getEngineName().equalsIgnoreCase("Oracle"))
	    		param = "2"; //+ Util.SepChar + Util.getOneSpace(userName);
	    	else
	    		param = "2"; //+ userName;
	    	DoDBAction.get(database,"User_Get",param,ret);	    	
	    	VWSLog.dbg("users pulled from db..." + ret);
	    	VWSLog.dbg("size of ret after getting users from db..." + ret.size());
	    	for (int cnt = 0; cnt < ret.size(); cnt++){
	    		StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
	    		int id = Util.to_Number(st.nextToken());
	    		String name = st.nextToken();
	    		usersFromDB.add(name.toLowerCase());
	    		String mailId = "";
	    		if(st.hasMoreTokens())
	    			mailId = st.nextToken();
	    		else
	    			mailId = "-";
	    		Creator user = new Creator(id, name, mailId,"");            
	    		userTable.put(name.toLowerCase(), user);
	    	}
	    	VWSLog.add("usersFromDB before comparing users :::: "+usersFromDB);
	    	VWSLog.add("users before comparing with usersFromDB :::: "+users);
	    	VWSLog.add("userscount for usersFromDB pulled from DB..." + usersFromDB.size());
	    	VWSLog.add("userscount for users pulled from directory..." + usersCount);
	    	
	    	for(int i=0;i<usersCount;i++)
	    	{
	    		try {
		    		userName = (String) users.get(i);
		    		VWSLog.add("userName :"+userName);
		    		userNameArr = userName.split("\t"); 
		    		VWSLog.add("userNameArr : "+userNameArr.length);
		    		userNameAlone = "";    		
		    		if (dirList.length>1) {
		    			VWSLog.add("dirList size : "+dirList[0]);
						VWSLog.add("directory1.getDirName().equalsIgnoreCase(dirList[0]) :: "+directory1.getDirName().equalsIgnoreCase(dirList[0]));
						if (directory1.getDirName().equalsIgnoreCase(dirList[0])) {
							if (userNameArr[0].indexOf("@")>0)
								userNameAlone = userNameArr[0].substring(0,userNameArr[0].indexOf("@"));
							else
								userNameAlone = userNameArr[0];
						} else {
							userNameAlone = userNameArr[0];
						}
		    		} else {
		    			userNameAlone = userNameArr[0];
		    		}
		    		VWSLog.add("userNameAlone.toLowerCase(): "+ userNameAlone.toLowerCase());
		    		
		    		if (!usersFromDB.contains(userNameAlone.toLowerCase())){
						VWSLog.add("user pull from directory not exists in db..." + userNameAlone);				
						Principal principal = new Principal(0,userNameAlone,Principal.USER_TYPE,directory1.getDirName().toString());
						principals.add(principal);				
		    		}
	    		} catch(Exception e) {
					VWSLog.add("Exception after identifying user that does not exist in DB: "+ e.getMessage());
				}
	    	}
    	}
		VWSLog.add("principals returned from getDirectoryPrincipalsNew ::::   "+principals);
    	/************************End of CV10.1  displaying users in add dialog******************/       
    	return principals;
    }
    
    public Vector getDirectoryPrincipals()
    {
    	VWSLog.dbg("Inside getDirectoryPrincipals from directorySync...");
    	Vector principals=new Vector();
    	Vector groups = new Vector();
    	if(isUserGroupXMLGenerated && isAdminWiseCall)
    		groups = getGroupsFromXML();
    	else
    		groups = directory.getGroups();
    	Vector ret = new Vector();
    	int groupId=0;
    	int userId = 0;
    	int count=0;
    	if(groups!=null)    count=groups.size();



    	ret = new Vector();
    	String param = "";
    	if(database.getEngineName().equalsIgnoreCase("Oracle"))
    		param = "2" + Util.SepChar + "~" + Util.SepChar;
    	else
    		param = "2" + Util.SepChar + "~";
    	DoDBAction.get(database,"Group_Get",param,ret);
    	Hashtable groupTable = new Hashtable(); 
    	for (int cnt = 0; cnt < ret.size(); cnt++){
    		StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
    		int id = Util.to_Number(st.nextToken());
    		String name = st.nextToken();
    		String mailId = "";
    		if(st.hasMoreTokens())
    			mailId = st.nextToken();
    		else
    			mailId = "-";
    		Creator group = new Creator(id, name, mailId,"");            
    		groupTable.put(name.toLowerCase(), group);
    	}
    	for(int i=0;i<count;i++)
    	{
    		String groupName = (String) groups.get(i);
    		if (!groupTable.containsKey(groupName.toLowerCase())){
    			Principal principal = new Principal(0,groupName,Principal.GROUP_TYPE);
    			principals.add(principal);
    		}
    	}
    	Vector users = new Vector();
    	if(isUserGroupXMLGenerated && isAdminWiseCall) {
    		users = getUsersFromXML();
    	}
    	else
    	{
    		VWSLog.dbg("Inside getDirectoryPrincipals and before getting users from directory (directory.getUsers())..." );
    		users = directory.getUsers(false);

    		String schemeType = VWSPreferences.getSSType();
    		/**
    		 * CV10.1 LDAP Issue fix.
    		 * 21/6/2017
    		 */
    		boolean syncotherouFlag = VWSPreferences.getSyncOtherOUUsers();
    		VWSLog.dbg("syncotherouFlag in getdirprincipals::::"+syncotherouFlag);
    		if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)&& syncotherouFlag) {
    			VWSLog.dbg("users list after pulling from directory:: " + users);
    			String groupName = "";
    			String otherOUUsers = "";
    			Vector ouUsers = new Vector();

    			for(int k=0;k<count;k++)
    			{
    				try{
    					groupName = (String) groups.get(k);
    					ouUsers.addAll(getUsersInGroup_New(groupName, 1));
    					VWSLog.dbg("Users pulled for group: " + groupName + " and users : " + ouUsers );
    					if(ouUsers.size()>0&&ouUsers!=null){
    						for(int i=0;i<ouUsers.size();i++){
    							otherOUUsers = ouUsers.get(i).toString();
    							otherOUUsers = otherOUUsers.substring(0,otherOUUsers.indexOf("@"));
    							VWSLog.dbg("otherOUUsers : " + otherOUUsers);
    							if(!(users.contains(otherOUUsers))){
    								VWSLog.dbg("user that does not exist in: " + otherOUUsers);
    								users.add(otherOUUsers);
    							}  
    						}
    					}
    				}catch(Exception ex){
    					VWSLog.dbg("Error while sync groups inside getdirectoryprincipals" + ex.getMessage() + " ::: " + groupName);
    				}
    			}
    		}
    	}
    	count=0;
    	if(users!=null) 
    		count=users.size();
    	VWSLog.dbg("Count for users pulled from directory..." + count);
    	ret = new Vector();
    	param = "";
    	if(database.getEngineName().equalsIgnoreCase("Oracle"))
    		param = "2"; //+ Util.SepChar + Util.getOneSpace(userName);
    	else
    		param = "2"; //+ userName;
    	DoDBAction.get(database,"User_Get",param,ret);
    	Hashtable userTable = new Hashtable(); 
    	VWSLog.dbg("users pulled from db..." + ret);
    	VWSLog.dbg("size of ret after getting users from db..." + ret.size());
    	for (int cnt = 0; cnt < ret.size(); cnt++){
    		StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
    		int id = Util.to_Number(st.nextToken());
    		String name = st.nextToken();
    		String mailId = "";
    		if(st.hasMoreTokens())
    			mailId = st.nextToken();
    		else
    			mailId = "-";
    		Creator user = new Creator(id, name, mailId,"");            
    		userTable.put(name.toLowerCase(), user);
    	}
    	VWSLog.dbg("Count for users pulled from directory..." + count);
    	for(int i=0;i<count;i++)
    	{
    		String userName = (String) users.get(i);
    		if (!userTable.containsKey(userName.toLowerCase())){
    			VWSLog.dbg("user pull from directory not exists in db..." + userName);
    			Principal principal = new Principal(0,userName,Principal.USER_TYPE);        	
    			principals.add(principal);
    		}
    	}
    	/**
    	 * CV110.1 Enhancement.
    	 * Commented on :- 9/6/2017
    	 */
    	/********CV10.1 pulling users to display in add dialog when sync users is true*****/
    	/*String schemeType = VWSPreferences.getSSType();
        if ((VWSPreferences.getSyncGroupUsers()==true) && (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)))
        {
        	List usersInGroup=new ArrayList();
        	for(int i=0;i<groups.size();i++)
        	{
        		String groupName = (String) groups.get(i);
        		usersInGroup=  getUsersInGroup( groupName);
        		for(int j=0;j<usersInGroup.size();j++){
        			String userInGroup=usersInGroup.get(j).toString();
        			if(!(users.contains(userInGroup.toLowerCase()))){
        				if (!userTable.containsKey(userInGroup.toLowerCase())){
        					Principal principal = new Principal(0,userInGroup,Principal.USER_TYPE);  
        					principals.add(principal);
        				}
        			}
        		}
        	}
        }*/
    	/************************End of CV10.1  displaying users in add dialog******************/       
    	return principals;
    }
    /**
     * CV10.1 Enhancement For ldap add dialog issue fix.
     * @param userName
     * @return
     */
    public Vector getLDAPSamdn(String userName)
    {
    	VWSLog.dbg("Inside getLDAPSamdn...Before updating sam information to users...");
    	Vector ret =new Vector();
    	try{
    	String param="";
    	//VWSLog.dbg("userName .....777....."+userName);
    	String domainName=userName.substring(userName.indexOf("@")+1,userName.length());
    	//VWSLog.dbg("domainName......."+domainName);
    	//VWSLog.dbg("directory.getDirName()...."+directory.getDirName());
    	if(directory.getDirName().equalsIgnoreCase(domainName)){
    		
    	}
    	String samName=directory.getSamAttribute(userName);
    	//VWSLog.dbg("samName .....777....."+samName);
    	try{
    	String SamName[]=samName.split(":");
    	if((samName!=null)&&(samName.length()>0)){
    	//VWSLog.dbg("samName .....99....."+SamName[1].trim());
    	param = "6" + Util.SepChar + Util.getOneSpace(userName) + Util.SepChar +SamName[1].trim()+Util.SepChar;
    	}
    	}catch(Exception e){
    		VWSLog.dbg("exception iside ldapsamdn....."+e.getMessage());
    	}
    	DoDBAction.get(database, "User_Get", param, ret);

    	String usersDN = directory.getUserBasicAttributes(userName);
    	String UsersDN[]=usersDN.split(":");
    	
    	param = "9" + Util.SepChar + Util.getOneSpace(userName) + Util.SepChar +UsersDN[1].trim()+Util.SepChar;
    	DoDBAction.get(database, "User_Get", param, ret);
    	}catch(Exception e){
    		VWSLog.dbg("Exception while getting ldap samdn......"+e.getMessage());
    	}
		return ret;
    }
    public List getUsersInGroup(String groupName)
    {
    	/*isAdminWiseCall condition added here because group to user sync should happen from adminwise*/
    	VWSLog.dbg("DS getUsersInGroup method groupName ::"+groupName);
    	//VWSLog.dbg("isAdminWiseCall::: "+isAdminWiseCall);
    	//VWSLog.dbg("isUserGroupXMLGenerated::: "+isUserGroupXMLGenerated);
    	if(isUserGroupXMLGenerated && isAdminWiseCall){
    		VWSLog.dbg("before getting users in group for cvgroup");
    		return getUsersInGroupsXML(groupName);
    	}
    	else
    		return directory.getUsersInGroup(groupName);
    }
    /**
     * CV10.2 - Method added to get usersingroup.
     * @param groupName
     * @return
     */
    public List getUsersInGroup_New(String groupName)
    {
    	/*isAdminWiseCall condition added here because group to user sync should happen from adminwise*/
    	VWSLog.dbg("DS getUsersInGroup method groupName ::"+groupName);
    	//VWSLog.dbg("isAdminWiseCall::: "+isAdminWiseCall);
    	//VWSLog.dbg("isUserGroupXMLGenerated::: "+isUserGroupXMLGenerated);
    	if(isUserGroupXMLGenerated && isAdminWiseCall){
    		VWSLog.dbg("before getting users in group for cvgroup");
    		return getUsersInGroupsXML(groupName);
    	}
    	else
    		return directory.getUsersInGroup_New(groupName);
    }
    /**
     * CV10.1 Enhancement method added to get usersingroup.
     * @param groupName
     * @param flag
     * @return
     */
    public List getUsersInGroup_New(String groupName, int flag)
    {
    	/*isAdminWiseCall condition added here because group to user sync should happen from adminwise*/
    	if(isUserGroupXMLGenerated && isAdminWiseCall)
    		return getUsersInGroupsXML(groupName);
    	else
    		return directory.getUsersInGroup(groupName);
    }
    
    public List getGroupsToUser(String userName)
    {
        List groups=getGroups();
        List groupsToUser=new LinkedList();
        for(int i=0;i<groups.size();i++)
        {
            String groupName=((Group)groups.get(i)).getName();
            List users = new LinkedList();
            if(isUserGroupXMLGenerated)
            	users=getUsersFromXML();
            else
            	users=directory.getUsersInGroup(groupName);
            if(users != null && users.contains(userName))
                                                    groupsToUser.add(groupName);
        }
        return groupsToUser;
    }
    //Method added to fix the Ldap user with upn suffix
    private void syncUserUPN() {
    	VWSLog.dbg("Inside syncUserUPN method... before update....");
    	Vector ret = new Vector();	
    	String param = "";
    	Vector usersUPN = new Vector();
    	usersUPN=directory.getUserWithUPN();
    	//VWSLog.dbg("usersUPN vector value:::::"+usersUPN);
    	Iterator UPNIterator = usersUPN.iterator();
    	//VWSLog.dbg("UPNIterator.hasnext()::::::"+UPNIterator.hasNext());
    	/*To do with User_Get procedure itself instead separate call.	Valli*/
    	while (UPNIterator.hasNext())
    	{
    		try{
    		String userName = (String) UPNIterator.next();
    		VWSLog.dbg("userName in syncUserUPN: " + userName);
    		/**
    		 * Added to sync the samAccountName for CV10.1 Enhancement
    		 */
    		String usersDN = directory.getUserBasicAttributes(userName);
			VWSLog.dbg("ldapuserDN value inside syncuserupn :::"+usersDN);
			if((usersDN.length()>0)&&(usersDN.contains(":"))){
				String UsersDN[]=usersDN.split(":");
				param = "9" + Util.SepChar + Util.getOneSpace(userName) + Util.SepChar +UsersDN[1].trim()+Util.SepChar+directory.getDirName()+Util.SepChar;
				DoDBAction.get(database, "User_Get", param, ret);
			}
			
    		String usersMailIds = directory.getUserAttributes(userName,"mail");
    		//VWSLog.dbg("usersMailIds inside syncUserUPN: " + usersMailIds);
    		String eMailIds[]=usersMailIds.split(":");
    		String samName=directory.getSamAttribute(userName);
    		VWSLog.dbg("samName inside syncuserupn :::"+samName);
    		if((samName.length()>0)&&(samName.contains(":"))){
    			String SamName[]=samName.split(":");
    			
    			/*String usersDN = directory.getUserBasicAttributes(userName);
        		if((usersDN.length()>0)&&(usersDN.contains(":"))) {
        			String UsersDN[]=usersDN.split(":");
        		
        		String usersDN1 = directory.getUserDNMailAttributes(userName);
        		if((usersDN1.length()>0)&&(usersDN1.contains(":"))) {
        			String UsersDN1[]=usersDN1.split(":");*/
        		
    			param = "6" + Util.SepChar + Util.getOneSpace(userName)+Util.SepChar+SamName[1].trim()+Util.SepChar+"-"+Util.SepChar+eMailIds[1].trim()+Util.SepChar+directory.getDirName()+Util.SepChar;
    			DoDBAction.get(database, "User_Get", param, ret);
    		}
    		
    		}catch(Exception e){
    			VWSLog.dbg("Inside catch block of while:::"+e.getMessage());
    		}
			

    	}
    }
    
    /**
     * CV10.2 - Added for Method to sync failed user UPN
     */
    private void syncFailedUserUPN() {
    	VWSLog.dbg("Inside syncUserUPN method... before update....");
    	Vector ret = new Vector();	
    	String param = "";
    	
    	Vector usersUPN = new Vector();
    	
    	String[] dirList = null;
    	String ldapDirectory = VWSPreferences.getTree();
		dirList = ldapDirectory.split(",");
		//boolean LDAPsingleToMultiple = VWSPreferences.getLDAPUpgradeToMultiDomain();
		
    	//usersUPN=directory.getUserWithUPN();
    	//VWSLog.dbg("usersUPN vector value:::::"+usersUPN);
    	Iterator UPNIterator = usersWithoutUserInfo.iterator();
    	//VWSLog.dbg("UPNIterator.hasnext()::::::"+UPNIterator.hasNext());
    	//To do with User_Get procedure itself instead separate call.	Valli
    	while (UPNIterator.hasNext())
    	{
    		try{
    		String userName = (String) UPNIterator.next();
    		VWSLog.add("userName in syncUserUPN: " + userName);
    		/**
    		 * Added to sync the samAccountName for CV10.1 Enhancement
    		 */
    		String usersDN = directory.getUserBasicAttributes(userName);
			VWSLog.dbg("ldapuserDN value inside syncuserupn :::"+usersDN);
			if((usersDN.length()>0)&&(usersDN.contains(":"))){
				String UsersDN[]=usersDN.split(":");
				
				String samName=directory.getSamAttribute(userName);
	    		VWSLog.dbg("samName inside syncuserupn :::"+samName);
	    		if((samName.length()>0)&&(samName.contains(":"))){
	    			String SamName[]=samName.split(":");
	    			
	    			
	    			String prinName=directory.getuserPrincipalName(userName);
		    		VWSLog.dbg("Principal Name inside syncuserupn :::"+prinName);
		    		if((prinName.length()>0)&&(prinName.contains(":"))){
		    			String pName[]=prinName.split(":");
		    			String upnSuffix = pName[1].substring(pName[1].indexOf("@")+1, pName[1].length());
		    			
	    			
	    			/*String usersMailIds = directory.getUserAttributes(userName,"mail");
	    			if((usersMailIds.length()>0)&&(usersMailIds.contains(":"))){
	    				String eMailIds[]=usersMailIds.split(":");
	        		
	        		*/
	        			
		    	/*if ((dirList[0].equalsIgnoreCase(upnSuffix)) && (LDAPsingleToMultiple) && (dirList.length > 1))
		    		param = "102" + Util.SepChar +  Util.getOneSpace(userName) +  Util.SepChar + upnSuffix.trim() +  Util.SepChar + UsersDN[1].trim()+Util.SepChar+SamName[1].trim()+Util.SepChar+" "+Util.SepChar+directory.getDirName()+Util.SepChar;
		    	else
		    		param = "102" + Util.SepChar +  pName[1].trim() +  Util.SepChar + upnSuffix.trim() +  Util.SepChar + UsersDN[1].trim()+Util.SepChar+SamName[1].trim()+Util.SepChar+" "+Util.SepChar+directory.getDirName()+Util.SepChar;*/
		    	
				if (dirList.length > 1) {
    				//if (LDAPsingleToMultiple) {
						if (dirList[0].equalsIgnoreCase(upnSuffix))
							param = "102" + Util.SepChar +  Util.getOneSpace(userName) +  Util.SepChar + upnSuffix.trim() +  Util.SepChar + UsersDN[1].trim()+Util.SepChar+SamName[1].trim()+Util.SepChar+" "+Util.SepChar+directory.getDirName()+Util.SepChar;
						else
							param = "101" + Util.SepChar +  pName[1].trim() + Util.SepChar + pName[1].trim() +  Util.SepChar +UsersDN[1].trim()+Util.SepChar+SamName[1].trim()+Util.SepChar+""+Util.SepChar;
					/*} else {
						param = "101" + Util.SepChar +  pName[1].trim() + Util.SepChar + pName[1].trim() +  Util.SepChar +UsersDN[1].trim()+Util.SepChar+SamName[1].trim()+Util.SepChar+""+Util.SepChar;
					}*/
    			}
    			else
    				param = "102" + Util.SepChar +  Util.getOneSpace(userName) +  Util.SepChar + upnSuffix.trim() +  Util.SepChar + UsersDN[1].trim()+Util.SepChar+SamName[1].trim()+Util.SepChar+" "+Util.SepChar+directory.getDirName()+Util.SepChar;
    				//param = "1" + Util.SepChar + Util.getOneSpace(userName) + Util.SepChar +UsersDN[1].trim()+Util.SepChar+SamName[1].trim()+Util.SepChar+"-"+Util.SepChar+directory.getDirName()+Util.SepChar;

				/*if ((LDAPsingleToMultiple) && (RampageServer.currentDomainName.equalsIgnoreCase(dirList[0])))
				param = "1" + Util.SepChar + Util.getOneSpace(userName);
				else
				param = "101" + Util.SepChar + Util.getOneSpace(userName);*/
				
				//param = "9" + Util.SepChar + Util.getOneSpace(userName) + Util.SepChar +UsersDN[1].trim()+Util.SepChar+directory.getDirName()+Util.SepChar;
				DoDBAction.get(database, "User_Get", param, ret);
			}
	    	//}
			}
			}
			
    		/*String usersMailIds = directory.getUserAttributes(userName,"mail");
    		//VWSLog.dbg("usersMailIds inside syncUserUPN: " + usersMailIds);
    		String eMailIds[]=usersMailIds.split(":");
    		String samName=directory.getSamAttribute(userName);
    		VWSLog.dbg("samName inside syncuserupn :::"+samName);
    		if((samName.length()>0)&&(samName.contains(":"))){
    			String SamName[]=samName.split(":");
    			
    			String usersDN = directory.getUserBasicAttributes(userName);
        		if((usersDN.length()>0)&&(usersDN.contains(":"))) {
        			String UsersDN[]=usersDN.split(":");
        		
        		String usersDN1 = directory.getUserDNMailAttributes(userName);
        		if((usersDN1.length()>0)&&(usersDN1.contains(":"))) {
        			String UsersDN1[]=usersDN1.split(":");
        		
    			param = "6" + Util.SepChar + Util.getOneSpace(userName)+Util.SepChar+SamName[1].trim()+Util.SepChar+"-"+Util.SepChar+eMailIds[1].trim()+Util.SepChar+directory.getDirName()+Util.SepChar;
    			DoDBAction.get(database, "User_Get", param, ret);
    		}*/
    		
    		}catch(Exception e){
    			VWSLog.dbg("Inside catch block of while:::"+e.getMessage());
    		}
			

    	}
    }
    
	/**CV2019 merges from CV10.2***/
    private void syncOtherOUUsers() 
    {
	    List allOtherOUUsers = null;
	    //allOtherOUUsers = getUsersInGroup_New(getNamedGroup(), 1);
	    VWSLog.dbg("Named Group Other OU users are "+ allOtherOUUsers);

    }
    
    private void syncUsers() 
    {
	try{
	    Vector users = new Vector();
	    List groupUsers = new Vector();
	    List adminList = new Vector();//Added for License changes Madhavan.B
	    List namedList = new Vector();//Added for License changes Madhavan.B
	    List namedOnlineList=new Vector();
	    List profesionalNamedList=new Vector();
	    List concurrentList=new Vector();
	    List concurrentOnlineList=new Vector();
	    List profConcrList=new Vector();	    
	    List publicWAList=new Vector();
	    
	    List roomLevelNamedList = new Vector();//Added for License changes Madhavan.B
	    List roomLevelNamedOnlineList=new Vector();
	    List roomLevelProfessionalNamedList=new Vector();
	    List roomLevelconcurrentList=new Vector();
	    List roomLeveleconcurrentOnlineList=new Vector();
	    List roomLevelprofConcrList=new Vector();
	    List roomLevelpublicList=new Vector();
		/**CV2019 code optimization after CV10.2 merge**/
	    List dirUsers = new Vector();//Added by Srikanth
	    
	    String cvNamedGroup_Reg = VWSPreferences.getRoomLevelNamed(room);
	    String cvNamedOnlineGroup_Reg = VWSPreferences.getRoomLevelNamedOnline(room);
	    String cvProfNamedGroup_Reg = VWSPreferences.getRoomLevelProfessionalNamed(room);
	    
	    String cvNamedGroup_SS = getNamedGroup();
	    String cvNamedOnlineGroup_SS = getNamedOnlineGroup();
	    String cvProfNamedGroup_SS = getProfessionalNamedGroup();	    

	    Vector ret = new Vector();
	    Vector emailUserList = new Vector();
	    String param = "";
	    List dbUsers = new Vector();
	    Vector initalUserList=new Vector();
	    String userName = null;
		Creator creator = null;
	    /**
	     * CV10.1 LDAp multi domain group/user sync issue
	     * 15-6-2017
	     */
		VWSLog.add("Inside syncUsers...");

	    //Fetching domains information that was configured in Server Settings
	    String[] dirList = null;
	    String schemeType = VWSPreferences.getSSType();
	    
	    if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
	    	VWSLog.dbg("Fetch users along with DN, UPNSuffix, SamName information from DB for LDAP.");
		    VWSLog.dbg("Checking RampageServer.currentDomainName.isEmpty()::::" + RampageServer.currentDomainName);
		    String ldapDirectory = VWSPreferences.getTree();
			VWSLog.dbg("ldapDirectory ::::::::"+ldapDirectory);
			dirList = ldapDirectory.split(",");
			//Deleting of named group should happen only one time when multiple domains are configured. so set deleteNamedGroupUsers flag accordingly.
		    if ((dirList.length>1) && ((RampageServer.currentDomainName.isEmpty()) && (RampageServer.currentDomainName.length()==0)) )  {
		    	VWSLog.dbg("before setting flag in if condition ");
				RampageServer.deleteNamedGroupUsers = true;
			} else  {
				VWSLog.dbg("before setting flag in else condition ");
				RampageServer.deleteNamedGroupUsers = false;
			}
		    if ((RampageServer.currentDomainName.isEmpty()) && (RampageServer.currentDomainName.length()==0)) {
		    	VWSLog.dbg("Before setting directory.getDirName to currentDomainName:::" +directory.getDirName() );
		    	RampageServer.currentDomainName = directory.getDirName();
		    }
		    
	    	 VWSLog.dbg("if condition of current domain equals dirName");
	    	 dbUsers = getUsers_New(directory.getDirName());
	    	 VWSLog.add("dbUsers size after getuser new ...."+dbUsers);
		   
	    /* boolean domainUsersExist = getUsersForDomain(directory.getDirName());
		    VWSLog.dbg("domain information :::::"+ directory.getDirName());
		    VWSLog.dbg("dbUsers inside syncUsers method::::::::: "+dbUsers);
		    VWSLog.dbg("domainUsersExist inside syncUsers method:::::: "+domainUsersExist);*/
	    }
		else {
			VWSLog.dbg("Fetch users from DB for ADS.");
			RampageServer.deleteNamedGroupUsers = false;
			dbUsers = getUsers();	
			VWSLog.dbg("ADS dbUsers inside syncUsers method::::::::: "+dbUsers);
		}
	    
	    	   
	    boolean isHosting = false;
	    try{
	    	isHosting = CVPreferences.getHosting();
	    }catch (Exception e) {
	    	isHosting = false;
	    }
	    VWSLog.add("Before deleting Named, Professional group users");	 
	    VWSLog.add("RampageServer.deleteNamedGroupUsers: "+ RampageServer.deleteNamedGroupUsers);
	    if (!schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS))  {  //When server is AD Server
	    	VWSLog.add("Before deleting Named, professional named, concurrent, professional concurrent users when schema is ADS...");
	    	 
	    	if (isHosting) {
		    	if (VWSPreferences.getRoomLevelNamed(room).length()>0) {
		    	deleteNamedUsers(VWSPreferences.getRoomLevelNamed(room));
		    	}
		    	if (VWSPreferences.getRoomLevelNamedOnline(room).length()>0) {
			    deleteNamedUsers(VWSPreferences.getRoomLevelNamedOnline(room));
		    	}
		    	//Commented date Date :-3/8/2017 BY :- madhavan
		    	if (VWSPreferences.getRoomLevelConcurrent(room).length()>0) {
			    deleteNamedUsers(VWSPreferences.getRoomLevelConcurrent(room));
		    	}
		    	if (VWSPreferences.getRoomLevelConcurrentOnline(room).length()>0) {
		    		deleteNamedUsers(VWSPreferences.getRoomLevelConcurrentOnline(room));
		    	}
		    	if (VWSPreferences.getRoomsProfessionalNamed(room).length()>0) {
		    		deleteNamedUsers(VWSPreferences.getRoomsProfessionalNamed(room));
		    	}
		    	if (VWSPreferences.getRoomLevelProfConcurrent(room).length()>0) {
			    deleteNamedUsers(VWSPreferences.getRoomLevelProfConcurrent(room));
		    	}
		    	if (VWSPreferences.getRoomLevelPublicWA(room).length()>0) {
		    		deleteNamedUsers(VWSPreferences.getRoomLevelPublicWA(room));
		    	}
			 
		    }
		    else {
		    	if(getAdminGroup().length()>0){
		    		deleteNamedUsers(getAdminGroup());
		    	}
		    	if (getNamedGroup().length()>0) {
		    	deleteNamedUsers(getNamedGroup());
		    	}
		    	if (getNamedOnlineGroup().length()>0) {
		    		deleteNamedUsers(getNamedOnlineGroup());
		    	}		    	
		    	//Commented date Date :-3/8/2017 BY :- madhavan
		    	if (getConcurrentGroup().length()>0) {
			    deleteNamedUsers(getConcurrentGroup());
		    	}
		    	if (getConcurrentOnlineGroup().length()>0) {
				    deleteNamedUsers(getConcurrentOnlineGroup());
			    	}
		    	if (getProfessionalNamedGroup().length()>0) {
		    		deleteNamedUsers(getProfessionalNamedGroup());
		    	}
		    	if (getProfessionalConcrGroup().length()>0) {
		    		deleteNamedUsers(getProfessionalConcrGroup());
		    	}
		    	if (getPublicWAGroup().length()>0) {
		    		deleteNamedUsers(getPublicWAGroup());
		    	}
		    	
		    }
	    }
	    else if ((dirList.length == 1) && (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)))  {  //When Server is LDAP Server and for single domain
	    	/**
	 	    * CV10.1 Multi domain issue fix
	 	    * 15-6-2017
	 	    */
	    	if (isHosting) {
	    		if (VWSPreferences.getRoomLevelNamed(room).length()>0) {
			    	deleteNamedUsers(VWSPreferences.getRoomLevelNamed(room));
			    	}
			    	if (VWSPreferences.getRoomLevelNamedOnline(room).length()>0) {
				    deleteNamedUsers(VWSPreferences.getRoomLevelNamedOnline(room));
			    	}
			    	//Commented date Date :-3/8/2017 BY :- madhavan
			    	if (VWSPreferences.getRoomLevelConcurrent(room).length()>0) {
				    deleteNamedUsers(VWSPreferences.getRoomLevelConcurrent(room));
			    	}
			    	if (VWSPreferences.getRoomLevelConcurrentOnline(room).length()>0) {
			    		deleteNamedUsers(VWSPreferences.getRoomLevelConcurrentOnline(room));
			    	}
			    	if (VWSPreferences.getRoomsProfessionalNamed(room).length()>0) {
			    		deleteNamedUsers(VWSPreferences.getRoomsProfessionalNamed(room));
			    	}
			    	if (VWSPreferences.getRoomLevelProfConcurrent(room).length()>0) {
				    deleteNamedUsers(VWSPreferences.getRoomLevelProfConcurrent(room));
			    	}
			    	if (VWSPreferences.getRoomLevelPublicWA(room).length()>0) {
			    		deleteNamedUsers(VWSPreferences.getRoomLevelPublicWA(room));
			    	}
			 
		    }
		    else {
		    	if(getAdminGroup().length()>0){
		    		deleteNamedUsers(getAdminGroup());
		    	}
		    	if (getNamedGroup().length()>0) {
		    	deleteNamedUsers(getNamedGroup());
		    	}
		    	if (getNamedOnlineGroup().length()>0) {
		    		deleteNamedUsers(getNamedOnlineGroup());
		    	}		    	
		    	//Commented date Date :-3/8/2017 BY :- madhavan
		    	if (getConcurrentGroup().length()>0) {
			    deleteNamedUsers(getConcurrentGroup());
		    	}
		    	if (getConcurrentOnlineGroup().length()>0) {
				    deleteNamedUsers(getConcurrentOnlineGroup());
			    	}
		    	if (getProfessionalNamedGroup().length()>0) {
		    		deleteNamedUsers(getProfessionalNamedGroup());
		    	}
		    	if (getProfessionalConcrGroup().length()>0) {
		    		deleteNamedUsers(getProfessionalConcrGroup());
		    	}
		    	if (getPublicWAGroup().length()>0) {
		    		deleteNamedUsers(getPublicWAGroup());
		    	}
		    }
	    }
	    else if ((RampageServer.deleteNamedGroupUsers == true)&&(dirList.length > 1))   { //for LDAP Multiple Domains
		    if (isHosting) {
		    	if (VWSPreferences.getRoomLevelNamed(room).length()>0) {
			    	deleteNamedUsers(VWSPreferences.getRoomLevelNamed(room));
			    	}
			    	if (VWSPreferences.getRoomLevelNamedOnline(room).length()>0) {
				    deleteNamedUsers(VWSPreferences.getRoomLevelNamedOnline(room));
			    	}
			    	//Commented date Date :-3/8/2017 BY :- madhavan
			    	if (VWSPreferences.getRoomLevelConcurrent(room).length()>0) {
				    deleteNamedUsers(VWSPreferences.getRoomLevelConcurrent(room));
			    	}
			    	if (VWSPreferences.getRoomLevelConcurrentOnline(room).length()>0) {
			    		deleteNamedUsers(VWSPreferences.getRoomLevelConcurrentOnline(room));
			    	}
			    	if (VWSPreferences.getRoomsProfessionalNamed(room).length()>0) {
			    		deleteNamedUsers(VWSPreferences.getRoomsProfessionalNamed(room));
			    	}
			    	if (VWSPreferences.getRoomLevelProfConcurrent(room).length()>0) {
				    deleteNamedUsers(VWSPreferences.getRoomLevelProfConcurrent(room));
			    	}
			    	if (VWSPreferences.getRoomLevelPublicWA(room).length()>0) {
			    		deleteNamedUsers(VWSPreferences.getRoomLevelPublicWA(room));
			    	}
			 
		    }
		    else {
		    	if(getAdminGroup().length()>0){
		    		deleteNamedUsers(getAdminGroup());
		    	}
		    	if (getNamedGroup().length()>0) {
		    	deleteNamedUsers(getNamedGroup());
		    	}
		    	if (getNamedOnlineGroup().length()>0) {
		    		deleteNamedUsers(getNamedOnlineGroup());
		    	}		    	
		    	//Commented date Date :-3/8/2017 BY :- madhavan
		    	if (getConcurrentGroup().length()>0) {
			    deleteNamedUsers(getConcurrentGroup());
		    	}
		    	if (getConcurrentOnlineGroup().length()>0) {
				    deleteNamedUsers(getConcurrentOnlineGroup());
			    	}
		    	if (getProfessionalNamedGroup().length()>0) {
		    		deleteNamedUsers(getProfessionalNamedGroup());
		    	}
		    	if (getProfessionalConcrGroup().length()>0) {
		    		deleteNamedUsers(getProfessionalConcrGroup());
		    	}
		    	if (getPublicWAGroup().length()>0) {
		    		deleteNamedUsers(getPublicWAGroup());
		    	}
		    }
	    
	    
	    /**
	    * End of Multi domain issue fix
	    * 15-6-2017
	    */
	    }
		

		//Added for License changes Madhavan.B
	    
	    VWSLog.dbg("gIgnoreSync flag status before checking dbUsers != null && dbUsers.size() > 0:::" + gIgnoreSync);
	    if(dbUsers != null && dbUsers.size() > 0)
	    {
	    	VWSLog.dbg("Inside dbUsers exists checking satisfied for domain::::" + directory.getDirName());
	    	gIgnoreSync = true;
	    	VWSLog.dbg("Inside dbusers is not null");
	    	
	    	/*if(schemeType==Constants.SCHEME_LDAP_ADS){
	    		String ldapDirectory = VWSPreferences.getTree();
	    		VWSLog.add("ldapDirectory ::::::::"+ldapDirectory);
	    		String[] dirList = ldapDirectory.split(",");
	    		for(int s=0; s< dirList.length; s++){
	    			if (dirList[s].equalsIgnoreCase(directory.getDirName())) {
	    				VWSLog.add("Inside dirList[s].equalsIgnoreCase(directory.getDirName()) true" + directory.getDirName());
	    				if (s == (dirList.length-1)) //&& (directory.getDirName().equalsIgnoreCase(dirList[s])) ) {
	    					{
	    					VWSLog.add("Inside checking and gIgnoreSync is true");
	    					if (domainUsersExist==true) {
	    						VWSLog.add("Inside domainuserExist true");
	    						gIgnoreSync = true;
	    					} else {
	    						VWSLog.add("Inside domainuserExist false");
	    						gIgnoreSync = false;
	    					}
	    	    		}else {
	    	    			VWSLog.add("Inside checking and gIgnoreSync is false");
	    	    			gIgnoreSync = true;
	    	    		}
	    				}
	    			}
	    		}
	    	if(schemeType==Constants.SSCHEME_ADS) {
	    		gIgnoreSync = true;
	    	} */
	    	adminList=getUsersInGroup(getAdminGroup());
	    		if(getAdminGrpCount()!=-1){
	    			 if (getDirAdminGrpCount() > getAdminGrpCount()){
	    				 for(int i=0;i<getAdminGrpCount();i++){
	    					 groupUsers.add(adminList.get(i));
	    				 }
	    				 String message = ResourceManager.getDefaultManager().getString("SyncMsg.AdminGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.AdminGroup"));
	    			 }else{
	    				 groupUsers.addAll(getUsersInGroup(getAdminGroup()));
	    			 }
	    			
	    			}
		    
		    VWSLog.dbg("groupUsers :::::::::::"+groupUsers);
		  //Added for License changes Madhavan.B
		   // groupUsers.addAll(getUsersInGroup(getNamedGroup()));
		     namedList = getUsersInGroup(getNamedGroup());
		     namedOnlineList=getUsersInGroup(getNamedOnlineGroup());
		     profesionalNamedList=getUsersInGroup(getProfessionalNamedGroup());
		     concurrentList=getUsersInGroup(getConcurrentGroup());
		     concurrentOnlineList=getUsersInGroup(getConcurrentOnlineGroup());
		     profConcrList=getUsersInGroup(getProfessionalConcrGroup());
		     publicWAList=getUsersInGroup(getPublicWAGroup());
		     
		     if (isHosting ) {
			     roomLevelNamedList= getUsersInGroup(VWSPreferences.getRoomLevelNamed(room));
			     roomLevelNamedOnlineList=getUsersInGroup(VWSPreferences.getRoomLevelNamedOnline(room));
			     roomLevelProfessionalNamedList= getUsersInGroup(VWSPreferences.getRoomLevelProfessionalNamed(room));
			     roomLevelconcurrentList=getUsersInGroup(VWSPreferences.getRoomLevelConcurrent(room));
			     roomLeveleconcurrentOnlineList=getUsersInGroup(VWSPreferences.getRoomLevelConcurrentOnline(room));
			     roomLevelprofConcrList=getUsersInGroup(VWSPreferences.getRoomLevelProfConcurrent(room));
			     roomLevelpublicList=getUsersInGroup(VWSPreferences.getRoomLevelPublicWA(room));
		     }
		     
		     //getNamedGrpCount will get named group count that is configured in license file. (.lic file)
		     //getDirNamedGrpCount() will get named group count from directory (ADS / LDAP_ADS)
		     if (getNamedGrpCount() != -1) {
		    	 /*int namedUsersCountInDB = getGroupUsers(getNamedGroup());
		    	 int loopCount = 0;
		    	 if (dirList.length>1) {
		    		 VWSLog.add("multiple domain:::::");
		    		 if (namedUsersCountInDB==0) {
		    			 loopCount = getNamedGrpCount();
		    		 } else {
		    			 loopCount = (getNamedGrpCount() - namedUsersCountInDB);
		    			 if (loopCount <= 0) loopCount = 1;
		    		 }
		    	 } else if ((dirList.length==1) || (schemeType.equalsIgnoreCase(Constants.SSCHEME_ADS))) {
		    		 VWSLog.add("Single domain:::::");
		    		 loopCount = getNamedGrpCount(); 
		    	 }*/
		    	 VWSLog.dbg("getDirNamedGrpCount() from directory for this group::" + getDirNamedGrpCount());
		    	 VWSLog.dbg("getNamedGrpCount() from license file for this group::" + getNamedGrpCount());
		    	 //VWSLog.add("loopCount value::" +loopCount);
		    	 
		    	 if (!isHosting) {
			    	 if (!isHosting&&getDirNamedGrpCount() > getNamedGrpCount()){
			    		 for(int i=0;i<getNamedGrpCount();i++){
			    		 //for(int i=0;i<loopCount;i++){
			    			 groupUsers.add(namedList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
			    		
			    	 }
			    	 else {
			    		 groupUsers.addAll(getUsersInGroup(getNamedGroup()));
	
			    	 }
		    	 }
		    	  VWSLog.dbg("groupUsers of named:::::::::::"+groupUsers);
		    	 //Synchronizing room level (Hosting environment) named group
		    	 if (isHosting) {
			    	 if (isHosting&&getDirRoomLevelNamedGrpCount() > getNamedGrpCount()){
			    		 for(int i=0;i<getNamedGrpCount();i++){
			    		 //for(int i=0;i<loopCount;i++){
			    			 groupUsers.add(roomLevelNamedList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
			    	 }
			    	 else { 
			    		 groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelNamed(room)));
			    	 }
		    	 }
		     }//end
		     //---------------CV2019 License changes ----------------------------------------------------//
		     
		     if (getNamedOnlineGrpCount() != -1) {
		    	
		    	 VWSLog.dbg("getDirNamedOnlineGrpCount from directory for this group::" + getDirNamedOnlineGrpCount());
		    	 VWSLog.dbg("getNamedOnlineGrpCount from license file for this group::" + getNamedOnlineGrpCount());
		    	
		    	 
		    	 if (!isHosting) {
			    	 if (!isHosting&&getDirNamedOnlineGrpCount() > getNamedOnlineGrpCount()){
			    		 for(int i=0;i<getNamedOnlineGrpCount();i++){
			    			 groupUsers.add(namedOnlineList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedOnlineGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedOnlineGroup"));
			    		
			    	 }
			    	 else {
			    		 groupUsers.addAll(getUsersInGroup(getNamedOnlineGroup()));
	
			    	 }
		    	 }
		    	  VWSLog.dbg("groupUsers of named:::::::::::"+groupUsers);
		    	 //Synchronizing room level (Hosting environment) named group
		    	 if (isHosting) {
			    	 if (isHosting&&getDirRoomLevelNamedOnlineGrpCount() > getNamedOnlineGrpCount()){
			    		 for(int i=0;i<getNamedOnlineGrpCount();i++){
			    			 groupUsers.add(roomLevelNamedOnlineList.get(i));
			    			 
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
			    	 }
			    	 else { 
			    		 groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelNamed(room)));
			    	 }
		    	 }
		     }//end
		     
		     
		   //---------------CV2019 License changes ----------------------------------------------------//
		     
		     	//Commented date Date :-5/8/2017 BY :- madhavan
		    /* if (getConcurrentGrpCount() != -1) {
		    	 if (!isHosting) {
		    		 //Commented date Date :-3/8/2017 BY :- madhavan
		    		 if (!isHosting&&getDirConcurrentGrpCount() > getConcurrentGrpCount()){
		    			 for(int i=0;i<getConcurrentGrpCount();i++){
		    				 groupUsers.add(concurrentList.get(i));
		    			 }
		    			 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup");
		    			 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    			 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup"));

		    		 }
		    		 else {
		    			 groupUsers.addAll(getUsersInGroup(getConcurrentGroup()));
		    		 }
		    	 }
		    	 //Synchronizing room level (Hosting environment) concurrent group
		    	 if (isHosting) {
		    		 //Commented date Date :-3/8/2017 BY :- madhavan
		    		 if (isHosting&&getDirRoomLevelConcurrentGrpCount() > getConcurrentGrpCount()){
		    			 for(int i=0;i<getConcurrentGrpCount();i++){
		    				 groupUsers.add(roomLevelconcurrentList.get(i));
		    			 }
		    			 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup");
		    			 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    			 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup"));
		    		 }
		    		 else { 
		    			 groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelConcurrent(room)));
		    			 //}
		    		 }
		    	 }//---------------CV10.1 License end----------------------------------------------------//

		     }*/
		     
		    /* if (getConcurrentOnlineGrpCount() != -1) {
		    	 if (!isHosting) {
		    		 //Commented date Date :-3/8/2017 BY :- madhavan
		    		 if (!isHosting&&getDirConcurrentOnlineGrpCount() > getConcurrentOnlineGrpCount()){
		    			 for(int i=0;i<getConcurrentOnlineGrpCount();i++){
		    				 groupUsers.add(concurrentOnlineList.get(i));
		    			 }
		    			 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentOnlineGroup");
		    			 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    			 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentOnlineGroup"));

		    		 }
		    		 else {
		    			 groupUsers.addAll(getUsersInGroup(getConcurrentOnlineGroup()));
		    		 }
		    	 }
		    	 //Synchronizing room level (Hosting environment) concurrent group
		    	 if (isHosting) {
		    		 //Commented date Date :-3/8/2017 BY :- madhavan
		    		 if (isHosting&&getDirRoomLevelConcurrentOnlineGrpCount() > getConcurrentOnlineGrpCount()){
		    			 for(int i=0;i<getConcurrentOnlineGrpCount();i++){
		    				 groupUsers.add(roomLevelconcurrentList.get(i));
		    			 }
		    			 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup");
		    			 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    			 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup"));
		    		 }
		    		 else { 
		    			 groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelConcurrentOnline(room)));
		    			 //}
		    		 }
		    	 }//---------------CV10.1 License end----------------------------------------------------//

		     }*/
		     
		     if (getProfessionalNamedGrpCount() != -1) {
		    	 if (!isHosting) {
			    	 if(!isHosting&&getDirProfessionalNamedGrpCount() > getProfessionalNamedGrpCount()){
			    		 for(int i=0;i<getProfessionalNamedGrpCount();i++){
			    			 groupUsers.add(profesionalNamedList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
			    	 }
			    	 else 
			    		 groupUsers.addAll(getUsersInGroup(getProfessionalNamedGroup()));
		    	 }
		    	 
		    	 //Synchronizing room level (Hosting environment) professional named group
		    	 if (isHosting ) {
			    	 if(isHosting&&getDirRoomLevelProfessionalNamedGrpCount() > getProfessionalNamedGrpCount()){
			    		 for(int i=0;i<getProfessionalNamedGrpCount();i++){
			    			 groupUsers.add(roomLevelProfessionalNamedList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
			    	 }
			    	 else 
			    		 groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelProfessionalNamed(room)));
		    	 }
		     }
		   //---------------CV10.1 License ----------------------------------------------------//
		   /*  if (getProfessConcrGrpCount() != -1) {
		    	 if (!isHosting) {
					//Commented date Date :-3/8/2017 BY :- madhavan
			    	 if(!isHosting&&getDirProfConcrGrpCount() > getProfessConcrGrpCount()){
			    		 for(int i=0;i<getProfessConcrGrpCount();i++){
			    			 groupUsers.add(profConcrList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalConcurrentGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalConcurrentGroup"));
			    	 }
			    	 else {
			    		 groupUsers.addAll(getUsersInGroup(getProfessionalConcrGroup()));
			    	 //}
		    	 }
		    	 
		    	 //Synchronizing room level (Hosting environment) professional concurrent group
		    	 if (isHosting ) {
					//Commented date Date :-3/8/2017 BY :- madhavan
			    	 if(isHosting&&getDirRoomLevelProfConcrGrpCount() > getProfessConcrGrpCount()){
			    		 for(int i=0;i<getProfessConcrGrpCount();i++){
			    			 groupUsers.add(roomLevelprofConcrList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalConcurrentGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalConcurrentGroup"));
			    	 }
			    	 else 
			    		 groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelProfConcurrent(room)));
		    	 }
		    	 }
		     }*/
		    /* if (getPublicWAGrpCount() != -1) {
		    	 if (!isHosting) {
		    		 VWSLog.dbg("getDirPublicWAGrpCount:::"+getDirPublicWAGrpCount());
		    		 VWSLog.dbg("getPublicWAGrpCount:::"+getPublicWAGrpCount());
			    	 if(!isHosting&&getDirPublicWAGrpCount() > getPublicWAGrpCount()){
			    		 for(int i=0;i<getPublicWAGrpCount();i++){
			    			 groupUsers.add(publicWAList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.publicWAGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.publicWAGroup"));
			    	 }
			    	 else {
			    		 groupUsers.addAll(getUsersInGroup(getPublicWAGroup()));
			    	 }
		    	 
		    	 //Synchronizing room level (Hosting environment) professional concurrent group
		    	 if (isHosting ) {
					//Commented date Date :-3/8/2017 BY :- madhavan
			    	 if(isHosting&&getDirRoomLevelpublicWAGrpCount() > getPublicWAGrpCount()){
			    		 for(int i=0;i<getPublicWAGrpCount();i++){
			    			 groupUsers.add(roomLevelpublicList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.publicWAGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.publicWAGroup"));
			    	 }
			    	 else 
			    		 groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelPublicWA(room)));
		    	 }
		    	 }
		     }*/
		   //---------------CV10.1 License end----------------------------------------------------//
		     
		    //
		    if (groupUsers == null || groupUsers.size() == 0) {
		    	VWSLog.dbg("group Users are not available inside syncUsers dbusers not null condition 1 so return from syncUsers method with gIgnoreSync value::: " + gIgnoreSync);
		    	return;
		    }else {
		    	VWSLog.dbg("groupusers count:::" + groupUsers.size() + " and gIgnoreSync flag value::: " + gIgnoreSync);
		    }
		    
		    VWSLog.dbg("List of Users from Security Server inside syncUsers method:::::: " + groupUsers);
		    for (int item = 0; item < dbUsers.size(); item++){
				String user = ((Creator)dbUsers.get(item)).getName();
				//VWSLog.add("From DB User " + user);
				while(groupUsers.contains(user)){
				    VWSLog.dbg("Removing DB User from groupUsers as it is already exist in DB:::" + user);
				    groupUsers.remove(user);
				}
		    }	
		    if (groupUsers == null || groupUsers.size() == 0) {
		    	VWSLog.dbg("return from syncUsers method as there are no registered group users to sync::: and gIgnoreSync flag value is :::" + gIgnoreSync);
		    	return;
		    } else {
		    	VWSLog.dbg("groupusers count:::" + groupUsers.size() + " and gIgnoreSync flag value::: " + gIgnoreSync);
		    }
		    isSyncRequired = true;     
		    //gIgnoreSync = false;	
		    //return;
		//}
	    }
	    if (isSyncRequired) {
	    	VWSLog.dbg("isSyncRequired checking: " + isSyncRequired);
	    	VWSLog.add("groupUsers before adding to uservector...."+groupUsers);
	    	users.addAll(groupUsers);
	    }
	    else {
	    	VWSLog.dbg("isUserGroupXMLGenerated checking: " + isUserGroupXMLGenerated);
	    	if(isUserGroupXMLGenerated) {
	    		users = getUsersFromXML();
	    	}
	    	else{
	    		VWSLog.dbg("Before getting users from directory::::");
	    		users = directory.getUsers(false);
	    		VWSLog.dbg("After Getting users from directory::::"+users);
	    		
	    	}
	    	if(users == null || users.size() == 0)
	    	{
	    		VWSLog.dbg("No user found or userGroupList.xml file not found so return from SyncUsers method with gIgnoreSync flag value as::: " + gIgnoreSync);
	    		gIgnoreSync = true;
	    		return;
	    	}
	    	
	    	VWSLog.dbg("getAdminGrpCount :::"+getAdminGrpCount()+"adminGroup::::"+getAdminGroup());
	    	if(getAdminGrpCount()!=-1){

	    		adminList = getUsersInGroup(getAdminGroup());
	    		for(int i=0;i<getAdminGrpCount();i++){
	    			if(users.contains(adminList.get(i))){
	    				users.remove(adminList.get(i));
	    			}
	    		}
	    		VWSLog.dbg("Before if condition ::: "+getAdminGrpCount());
	    		if(getAdminGrpCount()<adminList.size()){
	    			for(int i=0;i<getAdminGrpCount();i++){
	    				users.add(adminList.get(i));
	    			}
	    			String message = ResourceManager.getDefaultManager().getString("SyncMsg.AdminGroup");
	    			VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    			VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.AdminGroup"));
	    		}else{
	    			users.addAll(getUsersInGroup(getAdminGroup()));
	    			VWSLog.dbg("users in  else of admin group :::"+users);
	    		}
	    		VWSLog.dbg("users after admincheck ......"+users);
	    	}
	    	VWSLog.dbg("after admingroup sync.....");
	    	//Added for License changes Madhavan.B
	    	VWSLog.dbg("getNamedGrpCount :::"+getNamedGrpCount()+"namedGroup::::"+getNamedGroup());
	    	if (getNamedGrpCount() != -1) {
	    		if(!isHosting) {
	    			VWSLog.dbg("SyncUsers() getNamedGroup(): " +  getNamedGroup());
		    		namedList = getUsersInGroup(getNamedGroup());
		    		VWSLog.dbg("Inside sync users namedList before removing: " + namedList);
		    		for(int i=0;i<namedList.size();i++){
		    			if(users.contains(namedList.get(i))){
		    				users.remove(namedList.get(i));
		    			}
		    		}
	    		}
	    		if(!isHosting){
	    			if(!isHosting&&getNamedGrpCount()<namedList.size()){
	    				for(int i=0;i<getNamedGrpCount();i++){
	    					users.add(namedList.get(i));
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
	    			}

	    			else{
	    				//groupUsers.addAll(getUsersInGroup(getNamedGroup()));
	    				users.addAll(getUsersInGroup(getNamedGroup()));
	    			}
	    		}
	    		
	    		//for Hosting environment
	    		if(isHosting ) {
		    		roomLevelNamedList= getUsersInGroup(VWSPreferences.getRoomLevelNamed(room));
		    		for(int i=0;i<roomLevelNamedList.size();i++){
		    			if(users.contains(roomLevelNamedList.get(i))){
		    				users.remove(roomLevelNamedList.get(i));
		    			}
		    		}
	    		}
	    		if(isHosting){
	    			VWSLog.dbg("getNamedGrpCount()<roomLevelNamedList.size()" + getNamedGrpCount() + " :: " + roomLevelNamedList.size());
	    			if(isHosting&&getNamedGrpCount()<roomLevelNamedList.size()){
	    				for(int i=0;i<getNamedGrpCount();i++){
	    					VWSLog.dbg("Entered getNamedGrpCount()<roomLevelNamedList.size()::" + roomLevelNamedList.get(i));
	    					users.add(roomLevelNamedList.get(i));
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
	    			}
	    			else{
	    				//groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelNamed(room)));
	    				users.addAll(getUsersInGroup(VWSPreferences.getRoomLevelNamed(room)));
	    			}
	    		}
	    	}
	    	//----------------------CV2019 License changes----------------------//
	    	VWSLog.add("getNamedOnlineGrpCount :::"+getNamedOnlineGrpCount()+"namedGroup::::"+getNamedOnlineGroup());

	    	if (getNamedOnlineGrpCount() != -1) {
	    		if(!isHosting) {
	    			VWSLog.dbg("SyncUsers() getNamedGroup(): " +  getNamedOnlineGroup());
		    		namedOnlineList = getUsersInGroup(getNamedOnlineGroup());
		    		VWSLog.dbg("Inside sync users namedList before removing: " + namedList);
		    		for(int i=0;i<namedOnlineList.size();i++){
		    			if(users.contains(namedOnlineList.get(i))){
		    				VWSLog.dbg("namedOnlineList before removing :"+namedOnlineList.get(i));
		    				VWSLog.dbg("namedOnlineList user from user vector::" + namedOnlineList.get(i));
		    				users.remove(namedOnlineList.get(i));
		    			}
		    		}
	    		}
	    		if(!isHosting){
	    			if(!isHosting&&getNamedOnlineGrpCount()<namedOnlineList.size()){
	    				for(int i=0;i<getNamedOnlineGrpCount();i++){
	    					VWSLog.dbg("namedOnlineList.get(i) in getNamedOnlineGrpCount before readding :"+namedOnlineList.get(i));
	    					users.add(namedOnlineList.get(i));
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedOnlineGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedOnlineGroup"));
	    			}

	    			else{
	    				users.addAll(getUsersInGroup(getNamedOnlineGroup()));
	    			}
	    		}
	    		
	    		//for Hosting environment
	    		if(isHosting ) {
	    			roomLevelNamedOnlineList= getUsersInGroup(VWSPreferences.getRoomLevelNamedOnline(room));
		    		for(int i=0;i<roomLevelNamedOnlineList.size();i++){
		    			if(users.contains(roomLevelNamedOnlineList.get(i))){
		    				users.remove(roomLevelNamedOnlineList.get(i));
		    			}
		    		}
	    		}
	    		if(isHosting){
	    			VWSLog.dbg("getNamedOnlineGrpCount()<roomLeveleNamedOnlineList.size()" + getNamedOnlineGrpCount() + " :: " + roomLevelNamedOnlineList.size());
	    			if(isHosting&&getNamedOnlineGrpCount()<roomLevelNamedOnlineList.size()){
	    				for(int i=0;i<getNamedOnlineGrpCount();i++){
	    					VWSLog.dbg("Entered getNamedGrpCount()<roomLevelNamedList.size()::" + roomLevelNamedList.get(i));
	    					users.add(roomLevelNamedOnlineList.get(i));
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
	    			}
	    			else{
	    				//groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelNamed(room)));
	    				users.addAll(getUsersInGroup(VWSPreferences.getRoomLevelNamedOnline(room)));
	    			}
	    		}
	    	}
	    	//----------------------CV2019 License changes----------------------//
	    	//---------------------CV2019 License changes-------------------//
	    	//VWSLog.add("getConcurrentGrpCount :::"+getConcurrentGrpCount()+"getConcurrentGroup::::"+getConcurrentGroup());
	    	/*if (getConcurrentGrpCount() != -1) {
	    		if(!isHosting) {
	    			VWSLog.dbg("SyncUsers() getConcurrentGroup(): " +  getConcurrentGroup());
	    			concurrentList = getUsersInGroup(getConcurrentGroup());
	    			VWSLog.dbg("Inside sync users concurrentList before removing: " + concurrentList);
		    		for(int i=0;i<concurrentList.size();i++){
		    			if(users.contains(concurrentList.get(i))){
		    				//VWSLog.dbg("concurrentList.get(i) in getNamedGrpCount before removing :"+concurrentList.get(i));
		    				users.remove(concurrentList.get(i));
		    			}
		    		}
	    		}
	    		if(!isHosting){
	    			//Commented date Date :-3/8/2017 BY :- madhavan
	    			if(!isHosting&&getConcurrentGrpCount()<concurrentList.size()){
	    				for(int i=0;i<getConcurrentGrpCount();i++){
	    					//VWSLog.dbg("concurrentList.get(i) in getConcurrent before reading :"+concurrentList.get(i));
	    					users.add(concurrentList.get(i));
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup"));
	    			}

	    			else{
	    				users.addAll(getUsersInGroup(getConcurrentGroup()));
	    			}

	    			//for Hosting environment
	    			if(isHosting ) {
	    				roomLevelconcurrentList= getUsersInGroup(VWSPreferences.getRoomLevelConcurrent(room));
	    				for(int i=0;i<roomLevelconcurrentList.size();i++){
	    					if(users.contains(roomLevelconcurrentList.get(i))){
	    						users.remove(roomLevelconcurrentList.get(i));
	    					}
	    				}
	    			}
	    			if(isHosting){
	    				VWSLog.dbg("getConcurrentGrpCount()<roomLevelconcurrentList.size()" + getConcurrentGrpCount() + " :: " + roomLevelconcurrentList.size());
	    				if(isHosting&&getConcurrentGrpCount()<roomLevelconcurrentList.size()){
	    					for(int i=0;i<getConcurrentGrpCount();i++){
	    						users.add(roomLevelconcurrentList.get(i));
	    					}
	    					String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup");
	    					VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    					VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup"));
	    				}
	    				else{
	    					users.addAll(getUsersInGroup(VWSPreferences.getRoomLevelConcurrent(room)));
	    				}
	    			}
	    		}
	    	}*/
	    	//---------------------CV2019 License changes-------------------//
	    	//VWSLog.dbg("after concurrrnet list sync...");
	    	//VWSLog.dbg("getConcurrentOnlineGrpCount :::"+getConcurrentOnlineGrpCount()+"getConcurrentOnlineGroup::::"+getConcurrentOnlineGroup());

		    /*	if (getConcurrentOnlineGrpCount() != -1) {
		    		if(!isHosting) {
		    			VWSLog.dbg("SyncUsers() getConcurrentOnlineGroup(): " +  getConcurrentOnlineGroup());
		    			concurrentOnlineList = getUsersInGroup(getConcurrentOnlineGroup());
		    			VWSLog.dbg("Inside sync users concurrentList before removing: " + concurrentOnlineList);
			    		for(int i=0;i<concurrentOnlineList.size();i++){
			    			if(users.contains(concurrentOnlineList.get(i))){
			    				//VWSLog.dbg("concurrentList.get(i) in getNamedGrpCount before removing :"+concurrentList.get(i));
			    				users.remove(concurrentOnlineList.get(i));
			    			}
			    		}
		    		}
		    		if(!isHosting){
		    			//Commented date Date :-3/8/2017 BY :- madhavan
		    			if(!isHosting&&getConcurrentOnlineGrpCount()<concurrentOnlineList.size()){
		    				for(int i=0;i<getConcurrentOnlineGrpCount();i++){
		    					//VWSLog.dbg("concurrentList.get(i) in getConcurrent before reading :"+concurrentList.get(i));
		    					users.add(concurrentOnlineList.get(i));
		    				}
		    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentOnlineGroup");
		    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentOnlineGroup"));
		    			}

		    			else{
		    				//VWSLog.dbg("GroupUsers.addAll(getUsersInGroup(getConcurrent())): " + getUsersInGroup(getConcurrentGroup()));
		    				//groupUsers.addAll(getUsersInGroup(getConcurrentGroup()));
		    				users.addAll(getUsersInGroup(getConcurrentOnlineGroup()));
		    				//}
		    			}

		    			//for Hosting environment
		    			if(isHosting ) {
		    				roomLeveleconcurrentOnlineList= getUsersInGroup(VWSPreferences.getRoomLevelConcurrentOnline(room));
		    				for(int i=0;i<roomLeveleconcurrentOnlineList.size();i++){
		    					if(users.contains(roomLeveleconcurrentOnlineList.get(i))){
		    						users.remove(roomLeveleconcurrentOnlineList.get(i));
		    					}
		    				}
		    			}
		    			if(isHosting){
		    				//Commented date Date :-3/8/2017 BY :- madhavan
		    				//VWSLog.dbg("getConcurrentOnlineGrpCount()<roomLevelconcurrentOnlineList.size()" + getConcurrentOnlineGrpCount() + " :: " + roomLevelconcurrentOnlineList.size());
		    				if(isHosting&&getConcurrentOnlineGrpCount()<roomLeveleconcurrentOnlineList.size()){
		    					for(int i=0;i<getConcurrentOnlineGrpCount();i++){
		    						users.add(roomLeveleconcurrentOnlineList.get(i));
		    					}
		    					String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentOnlineGroup");
		    					VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
		    					VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentOnlineGroup"));
		    				}
		    				else{
		    					//groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelConcurrent(room)));
		    					users.addAll(getUsersInGroup(VWSPreferences.getRoomLevelConcurrentOnline(room)));
		    					//}
		    				}
		    			}
		    		}
		    	}*/
		    		//--------------------------------------------------------//
	    	
		    	VWSLog.dbg("getProfessionalNamedGrpCount :::"+getConcurrentOnlineGrpCount()+"getProfessionalNamedGroup::::"+getProfessionalNamedGroup());	    	
	    	if (getProfessionalNamedGrpCount() != -1) {
	    		if(!isHosting) {
	    			VWSLog.dbg("SyncUsers() getProfessionalNamedGroup(): " +  getProfessionalNamedGroup());
		    		profesionalNamedList = getUsersInGroup(getProfessionalNamedGroup());
		    		VWSLog.dbg("Inside sync users profesionalNamedList before removing: " + profesionalNamedList);
		    		for(int i=0;i<profesionalNamedList.size();i++){
		    			if(users.contains(profesionalNamedList.get(i))){
		    				users.remove(profesionalNamedList.get(i));
		    			}
		    		}
	    		}

	    		if(!isHosting){
	    			if(!isHosting&&getProfessionalNamedGrpCount()<profesionalNamedList.size()){
	    				for(int i=0;i<getProfessionalNamedGrpCount();i++){
	    					users.add(profesionalNamedList.get(i));	    			
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
	    			}
	    			else{
	    				VWSLog.dbg("groupUsers.addAll(getUsersInGroup(getProfessionalNamedGroup()))");
	    				//groupUsers.addAll(getUsersInGroup(getProfessionalNamedGroup()));
	    				users.addAll(getUsersInGroup(getProfessionalNamedGroup()));
	    			}
	    		}
	    		
	    		//for Hosting environment
	    		if(isHosting) {
		    		roomLevelProfessionalNamedList= getUsersInGroup(VWSPreferences.getRoomLevelProfessionalNamed(room));
		    		for(int i=0;i<roomLevelProfessionalNamedList.size();i++){
		    			if(users.contains(roomLevelProfessionalNamedList.get(i))){
		    				users.remove(roomLevelProfessionalNamedList.get(i));
		    			}
		    		}
	    		}
	    		if(isHosting){
	    			if(isHosting&&getProfessionalNamedGrpCount()<roomLevelProfessionalNamedList.size()){
	    				for(int i=0;i<getProfessionalNamedGrpCount();i++){
	    					users.add(roomLevelProfessionalNamedList.get(i));	    			
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
	    			}
	    			else{
	    				//groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelProfessionalNamed(room)));
	    				users.addAll(getUsersInGroup(VWSPreferences.getRoomLevelProfessionalNamed(room)));
	    			}
	    		}
	    		
	    	}
	    	//---------------------CV10.1 License changes end-------------------//
	       	VWSLog.dbg("getProfessionalConcrGroup :::"+getProfessionalConcrGroup()+"getProfessionalConcrGroup::::"+getProfessionalConcrGroup());	    	
	    	/*if (getProfessConcrGrpCount() != -1) {
	    		if(!isHosting) {
	    			VWSLog.dbg("SyncUsers() getProfessionalConcrGroup(): " +  getProfessionalConcrGroup());
	    			profConcrList = getUsersInGroup(getProfessionalConcrGroup());
	    			VWSLog.dbg("Inside sync users profConcrList before removing: " + profConcrList);
		    		for(int i=0;i<profConcrList.size();i++){
		    			if(users.contains(profConcrList.get(i))){
		    				users.remove(profConcrList.get(i));
		    			}
		    		}
	    		}
	    		//Commented date Date :-3/8/2017 BY :- madhavan
	    		if(!isHosting){
	    			if(!isHosting&&getProfessConcrGrpCount()<profConcrList.size()){
	    				for(int i=0;i<getProfessConcrGrpCount();i++){
	    					users.add(profConcrList.get(i));	    			
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
	    			}
	    			else{
	    				VWSLog.dbg("groupUsers.addAll(getUsersInGroup(getProfessionalNamedGroup()))");
	    				//groupUsers.addAll(getUsersInGroup(getProfessionalConcrGroup()));
	    				users.addAll(getUsersInGroup(getProfessionalConcrGroup()));
	    			}
	    		}
	    		//Commented date Date :-3/8/2017 BY :- madhavan
	    		//for Hosting environment
	    		if(isHosting) {
	    			roomLevelprofConcrList= getUsersInGroup(VWSPreferences.getRoomLevelProfConcurrent(room));
		    		for(int i=0;i<roomLevelprofConcrList.size();i++){
		    			if(users.contains(roomLevelprofConcrList.get(i))){
		    				users.remove(roomLevelprofConcrList.get(i));
		    			}
		    		}
	    		}
	    		if(isHosting){
					//Commented date Date :-3/8/2017 BY :- madhavan
	    			if(isHosting&&getProfessConcrGrpCount()<roomLevelprofConcrList.size()){
	    				for(int i=0;i<getProfessConcrGrpCount();i++){
	    					users.add(roomLevelprofConcrList.get(i));	    			
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
	    			}
	    			else{
	    				//groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelProfConcurrent(room)));
	    				users.addAll(getUsersInGroup(VWSPreferences.getRoomLevelProfConcurrent(room)));
	    				
	    			//}
	    		}
	    		
	    	}
	    }	
	      	VWSLog.dbg("getPublicWAGrpCount :::"+getPublicWAGrpCount()+"getPublicWAGroup::::"+getPublicWAGroup());	   */ 	
	    	/*if (getPublicWAGrpCount() != -1) {
	    		if(!isHosting) {
	    			VWSLog.dbg("SyncUsers() public(): " +  getPublicWAGrpCount());
	    			publicWAList = getUsersInGroup(getPublicWAGroup());
	    			VWSLog.dbg("Inside sync users publicgroup before removing: " + profConcrList);
		    		for(int i=0;i<publicWAList.size();i++){
		    			if(users.contains(publicWAList.get(i))){
		    				users.remove(publicWAList.get(i));
		    			}
		    		}
	    		}
	    		//Commented date Date :-3/8/2017 BY :- madhavan
	    		if(!isHosting){
	    			if(!isHosting&&getPublicWAGrpCount()<publicWAList.size()){
	    				for(int i=0;i<getPublicWAGrpCount();i++){
	    					users.add(publicWAList.get(i));	    			
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
	    			}
	    			else{
	    				VWSLog.dbg("groupUsers.addAll(getUsersInGroup(getProfessionalNamedGroup()))");
	    				//groupUsers.addAll(getUsersInGroup(getProfessionalConcrGroup()));
	    				users.addAll(getUsersInGroup(getPublicWAGroup()));
	    			}
	    		}
	    		//Commented date Date :-3/8/2017 BY :- madhavan
	    		//for Hosting environment
	    		if(isHosting) {
	    			roomLevelpublicList= getUsersInGroup(VWSPreferences.getRoomLevelPublicWA(room));
		    		for(int i=0;i<roomLevelprofConcrList.size();i++){
		    			if(users.contains(roomLevelprofConcrList.get(i))){
		    				users.remove(roomLevelprofConcrList.get(i));
		    			}
		    		}
	    		}
	    		if(isHosting){
					//Commented date Date :-3/8/2017 BY :- madhavan
	    			if(isHosting&&getPublicWAGrpCount()<roomLevelpublicList.size()){
	    				for(int i=0;i<getPublicWAGrpCount();i++){
	    					users.add(roomLevelpublicList.get(i));
	    					
	    				}
	    				String message = ResourceManager.getDefaultManager().getString("SyncMsg.publicWAGroup ");
	    				VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.publicWAGroup"));
	    			}
	    			else{
	    				//groupUsers.addAll(getUsersInGroup(VWSPreferences.getRoomLevelProfConcurrent(room)));
	    				users.addAll(getUsersInGroup(VWSPreferences.getRoomLevelPublicWA(room)));
	    				
	    			//}
	    		}
	    		
	    	}
	    }	*/
	    }
	    //---------------------------- CV10.1 Get all the groups information and users of those groups and add to groupUsers vector
	
	    VWSLog.dbg("VWSPreferences.getSyncGroupUsers().........."+VWSPreferences.getSyncGroupUsers());
	    //String schemeType = VWSPreferences.getSSType();
	    if ((VWSPreferences.getSyncGroupUsers()==true) && (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)))
	    {
	    	VWSLog.dbg("Inside ((VWSPreferences.getSyncGroupUsers()==true) && (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)))::: and before getting directory groups");
	    	Vector groups = directory.getGroups();
	    	VWSLog.dbg("groups from directory before checking and removing only for LDAP:::"+groups);
	    	/**
	    	 * CV10.1 19/06/2017
	    	 * Not to add  group users directly adding during server restart.
	    	 */
	    	//////////////////////////////
	    	String gparam = "";
	        if(database.getEngineName().equalsIgnoreCase("Oracle"))
	        	gparam = "2" + Util.SepChar + "~" + Util.SepChar;
	        else
	        	gparam = "2" + Util.SepChar + "~";
	        DoDBAction.get(database,"Group_Get",gparam,ret);
	        Vector dbGroups = new Vector();
	        for (int cnt = 0; cnt < ret.size(); cnt++){
	            StringTokenizer st = new StringTokenizer((String) ret.get(cnt), Util.SepChar);
	            int id = Util.to_Number(st.nextToken());
	            String name = st.nextToken();
	            dbGroups.add(name);
	        }
	        
	        for(int i=0;i<ret.size();i++)
	        {
	            String groupName = (String) dbGroups.get(i);
	            if (!(groups.contains(groupName))){
	            	 VWSLog.dbg("inside if condition groupname:::" + groupName.toLowerCase());
	            	groups.remove(groupName);
	            }
	        }
	        VWSLog.dbg("groups from directory after checking and removing only for LDAP:::"+groups);
	        
	    	//////////////////////////////////////////
	        
	    	VWSLog.dbg("After getting groups from directory inside syncUsers:::"+ groups);
	    	Iterator iterator11 = groups.iterator();
	    	List allUsers=null;
	    	String groupName = "";
	    	while (iterator11.hasNext())
	    	{
	    		try{
	    			groupName = (String) iterator11.next();
	    			/*
	    			 * CV10.1 Single domain user with other ou concurrent group user and registered group user
	    			 * added together and sync updates concurrnet group user also.
	    			 * Date:- 28/6/2017
	    			 * Modified by :- B.Madhavan
	    			 */
	    			boolean isOtherOU=VWSPreferences.getSyncOtherOUUsers();
	    			VWSLog.dbg("------isOtherOUregistry in sycusers----"+isOtherOU);
	    			if(isOtherOU==true) {
	    			VWSLog.dbg("---isOtherOU in syncusers------"+isOtherOU);
	    			if(!((isRegisteredGroup(groupName)==false)&&(dbUsers.size() >0))){
	    				
	    					allUsers = getUsersInGroup_New(groupName, 1);
	    				}
	    		//	}
	    			
	    			groupUsers.addAll( getUsersInGroup_New(groupName,1));
	    			Iterator iterator = allUsers.iterator();
	    			while (iterator.hasNext())
	    			{            	 
	    				userName = (String) iterator.next();

	    				String name="";
	    				if(userName.indexOf("@")!=-1)
	    					name = userName.substring(0, (userName.indexOf("@")));

	    				VWSLog.dbg("userName after removing @ :::::::::"+name);
	    				VWSLog.dbg("dbUsers :::::::"+dbUsers);
	    				VWSLog.dbg("dbUsers size:::::::"+dbUsers.size());
	    				/*
	    				 * This will add users to DB only when room is created newly and DB does not contains any users.
	    				 * Modified By:-Sudhansu
	    				 * Modified Date:-17-03-2017
	    				 */
	    				if(dbUsers.size() == 0)    
	    				{
		    				param = "1" + Util.SepChar + Util.getOneSpace(name);
		    				DoDBAction.get(database, "User_Get", param, ret);
	    				}else if(!(dbUsers.contains(name))){

		    				param = "1" + Util.SepChar + Util.getOneSpace(name);
		    				DoDBAction.get(database, "User_Get", param, ret);
	    				
	    				}
	    				if(ret.size() > 0)
	    	    		{
	    	    			int userId = Util.to_Number((String) ret.get(0));
	    	    			removeUserFromList(dbUsers, userId);
	    	    			if (userId > 0){
	    	    				emailUserList.add(userName);
	    	    			}
	    	    			ret.removeAllElements();
	    	    		}

	    				String samName=directory.getSamAttribute(userName);
	    				VWSLog.dbg("Sam Name ::::::::::::"+samName);
	    				String SamName[]=samName.split(":");

	    				param = "6" + Util.SepChar + Util.getOneSpace(userName) + Util.SepChar +SamName[1].trim()+Util.SepChar;
	    				DoDBAction.get(database, "User_Get", param, ret);
	    				
	    				
	    				/**
	    				 * CV10.1 Enahancement :- Getting the usersDN and stored in DB .
	                      * Modified by :- Sudhansu 
	    				 */
	    				String usersDN = directory.getUserBasicAttributes(userName);
	    				VWSLog.dbg("ldapuserDN value :::"+usersDN);
	    				String UsersDN[]=usersDN.split(":");
	    				param = "9" + Util.SepChar + Util.getOneSpace(userName) + Util.SepChar +UsersDN[1].trim()+Util.SepChar;
	    				VWSLog.dbg("params after getting UsersDN::::"+param);
	    				DoDBAction.get(database, "User_Get", param, ret);
	    			}
	    			}

	    		}catch(Exception ex){
	    			VWSLog.dbg("Error while sync groups " + ex.getMessage() + " ::: " + groupName);
	    		}
	    	}
	    }  
	    
	    //----------------------------Get all the groups information and users of those groups and add to groupUsers vector
	    //else  
	    {
	    	VWSLog.add(database.getName() + ": Synchronized " 
	    			+ users.size() + " Users to Security Server ");    
	    	Iterator dbUsersIterator = dbUsers.iterator();
	    	while (dbUsersIterator.hasNext())
	    	{            	 
	    		Creator user = (Creator)dbUsersIterator.next();
	    		VWSLog.dbg("Inside remove of users iteration" + user.getName());
	    		if (users.contains(user.getName())){
	    			emailUserList.add(user.getName());
	    			users.remove(user.getName());
	    			dbUsers.remove(user);
	    		}		
	    	}    
	    	Iterator iterator = users.iterator();
	    	VWSLog.dbg("before adding users into tables " + users);
	    	while (iterator.hasNext())
	    	{            	 
	    		userName = (String) iterator.next();
	    		param = "1" + Util.SepChar + Util.getOneSpace(userName);
	    		DoDBAction.get(database, "User_Get", param, ret);
	    		if(ret.size() > 0)
	    		{
	    			int userId = Util.to_Number((String) ret.get(0));
	    			removeUserFromList(dbUsers, userId);
	    			if (userId > 0){
	    				emailUserList.add(userName);
	    			}

	    			ret.removeAllElements();
	    		}
	    	}

	    	/*//Uncomment below code to fetch sAM name for users that belong to current OU groups and user are in other OU's 
	    	 * iterator = users.iterator();
	    	VWSLog.add("before adding users into tables " + users);
	    	while (iterator.hasNext())
	    	{            	 
	    		String userName = (String) iterator.next();
	    		String samName=directory.getSamAttribute(userName);
				VWSLog.add("Sam Name 1111111::::::::::::::::::"+samName);
				String SamName[]=samName.split(":");

				param = "6" + Util.SepChar + Util.getOneSpace(userName) + Util.SepChar +SamName[1].trim()+Util.SepChar;
				DoDBAction.get(database, "User_Get", param, ret1);	    		
	    	}
	    	*/
	    	
    		/**
    		 * Based on flag (false) Get all the user information  and stored in usertable.
    		 * modified by :sudhansu
    		 * 
    		 */
    		if (VWSPreferences.getSyncGroupUsers()==false) {
    			syncUserUPN();
    		}

	    }
	    Vector usersMailIds = new Vector();
	    usersMailIds = directory.getUserMailIds(emailUserList);
	    VWSLog.dbg(database.getName() + ": Synchronizing "+ usersMailIds.size() + " User Mail ids. ");
	    Iterator mailIterator = usersMailIds.iterator();
	    Iterator userIterator = emailUserList.iterator();//Iterator userIterator = users.iterator();
	    //To do with User_Get procedure itself instead separate call.	Valli
	    while (mailIterator.hasNext() && userIterator.hasNext() )
	    {
		String userMailId = (String) mailIterator.next();
		userName = (String) userIterator.next();
		param = userName+Util.SepChar+userMailId;
		DoDBAction.get(database, "VW_SetUserMails", param, ret);
	    }
	    if (isSyncRequired) return;
	    removeUsers(dbUsers);
		}
	    catch(Exception ex){
	    //VWSLog.err("Exception while synchronizing users " + ex.getMessage());
	    }
    }

	public void syncUserMailId(){
		String schemeType = VWSPreferences.getSSType();
		 List dbusers=null;
		 if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)){
			 dbusers = getUsersForEMail();
		 }else{
			  dbusers = getUsers();	
		}
       
        if(dbusers!=null && dbusers.size()>0)
        	syncUserMailId(dbusers);
    }
    
    private void syncUserMailId(List users){
    	
    	Vector usersMailIds = new Vector();
    	/*  synchUserMailId = 0  Don't update user mail ids from security server
    	* 	synchUserMailId = 1  update user mail ids from security server if mail id is blank in db table 
    	*	synchUserMailId = 2  overwrite all user mail ids from security server.*/
    	String synchUserMailId = VWSPreferences.getSynchUserMailId().trim();
    	if(synchUserMailId == null || (synchUserMailId!=null && synchUserMailId.equals("")))
    		synchUserMailId = "1"; 
    	if(synchUserMailId.equals("1") || synchUserMailId.equals("2")){
    		List usersList = new LinkedList();
    		for(int j = 0; j<users.size(); j++){
    			Creator creator = (Creator) users.get(j);
    			usersList.add(creator.getName().toString());
    		}
    		VWSLog.dbg("UsersList....."+usersList);
			/**Code commeted in CV10.2****/
    		/*usersMailIds = directory.getUserMailIds(usersList);
    		VWSLog.dbg("usersMailIds::::::::"+usersMailIds);
    		VWSLog.dbg(database.getName() + ": Synchronizing "+ usersMailIds.size() + " User Mail ids. ");
    		VWSLog.dbg("users ............"+users);
    		VWSLog.dbg("usersList ......."+usersList);
    		for(int i = 0; i<users.size(); i++)
    		{
    			Creator creator = (Creator) users.get(i);
    			String userName = creator.getName();
    			String mailId = creator.getEmail().trim();
    			String userMailId = usersMailIds.get(i).toString();
    			if( (synchUserMailId.equals("1") && (mailId == null || mailId.equalsIgnoreCase("-") || mailId.equals("")) ) 
    					||( synchUserMailId.equals("2")&&(!userMailId.equals("-")))){
    				String param = userName+Util.SepChar+userMailId;
       				DoDBAction.get(database, "VW_SetUserMails", param, new Vector());
    			}
    		}*/
    	}
    }
    private Hashtable getUsersTable()
    {
		/**CV2019 code merges from CV10.2 line**/
    	VWSLog.dbg("Start of getUsersTable from DirectorySync");
    	Hashtable userList = new Hashtable();
        Vector users = new Vector();
        String param = "";

        String[] dirList = null;
	    String ldapDirectory = VWSPreferences.getTree();
		VWSLog.dbg("ldapDirectory ::::::::"+ldapDirectory);
		dirList = ldapDirectory.split(",");
		boolean LDAPsingleToMultiple = VWSPreferences.getLDAPUpgradeToMultiDomain();
    	  //579
        String schemeType = VWSPreferences.getSSType();
        if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)){
        	if (dirList.length>1) {
        		//Commented bz we are not considering this registry
        		//if (LDAPsingleToMultiple) {
        			VWSLog.dbg("getting DB users for domain: " + directory.getDirName());			
        			VWSLog.dbg("getting DB users for domain: " + dirList[0]);
        			if (directory.getDirName().equalsIgnoreCase(dirList[0])) {
        				if(database.getEngineName().equalsIgnoreCase("Oracle"))
        					param = "15" + Util.SepChar + "-" + Util.SepChar;
        				else
        					param = "15" + Util.SepChar + "";
        			} else {
        				if(database.getEngineName().equalsIgnoreCase("Oracle"))
        					param = "15" + Util.SepChar + directory.getDirName() + Util.SepChar;
        				else
        					param = "15" + Util.SepChar + directory.getDirName();
        			}

        		/*} else {
        			VWSLog.dbg("getting DB users for domain: " + directory.getDirName());
        			if(database.getEngineName().equalsIgnoreCase("Oracle"))
        				param = "15" + Util.SepChar + directory.getDirName() + Util.SepChar;
        			else
        				param = "15" + Util.SepChar + directory.getDirName();
        		}*/
        	} else {
        		if(database.getEngineName().equalsIgnoreCase("Oracle"))
        			param = "15" + Util.SepChar + "-" + Util.SepChar ;
        		else
        			param = "15"+ Util.SepChar + "-";
        	}
        	 VWSLog.dbg("User_Get param :"+param);
    		 DoDBAction.get(database, "User_Get", param, users);
    		 try{
    			 VWSLog.add("users List from DB :"+users);
    			 int id = 0;
    			 String name = null , userUPnSuffix = null, userDn = null;
	    		 if(users!=null&&users.size()>0){
	    			for (int i = 0; i < users.size(); i++)
	    	        {
	    	            StringTokenizer st = new StringTokenizer
	    	                                          ((String) users.get(i), Util.SepChar);
	    	            if(st.hasMoreTokens()){
	    	            	try {
			    	            id = Util.to_Number(st.nextToken());
			    	            name = st.nextToken();
			    	            userUPnSuffix=st.nextToken();
			    	            userDn = st.nextToken();
			    	            VWSLog.dbg("id :::"+id+"name :::"+name+"userUPnSuffix:::"+userUPnSuffix+"userDn::"+userDn);
			    	            //Below line commented by srikanth on 12 Feb 2019
			    	            //userList.put(name+"@"+userUPnSuffix+Util.SepChar+userDn, id);
			    	            if (dirList.length>1) 
			    	            	userList.put(name, id);
			    	            else
			    	            	userList.put(name+"@"+userUPnSuffix, id);
			    	        } catch (Exception e) {
								/*
								 * This is added bz when we are syncing user and groups from AD to LADP then AD users are not getting added to the userslist. 
								 * Bz upnsuffix and userDomain is empty , so st.nextTocken() will throw an exception.
								 */	    	            		
			    	        	VWSLog.dbg("Exception while fetching user from usersTable : UserName : "+name);
	    	            		
	    	            	}
	    	            }
	    	        }
	    		 }
	    	}catch(Exception e){
    			 VWSLog.dbg("Exception e:::"+e.getMessage());
    		 }
    	}else{
	        if(database.getEngineName().equalsIgnoreCase("Oracle"))
	        	param = "2" + Util.SepChar;
	        else
	        	param = "2";
	        DoDBAction.get(database, "User_Get", param, users);
	        for (int i = 0; i < users.size(); i++)
	        {
	            StringTokenizer st = new StringTokenizer
	                                          ((String) users.get(i), Util.SepChar);
	            int id = Util.to_Number(st.nextToken());
	            String name = st.nextToken();
	            userList.put(name.toLowerCase(), id);
	        }
    	}
    	VWSLog.dbg("End of getUsersTable from DirectorySync"+userList);
        return userList;
    }
    
    private List getUsers()
    {
		/**CV2019 code merges from CV10.2 line**/
    	VWSLog.dbg("-------Inside Get users method-----");
    	List userList = new LinkedList();
    	try {	        
	        Vector users = new Vector();
	        String param = "";
	        if(database.getEngineName().equalsIgnoreCase("Oracle"))
	        	param = "2" + Util.SepChar ;
	        else
	        	param = "2" ;
	        DoDBAction.get(database, "User_Get", param, users);
	        for (int i = 0; i < users.size(); i++)
	        {
	        	StringTokenizer st = new StringTokenizer
	                                          ((String) users.get(i), Util.SepChar);
	        	//Creator user = new Creator(Util.to_Number(st.nextToken()),  st.nextToken());
	            int id = Util.to_Number(st.nextToken());
	            VWSLog.dbg("id..." + id);
	            String name = st.nextToken();
	            VWSLog.dbg("name..." + name);
	            String mailId = "";
	            if(st.hasMoreTokens())
	            	mailId = st.nextToken();
	            else
	            	mailId = "-";
	            VWSLog.dbg("mailId..." + mailId);
	            Creator user = new Creator(id,name, mailId,"");
	            userList.add(user);
	        }
    	} catch (Exception e) {
    		VWSLog.dbg("Exception in getUsers "+e.getMessage());
    	}
        VWSLog.dbg("--------End of Get users method--------");
        return userList;
    }
    private List getUsersForEMail()
    {
    	VWSLog.dbg("-------Inside Get users method with param 12-----");
        List userList = new LinkedList();
        Vector users = new Vector();
        String param = "";
        if(database.getEngineName().equalsIgnoreCase("Oracle"))
        	param = "12" + Util.SepChar+directory.getDirName()+Util.SepChar;
        else
        	param = "12"+Util.SepChar+directory.getDirName() ;
      
        DoDBAction.get(database, "User_Get", param, users);
        for (int i = 0; i < users.size(); i++)
        {
            StringTokenizer st = new StringTokenizer
                                          ((String) users.get(i), Util.SepChar);
            //Creator user = new Creator(Util.to_Number(st.nextToken()),  st.nextToken());
            int id = Util.to_Number(st.nextToken());
            String name = st.nextToken();
            String mailId = "";
            if(st.hasMoreTokens())
            	mailId = st.nextToken();
            else
            	mailId = "-";
            Creator user = new Creator(id, name, mailId,"");
            userList.add(user);
        }
        VWSLog.dbg("--------End of Get users method with param 12--------"+userList);
        return userList;
    }
    
    
    private List getUsers_New(String domainName)
    {
    	VWSLog.dbg("-------Inside getUsers_New method-----");
        List userList = new LinkedList();
        Vector users = new Vector();
        String param = "";
		/**CV2019 code merges from CV10.2 line**/
        if(database.getEngineName().equalsIgnoreCase("Oracle"))
        	param = "15" + Util.SepChar + domainName + Util.SepChar;
        else
        	param = "15" + Util.SepChar + domainName  ;
        DoDBAction.get(database, "User_Get", param, users);
        if(users!=null&&users.size()>0){
	        for (int i = 0; i < users.size(); i++)
	        {
	        	StringTokenizer st = new StringTokenizer
	        			((String) users.get(i), Util.SepChar);
	        	if(st.hasMoreTokens()){
	        		int id = Util.to_Number(st.nextToken());
	        		String name = st.nextToken();
	        		String userUPnSuffix=st.nextToken();
	        		String userDn=st.nextToken();
	        		String mailId=st.nextToken();
	        		VWSLog.dbg("id :::"+id+"    name :::"+name+"    userUPnSuffix:::"+userUPnSuffix+"    userDn::"+userDn);
	        		Creator user = new Creator(id, name+"@"+userUPnSuffix,mailId,userDn);
	        		userList.add(user);
	        	}
	        }
        }
        VWSLog.dbg("--------End of getUsers_New method--------");
        return userList;
    }
    
    private boolean getUsersForDomain(String domainName)
    {
    	VWSLog.dbg("-------Inside getUsersForDomain method-----");
        List userList = new LinkedList();
        Vector users = new Vector();
        String param = "";
        if(database.getEngineName().equalsIgnoreCase("Oracle"))
        	param = "10" + Util.SepChar + domainName + Util.SepChar;
        else
        	param = "10" + Util.SepChar + domainName;
        DoDBAction.get(database, "User_Get", param, users);
        VWSLog.dbg("--------End of getUsersForDomain method--------");
        if ((users.size() > 0) && (users != null)) {
        	VWSLog.dbg("-------Inside getUsersForDomain method returns true-----");
        	return true;
        } else {
        	VWSLog.dbg("-------Inside getUsersForDomain method returns false-----");
        	return false;
        }
    }
    /**
     * CV10.1 Method to check the given group is registered group or not.
     * @param groupName
     * @return
     */
    private boolean isRegisteredGroup(String groupName){
    	boolean isValidGroup=false;
    	try
    	{
    		String adminGroupName="";
    		String subAdminGroupName="";
    		String namedGroupName="";
    		String namedOnlineGroupName="";
    		String concurrentGroupName="";
    		String concurrentOnlineGroupName="";
    		String professionalNamedGroupName="";
    		String professionalConcurrentGroupName="";
    		String batchInputGroupName="";
    		/**publicWAGroup entry is not available in recent Server Settings UI. Commented date -22/08/2019 BY :- Vanitha**/
    		//String publicGroupName="";
    		adminGroupName=getAdminGroup();
    		subAdminGroupName=getSubAdminGroup();
    		namedGroupName=getNamedGroup();
    		namedOnlineGroupName=getNamedOnlineGroup();
    		concurrentGroupName=getConcurrentGroup();
    		concurrentOnlineGroupName=getConcurrentOnlineGroup();
    		professionalNamedGroupName=getProfessionalNamedGroup();
    		professionalConcurrentGroupName=getProfessionalConcrGroup();
    		batchInputGroupName = getBatchInputGroup();
    		/*publicGroupName=getPublicWAGroup();
    		if((groupName.equalsIgnoreCase(adminGroupName))||groupName.equalsIgnoreCase(namedGroupName)||groupName.equalsIgnoreCase(namedOnlineGroupName)||(groupName.equalsIgnoreCase(concurrentGroupName))||(groupName.equalsIgnoreCase(concurrentOnlineGroupName))||(groupName.equalsIgnoreCase(professionalNamedGroupName))||(groupName.equalsIgnoreCase(professionalConcurrentGroupName))||(groupName.equalsIgnoreCase(publicGroupName))){*/
    		if((groupName.equalsIgnoreCase(adminGroupName)) || (groupName.equalsIgnoreCase(subAdminGroupName)) ||groupName.equalsIgnoreCase(namedGroupName)||groupName.equalsIgnoreCase(namedOnlineGroupName)||(groupName.equalsIgnoreCase(concurrentGroupName))||(groupName.equalsIgnoreCase(concurrentOnlineGroupName))||(groupName.equalsIgnoreCase(professionalNamedGroupName))||(groupName.equalsIgnoreCase(professionalConcurrentGroupName)) || (groupName.equalsIgnoreCase(batchInputGroupName))){
    			isValidGroup= true;
    		}else
    			isValidGroup= false;            	
    	}
    	catch(Exception e)
    	{
    		VWSLog.dbg("Exception while chekcing the registered group...."+e.getMessage());
    	}
    	return isValidGroup;

    }
    private int getGroupUsers(String groupName)
    {
    	VWSLog.dbg("-------Inside getGroupUsers method-----");
        Vector users = new Vector();
        String param = "";
        if(database.getEngineName().equalsIgnoreCase("Oracle"))
        	param = "4" + Util.SepChar + groupName + Util.SepChar;
        else
        	param = "4" + Util.SepChar + groupName ;
        DoDBAction.get(database, "GroupToUser_Get", param, users);
        VWSLog.dbg("--------End of getGroupUsers method--------");
        return users.size();
    }
    
    private void removeUserFromList(List users, int userId)
    {
        Iterator iterator = users.iterator();
        while (iterator.hasNext())
        {
            Creator user = (Creator) iterator.next();
            if (user.getId() == userId)
            {
                iterator.remove();
                break;
            }
        }
    }
    private void removeUsers(List users)
    {
        String param = "";
        Vector ret = new Vector();
        Iterator iterator = users.iterator();
        while (iterator.hasNext())
        {
            Creator user = (Creator) iterator.next();
            int userId = user.getId();
            	param = "0" + Util.SepChar + userId;
            DoDBAction.get(database, "User_Del", param, ret);
        }
    }
    private void syncGroups() 
    {    	
    	VWSLog.dbg("Inside syncGroups method and gIgnoreSync flag value:::: " + gIgnoreSync);
    	/*VWSLog.add("Inside syncGroups method gIgnoreSync:::::" + gIgnoreSync);
    	if((gIgnoreSync)) {
	    	String ldapDirectory = VWSPreferences.getTree();
			VWSLog.add("ldapDirectory ::::::::"+ldapDirectory);
			String[] dirList = ldapDirectory.split(",");
			for(int s=0; s< dirList.length; s++){
				if (dirList[s].equalsIgnoreCase(directory.getDirName())) {
					if (s==(dirList.length-1)) {
						 boolean domainUsersExist = getUsersForDomain(directory.getDirName());
						 if (domainUsersExist==false) {
							 gIgnoreSync = false;
						 }
					}
				}
			}
    	 }*/
		
        if(gIgnoreSync) return;
    	// Changed when doing LDAP
        Vector groups = new Vector();
        if(isUserGroupXMLGenerated)
        	groups = getGroupsFromXML();
        else
    		groups = directory.getGroups();
        VWSLog.dbg("After getting groups inside syncGroups method::::" + groups);
        if(groups == null || groups.size() == 0)
        {
            gIgnoreSync = true;
            return;
        }
        VWSLog.add(database.getName() + ": Synchronized " 
                               + groups.size() + " Groups to Security Server ");        
        Hashtable userTable = getUsersTable(); 
        String param = "";
        Vector ret = new Vector();        
        List groupDataObjects = getGroups();
        Iterator iterator = groups.iterator();        
        while (iterator.hasNext())
        {
            try{
        	String groupName = (String) iterator.next();
            int groupId = 0;
            String schemeType = VWSPreferences.getSSType();
    		if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
    			param = "1" + Util.SepChar + Util.getOneSpace(groupName) + Util.SepChar + directory.getDirName() + Util.SepChar;
    		} else {
    			param = "1" + Util.SepChar + Util.getOneSpace(groupName) + Util.SepChar + "";
    		}
            DoDBAction.get(database, "Group_Get", param, ret);
            if(ret.size() > 0)
            {
                groupId = Util.to_Number( (String) ret.get(0));
                ret.removeAllElements();
            }
            removeGroupFromList(groupDataObjects, groupId);
            syncUsersInGroup(groupName, groupId, userTable, null);
            groupName = "";
            }catch(Exception ex){
        	 //VWSLog.add("Error while sync groups " + ex.getMessage() + " ::: " + groupName);
            }
        }
        //removeGroups(groupDataObjects);
    }
    public void syncLinks() 
    {
    	VWSLog.add("Synchronizing users to groups...");
    	VWSLog.dbg("isAdminWiseCall:" + isAdminWiseCall);
        List groups = getGroupsByDomain();
        Hashtable userTable = getUsersTable();
        VWSLog.dbg("inside sinklink all Groups " + groups);
        VWSLog.dbg("inside sinklink all users" + userTable);
        Iterator iterator = groups.iterator();
        while (iterator.hasNext())
        {
            try{    
            Group group = (Group) iterator.next();
            VWSLog.dbg("In syncLinks :::"+group.getName()+"GroupId ::"+group.getId()+"usertable :::"+userTable);
            syncUsersInGroup(group.getName(), group.getId(), userTable, null);
            }catch(Exception ex){
            }
        }   
        VWSLog.dbg("End of syncLinks method...");
    }
    private List getGroups()
    {
		/**CV2019 code merges from CV10.2 line**/
    	//boolean LDAPsingleToMultiple = VWSPreferences.getLDAPUpgradeToMultiDomain();
    	String[] dirList = null;
 	    String ldapDirectory = VWSPreferences.getTree();
 		dirList = ldapDirectory.split(",");
 		
    	VWSLog.dbg("Calling getGroups from directorySync....");
        List groupsList = new LinkedList();
        Vector ret = new Vector();
        String param = "";
        String schemeType = VWSPreferences.getSSType();
        if(database.getEngineName().equalsIgnoreCase("Oracle")){           
			/**CV10.2 - Condition added for multidomain****/
        	if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
        		if (dirList.length > 1) {
					//if (LDAPsingleToMultiple) {
						if (directory.getDirName().equalsIgnoreCase(dirList[0])) {
							param = "2"+Util.SepChar + "~" +Util.SepChar;
						} else {
							param = "2"+Util.SepChar + directory.getDirName()+Util.SepChar;
						}
					/*} else {
						param = "2"+Util.SepChar + directory.getDirName() +Util.SepChar;
					}*/
        		} else {
        			param = "2"+Util.SepChar + "~" +Util.SepChar;
        		}
        	} else {
        		param = "2"+Util.SepChar+"~";	
        	}
        } else {
        	if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
        		if (dirList.length > 1) {
					//if (LDAPsingleToMultiple) {
						if (directory.getDirName().equalsIgnoreCase(dirList[0])) {
							param = "2"+Util.SepChar + "~" +Util.SepChar;
						} else {
							param = "2"+Util.SepChar + directory.getDirName()+Util.SepChar;
						}
					/*} else {
						param = "2"+Util.SepChar + directory.getDirName() +Util.SepChar;
					}*/
        		} else {
        			param = "2"+Util.SepChar + "~" +Util.SepChar;
        		}
        	}else{
            	param = "2"+Util.SepChar+"~";

        	}
        }
        DoDBAction.get(database, "Group_Get", param, ret);
        for (int i = 0; i < ret.size(); i++)
        {
            StringTokenizer st = new StringTokenizer
                        ((String) ret.get(i), Util.SepChar);
            int groupId= Util.to_Number(st.nextToken());
            String groupName = st.nextToken();
            Group group = new Group(groupId);            
            group.setName(groupName);            
            groupsList.add(group);
        }
        VWSLog.dbg("End of getGroups from directorySync...." + groupsList);
        return groupsList;
    }

	/**CV2019 code merges from CV10.2 line**/
    private List getGroupsByDomain()
    {
    	VWSLog.dbg("Calling getGroups from directorySync....");  
    	List groupsList = new LinkedList();
    	try {
	    	boolean LDAPsingleToMultiple = VWSPreferences.getLDAPUpgradeToMultiDomain();
	    	String[] dirList = null;
	 	    String ldapDirectory = VWSPreferences.getTree();
	 		dirList = ldapDirectory.split(",");
	 		      
	        Vector ret = new Vector();
	        String param = "";
	        String schemeType = VWSPreferences.getSSType();
	        if(database.getEngineName().equalsIgnoreCase("Oracle")){           
	        	if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
	        		if (dirList.length > 1) {
						if (LDAPsingleToMultiple) {
							if (directory.getDirName().equalsIgnoreCase(dirList[0])) {
									param = "5"+Util.SepChar + "~" +Util.SepChar;
							} else {
								param = "5"+Util.SepChar + directory.getDirName()+Util.SepChar;
							}
						} else {
							param = "5"+Util.SepChar + directory.getDirName() +Util.SepChar;
						}
	        		} else {
	        			param = "5"+Util.SepChar + "~" +Util.SepChar;
	        		}
	        	}else{
	        		param = "5"+Util.SepChar+"~";	
	        	}
	        }
	        else{
	        	if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
	        		if (dirList.length > 1) {
						if (LDAPsingleToMultiple) {
							if (directory.getDirName().equalsIgnoreCase(dirList[0])) {
									param = "5"+Util.SepChar + "~" +Util.SepChar;
							} else {
								param = "5"+Util.SepChar + directory.getDirName()+Util.SepChar;
							}
						} else {
							param = "5"+Util.SepChar + directory.getDirName() +Util.SepChar;
						}
	        		} else {
	        			param = "5"+Util.SepChar + "~" +Util.SepChar;
	        		}
	        		//	param = "2"+Util.SepChar + directory.getDirName() ;
	        		// param = "2"+Util.SepChar + directory.getDirName()+Util.SepChar;
	        	}else{
	            	param = "5"+Util.SepChar+"~";
	
	        	}
	        }
	        VWSLog.dbg("getGroupsByDomain param...." + param);
	        DoDBAction.get(database, "Group_Get", param, ret);
	        for (int i = 0; i < ret.size(); i++)
	        {
	            StringTokenizer st = new StringTokenizer
	                        ((String) ret.get(i), Util.SepChar);
	            int groupId= Util.to_Number(st.nextToken());
	            String groupName = st.nextToken();
	            Group group = new Group(groupId);            
	            group.setName(groupName);            
	            groupsList.add(group);
	        }	        
    	} catch (Exception e) {
    		VWSLog.dbg("Exception in getGroupsByDomain : " + e.getMessage());
    	}
    	//VWSLog.add("End of getGroups from directorySync...." + groupsList);
        return groupsList;
    }
    private void removeGroupFromList(List groupDataObjects, int groupId)
    {
        Iterator iterator = groupDataObjects.iterator();
        while (iterator.hasNext())
        {
            Group group = (Group) iterator.next();

            if (group.getId() == groupId)
            {
                iterator.remove();
                break;
            }
        }
    }
    private void removeGroups(List groupDataObjects, String schemeType)
    {
        String param = "";
        Vector ret = new Vector();
        Group group = null;
        String groupName = null;
        Iterator iterator = groupDataObjects.iterator();
        while (iterator.hasNext())
        {
            group = (Group) iterator.next();
            //CV10.2 - Do not remove registered groups from GroupTable. As discussed with Raj on 03 Apr 2019
            //Added by Srikanth
            if(isRegisteredGroup(groupName)==false) {
            	int groupId = group.getId();
            	param = groupId+Util.SepChar;
            	DoDBAction.get(database, "Group_Del", param, ret);
            }
        }
    }
    /**This syncUsersInGroup method is renamed bz lot of code changes are there in 10.2 line syncUsersInGroup method**/
    private void syncUsersInGroup_MainLine(String groupName, int groupId, Hashtable userTable)
    {
    	String param = "";
    	Vector ret = new Vector();
    	List users = getUsersInGroup(groupName);//Gets users of a group from directory.
    	//Added for License changes, Madhavan.B
    	boolean isHosting = false;
    	try{
    		isHosting = CVPreferences.getHosting();
    		//VWSLog.add("isHosting : "+isHosting);
    	}catch (Exception e) {
    		isHosting = false;
    	}
    	int adminUsersCount=getAdminGrpCount();
    	VWSLog.dbg("adminUsersCount ::::"+adminUsersCount);
    	int namedUsersCount = getNamedGrpCount();
    	VWSLog.dbg("namedUsersCount ::::"+namedUsersCount);
    	int nameOnlineUserCount=getNamedOnlineGrpCount();
    	VWSLog.dbg("nameOnlineUserCount ::::"+nameOnlineUserCount);
    	int concurrentUsersCount=getConcurrentGrpCount();
    	VWSLog.dbg("concurrentUsersCount ::::"+concurrentUsersCount);
    	int concurrentOnlineUsersCount=getConcurrentOnlineGrpCount();
    	VWSLog.dbg("concurrentOnlineUsersCount ::::"+concurrentOnlineUsersCount);
    	int professionalnamedUsersCount=getProfessionalNamedGrpCount();
    	VWSLog.dbg("professionalnamedUsersCount ::::"+professionalnamedUsersCount);
    	int profConcrUsersCount=getProfessConcrGrpCount();
    	VWSLog.dbg("profConcrUsersCount ::::"+profConcrUsersCount);
    	int publicWAUsersCount= getPublicWAGrpCount();
    	VWSLog.dbg("publicWAUsersCount ::::"+publicWAUsersCount);

    	if (users == null)
    	{
    		//VWSLog.dbg (room + ": No Users in Group: " + groupName);
    		return;
    	}
    	List groupToUserDataObjects = getUsersInGroup(groupId); //Gets GroupToUser relationship from table.
    	Iterator iterator = users.iterator();        
    	while (iterator.hasNext())
    	{            
    		String userName = (String) iterator.next();
    		int userId = 0;
    		if (userTable.containsKey(userName.toLowerCase())){
    			try{    
    				userId = Util.to_Number(userTable.get(userName.toLowerCase()).toString());/*getUserId(userName);*/
    			}catch(Exception ex){
    				userId = 0;
    			}
    		}
    		removeGroupToUserFromList(groupToUserDataObjects, userId);
    		//Added for License changes Madhavan.B
    		if (userId == 0)  continue;
    		VWSLog.dbg("getAdminGroup in syncgroupuser :::"+getAdminGroup());
    		VWSLog.dbg("getNamedGroup in syncgroupuser :::"+getNamedGroup());
    		VWSLog.dbg("getConcurrentGroup in syncgroupuser :::"+getConcurrentGroup());
    		VWSLog.dbg("getConcurrentOnlineGroup in syncgroupuser :::"+getConcurrentOnlineGroup());
    		VWSLog.dbg("getProfessionalNamedGroup in syncgroupuser :::"+getProfessionalNamedGroup());
    		VWSLog.dbg("getProfessionalConcrGroup in syncgroupuser :::"+getProfessionalConcrGroup());
    		VWSLog.dbg("getProfessionalConcrGroup in syncgroupuser :::"+getPublicWAGroup());
    		if(groupName.equalsIgnoreCase(getAdminGroup()))
    		{
    			VWSLog.dbg("adminUsersCount before if :::::::::"+adminUsersCount);
    			if(adminUsersCount!=0){
    				param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    				DoDBAction.get(database, "GroupToUser_Get", param, ret);
    				adminUsersCount--;
    			}
    			VWSLog.dbg("adminUsersCount after if :::::::::"+adminUsersCount);
    		}
    		
    		else if(groupName.equalsIgnoreCase(getNamedGroup()))
    		{
    			if(!isHosting){
    				if(namedUsersCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					namedUsersCount--;
    				}
    			}
    			else
    			{
    				if(namedUsersCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					namedUsersCount--;
    				}
    			}
    		}
    		else if(groupName.equalsIgnoreCase(getNamedOnlineGroup()))
    		{
    			if(!isHosting){
    				if(nameOnlineUserCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					nameOnlineUserCount--;
    				}
    			}
    			else
    			{
    				if(nameOnlineUserCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					nameOnlineUserCount--;
    				}
    			}
    		}//Commented date Date :-3/8/2017 BY :- madhavan
    		/*else if(groupName.equalsIgnoreCase(getConcurrentGroup()))
    		{
    			if(!isHosting){
    				if(concurrentUsersCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					concurrentUsersCount--;
    				}
    			}
    			else
    			{
    				if(concurrentUsersCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					concurrentUsersCount--;
    				}
    			}
    		}
    		else if(groupName.equalsIgnoreCase(getConcurrentOnlineGroup()))
    		{
    			if(!isHosting){
    				if(concurrentOnlineUsersCount!=0){
    					VWSLog.dbg("concurrentOnlineUsersCount GrouptoUser"+concurrentOnlineUsersCount);
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					concurrentOnlineUsersCount--;
    				}
    			}
    			else
    			{
    				if(concurrentOnlineUsersCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					concurrentOnlineUsersCount--;
    				}
    			}
    		}*/
    		else if(groupName.equalsIgnoreCase(getProfessionalNamedGroup()))
    		{
    			if(!isHosting){
    				if(professionalnamedUsersCount!=0){
    					VWSLog.dbg("professionalnamedUsersCount GrouptoUser"+professionalnamedUsersCount);
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					professionalnamedUsersCount--;
    				}
    			}
    			else
    			{
    				if(professionalnamedUsersCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					professionalnamedUsersCount--;
    				}
    			}
    		}//Commented date Date :-3/8/2017 BY :- madhavan
    		/*else if(groupName.equalsIgnoreCase(getProfessionalConcrGroup()))
    		{
    			if(!isHosting){
    				if(profConcrUsersCount!=0){
    					VWSLog.dbg("professionalnamedUsersCount GrouptoUser"+professionalnamedUsersCount);
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					profConcrUsersCount--;
    				}
    			}
    			else
    			{
    				if(profConcrUsersCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					profConcrUsersCount--;
    				}
    			}
    		}*/
    		else if(groupName.equalsIgnoreCase(getPublicWAGroup()))
    		{
    			if(!isHosting){
    				if(publicWAUsersCount!=0){
    					VWSLog.dbg("professionalnamedUsersCount GrouptoUser"+professionalnamedUsersCount);
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					publicWAUsersCount--;
    				}
    			}
    			else
    			{
    				if(publicWAUsersCount!=0){
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
    					publicWAUsersCount--;
    				}
    			}
    		}
    		else{
    			VWSLog.dbg("Inside else of sync users group :::");
    			param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    			DoDBAction.get(database, "GroupToUser_Get", param, ret);
    		}
    	}
    	VWSLog.dbg("Outside sycn users group:::");
    	removeGroupToUsers(groupToUserDataObjects);
    }
    
    /**Existing MainLine syncUsersInGroup method renamed to syncUsersInGroup_MainLine **/
    /**CV2019 Merges from CV10.2. Method updated on 22/08/2019 by Vanitha.S  **/
    private void syncUsersInGroup(String groupName, int groupId, Hashtable userTable, String groupCategory) {
		VWSLog.add("Synchronizing users to groups started for "+groupName);
		String param = null;
		Vector ret = new Vector();	
		int groupLicenseCount = 0;
		int licensedUsersCount = 0;
		
		VWSLog.dbg("userTable inside syncUsersInGroup: " + userTable);
		//boolean LDAPsingleToMultiple = VWSPreferences.getLDAPUpgradeToMultiDomain();
		String schemeType = VWSPreferences.getSSType();
		String[] dirList = null;
		String ldapDirectory = VWSPreferences.getTree();
		dirList = ldapDirectory.split(",");
		String groupNameForSearch = null;
		
    	VWSLog.dbg("Before calling usersIngroup wher groupName is: "+groupName);    	
    	List users = getUsersInGroup(groupName);//Gets users of a group from directory.
    	VWSLog.dbg("users from syncUsersInGroup for: "+groupName+" directory.getDirname(): " + directory.getDirName() + " -- groupid: " + groupId + "  -- users: "+users);
    	
    	boolean isHosting = false;
	    try{
	    	isHosting = CVPreferences.getHosting();
	    }catch (Exception e) {
	    	isHosting = false;
	    }
	    String namedGrp = null, namedOnlineGrp = null, professionalNamedGrp = null, aipGroup = null;
	    if (!isHosting) {	    	
	    	namedGrp = getNamedGroup();
	    	namedOnlineGrp = getNamedOnlineGroup();
	    	professionalNamedGrp = getProfessionalNamedGroup();
	    	aipGroup = getBatchInputGroup();
	    } else {
	    	namedGrp = VWSPreferences.getRoomLevelNamed(room);
	    	namedOnlineGrp = VWSPreferences.getRoomLevelNamedOnline(room);
	    	professionalNamedGrp = VWSPreferences.getRoomLevelProfessionalNamed(room);
	    	aipGroup = VWSPreferences.getRoomLevelBatchInputAdministrator(room);
	    }
	    boolean isLicenseRestrictedGroup = false;
	    VWSLog.add("groupCategory inside syncUsersInGroup method: "+groupCategory);
	    /**Code optimized by Vanitha.S on 22/08/2019**/
	    
    	//AM-Admin Group/SA-Sub Administrators Group/ND-Named Desktop Group/NO-Named Online Group/PN-Professional Named Group
    	if ((groupCategory.equalsIgnoreCase("AM") && groupName.equalsIgnoreCase(getAdminGroup())) ||
    			(groupCategory.equalsIgnoreCase("SA") && groupName.equalsIgnoreCase(getSubAdminGroup())) ||
				(groupCategory.equalsIgnoreCase("ND") && groupName.equalsIgnoreCase(namedGrp)) ||
				(groupCategory.equalsIgnoreCase("NO") && groupName.equalsIgnoreCase(namedOnlineGrp)) ||
				(groupCategory.equalsIgnoreCase("PN") && groupName.equalsIgnoreCase(professionalNamedGrp)) || 
				(groupCategory.equalsIgnoreCase("BI") && groupName.equalsIgnoreCase(aipGroup))) {
    		isLicenseRestrictedGroup = true;
    		groupNameForSearch = groupName + "@" + "~";
			
			param = "4" + Util.SepChar + groupNameForSearch + Util.SepChar;
			DoDBAction.get(database, "GroupToUser_Get", param, ret);
			if (groupCategory.equalsIgnoreCase("AM") && groupName.equalsIgnoreCase(getAdminGroup())) {
				VWSLog.dbg("getAdminGroup(): " + getAdminGroup());
				groupLicenseCount = getAdminGrpCount() - ret.size();
			} else if (groupCategory.equalsIgnoreCase("SA") && groupName.equalsIgnoreCase(getSubAdminGroup())) {
				VWSLog.dbg("getSubAdminGroup(): " + getSubAdminGroup());
				groupLicenseCount = getSubAdminGrpCount() - ret.size();
			} else if (groupCategory.equalsIgnoreCase("ND") && groupName.equalsIgnoreCase(namedGrp)) {
				VWSLog.dbg("getNamedGrpCount(): " + getNamedGrpCount());
				groupLicenseCount = getNamedGrpCount() - ret.size();
			} else if (groupCategory.equalsIgnoreCase("NO") && groupName.equalsIgnoreCase(namedOnlineGrp)) {
				VWSLog.dbg("getNamedOnlineGrpCount(): " + getNamedOnlineGrpCount());
				groupLicenseCount = getNamedOnlineGrpCount() - ret.size();
			} else if (groupCategory.equalsIgnoreCase("PN") && groupName.equalsIgnoreCase(professionalNamedGrp)) {
				VWSLog.dbg("getProfessionalNamedGrpCount(): " + getProfessionalNamedGrpCount());
				groupLicenseCount = getProfessionalNamedGrpCount() - ret.size();
			} else if (groupCategory.equalsIgnoreCase("BI") && groupName.equalsIgnoreCase(aipGroup)) {
				VWSLog.dbg("aipGroup: " + aipGroup);
				groupLicenseCount = getBatchInputGrpCount() - ret.size();
			}
			ret.removeAllElements();
			VWSLog.dbg("ret val from grouptouser_get param 4 is " + groupLicenseCount);
			licensedUsersCount = groupLicenseCount;
    	}
	   
	    VWSLog.dbg("Available license count for this group :" + licensedUsersCount);
		List groupToUserDataObjects = getUsersInGroup(groupId); //Gets GroupToUser relationship from table.
    	VWSLog.dbg("Available users list in GroupToUsers Table: " + groupName + "  :::  "+ groupToUserDataObjects.size());
    	VWSLog.dbg("Group Users List before adding to DB : "+users);
    	if (users != null) {
	    	Iterator iterator = users.iterator();
	    	
			VWSLog.dbg(" inside if condition after checking groupToUserDataObjects");
	    	while (iterator.hasNext()) {            
	    		String userNameWithDN="";
	    		ret.removeAllElements();
	    		try {
	    			String userName = (String) iterator.next();
	    			VWSLog.dbg("userName before grouptouser add is :"+userName);
	
	    			int userId = 0;
	    			//567
					if (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
						if (userName.length() > 0) {
	    					String[] fullUserInfo=userName.split(Util.SepChar);
	    					if (dirList.length > 1) {
	    						//if (LDAPsingleToMultiple) {
	    							if (directory.getDirName().equalsIgnoreCase(dirList[0])) 
	    								userNameWithDN=fullUserInfo[0].substring(0,fullUserInfo[0].indexOf("@"));
	    							else 
	    								userNameWithDN=fullUserInfo[0];
	    						/*} else {
	    							userNameWithDN=fullUserInfo[0];
	    						}*/
	    					} else {
	    						userNameWithDN=fullUserInfo[0];
	    					}
	    					VWSLog.dbg("userNameWithDN before grouptouser add is :"+userNameWithDN);
	    				}
	    				VWSLog.add("User Name inside Group To User Sync :"+userNameWithDN);
	    				if (userTable.containsKey(userNameWithDN.toLowerCase())){
	    					VWSLog.dbg("Inside if condition of userdn from syncUsersInGroup");
	    					try{    
	    						VWSLog.dbg("Before getting userId is :::"+userId);
	    						userId = Util.to_Number(userTable.get(userNameWithDN.toLowerCase()).toString());
	    						VWSLog.dbg("After getting userId is :::"+userId);
	    					}catch(Exception ex){
	    						VWSLog.dbg("EXCEPTION WHILE fetching user id 1 :::: "+ex.getMessage());
	    						userId = 0;
	    					}
	    				} else {// else condition included by Srikanth on 12 Feb 2019
	    					VWSLog.dbg("Inside else condition of userdnfrom syncUsersInGroup");
	    					if (userTable.containsKey(userName.toLowerCase())){
	    						try{    
	    							userId = Util.to_Number(userTable.get(userName.toLowerCase()).toString());
	    						}catch(Exception ex){
	    							VWSLog.dbg("EXCEPTION WHILE fetching user id 2 :::: "+ex.getMessage());
	    							userId = 0;
	    						}
	    					}
	    				}
	    				//below else condition included by Srikanth on 12 Feb 2019
					} else {
						VWSLog.dbg("Users available in UsersTable:"+userTable);
						VWSLog.dbg("User name before adding to GroupToUsers table :"+userName);
						VWSLog.dbg("Check UsersTable contains UserName '"+userName.toLowerCase()+"' :"+userTable.containsKey(userName.toLowerCase()));
	    				if (userTable.containsKey(userName.toLowerCase())){
	    					try{    
	    						userId = Util.to_Number(userTable.get(userName.toLowerCase()).toString());
	    					}catch(Exception ex){
	    						VWSLog.dbg("EXCEPTION WHILE fetching user id 3 :::: "+ex.getMessage());
	    						userId = 0;
	    					}
	    				}
	    			}
	    			VWSLog.dbg("userId before adding user in GroupToUser table::::::"+userId);
	    			if (userId > 0) 
	    				removeGroupToUserFromList(groupToUserDataObjects, userId);    			
	    			if (userId == 0)  continue;
	    			/*if (groupName.equalsIgnoreCase(getAdminGroup()) ||
	    					groupName.equalsIgnoreCase(namedGrp) ||
	    					groupName.equalsIgnoreCase(namedOnlineGrp) ||
	    					groupName.equalsIgnoreCase(professionalNamedGrp)) {*/
	    			if (isLicenseRestrictedGroup) {
	    				VWSLog.dbg("Inside registered group sync....."+userId);
	    				VWSLog.dbg("licensedUsersCount......"+licensedUsersCount);
						if (licensedUsersCount != 0) {
    						VWSLog.dbg("Before calling GrouptoUser_Get for Admin/Named/Named Online/Prpfessional Named Group Groupid: "+groupId+" UserId: "+userId);
    						param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    						DoDBAction.get(database, "GroupToUser_Get", param, ret);
    						licensedUsersCount--;
    					} else {
    						VWSLog.dbg("inside licence count 0 ....");
    					}
	    			} else {
	    				VWSLog.dbg("Inside Non-Registered group sync....."+userId);
	    				VWSLog.dbg("Before calling GrouptoUser_Get for Concurrent/Concurrent Online/Professional concurrent/Non-Registered Group Groupid: "+groupId+" UserId: "+userId);
    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
    					VWSLog.dbgCheck("Param before calling GroupToUser_Get :"+param);
    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
	    			}
	    			/***Following codes are commented for code optimization purpose . Commented by Vanitha on 23/08/2019**/
	    			/*//Added for License changes Madhavan.B
	    			if(groupName.equalsIgnoreCase(getNamedGroup()))
	    			{
	    				VWSLog.dbg("namedUsersCount :"+namedUsersCount);	    				
	    				if(!isHosting){
	    					if(namedUsersCount!=0){
	    						VWSLog.dbg("Before calling GrouptoUser_Get for Named Group Groupid: "+groupId+" UserId: "+userId);
	    						param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
	    						DoDBAction.get(database, "GroupToUser_Get", param, ret);
	    						namedUsersCount--;
	    					}
	    				} else {
	    					if(namedUsersCount!=0){
	    						param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
	    						DoDBAction.get(database, "GroupToUser_Get", param, ret);
	    						namedUsersCount--;
	    					}
	    				}
	    			} else if(groupName.equalsIgnoreCase(getNamedOnlineGroup())) {
	        			if(!isHosting){
	        				if(nameOnlineUserCount!=0){
	        					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
	        					DoDBAction.get(database, "GroupToUser_Get", param, ret);
	        					nameOnlineUserCount--;
	        				}
	        			}
	        			else
	        			{
	        				if(nameOnlineUserCount!=0){
	        					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
	        					DoDBAction.get(database, "GroupToUser_Get", param, ret);
	        					nameOnlineUserCount--;
	        				}
	        			}
	        		}
	    			else if(groupName.equalsIgnoreCase(getProfessionalNamedGroup())){
	    				if(!isHosting){
	    					if(professionalnamedUsersCount!=0){
	    						VWSLog.dbg("Before calling GrouptoUser_Get for Professional Named Group Groupid: "+groupId+" UserId: "+userId);
	    						param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
	    						DoDBAction.get(database, "GroupToUser_Get", param, ret);
	    						professionalnamedUsersCount--;
	    					}
	    				}
	    				else
	    				{
	    					if(professionalnamedUsersCount!=0){
	    						param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;    						
	    						DoDBAction.get(database, "GroupToUser_Get", param, ret);
	    						professionalnamedUsersCount--;
	    					}
	    				}
	    			}
	    			else{
	    					VWSLog.dbg("Before calling GrouptoUser_Get for non registered Group Groupid: "+groupId+" UserId: "+userId);
	    					param = "1" + Util.SepChar + groupId +	Util.SepChar +userId;
	    					VWSLog.dbgCheck("Param before calling GroupToUser_Get :"+param);
	    					DoDBAction.get(database, "GroupToUser_Get", param, ret);
	    			}*/
	    		}catch(Exception e){
	    			VWSLog.dbg("Exception in synching users to groups : "+e.getMessage());
	    		}
	    	}
    	}
    	VWSLog.dbg("Before calling removeGroupToUsers method from syncUsersInGroup: " + groupName + "groupToUserDataObjects.size(): " + groupToUserDataObjects.size());
    	removeGroupToUsers(groupToUserDataObjects);
    	VWSLog.dbg("After calling removeGroupToUsers method from syncUsersInGroup: " + groupName);
    }
    
    /**
     * CV10.2 - Method to get group users list 
     * @param groupId
     * @return
     */
    private List getUsersInGroup(int groupId)
    {
    	VWSLog.dbg("getUsersInGroup for groupId: " + groupId);
    	List groupToUsersList = new LinkedList();
    	try {
	    	Vector ret = new Vector();
	    	String param = "";
	    	//String schemeType = VWSPreferences.getSSType();
	    	//CV10.1 issue fix for multiple domain group user sync with upnsuffix  issue fix.
	    	String schemeType = VWSPreferences.getSSType();
	    	VWSLog.dbg("schemeType in usersingroup:::"+schemeType);
	    	/*if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)){
	    		param = "2" + Util.SepChar + groupId+Util.SepChar + directory.getDirName() + Util.SepChar ;
	    	}
	    	else{*/
	    		param = "2" + Util.SepChar + groupId+Util.SepChar+"";
	    	//}
	    	DoDBAction.get(database, "GroupToUser_Get", param, ret);   
	    	//if condition added when doing LDAP
	    	if(ret.size()!=0){
	    		for (int i = 0; i < ret.size(); i++)
	    		{
	    			StringTokenizer st = new StringTokenizer
	    					((String) ret.get(i), Util.SepChar);
	    			int id = Util.to_Number(st.nextToken());
	    			int userId = Util.to_Number(st.nextToken());
	    		
	    			GroupUser gs = new GroupUser(id);            
	    			gs.setGroupId(groupId);
	    			gs.setUserId(userId);
	    			groupToUsersList.add(gs);
	    		}
	    	}
    	} catch (Exception e) {
    		VWSLog.dbg("Exception in getUsersInGroup " + e.getMessage());  
    	}
    	VWSLog.dbg("Users List in getUsersInGroup: " + groupToUsersList);    	
    	return groupToUsersList;
    }
    
    /**
     * CV10.2 - Method to remove synced users from list
     * @param groupToUserDataObjects
     * @param userId
     */
    private void removeGroupToUserFromList(List groupToUserDataObjects, int userId)
    {
    	VWSLog.dbg("Inside removeGroupTouserfrom list :::::"+groupToUserDataObjects);
    	Iterator iterator = groupToUserDataObjects.iterator();
        while (iterator.hasNext())
        {
            GroupUser gs = (GroupUser) iterator.next();
            if (gs.getUserId() == userId)
            {
                iterator.remove();
                break;
            }
        }
    }
    
    /**
     * CV10.2 - Method to remove synced users from table
     * @param groupToUserDataObjects
     */
    private void removeGroupToUsers(List groupToUserDataObjects)
    {
        Vector ret = new Vector();
        String param = "";
        Iterator iterator = groupToUserDataObjects.iterator();
        while (iterator.hasNext())
        {
        	GroupUser gs = (GroupUser) iterator.next();
        	VWSLog.dbg("Removing user userid: " + gs.getUserId());
        	param = gs.getId()+Util.SepChar;
        	VWSLog.dbg("param in GroupToUser_Del :"+param);
        	VWSLog.dbgCheck("param in GroupToUser_Del :"+param);
        	DoDBAction.get(database, "GroupToUser_Del", param, ret);
        }
    }
    
    /**
     * CV10.2 - Method to add user in table
     * @param userName
     * @return
     */
    private int addUser(String userName)
    {
        int userId = 0;
        Vector ret = new Vector();
        String param = "";
        	param = Util.getOneSpace(userName) + Util.SepChar;
        DoDBAction.get(database, "User_Add", param, ret);
        if(ret.size() > 0)
            return Util.to_Number( (String) ret.get(0));
        else
        return 0;
    }
    
    /**
     * CV10.2 - Method to add group in table
     * @param groupName
     * @return
     */
    private int addGroup(String groupName)
    {
        Vector ret = new Vector();
        String param = "";
        	param = Util.getOneSpace(groupName) + Util.SepChar;
        DoDBAction.get(database, "Group_Add", param, ret);
        if(ret.size() > 0)
            return Util.to_Number( (String) ret.get(0));
        else
            return 0;
    }
    
    /**CV2019 code merges from CV10.2 line**/
    private void addGroupToUser(int gid, int sid)
    {
        Vector ret = new Vector();
        String param = "";
        	param = gid + Util.SepChar+sid;
        DoDBAction.get(database, "GroupToUser_Add", param, ret); 
    }
    private int getUserId(String userName)
    {
        Vector ret = new Vector();
        String param = "";
        	param = "0" + Util.SepChar + Util.getOneSpace(userName);
        DoDBAction.get(database, "User_Get", param, ret);
        if (ret.size() > 0)
            return Util.to_Number( (String) ret.get(0));
        else
            return 0;
    }
    private void getUsrsGrpsFromSecurityServer()
    {
    	String useUsrGrpXMLFile = VWSPreferences.getUseUsrGrpXMLFile().trim();
    	if(useUsrGrpXMLFile!=null && !useUsrGrpXMLFile.equals("")){
    		if(useUsrGrpXMLFile.equalsIgnoreCase("true"))
    			isUserGroupXMLGenerated = true;
    		else if(useUsrGrpXMLFile.equalsIgnoreCase("false"))
    			isUserGroupXMLGenerated = false;
    		else
    			isUserGroupXMLGenerated = false;
    	}else
    		isUserGroupXMLGenerated = false;
    	VWSLog.dbg("isUserGroupXMLGenerated status::::"+ isUserGroupXMLGenerated);
    	if(isUserGroupXMLGenerated){	
    		VWSLog.dbg("isUserGroupXMLGenerated if condition");
	    	String fileName =  VWCUtil.getHome()+Util.pathSep+"lib"+Util.pathSep+Constants.userGroupXMLFile;
	        // getting users and dumping in xml
	        Vector users = new Vector();
	        VWSLog.dbg("getUsrsGrpsFromSecurityServer   before getting directory.getusers()");
	       	users = directory.getUsers(false);
	       	VWSLog.dbg("directory.getUsers " + users);
	        Vector userGroupList=new Vector();
	        userGroupList.add("<?xml version='1.0' encoding='UTF-16'?>");
	        userGroupList.add("<UserGroupList>");
	        userGroupList.add("<Users>");
	        userGroupList.add("<UserCount>"+users.size()+"</UserCount>");
	        for(int i = 0; i<users.size(); i++){
	        	userGroupList.add("<User"+i+">"+VWUtil.fixXMLString(users.get(i).toString())+"</User"+i+">");
	        }
	        userGroupList.add("</Users>");
	        // getting groups and dumping in xml
	        Vector groups = new Vector();
	        VWSLog.dbg("getUsrsGrpsFromSecurityServer   before getting directory.getGroups()");
			groups = directory.getGroups();
			VWSLog.dbg("directory.getGroups " + groups);
	        userGroupList.add("<Groups>");
	        userGroupList.add("<GroupCount>"+groups.size()+"</GroupCount>");
	        for(int i = 0; i<groups.size(); i++){
	        	userGroupList.add("<Group"+i+">"+VWUtil.fixXMLString(groups.get(i).toString())+"</Group"+i+">");
	        }
	        userGroupList.add("</Groups>");
	        //getting member of groups and dumping in xml
	        Iterator iterator = groups.iterator();
	        userGroupList.add("<UsersInGroup>");
	        for(int i = 0; i<groups.size(); i++)
	        {
	        	String groupName = groups.get(i).toString();
	        	userGroupList.add("<GroupName Name='"+VWUtil.fixXMLString(groupName)+"'>");
	            List usersInGroup = directory.getUsersInGroup(groupName);
	        	userGroupList.add("<MemberCount Name='"+VWUtil.fixXMLString(groupName)+"'>"+usersInGroup.size()+"</MemberCount>");
	            for(int j = 0; j<usersInGroup.size(); j++){
	            	userGroupList.add("<Member"+j+" Name='"+VWUtil.fixXMLString(groupName)+"'>"+VWUtil.fixXMLString(usersInGroup.get(j).toString())+"</Member"+j+">");
	            }
	            userGroupList.add("</GroupName>");
	        }
	        userGroupList.add("</UsersInGroup>");
	        userGroupList.add("</UserGroupList>");
	        File userFile = new File(Util.checkPath(fileName));
	    	
	    	if(userFile.exists())
	    		userFile.delete();
	    	String xmlFile = Util.checkPath(VWCUtil.getHome()+ Util.pathSep + "lib" + Util.pathSep + Constants.userGroupXMLFile);
		if (String.valueOf(File.separatorChar).equals("/") && !xmlFile.startsWith("/")){
		    xmlFile = "/" + xmlFile;
		}
		//VWSLog.dbg("Writing the users and groups to the xml file :: "+xmlFile);
	    	isUserGroupXMLGenerated = VWCUtil.writeListToFile(userGroupList, xmlFile, "", "UTF-16");
	    	//isUserGroupXMLGenerated = VWCUtil.writeListToFile(userGroupList, VWCUtil.getHome()+Util.pathSep+"lib"+Util.pathSep+Constants.userGroupXMLFile, "", "UTF-16");
	    	//VWSLog.dbg("User Group XML File Got Generated :: "+isUserGroupXMLGenerated);
	    	//VWSLog.add(" isUserGroupXMLGenerated "+isUserGroupXMLGenerated);
    	}
    }
    
    private Vector getUsersFromXML(){
    	UserGroupInXML userGroupInXML = new UserGroupInXML();
    	Vector userList = userGroupInXML.getUsersFromXML();
    	return userList;
    }
    private Vector getGroupsFromXML(){
    	UserGroupInXML userGroupInXML = new UserGroupInXML();
    	Vector groupList = userGroupInXML.getGroupsFromXML();
    	return groupList;
    }
    private List getUsersInGroupsXML(String groupName){
    	UserGroupInXML userGroupInXML = new UserGroupInXML();
    	List memberList = userGroupInXML.getUsersInGroupsXML(groupName);
    	return memberList;
    }
    /**
     * @return the adminGroup
     */
    public String getAdminGroup() {
        return adminGroup;
    }
    /**
     * @param adminGroup the adminGroup to set
     */
    public void setAdminGroup(String adminGroup) {
        this.adminGroup = adminGroup;
    }
    /**
     * @return the namedGroup
     */
    public String getNamedGroup() {
        return namedGroup;
    }
    /**
     * @param namedGroup the namedGroup to set
     */
    public void setNamedGroup(String namedGroup) {
        this.namedGroup = namedGroup;
    }
    
    public String getProfessionalNamedGroup() {
		return professionalNamedGroup;
	}

	public void setProfessionalNamedGroup(String professionalNamedGroup) {
		this.professionalNamedGroup = professionalNamedGroup;
	}
	public String getConcurrentGroup() {
		return concurrentGroup;
	}

	public void setConcurrentGroup(String concurrentGroup) {
		this.concurrentGroup = concurrentGroup;
	}

	public String getProfessionalConcrGroup() {
		return professionalConcrGroup;
	}

	public void setProfessionalConcrGroup(String professionalConcrGroup) {
		this.professionalConcrGroup = professionalConcrGroup;
	}
	
	public int getAdminGrpCount() {
		return adminGrpCount;
	}

	public void setAdminGrpCount(int adminGrpCount) {
		this.adminGrpCount = adminGrpCount;
	}
	/**
     * @return the namedGrpCount
     */
    public int getNamedGrpCount() {
        return namedGrpCount;
    }
    /**
     * @param namedGrpCount the namedGrpCount to set
     */
    public void setNamedGrpCount(int namedGrpCount) {
        this.namedGrpCount = namedGrpCount;
    }
    
    public int getNamedOnlineGrpCount() {
 		return namedOnlineGrpCount;
 	}

 	public void setNamedOnlineGrpCount(int namedOnlineGrpCount) {
 		this.namedOnlineGrpCount = namedOnlineGrpCount;
 	}

    /**
     * @return the namedGrpCount
     */
    public int getProfessionalNamedGrpCount() {
        return professionalNamedGrpCount;
    }
    /**
     * @param namedGrpCount the namedGrpCount to set
     */
    public void SetProfessionalNamedGrpCount(int professionalNamedGrpCount) {
        this.professionalNamedGrpCount = professionalNamedGrpCount;
    }
    
    public int getConcurrentGrpCount() {
		return concurrentGrpCount;
	}

	public void setConcurrentGrpCount(int concurrentGrpCount) {
		this.concurrentGrpCount = concurrentGrpCount;
	}

	public int getProfessConcrGrpCount() {
		return professConcrGrpCount;
	}

	public void setProfessConcrGrpCount(int professConcrGrpCount) {
		this.professConcrGrpCount = professConcrGrpCount;
	}
	
	public String getNamedOnlineGroup() {
		return namedOnlineGroup;
	}

	public void setNamedOnlineGroup(String namedOnlineGroup) {
		this.namedOnlineGroup = namedOnlineGroup;
	}

	public String getConcurrentOnlineGroup() {
		return concurrentOnlineGroup;
	}

	public void setConcurrentOnlineGroup(String concurrentOnlineGroup) {
		this.concurrentOnlineGroup = concurrentOnlineGroup;
	}
	public String getPublicWAGroup() {
		return publicWAGroup;
	}

	public void setPublicWAGroup(String publicWAGroup) {
		this.publicWAGroup = publicWAGroup;
	}

	public int getConcurrentOnlineGrpCount() {
		return concurrentOnlineGrpCount;
	}

	public void setConcurrentOnlineGrpCount(int concurrentOnlineGrpCount) {
		this.concurrentOnlineGrpCount = concurrentOnlineGrpCount;
	}


	public int getPublicWAGrpCount() {
		return publicWAGrpCount;
	}

	public void setPublicWAGrpCount(int publicWAGrpCount) {
		this.publicWAGrpCount = publicWAGrpCount;
	}
	
	
	public int getDirAdminGrpCount() {
		return dirAdminGrpCount;
	}

	public void setDirAdminGrpCount(int dirAdminGrpCount) {
		if (dirAdminGrpCount == 0 ){
			if (getAdminGroup() != null && getAdminGroup().trim().length() > 0) {
			    List adminList = getUsersInGroup(getAdminGroup());
			    this.dirAdminGrp = adminList;
			    if (adminList != null && adminList.size() > 0){
			    	this.dirAdminGrpCount = adminList.size();
			    }
			}
		}else{
			this.dirAdminGrpCount = dirAdminGrpCount;
		}
	}

    
	public int getDirNamedGrpCount() {
		return dirNamedGrpCount;
	}

	public void setDirNamedGrpCount(int dirNamedGrpCount) {
		if (dirNamedGrpCount == 0 ){
			if(getNamedGroup() != null && getNamedGroup().trim().length() > 0) {
			    List namedList = getUsersInGroup(getNamedGroup());
			    this.dirNamedGrp = namedList;
			    if (namedList != null && namedList.size() > 0){
			    	this.dirNamedGrpCount = namedList.size();
			    }
			}
		}else{
			this.dirNamedGrpCount = dirNamedGrpCount;
		}
	}
	public int getDirNamedOnlineGrpCount() {
		return dirNamedOnlineGrpCount;
	}
	
	public void setDirNamedOnlineGrpCount(int dirNamedOnlineGrpCount) {
		if (dirNamedOnlineGrpCount == 0 ){
			if (getNamedOnlineGroup() != null && getNamedOnlineGroup().trim().length() > 0) {
			    List namedOnlineList = getUsersInGroup(getNamedOnlineGroup());
			    this.dirNamedOnlineGrp = namedOnlineList;
			    if (namedOnlineList != null && namedOnlineList.size() > 0){
			    	this.dirNamedOnlineGrpCount = namedOnlineList.size();
			    }
			}
		}else{
			this.dirNamedOnlineGrpCount = dirNamedOnlineGrpCount;
		}
	}
	
	public void setDirConcurrentOnlineGrpCount(int concurrentOnlineGrpCount) {

	if (concurrentOnlineGrpCount == 0 ){
		if (getConcurrentOnlineGroup() != null && getConcurrentOnlineGroup().trim().length() > 0) {
		    List concurrentOnline = getUsersInGroup(getConcurrentOnlineGroup());
		    this.dirConcurrentOnlineGrp = concurrentOnline;
		    if (concurrentOnline != null && concurrentOnline.size() > 0){
		    	this.dirConcurrentOnlineGrpCount=concurrentOnline.size();
		    }
		}
	}else{
		this.dirConcurrentOnlineGrpCount=dirConcurrentOnlineGrpCount;
			
	}

}


	
	public int getDirPublicWAGrpCount() {
		return dirpublicWAGrpCount;
	}
	public void setDirPublicWAGrpCount(int publicWAGrpCount) {
		if (publicWAGrpCount == 0 ){
			if (getPublicWAGroup() != null && getPublicWAGroup().trim().length() > 0) {
			    List publicWAList = getUsersInGroup(getPublicWAGroup());
			    this.dirpublicWAGrp = publicWAList;
			    if (publicWAList != null && publicWAList.size() > 0){
			    	this.dirpublicWAGrpCount = publicWAList.size();
			    }
			}
		}else{
			this.dirpublicWAGrpCount = publicWAGrpCount;
		}
	}
	public int getDirConcurrentOnlineGrpCount() {
		return dirConcurrentOnlineGrpCount;
	}
		//getting room level named group count
	public int getDirRoomLevelNamedGrpCount() {
		return dirRoomLevelNamedGrpCount;
	}
	//setting room level named group count
	public void setDirRoomLevelNamedGrpCount(int dirRoomLevelNamedGrpCount) {
		if (dirRoomLevelNamedGrpCount == 0 ){
			String roomLevelNamed = VWSPreferences.getRoomLevelNamed(room);
			if (roomLevelNamed != null && roomLevelNamed.trim().length() > 0) {
			    List namedList = getUsersInGroup(roomLevelNamed);
			    this.dirRoomLevelNamedGrp = namedList;
			    if (namedList != null && namedList.size() > 0){
			    	this.dirRoomLevelNamedGrpCount = namedList.size();
			    }
			}
		}else{
			this.dirRoomLevelNamedGrpCount = dirRoomLevelNamedGrpCount;
		}
	}

	
	public int getDirRoomLevelConcurrentOnlineGrpCount() {
		return dirRoomLevelConcurrentOnlineGrpCount;
	}

	public void setDirRoomLevelConcurrentOnlineGrpCount(int dirRoomLevelConcurrentOnlineGrpCount) {
		if (dirRoomLevelConcurrentGrpCount == 0 ){
			String roomLevelConcurrentOnline = VWSPreferences.getRoomLevelConcurrentOnline(room);
			if (roomLevelConcurrentOnline != null && roomLevelConcurrentOnline.trim().length() > 0) {
			    List concurrentOnlineList = getUsersInGroup(roomLevelConcurrentOnline);
			    this.dirRoomLevelConcurrentGrp = concurrentOnlineList;
			    if (concurrentOnlineList != null && concurrentOnlineList.size() > 0){
			    	this.dirRoomLevelConcurrentGrpCount = concurrentOnlineList.size();
			    }
			}
		}else{
			this.dirRoomLevelConcurrentGrpCount = dirRoomLevelConcurrentGrpCount;
		}
	}
	
	public int getDirRoomLevelNamedOnlineGrpCount() {
		return dirRoomLevelNamedOnlineGrpCount;
	}
	//setting room level named group count
	public void setDirRoomLevelNamedOnlineGrpCount(int dirRoomLevelNamedGrpCount) {
		if (dirRoomLevelNamedGrpCount == 0 ){
			String roomLevelNamedOnline = VWSPreferences.getRoomLevelNamedOnline(room);
			if (roomLevelNamedOnline != null && roomLevelNamedOnline.trim().length() > 0) {
			    List namedOnlineList = getUsersInGroup(roomLevelNamedOnline);
			    this.dirRoomLevelNamedGrp = namedOnlineList;
			    if (namedOnlineList != null && namedOnlineList.size() > 0){
			    	this.dirRoomLevelNamedGrpCount = namedOnlineList.size();
			    }
			}
		}else{
			this.dirRoomLevelNamedGrpCount = dirRoomLevelNamedOnlineGrpCount;
		}
	}
	public int getDirProfessionalNamedGrpCount() {
		return dirProfessionalNamedGrpCount;
	}

	public void setDirProfessionalNamedGrpCount(int dirProfessionalNamedGrpCount) {
		if (dirProfessionalNamedGrpCount == 0 ){
			//VWSLog.dbg("getProfessionalNamedGroup 111454545"+getProfessionalNamedGroup());
			if (getProfessionalNamedGroup() != null && getProfessionalNamedGroup().trim().length() > 0) {
			    List professionalNamedList = getUsersInGroup(getProfessionalNamedGroup());
			    this.dirProfessionalNamedGrp = professionalNamedList;
			    //VWSLog.dbg("Get professional named group 1111"+professionalNamedList);
			    VWSLog.dbg("Dir professionalgroup NamedList  :::"+professionalNamedList);
			    if (professionalNamedList != null && professionalNamedList.size() > 0){
			    	this.dirProfessionalNamedGrpCount = professionalNamedList.size();
			    }
			}
		}else{
			this.dirProfessionalNamedGrpCount = dirProfessionalNamedGrpCount;
		}
	}
	
	//getting room level professional named group count
	public int getDirRoomLevelProfessionalNamedGrpCount() {
		return dirRoomLevelProfessionalNamedGrpCount;
	}
	//setting room level professional named group count
	public void setDirRoomLevelProfessionalNamedGrpCount(int dirRoomLevelProfessionalNamedGrpCount) {
		if (dirRoomLevelProfessionalNamedGrpCount == 0 ){
			//VWSLog.dbg("getProfessionalNamedGroup 111454545"+getProfessionalNamedGroup());
			String roomLevelProfessionalNamed = VWSPreferences.getRoomLevelProfessionalNamed(room);
			if (roomLevelProfessionalNamed != null && roomLevelProfessionalNamed.trim().length() > 0) {
			    List professionalNamedList = getUsersInGroup(roomLevelProfessionalNamed);
			    this.dirRoomLevelProfessionalNamedGrp = professionalNamedList;
			   // VWSLog.dbg("Get professional named group 1111"+professionalNamedList);
			    if (professionalNamedList != null && professionalNamedList.size() > 0){
			    	this.dirRoomLevelProfessionalNamedGrpCount = professionalNamedList.size();
			    }
			}
		}else{
			this.dirRoomLevelProfessionalNamedGrpCount = dirRoomLevelProfessionalNamedGrpCount;
		}
	}

	public int getDirConcurrentGrpCount() {
		return dirConcurrentGrpCount;
	}
	/**
	 * CV10.1 Public webaccess enhancement
	 * @param dirConcurrentGrpCount
	 */
	public void setDirConcurrentGrpCount(int dirConcurrentGrpCount) {
		if (dirConcurrentGrpCount == 0 ){
			if (getConcurrentGroup() != null && getConcurrentGroup().trim().length() > 0) {
			    List concurrentList = getUsersInGroup(getConcurrentGroup());
			    this.dirConcurrentGrp = concurrentList;
			    if (concurrentList != null && concurrentList.size() > 0){
			    	this.dirConcurrentGrpCount = concurrentList.size();
			    }
			}
		}else{
			this.dirConcurrentGrpCount = dirConcurrentGrpCount;
		}
	}

	public int getDirProfConcrGrpCount() {
		return dirProfConcrGrpCount;
	}
	/**
	 *  CV10.1 Public webaccess enhancement
	 * @param dirProfConcrGrpCount
	 */
	public void setDirProfConcrGrpCount(int dirProfConcrGrpCount) {
		if (dirProfConcrGrpCount == 0 ){
			if (getProfessionalConcrGroup() != null && getProfessionalConcrGroup().trim().length() > 0) {
				List professionalConcrList = getUsersInGroup(getProfessionalConcrGroup());
				this.dirProfConcrGrp = professionalConcrList;
				if (professionalConcrList != null && professionalConcrList.size() > 0){
					this.dirProfConcrGrpCount = professionalConcrList.size();
				}
			}
		}else{
			this.dirProfConcrGrpCount = dirProfConcrGrpCount;
		}
	}
	public int getDirRoomLevelConcurrentGrpCount() {
		return dirRoomLevelConcurrentGrpCount;
	}
	/**
	 * CV10.1 Public webaccess enhancement
	 * @param dirRoomLevelConcurrentGrpCount
	 */
	public void setDirRoomLevelConcurrentGrpCount(int dirRoomLevelConcurrentGrpCount) {
		if (dirRoomLevelConcurrentGrpCount == 0 ){
			String roomLevelConcurrent = VWSPreferences.getRoomLevelConcurrent(room);
			if (roomLevelConcurrent != null && roomLevelConcurrent.trim().length() > 0) {
				List concurrentList = getUsersInGroup(roomLevelConcurrent);
				this.dirRoomLevelConcurrentGrp = concurrentList;
				if (concurrentList != null && concurrentList.size() > 0){
					this.dirRoomLevelConcurrentGrpCount = concurrentList.size();
				}
			}
		}else{
			this.dirRoomLevelConcurrentGrpCount = dirRoomLevelConcurrentGrpCount;
		}

	}

	public int getDirRoomLevelProfConcrGrpCount() {
		return dirRoomLevelProfConcrGrpCount;
	}
	/**
	 * CV10.1 Public webaccess enhancement
	 * @param dirRoomLevelProfConcrGrpCount
	 */
	public void setDirRoomLevelProfConcrGrpCount(int dirRoomLevelProfConcrGrpCount) {
		if (dirRoomLevelProfConcrGrpCount == 0 ){
			String roomLevelProfConcurrent = VWSPreferences.getRoomLevelProfConcurrent(room);
			if (roomLevelProfConcurrent != null && roomLevelProfConcurrent.trim().length() > 0) {
				List profConcrList = getUsersInGroup(roomLevelProfConcurrent);
				this.dirRoomLevelProfConcrGrp = profConcrList;
				if (profConcrList != null && profConcrList.size() > 0){
					this.dirRoomLevelProfConcrGrpCount = profConcrList.size();
				}
			}
		}else{
			this.dirRoomLevelProfConcrGrpCount = dirRoomLevelProfConcrGrpCount;
		}
	}
	public int getDirRoomLevelpublicWAGrpCount() {
		return dirRoomLevelpublicWAGrpCount;
	}

	//public void setDirRoomLevelpublicWAGrpCount(int dirRoomLevelpublicWAGrpCount) {
		public void setDirRoomLevelpublicWAGrpCount(int dirRoomLevelPublicGrpCount) {
			if (dirRoomLevelPublicGrpCount == 0 ){
				String roomLevelPublicWA = VWSPreferences.getRoomLevelPublicWA(room);
				if (roomLevelPublicWA != null && roomLevelPublicWA.trim().length() > 0) {
					List publicRoomLevelList = getUsersInGroup(roomLevelPublicWA);
					this.dirRoomLevelpublicWAGrp = publicRoomLevelList;
					if (publicRoomLevelList != null && publicRoomLevelList.size() > 0){
						this.dirRoomLevelpublicWAGrpCount = publicRoomLevelList.size();
					}
				}
			}else{
				this.dirRoomLevelpublicWAGrpCount = dirRoomLevelpublicWAGrpCount;
			}
		}

	

	//Added for License changes, Madhavan.B
	public void deleteNamedUsers(String groupName){
		String param = "";
		Vector ret=new Vector();
        String schemeType = VWSPreferences.getSSType();
        if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)){
        	param =groupName;
        }else
			param =groupName;

		DoDBAction.get(database,"VWDelNamedUsers",param, ret);
	}
	
	public void updateADUsersToLDAP(Directory directory) {
		List usersList = new ArrayList();
		Creator userDetails = null;
		Vector resultValue = new Vector();
		String userName = null, param = null, domainName = null, name = null, domainCoponent = null;
		String principalName = null, upnSuffix = null, userDn = null, samName = null, emailId = null;
		String[] splittedDC = null;
		int result = -1;
		try {
			VWSLog.add("Fetch users from DB for ADS.");
			usersList = getUsers();
			VWSLog.add("DB Users List::::::::: " + usersList);
			for (int listIndex = 0; listIndex < usersList.size(); listIndex++) {
				domainName = "";
				userDetails = (Creator) usersList.get(listIndex);
				userName = userDetails.getName();
				VWSLog.add("-------------------------------START---------------------------------------------"+ listIndex);
				VWSLog.add("user name in AD to LPAD update.........." + userName);
				try {
					

					String userFullInfo = directory.getUserFullInfoOtherOU(userName);
					// VWSLog.add("Bf userFullInfo fetch from all OU's :::" + userFullInfo);
					if (userFullInfo.length() == 0)
						userFullInfo = directory.getUserFullInfo(userName);

					VWSLog.add("userFullInfo fetch from all OU's :::" + userFullInfo);
					if (userFullInfo.length() > 0) {
						StringTokenizer st = new StringTokenizer(userFullInfo, Util.SepChar);
						VWSLog.add("st.hashtokens " + st.countTokens());
						if (st.hasMoreTokens()) {
							name = st.nextToken();
							VWSLog.add("name :::" + name);
							if (name.contains("@")) {
								principalName = name.substring(0, name.indexOf("@"));
								upnSuffix = name.substring(name.indexOf("@") + 1, name.length());
							}
							VWSLog.add("principalName :::" + principalName);
							VWSLog.add("upnSuffix ===============>" + upnSuffix);
							userDn = st.nextToken();
							VWSLog.add("userDn ===============>" + userDn);
							samName = st.nextToken();
							VWSLog.add("samName ===============>" + samName);
							emailId = st.nextToken();
							VWSLog.add("emailId ===============>" + emailId);
							if (userDn.length() > 0) {
								domainCoponent = userDn.substring(userDn.indexOf("DC"), userDn.length());
								splittedDC = domainCoponent.split(",");
								for (String dcval : splittedDC) {
									dcval = dcval.substring(dcval.indexOf("=") + 1, dcval.length());
									domainName = domainName + dcval + ".";
								}
								domainName = domainName.substring(0, domainName.lastIndexOf("."));
								VWSLog.add("domainName ===============>" + domainName);
							}
							if (principalName != null && principalName.trim().length() > 0)
								name = principalName;
							VWSLog.add("name before add:::" + name);
							/*if (dirList.length > 1) {
								if (LDAPsingleToMultiple) {
									if (directory1.getDirName().equalsIgnoreCase(dirList[0])) {
										param = "16" + Util.SepChar + principalName + Util.SepChar + userDn
												+ Util.SepChar + samName + Util.SepChar + emailId + Util.SepChar
												+ domainName;
									} else {
										param = "16" + Util.SepChar + name + Util.SepChar + userDn + Util.SepChar
												+ samName + Util.SepChar + emailId + Util.SepChar + domainName;
									}
								} else {
									param = "16" + Util.SepChar + name + Util.SepChar + userDn + Util.SepChar + samName
											+ Util.SepChar + emailId + Util.SepChar + domainName + Util.SepChar
											+ upnSuffix;
								}
							} else*/
								param = "16" + Util.SepChar + name + Util.SepChar + userDn + Util.SepChar + samName
										+ Util.SepChar + emailId + Util.SepChar + domainName+ Util.SepChar
										+ upnSuffix;
							principalName = "";
							userDn = "";
							samName = "";
							emailId = "";
							domainName = "";

						}
						VWSLog.add("Param before calling user_get..." + param);
						if (param != null && param.trim().length() > 0) {
							result = DoDBAction.get(null, database, "User_Get", param, resultValue);
							if (result == 0)
								VWSLog.add("User : " + userName + " has successfully updated from AD to LDAP");
						}
						param = null;
					}

				} catch (Exception e) {
					VWSLog.add("Exception in syncADUserToLDAP" + e.getMessage());
				}
				VWSLog.add("-------------------------------END---------------------------------------------");
			}			
		} catch (Exception e) {
			VWSLog.add("Exception while fetching email, sam, dn info: " + e.getMessage());
		}
	}
	
	private boolean checkUpnsuffixSettingsInfo() {
		boolean updateUpnSuffix = false;
		try{	
		 	Vector ret = new Vector();
		 	String param = "Existing LDAP Room" + Util.SepChar;
		 	VWSLog.dbg("checkUpdateUpnsuffixFlag Domain : " + param );
    	    int flag = DoDBAction.get(database, "VWGetSettingsInfo", param, ret);
			if (flag == 0 && ret != null && ret.size() > 0) {
				String retFlag = ret.get(0).toString();
				if (retFlag.equalsIgnoreCase("true"))
					updateUpnSuffix = true;
			}
	    } catch (Exception e) {
    		VWSLog.dbg("Exception in checkUpdateUpnsuffixFlag "+e.getMessage());
    	}
		return updateUpnSuffix;
	}
	
	private void updateFirstDomain(String domainName)
	{
		try{	
		 	Vector ret = new Vector();
		 	String param = domainName + Util.SepChar;
		 	VWSLog.dbg("First LDAP Domain : " + param );
		 	DoDBAction.get(database, "CVUpdateLDAPTable", param, ret);
		} catch (Exception e) {
			VWSLog.dbg("Exception in updateFirstDomain "+e.getMessage());
		}
	}
	
	public String getFirstDomainDetailsFromDB() {
		String firstDomain = null;
		try{	
		 	Vector ret = new Vector();
		 	VWSLog.dbg("getFirstDomainDetailsFromDB database "+database);
		 	int flag = DoDBAction.get(database, "VWGetLDAPDomain", "", ret);
		 	if (flag ==0 && ret != null && ret.size() > 0) {
		 		firstDomain = ret.get(0).toString();
		 	}
		} catch (Exception e) {
			VWSLog.dbg("Exception in updateFirstDomain "+e.getMessage());
		}
		return firstDomain;
	}
	 /**
     * CV10.2 This method is used to sync the registered groups and added non-registered groups when restarting the contentverse service
     * @return
     */
	private void syncRegisteredGroups() 
    {    	
		String param = null, groupName = null;
        Vector ret = new Vector();
        Group group = null;
        int groupID = 0;
        String[] dirList = null;
        VWSLog.add("Synchronizing groups......");
		String schemeType = VWSPreferences.getSSType();    	    
	    String ldapDirectory = VWSPreferences.getTree();
		dirList = ldapDirectory.split(",");
		VWSLog.dbg("Before calling getServerSettingsGroup......");
		/**
		 * CV2019 issue fix - Commented on 12/12/2019 by Vanitha.S. 
		 * To identify the groups and its category(NamedDesktop/NamedOnline/ProfessionalNamed)
		 ***/
		//Vector<String> serverSettingGroupsList = getServerSettingsGroup();
		HashMap<String, String> serverSettingGroupsList = getServerSettingsGroup();
		VWSLog.dbg("Registered Groups in Contentverse settings :"+serverSettingGroupsList);
		if (serverSettingGroupsList == null && serverSettingGroupsList.size() == 0) {
			return;
		}		
		VWSLog.add("gIgnoreSync :" + gIgnoreSync);
        if(gIgnoreSync) return;
    	Vector groups = new Vector();
    	HashMap<String, String> groupsWithKeys = new HashMap<String, String>();
        if (isUserGroupXMLGenerated) {
        	groups = getGroupsFromXML();
        	VWSLog.dbg("Fetching groups using getGroupsFromXML()");
        } else {
        	/**
    		 * CV2019 issue fix - Commented on 12/12/2019 by Vanitha.S. 
    		 * To identify the groups and its category(NamedDesktop/NamedOnline/ProfessionalNamed)
    		***/
        	//groups = directory.getRegisteredGroups(serverSettingGroupsList);        	
        	groupsWithKeys = directory.getRegisteredGroups(serverSettingGroupsList);
        	groups = getGroupsNameFromGroupsWithKeyList(groupsWithKeys);
    		VWSLog.dbg("Fetching groups using directory.getRegisteredGroups()");
        }
        VWSLog.add("Registered Groups available in directory :" + groups);
        if(groups == null || groups.size() == 0)
        { 
        	gIgnoreSync = true;
            VWSLog.dbg("Registered groups not available in AD/LDAP directory...");
            return;
        }
        VWSLog.dbg(database.getName() + ": Synchronized " + groups.size() + " Groups to Security Server ");        
        Hashtable userTable = getUsersTable(); 
        VWSLog.add("users list from users table : " + userTable);
        
        List groupDataObjects = getGroupsByDomain();       
        VWSLog.dbg("Group size before adding it to the DB :" + groups.size());
        VWSLog.dbg("Groups after fetching from directory and before adding Group, Group users into DB :" + groups);
        //groupDataObjects - contains DB groups
        //groups - contains server settings registered groups
        deleteGroupToUserEntryForNonRegisteredGroups(groupDataObjects, groups); 
        
        Iterator iterator = groups.iterator();
        VWSLog.dbg("Group sync has started for registered groups..........");
        int loopCount = 0;
        while (iterator.hasNext())
        {
        	try{
	        	groupName = (String) iterator.next();
	        	VWSLog.add("Group Name.........: "+groupName);
	        	groupID = 0;
	           
	    		if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
					if (dirList.length > 1) {
						if (directory.getDirName().equalsIgnoreCase(dirList[0])) 
							param = "1" + Util.SepChar + Util.getOneSpace(groupName) + Util.SepChar + "~" + Util.SepChar;
						else
							param = "1" + Util.SepChar + Util.getOneSpace(groupName) + Util.SepChar + directory.getDirName() + Util.SepChar;					
					} else 
						param = "1" + Util.SepChar + Util.getOneSpace(groupName) + Util.SepChar + "~" + Util.SepChar;
	    		} else {
	    			param = "1" + Util.SepChar + Util.getOneSpace(groupName) + Util.SepChar + "~";
	    		}
	    		VWSLog.dbg("param before adding groups to the database: "+param);
	            DoDBAction.get(database, "Group_Get", param, ret);
	            if(ret.size() > 0)
	            {
	            	groupID = Util.to_Number( (String) ret.get(0));
	                ret.removeAllElements();
	            }	    	            
	            Object key = groupsWithKeys.keySet().toArray()[loopCount];
	            VWSLog.dbg("Category :"+key);
	            //groupCategory = groupsWithKeys.get(key);
	            //VWSLog.dbg("groupCategory from hashmap :" + groupCategory);
	            //removeGroupFromList(groupDataObjects, groupId);
	            VWSLog.dbg("before calling syncuseringroup() method for Registered group: " + groupName + " groupid: " + groupID);
	           	syncUsersInGroup(groupName, groupID, userTable, key.toString());            
	            VWSLog.dbg("After calling syncuseringroup() method for Registered group: "+ groupName);
	            groupName = "";
	        }catch(Exception ex){
        	    //VWSLog.dbg("Error while sync groups " + ex.getMessage() + " ::: " + groupName);
            }
        	loopCount++;
        }
        
        VWSLog.dbg("Group sync has started for non-registered groups..........");
        //Add all non registered groups if the group has added from the administration add dialog and then restarts the server.		
        List directoryUsersList = null;    
        /**This condition has added for St. Jude Medicals customer on 29/05/2019**/
        boolean syncRequired = VWSPreferences.getSyncFlagForNonRegisteredGroup();
        VWSLog.dbg("is sync Required For Non-Registered Group :" + syncRequired);
        if (syncRequired) {
        	VWSLog.dbg("Groups Available in DB :" + groupDataObjects);
	        if (groupDataObjects != null) {
	        	//nonRegisteredGroups = new Vector<>();
				iterator = groupDataObjects.iterator(); 			
		        while (iterator.hasNext())
		        {
		        	group = (Group) iterator.next();
		        	groupID = group.getId();
		        	groupName = group.getName();
		        	VWSLog.dbg("groupName from database : "+groupName);
		        	VWSLog.dbg("is group Non-Registered : "+isRegisteredGroup(groupName));
	 			    if(isRegisteredGroup(groupName)==false) {
	 			    	VWSLog.dbg("is this group available in the registered group list : "+!groups.contains(groupName));
	 			    	if (!groups.contains(groupName)) {
	 			    		// This condition is used to check whether the group is exist in the directory
	 			    		directoryUsersList = getUsersInGroup_New(groupName);
	 			    		VWSLog.dbg("is group exist in the directory : "+directoryUsersList);
	 			    		if (directoryUsersList != null) {
	 			    			VWSLog.dbg("directoryUsersList.size() size :"+directoryUsersList.size());
		 			    		if (directoryUsersList.size() > 0) {
		 			    			//nonRegisteredGroups.add(groupName);
		 			    			VWSLog.dbg("before calling syncuseringroup() method for non Registered group: " + groupName + " groupid: " + groupID);
		 			    			/**Group Category NR-Non Registered Group***/
		 				           	syncUsersInGroup(groupName, groupID, userTable, "NR");            
		 				            VWSLog.dbg("After calling syncuseringroup() method for non Registered group: "+ groupName);
		 			    		} else {
		 			    			if (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
			 			               	VWSLog.errMsg("Group " +groupName+ " not available in LDAP" );    
			 			            } else {
			 			               	VWSLog.errMsg("Group " +groupName+ " not available in AD" );
			 			            }
		 			    		}
	 			    		}
	 			    	}
	 			    }
	 	        } 		   
			}  
        }
        
        /****CV10.2 - DB deletion has stopped from GroupTable. As discussed with Rajesh on 09 Apr 2019
        /*Commented by vanitha*/
        //removeGroups(groupDataObjects, schemeType);
        /*if (groupDataObjects.size() > 0)
        	showMsgForDeletedGroups(groupDataObjects, schemeType);*/   
        VWSLog.add("Before setting AD to LDAP flag - Directory Name....."+directory.getDirName()+" Directory List :"+dirList+" Directory Length :"+dirList.length);
        if (VWSServer.LastRoom != null && VWSServer.LastRoom.equals(room)) {
        	VWSPreferences.setADToLDAPFlag(false);
        }
        VWSLog.add("End of synchronizing groups...");
    }
	
	/**
	 * CV10.2 This method is used to sync the registered and added non-registered users when restarting the contentverse service
	*/
	private void syncRegisteredUsers() {
		try{
		    Vector users = new Vector();
		    List groupUsers = new Vector();
		    String adminGroup_SS = null;
		    String subAdminGroup_SS = null;
		    String cvNamedGroup_SS = null;
		    String cvNamedOnlineGroup_SS = null;
		    String cvProfNamedGroup_SS = null;
		    String cvBatchAdminGroup_SS = null;
		    
		    boolean isHosting = false;
		    try{
		    	isHosting = CVPreferences.getHosting();
		    }catch (Exception e) {
		    	isHosting = false;
		    }
		    
		    adminGroup_SS = getAdminGroup();
		    subAdminGroup_SS = getSubAdminGroup();
		    if (!isHosting) {
			    cvNamedGroup_SS = getNamedGroup();
			    cvNamedOnlineGroup_SS = getNamedOnlineGroup();
			    cvProfNamedGroup_SS = getProfessionalNamedGroup();
			    cvBatchAdminGroup_SS = getBatchInputGroup();
		    } else {
		    	cvNamedGroup_SS = VWSPreferences.getRoomLevelNamed(room);
		    	cvNamedOnlineGroup_SS = VWSPreferences.getRoomLevelNamedOnline(room);
		    	cvProfNamedGroup_SS = VWSPreferences.getRoomLevelProfessionalNamed(room);
		    	cvBatchAdminGroup_SS = VWSPreferences.getRoomsBatchInputAdministrator(room);
		    }
		    Vector ret = new Vector();		 
		    String param = "";
		    List dbUsers = new ArrayList();
		    ArrayList<Creator> usersListForEmailUpdt = null;
		    Creator emailUser = null;
		    List dirNonRegUsers = new LinkedList();
		    Group group = null;
		    String groupName = null;	
		    String user = null;
    		Creator creator = null;
    		
			String pFailedUser= null;
	    	int userId = 0;		    
		    String userName = null;
		    /**
		     * CV10.1 LDAp multi domain group/user sync issue
		     * 15-6-2017
		     */
		    VWSLog.add("Synchronizing users......");

		    //Fetching domains information that was configured in Server Settings
		    String[] dirList = null;
		    //boolean LDAPsingleToMultiple = VWSPreferences.getLDAPUpgradeToMultiDomain();
		    String schemeType = VWSPreferences.getSSType();
		    
		    if(schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
		    	String ldapDirectory = VWSPreferences.getTree();
				dirList = ldapDirectory.split(",");
				//Deleting of named group should happen only one time when multiple domains are configured. so set deleteNamedGroupUsers flag accordingly.
			    if ((dirList.length>1) && ((RampageServer.currentDomainName.isEmpty()) && (RampageServer.currentDomainName.length()==0)) )  {
			    	RampageServer.deleteNamedGroupUsers = true;
				} else  {
					RampageServer.deleteNamedGroupUsers = false;
				}
			    if ((RampageServer.currentDomainName.isEmpty()) && (RampageServer.currentDomainName.length()==0)) {
			    	RampageServer.currentDomainName = directory.getDirName();
			    }		    
		    	dbUsers = getUsers_New(directory.getDirName());
		    } else {
				VWSLog.dbg("Fetch users from DB for ADS.");
				//RampageServer.deleteNamedGroupUsers = false;
				dbUsers = getUsers();	
				VWSLog.dbg("ADS dbUsers inside syncUsers method::::::::: "+dbUsers.size());
			}
		    
		    VWSLog.dbg("Before deleting Named, Professional group users");	 
		    if (!schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)
		    		|| ((dirList.length == 1) && (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS))))  {  //When server is AD Server (or) LDAP single domain
		    	VWSLog.dbg("Before deleting Named, professional named, concurrent, professional concurrent users when schema is ADS...");
		    	if (adminGroup_SS.length() > 0) {
		    		deleteNamedUsers(adminGroup_SS);
		    	}
		    	if (subAdminGroup_SS.length() > 0) {
		    		deleteNamedUsers(subAdminGroup_SS);
		    	}		    	
		    	if (cvNamedGroup_SS.length()>0) {
		    		VWSLog.dbg("Deleting named user :"+cvNamedGroup_SS);
		    		deleteNamedUsers(cvNamedGroup_SS);
		    	}
		    	if (cvNamedOnlineGroup_SS.length() > 0) {
		    		VWSLog.dbg("Deleting named online user :"+cvNamedOnlineGroup_SS);
		    		deleteNamedUsers(cvNamedOnlineGroup_SS);
		    	}
		    	if (cvProfNamedGroup_SS.length()>0) {
		    		VWSLog.dbg("Deleting professional named user :"+cvProfNamedGroup_SS);
		    		deleteNamedUsers(cvProfNamedGroup_SS);
		    	}
		    	if (cvBatchAdminGroup_SS.length()>0) {
		    		VWSLog.dbg("Deleting Batch Input Administrator user :"+cvBatchAdminGroup_SS);
		    		deleteNamedUsers(cvBatchAdminGroup_SS);
		    	}
		    } else if ((dirList.length > 1) && (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) )   { //for LDAP Multiple Domains
		    	String groupNameWithDomain = "";  
		    	VWSLog.dbg("Before deleting Named, professional named, concurrent, professional concurrent users when schema is LDAP multiple domain...");
		    	
		    	//Deleting of named group should happen only one time when multiple domains are configured. so if() condition has added.
		    	if (directory.getDirName().equalsIgnoreCase(dirList[0])) {				    	
			    	VWSLog.dbg("Inside else before deleting of adminGroup "+adminGroup_SS.length());
			    	if (adminGroup_SS.length() > 0) {
			    		deleteNamedUsers(adminGroup_SS);
			    	}
			    	VWSLog.dbg("Inside else before deleting of sub adminGroup "+subAdminGroup_SS.length());
			    	if (subAdminGroup_SS.length() > 0) {
			    		deleteNamedUsers(subAdminGroup_SS);
			    	}
			    	VWSLog.dbg("Before deleting named Group ::"+getNamedGroup());
			    	if (cvNamedGroup_SS.length()>0) {
			    		VWSLog.dbg("Deleting named user :"+cvNamedGroup_SS);
			    		deleteNamedUsers(cvNamedGroup_SS);
			    	}
			    	if (cvNamedOnlineGroup_SS.length() > 0) {
			    		VWSLog.dbg("Deleting named online user :"+cvNamedOnlineGroup_SS);
			    		deleteNamedUsers(cvNamedOnlineGroup_SS);
			    	}
			    	if (cvProfNamedGroup_SS.length()>0) {
			    		VWSLog.dbg("Deleting professional named user :"+cvProfNamedGroup_SS);
			    		deleteNamedUsers(cvProfNamedGroup_SS);
			    	}
			    	if (cvBatchAdminGroup_SS.length()>0) {
			    		VWSLog.dbg("Deleting Batch Input Administrator user :"+cvBatchAdminGroup_SS);
			    		deleteNamedUsers(cvBatchAdminGroup_SS);
			    	}
				}
			}
			VWSLog.dbg("gIgnoreSync flag status before checking dbUsers != null && dbUsers.size() > 0:::" + gIgnoreSync);
		    VWSLog.dbg("Users in database : " + dbUsers);
		    
/***********************************************Started to add registered group users in the list*******************************************************************/
	    	groupUsers = getRegisteredGroupUsersList(isHosting);	    	
		    VWSLog.dbg("Registered groupUsers size: "+groupUsers.size());
		    gIgnoreSync = false;
	    	//Below lines is added by Srikanth on 13 Feb 2019		    	
		    if (groupUsers == null || groupUsers.size() == 0) {
		    	gIgnoreSync = true;
		    	VWSLog.add("Registered group users not available in the AD/LADP directory" + gIgnoreSync);
		    	return;
		    }		    
			users.addAll(groupUsers);
			VWSLog.add("Registered group users in directory  : "+groupUsers);		    
			//---------------Following part is for existing room ----------------------------------------------------//
			try {
				Iterator iterator1 = null;
				String dbUser = null, user1 = null;
				int userID = 0;
		    	if (dbUsers != null && dbUsers.size() > 0) {
		    		usersListForEmailUpdt = new ArrayList<Creator>();
		    		VWSLog.dbg("UsersList Before Removing users as it is already exist in DB :"+users);		    		
		    		//Add all non registered group users if the group is added from the administration add dialog and then restarting the server.		    		
		    		try {
		    			/**This condition has added for St. Jude Medicals customer on 29/05/2019**/
		    			
		    	        boolean syncRequired = VWSPreferences.getSyncFlagForNonRegisteredGroup();
		    	        //VWSLog.add("is sync Required For Non-Registered Group users :" + syncRequired);
		    	        if (syncRequired) {
			    			List dbGroupList = getGroupsByDomain();
			    			String[] nonRegisteredUsersList = null;
			    			String nonRegisteredUser = null;
			    			int dbUserID = 0;
			    			VWSLog.dbg("GroupList in DB : "+dbGroupList);
			    			if (dbGroupList != null) {	    			
				    			VWSLog.dbg("inside dbGroupList is not null.....");
				    			Iterator iterator = dbGroupList.iterator();        
			    		        while (iterator.hasNext())
			    		        {
			    		        	group = (Group) iterator.next();
			    		        	groupName = group.getName();
						 			//VWSLog.add("Before checking whether group is registered nonregistered group or not : "+groupName);
					 			    if(isRegisteredGroup(groupName)==false) {	
					 			    	VWSLog.dbg("Pulling users for non registered group:  " + groupName);
					 			    	dirNonRegUsers = getUsersInGroup(groupName);		
					 			    	VWSLog.dbg("After fetching users for Non registered group : "+ dirNonRegUsers );				 			    	
					 			    	if (dirNonRegUsers.size()>0) {
					 			    		VWSLog.dbg("Non registered group user's list size : "+ dirNonRegUsers.size() );
					 			    		iterator1 = dirNonRegUsers.iterator();        
						    		        while (iterator1.hasNext()) {
						    		        	user = iterator1.next().toString();
						    		         	VWSLog.dbg("Non registered group user : "+ groupName + " is "+user);	
						    		         	nonRegisteredUsersList = user.split(Util.SepChar);
						    		         	if (nonRegisteredUsersList[0] != null) {
						    		         		nonRegisteredUser = nonRegisteredUsersList[0];
							    		         	VWSLog.dbg("Non registered user name :'"+nonRegisteredUser+"'");	
							    		         	if (nonRegisteredUser.contains("@"))
							    		         		nonRegisteredUser = nonRegisteredUser.substring(0, nonRegisteredUser.indexOf("@"));
							    		         	VWSLog.dbg("Non registered user name :'"+nonRegisteredUser+"'");	
							    		         	/**
							    		         	 * Below code is added by Vanitha on 26/08/2019 as Rajesh has asked to sync only added users for non-registered groups.[Added from the Administration]
									    			**/
							    		         	dbUserID = checkUserExistInDatabase(dbUsers, nonRegisteredUser);
							    		         	VWSLog.dbg("is user availbale in DB :"+dbUserID);
							    		         	if (dbUserID > 0) {
							    		         		/**Below conditon is commentd by vanitha. Hence the user is already added in the DB no need to add it in the users list.Have to add it in the usersListForEmailUpdt
							    		         		 * Users list is for new users. usersListForEmailUpdt is for existing users.
							    		         		**/
							 			    			/*if (!users.contains(user)) {
							 			    				VWSLog.dbg("Non-Registered User "+user);	
							 			    				users.add(user);
							 			    			}*/
							    		         		/**Below code is added for updating upnsuffix, same name and email id for existing users**/
							    		         		emailUser = new Creator(userID);
							    		         		emailUser.setName(user);
							    		         		usersListForEmailUpdt.add(emailUser);					    		         		
							    		         	}
						    		         	}
					 			    		}
					 			    	}
					 			    }
			    		        }													 		    
				    		}
		    	        }
		    			
		    			
		    			
		    			VWSLog.dbg("Users list before deleting db users from the userslist: " + users);
		    			//Remove users if the user is exist in DB.		
		    			//This block has added for showing error message in contentverse server manager
		    			try{
				    		VWSLog.dbg("dbUsers.size() in syncRegisteredUsers :"+dbUsers.size());		    		
				    		for (int item = (dbUsers.size()-1); item >= 0; item--){
				    			creator = (Creator)dbUsers.get(item);
				    			if (creator != null) {
									dbUser = creator.getName();
									userID = creator.getId();
									if (users != null) {
										iterator1 = users.iterator();        
					    		        while (iterator1.hasNext()) {
					    		        	user = iterator1.next().toString();
					    		        	user1 = user;
					    		        	if (user1.contains("@")) user1 = user1.substring(0, user1.indexOf("@"));
					    		        	if (dbUser.contains("@")) dbUser = dbUser.substring(0, dbUser.indexOf("@"));
					    		         	if (user1.equalsIgnoreCase(dbUser)) {
					    		         		/**Added this to update email id, UPNSuffix, UserDN and SamName for existing DB user**/
					    		         		if (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
						    		         		emailUser = new Creator(userID);
						    		         		emailUser.setName(user);
						    		         		usersListForEmailUpdt.add(emailUser);
					    		         		}
					    		         		//Ended////////////////////////
					    		         		users.remove(user);
					    		         		dbUsers.remove(item);
					    		         		break;
					    		         	}
				 			    		}
									}
				    			}
						    }
		    			}catch (Exception db){
		    				VWSLog.dbg("Exception while deleting db users in users list: " + db.getStackTrace());
		    			}
			    		VWSLog.dbg("Registered group users after cross checking with DBUsers::: " + users);
		    		} catch (Exception ee) {
		    			VWSLog.dbg("Exception while fetching non registered group users: " + ee.getMessage());
		    		}
		    	}
			}catch (Exception eee) {
				VWSLog.dbg("Exception while fetching registered and non registered group users: " + eee.getMessage());
			}
	    	//---------------Existing room part ended----------------------------------------------------//
	    	
	    	VWSLog.add("Users List after removing the db-existing users: " + users);
		    
/***********************************************End*******************************************************************/
	    	if (users != null && users.size() > 0) {
		    	VWSLog.add("Users list before adding to DB, after deleting existing users from list: " + users);
		    	VWSLog.dbg("users list available in DB: "+ dbUsers);		    		    
		    	
		    	//VWSLog.dbg(database.getName() + ": Synchronized " + users.size() + " Users to Security Server ");    		    	
		    	if (!schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
			    	for(int i=0;i<users.size();i++) {
			    		param = "1" + Util.SepChar + users.get(i).toString()+Util.SepChar+""+Util.SepChar+""+Util.SepChar+""+Util.SepChar;
			    		DoDBAction.get(database, "User_Get", param, ret);
			    		if(ret.size() > 0)
			    		{
			    			userId = Util.to_Number((String) ret.get(0));
			    			removeUserFromList(dbUsers, userId);
			    			ret.removeAllElements();
			    		}
			    	}
		    	} else {
		    		VWSLog.add("Sync process has started for Users.............");
		    		VWSLog.dbg("directory.getDirName(): " + directory.getDirName() + "dirList[0]" + dirList[0]);
					for (int i = 0; i < users.size(); i++)
			    	{            	 
			    		userName = (String) users.get(i);
			    		VWSLog.add("Adding user to DB: " + userName + "  DirectoryName: " + directory.getDirName() + " DirectoryList[0]: " + dirList[0]);			    		
		    			if (dirList.length > 1) {
		    				//Commented bz we are not considering this registry
		    				//if (LDAPsingleToMultiple) {
								if (directory.getDirName().trim().equalsIgnoreCase(dirList[0].trim()))
									param = "1" + Util.SepChar + Util.getOneSpace(userName);
								else
									param = "101" + Util.SepChar + Util.getOneSpace(userName);
	
							/*} else {
								param = "101" + Util.SepChar + Util.getOneSpace(userName);
							}*/
						} else {
							param = "1" + Util.SepChar + Util.getOneSpace(userName);
						}
			    		
			    		VWSLog.dbg ("Before adding to DB UserName : " + userName + " Params: " + param);
			    		DoDBAction.get(database, "User_Get", param, ret);
			    		if(ret.size() > 0)
			    		{		    			
			    			userId = Util.to_Number((String) ret.get(0));
			    			VWSLog.add("User inserted ");
			    			removeUserFromList(dbUsers, userId);
			    			ret.removeAllElements();
			    		}		    		
			    	}
		    	}
		    	//DB deletion has stopped from UsersTable. As discussed with Rajesh on 09 Apr 2019.Added by vanitha    
			    //removeUsers(dbUsers, schemeType);
		    	VWSLog.dbg("db users list size after insert ........"+dbUsers);
				if (dbUsers.size() > 0)
		    		showMsgForDeletedUsers(dbUsers, schemeType);
			}
	    	/**This code is to update user email id for existing DB users*/
	    	if (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
	    		String[] userDetails = null;
	    		String principalName = null, upnSuffix = null, userDn = null, samName = null, emailId = null, domainCoponent = null, domainName = null;
	    		String[] splittedDC = null;
	    		VWSLog.dbg("Users List For Email Update........"+usersListForEmailUpdt);
		    	if (usersListForEmailUpdt != null && usersListForEmailUpdt.size() > 0) {
		    		VWSLog.dbg("Users List size For Email Update........"+usersListForEmailUpdt.size());	
		    		/*for (Creator usersEmail : usersListForEmailUpdt) {
		    			userDetails = usersEmail.getName().split(Util.SepChar);
		    			if (userDetails.length > 3) {
		    				userName = userDetails[0];
		    				if ((dirList.length == 1) 
		    						|| (dirList.length > 1 && directory.getDirName().equalsIgnoreCase(dirList[0]))) {
		    					if (userName.contains("@")) 
			    					userName = userName.substring(0, userName.indexOf("@"));
		    				}
		    				VWSLog.dbg("user name........"+userName);
		    				VWSLog.dbg("Email ID........"+userDetails[3]);
		    				param = userName+Util.SepChar+userDetails[3];
		    				DoDBAction.get(database, "VW_SetUserMails", param, new Vector());
		    			}		    			
		    		}*/
					for (int count = 0; count < usersListForEmailUpdt.size(); count++) {
						try {
			    			String userDetails1 = usersListForEmailUpdt.get(count).toString();
			    			VWSLog.dbg("userDetails1........"+userDetails1);
			    			if (userDetails1 != null && userDetails1.length() > 0) {
			    				userDetails = userDetails1.split(Util.SepChar);
				    			userName = userDetails[0];
				    			if (userName.contains("@")) {
				    				if ((dirList.length == 1) || (dirList.length > 1 && directory.getDirName().trim().equalsIgnoreCase(dirList[0].trim()))) {
				    					principalName = userName.substring(0, userName.indexOf("@"));
				    				}
									upnSuffix = userName.substring(userName.indexOf("@") + 1, userName.length());									
				    			} else {
				    				upnSuffix = "";
				    			}
				    			VWSLog.dbg("principalName........"+principalName);
				    			VWSLog.dbg("upnSuffix........"+upnSuffix);
				    			userDn = userDetails[1];
								VWSLog.dbg("userDn ===============>" + userDn);
								samName = userDetails[2];
								if (upnSuffix != null && upnSuffix.trim().length() >0 && dirList.length > 1) {
									if (!directory.getDirName().trim().equalsIgnoreCase(dirList[0].trim()))
										samName = samName + "@" + upnSuffix;
								}
								VWSLog.dbg("samName ===============>" + samName);
								emailId = userDetails[3];
								VWSLog.dbg("emailId ===============>" + emailId);
								domainName = "";
								if (userDn.length() > 0) {
									domainCoponent = userDn.substring(userDn.indexOf("DC"), userDn.length());
									splittedDC = domainCoponent.split(",");
									for (String dcval : splittedDC) {
										dcval = dcval.substring(dcval.indexOf("=") + 1, dcval.length());
										domainName = domainName + dcval + ".";
									}
									domainName = domainName.substring(0, domainName.lastIndexOf("."));
									VWSLog.dbg("domainName ===============>" + domainName);
								} 
								if (emailId != null && emailId.length() > 0)
									emailId = emailId.trim();
			    			}
			    			if (principalName != null && principalName.trim().length() > 0)
			    				userName = principalName;
			    			param = "16" + Util.SepChar + userName + Util.SepChar + userDn + Util.SepChar + samName
									+ Util.SepChar + emailId + Util.SepChar + domainName+ Util.SepChar
									+ upnSuffix;
			    			VWSLog.dbg("User details updated......."+param);
			    			DoDBAction.get(null, database, "User_Get", param, new Vector());
							
						} catch(Exception e) {
							VWSLog.dbg("Exception while updating users :"+e.getMessage()+ " "+ e);
						}
						principalName = "";
						userDn = "";
						samName = "";
						emailId = "";
						domainName = "";
		    		}
		    	}
	    	}
		    VWSLog.add("End of syncRegisteredUsers...");
		    
		}	catch(Exception ex){
		    VWSLog.dbg("Exception while synchronizing registered/nonregistered users " + ex.getMessage());
		}
	    
	}
	
	/**
	 *  CV10.2 This method is used to get registered groups users list 
	 * @param isHosting
	 * @return
	 */
	public List getRegisteredGroupUsersList (boolean isHosting) {
		Vector users = new Vector();
	    List groupUsers = new Vector();
	    List adminList = new Vector();
	    List subAdminList = new Vector();
	    List namedList = new Vector();
	    List namedOnlineList = new Vector();
	    List profesionalNamedList=new Vector();
	    List concurrentList=new Vector();
	    List concurrentOnlineList = new Vector();
	    List profConcrList=new Vector();
	    List batchInputList = new Vector();
	    String adminName = null, subAdminGrpName = null, namedGroup = null, namedOnlineGroup = null, profNamedGroup = null, epConcurrentGroup = null, epConcurrentOnlineGroup = null, pfConcurrentGroup = null, batchInputGroup = null;
	    try {
		    Vector ret = new Vector();
		    adminName = getAdminGroup();
		    VWSLog.add("Admin group Name ======> "+adminName);
		    if (adminName != null && adminName.trim().length() > 0) {
		    	//this.dirAdminGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
		    	adminList = getDirectoryUsersInGroup(this.dirAdminGrp, adminName);
		    	VWSLog.add("Admin group users List :::::::::::"+adminList);   	
		    }
		   
		    subAdminGrpName = getSubAdminGroup();
		    VWSLog.add("Sub Admin group Name ======> "+subAdminGrpName);
		    if (subAdminGrpName != null && subAdminGrpName.trim().length() > 0) {
		    	//this.dirSubAdminGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
		    	subAdminList = getDirectoryUsersInGroup(this.dirSubAdminGrp, subAdminGrpName);
		    	VWSLog.add("Sub Admin group users List :::::::::::"+subAdminList);   	
		    }
		    
		    if (!isHosting) {
		    	namedGroup = getNamedGroup();
			    VWSLog.add("Named Group Name ======> "+namedGroup);
			    if (namedGroup != null && namedGroup.trim().length() > 0) {
			    	//this.dirNamedGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
			    	namedList = getDirectoryUsersInGroup(this.dirNamedGrp, namedGroup);
			    	/*if (this.dirNamedGrp != null) {
			    		namedList = this.dirNamedGrp;
				    } else {
				    	namedList = getUsersInGroup(namedGroup);
				    }*/
			    	VWSLog.add("Named Group users list :::::::::::"+namedList);
			    }
			    
			    namedOnlineGroup = getNamedOnlineGroup();
			    VWSLog.add("Named online Group Name ======> "+namedOnlineGroup);
			    if (namedOnlineGroup != null && namedOnlineGroup.trim().length() > 0) {
			    	//this.dirNamedOnlineGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
			    	namedOnlineList = getDirectoryUsersInGroup(this.dirNamedOnlineGrp, namedOnlineGroup);
			    	/*if (this.dirNamedOnlineGrp != null) {
			    		namedOnlineList = this.dirNamedOnlineGrp;
			    	} else { 
			    		namedOnlineList = getUsersInGroup(namedOnlineGroup);
			    	}*/
			    	VWSLog.add("Named online Group users list :::::::::::"+namedOnlineList);
			    }
			    
			    profNamedGroup = getProfessionalNamedGroup();
			    VWSLog.add("Professional Named Group Name ======> "+profNamedGroup);		    
			    if (profNamedGroup != null && profNamedGroup.trim().length() > 0) {
			    	//this.dirProfessionalNamedGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
			    	profesionalNamedList = getDirectoryUsersInGroup(this.dirProfessionalNamedGrp, profNamedGroup);
			    	/*if (this.dirProfessionalNamedGrp != null) {
			    		profesionalNamedList = this.dirProfessionalNamedGrp;
			    	} else {
			    		profesionalNamedList = getUsersInGroup(profNamedGroup);
			    	}*/
			    	VWSLog.add("Professional Named Group users list :::::::::::"+profesionalNamedList);
			    }
			    
			    epConcurrentGroup = getConcurrentGroup();
			    VWSLog.add("Enterprise concurrent Group Name ======> "+epConcurrentGroup);
			    if (epConcurrentGroup != null && epConcurrentGroup.trim().length() > 0) {
			    	//this.dirConcurrentGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
			    	concurrentList = getDirectoryUsersInGroup(this.dirConcurrentGrp, epConcurrentGroup);
			    	/*if (this.dirConcurrentGrp != null) {
			    		concurrentList = this.dirConcurrentGrp;
			    	} else {
			    		concurrentList = getUsersInGroup(epConcurrentGroup);
			    	}*/
			    	VWSLog.add("Enterprise concurrent Group users list :::::::::::"+concurrentList);
			    }
			    
			    epConcurrentOnlineGroup = getConcurrentOnlineGroup();
			    VWSLog.add("Enterprise concurrent online Group Name ======> "+epConcurrentOnlineGroup);
			    if (epConcurrentOnlineGroup != null && epConcurrentOnlineGroup.trim().length() > 0) {
			    	//this.dirConcurrentOnlineGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
			    	concurrentOnlineList = getDirectoryUsersInGroup(this.dirConcurrentOnlineGrp, epConcurrentOnlineGroup);
			    	/*if (this.dirConcurrentOnlineGrp != null) {
			    		concurrentOnlineList = this.dirConcurrentOnlineGrp;
			    	} else {
						concurrentOnlineList = getUsersInGroup(epConcurrentOnlineGroup);
			    	}*/
			    	VWSLog.add("Enterprise concurrent online Group users list :::::::::::"+concurrentOnlineList);
			    }
			    
			    pfConcurrentGroup = getProfessionalConcrGroup();
			    VWSLog.add("Professional concurrent Group Name ======> "+pfConcurrentGroup);
			    if (pfConcurrentGroup != null && pfConcurrentGroup.trim().length() > 0) {
			    	//this.dirProfConcrGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
			    	profConcrList = getDirectoryUsersInGroup(this.dirProfConcrGrp, pfConcurrentGroup);
			    	/*if (this.dirProfConcrGrp != null) {
			    		profConcrList = this.dirProfConcrGrp;
			    	} else {
						profConcrList = getUsersInGroup(pfConcurrentGroup);
			    	}*/
			    	VWSLog.add("Professional concurrent Group users list :::::::::::"+profConcrList);
			    }
			    batchInputGroup = getBatchInputGroup();
			    VWSLog.add("Batch Input Administrator group Name ======> "+batchInputGroup);
			    if (batchInputGroup != null && batchInputGroup.trim().length() > 0) {
			    	//this.dirSubAdminGrp variable has initialized in SirectorySync() constructor. If this is null then have to pull users from directory
			    	batchInputList = getDirectoryUsersInGroup(this.dirBatchInputGrp, batchInputGroup);
			    	VWSLog.add("Batch Input Administrator group users List :::::::::::"+batchInputList);   	
			    }
	
		    } else {  
		    //if (isHosting ) {
		    	 namedGroup = VWSPreferences.getRoomLevelNamed(room);
		    	 if (namedGroup != null && namedGroup.trim().length() > 0) {
		    		 namedList = getDirectoryUsersInGroup(this.dirRoomLevelNamedGrp, namedGroup);
		    	 }
		    	 namedOnlineGroup = VWSPreferences.getRoomLevelNamedOnline(room);
		    	 if (namedOnlineGroup != null && namedOnlineGroup.trim().length() > 0) {
		    		 namedOnlineList =  getDirectoryUsersInGroup(this.dirRoomLevelNamedOnlineGrp, namedOnlineGroup);
		    	 }
		    	 profNamedGroup = VWSPreferences.getRoomLevelProfessionalNamed(room);
		    	 if (profNamedGroup != null && profNamedGroup.trim().length() > 0) {
		    		 profesionalNamedList = getDirectoryUsersInGroup(this.dirRoomLevelProfessionalNamedGrp, profNamedGroup);
		    	 }
		    	 epConcurrentGroup = VWSPreferences.getRoomLevelConcurrent(room);
		    	 if (epConcurrentGroup != null && epConcurrentGroup.trim().length() > 0) {
		    		 concurrentList = getDirectoryUsersInGroup(this.dirRoomLevelConcurrentGrp, epConcurrentGroup);
		    	 }
		    	 epConcurrentOnlineGroup = VWSPreferences.getRoomLevelConcurrentOnline(room);
		    	 if (epConcurrentOnlineGroup != null && epConcurrentOnlineGroup.trim().length() > 0) {
		    		 concurrentOnlineList = getDirectoryUsersInGroup(this.dirRoomLevelConcurrentOnlineGrp, epConcurrentOnlineGroup);
		    	 }
		    	 pfConcurrentGroup = VWSPreferences.getRoomLevelProfConcurrent(room);
		    	 if (pfConcurrentGroup != null && pfConcurrentGroup.trim().length() > 0) {
		    		 profConcrList = getDirectoryUsersInGroup(this.dirRoomLevelProfConcrGrp, pfConcurrentGroup);
		    	 }
		    	 batchInputGroup = VWSPreferences.getRoomsBatchInputAdministrator(room);
			     VWSLog.add("Batch Input Administrator group Name ======> "+batchInputGroup);
			     if (batchInputGroup != null && batchInputGroup.trim().length() > 0) {
			    	 batchInputList = getDirectoryUsersInGroup(this.dirRoomLevelBatchInputAdminGrp, batchInputGroup);
			    	 VWSLog.add("Batch Input Administrator group users List :::::::::::"+batchInputList);   	
			     }
		    //}
		    }
		    int syncGrpCount = 0;
		    String param = null;
			if (getAdminGrpCount() != -1) {
				VWSLog.dbg("getDirAdminGrpCount() from directory for this group::" + getDirAdminGrpCount());
		    	VWSLog.dbg("getAdminGrpCount() from license file for this group::" + getAdminGrpCount());
		    	param = "4" + Util.SepChar + adminName + Util.SepChar;
		    	DoDBAction.get(database,"GroupToUser_Get", param, ret);
		    	VWSLog.dbg("ret val from grouptouser_get for admin group :"+ adminName + " param 4 is "+ret.size());
				syncGrpCount = getAdminGrpCount() - ret.size();
		    	VWSLog.dbg("getAdminGrpCount and admin list size: "+syncGrpCount + " , " + adminList.size());
				if (syncGrpCount < getDirAdminGrpCount()) { //getDirAdminGrpCount() is common for hosting and !hosting
					for (int i = 0; i < syncGrpCount; i++) {
						groupUsers.add(adminList.get(i));
					}
					String message = ResourceManager.getDefaultManager().getString("SyncMsg.AdminGroup");
					VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
					VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.AdminGroup"));
				} else {
					groupUsers.addAll(adminList);
				}
				ret.removeAllElements();
				VWSLog.dbg("groupUsers after adding admin:::::::::::"+groupUsers);
			}
			
			if (getSubAdminGrpCount() != -1) {
				VWSLog.dbg("getDirSubAdminGrpCount() from directory for this group::" + getDirSubAdminGrpCount());
		    	VWSLog.dbg("getSubAdminGrpCount() from license file for this group::" + getSubAdminGrpCount());
		    	param = "4" + Util.SepChar + subAdminGrpName + Util.SepChar;
		    	DoDBAction.get(database,"GroupToUser_Get", param, ret);
		    	VWSLog.dbg("ret val from grouptouser_get for sub admin group :"+ subAdminGrpName + " param 4 is "+ret.size());
				syncGrpCount = getSubAdminGrpCount() - ret.size();
		    	VWSLog.dbg("getAdminGrpCount and admin list size: "+syncGrpCount + " , " + subAdminList.size());
				if (syncGrpCount < getDirSubAdminGrpCount()) {
					for (int i = 0; i < syncGrpCount; i++) {
						groupUsers.add(subAdminList.get(i));
					}
					String message = ResourceManager.getDefaultManager().getString("SyncMsg.subAdminGroup");
					VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
					VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.subAdminGroup"));
				} else {
					groupUsers.addAll(subAdminList);
				}
				ret.removeAllElements();
				VWSLog.dbg("groupUsers after adding sub admin group:::::::::::"+groupUsers);
			}
		     
			if (getNamedGrpCount() != -1) {
				VWSLog.dbg("Named group count from directory for this group::" + namedList.size());
				VWSLog.dbg("getNamedGrpCount() from license file for this group::" + getNamedGrpCount());
				// VWSLog.dbg("loopCount value::" +loopCount);
				param = "4" + Util.SepChar + namedGroup + Util.SepChar;
				DoDBAction.get(database, "GroupToUser_Get", param, ret);
				VWSLog.dbg("ret val from grouptouser_get for named group :" + namedGroup + " param 4 is " + ret.size());
				syncGrpCount = getNamedGrpCount() - ret.size();
				VWSLog.dbg("syncNamedGrpCount and Named list size: " + syncGrpCount + " , " + namedList.size());
				// if (!isHosting) {
				if ((syncGrpCount < namedList.size())) { //getDirNamedGrpCount() is for hosting and getDirRoomLevelNamedGrpCount() is for !hosting. namedList.size() is common for both
					for (int i = 0; i < syncGrpCount; i++) {
						VWSLog.dbg("Inside syncNamedGrpCount<namedList.size() condition::");
						groupUsers.add(namedList.get(i));
					}
					String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
					VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
					VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
				} else {
					groupUsers.addAll(namedList);
				}
		    	/*} else {
		    		 if (getDirRoomLevelNamedGrpCount() > syncNamedGrpCount){
			    		 for(int i=0;i<syncNamedGrpCount;i++){				    		 
			    			 groupUsers.add(roomLevelNamedList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
			    	 } else { 
			    		 groupUsers.addAll(roomLevelNamedList);
			    	 }
		    	}*/
				ret.removeAllElements();
		    	VWSLog.dbg("groupUsers after adding named:::::::::::"+groupUsers);
		    }//end
		    if (getNamedOnlineGrpCount() != -1) {
		    	
		    	 VWSLog.dbg("Named online group count from directory for this group::" + namedOnlineList.size());
		    	 VWSLog.dbg("getNamedOnlineGrpCount from license file for this group::" + getNamedOnlineGrpCount());
		    	 param = "4" + Util.SepChar + namedOnlineGroup + Util.SepChar;
				 DoDBAction.get(database, "GroupToUser_Get", param, ret);
				 VWSLog.dbg("ret val from grouptouser_get for named online group :" + namedOnlineGroup + " param 4 is " + ret.size());
				 syncGrpCount = getNamedOnlineGrpCount() - ret.size();
				 VWSLog.dbg("syncNamedGrpCount and Named list size: " + syncGrpCount + " , " + namedList.size());
		    	 
		    	 //if (!isHosting) {
					if (namedOnlineList.size() > syncGrpCount) {//getDirNamedOnlineGrpCount() is for hosting and getDirRoomLevelNamedOnlineGrpCount() is for !hosting. namedOnlineList.size() is common for both
						for (int i = 0; i < syncGrpCount; i++) {
							groupUsers.add(namedOnlineList.get(i));
						}
						String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedOnlineGroup");
						VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
						VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedOnlineGroup"));
	
					} else {
						groupUsers.addAll(namedOnlineList);
					}
		    	 /*} else {
			    	 if (getDirRoomLevelNamedOnlineGrpCount() > getNamedOnlineGrpCount()){
			    		 for(int i=0;i<getNamedOnlineGrpCount();i++){
			    			 groupUsers.add(roomLevelNamedOnlineList.get(i));
			    			 
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.NamedGroup"));
			    	 } else { 
			    		 groupUsers.addAll(roomLevelNamedOnlineList);
			    	 }
		    	 }*/
				 ret.removeAllElements();
		    	 VWSLog.dbg("groupUsers after adding named online:::::::::::"+groupUsers);
		    }
		    
			if (getConcurrentGrpCount() != -1) {
				VWSLog.dbg("Concurrent group count from directory for this group::" + concurrentList.size());
				VWSLog.dbg("getConcurrentGrpCount() from license file for this group::" + getConcurrentGrpCount());

				//if (!isHosting) {
					//getDirConcurrentGrpCount() is for hosting and getDirRoomLevelConcurrentGrpCount() is for !hosting. concurrentList.size() is common for both
					for (int i = 0; i < concurrentList.size(); i++) {
						groupUsers.add(concurrentList.get(i));
					}
				/*} else {
					// Commented date Date :-3/8/2017 BY :- madhavan
					if (getDirRoomLevelConcurrentGrpCount() > getConcurrentGrpCount()) {
						for (int i = 0; i < getConcurrentGrpCount(); i++) {
							groupUsers.add(roomLevelconcurrentList.get(i));
						}
						String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup");
						VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
						VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup"));
					} else {
						groupUsers.addAll(roomLevelconcurrentList);
					}
				}*/
				VWSLog.dbg("groupUsers after adding concurrent:::::::::::"+groupUsers);
			}
			
			if (getConcurrentOnlineGrpCount() != -1) {
				//if (!isHosting) {
					//getDirConcurrentOnlineGrpCount() is for hosting and getDirRoomLevelConcurrentOnlineGrpCount() is for !hosting. concurrentOnlineList.size() is common for both
					for (int i = 0; i < concurrentOnlineList.size(); i++) {
						groupUsers.add(concurrentOnlineList.get(i));
					}
				/*} /else {
					if (getDirRoomLevelConcurrentOnlineGrpCount() > getConcurrentOnlineGrpCount()) {
						for (int i = 0; i < getConcurrentOnlineGrpCount(); i++) {
							groupUsers.add(roomLevelconcurrentList.get(i));
						}
						String message = ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup");
						VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
						VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ConcurrentGroup"));
					} else {
						groupUsers.addAll(roomLevelconcurrentList);						
					}
				} */
				VWSLog.dbg("groupUsers after adding concurrent online:::::::::::"+groupUsers);
			}
			if (getProfessionalNamedGrpCount() != -1) {
		    	 VWSLog.dbg("Professional named group count from directory for this group::" + profesionalNamedList.size());
		    	 VWSLog.dbg("getProfessionalNamedGrpCount() from license file for this group::" + getProfessionalNamedGrpCount());
	
		    	 param = "4" + Util.SepChar + profNamedGroup + Util.SepChar;
		    	 DoDBAction.get(database,"GroupToUser_Get",param, ret);
		    	 VWSLog.dbg("professRet ::"+ret);
		    	 syncGrpCount = getProfessionalNamedGrpCount()-ret.size();
		    	 VWSLog.dbg("syncProfessionalNamedGrpCount ::"+syncGrpCount);
		    	 //if (!isHosting) {
		    		 VWSLog.dbg("profesionalNamedList :"+profesionalNamedList);
		    		//getDirProfessionalNamedGrpCount() is for hosting and getDirRoomLevelProfessionalNamedGrpCount() is for !hosting. profesionalNamedList.size() is common for both
		    		 if(syncGrpCount < profesionalNamedList.size()){
						 for (int i = 0; i < syncGrpCount; i++) {
							VWSLog.dbg("Inside syncGrpCount < profesionalNamedList.size() condition::");
							groupUsers.add(profesionalNamedList.get(i));
						 }
		    			 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
	    				 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
	    				 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
		    		 } else {
	    				 groupUsers.addAll(profesionalNamedList);
		    		 }
		    		 VWSLog.dbg("users===============> :"+users);
		    		 VWSLog.dbg("groupUsers==========> :"+groupUsers);
		    	 /*} else {
			    	 if(getDirRoomLevelProfessionalNamedGrpCount() > syncProfessionalNamedGrpCount){
			    		 for(int i=0;i<syncProfessionalNamedGrpCount;i++){
			    			 groupUsers.add(roomLevelProfessionalNamedList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalNamedGroup"));
			    	 } else {
			    		 groupUsers.addAll(roomLevelProfessionalNamedList);
			    	 }
		    	 }*/
		    	 ret.removeAllElements();
		    	 VWSLog.dbg("groupUsers after professional named:::::::::::"+groupUsers);
		     }		   
		     if (getProfessConcrGrpCount() != -1) {
				 VWSLog.dbg("Professional concurrent count from directory for this group::" + profConcrList.size());
				 VWSLog.dbg("getProfessConcrGrpCount() from license file for this group::" + getProfessConcrGrpCount());
	
		    	 //if (!isHosting) {
				 	 //getDirProfConcrGrpCount() is for hosting and getDirRoomLevelProfConcrGrpCount() is for !hosting. profConcrList.size() is common for both
					 for(int i=0;i<profConcrList.size();i++){				    	
		    		 	groupUsers.add(profConcrList.get(i));
			    	 }				    	
		    	 /*} else {
		    		 if(getDirRoomLevelProfConcrGrpCount() > getProfessConcrGrpCount()){
			    		 for(int i=0;i<getProfessConcrGrpCount();i++){
			    			 groupUsers.add(roomLevelprofConcrList.get(i));
			    		 }
			    		 String message = ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalConcurrentGroup");
			    		 VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
			    		 VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.ProfessionalConcurrentGroup"));
			    	 } else {
			    		 groupUsers.addAll(roomLevelprofConcrList);
			    	 }
		    	 }*/
		    	 VWSLog.dbg("groupUsers after adding professional concurrent:::::::::::"+groupUsers);
		     }
		     if (getBatchInputGrpCount() != -1) {
					VWSLog.dbg("getDirBatchInputGrpCount() from directory for this group::" + getDirBatchInputGrpCount());
			    	VWSLog.dbg("getBatchInputGrpCount() from license file for this group::" + getBatchInputGrpCount());
			    	param = "4" + Util.SepChar + batchInputGroup + Util.SepChar;
			    	DoDBAction.get(database,"GroupToUser_Get", param, ret);
			    	VWSLog.dbg("ret val from grouptouser_get for batch input administrator group :"+ batchInputGroup + " param 4 is "+ret.size());
					syncGrpCount = getBatchInputGrpCount() - ret.size();
			    	VWSLog.dbg("getBatchInputGrpCount and batch list size: "+syncGrpCount + " , " + batchInputList.size());
					if (syncGrpCount < getDirBatchInputGrpCount()) {
						for (int i = 0; i < syncGrpCount; i++) {
							groupUsers.add(batchInputList.get(i));
						}
						String message = ResourceManager.getDefaultManager().getString("SyncMsg.subAdminGroup");
						VWMail.sendMailNotificationOnVSM(message, VWSConstants.VWS);
						VWSLog.errMsg(ResourceManager.getDefaultManager().getString("SyncMsg.subAdminGroup"));
					} else {
						groupUsers.addAll(batchInputList);
					}
					ret.removeAllElements();
					VWSLog.dbg("groupUsers after adding sub admin group:::::::::::"+groupUsers);
				}
		   //---------------CV10.1 License end----------------------------------------------------//			     
		} catch (Exception e) {
			VWSLog.dbg("Exception in getRegisteredGroupUsersList : "+e.getMessage());
		}
		return groupUsers;
	}
	
	private List getDirectoryUsersInGroup(List usersList, String groupName) {
		List dirUsersList = new Vector();
		VWSLog.dbg("Directory Users List ===> "+usersList);
		//usersList variable will not be empty bz it has already intialized in DirectorySyn() constructor.But by any chance if it is empty then have to pull users from directory
		if (usersList != null) {
			dirUsersList = usersList;
	    } else { 
	    	dirUsersList = getUsersInGroup(groupName);
	    }
		return dirUsersList;
	}
	
	/**
	 * CV10.2 This method is used to show the error message of deleted users which is available in DB.
	 * @param users
	 * @param schemeType
	 */
	private void showMsgForDeletedUsers(List users, String schemeType)
    {
    	VWSLog.dbg("Users list who is not available in the directory : "+users);
        Creator user = null;
        //String userName = null;
        Iterator iterator = users.iterator();
        List directoryUsers = directory.getUsers(true);
        VWSLog.dbg("Directory UsersList : "+directoryUsers);
        while (iterator.hasNext())
        {
            user = (Creator) iterator.next();
            String userName = user.getName();
            if (userName.contains("@"))
            	userName = userName.substring(0, userName.indexOf("@"));
            VWSLog.dbg("Check user available in directory :"+directoryUsers.contains(userName));
            if (!directoryUsers.contains(userName)) {
	            if (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
	            	VWSLog.errMsg("User " +userName+ " not available in LDAP" );    
	            } else {
	            	VWSLog.errMsg("User " +userName+ " not available in AD" );
	            }
            }
        }
    }
	/**
	 * CV10.2 This method is used to show error message of deleted groups which is available in DB. 
	 * @param groupDataObjects
	 * @param schemeType
	 */
	private void showMsgForDeletedGroups(List groupDataObjects, String schemeType)
    {
		VWSLog.dbg("Groups list which is not available in the directory : "+groupDataObjects);
		Group group = null;
        String groupName = null;
        Iterator iterator = groupDataObjects.iterator();
        while (iterator.hasNext())
        {
            group = (Group) iterator.next();
            groupName = group.getName();
            if (schemeType.equalsIgnoreCase(Constants.SCHEME_LDAP_ADS)) {
            	VWSLog.errMsg("Group " +groupName+ " not available in LDAP" );    
            } else {
            	VWSLog.errMsg("Group " +groupName+ " not available in AD" );
            }
        }
    }
	
	/**
	 * CV10.2 This method is used to get search filter text
	 * @param searchForName
	 * @return
	 */
	private String getSearchFilter(String searchForName) {
    	String filter = null;
    	String searchContent = null;
    	if (searchForName.trim().toUpperCase().contains("OBJECTCLASS=") || searchForName.trim().toUpperCase().contains("CN=")
			|| searchForName.trim().toUpperCase().contains("DC=")) {
    		//filter = searchForName;
    		String searchText[] = searchForName.split(",");
    		if (searchText.length > 0) {
    			filter = "";
				for (int index = 0; index < searchText.length; index++) {
					/*-----Framing objectClass search-----*/
					searchContent = searchText[index];
	    			if (searchContent.trim().equalsIgnoreCase("objectClass=groups") 
	    					|| searchContent.trim().equalsIgnoreCase("objectClass=group")) {
	    				filter = "(objectClass=group)";
	    			} else if (searchContent.trim().equalsIgnoreCase("objectClass=users") 
	    					|| searchContent.trim().equalsIgnoreCase("objectClass=user")
	    					|| searchContent.trim().equalsIgnoreCase("objectClass=person")) {
	    				filter = "(objectClass=Person)";
	    			} else {
	    				/*-----Framing CN,DC search-----*/
	    				if (searchContent.trim().toLowerCase().contains("cn=")) {
	    					filter = filter +"(CN="+(searchContent.substring(searchContent.toLowerCase().indexOf("=")+1, searchContent.length()))+")";
	    				} else if (searchContent.trim().toLowerCase().contains("dc=")) {
	    					filter = filter +"(DC="+(searchContent.substring(searchContent.toLowerCase().indexOf("=")+1, searchContent.length()))+")";
	    				}
	    			}
				}
    		}    		
    	}
    	return filter;
    }
	
	/**
	 * CV10.2  This method is for getting all the registered group from contentverse server settings and add these groups into the group list
	 * @return
	 */
	private Vector<String> getServerSettingsGroup_old() {
		Vector<String> groupsList = new Vector<String>();
		boolean isHosting = false;
	    try{
	    	isHosting = CVPreferences.getHosting();
	    }catch (Exception e) {
	    	isHosting = false;
	    }
		VWSLog.dbg("getServerSettingsGroup ");
		VWSLog.dbg("AdminGroup ===>"+this.adminGroup +" NamedGroup ===>"+this.namedGroup+" ProfessionalNamedGroup ===>"+this.professionalNamedGroup);
		VWSLog.dbg("ConcurrentGroup ===>"+this.concurrentGroup +" ProfessionalConcrGroup ===>"+this.professionalConcrGroup);
		if (getAdminGroup() != null && getAdminGroup().trim().length() > 0)
			groupsList.add(getAdminGroup());
		if (!isHosting) {
			if (this.namedGroup != null && this.namedGroup.trim().length() > 0)
				groupsList.add(this.namedGroup);
			if (this.namedOnlineGroup != null && this.namedOnlineGroup.trim().length() > 0)
				groupsList.add(this.namedOnlineGroup);
			if (this.professionalNamedGroup != null && this.professionalNamedGroup.trim().length() > 0)
				groupsList.add(this.professionalNamedGroup);
			if (this.concurrentGroup != null && this.concurrentGroup.trim().length() > 0)
				groupsList.add(this.concurrentGroup);
			if (this.concurrentOnlineGroup != null && this.concurrentOnlineGroup.trim().length() > 0)
				groupsList.add(this.concurrentOnlineGroup);
			if (this.professionalConcrGroup != null && this.professionalConcrGroup.trim().length() > 0)
				groupsList.add(this.professionalConcrGroup);
		} else {
			String groupName = VWSPreferences.getRoomLevelNamed(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.add(groupName);
			}
			groupName = VWSPreferences.getRoomLevelNamedOnline(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.add(groupName);
			}
			groupName = VWSPreferences.getRoomLevelProfessionalNamed(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.add(groupName);
			}
			groupName = VWSPreferences.getRoomLevelConcurrent(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.add(groupName);
			}
			groupName = VWSPreferences.getRoomLevelConcurrentOnline(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.add(groupName);
			}
			groupName = VWSPreferences.getRoomLevelProfConcurrent(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.add(groupName);
			}
		}
		VWSLog.dbg("getServerSettingsGroup groupsList "+groupsList);
		return groupsList;
	}
	
	/**
	 * CV10.2  This method is for getting all the registered group from contentverse server settings and add these groups into the group list
	 * Values : AM-Admin Group/ ND-Named Desktop/ NO-Named Online/ PN-Professional Named/ CD-Concurrent Desktop/ CO-Concurrent Online/ PC-Professional Concurrent
	 * @return
	 */
	private HashMap<String, String> getServerSettingsGroup() {
		HashMap<String, String> groupsList = new HashMap<String, String>();
		boolean isHosting = false;
	    try{
	    	isHosting = CVPreferences.getHosting();
	    }catch (Exception e) {
	    	isHosting = false;
	    }
		VWSLog.dbg("getServerSettingsGroup ");
		VWSLog.dbg("AdminGroup ===>"+this.adminGroup +" NamedGroup ===>"+this.namedGroup+" ProfessionalNamedGroup ===>"+this.professionalNamedGroup);
		VWSLog.dbg("ConcurrentGroup ===>"+this.concurrentGroup +" ProfessionalConcrGroup ===>"+this.professionalConcrGroup);
		if (getAdminGroup() != null && getAdminGroup().trim().length() > 0)
			groupsList.put("AM", this.adminGroup);
		if (getSubAdminGroup() != null && getSubAdminGroup().trim().length() > 0)
			groupsList.put("SA", this.subAdminGroup);
		if (!isHosting) {
			if (this.namedGroup != null && this.namedGroup.trim().length() > 0)
				groupsList.put("ND", this.namedGroup);
			if (this.namedOnlineGroup != null && this.namedOnlineGroup.trim().length() > 0)
				groupsList.put("NO", this.namedOnlineGroup);
			if (this.professionalNamedGroup != null && this.professionalNamedGroup.trim().length() > 0)
				groupsList.put("PN", this.professionalNamedGroup);
			if (this.concurrentGroup != null && this.concurrentGroup.trim().length() > 0)
				groupsList.put("CD", this.concurrentGroup);
			if (this.concurrentOnlineGroup != null && this.concurrentOnlineGroup.trim().length() > 0)
				groupsList.put("CO", this.concurrentOnlineGroup);
			if (this.professionalConcrGroup != null && this.professionalConcrGroup.trim().length() > 0)
				groupsList.put("PC", this.professionalConcrGroup);
			if (this.batchInputGroup != null && this.batchInputGroup.trim().length() > 0)
				groupsList.put("BI", this.batchInputGroup);
		} else {
			String groupName = VWSPreferences.getRoomLevelNamed(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.put("ND", groupName);
			}
			groupName = VWSPreferences.getRoomLevelNamedOnline(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.put("NO", groupName);
			}
			groupName = VWSPreferences.getRoomLevelProfessionalNamed(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.put("PN", groupName);
			}
			groupName = VWSPreferences.getRoomLevelConcurrent(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.put("CD", groupName);
			}
			groupName = VWSPreferences.getRoomLevelConcurrentOnline(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.put("CO", groupName);
			}
			groupName = VWSPreferences.getRoomLevelProfConcurrent(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.put("PC", groupName);
			}
			groupName = VWSPreferences.getRoomLevelBatchInputAdministrator(room);
			if (groupName != null && groupName.trim().length() > 0) {
				groupsList.put("BI", groupName);
			}
		}
		VWSLog.dbg("getServerSettingsGroup groupsList "+groupsList);
		return groupsList;
	}
	
	/**
	 * CV10.2 This method is used to delete the Non registered group's GroupToUser entry
	 * @param groupDataObjects
	 * @param groupsList 
	 */
	private void deleteGroupToUserEntryForNonRegisteredGroups(List<Group> groupDataObjects, Vector<String>groupsList) {
		try {
			VWSLog.dbg("Deleting non registered entry from GroupToUser Table Stated......");
			Iterator iterator = groupDataObjects.iterator();
			Group group = null;
			String groupName = null;
			int groupID = 0;
			List groupToUserDataObjects = null;
			boolean isExistInRegisteredGroupList = false;
	        while (iterator.hasNext())
	        {
	        	group = (Group) iterator.next();
	        	groupName = group.getName();	
	        	groupID = group.getId();
	        	VWSLog.dbg("groupName : "+groupName);
	        	isExistInRegisteredGroupList = false;
	        	for (String groupNameInList : groupsList) {
	        		//if (groupsList.contains(groupName)) { // groupsList entry will get deleted in syncUsersInGroup() method.
	        		if (groupNameInList.equalsIgnoreCase(groupName)) {
	        			isExistInRegisteredGroupList = true;
		        	}
	        	}
	        	VWSLog.dbg("is group exist in Registered group list : "+isExistInRegisteredGroupList);
	        	if (!isExistInRegisteredGroupList) {
	        		groupToUserDataObjects = getUsersInGroup(groupID); //Gets GroupToUser relationship from table.
	        		removeGroupToUsers(groupToUserDataObjects);
	        		VWSLog.dbg("GroupToUser entry has deleted for groupName :"+groupName+" and groupID :"+groupID); 
	        	}
	        } 
		} catch (Exception e) {
			VWSLog.dbg("Exception in deleteGroupToUserEntryForNonRegisteredGroups :"+e.getMessage());   
		}
	}
	
	/**
	 * This method used to get group names from the groups hashmap list
	 * @param groupsWithKey
	 * @return
	 */
	private Vector getGroupsNameFromGroupsWithKeyList(HashMap<String, String> groupsWithKeys) {
		Vector groups = new Vector();
		boolean isHosting = false;
	    try{
	    	isHosting = CVPreferences.getHosting();
	    }catch (Exception e) {
	    	isHosting = false;
	    }
	    String namedGrp = null, namedOnlineGrp = null, professionalNamedGrp = null;
	    if (!isHosting) {	    	
	    	namedGrp = getNamedGroup();
	    	namedOnlineGrp = getNamedOnlineGroup();
	    	professionalNamedGrp = getProfessionalNamedGroup();
	    } else {
	    	namedGrp = VWSPreferences.getRoomLevelNamed(room);
	    	namedOnlineGrp = VWSPreferences.getRoomLevelNamedOnline(room);
	    	professionalNamedGrp = VWSPreferences.getRoomLevelProfessionalNamed(room);
	    }
    	
    	int namedDesktopLicenseCount = getNamedGrpCount();	
		VWSLog.dbg("namedDesktopLicenseCount : " + namedDesktopLicenseCount);
		
		int namedOnlineLicenseCount = getNamedOnlineGrpCount();	
		VWSLog.dbg("namedOnlineLicenseCount: " + namedOnlineLicenseCount);
		
		int profNamedLicenseCount = getProfessionalNamedGrpCount();
		VWSLog.dbg("profNamedLicenseCount: " + profNamedLicenseCount);
    	
		boolean isSyncRequired = true;
		String keysToRemove = null;
		if (groupsWithKeys != null && groupsWithKeys.size() > 0) {
			for (String key : groupsWithKeys.keySet()) {
    		    VWSLog.dbg("Key = " + key + ", Value = " + groupsWithKeys.get(key));
				isSyncRequired = checkSyncRequired(key, groupsWithKeys.get(key), namedGrp, namedOnlineGrp,
						professionalNamedGrp, namedDesktopLicenseCount, namedOnlineLicenseCount, profNamedLicenseCount);
				VWSLog.dbg("isSyncRequired >>>>> "+isSyncRequired);
				if (isSyncRequired) {
					groups.add(groupsWithKeys.get(key));
				} else {					
					if (keysToRemove == null) 
						keysToRemove = key;
					else
						keysToRemove = keysToRemove + "," +key;
				}
    		}
			VWSLog.dbg("keysToRemove........"+keysToRemove);
			if (keysToRemove != null) {
				String[] keysList = keysToRemove.split(",");
				for (String keyName : keysList) {
					VWSLog.dbg("key to remove from hashmap :"+keyName);
					groupsWithKeys.remove(keyName);
				}
			}		
			VWSLog.dbg("final groupsWithKeys........"+groupsWithKeys);
		}
		return groups;
	}
	
	/**
	 * This method will check whether NamedDesktop, NamedOnlineLine and ProfessionalNamed
	 * groups names are same. If it is same then this method will return true for group which is having maximum license count.
	 * Reason : If the NamedDesktop, NamedOnlineLine and ProfessionalNamed groups names are same, then UserSync should happen only one time instead of 3 times
	 * @param groupCategory
	 * @return
	 */
	private boolean checkSyncRequired(String groupCategory, String groupName, String namedGrp, String namedOnlineGrp,
			String professionalNamedGrp, int namedDesktopLicenseCount, int namedOnlineLicenseCount,
			int profNamedLicenseCount) {
		boolean isSyncRequired = true;
		/**ND - Named Desktop, NO - Named Online, PN - Professional Named***/
		if ((groupCategory.equalsIgnoreCase("ND") && groupName.equalsIgnoreCase(namedGrp))) {
				/*|| (groupCategory.equalsIgnoreCase("NO") && groupName.equalsIgnoreCase(namedOnlineGrp))
				|| (groupCategory.equalsIgnoreCase("PN") && groupName.equalsIgnoreCase(professionalNamedGrp))) {*/
			if (namedGrp.equalsIgnoreCase(namedOnlineGrp)) {
				if (namedDesktopLicenseCount < namedOnlineLicenseCount) {
					isSyncRequired = false;
					return isSyncRequired;
				}
			}
			if (namedGrp.equalsIgnoreCase(professionalNamedGrp)) {
				if (namedDesktopLicenseCount < profNamedLicenseCount) {
					isSyncRequired = false;
					return isSyncRequired;
				}
			}
		}
		if (groupCategory.equalsIgnoreCase("NO") && groupName.equalsIgnoreCase(namedOnlineGrp)) {
			if (namedOnlineGrp.equalsIgnoreCase(namedGrp)) {
				if (namedOnlineLicenseCount <= namedDesktopLicenseCount) {
					isSyncRequired = false;
					return isSyncRequired;
				}
			}
			if (namedOnlineGrp.equalsIgnoreCase(professionalNamedGrp)) {
				if (namedOnlineLicenseCount < profNamedLicenseCount) {
					isSyncRequired = false;
					return isSyncRequired;
				}
			}
		}
		if ((groupCategory.equalsIgnoreCase("PN") && groupName.equalsIgnoreCase(professionalNamedGrp))) {
			if (professionalNamedGrp.equalsIgnoreCase(namedGrp)) {
				if (profNamedLicenseCount <= namedDesktopLicenseCount) {
					isSyncRequired = false;
					return isSyncRequired;
				}
			}
			if (professionalNamedGrp.equalsIgnoreCase(namedOnlineGrp)) {
				if (profNamedLicenseCount <= namedOnlineLicenseCount) {
					isSyncRequired = false;
					return isSyncRequired;
				}
			}
		}
		return isSyncRequired;
	}
	/**End of CV10.2 merges****/
	/**
	 * CV2019 - This check is required for non registered group user. If the user is already added from the administration then only have to consider that user for usersync 
	 * @param dbUsers
	 * @param userName
	 * @return
	 */
	private int checkUserExistInDatabase(List dbUsers, String userName) {
		int userID = 0;
		Creator user = null;
		String dbUserName = null;
		for (int loopCount = 0; loopCount < dbUsers.size(); loopCount++) {
			user = (Creator) dbUsers.get(loopCount);
			dbUserName = user.getName();
			if (dbUserName.contains("@"))
				dbUserName = dbUserName.substring(0, dbUserName.indexOf("@"));
			VWSLog.dbg("dbUserName >>>> "+dbUserName+" userName >>>> "+userName);
			if (dbUserName.equalsIgnoreCase(userName)) {
				userID = user.getId();
				break;
			}
		}
		return userID;
	}
	public boolean isSyncFailed() {
		return syncFailed;
	}
	public void setSyncFailed(boolean syncFailed) {
		this.syncFailed = syncFailed;
	}
	public String getSubAdminGroup() {
		return subAdminGroup;
	}
	public void setSubAdminGroup(String subAdminGroup) {
		this.subAdminGroup = subAdminGroup;
	}
	public int getSubAdminGrpCount() {
		return subAdminGrpCount;
	}
	public void setSubAdminGrpCount(int subAdminGrpCount) {
		this.subAdminGrpCount = subAdminGrpCount;
	}
	public int getDirSubAdminGrpCount() {
		return dirSubAdminGrpCount;
	}
	public void setDirSubAdminGrpCount(int dirSubAdminGrpCount) {
		if (dirSubAdminGrpCount == 0) {
			if (getSubAdminGroup() != null && getSubAdminGroup().trim().length() > 0) {
				List subAdminList = getUsersInGroup(getSubAdminGroup());
				this.dirSubAdminGrp = subAdminList;
				if (subAdminList != null && subAdminList.size() > 0) {
					this.dirSubAdminGrpCount = subAdminList.size();
				}
			}
		} else {
			this.dirSubAdminGrpCount = dirSubAdminGrpCount;
		}
	}
	public String getBatchInputGroup() {
		return batchInputGroup;
	}
	public void setBatchInputGroup(String batchInputGroup) {
		this.batchInputGroup = batchInputGroup;
	}
	public int getBatchInputGrpCount() {
		return batchInputGrpCount;
	}
	public void setBatchInputGrpCount(int batchInputGrpCount) {
		this.batchInputGrpCount = batchInputGrpCount;
	}
	public int getDirBatchInputGrpCount() {
		return dirBatchInputGrpCount;
	}
	public void setDirBatchInputGrpCount(int dirBatchInputGrpCount) {
		if (dirBatchInputGrpCount == 0) {
			if (getBatchInputGroup() != null && getBatchInputGroup().trim().length() > 0) {
				List batchInputList = getUsersInGroup(getBatchInputGroup());
				this.dirBatchInputGrp = batchInputList;
				if (batchInputList != null && batchInputList.size() > 0) {
					this.dirSubAdminGrpCount = batchInputList.size();
				}
			}
		} else {
			this.dirBatchInputGrpCount = dirBatchInputGrpCount;
		}
	}
	public int getDirRoomLevelBatchInputAdminGrpCount() {
		return dirRoomLevelBatchInputAdminGrpCount;
	}
	public void setDirRoomLevelBatchInputAdminGrpCount(int dirRoomLevelBatchInputAdminGrpCount) {
		if (dirRoomLevelBatchInputAdminGrpCount == 0 ){
			String roomLevelBatchAdmin = VWSPreferences.getRoomsBatchInputAdministrator(room);
			if (roomLevelBatchAdmin != null && roomLevelBatchAdmin.trim().length() > 0) {
			    List batchAdminList = getUsersInGroup(roomLevelBatchAdmin);
			    this.dirRoomLevelBatchInputAdminGrp = batchAdminList;
			    if (batchAdminList != null && batchAdminList.size() > 0){
			    	this.dirRoomLevelBatchInputAdminGrpCount = batchAdminList.size();
			    }
			}
		}else{
			this.dirRoomLevelBatchInputAdminGrpCount = dirRoomLevelBatchInputAdminGrpCount;
		}
	}
}
