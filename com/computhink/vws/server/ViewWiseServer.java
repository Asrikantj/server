/*
 * ViewWiseServer.java
 *
 * Created on December 9, 2003, 6:03 PM
 */
package com.computhink.vws.server;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.TimerTask;

import com.computhink.common.*;
import com.computhink.dss.server.DSSPreferences;
import com.computhink.manager.ManagerUtil;
/**
 *
 * @author  Administrator
 */
public class ViewWiseServer implements VWSConstants
{
    public static void main(String args[]) 
    {
        if (VWSPreferences.getProxy())
            Util.RegisterProxy(VWSPreferences.getProxyHost(), 
                               VWSPreferences.getProxyPort(), SERVER_VWS);
	       String hostNameWithIP = VWSPreferences.getHostName();
	       String hostName = "";
	       String forceBindIP = "";
	       try{
	        int pos = -1;	       
	        if (hostNameWithIP != null && hostNameWithIP.trim().length() > 0){
	        	pos = hostNameWithIP.indexOf("|");
	        }
	        
	        if (pos > 0){
	        	forceBindIP = hostNameWithIP.substring(pos+1,hostNameWithIP.length());
	        	hostName = hostNameWithIP.substring(0,pos);
	        }else{
	        	hostName = hostNameWithIP;
	        }
	        if (hostName.length() > 0)
	        {
	            if (forceBindIP != null && forceBindIP.trim().length() > 0){
	            	System.setProperty("java.rmi.server.hostname", forceBindIP);
	            }else{
	            	System.setProperty("java.rmi.server.hostname", hostName);
	            }
	        }
	        }catch (Exception e) {
				// TODO: handle exception
	        	//System.out.println("Error :::: " + e.getMessage());
	        	e.printStackTrace();
	        	
		}
        ServerSchema s = Util.getMySchema();
        s.comport = VWSPreferences.getComPort();
        s.dataport = VWSPreferences.getDataPort();
        s.type = SERVER_VWS;
        if (forceBindIP != null && forceBindIP.trim().length() > 0){
    		s.address = forceBindIP;
    		System.setProperty("java.rmi.server.hostname", forceBindIP);       	
        }else if (!hostName.equals(s.address)){
            //If multiple IP address are existing in server environment, forced to bind the server given host name.
        	try{
        	InetAddress localhost = InetAddress.getLocalHost();
        	InetAddress[] allIps = InetAddress.getAllByName(localhost.getCanonicalHostName());       	
        	  if (allIps != null && allIps.length > 1) {
        	    for (int i = 0; i < allIps.length; i++) {        	    	
        	    	if (hostName.equals(allIps[i].getHostAddress())){
        	    		s.address = allIps[i].getHostAddress();
        	    		System.setProperty("java.rmi.server.hostname", hostName);
        	    	}
        	    }
        	  }
        	}catch (Exception e) {
				// TODO: handle exception
			}
        }
        
        if (args != null
				&& args.length > 0
				&& args[0].equalsIgnoreCase("shutdown"))
         {
             VWSLog.add("Stop " + PRODUCT_NAME + " Server @" + hostName + 
                                                 "[" + s.address + "]");
             try
             {   
                ((VWS) Util.getServer(s)).shutDown();
                return;
             }
         catch(Exception e){}
         }
        int port = Util.createServer(s);
        if ( port != 0)
        {
            VWSLog.add("Started " + PRODUCT_NAME+ " Server @" + hostName + 
                                                 "[" + s.address + "]:" + port);
            // This feature is disabled and need to change the functionality
            //createRestartScheduler();
            try
            {
                ((VWS) Util.getServer(s)).startWorkers();
            }
            catch(Exception e){}
        }
        else
            VWSLog.err("Could not start " + PRODUCT_NAME + " Server on port: " + s.comport);
        
    }    
}
