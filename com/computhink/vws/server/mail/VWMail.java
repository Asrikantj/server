/**
 * 
 */
package com.computhink.vws.server.mail;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.internet.MimeUtility;
import javax.swing.JOptionPane;

import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.vns.server.VNSConstants;
import com.computhink.vns.server.VNSLog;
import com.computhink.vns.server.VNSPreferences;
import com.computhink.vws.csserver.CSSPreferences;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.VWSLog;
import com.computhink.vws.server.VWSPreferences;
import com.computhink.vws.server.VWSUtil;
import com.computhink.resource.ResourceManager;

/**
 * @author Nishad Nambiar
 *
 */
public class VWMail implements VWSConstants{
	/*public static String NEW_LINE="\n";
	 public static String NEW_TAB="\t";
	 public static String HTML_NEW_LINE="<BR>";
	 public static String HTML_NEW_TAB="&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";*/
	private static ResourceManager resourceManager=ResourceManager.getDefaultManager();
	public VWMail(){
		mailServerName="";
		mailServerPort="";
		mailServerUsername="";
		mailServerPassword="";
		fromEmailId= "";
		
	}
	public String mailServerName;
	public String mailServerPort;
	public String mailServerUsername;
	public String mailServerPassword;
	public boolean isSSL;
	public String fromEmailId;
	
	
	public void setMailServerSettings(String mailServerName, String mailServerPort, String mailServerUsername, String mailServerPassword) {
		try{
			this.mailServerName = mailServerName;
			this.mailServerPort = mailServerPort;
			this.mailServerUsername = mailServerUsername;
			this.mailServerPassword =mailServerPassword; 
		}catch(Exception e){
		}
	}
	
	public void setMailServerSettings() {
		try{
			this.mailServerName = VWSPreferences.getEmailServerName();
			this.mailServerPort = VWSPreferences.getEmailServerPort();
			this.mailServerUsername = VWSPreferences.getEmailServerUsername();
			this.mailServerPassword = VWSPreferences.getEmailServerPassword();
			this.isSSL=(VWSPreferences.getSSLEnable().equalsIgnoreCase("true")?true:false); 
			this.fromEmailId = VWSPreferences.getFromEmailId();
			if(this.fromEmailId.trim().equals("")){
				this.fromEmailId = "administrator@" + PRODUCT_NAME + ".com";
			}
		}catch(Exception e){
		}
	}
	public void setNotificationMailServerSettings() {
		try{
			this.mailServerName = VNSPreferences.getEmailServerName();
			this.mailServerPort = VNSPreferences.getEmailServerPort();
			this.mailServerUsername = VNSPreferences.getEmailServerUsername();
			this.mailServerPassword = VNSPreferences.getEmailServerPassword();
			this.isSSL=VNSPreferences.getSSL();
			this.fromEmailId = VNSPreferences.getFromEmailId();
			if(this.fromEmailId.trim().equals("")){
				this.fromEmailId = "administrator@" + PRODUCT_NAME+ ".com";
			}
		}catch(Exception e){
		}
	}
	public void setContentSentinelMailServerSettings() {
		try{
			this.mailServerName = CSSPreferences.getEmailServerName();
			this.mailServerPort = CSSPreferences.getEmailServerPort();
			this.mailServerUsername = CSSPreferences.getEmailServerUsername();
			this.mailServerPassword = CSSPreferences.getEmailServerPassword();
			this.isSSL=CSSPreferences.getSSL();
			this.fromEmailId = CSSPreferences.getFromEmailId();
			if(this.fromEmailId.trim().equals("")){
				this.fromEmailId = "administrator@" + PRODUCT_NAME+ ".com";
			}
		}catch(Exception e){
		}
	}
	
	public void sendMail(String from, String to, String subject, String message){
		setMailServerSettings();
		String mailTo = to;//change to address here
		String mailFrom = from;//change from address here
		String host = this.mailServerName;//change mailserver address here
		String port= this.mailServerPort;
		Properties props = new Properties();
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
//		props.put("mail.debug", "true");
		Session session = Session.getInstance(props);
		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(mailFrom));
			InternetAddress[] address = {new InternetAddress(mailTo)};
			msg.setRecipients(Message.RecipientType.TO, address);
			msg.setSubject(subject);//change subject here
			msg.setSentDate(new Date());
			msg.setText(message);//change message here
			//Send the message
			Transport.send(msg);
		}
		catch (MessagingException mex) {
			
		}
	}
	
	//Not in use
	/*public int sendDRSMail(String userEmailID, String subject, File attachment, 
			String docName, String status){
		File attachmentArr[] = new File[1];
		attachmentArr[0] = attachment;
		return sendDRSMail(userEmailID, subject, attachmentArr, docName, status, null, "");
	}*/
	/*
	 * Desc   :The below method modified for customizing mail contents.
	 * Author :Nishad Nambiar
	 * Date   :30-Oct-2007
	 */
	/**
	 * CV10.1 Enhancement :- method added to split the string into two.
	 * @param aString
	 * @param aPattern1
	 * @param aPattern2
	 * @return
	 */
	public static String getStringBetweenStrings(String aString, String aPattern1, String aPattern2) {
	    String ret = null;
	    int pos1,pos2;

	    pos1 = aString.indexOf(aPattern1) + aPattern1.length();
	    pos2 = aString.indexOf(aPattern2);

	    if ((pos1>0) && (pos2>0) && (pos2 > pos1)) {
	        return aString.substring(pos1, pos2);
	    }

	    return ret;
	}

	public int sendDRSMail(String mailTo, String subject, File attachment[], String docName, String status,
			Vector docNames, String routeName, String webAccessReference, String roomName,
			String mailCcTo, HashMap<String, String> indexDetails) {
		if(mailTo == null || mailTo == "")
			return -2;
		String mailMessage = "";
		String msg = ""; 
		String subStatus = "";
		int statusId = 0;
		File messageFile = null;
		String message1 = null;
		try{
			String vwsHome = VWSUtil.getHome();
			File msgFile = null;
			VWSLog.dbg("Document Name : "+docName);
			VWSLog.dbg("Room Name : "+roomName);
			VWSLog.dbg("Route Name : "+routeName);
			VWSLog.dbg("status : "+status);
			if (status.startsWith("ARS")){
				subStatus = status.substring(3, status.length());
				statusId = Integer.parseInt(subStatus);
			}
			/****CV2019 merges from 10.2 line--------------------------------***/
			/********************This check is for to get route and room based message****************************/
			if ((status.startsWith("ARS") || status.equalsIgnoreCase(Route_Status_Pending)
					|| status.equalsIgnoreCase(Route_Status_Reject)
					|| status.equalsIgnoreCase(Route_Status_Notify_OnRptGen) 
					|| status.equalsIgnoreCase(Route_Status_End)
					|| status.equals(Route_Status_Approve))
					&& routeName != null && routeName.trim().length() > 0 && !routeName.equalsIgnoreCase("null")
					&& roomName != null && roomName.trim().length() > 0 && !roomName.equalsIgnoreCase("null")) {
				String messageFilePath = vwsHome + Util.pathSep + "lib" + Util.pathSep;
				messageFile = getRoomAndRouteBasedMessge(messageFilePath, roomName, routeName, status, statusId);				
			}
			/****************************************************************************************************/
			VWSLog.dbg("RetentionFile  :::  "+messageFile);
			if (messageFile != null && messageFile.exists()) {
				msgFile = messageFile;
			} else {
				if(status.equalsIgnoreCase(Route_Status_Pending))
					msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_ROUTE_PENDING);
				else if(status.equalsIgnoreCase(Route_Status_Reject))
					msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_ROUTE_REJECTED);
				else if(status.equalsIgnoreCase(Route_Status_End) || status.equalsIgnoreCase(Route_Status_Approve_End)||status.equalsIgnoreCase(Route_Status_PSR_End))
					msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_ROUTE_ENDED);
				else if(status.equalsIgnoreCase(Route_Status_Notify_OnRptGen)
						&& !subject.equals("Route_Status_OnEndVWR"))
					msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_EMAIL_REPORT);
				else if(status.equals(Route_Status_Approve))
					msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_ROUTE_APPROVED);
				else if (status.startsWith("ARS")){
					if(statusId == 6) {
						msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_RETENTION_MAIL);
					} else if (statusId == 1) {
						msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_RETENTION_NOTIFICATION);
					}
				}
				else {
					if (!subject.equals("Route_Status_OnEndVWR")) {
						msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_ROUTE_PENDING);
					}
				}
			}
			/*------------------End of 10.2 merges----------------------------*/
			if(msgFile!=null && msgFile.exists()){
				String line = "";
				FileInputStream fin = new FileInputStream(msgFile);
				BufferedReader bReader = new BufferedReader(new InputStreamReader(fin));
				while( (line = bReader.readLine()) != null){
					String actLine = "";
					if(status.startsWith("ARS")){
						actLine = line.replace("<DOC NAME>", "\"" + docName + "\"");
						actLine = actLine.replace("<RETENTION NAME>", "\"" + docName + "\"");
						mailMessage += actLine;
						mailMessage += HTML_NEW_LINE;
					}
					else{
						actLine = line.replace("<DOC NAME>", "\""+docName+"\"");
						mailMessage += actLine;
						mailMessage += HTML_NEW_LINE;
					}
					
				}
				/**
				 * CV2019 - Enhancement - Replacing of <<Index Name>> tag with index value added
				 */
				if (mailMessage.contains("<<") && mailMessage.contains(">>")) {
					mailMessage = replaceIndexName(mailMessage, indexDetails);
				}
				
				/**
				 * CV10.1 Enhancement :- codintion added to get the subject from rtf.
				 */
				if(mailMessage.contains("<SUBJECT>")){
					String subMessage=getStringBetweenStrings(mailMessage,"<SUBJECT>","<ENDSUBJECT>");
					subject=subject.replaceAll(subject,subMessage);
				}else{
					if(subject.equals("Route_Status_Pending")){
						String orginalSub=PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.DocIsPending1")+"\""+docName+"\""+VWSConstants.EMAIL_SUBJECT;
						subject=subject.replaceAll(subject,orginalSub);
					}else if(subject.equals("Route_Status_Reject")){
						String orginalSub=PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.RouteRej1")+"\""+docName+"\""+" "+resourceManager.getString("VWMailMsg.RouteRej2")+" "+resourceManager.getString("VWMailMessage.Reject");
						subject=subject.replaceAll(subject,orginalSub);
					}else if(subject.equals("Route_Status_End")){
						String orginalSub=resourceManager.getString("VWMailMessage.EndWorkFlow")+" "+resourceManager.getString("VWMailMsg.RouteEnd1")+" "+ PRODUCT_NAME+" "+resourceManager.getString("VWMailMsg.RouteEnd2")+"\""+docName+"\"";
						subject=subject.replaceAll(subject,orginalSub);
					}else if(subject.equals("Route_Status_Approve")){
						String orginalSub=PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.Approve1")+"\""+docName+"\""+resourceManager.getString("VWMailMsg.Approve2")+" "+resourceManager.getString("VWMailMessage.Approved");
						subject=subject.replaceAll(subject,orginalSub);
					}
				}
			} else {  // This will execute only in first time. Initially first time default document will not be there, have to create it.
				if(status.equals(Route_Status_Pending)){
					if(subject.equals("Route_Status_Pending")){
						mailMessage +="<SUBJECT>"+PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.DocIsPending1")+"<DOC NAME>"+VWSConstants.EMAIL_SUBJECT+"<ENDSUBJECT>";
						String orginalSub=PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.DocIsPending1")+"\""+docName+"\""+VWSConstants.EMAIL_SUBJECT;
						subject=subject.replaceAll(subject,orginalSub);
					}
					mailMessage += resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("VWMailMsg.DocIsPending1")+" "+"<DOC NAME>"+" "+resourceManager.getString("VWMailMsg.DocIsPending2");
				}
				else if(status.equals(Route_Status_Reject)){
					if(subject.equals("Route_Status_Reject")){
						mailMessage +="<SUBJECT>"+PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.RouteRej1")+"<DOC NAME>"+" "+resourceManager.getString("VWMailMsg.RouteRej2")+" "+resourceManager.getString("VWMailMessage.Reject")+"<ENDSUBJECT>";
						String orginalSub=PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.RouteRej1")+"\""+docName+"\""+" "+resourceManager.getString("VWMailMsg.RouteRej2")+" "+resourceManager.getString("VWMailMessage.Reject");
						subject=subject.replaceAll(subject,orginalSub);
					}
					mailMessage += resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.RouteRej1")+" "+"<DOC NAME>"+" "+resourceManager.getString("VWMailMsg.RouteRej2")+" "+resourceManager.getString("VWMailMessage.Reject")+".\n\n";
				}
				else if(status.equals(Route_Status_End)||status.equals(Route_Status_Approve_End)||status.equals(Route_Status_PSR_End)){
					if(subject.equals("Route_Status_End")){
						mailMessage +="<SUBJECT>"+resourceManager.getString("VWMailMessage.EndWorkFlow")+" "+resourceManager.getString("VWMailMsg.RouteEnd1")+" "+ PRODUCT_NAME+" "+resourceManager.getString("VWMailMsg.RouteEnd2")+" "+"<DOC NAME>"+"<ENDSUBJECT>";
						String orginalSub=resourceManager.getString("VWMailMessage.EndWorkFlow")+" "+resourceManager.getString("VWMailMsg.RouteEnd1")+" "+ PRODUCT_NAME+" "+resourceManager.getString("VWMailMsg.RouteEnd2")+" "+"\""+docName+"\"";
						subject=subject.replaceAll(subject,orginalSub);
					}
					mailMessage += resourceManager.getString("VWMailMessage.EndWorkFlow")+" "+resourceManager.getString("VWMailMsg.RouteEnd1")+" "+ resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.RouteEnd2")+" "+ "<DOC NAME>"+".\n\n";
				}else if(status.trim().equals(Constants.Route_Status_Notify_OnRptGen)){
					/****CV2019 merges from SIDBI line --------------------------------***/
					if(subject.equals("Route_Status_Notify_OnRptGen") || subject.equals("Route_Status_OnEndVWR")){
						mailMessage +="<SUBJECT>"+PRODUCT_NAME + " - "+"<DOC NAME>"+"<ENDSUBJECT>";
						mailMessage += resourceManager.getString("WORKFLOW_NAME");
						if(!subject.equals("Route_Status_OnEndVWR")){
							mailMessage += " "+resourceManager.getString("VWMailMsg.EmailSummary1");
						} else {
							mailMessage += " "+resourceManager.getString("VWMailMsg.EmailSummary3");
						}
						String orginalSub=PRODUCT_NAME + " - \""+docName+"\"";
						subject=subject.replaceAll(subject,orginalSub);
					}
					mailMessage += " "+ resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.EmailSummary2")+" "+"<DOC NAME>"+".\n\n";
					/*------------------End of SIDBI merges----------------------------*/
				}else if(status.equals(Route_Status_Approve)){
					if(subject.equals("Route_Status_Approve")){
						mailMessage +="<SUBJECT>"+PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.Approve1")+"<DOC NAME>"+" "+resourceManager.getString("VWMailMsg.Approve2")+" "+resourceManager.getString("VWMailMessage.Approved")+"<ENDSUBJECT>";;
						String orginalSub=PRODUCT_NAME +" "+resourceManager.getString("VWMailMsg.Approve1")+"\""+docName+"\""+" "+resourceManager.getString("VWMailMsg.Approve2")+" "+resourceManager.getString("VWMailMessage.Approved");
						subject=subject.replaceAll(subject,orginalSub);
					}
					mailMessage += resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.Approve1")+" "+"<DOC NAME>"+" "+resourceManager.getString("VWMailMsg.Approve2")+" "+resourceManager.getString("VWMailMessage.Approved");
				}else if(status.startsWith("ARS")){
					// This is for future purpose - as of now it is not required to have the final action in the mail body
					/****CV2019 merges from SIDBI line --------------------------------***/
					switch(statusId){
					case 1:
						mailMessage +="<SUBJECT>"+PRODUCT_NAME +" "+resourceManager.getString("RamPageMessage.ARNotify")+" "+"<RETENTION NAME>"+"<ENDSUBJECT>";;
						subject = PRODUCT_NAME +" "+resourceManager.getString("RamPageMessage.ARNotify")+" "+"\""+docName+"\"";
						mailMessage += resourceManager.getString("VWMailMsg.ARSNotify")+" "+ resourceManager.getString("CVProduct.Name") +" "+  resourceManager.getString("VWMailMsg.RententionService");
						break;
					default:
						mailMessage +="<SUBJECT>"+PRODUCT_NAME +" "+resourceManager.getString("RamPageMessage.ARSummary")+" "+"<RETENTION NAME>"+"<ENDSUBJECT>";;
						subject = PRODUCT_NAME +" "+resourceManager.getString("RamPageMessage.ARSummary")+" "+"\""+docName+"\"";
						mailMessage += resourceManager.getString("VWMailMsg.ARSNotify")+" " + resourceManager.getString("CVProduct.Name") +" " +  resourceManager.getString("VWMailMsg.RententionService");
						break;
				}
				/*------------------End of SIDBI merges----------------------------*/	
				}
				else
					mailMessage += resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("VWMailMsg.DocPending1")+" "+"<DOC NAME>"+" "+resourceManager.getString("VWMailMsg.DocPending2");
				
				if(statusId==0){
				//mailMessage += "ViewWise Document <DOC NAME> is pending for review.\n\n";
					mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
					mailMessage += resourceManager.getString("VWMailMsg.AccessDoc");
					mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
					mailMessage += NEW_TAB + NEW_TAB + resourceManager.getString("VWMailMsg.Loginto")+" "+ resourceManager.getString("CVProduct.Name");
					mailMessage += HTML_NEW_LINE;
					if(status.trim().equals(Constants.Route_Status_Notify_OnRptGen))
						mailMessage += NEW_TAB + NEW_TAB + resourceManager.getString("VWMailMsg.DocFromResultList")+" "+"<DOC NAME>"+" "+resourceManager.getString("VWMailMsg.DocFromResultList2");	
					else
						mailMessage += NEW_TAB + NEW_TAB + resourceManager.getString("VWMailMsg.TotDolist_HitList");
					
					mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
					mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
					mailMessage += resourceManager.getString("VWMailMsg.AutoGenerated")+" " + resourceManager.getString("CVProduct.Name");
				}
				/****CV2019 merges from SIDBI line --------------------------------***/
				String fileContent = mailMessage.replaceAll(HTML_NEW_LINE, NEW_LINE);
				FileOutputStream fout = new FileOutputStream(msgFile);
				msg = fileContent;
				fout.write(msg.getBytes());
				//Should not replace <DOC NAME> with actual doc name in template file
				String actMailMessage = mailMessage.replace("<DOC NAME>", "\""+docName+"\"");
				actMailMessage = actMailMessage.replace("<RETENTION NAME>", "\""+docName+"\"");
				mailMessage = actMailMessage;
				/*------------------End of SIDBI merges----------------------------*/
			}
			/****CV2019 merges from SIDBI line --------------------------------***/
			if(!status.startsWith("ARS")){
				mailMessage += HTML_NEW_LINE;
				mailMessage += "<html><body><p><a href="+ webAccessReference +">" + webAccessReference + "</a></p></body></html>";				
			} else {
				if (statusId == 1) {//6 is For ARS Report Generation. - 1 is for Notify retention	
					message1 = HTML_NEW_LINE + HTML_NEW_LINE;
					message1 += resourceManager.getString("VWMailMsg.DocForRetention")+" "+docName;
					message1 += HTML_NEW_LINE ;
					
					
					message1 += "<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>";
					message1 += "<html>";
					message1 += "<head>";
					message1 += "<title>"+ resourceManager.getString("VWMailMsg.NotificationARS1")+" " + resourceManager.getString("CVProduct.Name")+" " + resourceManager.getString("VWMailMsg.NotificationARS2") +"</title>";
					message1 += "<style>";
					message1 += "TH {";
					message1 += "align='center'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px;; font-weight:bold";
					message1 += "}";
					message1 += "TD {";
					message1 += "align='left'; FONT-FAMILY: arial,helvetica,sans-serif;font-size: 10px";
					message1 += "}";
					message1 += "</style>";
					message1 += "</head>";
					message1 += "<body>";
					message1 += "<p align='center'><span lang='en-us'></span></p>";
					message1 += "<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2><font size='4'>";
					message1 += "<tr><td ><B>"+resourceManager.getString("VWMailMsg.DocName")+"</B></td>";
					message1 += "<td ><B>"+resourceManager.getString("VWMailMsg.DocLocation")+"</B></td></tr>";
					
					String temp = "";
					int hyphen = 0;
					if(docNames!=null && docNames.size()>0){
						int docListSize = docNames.size();
						for(int i=0; i<(docListSize-1); i++){
							
							temp = docNames.get(i).toString();
							if(temp.contains("zz#-zz")){
								hyphen = temp.indexOf("zz#-zz");
							}
							message1 += "<tr><td><B>"+temp.substring(0,hyphen)+"</td>"+"<td>"+temp.substring(hyphen+6,temp.length())+"</B></td></tr>";
						}
						temp = docNames.get(docListSize-1).toString();
						if(temp.contains("zz#-zz")){
							hyphen = temp.indexOf("zz#-zz");
						}
					}
					message1 += "<tr><td><B>"+temp.substring(0,hyphen)+"</td>"+"<td>"+temp.substring(hyphen+6,temp.length())+"<B></td></tr>";
					message1 += "</font></table>";
					message1 += HTML_NEW_LINE + HTML_NEW_LINE;
					message1 += resourceManager.getString("VWMailMsg.AutoGenerated") +" "+ resourceManager.getString("CVProduct.Name");
					message1 += "</body>";
					message1 += "</html>";
					mailMessage += message1;
				}
			}
			/*------------------End of SIDBI merges----------------------------*/
			//VWSLog.add("final mailMessage : "+mailMessage);
			if(mailMessage.contains("<ENDSUBJECT>")){
				String splitMessage[]=mailMessage.split("<ENDSUBJECT>");
				mailMessage=splitMessage[1];
			}
			
			//System.out.println(mailMessage);
		}catch(Exception e){
			
		}
		int mailSent = sendMail(mailTo, mailMessage, subject, attachment, docName, routeName, mailCcTo);
		return mailSent;
	}
	
	//Not in use
	/*public int sendMail(String userEmailID, String mailMessage, String subject, File attachment){
		File attachmentArr[] = new File[1];
		attachmentArr[0] = attachment;
		return sendMail(userEmailID, mailMessage, subject, attachmentArr[0]);
	}*/
	public int sendMail(String mailTo, String mailMessage, String subject, File attachment[], String docName,
			String notification, String mailCcTo) {
		VWSLog.dbg("inside sendMail........mailTo :"+mailTo);
		VWSLog.dbg("notification :"+notification);
		int mailSend = -1;
		if(notification!=null && notification.equalsIgnoreCase("notification"))
			setNotificationMailServerSettings();
		else if(notification!=null&&notification.equalsIgnoreCase("contentsentinel")) {
			setContentSentinelMailServerSettings();
		}
		else
			setMailServerSettings();		
		//String mailTo = userEmailID;//change to address here
		String mailFrom = fromEmailId;
		final String serverusername=this.mailServerUsername;
		final String serverpassword=this.mailServerPassword;
		String host = this.mailServerName;//change mailserver address here
		String port= this.mailServerPort;

		Properties props = new Properties();
		

		/*Following changes for Bug id:4383 added by Vanitha.S on 15/10/2019****/
		isSSL=false;
		if(notification!=null && notification.equalsIgnoreCase("notification")) {
			isSSL = VNSPreferences.getSSL();
		} else if(notification!=null&&notification.equalsIgnoreCase("contentsentinel")) {
			isSSL = CSSPreferences.getSSL();; 
		} else {
			isSSL=(VWSPreferences.getSSLEnable().equalsIgnoreCase("true")?true:false);
		}
		/************************************************************/
		VWSLog.dbg("Enable SSL :"+isSSL);
		if(isSSL){
				props.put("mail.smtp.auth", "true");
				/***ssl.trust Added for Could not convert socket to TLS exception. Added by Vanitha.S on 13/03/2020 ****/
				props.put("mail.smtp.ssl.trust", host);				
				props.put("mail.smtp.starttls.enable", "true");
				}
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
			 Session session = Session.getInstance(props,
			         new javax.mail.Authenticator() {
			   			
			            protected PasswordAuthentication getPasswordAuthentication() {
			            	 return new PasswordAuthentication(serverusername, serverpassword);
				   }
			            
			         });
		
		try {
			VWSLog.dbg("Before creating MimeMessage....");
			Message oMimeMessage = new MimeMessage(session);
			BodyPart messageBodyPart = new MimeBodyPart();
			/****CV2019 merges from SIDBI line (Hindi charaters issue fix)--------------------------------***/
			messageBodyPart.setDataHandler(new DataHandler(mailMessage,"text/html; charset=UTF-8"));
			messageBodyPart.addHeader("Content-type", "text/html; charset=UTF-8");
			Multipart multipart = new MimeMultipart();
			VWSLog.dbg("Before adding message body part....");
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			try{
				VWSLog.dbg("attachment....."+attachment);
				if(attachment!=null){
					DataSource source = null;
					for(int i=0; i<attachment.length; i++){
						VWSLog.dbg("attachment....."+i+" : "+attachment[i]);
						File tempAttachment = attachment[i];
						if(tempAttachment!=null){
							if(tempAttachment!=null && tempAttachment.exists()){
								source = new FileDataSource(tempAttachment);
								messageBodyPart.setDataHandler(new DataHandler(source));
								String fileName = checkForEuro(tempAttachment.getName());
								messageBodyPart.addHeader("Content-type", "text/plain; charset=UTF-8");

								// encodeText added for hindi characters
								/*bytes = fileName.getBytes();
								fileName = new String(bytes, Charset.forName("UTF-8"));*/
								messageBodyPart.setFileName(MimeUtility.encodeText(fileName, "UTF-8", "B"));
								multipart.addBodyPart(messageBodyPart);
								//this is for adding multiple attachment
								messageBodyPart = new MimeBodyPart();
							}
						}
					}
				}
			}catch(Exception exception){
			}
			VWSLog.dbg("mailFrom......"+mailFrom);
			oMimeMessage.setFrom(new InternetAddress(mailFrom));
			
			//Handling multiple addresses.
			
			/**Following addressTo is not working for multiple addresses**/
			/** commented by vanitha **/
			/*InternetAddress[] addressTo = new InternetAddress[1];
			if(mailTo.indexOf(";")!=-1){
				StringTokenizer st = new StringTokenizer(mailTo, ";");
				addressTo = new InternetAddress[st.countTokens()];
				int i = 0;
				while(st.hasMoreTokens()){
					addressTo[i] = new InternetAddress(st.nextToken().toString());
					i++;
				}
			}else{
				addressTo[0] = new InternetAddress(mailTo.toString());
			}
			oMimeMessage.setRecipients(Message.RecipientType.TO, addressTo);*/
			
			/**Following addressTo and addressCcTo is added for multiple addresses**/
			/** Added by vanitha **/
			InternetAddress[] addressTo = null;
			if (mailTo.indexOf(";")!=-1) {
				mailTo = mailTo.replaceAll(";", ",");								
			}
			VWSLog.dbg("mailTo ::: "+mailTo);
			addressTo = InternetAddress.parse(mailTo);
			oMimeMessage.setRecipients(Message.RecipientType.TO, addressTo);
			
			//Handling multiple addresses for mailccto.
			VWSLog.dbg("mailCcTo......"+mailCcTo);
			if (mailCcTo != null && mailCcTo != "") {
				InternetAddress[] addressCcTo = null;
				if (mailCcTo.indexOf(";")!=-1) {
					mailCcTo = mailCcTo.replaceAll(";", ",");								
				}
				VWSLog.dbg("mailCcTo ::: "+mailCcTo);
				addressCcTo = InternetAddress.parse(mailCcTo);
				oMimeMessage.addRecipients(Message.RecipientType.CC, addressCcTo);
			}
			//END	
			oMimeMessage.setSubject(MimeUtility.encodeText(subject, "UTF-8", "B"));
			//oMimeMessage.setSubject(subject);//change subject here
			if(notification==null ){
			oMimeMessage.addHeader("X-Priority","1");
			}
			oMimeMessage.setSentDate(new Date());
			oMimeMessage.setText(mailMessage);//change message here
			oMimeMessage.setContent(multipart,"text/html; charset=UTF-8");
			VWSLog.dbg("Before sending the mail....");
			Transport.send(oMimeMessage);
			VWSLog.dbg("After sending the mail....");
			mailSend = 1;
			/*------------------End of SIDBI merges----------------------------*/
		}
		catch (MessagingException mex) {
			VWSLog.add("MessagingException in sending mail ......"+mex.fillInStackTrace().getMessage().toString());
			VWSLog.add("MessagingException in sending mail ......"+mex.getMessage());
			mailSend = -1;
		}
		catch (Exception ex) {
			VWSLog.add("Exception in sending mail ......"+ex.fillInStackTrace().getMessage().toString());
			VWSLog.add("Exception in sending mail ......"+ex.getMessage());
			mailSend = -1;
		}
		
		/**
		 * This block for deleting document.vwr file has been commented.
		 * document.vwr file is getting deleted in RampageServer class after mail sent.
		 */
		//For ARS - Retention it should not the delete the report from the local file.
		/*if(!subject.contains("Retention")){
			if(attachment!=null){
				try{
					//Other than Document Route Summary Report delete the two files
					if(attachment[0]!=null && attachment[0].exists()){ 
						attachment[0].delete();//Document Meta data
					}
					if(attachment[2]!=null && attachment[2].exists()){
						attachment[2].delete();//Document Pages zip file
					}
				}catch(Exception ex){
					//VWSLog.add("Exception while deleting the files : "+ex.getMessage());
				}
				
			}
		}*/
		return mailSend;
	}
	
	public int sendNotificationMail(String userEmailId, String mailSubject, Vector mailMessageStr, File[] attachmentFile, int status, int nodeType, String nodeName, String fromMailID, String mailTo, String mailCcTo) {
		if(userEmailId == null || (userEmailId.trim().equals("-")))
			return -2;
		String mailMessage = "";
		try{
			String vwsHome = VWSUtil.getHome();
			File msgFile = null;
			status = MAIL_BUILK;

			mailMessage += resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("VWMailMsg.EmailGeneratedFor")+" "+ resourceManager.getString("CVProduct.Name")+" "+"<EVENT NAME>"+" "+ resourceManager.getString("VWMailMsg.EventName");
			mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
			mailMessage += "MAIL CONTENT";
			mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
			
			//Format is common for all module, except Document in ViewWise Server message...
			if(nodeType != VNSConstants.nodeType_Document){
				msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_NOTIFY_MODULE);
			}
			
			if(msgFile!=null && !msgFile.exists() && nodeType != VNSConstants.nodeType_Document){//If not exist create the rtf file.
				String msg = "";
				mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
				mailMessage += resourceManager.getString("VWMailMsg.AutoGenerated")+" " + resourceManager.getString("CVProduct.Name");
				try{
					FileOutputStream fout = new FileOutputStream(msgFile);
					msg = mailMessage;
					fout.write(msg.getBytes());
					fout.close();
				}catch(IOException io){}
			}
			
			switch(nodeType){
				case VNSConstants.nodeType_Document:
					msgFile = new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+MAIL_NOTIFY_DOCUMENT);
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.ReportForDoc");
					
					mailMessage += resourceManager.getString("VWMailMsg.ToAccessDoc");
					mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
					if(attachmentFile[0]!=null){//If vwr refrence file is attached...
						mailMessage += resourceManager.getString("VWMailMsg.DoubleClickVWR");
						mailMessage += HTML_NEW_LINE;
						mailMessage += "(or)";
						mailMessage += HTML_NEW_LINE;
					}
					mailMessage += NEW_TAB + NEW_TAB + resourceManager.getString("VWMailMsg.Loginto")+" " + resourceManager.getString("CVProduct.Name");
					mailMessage += HTML_NEW_LINE;
					mailMessage += NEW_TAB + NEW_TAB + resourceManager.getString("VWMailMsg.DocFromResultList");
			
					String msg = "";
					mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
					mailMessage += resourceManager.getString("VWMailMsg.AutoGenerated") +" "+ resourceManager.getString("CVProduct.Name");
					try{
						FileOutputStream fout = new FileOutputStream(msgFile);
						msg = mailMessage;
						fout.write(msg.getBytes());	
						fout.close();
					}catch(IOException io){
						
					}
					//Should not replace the <EVENT NAME> in the rtf template file.
					mailMessage = mailMessage.replace("<EVENT NAME>", "Document(s)");
					break;
					
				case VNSConstants.nodeType_Folder:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+resourceManager.getString("VWMailMsg.NotifyReportForFolder");
					mailMessage = mailMessage.replace("<EVENT NAME>", "Folder(s)");
					break;
					
				case VNSConstants.nodeType_DT:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.NotifyReportForDocType");
					mailMessage = mailMessage.replace("<EVENT NAME>", "Document Type(s)");
					break;
					
				case VNSConstants.nodeType_Storage:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.StorageManagement");
					mailMessage = mailMessage.replace("<EVENT NAME>", "Storage Management");
					break;
					
				case VNSConstants.nodeType_AT:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.AuditTrail");
					mailMessage = mailMessage.replace("<EVENT NAME>", "Audit Trail");
					break;
					
				case VNSConstants.nodeType_RecycledDocs:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.RecylceDoc");
					mailMessage = mailMessage.replace("<EVENT NAME>", "Recycled Document(s)");
					break;
					
				case VNSConstants.nodeType_RestoreDocs:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.RestoredDoc");
					mailMessage = mailMessage.replace("<EVENT NAME>", "Restored Document(s)");
					break;
					
				case VNSConstants.nodeType_Redaction:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.Redactions");
					mailMessage = mailMessage.replace("<EVENT NAME>", "Redaction(s)");
					break;
					
				case VNSConstants.nodeType_Retention:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.Retention");
					mailMessage = mailMessage.replace("<EVENT NAME>", "Retention(s)");
					break;
					
				case VNSConstants.nodeType_Route:
					mailSubject = resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.NotificationReportFor")+" "+ resourceManager.getString("WORKFLOW_NAME") +resourceManager.getString("RamPageMessage.S");
					mailMessage = mailMessage.replace("<EVENT NAME>", resourceManager.getString("WORKFLOW_NAME") +resourceManager.getString("RamPageMessage.S"));
					break;
			}
			
			
			String content = convertVectorToString (mailMessageStr);
			if(msgFile!=null && msgFile.exists()){
				
				String messageStr = readRTFInformation(msgFile, nodeType, content, nodeName);
				mailMessage = messageStr;
			}else{
/*				String msg = "";
				String actMailMessage = "";
			
				mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
				mailMessage += "This is an auto-generated notification from ViewWise";
				
				FileOutputStream fout = new FileOutputStream(msgFile);
				msg = mailMessage;
				fout.write(msg.getBytes());*/

				//Should not replace <DOC NAME> with actual doc name in template file
				String actMailMessage = mailMessage.replace("MAIL CONTENT", content);
				actMailMessage = actMailMessage.replace("<DOC NAME>", "\""+nodeName+"\"");
				mailMessage = actMailMessage;
			}
		}catch(Exception e){
			VWSLog.add("Excecption in sendNotificationMail : "+e.getMessage());
		}		
		/**CV2019 merges from SIDBI line***/
		int mailSent = sendMail(userEmailId, mailMessage, mailSubject, attachmentFile, nodeName, "notification", null);
		return mailSent;
	}
		
	private String readRTFInformation(File msgFile, int nodeType, String content, String nodeName) {
		String line = "";
		String messageStr = "";
		String eventName = "";
		try{
			FileInputStream fin = new FileInputStream(msgFile);
			BufferedReader bReader = new BufferedReader(new InputStreamReader(fin));

			switch(nodeType){
				case VNSConstants.nodeType_Document:
					eventName = "Document(s)";
					break;
				case VNSConstants.nodeType_Folder:
					eventName = "Folder(s)";
					break;
					
				case VNSConstants.nodeType_DT:
					eventName = "Document Type(s)";
					break;
					
				case VNSConstants.nodeType_Storage:
					eventName = "Storage Management";
					break;
					
				case VNSConstants.nodeType_AT:
					eventName = "Audit Trail";
					break;
					
				case VNSConstants.nodeType_RecycledDocs:
					eventName = "Recycled Document(s)";
					break;
					
				case VNSConstants.nodeType_RestoreDocs:
					eventName = "Restored Document(s)";
					break;
					
				case VNSConstants.nodeType_Redaction:
					eventName = "Redaction(s)";
					break;
					
				case VNSConstants.nodeType_Retention:
					eventName = "Retention(s)";
					break;
					
				case VNSConstants.nodeType_Route:
					eventName = WORKFLOW_MODULE_NAME +"(s)";
					break;
				default:
					break;
			}	
			while( (line = bReader.readLine()) != null){
				String actLine = "";
				actLine = line.replace("MAIL CONTENT", content);
				actLine = actLine.replace("<DOC NAME>", "\""+nodeName+"\"");
				actLine = actLine.replace("<EVENT NAME>", "\""+eventName+"\"");
				messageStr += actLine;
				messageStr += HTML_NEW_LINE;
			}
		}catch(Exception ex){
			VNSLog.add("Exception while readRTFInformation : "+ex.getMessage());
		}
		return messageStr;
	}

	public String convertVectorToString(Vector htmlContents){
		String mailMessage = "";
		if(htmlContents!=null && htmlContents.size()>0)
			for (int i=0; i<htmlContents.size(); i++){
				mailMessage +=htmlContents.get(i).toString();
			}
		VNSLog.add(mailMessage);
		return mailMessage;
	}
	public static String checkForEuro(String name){
		
		/*Currency Symbol	$ 	=  OK
		 Currency Symbol	???		= __
		 Currency Symbol	K?		= K?
		 Currency Symbol	�		= OK
		 Currency Symbol	???.	= __.
		 Currency Symbol	???.	= __.
		 Currency Symbol	�		= EURO
		 String temp =  "Currency_$_�_???_K?_???._???._�_symbols"; 
		 */
		StringBuffer sb = new StringBuffer();
		char[] fChars = name.toCharArray();
		for(int i=0;i<fChars.length;i++)
		{         
			short sName = (short)fChars[i];
			//VWSLog.add( " sName "+sName + " "+fChars[i]);
			if( fChars[i] == '�'|| (sName == 8364) ) {
				sb.append("EURO");
			}		
			else
			{
				sb.append(fChars[i]);
			}
		}       
		return (sb.toString());
	}
	
	//Sending Test mail ...
	public static void sendTestMail(String strHost, String strPort, String to, String from,String EmailServer,String EmailServerPassword,String Notification,String subject, String message){
		String mailTo = to;//change to address here
		String mailFrom = from;//change from address here
		String host = strHost;//change mailserver address here
		String port= strPort;
		boolean isSSL=false;
		if(Notification!=null && Notification.equalsIgnoreCase("Notification"))
		 isSSL=VNSPreferences.getSSL();
		else if(Notification!=null && Notification.equalsIgnoreCase("ContentSentianl")) {
			 isSSL=CSSPreferences.getSSL();	
		}
		else
		 isSSL=(VWSPreferences.getSSLEnable().equalsIgnoreCase("true")?true:false);
		final String serverusername=EmailServer;
		final String serverpassword=EmailServerPassword;
		Properties props = new Properties();
		if(isSSL){
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
			}
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		 Session session = Session.getInstance(props,
		         new javax.mail.Authenticator() {
		   			
		            protected PasswordAuthentication getPasswordAuthentication() {
		            	 return new PasswordAuthentication(serverusername, serverpassword);
			   }
		            
		         });
		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(mailFrom));
			InternetAddress[] address = {new InternetAddress(mailTo)};
			msg.setRecipients(Message.RecipientType.TO, address);
			msg.setSubject(subject);//change subject here
			msg.setSentDate(new Date());
			msg.setText(message);//change message here
			//Send the message
			Transport.send(msg);
			JOptionPane.showMessageDialog(null,resourceManager.getString("VWMailMsg.TestMailSent"));
		}catch (MessagingException mex) {
			String exceptionMsg = mex.toString();
			VWSLog.add(exceptionMsg);
			VWSLog.writeLog();
			if(exceptionMsg.indexOf("UnknownHostException")>=0){
				JOptionPane.showMessageDialog(null,resourceManager.getString("VWMailMsg.TestMailNotSentHost"));	        		
			}
			else if(exceptionMsg.indexOf("ConnectException")>=0){
				JOptionPane.showMessageDialog(null,resourceManager.getString("VWMailMsg.TestMailNotSentPort"));
			}
			else{
				JOptionPane.showMessageDialog(null,resourceManager.getString("VWMailMsg.TestMailNotSentSettings")+" "+"["+mex.getLocalizedMessage()+"]");
			}
			
		}
		catch(Exception ex){
			VWSLog.add(ex.toString());
			VWSLog.writeLog();
			JOptionPane.showMessageDialog(null,resourceManager.getString("VWMailMsg.TestMailNotSentOtherSettings")+" "+"["+ex.getLocalizedMessage()+"]");
		}
	}
	
	/**
	 * SendMailNotificationOnVSM
	 * @param strHost
	 * @param strPort
	 * @param to
	 * @param from
	 * @param subject
	 * @param message
	 * This is the enhancement for send mail for .
	 */
	public static int sendMailNotificationOnVSM(String message, int VWSCount){
		try{
			boolean sendMail = false;
			String mailTo = null;
			String subject = null;
			String mailToVWS = VWSPreferences.getVWSEmailId();
			String mailToDSS = VWSPreferences.getDSSEmailId();
			String mailToDRS = VWSPreferences.getDRSEmailId();
			String mailToARS = VWSPreferences.getARSEmailId();
			String mailToVNS = VWSPreferences.getVNSEmailId();
			String mailFrom = VWSPreferences.getFromEmailId();
			String host = VWSPreferences.getEmailServerName();
			String port= VWSPreferences.getEmailServerPort();
			final String serverusername=VWSPreferences.getEmailServerUsername();
			final String serverpassword=VWSPreferences.getEmailServerPassword();
			boolean isSSL=(VWSPreferences.getSSLEnable().equalsIgnoreCase("true")?true:false);
			boolean isMailToVWSSet = (VWSPreferences.getVWSEmailIdSet().equalsIgnoreCase("true")?true:false);
			boolean isMailToDSSSet = (VWSPreferences.getDSSEmailIdSet().equalsIgnoreCase("true")?true:false);
			boolean isMailToDRSSet = (VWSPreferences.getDRSEmailIdSet().equalsIgnoreCase("true")?true:false);
			boolean isMailToARSSet = (VWSPreferences.getARSEmailIdSet().equalsIgnoreCase("true")?true:false);
			boolean isMailToVNSSet = (VWSPreferences.getVNSEmailIdSet().equalsIgnoreCase("true")?true:false);
			
			Properties props = new Properties();
			if(isSSL){
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				}
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port", port);
			 Session session = Session.getInstance(props,
			         new javax.mail.Authenticator() {
			   			
			            protected PasswordAuthentication getPasswordAuthentication() {
			            return new PasswordAuthentication(serverusername, serverpassword);
				   }
			            
			         });
	
			switch(VWSCount){
				case VWSConstants.VWS:
					mailTo = mailToVWS;
					subject = resourceManager.getString("VWMailMsg.NotifiyOnServ1")+" "+ resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("VWMailMsg.NotifiyOnServ2");
					if(isMailToVWSSet)
						sendMail = true;
					break;
				case VWSConstants.DSS:
					mailTo = mailToDSS;
					subject = resourceManager.getString("VWMailMsg.DSS");
					if(isMailToDSSSet)
						sendMail = true;
					break;
	
				case VWSConstants.DRS:
					mailTo = mailToDRS;
					subject = resourceManager.getString("VWMailMsg.NotifiOnDocServ1")+" "+ resourceManager.getString("WORKFLOW_NAME") +" "+resourceManager.getString("VWMailMsg.NotifiOnDocServ2");
					if(isMailToDRSSet)
						sendMail = true;
					break;
	
				case VWSConstants.ARS:
					mailTo = mailToARS;
					subject = resourceManager.getString("VWMailMsg.ARS");
					if(isMailToARSSet)
						sendMail = true;
					break;
	
				case VWSConstants.VNS:
					mailTo = mailToVNS;
					subject = resourceManager.getString("VWMailMsg.NotifyServices");
					if(isMailToVNSSet)
						sendMail = true;
					break;
					
				default:
					System.out.println("move to will be of No server");
				break;
			}
			if(mailTo!=null && sendMail){
				try {
					String mailMessage = "";
					Message msg = new MimeMessage(session);
					msg.setFrom(new InternetAddress(mailFrom));
					InternetAddress[] address = new InternetAddress[1];
					if(mailTo.indexOf(";")!=-1){
						StringTokenizer st = new StringTokenizer(mailTo, ";");
						address = new InternetAddress[st.countTokens()];
						int i = 0;
						while(st.hasMoreTokens()){
							address[i] = new InternetAddress(st.nextToken().toString());
							i++;
						}
					}else{
						address[0] = new InternetAddress(mailTo.toString());
					}
					msg.setRecipients(Message.RecipientType.TO, address);
					msg.setSubject(subject);//change subject here
					msg.setSentDate(new Date());
					mailMessage += HTML_NEW_LINE;
					if(message.contains("\n")){
						message.replaceAll("\n", HTML_NEW_LINE);
					}
					mailMessage += message;
					mailMessage += HTML_NEW_LINE + HTML_NEW_LINE + HTML_NEW_LINE;
					mailMessage += resourceManager.getString("VWMailMsg.AutoGenerated") +" "+ resourceManager.getString("CVProduct.Name");
					msg.setText(mailMessage);//change message here
					//msg.setDataHandler(new DataHandler(mailMessage, "text/html; charset=window-1252"));
					msg.addHeader("Content-type", "text/html; charset=windows-1252");
					//msg.setContent(multipart);
					//Send the message
					Transport.send(msg);
					VWSLog.add("Mail sent successfully");
					//JOptionPane.showMessageDialog(null,"Test mail successfully sent.");
				}catch (MessagingException mex) {	        	
					String exceptionMsg = mex.toString();
					VWSLog.add(exceptionMsg);
					VWSLog.writeLog();
					if(exceptionMsg.indexOf("UnknownHostException")>=0){
						VWSLog.add("VSM Notification mail not sent, please check the host name");	        		
					}
					else if(exceptionMsg.indexOf("ConnectException")>=0){
						VWSLog.add("VSM Notification mail not sent, please check the port number");
					}
					else{
						VWSLog.add("VSM Notification mail not sent, please check the settings. ["+mex.getLocalizedMessage()+"]");
					}
				}
				catch(Exception ex){
					VWSLog.add(ex.toString());
					VWSLog.writeLog();
					//JOptionPane.showMessageDialog(null,"Test mail not sent, please check the other settings. ["+ex.getLocalizedMessage()+"]");
				}
			}else{
				VWSLog.add("Check the mail server settings");
			}
		}catch(Exception ex){
			VWSLog.err("Exception in Send Notification :: "+ex.getMessage());
		}
		return 10;
	}
	
	public int sendEscalationMail(String from, String to, String subject, String message){
		setMailServerSettings();
		String mailTo = to;//change to address here
		String mailFrom = from;//change from address here
		String host = this.mailServerName;//change mailserver address here
		String port= this.mailServerPort;
		String mailMessage="";
		boolean isSSL=(VWSPreferences.getSSLEnable().equalsIgnoreCase("true")?true:false);
		final String serverusername=VWSPreferences.getEmailServerUsername();
		final String serverpassword=VWSPreferences.getEmailServerPassword();
		Properties props = new Properties();
		if(isSSL){
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.starttls.enable", "true");
		}
		props.put("mail.smtp.host", host);
		props.put("mail.smtp.port", port);
		Session session = Session.getInstance(props,
				new javax.mail.Authenticator() {

			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(serverusername, serverpassword);
			}

		});
		
		try {
			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(mailFrom));
			InternetAddress[] address  = new InternetAddress[1];
			if(mailTo.indexOf(";")!=-1){
				StringTokenizer st = new StringTokenizer(mailTo, ";");
				address = new InternetAddress[st.countTokens()];
				int i = 0;
				while(st.hasMoreTokens()){
					address[i] = new InternetAddress(st.nextToken().toString());
					i++;
				}
			}else{
				address[0] = new InternetAddress(mailTo.toString());
			}
			msg.setRecipients(Message.RecipientType.TO, address);
			msg.setSubject(MimeUtility.encodeText(subject, "UTF-8", "B"));
			msg.setSentDate(new Date());
			mailMessage += HTML_NEW_LINE;
			mailMessage += message;
			mailMessage += HTML_NEW_LINE + HTML_NEW_LINE + HTML_NEW_LINE;
			mailMessage += resourceManager.getString("VWMailMsg.AutoGenerated") +" "+ resourceManager.getString("CVProduct.Name");
			
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setDataHandler(new DataHandler(mailMessage,"text/html; charset=UTF-8"));
			messageBodyPart.addHeader("Content-type", "text/html; charset=UTF-8");
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			msg.setText(mailMessage);//change message here
			msg.setContent(multipart,"text/html; charset=UTF-8");
			//Send the message
			Transport.send(msg);
		}
		catch (MessagingException mex) {
			return -1;
		}catch(Exception ex){
			return -1;
		}
		return 1;
	}
	static String returnEmail(String mailIdstr) {
		String modifieMailId="";
		if(mailIdstr!=null&mailIdstr.length()>0) {
			String mailIdsubString[]=mailIdstr.split("@");
			String firstPart=mailIdsubString[0];
			String stars="";
			if(firstPart.length()>3) {
				String test=firstPart.substring(2,firstPart.length());
				for(int j=0;j<test.length();j++) {
					stars=	test.substring(0, j) + "*" + test.substring(j + 1);

					test=stars;
				}
				modifieMailId=firstPart.substring(0, 2)+stars+"@"+mailIdsubString[1];
			}else {
				modifieMailId=mailIdstr;
			}
		}
		return modifieMailId;
	}
	public int sendSecureLinkMail(String mailIdstr,String mailSubject,String docName,String  message,String messsagetype,String expiryDt) {
		int mailSent=-1;
		String modifieMailId="";
		File msgFile = null;
		File msgPasswordFile = null;
		FileInputStream fin=null;
		FileOutputStream fout=null;
		String mailMessage = "";
		try {
			
			String vwsHome = VWSUtil.getHome();
						
			
					if(messsagetype.equalsIgnoreCase(VWSConstants.CSS_MAIL_Url)) {
						msgFile=new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+VWSConstants.MAIL_CSS_URL_MAIL);
					}else if(messsagetype.equalsIgnoreCase(VWSConstants.CSS_MAIL_PASSWORD)) {
						msgFile=new File(vwsHome + Util.pathSep+"lib"+Util.pathSep+VWSConstants.MAIL_CSS_PASS_MAIL);
					}
					if(msgFile!=null && msgFile.exists()) {
						//VWSLog.add("Inside message file is not null......"+msgFile.getName());
						String line = "";
						 fin = new FileInputStream(msgFile);
						BufferedReader bReader = new BufferedReader(new InputStreamReader(fin));
						while( (line = bReader.readLine()) != null){
							//VWSLog.add("line:::::"+line);
							//String actLine = "";
							if(line.contains("<DOCUMENT NAME>")) {
								line = line.replace("<DOCUMENT NAME>", "\""+docName+"\"");
							}else if (line.contains("<document name>")) {
								line = line.replace("<document name>", "\""+docName+"\"");
							}
							

							mailMessage += line;
							
							
							mailMessage += HTML_NEW_LINE;
							if(msgFile.getName().equalsIgnoreCase(VWSConstants.MAIL_CSS_URL_MAIL)) {
								if(line.contains("<EXPIRY DATE>")) {
									mailMessage =mailMessage.replace("<EXPIRY DATE>", "<a>"+expiryDt+"</a>");
								}
								if(line.contains("<URL>")) {
								String urlLine = "";
								urlLine=line.replace("<URL>", "<a>"+message+"</a>");
								mailMessage += urlLine;
								mailMessage += HTML_NEW_LINE;
								//VWSLog.add("mailMessage with url 11111::::"+mailMessage);
								}
							
							
							
							}else if(msgFile.getName().equalsIgnoreCase(VWSConstants.MAIL_CSS_PASS_MAIL)) {
								if(line.contains("<EMAIL>")) {
									mailMessage=mailMessage.replace("<EMAIL>", returnEmail(mailIdstr));
								}
								
								if(line.contains("<PASSWORD>")) {
									//VWSLog.add("PASSWORD :::"+message);
									//VWSLog.add("mailMessage after adding password  :::"+mailMessage);
									String urlpassword = "";
									urlpassword=line.replace("<PASSWORD>", "<a>"+message+"</a>");
									
									mailMessage += urlpassword;
									mailMessage += HTML_NEW_LINE;
									//VWSLog.add("mailMessage with password 2222::::"+mailMessage);
								}
							}
							//VWSLog.add("mailMessage inside file exist is :::::"+mailMessage);

						}
						msgFile=null;
					}else {
						//VWSLog.add("Inside message is null......"+msgFile.getName());
						if(!msgFile.exists()) {
							msgFile.createNewFile();
							//VWSLog.add("created msgFile "+msgFile.getName());
						}
						//VWSLog.add("Inside else of rtf.....");
						String msg = "";
						if(msgFile.getName().equalsIgnoreCase(VWSConstants.MAIL_CSS_URL_MAIL)) {
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="<SUBJECT>Document <DOCUMENT NAME> has been shared with you.<ENDSUBJECT>";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="Contentverse Document <DOCUMENT NAME> has been Shared with you";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="You can access this document using the below link until <EXPIRY DATE> :";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="<URL>";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="Thanks,";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="Contentverse team.";
							 fout = new FileOutputStream(msgFile);
							String fileContent = mailMessage.replaceAll(HTML_NEW_LINE, NEW_LINE);
							msg = fileContent;
							//VWSLog.add("Inside else of rtf....."+msg);
							fout.write(msg.getBytes());
							
							if(mailMessage.contains("<Expiry date>")) {
								
								mailMessage =mailMessage.replace("<EXPIRY DATE>", "<a>"+expiryDt+"</a>");
							}
							if(mailMessage.contains("<URL>")) {
								String urlLine = "";
								urlLine=mailMessage.replace("<URL>", "<a>"+message+"</a>");
								mailMessage = urlLine;
								mailMessage += HTML_NEW_LINE;
							}
							//VWSLog.add("mailMessage inside file generation with url :::::"+mailMessage);

						}else if(msgFile.getName().equalsIgnoreCase(VWSConstants.MAIL_CSS_PASS_MAIL)) {
							
							
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="<SUBJECT>Document <DOCUMENT NAME> has been shared with you.<ENDSUBJECT>";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="Contentverse Document <DOCUMENT NAME> has been Shared with you";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="The password assigned for the account <EMAIL> is ";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="<PASSWORD>";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="Thanks,";
							mailMessage += HTML_NEW_LINE + HTML_NEW_LINE;
							mailMessage +="Contentverse team.";
							 fout = new FileOutputStream(msgFile);
							String fileContent = mailMessage.replaceAll(HTML_NEW_LINE, NEW_LINE);
							msg = fileContent;
							//VWSLog.add("Inside inside file generation of password mail of rtf....."+msg);
							fout.write(msg.getBytes());
							if(mailMessage.contains("<EMAIL>")) {
								mailMessage=mailMessage.replace("<EMAIL>", returnEmail(mailIdstr));
							}
							if(mailMessage.contains("<PASSWORD>")) {
								//VWSLog.add("PASSWORD :::"+message);
								String urlpassword = "";
								urlpassword=mailMessage.replace("<PASSWORD>", "<a>"+message+"</a>");
								//mailMessage=mailMessage.replace("<EMAIL>", returnEmail(mailIdstr));
								mailMessage = urlpassword;
								//mailMessage += HTML_NEW_LINE;
							}
							//VWSLog.add("Inside inside file generation of with password  mail of rtf....."+mailMessage);

							
						}
						
						
					}
					
					//VWSLog.add("mailMessage before changing:::"+mailMessage);
			if(mailMessage.contains("<SUBJECT>")){
				String subMessage=getStringBetweenStrings(mailMessage,"<SUBJECT>","<ENDSUBJECT>");
				//VWSLog.add(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
				//VWSLog.add("subMessage>>>>>>>"+subMessage);
				
				if(subMessage.contains("<DOCUMENT NAME>")) {
					subMessage = subMessage.replace("<DOCUMENT NAME>", "\""+docName+"\"");
					}/*else if (subMessage.contains("<document name>")) {
						subMessage = subMessage.replace("<document name>", "\""+docName+"\"");
					}*/
				mailSubject=mailSubject.replaceAll(mailSubject,subMessage);
				//VWSLog.add("mailSubject-------------"+mailSubject);
				//mailMessage.replaceAll(mailSubject, "");
				//mailMessage.replaceAll(subMessage, "");
				//VWSLog.add("mailMessage after subject-------------"+mailMessage);
			}
			if(mailMessage.contains("<ENDSUBJECT>")){
				String splitMessage[]=mailMessage.split("<ENDSUBJECT>");
				mailMessage=splitMessage[1];
				VWSLog.dbg("mail message after split ...."+mailMessage);
			}
			if(mailIdstr == null || (mailIdstr.trim().equals("-")))
				return -2;
			VWSLog.dbg("mailMessage befoer sending msgFile "+mailMessage);
			VWSLog.dbg("Before calling sendMail from SecureLinkMail method...");
			mailSent = sendMail(mailIdstr, mailMessage, mailSubject, null, docName, "contentsentinel", null);
			VWSLog.dbg("after calling sendMail from SecureLinkMail method..."+mailSent);
		}catch(Exception e ) {

		}finally {
			if(fin!=null) {
				try {
					fin.close();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(fout!=null) {
				try {
					fout.close();
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return mailSent;
	}
	
	/**
	 * CV10.2 - Enhancement 
	 * getRoomAndRouteBasedMessge
	 * @param filePath
	 * @param roomName
	 * @param routeName	-- Workflow Name/ Retention Name/ Pending - workflow name/ Rejected - workflow name/ End - workflow name/ Notification Name
	 * This is the enhancement for ARS retention mail.
	 */
	public static File getRoomAndRouteBasedMessge(String filePath, String roomName, String routeName, String status, int statusId) {		
		File fileFolder = new File(filePath);
		String messageFileName = null;
		if (status.startsWith("ARS")) {
			if (statusId == 1) {
				messageFileName = roomName + "_" + routeName + "_" + MAIL_RETENTION_NOTIFICATION;
			} else {
				messageFileName = roomName + "_" + routeName + "_" + MAIL_RETENTION_MAIL;
			}
		} else {
			if (status.equalsIgnoreCase(Route_Status_Pending)) {
				messageFileName = roomName + "_" + routeName + "_" + MAIL_WORKFLOW_PENDING;
			} else if (status.equalsIgnoreCase(Route_Status_Approve)) {
				messageFileName = roomName + "_" + routeName + "_" + MAIL_WORKFLOW_APPROVE;
			} else if (status.equalsIgnoreCase(Route_Status_Reject)) {
				messageFileName = roomName + "_" + routeName + "_" + MAIL_WORKFLOW_REJECTED;
			} else if (status.equalsIgnoreCase(Route_Status_End)) {
				messageFileName = roomName + "_" + routeName + "_" + MAIL_WORKFLOW_END;
			} else if (status.equalsIgnoreCase(Route_Status_Notify_OnRptGen)) {
				messageFileName = roomName + "_" + routeName + "_" + MAIL_WORKFLOW_SUMMARY;
			}
		}	
		//VWSLog.add("filePath....."+filePath);
		VWSLog.dbg("MessageFileName :"+ messageFileName);
		//VWSLog.add("is file path exist :"+fileFolder.exists());
		if (messageFileName != null) {
			String fileName = null;
			if (fileFolder.exists()) {
				for (File f : fileFolder.listFiles()) {
					//VWSLog.dbg("file Name : "+f.getName() +" messageFileName :"+ messageFileName);
					fileName = f.getName();
					if (messageFileName.equalsIgnoreCase(fileName)) {
						VWSLog.dbg("Is room and retention based file exist ::: true ");
						return f;
					}				
				}
			}
		}
		return null;
	}
	
	/**
	 * CV2019 - Enhancement - <<Index Name>> replacement with index value
	 * @param mailMessage
	 * @param indexDetails
	 * @return
	 */
	public String replaceIndexName(String mailMessage, HashMap<String, String> indexDetails) {
		VWSLog.dbg("inside replaceIndexName method");
		String strPattern = "<<(.*?)>>";
		Pattern pattern = Pattern.compile(strPattern);
		Matcher patternMatcher = pattern.matcher(mailMessage);
		String indexName = null, indexValue = null;
		while (patternMatcher.find()) {
			VWSLog.dbg("patternMatcher :true");
			indexName = patternMatcher.group(1);
			VWSLog.dbg("Index name  :"+indexName);
			if (indexDetails != null && indexDetails.size() > 0) {
				indexValue = indexDetails.get(indexName.trim());
			} else {
				indexValue = "";
			}
			VWSLog.dbg("Index value :"+indexValue);
			if (indexValue == null) indexValue = ""; 
			mailMessage = mailMessage.replace("<<"+indexName+">>", indexValue);
		}
		
		VWSLog.dbg("Message after replacing with index value :"+mailMessage);
		return mailMessage;
	}
}
