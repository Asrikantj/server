/*
 * VWCreateDB.java
 *
 * Created on May 23, 2004, 5:43 PM
 */

package com.computhink.vws.server;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.io.RandomAccessFile;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;
import java.util.Vector;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.DefaultStyledDocument;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.rtf.RTFEditorKit;

//import oracle.jdbc.driver.OracleCallableStatement;
import oracle.jdbc.OracleCallableStatement;
import com.computhink.resource.ResourceManager;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import com.computhink.common.Constants;
import com.computhink.common.Util;
import com.computhink.manager.ManagerUtil;
import com.computhink.manager.VWRoomPanel;

public class VWCreateDB extends JDialog implements Constants
{
    private JLabel lScriptFile, lMasterFile, lDBName, lLogFile, lCollation, lDefaultCollation;
    public JTextField tDBName, tMasterFile, tLogFile, tScriptFile;

    private JScrollPane spDB;
    private JButton btCreate;
    private JButton btClose;
    private JButton btSaveLog;
    private JTextPane tpDBMessages;
    private JComboBox cmbCollation;
    
    private String engine;
    private String dbName;
    private String host;
    private String port;
    private String user;
    private String pass;

    private Connection cn;
    private ResultSet rs;
    private ResultSet rs1;
    private Statement st;

    private String sqlHome = "";
    private String version = "";
    private String jdbcClass = "";
    private String homeQ = "";
    private String versionQ = "";
    private String jdbcUrl = " ";
    String jdbcUrlService =" ";
    private String dbUser;
    private String dbUserName;
    private String dbPassword;
    private VWRoomPanel parent;
    private String collationQ = "";
    
    private Vector collationList;
    private static ResourceManager 
    resourceManager=ResourceManager.getDefaultManager();
    private boolean sqlDBUpdate = false; 
    private boolean oraDBUpdate = false;
    private boolean newOraDB = false;
    private boolean newSqlDB = false;
    private boolean isWindowClosed = false;
    private int userPromptFlag = JOptionPane.YES_NO_CANCEL_OPTION;
    private SimpleAttributeSet sas;
    private boolean SQLPathChanged = false;
    private static final String TITLE = resourceManager.getString("CreateDB.Title1")+" " + resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("CreateDB.Title2");
    private static final String createUserTitle = resourceManager.getString("CreateDB.Title3");

    public String initialDBName ="";
    SQLServerDataSource ds = null;

    VWCreateDB(){
	initComponents();
    }
    public VWCreateDB(Window parentDialog, VWRoomPanel parent, boolean modal, String engine)  
    {
	super(parentDialog);
	setModal(modal);
	this.parent = parent;
	this.engine = engine;
	initComponents();
    }
    public VWCreateDB(VWSSettings parent, boolean modal, String engine) 
    {
	super(parent, modal);
	//this.parent = parent;
	this.engine = engine;
	initComponents();
    }

    public String getOraPath(String user, String pass){     	
	String result = "";
	jdbcClass = "oracle.jdbc.driver.OracleDriver";

	jdbcUrl =  "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;
	jdbcUrlService =  "jdbc:oracle:thin:@//" + host + ":" + port + "/" + dbName;

	String query = "SELECT file_name FROM sys.dba_data_files WHERE upper(tablespace_name)=upper('"+user+"')";
	try{
	    Class.forName(jdbcClass);
	    DriverManager.setLoginTimeout(5); //seconds
	    try{
	    	cn = DriverManager.getConnection(jdbcUrl, user, pass);
	    	printToConsole("jdbcUrl in getOraPath:::::"+jdbcUrl);
	    }
	    catch(Exception e){
	    	
	    	try{
	    		cn = DriverManager.getConnection(jdbcUrlService, user, pass);	 
	    		printToConsole("jdbcUrl in getOraPath:::::"+jdbcUrlService);
	    	}
	    	catch(Exception exp){
	    		printToConsole("Inside exception 2 in getorapath:::::"+e.getMessage());
	    	}
	    	throw e;
	    }
	    st = cn.createStatement();
	    rs = st.executeQuery(query);
	    if (rs.next()){
		result = rs.getString("file_name");
		tMasterFile.setEnabled(false);
		tMasterFile.setText(result);
	    }
	    else{
		tMasterFile.setEnabled(true);
	    }
	}catch(Exception e){
	    tMasterFile.setEnabled(true);
	}
	return result;
    }

	public String getSQLPath(String user, String pass, String dbname) {
		String result = "";
		if (VWSPreferences.isCheckSQL2005())
			jdbcClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		else
			jdbcClass = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
		/*String registryConnectionString = VWSPreferences.getSQLConnectionString();
		if (registryConnectionString != null && registryConnectionString.trim().length() > 0) {
			jdbcUrl = registryConnectionString;
		} else {
			if (VWSPreferences.isCheckSQL2005())
				jdbcUrl = "jdbc:sqlserver://" + host + ":" + port + ";selectMethod=cursor;";
			else
				jdbcUrl = "jdbc:microsoft:sqlserver://" + host + ":" + port + ";";
		}*/
		String query = "select name, filename from sysdatabases where upper(name)=upper('" + dbname + "')";
		String collateQuery = "SELECT convert(varchar(512), DATABASEPROPERTYEX('" + dbname + "', 'Collation')) name"; // "select
		
		try {
			Class.forName(jdbcClass);
			/*DriverManager.setLoginTimeout(5); // seconds
			cn = DriverManager.getConnection(jdbcUrl, user, pass);*/
			cn = getSQLConnectionObject();
			st = cn.createStatement();
			rs = st.executeQuery(query);
			if (rs.next()) {
				result = rs.getString("filename");
				tMasterFile.setText(result);
				String ldfResult = result.substring(0, result.length() - 4);
				tLogFile.setText(ldfResult + ".ldf");
				tMasterFile.setEnabled(false);
			} else {
				tMasterFile.setEnabled(true);
			}
			/*
			 * get the default collation for the current database
			 */

			rs = st.executeQuery(collateQuery);
			if (rs.next()) {
				String collation = rs.getString("name");
				if (collation != null) {
					lDefaultCollation.setText(collation);
					lDefaultCollation.setVisible(true);
					cmbCollation.setVisible(false);
				}
			}

		} catch (Exception e) {
			tMasterFile.setEnabled(true);
		}
		return result;
	}

    private void initComponents() 
    {
	setBackground(new Color(238, 242, 244));
	getContentPane().setBackground(new Color(238, 242, 244));
	btCreate = new JButton(resourceManager.getString("CreateDBbtn.Create"));
	btClose = new JButton(resourceManager.getString("CreateDBbtn.Close"));
	btSaveLog = new JButton(resourceManager.getString("CreateDBbtn.SaveLog"));
	tpDBMessages = new JTextPane();
	tLogFile = new JTextField();
	tDBName = new JTextField();
	tScriptFile = new JTextField();
	tMasterFile = new JTextField();
	lLogFile = new JLabel(resourceManager.getString("CreateDBLbl.LogFile"));
	lScriptFile = new JLabel(resourceManager.getString("CreateDBLbl.ScriptFile"));
	lDBName = new JLabel(resourceManager.getString("CreateDBLbl.DBName"));
	lMasterFile = new JLabel(resourceManager.getString("CreateDBLbl.MastDataFile"));
	spDB = new JScrollPane(tpDBMessages);
	cmbCollation = new JComboBox();
	lCollation = new JLabel(resourceManager.getString("CreateDBLbl.Collation"));
	lDefaultCollation = new JLabel();
	
	getContentPane().setLayout(null);
	setTitle(TITLE);
	setResizable(false);

	getContentPane().add(btSaveLog);
	btSaveLog.setBounds(180, 355, 80, 26);
	btSaveLog.setEnabled(true);
	btSaveLog.setBackground(getBackground());

	getContentPane().add(btCreate);
	btCreate.setBounds(275, 355, 80, 26);
	btCreate.setEnabled(false);
	btCreate.setBackground(getBackground());

	getContentPane().add(btClose);
	btClose.setBounds(370, 355, 80, 26);
	btClose.setBackground(getBackground());
	
	spDB.setBorder(new TitledBorder(resourceManager.getString("CreateDB.TitleBorder")));
	spDB.setBackground(getBackground());
	tpDBMessages.setBackground(new Color(255, 255, 204));
	tpDBMessages.setForeground(Color.blue);
	tpDBMessages.setEditable(false);
	tpDBMessages.setDocument(new DefaultStyledDocument());
	tpDBMessages.setFont(new Font("Arial", Font.PLAIN, 11));
	//tpDBMessages.setFont(new Font("Tahoma", 1, 12));
	tpDBMessages.setRequestFocusEnabled(false);
	getContentPane().add(spDB);
	spDB.setBounds(10, 10, 443, 189);

	getContentPane().add(lScriptFile);
	lScriptFile.setBounds(20, 211, 130, 16);

	getContentPane().add(tScriptFile);
	tScriptFile.setBounds(135, 211, 315, 20);

	getContentPane().add(lDBName);
	lDBName.setBounds(20, 239, 130, 16);

	//tDBName.setEditable(false);
	getContentPane().add(tDBName);
	tDBName.setBounds(135, 239, 315, 20);

	getContentPane().add(lMasterFile);
	lMasterFile.setBounds(20, 267, 130, 16);

	//tMasterFile.setEditable(false);
	getContentPane().add(tMasterFile);
	tMasterFile.setBounds(135, 267, 315, 20);

	getContentPane().add(lLogFile);
	lLogFile.setBounds(20, 295, 130, 16);

	tLogFile.setEditable(false);
	getContentPane().add(tLogFile);
	tLogFile.setBounds(135, 295, 315, 20);

	getContentPane().add(lCollation);
	lCollation.setBounds(20, 325, 130, 16);

	getContentPane().add(cmbCollation);
	cmbCollation.setBounds(135, 325, 315, 20);
	
	getContentPane().add(lDefaultCollation);
	lDefaultCollation.setBounds(135, 323, 315, 20);
	lDefaultCollation.setVisible(false);

	
	addWindowListener(new WindowAdapter() {
	    public void windowClosing(WindowEvent evt) {
		closeDialog();
	    }
	});
	btClose.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
		closeDialog();
	    }
	});
	btCreate.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
		createDB();
	    }
	});
	btSaveLog.addActionListener(new ActionListener() {
	    public void actionPerformed(ActionEvent evt) {
		saveLog();
	    }
	});    

	tMasterFile.addKeyListener(new KeyAdapter(){
	    public void keyReleased(KeyEvent evt) {
		try{
		    String newPath="";
		    String path=tMasterFile.getText();
		    if(path.indexOf(".mdf")!=-1){
			newPath = path.substring(0,path.indexOf(".mdf"));
			tLogFile.setText(newPath+".ldf");
			//SQLPathChanged = true;
		    }	        		

		}catch(Exception exception){
			print("Exception in tmasterfile key listenerr"+exception.getMessage());
		}
	    }
	});

	tDBName.addFocusListener(new FocusAdapter() {
	    public void focusLost(FocusEvent evt) {
		if (engine.equalsIgnoreCase("Oracle")){
		    String userName = tDBName.getText().trim();
		    String password = tDBName.getText().trim();
		    getOraPath(userName, password);                      
		}
		else{
		    String dbname = tDBName.getText().trim();
		    dbName = dbname;
		    getSQLPath(user, pass, dbName);            		
		    parent.tDBName.setText(tDBName.getText());            		 
		}
	    }
	});


	//Make it invisible. later enable if save log functionality is required- PR
	btSaveLog.setVisible(false);
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	setSize(new Dimension(469, 430));
	setLocation((screenSize.width-469)/2,(screenSize.height-430)/2);
    }
    private void hideCollation(){
	lCollation.setVisible(false);
	lDefaultCollation.setVisible(false);
	cmbCollation.setVisible(false);

	btSaveLog.setBounds(180, 325, 80, 26);
	btCreate.setBounds(275, 325, 80, 26);
	btClose.setBounds(370, 325, 80, 26);

	
	setSize(new Dimension(469, 390));
    }
    private void saveLog()
    {

	String LOG_FILE_PREFIX = "DBCreateLog-";

	Document doc = tpDBMessages.getDocument();
	try
	{
	    File logFile = new File(assertLogFolder() + LOG_FILE_PREFIX + 
		    Util.getNow(1) + ".rtf");
	    FileOutputStream fos = new FileOutputStream(logFile);
	    new RTFEditorKit().write(fos, doc, 0, doc.getLength());
	    fos.close();
	}
	catch(Exception e){
		printToConsole(e.fillInStackTrace().getMessage().toString());
	}
	//tpDBMessages.setText("");	

    }
    private String assertLogFolder()
    {
	String LOG_FOLDER = "log";
	File folder = new File(ManagerUtil.getHome() + Util.pathSep + LOG_FOLDER);
	try
	{
	    if (!folder.exists()) folder.mkdir();
	    return folder.getPath() + Util.pathSep;
	}
	catch(Exception e){}
	return "c:" + Util.pathSep;
    }
    private void createDB()
    {
	String databaseName = tDBName.getText().trim().toUpperCase();
	String fileName = tMasterFile.getText();
	String masterFileName = "";
	try{
	    if(fileName.lastIndexOf("\\") != -1){
		masterFileName = fileName.substring(fileName.lastIndexOf("\\")+1,fileName.length() - 4);
	    }
	    else{
		masterFileName = fileName.substring(fileName.lastIndexOf("/")+1,fileName.length() - 4);
	    }
	}catch(Exception ex){
		printToConsole(ex.fillInStackTrace().getMessage().toString());
	}
	if( (!masterFileName.equalsIgnoreCase(databaseName)) && engine.equalsIgnoreCase("Oracle")){
	    Util.Msg(this, resourceManager.getString("CreateDBLbl.Schema"), TITLE);
	    return;
	}

	//if(true) return;
	if (engine.equalsIgnoreCase("SQLServer"))
	    createSqlDB();
	else
	    createOraDB(); 
    }
    private static Vector readSQLChunks(File inf)
    {
	RandomAccessFile inraf = null;
	Vector chunks = new Vector();
	byte[] buffer = new byte[1024];
	String unwrapped = "";
	String line = "";
	String chunk = "";
	int ix;

	try
	{
	    inraf = new RandomAccessFile(inf,"r");
	    while(inraf.read(buffer) != -1)
	    {
		line = line + new String(buffer);
		buffer = new byte[1024];
		while( (ix = line.indexOf("\r\n")) != -1)
		{
		    unwrapped = Util.decryptKey(line.substring(0, ix));
		    if (line.indexOf("\r\n")== 0 && unwrapped.startsWith("/"))chunk=unwrapped + "\n";
		    if (unwrapped.startsWith("/"))
		    {
			chunks.addElement(chunk);
			chunk = "";
		    }
		    else
		    {
			chunk = chunk + unwrapped + "\n";
		    }
		    line = line.substring(ix + 2);
		}
	    }
	    inraf.close();
	}
	catch (Exception e) 
	{
		printToConsole(e.fillInStackTrace().getMessage().toString());
	    return null;
	} 
	return chunks;
    }
    private Vector readORAChunks(File inf)
    {
	Vector chunks = new Vector();
	byte[] buffer = new byte[1024];
	String line = "";
	String chunk = "";
	String wrapped = "";
	int ix;
	print("Extracting " + PRODUCT_NAME + " SQL Objects:...", true);
	int pos = getPos();
	try
	{
	    RandomAccessFile inraf = new RandomAccessFile(inf,"r");
	    while(inraf.read(buffer) != -1)
	    {
		line = line + new String(buffer);
		buffer = new byte[1024];
		while( (ix = line.indexOf("\r\n")) != -1)
		{
		    wrapped = line.substring(0, ix);
		    // Oracle 10g and 11g wrap function result starts with "/" 
		    if (wrapped.startsWith("/") && wrapped.length() == 1)
		    {
			chunks.addElement(chunk);
			clearPos(pos);
			print(String.valueOf(chunks.size()), true, pos);
			chunk = "";
		    }
		    else chunk = chunk + wrapped + "\n";
		    line = line.substring(ix + 2);
		}
	    }
	    inraf.close();
	    print("");
	    printToConsole("Extracting " + PRODUCT_NAME + " SQL Objects:...Done");
	}
	catch (Exception e) 
	{
		printToConsole(e.fillInStackTrace().getMessage().toString());
	    return null;
	} 
	return chunks;
    }

	private void connect() {
		try {
			if (engine.equalsIgnoreCase("SQLServer")) {
				if (VWSPreferences.isCheckSQL2005())
					jdbcClass = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
				else
					jdbcClass = "com.microsoft.jdbc.sqlserver.SQLServerDriver";
				/*String registryConnectionString = VWSPreferences.getSQLConnectionString();
				if (registryConnectionString != null && registryConnectionString.trim().length() > 0) {
					jdbcUrl = registryConnectionString;
				} else {
					if (VWSPreferences.isCheckSQL2005())
						jdbcUrl = "jdbc:sqlserver://" + host + ":" + port + ";selectMethod=cursor;";
					else
						jdbcUrl = "jdbc:microsoft:sqlserver://" + host + ":" + port + ";";
				}*/
				homeQ = "select filename from sysdatabases where name = 'master'";
				printToConsole("jdbc driver  :" + jdbcClass);
				versionQ = "select @@Version As version";
				collationQ = "SELECT name FROM ::fn_helpcollations()";
				String defaultCollateQ = "select convert(sysname, serverproperty('collation')) name";
				String defaultCollate = "";
				Class.forName(jdbcClass);
				/*DriverManager.setLoginTimeout(5); // seconds
				cn = DriverManager.getConnection(jdbcUrl, user, pass);*/
				printToConsole("Before calling getSQLConnectionObject.....");
				cn = getSQLConnectionObject();
				printToConsole("Connection object :"+cn);
				st = cn.createStatement();
				rs = st.executeQuery(homeQ);
				if (rs.next())
					sqlHome = rs.getString("filename");
				rs = st.executeQuery(versionQ);
				if (rs.next())
					version = rs.getString("version");
				rs = st.executeQuery(collationQ);
				// print(" collationQ " + collationQ);
				collationList = new Vector();
				collationList.removeAllElements();
				while (rs.next()) {
					collationList.add(rs.getString("name"));
					// print(rs.getString("name"));
				}
				printToConsole("defaultCollateQ query :"+defaultCollateQ);
				rs = st.executeQuery(defaultCollateQ);
				if (rs.next())
					defaultCollate = rs.getString("name");
				cmbCollation.removeAllItems();
				cmbCollation.addItem("Default");
				if (collationList != null && collationList.size() > 0) {

					for (int i = 0; i < collationList.size(); i++) {
						cmbCollation.addItem(collationList.get(i));
						// print("count: "+cmbCollation.getItemCount());
					}
					if (defaultCollate != null && defaultCollate.length() > 0) {
						cmbCollation.setSelectedItem(defaultCollate);
					}
				}
			} else if (engine.equalsIgnoreCase("Oracle")) {
				printToConsole("Inside connect of oracle ......");
				jdbcClass = "oracle.jdbc.driver.OracleDriver";
				jdbcUrlService = "jdbc:oracle:thin:@//" + host + ":" + port + "/" + dbName;
				jdbcUrl = "jdbc:oracle:thin:@" + host + ":" + port + ":" + dbName;
				homeQ = "SELECT file_name FROM sys.dba_data_files WHERE rownum=1";
				versionQ = "SELECT * FROM V$VERSION";
				// "SELECT BANNER FROM V$VERSION WHERE BANNER LIKE '%Oracle%'";
				Class.forName(jdbcClass);
				printToConsole("Oracle driver loaded.....");
				DriverManager.setLoginTimeout(5); // seconds
				try {
					printToConsole("inside try connect of oracle ......");
					printToConsole("jdbcUrl......." + jdbcUrl + " user :" + user + " pass :" + pass);
					cn = DriverManager.getConnection(jdbcUrl, user, pass);
					printToConsole("cn::::" + cn);

				} catch (Exception e) {
					try {
						printToConsole("inside catch one  ......" + e.getMessage());
						printToConsole("jdbcUrlService......." + jdbcUrlService);
						cn = DriverManager.getConnection(jdbcUrlService, user, pass);
						printToConsole("cn::::" + cn);

					} catch (Exception exp) {
						// HERE only message need to be displayed
						printToConsole("inside catch two  ......" + exp.getMessage());
						print(e.getMessage());

					}

				}
				try {
					printToConsole("before calling statement...");
					st = cn.createStatement();
					printToConsole("homeQ script :" + homeQ);
					rs = st.executeQuery(homeQ);
					printToConsole("Result set :" + rs);
					if (rs.next()) {
						sqlHome = rs.getString("FILE_NAME");
						printToConsole("sqlHome from ora dbs:::::" + sqlHome);
					}
					printToConsole("versionQ >>>>>> :" + versionQ);
					rs = st.executeQuery(versionQ);
					printToConsole("Result set1111 >>>>>> :" + rs);
					while (rs.next())
						version = version + rs.getString("BANNER") + "\r\n";
				} catch (Exception e1) {
					printToConsole("Errow while version:::" + e1.getMessage());
				}

			}
			printToConsole("version :" + version);
			if (version.indexOf("Linux") > 0) {
				sqlHome = sqlHome.substring(0, sqlHome.lastIndexOf("/"));
			} else {
				if (sqlHome.lastIndexOf("/") > 0)
					sqlHome = sqlHome.substring(0, sqlHome.lastIndexOf("/"));
				else
					sqlHome = sqlHome.substring(0, sqlHome.lastIndexOf("\\"));
			}
			printToConsole("sqlHome :" + sqlHome);
			cn.close();
			st.close();
			rs.close();
		} catch (ClassNotFoundException e) {
			print("Cannot load JDBC Driver");
		} catch (SQLException se) {
			print("Cannot connect: " + se.getMessage());
		} catch (Exception ex) {
			print("Cannot connect: " + ex.getMessage());
			print("Cannot connect: " + ex);
		}
	}
    private boolean createOraSchema()
    {
	boolean success = false;
	String scFilePath = tScriptFile.getText();
	File sqlScript = new File(scFilePath);
	if (!sqlScript.exists())
	{
	    Util.Msg(this, resourceManager.getString("CreateDBMSG.DBNameReq")+" "+ sqlScript.getPath(), 
		    TITLE);
	    return false;
	}
	dbUser = tDBName.getText().toUpperCase(); //Oracle needs uppercase!!!
	if(dbUser.equals(""))
	{
	    Util.Msg(this, resourceManager.getString("CreateDBMSG.UserReq"), TITLE);
	    return false;
	}
	if (!Util.Ask(this, resourceManager.getString("CreateDBMSG.Create")+" " + resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("CreateDBMSG.RoomDBSchema")+" '"
		+ dbUser + "' ?", TITLE))
	    return false;
	paintComponents(getGraphics());
	String tsQuery = "CREATE TABLESPACE " + '"' + dbUser + 
	'"' + " NOLOGGING DATAFILE '" + tMasterFile.getText() +
	"' SIZE 60M EXTENT MANAGEMENT LOCAL UNIFORM SIZE 500K";
	String tsAlter = "ALTER DATABASE DATAFILE '" + tMasterFile.getText() +
	"' AUTOEXTEND ON NEXT 1M ";

	String userQuery = "CREATE USER " + '"' + dbUser + '"' + 
	" PROFILE " + '"' + "DEFAULT" + '"' + " IDENTIFIED BY " + '"' + dbUser +
	'"' + " DEFAULT TABLESPACE " + '"' + dbUser + '"' + " ACCOUNT UNLOCK";
	String[] grants = getGrants(dbUser);
	try
	{
		try{
			printToConsole("inside try of create schema");
			
			cn = DriverManager.getConnection(jdbcUrl, user, pass);
			printToConsole("jdbcUrl in ora schema:::"+jdbcUrl);
		}
		catch(Exception e){
			try{
				printToConsole("insie catch of create schema :"+e.getMessage());
				cn = DriverManager.getConnection(jdbcUrlService, user, pass);	   
			}
			catch(Exception exp){
				printToConsole("insie catch two of create schema :"+exp.getMessage());
			}

		}
	    st = cn.createStatement();
	    st.execute(tsQuery);
	    if (st.getWarnings() != null) print(st.getWarnings().getMessage());
	    st.execute("commit");
	    st.execute(tsAlter);
	    if (st.getWarnings() != null) print(st.getWarnings().getMessage());
	    st.execute("commit");
	    print("Created Tablespace '" + dbUser + "'...");
	    st.execute(userQuery);
	    if (st.getWarnings() != null) print(st.getWarnings().getMessage());
	    st.execute("commit");
	    print("Created User '" + dbUser + "'...");

	    for (int i = 0; i < grants.length; i++)
	    {
		st.execute(grants[i]);
		if (st.getWarnings() != null) 
		    print(st.getWarnings().getMessage());
	    }
	    st.execute("commit");
	    print("Granted Privileges to '" + dbUser + "'...");
	    success = true;
	}
	catch(SQLException se)
	{
	    if (se.getErrorCode() == 1543) //tableSpace exists
	    {
		oraDBUpdate = true;
		success = true;
	    }
	    else
	    {
	    printToConsole(se.fillInStackTrace().getMessage().toString());
		print("Error Creating Room Databse: " + se.getMessage());
	    }
	}
	try
	{
	    st.close();
	    rs.close();
	    cn.close();
	    cn = null;
	}
	catch(SQLException se){}
	finally
	{
	    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
	return success;
    }
    private void createOraDB()
    {
	if (!createOraSchema()) return; 
	if (oraDBUpdate)
	    if (!Util.Ask(this, resourceManager.getString("CreateDBMSG.RoomDBSchemaExist")
		    , TITLE))
	    {
		oraDBUpdate = false;
		return;
	    }
	paintComponents(getGraphics());
	setCursor(new Cursor(Cursor.WAIT_CURSOR));
	Vector chunks = readORAChunks(new File(tScriptFile.getText()));
	if (chunks == null)
	{
	    Util.Msg(this, resourceManager.getString("CreateDBMSG.ErrorReadingScript")+" "+ 
		    tScriptFile.getText(), TITLE);
	    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	    return;
	}
	int totalObjects = chunks.size();
	String command;
	String type;
	String object = "";
	String chunk;
	print("Executing " + PRODUCT_NAME + " SQL Objects:...", true);
	int pos = getPos();
	try
	{
	    if (oraDBUpdate){
	    	try{
	    		//jdbcUrl
	    		printToConsole("inside oradb update "+jdbcUrl);
	    		cn = DriverManager.getConnection(jdbcUrl, user, pass);
	    	}
	    	 catch(Exception e){
		    	try{
		    		//jdbcUrlService
		    		cn = DriverManager.getConnection(jdbcUrlService, user, pass);	   
		    	}
		    	catch(Exception exp){
		    	}
		    
		    }
	    	
	    }
	    else{
	    	printToConsole("inside else of oraupdate ");
	    	try{
	        	printToConsole("jdbcUrl inside createdb1111....::::::::"+jdbcUrl);
	    		cn = DriverManager.getConnection(jdbcUrl, dbUser, dbUser);
	    		printToConsole("jdbcUrl inside createdb2222....::::::::"+jdbcUrl);
	    	}
	    	catch(Exception e){
	    		try{
	    			printToConsole("jdbcUrl inside catch222....::::::::"+jdbcUrlService);
	    			cn = DriverManager.getConnection(jdbcUrlService, dbUser, dbUser);
	    			printToConsole("jdbcUrl inside catch2222....::::::::"+jdbcUrlService);
	    		}
	    		catch(Exception exp){
	    			printToConsole("jdbcUrl inside catch333....::::::::"+exp.getMessage());
	    		}

	    	}
	    	
	    }

	    st = cn.createStatement();
	    for (int i = 0; i < chunks.size(); i++) 
	    {
		command = ""; type = ""; object = "";
		chunk = (String) chunks.get(i);
		StringTokenizer token = new StringTokenizer(chunk);
		if (token.hasMoreTokens()) command = token.nextToken(); 
		if (token.hasMoreTokens()) type = token.nextToken();
		if (token.hasMoreTokens()) object = token.nextToken();
		if (object.equalsIgnoreCase("replace"))
		{
		    if (token.hasMoreTokens()) type = token.nextToken();
		    if (token.hasMoreTokens()) object = token.nextToken();
		    int ix = object.indexOf('(');
		    if (ix != -1) object = object.substring(0, ix);
		}
		if (oraDBUpdate)
		{
		    if (chunk.toUpperCase().indexOf("REPLACE") == -1 
			    || command.equalsIgnoreCase("insert"))
		    {
			// specific for migration from 5.6.0 to 5.6.1
			/*
Enhancement No / Purpose:  <633/In a Oracle, execute the Alter table Indexer for updating the room. it has to update the qc column. >
Created by: <Pandiya Raj.M>
Date: <17 Aug 2006>
			 */                         
			/* Purpose:	  To generalize Oracle Script for new and existing room following [BEGIN & IF] condition added
			 * Created by: C.Shanmugavalli		Date: 21 Sep 2006
			 */

			if ((!chunk.startsWith("BEGIN")&&(!type.startsWith("IF"))) && !chunk.startsWith("ALTER TABLE EHHistoryFiles") && !chunk.startsWith("ALTER TABLE Indexer"))
			    continue;
		    }
		}
		try
		{
			printToConsole("Executing : " + chunk);
		    st.execute(chunk);
		    st.execute("commit");
		    clearPos(pos);
		    print(String.valueOf(i+1) + "/" + totalObjects, true, pos);		    
		}
		catch(SQLException se)
		{
		    String msg = se.getMessage();
		    printToConsole("Oracle Exception 1 :"+msg);
		    // column alread exists in EHHistoryFiles		    
		    if (msg.startsWith("ORA-01430")) continue;
		    print("");
		    print(object + " ==> " + msg);
		    print("Executing " + PRODUCT_NAME + " SQL Objects:...", true);
		    pos = getPos();		    
		}
	    }
	    //String UseDB = "Use " + dbName;  
	    if(!oraDBUpdate){
		//st.execute(UseDB);
		//st.execute("CreateJob '0'");
		OracleCallableStatement ocs = null;
		try{
		    ocs = (OracleCallableStatement) cn.prepareCall("{call CreateJob(?)}");
		    ocs.setString(1, "0");
		    ocs.execute();
		    if (ocs != null)  { ocs.close(); ocs = null;}
		}catch(SQLException ex){
		    print("Exception in Oracle CreateJob " + ex.getMessage());
		    printToConsole(ex.fillInStackTrace().getMessage().toString());
		    if (ocs != null)  { ocs.close(); ocs = null;}
		}
	    }
	    //Upgradation data to 6.0
            if (oraDBUpdate){

            	OracleCallableStatement ocs = null;
	            try{
	            	//ocs = (OracleCallableStatement) cn.prepareCall("{call VW_CheckConsistency}");
	            	ocs = (OracleCallableStatement) cn.prepareCall("{call VW_MigrateRouteAddIns}");	            	
	            	ocs.execute();
	            	if (ocs != null)  { ocs.close(); ocs = null;}
	            }catch(SQLException ex){
	            	print("Exception in Oracle CheckConsistency " + ex.getMessage());
	            	printToConsole(ex.fillInStackTrace().getMessage().toString());
	            	if (ocs != null)  { ocs.close(); ocs = null;}
	            }


            }
         //    End*/
	    print("");
	    if (oraDBUpdate)
		print("Updated Room Database '" + dbUser);
	    else
	    {
		newOraDB = true;    
		print("Created Room Database '" + dbUser);
	    }
	    btCreate.setEnabled(false);
	}
	catch(SQLException se)
	{
	    print(object + " ==> " + se.getMessage());
	    printToConsole(se.fillInStackTrace().getMessage().toString());
	}
	try
	{
	    st.close();
	    rs.close();
	    cn.close();
	    cn = null;
	}
	catch(SQLException se){}
	setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    private void addSQLUserListener()
    {
	tDBName.addKeyListener(new KeyAdapter(){
	    public void keyReleased(KeyEvent ke){
		if(tMasterFile.getText().lastIndexOf("\\")!=-1){
		    String filePath = tMasterFile.getText();
		    String filePath1 = filePath.substring(0,filePath.lastIndexOf("\\"));
		    String mdfPath = filePath1+"\\"+tDBName.getText()+".mdf";
		    String ldfPath = filePath1+"\\"+tDBName.getText()+"_Log.ldf";
		    tMasterFile.setText(mdfPath);
		    tLogFile.setText(ldfPath);
		}
	    }
	});
    }


    private void addUserListener()
    {
	tDBName.getDocument().addDocumentListener(new DocumentListener(){
	    public void changedUpdate(DocumentEvent e){
		updateTSFile();
	    } 
	    public void insertUpdate(DocumentEvent e){
		updateTSFile();
	    } 
	    public void removeUpdate(DocumentEvent e){
		updateTSFile();
	    }
	});
    }
    private void updateTSFile()
    {
	String OraUser = tDBName.getText();
	if (OraUser.length() < 1) return;
	String TSFile = tMasterFile.getText();
	if(TSFile.lastIndexOf("\\")>0)
	    TSFile = TSFile.substring(0, TSFile.lastIndexOf("\\") + 1);
	else
	    TSFile = TSFile.substring(0, TSFile.lastIndexOf("/") + 1);
	TSFile = TSFile + OraUser + ".DBF";
	tMasterFile.setText(TSFile);
    }
    /**
     * This method is for creating a login for the new database.
     * @return
     */
    private boolean createSQLSchema()
    {
		boolean isSQLSchemaCreated = false;
		boolean isLoginCreated = false;
		boolean isExceptionThrown = false;
	
		String sqlErrorMessage = "";
		String databaseName = tDBName.getText().trim();
		if(databaseName.equals(""))
		{
		    Util.Msg(this, resourceManager.getString("CreateDBMSG.DBNameReq"), TITLE);
		    return false;
		}
		// To check the existance of database - 2000
		String dbExistsQuery = "SELECT name FROM sysdatabases Where name='"+databaseName+"'";
		// To check the existance of database - 2005
		String dbExistsQuery2005 = "SELECT name FROM sys.databases Where name='"+databaseName+"'";
		// To check the mapping of user and database - 2000 
		String dbUserMappingQuery = "SELECT '"+databaseName+"', name FROM "+databaseName+".dbo.sysusers " +
		"WHERE sid in (Select sid from syslogins where ltrim(Rtrim(name)) = '"+user+"')";
		// To check the mapping of user and database - 2000 
		String userName = user;
		if ( ds != null) {
			String roomName = parent.tName.getText();
			String domain = VWSPreferences.getDomain(roomName);
			domain = domain.substring(0, domain.indexOf("."));
			userName = domain+"\\"+userName;
		}
		printToConsole("userName --->:"+userName);
		String dbUserMappingQuery2005 = "SELECT '"+databaseName+"', name FROM ["+databaseName+"].sys.database_principals " +
		"WHERE sid in (Select sid from sys.server_principals where ltrim(Rtrim(name)) = '"+userName+"')";
		
		// use master database.
		String umQuery = "use master";
		// To get SQL version 
		String sqlVersionQuery = "Select @@Version as Version";
	
		String str[] = new String[3];
		try
		{
			printToConsole("ds object in createSQLSchema ------->:"+ds);
			if ( ds != null) {
				cn = ds.getConnection();
				printToConsole("After the connection....");
			} else {
				cn = DriverManager.getConnection(jdbcUrl, user, pass);
			}
			printToConsole("before calling createstatement....");
		    st = cn.createStatement();
		    printToConsole("before executing the umQuery query....");
		    st.execute(umQuery);
		    //if (st.getWarnings() != null) print(st.getWarnings().getMessage());
		    String result = "";
		    printToConsole("before executing the sqlVersionQuery query....");
		    rs = st.executeQuery(sqlVersionQuery);
		    printToConsole("sqlVersionQuery quresultset...."+rs);
		    if (st.getWarnings() != null) print(st.getWarnings().getMessage());
		    if (rs.next()){
		    	result = rs.getString("Version");
		    }
		    result = result.substring(0,26);
		    printToConsole("result...."+result);
		    if(result.equalsIgnoreCase("Microsoft SQL Server  2000")){
		    	rs = st.executeQuery(dbExistsQuery);
		    	if (st.getWarnings() != null) print(st.getWarnings().getMessage());
				if(rs.next()){
				    rs1 = st.executeQuery(dbUserMappingQuery);
				    if (st.getWarnings() != null) print(st.getWarnings().getMessage());
				    if(rs1.next()){
						sqlDBUpdate = true;
						dbUserName = user;
						dbPassword = pass;
						return true;
				    }else{
						Util.Msg(this, resourceManager.getString("CreateDBMSG.UserName_Password")+" "+databaseName, TITLE);
						isWindowClosed = true;
						newSqlDB = false;
						sqlDBUpdate = false;
						closeDialog();
						return false;
				    }
				}else{
				    //The message change as per rajesh request : Do you want to create a new user?				     
				    userPromptFlag = Util.Option(this, resourceManager.getString("CreateDBMSG.userPromptFlag1")+user+" "+resourceManager.getString("CreateDBMSG.userPromptFlag2")
				    		+" "+ resourceManager.getString("CVProduct.Name") + " "+resourceManager.getString("CreateDBMSG.userPromptFlag3") , TITLE);	                	 
				    //}
				    if(userPromptFlag == JOptionPane.YES_OPTION){
						VWCreateUserLogin login= new VWCreateUserLogin(this,resourceManager.getString("CreateDBMSG.UserLogin"));
						str = login.getValues();
						String cancel = login.getCancel();
						dbUserName = str[0];
						dbPassword = str[1];
						isWindowClosed = false;
						sqlDBUpdate = false;
						if(cancel!=null && cancel.equalsIgnoreCase("Cancel")){
						    btCreate.setEnabled(false);
						    dbUserName = user;
						    dbPassword = pass;
						    return true;
						}
						if(login.getClose()){
						    newSqlDB = false;
						    sqlDBUpdate = false;
						    isWindowClosed = true;
						    closeDialog();
						    return false;
						}
				    }else if(userPromptFlag == JOptionPane.NO_OPTION){
						sqlDBUpdate = false;
						newSqlDB = true;
						dbUserName = user;
						dbPassword = pass;
						return true;
				    }else{
						sqlDBUpdate = false;
						newSqlDB = false;
						isWindowClosed = false;	
						return false;
				    }
				}
				String uQuery2000 = "select name as Fld from syslogins where name = '"+dbUserName+"'";
		
				// To check the existance of user in SQL Server 2000
				rs = st.executeQuery(uQuery2000);
				if (st.getWarnings() != null) print(st.getWarnings().getMessage());
				if (rs.next()){
				    isLoginCreated = false;
					isSQLSchemaCreated = true;
				    //sqlDBUpdate = true;
				}else{
				    isLoginCreated = true;
				    //sqlDBUpdate = false;
				    // Creating user for SQL 2000
				    String clQuery2000 = "EXEC sp_addlogin '"+dbUserName+"','"+dbPassword+"'";
				    st.execute(clQuery2000);
				    if (st.getWarnings() != null) print(st.getWarnings().getMessage());
				}
				
				// Execute Master
			    String enQuery = "EXEC master..sp_addsrvrolemember @loginame = N'"+dbUserName+"', @rolename = N'dbcreator'";
		
			    // Create New Login
			    if(isLoginCreated){
			    	paintComponents(getGraphics());
			    	st.execute(enQuery);
			    	//if (st.getWarnings() != null) print(st.getWarnings().getMessage());
			    	print("Created Login '" + dbUserName + "'...");
			    	// Execute the scripts
			    	String[] grants = getGrantsSQL();
					for (int i = 0; i < grants.length; i++)
					{
					    st.execute(grants[i]);
					    if (st.getWarnings() != null) 
						print(st.getWarnings().getMessage());
					}
					print("Granted Privileges to '" + dbUserName + "'...");
					isSQLSchemaCreated = true;
				}
		    }
		    else{ // Starting of 2005 version
		    	printToConsole("Before executing the dbExistsQuery2005 query :"+dbExistsQuery2005);
				rs = st.executeQuery(dbExistsQuery2005);
				printToConsole("After executing the dbExistsQuery2005 query :"+rs);
				if (st.getWarnings() != null) print(st.getWarnings().getMessage());
				if(rs.next()){
					printToConsole("Before executing the dbUserMappingQuery2005 query :"+dbUserMappingQuery2005);
				    rs1 = st.executeQuery(dbUserMappingQuery2005);
				    printToConsole("After executing the dbUserMappingQuery2005 query :"+rs1);
				    printToConsole("st.getWarnings() :"+st.getWarnings());
				    if (st.getWarnings() != null) print(st.getWarnings().getMessage());
				    if(rs1.next()){
				    	printToConsole("inside update.......");
						sqlDBUpdate = true;
						dbUserName = user;
						dbPassword = pass;
						return true;
				    }else{
						Util.Msg(this, "Enter correct username and password for "+databaseName, TITLE);
						isWindowClosed = true;
						newSqlDB = false;
						sqlDBUpdate = false;
						closeDialog();
						return false;
				    }
				}else{
				    //userPromptFlag = Util.Option(this, "Do you want to create a new user?", TITLE);
				    //The message change as per rajesh request : Do you want to create a new user?				     
				    userPromptFlag = Util.Option(this, resourceManager.getString("CreateDBMSG.userPromptFlag1")+ user +" "+resourceManager.getString("CreateDBMSG.userPromptFlag2")+" "+ resourceManager.getString("CVProduct.Name") +" "+ resourceManager.getString("CreateDBMSG.userPromptFlag3"),TITLE);	                	 
				    if(userPromptFlag == JOptionPane.YES_OPTION){
						VWCreateUserLogin login= new VWCreateUserLogin(this,resourceManager.getString("CreateDBMSG.UserLogin"));
						str = login.getValues();
						String cancel = login.getCancel();
						if(cancel!=null && cancel.equalsIgnoreCase("Cancel")){
						    btCreate.setEnabled(false);
						    dbUserName = user;
						    dbPassword = pass;			    
						    return true;
						}
						if(login.getClose()){
						    newSqlDB = false;
						    sqlDBUpdate = false;
						    isWindowClosed = true;
						    closeDialog();
						    return false;
						}
						dbUserName = str[0];
						dbPassword = str[1];
				    }else if (userPromptFlag == JOptionPane.NO_OPTION){			
						sqlDBUpdate = false;
						newSqlDB = true;
						dbUserName = user;
						dbPassword = pass;
						return true;
				    }else{
						sqlDBUpdate = false;
						newSqlDB = false;
						isWindowClosed = false;
						return false;
				    }
				}
		
				String uQuery2005 = "select name as Fld from sys.server_principals where name = '"+dbUserName+"'";
				// To check the existance of user in SQL Server 2005
				rs = st.executeQuery(uQuery2005);
				if (st.getWarnings() != null) print(st.getWarnings().getMessage());
				if (rs.next()){
				    isLoginCreated = false;
					isSQLSchemaCreated = true;
				}else{
				    isLoginCreated = true;
				}
				// Creating user for SQL 2005
				try{
				    String clQuery2005 = "CREATE LOGIN "+dbUserName+" WITH PASSWORD = '"+dbPassword+"'"; 
				    st.execute(clQuery2005);
				    //if (st.getWarnings() != null) print(st.getWarnings().getMessage());
				}catch(SQLException ex){
					if(ex.getErrorCode()!=-1){
						isExceptionThrown = true;
						sqlErrorMessage = ex.getMessage();
					}
				}
				
				if(isExceptionThrown){
					isExceptionThrown = false;
					isLoginCreated = false;
					isWindowClosed = false;
					Util.Msg(this, sqlErrorMessage, createUserTitle);
					btCreate.setEnabled(true);
					newSqlDB = false;
					sqlDBUpdate = false;
					return false;
				}

				// Execute Master
			    String enQuery = "EXEC master..sp_addsrvrolemember @loginame = N'"+dbUserName+"', @rolename = N'dbcreator'";
		
			    // Create New Login
			    if(isLoginCreated){
			    	paintComponents(getGraphics());
			    	st.execute(enQuery);
			    	//if (st.getWarnings() != null) print(st.getWarnings().getMessage());
			    	print("Created Login '" + dbUserName + "'...");
			    	// Execute the scripts
			    	String[] grants = getGrantsSQL();
					for (int i = 0; i < grants.length; i++)
					{
					    st.execute(grants[i]);
					    if (st.getWarnings() != null) 
						print(st.getWarnings().getMessage());
					}
					print("Granted Privileges to '" + dbUserName + "'...");
					isSQLSchemaCreated = true;
				}
		    }// End of else part of 2005
		}
		catch(SQLException se)
		{
		    if(se.getErrorCode()!=-1){
				isExceptionThrown = true;
				sqlErrorMessage = se.getMessage();
				printToConsole(se.fillInStackTrace().getMessage().toString());
			}
		}
		if(isExceptionThrown){
			isExceptionThrown = false;
			isLoginCreated = false;
			isWindowClosed = false;
			Util.Msg(this, sqlErrorMessage, createUserTitle);
			btCreate.setEnabled(true);
			newSqlDB = false;
			sqlDBUpdate = false;
			return false;
		}
		if(isWindowClosed){
		    newSqlDB = false;
		    sqlDBUpdate = false;
		    closeDialog();
		    return false;
		}
		try
		{
		    st.close();
		    rs.close();
		    cn.close();
		    cn = null;
		}
		catch(SQLException se){}
		finally
		{
		    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
		}
		return isSQLSchemaCreated;
    }
    
    private void createSqlDB()
    {
	btCreate.setEnabled(false);
	boolean SQLSchema = createSQLSchema(); 
	//SQLSchema returns true to create / update database 
	//if it returns false - to cancel create database / back to server setting
	if(!SQLSchema && isWindowClosed){
	    return;
	} else if (SQLSchema){
	
	String confirm = "";
	String chunk = "";
	String scFilePath = tScriptFile.getText();
	File sqlScript = new File(scFilePath);
	if (!sqlScript.exists())
	{
	    Util.Msg(this, resourceManager.getString("CreateDBMSG.LocateFile") +" "+ scFilePath, TITLE);
	    return;
	}
	Vector chunks = readSQLChunks(sqlScript);
	if (chunks == null)
	{
	    Util.Msg(this, resourceManager.getString("CreateDBMSG.ErrorReadingMSScript") + " "+
		    scFilePath, TITLE);
	    return;
	}

	try
	{
		printToConsole("ds object in createSqlDB------->:"+ds);
		if ( ds != null) {
			cn = ds.getConnection();
		} else {			
			cn = DriverManager.getConnection(jdbcUrl, dbUserName, dbPassword);
		}	    
	    st = cn.createStatement();
	    rs = st.executeQuery("select dbid from sysdatabases where name = '" 
		    + dbName + "'");
	    if (rs.next() && rs.getString("dbid").length() > 0 )
	    {
		if (Util.Ask(this, resourceManager.getString("CreateDBMSG.RoomDBSchemaExist")
			, TITLE)){
		    sqlDBUpdate = true;
		}
		else
		{
		    btCreate.setEnabled(true);
		    st.close();
		    rs.close();
		    cn.close();
		    cn = null;
		    setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); 
		    return;
		}
	    }
	    paintComponents(getGraphics());
	    setCursor(new Cursor(Cursor.WAIT_CURSOR));

	    if (!sqlDBUpdate)
	    {
		//String CreateDB = "Create database " + dbName;
		String collate = "COLLATE ";
		boolean isCollateAdded = false;
		if (cmbCollation.getSelectedItem() != null && !cmbCollation.getSelectedItem().equals("Default")){
		    collate += cmbCollation.getSelectedItem();
		    isCollateAdded = true;
		}
		//Modified the initial size and file growth size as per rajesh request
		String CreateDB = "Create database " + dbName+" "+
		"ON" +
		"( NAME = '"+dbName+"_dat',"+
		"FILENAME = '"+tMasterFile.getText()+"',"+
		"SIZE = 10MB, MAXSIZE = UNLIMITED, FILEGROWTH = 1MB ) "+            		
		"LOG ON"+
		"( NAME = '"+dbName+"_ldf',"+
		"FILENAME = '"+tLogFile.getText()+"',"+
		"SIZE = 5MB,  MAXSIZE = UNLIMITED, FILEGROWTH = 10%) " + (isCollateAdded ?collate:"");

		//"GO";




		st.execute(CreateDB);
		if (st.getWarnings() != null) print(st.getWarnings().getMessage());
	    }
	    st.execute("Use " + dbName);
	    if (st.getWarnings() != null) print(st.getWarnings().getMessage());
	    if (VWSPreferences.getDebug())
		print("\nExecuting " + PRODUCT_NAME + " SQL Objects:...", true);
		int pos1 = getPos();
	    printToConsole(" chunk Size : " + chunks.size());
	    for (int i = 0; i < chunks.size(); i++)
	    {
		chunk = (String) chunks.get(i);
		if (sqlDBUpdate)
		{
		    /*
		     * Purpose:  633-2/ARS introduced 3 new tables and if ARS needs to implemneted to existing room
		     * they system should allow create table statement for those tables [DoctypeRetentionSetting,ARSProcessedDocs,DisposalActionLookUp
		     * Created by: Shanmugavalli.C		Date: <24 Aug 2006> 
		     */
			try{
		    if( !chunk.startsWith("CREATE Table DisposalActionLookUp") && !chunk.startsWith("CREATE Table ARSProcessedDocs") &&
			    !chunk.startsWith("CREATE Table DoctypeRetentionSetting")&&
			    chunk.toUpperCase().startsWith("CREATE TABLE") || chunk.toUpperCase().startsWith("INSERT INTO")  ||
			    (!chunk.toUpperCase().startsWith("CREATE PROCEDURE") && chunk.toUpperCase().indexOf("DROP TABLE") > 0)	)
			continue;
		    //Code added to print the executing Sql chunk count
		    clearPos(pos1);
			int totalObjects=chunks.size();
		    clearPos(pos1);
		    if (VWSPreferences.getDebug())
		    print(String.valueOf(i+1) + "/" + totalObjects, true,pos1);
		    clearPos(pos1);
		    printToConsole("Executing : " + chunk);
		}
			catch(Exception ch){
				printToConsole("Exception inside catch:::1");
				String msg=ch.getMessage();
				printToConsole(ch.fillInStackTrace().getMessage().toString());
			}
		// try catch added to handle sql exception - Valli
		}
		try
		{
		    printToConsole("Executing : " + chunk);
		    st.execute(chunk);
		}catch(SQLException se){
		    String msg = se.getMessage();
		    printToConsole(se.fillInStackTrace().getMessage().toString());
		    if( (msg.indexOf("There is already an object named")>0)||(se.getErrorCode() == 2714) )
			continue;
		    printToConsole(msg);
		    //print(msg);
		}

		if (st.getWarnings() != null)
		{
		    String warn = st.getWarnings().getMessage();
		    if (warn.indexOf("stored procedure will still be created") 
			    > 0) continue;
		    print(warn);
		}
	    }
	    if (sqlDBUpdate)
		print("\nUpdated Room Database '" + dbName + "'.");
	    else{
		newSqlDB = true;
		print("\nCreated Room Database '" + dbName + "'.");
	    }




	    //------------------------Create Text Catalog-----------------------
	    String catalog = dbName + "_Catalog";
	    if (!sqlDBUpdate) 
	    {
		String UseDB = "Use " + dbName;
		st.execute(UseDB);

		st.execute("sp_fulltext_database 'enable'");
		st.execute("sp_fulltext_catalog '" + catalog + "','create'");    
		st.execute("sp_fulltext_table  'Indexer','create','" + catalog +
		"','IX_Indexer'");
		st.execute("sp_fulltext_column  'Indexer','PageContents','add'"); 
		st.execute("sp_fulltext_table  'Indexer','activate'");
		st.execute("sp_fulltext_catalog  '" +  catalog + 
		"','start_incremental'");

		print("Created Text Catalog '" + catalog + "'.");

		st.execute("USE msdb");
		st.execute("sp_add_job @job_name='" + dbName + "_Job'");
		st.execute("sp_add_jobserver @job_name = '" + dbName + "_Job'");

		st.execute("sp_add_jobstep @job_name='" + dbName + "_Job'," +
			"@step_id=1," +
			"@step_name ='Full-Text Indexing'," +
			"@subsystem ='TSQL'," +
			"@command ='use " + dbName + " exec sp_fulltext_catalog " + 
			catalog + ", start_incremental'," +
			"@cmdexec_success_code=0," +
			"@on_success_action =1," +
			"@on_success_step_id =1," +
			"@on_fail_action =2," +
			"@on_fail_step_id =0, " +
			"@retry_attempts =0," +
			"@retry_interval =0," +
		"@os_run_priority =0");

		print("Created Job '" + dbName + "_Job'.");

		st.execute("sp_add_jobschedule @job_name = '" + dbName + 
			"_Job'," +
			"@name = '" + dbName + "_Schedule'," +
			"@freq_type =4," +
			"@freq_interval =1," +
			"@freq_subday_type =4," +
			"@freq_subday_interval =5," +
			"@freq_relative_interval =0," +
		"@freq_recurrence_factor =0"); 

		print("Scheduled Job '" + dbName + 
		"_Job' to execute every 5 minutes.");
	    }
	    else
	    {
	    try{
		//check if this was migrated from a 55 Room
		rs = st.executeQuery("if exists (select * from" + 
			" dbo.sysfulltextcatalogs where name = '" + PRODUCT_NAME + "Catalog')" + 
		" select fld=1 else select fld=0");
		if (rs.next() && rs.getInt("fld") == 1) //this is a 55 Room
		{
		    st.execute("sp_configure 'allow updates',1");
		    st.execute("RECONFIGURE WITH OVERRIDE");
		    st.execute("update dbo.sysfulltextcatalogs set" +
			    " name ='" + catalog + 
		    "' where name = '" + PRODUCT_NAME + "Catalog'");
		    st.execute("sp_configure 'allow updates',0");
		    st.execute("RECONFIGURE WITH OVERRIDE");
		}

		st.execute("sp_fulltext_table  'Indexer','create','" + catalog +
		"','IX_Indexer'");
		st.execute("sp_fulltext_column  'Indexer','PageContents','add'"); 
		st.execute("sp_fulltext_table  'Indexer','activate'");
	    }catch (Exception e) {
	    		print("Exception in FTS " + e.getMessage());
	    		printToConsole(e.fillInStackTrace().getMessage().toString());
				// TODO: handle exception
		}
	    }
	    /*Upgradation data to 6.0*/                
	    String UseDB = "Use " + dbName;                
	    try{
		st.execute(UseDB);
		st.execute("VW_CheckConsistency");
		st.execute("VW_MigrateRouteAddIns"); 
	    }catch(SQLException ex){
		print(ex.getMessage());     	
	    }
	    /* End*/

	}
	catch (SQLException se)
	{
	    print(se.getMessage());
	    printToConsole(se.fillInStackTrace().getMessage().toString());
	}
	try
	{
	    st.close();
	    rs.close();
	    cn.close();
	    cn = null;
	}
	catch (SQLException se){}
	setCursor(new Cursor(Cursor.DEFAULT_CURSOR)); 
	}else{
	    btCreate.setEnabled(true);   
	    setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
	}
    }
    public void show(String dbName, String host, String port, String user, 
	    String pass)
    {
	this.dbName = dbName;
	this.host = host;
	this.port = port;
	this.user = user;
	this.pass = pass;

	initialDBName = dbName;
	printToConsole("Before calling connect....");
	connect();
	printToConsole("After calling connect....");
	if (engine.equalsIgnoreCase("Oracle")){
		printToConsole("before calling getorapath in show ::::");
	    getOraPath(this.user, this.pass);
	    printToConsole("after calling getorapath in show ::::");
	    hideCollation();
	}


	if (version.length() > 0)
	{
	    print(version);
	    print("Connected...");
	    print("");
	    btCreate.setEnabled(true);
	    if (engine.equalsIgnoreCase("SQLServer"))
		writeSqlDBFiles();
	    else
		writeOraDBFiles();

	    if (engine.equalsIgnoreCase("SQLServer")){
		getSQLPath(this.user, this.pass, this.dbName);
	    }

	}
    }
    private void writeSqlDBFiles()
    {
	String scFilePath = VWSUtil.getHome() + Util.pathSep + "sql script" + 
	Util.pathSep +"MSScript.vww";
	tScriptFile.setText(scFilePath);
	tDBName.setText(dbName);
	tMasterFile.setText(sqlHome + "\\" +  dbName + ".mdf");
	tLogFile.setText(sqlHome + "\\" + dbName + "_log.ldf");
	addSQLUserListener();
    }
    private void writeOraDBFiles()
    {
	String schemaUser = "";
	String scFilePath = VWSUtil.getHome() + Util.pathSep + "sql script" + 
	Util.pathSep + "OraScript.vww";
	tScriptFile.setText(scFilePath);
	lDBName.setText("Schema User:");
	if (user.equalsIgnoreCase("system"))
	    schemaUser = "VW560";
	else
	    schemaUser = user;
	tDBName.setText(schemaUser);    
	tDBName.setEditable(true);
	lMasterFile.setText("TableSpace:");
	/*        if (version.indexOf("Linux") > 0)
            tMasterFile.setText(sqlHome + "/" + schemaUser + ".DBF");
        else
            tMasterFile.setText(sqlHome + "\\" + schemaUser + ".DBF");*/
	tLogFile.setVisible(false);
	lLogFile.setVisible(false);
	addUserListener();
	tDBName.requestFocus();
    }
    private void print(String str)
    {
	print(str, false);
    } 
    private void print(String str, boolean progress)
    {
	print(str, progress, getPos());
    }
    private void print(String str, boolean progress, int pos)
    {
	Rectangle r = null;
	Document doc = tpDBMessages.getDocument();
	printToConsole(str);
	try
	{
	    if (progress)
		doc.insertString(pos, str, sas);
	    else
		doc.insertString(pos, str + "\r\n", sas);
	    r = tpDBMessages.modelToView(doc.getLength());
	}
	catch(Exception e){
		printToConsole(e.fillInStackTrace().getMessage().toString());
	}
	r.setSize(tpDBMessages.getSize());
	tpDBMessages.scrollRectToVisible(r);

	spDB.paintImmediately(tpDBMessages.getVisibleRect());
	tpDBMessages.paintImmediately(tpDBMessages.getVisibleRect());	
    }
    private int getPos()
    {
	return tpDBMessages.getDocument().getLength();
    }
    private void clearPos(int pos)
    {
	Document doc = tpDBMessages.getDocument();
	try
	{
	    doc.remove(pos, getPos()- pos);
	}
	catch(Exception e){
		printToConsole(e.fillInStackTrace().getMessage().toString());
	}
    }
    private String[] getGrants(String dbUser)
    {
	String[] grants = 
	{ 
		" GRANT ALTER ANY TABLE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION",
		" GRANT CREATE ANY CLUSTER TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION" ,
		" GRANT CREATE ANY DIRECTORY TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE ANY INDEX TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE ANY PROCEDURE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE ANY SEQUENCE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE ANY TABLE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE SESSION TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE ANY TRIGGER TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE PROCEDURE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE SEQUENCE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE TABLE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		// added for view permission - Search procedure required CREATE VIEW Permission in VW6.1
		" GRANT CREATE VIEW TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE ANY VIEW TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		//End
		" GRANT CREATE TRIGGER TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT INSERT ANY TABLE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT SELECT ANY SEQUENCE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT SELECT ANY TABLE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT EXECUTE ANY PROCEDURE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT UNLIMITED TABLESPACE TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE ANY JOB TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT CREATE JOB TO " + '"' + dbUser + '"' +
		" WITH ADMIN OPTION " ,
		" GRANT " + '"' + "CONNECT" + '"' + " TO " + '"' + dbUser + '"' ,
		" GRANT " + '"' + "DBA" + '"' + " TO " + '"' + dbUser + '"'
	};
	return grants;
    }
    private String[] getGrantsSQL() {
	String[] grants = 
	{ 
		" USE msdb" +
		" GRANT EXECUTE ON msdb.dbo.sp_help_category TO public " +
		" GRANT EXECUTE ON msdb.dbo.sp_add_category TO public  " +
		" GRANT EXECUTE ON msdb.dbo.sp_add_job TO public " +
		" GRANT EXECUTE ON msdb.dbo.sp_add_jobserver TO public  " +
		" GRANT EXECUTE ON msdb.dbo.sp_add_jobstep TO public  " +
		" GRANT EXECUTE ON msdb.dbo.sp_add_jobschedule TO public  " +
		" GRANT EXECUTE ON msdb.dbo.sp_help_job TO public  " +
		" GRANT EXECUTE ON msdb.dbo.sp_delete_job TO public  " +
		" GRANT EXECUTE ON msdb.dbo.sp_help_jobschedule TO public  " +
		" GRANT EXECUTE ON msdb.dbo.sp_verify_job_identifiers TO public  " +
		" GRANT SELECT ON msdb.dbo.sysjobs TO public  " +
		" GRANT SELECT ON msdb.dbo.syscategories TO public  "
	};
	return grants;
    }
    private void closeDialog() 
    {
	if (newOraDB)
	{
	    parent.setUser(dbUser);
	    parent.setPass(dbUser);
	    Util.Msg(this, "Database User and password will be changed to " 
		    + dbUser + "\n" + 
		    "You need to click 'Apply' to save the new settings", TITLE);
	}

	if (newSqlDB)
	{
	    dbName = tDBName.getText().trim();
	    parent.setUser(dbUserName);
	    parent.setPass(dbPassword);
	    parent.setDatabaseName(dbName);
	    String roomName = parent.tName.getText();

	    VWSPreferences.setDBUserPassword(roomName, dbUserName, dbPassword);
	    VWSPreferences.setDBName(roomName, dbName);

	    parent.bRegister3.setEnabled(false);
	    parent.synchronizeRooms();

	}
	setVisible(false);
	dispose();
    }

	public static void printToConsole(String msg) {
		try {
			if (VWSPreferences.getOutputDebug()) {
				String now = new SimpleDateFormat("yyyy/MM/dd/HH:mm:ss").format(Calendar.getInstance().getTime());
				msg = now + " : " + msg + " \n";
				// java.util.Properties props = System.getProperties();
				String tempPath = System.getProperty("java.io.tmpdir");
				String filepath = tempPath + File.separator + "VWCreateDB.log";
				File log = new File(filepath);
				FileOutputStream fos = new FileOutputStream(log, true);
				fos.write(msg.getBytes());
				fos.close();
				fos.close();
				log = null;
			}
		} catch (Exception e) {
			// System.out.println( "printToConsole");
		}
	}

	public static void main(String[] args) {
	try{
	    String plasticLookandFeel  = "com.jgoodies.looks.plastic.PlasticXPLookAndFeel";
	    UIManager.setLookAndFeel(plasticLookandFeel);
	}catch(Exception ex){}

	VWCreateDB cdb = new VWCreateDB();
	cdb.setVisible(true);		
    }
	
	public Connection getSQLConnectionObject() {
		printToConsole("inside getSQLConnectionObject......");
		Connection con = null;	
		String roomName = parent.tName.getText();
		String sqlConnectionString = VWSPreferences.getSQLConnectionString(roomName);
		printToConsole("sqlConnectionString :"+sqlConnectionString);
		if (sqlConnectionString != null && sqlConnectionString.trim().length() > 0) {
			jdbcUrl = sqlConnectionString;
		} else {
			String authenticationMode = VWSPreferences.getAuthenticationMode(roomName);
			printToConsole("authenticationMode :"+authenticationMode);
			if (authenticationMode != null && authenticationMode.trim().length() > 0
					&& authenticationMode.equalsIgnoreCase("windows")) {
				try {
					printToConsole("Before creating data source object.....");
					ds = new SQLServerDataSource();
					printToConsole("Before setting server name--->"+host);
					ds.setServerName(host);
					printToConsole("Before setting server port--->"+port);
					ds.setPortNumber(Integer.parseInt(port));
					boolean integratedSecurity = VWSPreferences.getIntegratedSecurity(roomName);
					printToConsole("Before setting integratedSecurity--->"+integratedSecurity);
					ds.setIntegratedSecurity(integratedSecurity);
					printToConsole("Before setting AuthenticationScheme--->"+VWSPreferences.getAuthenticationScheme(roomName));
					ds.setAuthenticationScheme(VWSPreferences.getAuthenticationScheme(roomName));
					printToConsole("Before setting domain--->"+VWSPreferences.getDomain(roomName));
					ds.setDomain(VWSPreferences.getDomain(roomName));
					printToConsole("Before setting user--->"+user);
					ds.setUser(user);
					ds.setPassword(pass);
					ds.setDatabaseName("master");
					printToConsole("Before setting serverSpn--->"+user);
					ds.setServerSpn(VWSPreferences.getServerSpn(roomName));		
					printToConsole("ds ---->"+ds);
				} catch (Exception e) {
					printToConsole("Exception while setting the datasource registry :"+e);
				}
			} else {
				printToConsole("inside aythentication mode sql...");
				if (VWSPreferences.isCheckSQL2005())
					jdbcUrl = "jdbc:sqlserver://" + host + ":" + port + ";selectMethod=cursor;";
				else
					jdbcUrl = "jdbc:microsoft:sqlserver://" + host + ":" + port + ";";
			}
		}
		printToConsole("Data source object :"+ds);
		printToConsole("jdbcUrl >>>>>"+jdbcUrl);
		try {
			if (ds != null) {
				con = ds.getConnection();
			} else {
				DriverManager.setLoginTimeout(5); //seconds
				con = DriverManager.getConnection(jdbcUrl, user, pass);
			}
		} catch (Exception e) {
			printToConsole("Exception while creating DataBase Connection Object :"+e);
		}
		printToConsole("connection object :"+con);
		return con;
	}
}
