/*
 * Client.java
 *
 * 
 */
package com.computhink.vws.server;
import com.computhink.common.Constants;
import com.computhink.common.ServerSchema;

public class Client implements java.io.Serializable, Constants
{   
	public static final int All_Client_Type = 0;
	public static final int Fat_Client_Type = 1;
	public static final int Web_Client_Type = 2;
	public static final int Adm_Client_Type = 3;
	public static final int AIP_Client_Type = 4;    
	public static final int Ewk_Client_Type = 5;
	public static final int WebL_Client_Type = 6;
	public static final int ZZZ_Client_Type = 7;    
	public static final int IXR_Client_Type = 8;
	public static final int NFat_Client_Type = 9;    
	public static final int NWeb_Client_Type = 10;
	public static final int Agt_Client_Type = 11;
	public static final int sdk_Client_Type = 12;
	/*
	 * Purpose	 	: New Client Type added for ARS  	 
	 * Created By	: Valli
	 * Date			: 20-Jul-2006
	 */
	public static final int Ars_Client_Type = 13;
	public static final int Drs_Client_Type = 14;
	public static final int NWebL_Client_Type = 15;
	public static final int EAS_Client_Type = 16;
	public static final int WebAccess_Client_Type = 17;
	public static final int Notification_Client_Type = 18;
	public static final int Custom_Label_WebAccess_Client_Type = 19;

	public static final int Professional_WebAccess_Client_Type = 20;
	public static final int Fax_Client_Type = 21;
	public static final int Concurrent_WebAccess_Client_Type=22;
	public static final int Professional_Conc_WebAccess_Client_Type=23;
	public static final int ContentSentinel_client_Type=24;
	public static final int CSWebaccess_client_Type=25;
	public static final int NamedOnline_WebAccess_Client_Type = 26;
	public static final int SubAdm_Client_Type = 27;
	
	private int         id = 0;
	private int         port = 0;
	private String      userName;
	private String      ipAddress;
	/*
Issue No / Purpose:  <650/Terminal Services Client>
Created by: <Pandiya Raj.M>
Date: <19 Jul 2006>
Include the new variable "stubIPAddress" to hold the ipaddress of stub object created.
	 */    
	private String      stubIPAddress;
	private boolean     isAdmin = false;
	private boolean     isManager = false;
	private boolean     isNamed = false;
	private boolean     isNamedOnline = false;
	private boolean isProfessionalNamed=false;
	private boolean  isEnterpriceConcurrent=false;
	private boolean  isConcurrentOnline=false;
	private boolean  isProfessionalConcurrent=false;
	//private boolean isPublicWebAccess=false;
	private boolean     caller = false;
	private int         clientType;
	private long        lastActivateDate;
	private long        lastRequestFromClient;
	private boolean isSubAdmin = false;
	private boolean isAIP = false;

	/*Issue No : 672
	 * Issue Description - Add two columns Session Id,Connected Date and Time to Conneted user table in adminwise
	 * Developer - Nebu Alex
	 * ***/	
	private String     connectedDate;
	// End of fix
	private int instances = 0;

	private int connectedStatus = 0;

	private String actualUserName = "";// This is Actual user will be used for user name having '.' or user name with context if u entered we retain only user name
	public Client(String userName, int id){
		this.userName = userName;
		this.id = id;
	}
	Client(String userName, String address, int type,String stubIPAddress)
	{
		this.userName = userName;
		this.ipAddress = address;
		clientType = type;
		lastActivateDate = System.currentTimeMillis();
		this.stubIPAddress = stubIPAddress;
	}
	public ServerSchema getSchema()
	{
		ServerSchema schema = new ServerSchema();
		schema.address = ipAddress;
		schema.comport = port;
		schema.type = SERVER_MDSS;
		schema.stubIPaddress= stubIPAddress;
		return schema;
	}
	/*Issue No : 672
	 * Issue Description - Add two columns Session Id,Connected Date and Time to Conneted user table in adminwise
	 * Developer - Nebu Alex
	 * ***/	
	public String getConnectedDate()
	{
		return this.connectedDate;
	}
	public void setConnectedDate(String connectedDate)
	{
		this.connectedDate =  connectedDate;
	}
	// End of fix
	public int getId()
	{
		return this.id;
	}
	public void setId(int id)
	{
		this.id =  id;
	}
	public void setCaller(boolean b)
	{
		this.caller = b;
	}
	public boolean isAdmin()
	{
		return this.isAdmin;
	}
	public boolean isManager()
	{
		return this.isManager;
	}
	public boolean isNamed()
	{
		return this.isNamed;
	}
	public boolean isProfessionalNamed()
	{
		return this.isProfessionalNamed;
	}
	public boolean isEnterpriceConcurrent()
	{
		return this.isEnterpriceConcurrent;
	}
	
	
	public boolean isConcurrentOnline() {
		return isConcurrentOnline;
	}
	public void setConcurrentOnline(boolean isConcurrentOnline) {
		this.isConcurrentOnline = isConcurrentOnline;
	}
	
	public boolean isProfessionalConcurrent()
	{
		return this.isProfessionalConcurrent;
	}
	public boolean isNamedOnline() {
		return isNamedOnline;
	}
	public void setNamedOnline(boolean isNamedOnline) {
		this.isNamedOnline = isNamedOnline;
	}
	/*public boolean isPublicWebAccess() {
		return isPublicWebAccess;
	}
	public void setPublicWebAccess(boolean isPublicWebAccess) {
		this.isPublicWebAccess = isPublicWebAccess;
	}*/
	public void setAdmin(boolean b)
	{
		this.isAdmin = b;    
	}
	public void setManager(boolean b)
	{
		this.isManager = b;    
	}
	public void setNamed(boolean b)
	{
		this.isNamed = b;    
	}
	public void setProfessionalNamed(boolean b)
	{
		this.isProfessionalNamed = b;    
	}
	
	public void setEnterpriceConcurrent(boolean b)
	{
		this.isEnterpriceConcurrent = b;    
	}
	
	public void setProfessionalConcurrent(boolean b)
	{
		this.isProfessionalConcurrent = b;    
	}
	public void setClientType(int type)
	{
		clientType=type;    
	}
	public String getUserName()
	{
		return userName;
	}
	public int getClientType()
	{
		return clientType;
	}
	public String getIpAddress()
	{
		return ipAddress;
	}
	public void setIpAddress(String clientIPAddress)
	{
		ipAddress=clientIPAddress;
	}

	public int hashCode()
	{
		return new String(userName + ipAddress).hashCode();
	}

	public boolean equals(Object object)
	{
		if (!(object instanceof Client))
		{
			return false;
		}

		Client otherClient = (Client) object;

		if (userName != null && userName.equals(otherClient.userName) &&
				ipAddress != null && ipAddress.equals(otherClient.ipAddress))
		{
			return true;
		}

		return false;
	}
	public long getLastActivateDate()
	{
		return lastActivateDate;
	}
	public void setLastActivateDate()
	{
		lastActivateDate = System.currentTimeMillis();
	}
	public boolean isCaller() 
	{
		return caller;
	}
	public int getPort() 
	{
		return port;
	}
	public void setPort(int port) 
	{
		this.port = port;
	}
	public int getInstance()
	{
		return instances;
	}
	public void incrementInstance()
	{
		instances++;
	}
	public void decrementInstance()
	{
		instances--;
	}
	/**
	 * @return Returns the lastRequestFromClient.
	 */
	 public long getLastRequestFromClient() {
		return lastRequestFromClient;
	}
	/**
	 * @param lastRequestFromClient The lastRequestFromClient to set.
	 */
	 public void setLastRequestFromClient() {
		this.lastRequestFromClient = System.currentTimeMillis();		 
	}
	/**
	 * @return Returns the connectedStatus.
	 */
	 public int getConnectedStatus() {
		 return connectedStatus;
	 }
	 /**
	  * @param connectedStatus The connectedStatus to set.
	  */
	 public void setConnectedStatus(int connectedStatus) {
		 this.connectedStatus = connectedStatus;
	 }
	 public String getActualUserName() {
		 return actualUserName;
	 }
	 public void setActualUserName(String actualUserName) {
		 this.actualUserName = actualUserName;
	 }
	public boolean isSubAdmin() {
		return isSubAdmin;
	}
	public void setSubAdmin(boolean isSubAdmin) {
		this.isSubAdmin = isSubAdmin;
	}
	public boolean isAIP() {
		return isAIP;
	}
	public void setAIP(boolean isAIP) {
		this.isAIP = isAIP;
	}
}