/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vws.csserver;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TimerTask;
import java.util.Vector;

import com.computhink.common.LogMessage;
import com.computhink.common.SCMEvent;
import com.computhink.common.SCMEventListener;
import com.computhink.common.SCMEventManager;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.VWClient;
import com.computhink.vws.server.Client;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSPreferences;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CSServer extends UnicastRemoteObject implements CSS, SCMEventListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Vector clients = new Vector();
	private Hashtable  workers = new Hashtable();
	private long stime;
	private ServerSchema mySchema;
	//VNSServer shutdown is not stopping the final action process.
	public static boolean shutDownRequest = false;
	private static ServerSchema  managerSchema = null;
	//Timer processTimer;
	private final String SIGN = "0B9B5177-1011-4BBF-B617-455288043D4A";

	VWClient vwClient;
	VWS vws;

	public CSServer(ServerSchema s) throws RemoteException
	{
		super(s.dataport);
		this.mySchema = s;
		SCMEventManager scm = SCMEventManager.getInstance();
		scm.addSCMEventListener(this);
		stime = System.currentTimeMillis();
	}
	public void handleSCMEvent(SCMEvent event)
	{
		shutDown();
	}
	public void exit() throws RemoteException
	{
		shutDown();
	}
	public String getServerOS() throws RemoteException
	{
		return Util.getEnvOSName();
	}
	public void init() throws RemoteException
	{
		ServerSchema vwss = CSSPreferences.getVWS();
		vws = (VWS) Util.getServer(vwss);
		if (vws == null)
		{
			CSSLog.add(PRODUCT_NAME + " Server @" + vwss.address + ":" +vwss.comport + " inactive");
			CSSLog.war("Listening to " + PRODUCT_NAME + " Server...");
			vws = waitForViewWiseServer(vwss);
		}
		if (shutDownRequest) return;
		CSSLog.add("Connected to " + PRODUCT_NAME + " Server @" + vwss.address + ":" +vwss.comport);
		try
		{
			String serverName = null;
			String cacheDir = "c:\\temp";
			vwClient = new VWClient(SIGN, cacheDir,Client.ContentSentinel_client_Type);
			{
				/*if(vwClient != null) {
					CSSLog.add("vwClient for NOT NULL :::: "+vwClient);
				} else {
					CSSLog.add("vwClient for NULL :::: "+vwClient);
				}*/
				serverName = CSSConstants.DUMMY_SERVERNAME+"CSS";
				Vector rooms =new Vector();
				rooms = vws.getRoomNames();
				CSSLog.add("rooms :::::"+rooms);
				// loop for multiple rooms
				if(rooms!=null && rooms.size()>0)
				{
					for (int i = 0; i < rooms.size(); i++)
					{
						String roomName = (String) rooms.get(i);
						int sessionID = -1;
						//CSSLog.add("roomName :::::"+roomName);
						CSSLog.add("Client.ContentSentinel_client_Type :::::"+Client.ContentSentinel_client_Type);
						sessionID = vws.login(roomName, Client.ContentSentinel_client_Type, vwss.getAddress(), vwClient.encryptStr(CSSConstants.cssAdminUser), "", vwss.getComport());
						CSSLog.add("session id generated for cssserver.... "+sessionID);
						if(sessionID>0){
							/*CSSLog.add("Inside session Id greater than zero..... "+sessionID);
							CSSLog.add("serverName:::::::: "+serverName);
							CSSLog.add("roomName:::::::: "+roomName);
							CSSLog.add(" CSSConstants.cssAdminUser ::::"+ CSSConstants.cssAdminUser);*/
							vwClient.setSession(sessionID, serverName, roomName, CSSConstants.cssAdminUser);
							if(vwClient.getSession(sessionID)!=null) {
								//CSSLog.add("session is not null.........");
							}else {
								//CSSLog.add("session is  null.........");
							}
						}else{
							if(sessionID == ViewWiseErrors.NoMoreLicenseSeats){
								CSSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.NoMoreLicenseSeats));
							}else if(sessionID==ViewWiseErrors.ServerNotFound)
								CSSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.ServerNotFound));
							else if(sessionID==ViewWiseErrors.checkUserError)
								CSSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.checkUserError));
							else if(sessionID==ViewWiseErrors.NotAllowedConnections) 
								CSSLog.war("This Room is Administratively Down."+
										NewLineChar+"Please contact your " + PRODUCT_NAME + " Administrator"+
										NewLineChar+"before attempting to reconnect.");
							else if(sessionID==ViewWiseErrors.accessDenied)
								CSSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.accessDenied));
							else if(sessionID==ViewWiseErrors.LoginCountExceeded)
								CSSLog.war(vwClient.getErrorDescription(ViewWiseErrors.LoginCountExceeded));
							else if(sessionID==ViewWiseErrors.JarFileMisMatchErr)
								CSSLog.war(""+vwClient.getErrorDescription(ViewWiseErrors.JarFileMisMatchErr));
							else
								CSSLog.war(""+ PRODUCT_NAME +" room may be down.");
							continue;
						}
						int pollingTime = Integer.parseInt(CSSPreferences.getCSPollingTime()) * 60 * 1000;
						int schedulerStartInfo = 0; //This is nothing but service or application
						if(schedulerStartInfo == 0){
							Worker worker = new Worker(vwClient, roomName, pollingTime, 0, sessionID);
							if (worker == null)
							{
								CSSLog.war("Cannot start Content Sentinel Worker for room " + roomName);
								continue;
							}
							worker.start();
							workers.put(roomName, worker);
						}
					} // end for rooms loop
				} //if condition for room null
			} // end for servers loop
		}
		catch(Exception e)
		{
			CSSLog.err(PRODUCT_NAME + " Server connection error: " + e.getMessage());
		}
	}

	public String managerBegin(ServerSchema manager) throws RemoteException
	{
		if (managerSchema == null)
		{
			managerSchema = manager;
			return "";
		}
		else
		{
			if (managerSchema.address.equals(manager.address))
				return "";
			else
				return "Cannot monitor server. Manager @"
				+ managerSchema.address + " already connected.";
		}
	}
	public void managerEnd() throws RemoteException
	{
		managerSchema = null;
	}
	private void shutDown()
	{
		vwClient.shutdown();
		CSSLog.add("VW client stopped");
		//vwClient.shutdown() kills the MDSS schema which is not in use.
		vwClient = null;
		//VNSServer shutdown is not stopping the final action process.
		shutDownRequest = true;
		Enumeration wenum = workers.elements();
		while (wenum.hasMoreElements())
		{
			Worker worker = (Worker) wenum.nextElement();
			try{
				vws.logout(worker.roomName, worker.sessionID);
			}catch(Exception ex){}
			worker.shutDown();
			CSSLog.add("Stopped Notfication Worker for Room " + worker.roomName);
		}
		CSSLog.add("Stopping CSS Server...");
		CSSLog.writeLog();
		Util.sleep(500);
		Util.killServer(mySchema);
		CSSLog.err("Stopped");
	}

	private VWS waitForViewWiseServer(ServerSchema vwss)
	{
		VWS vws = null;
		while (vws == null && !shutDownRequest)
		{
			Util.sleep(2000);
			vws = (VWS) Util.getServer(vwss);
		}
		return vws;
	}

	/*private Vector waitForGetViewWiseRooms(VWS vws)
	{
		Vector ret=null;
		while ((ret == null || ret.size()==0) && !shutDownRequest)
		{
			try
			{
				Util.sleep(2000);
				ret = vws.getRoomNames();
			}
			catch(Exception e)
			{
				CSSLog.err("Error in getting " + PRODUCT_NAME + " rooms "+e.getMessage());
			}
		}
		return ret;
	}
	private Vector waitForGetViewWiseRooms(VWClient vwClient, String serverName)
	{
		Vector ret=null;
		while ((ret == null || ret.size()==0) && !shutDownRequest)
		{
			try
			{
				Util.sleep(2000);
				vwClient.getRooms(serverName,ret);
			}
			catch(Exception e)
			{
				CSSLog.err("Error in getting " + PRODUCT_NAME + " rooms "+e.getMessage());
			}
		}
		return ret;
	}*/

	public ServerSchema getSchema() throws RemoteException
	{
		return this.mySchema;
	}
	public void setComPort(int port) throws RemoteException
	{
		CSSPreferences.setComPort(port);
	}
	public int getComPort() throws RemoteException
	{
		return CSSPreferences.getComPort();
	}
	public void setDataPort(int port) throws RemoteException
	{
		CSSPreferences.setDataPort(port);
	}
	public int getDataPort() throws RemoteException
	{
		return CSSPreferences.getDataPort();
	}
	public ServerSchema getVWS() throws RemoteException
	{
		return CSSPreferences.getVWS();
	}
	public void setVWS(ServerSchema vws) throws RemoteException
	{
		CSSPreferences.setVWS(vws);
	}
	

	public boolean getDebugInfo() throws RemoteException
	{
		return CSSPreferences.getDebugInfo();
	}
	public void setDebugInfo(boolean b) throws RemoteException
	{
		CSSPreferences.setDebugInfo(b);
	}
	public boolean setManager(String man, String pas) throws RemoteException
	{
		return CSSPreferences.setManager(man, pas);
	}
	public String getManager() throws RemoteException
	{
		return CSSPreferences.getManager();
	}


	public long getTimeOn() throws RemoteException
	{
		return (System.currentTimeMillis() - stime) / 1000;
	}
	public LogMessage getLogMsg() throws RemoteException
	{
		return CSSLog.getLog();
	}
	public Vector getClients() throws RemoteException
	{
		return this.clients;
	}
	public int getLogCount() throws RemoteException
	{
		return CSSLog.getCount();
	}
	

	class SheduledProcess extends TimerTask {
		int ret = -1;
		private String roomName;
		private int sessionID;
		VWClient vwClient;
		public SheduledProcess(VWClient vwClient, int sessionID, String roomName){
			this.vwClient = vwClient;
			this.roomName = roomName;
			this.sessionID = sessionID;
		}
		private void doSheduledProcess(){
			//CSSLog.add("coming in doSheduledProcess : '"+roomName+"'");
			//VNSProcessNotification vnsProcessNotification = new VNSProcessNotification();
			//ret = vnsProcessDocs.getReportDocs(vwClient, sessionID, roomName);
			//ret = vnsProcessNotification.sendEmailNotify(vwClient, sessionID, roomName);
			for(int i=0; i<1000; i++){
				System.out.println(""+i);
			}
			//processTimer.cancel();
			//processTimer = null;
		}
		public void run(){
			doSheduledProcess();
		}
	}

	public boolean ping() throws RemoteException
    {
        return true;
    }
	//Need to Verify...
	public boolean getProxy() throws RemoteException {
		// TODO Auto-generated method stub
		return false;
	}
	public String getProxyHost() throws RemoteException {
		// TODO Auto-generated method stub
		return null;
	}
	public int getProxyPort() throws RemoteException {
		// TODO Auto-generated method stub
		return 0;
	}
	public void setProxy(boolean b) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	public void setProxyHost(String server) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	public void setProxyPort(int port) throws RemoteException {
		// TODO Auto-generated method stub
		
	}
	//
	
	
	
	
	//--------------
	//Need to take from VWS email settings.
	public String getEmailServerName() throws RemoteException{
		return CSSPreferences.getEmailServerName();
	}
	public void setEmailServerName(String emailServerName) throws RemoteException {
		CSSPreferences.setEmailServerName(emailServerName);
	}
	public String getEmailServerPort() throws RemoteException{
		return VWSPreferences.getEmailServerPort();
	}
	public void setEmailServerPort(String emailServerPort) throws RemoteException {
		CSSPreferences.setEmailServerPort(emailServerPort);
	}
	public String getEmailServerUsername() throws RemoteException{
		return CSSPreferences.getEmailServerName();
	}
	public void setEmailServerUsername(String emailServerUsername) throws RemoteException {
		CSSPreferences.setEmailServerUsername(emailServerUsername);
	}
	public String getEmailServerPassword() throws RemoteException{
		return CSSPreferences.getEmailServerPassword();
	}
	public void setEmailServerPassword(String emailServerPassword) throws RemoteException {
		CSSPreferences.setEmailServerPassword(emailServerPassword);
	}
	public String getFromEmailId() throws RemoteException {
		return VWSPreferences.getFromEmailId();
	}
	public void setFromEmailId(String emailId) throws RemoteException {
		CSSPreferences.setFromEmailId(emailId);
	}
	
	//--------------	
	public String getCSPollingTime() throws RemoteException{
		return CSSPreferences.getCSPollingTime();
	}
	public void setCSPollingTime(String pollingTime) throws RemoteException{
		CSSPreferences.setCSPollingTime(pollingTime);;
	}
	
	public String getCSFileSizeLimit() throws RemoteException {
		return CSSPreferences.getCSFileSizeLimit();
	}
	public void setCSFileSizeLimit(String notificationFileSizeLimit) throws RemoteException {
		CSSPreferences.setCSFileSizeLimit(notificationFileSizeLimit);
	}
	
	public String getCSRetries() throws RemoteException {
		return CSSPreferences.getCSRetries();
	}
	public void setCSRetries(String notificationRetries) throws RemoteException {
		CSSPreferences.setCSRetries(notificationRetries);
	}
	
	public String getCSEmailId() throws RemoteException{
		return CSSPreferences.getCSEmailId();
	}
	public void setCSEmailId(String notificationEmailId) throws RemoteException {
		CSSPreferences.setCSEmailId(notificationEmailId);
	}
	//--------------
	public boolean getLogInfo() throws RemoteException
	{
		return CSSPreferences.getLogInfo();
	}
	public void setLogInfo(boolean b) throws RemoteException
	{
		CSSPreferences.setLogInfo(b);
	}

}
