/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vws.csserver;

import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.manager.ManagerPreferences;
import com.computhink.vws.server.VWSConstants;
import com.computhink.vws.server.mail.VWMail;
import com.computhink.resource.ResourceManager;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ViewWiseCSS implements CSSConstants{

	public static void main(String[] args) {
		String message = "";

		//VNSLog.err("prop CLASSPATH "+System.getProperty("CLASSPATH"));
		//VNSLog.err("java.class.path : " + System.getProperty("java.class.path"));
		
	      /*if (VNSPreferences.getProxy())
            Util.RegisterProxy(VNSPreferences.getProxyHost(),
                               VNSPreferences.getProxyPort(),
                               SERVER_VNS);*/
        String hostName = CSSPreferences.getHostName();
        System.out.println("hostname in ViewWiseCSS");
        if (hostName.length() > 0)
        {
            System.getProperties().put("java.rmi.server.useLocalHostname", "true");
            System.getProperties().put("java.rmi.server.hostname", hostName);
        }
        ServerSchema s = Util.getMySchema();
        s.comport = CSSPreferences.getComPort();
        System.out.println(" s.comport::::"+ s.comport);
        CSSLog.add(" s.comport::::"+ s.comport);
        s.dataport = CSSPreferences.getDataPort();
        CSSLog.add(" s.comport::::"+ s.dataport);
        System.out.println("  s.dataport ::::"+ s.dataport );
        s.type = SERVER_CSS;
        //VNSLog.add("VNS Info " + VNSPreferences.getComPort()+":"+VNSPreferences.getDataPort()+":"+SERVER_VNS);
        /* 
		 *	Master Switch off added for VNS in registry
         */
        System.out.println("ManagerPreferences.getVNSEndable() : "+ManagerPreferences.getVNSEnable());
        if(ManagerPreferences.getCSSEnable().equalsIgnoreCase("true")){
        	System.out.println("Inside css server......");
        	
	        int port = Util.createServer(s);
	        System.out.println("port is : "+port);
	        if ( port != 0)
	        {
	            CSSLog.add("Started " + PRODUCT_NAME + " Content Sentinel Server @" + hostName +"[" + s.address + "]:" + port);
	            try
	            {
	                ((CSS) Util.getServer(s)).init();
				}
	            catch(Exception e){}
	        }
	        else{
	        	message = ResourceManager.getDefaultManager().getString("ViewWiseVNs.NotStartNotifyServOnPort1")+" "+ PRODUCT_NAME +" "+
	        			ResourceManager.getDefaultManager().getString("ViewWiseVNs.NotStartNotifyServOnPort2")+ port;
	        	VWMail.sendMailNotificationOnVSM(message, VWSConstants.VNS);

	        	CSSLog.err("Could not start " + PRODUCT_NAME + " Content Sentinel Server on port: " + port);
	        }
	        
	    }// Master switch off condition
	}
}
