package com.computhink.vws.csserver;

import java.rmi.RemoteException;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import com.computhink.vns.server.VNSLog;
import com.computhink.vns.server.VNSPreferences;
import com.computhink.common.Constants;
import com.computhink.common.NotificationHistory;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.common.ViewWiseErrors;
import com.computhink.vwc.VWCUtil;
import com.computhink.vwc.VWClient;
import com.computhink.vwc.VWDocATDetails.VWDocumentHistoryInfo;
import com.computhink.vws.server.VWS;
import com.computhink.vws.server.VWSConstants;
import com.computhink.resource.ResourceManager;
public class CSSProcessEmail implements ViewWiseErrors{
	String message = "";
	private ResourceManager resourceManager=null;
	public Hashtable<Integer, String> notificationMasterTable = new Hashtable<Integer, String>();
	public Vector master = new Vector();
	public int getProcessDetails(VWClient vwClient, int sessionID, String roomName){
		ServerSchema vwss = VNSPreferences.getVWS();
		VWS vws = (VWS) Util.getServer(vwss);
		resourceManager=ResourceManager.getDefaultManager();
		setInfo(vwClient, sessionID, roomName);
		try
		{
			int returnId = 0; 
			String failedFlag = "";
			String notificationRetries = "";
			Vector secureEmailVector = new Vector();
			Vector secureEmailUrlVector = new Vector();
			Vector failedNotificationHistoryVector = new Vector();
			failedFlag = "0";//To get the new mails
			//CSSLog.dbg("befor calling secure link details .....");
			if(vwClient!=null) {
			//CSSLog.add("sessionId  is not null:::::::::::::"+vwClient.getSession(sessionID));
			String clientip=	vwClient.getClientIP();
			CSSLog.dbg("clientip ::::::::"+clientip);
			}/*else {
				
			}*/
			returnId = vwClient.readSecureLinkDetails(roomName, sessionID,  secureEmailVector);
			int retVal=vwClient.readSecureLinkUrl(roomName, sessionID,  secureEmailUrlVector);
		
			//CSSLog.add("returnId :::::::::"+returnId);
			//CSSLog.add("notificationHistoryVector ....." +secureEmailVector);
		//	failedFlag = "1";//To get the failed mails
			//returnId = vwClient.readSecureLinkDetails(roomName, sessionID, failedNotificationHistoryVector);
			/*try{
				master = vws.getNotificationMasters(roomName, sessionID, 1, "-");
				
				if(master != null){
					//VNSLog.add("getNotifiationMaster table is : "+master.size());
					if(master.size()>0)
						notificationMasterTable = (Hashtable<Integer, String>)master.get(0);
				}
			}catch(Exception ex){
				VNSLog.add("exception while getnotification master");
			}
			*/
			
			/*if(failedNotificationHistoryVector!=null && failedNotificationHistoryVector.size()>0){
				for(int i=0; i<failedNotificationHistoryVector.size(); i++){
					secureEmailVector.add(failedNotificationHistoryVector.get(i));
				}
			}*/
			
			if(returnId<0){
				if(returnId == ViewWiseErrors.invalidSessionId){
					message = Constants.PRODUCT_NAME +" "+ resourceManager.getString("VNSProcessEmail.SeverDown");
					CSSLog.war(message);
					vws.sendNotification(roomName, sessionID, VWSConstants.ContentSentinelServer, message);
				}
				return Error;
			}
			if(secureEmailVector == null || (secureEmailVector!=null && secureEmailVector.size()<=0)){
				VNSLog.add(""+roomName+": No SecureLinks to process mail"); 	
				return NoError;
			} 	
			CSSLog.dbg("Before sending secure mail link is ......."+secureEmailVector);
			String secureurl="";
			if((secureEmailUrlVector!=null)&&(secureEmailUrlVector.size()>0)) {
				secureurl=secureEmailUrlVector.get(0).toString();
			}
			int ret = sendSecureLinkMail(vwClient, secureEmailVector,secureurl, roomName, sessionID);
			CSSLog.dbg("After sending secure mail link is .......");
			
			VNSLog.add(""+roomName+":No notifications to process mail"); 	
			
		}catch(Exception ex){
			//VNSLog.err("Error in getProcessDetails : "+ex.getMessage());
			return -1;
		}
		return 0;
	}
	
	public int sendSecureLinkMail(VWClient vwClient, Vector secureLinkVector,String url, String roomName, int sessionID) {
		ServerSchema vwss = VNSPreferences.getVWS();
		VWS vws = (VWS) Util.getServer(vwss);
		String expiryDate="";
		CSSLog.add("inside sendemail of css....");
		if(secureLinkVector!=null&&secureLinkVector.size()>0) {
			for(int i=0;i<secureLinkVector.size();i++) {
				String mailIdstr="";
				String password="";
				String suburl="";
				String secureId="";
				String docName="";
				String docId="";
				String subject="";
				int urlSentStatus=0;
				int passSentStatus=0;
				StringTokenizer st = new StringTokenizer(secureLinkVector.get(i).toString(), Util.SepChar);
				secureId=st.nextToken();
				docId=st.nextToken();
				docName=st.nextToken();
				mailIdstr=   st.nextToken();
				CSSLog.dbg("mailIdstr :::::::::"+mailIdstr);
				password=st.nextToken();
				st.nextToken();
				st.nextToken();
				suburl=st.nextToken();
				//CSSLog.add("docId :::::::::"+docId);
				try {
					Vector secureMPerm=new Vector();
				//	CSSLog.add("sessionID :::"+sessionID+"docId ::"+docId+"mailIdstr :::");
					vwClient.getSecureLinkPermissionModify(sessionID, docId, mailIdstr, password, secureMPerm);
					String[] splitedMperm=secureMPerm.get(0).toString().split(Util.SepChar);
				    expiryDate=	splitedMperm[5];
				//   CSSLog.add("secureMPerm :::::"+secureMPerm);
				//	CSSLog.dbg("before sendemail of css....");
					/*if(url.substring(url.length()-1).contains("/")) {
						message=url+suburl;
					}else*/
					String totalURL =url+suburl;
					if(totalURL.indexOf(" ") >= 0){
						totalURL = totalURL.replaceAll(" ", "%20");
					}
					message = "<html><body><p><a href="+ totalURL +">" + totalURL + "</a></p></body></html>";
					CSSLog.dbg("message for sendSecureLinkMail :::::"+message);
					//subject="Document  "+docName+"has been shared with you.";
					urlSentStatus=vws.sendCSSecureLinkEmail(roomName, sessionID,docName, VWSConstants.ContentSentinelServer, message,mailIdstr,subject,VWSConstants.CSS_MAIL_Url,expiryDate);
					CSSLog.dbg("urlSentStatus sendSecureLinkMail ::::::"+urlSentStatus);
					if(urlSentStatus==1) {
						//subject="Document "+docName+"has been shared with you.";
						passSentStatus=vws.sendCSSecureLinkEmail(roomName, sessionID,docName, VWSConstants.ContentSentinelServer, password,mailIdstr,subject,VWSConstants.CSS_MAIL_PASSWORD,"");
					}
					CSSLog.dbg("Url sent status ::::::"+urlSentStatus);
					if(urlSentStatus==1&&passSentStatus==1) {
						CSSLog.dbg("Before updatelink...");
						vws.updateSecureLink(roomName,sessionID,secureId,"1");
					}
					CSSLog.dbg("After mail sent from  of css server....");
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}
		
		return sessionID;
		
	}

	/*public int sendSecureLinkMail(VWClient vwClient, Vector secureLinkVector,String url, String roomName, int sessionID) {
		ServerSchema vwss = VNSPreferences.getVWS();
		VWS vws = (VWS) Util.getServer(vwss);
		boolean notify = false;
		
		int nsId = 0;//Notification settings id
		int nodeId = 0;//Document or FolderId
		int attachmentExist = 0;
		int firstId = 0;
		int lastId = 0;
		int failedCount = 0;
		int regFailedCount = 0;
		int failedFlag = 0;
		int sentFlag = 0;
		Vector<String> auditDetails = new Vector<String>();
		Vector<String> history = new Vector<String>();
		String creator = "";
		
		try{
			Vector<NotificationHistory> notifications = new Vector<NotificationHistory>();
			if(secureLinkVector!=null && secureLinkVector.size()>0){
				NotificationHistory notificationHistory;
				int lastRecordCount = secureLinkVector.size()-1;
				for(int i=0; i<secureLinkVector.size(); i++){
					CSSLog.add("securelink....."+secureLinkVector.get(i).toString());
					if(VNSPreferences.getNotificationDebug()){
						CSSLog.add("securelink....."+secureLinkVector.get(i).toString());
					}
					notificationHistory = (NotificationHistory)secureLinkVector.get(i);
					nsId = notificationHistory.getId();
					nodeId = notificationHistory.getNodeId();
					//Notification log updated wrong creator - fixed
					//creator = notificationHistory.getCreator();
					attachmentExist = notificationHistory.getAttachment();
					firstId = nsId;

					if(lastId==0 || (firstId == lastId)){
						notifications.add(notificationHistory);
						
						lastId = firstId;
						if(secureLinkVector.size()==1)
							notify = true;
						else
							notify = false;
						
						if(i==lastRecordCount){
							notify = true;
						}
					}else if(firstId!=lastId){
						i--;
						notify = true;
						lastId = 0;
					}
					
					if(notify){
						try{
							NotificationHistory notifyHistory = null;
							String mailIdstr = "";
							String historyIds = "";
							Vector<String> mailIds = new Vector<String>();
							int historyCount = 0;
							int attempt = 0;

							//Notification Server Settings - From registry entry
							if(VNSPreferences.getNotificationRetries().equals("")){
								VNSPreferences.setNotificationRetries("0");
							}
							failedCount = Integer.parseInt(VNSPreferences.getNotificationRetries());
							regFailedCount = failedCount;
							for(int index=0; index<notifications.size(); index++){
								notifyHistory = (NotificationHistory)notifications.get(index);
								mailIdstr = mailIdstr+notifyHistory.getMailIds()+";";
								//Notification log updated wrong creator - fixed
								creator = notifyHistory.getCreator();
							}

							if(mailIdstr.length()>0){

								if(mailIdstr.contains(";")){

									mailIdstr = getNonDuplicateEmailString(mailIdstr);
									String mail[] = mailIdstr.split(";");
									for(int m=0; m<mail.length; m++){
										String tempStr = mail[m].trim();
										if((tempStr.length()>0) && !mailIds.contains(tempStr))
											mailIds.add(tempStr);
									}

								}
								else{
									mailIds.add(mailIdstr);
								}
								
								if(attachmentExist==01 || attachmentExist == 11)
									history = getDocumentAuditHistory(vwClient, sessionID, nodeId, auditDetails);
								
								VNSLog.add("Processing Notifications for user : "+creator+ " and to mailId(s) : "+mailIdstr);
								sentFlag = saveMailConentInHtmlFile(notifications,mailIds,attempt,history);
								
								if(sentFlag < 0){
									//Resend the mail.
									while(sentFlag<0 && failedCount>0){
										attempt++;
										VNSLog.war("Retries to process mail ["+attempt+"]");
										sentFlag = saveMailConentInHtmlFile(notifications,mailIds,attempt,history);
										failedCount--;
									}
								}
								if(sentFlag > 0){
									failedCount = attempt;
									historyCount = notifications.size();
									for(int index=0; index<notifications.size(); index++){
										notifyHistory = (NotificationHistory)notifications.get(index);
										VNSLog.add("Message : - "+notifyHistory.getDesc());
										historyIds = historyIds + notifyHistory.getHistoryId() + Util.SepChar;
									}
									if(attempt>0)
										VNSLog.add("SMTP Sent Successfully after ["+attempt+"] attempt(s)");
									else
										VNSLog.add("SMTP Sent Successfully");
									
									VNSLog.add("SMTP send for user : "+mailIdstr);
								}
								if(attempt==regFailedCount && sentFlag<0){
									sentFlag = 0; //If not send, need to update the database as 0
									failedFlag = 1;
									message = resourceManager.getString("VNSProcessEmail.SMTPFailed1")+" "+"["+attempt+"]"+" "+resourceManager.getString("VNSProcessEmail.SMTPFailed2")+" "+mailIdstr;
									VWSLog.add(message);
									vws.sendNotification(roomName, sessionID, VWSConstants.VNS, message);
									
									VNSLog.add("SMTP failed to send ["+attempt+"] times");
									historyCount = notifications.size();
									for(int index=0; index<notifications.size(); index++){
										notifyHistory = (NotificationHistory)notifications.get(index);
										historyIds = historyIds + notifyHistory.getHistoryId() + Util.SepChar;
									}
								}
								
								//updateHistory with the send mail date and any failed count
								vws.updateNotificationHistory(roomName, sessionID, sentFlag, failedFlag, historyCount, historyIds);
								notifications = new Vector<NotificationHistory>();
								notify = false;
							}else{
								message = resourceManager.getString("VNSProcessEmail.NoEmails");
								VNSLog.add(message);
								vws.sendNotification(roomName, sessionID, VWSConstants.VNS, message);
							}
						}catch(Exception ex){
							//VNSLog.add("Exception while start sending the notification : "+ex.getMessage());
						}
					}//End of IF loop
				}
			}

		}catch(Exception ex){
			//VNSLog.add("exception in saveMailConentInHtmlFile is : "+ex.getMessage());
		}
		return NoError;
	}*/
	
	private String getNonDuplicateEmailString(String mailIdstr){
		Vector<String> mailIds = new Vector<String>();
		
		try{
			if(!mailIdstr.equals("") && mailIdstr.length()>0){
				if(mailIdstr.contains(";")){
					String mail[] = mailIdstr.split(";");
					for(int m=0; m<mail.length; m++){
						String tempStr = mail[m].trim();
						if((tempStr.length()>0) && !mailIds.contains(tempStr))
							mailIds.add(tempStr);
					}
				}else			
					mailIds.add(mailIdstr);

				if(mailIds!=null && mailIds.size()>1){
					mailIdstr = "";
					for(int j=0; j<mailIds.size(); j++){
						mailIdstr = mailIdstr + mailIds.get(j).toString() + ";";
					}
					mailIdstr = mailIdstr.substring(0,mailIdstr.length()-1);
				}else if(mailIds!=null && mailIds.size()==1){
					mailIdstr = mailIds.get(0).toString();
				}
			}
		}catch(Exception ex){
			//VNSLog.add("getNonDuplicateEmailString : "+ex.getMessage());
		}
		
		return mailIdstr;
	}
	private int saveMailConentInHtmlFile(Vector notifications, Vector mailIds, int attempt, Vector history) {
		ServerSchema vwss = VNSPreferences.getVWS();
		VWS vws = (VWS) Util.getServer(vwss);

		String mailMessage = "";
		String mailIdstr = "";

		Vector<String> htmlContents=new Vector<String>();
		Vector<String> nodeName = new Vector<String>();
		//Vector<String> history = new Vector<String>();
		Vector<String> auditDetails = new Vector<String>();
		int status = 0;
		int attachmentExist = 0;
		String[] caption = new String [1];
		String[] colWidth = new String [1];
		String historyFilePath = ""; 
			
		int docId = -1;
		int nodeType = 0;
		
		try{
			if(notifications!=null && notifications.size()>0){
				NotificationHistory notificationHistory;
				notificationHistory = (NotificationHistory)notifications.get(0);
				nodeType = notificationHistory.getNodeType();
				docId = notificationHistory.getNodeId();
				attachmentExist = notificationHistory.getAttachment();
				nodeName.add(notificationHistory.getNodeName());
				
				historyFilePath = VWCUtil.getHome()+"\\Server_Generated_Files\\"+"History"+"_"+nodeName.get(0)+".html";
				
				boolean generatedReport; 
					
				switch(attachmentExist){
					case 01:
						//history
						if(attempt==0){
							generatedReport = VWCUtil.writeListToFile(history,historyFilePath,"");
						}
						break;
						
					case 10:
						//vwr file
						break;
					case 11:
						if(attempt==0){
							generatedReport = VWCUtil.writeListToFile(history,historyFilePath,"");
						}
						break;
						
					default:
						break;
				}
				caption = new String[5];
				colWidth = new String [5];
				
				if(nodeType==CSSConstants.nodeType_Document || nodeType == CSSConstants.nodeType_DT ||
						nodeType == CSSConstants.nodeType_Folder || nodeType == CSSConstants.nodeType_Retention || 
						nodeType == CSSConstants.nodeType_Route){//Irrespective of the nodeType this is the format
					caption[0] = "Node Name";
					colWidth[0] = "15%";
				}
				caption[1] = "Event Name";
				caption[2] = "Message Description";
				caption[3] = "Modified By";
				caption[4] = "Modified Date";
				
				colWidth[1] = "25%";
				colWidth[2] = "40%";
				colWidth[3] = "12%";
				colWidth[4] = "13%";
				
				htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
				htmlContents.add("<html>");
				htmlContents.add("<head>");
				htmlContents.add("</head>");
				htmlContents.add("<body>");
				htmlContents.add("<table border=1 cellspacing=1 width='100%' id='AutoNumber1' cellpadding=2>");
				htmlContents.add("<tr>");
				if(nodeType==CSSConstants.nodeType_Document || nodeType == CSSConstants.nodeType_DT ||
						nodeType == CSSConstants.nodeType_Folder || nodeType == CSSConstants.nodeType_Retention || 
						nodeType == CSSConstants.nodeType_Route){
					htmlContents.add("<td width='"+colWidth[0]+"'><B>"+caption[0]+"</B>");htmlContents.add("</td>");
				}
				htmlContents.add("<td width='"+colWidth[1]+"'><B>"+caption[1]+"</B>");htmlContents.add("</td>");
				htmlContents.add("<td width='"+colWidth[2]+"'><B>"+caption[2]+"</B>");htmlContents.add("</td>");
				htmlContents.add("<td width='"+colWidth[3]+"'><B>"+caption[3]+"</B>");htmlContents.add("</td>");
				htmlContents.add("<td width='"+colWidth[4]+"'><B>"+caption[4]+"</B>");htmlContents.add("</td>");
				htmlContents.add("</tr>");

				for(int index=0; index<notifications.size(); index++){
					notificationHistory = (NotificationHistory)notifications.get(index);
					htmlContents.add("<tr>");
					if(nodeType==CSSConstants.nodeType_Document || nodeType == CSSConstants.nodeType_DT ||
							nodeType == CSSConstants.nodeType_Folder || nodeType == CSSConstants.nodeType_Retention || 
							nodeType == CSSConstants.nodeType_Route){
						
						htmlContents.add("<td width='"+colWidth[0]+"'>");
						htmlContents.add(""+notificationHistory.getNodeName());
						htmlContents.add("</td>");
					}
			        
					htmlContents.add("<td width='"+colWidth[1]+"'>");
					//htmlContents.add(""+getNotifyEventName(notificationHistory.getNodifyId()));
					htmlContents.add(""+notificationMasterTable.get(notificationHistory.getNodifyId()));
					htmlContents.add("</td>"); 	
					htmlContents.add("<td width='"+colWidth[2]+"'>");
					htmlContents.add(""+notificationHistory.getDesc());
					htmlContents.add("</td>");
					htmlContents.add("<td width='"+colWidth[3]+"'>");
					htmlContents.add(""+notificationHistory.getModifiedBy());
					htmlContents.add("</td>");
					htmlContents.add("<td width='"+colWidth[4]+"'>");
					htmlContents.add(""+notificationHistory.getCreated());
					htmlContents.add("</td>");
					htmlContents.add("</tr>");
					mailIdstr = mailIdstr+notificationHistory.getMailIds()+";";
				}
				mailIdstr = mailIdstr.substring(0,(mailIdstr.length()-1));

				htmlContents.add("</table>");
				htmlContents.add("</body>");
				htmlContents.add("</html>");
			}
		}catch(Exception ex){
			//VNSLog.add("exception in htmlContents");
		}

		int ret = 0;
		try{
			Vector<String> attachmentDatas = new Vector<String>();
			attachmentDatas.add(historyFilePath);
			mailMessage = convertVectorToString(htmlContents);
			//mailIds - send as single string seperated by semicolan after removal of duplication.
			String tempStr = "";
			for(int count=0; count<mailIds.size(); count++){
				tempStr+= mailIds.get(count).toString()+";";
			}
			if(tempStr.length()>0)
				tempStr = tempStr.substring(0,tempStr.length()-1);
			
			mailIds = new Vector();
			mailIds.add(tempStr);
			ret = vws.sendNotificationAlert(this.roomName, this.sessionID, mailIds, htmlContents, attachmentDatas, status, nodeType, docId, attachmentExist, nodeName, attempt);
			switch(ret){
				case -1:
					VNSLog.war("Error in Notification sending mail. Check mail server settings.");
					break;
				case -2:
					VNSLog.war("Error in Notification sending mail. Please check the user(s) mail id.");
					break;
			}
		}catch(Exception ex){
			//VNSLog.add("Exception in getting the mail sendNotificationMail : "+ex.getMessage());
		}
		return ret;
	}

	public String convertVectorToString(Vector htmlContents){
		String mailMessage = "";
		if(htmlContents!=null && htmlContents.size()>0)
			for (int i=0; i<htmlContents.size(); i++){
				mailMessage +=htmlContents.get(i).toString();
			}
		return mailMessage;
	}
	
	
	public String getNotifyEventName (int notifyId){
		String notifyType = "";
		switch(notifyId){
		case CSSConstants.notify_Doc_CheckIn:
			notifyType = CSSConstants.str_Doc_CheckIn;
			break;
		case CSSConstants.notify_Doc_Checkout:
			notifyType = CSSConstants.str_Doc_Checkout;
			break;
		case CSSConstants.notify_Doc_CommentModified:
			notifyType = CSSConstants.str_Doc_CommentModified;
			break;
		case CSSConstants.notify_Doc_Freeze:
			notifyType = CSSConstants.str_Doc_Freeze;
			break;
		case CSSConstants.notify_Doc_UnFreeze:
			notifyType = CSSConstants.str_Doc_UnFreeze;
			break;
		case CSSConstants.notify_Doc_Indexed:
			notifyType = CSSConstants.str_Doc_Indexed;
			break;
		case CSSConstants.notify_Doc_Moved:
			notifyType = CSSConstants.str_Doc_Moved;
			break;
		case CSSConstants.notify_Doc_PagesModified:
			notifyType = CSSConstants.str_Doc_PagesModified;
			break;
		case CSSConstants.notify_Doc_PermissionMod:
			notifyType = CSSConstants.str_Doc_PermissionMod;
			break;
		case CSSConstants.notify_Doc_PropModified:
			notifyType = CSSConstants.str_Doc_PropModified;
			break;
		case CSSConstants.notify_Doc_RouteEnded:
			notifyType = CSSConstants.str_Doc_RouteEnded;
			break;
		case CSSConstants.notify_Doc_Signature:
			notifyType = CSSConstants.str_Doc_Signature;
			break;
		case CSSConstants.notify_Fol_AddedDoc:
			notifyType = CSSConstants.str_Fol_AddedDoc;
			break;
		case CSSConstants.notify_Fol_Checkout:
			notifyType = CSSConstants.str_Fol_Checkout;
			break;
		case CSSConstants.notify_Fol_CheckIn:
			notifyType = CSSConstants.str_Fol_CheckIn;
			break;
		case CSSConstants.notify_Fol_CustomCols:
			notifyType = CSSConstants.str_Fol_CustomCols;
			break;
		case CSSConstants.notify_Fol_DocRemoved:
			notifyType = CSSConstants.str_Fol_DocRemoved;
			break;
		case CSSConstants.notify_Fol_NodeProp:
			notifyType = CSSConstants.str_Fol_NodeProp;
			break;
		case CSSConstants.notify_Fol_PermissionMod:
			notifyType = CSSConstants.str_Fol_PermissionMod;
			break;
		case CSSConstants.notify_DT_AssignRoute:
			notifyType = CSSConstants.str_DT_AssignRoute;
			break;
		case CSSConstants.notify_DT_DSNValue:
			notifyType = CSSConstants.str_DT_DSNValue;
			break;
		case CSSConstants.notify_DT_ExternalDB:
			notifyType = CSSConstants.str_DT_ExternalDB;
			break;
		case CSSConstants.notify_DT_VREnabled:
			notifyType = CSSConstants.str_DT_VREnabled;
			break;
		case CSSConstants.notify_Storage_LocationMod:
			notifyType = CSSConstants.str_Storage_LocationMod;
			break;
		case CSSConstants.notify_Storage_LocationMov:
			notifyType = CSSConstants.str_Storage_LocationMov;
			break;
		case CSSConstants.notify_AT_UpdateSet:
			notifyType = CSSConstants.str_AT_UpdateSet;
			break;
		case CSSConstants.notify_Recycle_Empty:
			notifyType = CSSConstants.str_Recycle_Empty;
			break;
		case CSSConstants.notify_Recycle_purgeDocs:
			notifyType = CSSConstants.str_Recycle_purgeDocs;
			break;
		case CSSConstants.notify_Recycle_RestoreDocs:
			notifyType = CSSConstants.str_Recycle_RestoreDocs;
			break;
		case CSSConstants.notify_Restore_NewDT:
			notifyType = CSSConstants.str_Restore_NewDT;
			break;
		case CSSConstants.notify_Restore_NewIndex:
			notifyType = CSSConstants.str_Restore_NewIndex;
			break;
		case CSSConstants.notify_Redaction_RepPassword:
			notifyType = CSSConstants.str_Redaction_RepPassword;
			break;
		case CSSConstants.notify_Retention_Delete:
			notifyType = CSSConstants.str_Retention_Delete;
			break;
		case CSSConstants.notify_Retention_DocExpired:
			notifyType = CSSConstants.str_Retention_DocExpired;
			break;
		case CSSConstants.notify_Retention_Update:
			notifyType = CSSConstants.str_Retention_Update;
			break;
		case CSSConstants.notify_Route_Comp:
			notifyType = CSSConstants.str_Route_Comp;
			break;
		case CSSConstants.notify_Route_Valid_to_Invalid:
			notifyType = CSSConstants.str_Route_Valid_to_Invalid;
			break;
		case CSSConstants.notify_Route_Invalid_to_Valid:
			notifyType = CSSConstants.str_Route_Invalid_to_Valid;
			break;
		case CSSConstants.notify_Route_Delete:
			notifyType = CSSConstants.str_Route_Delete;
			break;
		}
		return notifyType;
	}
	
	public Vector<String> getDocumentAuditHistory(VWClient vwClient, int sid, int docId, Vector<String> auditDetails){

		vwClient.getDocumentAuditDetails(sid, docId, 0, auditDetails);
		
		VWDocumentHistoryInfo vwdocumentHistoryInfo = null;
		Vector<VWDocumentHistoryInfo> data = new Vector<VWDocumentHistoryInfo>();
		for (int i = 0; i < auditDetails.size(); i++) {
			String value = auditDetails.get(i).toString();
			StringTokenizer tokens = new StringTokenizer(value, Util.SepChar);
			try {
				vwdocumentHistoryInfo = new VWDocumentHistoryInfo();
				tokens.nextToken();
				tokens.nextToken();
				tokens.nextToken();
				vwdocumentHistoryInfo.setAuditObjectType("Document");
				vwdocumentHistoryInfo.setAuditObjectName(tokens.nextToken());
				vwdocumentHistoryInfo.setAuditUserName(tokens.nextToken());
				vwdocumentHistoryInfo.setAuditClientHost(tokens.nextToken());
				vwdocumentHistoryInfo.setAuditDate(tokens.nextToken());
				tokens.nextToken();
				vwdocumentHistoryInfo.setAuditLocation(tokens.nextToken());
				vwdocumentHistoryInfo.setAuditDescription(tokens.nextToken());
				tokens.nextToken();
				vwdocumentHistoryInfo.setAuditEvent(tokens.nextToken());
				data.add(vwdocumentHistoryInfo);
				tokens = null;
			} catch (Exception ex) {
				VWClient.printToConsole("Exception in loadAudit Details " + ex.getMessage());
			}
		}
		Vector<String> htmlContents = new Vector<String>();
		
		htmlContents.add("<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.0 Transitional//EN'>");
		htmlContents.add("<html>");
		htmlContents.add("<body>");
		htmlContents.add("<table font='arial' border=1 cellspacing=0 width='100%' id='historyId' cellpadding=0>");
		htmlContents.add("<tr>");
		htmlContents.add("<th align='center' colspan='6'>");
		htmlContents.add("DOCUMENT   HISTORY");
		htmlContents.add("</th>");
		htmlContents.add("</tr>");
		htmlContents.add("<tr>");
		htmlContents.add("<th>Event</th><th>Date</th><th>Username</th><th>Client Host</th><th>Location</th><th>Description</th>");
		htmlContents.add("</tr>");

		for (int i = 0; i < data.size(); i++) {
			vwdocumentHistoryInfo = (VWDocumentHistoryInfo) data.get(i);
			htmlContents.add("<tr>");
			htmlContents.add("<td>");
			htmlContents.add(" "+vwdocumentHistoryInfo.getAuditEvent());
			htmlContents.add("</td>");
			htmlContents.add("<td>");
			htmlContents.add(" "+vwdocumentHistoryInfo.getAuditDate());
			htmlContents.add("</td>");
			htmlContents.add("<td>");
			htmlContents.add(" "+vwdocumentHistoryInfo.getAuditUserName());
			htmlContents.add("</td>");
			htmlContents.add("<td>");
			htmlContents.add(" "+vwdocumentHistoryInfo.getAuditClientHost());
			htmlContents.add("</td>");
			htmlContents.add("<td>");
			htmlContents.add(" "+(vwdocumentHistoryInfo.getAuditLocation()==""?"-":vwdocumentHistoryInfo.getAuditLocation()));
			htmlContents.add("</td>");
			htmlContents.add("<td>");
			htmlContents.add(" "+vwdocumentHistoryInfo.getAuditDescription());
			htmlContents.add("</td>");
			htmlContents.add("</tr>");
		}
		htmlContents.add("</table>");
		htmlContents.add("</body>");
		htmlContents.add("</html>");
		return htmlContents;
	}
	public boolean endForLoop = false;
	public Hashtable<String, Vector> reportTable = new Hashtable<String, Vector>();
	public VWClient vwClient = null;
	public int sessionID;
	public String roomName;
	public void setInfo(VWClient vwClient, int sessionId, String roomName){
		this.vwClient = vwClient;
		this.sessionID = sessionId;
		this.roomName = roomName;
	}
}
