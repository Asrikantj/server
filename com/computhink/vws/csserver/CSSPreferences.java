/*
 * Created on Nov 18, 2011
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package com.computhink.vws.csserver;

import java.rmi.RemoteException;
import java.util.prefs.Preferences;

import com.computhink.common.RoomProperty;
import com.computhink.common.ServerSchema;
import com.computhink.common.Util;
import com.computhink.vws.server.VWSPreferences;

/**
 * @author Vijaypriya.B.K
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class CSSPreferences implements CSSConstants{
	
	private static Preferences CSSPref;
    
    static
    {
        CSSPref= Preferences.systemRoot().node(CSS_PREF_ROOT);
    }
    public static String getHostName()
    {
        return getNode("host").get("name", "");
    }
    public static void setHostName(String name)
    {
        getNode("host").put("name", name);
        flush();
    }
    private static Preferences getNode(String node)
    {
        return CSSPref.node(node);
    }
    public static void setComPort(int port)
    {
        getNode("host").putInt("comport", port);
        flush();
    }
    public static int getComPort()
    {
        return getNode("host").getInt("comport", DEFAULT_COM_PORT);
    }
    public static void setDataPort(int port)
    {
        getNode("host").putInt("dataport", port);
        flush();
    }
    public static int getDataPort()
    {
        return getNode("host").getInt("dataport", DEFAULT_DATA_PORT);
    }
  
    public static boolean getLogInfo()
    {
        return getNode("general").getBoolean("loginfo", true);
    }
    public static void setLogInfo(boolean b)
    {
        getNode("general").putBoolean("loginfo", b);
        flush();
    }
    public static boolean getSSL()
    {
        return getNode("general").getBoolean("ssl", false);
    }
    public static void setSSL(boolean b){
    	getNode("general").putBoolean("ssl", b);
        flush();
    }
    
    public static boolean getDebugInfo()
    {
        return getNode("general").getBoolean("debuginfo", false);
    }
    public static void setDebugInfo(boolean b)
    {
        getNode("general").putBoolean("debuginfo", b);
        flush();
    }
    public static int getFlushLines()
    {
        return getNode("general").getInt("loglines", DEFAULT_LOG_LINES);
    }
    public static void setVWS(ServerSchema vws)
    {
        getNode("vws").put("host", vws.address);
        getNode("vws").putInt("port", vws.comport);
        flush();
    }
    public static ServerSchema getVWS()
    {
        ServerSchema ss = new ServerSchema();
        ss.address = getNode("vws").get("host", "");
        ss.comport = getNode("vws").getInt("port", 0);
        ss.type = SERVER_VWS;
        return ss;
    }
    
    public static String getManager()
    {
        String man = getNode("general").get("manager", "admin");
        String pas = getNode("general").get("manpass", "manager");
        if (!pas.equals("manager")) pas = Util.decryptKey(pas);
        return man + "||" + pas;
    }
    public static boolean setManager(String man, String pas)
    {
        try
        {
            pas = Util.encryptKey(pas);
            getNode("general").put("manager", man);
            getNode("general").put("manpass", pas);
            flush();
            return true;
        }
        catch(Exception e){}
        return false;
    }
    
    static private void flush()
    {
        try
        {
        	CSSPref.flush();
        }
        catch(java.util.prefs.BackingStoreException e){}
    }   
    
    public RoomProperty[] getRooms() throws RemoteException
    {
        return VWSPreferences.getRooms();
    }
    /* public static int getProxyPort()
    {
        return getNode("proxy").getInt("port", DEFAULT_PROXY_PORT);
    }*/
    /* public static void setProxy(boolean b)
    {
        getNode("proxy").putBoolean("enabled", b);
        flush();
    }
    public static void setProxyHost(String server)
    {
        getNode("proxy").put("host", server);
        flush();
    }
    public static void setProxyPort(int port)
    {
        getNode("proxy").putInt("port", port);
        flush();
    }
    public static boolean getProxy()
    {
        return getNode("proxy").getBoolean("enabled", false);
    }
    public static String getProxyHost()
    {
        return getNode("proxy").get("host", "");
    }*/
	
    public static void setLogFileCount(int count)
    {
        getNode("general").putInt("maxlogfilecount", count);
        flush();
    }    
    public static int getLogFileCount()
    {
        return getNode("general").getInt("maxlogfilecount",100);
    }
    
    public static String getEmailServerName()
    {
        return getNode("general").get("emailServerName", "");
    }
    public static void setEmailServerName(String emailServerName){
    	getNode("general").put("emailServerName", emailServerName);
        flush();
    }
    public static String getEmailServerPort()
    {
        return getNode("general").get("emailServerPort", "");
    }
    public static void setEmailServerPort(String emailServerPort){
    	getNode("general").put("emailServerPort", emailServerPort);
        flush();
    }
    public static String getEmailServerUsername()
    {
        return getNode("general").get("emailServerUsername", "");
    }
    public static void setEmailServerUsername(String emailServerUsername){
    	getNode("general").put("emailServerUsername", emailServerUsername);
        flush();
    }
    public static String getEmailServerPassword()
    {
    	String ePassword="";
    	byte eServerPassword[] = getNode("general").getByteArray("emailServerPassword", null);
    	try{
	    	if(eServerPassword==null)
	    		ePassword = Util.decryptKey(new String(""));
	    	else
	    		ePassword = Util.decryptKey(new String(eServerPassword));
    	}catch(Exception e){

    	}
    	return ePassword;
    }
    public static void setEmailServerPassword(String emailServerPassword)
    {
    	try{
    		getNode("general").putByteArray("emailServerPassword", (Util.encryptKey(emailServerPassword)).getBytes("UTF8"));
    	}catch(Exception e){}
        flush();
    }  
    public static String getFromEmailId()
    {
        return getNode("general").get("fromEmailId", "");
    }
    public static void setFromEmailId(String fromEmailId){
    	getNode("general").put("fromEmailId", fromEmailId);
        flush();
    }
    public static String getCSPollingTime()
    {
    	String csPollingTime = getNode("general").get("csPollingTime", "5");
    	/**
    	 * Issue: Notification service will run in inifinite loop.
    	 * Reason: Notification poll time value is 0.
    	 * Solution: Set Minimum poll time for notification as 5 minutes.
    	 */
    	if(csPollingTime != null && csPollingTime.trim().equalsIgnoreCase("0")){
    		csPollingTime = "5";
    		setCSPollingTime(csPollingTime);
    	}    	
        return csPollingTime;
    }
    public static void setCSPollingTime(String csPollingTime){
    	getNode("general").put("csPollingTime", csPollingTime);
        flush();
    }
    public static String getCSFileSizeLimit(){
    	return getNode("general").get("csFileSizeLimit", "0");
    }
    public static void setCSFileSizeLimit(String csFileSizeLimit){
    	getNode("general").put("csFileSizeLimit", csFileSizeLimit);
        flush();
    }
    public static String getCSRetries(){
    	return getNode("general").get("csRetries", "0");
    }
    public static void setCSRetries(String notificationRetries){
    	getNode("general").put("csRetries", notificationRetries);
        flush();
    }
    public static String getCSEmailId(){
    	return getNode("general").get("csEmailId", "");
    }
    public static void setCSEmailId(String csEmailId){
    	getNode("general").put("csEmailId", csEmailId);
        flush();
    }
	//This is to get the entire dbstring from database - for debugging purpose
    public static boolean getCSDebug(){
    	return getNode("general").getBoolean("csDebug", false);
    }
}
